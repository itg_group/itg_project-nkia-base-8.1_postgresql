package egovframework.com.cmm.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @Class Name : EgovFileMngService.java
 * @Description : 파일정보의 관리를 위한 서비스 인터페이스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 25.     이삼섭    최초생성
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 25.
 * @version
 * @see
 *
 */
public interface EgovFileMngService {

    /**
     * 파일에 대한 목록을 조회한다.
     * 
     * @param fvo
     * @return
     * @throws NkiaException
     */
    public List<FileVO> selectFileInfs(FileVO fvo) throws NkiaException;

    /**
     * 하나의 파일에 대한 정보(속성 및 상세)를 등록한다.
     * 
     * @param fvo
     * @throws NkiaException
     */
    public String insertFileInf(FileVO fvo) throws NkiaException;

    /**
     * 여러 개의 파일에 대한 정보(속성 및 상세)를 등록한다.
     * 
     * @param fvoList
     * @throws NkiaException
     */
    @SuppressWarnings("unchecked")
    public String insertFileInfs(List fvoList) throws NkiaException;

    /**
     * 여러 개의 파일에 대한 정보(속성 및 상세)를 수정한다.
     * 
     * @param fvoList
     * @throws NkiaException
     */
    @SuppressWarnings("unchecked")
    public void updateFileInfs(List fvoList) throws NkiaException;

    /**
     * 여러 개의 파일을 삭제한다.
     * 
     * @param fvoList
     * @throws NkiaException
     */
    @SuppressWarnings("unchecked")
    public void deleteFileInfs(List fvoList) throws NkiaException;

    /**
     * 하나의 파일을 삭제한다.
     * 
     * @param fvo
     * @throws NkiaException
     */
    public void deleteFileInf(FileVO fvo) throws NkiaException;

    /**
     * 파일에 대한 상세정보를 조회한다.
     * 
     * @param fvo
     * @return
     * @throws NkiaException
     */
    public FileVO selectFileInf(FileVO fvo) throws NkiaException;

    /**
     * 파일 구분자에 대한 최대값을 구한다.
     * 
     * @param fvo
     * @return
     * @throws NkiaException
     */
    public int getMaxFileSN(FileVO fvo) throws NkiaException;

    /**
     * 전체 파일을 삭제한다.
     * 
     * @param fvo
     * @throws NkiaException
     */
    public void deleteAllFileInf(FileVO fvo) throws NkiaException;

    /**
     * 파일명 검색에 대한 목록을 조회한다.
     * 
     * @param fvo
     * @return
     * @throws NkiaException
     */
    public Map<String, Object> selectFileListByFileNm(FileVO fvo) throws NkiaException;

    /**
     * 이미지 파일에 대한 목록을 조회한다.
     * 
     * @param vo
     * @return
     * @throws NkiaException
     */
    public List<FileVO> selectImageFileList(FileVO vo) throws NkiaException;
}
