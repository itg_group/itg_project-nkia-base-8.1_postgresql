/**
 * 
 */
package egovframework.com.cmm.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.nkia.itg.base.application.filter.SQLInjectionFilter;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * EgovComAbstractDAO.java 클래스
 * 
 * @author 서준식
 * @since 2011. 9. 23.
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2011. 9. 23.   서준식        최초 생성
 *   2013. 5. 14.   정영태        Exception 정책에 의해서 ItgAbstractDAO로 변경
 * </pre>
 */
public abstract class SubComAbstractDAO extends EgovAbstractDAO {
	
	@Resource(name="egov.sqlMapClient")
	public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
        super.setSuperSqlMapClient(sqlMapClient);
    }
	
	@Override
	public Object selectByPk(String queryId, Object parameterObject) {
		if(parameterObject instanceof Map) {			
			SQLInjectionFilter.runFilter((Map)parameterObject);
		}
        return getSqlMapClientTemplate().queryForObject(queryId, parameterObject);
    }
    

    @Override
    public List list(String queryId, Object parameterObject) {
		if(parameterObject instanceof Map) {
			SQLInjectionFilter.runFilter((Map)parameterObject);
		} 
        return getSqlMapClientTemplate().queryForList(queryId, parameterObject);
    }
}
