/**
 * 
 */
package egovframework.com.cmm.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.nkia.itg.base.application.filter.SQLInjectionFilter;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * EgovComAbstractDAO.java 클래스
 * 
 * @author 서준식
 * @since 2011. 9. 23.
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2011. 9. 23.   서준식        최초 생성
 *   2013. 5. 14.   정영태        Exception 정책에 의해서 ItgAbstractDAO로 변경
 * </pre>
 */
public abstract class EgovComAbstractDAO extends EgovAbstractDAO {
	
	private static final Logger logger = LoggerFactory.getLogger("ITG.DAO.LOG");
	
	@Resource(name="egov.sqlMapClient")
	public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
        super.setSuperSqlMapClient(sqlMapClient);
    }
	
	@Override
	public Object selectByPk(String queryId, Object parameterObject) {
		if(parameterObject instanceof Map) {			
			SQLInjectionFilter.runFilter((Map)parameterObject);
		}
		printQueryIdLog(queryId);
		long start = System.currentTimeMillis();
		
		Object result = super.selectByPk(queryId, parameterObject);
		
		long end = System.currentTimeMillis();
		printWaitingTimeLog(start, end, queryId);
        
		return result;
    }

	@Override
    public List list(String queryId, Object parameterObject) {
		if(parameterObject instanceof Map) {
			SQLInjectionFilter.runFilter((Map)parameterObject);
		}
		printQueryIdLog(queryId);
		long start = System.currentTimeMillis();
		
		List result = super.list(queryId, parameterObject);
		
		long end = System.currentTimeMillis();
		printWaitingTimeLog(start, end, queryId);
        return result;
    }
    
    public Object insert(String queryId, Object parameterObject) {
    	printQueryIdLog(queryId);
    	long start = System.currentTimeMillis();
    	
    	Object result = super.insert(queryId, parameterObject);
    	
    	long end = System.currentTimeMillis();
    	printWaitingTimeLog(start, end, queryId);
        return result;
    }
    
    public int update(String queryId, Object parameterObject) {
    	printQueryIdLog(queryId);
    	long start = System.currentTimeMillis();
    	
    	int result = super.update(queryId, parameterObject);
    	
    	long end = System.currentTimeMillis();
    	printWaitingTimeLog(start, end, queryId);
        return result;
    }
    
    public int delete(String queryId, Object parameterObject) {
    	printQueryIdLog(queryId);
    	long start = System.currentTimeMillis();
    	
    	int result = super.delete(queryId, parameterObject);
    	
    	long end = System.currentTimeMillis();
    	printWaitingTimeLog(start, end, queryId);
        return result;
    }
    
    private void printQueryIdLog(String queryId) {
    	logger.info("Execute query : [" + queryId + "]");
    }
    
    private void printWaitingTimeLog(long start, long end, String queryId) {
    	logger.info("[" + queryId + "] Waiting time : " + String.valueOf((end-start)/(double)1000) + "sec");
    }
}
