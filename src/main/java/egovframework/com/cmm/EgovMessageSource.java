package egovframework.com.cmm;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * 메시지 리소스 사용을 위한 MessageSource 인터페이스 및 ReloadableResourceBundleMessageSource 클래스의 구현체
 * @author 공통서비스 개발팀 이문준
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2009.03.11  이문준          최초 생성
 *
 * </pre>
 */

public class EgovMessageSource extends ReloadableResourceBundleMessageSource implements MessageSource {

	private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;

	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @param reloadableResourceBundleMessageSource - resource MessageSource
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
		this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
	}
	
	/**
	 * getReloadableResourceBundleMessageSource() 
	 * @return ReloadableResourceBundleMessageSource
	 */	
	public ReloadableResourceBundleMessageSource getReloadableResourceBundleMessageSource() {
		return reloadableResourceBundleMessageSource;
	}
	
	/**
	 * 정의된 메세지 조회
	 * @param code - 메세지 코드
	 * @return String
	 */	
	public String getMessage(String code) {
		String msg = null;
		try {
			Locale locale = new Locale("ko", "KR");
			msg = getReloadableResourceBundleMessageSource().getMessage(code, null, LocaleContextHolder.getLocale());
		} catch (Exception e) {
			return msg;
		}
		return msg;
	}
	
	/**
	 * 
	 * 정의된 Message Argument에 의한 Return
	 * 
	 * @param code
	 * @param args
	 * @return
	 * @throws NkiaException
	 */
	public String getMessage(String code, Object[] args)throws NkiaException {
		String msg = null;
		try {
			Locale locale = new Locale("ko", "KR");
			msg = getReloadableResourceBundleMessageSource().getMessage(code, args, LocaleContextHolder.getLocale());
		} catch (Exception e) {
			throw e;
		}
		return msg;
	}
	
}
