package com.nkia.intergration.polestar.enums;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;

public enum IntergrationPolestarEnums {
	
	DCA_WEB_SERVER_IP(NkiaApplicationPropertiesMap.getProperty("Dca.Web.Server.Ip")),
	DCA_WEB_SERVER_PORT(NkiaApplicationPropertiesMap.getProperty("Dca.Web.Server.Port")),
	DCA_AGENT_PORT(NkiaApplicationPropertiesMap.getProperty("Dca.Agent.Port")),
	DCA_NETWORK_SNMP_PORT(NkiaApplicationPropertiesMap.getProperty("Dca.Network.Snmp.Port")),
	DCA_FIRST_MANAGER_IP(NkiaApplicationPropertiesMap.getProperty("Dca.First.Manager.Ip")),
	DCA_SECOND_MANAGER_IP(NkiaApplicationPropertiesMap.getProperty("Dca.Second.Manager.Ip")),
	DCA_FIRST_REGION_CODE(NkiaApplicationPropertiesMap.getProperty("Dca.First.Manager.Code")),
	DCA_SECOND_REGION_CODE(NkiaApplicationPropertiesMap.getProperty("Dca.Second.Manager.Code"));
		
	private String value;
	
	IntergrationPolestarEnums(String value){
		this.value = value;
    }
	
	public String toString() {
		return this.value;
	}
}
