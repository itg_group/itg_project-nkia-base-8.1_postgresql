/**
 * cygnus-management::com.nkia.cygnus.management.service.report.oz.OZParameter.java
 * Create : 2015. 5. 28.
 *
 * Copyright 2015 Nkia.com, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.enums;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 * OZParameter
 *
 * @version <tt>Revision: 1.0</tt> 2015. 5. 28.
 * @author <a href="mailto:ssyoo@nkia.co.kr">Yoo Sung Soo</a>
 */
public class OZParameter {
	
	public static final String OZ_PARM_SEP = "@OZSEP@";
	public static final String OZ_EXT = "pdf";		// 확장자 "pdf,xlsx" 등 콤마로 연결하여 사용 가능.
	
	public OZParameter() {
	}
	
	public String getServletUrl() {
		return servletUrl;
	}

	public void setServletUrl(String servletUrl) {
		this.servletUrl = servletUrl;
	}

	public String getReportKey() {
		return reportKey;
	}

	public void setReportKey(String reportKey) {
		this.reportKey = reportKey;
	}

	public String getExportFileName() {
		return exportFileName;
	}

	public void setExportFileName(String exportFileName) {
		this.exportFileName = exportFileName;
	}

//	public List<ReportFormat> getExportFormats() {
//		return exportFormats;
//	}
//
//	public void setExportFormats(List<ReportFormat> exportFormats) {
//		this.exportFormats = exportFormats;
//	}

	public String getExportPath() {
		return exportPath;
	}

	public void setExportPath(String exportPath) {
		this.exportPath = exportPath;
	}

	private String servletUrl;
	private String reportKey;
	private String exportFileName;
//	private List<ReportFormat> exportFormats;
	private String exportPath;
	
	private HashMap<String, String> userProperty = new HashMap<String, String>();
	
	public void putUserProperty(String key, String value) {
		this.userProperty.put(key, value);
	}
	
	public String removeUserProperty(String key) {
		return this.userProperty.remove(key);
	}
	
	public String getParamString(){
		if(!checkVal()){
			throw new RuntimeException("OZParameterException");
		}
		StringBuilder sb = new StringBuilder();
		sb.append("connection.servlet=").append(this.servletUrl).append(OZParameter.OZ_PARM_SEP);
		sb.append("connection.reportname=").append(this.reportKey+".ozr").append(OZParameter.OZ_PARM_SEP);
		
		this.setUserPropertyParameter(sb);
		
		sb.append("export.filename=").append(this.exportFileName).append(OZParameter.OZ_PARM_SEP);
		sb.append("export.format=").append(OZParameter.OZ_EXT).append(OZParameter.OZ_PARM_SEP);
		
//		for(ReportFormat format : exportFormats){
//			sb.append(format.toExtString()).append(";");
//		}
//		sb.append(".ozd").append(OZParameter.OZ_PARM_SEP);
		//FORCS 가이드로 확장자 앞에 있는  [.] 제거와 구분자를  [;] 에서  [,] 로 변경
//		for(ReportFormat format : exportFormats){
//			sb.append(format.getExtString()).append(",");
//		}
//		sb.append("ozd").append(OZParameter.OZ_PARM_SEP);
		
		sb.append("export.path=").append(this.exportPath).append(OZParameter.OZ_PARM_SEP);
		
		this.setDefaultParameter(sb);
		
		return sb.toString();
	}
	
	public boolean checkVal(){
		return this.servletUrl!=null && this.reportKey!=null 
				&& this.exportFileName!=null 
//				&& this.exportFormats!=null 
				&& this.exportPath!=null;
	}
	
	private final static String DEFAULT_EXPORT_MODE = "export.mode=silent";
	private final static String DEFAULT_EXPORT_CONFIRMSAVE = "export.confirmsave=false";
	
	private final static String DEFAULT_VIEWER_MODE = "viewer.mode=export";
	private final static String DEFAULT_VIEWER_USE_PROGRESS_BAR = "viewer.useprogressbar=false";
	private final static String DEFAULT_VIEWER_ISFRAME = "viewer.isframe=false";
	private final static String DEFAULT_VIEWER_ERROR_COMMAND = "viewer.errorcommand=true";
	private final static String DEFAULT_VIEWER_SHOW_ERROR_MESSAGE = "viewer.showerrormessage=false";
	
	private final static String DEFAULT_TEXT_SEPARATOR = "text.separator=comma";
	private final static String DEFAULT_TEXT_EXCEPTFIRSTPAGE = "text.exceptfirstpage=false";
	private final static String DEFAULT_TEXT_ADDSEPARATOR = "text.addseparator=true";
	private final static String DEFAULT_TEXT_REMOVESEPARATOR = "text.removeseparator=false";
	private final static String DEFAULT_TEXT_SAVEASTABLE = "text.saveastable=false";
	private final static String DEFAULT_TEXT_CHARSET = "text.charset=ansi";
	
	// PDF
	private final static String DEFAULT_PDF_FONT_EMBEDDING = "pdf.fontembedding=true";
	
	// Excel 공백 제거 ( 2017-03-23 kdmcom 추가 )
	private final static String DEFAULT_EXCEL_REMOVEBLANK = "excel.removeblank=true";		// 페이지 공백제거 ( XLS )
	//private final static String DEFAULT_EXCEL_LARGEBUNDLE = "excel.largebundle=true";		// 한페이지 처리 ( XLS - 최대row 65535 )
	//private final static String DEFAULT_XLSX_LARGEBUNDLE = "xlsx.largebundle=true";		// 한페이지 처리 ( XLSX - 1048576)
	private final static String DEFAULT_XLSX_REMOVEBLANK = "xlsx.removeblank=true";			// 페이지 공백제거 ( XLSX )
	
	// Word 
	private final static String DEFAULT_WORD_SAVEASXML = "word.saveasxml=true";
	private final static String DEFAULT_WORD_SAVEASTABLE = "word.saveastable=false";
	
	// 대용량 데이터 처리
	private final static String DEFAULT_CONNECTION_FETCHTYPE = "connection.fetchtype=CONCURRENT";
	private final static String DEFAULT_CONNECTION_PAGEQUEBUNDLE = "connection.pagequebundle=10";
	private final static String DEFAULT_CONNECTION_PAGEQUE = "connection.pageque=10";
	private final static String DEFAULT_CONNECTION_SERVERDMTYPE = "connection.serverdmtype=MEMORY";
	private final static String DEFAULT_CONNECTION_CLIENTDMTYPE = "connection.clientdmtype=MEMORY";
	private final static String DEFAULT_CONNECTION_COMPRESSEDDATAMODULE = "connection.compresseddatamodule=false";
	private final static String DEFAULT_CONNECTION_COMPRESSEDFORM = "connection.compressedForm=false";
	
	private void setDefaultParameter(StringBuilder sb) {
		sb.append(DEFAULT_EXPORT_MODE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_EXPORT_CONFIRMSAVE).append(OZParameter.OZ_PARM_SEP);
		
		sb.append(DEFAULT_VIEWER_MODE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_VIEWER_USE_PROGRESS_BAR).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_VIEWER_ISFRAME).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_VIEWER_ERROR_COMMAND).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_VIEWER_SHOW_ERROR_MESSAGE).append(OZParameter.OZ_PARM_SEP);
		
		sb.append(DEFAULT_TEXT_SEPARATOR).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_TEXT_EXCEPTFIRSTPAGE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_TEXT_ADDSEPARATOR).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_TEXT_REMOVESEPARATOR).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_TEXT_SAVEASTABLE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_TEXT_CHARSET).append(OZParameter.OZ_PARM_SEP);
		
		sb.append(DEFAULT_PDF_FONT_EMBEDDING).append(OZParameter.OZ_PARM_SEP);
		
		sb.append(DEFAULT_EXCEL_REMOVEBLANK).append(OZParameter.OZ_PARM_SEP);
		//sb.append(DEFAULT_EXCEL_LARGEBUNDLE).append(OZParameter.OZ_PARM_SEP);
		//sb.append(DEFAULT_XLSX_LARGEBUNDLE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_XLSX_REMOVEBLANK).append(OZParameter.OZ_PARM_SEP);
		
		sb.append(DEFAULT_WORD_SAVEASXML).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_WORD_SAVEASTABLE).append(OZParameter.OZ_PARM_SEP);
		
		sb.append(DEFAULT_CONNECTION_FETCHTYPE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_CONNECTION_PAGEQUEBUNDLE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_CONNECTION_PAGEQUE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_CONNECTION_SERVERDMTYPE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_CONNECTION_CLIENTDMTYPE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_CONNECTION_COMPRESSEDDATAMODULE).append(OZParameter.OZ_PARM_SEP);
		sb.append(DEFAULT_CONNECTION_COMPRESSEDFORM).append(OZParameter.OZ_PARM_SEP);
	}
	
	private void setUserPropertyParameter(StringBuilder sb) {
		if(this.userProperty.size() > 0){
			
			setParamVal(sb, "connection.pcount", Integer.toString(this.userProperty.size()));
			sb.append(OZParameter.OZ_PARM_SEP);
			int cnt=0;
			for(Entry<String, String> entry : userProperty.entrySet()){
				String key = "connection.args" + (++cnt);
				setParamVal(sb, key, entry.getKey()+"="+entry.getValue());
				sb.append(OZParameter.OZ_PARM_SEP);
			}
		}
	}
	
	private void setParamVal(StringBuilder sb, String key, String val){
		sb.append(key);
		sb.append("=");
		sb.append(val);
	}

}
