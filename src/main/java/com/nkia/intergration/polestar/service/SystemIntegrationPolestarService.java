/*
 * @(#)PolestarService.java              2014. 1. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SystemIntegrationPolestarService {

	public List searchServerListForDcaRegist(ModelMap paramMap)throws NkiaException;

	public int searchServerListForDcaRegistCount(ModelMap paramMap)throws NkiaException;
	
	public List searchNetworkListForDcaRegist(ModelMap paramMap)throws NkiaException;

	public int searchNetworkListForDcaRegistCount(ModelMap paramMap)throws NkiaException;
	
	public List searchServerListForDcaRemove(ModelMap paramMap)throws NkiaException;

	public int searchServerListForDcaRemoveCount(ModelMap paramMap)throws NkiaException;

	public List searchNetworkListForDcaRemove(ModelMap paramMap)throws NkiaException;

	public int searchNetworkListForDcaRemoveCount(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * DCA 요청
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap sendDcaRequest(ModelMap paramMap) throws NkiaException;

	public List searchDcaRegistInfo(ModelMap paramMap) throws NkiaException;

	public int searchDcaRegistInfoCount(ModelMap paramMap) throws NkiaException;

	public void mergeIfAdDeviceStatus(HashMap adData, String adPath, String adType)throws NkiaException;

	public List searchIfAdDeviceStatusInfo(ModelMap paramMap)throws NkiaException;

	public int searchIfAdDeviceStatusInfoCount(ModelMap paramMap)throws NkiaException;

	public List searchAmSvNotLogicalList(ModelMap paramMap)throws NkiaException;

	public int searchAmSvNotLogicalListCount(ModelMap paramMap)throws NkiaException;

	public List searchAmNwApNotAdSyncList(ModelMap paramMap)throws NkiaException;

	public int searchAmNwApNotAdSyncListCount(ModelMap paramMap)throws NkiaException;

	public List searchAmSsNotAdSyncList(ModelMap paramMap)throws NkiaException;

	public int searchAmSsNotAdSyncListCount(ModelMap paramMap)throws NkiaException;

	public List searchAmStNotAdSyncList(ModelMap paramMap)throws NkiaException;

	public int searchAmStNotAdSyncListCount(ModelMap paramMap)throws NkiaException;
	
	public String createAssetId(HashMap paramMap) throws NkiaException;
	
	public String createConfId(HashMap paramMap) throws NkiaException;
	
	public void registPhysiServer(ModelMap paramMap)throws NkiaException;
	
	public void mappingPhysiServer(ModelMap paramMap)throws NkiaException;
	
	public void registVmServer(ModelMap paramMap)throws NkiaException;
	
	public void mappingNotServer(ModelMap paramMap)throws NkiaException;
	
	public void insertRegistNw(ModelMap paramMap)throws NkiaException;
	
	public List searchAdSvLongTermList(ModelMap paramMap)throws NkiaException;

	public int searchAdSvLongTermListCount(ModelMap paramMap)throws NkiaException;

	public String checkAdDevice(ModelMap paramMap)throws NkiaException;

	public void updateIfAdDeviceStatusCheckDeviceMsgByConfId(HashMap wsResultData)throws NkiaException;
	

}
