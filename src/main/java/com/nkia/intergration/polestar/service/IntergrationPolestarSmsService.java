package com.nkia.intergration.polestar.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface IntergrationPolestarSmsService {

	List<HashMap> searchEmsServerOsList() throws NkiaException;

	List<HashMap> searchEmsServerCpusList() throws NkiaException;

	List<HashMap> searchEmsServerDisksList() throws NkiaException;
	
	List<HashMap> searchEmsServerDiskList() throws NkiaException;

	List<HashMap> searchEmsServerMemoryList() throws NkiaException;

	List<HashMap> searchEmsServerNwInterfaceList() throws NkiaException;
	
	List<HashMap> searchEmsDBMariaDBList() throws NkiaException;
	
	List<HashMap> searchEmsDBMssqlList() throws NkiaException;
	
	List<HashMap> searchEmsDBMysqlList() throws NkiaException;

	List<HashMap> searchEmsDBOracleList() throws NkiaException;
	
	List<HashMap> searchEmsDBPostgreSQLList() throws NkiaException;
	
	List<HashMap> searchEmsDBTiberoList() throws NkiaException;
	
	List<HashMap> searchEmsWasInstanceList() throws NkiaException;
	
	List<HashMap> searchEmsNetworkInfoList() throws NkiaException;
	
	List<HashMap> searchEmsTreeList() throws NkiaException;
	
	List<HashMap> searchVmsEmsMapList() throws NkiaException;
	
	List<HashMap> searchVmsClusterList() throws NkiaException;
	
	List<HashMap> searchVmsPhysicalList() throws NkiaException;
	
	List<HashMap> searchVmsLogicalList() throws NkiaException;
	
	List<HashMap> searchVmsList() throws NkiaException;
	
	List<HashMap> searchWebURLList() throws NkiaException;
	
	// 20200323_이원재_001_신규작성
	List<HashMap> searchCommonAlarmList(Map paramMap) throws NkiaException;
	
	List<HashMap> searchReportGroupList() throws NkiaException;
}
