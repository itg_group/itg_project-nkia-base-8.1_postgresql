/*
 * @(#)IntergrationPolestarServiceImpl.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.polestar.dao.IntergrationPolestarDcaDAO;
import com.nkia.intergration.polestar.service.IntergrationPolestarService;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("intergrationPolestarService")
public class IntergrationPolestarServiceImpl implements IntergrationPolestarService{

	@Resource(name = "intergrationPolestarDcaDAO")
	public IntergrationPolestarDcaDAO intergrationPolestarDcaDAO;
	
	/**
	 * DCA 미등록 자산 VM목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaVmList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaVmList(); 
	}
	
	/**
	 * DCA 서버 OA 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaServerOaList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerOaList();
	}
	
	/**
	 * DCA 서버 CPUS 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaServerCpusList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerCpusList();
	}
	
	/**
	 * DCA 서버 Disks 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override 
	public List<HashMap> searchDcaServerDisksList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerDisksList();
	}
	
	/**
	 * DCA 서버 Disk 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaServerDiskList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerDiskList();
	}
	
	/**
	 * DCA 서버 Memory 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaServerMemoryList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerMemoryList();
	}
	
	/**
	 * DCA 서버 네트워크 Interfaces 목록
	 * @return
	 * @throws NkiaException
	 */  
	@Override
	public List<HashMap> searchDcaServerNwInterfacesList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerNwInterfacesList();
	}
	
	/**
	 * DCA 서버 네트워크 Interface 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaServerNwInterfaceList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerNwInterfaceList();
	}
	
	/**
	 * DCA 서버 HBA 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaServerHbaList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaServerHbaList();
	}
	
	/**
	 * DCA 네트워크 시스템 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaNwSystemInfoList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaNwSystemInfoList();
	}
	
	/**
	 * DCA 네트워크 > 네트워크 인터페이스 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaNwNetworkInterfaceList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaNwNetworkInterfaceList();
	}
	
	/**
	 * DCA 설치 소프트웨어 수집 목록
	 * @return
	 * @throws NkiaException
	 */ 
	@Override
	public List searchDcaInstalledSwList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaInstalledSwList();
	}
	
	/**
	 * DCA 소프트웨어 수집 마스터 목록
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaSwObjectList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaSwObjectList();
	}

	@Override
	public List<HashMap> searchDcaNwNetworkModuleList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaNwNetworkModuleList();
	}

	@Override
	public List selectUnregistAmServerLogicPartitionByDcaServer()throws NkiaException {
		return intergrationPolestarDcaDAO.selectUnregistAmServerLogicPartitionByDcaServer();
	}

	@Override
	public List selectSmsAgentList()throws NkiaException {
		return intergrationPolestarDcaDAO.selectSmsAgentList();
	}
	
	
	/**
	 * HBA 수량 리스트
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDcaInventoryHbaCntList() throws NkiaException {
		return intergrationPolestarDcaDAO.searchDcaInventoryHbaCntList();
	}
}
