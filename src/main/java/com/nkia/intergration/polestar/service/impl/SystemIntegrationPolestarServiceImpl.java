/*
 * @(#)PolestarServiceImpl.java              2014. 1. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.intergration.polestar.dao.SystemIntegrationPolestarDAO;
import com.nkia.intergration.polestar.enums.IntergrationPolestarEnums;
import com.nkia.intergration.polestar.job.service.ItgJobService;
import com.nkia.intergration.polestar.service.SystemIntegrationPolestarService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.client.WsDcaClient;
import com.nkia.itg.interfaces.util.WsUtil;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;

//@Service("systemIntegrationPolestarService")
public class SystemIntegrationPolestarServiceImpl implements SystemIntegrationPolestarService{
	
	private static final String msgAdToAmSyncSuccess = "Agent 등록완료";
	private static final String msgCodeAdToAmSyncSuccess = "COMPLETE";
	 
	private static final String msgCodeAgentSyncFail = "AD_SYNC_FAIL";
	
	
	private static final String msgNotAgentVm = "설치된 Agent정보가 없습니다.";
	private static final String msgCodeNotAgentVm = "AD_NOT_INSTALL";

	@Resource(name = "opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;
	
	@Resource(name = "systemIntegrationPolestarDAO")
	public SystemIntegrationPolestarDAO systemIntegrationPolestarDAO;	
	
	@Resource(name = "wsDcaClient")
	private WsDcaClient wsDcaClient;
	
	@Resource(name = "wsUtil")
	private WsUtil wsUtil; 
	
	@Resource(name = "itgJobService")
	private ItgJobService itgJobService;

	@Override
	public List searchServerListForDcaRegist(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchServerListForDcaRegist(paramMap);
	}

	@Override
	public int searchServerListForDcaRegistCount(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchServerListForDcaRegistCount(paramMap);
	}
	
	@Override
	public List searchServerListForDcaRemove(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchServerListForDcaRemove(paramMap);
	}

	@Override
	public int searchServerListForDcaRemoveCount(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchServerListForDcaRemoveCount(paramMap);
	}
	
	@Override
	public List searchNetworkListForDcaRegist(ModelMap paramMap)throws NkiaException {
		paramMap.put("snmp_port", IntergrationPolestarEnums.DCA_NETWORK_SNMP_PORT.toString());
		return systemIntegrationPolestarDAO.searchNetworkListForDcaRegist(paramMap);
	}

	@Override
	public int searchNetworkListForDcaRegistCount(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchNetworkListForDcaRegistCount(paramMap);
	}
	
	@Override
	public List searchNetworkListForDcaRemove(ModelMap paramMap)throws NkiaException {
		paramMap.put("snmp_port", IntergrationPolestarEnums.DCA_NETWORK_SNMP_PORT.toString());
		return systemIntegrationPolestarDAO.searchNetworkListForDcaRemove(paramMap);
	}

	@Override
	public int searchNetworkListForDcaRemoveCount(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchNetworkListForDcaRemoveCount(paramMap);
	}
	
	/**
	 * 
	 * DCA 요청
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public HashMap sendDcaRequest(ModelMap paramMap) throws NkiaException {
		HashMap resultMap = new HashMap();
		String dcaResultXml = null;
		
		String requestType = (String)paramMap.get("request_type");
		String requestAction = (String)paramMap.get("request_action");
		String requestTangibleAssetYn = (String)paramMap.get("request_tangible_asset_yn");
		boolean isUpdateAmAsset = (Boolean)paramMap.get("is_am_update_asset");
		List assetList = (List)paramMap.get("asset_list");
		if( assetList != null && assetList.size() > 0 ){
			String dcaAgentPort = IntergrationPolestarEnums.DCA_AGENT_PORT.toString();
			if( requestType.equalsIgnoreCase("SERVER") ){
				String dcaServerXml = wsUtil.parserRequestDcaServerXml(assetList, dcaAgentPort);
				if( requestAction.equalsIgnoreCase("REGIST") ){
					dcaResultXml = wsDcaClient.sendDcaServerRegist(dcaServerXml);	
				}else if( requestAction.equalsIgnoreCase("REMOVE") ){
					dcaResultXml = wsDcaClient.sendDcaServerRemove(dcaServerXml);
				}
			}else if( requestType.equalsIgnoreCase("NETWORK") ){
				String dcaNetworkXml = wsUtil.parserRequestDcaNetworkXml(assetList);
				if( requestAction.equalsIgnoreCase("REGIST") ){
					dcaResultXml = wsDcaClient.sendDcaNetworkRegist(dcaNetworkXml);	
				}else if( requestAction.equalsIgnoreCase("REMOVE") ){
					dcaResultXml = wsDcaClient.sendDcaNetworkRemove(dcaNetworkXml);
				}
			}else if( requestType.equalsIgnoreCase("SNMP") ){
				String dcaSnmpXml = wsUtil.parserRequestDcaSnmpXml(assetList);
				dcaResultXml = wsDcaClient.sendDcaSnmpRequest(dcaSnmpXml);	
			}
			if( dcaResultXml != null && dcaResultXml.length() > 0 ){
				List wsResultList = wsUtil.parserResultDcaXml(dcaResultXml, assetList);
				if( wsResultList != null && wsResultList.size() > 0 ){
					processWsDcaResult(wsResultList, requestType, requestAction, requestTangibleAssetYn, isUpdateAmAsset, resultMap);
				}
			}
		}
		
		return resultMap;
	}
	
	/**
	 * 
	 * DCA 결과처리
	 * 
	 * @param wsResultList
	 * @param resultMap
	 * @throws NkiaException
	 */
	private void processWsDcaResult(List wsResultList, String requestType, String requestAction, String requestTangibleAssetYn, boolean isUpdateAmAsset, HashMap resultMap)throws NkiaException {
		int successCount = 0;
		int failCount = 0;
		for( int i = 0; i < wsResultList.size(); i++ ){
			HashMap wsResultMap = (HashMap)wsResultList.get(i);
			wsResultMap.put("ad_system", "DCA");
			wsResultMap.put("ad_request_type", requestType);
			wsResultMap.put("ad_request_action", requestAction);
			if( wsResultMap.containsKey("status") && wsResultMap.get("status") != null ){
				String status = ((String)wsResultMap.get("status")).toUpperCase();
				if( status.equalsIgnoreCase("SUCCESS")){
					if( requestAction.equalsIgnoreCase("REGIST") ){
						// 등록 성공인 경우
						systemIntegrationPolestarDAO.updateAmDcaId(wsResultMap);
						if( requestType.equalsIgnoreCase("SERVER") ){
							if( isUpdateAmAsset && requestTangibleAssetYn.equalsIgnoreCase("Y") ){
								systemIntegrationPolestarDAO.updateAmSvPhysiMainIp(wsResultMap);
							}else if( isUpdateAmAsset && requestTangibleAssetYn.equalsIgnoreCase("N") ){
								systemIntegrationPolestarDAO.updateAmSvLogicMainIp(wsResultMap);
							}
						}else if( requestType.equalsIgnoreCase("NETWORK") ){
							if( isUpdateAmAsset && requestTangibleAssetYn.equalsIgnoreCase("Y") ){
								systemIntegrationPolestarDAO.updateAmNwMainIp(wsResultMap);
							}
						}
					}else if( requestAction.equalsIgnoreCase("REMOVE") ){
						systemIntegrationPolestarDAO.updateAmDcaIdIsNull(wsResultMap);
					}	
					successCount++;
				}else if( status.equalsIgnoreCase("FAIL")){
					// 서버등록 실패인 경우
					failCount++;
				}
			}
			systemIntegrationPolestarDAO.insertIfAdRequestResult(wsResultMap);
		}
		
		resultMap.put("totalCount", wsResultList.size());
		resultMap.put("successCount", successCount);
		resultMap.put("failCount", failCount);
		resultMap.put("wsResultList", wsResultList);
	}

	@Override
	public List searchDcaRegistInfo(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchDcaRegistInfo(paramMap);
	}

	@Override
	public int searchDcaRegistInfoCount(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchDcaRegistInfoCount(paramMap);
	}
	
	@Override
	public List searchIfAdDeviceStatusInfo(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchIfAdDeviceStatusInfo(paramMap);
	}

	@Override
	public int searchIfAdDeviceStatusInfoCount(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchIfAdDeviceStatusInfoCount(paramMap);
	}

	@Override
	public void mergeIfAdDeviceStatus(HashMap adData, String adPath, String adType)throws NkiaException {
		adData.put("ad_path", adPath);
		adData.put("ad_type", adType);
		if( adType.equalsIgnoreCase("NW_AP") ){
			adData.put("key_column", "AD_ID");
		}else if( adType.equalsIgnoreCase("PHYSICAL_SERVER") || adType.equalsIgnoreCase("NETWORK") ){
			if( adPath.equals("AD_AM") ){
				adData.put("key_column", "AD_ID");
				adData.put("ad_ip", adData.get("IP"));
				adData.put("serial_no", adData.get("SERIALNO"));
				adData.put("host_nm", adData.get("HOSTNAME"));
			}else{
				adData.put("key_column", "CONF_ID");
				
				adData.put("is_update", true);	// AD동기화가 안된 목록으로 수집되므로 무조건 UPDATE
				
				adData.put("ad_ip", adData.get("ip"));
				adData.put("serial_no", adData.get("SERIALNO"));
				
				String adSystem = (String)adData.get("ad_system");
				if( adSystem.equals("EMS") ){
					adData.put("ad_id", adData.get("ems_id"));
				}else if( adSystem.equals("DCA") ){
					adData.put("ad_id", adData.get("dca_id"));
				}
				
				if( adData.containsKey("status") && adData.get("status") != null ){
					String status = ((String)adData.get("status")).toUpperCase();
					if( status.equalsIgnoreCase("SUCCESS")){
						adData.put("ad_state", "ACTIVE");
						adData.put("ad_sync_yn", "Y");
						adData.put("message", msgAdToAmSyncSuccess);
						adData.put("message_code", msgCodeAdToAmSyncSuccess);
					}else{
						adData.put("ad_sync_yn", "N");
						adData.put("message", adData.get("msg"));
						adData.put("message_code", msgCodeAgentSyncFail);
					}
				}else{
					adData.put("ad_sync_yn", "N");
					adData.put("message", adData.get("msg"));
					adData.put("message_code", msgCodeAgentSyncFail);
				}
				
			}
		}else if( adType.equalsIgnoreCase("VM") ){
			adData.put("key_column", "VM_ID");
			
			adData.put("ad_ip", adData.get("AD_IP"));
			adData.put("vm_host_ip", adData.get("VM_HOST_IP"));
			adData.put("host_nm", adData.get("HOST_NM"));
			
			adData.put("ad_sync_yn", "N");
			if( adData.containsKey("status") && adData.get("status") != null ){
				String status = ((String)adData.get("status")).toUpperCase();
				if( status.equalsIgnoreCase("SUCCESS")){
					adData.put("ad_sync_yn", "Y");
				}
			}
			
		}else if( adType.equalsIgnoreCase("HSRM_ST") || adType.equalsIgnoreCase("HSRM_SS") ){
			adData.put("key_column", "AD_ID");
			adData.put("ad_ip", adData.get("IP_ADDR"));
		}
		systemIntegrationPolestarDAO.mergeIfAdDeviceStatus(adData);
	}

	@Override
	public List searchAmSvNotLogicalList(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmSvNotLogicalList(paramMap);
	}

	@Override
	public int searchAmSvNotLogicalListCount(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmSvNotLogicalListCount(paramMap);
	}

	@Override
	public List searchAmNwApNotAdSyncList(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmNwApNotAdSyncList(paramMap);
	}

	@Override
	public int searchAmNwApNotAdSyncListCount(ModelMap paramMap)throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmNwApNotAdSyncListCount(paramMap);
	}

	@Override
	public List searchAmSsNotAdSyncList(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmSsNotAdSyncList(paramMap);
	}

	@Override
	public int searchAmSsNotAdSyncListCount(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmSsNotAdSyncListCount(paramMap);
	}

	@Override
	public List searchAmStNotAdSyncList(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmStNotAdSyncList(paramMap);
	}

	@Override
	public int searchAmStNotAdSyncListCount(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAmStNotAdSyncListCount(paramMap);
	}
	
	@Override
	public String createAssetId(HashMap paramMap)throws NkiaException {
		
		String asset_id = "";
		String intro_dt = (String)paramMap.get("INTRO_DT");
		
		if("".equals(intro_dt) || intro_dt == null) {
			asset_id = opmsDetailDAO.createAssetId(paramMap);
		} else {
			asset_id = opmsDetailDAO.createAssetIdExistIntroDt(paramMap);
		}
		return asset_id;
	}
	
	@Override
	public String createConfId(HashMap paramMap)throws NkiaException {
		
		String conf_id = "";
		String intro_dt = (String)paramMap.get("INTRO_DT");
		
		if("".equals(intro_dt) || intro_dt == null) {
			conf_id = opmsDetailDAO.createConfId(paramMap);
		} else {
			conf_id = opmsDetailDAO.createConfIdExistIntroDt(paramMap);
		}
		return conf_id;
	}
	
	@Override
	public void registPhysiServer(ModelMap paramMap)throws NkiaException {
		
		HashMap tempMap = new HashMap();
		tempMap.putAll(paramMap);
		
		String asset_id = createAssetId(tempMap);
		paramMap.put("ASSET_ID", asset_id);
		
		tempMap.put("class_type", "SV");
		String conf_id = createConfId(tempMap);
		paramMap.put("PHYSI_CONF_ID", conf_id);
		paramMap.put("VIRTUAL_EQUIP_YN", "N");
		
		
		tempMap.put("class_type", "SL");
		conf_id = createConfId(tempMap);
		paramMap.put("LOGIC_CONF_ID", conf_id);
		
		paramMap.put("PHYSI_CLASS_TYPE", "SV");
		paramMap.put("LOGIC_CLASS_TYPE", "SL");
		
		systemIntegrationPolestarDAO.insertRegistAmAsset(paramMap);
		systemIntegrationPolestarDAO.insertRegistPhysiServerInfraPhysi(paramMap);
		systemIntegrationPolestarDAO.insertRegistPhysiServerPhysi(paramMap);
		systemIntegrationPolestarDAO.insertRegistPhysiServerInfraLogic(paramMap);
		systemIntegrationPolestarDAO.insertRegistPhysiServerLogic(paramMap);
		
		// 업데이트 AD Device
		paramMap.put("ad_id", paramMap.get("DCA_ID"));
		paramMap.put("conf_id", paramMap.get("LOGIC_CONF_ID"));
		
		updateIfAdDeviceStatusCompleteByAdId(paramMap);
		
		// AD 데이터 동기화
		paramMap.put("CONF_ID", paramMap.get("LOGIC_CONF_ID"));
		itgJobService.updateSyncDcaDataToAmServerAtOnce(paramMap);
	}
	
	@Override
	public void mappingPhysiServer(ModelMap paramMap)throws NkiaException {
		
		HashMap typeMap = new HashMap();
		typeMap.put("class_type", "SL");
		
		String mappingType = (String)paramMap.get("MAPPING_TYPE");
		
		if("SERIAL".equals(mappingType)) {
			systemIntegrationPolestarDAO.mappingSyncSerialNo(paramMap);			//시리얼번호 업데이트
		} else if("HOST_IP".equals(mappingType)) {
			systemIntegrationPolestarDAO.mappingSyncHostIp(paramMap);			//호스트IP 업데이트
		}
		
		String logicType = (String)paramMap.get("LOGIC_TYPE");
		if( logicType.equalsIgnoreCase("VM") ){
			// 가상화 매핑일 경우
			registAmSvLogicByVM(paramMap, typeMap);
		}else{
			String conf_id = createConfId(typeMap);
			paramMap.put("LOGIC_CONF_ID", conf_id);
			
			systemIntegrationPolestarDAO.mappingUpdateDcaIdInfra(paramMap);			//Infra의 DCA_ID 업데이트
			systemIntegrationPolestarDAO.insertMappingPhysiServerInfra(paramMap);	//논리서버를 Infra에 등록
			systemIntegrationPolestarDAO.insertMappingPhysiServerLogic(paramMap);	//논리서버를 Logic에 등록
			
			
			// 업데이트 AD Device
			paramMap.put("ad_id", paramMap.get("DCA_ID"));
			paramMap.put("conf_id", paramMap.get("LOGIC_CONF_ID"));
			
			updateIfAdDeviceStatusCompleteByAdId(paramMap);
			
			// AD 데이터 동기화
			paramMap.put("CONF_ID", paramMap.get("LOGIC_CONF_ID"));
			itgJobService.updateSyncDcaDataToAmServerAtOnce(paramMap);
		}
	}
	

	@Override
	public void registVmServer(ModelMap paramMap)throws NkiaException {
		
		HashMap tempMap = new HashMap();
		tempMap.putAll(paramMap);
		
		String asset_id = createAssetId(tempMap);
		paramMap.put("ASSET_ID", asset_id);
		
		tempMap.put("class_type", "SV");
		String conf_id = createConfId(tempMap);
		paramMap.put("PHYSI_CONF_ID", conf_id);
		
		systemIntegrationPolestarDAO.insertRegistAmAsset(paramMap);
		systemIntegrationPolestarDAO.insertRegistPhysiServerInfraPhysi(paramMap);
		systemIntegrationPolestarDAO.insertRegistVmServerPhysi(paramMap);
		
		registAmSvLogicByVM(paramMap, tempMap);
	}
	
	public void registAmSvLogicByVM(ModelMap paramMap, HashMap tempMap)throws NkiaException{
		List adDeviceVmList = systemIntegrationPolestarDAO.selectIfAdDeviceVmListByVmHostType(paramMap);
		for( int i = 0; i < adDeviceVmList.size(); i++ ){
			HashMap adDeviceVmData = (HashMap)adDeviceVmList.get(i); 
			String vmId = (String)adDeviceVmData.get("VM_ID");
			
			tempMap.put("class_type", "SL");
			String conf_id = createConfId(tempMap);
			paramMap.put("LOGIC_CONF_ID", conf_id);
			
			paramMap.put("PHYSI_CLASS_TYPE", "SV");
			paramMap.put("LOGIC_CLASS_TYPE", "SL");	
			paramMap.put("VM_ID", (String)adDeviceVmData.get("VM_ID"));
			paramMap.put("MAIN_IP", (String)adDeviceVmData.get("AD_IP"));
			paramMap.put("SL_CONF_NM", (String)adDeviceVmData.get("HOST_NM"));
			paramMap.put("HOST_NM", (String)adDeviceVmData.get("HOST_NM"));
			paramMap.put("OPER_STATE", (String)adDeviceVmData.get("AD_STATE"));
			
			systemIntegrationPolestarDAO.insertRegistPhysiServerInfraLogic(paramMap);
			systemIntegrationPolestarDAO.insertRegistPhysiServerLogic(paramMap);
			
			ModelMap vmRequestData = new ModelMap();
			
			vmRequestData.put("conf_id", conf_id);
			vmRequestData.put("vm_id", (String)adDeviceVmData.get("VM_ID"));
			vmRequestData.put("request_type", "SERVER");
			vmRequestData.put("request_action", "REGIST");
			vmRequestData.put("request_tangible_asset_yn", "N");
			vmRequestData.put("is_am_update_asset", false);
			
			List reqAssetList = new ArrayList();
			HashMap reqAssetData = new HashMap();
			reqAssetData.put("conf_id", conf_id);
			reqAssetData.put("ip", (String)adDeviceVmData.get("AD_IP"));
			
			if( adDeviceVmData.get("REGION") != null ){
				String region = (String)adDeviceVmData.get("REGION");
				if( region.length() > 0 ){
					if( region.equals(IntergrationPolestarEnums.DCA_SECOND_REGION_CODE.toString()) ){
						reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_SECOND_MANAGER_IP.toString());
					}else{
						reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString());
					}
				}else{
					reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString() );
				}
			}else{
				reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString() );
			}
			reqAssetList.add(reqAssetData);
			
			vmRequestData.put("asset_list", reqAssetList);
			
//			HashMap wsDcaResult = sendDcaRequest(vmRequestData);
			
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			//HashMap wsDcaResult = null;
			HashMap wsDcaResult = new HashMap();
			
			// VM 등록 결과 처리
			boolean isSuccess = false;
			if( wsDcaResult != null && wsDcaResult.containsKey("successCount") ){
				if( (Integer)wsDcaResult.get("successCount") == 1 ){
					List wsResultList = (List)wsDcaResult.get("wsResultList");
					HashMap dataMap = (HashMap)wsResultList.get(0);
					vmRequestData.put("ad_id", dataMap.get("dca_id"));
					vmRequestData.put("DCA_ID", dataMap.get("dca_id"));
					isSuccess = true;
				}
			}
			
			if( isSuccess ){
				updateIfAdDeviceStatusCompleteByVmId(vmRequestData);
				
				// 데이터 동기화
				vmRequestData.put("CONF_ID", paramMap.get("LOGIC_CONF_ID"));
				itgJobService.updateSyncDcaDataToAmServerAtOnce(vmRequestData);
			}else{
				updateIfAdDeviceStatusFailByVmId(vmRequestData);
			}
		}
	}
	
	@Override
	public void mappingNotServer(ModelMap paramMap)throws NkiaException {
		String mappingType = (String)paramMap.get("MAPPING_TYPE");
		
		if("SERIAL".equals(mappingType)) {
			systemIntegrationPolestarDAO.mappingSyncSerialNo(paramMap);			//시리얼번호 업데이트
		} 
		systemIntegrationPolestarDAO.mappingUpdateDcaIdInfra(paramMap);			//Infra의 DCA_ID 업데이트
		
		// 업데이트 AD Device
		paramMap.put("ad_id", paramMap.get("AD_ID"));
		paramMap.put("conf_id", paramMap.get("PHYSI_CONF_ID"));
		
		updateIfAdDeviceStatusCompleteByAdId(paramMap);
		
		String adType = (String)paramMap.get("AD_TYPE");
		// AD 데이터 동기화
		if( adType.equalsIgnoreCase("NW_AP") ){
			itgJobService.updateSyncDcaDataToAmNetworkAtOnce(paramMap);
		}
	}
	
	@Override
	public void insertRegistNw(ModelMap paramMap)throws NkiaException {
		
		HashMap tempMap = new HashMap();
		tempMap.putAll(paramMap);
		
		String asset_id = createAssetId(tempMap);
		paramMap.put("ASSET_ID", asset_id);
		
		tempMap.put("class_type", "NW");
		String conf_id = createConfId(tempMap);
		
		paramMap.put("CONF_ID", conf_id);
		paramMap.put("CLASS_TYPE", "NW");
		
		systemIntegrationPolestarDAO.insertRegistAmAsset(paramMap);
		systemIntegrationPolestarDAO.insertRegistNotServerInfra(paramMap);
		systemIntegrationPolestarDAO.insertRegistNw(paramMap);
		
		// 업데이트 AD Device
		paramMap.put("ad_id", paramMap.get("AD_ID"));
		paramMap.put("conf_id", paramMap.get("CONF_ID"));
		
		updateIfAdDeviceStatusCompleteByAdId(paramMap);
		
		// AD 데이터 동기화
		itgJobService.updateSyncDcaDataToAmNetworkAtOnce(paramMap);
		
	}
	
	private void updateIfAdDeviceStatusCompleteByAdId(ModelMap paramMap)throws NkiaException {
		paramMap.put("ad_sync_yn", "Y");
		paramMap.put("message", msgAdToAmSyncSuccess);
		paramMap.put("message_code", msgCodeAdToAmSyncSuccess);
		systemIntegrationPolestarDAO.updateIfAdDeviceStatusCompleteByAdId(paramMap);
	}
	
	private void updateIfAdDeviceStatusCompleteByVmId(ModelMap paramMap)throws NkiaException {
		paramMap.put("ad_sync_yn", "Y");
		paramMap.put("message", msgAdToAmSyncSuccess);
		paramMap.put("message_code", msgCodeAdToAmSyncSuccess);
		systemIntegrationPolestarDAO.updateIfAdDeviceStatusByVmId(paramMap);
	}
	
	private void updateIfAdDeviceStatusFailByVmId(ModelMap paramMap)throws NkiaException {
		paramMap.put("ad_sync_yn", "N");
		paramMap.put("message", msgNotAgentVm);
		paramMap.put("message_code", msgCodeNotAgentVm);
		systemIntegrationPolestarDAO.updateIfAdDeviceStatusByVmId(paramMap);
	}

	@Override
	public List searchAdSvLongTermList(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAdSvLongTermList(paramMap);
	}

	@Override
	public int searchAdSvLongTermListCount(ModelMap paramMap) throws NkiaException {
		return systemIntegrationPolestarDAO.searchAdSvLongTermListCount(paramMap);
	}

	@Override
	public String checkAdDevice(ModelMap paramMap) throws NkiaException {
		String deviceId = (String)paramMap.get("AD_ID");
		String resultDeviceStatus = wsDcaClient.sendDcaCheckDevice(deviceId);	
		return resultDeviceStatus;
	}

	@Override
	public void updateIfAdDeviceStatusCheckDeviceMsgByConfId(HashMap wsResultData)throws NkiaException {
		systemIntegrationPolestarDAO.updateIfAdDeviceStatusCheckDeviceMsgByConfId(wsResultData);
	}
	
}

