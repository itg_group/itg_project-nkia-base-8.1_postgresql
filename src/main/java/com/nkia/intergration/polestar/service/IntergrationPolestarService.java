/*
 * @(#)IntergrationPolestarService.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface IntergrationPolestarService {
	
	/**
	 * DCA VM목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaVmList() throws NkiaException;
	
	/**
	 * DCA 서버 OA 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerOaList() throws NkiaException;
	
	/**
	 * DCA 서버 CPUS 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerCpusList() throws NkiaException;
	
	/**
	 * DCA 서버 DISKS 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDcaServerDisksList()throws NkiaException;
	
	/**
	 * DCA 서버 Disk 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerDiskList() throws NkiaException;
	
	/**
	 * DCA 서버 Memory 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerMemoryList() throws NkiaException;
	
	/**
	 * DCA 서버 네트워크 Interfaces 목록
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDcaServerNwInterfacesList()throws NkiaException;
	
	/**
	 * DCA 서버 네트워크 Interface 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerNwInterfaceList() throws NkiaException;
	
	/**
	 * DCA 서버 HBA 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerHbaList() throws NkiaException;
	
	/**
	 * DCA 네트워크 시스템 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaNwSystemInfoList() throws NkiaException;
	
	/**
	 * DCA 네트워크 > 네트워크 인터페이스 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaNwNetworkInterfaceList() throws NkiaException;
	
	/**
	 * DCA 설치 소프트웨어 수집 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaInstalledSwList() throws NkiaException;
	
	/**
	 * DCA 소프트웨어 수집 마스터 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaSwObjectList() throws NkiaException;

	public List<HashMap> searchDcaNwNetworkModuleList() throws NkiaException;
	
	/**
	 * 
	 * DCA Agent 미등록 AM 논리서버 목록
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List selectUnregistAmServerLogicPartitionByDcaServer() throws NkiaException;

	
	/**
	 * 
	 * SMS Agent 조회
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List selectSmsAgentList() throws NkiaException;

	/**
	 * HBA 수량 리스트
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDcaInventoryHbaCntList() throws NkiaException;


	
	
}
