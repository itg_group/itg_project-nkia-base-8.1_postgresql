package com.nkia.intergration.polestar.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.polestar.dao.IntergrationPolestarSmsDAO;
import com.nkia.intergration.polestar.service.IntergrationPolestarSmsService;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("intergrationPolestarSmsService")
public class IntergrationPolestarSmsServiceImpl implements IntergrationPolestarSmsService{
	
	@Resource(name="intergrationPolestarSmsDAO")
	private IntergrationPolestarSmsDAO intergrationPolestarSmsDAO;

	@Override
	public List<HashMap> searchEmsServerOsList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsServerOsList();
	}

	@Override
	public List<HashMap> searchEmsServerCpusList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsServerCpusList();
	}

	@Override
	public List<HashMap> searchEmsServerDisksList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsServerDisksList();
	}
	
	@Override
	public List<HashMap> searchEmsServerDiskList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsServerDiskList();
	}

	@Override
	public List<HashMap> searchEmsServerMemoryList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsServerMemoryList();
	}

	@Override
	public List<HashMap> searchEmsServerNwInterfaceList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsServerNwInterfaceList();
	}
	
	@Override
	public List<HashMap> searchEmsDBMariaDBList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsDBMariaDBList();
	}
	
	@Override
	public List<HashMap> searchEmsDBMssqlList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsDBMssqlList();
	}
	
	@Override
	public List<HashMap> searchEmsDBMysqlList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsDBMysqlList();
	}

	@Override
	public List<HashMap> searchEmsDBOracleList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsDBOracleList();
	}
	
	@Override
	public List<HashMap> searchEmsDBPostgreSQLList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsDBPostgreSQLList();
	}
	
	@Override
	public List<HashMap> searchEmsDBTiberoList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsDBTiberoList();
	}
	
	@Override
	public List<HashMap> searchEmsWasInstanceList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsWasInstanceList();
	}
	
	@Override
	public List<HashMap> searchEmsNetworkInfoList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsNetworkInfoList();
	}
	
	@Override
	public List<HashMap> searchEmsTreeList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchEmsTreeList();
	}

	@Override
	public List<HashMap> searchVmsEmsMapList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchVmsEmsMapList();
	}

	@Override
	public List<HashMap> searchVmsClusterList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchVmsClusterList();
	}

	@Override
	public List<HashMap> searchVmsPhysicalList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchVmsPhysicalList();
	}

	@Override
	public List<HashMap> searchVmsLogicalList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchVmsLogicalList();
	}
	
	@Override
	public List<HashMap> searchVmsList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchVmsList();
	}
	
	@Override
	public List<HashMap> searchWebURLList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchWebURLList();
	}
	
	// 20200323_이원재_001_신규작성
	@Override
	public List<HashMap> searchCommonAlarmList(Map paramMap) throws NkiaException {
		return intergrationPolestarSmsDAO.searchCommonAlarmList(paramMap);
	}

	@Override
	public List<HashMap> searchReportGroupList() throws NkiaException {
		return intergrationPolestarSmsDAO.searchReportGroupList();
	}
}
