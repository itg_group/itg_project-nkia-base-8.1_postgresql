/*
 * @(#)IntergrationPolestarDcaDAO.java              2014. 1. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.datasource;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

public class IntergrationPolestarSmsDS extends  EgovAbstractDAO{
	
	// DB 변경시 @Resource name 변경 
	// dataSource, sqlMap, transaction.xml 수정 
	@Resource(name="sms.sqlMapClient")
	public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
        super.setSuperSqlMapClient(sqlMapClient);
    }
	
}
