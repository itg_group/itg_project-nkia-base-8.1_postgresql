/*
 * @(#)IntergrationPolestarDcaDAO.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.intergration.polestar.datasource.IntergrationPolestarDcaDS;
import com.nkia.itg.base.application.exception.NkiaException;

@Repository("intergrationPolestarDcaDAO")
public class IntergrationPolestarDcaDAO extends IntergrationPolestarDcaDS{
	
	/**
	 * DCA 미등록 자산 VM목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaVmList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaVmList", null);
	}
	
	/**
	 * DCA 서버 OA 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerOaList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerOaList", null);
	}
	
	/**
	 * DCA 서버 CPUS 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerCpusList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerCpusList", null);
	}
	
	/**
	 * DCA 서버 Disks 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDcaServerDisksList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerDisksList", null);
	}
	
	/**
	 * DCA 서버 Disk 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerDiskList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerDiskList", null);
	}
	
	/**
	 * DCA 서버 Memory 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerMemoryList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerMemoryList", null);
	}
	
	/**
	 * DCA 서버 네트워크 Interfaces 목록
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDcaServerNwInterfacesList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerNwInterfacesList", null);
	}
	
	/**
	 * DCA 서버 네트워크 Interface 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerNwInterfaceList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerNwInterfaceList", null);
	}
	
	/**
	 * DCA 서버 HBA 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaServerHbaList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaServerHbaList", null);
	}
	
	/**
	 * DCA 네트워크 시스템 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaNwSystemInfoList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaNwSystemInfoList", null);
	}
	
	/**
	 * DCA 네트워크 > 네트워크 인터페이스 정보 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaNwNetworkInterfaceList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaNwNetworkInterfaceList", null);
	}
	
	/**
	 * DCA 설치 소프트웨어 수집 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaInstalledSwList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaInstalledSwList", null);
	}
	
	/**
	 * DCA 소프트웨어 수집 마스터 목록
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaSwObjectList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaSwObjectList", null);
	}

	public List<HashMap> searchDcaNwNetworkModuleList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaNwNetworkModuleList", null);
	}

	public List selectUnregistAmServerLogicPartitionByDcaServer()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.selectUnregistAmServerLogicPartitionByDcaServer", null);
	}
	/**
	 * SMS Agent 
	 * @return
	 * @throws NkiaException
	 */
	public List selectSmsAgentList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.selectSmsAgentList", null);
	}	

	/**
	 * HBA 수량 리스트
	 * @return
	 * @throws NkiaException
	 */
	public List searchDcaInventoryHbaCntList()throws NkiaException {
		return list("IntergrationPolestarDcaDAO.searchDcaInventoryHbaCntList", null);
	}
}
