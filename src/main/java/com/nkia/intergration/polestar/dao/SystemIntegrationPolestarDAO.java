/*
 * @(#)SystemIntegrationPolestarDAO.java              2014. 1. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("systemIntegrationPolestarDAO")
public class SystemIntegrationPolestarDAO extends EgovComAbstractDAO {

	public List searchServerListForDcaRegist(ModelMap paramMap)throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchServerListForDcaRegist", paramMap);
	}

	public int searchServerListForDcaRegistCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchServerListForDcaRegistCount", paramMap);
	}
	
	public List searchNetworkListForDcaRegist(ModelMap paramMap)throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchNetworkListForDcaRegist", paramMap);
	}

	public int searchNetworkListForDcaRegistCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchNetworkListForDcaRegistCount", paramMap);
	}

	public List searchServerListForDcaRemove(ModelMap paramMap)throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchServerListForDcaRemove", paramMap);
	}

	public int searchServerListForDcaRemoveCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchServerListForDcaRemoveCount", paramMap);
	}

	public List searchNetworkListForDcaRemove(ModelMap paramMap)throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchNetworkListForDcaRemove", paramMap);
	}

	public int searchNetworkListForDcaRemoveCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchNetworkListForDcaRemoveCount", paramMap);
	}
	
	public void updateAmDcaId(HashMap wsResultMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateAmDcaId", wsResultMap);
	}

	public void updateAmSvPhysiMainIp(HashMap wsResultMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateAmSvPhysiMainIp", wsResultMap);
	}
	
	public void updateAmSvLogicMainIp(HashMap wsResultMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateAmSvLogicMainIp", wsResultMap);
	}

	public void updateAmNwMainIp(HashMap wsResultMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateAmNwMainIp", wsResultMap);
	}
	
	public void updateAmDcaIdIsNull(HashMap wsResultMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateAmDcaIdIsNull", wsResultMap);
	}

	public void insertIfAdRequestResult(HashMap wsResultMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertIfAdRequestResult", wsResultMap);
	}

	public List searchDcaRegistInfo(ModelMap paramMap) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchDcaRegistInfo", paramMap);
	}

	public int searchDcaRegistInfoCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchDcaRegistInfoCount", paramMap);
	}

	public void mergeIfAdDeviceStatus(HashMap adData)throws NkiaException {
		update("SystemIntegrationPolestarDAO.mergeIfAdDeviceStatus", adData);
	}

	public List searchIfAdDeviceStatusInfo(ModelMap paramMap) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchIfAdDeviceStatusInfo", paramMap);
	}

	public int searchIfAdDeviceStatusInfoCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchIfAdDeviceStatusInfoCount", paramMap);
	}

	public List searchAmSvNotLogicalList(ModelMap paramMap) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchAmSvNotLogicalList", paramMap);
	}

	public int searchAmSvNotLogicalListCount(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchAmSvNotLogicalListCount", paramMap);
	}

	public List searchAmNwApNotAdSyncList(ModelMap paramMap) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchAmNwApNotAdSyncList", paramMap);
	}

	public int searchAmNwApNotAdSyncListCount(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchAmNwApNotAdSyncListCount", paramMap);
	}

	public List searchAmSsNotAdSyncList(ModelMap paramMap) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchAmSsNotAdSyncList", paramMap);
	}

	public int searchAmSsNotAdSyncListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchAmSsNotAdSyncListCount", paramMap);
	}

	public List searchAmStNotAdSyncList(ModelMap paramMap) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchAmStNotAdSyncList", paramMap);
	}

	public int searchAmStNotAdSyncListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchAmStNotAdSyncListCount", paramMap);
	}
	
	public void mappingSyncSerialNo(ModelMap paramMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.mappingSyncSerialNo", paramMap);
	}
	
	public void mappingUpdateDcaIdInfra(ModelMap paramMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.mappingUpdateDcaIdInfra", paramMap);
	}
	
	public void insertMappingPhysiServerInfra(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertMappingPhysiServerInfra", paramMap);
	}
	
	public void insertMappingPhysiServerLogic(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertMappingPhysiServerLogic", paramMap);
	}
	
	public void insertRegistAmAsset(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistAmAsset", paramMap);
	}
	
	public void insertRegistPhysiServerInfraPhysi(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistPhysiServerInfraPhysi", paramMap);
	}

	public void insertRegistPhysiServerPhysi(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistPhysiServerPhysi", paramMap);
	}
	
	public void insertRegistPhysiServerInfraLogic(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistPhysiServerInfraLogic", paramMap);
	}
	
	public void insertRegistPhysiServerLogic(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistPhysiServerLogic", paramMap);
	}
	
	public void mappingSyncHostIp(ModelMap paramMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.mappingSyncHostIp", paramMap);
	}

	public void insertRegistVmServerPhysi(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistVmServerPhysi", paramMap);
	}
	
	public void insertRegistNotServerInfra(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistNotServerInfra", paramMap);
	}
	
	public void insertRegistNw(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistNw", paramMap);
	}
	
	public void insertRegistSs(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistSs", paramMap);
	}
	
	public void insertRegistSt(ModelMap paramMap)throws NkiaException {
		insert("SystemIntegrationPolestarDAO.insertRegistSt", paramMap);
	}

	public void updateIfAdDeviceStatusCompleteByAdId(ModelMap paramMap)throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateIfAdDeviceStatusCompleteByAdId", paramMap);
	}

	public List searchAdSvLongTermList(ModelMap paramMap) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.searchAdSvLongTermList", paramMap);
	}

	public int searchAdSvLongTermListCount(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("SystemIntegrationPolestarDAO.searchAdSvLongTermListCount", paramMap);
	}

	public List selectIfAdDeviceVmListByVmHostType(HashMap vmHostData) throws NkiaException {
		return list("SystemIntegrationPolestarDAO.selectIfAdDeviceVmListByVmHostType", vmHostData);
	}

	public void updateIfAdDeviceStatusByVmId(ModelMap paramMap) throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateIfAdDeviceStatusByVmId", paramMap);
	}

	public void updateIfAdDeviceStatusCheckDeviceMsgByConfId(HashMap wsResultData) throws NkiaException {
		update("SystemIntegrationPolestarDAO.updateIfAdDeviceStatusCheckDeviceMsgByConfId", wsResultData);
	}
	
	
}
