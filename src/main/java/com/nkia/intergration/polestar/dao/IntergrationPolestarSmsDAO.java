package com.nkia.intergration.polestar.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.intergration.polestar.datasource.IntergrationPolestarSmsDS;
import com.nkia.itg.base.application.exception.NkiaException;

@Repository("intergrationPolestarSmsDAO")
public class IntergrationPolestarSmsDAO extends IntergrationPolestarSmsDS{

	public List<HashMap> searchEmsServerOsList() throws NkiaException{
		return list("IntergrationPolestarSmsDAO.searchEmsServerOsList", null);
	}

	public List<HashMap> searchEmsServerCpusList() throws NkiaException {
		return list("IntergrationPolestarSmsDAO.searchEmsServerCpusList", null);
	}

	public List<HashMap> searchEmsServerDisksList() {
		return list("IntergrationPolestarSmsDAO.searchEmsServerDisksList", null);
	}
	
	public List<HashMap> searchEmsServerDiskList() {
		return list("IntergrationPolestarSmsDAO.searchEmsServerDiskList", null);
	}

	public List<HashMap> searchEmsServerMemoryList() {
		return list("IntergrationPolestarSmsDAO.searchEmsServerMemoryList", null);
	}

	public List<HashMap> searchEmsServerNwInterfaceList() {
		return list("IntergrationPolestarSmsDAO.searchEmsServerNwInterfaceList", null);
	}
	
	public List<HashMap> searchEmsDBMariaDBList() {
		return list("IntergrationPolestarSmsDAO.searchEmsDBMariaDBList", null);
	}
	
	public List<HashMap> searchEmsDBMssqlList() {
		return list("IntergrationPolestarSmsDAO.searchEmsDBMssqlList", null);
	}
	
	public List<HashMap> searchEmsDBMysqlList() {
		return list("IntergrationPolestarSmsDAO.searchEmsDBMysqlList", null);
	}
	
	public List<HashMap> searchEmsDBOracleList() {
		return list("IntergrationPolestarSmsDAO.searchEmsDBOracleList", null);
	}
	
	public List<HashMap> searchEmsDBPostgreSQLList() {
		return list("IntergrationPolestarSmsDAO.searchEmsDBPostgreSQLList", null);
	}
	
	public List<HashMap> searchEmsDBTiberoList() {
		return list("IntergrationPolestarSmsDAO.searchEmsDBTiberoList", null);
	}
	
	public List<HashMap> searchEmsWasInstanceList() {
		return list("IntergrationPolestarSmsDAO.searchEmsWasInstanceList", null);
	}

	public List<HashMap> searchEmsNetworkInfoList() {
		return list("IntergrationPolestarSmsDAO.searchEmsNetworkInfoList", null);
	}
	
	
	public List<Map<String, Object>> searchIncidentEmsList(Map<String, Object> paramMap) {
		return list("IntergrationPolestarSmsDAO.searchIncidentEmsList", paramMap);
	}

	public List<HashMap> searchEmsTreeList() {
		return list("IntergrationPolestarSmsDAO.searchEmsTreeList", null);
	}
	
	public List<HashMap> searchVmsEmsMapList() {
		return list("IntergrationPolestarSmsDAO.searchVmsEmsMapList", null);
	}
	
	public List<HashMap> searchVmsClusterList() {
		return list("IntergrationPolestarSmsDAO.searchVmsClusterList", null);
	}
	
	public List<HashMap> searchVmsPhysicalList() {
		return list("IntergrationPolestarSmsDAO.searchVmsPhysicalList", null);
	}
	
	public List<HashMap> searchVmsLogicalList() {
		return list("IntergrationPolestarSmsDAO.searchVmsLogicalList", null);
	}
	
	public List<HashMap> searchVmsList() {
		return list("IntergrationPolestarSmsDAO.searchVmsList", null);
	}
	
	public List<HashMap> searchWebURLList() {
		return list("IntergrationPolestarSmsDAO.searchWebURLList", null);
	}
	
	// 20200323_이원재_001_신규작성
	public List<HashMap> searchCommonAlarmList(Map paramList) {
		return list("IntergrationPolestarSmsDAO.searchCommonAlarmList", paramList);
	}
	
	public List<HashMap> searchReportGroupList() {
		return list("IntergrationPolestarSmsDAO.searchReportGroupList", null);
	}
}
