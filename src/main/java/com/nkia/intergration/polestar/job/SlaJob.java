/*
 * @(#)SlaJob.java              2020. 2. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.util.HashMap;

import javax.annotation.Resource;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.NamedParamDAO;
import com.nkia.itg.sla.manage.service.SlaManageIndicatorService;
import com.nkia.itg.system.scheduler.service.SchedulerService;
 
public class SlaJob implements Job {
	
	public static final Logger logger = LoggerFactory.getLogger(SlaJob.class);

	@Resource(name="namedParamDAO")
	private NamedParamDAO namedParamDAO;

	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");;
	
	private HashMap initSchedulerJob(String[] jobArgs) throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId); //202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
			
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		
		String scheduleId = jobDataMap.getString("scheduleId");
        String scheduleNm = jobDataMap.getString("scheduleNm");
        String executeMethod = jobDataMap.getString("executeMethod");
        String scheduleConfig = jobDataMap.getString("scheduleConfig");
		String executor = jobDataMap.getString("executor"); 
		String[] jobArgs = {scheduleId, scheduleNm, scheduleConfig, executor};
		
		if("slaExecDaily".equalsIgnoreCase(executeMethod)) {
			slaExecDaily(jobArgs);
		}
	}
	
	/**
	 * 
	 * SLA 일배치 스케줄러 실행
	 *
	 */
	public void slaExecDaily(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			SlaManageIndicatorService slaManageIndicatorService = (SlaManageIndicatorService) NkiaApplicationContext.getCtx().getBean("SlaManageIndicatorService");
			slaManageIndicatorService.procSpSlaExecDaily();

			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e);
			}
			new NkiaException(e);
		}
	}
	
}
