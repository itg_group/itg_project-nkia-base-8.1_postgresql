package com.nkia.intergration.polestar.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.service.ItgBaseService;

@SuppressWarnings("rawtypes")
public class MailSendJob {
	
	private static final Logger logger = LoggerFactory.getLogger(MailSendJob.class);
	
	// 일일 점검일지
	public void sendCheckListMail(){
		ItgBaseService itgBaseService = (ItgBaseService) NkiaApplicationContext.getCtx().getBean("itgBaseService");
		
		String from = "c1sdesk@mnd.mil";
		String to = "c1sdesk@mnd.mil";

		String subject = "[ITSM 시스템] 일일업무일지를 등록하세요.";
		String content = "[일일업무일지] 업무일지가 완료되지 않았습니다." +
				"<br> <a href='http://oa.didc1.mnd.mil/' target='_new'>ITSM 시스템 바로가기 클릭!</a>";
		
		try {
			Map alarmMap = new HashMap();
			//알림 메일
			alarmMap.put("KEY", "SYSTEM");
			//html 템플릿 없이 content를 담는 경우
			alarmMap.put("TEMPLATE_ID", "EMPTY_TEMPLATE");
			// 1. 메일수신자셋팅(LIST로)
			List user = itgBaseService.searchCheckListSendList();
			alarmMap.put("TO_USER_LIST", user);
			// 2. 제목, 내용
			Map dataMap = new HashMap();
			dataMap.put("TITLE", subject);
			dataMap.put("CONTENT", content);
			alarmMap.put("DATA", dataMap);
			
			EmailSetData emailSender = new EmailSetData(alarmMap);
			emailSender.sendMail();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}
	
	public void sendAccountMail(){
		ItgBaseService itgBaseService = (ItgBaseService) NkiaApplicationContext.getCtx().getBean("itgBaseService");
		
		String from = "c1sdesk@mnd.mil";
		String to = "c1sdesk@mnd.mil";

		String subject = "[ITSM 시스템] 계정 만료일이 임박하였습니다.";
		String content = "[서비스요청>계정요청] 계정 만료일이 임박하였습니다." +
				"<br> <a href='http://oa.didc1.mnd.mil/' target='_new'>ITSM 시스템 바로가기 클릭!</a>";
		
		try {
			Map alarmMap = new HashMap();
			//알림 메일
			alarmMap.put("KEY", "SYSTEM");
			alarmMap.put("TEMPLATE_ID", "EMPTY_TEMPLATE");
			// 1. 메일수신자셋팅(LIST로)
			Map dataMap = new HashMap();
			List user = itgBaseService.searchAccountSendList();
			dataMap.put("TO_USER_LIST", user);
			// 2. 제목, 내용
			dataMap.put("TITLE", subject);
			dataMap.put("CONTENT", content);
			alarmMap.put("DATA", dataMap);
			//메일발송유형, 템플릿ID, 알림정보, 추가정보
			EmailSetData emailSender = new EmailSetData(alarmMap);
			emailSender.sendMail();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}
}
