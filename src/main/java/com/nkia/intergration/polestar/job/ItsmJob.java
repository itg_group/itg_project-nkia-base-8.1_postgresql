/*
 * @(#)DcaJob.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.intergration.customize.job.service.DrService;
import com.nkia.intergration.customize.job.service.ItsmService;
import com.nkia.intergration.polestar.job.service.ItgItsmJobService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.scheduler.service.SchedulerService;
 
public class ItsmJob implements Job {
	
	public static final Logger logger = LoggerFactory.getLogger(ItsmJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId);
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * Job Handler - DB 방식인 경우 실행시간에 맞춰 execute 메소드가 호출 된다.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		// Job Args 값 설정
        String scheduleId = jobDataMap.getString("scheduleId");
        String scheduleNm = jobDataMap.getString("scheduleNm");
        String executeMethod = jobDataMap.getString("executeMethod");
        String scheduleConfig = jobDataMap.getString("scheduleConfig");
		String executor = jobDataMap.getString("executor"); 
		String[] jobArgs = {scheduleId, scheduleNm, scheduleConfig, executor};
		
		if("gatherItsmData".equalsIgnoreCase(executeMethod)){
			gatherItsmData(jobArgs);
		} else if("gatherLCloudData".equalsIgnoreCase(executeMethod)){
			gatherLCloudData(jobArgs);
		} else if("gatherDRData".equalsIgnoreCase(executeMethod)){
			gatherDRData(jobArgs);
		} else if("gatherLCloudHistData".equalsIgnoreCase(executeMethod)){
			gatherLCloudHistData(jobArgs);
		} else if("gatherDRHistData".equalsIgnoreCase(executeMethod)){
			gatherDRHistData(jobArgs);
		} else if("gatherCodeData".equalsIgnoreCase(executeMethod)) {
			gatherCodeData(jobArgs);
		} else if("gatherDNSData".equalsIgnoreCase(executeMethod)) {
			gatherDNSData(jobArgs);
		} else if("gatherUserData".equalsIgnoreCase(executeMethod)) {
			gatherUserData(jobArgs);
		}
	}
	
	/**
	 * ITSM 자산 데이터 수집
	 */
	public void gatherItsmData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = true;
		String resultMsg = null;
		
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItsmService itsmService = (ItsmService) NkiaApplicationContext.getCtx().getBean("itsmService");
			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
			
			// ITSM 데이터 목록
			List<HashMap> itsmDataList = itsmService.searchItsmDataList();
			
			if( itsmDataList != null && itsmDataList.size() > 0 ){
				itgItsmJobService.insertIfItsmList(itsmDataList);
			}
			
			String cmdbSyncTypeCode = NkiaApplicationPropertiesMap.getProperty("Globals.Cmdb.Sync.Type");
			
			if(cmdbSyncTypeCode != null) {
				String[] cmdbSyncTypeCodeArr = cmdbSyncTypeCode.split(",");
				
				for(int i=0; i<cmdbSyncTypeCodeArr.length; i++) {
					// 데이터 자산으로 동기화 
					Map map = new HashMap();
					map.put("V_RESULT", "");
					map.put("V_CLASS_TYPE", cmdbSyncTypeCodeArr[i].trim());
					itgItsmJobService.callSyncCmdbData(map);
					
					if(!"SUCCESS".equals(map.get("V_RESULT").toString())) {
						isSuccess = false;
						resultMsg = map.get("V_RESULT").toString();
						
						break;
					}
				}
			}
			
			finishedSchedulerJob(schedulerJobMap, isSuccess, resultMsg);
		} catch (Exception e) {
			try {
				isSuccess = false;
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
	/**
	 * ITSM 코드 데이터 수집
	 */
	public void gatherCodeData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItsmService itsmService = (ItsmService) NkiaApplicationContext.getCtx().getBean("itsmService");
			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
			
			// ITSM 코드 목록
			List<HashMap> codeList = itsmService.searchCodeList();
			
			if( codeList != null && codeList.size() > 0 ){
				itgItsmJobService.insertIfCodeList(codeList);
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
	/**
	 * L-Cloud 자산 데이터 수집
	 */
	public void gatherLCloudData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = true;
		String resultMsg = null;
		
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
//			LCloudService lCloudService = (LCloudService) NkiaApplicationContext.getCtx().getBean("lCloudService");
//			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
//			
//			// L-Cloud 데이터 목록
//			List<HashMap> cloudDataList = lCloudService.searchCloudDataList();
//			
//			// 1. L-Cloud 데이터 가져오기
//			if( cloudDataList != null && cloudDataList.size() > 0 ){
//				itgItsmJobService.insertIfCloudList(cloudDataList);
//			}
//			
//			// 2. L-Cloud 데이터 자산으로 동기화 
//			Map map = new HashMap();
//			map.put("V_CLASS_TYPE", "CL");
//			map.put("V_RESULT", "");
//			itgItsmJobService.callSyncCloud(map);
//			
//			if("SUCCESS".equals(map.get("V_RESULT").toString())) {
//				// 3. 물리장비 업데이트
//				map.put("V_CLASS_TYPE", "PHYSI_CONF");
//				map.put("V_RESULT", "");
//				itgItsmJobService.callSyncCloud(map);
//				
//				if(!"SUCCESS".equals(map.get("V_RESULT").toString())) {
//					isSuccess = false;
//					resultMsg = map.get("V_RESULT").toString();
//				}
//			} else {
//				isSuccess = false;
//				resultMsg = map.get("V_RESULT").toString();
//			}
			
			finishedSchedulerJob(schedulerJobMap, isSuccess, resultMsg);
		} catch (Exception e) {
			try {
				isSuccess = false;
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
	/**
	 * L-Cloud 자산 수정이력 데이터 수집
	 */
	public void gatherLCloudHistData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = true;
		String resultMsg = null;
		
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
//			LCloudService lCloudService = (LCloudService) NkiaApplicationContext.getCtx().getBean("lCloudService");
//			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
//			
//			HashMap paramMap = new HashMap();
//			paramMap.put("COLLECT_TYPE", NkiaApplicationPropertiesMap.getProperty("Globals.Cloud.Hist.Collect.Type"));
//			
//			// L-Cloud 이력 데이터 목록
//			List<HashMap> cloudDataList = lCloudService.searchCloudHistDataList(paramMap);
//			
//			// 0. L-Cloud 이력 데이터 삭제
//			itgItsmJobService.deleteIfCloudHist();
//			
//			// 1. L-Cloud 이력 데이터 가져오기
//			if( cloudDataList != null && cloudDataList.size() > 0 ){
//				itgItsmJobService.insertIfCloudHistList(cloudDataList);
//			}
//			
//			// 2. L-Cloud 이력 데이터 자산으로 동기화 
//			Map map = new HashMap();
//			map.put("V_RESULT", "");
//			itgItsmJobService.callSyncCloudHist(map);
//			
//			if(!"SUCCESS".equals(map.get("V_RESULT").toString())) {
//				isSuccess = false;
//				resultMsg = map.get("V_RESULT").toString();
//			}
			
			finishedSchedulerJob(schedulerJobMap, isSuccess, resultMsg);
		} catch (Exception e) {
			try {
				isSuccess = false;
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
	/**
	 * DR 자산 데이터 수집
	 */
	public void gatherDRData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = true;
		String resultMsg = null;
		
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			DrService drService = (DrService) NkiaApplicationContext.getCtx().getBean("drService");
			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
			
			// DR 데이터 목록
			List<HashMap> cloudDataList = drService.searchDRDataList();
			
			// 1. DR 데이터 가져오기
			if( cloudDataList != null && cloudDataList.size() > 0 ){
				itgItsmJobService.insertIfCloudDRList(cloudDataList);
			}
			
			// 2. DR 데이터 자산으로 동기화 
			Map map = new HashMap();
			map.put("V_CLASS_TYPE", "DR");
			map.put("V_RESULT", "");
			itgItsmJobService.callSyncCloudDR(map);
			
			if("SUCCESS".equals(map.get("V_RESULT").toString())) {
				// 3. 물리장비 업데이트
				map.put("V_CLASS_TYPE", "PHYSI_CONF");
				map.put("V_RESULT", "");
				itgItsmJobService.callSyncCloudDR(map);
				
				if(!"SUCCESS".equals(map.get("V_RESULT").toString())) {
					isSuccess = false;
					resultMsg = map.get("V_RESULT").toString();
				}
			} else {
				isSuccess = false;
				resultMsg = map.get("V_RESULT").toString();
			}
			
			finishedSchedulerJob(schedulerJobMap, isSuccess, resultMsg);
		} catch (Exception e) {
			try {
				isSuccess = false;
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
	/**
	 * DR 자산 수정이력 데이터 수집
	 */
	public void gatherDRHistData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			DrService drService = (DrService) NkiaApplicationContext.getCtx().getBean("drService");
			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
			
			// DR 데이터 목록
			List<HashMap> cloudDataList = drService.searchDRHistDataList();
			
			// 1. DR 데이터 가져오기
			if( cloudDataList != null && cloudDataList.size() > 0 ){
				itgItsmJobService.insertIfCloudDRHistList(cloudDataList);
			}
			
//			// 2. DR 데이터 자산으로 동기화 
//			Map map = new HashMap();
//			map.put("V_CLASS_TYPE", "DR");
//			map.put("V_RESULT", "");
//			itgItsmJobService.callSyncCloudDR(map);
//			
//			// 3. 물리장비 업데이트
//			map.put("V_CLASS_TYPE", "PHYSI_CONF");
//			map.put("V_RESULT", "");
//			itgItsmJobService.callSyncCloudDR(map);
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
	/**
	 * DNS 데이터 수집
	 */
	public void gatherDNSData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItsmService itsmService = (ItsmService) NkiaApplicationContext.getCtx().getBean("itsmService");
			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
			
			// DNS ZONE 목록
			List<HashMap> dnsZoneList = itsmService.searchDNSZoneList();
			
			if( dnsZoneList != null && dnsZoneList.size() > 0 ){
				itgItsmJobService.insertIfDNSZoneList(dnsZoneList);
			}
			
			// DNS RECORD 목록
			List<HashMap> dnsRecordList = itsmService.searchDNSRecordList();
			
			if( dnsRecordList != null && dnsRecordList.size() > 0 ){
				itgItsmJobService.insertIfDNSRecordList(dnsRecordList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
	/**
	 * 유저정보 및 권한테이블 
	 */
	public void gatherUserData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItsmService itsmService = (ItsmService) NkiaApplicationContext.getCtx().getBean("itsmService");
			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
			
			// ITSM 유저데이터 목록
			List<HashMap> itsmUserDataList = itsmService.searchUserDataList();
			
			if( itsmUserDataList != null && itsmUserDataList.size() > 0 ){
				itgItsmJobService.insertIfUserList(itsmUserDataList);
			}
			// ITSM 유저데이터 목록
			List<HashMap> itsmMappingDataList = itsmService.searchMappingDataList();
			
			if( itsmMappingDataList != null && itsmMappingDataList.size() > 0 ){
				itgItsmJobService.insertIfMappingList(itsmMappingDataList);
			}

			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
}
