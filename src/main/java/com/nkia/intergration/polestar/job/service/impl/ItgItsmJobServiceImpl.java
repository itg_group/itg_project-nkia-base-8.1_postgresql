/*
 * @(#)ItgJobServiceImpl.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.polestar.job.dao.ItgItsmJobDAO;
import com.nkia.intergration.polestar.job.service.ItgItsmJobService;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("itgItsmJobService")
public class ItgItsmJobServiceImpl implements ItgItsmJobService {
	
	@Resource(name = "itgItsmJobDAO")
	public ItgItsmJobDAO itgItsmJobDAO;
	
	@Override
	public void insertIfItsmList(List<HashMap> itsmList) throws NkiaException {
		itgItsmJobDAO.truncateIfItsm();
		for(HashMap itsmData: itsmList) {
			itgItsmJobDAO.insertIfItsm(itsmData);
		}
	}

	@Override
	public void insertIfCodeList(List<HashMap> codeList) throws NkiaException {
		itgItsmJobDAO.truncateIfCode();
		for(HashMap codeData: codeList) {
			itgItsmJobDAO.insertIfCode(codeData);
		}
	}

	@Override
	public void callSyncCloud(Map map) throws NkiaException {
		itgItsmJobDAO.callSyncCloud(map);
	}
	
	@Override
	public void callSyncCloudHist(Map map) throws NkiaException {
		itgItsmJobDAO.callSyncCloudHist(map);
	}
	
	@Override
	public void callSyncCloudDR(Map map) throws NkiaException {
		itgItsmJobDAO.callSyncCloudDR(map);
	}

	@Override
	public void insertIfCloudList(List<HashMap> cloudList) throws NkiaException {
		itgItsmJobDAO.truncateIfCloudLog();
		itgItsmJobDAO.truncateIfCloud();
		for(HashMap cloudData: cloudList) {
			itgItsmJobDAO.insertIfCloud(cloudData);
		}
	}

	@Override
	public void insertIfCloudDRList(List<HashMap> drList) throws NkiaException {
		itgItsmJobDAO.truncateIfCloudDRLog();
		itgItsmJobDAO.truncateIfCloudDR();
		for(HashMap drData: drList) {
			itgItsmJobDAO.insertIfCloudDR(drData);
		}
	}

	@Override
	public void deleteIfCloudHist() throws NkiaException {
		itgItsmJobDAO.truncateIfCloudHist();
	}
	
	@Override
	public void insertIfCloudHistList(List<HashMap> cloudList) throws NkiaException {
//		itgItsmJobDAO.truncateIfCloudHist();
		for(HashMap cloudData: cloudList) {
			itgItsmJobDAO.insertIfCloudHist(cloudData);
		}
	}

	@Override
	public void insertIfCloudDRHistList(List<HashMap> drList) throws NkiaException {
		itgItsmJobDAO.truncateIfCloudDRHist();
		for(HashMap drData: drList) {
			itgItsmJobDAO.insertIfCloudDRHist(drData);
		}
	}

	@Override
	public void insertIfDNSZoneList(List<HashMap> dataList) throws NkiaException {
		itgItsmJobDAO.truncateIfDNSZone();
		for(HashMap data: dataList) {
			itgItsmJobDAO.insertIfDNSZone(data);
		}
	}
	
	@Override
	public void insertIfDNSRecordList(List<HashMap> dataList) throws NkiaException {
		itgItsmJobDAO.truncateIfDNSRecord();
		for(HashMap data: dataList) {
			itgItsmJobDAO.insertIfDNSRecord(data);
		}
	}
	@Override
	public void insertIfUserList(List<HashMap> userList) throws NkiaException {
		itgItsmJobDAO.truncateIfUser();
		for(HashMap userData: userList) {
			itgItsmJobDAO.insertIfUser(userData);
		}
	}

	@Override
	public void insertIfMappingList(List<HashMap> mappingList) throws NkiaException {
		itgItsmJobDAO.truncateIfMapping();
		for(HashMap mappingData: mappingList) {
			itgItsmJobDAO.insertIfMapping(mappingData);
		}
	}

	@Override
	public void callSyncCmdbData(Map map) throws NkiaException {
		itgItsmJobDAO.callSyncCmdbData(map);
	}

	@Override
	public List selectAssetLnIncreaseEndList() throws NkiaException {
		return itgItsmJobDAO.selectAssetLnIncreaseEndList();
	}

	@Override
	public List selectAssetLNAlarmUserList() throws NkiaException {
		return itgItsmJobDAO.selectAssetLNAlarmUserList();
	}

}
