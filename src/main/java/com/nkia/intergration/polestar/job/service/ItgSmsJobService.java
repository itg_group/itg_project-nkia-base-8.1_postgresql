package com.nkia.intergration.polestar.job.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ItgSmsJobService {

	void insertIfEmsServerOsList(List<HashMap> emsServerOsList) throws NkiaException;

	void insertIfEmsServerCpusList(List<HashMap> emsServerCpusList) throws NkiaException;

	void insertIfEmsServerDisksList(List<HashMap> emsServerDisksList) throws NkiaException;
	
	void insertIfEmsServerDiskList(List<HashMap> emsServerDisksList) throws NkiaException;

	void insertIfEmsServerMemoryList(List<HashMap> emsServerMemoryList) throws NkiaException;

	void insertIfEmsServerNwInterfaceList(List<HashMap> emsServerNwInterfaceList) throws NkiaException;

	void updateSyncEmsDataToAmServer(String syncPeriod) throws NkiaException;
	
	void insertIfEmsDBMariaDBList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfEmsDBMssqlList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfEmsDBMysqlList(List<HashMap> paramList) throws NkiaException;

	void insertIfEmsDBOracleList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfEmsDBPostgreSQLList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfEmsDBTiberoList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfEmsWasInstanceList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfEmsNwInfoList(List<HashMap> paramList) throws NkiaException;
	
	void updateSyncEmsDataToAmNetwork(String syncPeriod) throws NkiaException;
	
	void insertIfEmsTreeList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfVmsEmsMapList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfVmsClusterList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfVmsPhysicalList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfVmsLogicalList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfVmsList(List<HashMap> paramList) throws NkiaException;
	
	void insertIfWebURLList(List<HashMap> paramList) throws NkiaException;
	
	void callSyncVmsCluster(Map map)throws NkiaException;
	
	// 20200323_이원재_001_신규작성
	void insertEmsMessage(List<HashMap> paramList) throws NkiaException;
	// 20200323_이원재_001_신규작성 END
	// 20200326_이원재_001_신규작성
	List selectEmsMessageList(Map paramMap) throws NkiaException;
	
	int selectEmsMessageCount(Map paramMap) throws NkiaException;
	// 20200326_이원재_001_신규작성 END
	
	void insertIfReportGroupList(List<HashMap> paramList) throws NkiaException;
}
