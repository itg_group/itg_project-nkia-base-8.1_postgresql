/*
 * @(#)ItgJobService.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;


public interface ItgItsmJobService {
	
	/**
	 * 
	 * ITSM 자산 등록
	 * 
	 * @param itsmList
	 * @throws NkiaException
	 */
	public void insertIfItsmList(List<HashMap> itsmList)throws NkiaException;

	/**
	 * 
	 * ITSM 코드 등록
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void insertIfCodeList(List<HashMap> codeList)throws NkiaException;
	
	/**
	 * 
	 * L-Cloud 등록
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void insertIfCloudList(List<HashMap> cloudList)throws NkiaException;
	
	/**
	 * 
	 * L-Cloud 수정이력 등록
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void insertIfCloudHistList(List<HashMap> cloudList)throws NkiaException;
	
	/**
	 * 
	 * DR 등록
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void insertIfCloudDRList(List<HashMap> drList)throws NkiaException;
	
	/**
	 * 
	 * DR 수정이력 등록
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void insertIfCloudDRHistList(List<HashMap> drList)throws NkiaException;
	
	/**
	 * 
	 * DNS ZONE 등록
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void insertIfDNSZoneList(List<HashMap> drList)throws NkiaException;
	
	/**
	 * 
	 * DNS RECORD 등록
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void insertIfDNSRecordList(List<HashMap> drList)throws NkiaException;
	
	/**
	 * 
	 * CLOUD 수정이력 삭제
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void deleteIfCloudHist()throws NkiaException;
	
	/**
	 * 
	 * CLOUD 데이터 동기화
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void callSyncCloud(Map map)throws NkiaException;
	
	/**
	 * 
	 * CLOUD 이력 데이터 동기화
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void callSyncCloudHist(Map map)throws NkiaException;
	
	/**
	 * 
	 * CLOUD DR 데이터 동기화
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void callSyncCloudDR(Map map)throws NkiaException;
	/**
	 * 
	 * ASIS ITSM USER정보
	 * 
	 * @param userList
	 * @throws NkiaException
	 */
	public void insertIfUserList(List<HashMap> userList)throws NkiaException;
	
	/**
	 * 
	 * ASIS ITSM USER - CUSTOMER MAPPING정보
	 * 
	 * @param mappingList
	 * @throws NkiaException
	 */
	public void insertIfMappingList(List<HashMap> mappingList)throws NkiaException;
	
	/**
	 * 
	 * CMDB 데이터 동기화
	 * 
	 * @param codeList
	 * @throws NkiaException
	 */
	public void callSyncCmdbData(Map map)throws NkiaException;
	
	/**
	 * 
	 * 회선 증속종료 3일전 데이터 조회
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List selectAssetLnIncreaseEndList()throws NkiaException;
	
	/**
	 * 
	 * 작업알림(영업) 담당자 조회
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List selectAssetLNAlarmUserList()throws NkiaException;
}
