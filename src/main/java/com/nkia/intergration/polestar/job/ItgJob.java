/*
 * @(#)DcaJob.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;

import com.nkia.intergration.polestar.job.service.ItgJobService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.email.EmailTemplateGenerator;
import com.nkia.itg.system.scheduler.service.SchedulerService;
import com.nkia.itg.system.user.service.UserService;

public class ItgJob implements Job{
	
	public static final Logger logger = LoggerFactory.getLogger(ItgJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");
	
	private static final String noticeTitleSwDif = "불일치 AD 소프트웨어 목록";
	private static final String noticeTitleSvSpecChange = "서버 스펙 변경 라이선스 목록";
	private static final String noticeTitleApplyBaseChange = "라이선스 적용기준 변경 목록";
	private static final String noticeTitleIllegalSw = "불법 소프트웨어 목록";
	private static final String noticeTitleNewSwGroup = "신규 소프트웨어 그룹";
	
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId);
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * Job Handler - DB 방식인 경우 실행시간에 맞춰 execute 메소드가 호출 된다.
	*/ 
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		// Job Args 값 설정
        String scheduleId = jobDataMap.getString("scheduleId");
        String scheduleNm = jobDataMap.getString("scheduleNm");
        String executeMethod = jobDataMap.getString("executeMethod");
        String scheduleConfig = jobDataMap.getString("scheduleConfig");
		String executor = jobDataMap.getString("executor"); 
		String[] jobArgs = {scheduleId, scheduleNm, scheduleConfig, executor};
		
		if("InitializationUserPwBatch".equalsIgnoreCase(executeMethod)){
			InitializationUserPwBatch(jobArgs);
		} else if("deleteSysLoginSession".equalsIgnoreCase(executeMethod)){
			deleteSysLoginSession(jobArgs);
		} else if("deleteScheduleResult".equalsIgnoreCase(executeMethod)){
			deleteScheduleResult(jobArgs);
		}	
	}
	
	
	/**
	 * 패스워드 사용대상 사용자 패스워드 생성
	 */
	public void InitializationUserPwBatch(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			UserService userService = (UserService) NkiaApplicationContext.getCtx().getBean("userService");
			userService.InitializationUserPwBatch();
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e);
			}
			new NkiaException(e);
		}
    }
	
	
	/**
	 * SAM Notice(소프트웨어관리 통보 스케쥴)
	 * 불일치 AD소프트웨어 통지
	 * 서버 스펙변경 라이선스 통지
	 * 라이선스 적용기준 변경 통지
	 * 불법 소프트웨어 통지
	 * 
	 */
	public void sendSamNotice(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
//			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
//			
//			
//			HashMap adSystemAdminInfo = itgJobService.selectAmSystemAdminInfo();
//			
//			String fromUserId = (String)adSystemAdminInfo.get("USER_ID");
//			String fromUserEmail = (String)adSystemAdminInfo.get("EMAIL");
//			
//			ModelMap paramMap = new ModelMap();
//			
//			/** 서버 담당자에게 통보 **/
//			String noticeType = "OPER_USER";
//			paramMap.put("notice_type", noticeType);
//			paramMap.put("sort_column", noticeType + "_ID ASC");
//			
//			// 불일치 AD 소프트웨어 목록 통지
//			List noticeSwDifOperUserList = swLicenceService.selectLicenceAssignedSwDifList(paramMap);
//			if( noticeSwDifOperUserList.size() > 0 ){
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleSwDif, noticeSwDifOperUserList, noticeType, "SW_AD_DIF_NOTI_MAIL", "AD_DIF_SW_LIST");
//			}
//			
//			// 소프트웨어 라이선스 적용기준 변경 목록 통지
//			List noticeSwApplyBaseChangeOperUserList = swLicenceService.selectAmSwAssignedLicenceApplyBaseChangeList(paramMap);
//			if( noticeSwApplyBaseChangeOperUserList.size() > 0 ){
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleApplyBaseChange, noticeSwApplyBaseChangeOperUserList, noticeType, "SW_APPLY_BASE_NOTI_MAIL", "APPLY_BASE_CHANGE_SW_LIST");
//			}
//			
//			// 소프트웨어 서버스펙변경 목록 통지
//			List noticeSwSpecChangeOperUserList = swLicenceService.selectAmSwAssignedAmSlSpecChangeList(paramMap);
//			if( noticeSwSpecChangeOperUserList.size() > 0 ){
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleSvSpecChange, noticeSwSpecChangeOperUserList, noticeType, "SW_SPEC_NOTI_MAIL", "SPCE_CHANGE_SW_LIST");
//			}
//			
//			List noticeSwIllegalOperUserList = swLicenceService.selectIllegalInstalledSwList(paramMap);
//			if( noticeSwIllegalOperUserList.size() > 0 ){
//				// 불법 소프트웨어 목록 통지
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleIllegalSw, noticeSwIllegalOperUserList, noticeType, "SW_ILLEGAL_NOTI_MAIL", "ILLEGAL_SW_LIST");
//			}
//			
//			/** 소프트웨어 관리자에게 통보 **/
//			noticeType = "SW_MANAGER";
//			paramMap.put("notice_type", noticeType);
//			paramMap.put("sort_column", noticeType + "_ID ASC");
//			
//			// 불일치 AD 소프트웨어 목록 통지
//			List noticeSwDifSwManagerList = swLicenceService.selectLicenceAssignedSwDifList(paramMap);
//			if( noticeSwDifSwManagerList.size() > 0 ){
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleSwDif, noticeSwDifSwManagerList, noticeType, "SW_AD_DIF_NOTI_MAIL", "AD_DIF_SW_LIST");
//			}
//			
//			// 소프트웨어 라이선스 적용기준 변경 목록 통지
//			List noticeSwApplyBaseChangeSwManagerList = swLicenceService.selectAmSwAssignedLicenceApplyBaseChangeList(paramMap);
//			if( noticeSwApplyBaseChangeSwManagerList.size() > 0 ){
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleApplyBaseChange, noticeSwApplyBaseChangeSwManagerList, noticeType, "SW_APPLY_BASE_NOTI_MAIL", "APPLY_BASE_CHANGE_SW_LIST");
//			}
//			
//			// 소프트웨어 서버스펙변경 목록 통지
//			List noticeSwSpecChangeSwManagerList = swLicenceService.selectAmSwAssignedAmSlSpecChangeList(paramMap);
//			if( noticeSwSpecChangeSwManagerList.size() > 0 ){
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleSvSpecChange, noticeSwSpecChangeSwManagerList, noticeType, "SW_SPEC_NOTI_MAIL", "SPCE_CHANGE_SW_LIST");
//			}
//			
//			List noticeSwIllegalSwManagerList = swLicenceService.selectIllegalInstalledSwList(paramMap);
//			if( noticeSwIllegalSwManagerList.size() > 0 ){
//				// 불법 소프트웨어 목록 통지
//				parserSamNotice(fromUserEmail, fromUserId, noticeTitleIllegalSw, noticeSwIllegalSwManagerList, noticeType, "SW_ILLEGAL_NOTI_MAIL", "ILLEGAL_SW_LIST");
//			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }  
	
	/**
	 * 
	 * 소프트웨어 자산 신규등록건에 대하여 소프트웨어 그룹에도 생성시켜주고 통지해준다.
	 * 
	 * @param jobArgs
	 */
	public void insertAmSwGroupByNewAmSw(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			int insertAmSwGroupCount = itgJobService.insertAmSwGroupByNewAmSw();
			
			if( insertAmSwGroupCount > 0 ){
				//메일발송
				// 소프트웨어 담당자에게 통지
				Map alarmMap = new HashMap();
				alarmMap.put("KEY", "ITAM");
				alarmMap.put("TEMPLATE_ID", "NEW_SW_GROUP_NOTI_MAIL");
				
				Map dataMap = new HashMap();
				//수신자
				HashMap amSwManagerInfo = itgJobService.selectAmSwManagerInfo();
				List<String> list = new ArrayList<String>(amSwManagerInfo.keySet());
				dataMap.put("TO_USER_LIST", list);
				dataMap.put("TITLE", noticeTitleNewSwGroup);
				
				HashMap sendMap = new HashMap();
				List newSwGroupList =  itgJobService.selectNewSwGroupList();
				sendMap.put("NEW_SW_GROUP_LIST", newSwGroupList);
				EmailTemplateGenerator etg = new EmailTemplateGenerator();
				String result = etg.generateContent("NEW_SW_GROUP_NOTI_MAIL", sendMap);
				dataMap.put("CONTENT", result);
				alarmMap.put("DATA", dataMap);
				
				EmailSetData emailSender = new EmailSetData(alarmMap);
				emailSender.sendMail();
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }  
	
	/**
	 * 
	 * 소프트웨어 통지 리스트 생성
	 * 
	 * @param noticeTitle
	 * @param noticeList
	 * @param noticeType
	 * @param templateName
	 * @param tempalteParamName
	 * @throws NkiaException
	 */
	public void parserSamNotice(String fromUserEmail, String fromUserId, String noticeTitle, List noticeList, String noticeType, String templateName, String tempalteParamName)throws NkiaException{
		List sendNoticeList = new ArrayList();
		
		// 통보 생성 목록
		if( noticeList.size() > 0 ){
			HashMap sendMap = new HashMap();
			
			String toUserId = null;
			String toUserEmail = null;
			for( int i = 0; i < noticeList.size(); i++ ){
				HashMap noticeData = (HashMap)noticeList.get(i);
				String userId = (String)noticeData.get(noticeType + "_ID");
				String userEmail = (String)noticeData.get(noticeType + "_EMAIL");
				
				if( i == 0 ){
					toUserId = userId;
					toUserEmail = userEmail;
					sendNoticeList.add(noticeData);
					
					if( i == noticeList.size() - 1  ){
						sendMap.put(tempalteParamName, sendNoticeList);
						sendSamNotice(toUserEmail, toUserId, noticeTitle, sendMap, templateName);
					}
				}else if( i == noticeList.size() - 1 ){
					sendNoticeList.add(noticeData);
					
					sendMap.put(tempalteParamName, sendNoticeList);
					sendSamNotice(toUserEmail, toUserId, noticeTitle, sendMap, templateName);
				}else{
					if( !toUserId.equals(userId) ){
						sendMap.put(tempalteParamName, sendNoticeList);
						sendSamNotice(toUserEmail, toUserId, noticeTitle, sendMap, templateName);
						
						sendNoticeList = new ArrayList();
						
						toUserId = userId;
						toUserEmail = userEmail;
						sendNoticeList.add(noticeData);
					}else{
						sendNoticeList.add(noticeData);
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * 소프트웨어 메일/통지 발송
	 * 
	 * @param fromUserEmail
	 * @param fromUserId
	 * @param toUserEmail
	 * @param toUserId
	 * @param noticeTitle
	 * @param content
	 * @param templateName
	 * @throws NkiaException
	 */
	public void sendSamNotice(String toUserEmail, String toUserId, String noticeTitle, HashMap content, String templateName)throws NkiaException{
		Map alarmMap = new HashMap();
		
		try {
			//알림 메일
			alarmMap.put("KEY", "ITAM");
			alarmMap.put("TEMPLATE_ID", templateName);
			
			Map dataMap = new HashMap();
			
			// 1. 메일수신자셋팅(LIST로)
			List user = new ArrayList();
			HashMap toData = new HashMap();
			toData.put("TARGET_USER_ID", toUserId);
			toData.put("EMAIL", toUserEmail);
			user.add(toData);
			dataMap.put("TO_USER_LIST", user);
			// 2. 제목, 내용
			dataMap.put("TITLE", noticeTitle);
			EmailTemplateGenerator etg = new EmailTemplateGenerator();
			String result = etg.generateContent(templateName, content);
			dataMap.put("CONTENT", result);
			
			alarmMap.put("DATA", dataMap);
			
			EmailSetData emailSender = new EmailSetData(alarmMap);
			emailSender.sendMail();
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
	
	/**
	 * AD EMS자원 동기화 데이터 생성
	 */
	public void createAmAdEmsData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.createAmAdEmsData();
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }  
	
	/**
	 * 세션 정보 테이블 데이터 삭제 (3일 지난 세션)
	 */
	public void deleteSysLoginSession(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.deleteSysLoginSession();
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	

	/**
	 * 결과이력 삭제 스케쥴
	 */
	public void deleteScheduleResult(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.deleteScheduleResult();
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	
}
