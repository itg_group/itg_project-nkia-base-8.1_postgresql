/*
 * @(#)ItgJobServiceImpl.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.polestar.job.dao.NotificationJobDAO;
import com.nkia.intergration.polestar.job.service.NotificationJobService;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("notificationJobService")
public class NotificationJobServiceImpl implements NotificationJobService{
	
	@Resource(name = "notificationJobDAO")
	public NotificationJobDAO notificationJobDAO;
	
	@Override
	public List selectSysNtcnTySetupList(HashMap paramMap) throws NkiaException {
		return notificationJobDAO.selectSysNtcnTySetupList(paramMap);
	}
	@Override
	public List selectNtcnSetupList() throws NkiaException, SQLException {
		return notificationJobDAO.selectNtcnSetupList();
	}
	@Override
	public List selectExcuteQueryList(String excuteQuery) throws NkiaException, SQLException {
		return notificationJobDAO.selectExcuteQueryList(excuteQuery);
	}
	@Override
	public int selectNotificationBox(Map paramMap) throws NkiaException {
		return notificationJobDAO.selectNotificationBox(paramMap);
	}
	@Override
	public void insertNtcnBox(HashMap paramMap) throws NkiaException {
		notificationJobDAO.insertNtcnBox(paramMap);
	}
	@Override
	public void deleteNotificationBoxList() throws NkiaException {
		notificationJobDAO.deleteNotificationBoxList();
	}
}
