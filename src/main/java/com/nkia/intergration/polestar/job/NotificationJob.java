package com.nkia.intergration.polestar.job;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.intergration.polestar.job.service.NotificationJobService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.NotificationManager;
import com.nkia.itg.system.user.service.UserService;

@SuppressWarnings("rawtypes")
public class NotificationJob {
	
	private static final Logger logger = LoggerFactory.getLogger(NotificationJob.class);
	private static final String USE_Y = "Y";
	
	public void searchNotificationList(String[] jobArgs) throws NkiaException{
		NotificationJobService notificationJobService = (NotificationJobService) NkiaApplicationContext.getCtx().getBean("notificationJobService");
		
		try {
			HashMap<String, Object> paramMap = new HashMap<String, Object>();
			HashMap<String, Object> useMap = new HashMap<String, Object>();
			
			paramMap.put("useYn", USE_Y);
			List sysNtcnTySetupList = notificationJobService.selectSysNtcnTySetupList(paramMap);
			if(sysNtcnTySetupList != null && sysNtcnTySetupList.size() > 0){
				for( int i = 0; i < sysNtcnTySetupList.size(); i++ ){
					Map item = (Map) sysNtcnTySetupList.get(i);
					String ntcnTyCd = (String) item.get("NTCN_TY_CD");
					useMap.put(ntcnTyCd, true);
				}
				
				List ntcnSetupList = notificationJobService.selectNtcnSetupList();
				
				if(ntcnSetupList != null && ntcnSetupList.size() > 0){
					for( int i = 0; i < ntcnSetupList.size(); i++ ){
						Map ntcnSetupMap = (Map) ntcnSetupList.get(i);
						String colctQuery = (String)ntcnSetupMap.get("COLCT_QUERY_CN");
						
						List resultList = notificationJobService.selectExcuteQueryList(colctQuery);
						if(resultList != null && resultList.size() > 0){
							setNotificationData(ntcnSetupMap, useMap, resultList);
						}
					}
				}
			} else {
				logger.info("sysNtcnTySetupList is null!");
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	}
	
	private void setNotificationData(Map ntcnSetupMap, HashMap<String, Object> useMap, List resultList) throws NkiaException, JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, MessagingException{
		NotificationJobService notificationJobService = (NotificationJobService) NkiaApplicationContext.getCtx().getBean("notificationJobService");
		UserService userService = (UserService) NkiaApplicationContext.getCtx().getBean("userService");
		
		String ntcnMsgFormCn = (String)ntcnSetupMap.get("NTCN_MSSAGE_FORM_CN");
		String endMsgFormCn = (String)ntcnSetupMap.get("END_MSSAGE_FORM_CN");
		String executSetupVal = (String)ntcnSetupMap.get("EXECUT_SETUP_VAL");
			
		for( int j = 0; j < resultList.size(); j++ ){
			HashMap resultMap = (HashMap) resultList.get(j);
			String message = (String)resultMap.get("MESSAGE");
			String targetUserId = (String)resultMap.get("TRGET_USER_ID");
			long term = (Long)resultMap.get("TERM");
			int executSetupValInt = Integer.parseInt(executSetupVal);
			
			String returnMsg = null;
			if(term <= 0 & term >= executSetupValInt){
				returnMsg = msgFormReplace(ntcnMsgFormCn, message, term);
			} else {
				returnMsg = msgFormReplace(endMsgFormCn, message, term);
			}
			
			HashMap ntcnMap = new HashMap();
			ntcnMap.put("ntcn_setup_id", (String)ntcnSetupMap.get("NTCN_SETUP_ID"));
			ntcnMap.put("mssage_titl", (String)ntcnSetupMap.get("NTCN_SETUP_NM"));
			ntcnMap.put("trget_user_id", targetUserId);
			ntcnMap.put("mssage_cn", returnMsg);
			ntcnMap.put("term", term);
			ntcnMap.put("upd_user_id", "omsadmin");
			
			int cnt = notificationJobService.selectNotificationBox(ntcnMap);
			
			if(cnt == 0){
		
				notificationJobService.insertNtcnBox(ntcnMap);
				
				NotificationManager notiMng = new NotificationManager();
				notiMng.getNotificationSendJob(useMap, ntcnMap);
			}
		}
	}
	
	/*
	 * 메세지 내용 파싱
	 */
	private String msgFormReplace(String ntcnForm, String msg, long term) throws NkiaException {
		ntcnForm = ntcnForm.replace("$MESSAGE$", msg);
		String StrTerm = String.valueOf(term);
		ntcnForm = ntcnForm.replace("$TERM$", StrTerm);
		return ntcnForm;
	}
	
	/*
	 * 3개월이전 알림 내용 삭제
	 */
	public void deleteNotificationList(String[] jobArgs) throws NkiaException{
		NotificationJobService notificationJobService = (NotificationJobService) NkiaApplicationContext.getCtx().getBean("notificationJobService");
		notificationJobService.deleteNotificationBoxList();
	}
	
}
