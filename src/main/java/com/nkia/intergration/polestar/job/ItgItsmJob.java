/*
 * @(#)ItgItsmJob.java              2020. 4. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.intergration.polestar.job.service.ItgItsmJobService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.system.scheduler.service.SchedulerService;

public class ItgItsmJob implements Job{
	
	public static final Logger logger = LoggerFactory.getLogger(ItgJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId);
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * Job Handler - DB 방식인 경우 실행시간에 맞춰 execute 메소드가 호출 된다.
	*/ 
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		// Job Args 값 설정
        String scheduleId = jobDataMap.getString("scheduleId");
        String scheduleNm = jobDataMap.getString("scheduleNm");
        String executeMethod = jobDataMap.getString("executeMethod");
        String scheduleConfig = jobDataMap.getString("scheduleConfig");
		String executor = jobDataMap.getString("executor"); 
		String[] jobArgs = {scheduleId, scheduleNm, scheduleConfig, executor};
		
		if("sendMailLNIncreaseEnd".equalsIgnoreCase(executeMethod)){
			sendMailLNIncreaseEnd(jobArgs);
		}	
	}
	
	/**
	 * 
	 * 회선 자산중 증속종료일 3일전 데이터를 담당자에게 이메일로 발송한다 
	 * 
	 * @param jobArgs
	 */
	public void sendMailLNIncreaseEnd(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgItsmJobService itgItsmJobService = (ItgItsmJobService) NkiaApplicationContext.getCtx().getBean("itgItsmJobService");
			List<HashMap> assetLNList = itgItsmJobService.selectAssetLnIncreaseEndList();
			List<HashMap> alarmUserList = itgItsmJobService.selectAssetLNAlarmUserList();
			
			for(HashMap assetLNInfo : assetLNList) {
				// 메일발송
				// 작업알림(영업) 담당자에게 통지
				Map mailInfoMap = new HashMap();
				mailInfoMap.put("KEY", "INCREASE_END_MAIL");
				mailInfoMap.put("TEMPLATE_ID", "INCREASE_END_MAIL");
				mailInfoMap.put("TO_USER_LIST", alarmUserList);
				mailInfoMap.put("DATA", assetLNInfo);
				mailInfoMap.put("LINK_URL", NkiaApplicationPropertiesMap.getProperty("Globals.Email.ItsmUrL"));
				mailInfoMap.put("EMAIL_TITLE", "[" + assetLNInfo.get("SERVICE_CUSTOMER_ID_NM") + "] 회선 증속종료 원복 대상 안내 ");
				
				EmailSetData emailSender = new EmailSetData(mailInfoMap);
				emailSender.sendMail();
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
}
