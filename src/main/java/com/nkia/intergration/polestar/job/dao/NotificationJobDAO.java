/*
 * @(#)ItgJobDAO.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("notificationJobDAO")
public class NotificationJobDAO extends EgovComAbstractDAO{

	/**
	 * 
	 * SYSTEM 알림 유형 설정 조회
	 * 
	 * @param HashMap map
	 * @return
	 * @throws NkiaException
	 */
	public List selectSysNtcnTySetupList(HashMap paramMap)throws NkiaException {
		return list("NotificationJobDAO.selectSysNtcnTySetupList", paramMap);
	}
	
	/**
	 * 
	 * 알림기능내역 조회
	 * 
	 * @param HashMap map
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */
	public List selectNtcnSetupList()throws NkiaException, SQLException {
		//@@최양규 20160715 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 시작		
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("NotificationJobDAO.selectNtcnSetupList", null, handler);
		return handler.getReturnList();
		//@@최양규 20160715 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 종료
	}

	/**
	 * 
	 * 수집 쿼리 내용으로  조회
	 * 
	 * @param HashMap map
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */
	public List selectExcuteQueryList(String excuteQuery)throws NkiaException, SQLException {
		//@@최양규 20160715 리스트 조회시 BigDecimal 도 가져올수 있게끔 핸들러 사용 시작		
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("NotificationJobDAO.excuteQuery", excuteQuery, handler);
		return handler.getReturnList();
		//@@최양규 20160715 리스트 조회시 BigDecimal 도 가져올수 있게끔 핸들러 사용 종료
	}
	
	/**
	 * 
	 * 수집 쿼리 내용으로  조회
	 * 
	 * @param HashMap map
	 * @return
	 * @throws NkiaException
	 */
	public int selectNotificationBox(Map paramMap)throws NkiaException {
		return (Integer) this.selectByPk("NotificationJobDAO.selectNotificationBox", paramMap);
	}
	
	/**
	 * 
	 * 알림함 등록
	 * 
	 * @param HashMap map
	 * @return
	 * @throws NkiaException
	 */
	public void insertNtcnBox(HashMap paramMap)throws NkiaException {
		insert("NotificationJobDAO.insertNtcnBox", paramMap);
	}
	
	/**
	 * 
	 * 알림함 등록
	 * 
	 * @param HashMap map
	 * @return
	 * @throws NkiaException
	 */
	public void deleteNotificationBoxList()throws NkiaException {
		delete("NotificationJobDAO.deleteNotificationBoxList", null);
	}
	
}
