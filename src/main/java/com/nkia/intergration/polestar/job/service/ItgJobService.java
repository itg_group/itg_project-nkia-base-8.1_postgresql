/*
 * @(#)ItgJobService.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;


public interface ItgJobService {
	
	/**
	 * 
	 * DCA 서버 OA 등록
	 * 
	 * @param dcaServerOaList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerOaList(List<HashMap> dcaServerOaList)throws NkiaException;

	/**
	 * 
	 * DCA 서버 CPUS 등록
	 * 
	 * @param dcaServerCpusList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerCpusList(List<HashMap> dcaServerCpusList)throws NkiaException;
	
	/**
	 * 
	 * DCA 서버 DISKS 등록
	 * 
	 * @param dcaServerDisksList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerDisksList(List<HashMap> dcaServerDisksList)throws NkiaException;

	/**
	 * 
	 * DCA 서버 DISK 등록
	 * 
	 * @param dcaServerDiskList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerDiskList(List<HashMap> dcaServerDiskList)throws NkiaException;

	/**
	 * 
	 * DCA 서버 MEMORY 등록
	 * 
	 * @param dcaServerMemoryList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerMemoryList(List<HashMap> dcaServerMemoryList)throws NkiaException;
	
	/**
	 * 
	 * DCA 서버 NW INTERFACES 등록
	 * 
	 * @param dcaServerNwInterfacesList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerNwInterfacesList(List<HashMap> dcaServerNwInterfacesList)throws NkiaException;
	
	/**
	 * 
	 * DCA 서버 NE INTERFACE 등록
	 * 
	 * @param dcaServerNwInterfaceList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerNwInterfaceList(List<HashMap> dcaServerNwInterfaceList)throws NkiaException;
	
	/**
	 * 
	 * DCA 서버 HBA 등록
	 * 
	 * @param dcaServerHbaList
	 * @throws NkiaException
	 */
	public void insertIfDcaServerHbaList(List<HashMap> dcaServerHbaList)throws NkiaException;
	
	/**
	 * 
	 * DCA 네트워크 시스템 등록
	 * 
	 * @param dcaNwSystemInfoList
	 * @throws NkiaException
	 */
	public void insertIfDcaNwSystemInfoList(List<HashMap> dcaNwSystemInfoList)throws NkiaException;
	
	/**
	 * 
	 * DCA 네트워크 모듈 등록
	 * 
	 * @param dcaNwNetworkModuleList
	 * @throws NkiaException
	 */
	public void insertIfDcaNwNetworkModuleList(List<HashMap> dcaNwNetworkModuleList)throws NkiaException;
	
	/**
	 * 
	 * DCA 네트워크 인터페이스 등록
	 * 
	 * @param dcaNwNetworkInterfaceList
	 * @throws NkiaException
	 */
	public void insertIfDcaNwInterfaceList(List<HashMap> dcaNwNetworkInterfaceList)throws NkiaException;
	
	/**
	 * 
	 * DCA 설치소프트웨어 등록
	 * 
	 * @param dcaInstalledSwList
	 * @throws NkiaException
	 */
	public void insertIfDcaInstalledSwList(List<HashMap> dcaInstalledSwList)throws NkiaException;
	
	/**
	 * 
	 * DCA 수집 소프트웨어 마스터 등록
	 * 
	 * @param dcaSwObjectList
	 * @throws NkiaException
	 */
	public void insertIfDcaSwMasterObjectList(List<HashMap> dcaSwObjectList)throws NkiaException;
	
	/**
	 * AM서버중에 DCA에 미등록된 자산을 조회 
	 * @return
	 * @throws NkiaException
	 */
	public List selectUnregistDcaByAmServerPhysi()throws NkiaException;
	
	public List selectUnregistDcaByAmNetwork()throws NkiaException;
	
	public List selectUnregistDcaByAmServerLogicPartition()throws NkiaException;
	
	/**
	 * 
	 * DCA 서버 데이터 초기등록
	 * @param syncPeriod 
	 * 
	 * @throws NkiaException
	 */
	public void updateSyncDcaDataToAmServer(String syncPeriod)throws NkiaException;
	
	/**
	 * 
	 * DCA 설치된 소프트웨어 등록
	 * 
	 * @throws NkiaException
	 */
	public void mergeSyncDcaDataToAmServerInstalledSw()throws NkiaException;
	
	/**
	 * 
	 * DCA 네트워크 데이터 초기등록
	 * 
	 * @throws NkiaException
	 */
	public void updateSyncDcaDataToAmNetwork(String syncPeriod)throws NkiaException;
	
	/**
	 * 
	 * AD 자원 동기화 불일치 데이터 생성
	 * 
	 * @throws NkiaException
	 */
	public void createAmAdDifData()throws NkiaException;
	
	
	/**
	 * 
	 * AM 가상화 목록
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerLogicVmList()throws NkiaException;
	
	/**
	 * 
	 * 시리얼번호가 일치되는 물리서버 정보
	 * 
	 * @param serialNo
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerPhysicalBySerialNo(String serialNo)throws NkiaException;
	
	/**
	 * 
	 * AM -> AD DEVICE 데이터 현행화
	 * 
	 * @param wsResultData
	 * @throws NkiaException
	 */
	public void mergeIfAdDeviceByAm(HashMap wsResultData)throws NkiaException;
	
	
	/**
	 * 
	 * DCA Partition 데이터 현행화
	 * 
	 * @param dcaVmData
	 * @throws NkiaException
	 */
	public void mergeIfAdPartitionByDca(HashMap dcaPartitionData)throws NkiaException;
	
	/**
	 * 
	 * DCA VM 데이터 현행화
	 * 
	 * @param dcaVmData
	 * @throws NkiaException
	 */
	public void mergeIfAdVmByDca(HashMap dcaVmData)throws NkiaException;
	
	/**
	 * 
	 * VM등록시에 자산의 가상화장비 정보를 VM HOST_IP 기준으로 가져오기
	 * 
	 * @param hostIp
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerPhysiByVmHostIp(String hostIp)throws NkiaException;
	
	/**
	 * 
	 * VM등록시에 자산의 가상화장비 정보를 VM SERIAL_NO 기준으로 가져오기
	 * 
	 * @param serialNo
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerPhysiByVmHostSerial(String serialNo)throws NkiaException;
	
	/**
	 * 
	 * VM ID로 등록된 논리서버정보를 가져온다.
	 * 
	 * @param vmId
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAmServerLogicByVmId(String vmId)throws NkiaException;
	
	/**
	 * 
	 * 소프트웨어 자산 신규등록건에 대하여 소프트웨어 그룹에도 생성
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public int insertAmSwGroupByNewAmSw()throws NkiaException;
	
	/**
	 * 
	 * 자산 시스템관리자 정보(통보용)
	 * 
	 * @throws NkiaException
	 */
	public HashMap selectAmSystemAdminInfo()throws NkiaException;
	
	/**
	 * 
	 * 소프트웨어 관리자 정보(통보용)
	 * 
	 * @throws NkiaException
	 */
	public HashMap selectAmSwManagerInfo()throws NkiaException;
	
	/**
	 * 
	 * 신규 소프트웨어 그룹 List
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List selectNewSwGroupList()throws NkiaException;

	public List selectCheckAmStorageList()throws NkiaException;
	
	public void updateSyncDcaDataToAmServerAtOnce(HashMap serverData)throws NkiaException;

	public void updateSyncDcaDataToAmNetworkAtOnce(HashMap nwData)throws NkiaException;
	
	/**
	 * 
	 * SMSAgentKey 등록을 위해 DeviceID로 조회
	 * 
	 * @param deviceId
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerPhysiByDcaId(String deviceId)throws NkiaException;

	/**
	 * 
	 * SmsAgentKey 등록
	 * 
	 * @param serverInfo
	 * @throws NkiaException
	 */
	public void updateSmsAgentKey(HashMap serverInfo)throws NkiaException;
	
	/**
	 * 
	 * AD 자원 EMS 동기화 불일치 데이터 생성
	 * 
	 * @throws NkiaException
	 */
	public void createAmAdEmsData()throws NkiaException;

	public void insertIfDcaServerHbaCntList(List<HashMap> dcaServerHbaCntList)throws NkiaException;
	
	public void deleteSysLoginSession()throws NkiaException;

	public void deleteScheduleResult() throws NkiaException;
	
}
