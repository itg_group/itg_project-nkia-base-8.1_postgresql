/*
 * @(#)ItgJobServiceImpl.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.polestar.job.dao.ItgJobDAO;
import com.nkia.intergration.polestar.job.service.ItgJobService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;

@Service("itgJobService")
public class ItgJobServiceImpl implements ItgJobService {
	
	@Resource(name = "itgJobDAO")
	public ItgJobDAO itgJobDAO;
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	@Override
	public void insertIfDcaServerOaList(List<HashMap> dcaServerOaList) throws NkiaException {
		itgJobDAO.truncateIfDcaServerOa();
		for(HashMap dcaServerOaData: dcaServerOaList) {
			itgJobDAO.insertIfDcaServerOa(dcaServerOaData);
		}
	}

	@Override
	public void insertIfDcaServerCpusList(List<HashMap> dcaServerCpusList)throws NkiaException {
		itgJobDAO.truncateIfDcaServerCpus();
		for(HashMap dcaServerCpusData: dcaServerCpusList) {
			itgJobDAO.insertIfDcaServerCpus(dcaServerCpusData);
		}
	}
	
	@Override
	public void insertIfDcaServerDisksList(List<HashMap> dcaServerDisksList)throws NkiaException {
		itgJobDAO.truncateIfDcaServerDisks();
		for(HashMap dcaServerDisksData: dcaServerDisksList) {
			itgJobDAO.insertIfDcaServerDisks(dcaServerDisksData);
		}
	}

	@Override
	public void insertIfDcaServerDiskList(List<HashMap> dcaServerDiskList)throws NkiaException {
		itgJobDAO.truncateIfDcaServerDisk();
		for(HashMap dcaServerDiskData: dcaServerDiskList) {
			itgJobDAO.insertIfDcaServerDisk(dcaServerDiskData);
		}
	}

	@Override
	public void insertIfDcaServerMemoryList(List<HashMap> dcaServerMemoryList)throws NkiaException {
		itgJobDAO.truncateIfDcaServerMemory();
		for(HashMap dcaServerMemoryData: dcaServerMemoryList) {
			itgJobDAO.insertIfDcaServerMemory(dcaServerMemoryData);
		}
	}
	
	@Override
	public void insertIfDcaServerNwInterfacesList(List<HashMap> dcaServerNwInterfacesList) throws NkiaException {
		itgJobDAO.truncateIfDcaServerNwInterfaces();
		for(HashMap dcaServerNwInterfacesData: dcaServerNwInterfacesList) {
			itgJobDAO.insertIfDcaServerNwInterfaces(dcaServerNwInterfacesData);
		}
	}
  
	@Override
	public void insertIfDcaServerNwInterfaceList(List<HashMap> dcaServerNwInterfaceList)throws NkiaException {
		itgJobDAO.truncateIfDcaServerNwInterface();
		for(HashMap dcaServerNwInterfaceData: dcaServerNwInterfaceList) {
			itgJobDAO.insertIfDcaServerNwInterface(dcaServerNwInterfaceData);
		}
	}
	
	@Override
	public void insertIfDcaServerHbaList(List<HashMap> dcaServerHbaList)throws NkiaException {
		itgJobDAO.truncateIfDcaServerHba();
		for(HashMap dcaServerHbaData: dcaServerHbaList) {
			itgJobDAO.insertIfDcaServerHba(dcaServerHbaData);
		}
	}

	@Override
	public void insertIfDcaNwSystemInfoList(List<HashMap> dcaNwSystemInfoList)throws NkiaException {
		itgJobDAO.truncateIfDcaNwSystemInfo();
		for(HashMap dcaNwSystemInfoData: dcaNwSystemInfoList) {
			itgJobDAO.insertIfDcaNwSystemInfo(dcaNwSystemInfoData);
		}
	}
	
	@Override
	public void insertIfDcaNwNetworkModuleList(List<HashMap> dcaNwNetworkModuleList) throws NkiaException {
		itgJobDAO.truncateIfDcaNwNetworkModule();
		for(HashMap dcaNwNetworkModuleData: dcaNwNetworkModuleList) {
			itgJobDAO.insertIfDcaNwNetworkModule(dcaNwNetworkModuleData);
		}
	}

	@Override
	public void insertIfDcaNwInterfaceList(List<HashMap> dcaNwNetworkInterfaceList) throws NkiaException {
		itgJobDAO.truncateIfDcaNwInterface();
		for(HashMap dcaNwNetworkInterfaceData: dcaNwNetworkInterfaceList) {
			itgJobDAO.insertIfDcaNwInterface(dcaNwNetworkInterfaceData);
		}
	}

	@Override
	public void insertIfDcaInstalledSwList(List<HashMap> dcaInstalledSwList)throws NkiaException {
		itgJobDAO.truncateIfDcaInstalledSw();
		for(HashMap dcaInstalledSwData: dcaInstalledSwList) {
			itgJobDAO.insertIfDcaInstalledSw(dcaInstalledSwData);
		}
		itgJobDAO.truncateAmSwInstlGroup();
		// 수집된 pc sw 를 그룹으로 저장	- 본사 ITAM 용 (SW를 PC, SV 구분할 경우에 사용함.)
//		itgJobDAO.insertAmSwInstlPcGroup();	
		// 수집된 서버 sw 를 그룹으로 저장
		itgJobDAO.insertAmSwInstlSvGroup();
	}

	@Override
	public void insertIfDcaSwMasterObjectList(List<HashMap> dcaSwObjectList)throws NkiaException {
		for(HashMap dcaSwObjectData: dcaSwObjectList) {
			itgJobDAO.insertIfDcaSwMasterObject(dcaSwObjectData);
		}
	}

	@Override
	public List selectUnregistDcaByAmServerPhysi() throws NkiaException {
		return itgJobDAO.selectUnregistDcaByAmServerPhysi();
	}
	
	@Override
	public List selectUnregistDcaByAmServerLogicPartition() throws NkiaException {
		return itgJobDAO.selectUnregistDcaByAmServerLogicPartition();
	}
	
	@Override
	public List selectUnregistDcaByAmNetwork() throws NkiaException {
		return itgJobDAO.selectUnregistDcaByAmNetwork();
	} 
	
	/**
	 * 
	 * DCA 서버 데이터 초기등록
	 * 
	 * @throws NkiaException
	 */
	@Override
	public void updateSyncDcaDataToAmServer(String syncPeriod) throws NkiaException {
		if( syncPeriod.equals("ONCE") ){
			List syncDcaAmServerList = itgJobDAO.selectSyncDcaAmServerList();
			if( syncDcaAmServerList != null && syncDcaAmServerList.size() > 0 ){
				for( int i = 0; i < syncDcaAmServerList.size(); i++ ){
					HashMap syncDcaAmServerData = (HashMap)syncDcaAmServerList.get(i);
					updateSyncDcaDataToAmServerAtOnce(syncDcaAmServerData);
				}
			}
		}else if( syncPeriod.equals("DAILY") ){
			// 논리서버의 신규 수집된 스펙(속성)을 불러온다.
			List newLogicSpecList = assetHistoryService.selectNewLogicAttrList();
			
			if(null != newLogicSpecList && newLogicSpecList.size() > 0) {
				
				for(int i=0; i<newLogicSpecList.size(); i++) {
					
					HashMap specMap = new HashMap();
					specMap = (HashMap)newLogicSpecList.get(i);
					
					String assetId = (String)specMap.get("ASSET_ID");
					String confId = (String)specMap.get("CONF_ID");
					
					specMap.remove("ASSET_ID");
					specMap.remove("CONF_ID");
					specMap.remove("CLASS_ID");
					specMap.remove("CLASS_TYPE");
					
					List newDataList = new ArrayList();
					Iterator iter = specMap.keySet().iterator();
					while(iter.hasNext()){
						String key = (String) iter.next();
						HashMap tempMap = new HashMap();
						tempMap.put("COLUMN_ID", key);
						tempMap.put("CHG_VALUE", specMap.get(key));
						newDataList.add(tempMap);
					}
					// 신규 수집된 값으로 이력 생성 모듈 호출
//					assetHistoryService.updateAssetAttrHistory(assetId,  confId, classId, "SL", "DCA-DB", "시스템 자동 업데이트", specMap);
					assetHistoryService.insertAssetAttrHistory(confId, newDataList, "IF 자동 업데이트", "DCA-DB");
				}
			}
						
			// 논리서버 정보 업데이트
			itgJobDAO.updateAmSvLogicFromDcaDataOnDaily();
			
			
			// 개별 DISK 정보 삭제
			itgJobDAO.truncateAmInsDisk();
			
			// 개별 DISK 정보 업데이트
			itgJobDAO.insertAmInsDiskFromDcaDataOnDaily();
			
			// 개별 DISK 정보 이력 생성 모듈 호출
			assetHistoryService.insertDcaInsDiskHistory("DCA-DB");
//			assetHistoryService.updateInsDiskHistory("DCA-DB");
			
			
			// 개별 NIF 정보 삭제
			itgJobDAO.truncateAmInsNif();
			
			// 개별 네트워크 정보 인터페이스 등록
			itgJobDAO.insertAmInsNifFromDcaDataOnDaily();

			// 개별 NIF 정보 이력 생성 모듈 호출
//			assetHistoryService.updateInsNifHistory("DCA-DB");
			assetHistoryService.insertDcaInsNifHistory("DCA-DB");
			
			// 개별 HBA 정보 삭제
			//itgJobDAO.truncateAmInsHba();
			
			// 개별 HBA 데이터 등록
			//itgJobDAO.insertAmInsHbaFromDcaDataOnDaily();
		}
	}
	
	public void updateSyncDcaDataToAmServerAtOnce(HashMap syncDcaAmServerData)throws NkiaException{
		// 서버 정보 업데이트
		itgJobDAO.updateAmSvLogicFromDcaData(syncDcaAmServerData);
		
		// 개별 DISK 정보 업데이트
		itgJobDAO.insertAmInsDiskFromDcaData(syncDcaAmServerData);
		
		// 개별 네트워크 정보 인터페이스 등록
		itgJobDAO.insertAmInsNifFromDcaData(syncDcaAmServerData);
		
		// 개별 HBA 데이터 등록
		//itgJobDAO.insertAmInsHbaFromDcaData(syncDcaAmServerData);
		
		// DCA 데이터 동기화 여부 업데이트('Y')
		itgJobDAO.updateAmInfraDcaDataSyncYn(syncDcaAmServerData);
	}
	
	/**
	 * 
	 * DCA 설치된 소프트웨어 등록
	 * 
	 * @throws NkiaException
	 */
	@Override
	public void mergeSyncDcaDataToAmServerInstalledSw() throws NkiaException {
		// 신규 소프트웨어 등록
		itgJobDAO.insertNotRegistAmInsSwFromDcaData();
		
//		// 라이선스 할당 불일치 AD소프트웨어 수집실패로 업데이트
//		itgJobDAO.updateAssignedLicenceAmInsSwByDcaDataDif();
//		
//		// 라이선스 미할당 불일치 AD소프트웨어 삭제
//		itgJobDAO.deleteUnassignedLicenceAmInsSwByDcaDataDif();
	}

	@Override
	public void updateSyncDcaDataToAmNetwork(String syncPeriod) throws NkiaException {
		if( syncPeriod.equals("ONCE") ){
			List syncDcaAmNetworkList = itgJobDAO.selectSyncDcaAmNetworkList();
			if( syncDcaAmNetworkList != null && syncDcaAmNetworkList.size() > 0 ){
				for( int i = 0; i < syncDcaAmNetworkList.size(); i++ ){
					HashMap syncDcaAmNetworkData = (HashMap)syncDcaAmNetworkList.get(i);
					
					updateSyncDcaDataToAmNetworkAtOnce(syncDcaAmNetworkData);
				}
			}
		}else if( syncPeriod.equals("DAILY") ){
			// 네트워크 정보 업데이트
			itgJobDAO.updateAmNwFromDcaDataOnDaily();
			
			// 네트워크 보안 정보 업데이트
			itgJobDAO.updateAmNsFromDcaDataOnDaily();
			
			// 개별 네트워크 정보 삭제
			itgJobDAO.truncateAmInsNwModule();
			
			// 개별 네트워크 정보 모듈 등록
			itgJobDAO.insertAmInsNwModuleFromDcaDataOnDaily();
			
			// 개별 네트워크 정보 삭제
			itgJobDAO.truncateAmInsNwNif();
			
			// 개별 네트워크 정보 인터페이스 등록
			itgJobDAO.insertAmInsNwNifFromDcaDataOnDaily();
		}
	}
	
	public void updateSyncDcaDataToAmNetworkAtOnce(HashMap syncDcaAmNetworkData)throws NkiaException{
		String classType = (String)syncDcaAmNetworkData.get("CLASS_TYPE");
		if( classType.equals("NW") ){
			// 네트워크 정보 업데이트
			itgJobDAO.updateAmNwFromDcaData(syncDcaAmNetworkData);
		}else if( classType.equals("NS") ){
			// 네트워크 보안 정보 업데이트
			itgJobDAO.updateAmNsFromDcaData(syncDcaAmNetworkData);	
		}
		
		// 개별 네트워크 정보 모듈 등록
		itgJobDAO.insertAmInsNwModuleFromDcaData(syncDcaAmNetworkData);
		
		// 개별 네트워크 정보 인터페이스 등록
		itgJobDAO.insertAmInsNwNifFromDcaData(syncDcaAmNetworkData);
		
		// DCA 데이터 동기화 여부 업데이트('Y')
		itgJobDAO.updateAmInfraDcaDataSyncYn(syncDcaAmNetworkData);
	}
	
	@Override
	public void createAmAdDifData() throws NkiaException {
		itgJobDAO.createAmAdDifData();
	}

	@Override
	public List selectAmServerLogicVmList() throws NkiaException {
		return itgJobDAO.selectAmServerLogicVmList();
	}

	@Override
	public void mergeIfAdDeviceByAm(HashMap wsResultData) throws NkiaException {
		itgJobDAO.mergeIfAdDeviceByAm(wsResultData);
	}

	@Override
	public void mergeIfAdPartitionByDca(HashMap dcaPartitionData)throws NkiaException {
		itgJobDAO.mergeIfAdPartitionByDca(dcaPartitionData);
	}
	
	@Override
	public void mergeIfAdVmByDca(HashMap dcaVmData) throws NkiaException {
		itgJobDAO.mergeIfAdVmByDca(dcaVmData);
	}
	
	@Override
	public List selectAmServerPhysicalBySerialNo(String serialNo)throws NkiaException {
		return itgJobDAO.selectAmServerPhysicalBySerialNo(serialNo);
	}
	
	/**
	 * 
	 * VM등록시에 자산의 가상화장비 정보를 VM HOST_IP 기준으로 가져오기
	 * 
	 * @param hostIp
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectAmServerPhysiByVmHostIp(String hostIp) throws NkiaException {
		return itgJobDAO.selectAmServerPhysiByVmHostIp(hostIp);
	}
	
	/**
	 * 
	 * VM등록시에 자산의 가상화장비 정보를 VM SERIAL_NO 기준으로 가져오기
	 * 
	 * @param serialNo
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectAmServerPhysiByVmHostSerial(String serialNo)throws NkiaException {
		return itgJobDAO.selectAmServerPhysiByVmHostSerial(serialNo);
	}
	
	/**
	 * 
	 * VM ID로 등록된 논리서버정보를 가져온다.
	 * 
	 * @param vmId
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public HashMap selectAmServerLogicByVmId(String vmId) throws NkiaException {
		return itgJobDAO.selectAmServerLogicByVmId(vmId);
	}
	
	/**
	 * 
	 * 소프트웨어 자산 신규등록건에 대하여 소프트웨어 그룹에도 생성
	 * 
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public int insertAmSwGroupByNewAmSw() throws NkiaException {
		return itgJobDAO.insertAmSwGroupByNewAmSw();
	}

	@Override
	public HashMap selectAmSystemAdminInfo() throws NkiaException {
		return itgJobDAO.selectAmSystemAdminInfo();
	}

	@Override
	public HashMap selectAmSwManagerInfo() throws NkiaException {
		return itgJobDAO.selectAmSwManagerInfo();
	}

	@Override
	public List selectNewSwGroupList() throws NkiaException {
		return itgJobDAO.selectNewSwGroupList();
	}

	@Override
	public List selectCheckAmStorageList() throws NkiaException {
		return itgJobDAO.selectCheckAmStorageList();
	}

	@Override
	public List selectAmServerPhysiByDcaId(String deviceId)throws NkiaException {
		return itgJobDAO.selectAmServerPhysiByDcaId(deviceId);
	}	
	@Override
	public void updateSmsAgentKey(HashMap serverInfo)throws NkiaException {
			itgJobDAO.updateSmsAgentKey(serverInfo);
	}	
	@Override
	public void createAmAdEmsData() throws NkiaException {
		itgJobDAO.createAmAdEmsData();
	}

	@Override
	public void insertIfDcaServerHbaCntList(List<HashMap> dcaServerHbaCntList)throws NkiaException {
		itgJobDAO.truncateIfDcaServerHbaCnt();
		for(HashMap dcaServerHbaData: dcaServerHbaCntList) {
			itgJobDAO.insertIfDcaServerHbaCnt(dcaServerHbaData);
		}
	}
	
	@Override
	public void deleteSysLoginSession()throws NkiaException {
		itgJobDAO.deleteSysLoginSession();
	}

	@Override
	public void deleteScheduleResult() throws NkiaException {
		itgJobDAO.deleteScheduleResult();
	}
	
}
