/*
 * @(#)ItgJobService.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface NotificationJobService {
	
	/**
	 * SYSTEM 알림 유형 설정 조회 
	 * @return
	 * @throws NkiaException
	 */
	public List selectSysNtcnTySetupList(HashMap paramMap)throws NkiaException;
	
	/**
	 * 알림기능내역 조회 
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */
	public List selectNtcnSetupList()throws NkiaException, SQLException;
	/**
	 * 수집 쿼리 내용으로  조회 
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */
	public List selectExcuteQueryList(String excuteQuery)throws NkiaException, SQLException;
	/**
	 * 수집 쿼리 내용으로  조회 
	 * @return
	 * @throws NkiaException
	 */
	public int selectNotificationBox(Map paramMap)throws NkiaException;
	/**
	 * 알림함 등록 
	 * @return
	 * @throws NkiaException
	 */
	public void insertNtcnBox(HashMap paramMap) throws NkiaException;
	/**
	 * 지난 알림들 삭제 
	 * @return
	 * @throws NkiaException
	 */
	public void deleteNotificationBoxList() throws NkiaException;
}
