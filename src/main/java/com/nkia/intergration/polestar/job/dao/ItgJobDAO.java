/*
 * @(#)ItgJobDAO.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("itgJobDAO")
public class ItgJobDAO extends EgovComAbstractDAO{

	public void truncateIfDcaServerOa()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerOa", null);
	}

	public void truncateIfDcaServerCpus()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerCpus", null);
	}
	
	public void truncateIfDcaServerDisks()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerDisks", null);
	}

	public void truncateIfDcaServerDisk()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerDisk", null);
	}

	public void truncateIfDcaServerMemory()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerMemory", null);
	}

	public void truncateIfDcaServerNwInterfaces()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerNwInterfaces", null);
	}
	
	public void truncateIfDcaNwNetworkModule()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaNwNetworkModule", null);
	}
	
	public void truncateIfDcaServerNwInterface()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerNwInterface", null);
	}
	
	public void truncateIfDcaServerHba()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerHba", null);
	}

	public void truncateIfDcaNwSystemInfo()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaNwSystemInfo", null);
	}

	public void truncateIfDcaNwInterface()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaNwInterface", null);
	}

	public void truncateIfDcaInstalledSw()throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaInstalledSw", null);
	}
	
	public void insertIfDcaServerOa(HashMap dcaServerOaData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerOa", dcaServerOaData);
	}

	public void insertIfDcaServerCpus(HashMap dcaServerCpusData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerCpus", dcaServerCpusData);
	}
	
	public void insertIfDcaServerDisks(HashMap dcaServerDisksData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerDisks", dcaServerDisksData);
	}

	public void insertIfDcaServerDisk(HashMap dcaServerDiskData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerDisk", dcaServerDiskData);
	}

	public void insertIfDcaServerMemory(HashMap dcaServerMemoryData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerMemory", dcaServerMemoryData);
	}

	public void insertIfDcaServerNwInterface(HashMap dcaServerNwInterfaceData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerNwInterface", dcaServerNwInterfaceData);
	}
	
	public void insertIfDcaServerHba(HashMap dcaServerHbaData) {
		insert("ItgJobDAO.insertIfDcaServerHba", dcaServerHbaData);
	}

	public void insertIfDcaNwSystemInfo(HashMap dcaNwSystemInfoData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaNwSystemInfo", dcaNwSystemInfoData);
	}

	public void insertIfDcaServerNwInterfaces(HashMap dcaServerNwInterfacesData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerNwInterfaces", dcaServerNwInterfacesData);
	}
	
	public void insertIfDcaNwNetworkModule(HashMap dcaNwNetworkModuleData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaNwNetworkModule", dcaNwNetworkModuleData);
	}
	
	public void insertIfDcaNwInterface(HashMap dcaNwNetworkInterfaceData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaNwInterface", dcaNwNetworkInterfaceData);
	}

	public void insertIfDcaInstalledSw(HashMap dcaInstalledSwData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaInstalledSw", dcaInstalledSwData);
	}

	public void insertIfDcaSwMasterObject(HashMap dcaSwObjectData)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaSwMasterObject", dcaSwObjectData);
	}

	public List selectUnregistDcaByAmServerPhysi()throws NkiaException {
		return list("ItgJobDAO.selectUnregistDcaByAmServerPhysi", null);
	}
	
	public List selectUnregistDcaByAmServerLogicPartition()throws NkiaException {
		return list("ItgJobDAO.selectUnregistDcaByAmServerLogicPartition", null);
	}
	
	public List selectUnregistDcaByAmNetwork()throws NkiaException {
		return list("ItgJobDAO.selectUnregistDcaByAmNetwork", null);
	}
	
	public List selectSyncDcaAmServerList()throws NkiaException {
		return list("ItgJobDAO.selectSyncDcaAmServerList", null);
	}

	public void updateAmSvLogicFromDcaData(HashMap syncDcaAmServerData)throws NkiaException {
		update("ItgJobDAO.updateAmSvLogicFromDcaData", syncDcaAmServerData);
	}

	public void insertAmInsDiskFromDcaData(HashMap syncDcaAmServerData)throws NkiaException {
		insert("ItgJobDAO.insertAmInsDiskFromDcaData", syncDcaAmServerData);
	}

	public void insertAmInsNifFromDcaData(HashMap syncDcaAmServerData)throws NkiaException {
		insert("ItgJobDAO.insertAmInsNifFromDcaData", syncDcaAmServerData);
	}

	public void insertAmInsHbaFromDcaData(HashMap syncDcaAmServerData)throws NkiaException {
		insert("ItgJobDAO.insertAmInsHbaFromDcaData", syncDcaAmServerData);
	}

	public void updateAmInfraDcaDataSyncYn(HashMap syncDcaAmServerData)throws NkiaException {
		update("ItgJobDAO.updateAmInfraDcaDataSyncYn", syncDcaAmServerData);
	}

	public void insertNotRegistAmInsSwFromDcaData()throws NkiaException {
		insert("ItgJobDAO.insertNotRegistAmInsSwFromDcaData", null);
	}
	
	public void updateAssignedLicenceAmInsSwByDcaDataDif()throws NkiaException {
		update("ItgJobDAO.updateAssignedLicenceAmInsSwByDcaDataDif", null);
	}

	public void deleteUnassignedLicenceAmInsSwByDcaDataDif()throws NkiaException {
		delete("ItgJobDAO.deleteUnassignedLicenceAmInsSwByDcaDataDif", null);
	}

	public List selectSyncDcaAmNetworkList()throws NkiaException {
		return list("ItgJobDAO.selectSyncDcaAmNetworkList", null);
	}

	public void updateAmNwFromDcaData(HashMap syncDcaAmNetworkData)throws NkiaException {
		update("ItgJobDAO.updateAmNwFromDcaData", syncDcaAmNetworkData);
	}

	public void updateAmNsFromDcaData(HashMap syncDcaAmNetworkData)throws NkiaException {
		update("ItgJobDAO.updateAmNsFromDcaData", syncDcaAmNetworkData);
	}
	
	public void insertAmInsNwModuleFromDcaData(HashMap syncDcaAmNetworkData)throws NkiaException {
		insert("ItgJobDAO.insertAmInsNwModuleFromDcaData", syncDcaAmNetworkData);
	}
	
	public void insertAmInsNwNifFromDcaData(HashMap syncDcaAmNetworkData)throws NkiaException {
		insert("ItgJobDAO.insertAmInsNwNifFromDcaData", syncDcaAmNetworkData);
	}

	public void createAmAdDifData()throws NkiaException {
		insert("ItgJobDAO.createAmAdDifData", null);
	}

	public List selectAmServerLogicVmList()throws NkiaException {
		return list("ItgJobDAO.selectAmServerLogicVmList", null);
	}

	public List selectAmServerPhysicalBySerialNo(String serialNo)throws NkiaException {
		return list("ItgJobDAO.selectAmServerPhysicalBySerialNo", serialNo);
	}

	public void mergeIfAdPartitionByDca(HashMap dcaPartitionData)throws NkiaException {
		update("ItgJobDAO.mergeIfAdPartitionByDca", dcaPartitionData);
	}
	
	public void mergeIfAdVmByDca(HashMap dcaVmData)throws NkiaException {
		update("ItgJobDAO.mergeIfAdVmByDca", dcaVmData);
	}

	public void mergeIfAdDeviceByAm(HashMap wsResultData)throws NkiaException {
		update("ItgJobDAO.mergeIfAdDeviceByAm", wsResultData);
	}
	
	/**
	 * 
	 * VM등록시에 자산의 가상화장비 정보를 VM HOST_IP 기준으로 가져오기
	 * 
	 * @param hostIp
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerPhysiByVmHostIp(String vmHostIp)throws NkiaException {
		return list("ItgJobDAO.selectAmServerPhysiByVmHostIp", vmHostIp);
	}
	
	/**
	 * 
	 * VM등록시에 자산의 가상화장비 정보를 VM SERIAL_NO 기준으로 가져오기
	 * 
	 * @param serialNo
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerPhysiByVmHostSerial(String serialNo)throws NkiaException {
		return list("ItgJobDAO.selectAmServerPhysiByVmHostSerial", serialNo);
	}
	
	/**
	 * 
	 * VM ID로 등록된 논리서버정보를 가져온다.
	 * 
	 * @param vmId
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAmServerLogicByVmId(String vmId)throws NkiaException {
		return (HashMap)selectByPk("ItgJobDAO.selectAmServerLogicByVmId", vmId);
	}

	public void updateAmSvLogicFromDcaDataOnDaily() throws NkiaException {
		update("ItgJobDAO.updateAmSvLogicFromDcaDataOnDaily", null);
	}

	public void truncateAmInsDisk() throws NkiaException {
		delete("ItgJobDAO.truncateAmInsDisk", null);
	}

	public void insertAmInsDiskFromDcaDataOnDaily() throws NkiaException {
		insert("ItgJobDAO.insertAmInsDiskFromDcaDataOnDaily", null);
	}

	public void truncateAmInsNif() throws NkiaException {
		delete("ItgJobDAO.truncateAmInsNif", null);
	}

	public void insertAmInsNifFromDcaDataOnDaily() throws NkiaException {
		insert("ItgJobDAO.insertAmInsNifFromDcaDataOnDaily", null);
	}

	public void truncateAmInsHba() throws NkiaException {
		delete("ItgJobDAO.truncateAmInsHba", null);
	}

	public void insertAmInsHbaFromDcaDataOnDaily() throws NkiaException {
		insert("ItgJobDAO.insertAmInsHbaFromDcaDataOnDaily", null);
	}

	public void updateAmNwFromDcaDataOnDaily() throws NkiaException {
		update("ItgJobDAO.updateAmNwFromDcaDataOnDaily", null);
	}

	public void updateAmNsFromDcaDataOnDaily() throws NkiaException {
		update("ItgJobDAO.updateAmNsFromDcaDataOnDaily", null);
	}

	public void truncateAmInsNwModule() throws NkiaException {
		delete("ItgJobDAO.truncateAmInsNwModule", null);
	}

	public void insertAmInsNwModuleFromDcaDataOnDaily() throws NkiaException {
		insert("ItgJobDAO.insertAmInsNwModuleFromDcaDataOnDaily", null);
	}

	public void truncateAmInsNwNif() throws NkiaException {
		delete("ItgJobDAO.truncateAmInsNwNif", null);
	}

	public void insertAmInsNwNifFromDcaDataOnDaily() throws NkiaException {
		insert("ItgJobDAO.insertAmInsNwNifFromDcaDataOnDaily", null);
	}

	public int insertAmSwGroupByNewAmSw()throws NkiaException {
		return (Integer)update("ItgJobDAO.insertAmSwGroupByNewAmSw", null);
	}

	public HashMap selectAmSystemAdminInfo()throws NkiaException {
		return (HashMap)selectByPk("ItgJobDAO.selectAmSystemAdminInfo", null);
	}

	public HashMap selectAmSwManagerInfo() throws NkiaException {
		return (HashMap)selectByPk("ItgJobDAO.selectAmSwManagerInfo", null);
	}

	public List selectNewSwGroupList() throws NkiaException {
		return list("ItgJobDAO.selectNewSwGroupList", null);
	}

	public List selectCheckAmStorageList() throws NkiaException {
		return list("ItgJobDAO.selectCheckAmStorageList", null);
	}
	/**
	 * 
	 * SMSAgentKey 등록을 위해 DeviceID로 조회
	 * 
	 * @param deviceId
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmServerPhysiByDcaId(String deviceId)throws NkiaException {
		return list("ItgJobDAO.selectAmServerPhysiByDcaId", deviceId);
	}	

	public void updateSmsAgentKey(HashMap serverInfo)throws NkiaException {
		insert("ItgJobDAO.updateSmsAgentKey", serverInfo);
	}
	
	public void createAmAdEmsData()throws NkiaException {
		insert("ItgJobDAO.createAmAdEmsData", null);
	}
	
	/**
	 * 
	 * HBA 수량 관련
	 * 
	 * @param dcaSwObjectData
	 * @return
	 */
	
	public void truncateIfDcaServerHbaCnt() throws NkiaException {
		delete("ItgJobDAO.truncateIfDcaServerHbaCnt", null);
	}
	
	public void insertIfDcaServerHbaCnt(HashMap data)throws NkiaException {
		insert("ItgJobDAO.insertIfDcaServerHbaCnt", data);
	}
	
	/**
	 * 설치된 소프트웨어 그룹 등록
	 * @throws NkiaException
	 */
	public void insertAmSwInstlPcGroup() throws NkiaException {
		insert("ItgJobDAO.insertAmSwInstlPcGroup", null);
	}
	public void insertAmSwInstlSvGroup() throws NkiaException {
		insert("ItgJobDAO.insertAmSwInstlSvGroup", null);
	}

	public void truncateAmSwInstlGroup() {
		delete("ItgJobDAO.truncateAmSwInstlGroup", null);
	}
	
	public void deleteSysLoginSession()throws NkiaException {
		delete("ItgJobDAO.deleteSysLoginSession", null);
	}

	public void deleteScheduleResult() {
		delete("ItgJobDAO.deleteScheduleResult", null);
	}
	
}
