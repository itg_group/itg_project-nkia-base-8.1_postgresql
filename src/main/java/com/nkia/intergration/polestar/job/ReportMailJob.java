package com.nkia.intergration.polestar.job;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.intergration.polestar.job.service.ItgSmsJobService;
import com.nkia.intergration.polestar.service.IntergrationPolestarSmsService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.system.scheduler.service.SchedulerService;

import egovframework.com.cmm.service.EgovProperties;

public class ReportMailJob {

public static final Logger logger = LoggerFactory.getLogger(DcaJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");;
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId); //202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * Mail 발송
	 */
	public void ReportMailSend(String[] jobArgs) throws NkiaException{
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
				
			//알림 메일
			Map mailInfoMap = new HashMap();
			mailInfoMap.put("KEY", "REPORT");
			mailInfoMap.put("TEMPLATE_ID", "REPORT_NOTI_MAIL");
			
			
			EmailSetData emailSender = new EmailSetData(mailInfoMap);
			emailSender.sendMail();
	
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }  
	
	/**
	 * EMS 수집정보 자동 동기화
	 * @param jobArgs
	 */
	public void syncEmsDataToAmServerDaily(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			itgJobService.updateSyncEmsDataToAmServer("DAILY");
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
	}
	
}
