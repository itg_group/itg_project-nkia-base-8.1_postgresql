package com.nkia.intergration.polestar.job.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("itgSmsJobDAO")
public class ItgSmsJobDAO extends EgovComAbstractDAO{

	public void truncateIfEmsServerOs() {
		delete("ItgSmsJobDAO.truncateIfEmsServerOs", null);
	}

	public void insertIfEmsServerOs(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsServerOs", dataMap);
	}

	public void truncateIfEmsServerCpus() {
		delete("ItgSmsJobDAO.truncateIfEmsServerCpus", null);
	}

	public void insertIfEmsServerCpus(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsServerCpus", dataMap);
	}

	public void truncateIfEmsServerDisks() {
		delete("ItgSmsJobDAO.truncateIfEmsServerDisks", null);
	}

	public void insertIfEmsServerDisks(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsServerDisks", dataMap);
	}
	
	public void truncateIfEmsServerDisk() {
		delete("ItgSmsJobDAO.truncateIfEmsServerDisk", null);
	}

	public void insertIfEmsServerDisk(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsServerDisk", dataMap);
	}

	public void truncateIfEmsServerMemory() {
		delete("ItgSmsJobDAO.truncateIfEmsServerMemory", null);
	}

	public void insertIfEmsServerMemory(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsServerMemory", dataMap);
	}

	public void truncateIfEmsServerNwInterface() {
		delete("ItgSmsJobDAO.truncateIfEmsServerNwInterface", null);
	}

	public void insertIfEmsServerNwInterface(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsServerNwInterface", dataMap);
	}

	public List selectEmsServerList() {
		return list("ItgSmsJobDAO.selectEmsServerList", null);
	}

	public void syncEmsInfoAmSvInfra(HashMap dataMap) throws NkiaException{
		update("ItgSmsJobDAO.syncEmsInfoAmSvInfra", dataMap);
	}

	public void syncEmsInfoAmSvPhysi(HashMap dataMap) {
		update("ItgSmsJobDAO.syncEmsInfoAmSvPhysi", dataMap);
	}

	public void syncSmsidByHostNm(HashMap dataMap) {
		update("ItgSmsJobDAO.syncSmsidByHostNm", dataMap);
	}

	public void updateAdSyncDt(HashMap dataMap) {
		update("ItgSmsJobDAO.updateAdSyncDt", dataMap);
	}

	public void syncEmsInfoAmSvPhysiCore(HashMap dataMap) {
		update("ItgSmsJobDAO.syncEmsInfoAmSvPhysiCore", dataMap);
	}
	
	public void truncateIfEmsDBMariaDB() {
		delete("ItgSmsJobDAO.truncateIfEmsDBMariaDB", null);
	}

	public void insertIfEmsDBMariaDB(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsDBMariaDB", dataMap);
	}
	
	public void truncateIfEmsDBMssql() {
		delete("ItgSmsJobDAO.truncateIfEmsDBMssql", null);
	}

	public void insertIfEmsDBMssql(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsDBMssql", dataMap);
	}

	public void truncateIfEmsDBMysql() {
		delete("ItgSmsJobDAO.truncateIfEmsDBMysql", null);
	}

	public void insertIfEmsDBMysql(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsDBMysql", dataMap);
	}
	
	public void truncateIfEmsDBOracle() {
		delete("ItgSmsJobDAO.truncateIfEmsDBOracle", null);
	}

	public void insertIfEmsDBOracle(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsDBOracle", dataMap);
	}
	
	public void truncateIfEmsDBPostgreSQL() {
		delete("ItgSmsJobDAO.truncateIfEmsDBPostgreSQL", null);
	}

	public void insertIfEmsDBPostgreSQL(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsDBPostgreSQL", dataMap);
	}
	
	public void truncateIfEmsDBTibero() {
		delete("ItgSmsJobDAO.truncateIfEmsDBTibero", null);
	}

	public void insertIfEmsDBTibero(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsDBTibero", dataMap);
	}
	
	public void truncateIfEmsWasInstance() {
		delete("ItgSmsJobDAO.truncateIfEmsWasInstance", null);
	}
	
	public void insertIfEmsWasInstance(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsWasInstance", dataMap);
	}
	
	public void truncateIfEmsNwInfo() {
		delete("ItgSmsJobDAO.truncateIfEmsNwInfo", null);
	}

	public void insertIfEmsNwInfo(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsNwInfo", dataMap);
	}
	
	public List selectEmsNetworkList() {
		return list("ItgSmsJobDAO.selectEmsNetworkList", null);
	}
	
	public void syncEmsInfoAmNwInfra(HashMap dataMap) throws NkiaException{
		update("ItgSmsJobDAO.syncEmsInfoAmNwInfra", dataMap);
	}
	
	public void truncateIfEmsTree() {
		delete("ItgSmsJobDAO.truncateIfEmsTree", null);
	}

	public void insertIfEmsTree(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfEmsTree", dataMap);
	}
	
	public void truncateIfVmsEmsMap() {
		delete("ItgSmsJobDAO.truncateIfVmsEmsMap", null);
	}

	public void insertIfVmsEmsMap(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfVmsEmsMap", dataMap);
	}
	
	public void truncateIfVmsCluster() {
		delete("ItgSmsJobDAO.truncateIfVmsCluster", null);
	}

	public void insertIfVmsCluster(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfVmsCluster", dataMap);
	}
	
	public void truncateIfVmsPhysical() {
		delete("ItgSmsJobDAO.truncateIfVmsPhysical", null);
	}

	public void insertIfVmsPhysical(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfVmsPhysical", dataMap);
	}
	
	public void truncateIfVmsLogical() {
		delete("ItgSmsJobDAO.truncateIfVmsLogical", null);
	}

	public void insertIfVmsLogical(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfVmsLogical", dataMap);
	}
	
	public void truncateIfVms() {
		delete("ItgSmsJobDAO.truncateIfVms", null);
	}

	public void insertIfVms(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfVms", dataMap);
	}
	
	public void truncateIfWebURL() {
		delete("ItgSmsJobDAO.truncateIfWebURL", null);
	}

	public void insertIfWebURL(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfWebURL", dataMap);
	}
	
	public void callSyncVmsCluster(Map map) throws NkiaException {
		this.insert("ItgSmsJobDAO.callSyncVmsCluster", map);
	}
	
	// 20200323_이원재_001_신규작성
	public void deleteEmsMessage() throws NkiaException {
		this.delete("ItgSmsJobDAO.deleteEmsMessage", null);
	}
	
	public void insertEmsMessage(HashMap dataMap) throws NkiaException {
		insert("ItgSmsJobDAO.insertEmsMessage", dataMap);
	}
	// 20200323_이원재_001_신규작성 END
	// 20200326_이원재_001_신규작성
	public List selectEmsMessageList(Map paramMap) throws NkiaException {
		return this.list("ItgSmsJobDAO.selectEmsMessageList", paramMap);
	}
	public int selectEmsMessageCount(Map paramMap) throws NkiaException {
		return (int) this.selectByPk("ItgSmsJobDAO.selectEmsMessageCount", paramMap);
	}
	// 20200326_이원재_001_신규작성 end
	
	public void truncateIfReportGroup() {
		delete("ItgSmsJobDAO.truncateIfReportGroup", null);
	}

	public void insertIfReportGroup(HashMap dataMap) {
		insert("ItgSmsJobDAO.insertIfReportGroup", dataMap);
	}
}
