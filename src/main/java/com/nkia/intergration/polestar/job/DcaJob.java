/*
 * @(#)DcaJob.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;

import com.nkia.intergration.polestar.enums.IntergrationPolestarEnums;
import com.nkia.intergration.polestar.job.service.ItgJobService;
import com.nkia.intergration.polestar.service.IntergrationPolestarService;
import com.nkia.intergration.polestar.service.SystemIntegrationPolestarService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.logic.service.DcaMappingLogicService;
import com.nkia.itg.itam.logic.service.DcaMappingPcService;
import com.nkia.itg.itam.logic.service.LogicManagerService;
import com.nkia.itg.system.scheduler.service.SchedulerService;
 
public class DcaJob implements Job {
	
	public static final Logger logger = LoggerFactory.getLogger(DcaJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");;
	
	private static final String msgAdToAmSyncSuccess = "Agent 등록완료";
	private static final String msgCodeAdToAmSyncSuccess = "COMPLETE";
	
	private static final String msgAgentSyncFail = "Agent 동기화 실패";
	private static final String msgCodeAgentSyncFail = "AD_SYNC_FAIL";
	
	private static final String msgDuplicationAmSerial = "ITAM에 Serial 번호가 중복됩니다.";
	private static final String msgCodeDuplicationAmSerial = "AM_DUPL_SERIAL";
	
	private static final String msgNotAmSerial = "ITAM에 일치하는 Serial 번호가 없습니다.";
	private static final String msgCodeNotAmSerial = "AM_NOT_SERIAL";
	
	private static final String msgNotAdSerial = "AD에 Serial 번호가 없습니다.";
	private static final String msgCodeNotAdSerial = "AD_NO_SERIAL";
	
	private static final String msgDuplicationAmHostSerial = "ITAM에 HOST Serial 번호가 중복됩니다.";
	private static final String msgCodeDuplicationAmHostSerial = "AM_DUPL_HOST_SERIAL";
	
	private static final String msgDuplicationAmHostIp = "ITAM에 HOST IP가 중복됩니다.";
	private static final String msgCodeDuplicationAmHostIp = "AM_DUPL_HOST_IP";
	
	private static final String msgNotAmHostSerial = "ITAM에 일치하는 HOST Serial 번호가 없습니다.";
	private static final String msgCodeNotAmHostSerial = "AM_NOT_HOST_SERIAL";
	
	private static final String msgNotAmHostIp = "ITAM에 일치하는 HOST IP가 없습니다.";
	private static final String msgCodeNotAmHostIp = "AM_NOT_HOST_IP";
	
	private static final String msgNotAgentVm = "설치된 Agent정보가 없습니다.";
	private static final String msgCodeNotAgentVm = "AD_NOT_INSTALL";
	
	private static final String msgRemoveVm = "VM이 삭제되었습니다.";
	private static final String msgCodeRemoveVm = "VM_REMOVE";
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId);
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && ((schedulerJobMap.isEmpty() == false))) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	 /**
	 * Job Handler - DB 방식인 경우 실행시간에 맞춰 execute 메소드가 호출 된다.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		// Job Args 값 설정
                String scheduleId = jobDataMap.getString("scheduleId");
                String scheduleNm = jobDataMap.getString("scheduleNm");
                String executeMethod = jobDataMap.getString("executeMethod");
               String scheduleConfig = jobDataMap.getString("scheduleConfig");
		String executor = jobDataMap.getString("executor"); 
		String[] jobArgs = {scheduleId, scheduleNm, scheduleConfig, executor};
		
		// 실제 실행될 잡에 대한 분기 처리
		if("gatherDcaSoftwareData".equalsIgnoreCase(executeMethod)){
			gatherDcaSoftwareData(jobArgs);
		} else if("syncDcaDataToAmServerInstalledSw".equalsIgnoreCase(executeMethod)){
			syncDcaDataToAmServerInstalledSw(jobArgs);
		}
	}
	
	/**
	 * DCA 서버 데이터 수집
	 */ 
	public void gatherDcaServerData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			
			// DCA 서버정보 목록
			List<HashMap> dcaServerOaList = intergrationPolestarService.searchDcaServerOaList();
			if( dcaServerOaList != null && dcaServerOaList.size() > 0 ){
				itgJobService.insertIfDcaServerOaList(dcaServerOaList);
			}
			 
			// DCA 서버 CPUS 정보 목록
			List<HashMap> dcaServerCpusList = intergrationPolestarService.searchDcaServerCpusList();
			if( dcaServerCpusList != null && dcaServerCpusList.size() > 0 ){
				itgJobService.insertIfDcaServerCpusList(dcaServerCpusList);
			}
			
			// DCA 서버 Disks 정보 목록
			List<HashMap> dcaServerDisksList = intergrationPolestarService.searchDcaServerDisksList();
			if( dcaServerDisksList != null && dcaServerDisksList.size() > 0 ){
				itgJobService.insertIfDcaServerDisksList(dcaServerDisksList);
			}
			
			// DCA 서버 Disk 정보 목록
			List<HashMap> dcaServerDiskList = intergrationPolestarService.searchDcaServerDiskList();
			if( dcaServerDiskList != null && dcaServerDiskList.size() > 0 ){
				itgJobService.insertIfDcaServerDiskList(dcaServerDiskList);
			}
			
			// DCA 서버 Memory 정보 목록
			List<HashMap> dcaServerMemoryList = intergrationPolestarService.searchDcaServerMemoryList();
			if( dcaServerMemoryList != null && dcaServerMemoryList.size() > 0 ){
				itgJobService.insertIfDcaServerMemoryList(dcaServerMemoryList);
			}
			
			// DCA 서버 네트워크 Interfaces 목록
			List<HashMap> dcaServerNwInterfacesList = intergrationPolestarService.searchDcaServerNwInterfacesList();
			if( dcaServerNwInterfacesList != null && dcaServerNwInterfacesList.size() > 0 ){
				itgJobService.insertIfDcaServerNwInterfacesList(dcaServerNwInterfacesList);
			}
			
			// DCA 서버 네트워크 Interface 목록
			List<HashMap> dcaServerNwInterfaceList = intergrationPolestarService.searchDcaServerNwInterfaceList();
			if( dcaServerNwInterfaceList != null && dcaServerNwInterfaceList.size() > 0 ){
				itgJobService.insertIfDcaServerNwInterfaceList(dcaServerNwInterfaceList);
			}
			
			// DCA 서버 HBA Interface 목록
			List<HashMap> dcaServerHbaList = intergrationPolestarService.searchDcaServerHbaList();
			if( dcaServerHbaList != null && dcaServerHbaList.size() > 0 ){
				itgJobService.insertIfDcaServerHbaList(dcaServerHbaList);
			}
			
			// DCA 서버 HBA 수량 목록
			List<HashMap> dcaServerHbaCntList = intergrationPolestarService.searchDcaInventoryHbaCntList();
			if( dcaServerHbaCntList != null && dcaServerHbaCntList.size() > 0 ){
				itgJobService.insertIfDcaServerHbaCntList(dcaServerHbaCntList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }  

	
	/**
	 * DCA 네트워크 데이터 수집
	 */
	public void gatherDcaNetworkData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			
			// DCA 네트워크정보 목록
			List<HashMap> dcaNwSystemInfoList = intergrationPolestarService.searchDcaNwSystemInfoList();
			if( dcaNwSystemInfoList != null && dcaNwSystemInfoList.size() > 0 ){
				itgJobService.insertIfDcaNwSystemInfoList(dcaNwSystemInfoList);
			}
			
			// DCA 모듈정보 목록
			List<HashMap> dcaNwNetworkModuleList = intergrationPolestarService.searchDcaNwNetworkModuleList();
			if( dcaNwNetworkModuleList != null && dcaNwNetworkModuleList.size() > 0 ){
				itgJobService.insertIfDcaNwNetworkModuleList(dcaNwNetworkModuleList);
			}
			
			// DCA 네트워크 인터페이스 정보 목록
			List<HashMap> dcaNwNetworkInterfaceList = intergrationPolestarService.searchDcaNwNetworkInterfaceList();
			if( dcaNwNetworkInterfaceList != null && dcaNwNetworkInterfaceList.size() > 0 ){
				itgJobService.insertIfDcaNwInterfaceList(dcaNwNetworkInterfaceList);
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }  

	
	/**
	 * DCA 소프트웨어 데이터 수집
	 */
	public void gatherDcaSoftwareData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			
			// DCA 수집소프트웨어 목록
			List<HashMap> dcaInstalledSwList = intergrationPolestarService.searchDcaInstalledSwList();
			if( dcaInstalledSwList != null && dcaInstalledSwList.size() > 0 ){
				itgJobService.insertIfDcaInstalledSwList(dcaInstalledSwList);
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    } 

	
	/**
	 * 
	 * DCA TO AM 서버 데이터 Sync(최초 데이터 한번만 등록해준다.)
	 *
	 */
	public void syncDcaDataToAmServerAtOnce(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.updateSyncDcaDataToAmServer("ONCE");
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}

	
	/**
	 * 
	 * DCA TO AM 서버 데이터 Sync(자동 동기화)
	 *
	 */
	public void syncDcaDataToAmServerDaily(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.updateSyncDcaDataToAmServer("DAILY");
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}

	
	/**
	 * 
	 * DCA TO AM 네트워크 데이터 Sync(최초 데이터 한번만 등록해준다.)
	 *
	 */
	public void syncDcaDataToAmNetworkAtOnce(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.updateSyncDcaDataToAmNetwork("ONCE");
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}

	
	/**
	 * 
	 * DCA TO AM 네트워크 데이터 Sync 자동 동기화
	 * 
	 * @param jobArgs
	 */
	public void syncDcaDataToAmNetworkDaily(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.updateSyncDcaDataToAmNetwork("DAILY");
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}

	
	/**
	 * 
	 * DCA TO AM 설치 소프트웨어 데이터 등록
	 *
	 */
	public void syncDcaDataToAmServerInstalledSw(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.mergeSyncDcaDataToAmServerInstalledSw();
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}

	
	/**
	 * DCA 소프트웨어 마스터 오브젝트 데이터 수집
	 */
	public void gatherDcaSwMasterObjectData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			
			// DCA 소프트웨어 수집 마스터 목록
			List<HashMap> dcaSwObjectList = intergrationPolestarService.searchDcaSwObjectList();
			if( dcaSwObjectList != null && dcaSwObjectList.size() > 0 ){
				itgJobService.insertIfDcaSwMasterObjectList(dcaSwObjectList);
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    } 

	
	/**
	 * ITAM 물리서버 자산을 DCA에 등록 : DCA 미등록 자산검색(서버자산이고 IP정보가 있는 것)에 대하여 DCA에 서버 등록요청
	 */
	public void registDcaServerByAmServerPhysi(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			SystemIntegrationPolestarService polestarService = (SystemIntegrationPolestarService) NkiaApplicationContext.getCtx().getBean("polestarService");
			
			List unregistDcaByAmServerPhysiList = itgJobService.selectUnregistDcaByAmServerPhysi();
			if( unregistDcaByAmServerPhysiList != null && unregistDcaByAmServerPhysiList.size() > 0 ){
				// Agent 설치여부를 알기위해서 DCA에 서버요청을 먼저한다.
				ModelMap requestDcaDataMap = parserDcaRequestData("SERVER", "REGIST", "Y", unregistDcaByAmServerPhysiList);
				HashMap wsDcaResult = polestarService.sendDcaRequest(requestDcaDataMap);
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }

	
	/**
	 * ITAM 논리서버(파티션서버) 자산을 DCA에 등록 : DCA 미등록 자산검색(서버자산이고 IP정보가 있는 것)에 대하여 DCA에 서버 등록요청
	 */
	public void registDcaServerByAmServerLogicPartition(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			SystemIntegrationPolestarService polestarService = (SystemIntegrationPolestarService) NkiaApplicationContext.getCtx().getBean("systemIntegrationPolestarService");
			
			List unregistDcaByAmServerLogicPartitionList = itgJobService.selectUnregistDcaByAmServerLogicPartition();
			if( unregistDcaByAmServerLogicPartitionList != null && unregistDcaByAmServerLogicPartitionList.size() > 0 ){
				// Agent 설치여부를 알기위해서 DCA에 서버요청을 먼저한다.
				ModelMap requestDcaDataMap = parserDcaRequestData("SERVER", "REGIST", "N", unregistDcaByAmServerLogicPartitionList);
				HashMap wsDcaResult = polestarService.sendDcaRequest(requestDcaDataMap);
				List wsResultList = (List)wsDcaResult.get("wsResultList");
				if( wsResultList != null && wsResultList.size() > 0 ){
					for( int i = 0; i < wsResultList.size(); i++ ){
						HashMap wsResultData = (HashMap)wsResultList.get(i);
						polestarService.mergeIfAdDeviceStatus(wsResultData, "AM_AD", "PHYSICAL_SERVER");
					}
				}
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }


	/**
	 * ITAM 네트워크 자산을 DCA에 등록 : DCA 미등록 자산검색(네트워크자산이고 IP, COMMUNITY, Version)에 대하여 DCA에 네트워크 등록요청
	 */
	public void registDcaServerByAmNetwork(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
				schedulerJobMap = initSchedulerJob(jobArgs);
				
				ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
				SystemIntegrationPolestarService polestarService = (SystemIntegrationPolestarService) NkiaApplicationContext.getCtx().getBean("systemIntegrationPolestarService");
				
				List unregistDcaByAmNetwork = itgJobService.selectUnregistDcaByAmNetwork();
				if( unregistDcaByAmNetwork != null && unregistDcaByAmNetwork.size() > 0 ){
					// Agent 설치여부를 알기위해서 DCA에 서버요청을 먼저한다.
					ModelMap requestDcaDataMap = parserDcaRequestData("NETWORK", "REGIST", "Y", unregistDcaByAmNetwork);
					HashMap wsDcaResult = polestarService.sendDcaRequest(requestDcaDataMap);
					List wsResultList = (List)wsDcaResult.get("wsResultList");
					if( wsResultList != null && wsResultList.size() > 0 ){
						for( int i = 0; i < wsResultList.size(); i++ ){
							HashMap wsResultData = (HashMap)wsResultList.get(i);
							polestarService.mergeIfAdDeviceStatus(wsResultData, "AM_AD", "NETWORK");
						}
					}
				}
				isSuccess = true;
				finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }

	
	/**
	 * ITAM 스토리지 점검 SNMP방식
	 */
	public void checkAmStorageBySnmp(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
				schedulerJobMap = initSchedulerJob(jobArgs);
				
				ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
				SystemIntegrationPolestarService polestarService = (SystemIntegrationPolestarService) NkiaApplicationContext.getCtx().getBean("systemIntegrationPolestarService");
				
				List checkAmStorageList = itgJobService.selectCheckAmStorageList();
				if( checkAmStorageList != null && checkAmStorageList.size() > 0 ){
					// Agent 설치여부를 알기위해서 DCA에 서버요청을 먼저한다.
					ModelMap requestDcaDataMap = parserDcaRequestData("SNMP", "CHECK", "Y", checkAmStorageList);
					HashMap wsDcaResult = polestarService.sendDcaRequest(requestDcaDataMap);
					List wsResultList = (List)wsDcaResult.get("wsResultList");
					if( wsResultList != null && wsResultList.size() > 0 ){
						for( int i = 0; i < wsResultList.size(); i++ ){
							HashMap wsResultData = (HashMap)wsResultList.get(i);
							polestarService.updateIfAdDeviceStatusCheckDeviceMsgByConfId(wsResultData);
						}
					}
				}
				isSuccess = true;
				finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }


	/**
	 * 
	 * DCA VM 동기화
	 * DCA VM 목록을 가져와서 AM서버자산을 HOST_IP 또는 Serial No를 비교하여 존재하면 AM에 가상화로 등록하고  DCA등록요청 .
	 *
	 */
	public void syncDcaVmToAmServer(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			SystemIntegrationPolestarService polestarService = (SystemIntegrationPolestarService) NkiaApplicationContext.getCtx().getBean("systemIntegrationPolestarService");
			LogicManagerService logicManagerService = (LogicManagerService) NkiaApplicationContext.getCtx().getBean("logicManagerService");
			
			// DCA VM 목록
			List dcaVmList = intergrationPolestarService.searchDcaVmList();
			if( dcaVmList != null && dcaVmList.size() > 0 ){
				for( int i = 0; i < dcaVmList.size(); i++ ){
					HashMap dcaVmData = (HashMap)dcaVmList.get(i);	// DCA VM 데이터
					String vmId = ((BigDecimal)dcaVmData.get("VM_ID")).toString();			// VM ID
					String vmHostIp = (String)dcaVmData.get("VM_HOST_IP");					// VM HOST_IP
					String vmHostSerialNo = (String)dcaVmData.get("VM_HOST_SERIAL_NO");		// VM HOST SERIAL_NO
					
					dcaVmData.put("vm_id", vmId);
					dcaVmData.put("serial_no", vmHostSerialNo);
					String vmStatus = ((String)dcaVmData.get("STATUS")).toUpperCase();
					if( vmStatus.equalsIgnoreCase("UP") ){
						dcaVmData.put("ad_state", "ACTIVE");
					}else{
						dcaVmData.put("ad_state", "INACTIVE");
					}
					
					String adManagerIp = (String)dcaVmData.get("MANAGER_IP");
					if( adManagerIp != null && adManagerIp.equals(IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString()) ){
						dcaVmData.put("region", IntergrationPolestarEnums.DCA_FIRST_REGION_CODE.toString());
					}else{
						dcaVmData.put("region", IntergrationPolestarEnums.DCA_SECOND_REGION_CODE.toString());
					}
					
					dcaVmData.put("ad_manager_ip", adManagerIp);
					
					boolean isVmHostSerialSearch = false;
					// VM HOST IP로 IT자산정보 가져오기
					List amServerList = itgJobService.selectAmServerPhysiByVmHostIp(vmHostIp);
					if( vmHostSerialNo != null && vmHostSerialNo.length() > 0 && ( amServerList == null || amServerList.size() == 0 ) ){
						// VM HOST IP정보가 없을 경우 VM HOST SERIAL번호로 IT자산정보 가져오기
						isVmHostSerialSearch = true;
						amServerList = itgJobService.selectAmServerPhysiByVmHostSerial(vmHostSerialNo);
					}
					if( amServerList != null && amServerList.size() == 1 ){
						// 한건만 탐색될 경우 처리한다.( 중복 IT자산은 무시 )
						
						// 물리서버에 등록된 VM 정보를 가지고 온다.
						HashMap amServerLogicVmData = itgJobService.selectAmServerLogicByVmId(vmId);
						
						// VM 데이터 등록 또는 업데이트 하기 위하여 데이터를 Parser
						ModelMap amSvLogicDataMap = parserAmSvLogicVmData(amServerList, dcaVmData, amServerLogicVmData);
						
						boolean isDcaRequest = false;
						String vmConfId = null;
						
						if( amServerLogicVmData != null && amServerLogicVmData.size() > 0 ){
							// DCA에 등록되어 있으면 VM의 현행 상태를 업데이트 해준다.
							
							// 이미 등록된 VM 논리서버가 존재하면 상태를 업데이트 해준다.
							if( dcaVmData.get("DEVICE_ID") != null && ((String)dcaVmData.get("DEVICE_ID")).length() > 0 ){
								// DCA에 등록되어 있으면 VM의 현행 상태를 업데이트 해준다.
								logicManagerService.updateServerLogicVmStatus(amSvLogicDataMap);
							}else{
								// DCA 미등록 / VM 데이터를 업데이트 하고 DCA에 등록요청을 한다.
								logicManagerService.updateServerLogicInfoByVm(amSvLogicDataMap);
								vmConfId = (String)amServerLogicVmData.get("CONF_ID");
								isDcaRequest = true;
							}
						}else{
							// 등록된 적이 없는 VM인 경우에는 신규로 등록해준다.
							logicManagerService.insertServerLogicInfo(amSvLogicDataMap);
							
							Map basicInfo = (Map)amSvLogicDataMap.get("basicInfo");
							vmConfId = (String)basicInfo.get("conf_id");
							
							if( dcaVmData.get("AD_IP") != null && ((String)dcaVmData.get("AD_IP")).length() > 0 && !((String)dcaVmData.get("AD_IP")).equalsIgnoreCase("0.0.0.0") ){
								isDcaRequest = true;
							}
						}
						
						HashMap wsDcaResult = null;
						if( isDcaRequest ){
							// Agent 설치여부를 알기위해서 DCA에 서버요청을 먼저한다.
							ModelMap requestDcaDataMap = parserDcaRequestDataForLogical("SERVER", "REGIST", dcaVmData, vmConfId);
							wsDcaResult = polestarService.sendDcaRequest(requestDcaDataMap);
							
							// VM 등록 결과 처리
							dcaVmData.put("conf_id", vmConfId);
							dcaVmData.put("is_update", true);
							if( wsDcaResult != null && wsDcaResult.containsKey("successCount") ){
								if( (Integer)wsDcaResult.get("successCount") == 1 ){
									List wsResultList = (List)wsDcaResult.get("wsResultList");
									HashMap dataMap = (HashMap)wsResultList.get(0);
									dcaVmData.put("ad_id", dataMap.get("dca_id"));
									dcaVmData.put("message", msgAdToAmSyncSuccess);
									dcaVmData.put("message_code", msgCodeAdToAmSyncSuccess);
									dcaVmData.put("status", "success");
								}else{
									if( dcaVmData.get("DEVICE_ID") == null || ((String)dcaVmData.get("DEVICE_ID")).length() == 0 ){
										dcaVmData.put("is_update", true);
										dcaVmData.put("message", msgNotAgentVm);
										dcaVmData.put("message_code", msgCodeNotAgentVm);
									}
								}
							}else{
								if( dcaVmData.get("DEVICE_ID") == null || ((String)dcaVmData.get("DEVICE_ID")).length() == 0 ){
									dcaVmData.put("is_update", true);
									dcaVmData.put("message", msgNotAgentVm);
									dcaVmData.put("message_code", msgCodeNotAgentVm);
								}
							}
							polestarService.mergeIfAdDeviceStatus(dcaVmData, "AD_AM", "VM");
						}else{
							// 이미 등록되어 있는 VM은 상태변경만 해준다.
							polestarService.mergeIfAdDeviceStatus(dcaVmData, "AD_AM", "VM");
						}
					}else{
						dcaVmData.put("is_update", true);
						// IT자산이 없거나 중복처리된 자산처리
						if( amServerList != null && amServerList.size() > 1 ){
							if( isVmHostSerialSearch ){
								dcaVmData.put("message", msgDuplicationAmHostSerial);
								dcaVmData.put("message_code", msgCodeDuplicationAmHostSerial);
							}else{
								dcaVmData.put("message", msgDuplicationAmHostIp);
								dcaVmData.put("message_code", msgCodeDuplicationAmHostIp);
							}
						}else{
							if( isVmHostSerialSearch ){
								dcaVmData.put("message", msgNotAmHostSerial);
								dcaVmData.put("message_code", msgCodeNotAmHostSerial);
							}else{
								dcaVmData.put("message", msgNotAmHostIp);
								dcaVmData.put("message_code", msgCodeNotAmHostIp);
							}
						}
						polestarService.mergeIfAdDeviceStatus(dcaVmData, "AD_AM", "VM");
					}
				}
				// 반환되거나 삭제된 VM 처리
				List amServerLogicVmList = itgJobService.selectAmServerLogicVmList();
				if( amServerLogicVmList != null && amServerLogicVmList.size() > 0 ){
					for( int i = 0; i < amServerLogicVmList.size(); i++ ){
						HashMap amServerLogicVmData = (HashMap)amServerLogicVmList.get(i);
						String amConfId = (String)amServerLogicVmData.get("CONF_ID");
						String amVmId = (String)amServerLogicVmData.get("VM_ID");
						String amDcaId = (String)amServerLogicVmData.get("DCA_ID");
						
						boolean isExist = false;
						for( int j = 0; j < dcaVmList.size(); j++ ){
							HashMap dcaVmData = (HashMap)dcaVmList.get(j);
							String dcaVmId = ((BigDecimal)dcaVmData.get("VM_ID")).toString();
							if( amVmId.equals(dcaVmId) ){
								isExist = true;
								break;
							}
						}
						if( !isExist ){
							// AM에 존재하지만 DCA없으면 반환되거나 삭제된 VM임
							
							// VM 삭제시에 AM_SV_LOGIC에서는 삭제되고 AM_SV_LOGIC_HIS 테이블에 INSERT 해준다.
							logicManagerService.insertAmSvLogicHis(amServerLogicVmData);
							
							// AM_SV_LOGIC 삭제
							logicManagerService.deleteAmSvLogic(amConfId);
							
							amServerLogicVmData.put("vm_id", amVmId);
							amServerLogicVmData.put("conf_id", amConfId);
							amServerLogicVmData.put("dca_id", amDcaId);
							amServerLogicVmData.put("oper_state", "RETURN");
							amServerLogicVmData.put("upd_user_id", "IF_VM_SYSTEM");
							amServerLogicVmData.put("is_delete", true);
							amServerLogicVmData.put("ad_sync_yn", "D");
							amServerLogicVmData.put("ad_state", "RETURN");
							amServerLogicVmData.put("message", msgRemoveVm);
							amServerLogicVmData.put("message_code", msgCodeRemoveVm);
							polestarService.mergeIfAdDeviceStatus(amServerLogicVmData, "AD_AM", "VM");
						}
					}
				}
				
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }

	
	/**
	 * 
	 * AD에 등록된 Agent를 논리서버로 자동등록
	 * 2016.12.29 현재 사용 안함.
	 * registLogicServerSyncSerialNo 사용.
	 * 
	 * @param jobArgs
	 */
	public void registAmServerLogicPartitionByDcaServer(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			SystemIntegrationPolestarService polestarService = (SystemIntegrationPolestarService) NkiaApplicationContext.getCtx().getBean("systemIntegrationPolestarService");
			LogicManagerService logicManagerService = (LogicManagerService) NkiaApplicationContext.getCtx().getBean("logicManagerService");
			
			// DCA에 논리 파티션 서버중에서 AM에 미등록된 목록
			List unregistAmServerLogicPartitionByDcaServerList = intergrationPolestarService.selectUnregistAmServerLogicPartitionByDcaServer();
			if( unregistAmServerLogicPartitionByDcaServerList != null && unregistAmServerLogicPartitionByDcaServerList.size() > 0 ){
				for( int i = 0; i < unregistAmServerLogicPartitionByDcaServerList.size(); i++ ){
					// DCA에 논리 파티션 서버중에서 AM에 미등록된 데이터
					HashMap unregistAmServerLogicPartitionByDcaServerData = (HashMap)unregistAmServerLogicPartitionByDcaServerList.get(i);
					
					unregistAmServerLogicPartitionByDcaServerData.put("ad_id", ((BigDecimal)unregistAmServerLogicPartitionByDcaServerData.get("DEVICE_ID")).toString());
					String adManagerIp = (String)unregistAmServerLogicPartitionByDcaServerData.get("MANAGER_IP");
					if( adManagerIp != null && adManagerIp.equals(IntergrationPolestarEnums.DCA_SECOND_MANAGER_IP.toString()) ){
						unregistAmServerLogicPartitionByDcaServerData.put("region", IntergrationPolestarEnums.DCA_SECOND_REGION_CODE.toString());
					}else{
						unregistAmServerLogicPartitionByDcaServerData.put("region", IntergrationPolestarEnums.DCA_FIRST_REGION_CODE.toString());
					}
					unregistAmServerLogicPartitionByDcaServerData.put("ad_state", "ACTIVE");
					unregistAmServerLogicPartitionByDcaServerData.put("ad_sync_yn", "N");
					
					String serialNo = (String)unregistAmServerLogicPartitionByDcaServerData.get("SERIALNO");
					
					if( serialNo != null && serialNo.length() > 0 ){
						// 시리얼번호가 일치하는 물리서버정보(가상화장비 제외)
						List amServerPhysicalList = itgJobService.selectAmServerPhysicalBySerialNo(serialNo);
						if( amServerPhysicalList != null && amServerPhysicalList.size() == 1 ){
							HashMap amServerPhysicalData = (HashMap)amServerPhysicalList.get(0);
							
							// 일치하는 시리얼번호가 있음.
							ModelMap amSvLogicPartitionData = parserAmSvLogicPartitionData(unregistAmServerLogicPartitionByDcaServerData, amServerPhysicalData);
							
							// Am Server Logic 등록
							logicManagerService.insertServerLogicInfo(amSvLogicPartitionData);
							
							Map basicInfo = (Map)amSvLogicPartitionData.get("basicInfo");
							String partitionConfId = (String)basicInfo.get("conf_id");
							
							// Agent에 구성ID 등록
							ModelMap requestDcaDataMap = parserDcaRequestDataForLogical("SERVER", "REGIST", unregistAmServerLogicPartitionByDcaServerData, partitionConfId);
							HashMap wsDcaResult = polestarService.sendDcaRequest(requestDcaDataMap);
							boolean isAgentSyncFail = true;
							if( wsDcaResult != null && wsDcaResult.containsKey("successCount") ){
								unregistAmServerLogicPartitionByDcaServerData.put("conf_id", partitionConfId);
								List wsResultList = (List)wsDcaResult.get("wsResultList");
								HashMap wsResultData = (HashMap)wsResultList.get(0);
								if( (Integer)wsDcaResult.get("successCount") == 1 ){
									unregistAmServerLogicPartitionByDcaServerData.put("is_update", true);
									unregistAmServerLogicPartitionByDcaServerData.put("ad_sync_yn", "Y");
									unregistAmServerLogicPartitionByDcaServerData.put("message", msgAdToAmSyncSuccess);
									unregistAmServerLogicPartitionByDcaServerData.put("message_code", msgCodeAdToAmSyncSuccess);
									isAgentSyncFail = false;
								}else{
									unregistAmServerLogicPartitionByDcaServerData.put("is_update", true);
									unregistAmServerLogicPartitionByDcaServerData.put("ad_sync_yn", "N");
									unregistAmServerLogicPartitionByDcaServerData.put("message", wsResultData.get("msg"));
									unregistAmServerLogicPartitionByDcaServerData.put("message_code", msgCodeAgentSyncFail);
								}
								polestarService.mergeIfAdDeviceStatus(unregistAmServerLogicPartitionByDcaServerData, "AD_AM", "PHYSICAL_SERVER");
							}else{
								// Agent 등록 실패
								unregistAmServerLogicPartitionByDcaServerData.put("is_update", true);
								unregistAmServerLogicPartitionByDcaServerData.put("ad_sync_yn", "N");
								unregistAmServerLogicPartitionByDcaServerData.put("message", msgAgentSyncFail);
								unregistAmServerLogicPartitionByDcaServerData.put("message_code", msgCodeAgentSyncFail);
								polestarService.mergeIfAdDeviceStatus(unregistAmServerLogicPartitionByDcaServerData, "AD_AM", "PHYSICAL_SERVER");
							}
							
							if( isAgentSyncFail ){
								// Agent 등록이 실패될 경우 삭제해준다.
								logicManagerService.deleteAmSvLogic(partitionConfId);
							}
						}else{
							unregistAmServerLogicPartitionByDcaServerData.put("is_update", true);
							if( amServerPhysicalList != null && amServerPhysicalList.size() > 1 ){
								// 시리얼번호 중복
								unregistAmServerLogicPartitionByDcaServerData.put("message", msgDuplicationAmSerial);
								unregistAmServerLogicPartitionByDcaServerData.put("message_code", msgCodeDuplicationAmSerial);
							}else{
								// 일치하는 시리얼번호가 없음.
								unregistAmServerLogicPartitionByDcaServerData.put("message", msgNotAmSerial);
								unregistAmServerLogicPartitionByDcaServerData.put("message_code", msgCodeNotAmSerial);
							}
							polestarService.mergeIfAdDeviceStatus(unregistAmServerLogicPartitionByDcaServerData, "AD_AM", "PHYSICAL_SERVER");
						}
					}else{
						unregistAmServerLogicPartitionByDcaServerData.put("is_update", true);
						// AD에서 수집된 시리얼번호가 없음
						unregistAmServerLogicPartitionByDcaServerData.put("message", msgNotAdSerial);
						unregistAmServerLogicPartitionByDcaServerData.put("message_code", msgCodeNotAdSerial);
						polestarService.mergeIfAdDeviceStatus(unregistAmServerLogicPartitionByDcaServerData, "AD_AM", "PHYSICAL_SERVER");
					}
					
				}
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }  


	
	/**
	 * AD 자원 동기화 데이터 생성
	 */
	public void createAmAdDifData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			itgJobService.createAmAdDifData();
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }  

	
	/**
	 * DCA 등록 요청 데이터 생성 [ 서버 / 네트워크 ]
	 * @param requestType
	 * @param requestAction
	 * @param assetList
	 * @return
	 * @throws NkiaException
	 */
	public ModelMap parserDcaRequestData(String requestType, String requestAction, String tangibleAssetYn, List assetList)throws NkiaException{
		ModelMap paramMap = new ModelMap();
		paramMap.put("request_type", requestType);
		paramMap.put("request_action", requestAction);
		paramMap.put("request_tangible_asset_yn", tangibleAssetYn);
		paramMap.put("is_am_update_asset", false);
		
		
		List reqAssetList = new ArrayList();
		for( int i = 0; i < assetList.size(); i++ ){
			HashMap reqAssetData = new HashMap();
			HashMap assetData = (HashMap)assetList.get(i);
			reqAssetData.put("conf_id", (String)assetData.get("CONF_ID"));
			reqAssetData.put("ip", (String)assetData.get("MAIN_IP"));
			
			if( assetData.get("REGION") != null ){
				String region = (String)assetData.get("REGION");
				if ( region.length() > 0 ) {
					if ( region.equals(IntergrationPolestarEnums.DCA_SECOND_REGION_CODE.toString()) ) {
						reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_SECOND_MANAGER_IP.toString());
					} else {
						reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString());
					}
				} else {
					reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString());
				}
			} else {
				reqAssetData.put("target_manager_ip", IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString());
			}
			
			if( assetData.containsKey("COMMUNITY") ){
				reqAssetData.put("community", (String)assetData.get("COMMUNITY"));
				if( assetData.containsKey("SNMP_PORT") && ((String)assetData.get("SNMP_PORT")).length() > 0 ){
					reqAssetData.put("port", assetData.get("SNMP_PORT"));
				}
				if( assetData.containsKey("SNMP_VERSION") && ((String)assetData.get("SNMP_VERSION")).length() > 0 ){
					reqAssetData.put("version", assetData.get("SNMP_VERSION"));
				}
			}
			reqAssetList.add(reqAssetData);
		}
		paramMap.put("asset_list", reqAssetList);
		return paramMap;
	}

	
	/**
	 * DCA에 논리서버/가상화 등록 요청 데이터 생성
	 * @param requestType
	 * @param requestAction
	 * @param assetList
	 * @param dcaUnregistVmData
	 * @param vmConfId
	 * @return
	 * @throws NkiaException
	 */
	public ModelMap parserDcaRequestDataForLogical(String requestType, String requestAction, HashMap dcaUnregistServerData, String confId)throws NkiaException{
		ModelMap paramMap = new ModelMap();
		paramMap.put("request_type", requestType);
		paramMap.put("request_action", requestAction);
		paramMap.put("request_tangible_asset_yn", "N");
		paramMap.put("is_am_update_asset", false);
		
		List reqAssetList = new ArrayList();
		HashMap reqAssetData = new HashMap();
		reqAssetData.put("conf_id", confId);
		if( dcaUnregistServerData.containsKey("AD_IP") ){
			reqAssetData.put("ip", (String)dcaUnregistServerData.get("AD_IP"));
		}else{
			reqAssetData.put("ip", (String)dcaUnregistServerData.get("IP"));
		}
		if( dcaUnregistServerData.containsKey("MANAGER_IP") ){
			reqAssetData.put("target_manager_ip", (String)dcaUnregistServerData.get("MANAGER_IP"));
		}else{
			reqAssetData.put("target_manager_ip", (String)dcaUnregistServerData.get("target_manager_ip"));
		}
		
		reqAssetList.add(reqAssetData);
		
		paramMap.put("asset_list", reqAssetList);
		return paramMap;
	}
	
	/**
	 * AM 서버 Partition 데이터 생성
	 * @param assetList
	 * @param dcaUnregistVmData
	 * @return
	 * @throws NkiaException
	 */
	private ModelMap parserAmSvLogicPartitionData(HashMap unregistAmServerLogicPartitionByDcaServerData, HashMap amServerPhysical) {
		ModelMap paramMap = new ModelMap();
		paramMap.put("upd_user_id", "AD_SYSTEM");
		
		// 서버 Infra등록
		Map basicInfo = new HashMap();
		basicInfo.put("asset_id", (String)amServerPhysical.get("ASSET_ID")); 
		basicInfo.put("class_id", (String)amServerPhysical.get("CLASS_ID"));
		basicInfo.put("conf_nm", (String)unregistAmServerLogicPartitionByDcaServerData.get("HOSTNAME"));
		basicInfo.put("dca_id", (BigDecimal)unregistAmServerLogicPartitionByDcaServerData.get("DEVICE_ID"));
		if( ((String)amServerPhysical.get("ASSET_STATE")).equals("ACTIVE") ){
			basicInfo.put("oper_state", "ACTIVE");
		}else{
			basicInfo.put("oper_state", "INACTIVE");
		}
		
		paramMap.put("upd_user_id", "IF_PARTITON_SYSTEM");
		paramMap.put("basicInfo", basicInfo);
		
		// 논리정보 등록
		Map specInfo = new HashMap();
		specInfo.put("physi_conf_id", (String)amServerPhysical.get("CONF_ID"));
		specInfo.put("logic_type", "PARTITION");
		specInfo.put("main_ip", (String)unregistAmServerLogicPartitionByDcaServerData.get("IP"));
		specInfo.put("os_type", (String)unregistAmServerLogicPartitionByDcaServerData.get("OS_TYPE"));
		specInfo.put("os_nm", (String)unregistAmServerLogicPartitionByDcaServerData.get("OSNAME"));
		specInfo.put("os_version", (String)unregistAmServerLogicPartitionByDcaServerData.get("OSVERSION"));
		specInfo.put("os_patch_version", (String)unregistAmServerLogicPartitionByDcaServerData.get("PATCHLEVEL"));
		specInfo.put("upd_user_id", "IF_PARTITON_SYSTEM");
		paramMap.put("specInfo", specInfo);
		
		return paramMap;
	}
	
	/**
	 * AM 서버 VM 데이터 생성
	 * @param assetList
	 * @param dcaUnregistVmData
	 * @return
	 * @throws NkiaException
	 */
	public ModelMap parserAmSvLogicVmData(List assetList, HashMap dcaUnregistVmData, HashMap amServerLogicVmData)throws NkiaException{
		ModelMap paramMap = new ModelMap();
		paramMap.put("upd_user_id", "AD_SYSTEM");
		
		HashMap assetData = (HashMap)assetList.get(0);
		
		// 서버 Infra등록
		Map basicInfo = new HashMap();
		basicInfo.put("asset_id", (String)assetData.get("ASSET_ID")); 
		basicInfo.put("class_id", (String)assetData.get("CLASS_ID"));
		basicInfo.put("conf_nm", (String)dcaUnregistVmData.get("VM_NM"));
		if( amServerLogicVmData != null && amServerLogicVmData.get("CONF_ID") != null ){
			basicInfo.put("conf_id", (String)amServerLogicVmData.get("CONF_ID"));
		}
		
		if( assetData.get("REGION") != null ){
			String region = (String)assetData.get("REGION");
			if( region.length() > 0 ){
				if( region.equals(IntergrationPolestarEnums.DCA_SECOND_REGION_CODE.toString()) ){
					dcaUnregistVmData.put("target_manager_ip", IntergrationPolestarEnums.DCA_SECOND_MANAGER_IP.toString());
				}else{
					dcaUnregistVmData.put("target_manager_ip", IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString());
				}
			}else{
				dcaUnregistVmData.put("target_manager_ip", dcaUnregistVmData.get("ad_manager_ip") );
			}
		}else{
			dcaUnregistVmData.put("target_manager_ip", dcaUnregistVmData.get("ad_manager_ip") );
		}
		
		String vmStatus = ((String)dcaUnregistVmData.get("STATUS")).toUpperCase();
		if( vmStatus.equalsIgnoreCase("UP") ){
			basicInfo.put("oper_state", "ACTIVE");
		}else{
			basicInfo.put("oper_state", "INACTIVE");
		}
		basicInfo.put("upd_user_id", "AD_SYSTEM");
		paramMap.put("basicInfo", basicInfo);
		
		// 가상화정보 등록
		Map specInfo = new HashMap();
		specInfo.put("physi_conf_id", (String)assetData.get("CONF_ID"));
		if( amServerLogicVmData != null && amServerLogicVmData.get("CONF_ID") != null ){
			specInfo.put("conf_id", (String)amServerLogicVmData.get("CONF_ID"));
		}
		specInfo.put("logic_type", "VM");
		specInfo.put("main_ip", (String)dcaUnregistVmData.get("AD_IP"));
		specInfo.put("host_nm", (String)dcaUnregistVmData.get("HOST_NM"));
		specInfo.put("os_type", (String)dcaUnregistVmData.get("OS_TYPE"));
		specInfo.put("os_nm", (String)dcaUnregistVmData.get("OS"));
		specInfo.put("vm_id", (BigDecimal)dcaUnregistVmData.get("VM_ID"));
		specInfo.put("cpu_core_cnt", (BigDecimal)dcaUnregistVmData.get("VCORECNT"));
		specInfo.put("mem_tot_capa", (BigDecimal)dcaUnregistVmData.get("MEMTOTALSIZE"));
		specInfo.put("disk_tot_size", (BigDecimal)dcaUnregistVmData.get("DISKTOTALSIZE"));
		specInfo.put("upd_user_id", "AD_SYSTEM");
		paramMap.put("specInfo", specInfo);
		
		return paramMap;
	}
	
	/**
	 * SMS Agent key 매핑
	 * @param assetList
	 * @param dcaUnregistVmData
	 * @return
	 * @throws NkiaException
	 */
	public void syncSMSAgentKeytoAmServer(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			SystemIntegrationPolestarService polestarService = (SystemIntegrationPolestarService) NkiaApplicationContext.getCtx().getBean("systemIntegrationPolestarService");
			LogicManagerService logicManagerService = (LogicManagerService) NkiaApplicationContext.getCtx().getBean("logicManagerService");
			
			// DCA VM 목록
			List smsAgentList = intergrationPolestarService.selectSmsAgentList();
			if( smsAgentList != null && smsAgentList.size() > 0 ){
				for( int i = 0; i < smsAgentList.size(); i++ ){
					HashMap smsAgentData = (HashMap)smsAgentList.get(i);	
					String saDeviceId = ((BigDecimal)smsAgentData.get("DEVICE_ID")).toString();			
					String saHostName = (String)smsAgentData.get("HOSTNAME");					
					String saHostIP= (String)smsAgentData.get("IP_ADDRESS");		
					String saKey = (String)smsAgentData.get("SMSAGENT_KEY");
					
					
					boolean isVmHostSerialSearch = false;
					// VM HOST IP로 IT자산정보 가져오기
					List amServerList = itgJobService.selectAmServerPhysiByDcaId(saDeviceId);
					if( amServerList != null && amServerList.size() == 1 ){
						// 한건만 탐색될 경우 처리한다.( 중복 IT자산은 무시 )
						// 물리서버에 등록된 VM 정보를 가지고 온다.
						HashMap amServer = (HashMap)amServerList.get(0);
						amServer.put("SMSAGENT_KEY", saKey);
						itgJobService.updateSmsAgentKey(amServer);
					}
				}
			}
				
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }


	/**
	 * 시리얼이 일치하는 논리서버 등록
	 * 추가 일자 : 2016.08.09
	 * NH 기능 적용
	 * @param assetList
	 * @param dcaUnregistVmData
	 * @return
	 * @throws NkiaException
	 */
	public void registLogicServerSyncSerialNo(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			DcaMappingLogicService dcaMappingLogicService = (DcaMappingLogicService) NkiaApplicationContext.getCtx().getBean("dcaMappingLogicService");
			
			// DCA에서 수집된 논리서버 목록
			ModelMap paramMap = new ModelMap();
			List dcaLogicList = dcaMappingLogicService.selectDcaLogicList(paramMap);
			
			if( dcaLogicList != null && dcaLogicList.size() > 0 ){
				
				for(int i=0; i<dcaLogicList.size(); i++) {
					
					HashMap dataMap = new HashMap();
					dataMap = (HashMap)dcaLogicList.get(i);
					
					// 해당 시리얼 번호랑 같은 시리얼을 가진 물리서버 정보를 가져온다 (물리서버 + 보안서버)
					List physiInfo = dcaMappingLogicService.selectPhysiServerBySerialNo(dataMap);
					
					// 수집논리와 같은 시리얼번호를 가진 물리서버가 있으면
					if( physiInfo != null && physiInfo.size() == 1 ){
						
						HashMap physiMap = new HashMap();
						physiMap = (HashMap)physiInfo.get(0);
						
						// 해당물리의 정보를 각각 DCA 수집 논리서버 등록에 사용 될 dataMap 에 담아준 후
						dataMap.put("PHYSI_ASSET_ID", physiMap.get("ASSET_ID"));
						dataMap.put("PHYSI_CONF_ID", physiMap.get("CONF_ID"));
						dataMap.put("CLASS_ID", physiMap.get("CLASS_ID"));
						dataMap.put("UPD_USER_ID", "DCA-DB");
						dataMap.put("CONF_ID", dcaMappingLogicService.createConfIdByDcaLogic());
						dataMap.put("CLASS_TYPE", physiMap.get("CLASS_TYPE"));
						
						// DCA 수집 논리서버 정보를 AM_INFRA에 등록한다.
						dcaMappingLogicService.insertDcaLogicToAmInfra(dataMap);
						
						// DCA 수집 논리서버 정보를 AM_SV_LOGIC에 등록한다.
						dcaMappingLogicService.insertDcaLogicToAmSvLogic(dataMap);
					}
				}
				
				// 논리서버 정보 Insert 완료후, 논리서버 정보 동기화한다.
				itgJobService.updateSyncDcaDataToAmServer("ONCE");
			}
				
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }
	

	/**
	 * 시리얼이 일치하는 PC 매핑
	 * 추가 일자 : 2016.09.26
	 * 좌은진
	 * @param assetList
	 * @param dcaUnregistVmData
	 * @return
	 * @throws NkiaException
	 */
	public void registPcSyncSerialNo(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			DcaMappingPcService dcaMappingPcService = (DcaMappingPcService) NkiaApplicationContext.getCtx().getBean("dcaMappingPcService");
			
			// DCA에서 수집된 PC 목록
			ModelMap paramMap = new ModelMap();
			List dcaPcList = dcaMappingPcService.selectDcaPcList(paramMap);
			
			if( dcaPcList != null && dcaPcList.size() > 0 ){
				
				for(int i=0; i<dcaPcList.size(); i++) {
					
					HashMap dataMap = new HashMap();
					dataMap = (HashMap)dcaPcList.get(i);
					
					List pcAssetList = dcaMappingPcService.selectPcBySerialNo(dataMap);
					
					// 수집논리와 같은 시리얼번호를 가진 물리서버가 있으면
					if( pcAssetList != null && pcAssetList.size() == 1 ){
						
						HashMap pcMap = new HashMap();
						pcMap = (HashMap)pcAssetList.get(0);
						
						// 해당물리의 정보를 각각 DCA 수집 논리서버 등록에 사용 될 dataMap 에 담아준 후
						dataMap.put("ASSET_ID", pcMap.get("ASSET_ID"));
						dataMap.put("CONF_ID", pcMap.get("CONF_ID"));
						dcaMappingPcService.updatePcDcaId(dataMap);
					}
				}
			}
				
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
    }

}
