package com.nkia.intergration.polestar.job.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.icu.math.BigDecimal;
import com.nkia.intergration.polestar.job.dao.ItgSmsJobDAO;
import com.nkia.intergration.polestar.job.service.ItgSmsJobService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.amdb.dao.AmdbAttributeDAO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;

@Service("itgSmsJobService")
public class ItgSmsJobServiceImpl implements ItgSmsJobService{

	@Resource(name="itgSmsJobDAO")
	private ItgSmsJobDAO itgSmsJobDAO;
	
	@Resource(name = "amdbAttributeDao")
	public AmdbAttributeDAO amdbAttributeDao;
	
	@Resource(name="assetHistoryService")
	private AssetHistoryService assetHistoryService;

	@Override
	public void insertIfEmsServerOsList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfEmsServerOs();
		for(HashMap dataMap : paramList){
			itgSmsJobDAO.insertIfEmsServerOs(dataMap);
		}
	}

	@Override
	public void insertIfEmsServerCpusList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsServerCpus();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsServerCpus(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void insertIfEmsServerDisksList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsServerDisks();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsServerDisks(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	@Override
	public void insertIfEmsServerDiskList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsServerDisk();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsServerDisk(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void insertIfEmsServerMemoryList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsServerMemory();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsServerMemory(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void insertIfEmsServerNwInterfaceList(
			List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfEmsServerNwInterface();
		for(HashMap dataMap : paramList){
			itgSmsJobDAO.insertIfEmsServerNwInterface(dataMap);
		}
		
	}
	
	@Override
	public void insertIfEmsNwInfoList(
			List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfEmsNwInfo();
		for(HashMap dataMap : paramList){
			itgSmsJobDAO.insertIfEmsNwInfo(dataMap);
		}
		
	}

	/**
	 * EMS 수집정보 동기화
	 */
	@Override
	public void updateSyncEmsDataToAmServer(String syncPeriod) throws NkiaException {
		HashMap smsSyncMap = new HashMap<>();
		smsSyncMap.put("V_CLASS_TYPE", "SV");
		
		// SMS 미등록 자산 SMS_ID 매핑등록
		itgSmsJobDAO.syncSmsidByHostNm(smsSyncMap);
		
		// ems 수집정보 목록 조회
		List<HashMap> emsSvList = itgSmsJobDAO.selectEmsServerList();
		
		BigDecimal zero = new BigDecimal(0);
		BigDecimal emsVal = new BigDecimal(0);
		
		for(HashMap dataMap : emsSvList){
			String confId = (String) dataMap.get("CONF_ID");
			String classType = (String) dataMap.get("CLASS_TYPE");
			
			List<HashMap> emsCollectColList = amdbAttributeDao.selectEmsCollectColList(dataMap);
			
			for(HashMap emsColMap : emsCollectColList) {
				String emsColId = (String)emsColMap.get("COL_ID");
				
				// NULL은 업데이트 제외
				if(StringUtil.isNull(dataMap.get(emsColId))) {
					dataMap.remove(emsColId);
				} else {
					// 숫자 타입 항목 일 경우 0는 업데이트 제외
					if("CPU_CNT".equals(emsColId)
							|| "DISK_INTERNAL".equals(emsColId)
							|| "TOTAL_MEMORY".equals(emsColId)
							|| "CPU_SOCKET_CNT".equals(emsColId)) {
						emsVal = new BigDecimal(dataMap.get(emsColId).toString());
						
						if(zero.compareTo(emsVal) >= 0) {
							dataMap.remove(emsColId);
						
						}
					}
				}
			}
			
			// 이력 등록
			assetHistoryService.insertAssetAttrHistory(confId, dataMap, "EMS 정보 동기화", "SYSTEM");
			
			// 정보 동기화
			itgSmsJobDAO.syncEmsInfoAmSvInfra(dataMap);
			
			// 분류체계가 서버 일 경우
			if("SV".equals(classType)) {
				itgSmsJobDAO.syncEmsInfoAmSvPhysi(dataMap);
			}
			
			// Agent동기화일시 적용
			itgSmsJobDAO.updateAdSyncDt(dataMap);
			
		}
	}

	@Override
	public void insertIfEmsDBMariaDBList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsDBMariaDB();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsDBMariaDB(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfEmsDBMssqlList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsDBMssql();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsDBMssql(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfEmsDBMysqlList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsDBMysql();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsDBMysql(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void insertIfEmsDBOracleList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsDBOracle();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsDBOracle(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfEmsDBPostgreSQLList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsDBPostgreSQL();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsDBPostgreSQL(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfEmsDBTiberoList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsDBTibero();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsDBTibero(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void insertIfEmsWasInstanceList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsWasInstance();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsWasInstance(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * EMS NW 수집정보 동기화
	 */
	@Override
	public void updateSyncEmsDataToAmNetwork(String syncPeriod) throws NkiaException {
		HashMap smsSyncMap = new HashMap<>();
		smsSyncMap.put("V_CLASS_TYPE", "NW");
		
		// SMS 미등록 자산 SMS_ID 매핑등록
		itgSmsJobDAO.syncSmsidByHostNm(smsSyncMap);
		
		// ems 수집정보 목록 조회
		List<HashMap> emsNwList = itgSmsJobDAO.selectEmsNetworkList();
		
		BigDecimal zero = new BigDecimal(0);
		BigDecimal emsVal = new BigDecimal(0);
		
		for(HashMap dataMap : emsNwList){
			String confId = (String) dataMap.get("CONF_ID");
			
			List<HashMap> emsCollectColList = amdbAttributeDao.selectEmsCollectColList(dataMap);
			
			for(HashMap emsColMap : emsCollectColList) {
				String emsColId = (String)emsColMap.get("COL_ID");
				
				// NULL은 업데이트 제외
				if(StringUtil.isNull(dataMap.get(emsColId))) {
					dataMap.remove(emsColId);
				} else {
					// 숫자 타입 항목 일 경우 0는 업데이트 제외
					if("CPU_CNT".equals(emsColId)
							|| "DISK_INTERNAL".equals(emsColId)
							|| "TOTAL_MEMORY".equals(emsColId)
							|| "CPU_SOCKET_CNT".equals(emsColId)) {
						emsVal = new BigDecimal(dataMap.get(emsColId).toString());
						
						if(zero.compareTo(emsVal) >= 0) {
							dataMap.remove(emsColId);
						
						}
					}
				}
			}
			
			// 이력 등록
			assetHistoryService.insertAssetAttrHistory(confId, dataMap, "EMS 정보 동기화", "SYSTEM");
			
			// 정보 동기화
			itgSmsJobDAO.syncEmsInfoAmNwInfra(dataMap);
			
			// Agent동기화일시 적용
			itgSmsJobDAO.updateAdSyncDt(dataMap);
			
		}
	}
	
	@Override
	public void insertIfEmsTreeList(List<HashMap> paramList)
			throws NkiaException {
		itgSmsJobDAO.truncateIfEmsTree();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfEmsTree(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfVmsEmsMapList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfVmsEmsMap();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfVmsEmsMap(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfVmsClusterList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfVmsCluster();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfVmsCluster(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfVmsPhysicalList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfVmsPhysical();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfVmsPhysical(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void insertIfVmsLogicalList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfVmsLogical();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfVmsLogical(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void insertIfVmsList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfVms();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfVms(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void insertIfWebURLList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfWebURL();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfWebURL(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void callSyncVmsCluster(Map map) throws NkiaException {
		itgSmsJobDAO.callSyncVmsCluster(map);
	}
	
	/* 20200323_이원재_001_신규작성
	 * (20200330_001_수정이력: @Transactional 어노테이션 추가 및 insert과정 중 예외 발생 시 스케쥴링 작업 롤백 로직 추가)
	 */
	@Override
	@Transactional
	public void insertEmsMessage(List<HashMap> paramList) throws NkiaException {
		try {
			itgSmsJobDAO.deleteEmsMessage();
			for(HashMap dataMap: paramList) {
				itgSmsJobDAO.insertEmsMessage(dataMap);
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
	// 20200323_이원재_001_신규작성 END
	// 20200326_이원재_001_신규작성 
	@Override
	public List selectEmsMessageList(Map paramMap) throws NkiaException {
		return itgSmsJobDAO.selectEmsMessageList(paramMap);
	}
	
	@Override
	public int selectEmsMessageCount(Map paramMap) throws NkiaException {
		return itgSmsJobDAO.selectEmsMessageCount(paramMap);
	}
	// 20200326_이원재_001_신규작성 END

	@Override
	public void insertIfReportGroupList(List<HashMap> paramList) throws NkiaException {
		itgSmsJobDAO.truncateIfReportGroup();
		for(HashMap dataMap : paramList){
			try {
				itgSmsJobDAO.insertIfReportGroup(dataMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
