/*
 * @(#)DataMonitorJob.java              2018. 1. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.dao.NamedParamDAO;
import com.nkia.itg.base.service.NamedParamService;
import com.nkia.itg.system.datamonitor.service.DataMonitorService;
import com.nkia.itg.system.scheduler.service.SchedulerService;
 
public class DataMonitorJob {
	
	public static final Logger logger = LoggerFactory.getLogger(DataMonitorJob.class);

	@Resource(name="namedParamDAO")
	private NamedParamDAO namedParamDAO;

	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");;
	
	private HashMap initSchedulerJob(String[] jobArgs) throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId);
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * 
	 * 데이타모니터링 실행 
	 *
	 */
	public void runDataMonitor(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			// START
			String  query = "";
			String datamonId = "";
			
			NamedParamService namedParamService = (NamedParamService) NkiaApplicationContext.getCtx().getBean("namedParamService");
			DataMonitorService dataMonitorService = (DataMonitorService) NkiaApplicationContext.getCtx().getBean("dataMonitorService");
			
			StringBuffer querySb = new StringBuffer();
			querySb.append("\n   SELECT DATAMON_ID, DATAMON_NM  ") ;
			querySb.append("\n     FROM SYS_DATAMON A ") ;
			querySb.append("\n    WHERE A.USE_YN = 'Y' ") ;
			querySb.append("\n ORDER BY DISPLAY_NO ") ;
			query = querySb.toString();
			Map<String, Object> params = null;
			params = new HashMap<String, Object>();
			List<Map<String, Object>> list = namedParamService.selectList(query, params);
			
			for(Map<String, Object> rowMap: list){
				datamonId = StringUtil.parseString(rowMap.get("DATAMON_ID"));
				Map<String, Object> resultMap = dataMonitorService.runDataMonitor(datamonId, params);
			}
			// END
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}
	
}
