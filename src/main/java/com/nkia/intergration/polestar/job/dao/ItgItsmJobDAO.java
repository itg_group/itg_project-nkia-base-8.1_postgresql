/*
 * @(#)ItgJobDAO.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("itgItsmJobDAO")
public class ItgItsmJobDAO extends EgovComAbstractDAO{

	public void truncateIfItsm()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfItsm", null);
	}
	
	public void insertIfItsm(HashMap itsmData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfItsm", itsmData);
	}
	
	public void truncateIfCode()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfCode", null);
	}
	
	public void insertIfCode(HashMap codeData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfCode", codeData);
	}
	
	public void truncateIfCloudLog()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfCloudLog", null);
	}
	
	public void truncateIfCloudDRLog()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfCloudDRLog", null);
	}
	
	public void truncateIfCloud()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfCloud", null);
	}
	
	public void insertIfCloud(HashMap codeData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfCloud", codeData);
	}
	
	public void truncateIfCloudDR()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfCloudDR", null);
	}
	
	public void insertIfCloudDR(HashMap codeData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfCloudDR", codeData);
	}
	
	public void truncateIfCloudHist()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfCloudHist", null);
	}
	
	public void insertIfCloudHist(HashMap codeData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfCloudHist", codeData);
	}
	
	public void truncateIfCloudDRHist()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfCloudDRHist", null);
	}
	
	public void insertIfCloudDRHist(HashMap codeData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfCloudDRHist", codeData);
	}
	
	public void truncateIfDNSZone()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfDNSZone", null);
	}
	
	public void insertIfDNSZone(HashMap codeData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfDNSZone", codeData);
	}
	
	public void truncateIfDNSRecord()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfDNSRecord", null);
	}
	
	public void insertIfDNSRecord(HashMap codeData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfDNSRecord", codeData);
	}
	
	public void callSyncCloud(Map map) throws NkiaException {
		this.insert("ItgItsmJobDAO.callSyncCloud", map);
	}
	
	public void callSyncCloudHist(Map map) throws NkiaException {
		this.insert("ItgItsmJobDAO.callSyncCloudHist", map);
	}
	
	public void callSyncCloudDR(Map map) throws NkiaException {
		this.insert("ItgItsmJobDAO.callSyncCloudDR", map);
	}
	public void truncateIfUser()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfUser", null);
	}
	
	public void insertIfUser(HashMap userData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfUser", userData);
	}
	public void truncateIfMapping()throws NkiaException {
		this.delete("ItgItsmJobDAO.truncateIfMapping", null);
	}
	
	public void insertIfMapping(HashMap mappingData)throws NkiaException {
		this.insert("ItgItsmJobDAO.insertIfMapping", mappingData);
	}
	
	public void callSyncCmdbData(Map map) throws NkiaException {
		this.insert("ItgItsmJobDAO.callSyncCmdbData", map);
	}
	
	public List selectAssetLnIncreaseEndList()throws NkiaException {
		return list("ItgItsmJobDAO.selectAssetLnIncreaseEndList", null);
	}
	
	public List selectAssetLNAlarmUserList()throws NkiaException {
		return list("ItgItsmJobDAO.selectAssetLNAlarmUserList", null);
	}
}
