package com.nkia.intergration.polestar.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mortbay.log.Log;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.intergration.polestar.job.service.ItgSmsJobService;
import com.nkia.intergration.polestar.service.IntergrationPolestarSmsService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.sla.manage.service.SlaManageService;
import com.nkia.itg.system.scheduler.service.SchedulerService;

public class SmsJob implements Job {

public static final Logger logger = LoggerFactory.getLogger(SmsJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");;
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId); //202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * Job Handler - DB 방식인 경우 실행시간에 맞춰 execute 메소드가 호출 된다.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		// Job Args 값 설정
                String scheduleId = jobDataMap.getString("scheduleId");
                String scheduleNm = jobDataMap.getString("scheduleNm");
                String executeMethod = jobDataMap.getString("executeMethod");
               String scheduleConfig = jobDataMap.getString("scheduleConfig");
		String executor = jobDataMap.getString("executor"); 
		String[] jobArgs = {scheduleId, scheduleNm, scheduleConfig, executor};
		
		// 실제 실행될 잡에 대한 분기 처리
		if("gatherEmsServerData".equalsIgnoreCase(executeMethod)){
			gatherEmsServerData(jobArgs);
		} else if("gatherEmsDBData".equalsIgnoreCase(executeMethod)){
			gatherEmsDBData(jobArgs);
		} else if("gatherEmsWASData".equalsIgnoreCase(executeMethod)){
			gatherEmsWASData(jobArgs);
		} else if("gatherEmsVMSData".equalsIgnoreCase(executeMethod)){
			gatherEmsVMSData(jobArgs);
		} else if("gatherEmsWebURLData".equalsIgnoreCase(executeMethod)){
			gatherEmsWebURLData(jobArgs);
		} else if("gatherEmsReportGroupData".equalsIgnoreCase(executeMethod)){
			gatherEmsReportGroupData(jobArgs);
		} else if("gatherEmsNetworkData".equalsIgnoreCase(executeMethod)){
			gatherEmsNetworkData(jobArgs);
		} else if("gatherEmsMessage".equalsIgnoreCase(executeMethod)) {
			gatherEmsMessage(jobArgs);
		} else if("syncEmsDataToAmServerDaily".equalsIgnoreCase(executeMethod)){
			syncEmsDataToAmServerDaily(jobArgs);
		} else if("syncEmsDataToAmNetworkDaily".equalsIgnoreCase(executeMethod)){
			syncEmsDataToAmNetworkDaily(jobArgs);
		} else if("syncVmsClusterDataToAM".equalsIgnoreCase(executeMethod)){
			syncVmsClusterDataToAM(jobArgs);
		}
	}
	
	/**
	 * EMS 서버 데이터 수집
	 */
	public void gatherEmsServerData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			
			// EMS 서버정보 목록
			List<HashMap> emsServerOsList = intergrationPolestarService.searchEmsServerOsList();
			if( emsServerOsList != null && emsServerOsList.size() > 0 ){
				itgJobService.insertIfEmsServerOsList(emsServerOsList);
			}
			 
			// EMS 서버 CPUS 정보 목록
			List<HashMap> emsServerCpusList = intergrationPolestarService.searchEmsServerCpusList();
			if( emsServerCpusList != null && emsServerCpusList.size() > 0 ){
				itgJobService.insertIfEmsServerCpusList(emsServerCpusList);
			}
			
			// EMS 서버 Disks 정보 목록
			List<HashMap> emsServerDisksList = intergrationPolestarService.searchEmsServerDisksList();
			if( emsServerDisksList != null && emsServerDisksList.size() > 0 ){
				itgJobService.insertIfEmsServerDisksList(emsServerDisksList);
			}
			
			// EMS 서버 Disk 정보 목록
			List<HashMap> emsServerDiskList = intergrationPolestarService.searchEmsServerDiskList();
			if( emsServerDiskList != null && emsServerDiskList.size() > 0 ){
				itgJobService.insertIfEmsServerDiskList(emsServerDiskList);
			}
			
			// EMS 서버 Memory 정보 목록
			List<HashMap> emsServerMemoryList = intergrationPolestarService.searchEmsServerMemoryList();
			if( emsServerMemoryList != null && emsServerMemoryList.size() > 0 ){
				itgJobService.insertIfEmsServerMemoryList(emsServerMemoryList);
			}
			
			// EMS 서버 네트워크 Interfaces 목록
			List<HashMap> emsServerNwInterfaceList = intergrationPolestarService.searchEmsServerNwInterfaceList();
			if( emsServerNwInterfaceList != null && emsServerNwInterfaceList.size() > 0 ){
				itgJobService.insertIfEmsServerNwInterfaceList(emsServerNwInterfaceList);
			}
			
			// EMS TREE 목록
			List<HashMap> emsTreeList = intergrationPolestarService.searchEmsTreeList();
			if( emsTreeList != null && emsTreeList.size() > 0 ){
				itgJobService.insertIfEmsTreeList(emsTreeList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS DB 데이터 수집
	 */
	public void gatherEmsDBData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			// EMS DB MariaDB 목록
			List<HashMap> emsDBMariaDBList = intergrationPolestarService.searchEmsDBMariaDBList();
			if( emsDBMariaDBList != null && emsDBMariaDBList.size() > 0 ){
				itgJobService.insertIfEmsDBMariaDBList(emsDBMariaDBList);
			}
			
			// EMS DB MSSQL 목록
			List<HashMap> emsDBMssqlList = intergrationPolestarService.searchEmsDBMssqlList();
			if( emsDBMssqlList != null && emsDBMssqlList.size() > 0 ){
				itgJobService.insertIfEmsDBMssqlList(emsDBMssqlList);
			}
			
			// EMS DB MYSQL 목록
			List<HashMap> emsDBMysqlList = intergrationPolestarService.searchEmsDBMysqlList();
			if( emsDBMysqlList != null && emsDBMysqlList.size() > 0 ){
				itgJobService.insertIfEmsDBMysqlList(emsDBMysqlList);
			}
			
			// EMS DB ORACLE 목록
			List<HashMap> emsDBOracleList = intergrationPolestarService.searchEmsDBOracleList();
			if( emsDBOracleList != null && emsDBOracleList.size() > 0 ){
				itgJobService.insertIfEmsDBOracleList(emsDBOracleList);
			}
			
			// EMS DB PostgreSQL 목록
			List<HashMap> emsDBPostgreSQLList = intergrationPolestarService.searchEmsDBPostgreSQLList();
			if( emsDBPostgreSQLList != null && emsDBPostgreSQLList.size() > 0 ){
				itgJobService.insertIfEmsDBPostgreSQLList(emsDBPostgreSQLList);
			}
			
			// EMS DB TIBERO 목록
			List<HashMap> emsDBTiberoList = intergrationPolestarService.searchEmsDBTiberoList();
			if( emsDBTiberoList != null && emsDBTiberoList.size() > 0 ){
				itgJobService.insertIfEmsDBTiberoList(emsDBTiberoList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS WAS 데이터 수집
	 */
	public void gatherEmsWASData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			// EMS WAS INSTANCE 목록
			List<HashMap> wasInstanceList = intergrationPolestarService.searchEmsWasInstanceList();
			if( wasInstanceList != null && wasInstanceList.size() > 0 ){
				itgJobService.insertIfEmsWasInstanceList(wasInstanceList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS VMS 데이터 수집
	 */
	public void gatherEmsVMSData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			// VMS EMS MAP 목록
			List<HashMap> vmsMapList = intergrationPolestarService.searchVmsEmsMapList();
			if( vmsMapList != null && vmsMapList.size() > 0 ){
				itgJobService.insertIfVmsEmsMapList(vmsMapList);
			}
			
			// VMS CLUSTER 목록
			List<HashMap> vmsClusterList = intergrationPolestarService.searchVmsClusterList();
			if( vmsClusterList != null && vmsClusterList.size() > 0 ){
				itgJobService.insertIfVmsClusterList(vmsClusterList);
			}
			
			// VMS PHYSICAL 목록
			List<HashMap> vmsPhysicalList = intergrationPolestarService.searchVmsPhysicalList();
			if( vmsPhysicalList != null && vmsPhysicalList.size() > 0 ){
				itgJobService.insertIfVmsPhysicalList(vmsPhysicalList);
			}
			
			// VMS LOGICAL 목록
			List<HashMap> vmsLogicalList = intergrationPolestarService.searchVmsLogicalList();
			if( vmsLogicalList != null && vmsLogicalList.size() > 0 ){
				itgJobService.insertIfVmsLogicalList(vmsLogicalList);
			}
			
			// VMS 목록
			List<HashMap> vmsList = intergrationPolestarService.searchVmsList();
			if( vmsList != null && vmsList.size() > 0 ){
				itgJobService.insertIfVmsList(vmsList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS WEB URL 데이터 수집
	 */
	public void gatherEmsWebURLData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			// EMS WEB URL 목록
			List<HashMap> webURLList = intergrationPolestarService.searchWebURLList();
			if( webURLList != null && webURLList.size() > 0 ){
				itgJobService.insertIfWebURLList(webURLList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS REPORT GROUP 데이터 수집
	 */
	public void gatherEmsReportGroupData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			// EMS REPORT GROUP 목록
			List<HashMap> reportGroupList = intergrationPolestarService.searchReportGroupList();
			if( reportGroupList != null && reportGroupList.size() > 0 ){
				itgJobService.insertIfReportGroupList(reportGroupList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS NW 데이터 수집
	 */
	public void gatherEmsNetworkData(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			
			// EMS NW정보 목록
			List<HashMap> nwInfoList = intergrationPolestarService.searchEmsNetworkInfoList();
			if( nwInfoList != null && nwInfoList.size() > 0 ){
				itgJobService.insertIfEmsNwInfoList(nwInfoList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS 메세지 데이터 수집
	 */
	public void gatherEmsMessage(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;

		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			SlaManageService slaManageService = (SlaManageService) NkiaApplicationContext.getCtx().getBean("SlaManageService");
			IntergrationPolestarSmsService intergrationPolestarService = (IntergrationPolestarSmsService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarSmsService");
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			Map paramMap = new HashMap();
			paramMap.put("point_type", "EMS_MESSAGE_LEVEL");
			List emsCustomerIdList = slaManageService.selectEmsCustomerNmList(paramMap);
			
			paramMap = new HashMap();
			paramMap.put("emsCustomerNmItem", emsCustomerIdList);
			
			// EMS 메세지정보 목록
			List<HashMap> emsMessageList = intergrationPolestarService.searchCommonAlarmList(paramMap);
			if( emsMessageList != null && emsMessageList.size() > 0 ){
				itgJobService.insertEmsMessage(emsMessageList);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
    }
	
	/**
	 * EMS 서버 수집정보 자동 동기화
	 * @param jobArgs
	 */
	public void syncEmsDataToAmServerDaily(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			itgJobService.updateSyncEmsDataToAmServer("DAILY");
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * EMS NW 수집정보 자동 동기화
	 * @param jobArgs
	 */
	public void syncEmsDataToAmNetworkDaily(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			itgJobService.updateSyncEmsDataToAmNetwork("DAILY");
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
	}
	
	/**
	 * VMS CLUSTER 정보 자동 동기화
	 * @param jobArgs
	 */
	public void syncVmsClusterDataToAM(String[] jobArgs){
		HashMap schedulerJobMap = null;
		boolean isSuccess = true;
		String resultMsg = null;
		
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			ItgSmsJobService itgJobService = (ItgSmsJobService) NkiaApplicationContext.getCtx().getBean("itgSmsJobService");
			
			Map map = new HashMap();
			map.put("V_RESULT", "");
			itgJobService.callSyncVmsCluster(map);
			
			if(!"SUCCESS".equals(map.get("V_RESULT").toString())) {
				isSuccess = false;
				resultMsg = map.get("V_RESULT").toString();
			}
			
			finishedSchedulerJob(schedulerJobMap, isSuccess, resultMsg);
		} catch (Exception e) {
			try {
				isSuccess = false;
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				logger.error(e1.getMessage(), e1);
			}
			logger.error(e.getMessage(), e);
		}
	}
}
