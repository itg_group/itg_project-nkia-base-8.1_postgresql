/*
 * @(#)DcaJob.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.nkia.intergration.polestar.enums.OZParameter;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.system.scheduler.service.SchedulerService;

public class ItgReportJob {
	
	public static final Logger logger = LoggerFactory.getLogger(ItgReportJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");
	
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId);
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * 오즈보고서 생성
	 */
	public void createReportFileDaily(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
//
//			// 오즈보고서 export 경로
//			String exportFilePath = NkiaApplicationPropertiesMap.getProperty("Oz.Report.Path");
//			String exportFileName = NkiaApplicationPropertiesMap.getProperty("Oz.Report.Name");
//			
//			// export 하기 전, 지정된 경로의 모든 파일 삭제
//			File file = new File(exportFilePath);
//			for(File fileObj : file.listFiles()){
//				fileObj.delete();
//			}
//			
//			/********  보고서 export ************/
//			// OZ API 필요 Parameter 정의
//			String res = null;
//			//OZReportAPI api = new OZReportAPI();
//
//			String currentDate = DateUtil.getDate();
//		    OZParameter param = new OZParameter();
//		    param.setExportPath(exportFilePath);	// 파일 export할 경로
//		    param.setReportKey("DAILY_TOT_LIST");								// export 할 보고서파일
//		    param.setExportFileName(exportFileName + "_" + currentDate);			// export 되서 download 보고서 파일 명(pdf)
//		    param.setServletUrl(NkiaApplicationPropertiesMap.getProperty("Oz.Connection.Servlet"));		// OZ 서블릿
//		    
//		    // 보고서 조회 파라메터 설정
//		    param.putUserProperty("SEARCH_DT", DateUtil.getDate("-"));
//		    
//		    // 보고서 파일 다운로드 API 호출
//		    String paramStr = param.getParamString();
//		    //res = api.runToAPI(paramStr, "@OZSEP@", Category.getInstance("Runozviewer"));
//			
//			isSuccess = true;
//			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e);
			}
			new NkiaException(e);
		}
    }
	
	
	
	
	
}
