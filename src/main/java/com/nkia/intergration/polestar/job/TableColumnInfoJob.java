/*
 * @(#)DataMonitorJob.java              2018. 1. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.polestar.job;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.system.dataset.service.TableColumnService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.scheduler.service.SchedulerService;
 
public class TableColumnInfoJob {
	
	public static final Logger logger = LoggerFactory.getLogger(TableColumnInfoJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");;
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		String scheduleId = jobArgs[0];
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId); 
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		schedulerJobMap.put("execute_result", isSuccess);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.isEmpty() == false)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
	}
	
	/**
	 * 
	 * 테이블컬럼정보변경 실행 
	 * 
	 * 배치실행 : ScheduleExecute.java - createSchedule()
	 * 임의실행 : SchedulerController.java - executeScheduler.do
	 */
	public void runTableColumnInfoUpdate(String[] jobArgs) throws NkiaException {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			// START
			String scheduleId = ""; // jobArgs[0]
			String scheduleNm = ""; // jobArgs[1]
			String scheduleConfig = ""; // jobArgs[2]
			String login_user_id = ""; // jobArgs[3]
			if(null != jobArgs){
				if(0 < jobArgs.length){
					scheduleId = jobArgs[0];
				}
				if(1 < jobArgs.length){
					scheduleNm = jobArgs[1];
				}
				if(2 < jobArgs.length){
					scheduleConfig = jobArgs[2];
				}
				if(3 < jobArgs.length){
					login_user_id = jobArgs[3];
				}
			}

			TableColumnService tableColumnService = (TableColumnService) NkiaApplicationContext.getCtx().getBean("tableColumnService");
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("scheduleId", scheduleId);
			paramMap.put("scheduleNm", scheduleNm);
			paramMap.put("scheduleConfig", scheduleConfig);
			paramMap.put("login_user_id", login_user_id);
			Map<String, Object> resMap = tableColumnService.insertTableColumnInfoWithSchedule(paramMap);
			if("SUCCESS".equals(resMap.get("RESULT_CD"))){
				isSuccess = true;
			}
			// END
			
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				throw new NkiaException(e1);
			}
			throw new NkiaException(e);
		}
	}
	
}
