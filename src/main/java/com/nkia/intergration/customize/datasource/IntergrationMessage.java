package com.nkia.intergration.customize.datasource;

import javax.annotation.Resource;

import com.ibatis.sqlmap.client.SqlMapClient;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

public class IntergrationMessage extends EgovAbstractDAO{

	// DB 변경시 @Resource name 변경 
	// dataSource, sqlMap, transaction.xml 수정 
	@Resource(name="message.sqlMapClient")
	public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
        super.setSuperSqlMapClient(sqlMapClient);
    }
}
