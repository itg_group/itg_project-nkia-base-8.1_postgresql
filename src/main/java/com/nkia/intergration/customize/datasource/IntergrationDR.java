/*
 * @(#)IntergrationPolestarDcaDAO.java              2014. 1. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.datasource;

import javax.annotation.Resource;

import com.ibatis.sqlmap.client.SqlMapClient;

import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

public class IntergrationDR extends  EgovAbstractDAO{
	
	// DB 변경시 @Resource name 변경 
	// dataSource, sqlMap, transaction.xml 수정 
	@Resource(name="dr.sqlMapClient")
	public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
        super.setSuperSqlMapClient(sqlMapClient);
    }
	
}
