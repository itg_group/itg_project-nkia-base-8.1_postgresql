package com.nkia.intergration.customize.job;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.jasypt.contrib.org.apache.commons.codec_1_3.binary.Base64;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.intergration.customize.job.service.UbitJobService;
import com.nkia.intergration.polestar.job.DcaJob;
import com.nkia.intergration.polestar.job.service.ItgJobService;
import com.nkia.intergration.polestar.service.IntergrationPolestarService;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.service.SmsSendService;
import com.nkia.itg.customize.ubit.service.CustomizeUbitService;
import com.nkia.itg.system.scheduler.service.SchedulerService;

public class UbitJob implements Job{

	public static final Logger logger = LoggerFactory.getLogger(UbitJob.class);
	
	private static final SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");
	private static final UbitJobService ubitJobService = (UbitJobService) NkiaApplicationContext.getCtx().getBean("ubitJobService");
		
	
	private HashMap initSchedulerJob(String[] jobArgs)throws NkiaException {
		HashMap schedulerJobMap = new HashMap();
		/*
		String scheduleId = jobArgs[0];
		
		if(scheduleId != null) {
			if(!"".equals(scheduleId)) {
				Integer scheduleIdIdx = schedulerService.getSchedulerNextIndexById(scheduleId);
				schedulerJobMap.put("schedule_id", scheduleId); 
				schedulerJobMap.put("schedule_nm", jobArgs[1]);
				schedulerJobMap.put("idx", scheduleIdIdx);
				schedulerJobMap.put("schedule_executor", jobArgs[3]);
				schedulerService.insertSysSchedulerResult(schedulerJobMap);
			}
		}
		*/
		return schedulerJobMap;
	}
	
	private void finishedSchedulerJob(HashMap schedulerJobMap, boolean isSuccess, String errorMsg)throws NkiaException {
		/*
		schedulerJobMap.put("execute_result", isSuccess);
		if( (errorMsg != null) && (errorMsg.length() > 0)){
			schedulerJobMap.put("error_msg", errorMsg);
		}
		if((schedulerJobMap != null) && (schedulerJobMap.size() > 0)) {
			schedulerService.updateSysSchedulerResult(schedulerJobMap);			
		}
		*/
	}
	
	/**
	 * Job Handler - DB 방식인 경우 실행시간에 맞춰 execute 메소드가 호출 된다.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		/*
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		// Job Args 값 설정
        String scheduleId = jobDataMap.getString("scheduleId");
        String scheduleNm = jobDataMap.getString("scheduleNm");
        String executeMethod = jobDataMap.getString("executeMethod");
        String scheduleConfig = jobDataMap.getString("scheduleConfig");
		String executor = jobDataMap.getString("executor"); 
		String[] jobArgs = {scheduleId, scheduleNm, scheduleConfig, executor};
		
		if("syncUserCustInfo".equalsIgnoreCase(executeMethod)){
			syncUserCustInfo(jobArgs);
		} else if("syncUserMaxDay".equalsIgnoreCase(executeMethod)) {
			syncUserMaxDay(jobArgs);
		}else if("deleteAttachFile".equalsIgnoreCase(executeMethod)) {
			deleteAttachFile(jobArgs);
		}else if("deadlineAlarm".equalsIgnoreCase(executeMethod)) {
			deadlineAlarm(jobArgs);
		}else if("deleteHistory".equalsIgnoreCase(executeMethod)) {
			deleteHistory(jobArgs);
		}else if("workExpectedAlarm".equalsIgnoreCase(executeMethod)) {
			workExpectedAlarm(jobArgs);
		}else if("snapshotTest".equalsIgnoreCase(executeMethod)) {
			snapshotTest(jobArgs);
		}else if("userDataMerge".equalsIgnoreCase(executeMethod)) {
			userDataMerge(jobArgs);
		}else if("sceduleCheckDaily".equalsIgnoreCase(executeMethod)) {
			sceduleCheckDaily(jobArgs);
		}
		*/
	}
	
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 사용자,부서 연계
	 * 
	 */
	public void syncUserCustInfo(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			/*
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			
			// 1.JWT 토큰요청
			Map<String, Object> inJsonTokenMap = new HashMap<String, Object>();
			String key = NkiaApplicationPropertiesMap.getProperty("SyncUser.Token.Key");
			
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date time = new Date();		
			String vector = format.format(time)+ "_LDCC_IV";
			String str = NkiaApplicationPropertiesMap.getProperty("Sync.User.Id");
			byte[] textBytes = str.getBytes("UTF-8");

            IvParameterSpec ivSpec = new IvParameterSpec( vector.getBytes() );
            SecretKeySpec newKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = null;
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
            byte[] encrypted = cipher.doFinal(textBytes);
    		String enStr = new String(Base64.encodeBase64(encrypted));
    		//String enStr = new String(Base64.encodeBase64(encrypted));
			inJsonTokenMap.put("emp_id", enStr); //21079 담당자님ID 
			String body = JsonMapper.toJson(inJsonTokenMap);
			
			String sUrl = "";
			sUrl = NkiaApplicationPropertiesMap.getProperty("SyncUser.Token.Url");
			
			URL postUrl = new URL(sUrl);
			HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setConnectTimeout(3000);
			
			OutputStream os = conn.getOutputStream();
			os.write(body.getBytes());
			os.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			String responseJson = "";
			while ((output = br.readLine()) != null) {
				responseJson += output;
			}
			conn.disconnect();

			Map<String, Object> resultMap = null;
			try {
				resultMap= JsonMapper.fromJson(responseJson, Map.class);
				//Gson gson = new Gson();
				//resultVO = gson.fromJson(responseJson, ChangeVO.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//1.1 토큰정보셋팅
			String temp = resultMap.get("d").toString();
			resultMap = null;
			resultMap= JsonMapper.fromJson(temp, Map.class);
			String token = resultMap.get("result").toString();
			token = token.substring(0, token.lastIndexOf(","));
			token = token.substring(token.indexOf("=")+1);
			
			
			// 2. 사용자정보 요청
			
			Map<String, Object> inJsonUserMap = new HashMap<String, Object>();
			Map<String, Object> inJsonMethodParamMap = new HashMap<String, Object>();
			Map<String, Object> inJsonEmpidMap = new HashMap<String, Object>();
			inJsonEmpidMap.put("EMP_ID", "");
			//inJsonMethodParamMap.put("method", "/Common/v1/users");
			inJsonMethodParamMap.put("method", "/UBIT/v1/Users");
			inJsonMethodParamMap.put("parameter", inJsonEmpidMap);
			// 요청 기본정보
			inJsonUserMap.put("request", inJsonMethodParamMap);
			body = JsonMapper.toJson(inJsonUserMap);
			
			sUrl = NkiaApplicationPropertiesMap.getProperty("SyncUser.Data.Url");
			
			postUrl = new URL(sUrl);
			conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setConnectTimeout(3000);
			conn.setRequestProperty("Authorization", token.toString());
			os = conn.getOutputStream();
			os.write(body.getBytes());
			os.flush();
			
			br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			output = "";
			responseJson = "";
			while ((output = br.readLine()) != null) {
				responseJson += output;
			}
			conn.disconnect();
			resultMap = null;
			try {
				resultMap= JsonMapper.fromJson(responseJson, Map.class);
				//Gson gson = new Gson();
				//resultVO = gson.fromJson(responseJson, ChangeVO.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 3. 사용자정보 임시테이블(IF_USER) 삭제
			// 4. 사용자정보 임시테이블(IF_USER) 테이블에 등록
			// 5. 임시테이블(IF_USER)와 사용자테이블 (SYS_USER) MERGE
			Map<String, Object> userMap = new HashMap<String, Object>();
			String tempUser = resultMap.get("d").toString();
			userMap = JsonMapper.fromJson(tempUser, Map.class);
			List<Map<String, Object>> userList = (List)userMap.get("result");
			ubitJobService.insertUserList(userList);
			
			// 6. 조직정보 요청
			
			body = "";
			Map<String, Object> inJsonDeptMap = new HashMap<String, Object>();
			Map<String, Object> inJsonDeptMethodParamMap = new HashMap<String, Object>();
			Map<String, Object> inJsonDeptCodeMap = new HashMap<String, Object>();
			inJsonDeptCodeMap.put("DEPT_CODE", "");
			inJsonMethodParamMap.put("method", "/Common/v1/depts");
			inJsonMethodParamMap.put("parameter", inJsonEmpidMap);
			// 요청 기본정보
			inJsonUserMap.put("request", inJsonMethodParamMap);
			body = JsonMapper.toJson(inJsonUserMap);
			
			sUrl = NkiaApplicationPropertiesMap.getProperty("SyncUser.Data.Url");
			
			postUrl = new URL(sUrl);
			conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setConnectTimeout(3000);
			conn.setRequestProperty("Authorization", token.toString());
			os = conn.getOutputStream();
			os.write(body.getBytes());
			os.flush();
			
			br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			output = "";
			responseJson = "";
			while ((output = br.readLine()) != null) {
				responseJson += output;
			}
			conn.disconnect();
			
			resultMap = null;
			try {
				resultMap= JsonMapper.fromJson(responseJson, Map.class);
				//Gson gson = new Gson();
				//resultVO = gson.fromJson(responseJson, ChangeVO.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Map<String, Object> deptMap = new HashMap<String, Object>();
			String tempDept = resultMap.get("d").toString();
			deptMap= JsonMapper.fromJson(tempDept, Map.class);
			List<Map<String, Object>> deptList = (List)deptMap.get("result");
			// 7. 조직정보 임시테이블(IF_CUST) 삭제
			// 8. 조직정보 임시테이블(IF_CUST) 테이블에 등록
			// 9. 조직정보 임시테이블(IF_CUST)와 조직정보테이블(IF_CUST) MERGE
			ubitJobService.insertDeptList(deptList);
			
			

			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
			*/
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 최종접속일로 MaxDay 경과후 계정잠그고 개인정보삭제
	 * 
	 */
	public void syncUserMaxDay(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			/*
			schedulerJobMap = initSchedulerJob(jobArgs);
			
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			
			// 1.MAXDAY값을 가져와서 등록
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("MAX_DAY", NkiaApplicationPropertiesMap.getProperty("Globals.Login.MaxDay"));
			ubitJobService.syncUserMaxDay(paramMap);			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
			*/
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 최종접속일로 MaxDay 경과후 서비스요청-계정등록에 있는 첨부파일 삭제 
	 * 
	 */
	public void deleteAttachFile(String[] jobArgs) {
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			/*
			schedulerJobMap = initSchedulerJob(jobArgs);
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			// 1.MAXDAY값을 가져와서 등록
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("MAX_DAY", NkiaApplicationPropertiesMap.getProperty("Globals.Login.MaxDay"));
			ubitJobService.deleteAttachFile(paramMap);			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
			*/
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
	}
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 접수하지 않은건에 알람 보내기. 
	 * 
	 */
	public void deadlineAlarm(String[] jobArgs) {
		/*
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			
			schedulerJobMap = initSchedulerJob(jobArgs);
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			//1. 대상건 조회 (처리마감시한 -3일  , +1
			List resultList = ubitJobService.selectDeadlineAlarm();
			for(int i=0;i<resultList.size();i++){
				Map smsInfo = new HashMap();
				Map resultMap = (Map)resultList.get(i);
				String toPhone = (String)(resultMap.get("HP_NO") == null ? "-" : resultMap.get("HP_NO"));
				smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
				String content = "ITSM 업무 알림입니다. \n";
				content += "▶프로세스 정보 : "+ (String)resultMap.get("REQ_TYPE_NM") + "\n";
				content += "▶ID : " + (String)resultMap.get("SR_ID")+ "\n";
				if("BEFORE".equals(resultMap.get("ALARMTYPE"))){
					content += "▶알림내용 : 요청지연예정" + "\n";
				}else{
					content += "▶알림내용 : 요청지연" + "\n";
				}
				content += "▶처리마감시한  : " + (String)resultMap.get("END_DUE_DT")+ "\n";
				content += "▶제목 : " +(String)resultMap.get("TITLE")+"\n";
				content += "빠른 처리 바랍니다.";
				smsInfo.put("MSG_BODY", content);
				//smsInfo.put("RE_BODY", "["+(String)processDetail.get("SR_ID")+"]"+(String)processDetail.get("REQ_TYPE_NM")+"_업무가 배정되었습니다.");
				smsInfo.put("RE_BODY", "["+(String)resultMap.get("SR_ID")+"]"+"처리마감시한 입니다.");
				smsInfo.put("TEMPLATE_CODE", "LMSG_20200317133022746749"); //ITSM업무알림 템플릿코드
				smsInfo.put("SENDER_KEY", "ef834c839e4a9c83ac3e027b0ed782a7545014d4"); // 센더키
				smsInfo.put("SEND_PHONE", "0226264210"); // 보내는사람
				smsInfo.put("USER_NM", (String)resultMap.get("USER_NM")); // 보내는사람
				SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
				smsSendService.insertTalkList(smsInfo);
			}			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
			
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
		*/
	}
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 최종접속일로 특정시간 후 히스토리 삭제 
	 * \
	 */
	public void deleteHistory(String[] jobArgs) {
		/*
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			
			schedulerJobMap = initSchedulerJob(jobArgs);
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			// 1.MAXDAY값을 가져와서 등록
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("MAX_DAY", NkiaApplicationPropertiesMap.getProperty("Archive.Log.Maxday")); //사용자정보 권한변경이력
			paramMap.put("MAX_DAY_PRIVACY", NkiaApplicationPropertiesMap.getProperty("Archive.Log.Maxday_privacy")); //사용자정보 권한변경이력
			ubitJobService.deleteHistory(paramMap);
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
			
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
		*/
	}
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 최종접속일로 특정시간 후 히스토리 삭제 
	 * \
	 */
	public void workExpectedAlarm(String[] jobArgs) {
		/*
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			
			schedulerJobMap = initSchedulerJob(jobArgs);
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			CustomizeUbitService customizeUbitService = (CustomizeUbitService) NkiaApplicationContext.getCtx().getBean("customizeUbitService");
			//1. 대상건 조회 (처리마감시한 -3일  , +1
			List resultList = ubitJobService.workExpectedAlarm();
			for(int i=0;i<resultList.size();i++){
				Map paramMap = new HashMap();
				Map resultMap = (Map)resultList.get(i);
				paramMap.put("sr_id", (String)resultMap.get("SR_ID"));
				paramMap.put("popUpType", "workExpected");
				paramMap.put("workStrDt",resultMap.get("START_DT"));
				paramMap.put("workEndDt", resultMap.get("END_DT"));
				customizeUbitService.workStartAlarm(paramMap);
			}
			
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
		*/
	}
	
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * EMS 스냅샷연계테스트 
	 * \
	 */
	public void snapshotTest(String[] jobArgs) {
		/*
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			
			Map returnMap = new HashMap();
//			{
//				"taskId": "1234", //srId
//				"flag": "plan",
//				"resourceInfo":[
//				    {
//				    		"resourceId": 37,
//				    		"resourceType": "Server",
//				    		"requestType": "1,2,4"
//				    }
//				]
//
//			}
			Map<String, Object> resourceMap = new HashMap<String, Object>();
			resourceMap.put("resourceId", 10614);  //EMS ID 숫자형
			resourceMap.put("resourceType", "SERVER");    //"SERVER"고정
			resourceMap.put("requestType", "1,2,3,4,5"); 
			// 요청 기본정보
			Map<String, Object> inJsonEmsMap = new HashMap<String, Object>();
			inJsonEmsMap.put("taskId", "12312314"); //SRID
			//inJsonEmsMap.put("userId", paramMap.get("user_id")); //사용자
			inJsonEmsMap.put("flag", "plan"); //plan /work  < 작업전 후
			ArrayList resourceList = new ArrayList();
			resourceList.add(resourceMap);
			inJsonEmsMap.put("resourceInfo", resourceList);
			String body = JsonMapper.toJson(inJsonEmsMap);
			
			
			//body = JsonMapper.toJson(body);
			
			String sUrl = "";
			//sUrl = "http://tems.lotte.center/rest/v1/itsm/snapshot";
			sUrl = "http://10.231.128.93:8723/rest/v1/itsm/snapshot";
			//sUrl = NkiaApplicationPropertiesMap.getProperty("EMS.maint");
			URL postUrl = new URL(sUrl);
			HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			conn.setConnectTimeout(3000);
			conn.setAllowUserInteraction(false);
			OutputStream os = conn.getOutputStream();
			os.write(body.getBytes());
			os.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			String responseJson = "";
			while ((output = br.readLine()) != null) {
				responseJson += output;
			}
			conn.disconnect();
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
			
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
		*/
	}
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 최종접속일로 MaxDay 경과후 서비스요청-계정등록에 있는 첨부파일 삭제 
	 * 
	 */
	public void userDataMerge(String[] jobArgs) {
		/*
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			
			schedulerJobMap = initSchedulerJob(jobArgs);
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			ubitJobService.userDataMerge();
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
			
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
		*/
	}
	
	/**
	 * 
	 * UBIT(롯데정보통신)
	 * 매일 스케쥴 에러건 체크 (0시에서 8시 스케쥴건). 
	 * 
	 */
	public void sceduleCheckDaily(String[] jobArgs) {
		/*
		HashMap schedulerJobMap = null;
		boolean isSuccess = false;
		try {
			schedulerJobMap = initSchedulerJob(jobArgs);
			IntergrationPolestarService intergrationPolestarService = (IntergrationPolestarService) NkiaApplicationContext.getCtx().getBean("intergrationPolestarService");
			ItgJobService itgJobService = (ItgJobService) NkiaApplicationContext.getCtx().getBean("itgJobService");
			//1. 대상건 조회 (처리마감시한 -3일  , +1
			List userList = ubitJobService.sceduleCheckDailyUser();
			List resultList = ubitJobService.sceduleCheckDaily();
			for(int j=0;j<userList.size();j++){
				Map userMap = (Map)userList.get(j);
				for(int i=0;i<resultList.size();i++){
					Map smsInfo = new HashMap();
					Map resultMap = (Map)resultList.get(i);
					String toPhone = (String)(userMap.get("HP_NO") == null ? "-" : userMap.get("HP_NO"));
					smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
					String content = "ITSM 스케쥴러 작업실패 알림입니다. \n";
					content += "▶프로세스 정보 : 스케쥴러 관리 \n";
					content += "▶ID : " + (String)resultMap.get("SCHEDULE_ID")+ "\n";
					content += "▶실행시간  : " + (String)resultMap.get("EXECUTE_START_DT")+ "\n";
					content += "▶제목 : " + (String)resultMap.get("SCHEDULE_NM")+ "\n";
					smsInfo.put("MSG_BODY", content);
					smsInfo.put("RE_BODY", "ITSM 스케쥴러 작업실패 알림입니다.");
					smsInfo.put("TEMPLATE_CODE", "LMSG_20200317133022746749"); //ITSM업무알림 템플릿코드
					smsInfo.put("SENDER_KEY", "ef834c839e4a9c83ac3e027b0ed782a7545014d4"); // 센더키
					smsInfo.put("SEND_PHONE", "0226264210"); // 보내는사람
					smsInfo.put("USER_NM", (String)userMap.get("USER_NM")); // 보내는사람
					SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
					smsSendService.insertTalkList(smsInfo);
				}
			}
			isSuccess = true;
			finishedSchedulerJob(schedulerJobMap, isSuccess, null);
		} catch (Exception e) {
			try {
				finishedSchedulerJob(schedulerJobMap, isSuccess, e.getMessage());
			} catch (Exception e1) {
				new NkiaException(e1);
			}
			new NkiaException(e);
		}
		*/
	}
}
