/*
 * @(#)IntergrationPolestarDcaDAO.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.job.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.intergration.customize.datasource.IntergrationDR;
import com.nkia.itg.base.application.exception.NkiaException;

@Repository("drDAO")
public class DrDAO extends IntergrationDR {
	
	/**
	 * DR 자산 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchDRDataList()throws NkiaException {
		return list("DrDAO.searchDRDataList", null);
	}
	
	/**
	 * DR 자산 수정이력 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchDRHistDataList()throws NkiaException {
		return list("DrDAO.searchDRHistDataList", null);
	}
	
}
