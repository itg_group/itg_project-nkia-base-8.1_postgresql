package com.nkia.intergration.customize.job.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface UbitJobService {

	public void insertUserList(List userList)throws NkiaException;
	
	public void insertDeptList(List deptList)throws NkiaException;
	
	public void syncUserMaxDay(Map paramMap)throws NkiaException;
	
	public void deleteAttachFile(Map paramMap)throws NkiaException;
	
	public List selectDeadlineAlarm()throws NkiaException;
	
	public void deleteHistory(Map paramMap)throws NkiaException;
	
	public List workExpectedAlarm()throws NkiaException;
	
	public void userDataMerge()throws NkiaException;
	
	public List sceduleCheckDailyUser()throws NkiaException;
	
	public List sceduleCheckDaily()throws NkiaException;
}
