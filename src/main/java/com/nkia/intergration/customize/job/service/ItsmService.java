/*
 * @(#)IntergrationPolestarService.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.job.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ItsmService {
	
	/**
	 * ITSM 자산 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchItsmDataList() throws NkiaException;
	
	/**
	 * ITSM 코드 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchCodeList() throws NkiaException;
	
	/**
	 * ITSM DNS ZONE 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDNSZoneList() throws NkiaException;
	
	/**
	 * ITSM DNS RECORD 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDNSRecordList() throws NkiaException;
	/**
	 * ITSM ASIS USER 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchUserDataList() throws NkiaException;
	/**
	 * ITSM ASIS USER-CUSTOMER MAPPING 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchMappingDataList() throws NkiaException;
}
