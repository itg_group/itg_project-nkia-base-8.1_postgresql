/*
 * @(#)IntergrationPolestarDcaDAO.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.job.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.intergration.customize.datasource.IntergrationITSM;
import com.nkia.itg.base.application.exception.NkiaException;

@Repository("itsmDAO")
public class ItsmDAO extends IntergrationITSM {
	
	/**
	 * ITSM 자산 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchItsmDataList()throws NkiaException {
		return list("ItsmDAO.searchItsmDataList", null);
	}
	
	/**
	 * ITSM 코드 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchCodeList()throws NkiaException {
		return list("ItsmDAO.searchCodeList", null);
	}
	
	/**
	 * ITSM DNS ZONE 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchDNSZoneList()throws NkiaException {
		return list("ItsmDAO.searchDNSZoneList", null);
	}
	
	/**
	 * ITSM DNS RECORD 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchDNSRecordList()throws NkiaException {
		return list("ItsmDAO.searchDNSRecordList", null);
	}
	/**
	 * ITSM USER 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserDataList()throws NkiaException {
		return list("ItsmDAO.searchUserDataList", null);
	}
	/**
	 * ITSM USER-CUSTOMER MAPPING 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List searchMappingDataList()throws NkiaException {
		return list("ItsmDAO.searchMappingDataList", null);
	}
}
