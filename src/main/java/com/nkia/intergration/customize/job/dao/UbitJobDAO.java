/*
 * @(#)ItgJobDAO.java              2014. 1. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.job.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("ubitJobDAO")
public class UbitJobDAO extends EgovComAbstractDAO{
	
	public void truncateUserList()throws NkiaException {
		delete("UbitJobDAO.truncateUserList", null);
	}
	public void insertUserList(Map tempMap)throws NkiaException {
		insert("UbitJobDAO.insertUserList", tempMap);
	}
	public void truncateDeptList()throws NkiaException {
		delete("UbitJobDAO.truncateDeptList", null);
	}
	public void insertDeptList(Map tempMap)throws NkiaException {
		insert("UbitJobDAO.insertDeptList", tempMap);
	}
	public void mergeUserList()throws NkiaException {
		update("UbitJobDAO.mergeUserList", null);
	}
	public void mergeUserGrpList()throws NkiaException {
		update("UbitJobDAO.mergeUserGrpList", null);
	}
	public void mergeCustList()throws NkiaException {
		update("UbitJobDAO.mergeCustList", null);
	}
	public void syncUserUnused()throws NkiaException {
		update("UbitJobDAO.syncUserUnused", null);
	}
	public void syncCustUnused()throws NkiaException {
		update("UbitJobDAO.syncCustUnused", null);
	}
	public void syncUserMaxDay(Map paramMap)throws NkiaException {
		update("UbitJobDAO.syncUserMaxDay", paramMap);
	}
	public List selectAtchFileSRID(Map paramMap) {
		return list("UbitJobDAO.selectAtchFileSRID", paramMap);
	}
	public void deleteAtchFileSRID(Map paramMap)throws NkiaException {
		delete("UbitJobDAO.deleteAtchFileSRID", paramMap);
	}
	public void deleteAtchFileDetailSRID(Map paramMap)throws NkiaException {
		delete("UbitJobDAO.deleteAtchFileDetailSRID", paramMap);
	}
	public List selectDeadlineAlarm()throws NkiaException {
		return list("UbitJobDAO.selectDeadlineAlarm", null);
	}
	public void deleteUserHistory(Map paramMap)throws NkiaException {
		delete("UbitJobDAO.deleteUserHistory", paramMap);
	}
	public void deleteUserGrpHistory(Map paramMap)throws NkiaException {
		delete("UbitJobDAO.deleteUserGrpHistory", paramMap);
	}
	public void deleteCustomerHistory(Map paramMap)throws NkiaException {
		delete("UbitJobDAO.deleteCustomerHistory", paramMap);
	}
	public List workExpectedAlarm()throws NkiaException {
		return list("UbitJobDAO.workExpectedAlarm", null);
	}
	public void updateUserDataMerge()throws NkiaException {
		update("UbitJobDAO.updateUserDataMerge", null);
	}
	public void deleteUserDataMerge()throws NkiaException {
		delete("UbitJobDAO.deleteUserDataMerge", null);
	}
	public void insertUserDataMerge()throws NkiaException {
		insert("UbitJobDAO.insertUserDataMerge", null);
	}
	public void deleteCenterHpNo(Map paramMap)throws NkiaException {
		delete("UbitJobDAO.deleteCenterHpNo", paramMap);
	}
	public void deleteSMSHistory(Map paramMap)throws NkiaException {
		delete("UbitJobDAO.deleteSMSHistory", paramMap);
	}
	public List sceduleCheckDailyUser()throws NkiaException {
		return list("UbitJobDAO.sceduleCheckDailyUser", null);
	}
	public List sceduleCheckDaily()throws NkiaException {
		return list("UbitJobDAO.sceduleCheckDaily", null);
	}
}
