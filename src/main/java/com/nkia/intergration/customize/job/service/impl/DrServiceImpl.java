/*
 * @(#)IntergrationPolestarServiceImpl.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.job.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.customize.job.dao.DrDAO;
import com.nkia.intergration.customize.job.service.DrService;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("drService")
public class DrServiceImpl implements DrService{

	@Resource(name = "drDAO")
	public DrDAO drDAO;
	
	/**
	 * DR 자산 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List<HashMap> searchDRDataList() throws NkiaException {
		return drDAO.searchDRDataList(); 
	}

	@Override
	public List<HashMap> searchDRHistDataList() throws NkiaException {
		return drDAO.searchDRHistDataList(); 
	}

}
