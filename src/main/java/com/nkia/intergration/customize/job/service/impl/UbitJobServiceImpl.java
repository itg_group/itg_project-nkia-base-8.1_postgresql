package com.nkia.intergration.customize.job.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.customize.job.dao.UbitJobDAO;
import com.nkia.intergration.customize.job.service.UbitJobService;
import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("ubitJobService")
public class UbitJobServiceImpl implements UbitJobService{


	@Resource(name = "ubitJobDAO")
	public UbitJobDAO ubitJobDAO;
	

	public void insertUserList(List userList) throws NkiaException {
		ubitJobDAO.truncateUserList();
		for(int i=0;i<userList.size();i++) {
			Map tempMap = (Map) userList.get(i);
			ubitJobDAO.insertUserList(tempMap);
		}
		ubitJobDAO.mergeUserList();
		//ubitJobDAO.mergeUserGrpList();
		ubitJobDAO.syncUserUnused();
	};
	
	public void insertDeptList(List deptList) throws NkiaException {
		ubitJobDAO.truncateDeptList();
		for(int i=0;i<deptList.size();i++) {
			Map tempMap = (Map) deptList.get(i);
			ubitJobDAO.insertDeptList(tempMap);
		}
		ubitJobDAO.mergeCustList();
		ubitJobDAO.syncCustUnused();
	};
	
	public void syncUserMaxDay(Map paramMap) throws NkiaException {
		ubitJobDAO.syncUserMaxDay(paramMap);
	};
	
	public void deleteAttachFile(Map paramMap) throws NkiaException {
		//실제파일삭제
		//paramMap.put("MAX_DAY", 0);  		//테스트용
		List srList = ubitJobDAO.selectAtchFileSRID(paramMap); //실제파일경로 가져오는쿼리
		for(int i=0;i<srList.size();i++) {
			Map tempMap = (Map) srList.get(i);
			
			String filePath = (String)tempMap.get("FILE_STORE_COURS")+tempMap.get("STORE_FILE_NAME");
			String deleteFilePath = BaseConstraints.getDeleteMovePath()+tempMap.get("STORE_FILE_NAME");
			
			filePath.replace('\\','/');
  			File file = new File(filePath);
			if(file.exists()){
				file.delete(); 
			}
			
			// 삭제된 파일 삭제
			file = new File(deleteFilePath);
			if(file.exists()){
				file.delete(); 
			}
		}
		//DB삭제
		ubitJobDAO.deleteAtchFileSRID(paramMap);
		ubitJobDAO.deleteAtchFileDetailSRID(paramMap);
		ubitJobDAO.deleteCenterHpNo(paramMap);
		ubitJobDAO.deleteSMSHistory(paramMap);
	};
	
	public List selectDeadlineAlarm() throws NkiaException {
		List resultList = ubitJobDAO.selectDeadlineAlarm(); 
		return resultList;
	};
	
	public void deleteHistory(Map paramMap) throws NkiaException {
		ubitJobDAO.deleteUserHistory(paramMap);
		ubitJobDAO.deleteUserGrpHistory(paramMap);
		ubitJobDAO.deleteCustomerHistory(paramMap);
	};
	
	public List workExpectedAlarm() throws NkiaException {
		List resultList = ubitJobDAO.workExpectedAlarm(); 
		return resultList;
	};
	
	public void userDataMerge() throws NkiaException {
		ubitJobDAO.updateUserDataMerge(); // 대표고객사 업데이트
		ubitJobDAO.deleteUserDataMerge(); // 고객사조회권한 삭제
		ubitJobDAO.insertUserDataMerge(); // 고객사조회권한 삽입
	};
	
	public List sceduleCheckDailyUser() throws NkiaException {
		List resultList = ubitJobDAO.sceduleCheckDailyUser(); 
		return resultList;
	};
	
	public List sceduleCheckDaily() throws NkiaException {
		List resultList = ubitJobDAO.sceduleCheckDaily(); 
		return resultList;
	};
}
