/*
 * @(#)IntergrationPolestarServiceImpl.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.job.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.intergration.customize.job.dao.ItsmDAO;
import com.nkia.intergration.customize.job.service.ItsmService;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("itsmService")
public class ItsmServiceImpl implements ItsmService{

	@Resource(name = "itsmDAO")
	public ItsmDAO itsmDAO;
	
	/**
	 * ITSM 자산 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List<HashMap> searchItsmDataList() throws NkiaException {
		return itsmDAO.searchItsmDataList(); 
	}

	@Override
	public List<HashMap> searchCodeList() throws NkiaException {
		return itsmDAO.searchCodeList();
	}
	
	@Override
	public List<HashMap> searchDNSZoneList() throws NkiaException {
		return itsmDAO.searchDNSZoneList();
	}
	
	@Override
	public List<HashMap> searchDNSRecordList() throws NkiaException {
		return itsmDAO.searchDNSRecordList();
	}
	
	@Override
	public List<HashMap> searchUserDataList() throws NkiaException {
		return itsmDAO.searchUserDataList();
	}
	
	@Override
	public List<HashMap> searchMappingDataList() throws NkiaException {
		return itsmDAO.searchMappingDataList();
	}
}
