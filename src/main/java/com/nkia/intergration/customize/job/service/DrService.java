/*
 * @(#)IntergrationPolestarService.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.intergration.customize.job.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DrService {
	
	/**
	 * DR 자산 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDRDataList() throws NkiaException;
	
	/**
	 * DR 자산 수정이력 목록 조회
	 * @return
	 * @throws NkiaException
	 */
	public List<HashMap> searchDRHistDataList() throws NkiaException;
}
