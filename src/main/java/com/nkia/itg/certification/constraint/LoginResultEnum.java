package com.nkia.itg.certification.constraint;

/**
 * 로그인 결과 확인에 필요한 상수들을 정의함
 * @author lbass
 *
 */
public enum LoginResultEnum {
	
	NO_ERR("NO_ERR"),
	KEY_ERR("PRIVATE_KEY_ERR"),
	PASSWORD_ERR("PASSWORD_ERR"),
	NOT_USE_ERR("NOT_USE_ERR"),
	DORMANT_ERR("DORMANT_ERR"),
	LOCK_ERR("LOCK_ERR"),
	NOT_USER_ERR("NOT_USER_ERR"),
	USER_GRP_ERR("USER_GRP_ERR"),
	SSO_ERR("SSO_ERR"),
	NULL_ERR("NULL_ERR"),
	MULTI_LOGIN_ERR("MULTI_LOGIN_ERR"),
	TOPMENU_NO_MAPPING_ERR("TOPMENU_NO_MAPPING_ERR"),
	SUBMENU_NO_MAPPING_ERR("SUBMENU_NO_MAPPING_ERR"),
	LONGTERM_ERR("LONGTERM_ERR")
	;
		
	private String message;
	
	LoginResultEnum(String message){
		this.message = message;
    }
	
	public String getMessage() {
		return this.message;
	}
}
