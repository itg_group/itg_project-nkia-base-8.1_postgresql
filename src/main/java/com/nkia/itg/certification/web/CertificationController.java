/*
 * @(#)CertificationController.java       2013. 1. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.certification.web;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.application.util.security.ACipher;
import com.nkia.itg.base.application.util.security.CipherDES;
import com.nkia.itg.base.application.util.security.CryptionChange;
import com.nkia.itg.base.service.SessionService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.constraint.CertificationConstant;
import com.nkia.itg.certification.constraint.LoginResultEnum;
import com.nkia.itg.certification.service.CertificationService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.loginManager.service.LoginManagerService;
import com.nkia.itg.system.menu.service.MenuService;
import com.nkia.itg.system.process.service.ProcessAuthService;
import com.nkia.itg.system.system.service.SystemService;
import com.nkia.itg.system.user.service.UserService;
import com.nkia.itg.system.userGrp.service.UserGrpService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

import com.nets.sso.*;
import com.nets.sso.agent.AuthUtil;
import com.nets.sso.agent.authcheck.AuthCheck;
import com.nets.sso.agent.configuration.SSOConfig;
import com.nets.sso.agent.configuration.SSOProvider;
import com.nets.sso.agent.configuration.SSOSite;
import com.nets.sso.common.AgentException;
import com.nets.sso.common.Utility;
import com.nets.sso.common.enums.AuthStatus;

/**
 * 
 * @version 1.0 2013. 1. 11.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * 
 *        <pre>
 * 인증처리
 * </pre>
 *
 */
@Controller
@RequestMapping(value = "/itg/certification")
public class CertificationController {

	// DB에 코드데이터로 등록 되므로 상수로 선언

	@Resource(name = "certificationService")
	protected CertificationService certificationService;

	@Resource(name = "menuService")
	protected MenuService menuService;

	@Resource(name = "processAuthService")
	protected ProcessAuthService processAuthService;

	@Resource(name = "systemService")
	protected SystemService systemService;

	@Resource(name = "userService")
	protected UserService userService;

	@Resource(name = "userGrpService")
	protected UserGrpService userGrpService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	@Resource(name = "cryptionChange")
	public CryptionChange cryptionChange;

	@Resource(name = "loginManagerService")
	public LoginManagerService loginManagerService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	private int KEY_SIZE = 2048;

	/**
	 * 일반 로그인 화면으로 들어간다
	 * 
	 * @param vo
	 *            - 로그인후 이동할 URL이 담긴 LoginVO
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value = "goLoginPage.do")
	public String goLoginPage(UserVO userVO, HttpServletRequest request,HttpServletResponse response, ModelMap model) throws NkiaException {
		// 세션삭제
		request.getSession().invalidate();
		// 세션에서 인증키를 구해옵니다.
		// generatorKey(request);

		// return "/logon.jsp";
		return "/itg/customize/polestar_itsm/login.nvf";

	}

	@RequestMapping(value = "pwResetTest.do")
	public String pwResetTest(UserVO userVO, HttpServletRequest request,HttpServletResponse response, ModelMap model) throws NkiaException {
		// 세션삭제
		request.getSession().invalidate();
		// 세션에서 인증키를 구해옵니다.
		// generatorKey(request);
		// return "/itg/certification/polestar_login.nvf";
		return "/itg/customize/polestar_itsm/pwresetTest.nvf";
		// return "/itg/portal/login.nvf";
	}

	@RequestMapping(value = "pwResetPopup.do")
	public String pwResetPopup(UserVO userVO, HttpServletRequest request,HttpServletResponse response, ModelMap model) throws NkiaException {

		// 세션삭제
		request.getSession().invalidate();
		// 세션에서 인증키를 구해옵니다.
		// generatorKey(request);

		// return "/itg/certification/polestar_login.nvf";
		return "/itg/customize/polestar_itsm/pwreset.nvf";
		// return "/itg/portal/login.nvf";
	}

	/**
	 * SSO 로그인 화면으로 들어간다
	 * 
	 * @param vo
	 *            - 로그인후 이동할 URL이 담긴 LoginVO
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value = "/goLoginSSOPage.do")
	public String goSSOLoginPage(UserVO userVO, HttpServletRequest request,HttpServletResponse response, ModelMap model) throws NkiaException {

		// 세션에서 인증키를 구해옵니다.
		// generatorKey(request);

		return "/itg/certification/polestar_ssoLogin.nvf";
	}

	/**
	 * 일반(세션) 로그인을 처리한다 외부사용자일때 보통로그인과 같이
	 * 
	 * @param vo
	 *            - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request
	 *            - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @throws IOException 
	 * @exception Exception
	 */
	@RequestMapping(value = "/login.do")
	public String login(@ModelAttribute("thrForm") UserVO userVO,HttpServletRequest request, HttpServletResponse response,ModelMap model) throws NkiaException, IOException {
		// 페이지 이동
		String forwardPage = "/itg/certification/login_result.nvf";

		// 2016.08.01 최양규 추가 시작
		// 프로퍼티에서 포탈 사용유무가 'Y'이면 따라 포탈화면으로 이동한다.
		String portalUseYn = NkiaApplicationPropertiesMap.getProperty("Globals.Portal.Use");
		if ("Y".equals(portalUseYn)) {
			request.setAttribute("tabUrl", NkiaApplicationPropertiesMap.getProperty("Globals.Portal.Url"));
		}

		// 2014.10.20. 정정윤 추가 시작
		// 마지막 로그인 이력을 조회해서 이전 최신로그아웃 날짜가 없다면
		// 로그아웃 날짜를 새로 로그인한 날짜로 업데이트

		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		PrivateKey privateKey = (PrivateKey) session.getAttribute("__rsaPrivateKey__");
		String securedPassWord = request.getParameter("pw");

		// 로그인 설정 불러오기 2018.03.31.정정윤
		List loginProperty = loginManagerService.searchLoginManagerList(null);

		String ip = req.getHeader("X-FORWARDED-FOR");
		String ip2 = "";
		if (ip == null) {
			ip = req.getRemoteAddr();
			ip2 = InetAddress.getLocalHost().getHostAddress();
		}

		try {
			String userID = userVO.getUser_id();
			UserVO resultVO = certificationService.login(userVO);

			if (resultVO == null || resultVO.getUser_id() == null || "".equals(resultVO.getUser_id())) {
				model.put("message", LoginResultEnum.NOT_USER_ERR);
				return forwardPage;
			}

			userVO.setLast_login_ip(ip);
			userVO.setLast_login_dt(resultVO.getLast_login_dt());

			ModelMap userMap = new ModelMap();
			userMap.put("ip", ip);
			userMap.put("user_id", userID);

			model.put("user_id", userID);

			if (userID != null && !userID.equals("")) {
				int loginInfoCnt = certificationService.selectLogOutDtCount(userMap);
				if (loginInfoCnt < 1) {
					String logOutDt = certificationService.selectLogOutDt(userMap);
					if (logOutDt == null || logOutDt.equals("")) {
						updateLoginInfo(userID);
					}
				}
			}

			// 2014.10.20. 정정윤 추가 끝

			// 2015.04.29 김도원 이하 로직 소스 수정 START
			// 유저확인 / 메뉴확인 / 패스워드 체크
			LoginResultEnum result = certificationService.checkLoginInfo(privateKey, securedPassWord, resultVO,model, loginProperty);
			// LoginResultEnum result = LoginResultEnum.NO_ERR; //로그인이 무조건 성공함
			// LoginResultEnum result = LoginResultEnum.PASSWORD_ERR; //로그인이 무조건
			// 성공함

			if (result == LoginResultEnum.NO_ERR) {
				createSessionAndAuth(resultVO);
				// 세션에 정보 셋팅
				request.getSession().removeAttribute("userVO");
				request.getSession().setAttribute("userVO", resultVO);

				// Session에 Locale 설정 ( 참조되는 Resource Locale이 달라진다. )
				if (userVO.getLocale() != null && userVO.getLocale().length() > 0) {
					request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,StringUtils.parseLocaleString(userVO.getLocale()));
				}

				if (loginProperty != null && loginProperty.size() > 0) {
					HashMap loginMngMap = (HashMap) loginProperty.get(0);
					String multiLoginYn = StringUtil.parseString(loginMngMap.get("MULTI_LOGIN_YN"));

					// 중복로그인 허용 불가일 경우
					if ("N".equals(multiLoginYn)) {
						// 중복로그인에 대한 처리 실행
						this.doMultiLoginProcess(session, userID, ip);
					}
				}

				// 최종 접속일을 등록합니다.
				certificationService.updateLastLoginUpdt(userVO);
				// 로그인 실패 횟수 0으로 초기화
				certificationService.updateResetUserLoginFailCount(userVO.getUser_id());
			}

			inputLoginLogData(ip, userVO, result.getMessage(),CertificationConstant.ACCESS_COMMON);
			model.put("message", result.getMessage());
			// 2015.04.29 김도원 이하 로직 소스 수정 END
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			session.removeAttribute("__rsaPrivateKey__"); // 키의 재사용을 막는다. 항상 새로운
															// 키를 받도록 강제.
		}
		return forwardPage;
	}

	/**
	 * 일반(세션) 로그인을 처리한다 //내부사용자일때 SSO체크
	 * 
	 * @param vo
	 *            - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request
	 *            - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @throws IOException 
	 * @exception Exception
	 */
	@RequestMapping(value = "/loginOutside.do")
	public String loginOutside(@ModelAttribute("thrForm") UserVO userVO,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws NkiaException, IOException {
		// 페이지 이동
		String forwardPage = "/itg/certification/login_result.nvf";

		// 2016.08.01 최양규 추가 시작
		// 프로퍼티에서 포탈 사용유무가 'Y'이면 따라 포탈화면으로 이동한다.
		String portalUseYn = NkiaApplicationPropertiesMap.getProperty("Globals.Portal.Use");
		if ("Y".equals(portalUseYn)) {
			request.setAttribute("tabUrl", NkiaApplicationPropertiesMap.getProperty("Globals.Portal.Url"));
		}

		// 2014.10.20. 정정윤 추가 시작
		// 마지막 로그인 이력을 조회해서 이전 최신로그아웃 날짜가 없다면
		// 로그아웃 날짜를 새로 로그인한 날짜로 업데이트

		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		HttpSession session = request.getSession();
		PrivateKey privateKey = (PrivateKey) session.getAttribute("__rsaPrivateKey__");
		String securedPassWord = request.getParameter("pw");

		// 로그인 설정 불러오기 2018.03.31.정정윤
		List loginProperty = loginManagerService.searchLoginManagerList(null);

		String ip = req.getHeader("X-FORWARDED-FOR");
		String ip2 = "";
		if (ip == null) {
			ip = req.getRemoteAddr();
			ip2 = InetAddress.getLocalHost().getHostAddress();
		}
		// IP로컬IP로 변경
		// ip = getCurrentEnvironmentNetworkIp();
		// for (Enumeration<NetworkInterface> en =
		// NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
		// {
		// NetworkInterface intf = en.nextElement();
		// for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses();
		// enumIpAddr.hasMoreElements();)
		// {
		// InetAddress inetAddress = enumIpAddr.nextElement();
		// if (!inetAddress.isLoopbackAddress() &&
		// !inetAddress.isLinkLocalAddress() &&
		// inetAddress.isSiteLocalAddress())
		// {
		// ip = inetAddress.getHostAddress().toString();
		// }
		// }
		// }
		//
		try {

			String userID = userVO.getUser_id();
			UserVO resultVO = certificationService.login_outside(userVO);

			if (resultVO == null || resultVO.getUser_id() == null || "".equals(resultVO.getUser_id())) {
				model.put("message", LoginResultEnum.NOT_USER_ERR);
				return forwardPage;
			}
			userID = resultVO.getUser_id();
			userVO.setLast_login_ip(ip);
			userVO.setLast_login_dt(resultVO.getLast_login_dt());

			ModelMap userMap = new ModelMap();
			userMap.put("ip", ip);
			userMap.put("user_id", userID);

			model.put("user_id", userID);

			if (userID != null && !userID.equals("")) {
				int loginInfoCnt = certificationService.selectLogOutDtCount(userMap);
				if (loginInfoCnt < 1) {
					String logOutDt = certificationService.selectLogOutDt(userMap);
					if (logOutDt == null || logOutDt.equals("")) {
						updateLoginInfo(userID);
					}
				}
			}

			// 2014.10.20. 정정윤 추가 끝

			// 2015.04.29 김도원 이하 로직 소스 수정 START
			// 유저확인 / 메뉴확인 / 패스워드 체크
			LoginResultEnum result = certificationService.checkLoginInfo(privateKey, securedPassWord, resultVO,model, loginProperty);
			// LoginResultEnum result = LoginResultEnum.NO_ERR; //로그인이 무조건 성공함
			// LoginResultEnum result = LoginResultEnum.PASSWORD_ERR; //로그인이 무조건
			// 성공함

			if (result == LoginResultEnum.NO_ERR) {
				createSessionAndAuth(resultVO);
				// 세션에 정보 셋팅
				request.getSession().removeAttribute("userVO");
				request.getSession().setAttribute("userVO", resultVO);

				// Session에 Locale 설정 ( 참조되는 Resource Locale이 달라진다. )
				if (userVO.getLocale() != null && userVO.getLocale().length() > 0) {
					request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,StringUtils.parseLocaleString(userVO.getLocale()));
				}

				if (loginProperty != null && loginProperty.size() > 0) {
					HashMap loginMngMap = (HashMap) loginProperty.get(0);
					String multiLoginYn = StringUtil.parseString(loginMngMap.get("MULTI_LOGIN_YN"));
					// 중복로그인 허용 불가일 경우
					if ("N".equals(multiLoginYn)) {
						// 중복로그인에 대한 처리 실행
						this.doMultiLoginProcess(session, userID, ip);
					}
				}

				// 최종 접속일을 등록합니다.
				certificationService.updateLastLoginUpdt(userVO);
				// 로그인 실패 횟수 0으로 초기화
				certificationService.updateResetUserLoginFailCount(userVO.getUser_id());
			}

			inputLoginLogData(ip, userVO, result.getMessage(),CertificationConstant.ACCESS_COMMON);
			model.put("message", result.getMessage());
			// 2015.04.29 김도원 이하 로직 소스 수정 END
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			session.removeAttribute("__rsaPrivateKey__"); // 키의 재사용을 막는다. 항상 새로운
															// 키를 받도록 강제.
		}

		return forwardPage;
	}

	/**
	 * SSO를 이용한 로그인
	 * 
	 * @param userVO
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws NkiaException
	 * @throws ParseException 
	 * @throws IOException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
	@RequestMapping(value = "/loginSSO.do")
	public String loginSSO(@ModelAttribute("thrForm") UserVO userVO,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws NkiaException, ParseException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {

		/* SAMPLE
		String param = request.getParameter("v");
		String a = CipherDES.decode(param);
		*/
		JSONObject jsonObject = JsonUtil.readDataToJsonObject("{login_id:,timestamp:}");
		
		// 페이지 이동
		String forwardPage = "/itg/certification/login_result.nvf";
		String ip = request.getHeader("X-FORWARDED-FOR");
		if (ip == null) {
			ip = request.getRemoteAddr();
		}
		String userID = jsonObject.getString("login_id");
		String timestamp = jsonObject.getString("timestamp");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Date dateTemp = sdf.parse(timestamp);
		long sysdate = System.currentTimeMillis();
		long ssodate = dateTemp.getTime();
		TimeZone zone = TimeZone.getDefault();
		int offset = zone.getOffset(ssodate);
		long ssoLocaldate = ssodate + offset;
		long resultdate = sysdate - ssoLocaldate;
		long mindate = resultdate / 60000;
		if (mindate > 10) {
			model.put("message", "MOINSSO_FAIL");
		} else {
			// ///////////////
			// String userID = userVO.getUser_id();
			userVO.setUser_id(userID);
			UserVO resultVO = certificationService.login_inside(userVO);
			createSessionAndAuth(resultVO);
			userVO.setLast_login_ip(ip);
			userVO.setLast_login_dt(resultVO.getLast_login_dt());
			String loginResult = LoginResultEnum.NO_ERR.toString();
			// Session 셋팅 및 권한 생성
			createSessionAndAuth(resultVO);
			// 세션에 정보 셋팅
			request.getSession().removeAttribute("userVO");
			request.getSession().setAttribute("userVO", resultVO);
			request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,StringUtils.parseLocaleString("ko_KR"));
			List loginProperty = loginManagerService.searchLoginManagerList(null);
			HttpSession session = request.getSession();
			if (loginProperty != null && loginProperty.size() > 0) {
				HashMap loginMngMap = (HashMap) loginProperty.get(0);
				String multiLoginYn = StringUtil.parseString(loginMngMap.get("MULTI_LOGIN_YN"));
				// 중복로그인 허용 불가일 경우
				if ("N".equals(multiLoginYn)) {
					// 중복로그인에 대한 처리 실행
					this.doMultiLoginProcess(session, userID, ip);
				}
			}
			// 최종 접속일을 등록합니다.
			certificationService.updateLastLoginUpdt(userVO);

			inputLoginLogData(ip, userVO, loginResult,CertificationConstant.ACCESS_SSO);
			model.put("message", loginResult);
		}
		// forward 페이지 이동
		return forwardPage;

	}

	/**
	 * 세션 및 권한 설정
	 * 
	 * @param userVO
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws NkiaException
	 */
	public void createSessionAndAuth(UserVO userVO) throws NkiaException {
		Map paramMap = new HashMap();
		paramMap.put("user_id", userVO.getUser_id());
		// 권한 리스트 불러오기
		List sysAuthList = systemService.searchUserSystemAuthList(paramMap);
		List processAuthList = processAuthService.searchUserProcessAuthList(paramMap);
		List grpAuthList = userGrpService.selectUserGrpAuthList(paramMap);
		userVO.setSysAuthList(sysAuthList);
		userVO.setProcessAuthList(processAuthList);
		userVO.setGrpAuthList(grpAuthList);
	}

	/**
	 * SSO 인증토큰 체크하고 원하는 URL로 보내준다.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/checkSSO.do")
	public String checkSSO(@ModelAttribute("thrForm") UserVO userVO,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws NkiaException {
		/*
		 * 포탈화면 연동으로 임시 소스 수정 String forwardPage =
		 * "/itg/certification/login_result.nvf";
		 * 
		 * String root = NkiaApplicationContext.getProjectPath(); String
		 * filePath = root +
		 * NkiaApplicationPropertiesMap.getProperty("Sso.ConfFile.Path"); SsoExt
		 * se = new SsoExt(filePath); Hashtable table =
		 * se.ssoVerifyToken(request);
		 * 
		 * String tabUrl = request.getParameter("tabUrl");
		 * 
		 * if(null != table){
		 * 
		 * // 에러코드를 이용하여 경고창을 띄워줍니다. int nRet = (Integer) table.get("nRet");
		 * if(0 > nRet){ model.put("nRet", nRet); }else{ String user_id =
		 * (String) table.get("UID");
		 * 
		 * // String user_id = "omsadmin"; // 테스트용 userVO.setUser_id(user_id);
		 * 
		 * //resultVO 새로 생성 UserVO resultVO = new UserVO(); // user_id를 이용하여 정보
		 * 조회 resultVO = certificationService.login(userVO);
		 * 
		 * createSessionAndAuth(resultVO);
		 * 
		 * // 넘겨줄 url을 가지고 옵니다. model.put("tabUrl", tabUrl);
		 * 
		 * // 서비스 포탈에서 보내는 추가 파라미터 createRequestMap(request, model);
		 * 
		 * String searchValueMap = request.getParameter("searchValueMap") !=
		 * null ? request.getParameter("searchValueMap") : ""; if(
		 * !searchValueMap.equals("") ){ model.put("searchValueMap",
		 * searchValueMap); }
		 * 
		 * String portalParam = request.getParameter("portal_param") != null ?
		 * request.getParameter("portal_param") : ""; if(
		 * !portalParam.equals("") ){ model.put("searchValueMap", "");
		 * model.put("portal_param", portalParam); } } }else{
		 * 
		 * model.put("message", "TOKEN_ERR"); }
		 * 
		 * return forwardPage;
		 */
		String forwardPage = "/itg/certification/login_result.nvf";
		String tabUrl = request.getParameter("tabUrl");
		int nRet = 1;
		model.put("nRet", nRet);
		String user_id = "omsadmin"; // 테스트용
		userVO.setUser_id(user_id);

		// resultVO 새로 생성
		UserVO resultVO = new UserVO();
		// user_id를 이용하여 정보 조회
		resultVO = certificationService.login(userVO);
		createSessionAndAuth(resultVO);

		// 넘겨줄 url을 가지고 옵니다.
		model.put("tabUrl", tabUrl);

		// 서비스 포탈에서 보내는 추가 파라미터
		createRequestMap(request, model);

		request.getSession().removeAttribute("userVO");
		request.getSession().setAttribute("userVO", resultVO);

		String searchValueMap = request.getParameter("searchValueMap") != null ? request.getParameter("searchValueMap") : "";
		if (searchValueMap == null) {
			searchValueMap = "";
		}

		if (!searchValueMap.equals("")) {
			model.put("searchValueMap", searchValueMap);
		}

		String portalParam = request.getParameter("portal_param") != null ? request.getParameter("portal_param") : "";
		if (!portalParam.equals("")) {
			model.put("searchValueMap", "");
			model.put("portal_param", portalParam);
		}

		return forwardPage;
	}

	/**
	 * 로그아웃 처리
	 * 
	 * @param vo
	 *            - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request
	 *            - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @throws AgentException 
	 * @exception Exception
	 */
	@RequestMapping(value = "/logout.do")
	public String logout(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws NkiaException, AgentException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
			UserVO user = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
			String user_id = user.getUser_id();
			ModelMap userMap = new ModelMap();
			userMap.put("user_id", user_id);
			List stateList = certificationService.checkUserState(userMap);
			if("Y".equals(((Map)stateList.get(0)).get("INSIDE_YN"))){
				AuthCheck auth = new AuthCheck(request, response);
				Utility.delCookie(request, response, auth.getSsoSite().getTokenConfigs().get(0).getName(), "/", request.getServerName());
			}
    	}
		String forwardPage = "redirect:/itg/certification/goDenialPage.do";
		return forwardPage;
	}

	@RequestMapping(value = "/goDenialPage.do")
	public String goDenialPage(HttpServletRequest request, ModelMap model)
			throws NkiaException {

		// 필터에서 로그아웃이 된 메시지를 추가하여 로그아웃시 출력할 수 있도록 구조 변경
		String msg = StringUtil.parseString(request.getAttribute("filter_message"));

		if ("".equals(msg)) {
			msg = StringUtil.parseString(request.getParameter("filter_message"));
		}

		if (!"".equals(msg)) {
			String alertMsg = "";
			if (msg.equals(LoginResultEnum.MULTI_LOGIN_ERR.getMessage())) {
				alertMsg = egovMessageSource.getMessage("msg.session.multi.login.error");
			} else if (msg.equals(BaseEnum.SESSION_EXPIRED_STATUS.getString())) {
				alertMsg = egovMessageSource.getMessage("msg.session.expired");
			} else {
				alertMsg = msg;
			}
			model.put("message_key", msg);
			model.put("message", alertMsg);
		}

		String forwardPage = "/itg/certification/denial.nvf";
		return forwardPage;
	}

	/**
	 * 
	 * 인증처리 로그인
	 * 
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/index.do")
	public String goIndexPage(HttpServletRequest request, ModelMap model)
			throws NkiaException {
		String forwarPage = "/index.nvf";
		try {
			// 로그인실패시 리턴 URL
			model.put("returnUrl", NkiaApplicationPropertiesMap.getProperty("Globals.Login.Fail.Url"));
			model.put("twoFactor", NkiaApplicationPropertiesMap.getProperty("Globals.Login.TwoFactor"));
			model.put("sessionTime", NkiaApplicationPropertiesMap.getProperty("Globals.Login.sessionTime"));
			model.put("sessionTimeMessage", NkiaApplicationPropertiesMap.getProperty("Globals.Login.sessionTimeMessage"));
			String pcmobile = "";
			HashMap searchConditionMap = new HashMap();
			Map paramMap = request.getParameterMap();
			if (paramMap != null) {
				Iterator keySet = paramMap.keySet().iterator();
				while (keySet.hasNext()) {
					String paramName = (String) keySet.next();
					String paramValue = request.getParameter(paramName);
					if ("tabUrl".equals(paramName) && (paramValue != null && !"".equals(paramValue))) {
						model.put(paramName, paramValue);
						Map menuInfoMap = menuService.selectTabMenuInfo(paramValue);
						if (menuInfoMap != null) {
							Iterator menuInfoKeySet = menuInfoMap.keySet().iterator();
							while (menuInfoKeySet.hasNext()) {
								String menuInfoKey = (String) menuInfoKeySet.next();
								searchConditionMap.put(menuInfoKey,(String) menuInfoMap.get(menuInfoKey));
							}
						}
					} else if ("portal_param".equals(paramName)&& (paramValue != null && !"".equals(paramValue))) {
						model.put(paramName, paramValue);
					} else if ("pcMobile".equals(paramName) && (paramValue != null && !"".equals(paramValue))) {
						pcmobile = paramValue;
					} else {
						searchConditionMap.put(paramName, paramValue);
					}
				}
				model.put("searchConditionMap", searchConditionMap);
			}

			// 1. Spring Security 사용자권한 처리
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if (!isAuthenticated) {
				// model.addAttribute("message", "비밀번호가 다릅니다.");
				// return "redirect:/itg/certification/goLoginPage.do";
				return "redirect:/itg/certification/goDenialPage.do";
			}
			// 1. 메뉴조회
			UserVO user = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();

			Map param = new HashMap();
			param.put("user_id", user.getUser_id());
			param.put("session_locale", sessionService.getSessionLocale());
			param.put("pcmobile", pcmobile);
			Map resultMap = menuService.searchTopMenuForUser(param);
			List topMenuList = (List) resultMap.get("topMenuList");
			List subMenuList = (List) resultMap.get("subMenuList");

			// @@정중훈20131209 메뉴리스트가 존재하지 않을 경우 경고창 띄우기 시작
			// login_result.nvf페이지로 보내서 return
			model.put("user_id", user.getUser_id());
			if (0 == topMenuList.size()) {
				model.put("message", LoginResultEnum.TOPMENU_NO_MAPPING_ERR);
				forwarPage = "/itg/certification/login_result.nvf";
				// model.put("message", "topNoMapping");
			} else {
				if (0 == subMenuList.size()) {
					model.put("message", LoginResultEnum.SUBMENU_NO_MAPPING_ERR);
					forwarPage = "/itg/certification/login_result.nvf";
					// model.put("message", "subNoMapping");
				} else {
					// 2. 사용자권한 처리
					model.put("topMenuList", topMenuList);
					model.put("subMenuList", subMenuList);
					model.put("defaultTopMenuId",((TreeVO) topMenuList.get(0)).getNode_id());
					model.put("last_login_ip", user.getLast_login_ip());
					model.put("last_login_dt", user.getLast_login_dt());
					model.put("session_locale", sessionService.getSessionLocale());
				}
			}
			// @@정중훈20131209 메뉴리스트가 존재하지 않을 경우 경고창 띄우기 종료

			// 알림함 사용여부
			String notificationUseYN = NkiaApplicationPropertiesMap.getProperty("Globals.Notification.UseYN");
			if (notificationUseYN != null && "Y".equals(notificationUseYN)) {
				model.put("notificationUseYN", "Y");
			} else {
				model.put("notificationUseYN", "N");
			}

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}

	/**
	 * 
	 * 인증처리 로그인
	 * 
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/indexSSO.do")
	public String goIndexPageSSO(HttpServletRequest request, ModelMap model,
			HttpServletResponse response) throws NkiaException {
		String forwarPage = "/index.nvf";
		// /최초 진입시 SSO체크
		String idTagName, pwdTagName, credTagName, returnUrlTagName, siteTagName;

		String ssoLogonUrl; // 로그온 URL
		String ssoCheckUrl; // 인증체크 URL
		String returnUrl; // 되돌아올 URL

		String errorCode; // 에러코드
		String errorMessage = ""; // 에러 메시지
		String policyVersion; // 정책 버전
		String siteID; // 사이트 식별자

		String serverName;
		SSOProvider ssoProvider;
		SSOSite ssoSite;

		// 인증객체생성 및 인증확인
		try {
			// 인증 객체 선언(Request와 Response 인계)
			UserVO userVO = new UserVO();
			UserVO resultVO = new UserVO();
			String user_id = "";
			Map checkMap = request.getParameterMap();
			
			AuthCheck auth = new AuthCheck(request, response);

			serverName = request.getServerName();
			ssoProvider = SSOConfig.getInstance().getCurrentSSOProvider(serverName);
			ssoSite = ssoProvider.getCurrentSSOSite(serverName);

			// 정책 변경 체크 ( 코드 제거하면 안됨 )
			// SSO 서버에서 정책을 새로 배포했을 경우 변경을 감지하여 정책을 다시 받아오도록 하는 코드
			policyVersion = Utility.getRequestValue(request, "policyVersion",Utility.EMPTY_STRING);
			if (!Utility.isNullOrEmpty(policyVersion) && Integer.parseInt(policyVersion) > ssoProvider.getPolicyVer()) {
				SSOConfig.getInstance().reLoad();
				ssoProvider = SSOConfig.getInstance().getCurrentSSOProvider(serverName);
				ssoSite = ssoProvider.getCurrentSSOSite(serverName);
			}

			// 일반 설정값들
			idTagName = ssoProvider.getParamName(AuthUtil.ParamInfo.USER_ID);
			pwdTagName = ssoProvider.getParamName(AuthUtil.ParamInfo.USER_PW);
			credTagName = ssoProvider.getParamName(AuthUtil.ParamInfo.CRED_TYPE);
			returnUrlTagName = ssoProvider.getParamName(AuthUtil.ParamInfo.RETURN_URL);
			siteTagName = ssoProvider.getParamName(AuthUtil.ParamInfo.SITE_ID);

			// 인증 체크(인증 상태 값 리턴)
			AuthStatus status = auth.checkLogon();
			// 인증 체크 후 상세 에러코드 조회
			errorCode = String.valueOf(auth.getErrCode());
			// 인증상태별 처리u
			if (status == AuthStatus.SSOSuccess) {
				// ---------------------------------------------------------------------
				// 인증 상태: 인증 성공
				// - 인증 토큰(쿠키) 존재하고, 토큰 형식에 맞고, SSO 정책 체크 결과 유효함.
				// ---------------------------------------------------------------------
				// 로그온 UI 페이지가 아닌 업무 페이지로 이동 시킴
				// response.sendRedirect("default.jsp");
				//auth.checkHijacking();
				forwarPage = "/itg/certification/login_result.nvf";
				user_id = auth.getUserID();
				// user_id = "lotte1002";
				userVO.setUser_id(user_id);

				resultVO = certificationService.login_inside(userVO);
				createSessionAndAuth(resultVO);

				String ip = request.getHeader("X-FORWARDED-FOR");
				if (ip == null) {
					ip = request.getRemoteAddr();
				}
				String loginResult = LoginResultEnum.NO_ERR.toString();
				// Session 셋팅 및 권한 생성
				// 세션에 정보 셋팅
				userVO.setLast_login_ip(ip);
				userVO.setLast_login_dt(resultVO.getLast_login_dt());
				request.getSession().removeAttribute("userVO");
				request.getSession().setAttribute("userVO", resultVO);
				// 최초로그인시 한국어를 기본셋팅
				request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,StringUtils.parseLocaleString("ko_KR"));

				// 최종 접속일을 등록합니다.
				certificationService.updateLastLoginUpdt(userVO);
				List loginProperty = loginManagerService.searchLoginManagerList(null);
				HttpSession session = request.getSession();
				if (loginProperty != null && loginProperty.size() > 0) {
					HashMap loginMngMap = (HashMap) loginProperty.get(0);
					String multiLoginYn = StringUtil.parseString(loginMngMap.get("MULTI_LOGIN_YN"));
					// 중복로그인 허용 불가일 경우
					if ("N".equals(multiLoginYn)) {
						// 중복로그인에 대한 처리 실행
						this.doMultiLoginProcess(session, user_id, ip);
					}
				}

				inputLoginLogData(ip, userVO, loginResult,CertificationConstant.ACCESS_SSO);
				model.put("message", loginResult);
				return forwarPage;
			 } else if (status == AuthStatus.SSOFirstAccess) {
		            // ---------------------------------------------------------------------
		            // 인증 상태: 최초 접근
		            // - 인증 토큰(쿠키)가 존재하지 않음.
		            // ---------------------------------------------------------------------

		            // 인증 확인을 위해 페이지 이동

		            // ThisURL을 이용하여 현재 페이지로 다시 돌아오도록 함.
		            // 직접 되돌아 올 URL을 직접 입력하여 호출 할 수도 있음
				 	return "redirect:/itg/certification/goDenialPage.do";
				 
				 
				 	//auth.trySSO();
		            //return "redirect:/itg/ssoagent/logonService.jsp";

		   } else {
				// ---------------------------------------------------------------------
				// 인증 상태 : 인증 실패 또는 로그오프 상태
				// - 인증 오류 발생 또는 로그온 하지 않은 로그오프 상태
				// ---------------------------------------------------------------------

				// SSO 장애 시 정책에 따라 자체 로그인 페이지로 이동 시키거나, SSO 인증을 위한 포탈 로그인 페이지로
				// 이동
				// errorMessage =
				// "SSO service is not available now. Please try again in a few minutes.";
				return "redirect:/itg/certification/goDenialPage.do";
			}
		} catch (AgentException e) {
			return "redirect:/itg/certification/goDenialPage.do";
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	}

	/**
	 * 권한 리스트 불러오기
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/searchAuthList.do")
	public @ResponseBody ResultVO searchAuthList(ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO user = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();

			paramMap.put("user_id", user.getUser_id());

			HashMap resultMap = new HashMap();
			List sysAuthList = systemService.searchUserSystemAuthList(paramMap);
			List processAuthList = processAuthService.searchUserProcessAuthList(paramMap);
			resultMap.put("sysAuthList", sysAuthList);
			resultMap.put("processAuthList", processAuthList);

			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 로그인한 유저 정보 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/searchUserInfo.do")
	public @ResponseBody ResultVO searchUserInfo(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
			String userID = userVO.getUser_id();

			paramMap.put("userId", userID);

			HashMap resultMap = certificationService.selectUserInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 개인정보 수정 팝업
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/updateUserInfo.do")
	public @ResponseBody ResultVO updateUserInfo(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			HashMap<String, String> resultMap = certificationService.updateUserInfo(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 세션 인증키를 가지고 옵니다.
	 * 
	 * @param userVO
	 * @param request
	 * @param response
	 * @param model
	 * @throws NkiaException
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	public void generatorKey(HttpServletRequest request) throws NkiaException, NoSuchAlgorithmException, InvalidKeySpecException {
		// 세션 인증키 받는 부분 시작
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(KEY_SIZE);

		KeyPair keyPair = generator.genKeyPair();
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");

		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();

		HttpSession session = request.getSession();
		// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
		session.setAttribute("__rsaPrivateKey__", privateKey);

		// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
		RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(
				publicKey, RSAPublicKeySpec.class);

		String publicKeyModulus = publicSpec.getModulus().toString(16);
		String publicKeyExponent = publicSpec.getPublicExponent().toString(16);

		request.setAttribute("publicKeyModulus", publicKeyModulus);
		request.setAttribute("publicKeyExponent", publicKeyExponent);

	}

	/**
	 * 사용자 패스워드 수정 팝업 확인용
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/checkPwUpdateRequestDate.do")
	public @ResponseBody ResultVO checkPwUpdateRequestDate(
			@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			// 파라미터 맵에 기한설정 셋팅
			paramMap.put("PW_MAX_DAY", NkiaApplicationPropertiesMap
					.getProperty("Globals.Password.MaxDay"));

			UserVO user = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
			String userID = user.getUser_id();
			paramMap.put("user_id", userID);

			HashMap resultMap = certificationService
					.selectPwUpdateUserInfo(paramMap);
			boolean result = certificationService
					.checkPwUpdateRequestDate(paramMap);

			resultVO.setResultMap(resultMap);
			resultVO.setResultBoolean(result);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 사용자 패스워드 수정 팝업 확인용
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/checkUserState.do")
	public @ResponseBody ResultVO checkUserState(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			// 파라미터 맵에 기한설정 셋팅
			String userID = (String) paramMap.get("user_id");
			if ("".equals(userID)) {
				UserVO user = (UserVO) EgovUserDetailsHelper
						.getAuthenticatedUser();
				userID = user.getUser_id();
			}
			paramMap.put("user_id", userID);
			GridVO gridVO = new GridVO();
			List resultList = certificationService.checkUserState(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 수정팝업 업데이트 팝업 하루동안 숨기기 업데이트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/updateOneDayPwPopupShow.do")
	public @ResponseBody ResultVO updateOneDayPwPopupShow(
			@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			certificationService.updateOneDayPwPopupShow(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 입력받은 비밀번호를 개인정보 및 패스워드 이력관리에 업데이트 합니다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/updatePwPopupChange.do")
	public @ResponseBody ResultVO updatePwPopupChange(
			@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			resultVO = certificationService.updatePwPopupChange(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 암호화된 암호 알아내기 위함
	 * 
	 * @throws NkiaException
	 * @RequestMapping(value="/pwTest.do") public void pwTest() throws NkiaException
	 *                                     { UserVO userVO = new UserVO();
	 *                                     userVO.setUser_id("role2"); try {
	 *                                     UserVO testVO = new UserVO(); //DB에
	 *                                     있는 user정보를 들고옵니다. testVO =
	 *                                     certificationService.login(userVO);
	 *                                     String pass = testVO.getPw(); String
	 *                                     decrypted_passwd =
	 *                                     ACipher.decrypt(ACipher.CIPHER_SEED,
	 *                                     pass); String encrypted_passwd =
	 *                                     ACipher.encrypt(ACipher.CIPHER_SEED,
	 *                                     decrypted_passwd); } catch (Exception
	 *                                     e) { throw new NkiaException(e); }
	 * 
	 *                                     }
	 */

	/**
	 * 통계페이지에서 현황 조회시 검색영역에 입력값 저장을 위한 맵을 만드는 부분
	 * 
	 * @param request
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void createRequestMap(HttpServletRequest request, ModelMap paramMap)
			throws NkiaException {

		HashMap searchValueMap = new HashMap();
		Map tempMap = request.getParameterMap();

		if (tempMap != null) {
			Iterator keySet = tempMap.keySet().iterator();
			while (keySet.hasNext()) {
				String paramName = (String) keySet.next();
				if (paramName.equals("tabUrl")) {
					continue;
				}
				String paramValue = request.getParameter(paramName);

				searchValueMap.put(paramName, paramValue);
			}
			paramMap.put("searchValueMap", searchValueMap);
		}
	}

	/**
	 * dashBoard 팀에서 사용할 다이렉트 상세보기 정보 탭
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/directUrlForward.do")
	public String directUrlForward(HttpServletRequest request, ModelMap paramMap)
			throws NkiaException {
		// 임의의 세션 생성
		UserVO resultVO = new UserVO();
		resultVO.setUser_id("dash_ali");
		request.getSession().setAttribute("userVO", resultVO);

		String sr_id = request.getParameter("sr_id");
		String gubn = request.getParameter("gubn");
		paramMap.put("sr_id", sr_id);
		paramMap.put("gubn", gubn);

		return "/itg/certification/direct_result.nvf";
	}
	
	/**
	 * 로그인 접속정보Insert
	 * 
	 * @param paramMap
	 * @return
	 * @return
	 * @throws NkiaException
	 */
	public void inputLoginLogData(String ip, UserVO userVO, String resultMsg,
			String LoginType) throws NkiaException {
		// CLIENT IP 조회
		HashMap<String, String> insertMap = new HashMap<String, String>();
		insertMap.put("client_ip", ip);
		insertMap.put("login_user_id", userVO.getUser_id());
		insertMap.put("message", resultMsg);
		insertMap.put("login_type", LoginType); // 접속방법(00001 = 일반접속, 00002 =
												// 인증서 접속)
		insertMap.put("login_location", "0002"); // 운영자동화 로그인시 0002

		// 접속정보ID 조회 및 접속정보 INSERT
		certificationService.insertLoginInfo(insertMap);
	}

	/**
	 * 로그인 접속정보 로그아웃 시간입력
	 * 
	 * @param paramMap
	 * @return
	 * @return
	 * @throws NkiaException
	 */
	public void updateLoginInfo(String userId) throws NkiaException {
		certificationService.updateLoginInfo(userId);
	}

	/**
	 * 로그인 잠금해제 Row Insert
	 * 
	 * @param paramMap
	 * @return
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/insertUnLockRow.do")
	public @ResponseBody ResultVO insertUnLockRow(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			certificationService.insertUnLockRow(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 이전 비밀번호와 일치하는지 여부
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/searchPreUserPW.do")
	public @ResponseBody ResultVO searchPreUserPW(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			String curPw = (String) paramMap.get("cur_pw");

			HashMap resultMap = certificationService.selectUserInfo(paramMap);
			String dbPw = (String) resultMap.get("PWCHECK");

			if (curPw.equals(dbPw)) {
				resultMap.put("PWFLAG", "false");
			} else {
				resultMap.put("PWFLAG", "true");
			}
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 사용자 비밀번호 초기화
	 * 
	 * @param paramMap
	 * @return
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/updatePwInit.do")
	public @ResponseBody ResultVO updatePwInit(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			certificationService.updatePwInit(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * .
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/goLoginResultPage.do")
	public String goLoginResultPage(@ModelAttribute("thrForm") UserVO userVO,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws NkiaException {
		String forwardPage = "/itg/certification/login_result.nvf";
		String tabUrl = request.getParameter("tabUrl");

		// 넘겨줄 url을 가지고 옵니다.
		model.put("tabUrl", tabUrl);
		// 서비스 포탈에서 보내는 추가 파라미터
		createRequestMap(request, model);

		String searchValueMap = request.getParameter("searchValueMap") != null ? request
				.getParameter("searchValueMap") : "";
		if (searchValueMap == null) {
			searchValueMap = "";
		}

		if (!searchValueMap.equals("")) {
			model.put("searchValueMap", searchValueMap);
		}
		String portalParam = request.getParameter("portal_param") != null ? request
				.getParameter("portal_param") : "";
		if (!portalParam.equals("")) {
			model.put("searchValueMap", "");
			model.put("portal_param", portalParam);
		}
		return forwardPage;
	}

	/**
	 * 중복로그인 처리 함수
	 * 
	 * @param session
	 * @param userID
	 * @param ip
	 * @throws NkiaException
	 */
	private void doMultiLoginProcess(HttpSession session, String userId,
			String ip) throws NkiaException {

		String sessionId = session.getId();

		HashMap mySessionMap = new HashMap();
		mySessionMap.put("USER_ID", userId);
		mySessionMap.put("SESSION_ID", sessionId);
		mySessionMap.put("LOGIN_IP", ip);

		// 현재 접속자의 세션 조회
		int mySessionCnt = loginManagerService
				.searchLoginSessionListCount(mySessionMap);

		// 현재 접속자의 세션이 없으면, 접속자의 세션을 DB에 추가
		if (mySessionCnt < 1) {
			loginManagerService.insertLoginSession(mySessionMap);
		}

		// 현재 접속자의 세션을 제외하고 동일한 USER_ID를 사용중인 세션들의 상태값을 전부 'EXPIRED' 로 업데이트
		mySessionMap.put("STATUS", BaseEnum.SESSION_EXPIRED_STATUS.getString());
		loginManagerService.updateStatusLoginSession(mySessionMap);

	}

	/**
	 * 비밀번호초기화로직
	 * 
	 * @param vo
	 *            - 아이디
	 * @param request
	 *            - 세션처리를 위한 HttpServletRequest
	 * @return result - 초기화성공여부
	 * @exception Exception
	 */
	@RequestMapping(value = "/passWordReset.do")
	public @ResponseBody ResultVO passWordReset(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();

//		HashMap resultMap = certificationService.selectUserInfo(paramMap);
//
//		if (resultMap != null) {
//			char pwCollection[] = new char[] { '1', '2', '3', '4', '5', '6',
//					'7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
//					'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
//					'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
//					'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
//					's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '!', '@', '#', '$',
//					'%', '^', '&', '*', '(', ')' };// 배열에 선언
//
//			String ranPw = "";
//			for (int i = 0; i < 10; i++) {
//				int selectRandomPw = (int) (Math.random() * (pwCollection.length));// Math.rondom()은
//																					// 0.0이상
//																					// 1.0미만의
//																					// 난수를
//																					// 생성해
//																					// 준다.
//				ranPw += pwCollection[selectRandomPw];
//			}
//			paramMap.put("initPw", ranPw);
//			paramMap.put("user_id", paramMap.get("userId"));
//
//			certificationService.updatePwInit(paramMap);
//			Map mailInfoMap = new HashMap();
//			mailInfoMap.put("KEY", "PWRESET");
//			mailInfoMap.put("TEMPLATE_ID", "PWRESET_NOTI_MAIL");
//			mailInfoMap.put("USER_ID", (String) paramMap.get("userId"));
//			mailInfoMap.put("PW", ranPw);
//
//			EmailSetData emailSender = new EmailSetData(mailInfoMap);
//			emailSender.sendMail();
//
//			resultVO.setResultString("ITSM에 등록된 메일로 비밀번호가 발송완료되었습니다.");
//			resultVO.setSuccess(true);
//			resultVO.setResultMsg("1");
//		} else {
//			resultVO.setSuccess(true);
//			resultVO.setResultString("유효하지 않은 ID입니다.");
//			resultVO.setResultMsg("0");
//		}
		return resultVO;
	}

	public static String getCurrentEnvironmentNetworkIp() {

		Enumeration netInterfaces = null;
		try {
			netInterfaces = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			return getLocalIp();
		}

		while (netInterfaces.hasMoreElements()) {
			NetworkInterface ni = (NetworkInterface) netInterfaces
					.nextElement();
			Enumeration address = ni.getInetAddresses();
			if (address == null) {
				return getLocalIp();
			}
			while (address.hasMoreElements()) {
				InetAddress addr = (InetAddress) address.nextElement();
				if (!addr.isLoopbackAddress() && !addr.isSiteLocalAddress()
						&& !addr.isAnyLocalAddress()) {
					String ip = addr.getHostAddress();
					if (ip.indexOf(".") != -1 && ip.indexOf(":") == -1) {
						return ip;
					}
				}
			}
		}
		return getLocalIp();
	}

	public static String getLocalIp() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			return null;
		}
	}
	
	/**
	 * 입력받은 2FW인증번호를 검증
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/inspectionTF.do")
	public @ResponseBody ResultVO inspectionTF(
			@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			resultVO = certificationService.inspectionTF(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
