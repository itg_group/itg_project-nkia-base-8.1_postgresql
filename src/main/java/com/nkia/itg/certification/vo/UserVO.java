/*
 * @(#)UserVO.java              2013. 4. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.certification.vo;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.nkia.itg.base.application.util.common.StringUtil;

public class UserVO {
	private String user_id;
	private String user_nm;
	private String cust_id;
	private String cust_nm;
	private String pw;
	private String cust_op_type;
	private String tel_no;
	private String hp_no;
	private String email;
	private String position;
	private String absent_id;
	private String sid;
	private String emp_id;
	private String fax_no;
	private String msg_id;
	private String esign_id;
	private String home_tel_no;
	private String tel_ext;
	private String rel_path;
	private String use_yn;
	private String upd_user_id;
	private String upd_id;
	private String req_zone_id;
	private String req_zone_nm;
	private String customer_id;
	private String customer_nm;
	private String inside_yn;
	private String alarm_yn;
	

	// 암호화된 ID 및 비밀번호
	private String securedUserId;
	private String securedPassWord;
	
	//최종접속일 
	private String lastLoginDt;
	//휴면계정상태
	private String dormantYn;
	//휴면변경ID
	private String dormantUserID;
	
	public List sysAuthList;
	public List processAuthList;
	public List grpAuthList;
	
	// 보안 Salt
	private String scrty_salt;

	//부서(계열사) 세부분류 
	private String cust_class;
	
	private String last_login_ip;
	private String last_login_dt;
	
	private String locale;
	
	private String auth_list; // *ITAM 정기점검*
	
	public String getAlarm_yn() {
		return alarm_yn;
	}
	public void setAlarm_yn(String alarm_yn) {
		this.alarm_yn = alarm_yn;
	}
	public String getScrty_salt() {
		return scrty_salt;
	}
	public void setScrty_salt(String scrty_salt) {
		this.scrty_salt = scrty_salt;
	}
	public String getReq_zone_id() {
		return req_zone_id;
	}
	public void setReq_zone_id(String req_zone_id) {
		this.req_zone_id = req_zone_id;
	}
	public String getReq_zone_nm() {
		return req_zone_nm;
	}
	public void setReq_zone_nm(String req_zone_nm) {
		this.req_zone_nm = req_zone_nm;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_nm() {
		return user_nm;
	}
	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getCust_op_type() {
		return cust_op_type;
	}
	public void setCust_op_type(String cust_op_type) {
		this.cust_op_type = cust_op_type;
	}
	public String getTel_no() {
		return tel_no;
	}
	public void setTel_no(String tel_no) {
		this.tel_no = tel_no;
	}
	public String getHp_no() {
		return hp_no;
	}
	public void setHp_no(String hp_no) {
		this.hp_no = hp_no;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getAbsent_id() {
		return absent_id;
	}
	public void setAbsent_id(String absent_id) {
		this.absent_id = absent_id;
	}
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	public String getFax_no() {
		return fax_no;
	}
	public void setFax_no(String fax_no) {
		this.fax_no = fax_no;
	}
	public String getMsg_id() {
		return msg_id;
	}
	public void setMsg_id(String msg_id) {
		this.msg_id = msg_id;
	}
	public String getEsign_id() {
		return esign_id;
	}
	public void setEsign_id(String esign_id) {
		this.esign_id = esign_id;
	}
	public String getHome_tel_no() {
		return home_tel_no;
	}
	public void setHome_tel_no(String home_tel_no) {
		this.home_tel_no = home_tel_no;
	}
	public String getTel_ext() {
		return tel_ext;
	}
	public void setTel_ext(String tel_ext) {
		this.tel_ext = tel_ext;
	}
	public String getRel_path() {
		return rel_path;
	}
	public void setRel_path(String rel_path) {
		this.rel_path = rel_path;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getUpd_user_id() {
		return upd_user_id;
	}
	public void setUpd_user_id(String upd_user_id) {
		this.upd_user_id = upd_user_id;
	}
	public String getUpd_id() {
		return upd_id;
	}
	public void setUpd_id(String upd_id) {
		this.upd_id = upd_id;
	}
	public String getCust_nm() {
		return cust_nm;
	}
	public void setCust_nm(String cust_nm) {
		this.cust_nm = cust_nm;
	}
	public String getDetailInfo() {
		//롯데UBIT 요청자 디테일정보 수정
		//return getCust_nm()+"(tel:"+StringUtil.replaceNullToNA(getTel_no())+" - email:"+StringUtil.replaceNullToNA(getEmail())+")";
		return getCust_nm()+"(HP:"+StringUtil.replaceNullToNA(getHp_no())+")";
	}
	public List getSysAuthList() {
		return sysAuthList;
	}
	public void setSysAuthList(List sysAuthList) {
		this.sysAuthList = sysAuthList;
	}
	public List getProcessAuthList() {
		return processAuthList;
	}
	public void setProcessAuthList(List processAuthList) {
		this.processAuthList = processAuthList;
	}
	
	public String getSecuredUserId() {
		return securedUserId;
	}
	public void setSecuredUserId(String securedUserId) {
		this.securedUserId = securedUserId;
	}
	public String getSecuredPassWord() {
		return securedPassWord;
	}
	public void setSecuredPassWord(String securedPassWord) {
		this.securedPassWord = securedPassWord;
	}
	public List getGrpAuthList() {
		return grpAuthList;
	}
	public void setGrpAuthList(List grpAuthList) {
		this.grpAuthList = grpAuthList;
	}
	public String getDormantYn() {
		return dormantYn;
	}
	public void setDormantYn(String dormantYn) {
		this.dormantYn = dormantYn;
	}
	
	public String getCust_class() {
		return cust_class;
	}
	public void setCust_class(String cust_class) {
		this.cust_class = cust_class;
	}
	
	public String getLast_login_ip() {
		return last_login_ip;
	}
	public void setLast_login_ip(String last_login_ip) {
		this.last_login_ip = last_login_ip;
	}
	public String getLast_login_dt() {
		return last_login_dt;
	}
	public void setLast_login_dt(String last_login_dt) {
		this.last_login_dt = last_login_dt;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_nm() {
		return customer_nm;
	}
	public void setCustomer_nm(String customer_nm) {
		this.customer_nm = customer_nm;
	}
	public String getInside_yn() {
		return inside_yn;
	}
	public void setInside_yn(String inside_yn) {
		this.inside_yn = inside_yn;
	}
	/** toString 생성 부분 */
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}
	public String getLastLoginDt() {
		return lastLoginDt;
	}
	public void setLastLoginDt(String lastLoginDt) {
		this.lastLoginDt = lastLoginDt;
	}
	public String getDormantUserID() {
		return dormantUserID;
	}
	public void setDormantUserID(String dormantUserID) {
		this.dormantUserID = dormantUserID;
	}

	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	// *ITAM 정기점검*
	public String getAuth_list() {
		return auth_list;
	}
	// *ITAM 정기점검*
	public void setAuth_list(String auth_list) {
		this.auth_list = auth_list;
	}

	
}
