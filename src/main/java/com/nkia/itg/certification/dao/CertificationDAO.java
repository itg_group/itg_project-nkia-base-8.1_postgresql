/*
 * @(#)CertificationDAO.java              2013. 4. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.certification.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("certificationDAO")
public class CertificationDAO extends EgovComAbstractDAO {

	public UserVO selectLoginInfo(UserVO userVO)throws NkiaException {
		return (UserVO) selectByPk("CertificationDAO.selectLoginInfo", userVO);
	}
	
	public UserVO selectLoginInfo_inside(UserVO userVO)throws NkiaException {
		return (UserVO) selectByPk("CertificationDAO.selectLoginInfo_inside", userVO);
	}
	
	public UserVO selectLoginInfo_outside(UserVO userVO)throws NkiaException {
		return (UserVO) selectByPk("CertificationDAO.selectLoginInfo_outside", userVO);
	}

	public HashMap selectUserInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap) selectByPk("CertificationDAO.selectUserInfo", paramMap);
	}

	public int selectUserPwHistoryCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("CertificationDAO.selectUserPwHistoryCount", paramMap);
	}

	public void insertUserPwHistory(ModelMap paramMap) throws NkiaException{
		insert("CertificationDAO.insertUserPwHistory", paramMap);
	}

	public int selectLoginUserCount(UserVO userVO) throws NkiaException{
		return (Integer) selectByPk("CertificationDAO.selectLoginUserCount", userVO);
	}

	public int selectUserGrpCount(UserVO userVO) {
		return (Integer) selectByPk("CertificationDAO.selectUserGrpCount", userVO);
	}
	
	public List selectUserGrp(String userID) {
		return list("CertificationDAO.selectUserGrp", userID);
	}

	public String selectMaxDate(ModelMap paramMap) throws NkiaException {
		return (String) selectByPk("CertificationDAO.selectMaxDate", paramMap);
	}
	
	public List checkUserState(ModelMap paramMap) throws NkiaException {
		return list("CertificationDAO.checkUserState", paramMap);
	}
	
	public HashMap selectPwUpdateUserInfo(ModelMap paramMap) throws NkiaException {
		
		return (HashMap) selectByPk("CertificationDAO.selectPwUpdateUserInfo", paramMap);
	}
	

	public String selectHideDate(ModelMap paramMap) throws NkiaException {

		return (String) selectByPk("CertificationDAO.selectHideDate", paramMap);
	}
	
	public String selectNextPwUpdateDate(ModelMap paramMap) throws NkiaException {

		return (String) selectByPk("CertificationDAO.selectNextPwUpdateDate", paramMap);
	}

	public void updateOneDayPwPopupShow(ModelMap paramMap) throws NkiaException {

		update("CertificationDAO.updateOneDayPwPopupShow", paramMap);
	}

	public void updatePopupShowDate(ModelMap paramMap) throws NkiaException {

		update("CertificationDAO.updatePopupShowDate", paramMap);
	}

	public void updateUserTablePwChange(ModelMap paramMap) throws NkiaException{
		
		update("CertificationDAO.updateUserTablePwChange", paramMap);
	
	}
	
	/**
	 * 최종접속일을 업데이트 합니다.  
	 * @param resultVO
	 * @throws NkiaException
	 */
	public void updateLastLoginUpdt(UserVO resultVO) throws NkiaException{

		update("CertificationDAO.updateLastLoginUpdt", resultVO);
	}
	
	/**
	 * 최종접속IP를 업데이트 합니다.  
	 * @param resultVO
	 * @throws NkiaException
	 */
	public void updateLastLoginIp(UserVO resultVO) throws NkiaException{

		update("CertificationDAO.updateLastLoginIp", resultVO);
	}


	/**
	 * 접속정보 ID 생성
	 * Deprecated : 키 충돌 문제로 사용하지 않음 (20151127 김도원) 
	 */
	@Deprecated
	public String selectLoginInfoID(Map paramMap) throws NkiaException {

		return (String) selectByPk("CertificationDAO.selectLoginInfoID", paramMap);
	}
	
	/**
	 * 접속정보 INSERT
	 */
	public void insertLoginInfo(Map paramMap) throws NkiaException{
		
		insert("CertificationDAO.insertLoginInfo", paramMap);
		
	}
	
	/**
	 * 접속정보 로그아웃시간 UPDATE
	 */
	public void updateLoginInfo(String userID) throws NkiaException{
		
		update("CertificationDAO.updateLoginInfo", userID);
		
	}
	
	/**
	 * 최근 5회 로그인 에러타입을 가져옴  (결과가 0이면 5회 실패한것으로 간주)
	 */
	public int selectLoginPwErrorCount(HashMap paramMap) throws NkiaException {

		return (Integer) selectByPk("CertificationDAO.selectLoginPwErrorCount", paramMap);
	}
	
	
	/**
	 * 최근 5회 모두 에러타입이 있는경우 /결과값 : 0/ 해당 사용자 ID의 DORMANT_YN을 N으로 업데이트
	 */
	public void updateUserDormantLock(String userID) throws NkiaException{
		
		update("CertificationDAO.updateUserDormantLock", userID);
		
	}
	
	
	/**
	 * 로그인 잠금해제 Row Insert 
	 */
	public void insertUnLockRow(ModelMap paramMap) throws NkiaException{

		insert("CertificationDAO.insertUnLockRow", paramMap);
	}
	
	
	/**
	 * 로그아웃 날짜 가져옴
	 */
	public String selectLogOutDt(ModelMap paramMap) throws NkiaException {

		return (String) selectByPk("CertificationDAO.selectLogOutDt", paramMap);
	}
	
	
	/**
	 * 로그인 이력 수 카운트
	 */
	public int selectLogOutDtCount(ModelMap paramMap) throws NkiaException {

		return (Integer) selectByPk("CertificationDAO.selectLogOutDtCount", paramMap);
	}
	
	/**
	 * 로그인 실패 횟수 초기화 
	 * @param resultVO
	 * @throws NkiaException
	 */
	public void updateResetUserLoginFailCount(HashMap paramMap) throws NkiaException{
		update("CertificationDAO.updateResetUserLoginFailCount", paramMap);
	}
	
	/**
	 * 로그인 실패 횟수 1 증가
	 * @param resultVO
	 * @throws NkiaException
	 */
	public void updateAddUserLoginFailCount(HashMap paramMap) throws NkiaException{
		update("CertificationDAO.updateAddUserLoginFailCount", paramMap);
	}

	/**
	 * 로그인 시도 사용자의 로그인 실패 횟수 가져오기
	 */
	public int selectUserLoginFailCount(HashMap paramMap) throws NkiaException {
		return (Integer) selectByPk("CertificationDAO.selectUserLoginFailCount", paramMap);
	}
}
