package com.nkia.itg.certification.dwr;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nkia.itg.base.application.exception.NkiaException;

import uk.ltd.getahead.dwr.WebContext;
import uk.ltd.getahead.dwr.WebContextFactory;

public class RsaKeyGenDwr {

	private int KEY_SIZE = 2048;
	
	public Map generatorKey() throws NkiaException, NoSuchAlgorithmException, InvalidKeySpecException {
		// 세션 인증키 받는 부분 시작
		Map result = new HashMap();
		WebContext ctx = WebContextFactory.get();
		HttpServletRequest req = ctx.getHttpServletRequest();
    	KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
    	generator.initialize(KEY_SIZE);
    	
    	KeyPair keyPair = generator.genKeyPair();
    	KeyFactory keyFactory = KeyFactory.getInstance("RSA");

    	PublicKey publicKey = keyPair.getPublic();
    	PrivateKey privateKey = keyPair.getPrivate();

    	HttpSession session = req.getSession();
    	// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
    	session.setAttribute("__rsaPrivateKey__", privateKey);
    	
    	// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
    	RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);

    	String publicKeyModulus = publicSpec.getModulus().toString(16);
    	String publicKeyExponent = publicSpec.getPublicExponent().toString(16);
    	result.put("KeyModulus", publicKeyModulus);
    	result.put("KeyExponent", publicKeyExponent);
    	//String result = "'"+ publicKeyModulus +"','" + publicKeyExponent + "'";
    	return result;
	}
}
