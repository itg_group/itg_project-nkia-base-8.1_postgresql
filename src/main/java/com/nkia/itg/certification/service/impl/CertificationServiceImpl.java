/*
 * @(#)CertificationServiceImpl.java              2013. 4. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.certification.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.security.CipherRSA;
import com.nkia.itg.base.application.util.security.CipherSHA;
import com.nkia.itg.base.application.util.security.CryptionChange;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.constraint.LoginResultEnum;
import com.nkia.itg.certification.dao.CertificationDAO;
import com.nkia.itg.certification.service.CertificationService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.loginManager.service.LoginManagerService;
import com.nkia.itg.system.user.dao.UserDAO;
import com.nkia.itg.system.user.service.UserService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("certificationService")
public class CertificationServiceImpl implements CertificationService {
	
	private static final Logger logger = LoggerFactory.getLogger(CertificationServiceImpl.class);

	@Resource(name="certificationDAO")
	public CertificationDAO certificationDAO;
	
	@Resource(name = "userDAO")
	public UserDAO userDAO;
	
	@Resource(name = "cryptionChange")
	public CryptionChange cryptionChange;
	
	/** EgovMessageSource */
    @Resource(name="egovMessageSource")
    EgovMessageSource egovMessageSource;
    
    @Resource(name = "userService")
	public UserService userService;
    
    @Resource(name = "loginManagerService")
	public LoginManagerService loginManagerService;
    
	public UserVO login(UserVO userVO) throws NkiaException {
		// 유저정보를 가지고 옵니다. 
    	UserVO loginUserVO = certificationDAO.selectLoginInfo(userVO);
    	return loginUserVO;
	}
	
	public UserVO login_inside(UserVO userVO) throws NkiaException {
		// 유저정보를 가지고 옵니다. 
    	UserVO loginUserVO = certificationDAO.selectLoginInfo_inside(userVO);
    	return loginUserVO;
	}
	
	public UserVO login_outside(UserVO userVO) throws NkiaException {
		// 유저정보를 가지고 옵니다. 
    	UserVO loginUserVO = certificationDAO.selectLoginInfo_outside(userVO);
    	return loginUserVO;
	}

	public HashMap selectUserInfo(ModelMap paramMap) throws NkiaException {
		HashMap resultMap = certificationDAO.selectUserInfo(paramMap);
		return resultMap;
	}

	public HashMap<String, String> updateUserInfo(ModelMap paramMap) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		HashMap<String, String> resultMap = new HashMap<String, String>();
		String resultMsg = egovMessageSource.getMessage("msg.common.result.00004");
		
		// 세션 User ID 비교 - 보안취약점 고려.
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		if(userVO.getUser_id().equalsIgnoreCase((String) paramMap.get("user_id"))){
    			
    			// 패스워드 변경여부
    			String isPasswordChange = (String)paramMap.get("is_password_change");
    			
    			// 패스워드 변경일 경우
    			if("Y".equals(isPasswordChange)){
    				// 이전 사용자 정보
    				HashMap detailMap = userDAO.selectUserInfoPassword(paramMap);
    				
    				String currentPassword = (String) paramMap.get("current_pw");
    				String changePassword = (String) paramMap.get("change_pw");
    				String changePasswordCheck = (String) paramMap.get("change_pw_check");
    				
    				// 클라이언트 RSA복호화
    				PrivateKey privateKey = CipherRSA.getPrivateKey();
    				
    				// 입력된 현재 패스워드 암호화
    				currentPassword = CipherSHA.getEncrypt(CipherRSA.getDecrypt(privateKey, currentPassword), StringUtil.base64ToByte((String)detailMap.get("SCRTY_SALT")));
    				changePassword = CipherRSA.getDecrypt(privateKey, changePassword);
    				changePasswordCheck = CipherRSA.getDecrypt(privateKey, changePasswordCheck);
    				
    				if(!currentPassword.equalsIgnoreCase((String)detailMap.get("PW"))){
    					resultMap.put("IS_VALID", "false");
        				resultMap.put("RESULT_MSG", egovMessageSource.getMessage("msg.system.user.pw.validation.00006"));
        				return resultMap;
    				}else{
    					// 패스워드 검증(자리수, 반복, 패스워드일치, 이전 패스워드 사용여부 등을 검증)
        				// jjy
        				String encryptPw = CipherSHA.getEncrypt(changePassword, StringUtil.base64ToByte((String)detailMap.get("SCRTY_SALT")));
        				resultMap = userService.getUserPwValidation((String)paramMap.get("user_id"), changePassword, changePasswordCheck, encryptPw);
        				
        				String isValid = resultMap.get("IS_VALID");
        				
        				if(isValid.equals("false")) {
        					return resultMap;
        				} 
        				
        				paramMap.put("pw", encryptPw);
    				}
    			} else {
    				resultMap.put("IS_VALID", "true");
    				resultMap.put("RESULT_MSG", resultMsg);
    			}
    			
    			// 사용자 업데이트
    			userDAO.updateUserInfo(paramMap);
    			
    			// 패스워드 변경시 : 패스워드 이력등록
    			if("Y".equals(isPasswordChange)){
    				userDAO.insertPwHistory(paramMap);
    			}
    		}else{
    			resultMap.put("RESULT_MSG", egovMessageSource.getMessage("msg.login.failAccess"));
    		}
    	}else{
    		resultMap.put("RESULT_MSG", egovMessageSource.getMessage("msg.sso.2902"));
    	}
		return resultMap;
	}

	public int selectLoginUserCount(UserVO userVO) throws NkiaException {
	
		return certificationDAO.selectLoginUserCount(userVO);
	}

	public int selectUserGrpCount(UserVO userVO) throws NkiaException {
	
		return certificationDAO.selectUserGrpCount(userVO);
	}
	
	public HashMap selectPwUpdateUserInfo(ModelMap paramMap) throws NkiaException {

		HashMap resultMap;
		
		String MaxDate = certificationDAO.selectMaxDate(paramMap);
		paramMap.put("maxDate", MaxDate);
		
		resultMap = certificationDAO.selectPwUpdateUserInfo(paramMap);
		
		return resultMap;
	}
	
	
	public boolean checkPwUpdateRequestDate(ModelMap paramMap) throws NkiaException {

		boolean result;
		
		// 현재날짜 구하기
		Date date =  new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		int toDay = Integer.parseInt(sdf.format(date));
		
		// 패스워드 수정 기한 
		String nextPwUpdateDate = certificationDAO.selectNextPwUpdateDate(paramMap);
		int updateDay = 0;
		if(nextPwUpdateDate != null) {
			updateDay = Integer.parseInt(nextPwUpdateDate);
		}
		
		// 오늘 날짜가 업데이트 데이보다 크면은 수정팝업을 보여줄 필요 없다
		if(toDay >= updateDay){
			
			HashMap resultMap;
			resultMap = certificationDAO.selectPwUpdateUserInfo(paramMap);
			
			// 파라미터 값이 N인경우에는 패스워드 수정 기한과 팝업 숨김 파라미터 업데이트 한 날짜를 비교합니다. 
			if("N".equals((String)resultMap.get("UPD_POPUP_SHOW"))){
				// 팝업파라미터 업데이트 Date가 현재날짜보다 작으면 널로 업데이트하고 팝업을 보여줍니다.
				int popUpShowDate = Integer.parseInt((String)resultMap.get("UPD_POPUP_DT"));
				
				if(toDay > popUpShowDate){
					paramMap.put("upd_dt", resultMap.get("UPD_DT"));	
					// 팝업보여주는 파라미터 및 업데이트 날짜를 초기화 한다. 
					certificationDAO.updatePopupShowDate(paramMap);
					result = true;
				}else{
					result = false;		
				}
				
			}else{
				result = true;
			}
			
		}else {
			result = false;
		}
		
		return result;
	}
	
	public List checkUserState(ModelMap paramMap) throws NkiaException {
		return certificationDAO.checkUserState(paramMap);
	}
	public void updateOneDayPwPopupShow(ModelMap paramMap) throws NkiaException {

		certificationDAO.updateOneDayPwPopupShow(paramMap);
	}

	public ResultVO updatePwPopupChange(ModelMap paramMap) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		
		ResultVO resultVO = new ResultVO();
		HashMap detailMap = userDAO.selectUserInfoPassword(paramMap);
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String oldPassword = (String) paramMap.get("old_change_pw");
		String changePassword = (String) paramMap.get("change_pw");
		String changePasswordCheck = (String) paramMap.get("change_pw_check");
		
		// 클라이언트 RSA복호화
		PrivateKey privateKey = CipherRSA.getPrivateKey();
		oldPassword = CipherRSA.getDecrypt(privateKey, oldPassword);
		changePassword = CipherRSA.getDecrypt(privateKey, changePassword);
		changePasswordCheck = CipherRSA.getDecrypt(privateKey, changePasswordCheck);
		
		// 패스워드 검증(자리수, 반복, 패스워드일치, 이전 패스워드 사용여부 등을 검증)
		// jjy
		String oldDbPw = StringUtil.parseString(detailMap.get("PW"));
		String oldEncryptPw = CipherSHA.getEncrypt(oldPassword, StringUtil.base64ToByte(StringUtil.parseString(detailMap.get("SCRTY_SALT"))));
		String newEncryptPw = CipherSHA.getEncrypt(changePassword, StringUtil.base64ToByte(StringUtil.parseString(detailMap.get("SCRTY_SALT"))));
		String newCheckEncryptPw = CipherSHA.getEncrypt(changePasswordCheck, StringUtil.base64ToByte(StringUtil.parseString(detailMap.get("SCRTY_SALT"))));
		
		// 입력된 패스워드와 저장된 패스워드가 다를때
		if(!oldEncryptPw.equals(oldDbPw)){
			resultVO.setResultMsg("변경전 패스워드가 일치하지 않습니다.");
		}else {
			// 새로 받은 비밀번호와 DB에 저장된 기존 비밀번호가 같습니다. 
			if(oldDbPw.equals(newEncryptPw)){
				resultVO.setResultBoolean(false);
				String spMsg = egovMessageSource.getMessage("msg.login.samePassWord");
				String reSpMsg = "";
				if(spMsg.indexOf("\\n") > -1) {
					String[] tempSpMsg = spMsg.split("\\\\n");
					for(int i=0; i<tempSpMsg.length; i++) {
						if(i==0)
							reSpMsg += tempSpMsg[i];
						else
							reSpMsg += ("\n" + tempSpMsg[i]);
					}
				}
				resultVO.setResultMsg(reSpMsg);
			}else{
				HashMap updateMap = new HashMap();
				
				updateMap.put("pw", newEncryptPw);
				updateMap.put("user_id", userVO.getUser_id());
				updateMap.put("ins_user_id", userVO.getUser_id());
				
				// 패스워드 업데이트
				userDAO.updateUserPwInfo(updateMap);
				// 패스워드 이력 추가
				userDAO.insertPwHistory(updateMap);
				
				resultVO.setResultBoolean(true);
				resultVO.setResultMsg(egovMessageSource.getMessage("msg.common.result.00004"));
			}
		}
		
		return resultVO;
	}
	
	/**
	 * 최종접속일을 업데이트 합니다.  
	 * @param resultVO
	 * @throws NkiaException
	 */
	public void updateLastLoginUpdt(UserVO resultVO) throws NkiaException {
		
		certificationDAO.updateLastLoginUpdt(resultVO);
		certificationDAO.updateLastLoginIp(resultVO);
	}

	@Override
	public void insertLoginInfo(Map paramMap)throws NkiaException{
		//20151127 김도원 : 키값 세팅 하지 않음 
		certificationDAO.insertLoginInfo(paramMap);
		
		/*
		// 최근 5회 로그인 에러타입을 가져옴 
    	int pwErrorCnt = 0;
    	String userId = (String) paramMap.get("login_user_id");
    	
    	HashMap params = new HashMap();
    	params.put("login_id", userId);
    	
    	pwErrorCnt = certificationDAO.selectLoginPwErrorCount(params);
    	
    	// 최근 5회 모두 에러타입이 있는경우, 해당 사용자 ID의 DORMANT_YN을 N으로 업데이트
    	if(pwErrorCnt >= 5) {
    		certificationDAO.updateUserDormantLock(userId);
    	}
    	*/
	}

	public void updateLoginInfo(String userID)throws NkiaException{
		certificationDAO.updateLoginInfo(userID);
	}	
	
	public void insertUnLockRow(ModelMap paramMap)throws NkiaException{
		certificationDAO.insertUnLockRow(paramMap);
	}
	
	public void updatePwInit(ModelMap paramMap)throws NkiaException, NoSuchAlgorithmException, IOException{
		
		String userId = "";
		String newValue = "";
		String oldValue = "";
		String enNewValue = "";
		
		userId = (String) paramMap.get("user_id");
		//newValue = (String) paramMap.get("initPw");
		//롯데정보통신에서는 비밀번호를 ID랑 동일하게 감
		newValue = (String) paramMap.get("user_id");
		paramMap.put("user_id",userId);
		HashMap detailMap = new HashMap();
		ModelMap requestMap = new ModelMap();
		
		detailMap = userDAO.selectUserInfoDetail(paramMap); 
		// 사용자의 이전 패스워드를 가져온다
		oldValue = (String)detailMap.get("PW");
		// 등록할 패스워드를 인코딩한다.
		//enNewValue = cryptionChange.encryptPassWord(newValue);
		enNewValue = CipherSHA.getEncrypt(newValue, StringUtil.base64ToByte((String)detailMap.get("SCRTY_SALT")));
		
		requestMap.put("user_id", userId);
		requestMap.put("updId", userId);
		requestMap.put("securedPassWord", enNewValue);
		requestMap.put("originPw", oldValue);
		
		// SYS_USER 테이블의 비밀번호를 업데이트한다. (센터 동기화)
		certificationDAO.updateUserTablePwChange(requestMap);
		
		// 비밀번호 변경 이력을 남긴다. (센터 동기화)
		certificationDAO.insertUserPwHistory(requestMap);
		
		// 메일전송
		//sendMail(paramMap, newValue);
	}
	
	/*
	private void sendMail( Map paramMap, String newPw ){
		
		MailVO mailVO;
		
		try {
			String email = (String) paramMap.get("email");
			String to_user_id = (String) paramMap.get("userId");
			
			mailVO = new MailVO();
			
			mailVO.setTo(email);
			mailVO.setTo_user_id(to_user_id);
			
			mailVO.setSubject(to_user_id + "님! 비밀번호 초기화 안내입니다.");
			
			String content = "<br/>비밀번호가 초기화되어 내역을 안내드립니다."
					+ "<br/><br/>"
					+ "<b>* 계정</b>"
					+ "<br/>&nbsp;&nbsp;&nbsp;"
					+ "[ " + to_user_id + " ]"
					+ "<br/><br/>"
					+ "<b>* 초기화 비밀번호</b>"
					+ "<br/>&nbsp;&nbsp;&nbsp;"
					+ "[ <font color=\"red\">" + newPw + "</font> ]"
					;
			
			mailVO.setContent(content);
			
			MailSender aMailSender = new MailSender();
			aMailSender.setMailVO(mailVO);
			
			Thread t = new Thread(aMailSender);
			t.start();
		} catch (Exception e) {
			
			logger.info( e );
		}
		
	}
	*/
	
	/**
	 * 로그아웃 날짜 가져옴
	 */
	public String selectLogOutDt(ModelMap paramMap) throws NkiaException {

		return (String) certificationDAO.selectLogOutDt(paramMap);
	}
	
	
	/**
	 * 로그인 이력 수 카운트
	 */
	public int selectLogOutDtCount(ModelMap paramMap) throws NkiaException {

		return (Integer) certificationDAO.selectLogOutDtCount(paramMap);
	}

	@Override
	public LoginResultEnum checkLoginInfo(PrivateKey privateKey, String pPassWord, UserVO userVO, ModelMap model, List loginMngList) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,NoSuchAlgorithmException, IOException {
		LoginResultEnum result = LoginResultEnum.NO_ERR;
		if (userVO != null) {
	    	//DB의 사용자정보 상태를 확인합니다.
	    	if(!"".equals(userVO.getUser_id())) {
	    		result = checkStateUserInfo(userVO);
	    	} else {
	    		result = LoginResultEnum.NOT_USER_ERR;
	    	}
	
	    	//데이터가 정상적일 경우에만 패스워드를 체크합니다.
	    	if(result == LoginResultEnum.NO_ERR) {
	    		// 사용자 계정이 5회이상 연속 틀렸는지 먼저 확인
	    		HashMap<String, String> dataMap = new HashMap<String, String>();
	    		dataMap.put("user_id", userVO.getUser_id());
	    		
	    		boolean isLoginAccept = false;
	    		
	    		if(loginMngList != null && loginMngList.size()>0) {
	    			
	    			HashMap loginMngMap = (HashMap)loginMngList.get(0);
	    			String systemLoginFailYn = StringUtil.parseString(loginMngMap.get("LOGIN_FAIL_BLOCK_YN"));
	    			String systemLoginCntStr = StringUtil.parseString(loginMngMap.get("LOGIN_FAIL_CNT_VALUE"));
	    			int systemLoginCnt = StringUtil.parseInteger(systemLoginCntStr);
	    			
	    			if("".equals(systemLoginCntStr) || "0".equals(systemLoginCntStr)) {
	    				// 로그인 실패 기준이 null이거나 0이면 '로그인 실패 시 계정잠금' 사용 안함.
	    				systemLoginFailYn = "N";
	    			}
	    			
	    			// 로그인 실패 계정잠금 여부가 Y 이면 로그인 실패 기준에 따라 로그인 불가 처리.
	    			if("Y".equals(systemLoginFailYn)) {
	    				int loginFailCnt = certificationDAO.selectUserLoginFailCount(dataMap);
	    				if( systemLoginCnt <= loginFailCnt ) {
	            			result = LoginResultEnum.LOCK_ERR;
	            			model.put("fail_check_cnt", systemLoginCntStr);
	            		} else {
	            			isLoginAccept = true;
	            		}
	    			} else {
	    				isLoginAccept = true;
	    			}
	    		} else {
	    			isLoginAccept = true;
	    		}
	    		
	    		if(isLoginAccept) {
	    			result = checkPassword(privateKey, pPassWord, userVO.getPw().trim(), userVO.getScrty_salt().trim(), userVO.getUser_id());
	    		}
	    	}
    	} else {
    		result = LoginResultEnum.NULL_ERR;
    	}
    	
    	return result;
	}
	
    private LoginResultEnum checkStateUserInfo(UserVO userVO) throws NkiaException {    	
    	LoginResultEnum result = LoginResultEnum.NO_ERR;
    	//사용자그룹정보를 확인합니다.
		int userGrpCount = certificationDAO.selectUserGrpCount(userVO);
		if (0 >= userGrpCount){
			result = LoginResultEnum.USER_GRP_ERR;
		} else {
			String dormantYn = userVO.getDormantYn();
			String useYn = userVO.getUse_yn();
			if("N".equals(useYn)) {
				result = LoginResultEnum.NOT_USE_ERR;
			} else if("S".equals(useYn)){  
				result = LoginResultEnum.LONGTERM_ERR;
			}else if("Y".equals(dormantYn)){  
				result = LoginResultEnum.DORMANT_ERR;
			} else if("L".equals(dormantYn)) {  
				result = LoginResultEnum.LOCK_ERR;
			}
		}
		return result;
    }
    
    private LoginResultEnum checkPassword(PrivateKey privateKey, String pPassWord, String vPassWork, String scrtySalt, String userId) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
    	String password = null;
    	LoginResultEnum result = LoginResultEnum.NO_ERR;
    	if (privateKey != null) {
    		// SHA256으로 단방향 암호화
    		password = CipherSHA.getEncrypt(CipherRSA.getDecrypt(privateKey, pPassWord), StringUtil.base64ToByte(scrtySalt));
        }
        
    	if (password != null) {
			// 인증키 에러시 
			if (password.equals("")) {
				result = LoginResultEnum.KEY_ERR;
			} else {
	    		// 입력된 값과 DB에 저장된 값과 같으면 Login OK
				if(!password.equals(vPassWork)) {
					result = LoginResultEnum.PASSWORD_ERR;
					// 다르면 로그인 실패 카운트 1 증가
					HashMap<String, String> paramMap = new HashMap<String, String>();
					paramMap.put("user_id", userId);
					certificationDAO.updateAddUserLoginFailCount(paramMap);
				}
			}
    	} else {
    		result = LoginResultEnum.KEY_ERR;
    	}
    	
		return result;
	}
    
	/**
	 * 실제로 복호화 하는 부분 
	 * @param privateKey
	 * @param securedValue
	 * @return
	 * @throws NkiaException
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws UnsupportedEncodingException 
	 */
    private String decryptRsa(PrivateKey privateKey, String securedValue) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
       
//    	System.out.println("will decrypt : " + securedValue);
        Cipher cipher = Cipher.getInstance("RSA");
        byte[] encryptedBytes = StringUtil.hexToByteArray(securedValue);
        
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
       
        // 복호화된 userId와 passWord를 가지고 있습니다. 
        String decryptedValue = new String(decryptedBytes, "utf-8"); // 문자 인코딩 주의.
        return decryptedValue;
    }
 
	@Override
	public void updateResetUserLoginFailCount(String userId)throws NkiaException {
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("user_id", userId);
		certificationDAO.updateResetUserLoginFailCount(paramMap);
	}
	@Override
	public ResultVO inspectionTF(ModelMap paramMap) throws NkiaException {
		
		ResultVO resultVO = new ResultVO();
		HashMap detailMap = userDAO.selectUserInfoPassword(paramMap);
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String cerPassword = (String) paramMap.get("cerPw");
		
		if(!cerPassword.equals(detailMap.get("TWOFACTOR"))){
			resultVO.setResultMsg("인증번호가 일치하지 않습니다.");
			resultVO.setResultBoolean(false);
		}else if("Y".equals(detailMap.get("TWOFACTOR_SW"))){
			resultVO.setResultMsg("인증번호 유효기간이 만료되었습니다. 인증번호를 다시 발급하여 주세요.");
			resultVO.setResultBoolean(false);
		}else {
			resultVO.setResultMsg("인증되었습니다.");
			resultVO.setResultBoolean(true);
		}
		return resultVO;
	}
}
