/*
 * @(#)CertificationService.java              2013. 4. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.certification.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.constraint.LoginResultEnum;
import com.nkia.itg.certification.vo.UserVO;

public interface CertificationService {

	public UserVO login(UserVO userVO) throws NkiaException;
	
	public UserVO login_inside(UserVO userVO) throws NkiaException;
	
	public UserVO login_outside(UserVO userVO) throws NkiaException;

	public HashMap selectUserInfo(ModelMap paramMap) throws NkiaException;

	public HashMap<String, String> updateUserInfo(ModelMap paramMap)  throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException;

	public int selectLoginUserCount(UserVO userVO) throws NkiaException;

	public int selectUserGrpCount(UserVO userVO) throws NkiaException;
	
	public HashMap selectPwUpdateUserInfo(ModelMap paramMap) throws NkiaException;

	public boolean checkPwUpdateRequestDate(ModelMap paramMap) throws NkiaException;
	
	public List checkUserState(ModelMap paramMap) throws NkiaException;

	public void updateOneDayPwPopupShow(ModelMap paramMap) throws NkiaException;

	public ResultVO updatePwPopupChange(ModelMap paramMap) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, IOException;
	
	/**
	 * 최종접속일을 업데이트 합니다.  
	 * @param resultVO
	 * @throws NkiaException
	 */
	public void updateLastLoginUpdt(UserVO resultVO) throws NkiaException;

	/**
	 * 접속정보 등록 및 접속정보ID 조회 
	 */
	public void insertLoginInfo(Map paramMap)throws NkiaException;
	
	/**
	 * 접속정보 로그아웃 시간 등록 
	 */
	public void updateLoginInfo(String userID)throws NkiaException;
	
	/**
	 * 로그인 잠금해제 Row Insert 
	 */
	public void insertUnLockRow(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 사용자 비밀번호 초기화 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 */
	public void updatePwInit(ModelMap paramMap)throws NkiaException, NoSuchAlgorithmException, UnsupportedEncodingException, IOException;
	
	/**
	 * 로그아웃 날짜 가져옴
	 */
	public String selectLogOutDt(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 로그인 이력 수 카운트
	 */
	public int selectLogOutDtCount(ModelMap paramMap) throws NkiaException;

	public LoginResultEnum checkLoginInfo(PrivateKey privateKey, String pPassWord, UserVO userVO, ModelMap model, List loginMngList) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException;
	
	/**
	 * 로그인 실패 횟수 0으로 초기화
	 * @param user_id
	 */
	public void updateResetUserLoginFailCount(String user_id) throws NkiaException;
	
	public ResultVO inspectionTF(ModelMap paramMap) throws NkiaException;
	
	
}
