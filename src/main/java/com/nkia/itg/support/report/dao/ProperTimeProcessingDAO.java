/*
 * @(#)PostMatricDAO.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("properTimeProcessingDAO")
public class ProperTimeProcessingDAO extends EgovComAbstractDAO  {
	
	
	public List searchReqName(Map param) throws NkiaException {
	return list("properTimeProcessingDAO.searchReqName", param);
	}
	
	public int searchProperTimeMonthCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("properTimeProcessingDAO.searchProperTimeMonthCount", paramMap);
	}
	public List searchProperTimeMonth(Map paramMap) throws NkiaException {
		return list("properTimeProcessingDAO.searchProperTimeMonth", paramMap);
	}
	public int searchProperTimeDayCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("properTimeProcessingDAO.searchProperTimeDayCount", paramMap);
	}
	public List searchProperTimeDay(Map paramMap) throws NkiaException {
		return list("properTimeProcessingDAO.searchProperTimeDay", paramMap);
	}	
	
	public int searchDblClickCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("properTimeProcessingDAO.searchDblClickCount", paramMap);
	}
	public List searchDblClick(Map paramMap) throws NkiaException {
		return list("properTimeProcessingDAO.searchDblClick", paramMap);
	}	
	
	
	
	public List searchWorkReqDocListTree(Map paramMap) throws NkiaException {
	List resultList = null;
	resultList = this.list("properTimeProcessingDAO.searchWorkReqDocListTree", paramMap);
	return resultList;
}
}
