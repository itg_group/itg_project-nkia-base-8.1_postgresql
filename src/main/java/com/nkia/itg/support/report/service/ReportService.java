/*
 * @(#)PostMatricService.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ReportService {
	
	public int searchReportListCount(ModelMap paramMap) throws NkiaException;

	public List searchReportList(ModelMap paramMap) throws NkiaException;
	
	public Map searchMonthlyReportMng(Map paramMap) throws NkiaException;
	
	public Map searchMonthlyReportAtchId(Map paramMap) throws NkiaException;
	
	public void insertMonthlyReportMng(Map paramMap) throws NkiaException;
	
	public void updateMonthlyReportMng(Map paramMap) throws NkiaException;
	
	public List searchTwoHourDelay(ModelMap paramMap) throws NkiaException;
	
	public int searchTwoHourDelayCnt(ModelMap paramMap) throws NkiaException;
	
	public List searchTwoHourRceptDateRate(ModelMap paramMap) throws NkiaException;
	
	public int searchTwoHourRceptDateRateCnt(ModelMap paramMap) throws NkiaException;
	
	public List searchTwoHourRceptMonthRate(ModelMap paramMap) throws NkiaException;
	
	public int searchTwoHourRceptMonthRateCnt(ModelMap paramMap) throws NkiaException;
	
	public List selectOperThroughput(Map paramMap) throws NkiaException;
	
	public int selectOperThroughputCnt(ModelMap paramMap) throws NkiaException;
	
	// 20200311_이원재_002_신규작성
	public List selectSatisfactionStatusList(Map paramMap) throws NkiaException;
	
	public int selectSatisfactionStatusCount(Map paramMap) throws NkiaException;
	
	public List selectSatisfactionStatisticsList(Map paramMap) throws NkiaException;
	
	public int selectSatisfactionStatisticsCount(Map paramMap) throws NkiaException;
	
	public List selectSatisfactionStatisticsDetail(Map paramMap) throws NkiaException;
	
	public int selectSatisfactionStatisticsDetailCount(Map paramMap) throws NkiaException;
	
	public List selectSuccessRatePerCategoryList(Map paramMap) throws NkiaException;
	
	public int selectSuccessRatePerCategoryCount(Map paramMap) throws NkiaException;
	
	public List selectSuccessRatePerCategoryDetail(Map paramMap) throws NkiaException;
	
	public int selectSuccessRatePerCategoryDetailCount(Map paramMap) throws NkiaException;
	// 20200311_이원재_002_신규작성 END
	public List selectOperThroughputUserAll(Map paramMap) throws NkiaException;
	
	public int selectOperThroughputUserAllCnt(ModelMap paramMap) throws NkiaException;
	
	public List searchTwoHourRceptRateDetail(ModelMap paramMap) throws NkiaException;
	
	public int searchTwoHourRceptRateDetailCnt(ModelMap paramMap) throws NkiaException;
	
	// 20200401_이원재_002_신규작성
	public List selectOperWorkPerCustomerList(Map paramMap) throws NkiaException;
	
	public int selectOperWorkPerCustomerCount(Map paramMap) throws NkiaException;
	
	public List selectOperWorkPerCustomerDetailList(Map paramMap) throws NkiaException;
	
	public int selectOperWorkPerCustomerDetailCount(Map paramMap) throws NkiaException;
	// 20200401_이원재_002_신규작성 END
}
