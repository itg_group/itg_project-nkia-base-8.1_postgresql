/*
 * @(#)PostMatricController.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.support.report.service.ProperTimeProcessingService;
import com.nkia.itg.support.report.service.ReportService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ProperTimeProcessingController {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="ReportService")
	private ReportService reportService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "properTimeProcessingService")
	private ProperTimeProcessingService properTimeProcessingService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	@RequestMapping(value="/itg/support/report/properTimeProcessing.do")
	public String monthlyReport(ModelMap param) {
		String forwardPage = "/itg/support/report/properTimeProcessing.nvf";
		return forwardPage;
	}
	
	@RequestMapping(value="/itg/support/report/searchReqName.do")
	public @ResponseBody ResultVO searchReqName(@RequestBody ModelMap paramMap)throws NkiaException  {		
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("login_user_id", userVO.getUser_id());
    		
			GridVO gridVO = new GridVO();
			List resultList = properTimeProcessingService.searchReqName(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/searchProperTime.do")
	public @ResponseBody ResultVO searchProperTime(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		try{
			paramMap.put("login_user_id", login_user_id);   		
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			List resultList;
			
			
			if("MONTH".equals(paramMap.get("SEARCH_DT_TYPE"))){
			totalCount = properTimeProcessingService.searchProperTimeMonthCount(paramMap);
			resultList = properTimeProcessingService.searchProperTimeMonth(paramMap);
			}
			else{
			totalCount = properTimeProcessingService.searchProperTimeDayCount(paramMap);
			resultList = properTimeProcessingService.searchProperTimeDay(paramMap);	
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}

	@RequestMapping(value="/itg/support/report/searchDblClick.do")
	public @ResponseBody ResultVO searchDblClick(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		try{
			paramMap.put("login_user_id", login_user_id);   
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = properTimeProcessingService.searchDblClickCount(paramMap);
			List resultList = properTimeProcessingService.searchDblClick(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}

	
	
	
	@RequestMapping(value="/itg/support/report/serviceproperExcelDown.do")
	public @ResponseBody ResultVO serviceproperExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		
		Map excelParam = (Map) paramMap.get("excelParam");
		Map temp = (Map) paramMap.get("param");
		
		try{
			//page=1, start=0, limit=10 있는 경우, 
			if("MONTH".equals(temp.get("SEARCH_DT_TYPE"))){
				resultList = properTimeProcessingService.searchProperTimeMonth(temp);
				}
				else{				
				resultList = properTimeProcessingService.searchProperTimeDay(temp);	
				}
			
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);				
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;		
	}	
	
	@RequestMapping(value="/itg/support/report/servicedblclickExcelDown.do")
	public @ResponseBody ResultVO servicedblclickExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		
		Map excelParam = (Map) paramMap.get("excelParam");
		Map temp = (Map) paramMap.get("param");
		
		try{
			//page=1, start=0, limit=10 있는 경우, 
			resultList = properTimeProcessingService.searchDblClick(temp);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);				
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;		
	}	
	
	/**
	 * 작업요청양식 리스트 트리 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/support/report/searchWorkReqDocListTree.do")
	public @ResponseBody ResultVO searchWorkReqDocListTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = properTimeProcessingService.searchWorkReqDocListTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	
}