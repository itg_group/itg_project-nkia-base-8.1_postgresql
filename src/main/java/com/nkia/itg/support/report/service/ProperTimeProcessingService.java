/*
 * @(#)PostMatricService.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ProperTimeProcessingService {
	
	public List searchReqName(Map param) throws NkiaException ;
	
	
	public List searchProperTimeMonth(Map paramMap) throws NkiaException;	
	public int searchProperTimeMonthCount(Map paramMap) throws NkiaException ;
	
	public List searchProperTimeDay(Map paramMap) throws NkiaException;	
	public int searchProperTimeDayCount(Map paramMap) throws NkiaException ;	
	

	public int searchDblClickCount(Map paramMap) throws NkiaException;

	public List searchDblClick(Map paramMap) throws NkiaException ;
	
	



	public List searchWorkReqDocListTree(Map paramMap) throws NkiaException ;
	
}
