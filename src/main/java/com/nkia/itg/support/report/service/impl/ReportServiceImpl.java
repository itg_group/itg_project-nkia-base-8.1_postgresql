/*
 * @(#)PostMatricServiceImpl.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.support.report.dao.ReportDAO;
import com.nkia.itg.support.report.service.ReportService;

@Service("ReportService")
public class ReportServiceImpl implements ReportService {
	
	private static final Logger logger = LoggerFactory.getLogger(ReportServiceImpl.class);
	
	@Resource(name="reportDAO")
	public ReportDAO reportDAO;
	
	public List searchReportList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchReportList(paramMap);
	}
	
	public int searchReportListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchReportListCount(paramMap);
	}
	
	public Map searchMonthlyReportMng(Map paramMap) throws NkiaException {
		return reportDAO.searchMonthlyReportMng(paramMap);
	}
	
	public Map searchMonthlyReportAtchId(Map paramMap) throws NkiaException {
		return reportDAO.searchMonthlyReportAtchId(paramMap);
	}
	
	public void insertMonthlyReportMng(Map paramMap) throws NkiaException {
		reportDAO.insertMonthlyReportMng(paramMap);
	}
	
	public void updateMonthlyReportMng(Map paramMap) throws NkiaException {
		reportDAO.updateMonthlyReportMng(paramMap);
	}
	
	public List searchTwoHourDelay(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourDelay(paramMap);
	}
	
	public int searchTwoHourDelayCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourDelayCnt(paramMap);
	}
	
	public List searchTwoHourRceptDateRate(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourRceptDateRate(paramMap);
	}
	
	public int searchTwoHourRceptDateRateCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourRceptDateRateCnt(paramMap);
	}
	
	public List searchTwoHourRceptMonthRate(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourRceptMonthRate(paramMap);
	}
	
	public int searchTwoHourRceptMonthRateCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourRceptMonthRateCnt(paramMap);
	}
	
	public List selectOperThroughput(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.selectOperThroughput(paramMap);
	}
	
	public int selectOperThroughputCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.selectOperThroughputCnt(paramMap);
	}
	
	// 20200311_이원재_002_신규작성
	public List selectSatisfactionStatusList(Map paramMap) throws NkiaException {
		return reportDAO.selectSatisfactionStatusList(paramMap);
	}
	
	public int selectSatisfactionStatusCount(Map paramMap) throws NkiaException {
		return reportDAO.selectSatisfactionStatusCount(paramMap);
	}
	
	public List selectSatisfactionStatisticsList(Map paramMap) throws NkiaException {
		return reportDAO.selectSatisfactionStatisticsList(paramMap);
	}
	
	public int selectSatisfactionStatisticsCount(Map paramMap) throws NkiaException {
		return reportDAO.selectSatisfactionStatisticsCount(paramMap);
	}
	
	public List selectSatisfactionStatisticsDetail(Map paramMap) throws NkiaException {
		return reportDAO.selectSatisfactionStatisticsDetail(paramMap);
	}
	
	public int selectSatisfactionStatisticsDetailCount(Map paramMap) throws NkiaException {
		return reportDAO.selectSatisfactionStatisticsDetailCount(paramMap);
	}
	
	public List selectSuccessRatePerCategoryList(Map paramMap) throws NkiaException {
		return reportDAO.selectSuccessRatePerCategoryList(paramMap);
	}
	
	public int selectSuccessRatePerCategoryCount(Map paramMap) throws NkiaException {
		return reportDAO.selectSuccessRatePerCategoryCount(paramMap);
	}
	
	public List selectSuccessRatePerCategoryDetail(Map paramMap) throws NkiaException {
		return reportDAO.selectSuccessRatePerCategoryDetail(paramMap);
	}
	
	public int selectSuccessRatePerCategoryDetailCount(Map paramMap) throws NkiaException {
		return reportDAO.selectSuccessRatePerCategoryDetailCount(paramMap);
	}
	
	public List selectOperThroughputUserAll(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.selectOperThroughputUserAll(paramMap);
	}

	public int selectOperThroughputUserAllCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.selectOperThroughputUserAllCnt(paramMap);
	}
	
	public List searchTwoHourRceptRateDetail(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourRceptRateDetail(paramMap);
	}
	
	public int searchTwoHourRceptRateDetailCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return reportDAO.searchTwoHourRceptRateDetailCnt(paramMap);
	}
	
	// 20200401_이원재_002_신규작성
		@Override
		public List selectOperWorkPerCustomerList(Map paramMap) throws NkiaException {
			return reportDAO.selectOperWorkPerCustomerList(paramMap);
		}
		
		@Override
		public int selectOperWorkPerCustomerCount(Map paramMap) throws NkiaException {
			return reportDAO.selectOperWorkPerCustomerCount(paramMap);
		}
		
		@Override
		public List selectOperWorkPerCustomerDetailList(Map paramMap) throws NkiaException {
			switch((String) paramMap.get("column")) {
			case "RES_01": return reportDAO.selectOperWorkPerCustomerDetailList_Res01(paramMap);
			case "RES_02": return reportDAO.selectOperWorkPerCustomerDetailList_Res02(paramMap);
			case "RES_03": return reportDAO.selectOperWorkPerCustomerDetailList_Res03(paramMap);
			case "RES_04": return reportDAO.selectOperWorkPerCustomerDetailList_Res04(paramMap);
			case "RES_05": return reportDAO.selectOperWorkPerCustomerDetailList_Res05(paramMap);
			case "RES_06": return reportDAO.selectOperWorkPerCustomerDetailList_Res06(paramMap);
			case "RES_07": return reportDAO.selectOperWorkPerCustomerDetailList_Res07(paramMap);
			case "RES_08": return reportDAO.selectOperWorkPerCustomerDetailList_Res08(paramMap);
			case "RES_09": return reportDAO.selectOperWorkPerCustomerDetailList_Res09(paramMap);
			case "RES_10": return reportDAO.selectOperWorkPerCustomerDetailList_Res10(paramMap);
			case "RES_11": return reportDAO.selectOperWorkPerCustomerDetailList_Res11(paramMap);
			case "RES_12": return reportDAO.selectOperWorkPerCustomerDetailList_Res12(paramMap);
			case "RES_13": return reportDAO.selectOperWorkPerCustomerDetailList_Res13(paramMap);
			case "RES_14": return reportDAO.selectOperWorkPerCustomerDetailList_Res14(paramMap);
			case "RES_15": return reportDAO.selectOperWorkPerCustomerDetailList_Res15(paramMap);
			case "RES_16": return reportDAO.selectOperWorkPerCustomerDetailList_Res16(paramMap);
			default: return null;
			}
		}
		
		@Override
		public int selectOperWorkPerCustomerDetailCount(Map paramMap) throws NkiaException {
			switch((String) paramMap.get("column")) {
			case "RES_01": return reportDAO.selectOperWorkPerCustomerDetailCount_Res01(paramMap);
			case "RES_02": return reportDAO.selectOperWorkPerCustomerDetailCount_Res02(paramMap);
			case "RES_03": return reportDAO.selectOperWorkPerCustomerDetailCount_Res03(paramMap);
			case "RES_04": return reportDAO.selectOperWorkPerCustomerDetailCount_Res04(paramMap);
			case "RES_05": return reportDAO.selectOperWorkPerCustomerDetailCount_Res05(paramMap);
			case "RES_06": return reportDAO.selectOperWorkPerCustomerDetailCount_Res06(paramMap);
			case "RES_07": return reportDAO.selectOperWorkPerCustomerDetailCount_Res07(paramMap);
			case "RES_08": return reportDAO.selectOperWorkPerCustomerDetailCount_Res08(paramMap);
			case "RES_09": return reportDAO.selectOperWorkPerCustomerDetailCount_Res09(paramMap);
			case "RES_10": return reportDAO.selectOperWorkPerCustomerDetailCount_Res10(paramMap);
			case "RES_11": return reportDAO.selectOperWorkPerCustomerDetailCount_Res11(paramMap);
			case "RES_12": return reportDAO.selectOperWorkPerCustomerDetailCount_Res12(paramMap);
			case "RES_13": return reportDAO.selectOperWorkPerCustomerDetailCount_Res13(paramMap);
			case "RES_14": return reportDAO.selectOperWorkPerCustomerDetailCount_Res14(paramMap);
			case "RES_15": return reportDAO.selectOperWorkPerCustomerDetailCount_Res15(paramMap);
			case "RES_16": return reportDAO.selectOperWorkPerCustomerDetailCount_Res16(paramMap);
			default: return 0;
			}
		}
		// 20200401_이원재_002_신규작성 END
}
