/*
 * @(#)PostMatricController.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.support.report.service.ReportService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ReportController {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name="ReportService")
	private ReportService reportService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 보고서 조회 (공통)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */

	@RequestMapping(value="/itg/support/report/selectReport.do")
	public String selectReport(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/support/report/selectReport.nvf";
		return forwarPage;
	}
	
	
	/**
	 * 
	 * 일일종합운영현황 조회
	 * 
	 * @param paramMap
	 * @return String
	 */
	@RequestMapping(value="/itg/support/report/dailyTotReport.do")
	public String postSloSearch(ModelMap paramMap){
		
		return "/itg/support/report/dailyTotReport.nvf";
	}
	
	/**
	 * 
	 * 서비스 수준 관리 월간보고서 조회
	 * 
	 * @param paramMap
	 * @return String
	 */
	@RequestMapping(value="/itg/slms/report/reportMonth.do")
	public String postSlmsSearch(ModelMap paramMap){
		
		return "/itg/slms/report/reportMonth.nvf";
	}
	
	/**
	 * 월간보고서 리스트
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/report/searchReportList.do")
	public @ResponseBody ResultVO searchNewAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try{
			int totalCount = 0;
			
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    		paramMap.put("login_cust_id", userVO.getCust_id());
	    	}			
	    	
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			totalCount = reportService.searchReportListCount(paramMap);
			resultList = reportService.searchReportList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/monthlyReport.do")
	public String monthlyReport(ModelMap param) {
		String forwardPage = "/itg/support/report/monthlyReport.nvf";
		return forwardPage;
	}
	
	@RequestMapping(value="/itg/support/report/monthlyReportMng.do")
	public String monthlyReportMng(ModelMap param) {
		String forwardPage = "/itg/support/report/monthlyReportMng.nvf";
		return forwardPage;
	}
	
	@RequestMapping(value="/itg/support/report/searchMonthlyReportMng.do")
	public @ResponseBody ResultVO searchMonthlyReportMng(@RequestBody ModelMap paramMap) throws NkiaException {		
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try{
			resultMap = reportService.searchMonthlyReportMng(paramMap);
			resultVO.setResultMap(resultMap);	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/searchMonthlyReportAtchId.do")
	public @ResponseBody ResultVO searchMonthlyReportAtchId(@RequestBody ModelMap paramMap) throws NkiaException {		
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		
		try{
			resultMap = reportService.searchMonthlyReportAtchId(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/insertMonthlyReportMng.do")
	public @ResponseBody ResultVO insertMonthlyReportMng(@RequestBody Map paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "";
			userId = userVO.getUser_id();
			paramMap.put("login_user_id", userId);
			reportService.insertMonthlyReportMng(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/updateMonthlyReportMng.do")
	public @ResponseBody ResultVO updateMonthlyReportMng(@RequestBody Map paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "";
			userId = userVO.getUser_id();
			paramMap.put("login_user_id", userId);
			reportService.updateMonthlyReportMng(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));							
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/serRecTwoHourDelayCnt.do")
	public String serRecTwoHourDelayCnt(ModelMap param) {
		String forwardPage = "/itg/support/report/serRecTwoHourDelayCnt.nvf";
		return forwardPage;
	}
	
	/**
	 * 서비스요청 2시간 이내 접수 지연건
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/support/report/searchTwoHourDelay.do")
	public @ResponseBody ResultVO searchTwoHourDelay(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.searchTwoHourDelay(paramMap);		
			totalCount = reportService.searchTwoHourDelayCnt(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스요청 2시간 이내 접수율 화면
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/support/report/searchTwoHourRceptRate.do")
	public String searchTwoHourRceptRate(ModelMap param) {
		String forwardPage = "/itg/support/report/searchTwoHourRceptRate.nvf";
		return forwardPage;
	}
	
	/**
	 * 서비스요청 2시간 이내 접수율(일별)
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/support/report/searchTwoHourRceptDateRate.do")
	public @ResponseBody ResultVO searchTwoHourRceptDateRate(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.searchTwoHourRceptDateRate(paramMap);		
			totalCount = reportService.searchTwoHourRceptDateRateCnt(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스요청 2시간 이내 접수율(월별)
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/support/report/searchTwoHourRceptMonthRate.do")
	public @ResponseBody ResultVO searchTwoHourRceptMonthRate(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			if(paramMap.get("date_type") != null && paramMap.get("date_type").equals("DAY")){				
				resultList = reportService.searchTwoHourRceptDateRate(paramMap);		
				totalCount = reportService.searchTwoHourRceptDateRateCnt(paramMap);
			}else{
				resultList = reportService.searchTwoHourRceptMonthRate(paramMap);		
				totalCount = reportService.searchTwoHourRceptMonthRateCnt(paramMap);
			}			
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/operThroughput.do")
	public String operThroughput(ModelMap param) {
		String forwardPage = "/itg/support/report/operThroughput.nvf";
		return forwardPage;
	}
	
	/**
	 * 처리자별 목표시간 내 처리율
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/support/report/selectOperThroughput.do")
	public @ResponseBody ResultVO selectOperThroughput(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "";
			userId = userVO.getUser_id();
			
			paramMap.put("user_id", userId);
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectOperThroughput(paramMap);		
			totalCount = reportService.selectOperThroughputCnt(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	// 20200311_이원재_002_신규작성
	@RequestMapping(value="/itg/support/report/satisfactionStatus.do")
	public String satisfactionStatus(ModelMap param) {
		String forwardPage = "/itg/support/report/satisfactionStatus.nvf";
		return forwardPage;
	}
	
	@RequestMapping(value="/itg/support/report/selectSatisfactionStatusList.do")
	public @ResponseBody ResultVO selectSatisfactionStatusList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectSatisfactionStatusList(paramMap);		
			totalCount = reportService.selectSatisfactionStatusCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/selectSatisfactionStatusExcel.do")
	public @ResponseBody ResultVO searchSatisfactionStatusExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			List tempArrayList = reportService.selectSatisfactionStatusList(param);
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/satisfactionStatistics.do")
	public String satisfactionStatistics(ModelMap param) {
		String forwardPage = "/itg/support/report/satisfactionStatistics.nvf";
		return forwardPage;
	}
	
	@RequestMapping(value="/itg/support/report/selectSatisfactionStatisticsList.do")
	public @ResponseBody ResultVO selectSatisfactionStatisticsList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectSatisfactionStatisticsList(paramMap);		
			totalCount = reportService.selectSatisfactionStatisticsCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/selectSatisfactionStatisticsDetail.do")
	public @ResponseBody ResultVO selectSatisfactionStatisticsDetail(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectSatisfactionStatisticsDetail(paramMap);		
			totalCount = reportService.selectSatisfactionStatisticsDetailCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/selectSatisfactionStatisticsExcel.do")
	public @ResponseBody ResultVO searchSatisfactionStatisticsExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			String type = (String) param.get("type");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			List tempArrayList = null;
			
			if("list".equals(type)) tempArrayList = reportService.selectSatisfactionStatisticsList(param);
			else if("detail".equals(type)) tempArrayList = reportService.selectSatisfactionStatisticsDetail(param);
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/successRatePerCategory.do")
	public String successRatePerCategory(ModelMap param) {
		String forwardPage = "/itg/support/report/successRatePerCategory.nvf";
		return forwardPage;
	}
	
	@RequestMapping(value="/itg/support/report/selectSuccessRatePerCategoryList.do")
	public @ResponseBody ResultVO selectSuccessRatePerCategoryList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectSuccessRatePerCategoryList(paramMap);		
			totalCount = reportService.selectSuccessRatePerCategoryCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/selectSuccessRatePerCategoryDetail.do")
	public @ResponseBody ResultVO selectSuccessRatePerCategoryDetail(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectSuccessRatePerCategoryDetail(paramMap);		
			totalCount = reportService.selectSuccessRatePerCategoryDetailCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/selectSuccessRatePerCategoryExcel.do")
	public @ResponseBody ResultVO searchSuccessRatePerCategoryExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			String type = (String) param.get("type");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			List tempArrayList = null;
			
			if("list".equals(type)) tempArrayList = reportService.selectSuccessRatePerCategoryList(param);
			else if("detail".equals(type)) tempArrayList = reportService.selectSuccessRatePerCategoryDetail(param);
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	// 20200311_이원재_002_신규작성 END
	/**
	 * 처리자별 목표시간 내 처리율
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/support/report/selectOperThroughputUserAll.do")
	public @ResponseBody ResultVO selectOperThroughputUserAll(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			/*UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "";
			userId = userVO.getUser_id();
			
			paramMap.put("login_user_id", userId);*/
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectOperThroughputUserAll(paramMap);		
			totalCount = reportService.selectOperThroughputUserAllCnt(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 서비스요청 2시간 이내 접수율 상세조회
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/support/report/searchTwoHourRceptRateDetail.do")
	public @ResponseBody ResultVO searchTwoHourRceptRateDetail(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			/*UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "";
			userId = userVO.getUser_id();
			
			paramMap.put("login_user_id", userId);*/
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.searchTwoHourRceptRateDetail(paramMap);		
			totalCount = reportService.searchTwoHourRceptRateDetailCnt(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	// 20200331_이원재_003_그룹사별 운영 작업건수
	@RequestMapping(value="/itg/support/report/operWorkPerCustomer.do")
	public String operWorkPerCustomer(ModelMap param) {
		String forwardPage = "/itg/support/report/operWorkPerCustomer.nvf";
		return forwardPage;
	}
	
	@RequestMapping(value="/itg/support/report/selectOperWorkPerCustomerList.do")
	public @ResponseBody ResultVO selectOperWorkPerCustomerList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectOperWorkPerCustomerList(paramMap);
			totalCount = reportService.selectOperWorkPerCustomerCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/selectOperWorkPerCustomerDetail.do")
	public @ResponseBody ResultVO selectOperWorkPerCustomerDetailList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			resultList = reportService.selectOperWorkPerCustomerDetailList(paramMap);
			totalCount = reportService.selectOperWorkPerCustomerDetailCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/support/report/selectOperWorkPerCustomerExcel.do")
	public @ResponseBody ResultVO selectOperWorkPerCustomerExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			String type = (String) param.get("type");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			List tempArrayList = null;
			
			if("list".equals(type)) tempArrayList = reportService.selectOperWorkPerCustomerList(param);
			else if("detail".equals(type)) tempArrayList = reportService.selectOperWorkPerCustomerDetailList(param);
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	// 20200331_이원재_003_그룹사별 운영 작업건수 END
	
	//개인별KPI 엑셀다운로드
	@RequestMapping(value="/itg/support/report/operThroughputExcelDown.do")
		public @ResponseBody ResultVO operThroughputExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
			String login_user_id = "";
			UserVO userVO = new UserVO();
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			List resultList = new ArrayList();
			ResultVO resultVO = new ResultVO();
			ModelMap excelMap = new ModelMap();
			
			Map excelParam = (Map) paramMap.get("excelParam");
			Map temp = (Map) paramMap.get("param");
			paramMap.put("login_user_id", userVO.getUser_id());
			try{
				//page=1, start=0, limit=10 있는 경우, 
				resultList = reportService.selectOperThroughput(temp);
				excelMap = excelMaker.makeExcelFile(excelParam, resultList);				
				resultVO.setResultMap(excelMap);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
			
		}	
	
	//개인별KPI 상세조회 엑셀다운로드
	@RequestMapping(value="/itg/support/report/operThroughputDetailExcelDown.do")
		public @ResponseBody ResultVO operThroughputDetailExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
			String login_user_id = "";
			UserVO userVO = new UserVO();
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			List resultList = new ArrayList();
			ResultVO resultVO = new ResultVO();
			ModelMap excelMap = new ModelMap();
			
			Map excelParam = (Map) paramMap.get("excelParam");
			Map temp = (Map) paramMap.get("param");
			paramMap.put("login_user_id", userVO.getUser_id());
			try{
				//page=1, start=0, limit=10 있는 경우, 
				resultList = reportService.selectOperThroughputUserAll(temp);
				excelMap = excelMaker.makeExcelFile(excelParam, resultList);				
				resultVO.setResultMap(excelMap);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
			
		}	
}
