/*
 * @(#)PostMatricServiceImpl.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.support.report.dao.ProperTimeProcessingDAO;
import com.nkia.itg.support.report.service.ProperTimeProcessingService;

@Service("properTimeProcessingService")
public class ProperTimeProcessingServiceImpl implements ProperTimeProcessingService {
	
	private static final Logger logger = LoggerFactory.getLogger(ProperTimeProcessingServiceImpl.class);
	
	@Resource(name="properTimeProcessingDAO")
	public ProperTimeProcessingDAO properTimeProcessingDAO;

	@Override
	public List searchReqName(Map param) throws NkiaException {
		return properTimeProcessingDAO.searchReqName(param);
	}
	
	@Override
	public List searchProperTimeMonth(Map paramMap) throws NkiaException {
		return properTimeProcessingDAO.searchProperTimeMonth(paramMap);
	}
	@Override
	public int searchProperTimeMonthCount(Map paramMap) throws NkiaException {
		return properTimeProcessingDAO.searchProperTimeMonthCount(paramMap);
	}
	@Override
	public List searchProperTimeDay(Map paramMap) throws NkiaException {
		return properTimeProcessingDAO.searchProperTimeDay(paramMap);
	}
	@Override
	public int searchProperTimeDayCount(Map paramMap) throws NkiaException {
		return properTimeProcessingDAO.searchProperTimeDayCount(paramMap);
	}	
	
	@Override
	public int searchDblClickCount(Map paramMap) throws NkiaException {
		return properTimeProcessingDAO.searchDblClickCount(paramMap);
	}
	@Override
	public List searchDblClick(Map paramMap) throws NkiaException {
		return properTimeProcessingDAO.searchDblClick(paramMap);
	}

	
	
	public List searchWorkReqDocListTree(Map paramMap) throws NkiaException {
	return properTimeProcessingDAO.searchWorkReqDocListTree(paramMap);
	}	
	
}
