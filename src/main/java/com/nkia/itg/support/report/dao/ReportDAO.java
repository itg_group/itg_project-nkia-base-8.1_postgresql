/*
 * @(#)PostMatricDAO.java              2013. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.report.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("reportDAO")
public class ReportDAO extends EgovComAbstractDAO  {
	
	public List searchReportList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("reportDAO.searchReportList", paramMap);
		return resultList;
	}
	
	public int searchReportListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("reportDAO.searchReportListCount", paramMap);
		return count;
	}
	
	public Map searchMonthlyReportMng(Map paramMap) throws NkiaException {
		return (Map)selectByPk("reportDAO.searchMonthlyReportMng", paramMap);
	}
	
	public Map searchMonthlyReportAtchId(Map paramMap) throws NkiaException {
		return (Map)selectByPk("reportDAO.searchMonthlyReportAtchId", paramMap);
	}
	
	public void insertMonthlyReportMng(Map paramMap) throws NkiaException {
		this.insert("reportDAO.insertMonthlyReportMng", paramMap);
	}
	
	public void updateMonthlyReportMng(Map paramMap) throws NkiaException {
		this.update("reportDAO.updateMonthlyReportMng", paramMap);
	}
	
	public List searchTwoHourDelay(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("reportDAO.searchTwoHourDelay", paramMap);
		return resultList;
	}
	
	public int searchTwoHourDelayCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("reportDAO.searchTwoHourDelayCnt", paramMap);
		return count;
	}
	
	public List searchTwoHourRceptDateRate(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("reportDAO.searchTwoHourRceptDateRate", paramMap);
		return resultList;
	}
	
	public int searchTwoHourRceptDateRateCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("reportDAO.searchTwoHourRceptDateRateCnt", paramMap);
		return count;
	}
	
	public List searchTwoHourRceptMonthRate(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("reportDAO.searchTwoHourRceptMonthRate", paramMap);
		return resultList;
	}
	
	public int searchTwoHourRceptMonthRateCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("reportDAO.searchTwoHourRceptMonthRateCnt", paramMap);
		return count;
	}
	
	public List selectOperThroughput(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("reportDAO.selectOperThroughput", paramMap);
		return resultList;
	}
	
	public int selectOperThroughputCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("reportDAO.selectOperThroughputCnt", paramMap);
		return count;
	}
	
	// 20200311_이원재_002_신규작성
	public List selectSatisfactionStatusList(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectSatisfactionStatusList", paramMap);
	}
	
	public int selectSatisfactionStatusCount(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectSatisfactionStatusCount", paramMap);
	}
	
	public List selectSatisfactionStatisticsList(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectSatisfactionStatisticsList", paramMap);
	}
	
	public int selectSatisfactionStatisticsCount(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectSatisfactionStatisticsCount", paramMap);
	}
	
	public List selectSatisfactionStatisticsDetail(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectSatisfactionStatisticsDetail", paramMap);
	}
	
	public int selectSatisfactionStatisticsDetailCount(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectSatisfactionStatisticsDetailCount", paramMap);
	}
	
	public List selectSuccessRatePerCategoryList(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectSuccessRatePerCategoryList", paramMap);
	}
	
	public int selectSuccessRatePerCategoryCount(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectSuccessRatePerCategoryCount", paramMap);
	}
	
	public List selectSuccessRatePerCategoryDetail(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectSuccessRatePerCategoryDetail", paramMap);
	}
	
	public int selectSuccessRatePerCategoryDetailCount(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectSuccessRatePerCategoryDetailCount", paramMap);
	}
	// 20200311_이원재_002_신규작성 END
	public List selectOperThroughputUserAll(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("reportDAO.selectOperThroughputUserAll", paramMap);
		return resultList;
	}
	
	public int selectOperThroughputUserAllCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("reportDAO.selectOperThroughputUserAllCnt", paramMap);
		return count;
	}
	
	public List searchTwoHourRceptRateDetail(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("reportDAO.searchTwoHourRceptRateDetail", paramMap);
		return resultList;
	}
	
	public int searchTwoHourRceptRateDetailCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("reportDAO.searchTwoHourRceptRateDetailCnt", paramMap);
		return count;
	}
	
	// 20200401_이원재_002_신규작성
	public List selectOperWorkPerCustomerList(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerList", paramMap);
	}
	
	public int selectOperWorkPerCustomerCount(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerCount", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res01(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res01", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res01(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res01", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res02(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res02", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res02(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res02", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res03(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res03", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res03(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res03", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res04(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res04", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res04(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res04", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res05(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res05", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res05(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res05", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res06(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res06", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res06(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res06", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res07(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res07", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res07(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res07", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res08(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res08", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res08(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res08", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res09(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res09", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res09(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res09", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res10(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res10", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res10(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res10", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res11(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res11", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res11(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res11", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res12(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res12", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res12(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res12", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res13(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res13", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res13(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res13", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res14(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res14", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res14(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res14", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res15(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res15", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res15(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res15", paramMap);
	}
	
	public List selectOperWorkPerCustomerDetailList_Res16(Map paramMap) throws NkiaException {
		return this.list("reportDAO.selectOperWorkPerCustomerDetailList_Res16", paramMap);
	}
	
	public int selectOperWorkPerCustomerDetailCount_Res16(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("reportDAO.selectOperWorkPerCustomerDetailCount_Res16", paramMap);
	}
	// 20200401_이원재_002_신규작성 END
}
