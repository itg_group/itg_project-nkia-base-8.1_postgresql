/*
 * @(#)DashboardServiceImpl.java              2017. 11. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.dashboard.service.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.support.dashboard.dao.DashboardDAO;
import com.nkia.itg.support.dashboard.service.DashboardService;
import com.nkia.itg.system.listWidget.service.impl.ListWidgetServiceImpl;

@Service("DashboardService")
public class DashboardServiceImpl implements DashboardService {
	
private static final Logger logger = LoggerFactory.getLogger(ListWidgetServiceImpl.class);
	
	@Resource(name = "dashboardDAO")
	public DashboardDAO dashboardDAO;
	
	
}
