/*
 * @(#)DashboardDAO.java              2017. 11. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.dashboard.dao;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("dashboardDAO")
public class DashboardDAO extends EgovComAbstractDAO {
	
	
}
