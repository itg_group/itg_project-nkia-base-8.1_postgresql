package com.nkia.itg.support.dashboard.vo;

public class DashboardVO {
	
	private String day;
	
	private String req_type;
	
	private String start_dt;
	
	private String end_dt;
	
	private String select;
	
	private String im_lvl;
	
	private String p_work_state;
	
	private String date_type;
	
	private String result_type;
	
	private String notYet_job_yn;
	
	private String search_month;
	
	private String search_year;
	
	private String page_gubun;
	
	private String search_date_gubun;
	
	private String service_type;
	
	
	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getReq_type() {
		return req_type;
	}

	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}

	public String getStart_dt() {
		return start_dt;
	}

	public void setStart_dt(String start_dt) {
		this.start_dt = start_dt;
	}

	public String getEnd_dt() {
		return end_dt;
	}

	public void setEnd_dt(String end_dt) {
		this.end_dt = end_dt;
	}

	public String getSelect() {
		return select;
	}

	public void setSelect(String select) {
		this.select = select;
	}

	public String getIm_lvl() {
		return im_lvl;
	}

	public void setIm_lvl(String im_lvl) {
		this.im_lvl = im_lvl;
	}

	public String getP_work_state() {
		return p_work_state;
	}

	public void setP_work_state(String p_work_state) {
		this.p_work_state = p_work_state;
	}

	public String getDate_type() {
		return date_type;
	}

	public void setDate_type(String date_type) {
		this.date_type = date_type;
	}

	public String getResult_type() {
		return result_type;
	}

	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}

	public String getNotYet_job_yn() {
		return notYet_job_yn;
	}

	public void setNotYet_job_yn(String notYet_job_yn) {
		this.notYet_job_yn = notYet_job_yn;
	}

	public String getSearch_month() {
		return search_month;
	}

	public void setSearch_month(String search_month) {
		this.search_month = search_month;
	}

	public String getSearch_year() {
		return search_year;
	}

	public void setSearch_year(String search_year) {
		this.search_year = search_year;
	}

	public String getPage_gubun() {
		return page_gubun;
	}

	public void setPage_gubun(String page_gubun) {
		this.page_gubun = page_gubun;
	}
	
	public String getSearch_date_gubun() {
		return search_date_gubun;
	}

	public void setSearch_date_gubun(String search_date_gubun) {
		this.search_date_gubun = search_date_gubun;
	}
	
	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	@Override
	public String toString() {
		return "DashboardVO [day=" + day + ", req_type=" + req_type
				+ ", start_dt=" + start_dt + ", end_dt=" + end_dt + ", select="
				+ select + ", im_lvl=" + im_lvl + ", p_work_state="
				+ p_work_state + ", date_type=" + date_type + ", result_type="
				+ result_type + ", notYet_job_yn=" + notYet_job_yn
				+ ", search_month=" + search_month + ", search_year="
				+ search_year + ", page_gubun=" + page_gubun
				+ ", search_date_gubun=" + search_date_gubun
				+ ", service_type=" + service_type + "]";
	}
	

}