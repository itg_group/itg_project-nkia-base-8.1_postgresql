package com.nkia.itg.support.dashboard.constraint;

public enum DashboardEnum {
	
	REQ_TYPE_AP("AP"),
	REQ_TYPE_AP_EXTRL("AP_EXTRL"),
	REQ_TYPE_INFRA("INFRA"),
	REQ_TYPE_SERVICE("SERVICE"),
	REQ_TYPE_INC("INCIDENT"),
	REQ_TYPE_PRO("PROBLEM"),
	DEFAULT_DATE_SEARCH("SCH_REQ_DT"),
	SERVICE_SUB_TYPE_GRP_CODE("SERVICE_SUB_TYPE"),
	DEFAULT_RESULT_TYPE("None")
	;
	
	private String CODE_ID;

	private DashboardEnum(String CODE_ID) {
		this.CODE_ID = CODE_ID;
	}

	public String getCODE_ID() {
		return CODE_ID;
	}
	
}
