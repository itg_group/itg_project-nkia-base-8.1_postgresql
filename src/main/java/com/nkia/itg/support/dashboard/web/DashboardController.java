/*
 * @(#)DashboardController.java              2017. 11. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.support.dashboard.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;

import com.nkia.itg.support.dashboard.service.DashboardService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class DashboardController {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="DashboardService")
	private DashboardService dashboardService;
	
	
}
