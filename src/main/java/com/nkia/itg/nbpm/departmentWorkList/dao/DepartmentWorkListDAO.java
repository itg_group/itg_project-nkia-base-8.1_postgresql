/*
 * @(#)MyListDAO.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.departmentWorkList.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("departmentWorkListDAO")
public class DepartmentWorkListDAO extends NbpmAbstractDAO {

	public int searchReadyDepartmentWorkListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("departmentWorkListDAO.searchReadyDepartmentWorkListCount", paramMap);
	}

	public List searchReadyDepartmentWorkList(ModelMap paramMap) throws SQLException {
		return list("departmentWorkListDAO.searchReadyDepartmentWorkList", paramMap);
	}
}
