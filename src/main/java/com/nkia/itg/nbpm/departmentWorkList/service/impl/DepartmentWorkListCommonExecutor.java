/*
 * @(#)MyListServiceImpl.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.departmentWorkList.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.departmentWorkList.dao.DepartmentWorkListDAO;
import com.nkia.itg.nbpm.departmentWorkList.service.DepartmentWorkListExecutor;

@Service("departmentWorkListCommonExecutor")
public class DepartmentWorkListCommonExecutor implements DepartmentWorkListExecutor {

	@Resource(name = "departmentWorkListDAO")
	private DepartmentWorkListDAO departmentWorkListDAO;
	
	@Resource(name = "nbpmCommonDAO")
	private NbpmCommonDAO nbpmCommonDAO;
	
	public int searchReadyDepartmentWorkListCount(ModelMap paramMap) throws NkiaException, SQLException {
		return departmentWorkListDAO.searchReadyDepartmentWorkListCount(paramMap);
	}

	public List searchReadyDepartmentWorkList(ModelMap paramMap) throws NkiaException, SQLException {
		return departmentWorkListDAO.searchReadyDepartmentWorkList(paramMap);
	}
	
	public int updateClaimTask(ModelMap paramMap, String loginUserId) throws NkiaException {
		int cnt = 0;
		List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("taskList");
		
		for(int i=0; i < paramList.size(); i++){
			HashMap map = paramList.get(i);
			map.put("user_id", loginUserId);
			
			cnt = nbpmCommonDAO.updateClaimTask(map);
			if(cnt > 0) {
				cnt = nbpmCommonDAO.updateClaimProcOper(map);
			}
		}
		return cnt;
	}
}
