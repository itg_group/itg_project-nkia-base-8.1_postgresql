/*
 * @(#)MyListService.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.departmentWorkList.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DepartmentWorkListExecutor {

	public int searchReadyDepartmentWorkListCount(ModelMap paramMap) throws NkiaException, SQLException;
	
	public List searchReadyDepartmentWorkList(ModelMap paramMap) throws NkiaException, SQLException;
	
	public int updateClaimTask(ModelMap paramMap, String loginUserId) throws NkiaException;

}
