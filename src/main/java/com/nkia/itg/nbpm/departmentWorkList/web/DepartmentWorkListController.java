/*
 * @(#)DepartmentWorkListController.java              2016. 6. 15.
 *
 * Copyright 2016 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.departmentWorkList.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.departmentWorkList.service.DepartmentWorkListExecutor;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class DepartmentWorkListController {
	
	@Resource(name="departmentWorkListCommonExecutor")
	private DepartmentWorkListExecutor departmentWorkListExecutor;
	
	@Resource(name="nbpmCommonClient")
	private NbpmClient nbpmCommonClient;
	
	@RequestMapping(value="/itg/nbpm/goDepartmentWorkListTab.do")
	public String goServicedeskTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/departmentWorkList/departmentWorkListTab.nvf";
		paramMap.put("task_id", request.getParameter("task_id"));
		paramMap.put("sr_id", request.getParameter("sr_id"));
		paramMap.put("list_type", request.getParameter("list_type"));
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/goDepartmentWorkListMain.do")
	public String goDepartmentWorkListMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/departmentWorkList/departmentWorkListMain.nvf";
		try {
			
			String user_id = request.getParameter("user_id");
			
			paramMap.put("list_type", request.getParameter("list_type"));
			
			NbpmProcessVO processVO = new NbpmProcessVO();
			
			paramMap.put("processVO", processVO);
			paramMap.put("nbpm_user_id", user_id);
			paramMap.put("nbpm_task_name", NbpmConstraints.TASK_NAME_START);
			paramMap.put("nbpm_process_type", processVO.getProcess_type());
			paramMap.put("nbpm_processId", processVO.getProcessId());
			paramMap.put("nbpm_processInstanceId", processVO.getProcessInstanceId());
			paramMap.put("nbpm_version", processVO.getVersion());
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	 
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchMyDepartmentList.do")
	public @ResponseBody ResultVO searchMyList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			
			String listType = (String)paramMap.get("list_type");
			paramMap.put("user_id", userId);
			PagingUtil.getFristEndNum(paramMap);
			
			if(NbpmConstraints.LIST_TYPE_READY.equals(listType)) {
				totalCount = departmentWorkListExecutor.searchReadyDepartmentWorkListCount(paramMap);
				resultList = departmentWorkListExecutor.searchReadyDepartmentWorkList(paramMap);
			}
			
			for(int i = 0 ; resultList != null && i < resultList.size() ; i++) {
				Map srData = (Map)resultList.get(i);
				String endType = (String)srData.get("REJECT_YN");
				if("Y".equals(endType)) {
					srData.put("SR_ID", "<font style='color:#FF0000'>"+(String)srData.get("SR_ID")+"</font>");
				}
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true); 
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	// 부서업무를 나에게 할당하기.
	@RequestMapping(value="/itg/nbpm/updateClaimTask.do")
	public @ResponseBody ResultVO updateClaimTask(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		int resultCnt = 0;
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			resultCnt = departmentWorkListExecutor.updateClaimTask(paramMap, loginUserId);
			
			
			resultVO.setResultInt(resultCnt);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
