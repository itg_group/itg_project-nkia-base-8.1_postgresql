/*
 * @(#)MyListServiceImpl.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.mylist.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.mylist.dao.MyListDAO;
import com.nkia.itg.nbpm.mylist.service.MyListExecutor;

@Service("myListCommonExecutor")
public class MyListCommonExecutor implements MyListExecutor {

	@Resource(name = "myListDAO")
	private MyListDAO myListDAO;
	
	public int searchReadyMyListCount(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchReadyMyListCount(paramMap);
	}

	public List searchReadyMyList(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchReadyMyList(paramMap);
	}

	@Override
	public int searchIngMyListCount(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchIngMyListCount(paramMap);
	}

	@Override
	public List searchIngMyList(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchIngMyList(paramMap);
	}

	@Override
	public int searchCompleteMyListCount(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchCompleteMyListCount(paramMap);
	}

	@Override
	public List searchCompleteMyList(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchCompleteMyList(paramMap);
	}
	
	public List searchMyCalendarList(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchMyCalendarList(paramMap);
	}
	
	public String searchSrReceiptYn(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchSrReceiptYn(paramMap);
	}
	
	public Map selectHomeTaskPageParam(HashMap paramMap) throws NkiaException, SQLException {
		return myListDAO.selectHomeTaskPageParam(paramMap);
	}
	
	@Override
	public int searchHomeMultiListCount(Map paramMap) throws NkiaException, SQLException {
		return myListDAO.searchHomeMultiListCount(paramMap);
	}
	
	@Override
	public String searchHomeMultiList(Map paramMap) throws NkiaException, SQLException {
		return myListDAO.searchHomeMultiList(paramMap);
	}
	
	@Override
	public List searchChangeAppmList(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchChangeAppmList(paramMap);
	}
	
	@Override
	public List searchtestid(ModelMap paramMap) throws NkiaException, SQLException {
		return myListDAO.searchtestid(paramMap);
	}
//	
//	@Override
//	public Map searchtestid(ModelMap paramMap) throws NkiaException {
//		return myListDAO.searchtestid(paramMap);
//	}
}
