/*
 * @(#)MyListService.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.mylist.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface MyListExecutor {

	public int searchReadyMyListCount(ModelMap paramMap) throws NkiaException, SQLException;
	
	public List searchReadyMyList(ModelMap paramMap) throws NkiaException, SQLException;

	public int searchIngMyListCount(ModelMap paramMap) throws NkiaException, SQLException;

	public List searchIngMyList(ModelMap paramMap) throws NkiaException, SQLException;

	public int searchCompleteMyListCount(ModelMap paramMap) throws NkiaException, SQLException;

	public List searchCompleteMyList(ModelMap paramMap) throws NkiaException, SQLException;
	
	public List searchMyCalendarList(ModelMap paramMap) throws NkiaException, SQLException;

	public String searchSrReceiptYn(ModelMap paramMap) throws NkiaException, SQLException;
	
	public Map selectHomeTaskPageParam(HashMap paramMap) throws NkiaException, SQLException;
	
	public int searchHomeMultiListCount(Map paramMap) throws NkiaException, SQLException;
	
	public String searchHomeMultiList(Map paramMap) throws NkiaException, SQLException;
	
	public List searchChangeAppmList(ModelMap paramMap) throws NkiaException, SQLException;
	
	public List searchtestid(ModelMap paramMap) throws NkiaException, SQLException;
	
	//public Map searchtestid(ModelMap paramMap) throws NkiaException;
}
