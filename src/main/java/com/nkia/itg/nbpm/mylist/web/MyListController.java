/*
 * @(#)MyListController.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.mylist.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Calendar;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.gui.designer.GuiDesignerUtil;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.mylist.service.MyListExecutor;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class MyListController {
	private static final Logger logger = LoggerFactory.getLogger(MyListController.class);
	
	@Resource(name="myListCommonExecutor")
	private MyListExecutor myListExecutor;
	
	@Resource(name="nbpmCommonClient")
	private NbpmClient nbpmCommonClient;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;

	
	@RequestMapping(value="/itg/nbpm/goMyListTab.do")
	public String goServicedeskTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if(!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = userVO.getUser_id();
		String forwarPage = "/itg/nbpm/mylist/myListTab.nvf";
		
		paramMap.put("task_id", request.getParameter("task_id"));
		paramMap.put("sr_id", request.getParameter("sr_id"));
		paramMap.put("list_type", request.getParameter("list_type"));
		paramMap.put("login_user_id", userId);
		
		//이번달 종료 상태인 요청건 조회
		String main_search_yn = (String) request.getParameter("main_search_yn");
		if (main_search_yn != null && !"".equals(main_search_yn) ) {
			paramMap.put("main_search_yn", main_search_yn);
		} else {
			paramMap.put("main_search_yn", "N");
		}
		
		//만족도미평가
		String req_statis_chk = (String) request.getParameter("req_statis_chk");
		if (req_statis_chk != null && !"".equals(req_statis_chk) ) {
			paramMap.put("req_statis_chk", req_statis_chk);
		} else {
			paramMap.put("req_statis_chk", "N");
		}
		
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/goMyListMain.do")
	public String goMyListMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/mylist/myListMain.nvf";
		try {
			String userId = request.getParameter("user_id");
			if(userId == null || "".equals(userId)) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				userId = userVO.getUser_id();
			}
			paramMap.put("list_type", request.getParameter("list_type"));
			paramMap.put("login_user_id", userId);
			
			//이번달 종료 상태인 요청건 조회
			String main_search_yn = (String) request.getParameter("main_search_yn");
			if (main_search_yn != null && !"".equals(main_search_yn) ) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar currentCalendar = Calendar.getInstance();
				//이번달 첫날
				int year = currentCalendar.get(Calendar.YEAR);
				int month = currentCalendar.get(Calendar.MONTH) + 1;
				String date = "01";
				
				String startDay = Integer.toString(year) + "-" +
		                 ((month<10) ? "0" + Integer.toString(month) : Integer.toString(month)) + "-" +
		                 date;
				Date convertSDay = sdf.parse(startDay);
				startDay = sdf.format(convertSDay);
				
				//이번달 마지막날 
		        int endDay = currentCalendar.getActualMaximum(Calendar.DAY_OF_MONTH );
		        
		        String lastDay = Integer.toString(year) + "-" +
		                 ((month<10) ? "0" + Integer.toString(month) : Integer.toString(month)) + "-" +
		                 Integer.toString(endDay);
				Date convertEDay = sdf.parse(lastDay);
				lastDay = sdf.format(convertEDay);

		        paramMap.put("end_dt_startDate", startDay);
		        paramMap.put("end_dt_endDate", lastDay);
				paramMap.put("main_search_yn", main_search_yn);
			} else {
				paramMap.put("main_search_yn", "N");
			}
			
			//만족도미평가
			String req_statis_chk = (String) request.getParameter("req_statis_chk"); 
			if (req_statis_chk != null && !"".equals(req_statis_chk) ) {
				paramMap.put("req_statis_chk", req_statis_chk);
			} else {
				paramMap.put("req_statis_chk", "N");
			}
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	@RequestMapping(value="/itg/nbpm/goChangeAppm.do")
	public String goChangeAppm(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if(!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/nbpm/mylist/changeAppmManger.nvf";
		return forwarPage;
	}

	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchMyList.do")
	public @ResponseBody ResultVO searchMyList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String custId = userVO.getCust_id();
			
			int totalCount = 0;
			
			String listType = (String)paramMap.get("list_type");
			paramMap.put("user_id", userId);
			paramMap.put("cust_id", custId);
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("recallType",NkiaApplicationPropertiesMap.getProperty("Process.Recall.Type"));
			
			if(NbpmConstraints.LIST_TYPE_READY.equals(listType)) {
				totalCount = myListExecutor.searchReadyMyListCount(paramMap);
				resultList = myListExecutor.searchReadyMyList(paramMap);
			} else if(NbpmConstraints.LIST_TYPE_ING.equals(listType)) {
				totalCount = myListExecutor.searchIngMyListCount(paramMap);
				resultList = myListExecutor.searchIngMyList(paramMap);
			} else if(NbpmConstraints.LIST_TYPE_COMPLETE.equals(listType)) {
				totalCount = myListExecutor.searchCompleteMyListCount(paramMap);
				resultList = myListExecutor.searchCompleteMyList(paramMap);
			}
			
			for(int i = 0 ; resultList != null && i < resultList.size() ; i++) {
				Map srData = (Map)resultList.get(i);
				String endType = (String)srData.get("REJECT_YN");
				if("Y".equals(endType)) {
					srData.put("SR_ID", "<font style='color:#FF0000'>"+(String)srData.get("SR_ID")+"</font>");
				}
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true); 
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	@RequestMapping(value="/itg/nbpm/mylist/completeSelectedConfirmTask.do")
	public @ResponseBody ResultVO processRequest(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			nbpmCommonClient.processTaskList(paramMap, loginUserId);
			
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 요청진행함(달력) 화면
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/goMyCalendarMonth.do")
	public String goMyCalendarMonth(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/mylist/myCalendarMonth";
		try {
			String userId = request.getParameter("user_id");
			if(userId == null || "".equals(userId)) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				userId = userVO.getUser_id();
			}
			paramMap.put("login_user_id", userId);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 
	 * 요청진행함(달력) 데이타조회
	 * 
	 * @param paramMap {calendar_startDate, calendar_endDate}
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchMyCalendarList.do")
	public @ResponseBody ResultVO searchMyCalendarList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String custId = userVO.getCust_id();
			paramMap.put("user_id", userId);
			paramMap.put("cust_id", custId);
			List outList1 = myListExecutor.searchMyCalendarList(paramMap);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("outList1", outList1);
			result.setResultMap(resultMap);
			result.setSuccess(true); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	/**
	 * 접수 여부 확인
	 * 
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchSrReceiptYn.do")
	public @ResponseBody ResultVO searchSrReceiptYn(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO result = new ResultVO();
		String resultYn = "";
		try {
			resultYn = myListExecutor.searchSrReceiptYn(paramMap);
			result.setResultString(resultYn);
			result.setSuccess(true);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return result;
	}
	
	/**
	 * 메인에서 요청의 상세페이지로 이동한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/goHomeTaskPage.do")
	public String goHomeTaskPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";
		try {
			NbpmProcessVO processVO = null;
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			String sr_id = request.getParameter("sr_id");
			
			int checkCnt = 0;
			Map searchMap = new HashMap();
			searchMap.put("sr_id", sr_id);
			searchMap.put("login_user_id", userId);
			
			// 멀티티켓 중 나의 티켓이 있는지 확인
			checkCnt = myListExecutor.searchHomeMultiListCount(searchMap);
			
			String nbpmTaskId = "";
			if(checkCnt > 0){
				// 멀티티켓 중 나의 티켓이 있는 경우, 멀티 티켓 가져오기
				nbpmTaskId = myListExecutor.searchHomeMultiList(searchMap);
				
			}else{
				nbpmTaskId = processCommonExecutor.selectLastTaskId(sr_id);
				
			}
			
			//String nbpmTaskId = processCommonExecutor.selectLastTaskId(sr_id);
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			paramVO.setSr_id(sr_id);
			
			if(nbpmTaskId != null) {
				paramVO.setTask_id(Long.parseLong(nbpmTaskId));
			} else {
				paramVO.setTask_id(0);
			}
			
			processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			
			String grpSlctyn = processVO.getGrp_slct_yn();
			String actualownerId = processVO.getCurrent_worker_id();
			String claimYn = ""; 
			
			if("Y".equals(grpSlctyn) && !userId.equals(actualownerId)){
				claimYn = "Y";
			}else{
				claimYn = "N";
			}
			
			String taskName = null;
			
			if(userId.equals(processVO.getCurrent_worker_id())) {
				//데이터를 확인 하였으므로 접수일 시 및 Readed 를 업데이트 한다.
				processCommonExecutor.updateAcceptDate(paramVO);
			}
			
			//페이지에 대한 사용자의 권한을 체크한다.
			boolean authFlag = checkUserPageAuth(processVO, userVO);
			if (authFlag) {
				//포워딩하려는 페이지 경로를 획득한다.
				forwardPath = NbpmUtil.parseForwardPage(
						processVO.getProcess_type(),
						processVO.getSub_req_type(), processVO.getVersion(),
						processVO.getTask_name());
			} else {
				//권한이 없을 경우 뷰전용 페이지로 이동한다.
				processVO.setTask_name(NbpmConstraints.END_VIEW_PAGE);
				forwardPath = NbpmUtil.parseForwardPage(
						processVO.getProcess_type(),
						processVO.getSub_req_type(), processVO.getVersion(),
						NbpmConstraints.END_VIEW_PAGE.toUpperCase());
			}
			
			paramMap.put("processVO", processVO);
			paramMap.put("user_id", userId);
			
			//프로세스 화면 자동화에서 사용하기 위한 dataMpa 데이터를 세팅함.
			HashMap<String, Object> dataMap = NbpmUtil.getDataMapForTaskPage(processVO, userVO);
			dataMap.put("claim_yn", claimYn);
			dataMap.put("sub_req_type", processVO.getSub_req_type());
			paramMap.put("dataMap", dataMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
		return forwardPath;
	}
	
	/**
	 * 나의 할일 리스트 - 현재 태스크명 조회(탭명)
	 * 
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/selectTaskName.do")
	public @ResponseBody ResultVO selectTaskName(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO result = new ResultVO();
		Map<String, String> resultMap = new HashMap();
		try {
			NbpmProcessVO processVO = null;
			String sr_id = (String)paramMap.get("sr_id");
			
			String nbpmTaskId = processCommonExecutor.selectLastTaskId(sr_id);
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			paramVO.setSr_id(sr_id);
			
			if(nbpmTaskId != null) {
				paramVO.setTask_id(Long.parseLong(nbpmTaskId));
			} else {
				paramVO.setTask_id(0);
			}
			
			processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			
			resultMap.put("sr_id", processVO.getSr_id() );
			resultMap.put("task_name", processVO.getNodename());
			
			result.setResultMap(resultMap);
			result.setSuccess(true);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return result;
	}
	
	/**
	 * 요청의 상세페이지 이동 시 로그인 사용자의 권한의 체크한다. 
	 * @param processVO
	 * @param userVO
	 * @return
	 */
	private boolean checkUserPageAuth(NbpmProcessVO processVO, UserVO userVO) {
		boolean result = false;
		String status = "";
		String currentWorkerId = "";
		
		if(processVO.getStatus() != null) {
			status = processVO.getStatus();
		}
		if(processVO.getCurrent_worker_id() != null) {
			currentWorkerId = processVO.getCurrent_worker_id();
		}
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(!"Completed".equals(status) && userVO.getUser_id().equals(currentWorkerId)) {
			result = true;
		}
		
		return result;
	}
	
	/**
	 * 
	 * 변경담당자 변경할 리스트 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchChangeAppmList.do")
	public @ResponseBody ResultVO searchChangeAppmList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();

			
			paramMap.put("user_id", userId);
			resultList = myListExecutor.searchChangeAppmList(paramMap);
			gridVO.setRows(resultList);
			result.setGridVO(gridVO);
			result.setSuccess(true); 
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	/**
	 * 
	 * 변경담당자 변경할 리스트 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
//	@RequestMapping(value="/itg/nbpm/test.do")
//	public @ResponseBody ResultVO test(@RequestBody ModelMap paramMap) throws NkiaException {
//		List resultList = new ArrayList();
//		GridVO gridVO = new GridVO();
//		ResultVO result = new ResultVO();
//		String body ="";
//		try{
//			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
//			String userId = userVO.getUser_id();
//
//			
//			paramMap.put("user_id", userId);
//			resultList = myListExecutor.searchtestid(paramMap);
//			gridVO.setRows(resultList);
//			result.setGridVO(gridVO);
//			result.setSuccess(true); 
//			body = JsonMapper.toJson(resultList);
//			
//		} catch(Exception e) {
//			throw new NkiaException(e);
//		}
//		return result;
//	}
	
	@RequestMapping(value="/itg/nbpm/suggestSample.do")
	public void testJson(HttpServletRequest request, HttpServletResponse response){
//		List resultList = new ArrayList();
//		ModelMap paramMap = new ModelMap();
//		try {
//			String userNm = request.getParameter("params");
//			paramMap.put("user_nm", userNm);
//			resultList = myListExecutor.searchtestid(paramMap);
//			response.setContentType("application/json");
//			response.setCharacterEncoding("utf-8");
//			JSONArray data_list2 = JSONArray.fromObject(resultList);
//			String sample = data_list2.toString();
//			sample = sample.replaceAll("\"ID\"", "\"id\"");
//			sample = sample.replaceAll("\"VALUE\"", "\"value\"");
//			JSONArray data_list3 = JSONArray.fromObject(sample);
//			PrintWriter out;
//			out = response.getWriter();
//			out.println(data_list3);
//			out.flush();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
	}

}
