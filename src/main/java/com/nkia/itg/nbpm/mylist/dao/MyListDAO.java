/*
 * @(#)MyListDAO.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.mylist.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("myListDAO")
public class MyListDAO extends NbpmAbstractDAO {

	public int searchReadyMyListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("MyListDAO.searchReadyMyListCount", paramMap);
	}

	public List searchReadyMyList(ModelMap paramMap) throws SQLException {
		return list("MyListDAO.searchReadyMyList", paramMap);
	}

	public int searchIngMyListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("MyListDAO.searchIngMyListCount", paramMap);
	}

	public List searchIngMyList(ModelMap paramMap) throws SQLException {
		return list("MyListDAO.searchIngMyList", paramMap);
	}

	public int searchCompleteMyListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("MyListDAO.searchCompleteMyListCount", paramMap);
	}

	public List searchCompleteMyList(ModelMap paramMap) throws SQLException {
		return list("MyListDAO.searchCompleteMyList", paramMap);
	}

	public List searchMyCalendarList(ModelMap paramMap) throws SQLException {
		return list("MyListDAO.searchMyCalendarList", paramMap);
	}
	
	public String searchSrReceiptYn(ModelMap paramMap) throws SQLException {
		return (String) selectByPk("MyListDAO.searchSrReceiptYn", paramMap);
	}
	
	public Map selectHomeTaskPageParam(HashMap paramMap) throws NkiaException {
		return(Map)selectByPk("MyListDAO.selectHomeTaskPageParam", paramMap);
	}
	
	public int searchHomeMultiListCount(Map paramMap) throws SQLException {
		return (Integer)selectByPk("MyListDAO.searchHomeMultiListCount", paramMap);
	}
	
	public String searchHomeMultiList(Map paramMap) throws NkiaException {
		return(String)selectByPk("MyListDAO.searchHomeMultiList", paramMap);
	}
	public List searchChangeAppmList(ModelMap paramMap) throws SQLException {
		return list("MyListDAO.searchChangeAppmList", paramMap);
	}
	public List searchtestid(ModelMap paramMap) throws SQLException {
		return list("MyListDAO.searchtestid", paramMap);
	}
}
