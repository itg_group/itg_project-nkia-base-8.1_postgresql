package com.nkia.itg.nbpm.provide.common.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public interface NbpmClient {

	public String createSrId() throws NkiaException;
	
	public String createSrId(String prefix) throws NkiaException;
	
	public String registerProcess(Map param) throws NkiaException;

	public String selectBpmnXmlData(String processId) throws NkiaException, SQLException;
	
	public void processTaskList(Map paramMap, String workerId) throws NkiaException;
	
	public void completeTask(NbpmProcessVO processVO) throws NkiaException;
	
	public void workTaskForRule(Map paramMap) throws NkiaException, SQLException, Exception;
	
	public void updateProcMasterWorkState(NbpmProcessVO processVO, ArrayList nextNodeIds) throws SQLException, Exception;
	
	public void setNextTaskInfoMap(Map resultMap, NbpmProcessVO processVO) throws SQLException, Exception;

	public Map createMultiTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	public void registerUser(NbpmProcessVO processVO, Map paramMap) throws NkiaException;
	
	/**
	 * 조건 : 요청번호
	 * 임시등록된 데이터 조회
	 */
	public Map selectTempSrData(String srId) throws NkiaException;
	
	/**
	 * 조건 : 요청번호
	 * 요청번호 조건으로 요청의 구성명 조회
	 */
	public String selectSrConfNameData(String srId) throws NkiaException;

	void notifyTask(NbpmProcessVO processVO, Map result) throws NkiaException;
	
}
