/*
 * @(#)NbpmSampleProviderService.java              2018. 9. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.provide.common.service;

import java.sql.SQLException;

import com.nkia.itg.base.application.exception.NkiaException;


public interface NbpmSampleProviderService {

	public String selectSampleSGApprData(String sr_id) throws NkiaException, SQLException;
	
}
