/*
 * @(#)NbpmSampleProviderServiceImpl.java              2018. 9. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.provide.common.service.impl;

import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.provide.common.dao.NbpmSampleProviderDAO;
import com.nkia.itg.nbpm.provide.common.service.NbpmSampleProviderService;

@Service("nbpmSampleProviderService")
public class NbpmSampleProviderServiceImpl implements NbpmSampleProviderService {

	@Resource(name = "nbpmSampleProviderDAO")
	private NbpmSampleProviderDAO nbpmSampleProviderDAO;
	
	public String selectSampleSGApprData(String sr_id) throws NkiaException, SQLException {
		return nbpmSampleProviderDAO.selectSampleSGApprData(sr_id);
	}

}
