/*
 * @(#)NbpmProviderController.java              2013. 6. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.provide.common.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.service.NbpmProviderService;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.system.service.SystemService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class NbpmProviderController {

	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "systemService")
	private SystemService systemService;
	
	@Resource(name="nbpmProviderService")
	private NbpmProviderService nbpmProviderService;
	
	//@@20130711 김도원 START
	//현황 또는 요청이력 페이지에서 요청상세화면 전환하는 기능
	@RequestMapping(value="/itg/nbpm/provide/goDetailPage.do")
	public String goDetailPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		
		String forwardPath = "";
		
		try {
			NbpmProcessVO processVO = null;
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String srId = request.getParameter("sr_id");
			String type = request.getParameter("type");
			String sub_ty_cd = request.getParameter("sub_ty_cd");
			String reqDocId = request.getParameter("req_doc_id");
			String customerId = request.getParameter("customer_id");
			String nbpmTaskId = processCommonExecutor.selectLastTaskId(srId);
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			paramVO.setSr_id(srId);
			if(nbpmTaskId != null) {
				paramVO.setTask_id(Long.parseLong(nbpmTaskId));
			} else {
				paramVO.setTask_id(0);
			}

			processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			String taskName = null;
			if(processVO != null) {
				taskName = getTaskNameForForwardPath(type, processVO.getWork_state(), userId);
			} else {
				taskName = NbpmConstraints.REQUEST_PAGE;
				processVO = processCommonExecutor.selectRequestPageInfo(paramVO);
				processVO.setSr_id(srId);
				processVO.setTask_name(NbpmConstraints.TASK_NAME_START);
				processVO.setWork_state(NbpmConstraints.WORK_STATE_TEMP);
			}
			
			String processType = processVO.getProcess_type();
			String subReqType = processVO.getSub_req_type();
			String version = processVO.getVersion();
			
			forwardPath = NbpmUtil.parseForwardPage(processType, subReqType, version, taskName);
			HashMap<String, Object> dataMap = NbpmUtil.getDataMapForTaskPage(processVO, userVO);
			dataMap.put("myhistorylist_yn", "Y");
			dataMap.put("ptype", type);
			dataMap.put("sub_ty_cd", reqDocId);
			//dataMap.put("nbpm_task_name", taskName);
			paramMap.put("dataMap", dataMap);
			
		} catch(Exception e) {
			new NkiaException(e);
		}		
		return forwardPath;
	}
	//@@20130711 김도원 START
	//현황 또는 요청이력 페이지에서 요청상세화면 전환하는 기능
	@RequestMapping(value="/itg/nbpm/provide/goDetailPageExternalConnection.do")
	public String goDetailPageExternalConnection(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		
		String forwardPath = "";
		
		try {
			NbpmProcessVO processVO = null;
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "ExternalConnection";
			String srId = request.getParameter("sr_id");
			String type = request.getParameter("type");
			String nbpmTaskId = processCommonExecutor.selectLastTaskId(srId);
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			paramVO.setSr_id(srId);
			if(nbpmTaskId != null) {
				paramVO.setTask_id(Long.parseLong(nbpmTaskId));
			} else {
				paramVO.setTask_id(0);
			}

			processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			String taskName = null;
			if(processVO != null) {
				taskName = getTaskNameForForwardPath(type, processVO.getWork_state(), userId);
			} else {
				taskName = NbpmConstraints.REQUEST_PAGE;
				processVO = processCommonExecutor.selectRequestPageInfo(paramVO);
				processVO.setSr_id(srId);
				processVO.setTask_name(NbpmConstraints.TASK_NAME_START);
				processVO.setWork_state(NbpmConstraints.WORK_STATE_TEMP);
			}
			
			String processType = processVO.getProcess_type();
			String subReqType = processVO.getSub_req_type();
			String version = processVO.getVersion();
			
			forwardPath = NbpmUtil.parseForwardPage(processType, subReqType, version, taskName);
			forwardPath = forwardPath.substring(0,forwardPath.lastIndexOf(".")) + "_EC.nvf";
			//HashMap<String, Object> dataMap = NbpmUtil.getDataMapForTaskPage(processVO, userVO);
			HashMap<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("nbpm_user_id", userId);
			dataMap.put("nbpm_node_name", processVO.getNodename());
			dataMap.put("nbpm_task_name", processVO.getTask_name());
			dataMap.put("nbpm_task_id", processVO.getTask_id());
			dataMap.put("nbpm_process_type", processVO.getProcess_type());
			dataMap.put("nbpm_processId", processVO.getProcessId());
			dataMap.put("nbpm_processInstanceId", processVO.getProcessInstanceId());
			dataMap.put("nbpm_version", processVO.getVersion());
			dataMap.put("req_type", processVO.getProcess_type());
			dataMap.put("sr_id", processVO.getSr_id());
			dataMap.put("sub_req_type", processVO.getSub_req_type());
			dataMap.put("multi_yn", processVO.getMulti_yn());
			dataMap.put("task_group_id", processVO.getTask_group_id());
			dataMap.put("sanctn_line_slct_yn", processVO.getSanctn_line_slct_yn());
			dataMap.put("faq_yn", processVO.getFaq_yn());
			dataMap.put("process_statis_yn", processVO.getProcess_statis_yn());
			dataMap.put("current_worker_id", processVO.getCurrent_worker_id());
			dataMap.put("myhistorylist_yn", "Y");
			//dataMap.put("nbpm_task_name", taskName);
			paramMap.put("dataMap", dataMap);
			//forwardPath = "/itg/nbpm/processPage/KAIR/WORK-1/END_VIEW_EC.nvf";
		} catch(Exception e) {
			new NkiaException(e);
		}		
		return forwardPath;
	}
	@RequestMapping(value="/itg/nbpm/provide/goMultiTaskDetailPage.do")
	public String goMultiTaskDetailPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";	
		try {
			NbpmProcessVO processVO = null;
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String srId = request.getParameter("sr_id");
			String type = request.getParameter("type");
			String multiTaskId = request.getParameter("task_id");
			String event_name = request.getParameter("event_name");
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			
			paramVO.setSr_id(srId);
			paramVO.setTask_id(Long.parseLong(multiTaskId));
			processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			
       		
       		String taskName = getTaskNameForForwardPath(type, processVO.getWork_state(), userId);
       		if(event_name != null){
       			taskName = event_name+"_END_VIEW";
       		}
       		processVO.setTask_name(taskName);
    		
    		String processType = processVO.getProcess_type();
    		String subReqType = processVO.getSub_req_type();
    		String version = processVO.getVersion();
    		forwardPath = NbpmUtil.parseForwardPage(processType, subReqType, version, taskName);
    		
    		HashMap<String, Object> dataMap = NbpmUtil.getDataMapForTaskPage(processVO, userVO);
    		paramMap.put("dataMap", dataMap);
       		
		} catch(Exception e) {
			new NkiaException(e);
		}
		return forwardPath;
	}
	
	@RequestMapping(value="/itg/nbpm/provide/goMultiTaskDetailPage_EC.do")
	public String goMultiTaskDetailPage_EC(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";	
		try {
			NbpmProcessVO processVO = null;
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "ExternalConnection";
			String srId = request.getParameter("sr_id");
			String type = request.getParameter("type");
			String multiTaskId = request.getParameter("task_id");
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			
			paramVO.setSr_id(srId);
			paramVO.setTask_id(Long.parseLong(multiTaskId));
			processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			
       		
       		String taskName = getTaskNameForForwardPath(type, processVO.getWork_state(), userId);
    		processVO.setTask_name(taskName);
    		
    		String processType = processVO.getProcess_type();
    		String subReqType = processVO.getSub_req_type();
    		String version = processVO.getVersion();
    		forwardPath = NbpmUtil.parseForwardPage(processType, subReqType, version, taskName);
    		forwardPath = forwardPath.substring(0,forwardPath.lastIndexOf(".")) + "_EC.nvf";
			//HashMap<String, Object> dataMap = NbpmUtil.getDataMapForTaskPage(processVO, userVO);
			HashMap<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("nbpm_user_id", userId);
			dataMap.put("nbpm_node_name", processVO.getNodename());
			dataMap.put("nbpm_task_name", processVO.getTask_name());
			dataMap.put("nbpm_task_id", processVO.getTask_id());
			dataMap.put("nbpm_process_type", processVO.getProcess_type());
			dataMap.put("nbpm_processId", processVO.getProcessId());
			dataMap.put("nbpm_processInstanceId", processVO.getProcessInstanceId());
			dataMap.put("nbpm_version", processVO.getVersion());
			dataMap.put("req_type", processVO.getProcess_type());
			dataMap.put("sr_id", processVO.getSr_id());
			dataMap.put("sub_req_type", processVO.getSub_req_type());
			dataMap.put("multi_yn", processVO.getMulti_yn());
			dataMap.put("task_group_id", processVO.getTask_group_id());
			dataMap.put("sanctn_line_slct_yn", processVO.getSanctn_line_slct_yn());
			dataMap.put("faq_yn", processVO.getFaq_yn());
			dataMap.put("process_statis_yn", processVO.getProcess_statis_yn());
			dataMap.put("current_worker_id", processVO.getCurrent_worker_id());
			dataMap.put("myhistorylist_yn", "Y");
			//dataMap.put("nbpm_task_name", taskName);
			paramMap.put("dataMap", dataMap);
		} catch(Exception e) {
			new NkiaException(e);
		}
		return forwardPath;
	}
	
	private String getTaskNameForForwardPath(String type, String workState, String userId) throws NkiaException {
		String taskName = null;
		if("HIST".equals(type) || "SDESK".equals(type)) {
			if(NbpmConstraints.WORK_STATE_TEMP.equals(workState)) {
				taskName = NbpmConstraints.REQUEST_PAGE.toUpperCase();
			} else {
				taskName = NbpmConstraints.END_VIEW_PAGE.toUpperCase();				
			}
		} else if("EDIT".equals(type)) {
			taskName = NbpmConstraints.END_EDIT_PAGE.toUpperCase();
		} else if("RECALL_EDIT".equals(type)) {
			taskName = NbpmConstraints.RECALL_EDIT_PAGE.toUpperCase();
		} else if("MULTI_VIEW".equals(type)) {
			taskName = NbpmConstraints.MULTI_END_VIEW_PAGE.toUpperCase();
		} else if("MULTI_EDIT".equals(type)) {
			taskName = NbpmConstraints.MULTI_END_EDIT_PAGE.toUpperCase();
		} else if("REQUST_REGIST_EDIT".equals(type)) {
			taskName = NbpmConstraints.REQUST_REGIST_EDIT.toUpperCase();
		} else if("CHANGE_PLAN_EDIT".equals(type)) {
			taskName = NbpmConstraints.CHANGE_PLAN_EDIT.toUpperCase();
		} else {
			taskName = NbpmConstraints.END_VIEW_PAGE.toUpperCase();
		}
		return taskName;
	}
	
	@RequestMapping(value="/itg/nbpm/provide/searchSendMailHistoryList.do")
	public @ResponseBody ResultVO searchSendMailHistoryList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = nbpmProviderService.searchSendMailHistoryList(paramMap);
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(Exception e) {
			new NkiaException(e);
		}
		return result;
	}
		
	@RequestMapping(value="/itg/nbpm/provide/goMultiTaskPop.do")
	public String goMultiTaskPop(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "/itg/nbpm/common/multiTaskPop.nvf";
		try {
			String srId = request.getParameter("sr_id");
			String multiTaskId = request.getParameter("multi_task_id");
			String type = request.getParameter("type");
			String event_name = request.getParameter("event_name");

			paramMap.put("multi_task_id", multiTaskId);
			paramMap.put("sr_id", srId);
			paramMap.put("type", type);
			paramMap.put("event_name",event_name);
			
		} catch(Exception e) {
			new NkiaException(e);
		}		
		return forwardPath;
	}
	//외부접속용팝업
	@RequestMapping(value="/itg/nbpm/provide/goMultiTaskPop_EC.do")
	public String goMultiTaskPop_EC(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "/itg/nbpm/common/multiTaskPop_EC.nvf";
		try {
			String srId = request.getParameter("sr_id");
			String multiTaskId = request.getParameter("multi_task_id");
			String type = request.getParameter("type");

			paramMap.put("multi_task_id", multiTaskId);
			paramMap.put("sr_id", srId);
			paramMap.put("type", type);
			
		} catch(Exception e) {
			new NkiaException(e);
		}		
		return forwardPath;
	}

}
