/*
 * @(#)NbpmInterfaceServiceImpl.java              2013. 3. 29.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.provide.common.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.dao.NbpmProviderDAO;
import com.nkia.itg.nbpm.provide.common.service.NbpmProviderService;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

@Service("nbpmProviderService")
public class NbpmProviderServiceImpl implements NbpmProviderService {

	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name = "nbpmProviderDAO")
	private NbpmProviderDAO nbpmProviderDAO;
	
	public List searchTaskList(UserVO userVO, long taskId, String sr_id) throws NkiaException {
		NbpmProcessVO processVO = new NbpmProcessVO();
		processVO.setTask_id(taskId);
		NbpmProcessVO resultVO = postWorkDAO.selectTaskPageInfo(processVO);
		
		boolean authFlag = checkUserPageAuth(resultVO, userVO);
		List result = null;
		if(authFlag) {
			result = postWorkDAO.searchWorkerTaskList(sr_id);
		} else {
			result = postWorkDAO.searchViewerTaskList(sr_id);
		}
		
		return result;
	}
	public List searchTaskList_EC(String sr_id) throws NkiaException {
		
		List result = null;
		result = postWorkDAO.searchViewerTaskList(sr_id);
		
		return result;
	}
	private boolean checkUserPageAuth(NbpmProcessVO processVO, UserVO userVO) {
		boolean result = false;
		if(processVO != null) {
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String workState = "";
			if(processVO.getWork_state() != null) workState = processVO.getWork_state();
			if(!"END".equals(workState)
					&& userVO.getUser_id().equals(processVO.getCurrent_worker_id())) {
				result = true;
			}
			

			if(NbpmConstraints.SERVICEDESK_ROLE.equals(processVO.getCurrent_worker_id())) {
				List sysAuthList = userVO.getSysAuthList();
				for(int i = 0 ; i < sysAuthList.size() ; i++) {
					Map authMap = (Map)sysAuthList.get(i);
					String authId = (String)authMap.get("AUTH_ID");
					if(authId.toUpperCase().indexOf(NbpmConstraints.SERVICEDESK_ROLE) > -1) {
						result = true;
					}
				}			
			}			
		}
		return result;
	}

	@Override
	public Map parserReceivedMsg(String receivedMsg) throws NkiaException {
		Map receivedData = null;
		return receivedData;
	}

	@Override
	public String createSrId() throws NkiaException {
		return postWorkDAO.createSrId();
	}	
	
	@Override
	public String getRetuenMsg(String srId) throws NkiaException {
		return null;
	}
	
	@Override
	public void insertReceivedSoftPhoneMsg(String receivedMsg) throws NkiaException {
		
	}

	@Override
	public List searchSendMailHistoryList(Map paramMap) throws NkiaException, SQLException {
		return nbpmProviderDAO.searchSendMailHistoryList(paramMap);
	}

	public List searchTaskListEndEdit(UserVO userVO, long taskId, String sr_id) throws NkiaException {
		NbpmProcessVO processVO = new NbpmProcessVO();
		processVO.setTask_id(taskId);
		NbpmProcessVO resultVO = postWorkDAO.selectTaskPageInfo(processVO);
		
		boolean authFlag = checkUserPageAuth(resultVO, userVO);
		List result = null;
		if(authFlag) {
			result = postWorkDAO.searchWorkerTaskListEndEdit(sr_id);
		} else {
			result = postWorkDAO.searchViewerTaskListEndEdit(sr_id);
		}
		
		return result;
	}
}
