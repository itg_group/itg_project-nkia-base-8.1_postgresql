package com.nkia.itg.nbpm.provide.common.api.impl;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.certification.dao.CertificationDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.excpetion.NbpmMultiTaskException;
import com.nkia.itg.nbpm.excpetion.NbpmRuleException;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.AfterWorkCmd;
import com.nkia.itg.nbpm.executor.CompleteTaskCmd;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.executor.WorkMultiTicketCmd;
import com.nkia.itg.nbpm.executor.WorkRuleTaskCmd;
import com.nkia.itg.nbpm.executor.WorkTaskCmd;
import com.nkia.itg.nbpm.process.dao.DeployDAO;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.nbpm.util.NbpmPortalPushSender;
import com.nkia.itg.nbpm.util.NbpmSmsSender;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Component("nbpmCommonClient")
public class NbpmCommonClient implements NbpmClient {

	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name = "deployDAO")
	private DeployDAO deployDAO;
	
	@Resource(name = "nbpmCommonDAO")
	private NbpmCommonDAO nbpmCommonDAO;
	@Resource(name="certificationDAO")
	public CertificationDAO certificationDAO;
	
		
	@Override
	public String createSrId() throws NkiaException {
		// TODO Auto-generated method stub
		return postWorkDAO.createSrId();
	}

	@Override
	public String createSrId(String prefix) throws NkiaException {
		
		return postWorkDAO.createSrId(prefix);
	}

	@Override
	public String registerProcess(Map param) throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		String reqType = (String)param.get("req_type");
		String processId = postWorkDAO.selectFinalVersion(reqType);

		NbpmProcessVO processVO = NbpmUtil.setProcessParameter(param);
		processVO.setTask_name(NbpmConstraints.TASK_NAME_START);
		processVO.setProcessId(processId);
		processVO.setWorker_user_id("omsadmin");
		
		processExecutor.registerUser(processVO, null);
		processExecutor.executeTask(processVO, param);
		return null;
	}

	@Override
	public String selectBpmnXmlData(String processId) throws NkiaException, SQLException {
		Map bpmnDataMap = deployDAO.selectBpmnXmlData(processId);
		Blob blobValue = (Blob) bpmnDataMap.get("BLOB_VALUE");
		String bpmnStringData = new String(blobValue.getBytes(1, (int) blobValue.length()));
		return bpmnStringData;
	}
	
	@Override
	public void processTaskList(Map paramMap, String workerId) throws NkiaException {
		List tasks = (List)paramMap.get("tasks");
		String comment = (String)paramMap.get("comment");
		
		Map returnMap = new HashMap();
		try {

			for(int i = 0 ; i < tasks.size() ; i++) {
				Map task = (Map)tasks.get(i);
				Map processInfoMap = new HashMap();
				processInfoMap.put("nbpm_task_id", String.valueOf(task.get("taskId")));
				processInfoMap.put("sr_id", String.valueOf(task.get("srId")));
				processInfoMap.put("nbpm_processId", String.valueOf(task.get("processId")));
				processInfoMap.put("nbpm_processInstanceId", String.valueOf(task.get("processinstanceId")));
				processInfoMap.put("nbpm_process_type", String.valueOf(task.get("processType")));
				processInfoMap.put("workerUserId", workerId);
				processInfoMap.put("nbpm_comment", comment);
				
				processTask(processInfoMap, workerId);
			}
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}

	private void processTask(Map paramMap, String workerId) throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
		//WORK_TYPE은 일반 처리로 반드시 세팅된다.
		processVO.setWorkType(NbpmConstraints.WORKTYPE_NORMAL_PROC);
		processExecutor.workTask(processVO, new HashMap());
	}
	
	@Override
	public void completeTask(NbpmProcessVO processVO) throws NkiaException {
		AbstractNbpmCommand cmd = new CompleteTaskCmd(processVO);
		cmd.execute();
		
		Map paramMap = new HashMap();
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(processVO.getSr_id() != null) paramMap.put("sr_id", processVO.getSr_id());
		if(processVO.getWorker_user_id() != null) paramMap.put("upd_user_id", processVO.getWorker_user_id());
//		paramMap.put("sr_id", processVO.getSr_id());
//		paramMap.put("upd_user_id", processVO.getWorker_user_id());
		paramMap.put("proc_state", NbpmConstraints.PROC_STATE_NRMLT);
		postWorkDAO.updateEndWorkState(paramMap);
	}
	
	
	/**
	 * 작업을 처리한다. (Rule Task에서 실행할 경우)
	 * @param paramMap
	 * @return
	 * @throws Exception 
	 * @throws SQLException 
	 */
	@Override
	public void workTaskForRule(Map paramMap) throws SQLException, Exception {
		Map resultMap = new HashMap();
		NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
		String task_name = processVO.getTask_name();
		
		//프로세스에서 사용하기로 정의된 변수 명들을 조회하고 화면에서 넘어온 데이터를 비교하여 일치하는게 있다면 값을 세팅한다.
		List extParamNameList = postWorkDAO.searchNbpmProcessVariable(processVO.getProcessId());
		HashMap extParamMap = new HashMap();
		for(int i = 0 ; i < extParamNameList.size() ; i++) {
			String paramName = (String)extParamNameList.get(i);
			if(paramName != null && !"".equals(paramName)) {
				String paramValue = (String)paramMap.get(paramName.toLowerCase());
				extParamMap.put((String)extParamNameList.get(i), paramValue);					
			}
		}
		processVO.setExpansionParamMap(extParamMap);
		
		//WORK_TYPE은 일반 처리로 반드시 세팅된다.
		String workType = (String)paramMap.get("work_type");
		if(workType != null){
			processVO.setWorkType(workType);
		}else{
			processVO.setWorkType(NbpmConstraints.WORKTYPE_NORMAL_PROC);
		}
		
		//processExecutor.workTask(processVO, new HashMap());
		List roleList = postWorkDAO.searchRoleList(processVO);
		List nodeList = postWorkDAO.searchNodeList(processVO);
		//타스크 처리를 위한 엔진 처리
		AbstractNbpmCommand workCommand = new WorkTaskCmd(processVO, roleList);
		Map workResult = (Map)workCommand.execute();
		
		//프로세스 인스턴스 ID를 PROC_MASTER에 세팅한다.
		long procInstId = (Long)workResult.get(NbpmConstraints.NBPM_PARAM_PROCID);
		paramMap.put("proc_id", procInstId);
		if(NbpmConstraints.TASK_NAME_START.equals(task_name)) {
			postWorkDAO.updateProcMasterProcId(paramMap);
		}
		processVO.setProcessInstanceId(procInstId);
		
		//엔진의  Rule Task를 실행한다.
		try {
			AbstractNbpmCommand workRuleTaskCommand = new WorkRuleTaskCmd(processVO);				
			workRuleTaskCommand.execute();			
		} catch (NbpmRuleException e) {
			throw new NbpmRuleException(e);
		} catch (Exception e) {
			throw new NbpmRuleException(e);
		}
		
		//엔진의  후 처리를 위한 엔진 호출
		AbstractNbpmCommand afterWorkCommand = new AfterWorkCmd(processVO, roleList, nodeList);				
		resultMap = (Map)afterWorkCommand.execute();
		String state = (String)resultMap.get(NbpmConstraints.NBPM_PARAM_STATE);
		
		if(NbpmConstraints.NBPM_STATE_ING.equals(state)) {
			//상태가 아직 진행 중일 경우
			ArrayList nextNodeIds = (ArrayList)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTNODEIDS);
			//상태를 업데이트 한다.
			updateProcMasterWorkState(processVO , nextNodeIds);
			//알림과 다음 페이지 (자신이 담당자일 경우) 포워딩을 위해 다음 Task에 대한 정보를 세팅한다.
			setNextTaskInfoMap(resultMap, processVO);
			
		} else if(NbpmConstraints.NBPM_STATE_END.equals(state)) {
			//요청이 종료되었을 때
			paramMap.put("sr_id", processVO.getSr_id());
			paramMap.put("upd_user_id", processVO.getWorker_user_id());
			
			paramMap.put("reject_end_code", NbpmConstraints.PROC_STATE_REJECT_END);
			
			//반려 종료로 요청이 종료되었을 시를 판단한다.(로직상 반려종료와 동시에 요청이 끝날 경우에는 PROC_STATE 정보가 없기 때문에 추가로 판단하는 로직을 추가한다).
			if(NbpmConstraints.WORKTYPE_RETURN_END_PROC.equals(processVO.getWorkType())) {
				paramMap.put("proc_state", NbpmConstraints.PROC_STATE_REJECT_END);
			} else {
				paramMap.put("proc_state", NbpmConstraints.PROC_STATE_NRMLT);
			}
			
			postWorkDAO.updateEndWorkState(paramMap);
		}
	}
	
	/**
	 * 작업을 처리한다. (Rule Task에서 실행할 경우)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void updateProcMasterWorkState(NbpmProcessVO processVO, ArrayList nextNodeIds) throws SQLException, Exception {
		for(int i = 0 ; i < nextNodeIds.size() ; i++){
			processVO.setNext_node_id((String)nextNodeIds.get(i));
			Map workStateMap = postWorkDAO.selectCurrentWorkState(processVO);
			String workState= (String)workStateMap.get("WORK_STATE");
			String procState= (String)workStateMap.get("PROC_STATE");
			String workDtUpdateYn = (String)workStateMap.get("WORK_DT_UPDATE_YN");
			if((!"".equals(workState) && workState != null)) {
				if(!workState.equals(NbpmConstraints.WORK_STATE_NON)) {
					HashMap param = new HashMap();
					param.put("sr_id", processVO.getSr_id());
					param.put("work_state", workState);
					param.put("work_dt_update_yn", workDtUpdateYn);
					param.put("reject_end_code", NbpmConstraints.PROC_STATE_REJECT_END);
					//WORKTYPE 코드 상수로 등록할 것
					if(NbpmConstraints.WORKTYPE_RETURN_END_PROC.equals(processVO.getWorkType())) {
						param.put("proc_state", NbpmConstraints.PROC_STATE_REJECT_END);
					} else {
						param.put("proc_state", procState);
					}
					
					postWorkDAO.updateWorkState(param);
				}
			}
		}
	}
	
	/**
	 * 작업을 처리한다. (Rule Task에서 실행할 경우)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void setNextTaskInfoMap(Map resultMap, NbpmProcessVO processVO) throws SQLException, Exception {
		ArrayList nextNodeIds = (ArrayList)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTNODEIDS);		

		List workerIds = (List)resultMap.get(NbpmConstraints.NBPM_PARAM_WORKERIDS);
		List taskIds = (List)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTTASKIDS);
		List nodeNames = (List)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTNODENAMES);
		
		boolean selfWork = false;
		long taskId = 0;
		String nodeId = "";
		String nodeName = "";
		for(int i = 0 ; i < workerIds.size() ; i++) {
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(processVO.getWorker_user_id() != null) {
				if(processVO.getWorker_user_id().equals((String)workerIds.get(i))) {
					selfWork = true;
					taskId = (Long)taskIds.get(i);
					nodeId = (String)nextNodeIds.get(i);
					nodeName = (String)nodeNames.get(i);
				}
			}
			
		}
		
		resultMap.put("selfWork", selfWork);
		resultMap.put("nextTaskId", taskId);
		resultMap.put("nextNodeName", nodeName);
		
		resultMap.put("sr_id", processVO.getSr_id());
		
	}
	
	/**
	 * 작업을 처리한다. (Rule Task에서 실행할 경우)
	 * @param processVO
	 * @param paramMap
	 * @throws NkiaException
	 */
	@Override
	public void registerUser(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");

		//NbpmTransactionManager tm = new NbpmTransactionManager();
		try {
			//tm.begin();
			//shshin
			String requestor = "";
			if(processVO.getReq_user_id() != null) requestor = processVO.getReq_user_id(); //202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(requestor != null && !"".equals(requestor)) {
				int count = postWorkDAO.isExistNbpmUser(requestor);
				if(count == 0) {
					postWorkDAO.insertNbpmUser(requestor);
				}
			}
			
			String loginId = processVO.getWorker_user_id();
			if(loginId != null && !"".equals(loginId)) {
				int count = postWorkDAO.isExistNbpmUser(loginId);
				if(count == 0) {
					postWorkDAO.insertNbpmUser(loginId);
				}
			}
			for(int i = 0 ; processVO.getOperList() != null && i < processVO.getOperList().size(); i++) {
				Object operUserId = processVO.getOperList().get(i).get("oper_user_id");
				String grp_slct_yn =  processVO.getOperList().get(i).get("grp_slct_yn");
				if(operUserId instanceof String) {
					if(operUserId != null && !"".equals((String)operUserId)) {
						//freechang 17.05.10 담당자 그룹으로 지정시  ORGANIZATIONALENTITY table에 등록
						/*
						if("Y".equals(grp_slct_yn))
						{
							postWorkDAO.insertGrpNbpmUser((String)operUserId);
						}else
						{
						*/
							int count = postWorkDAO.isExistNbpmUser((String)operUserId);
							if(count == 0) {
								postWorkDAO.insertNbpmUser((String)operUserId);
							}
//						}
					}
				} else if(operUserId instanceof List) {
					for(int j = 0 ; j < ((List)operUserId).size() ; j++) {
						String userId = (String)((List)operUserId).get(j);
						if(userId != null && !"".equals(userId)) {
							int count = postWorkDAO.isExistNbpmUser(userId);
							if(count == 0) {
								postWorkDAO.insertNbpmUser(userId);
							}
						}
					}
				}
			}			
			//tm.commit();
		} catch(Exception e) {
			//tm.rollback();				
			throw e;
		} finally {
			//tm.close();
		}
	}
	
	/**
	 * 작업을 처리한다. (Rule Task에서 실행할 경우)
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public Map createMultiTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		Map resultMap = new HashMap();
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {		
			//nsm.begin();
			String eventName = (String) paramMap.get("event_name");

			AbstractNbpmCommand workCommand = new WorkMultiTicketCmd(processVO, eventName);
			Map workResult = (Map) workCommand.execute();

			long task_id = (Long) workResult.get("task_id");
			String taskName = (String) workResult.get("task_name");
			paramMap.put("task_id", task_id);
			paramMap.put("task_group_id", task_id);
			paramMap.put("task_name", taskName);
			String nodename = null;
			int cnt = postWorkDAO.selectMultiTicketCnt(paramMap);
			if (cnt > 0) {
				throw new NbpmMultiTaskException("Task is exist");
			} else {
				postWorkDAO.insertMultiTaskInfo(paramMap);
				postWorkDAO.insertMultiTaskTableInfo(paramMap);

				String operType = (String)paramMap.get("oper_type");
				Map workerUserInfo = new HashMap();
				//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
				if(processVO.getWorker_user_id() != null) workerUserInfo.put("oper_user_id", processVO.getWorker_user_id());				
				workerUserInfo.put("sr_id", processVO.getSr_id());
				workerUserInfo.put("oper_type", operType);
				workerUserInfo.put("task_group_id", task_id);
				workerUserInfo.put("oper_user_id", (String)paramMap.get("worker_user_id"));
				postWorkDAO.insertProcessOrerator(workerUserInfo);
				
				nodename = postWorkDAO.selectTaskNodeName(paramMap);
			}
			resultMap.put("result", "success");
			resultMap.put("task_name", taskName);
			resultMap.put("task_id", task_id);
			resultMap.put("task_group_id", task_id);
			resultMap.put("nodename", nodename);

			//nsm.commit();				
		} catch(NbpmMultiTaskException e) {
			//nsm.rollback();
			resultMap.put("result", "fail");
		} catch(Exception e) {
			//nsm.rollback();
			throw e;
		} finally {
			//nsm.close();
		}
		return resultMap;
	}
	
	/**
	 * 조건 : 요청번호
	 * 임시등록된 데이터 조회
	 */
	public Map selectTempSrData(String srId) throws NkiaException {
		Map resultMap = postWorkDAO.selectSimpleProcessDetail(srId);
		
		Map operMap = new HashMap();
		operMap.put("sr_id", srId);
		List srOperList = postWorkDAO.selectProcessOperList(operMap);
		List setOperList = new ArrayList();
		for (int i=0; i < srOperList.size(); i++) {
			HashMap temp = (HashMap)srOperList.get(i);
			LinkedHashMap operInfo = new LinkedHashMap();
			operInfo.put("select_type", "SINGLE");
			operInfo.put("oper_type", temp.get("OPER_TYPE"));
			operInfo.put("oper_user_id", temp.get("OPER_USER_ID"));
			operInfo.put("oper_user_nm", temp.get("OPER_USER_NM"));
			operInfo.put("grp_slct_yn", temp.get("GRP_SLCT_YN"));
			setOperList.add(operInfo);
		}
		
		resultMap.put("oper_list", setOperList);
		return resultMap;
	}
	
	/**
	 * 요청의 구성명 조회
	 * @param srId
	 * @return
	 * @throws NkiaException
	 */
	public String selectSrConfNameData(String srId) throws NkiaException {
		return nbpmCommonDAO.selectSrConfNameData(srId);
	}
	
	public void notifyTask(NbpmProcessVO processVO, Map result) throws NkiaException {
	
		try {
			ArrayList nextTaskIds = (ArrayList)result.get(NbpmConstraints.NBPM_PARAM_NEXTTASKIDS);
			ArrayList nextNodeIds = (ArrayList)result.get(NbpmConstraints.NBPM_PARAM_NEXTNODEIDS);
			
			//프로세스의 상세정보를 조회한다.
			Map processDetail = postWorkDAO.selectProcessDetail(processVO.getSr_id());
			
			for (int i=0; nextTaskIds != null && i < nextTaskIds.size(); i++) {
				processVO.setTask_id((Long)nextTaskIds.get(i));
				processVO.setNext_node_id((String)nextNodeIds.get(i));
				
				//다음 task의 알림 확인
				Map nextTaskAlarm = postWorkDAO.selectNextTaskAlamInfo(processVO);
				
				String emailSend = (String)nextTaskAlarm.get("EMAIL_SEND"); //이메일발송여부
				String smsSend = (String)nextTaskAlarm.get("SMS_SEND"); //문자발송여부
				String grpSlctYn = (String)nextTaskAlarm.get("GRP_SLCT_YN"); //그룹여부
				String emailSendGrpYn = (String)nextTaskAlarm.get("EMAIL_SEND_GRP_YN"); //(담당자, 처리자 등의) 그룹에 메일 발송 여부
				String nodeName = (String)nextTaskAlarm.get("NODENAME"); //작업명
				
				//E-MAIL
				// 프로세스 작업 상태 관리에서 메일 송신 여부가 활성화 되어 있으면 알림메일를 보낸다.
				//if (emailSend.equals(EmailEnum.COMMON_Y.getString())) { //부서승인시 상위에서 거르고넘어오기때문에 주석
					Map dataMap = new HashMap();
					
					//작업명
					//dataMap.put("NODENAME", nodeName);
			//		String templateId = (String)nextTaskAlarm.get("EMAIL_TEMPLATE_ID"); //템플릿ID
					
					//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
					List targetUserList = postWorkDAO.searchNbpmAlamEmailList(processVO);
					dataMap.put("TO_USER_LIST", targetUserList);
					//boolean emailSendFlag = false; //이메일발송
					
					//그룹여부
//					if (!"Y".equals(grpSlctYn)) {
//						emailSendFlag = true;
//					} else {
//						//그룹에도 메일 발송할지 여부
//						if (emailSendGrpYn != null && emailSendGrpYn.equals(EmailEnum.COMMON_Y.getString())) {
//							emailSendFlag = true;
//						}
//					}
					
					//메일 발송
					//if (emailSendFlag) {
						try {
							//메일 데이터 셋팅
							Map mailInfoMap = new HashMap();
							mailInfoMap.put("KEY","PROCESS");
							//mailInfoMap.put("TEMPLATE_ID", templateId);
							mailInfoMap.put("DATA", dataMap);
							mailInfoMap.put("ADD_DATA", processDetail);
							
							EmailSetData emailSender = new EmailSetData(mailInfoMap);
							emailSender.sendMail();
							processVO.setEmailSended("Y");
						} catch(Exception e){
							throw new NkiaException(e);
						}
					//} else {
						processVO.setEmailSended("N");
					//}
					
					//메일 발송이 되면 메일 발송 여부를 업데이트 한다.
					AbstractNbpmCommand emailSendedCommand = new EmailSendedCommand(processVO);		
					emailSendedCommand.execute();
				//}
				
				//SMS
//				if (smsSend.equals(emailSend.equals(EmailEnum.COMMON_Y.getString()))) {
//					//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
//					List alamList = postWorkDAO.searchNbpmAlamSmsList(processVO);
//					
//					for (int j = 0; j < alamList.size(); j++) {
//						Map alamInfo = (Map)alamList.get(j);
//						nodeName = (String)alamInfo.get("NODENAME"); //절차단계
//						String userId = (String)alamInfo.get("TARGET_USER_ID"); //수신자ID
//						
//						//프로세스 작업 상태 관리에서 문자 송신 여부가 활성화 되어 있으면 문자를 보낸다.
//						ModelMap modelMap = new ModelMap();
//						modelMap.put("userId", userId);
//						Map userInfoMap = certificationDAO.selectUserInfo(modelMap);
//						
//						Map smsInfo = new HashMap();
//						smsInfo.put("nodeName", nodeName);
//						String toName = (String)userInfoMap.get("USER_NM");
//						String toPhone = (String)(userInfoMap.get("HP_NO") == null ? "-" : userInfoMap.get("HP_NO"));
//						smsInfo.put("TO_NAME", toName);
//						smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
//						smsInfo.put("TITLE", (String)processDetail.get("REQ_TYPE_NM") + " > " + nodeName);
//						smsInfo.put("CONTENT", (String)processDetail.get("TITLE"));
//						
//						//sms 발송 (스레드로 처리하여 오류가 나도 무시할 수 있도록 한다.
//						try {
//							NbpmSmsSender smsSender = new NbpmSmsSender(processDetail, smsInfo);
//							Thread t = new Thread(smsSender);
//							t.start();
//						} catch (Exception e) {
//							throw new NkiaException(e);
//						}
//					}
//				}
			}
			
			//포탈과 연동을 할 경우에는 포탈에 푸시를 보낸다.
//			if("true".equals(NkiaApplicationPropertiesMap.getProperty("nbpm.push.portal"))) {
//				ArrayList nextWorkerIds = (ArrayList)result.get(NbpmConstraints.NBPM_PARAM_WORKERIDS);
//				for(int i = 0 ; nextWorkerIds != null && i < nextWorkerIds.size() ; i ++ ) {
//					NbpmPortalPushSender pushSender = new NbpmPortalPushSender(processDetail, (String)nextWorkerIds.get(i), processVO.getTask_name());
//					Thread t = new Thread(pushSender);
//					t.start();
//				}
//			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}


	
}
