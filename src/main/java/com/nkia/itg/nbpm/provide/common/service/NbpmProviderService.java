/*
 * @(#)NbpmInterfaceService.java              2013. 3. 29.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.provide.common.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

public interface NbpmProviderService {

	public List searchTaskList(UserVO userVO, long taskId, String sr_id) throws NkiaException;
	
	public String getRetuenMsg(String srId) throws NkiaException;
	
	public String createSrId() throws NkiaException;

	public void insertReceivedSoftPhoneMsg(String receivedMsg) throws NkiaException;

	public Map parserReceivedMsg(String receivedMsg) throws NkiaException;
	
	public List searchSendMailHistoryList(Map paramMap) throws NkiaException, SQLException;

	public List searchTaskList_EC(String sr_id)throws NkiaException;
	
	public List searchTaskListEndEdit(UserVO userVO, long taskId, String sr_id) throws NkiaException;
	
}
