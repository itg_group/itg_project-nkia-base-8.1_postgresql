package com.nkia.itg.nbpm.provide.common.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("nbpmProviderDAO")
public class NbpmProviderDAO extends NbpmAbstractDAO {

	public List searchSendMailHistoryList(Map paramMap) throws SQLException {
		return list("NbpmProviderDAO.searchSendMailHistoryList", paramMap);
	}

}
