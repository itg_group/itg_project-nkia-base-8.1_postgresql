package com.nkia.itg.nbpm.provide.common.dao;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("nbpmSampleProviderDAO")
public class NbpmSampleProviderDAO extends NbpmAbstractDAO {

	public String selectSampleSGApprData(String sr_id) throws SQLException {
		return (String) selectByPk("NbpmSampleProviderDAO.selectSampleSGApprData", sr_id);
	}
	
}
