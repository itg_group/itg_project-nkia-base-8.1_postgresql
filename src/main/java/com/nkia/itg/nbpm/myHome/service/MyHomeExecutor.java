/*
 * @(#)myHomeService.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.myHome.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface MyHomeExecutor {

	public int searchReadyMyHomeCount(ModelMap paramMap) throws NkiaException, SQLException;
	
	public List searchReadyMyHome(ModelMap paramMap) throws NkiaException, SQLException;

	public int searchIngMyHomeCount(ModelMap paramMap) throws NkiaException, SQLException;

	public List searchIngMyHome(ModelMap paramMap) throws NkiaException, SQLException;

	public int searchCompleteMyHomeCount(ModelMap paramMap) throws NkiaException, SQLException;

	public List searchCompleteMyHome(ModelMap paramMap) throws NkiaException, SQLException;
}
