/*
 * @(#)MyListController.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.myHome.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.mylist.service.MyListExecutor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class MyHomeController {
	
	@Resource(name="myListCommonExecutor")
	private MyListExecutor myListExecutor;
	
	@RequestMapping(value="/itg/nbpm/goMyHomeTab.do")
	public String goServicedeskTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/myHome/myHomeTab.nvf";
		paramMap.put("list_type", request.getParameter("list_type"));
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/goMyHomeMain.do")
	//@@20130711 김도원 START
	//메소드명 url과 일치하도록 변경
	public String goMyListMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
	//@@20130711 김도원 END
		String forwarPage = "/itg/nbpm/myHome/myHomeMain.nvf";
		try {
			
			String user_id = request.getParameter("user_id");
			String user_nm = request.getParameter("user_nm");
			
			paramMap.put("list_type", request.getParameter("list_type"));
			
			NbpmProcessVO processVO = new NbpmProcessVO();
			
			paramMap.put("processVO", processVO);
			paramMap.put("nbpm_user_id", user_id);
			paramMap.put("nbpm_task_name", NbpmConstraints.TASK_NAME_START);
			paramMap.put("nbpm_process_type", processVO.getProcess_type());
			paramMap.put("nbpm_processId", processVO.getProcessId());
			paramMap.put("nbpm_processInstanceId", processVO.getProcessInstanceId());
			paramMap.put("nbpm_version", processVO.getVersion());
			
		} catch (Exception e) {			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	 
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchMyHome.do")
	public @ResponseBody ResultVO searchMyList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			
			String listType = (String)paramMap.get("list_type");
			paramMap.put("user_id", userId);
			PagingUtil.getFristEndNum(paramMap);
			
			if(NbpmConstraints.LIST_TYPE_READY.equals(listType)) {
				totalCount = myListExecutor.searchReadyMyListCount(paramMap);
				resultList = myListExecutor.searchReadyMyList(paramMap);
			} else if(NbpmConstraints.LIST_TYPE_ING.equals(listType)) {
				totalCount = myListExecutor.searchIngMyListCount(paramMap);
				resultList = myListExecutor.searchIngMyList(paramMap);
			} else if(NbpmConstraints.LIST_TYPE_COMPLETE.equals(listType)) {
				totalCount = myListExecutor.searchCompleteMyListCount(paramMap);
				resultList = myListExecutor.searchCompleteMyList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
}
