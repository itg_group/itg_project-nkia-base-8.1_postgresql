/*
 * @(#)MyListServiceImpl.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.myHome.service.impl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.myHome.dao.MyHomeDAO;
import com.nkia.itg.nbpm.myHome.service.MyHomeExecutor;

@Service("myHomeCommonExecutor")
public class MyHomeCommonExecutor implements MyHomeExecutor {

	@Resource(name = "myHomeDAO")
	private MyHomeDAO myHomeDAO;
	//의심병
	public int searchReadyMyHomeCount(ModelMap paramMap) throws NkiaException, SQLException {
		return myHomeDAO.searchReadyMyHomeCount(paramMap);
	}

	public List searchReadyMyHome(ModelMap paramMap) throws NkiaException, SQLException {
		return myHomeDAO.searchReadyMyHome(paramMap);
	}

	@Override
	public int searchIngMyHomeCount(ModelMap paramMap) throws NkiaException, SQLException {
		return myHomeDAO.searchIngMyHomeCount(paramMap);
	}

	@Override
	public List searchIngMyHome(ModelMap paramMap) throws NkiaException, SQLException {
		return myHomeDAO.searchIngMyHome(paramMap);
	}

	@Override
	public int searchCompleteMyHomeCount(ModelMap paramMap) throws NkiaException, SQLException {
		return myHomeDAO.searchCompleteMyHomeCount(paramMap);
	}

	@Override
	public List searchCompleteMyHome(ModelMap paramMap) throws NkiaException, SQLException {
		return myHomeDAO.searchCompleteMyHome(paramMap);
	}
}
