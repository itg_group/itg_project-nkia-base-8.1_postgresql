/*
 * @(#)MyListDAO.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.myHome.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("myHomeDAO")
public class MyHomeDAO extends NbpmAbstractDAO {

	public int searchReadyMyHomeCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("MyHomeDAO.searchReadyMyHomeCount", paramMap);
	}

	public List searchReadyMyHome(ModelMap paramMap) throws SQLException {
		return list("MyHomeDAO.searchReadyMyHome", paramMap);
	}

	public int searchIngMyHomeCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("MyHomeDAO.searchIngMyHomeCount", paramMap);
	}

	public List searchIngMyHome(ModelMap paramMap) throws SQLException {
		return list("MyHomeDAO.searchIngMyHome", paramMap);
	}

	public int searchCompleteMyHomeCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("MyHomeDAO.searchCompleteMyHomeCount", paramMap);
	}

	public List searchCompleteMyHome(ModelMap paramMap) throws SQLException {
		return list("MyHomeDAO.searchCompleteMyHome", paramMap);
	}
}
