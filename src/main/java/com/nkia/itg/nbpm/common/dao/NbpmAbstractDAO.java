/*
 * @(#)NbpmAbstratDAO.java              2013. 6. 27.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.common.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.nkia.itg.base.application.filter.SQLInjectionFilter;

public class NbpmAbstractDAO extends SqlMapClientDaoSupport {
	
	public static final Logger logger = LoggerFactory.getLogger("ITG.DAO.LOG");
	
    @Resource(name = "egov.sqlMapClient")
    public void setSuperSqlMapClient(SqlMapClient sqlMapClient) {
        super.setSqlMapClient(sqlMapClient);
    }
    
	public Object selectByPk(String queryId, Object parameterObject) {
		Object result = null;
		if(parameterObject instanceof Map) {
			SQLInjectionFilter.runFilter((Map)parameterObject);
		}
		
		printQueryIdLog(queryId);
		long start = System.currentTimeMillis();
		result = getSqlMapClientTemplate().queryForObject(queryId, parameterObject);			
		long end = System.currentTimeMillis();
		printWaitingTimeLog(start, end, queryId);
        
		return result;
    }


    public List list(String queryId, Object parameterObject) {
		if(parameterObject instanceof Map) {
			SQLInjectionFilter.runFilter((Map)parameterObject);
		}
		printQueryIdLog(queryId);
		long start = System.currentTimeMillis();
		
		List result = getSqlMapClientTemplate().queryForList(queryId, parameterObject);
		
		long end = System.currentTimeMillis();
		printWaitingTimeLog(start, end, queryId);
        return result;
    }
    
    public Object insert(String queryId, Object parameterObject) {
    	printQueryIdLog(queryId);
    	long start = System.currentTimeMillis();
    	
    	Object result = getSqlMapClientTemplate().insert(queryId, parameterObject);
    	
    	long end = System.currentTimeMillis();
    	printWaitingTimeLog(start, end, queryId);
        return result;
    }
    
    public int update(String queryId, Object parameterObject) {
    	printQueryIdLog(queryId);
    	long start = System.currentTimeMillis();
    	
    	int result = getSqlMapClientTemplate().update(queryId, parameterObject);
    	
    	long end = System.currentTimeMillis();
    	printWaitingTimeLog(start, end, queryId);
        return result;
    }
    
    public void delete(String queryId, Object parameterObject) {
    	printQueryIdLog(queryId);
    	long start = System.currentTimeMillis();
    	
    	int result = getSqlMapClientTemplate().delete(queryId, parameterObject);
    	
    	long end = System.currentTimeMillis();
    	printWaitingTimeLog(start, end, queryId);
    }
    
    private void printQueryIdLog(String queryId) {
    	logger.info("Execute query : [" + queryId + "]");
    }
    
    private void printWaitingTimeLog(long start, long end, String queryId) {
    	logger.info("[" + queryId + "] Waiting time : " + String.valueOf((end-start)/(double)1000) + "sec");
    }
}
