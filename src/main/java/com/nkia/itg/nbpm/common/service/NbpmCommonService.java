/*
 * @(#)NbpmCommonService.java              2013. 5. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

public interface NbpmCommonService {

	public int searchProcessListCount(Map paramMap) throws NkiaException;

	public List searchProcessList(Map paramMap) throws NkiaException;

	public List searchProcessOperUserList(Map paramMap) throws NkiaException;

	public int searchProcessOperUserListCount(Map paramMap) throws NkiaException;

	public List searchGridDataList(Map paramMap) throws NkiaException;

	public List searchOperGridList(Map paramMap) throws NkiaException;

	public int searchConfListCount(Map paramMap) throws NkiaException;

	public List searchConfList(Map paramMap) throws NkiaException;

	public List searchIncLevelMatrix(Map paramMap) throws NkiaException;

	public List searchSrRelationList(Map paramMap) throws NkiaException;
	
	public List searchIncRelationList(Map paramMap) throws NkiaException;

	public Map searchIncLevelInfo(Map paramMap) throws NkiaException;

	public Map selectRelIncStatus(String sr_id) throws NkiaException;
	
	public List searchServiceList(Map paramMap) throws NkiaException;
	
	public List searchServiceTree(Map paramMap) throws NkiaException;

	public List searchProcessSubGrid(Map paramMap) throws NkiaException;

	public List searchSelecetedConfRelServiceList(Map paramMap) throws NkiaException;

	public List searchSelecetedConfRelAssetList(Map paramMap) throws NkiaException;

	public UserVO selectUserInfoFromHpNo(String hp_no) throws NkiaException;
	
	/**
	 * 
	 * 프로세스 참조 List( 디자이너에서 생성된 쿼리정보를 기준으로 조회 ) / textfield + popup
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProcessReferenceList(Map paramMap) throws NkiaException;

	public List searchLinkRelProcess(Map paramMap) throws NkiaException;

	public List searchMultiTaskGrid(Map paramMap) throws NkiaException;
	
	/**
	 * PROCESSSID와 TASK_ID로 NodeName을 가져오는 메소드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String getNbpmNodeName(ModelMap paramMap)throws NkiaException;

	public List searchProcessGridField(ModelMap paramMap) throws NkiaException;

	public void insertProcessGridRecord(Map paramMap) throws NkiaException;

	public void deleteProcessGridRecord(ModelMap paramMap) throws NkiaException;

	/**
	 * 담당자유형별 담당자그룹 매핑을 위한 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map selectGrpAppoint(HashMap paramMap) throws NkiaException;
}
