/*
 * @(#)NbpmCommonDAO.java              2013. 5. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.common.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.certification.vo.UserVO;

@Repository("nbpmCommonDAO")
public class NbpmCommonDAO extends NbpmAbstractDAO {

	//@@20130715 김도원 START
	//쿼리 성격에 맞게 searchProcessList, searchProcessListCount 를 NbpmCommon으로 이동함
	public int searchProcessListCount(Map paramMap) {
		return (Integer)selectByPk("NbpmCommon.searchProcessListCount", paramMap);
	}

	public List searchProcessList(Map paramMap) {
		return list("NbpmCommon.searchProcessList", paramMap);
	}
	//@@20130715 김도원 END

	public List searchProcessOperUserList(Map paramMap) {
		return list("NbpmCommon.searchProcessOperUserList", paramMap);
	}

	public int searchProcessOperUserListCount(Map paramMap) {
		return (Integer)selectByPk("NbpmCommon.searchProcessOperUserListCount", paramMap);
	}
	

	public void deleteGridData(String gridDeleteQueryKey, Map param)  {
		update("ProcDetail." + gridDeleteQueryKey, param);
	}

	public void insertGridData(String gridInsertQueryKey, Map param)  {
		update("ProcDetail." + gridInsertQueryKey, param);
	}

	public List searchGridDataList(String queryKey, Map paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("ProcDetail." + queryKey, paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}		
		
		return handler.getReturnList();
	}

	public List searchOperGridList(Map paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("NbpmCommon.searchOperGridList", paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
	}

	public int searchConfListCount(Map paramMap) {
		return (Integer)selectByPk("NbpmCommon.searchConfListCount", paramMap);
	}

	public List searchConfList(Map paramMap) {
		return list("NbpmCommon.searchConfList", paramMap);
	}
	
	public int searchRelConfListCount(Map paramMap) {
		return (Integer)selectByPk("NbpmCommon.searchRelConfListCount", paramMap);
	}

	public List searchRelConfCiList(Map paramMap) {
		return list("NbpmCommon.searchRelConfCiList", paramMap);
	}

	public List searchRelConfServiceList(Map paramMap)  {
		return list("NbpmCommon.searchRelConfServiceList", paramMap);
	}

	public List searchIncLevelMatrix(Map paramMap)  {
		return list("NbpmCommon.searchIncLevelMatrix", paramMap);
	}
	
	public List searchTestResultList(Map paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("NbpmCommon.searchTestResultList", paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
	}
	
	public List searchSrRelationList(Map paramMap) {
		return list("NbpmCommon.searchSrRelationList", paramMap);
	}
	
	public List searchIncRelationList(Map paramMap) {
		return list("NbpmCommon.searchIncRelationList", paramMap);
	}
	
	public Map selectRelIncStatus(String sr_id) throws NkiaException {
		return(Map)selectByPk("NbpmCommon.selectRelIncStatus", sr_id);
	}

	public Map searchIncLevelInfo(Map paramMap) {
		return (Map)selectByPk("NbpmCommon.searchIncLevelInfo", paramMap);
	}

	public List searchServiceList(Map paramMap) {
		return list("NbpmCommon.searchServiceList", paramMap);
	}
	
	public List searchServiceTree(Map paramMap) {
		return list("NbpmCommon.searchServiceTree", paramMap);
	}
	
	public List searchProcessSubGrid(Map paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("ProcDetail." + paramMap.get("selectQuerykey"), paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
	}

	public List searchSelecetedConfRelServiceList(Map paramMap) {
		return list("NbpmCommon.searchSelecetedConfRelServiceList", paramMap);
	}

	public List searchSelecetedConfRelAssetList(Map paramMap) {
		return list("NbpmCommon.searchSelecetedConfRelAssetList", paramMap);
	}

	public UserVO selectUserInfoFromHpNo(String hp_no) {
		return (UserVO) selectByPk("NbpmCommon.selectUserInfoFromHpNo", hp_no);
	}
	
	/**
	 * 
	 * 프로세스 참조 List( 디자이너에서 생성된 쿼리정보를 기준으로 조회 ) / textfield + popup
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProcessReferenceList(Map paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("ProcDetail." + paramMap.get("selectQuerykey"), paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
	}

	public List searchProcRelTable(Map paramMap) {
		return list("NbpmCommon.searchProcRelTable", paramMap);
	}

	public List searchLinkRelProcess(Map paramMap) {
		return list("NbpmCommon.searchLinkRelProcess", paramMap);
	}

	public List searchMultiTaskGrid(Map paramMap) {
		return list("NbpmCommon.searchMultiTaskGrid", paramMap);
	}

	public void deleteBpmUserConstraint(Map constraint) {
		delete("NbpmCommon.deleteBpmUserConstraint", constraint);
	}

	public List searchConstraintList() {
		return list("NbpmCommon.searchConstraintList", null);
	}
	
	public int updateClaimTask(HashMap paramMap) {
		return (Integer) update("NbpmCommon.updateClaimTask", paramMap);
	}

	public int updateClaimProcOper(HashMap paramMap) {
		return (Integer) update("NbpmCommon.updateClaimProcOper", paramMap);
	}
	
	public int updateClaimProcOperAll(HashMap paramMap) {
		return (Integer) update("NbpmCommon.updateClaimProcOperAll", paramMap);
	}
	
	public String getNbpmNodeName(ModelMap paramMap) {
		return (String)selectByPk("NbpmCommon.getNbpmNodeName", paramMap);
	}

	public List searchProcessGridField(ModelMap paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("ProcDetail." + paramMap.get("selectQuerykey"), paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
	}

	public String selectProcGridFieldId() {
		return (String) selectByPk("NbpmCommon.selectProcGridFieldId", null);
	}
	
	public String selectSrConfNameData(String srId) throws NkiaException {
		return (String) selectByPk("NbpmCommon.selectSrConfNameData", srId);
	}
	
	public Map selectGrpAppoint(HashMap paramMap) throws NkiaException {
		return(Map)selectByPk("NbpmCommon.selectGrpAppoint", paramMap);
	}

}
