package com.nkia.itg.nbpm.common;


public class NbpmEngineInitializer {

	public static void startEngine() {
		
		try {
			NbpmRuntimeContainer.initContainer();
		} catch (RuntimeException e) {
			//log.error(t.getMessage(), t.getCause());
			throw new RuntimeException("error while creating session", e);
		} catch (Exception e) {
			//log.error(t.getMessage(), t.getCause());
			throw new RuntimeException("error while creating session", e);
		}
	}

}
