package com.nkia.itg.nbpm.common;

import java.io.File;
import java.net.URL;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.jbpm.task.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.file.FileUtils;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.process.dao.DeployDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;

import egovframework.com.cmm.EgovMessageSource;
public class NbpmRuntimeContainer {
	
	private static final Logger logger = LoggerFactory.getLogger(NbpmRuntimeContainer.class);
	private static KnowledgeBase kbase;
	private static KnowledgeBuilder kbuilder;
	
	private final static String DRL_FILE_PATH = "/egovframework/egovProps/";
	private final static String DRL_FILE_NAME = "commonRules.drl";
	private final static String BPMN_FILE_PATH = "/com/nkia/itg/nbpm/common/bpmn/";
	
	
	public static void initContainer() {
		deleteBpmUserConstraint();
		loadKBuilder();
		try {

			PostWorkExecutor processCommonExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
			List<User> systemUserList = new ArrayList<User>();
			systemUserList.add(new User("Administrator"));
			systemUserList.add(new User("RQESTER"));
			systemUserList.add(new User("rel_inc"));
			systemUserList.add(new User("MULTIUSER"));
			systemUserList.add(new User("CNSLR"));
			systemUserList.add(new User("SYSTEM"));
			systemUserList.add(new User("omsadmin"));
			
			processCommonExecutor.registerUser(systemUserList);
			
			logger.info("Nbpm Engine Start Complete");
		} catch (RuntimeException e) {
			throw new RuntimeException("error while creating session", e);
		} catch (Exception e) {
			throw new RuntimeException("error while creating session", e);
		}
	}
	
	private static void deleteBpmUserConstraint() {
		NbpmCommonDAO nbpmCommonDAO = (NbpmCommonDAO)NkiaApplicationContext.getCtx().getBean("nbpmCommonDAO");
		List constraintList = nbpmCommonDAO.searchConstraintList();
		for(int i = 0 ; i < constraintList.size() ; i++) {
			Map constraint = (Map)constraintList.get(i);
			constraint = WebUtil.lowerCaseMapKey(constraint);
			nbpmCommonDAO.deleteBpmUserConstraint(constraint);
		}
	}
 
	public static void loadKBuilder() {
		try {
			System.setProperty("jbpm.enable.multi.con", "true");
			
			kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
			
			String bpmnFilePath = BPMN_FILE_PATH;
			String rootPath = "classes";
			
			URL classResource = NbpmRuntimeContainer.class.getResource("");
			if (classResource != null) {
				rootPath = classResource.getPath().substring(0, classResource.getPath().lastIndexOf("classes")) + "classes";
			}
			
			List bpmnFile = new ArrayList();
			Map<Resource,ResourceType> resources = new HashMap<Resource, ResourceType>();
			
			DeployDAO deployDAO = (DeployDAO)NkiaApplicationContext.getCtx().getBean("deployDAO");
			List bpmnDataList = deployDAO.searchDeployList();
			List processList = deployDAO.searchFinalProcessList();
			
			
			for(int i = 0 ; i < bpmnDataList.size() ; i++) {
				HashMap bpmnDataMap = (HashMap)bpmnDataList.get(i);
				Blob blobValue = (Blob)bpmnDataMap.get("BLOB_VALUE");
				if(blobValue != null) {
					String bpmnStringData = new String(blobValue.getBytes(1, (int)blobValue.length()));		        		
					try {
						File blobFile = FileUtils.writeFileFromByte(rootPath + bpmnFilePath + (String)bpmnDataMap.get("FILE_NM"), bpmnStringData.getBytes());
						resources.put(ResourceFactory.newFileResource(blobFile), ResourceType.BPMN2);
					} catch (Exception e) {
						throw new NkiaException(e);
					}
				}
			}
			
			String drlPath = rootPath + DRL_FILE_PATH  + DRL_FILE_NAME;
			
			try {
				File drlFile = new File(drlPath);
				if(drlFile.exists()) {
					resources.put(ResourceFactory.newFileResource(drlFile), ResourceType.DRL);																
				} else {
					new NkiaException("Rule File Not Found");
				}
			} catch (Exception e) {
				new NkiaException(e);
			}
			
			for (Map.Entry<Resource, ResourceType> entry : resources.entrySet()) {
				kbuilder.add(entry.getKey(), entry.getValue());
			}
			kbase = KnowledgeBaseFactory.newKnowledgeBase();
			kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
			resources.clear();
		} catch(Exception e) {
			new NkiaException(e);
		}
	}

	public static KnowledgeBuilder getKbuilderInstance() {
		if(kbuilder == null) {
			loadKBuilder();
		}
		return kbuilder;
	}

	public static KnowledgeBase getKbaseInstance() {
		if(kbuilder == null) {
			loadKBuilder();
		}
		return kbase;
	}

	public static String getResources(String code) {
		EgovMessageSource egovMessageSource = (EgovMessageSource) NkiaApplicationContext.getCtx().getBean("egovMessageSource");
		String result = egovMessageSource.getMessage(code);
		return result;
	}
	

	
}
