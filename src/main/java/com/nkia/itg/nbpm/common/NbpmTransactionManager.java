package com.nkia.itg.nbpm.common;

import java.util.HashMap;

import javax.persistence.EntityManagerFactory;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.SystemEventListener;
import org.drools.command.impl.CommandBasedStatefulKnowledgeSession;
import org.drools.container.spring.beans.persistence.DroolsSpringJpaManager;
import org.drools.container.spring.beans.persistence.DroolsSpringTransactionManager;
import org.drools.container.spring.beans.persistence.HumanTaskSpringTransactionManager;
import org.drools.persistence.PersistenceContextManager;
import org.drools.persistence.jpa.JPAKnowledgeService;
import org.drools.runtime.Environment;
import org.drools.runtime.EnvironmentName;
import org.drools.runtime.StatefulKnowledgeSession;
import org.jbpm.process.audit.JPAProcessInstanceDbLog;
import org.jbpm.process.audit.JPAWorkingMemoryDbLogger;
import org.jbpm.process.workitem.wsht.MinaHTWorkItemHandler;
import org.jbpm.task.service.TaskService;
import org.jbpm.task.service.local.LocalTaskService;
import org.jbpm.task.service.persistence.TaskSessionSpringFactoryImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.ResourceTransactionManager;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.util.NbpmThreadManager;

@SuppressWarnings("serial")
public class NbpmTransactionManager implements ResourceTransactionManager, BeanFactoryAware, InitializingBean {
	
	private JpaTransactionManager tm = null;
	

	public StatefulKnowledgeSession getSession() {
		return (StatefulKnowledgeSession)NbpmThreadManager.getTxMap().get("session");
	}
		
	public LocalTaskService getTaskClient() {
		return (LocalTaskService)NbpmThreadManager.getTxMap().get("taskClient");
	}
	
	public TaskService getTaskService() {
		return (TaskService)NbpmThreadManager.getTxMap().get("taskService");
	}
		
	@Override
	public void afterPropertiesSet() throws NkiaException {
		
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		
	}

	@Override
	public Object getResourceFactory() {
		
		return null;
	}


	@Override
	public TransactionStatus getTransaction(TransactionDefinition definition)
			throws TransactionException {
		try {
			KnowledgeBase kbase = NbpmRuntimeContainer.getKbaseInstance();

			tm = (JpaTransactionManager) NkiaApplicationContext.getCtx().getBean("transactionManager");
			HumanTaskSpringTransactionManager hTransactionManager = new HumanTaskSpringTransactionManager(tm);
			DroolsSpringTransactionManager pTransactionManager = new DroolsSpringTransactionManager(tm);

			Environment env = KnowledgeBaseFactory.newEnvironment();
			env.set(EnvironmentName.TRANSACTION_MANAGER, pTransactionManager);
			env.set("IS_JTA_TRANSACTION", true);

			EntityManagerFactory emf = (EntityManagerFactory) NkiaApplicationContext.getCtx().getBean("entityManagerFactory");
			env.set(EnvironmentName.ENTITY_MANAGER_FACTORY, emf);
			PersistenceContextManager persistenceContextManager = new DroolsSpringJpaManager(env);
			env.set(EnvironmentName.PERSISTENCE_CONTEXT_MANAGER, persistenceContextManager);

			CommandBasedStatefulKnowledgeSession session = (CommandBasedStatefulKnowledgeSession) JPAKnowledgeService.newStatefulKnowledgeSession(kbase, null, env);

			new JPAWorkingMemoryDbLogger(session);
			JPAProcessInstanceDbLog log = new JPAProcessInstanceDbLog(env);

			SystemEventListener systemEventListener = (SystemEventListener) NkiaApplicationContext.getCtx().getBean("systemEventListener");
			TaskService taskService = new TaskService(emf, systemEventListener);

			TaskSessionSpringFactoryImpl taskSessionFactory = new TaskSessionSpringFactoryImpl();
			taskSessionFactory.setUseJTA(true);
			taskSessionFactory.setEntityManagerFactory(emf);
			taskSessionFactory.setTransactionManager(hTransactionManager);
			taskSessionFactory.setTaskService(taskService);

			taskService.setTaskSessionFactory(taskSessionFactory);
			LocalTaskService taskClient = new LocalTaskService(taskService);
			MinaHTWorkItemHandler handler = new MinaHTWorkItemHandler(taskClient, session);
			handler.setLocal(true);
			session.getWorkItemManager().registerWorkItemHandler("Human Task", handler);

			//def.setIsolationLevel(AbstractPlatformTransactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION);
			
			HashMap<String, Object> txMap = new HashMap();
			txMap.put("taskClient", taskClient);
			txMap.put("session", session);
			txMap.put("taskService", taskService);
			NbpmThreadManager.setTxMap(txMap);
			
		} catch (TransactionException e) {
			this.close();
			throw new TransactionException(e.getMessage()) {};
		} catch (Exception e) {
			this.close();
			throw new TransactionException(e.getMessage()) {};
		}
		return tm.getTransaction(definition);
	}

	@Override
	public void commit(TransactionStatus status) throws TransactionException {
		try{
			
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if (tm != null && status != null) {
				tm.commit(status);
			}
			
		}finally{
			this.close();
		}
	}

	@Override
	public void rollback(TransactionStatus status) throws TransactionException {
		try{
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if (tm != null && status != null) {
				tm.rollback(status);
			}
			
		}finally{
			this.close();
		}
	}
	
	public void close() {
		getTaskClient().dispose();
		getSession().dispose();
		NbpmThreadManager.deleteSessionManager();
	}

}
