/*
 * @(#)NbpmCommonServiceImpl.java              2013. 5. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.common.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.automation.ui.WebixUIHandler;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.common.service.NbpmCommonService;

@Service("nbpmCommonService")
public class NbpmCommonServiceImpl implements NbpmCommonService {

	@Resource(name = "nbpmCommonDAO")
	private NbpmCommonDAO nbpmCommonDAO;
	
	@Resource(name = "webixUIHandler")
	private WebixUIHandler webixUIHandler;
	
	@Override
	public int searchProcessListCount(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return nbpmCommonDAO.searchProcessListCount(paramMap);
	}

	@Override
	public List searchProcessList(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return nbpmCommonDAO.searchProcessList(paramMap);
	}

	@Override
	public List searchProcessOperUserList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchProcessOperUserList(paramMap);
	}

	@Override
	public int searchProcessOperUserListCount(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchProcessOperUserListCount(paramMap);
	}

	@Override
	public List searchGridDataList(Map paramMap) throws NkiaException {
		String queryKey = (String)paramMap.get("gridSearchQueryKey");
		return nbpmCommonDAO.searchGridDataList(queryKey, paramMap);
	}

	@Override
	public List searchOperGridList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchOperGridList(paramMap);
	}

	@Override
	public int searchConfListCount(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchConfListCount(paramMap);
	}

	@Override
	public List searchConfList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchConfList(paramMap);
	}

	@Override
	public List searchIncLevelMatrix(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchIncLevelMatrix(paramMap);
	}

	public List searchSrRelationList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchSrRelationList(paramMap);
	}
	
	public List searchIncRelationList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchIncRelationList(paramMap);
	}
	
	public Map selectRelIncStatus(String sr_id) throws NkiaException {
		return nbpmCommonDAO.selectRelIncStatus(sr_id);
	}

	public Map searchIncLevelInfo(Map paramMap) throws NkiaException {
		Map result = new HashMap();
		Map incLevelInfo = nbpmCommonDAO.searchIncLevelInfo(paramMap);
		String effect = (String)incLevelInfo.get("effect");
		if(incLevelInfo != null) {
			String defaultUrgency = NkiaApplicationPropertiesMap.getProperty("Nbpm.DefaultUrgency.Value");
			if(defaultUrgency != null && !"".equals(defaultUrgency)) {
				
			} else {
				defaultUrgency = effect;
			}
			result.put("inc_level", (String)incLevelInfo.get("INC_LEVEL_" + defaultUrgency));
			result.put("inc_level_nm", (String)incLevelInfo.get("INC_LEVEL_NM_" + defaultUrgency));
			result.put("inc_level_time", (String)incLevelInfo.get("INC_LEVEL_TIME_" + defaultUrgency));	
			result.put("uegency_nm", (String)incLevelInfo.get("URGENCY_NM_" + defaultUrgency));
			result.put("effect_nm", (String)incLevelInfo.get("EFFECT_NM"));
		}
		return result;
	}

	@Override
	public List searchServiceList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchServiceList(paramMap);
	}
	
	@Override
	public List searchServiceTree(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchServiceTree(paramMap);
	}
	
	@Override
	public List searchProcessSubGrid(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchProcessSubGrid(paramMap);
	}

	@Override
	public List searchSelecetedConfRelServiceList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchSelecetedConfRelServiceList(paramMap);
	}

	@Override
	public List searchSelecetedConfRelAssetList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchSelecetedConfRelAssetList(paramMap);
	}

	@Override
	public UserVO selectUserInfoFromHpNo(String hp_no) throws NkiaException {
		UserVO result = nbpmCommonDAO.selectUserInfoFromHpNo(hp_no);
		if(result == null) {
			result = new UserVO();
		}
		return result;
	}
	
	/**
	 * 
	 * 프로세스 참조 List( 디자이너에서 생성된 쿼리정보를 기준으로 조회 ) / textfield + popup
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchProcessReferenceList(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchProcessReferenceList(paramMap);
	}
	
	@Override
	public List searchLinkRelProcess(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchLinkRelProcess(paramMap);
	}
	
	@Override
	public List searchMultiTaskGrid(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchMultiTaskGrid(paramMap);
	}
	
	/**
	 * PROCESSSID와 TASK_ID로 NodeName을 가져오는 메소드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public String getNbpmNodeName(ModelMap paramMap) throws NkiaException {
		return nbpmCommonDAO.getNbpmNodeName(paramMap);
	}

	@Override
	public List searchProcessGridField(ModelMap paramMap) throws NkiaException {
		return nbpmCommonDAO.searchProcessGridField(paramMap);
	}

	@Override
	public void insertProcessGridRecord(Map paramMap) throws NkiaException {
		String insertQuerykey = (String)paramMap.get("insertQuerykey");
		String deleteQuerykey = (String)paramMap.get("deleteQuerykey");
		
		paramMap = WebUtil.lowerCaseMapKey(paramMap);
		nbpmCommonDAO.deleteGridData(deleteQuerykey, paramMap);		
	
		List gridRows = (List)paramMap.get("gridlist");
		int LENGTH = gridRows.size();
		
		for(int i=0; i<LENGTH; i++){
			Map insertRowMap = new HashMap();
			insertRowMap = (HashMap)gridRows.get(i);
			insertRowMap = WebUtil.lowerCaseMapKey(insertRowMap);
			
			String srId = (String)insertRowMap.get("sr_id");
			if(srId == null){
				srId = (String)paramMap.get("sr_id");
				insertRowMap.put("sr_id", srId);
			}
			
			String procGridFieldId = nbpmCommonDAO.selectProcGridFieldId();
			insertRowMap.put("proc_grid_field_id", procGridFieldId);
			
			nbpmCommonDAO.insertGridData(insertQuerykey, insertRowMap);
		}
		
	}

	@Override
	public void deleteProcessGridRecord(ModelMap paramMap) throws NkiaException {
		String deleteQuerykey = (String)paramMap.get("deleteQuerykey");
		//모든키를 소문자로 변환해서 추가한다.
		webixUIHandler.replaceAddRowerKey(paramMap);
		nbpmCommonDAO.deleteGridData(deleteQuerykey, paramMap);
	}

	/**
	 * 담당자유형별 담당자그룹 매핑을 위한 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	@Override
	public Map selectGrpAppoint(HashMap paramMap) throws NkiaException {
		return nbpmCommonDAO.selectGrpAppoint(paramMap);
	}
}
