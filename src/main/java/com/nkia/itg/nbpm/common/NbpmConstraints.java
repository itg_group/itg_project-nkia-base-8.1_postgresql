/*
 * @(#)NbpmConstraints.java              2013. 3. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.common;

import java.io.Serializable;

public class NbpmConstraints implements Serializable{
	//@@20130726 김도원 START
	//변수 추가(프로젝트정보)
	public static final String PROJECT_NAME = "POC";
	//@@20130726 김도원 END
	//프로세스 화면 경로
	public static final String NBPM_PATH = "/itg/nbpm/processPage";
	//등록 화면 명
	public static final String REQUEST_PAGE = "REQUST_REGIST";
	//요청등록수정화면
	public static final String REQUST_REGIST_EDIT = "REQUST_REGIST_EDIT";
	
	//변경 계획수립 화면
	public static final String CHANGE_PLAN_EDIT = "CHANGE_PLAN_EDIT";

	//멀티 이력 보기 화면 명
	public static final String MULTI_END_VIEW_PAGE = "MULTI_END_VIEW";
	//멀티 수정 화면 명
	public static final String MULTI_END_EDIT_PAGE = "MULTI_END_EDIT";
	
	//이력보기 화면 명
	public static final String END_VIEW_PAGE = "END_VIEW";
	//수정 화면 명
	public static final String END_EDIT_PAGE = "END_EDIT";
	
	//회수 수정화면 명
	public static final String RECALL_EDIT_PAGE = "RECALL_EDIT";
	
	//상담원 롤 코드
	public static final String SERVICEDESK_ROLE = "CNSLR";
	//요청자 롤 코드
	public static final String REQUESTOR_ROLE = "RQESTER";
	//연관 장애 롤 코드
	public static final String REL_INC_ROLE = "REL_INC_ROLE";
	
	//작업함의 코드
	//대기함
	public static final String LIST_TYPE_READY = "READY";
	//진행함
	public static final String LIST_TYPE_ING = "ING";
	//완료함
	public static final String LIST_TYPE_COMPLETE = "COMPLETE";
	
	//BPM에서 데이터 처리 후 결과값에 담기는 파라메터 명
	public static final String NBPM_PARAM_WORKERIDS = "workerIds";
	public static final String NBPM_PARAM_STATE = "state";
	public static final String NBPM_PARAM_PROCID = "processInstanceId";
	public static final String NBPM_PARAM_NEXTTASKIDS = "nextTaskIds";
	public static final String NBPM_PARAM_NEXTNODEIDS = "nextNodeIds";
	public static final String NBPM_PARAM_NEXTNODENAMES = "nextNodeNames";
	//@@20130719 김도원 START
	//멀티프로세스관련하여 변수 추가
	public static final String NBPM_PARAM_SUBPROCESS = "subProcess";
    //@@20130719 김도원 END
	
	public static final String NBPM_STATE_ING = "ING";
	public static final String NBPM_STATE_END = "END";
	
	//WORK_STATE 코드정보 - 작업 상태
	//변경없음
	public static final String WORK_STATE_NON = "NONE";
	//임시저장
	public static final String WORK_STATE_TEMP = "TEMP";
	//대기
	public static final String WORK_STATE_WAIT = "WAIT";
	//준비
	public static final String WORK_STATE_READY = "READY";
	
	//TASK_NAME은 타스크의 영문명
	//start 타스크의 시작(코드상으로 사용)
	public static final String TASK_NAME_START = "start";
	//요청등록
	public static final String TASK_NAME_REQUEST = "REQUST_REGIST";
    //@@20130711 김도원 END
	
	//PROC_STATE 코드 정보
	//정상종료
	public static final String PROC_STATE_NRMLT = "NRMLT_END";
	//반려종료
	public static final String PROC_STATE_REJECT_END = "RETURN_END";
	//강제종료 - 시스템 상의 종료 같은 경우
	public static final String END_TYPE_FORCEEND = "FORCEEND";
	
	public static final String PUSH_TYPE_SERVICEDESK = "sdesk";
	public static final String PUSH_TYPE_ALRAM = "alram";
	
	public static final String REQ_TYPE_DISCONTENT = "discontent";
	
	
	//WORK TYPE에 대한 정의 - 화면의 버튼 액션에 따른 타입을 말함
	//정상처리
	public static final String WORKTYPE_NORMAL_PROC = "NRMLT_PROC";
	//재지정처리
	public static final String WORKTYPE_REASSIGN_PROC = "REAPPN_PROC";
	//반려처리
	public static final String WORKTYPE_RETURN_PROC = "RETURN_PROC";
	//반려종료처리
	public static final String WORKTYPE_RETURN_END_PROC = "RETURN_END_PROC";
	//1선처리
	public static final String WORKTYPE_FIRST_PROC = "FIRST_PROC";
	//이관처리
	public static final String WORKTYPE_TRNSFER_PROC = "TRNSFER_PROC";
	
	public static String getPushTypeServicedesk() {
		return PUSH_TYPE_SERVICEDESK;
	}
	public static String getPushTypeEtc() {
		return PUSH_TYPE_ALRAM;
	}
}
