/*
 * @(#)NbpmCommonController.java              2013. 5. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.common.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.service.NbpmCommonService;
import com.nkia.itg.nbpm.edit.service.EditExecutor;
import com.nkia.itg.nbpm.mylist.service.MyListExecutor;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.servicedesk.service.ServicedeskListExecutor;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class NbpmCommonController {
	
	@Resource(name = "nbpmCommonService")
	private NbpmCommonService nbpmCommonService;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name="myListCommonExecutor")
	private MyListExecutor myListExecutor;
	
	@Resource(name="servicedeskCommonListExecutor")
	private ServicedeskListExecutor servicedeskListExecutor;
	
	@Resource(name="editCommonExecutor")
	private EditExecutor editExecutor;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/common/searchProcessList.do")
	public @ResponseBody ResultVO searchProcessList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			//paramMap.put("user_id", userId);
			
			totalCount = nbpmCommonService.searchProcessListCount(paramMap);
			resultList = nbpmCommonService.searchProcessList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/nbpm/common/searchProcessOperUserList.do")
	public @ResponseBody ResultVO searchProcessOperUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			
			//page=1, start=0, limit=10 있는 경우, 
			if (paramMap.get("page") != null) {
				int totalCount = 0;
				PagingUtil.getFristEndNum(paramMap);
				
				totalCount = nbpmCommonService.searchProcessOperUserListCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			}
			
			resultList = nbpmCommonService.searchProcessOperUserList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/goRelationRequestPop.do")
	public String goRelationRequestPop(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/common/relationRequestPop.nvf";
		String up_sr_id = request.getParameter("up_sr_id");
		String processType = request.getParameter("processType");
		String targetFormId = request.getParameter("targetFormId");
		String updateCol = request.getParameter("updateCol");
		String updateTable = request.getParameter("updateTable");
		String submitType = request.getParameter("submitType");
		String reqType = request.getParameter("reqType");
		String subReqType = request.getParameter("subReqType");
		String subTyCd = request.getParameter("subTyCd");
		String reloadGridId = request.getParameter("reloadGridId");
		String url = request.getParameter("ref_url");
		
		
		String field_name = request.getParameter("field_name");
		paramMap.put("up_sr_id", up_sr_id);
		paramMap.put("field_name", field_name);
		paramMap.put("processType", processType);
		paramMap.put("targetFormId", targetFormId);
		paramMap.put("updateCol", updateCol);
		paramMap.put("updateTable", updateTable);
		paramMap.put("submitType", submitType);
		paramMap.put("reqType", reqType);
		paramMap.put("subReqType", subReqType);
		paramMap.put("subTyCd", subTyCd);
		paramMap.put("reloadGridId", reloadGridId);
		paramMap.put("ref_url", url);
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/common/goRelationRequestPopEdit.do")
	public String goRelationRequestPopEdit(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/common/relationRequestPopEdit.nvf";
		String up_sr_id = request.getParameter("up_sr_id");
		String processType = request.getParameter("processType");
		String targetFormId = request.getParameter("targetFormId");
		String updateCol = request.getParameter("updateCol");
		String updateTable = request.getParameter("updateTable");
		String submitType = request.getParameter("submitType");
		String reqType = request.getParameter("reqType");
		String subReqType = request.getParameter("subReqType");
		String subTyCd = request.getParameter("subTyCd");
		String reloadGridId = request.getParameter("reloadGridId");
		String url = request.getParameter("ref_url");		
		String setData = request.getParameter("setData");
		//setData = setData.replaceAll("&quot;", "\"");
				
		String field_name = request.getParameter("field_name");
		paramMap.put("up_sr_id", up_sr_id);
		paramMap.put("field_name", field_name);
		paramMap.put("processType", processType);
		paramMap.put("targetFormId", targetFormId);
		paramMap.put("updateCol", updateCol);
		paramMap.put("updateTable", updateTable);
		paramMap.put("submitType", submitType);
		paramMap.put("reqType", reqType);
		paramMap.put("subReqType", subReqType);
		paramMap.put("subTyCd", subTyCd);
		paramMap.put("reloadGridId", reloadGridId);
		paramMap.put("ref_url", url);
		paramMap.put("setData", setData);
		return forwarPage;
	}

	@RequestMapping(value="/itg/nbpm/common/goCiServiceSelectPop.do")
	public String goCiServiceSelectPop(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/common/ciServiceSelectPop.nvf";
		String popId = request.getParameter("popId");
		paramMap.put("popId", popId);
		return forwarPage;
	}	
	
	@RequestMapping(value="/itg/nbpm/common/goCiAssetSelectPop.do")
	public String goCiSelectPopup(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/common/ciAssetSelectPop.nvf";
		String popId = request.getParameter("popId");
		paramMap.put("popId", popId);
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/common/goRequestPop.do")
	public String goRequestPop(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/common/requestPop.nvf";
		String sr_id = request.getParameter("sr_id");
		String hp_no = request.getParameter("hp_no");
		String req_path = request.getParameter("req_path");
		String sdesk_yn = request.getParameter("sdesk_yn");
		sdesk_yn = StringUtil.replaceNull(sdesk_yn, "");
		
		//서비스데스크에서 팝업으로 요청할 경우 End
		UserVO userVO = null;
		String reqUserId = "";
		if(hp_no != null && !"".equals(hp_no)) {
			userVO = nbpmCommonService.selectUserInfoFromHpNo(hp_no);
			reqUserId = userVO.getUser_id();
		}
		
		if(!(sr_id != null && !"".equals(sr_id))) {
			sr_id = processCommonExecutor.createSrId();
		}
		
		paramMap.put("sr_id", sr_id);
		paramMap.put("hp_no", hp_no);
		paramMap.put("req_path", req_path);
		paramMap.put("sdesk_yn", sdesk_yn);
		paramMap.put("req_user_id", reqUserId);
		return forwarPage;
	}

	@RequestMapping(value="/itg/nbpm/common/searchOperGridList.do")
	public @ResponseBody ResultVO searchOperGridList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try {
			resultList = nbpmCommonService.searchOperGridList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	@RequestMapping(value="/itg/nbpm/common/searchGridDataList.do")
	public @ResponseBody ResultVO searchGridDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try {
			resultList = nbpmCommonService.searchGridDataList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchConfList.do")
	public @ResponseBody ResultVO searchConfList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			String assetState = (String)paramMap.get("asset_state");
			if(assetState != null) {
				String[] assetStateArry = assetState.split(",");
				paramMap.put("assetStateArry", assetStateArry);				
			}
			
			totalCount = nbpmCommonService.searchConfListCount(paramMap);
			resultList = nbpmCommonService.searchConfList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchServiceList.do")
	public @ResponseBody ResultVO searchServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			CheckTreeVO treeVO = null;
			List resultList = nbpmCommonService.searchServiceList(paramMap);	
			String up_node_id = StringUtil.parseString(paramMap.get("up_node_id"));
			int expand_level = StringUtil.parseInteger(paramMap.get("expandLevel"));
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, true);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchServiceTree.do")
	public @ResponseBody ResultVO searchServiceTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			TreeVO treeVO = null;
			List resultList = nbpmCommonService.searchServiceTree(paramMap);	
			String up_node_id = StringUtil.parseString(paramMap.get("up_node_id"));
			int expand_level = StringUtil.parseInteger(paramMap.get("expandLevel"));
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchSelecetedConfRelServiceList.do")
	public @ResponseBody ResultVO searchSelecetedConfRelServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchSelecetedConfRelServiceList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchIncLevelMatrix.do")
	public @ResponseBody ResultVO searchIncLevelMatrix(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchIncLevelMatrix(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchIncLevelInfo.do")
	public @ResponseBody ResultVO searchIncLevelInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		Map resultMap = new HashMap();
		ResultVO resultVO = new ResultVO();
		try {
			resultMap = nbpmCommonService.searchIncLevelInfo(paramMap);		
			resultVO.setResultMap((HashMap)resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//@@20130711 김도원 START
	//연계정보 조회기능
	@RequestMapping(value="/itg/nbpm/common/searchIncRelationList.do")
	public @ResponseBody ResultVO searchIncRelationList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			//UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			//if(userVO != null){
				resultList = nbpmCommonService.searchIncRelationList(paramMap);
				gridVO.setTotalCount(resultList.size());
				gridVO.setRows(resultList);
			//}
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/selectRelIncStatus.do")
	public @ResponseBody ResultVO selectRelIncStatus(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			String sr_Id = (String)paramMap.get("sr_id");
			Map resultMap = nbpmCommonService.selectRelIncStatus(sr_Id);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	//연관장애 조회기능
	@RequestMapping(value="/itg/nbpm/common/searchSrRelationList.do")
	public @ResponseBody ResultVO searchSrRelationList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			//UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			//if(userVO != null){
				resultList = nbpmCommonService.searchSrRelationList(paramMap);
				gridVO.setTotalCount(resultList.size());
				gridVO.setRows(resultList);
			//}
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/searchProcessSubGrid.do")
	public @ResponseBody ResultVO searchProcessSubGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchProcessSubGrid(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchProcessGridField.do")
	public @ResponseBody ResultVO searchProcessGridField(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchProcessGridField(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/insertProcessGridRecord.do")
	public @ResponseBody ResultVO insertProcessGridRecord(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String upd_user_id = userVO.getUser_id();
			paramMap.put("upd_user_id", upd_user_id); 
			nbpmCommonService.insertProcessGridRecord(paramMap);		
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/deleteProcessGridRecord.do")
	public @ResponseBody ResultVO deleteProcessGridRecord(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			nbpmCommonService.deleteProcessGridRecord(paramMap);		
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
		
	@RequestMapping(value="/itg/nbpm/common/searchLinkRelProcess.do")
	public @ResponseBody ResultVO searchLinkRelProcess(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchLinkRelProcess(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	@RequestMapping(value="/itg/nbpm/common/searchSelecetedConfRelAssetList.do")
	public @ResponseBody ResultVO searchSelecetedConfRelAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchSelecetedConfRelAssetList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 업무(구성/서비스)리스트
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/common/searchRelConfList.do")
	public @ResponseBody ResultVO searchRelConfList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = processCommonExecutor.searchRelConfListCount(paramMap);
			List resultList = new ArrayList();
			resultList = processCommonExecutor.searchRelConfList(paramMap);		
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 프로세스 참조 List( 디자이너에서 생성된 쿼리정보를 기준으로 조회 ) / textfield + popup
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchProcessReferenceList.do")
	public @ResponseBody ResultVO searchProcessReferenceList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchProcessReferenceList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/deleteLinkRelSrData.do")
	public @ResponseBody ResultVO deleteLinkRelSrData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			editExecutor.deleteSrData(paramMap, loginUserId);
			resultVO.setResultMsg("삭제되었습니다."); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	@RequestMapping(value="/itg/nbpm/common/getLoginUserInfo.do")
	public @ResponseBody ResultVO getLoginUserInfo(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String userNm = userVO.getUser_nm();
			HashMap resultMap = new HashMap();
			resultMap.put("user_id", userId);
			resultMap.put("user_nm", userNm);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchMyListCount.do")
	public @ResponseBody ResultVO searchMyListCount(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			paramMap.put("user_id", userId);
			
			int readyCount;
			int ingCount;
			boolean isSM = paramMap.containsKey("isServicedeskForMain") ? true : false;

			if(isSM){
				readyCount = myListExecutor.searchReadyMyListCount(paramMap);
				ingCount = myListExecutor.searchIngMyListCount(paramMap);
			}else{
				readyCount = myListExecutor.searchReadyMyListCount(paramMap);
				ingCount = myListExecutor.searchIngMyListCount(paramMap);
			}

			HashMap map = new HashMap();
			map.put("ready", readyCount);
			map.put("ing", ingCount);
		
			resultVO.setResultMap(map);
			resultVO.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/searchMultiTaskGrid.do")
	public @ResponseBody ResultVO searchMultiTaskGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			resultList = nbpmCommonService.searchMultiTaskGrid(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/common/getNbpmNodeName.do")
	public @ResponseBody ResultVO getNbpmNodeName(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			String nodeName = nbpmCommonService.getNbpmNodeName(paramMap);		
			resultVO.setResultString(nodeName);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
}
