package com.nkia.itg.nbpm.vo;

import java.io.Serializable;
import java.util.ArrayList;

public class NbpmEmailVO implements Serializable {

	private String type;
	private Integer count;
	private Integer loopcondition;
	public ArrayList nodeIds;
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Integer getLoopcondition() {
		return loopcondition;
	}
	public void setLoopcondition(Integer loopcondition) {
		this.loopcondition = loopcondition;
	}
	public ArrayList getNodeIds() {
		return nodeIds;
	}
	public void setNodeIds(ArrayList nodeIds) {
		this.nodeIds = nodeIds;
	}
	
	
}
