package com.nkia.itg.nbpm.vo;

import java.io.Serializable;


public class Message implements Serializable {

	private String status = "test";
	private long message;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getMessage() {
		return message;
	}
	public void setMessage(long message) {
		this.message = message;
	}
}
