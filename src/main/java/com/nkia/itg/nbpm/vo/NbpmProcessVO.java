package com.nkia.itg.nbpm.vo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//@@20130719 김도원 START
//불필요한 변수를 삭제
//@@20130719 김도원 END
public class NbpmProcessVO {

	private String sr_id = null;
	public List<LinkedHashMap<String, String>> operList = null;
	public List itemList = null;
	private String req_user_id = null;
	private String worker_user_id = null;
	private long task_id = 0;
	private String processId = null;
	private long processInstanceId = 0;
	private String task_name = null;
	private String process_name = null;
	private String process_type = null;
	private String version = null;
	private String next_node_id = null;
	private String workType = null;
	private String comment = null;
	private String work_state = null;
	public Map expansionParamMap = null;
	private String nodename = null;
	private String current_worker_id = null;
	private String status = null;
	public List gridMapList = null;
	private String sub_req_type = null;
	private String task_group_id = null;
	private String multi_yn = null;
	private String batch_proc_yn = null;
	private String sanctn_line_slct_yn = null;
	private boolean read_mode;
	public List gridFieldList = null;
	private String faq_yn = null;
	private String process_statis_yn = null;
	private String grp_slct_yn = null;
	private String sanctn_grp_slct_yn = null;
	private String emailSended = null;
	private String req_doc_id = null;
	
	public String getProcess_statis_yn() {
		return process_statis_yn;
	}
	public void setProcess_statis_yn(String process_statis_yn) {
		this.process_statis_yn = process_statis_yn;
	}
	public List getGridFieldList() {
		return gridFieldList;
	}
	public void setGridFieldList(List gridFieldList) {
		this.gridFieldList = gridFieldList;
	}
	public List getGridMapList() {
		return gridMapList;
	}
	public void setGridMapList(List gridMapList) {
		this.gridMapList = gridMapList;
	}
	public String getCurrent_worker_id() {
		return current_worker_id;
	}
	public void setCurrent_worker_id(String current_worker_id) {
		this.current_worker_id = current_worker_id;
	}
	public String getNodename() {
		return nodename;
	}
	public void setNodename(String nodename) {
		this.nodename = nodename;
	}
	public String getWork_state() {
		return work_state;
	}
	public void setWork_state(String work_state) {
		this.work_state = work_state;
	}
	public String getReq_user_id() {
		return req_user_id;
	}
	public void setReq_user_id(String req_user_id) {
		this.req_user_id = req_user_id;
	}
	public String getWorker_user_id() {
		return worker_user_id;
	}
	public void setWorker_user_id(String worker_user_id) {
		this.worker_user_id = worker_user_id;
	}
	
	public List getItemList() {
		return itemList;
	}
	public void setItemList(List itemList) {
		this.itemList = itemList;
	}
	public List<LinkedHashMap<String, String>> getOperList() {
		return operList;
	}
	public void setOperList(List<LinkedHashMap<String, String>> operList) {
		this.operList = operList;
	}
	public String getSr_id() {
		return sr_id;
	}
	public void setSr_id(String sr_id) {
		this.sr_id = sr_id;
	}
	public long getTask_id() {
		return task_id;
	}
	public void setTask_id(long task_id) {
		this.task_id = task_id;
	}
	public String getTask_name() {
		return task_name;
	}
	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}
	public String getProcess_name() {
		return process_name;
	}
	public void setProcess_name(String process_name) {
		this.process_name = process_name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getProcess_type() {
		return process_type;
	}
	public void setProcess_type(String process_type) {
		this.process_type = process_type;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public long getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getNext_node_id() {
		return next_node_id;
	}
	public void setNext_node_id(String next_node_id) {
		this.next_node_id = next_node_id;
	}
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Map getExpansionParamMap() {
		return expansionParamMap;
	}
	public void setExpansionParamMap(Map expansionParamMap) {
		this.expansionParamMap = expansionParamMap;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSub_req_type() {
		return sub_req_type;
	}
	public void setSub_req_type(String sub_req_type) {
		this.sub_req_type = sub_req_type;
	}
	public String getTask_group_id() {
		return task_group_id;
	}
	public void setTask_group_id(String task_group_id) {
		this.task_group_id = task_group_id;
	}
	public String getMulti_yn() {
		return multi_yn;
	}
	public void setMulti_yn(String multi_yn) {
		this.multi_yn = multi_yn;
	}
	public String getSanctn_line_slct_yn() {
		return sanctn_line_slct_yn;
	}
	public String getSanctn_grp_slct_yn() {
		return sanctn_grp_slct_yn;
	}
	public void setSanctn_line_slct_yn(String sanctn_line_slct_yn) {
		this.sanctn_line_slct_yn = sanctn_line_slct_yn;
	}
	public String getBatch_proc_yn() {
		return batch_proc_yn;
	}
	public void setBatch_proc_yn(String batch_proc_yn) {
		this.batch_proc_yn = batch_proc_yn;
	}
	public boolean isRead_mode() {
		return read_mode;
	}
	public void setRead_mode(boolean read_mode) {
		this.read_mode = read_mode;
	}
	public String getFaq_yn() {
		return faq_yn;
	}
	public void setFaq_yn(String faq_yn) {
		this.faq_yn = faq_yn;
	}
	public String getGrp_slct_yn() {
		return grp_slct_yn;
	}
	public void setGrp_slct_yn(String grp_slct_yn) {
		this.grp_slct_yn = grp_slct_yn;
	}
	public void setSanctn_grp_slct_yn(String sanctn_grp_slct_yn) {
		this.sanctn_grp_slct_yn = sanctn_grp_slct_yn;
	}
	public String getEmailSended() {
		return emailSended;
	}
	public void setEmailSended(String emailSended) {
		this.emailSended = emailSended;
	}
	public String getReq_doc_id() {
		return req_doc_id;
	}
	public void setReq_doc_id(String req_doc_id) {
		this.req_doc_id = req_doc_id;
	}
	
}
