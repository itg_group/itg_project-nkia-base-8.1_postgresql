/*
 * @(#)NbpmOperatorVO.java              2013. 3. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.vo;

public class NbpmOperatorVO {

	private String sr_id;
	private String oper_user_id;
	private String oper_type;
	private String seq;
	
	public String getSr_id() {
		return sr_id;
	}
	public void setSr_id(String sr_id) {
		this.sr_id = sr_id;
	}
	public String getOper_user_id() {
		return oper_user_id;
	}
	public void setOper_user_id(String oper_user_id) {
		this.oper_user_id = oper_user_id;
	}
	public String getOper_type() {
		return oper_type;
	}
	public void setOper_type(String oper_type) {
		this.oper_type = oper_type;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
}
