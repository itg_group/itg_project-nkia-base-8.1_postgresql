package com.nkia.itg.nbpm.vo;

import java.io.Serializable;

public class NbpmRuleVO implements Serializable {
	private long processInstanceId;
	private String process_type = null;
	
	public long getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(long processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcess_type() {
		return process_type;
	}

	public void setProcess_type(String process_type) {
		this.process_type = process_type;
	}
	
}
