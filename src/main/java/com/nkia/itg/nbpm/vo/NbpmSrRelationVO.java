package com.nkia.itg.nbpm.vo;

import com.nkia.itg.base.vo.TreeVO;

public class NbpmSrRelationVO extends TreeVO {
	private String sr_id;
	private String req_type_nm;
	private String title;
	private String work_state_nm;
	private String req_dt;
	private String end_dt;
	
	public String getSr_id() {
		return sr_id;
	}
	public void setSr_id(String sr_id) {
		this.sr_id = sr_id;
	}
	public String getReq_type_nm() {
		return req_type_nm;
	}
	public void setReq_type_nm(String req_type_nm) {
		this.req_type_nm = req_type_nm;
	}
	public String getWork_state_nm() {
		return work_state_nm;
	}
	public void setWork_state_nm(String work_state_nm) {
		this.work_state_nm = work_state_nm;
	}
	public String getReq_dt() {
		return req_dt;
	}
	public void setReq_dt(String req_dt) {
		this.req_dt = req_dt;
	}
	public String getEnd_dt() {
		return end_dt;
	}
	public void setEnd_dt(String end_dt) {
		this.end_dt = end_dt;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
