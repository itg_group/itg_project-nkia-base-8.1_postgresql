/*
 * @(#)ExtjsPrototypeService.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.processFlowChart.service;

import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ProcessFlowChartService {

	List searchProcessFlowList(String sr_id)throws NkiaException;

}
