/*
 * @(#)SampleCodeController.java              2013. 1. 28.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.processFlowChart.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.processFlowChart.service.ProcessFlowChartService;

/**
 * @version 1.0 2013. 1. 28.
 * @author <a href="mailto:sangha@nkia.co.kr"> Sang Ha
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class ProcessFlowChartController {
	
	@Resource(name = "processFlowChartService")
	private ProcessFlowChartService processFlowChartService;
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/processFlowChart/ProcessFlowChart.do")
	public String processFlowChart(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/sampleProcessFlowChart.nvf";
		
		List processFlowList = new ArrayList();
		
		try{
			//KEY			
			String sr_id = request.getParameter("sr_id");
			paramMap.addAttribute("sr_id", sr_id);
			
			//Getting Search Part Contents
			processFlowList = processFlowChartService.searchProcessFlowList(sr_id);
			
			//Parse to Json format
			JSONArray processFlowListJson = JSONArray.fromObject(processFlowList);
			paramMap.addAttribute("processFlowListJson", processFlowListJson);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return forwarPage;
	}
	
	
	
}
