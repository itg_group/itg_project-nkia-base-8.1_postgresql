/*
 * @(#)ExtjsPrototypeServiceImpl.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.processFlowChart.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.processFlowChart.dao.ProcessFlowChartDAO;
import com.nkia.itg.nbpm.processFlowChart.service.ProcessFlowChartService;

@Service("processFlowChartService")
public class ProcessFlowChartServiceImpl implements ProcessFlowChartService{

	@Resource(name = "processFlowChartDAO")
	public ProcessFlowChartDAO processFlowChartDAO;
	
	public List searchProcessFlowList(String sr_id) throws NkiaException {
		// TODO Auto-generated method stub
		return processFlowChartDAO.searchProcessFlowList(sr_id);
	}

	
}
