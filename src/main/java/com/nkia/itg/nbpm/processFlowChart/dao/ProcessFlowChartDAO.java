/*
 * @(#)ExtjsPrototypeDAO.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.processFlowChart.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * 
 * @version 1.0 2013. 1. 15.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("processFlowChartDAO")
public class ProcessFlowChartDAO extends EgovComAbstractDAO{

	public List searchProcessFlowList(String sr_id)throws NkiaException {
		List resultList = null;
		resultList = list("ProcessFlowChartDAO.searchProcessFlowList", sr_id);
		return resultList;
	}
	
}
