package com.nkia.itg.nbpm.excpetion;

public class NbpmRuleException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public NbpmRuleException(Exception e) {
		super(e);
	}
	
	public NbpmRuleException(String message) {
		super(message);
	}
}