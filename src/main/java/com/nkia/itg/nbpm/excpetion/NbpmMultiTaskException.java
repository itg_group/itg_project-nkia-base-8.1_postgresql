package com.nkia.itg.nbpm.excpetion;

public class NbpmMultiTaskException extends Exception {

	public NbpmMultiTaskException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = -7004219349869299104L;

}
