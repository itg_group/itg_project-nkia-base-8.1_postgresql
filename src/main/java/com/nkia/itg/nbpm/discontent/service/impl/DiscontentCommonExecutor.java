package com.nkia.itg.nbpm.discontent.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.common.NbpmTransactionManager;
import com.nkia.itg.nbpm.discontent.dao.DiscontentDAO;
import com.nkia.itg.nbpm.discontent.service.DiscontentExecutor;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

@Service("discontentService")
public class DiscontentCommonExecutor implements DiscontentExecutor {

	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "discontentDAO")
	private DiscontentDAO discontentDAO;
	
	@Override
	public int searchDiscontentListCount(Map paramMap) throws NkiaException {
		return discontentDAO.searchDiscontentListCount(paramMap);
	}

	@Override
	public List searchDiscontentList(Map paramMap) throws NkiaException {
		return discontentDAO.searchDiscontentList(paramMap);
	}

	@Override
	public Map selectDiscontentDetail(String srId) throws NkiaException, SQLException {
		return discontentDAO.selectDiscontentDetail(srId);
	}

	private Map selectDiscontentDetailForRegister(String srId) throws NkiaException, SQLException {
		Map temp = discontentDAO.selectDiscontentDetailForRegister(srId);
		Map result = new HashMap();
		if (temp != null) {
			Iterator keySet = temp.keySet().iterator();
			while(keySet.hasNext()) {
				String key = (String)keySet.next();
				if("TITLE".equals(key)) {
					result.put(key.toLowerCase(), "[불만요청] " + temp.get(key));
				} else if("CONTENT".equals(key)) {
					String statisResultNm = (String)temp.get("STATIS_RESULT_NM");
					StringBuffer content = new StringBuffer();
					content.append("만족도결과 : ");
					content.append(statisResultNm);
					content.append("\n");
					content.append("만족도의견 : ");
					content.append(temp.get(key));
					result.put(key.toLowerCase(), content.toString());
				} else {
					result.put(key.toLowerCase(), temp.get(key));				
				}
			}
		}
		return result;
	}
	
	@Override
	public void registerDiscontentReq(Map paramMap, UserVO userVO) throws NkiaException {
		try {
			String newSrId = processCommonExecutor.createSrId();
			String srId = (String)paramMap.get("sr_id");
			Map reqInfoMap = selectDiscontentDetailForRegister(srId);
			reqInfoMap.put("sr_id", newSrId);
			
			String loginUserId = userVO.getUser_id();
			String reqUserId = (String)reqInfoMap.get("REQ_USER_ID");
			
			NbpmProcessVO processVO = processCommonExecutor.selectFinalVersionProcessInfo(NbpmConstraints.REQ_TYPE_DISCONTENT);
			processVO.setSr_id(newSrId);
			reqInfoMap.put("req_type", NbpmConstraints.REQ_TYPE_DISCONTENT);
			
			NbpmUtil.setRegisterForData(processVO, reqUserId, loginUserId);
			
			processCommonExecutor.workTask(processVO, reqInfoMap);
			
			discontentDAO.updateDiscontentReq(srId);

		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}

}
