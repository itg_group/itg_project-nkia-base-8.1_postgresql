package com.nkia.itg.nbpm.discontent.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.discontent.service.DiscontentExecutor;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class DiscontentController {
	
	@Resource(name="discontentService")
	private DiscontentExecutor discontentService;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@RequestMapping(value="/itg/nbpm/discontent/goDiscontentTab.do")
	public String goDiscontentTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/discontent/discontentTab.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/discontent/goDiscontentMain.do")
	public String goDiscontentMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/discontent/discontentMain.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/discontent/goDiscontentDetailPage.do")
	public String goDiscontentDetailPage(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String sr_id = request.getParameter("sr_id");
		paramMap.put("sr_id", sr_id);
		String forwarPage = "/itg/nbpm/discontent/discontentDetail.nvf";
		return forwarPage;
	}
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/discontent/searchDiscontentList.do")
	public @ResponseBody ResultVO searchDiscontentList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			ArrayList discontentList = new ArrayList();
			String codeValue = NkiaApplicationPropertiesMap.getProperty("Nbpm.Discontent.Result");
			String[] disResults = codeValue.split(",");
			for(int i = 0 ; i < disResults.length ; i++) {
				discontentList.add(disResults[i]);
			}

			paramMap.put("user_id", userId);
			paramMap.put("discontentList", discontentList);
			
			totalCount = discontentService.searchDiscontentListCount(paramMap);
			resultList = discontentService.searchDiscontentList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	@RequestMapping(value="/itg/nbpm/discontent/selectDiscontentDetail.do")
	public @ResponseBody ResultVO selectDiscontentDetail(@RequestBody ModelMap paramMap) throws NkiaException {
		Map resultMap = null;
		ResultVO result = new ResultVO();
		try{
			String srId = (String)paramMap.get("sr_id");
			resultMap = discontentService.selectDiscontentDetail(srId);
			result.setResultMap((HashMap)resultMap);
			result.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	@RequestMapping(value="/itg/nbpm/discontent/registerDiscontentReq.do")
	public @ResponseBody ResultVO registerDiscontentReq(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO result = new ResultVO();
		try{			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();				
			discontentService.registerDiscontentReq(paramMap, userVO);
			
			result.setResultMsg("등록되었습니다.");
			result.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
}
