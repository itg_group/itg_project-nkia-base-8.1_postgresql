package com.nkia.itg.nbpm.discontent.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("discontentDAO")
public class DiscontentDAO extends NbpmAbstractDAO {

	public int searchDiscontentListCount(Map paramMap) {
		return (Integer)selectByPk("DiscontentDAO.searchDiscontentListCount", paramMap);
	}

	public List searchDiscontentList(Map paramMap) {
		return list("DiscontentDAO.searchDiscontentList", paramMap);
	}

	public Map selectDiscontentDetailForRegister(String srId) throws SQLException {
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("DiscontentDAO.selectDiscontentDetail", srId, handler);
		return handler.getReturnMap();
	}

	public void updateDiscontentReq(String srId) throws SQLException {
		this.getSqlMapClient().update("DiscontentDAO.updateDiscontentReq", srId);
	}

	public Map selectDiscontentDetail(String srId) throws SQLException {
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("DiscontentDAO.selectProcMasterDetail", srId, handler);
		return handler.getReturnMap();
	}

}
