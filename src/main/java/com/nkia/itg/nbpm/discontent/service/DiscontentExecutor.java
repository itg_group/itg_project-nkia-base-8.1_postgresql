package com.nkia.itg.nbpm.discontent.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

public interface DiscontentExecutor {

	public int searchDiscontentListCount(Map paramMap) throws NkiaException;

	public List searchDiscontentList(Map paramMap) throws NkiaException;

	public Map selectDiscontentDetail(String paramMap) throws NkiaException, SQLException;
	
	public void registerDiscontentReq(Map paramMap, UserVO userVO) throws NkiaException;

}
