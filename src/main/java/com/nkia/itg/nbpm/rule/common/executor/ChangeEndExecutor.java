package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;


public class ChangeEndExecutor implements Executor {

	@Override
	public void execute(long procId) throws Exception {

		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");

		String srId = commonDAO.selectSrId(procId);
		String upSrId = commonDAO.changeEndUpsrId(srId);
		
		//마지막 연계요청일 경우만 상위 요청의 대기처리
		int notEndCount = commonDAO.selectEndCount(upSrId);

		//조건
		//상위연계요청있음 && 상위연계요청의 REQ_TYPE이 작업요청 or 서버폐기요청인 경우
		//상위연계요청에서 대기중인 [변경요청] 임시태스크 진행
		if(notEndCount == 0){
		
			Map searchMap = new HashMap();
			searchMap.put("sr_id", upSrId);
			searchMap.put("task_name", "TEMP_FOR_CHANGE");
			List insertList = new ArrayList();
			insertList = commonDAO.selectTempTaskConfirm(searchMap);
			
			// 조건,
			// PROC_MASTER.PROC_ID = TASK.PROCESSINSTANCEID
			// TASK.TASK_NAME like 'TEMP_FOR_CHANGE%'
			// TASK.STATUS = 'Reserved' 
			if(insertList.size() > 0){
				for(int i=0; i<insertList.size(); i++){
					Map processInfoMap = new HashMap();
					processInfoMap = (Map) insertList.get(i);
										
					Map insertMap = new HashMap();
					insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
					insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
					insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
					insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
					insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
					insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
				//	insertMap.put("nbpm_comment", "");
					
					nbpmCommonClient.workTaskForRule(insertMap);
				}
			}
		}
	}
}
