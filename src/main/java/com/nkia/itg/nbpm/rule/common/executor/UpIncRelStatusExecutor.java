package com.nkia.itg.nbpm.rule.common.executor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

public class UpIncRelStatusExecutor implements Executor {

	/**
	 * 요청등록에서 연계요청시 하위요청은 
	 * 임시 태스크로 이동후 메인요청 등록시 강제진행
	 * @throws Exception 
	 * 
	 */
	//연관장애 SR_ID의 작업상태값 체크하여 임시TASK, 또는 요청종료 
	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		SmsSendDAO smsSendDAO = (SmsSendDAO)NkiaApplicationContext.getCtx().getBean("smsSendDAO");
		SmsgHistDAO smsgHistDAO = (SmsgHistDAO)NkiaApplicationContext.getCtx().getBean("smsgHistDAO");
		
		String sr_id = commonDAO.selectSrId(procId);
		//연관장애의 작업상태값
		String rel_inc_work_state = commonDAO.selectRelIncWorkState(sr_id); 
		
		//연관장애의 작업상태가 결과통보, 요청종료인 경우 프로세스 추가진행(종료)
		if("NOTI".equals(rel_inc_work_state) || "END".equals(rel_inc_work_state)){
			List insertList = new ArrayList();
			insertList = commonDAO.selectIncInfo(sr_id);
			
			if(insertList.size() > 0){
				for(int j=0; j<insertList.size(); j++){
					Map processInfoMap = new HashMap();
					processInfoMap = (Map) insertList.get(j);
										
					Map insertMap = new HashMap();
					insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
					insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
					insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
					insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
					insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
					insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
					
					nbpmCommonClient.workTaskForRule(insertMap);
				}
			}
		}
		
		/*//해당요청을 상위요청으로 삼는 SR번호 리스트
		List LowerSrList = new ArrayList();
		LowerSrList = commonDAO.selectLowerSrId(sr_id);
		Map srMap = new HashMap();
		
		if(LowerSrList.size() > 0){
			srMap.put("proc_auth_id", "SYSTEM");
			srMap.put("task_name", "TEMP_LINK_SERVICE");
			for(int i=0; i<LowerSrList.size(); i++){
				Map LowerSrMap = new HashMap();
				LowerSrMap = (Map) LowerSrList.get(i);
				srMap.put("sr_id", (String)LowerSrMap.get("SR_ID"));
				
				List insertList = new ArrayList();
				insertList = commonDAO.selectTempTaskConfirm(srMap);
				
				if(insertList.size() > 0){
					for(int j=0; j<insertList.size(); j++){
						Map processInfoMap = new HashMap();
						processInfoMap = (Map) insertList.get(j);
											
						Map insertMap = new HashMap();
						insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
						insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
						insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
						insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
						insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
						insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
					//	insertMap.put("nbpm_comment", "");
						
						nbpmCommonClient.workTaskForRule(insertMap);
					}
				}
			
			}
		}	*/
	}
}
