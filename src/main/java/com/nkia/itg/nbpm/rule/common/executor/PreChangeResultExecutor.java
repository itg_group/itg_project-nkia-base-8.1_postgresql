package com.nkia.itg.nbpm.rule.common.executor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.AbstractEmailSendJob;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSendJobContainer;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;


public class PreChangeResultExecutor implements Executor {
	private static final Logger logger = LoggerFactory.getLogger(PreChangeResultExecutor.class);
	
	@Override
	public void execute(long procId) {

		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		SmsSendDAO smsSendDAO = (SmsSendDAO)NkiaApplicationContext.getCtx().getBean("smsSendDAO");
		SmsgHistDAO smsgHistDAO = (SmsgHistDAO)NkiaApplicationContext.getCtx().getBean("smsgHistDAO");
		try {
			String srId = commonDAO.selectSrId(procId);
			HashMap<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("sr_id", srId);
			
			HashMap<String, Object> procChangeMap = new HashMap<String, Object>();
			procChangeMap = (HashMap<String, Object>) customizeUbitDAO.selectProcChangeRule(paramMap);
			
			int emsCount = customizeUbitDAO.selectProcRelConfEmsIdCount(paramMap);
			if(emsCount > 0){
				if(procChangeMap.get("MONITOR_TERMINATE_YN").equals("Y")){
					SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					
					String transTime = transFormat.format(procChangeMap.get("MONITOR_TERMINATE_STR_DT"));
					Date transDate = transFormat.parse(transTime);
					long startTime = transDate.getTime() + 10000;
					
					List<Map<String, Object>> emsList = new ArrayList();
					emsList = customizeUbitDAO.selectProcRelConfEmsId(paramMap);
					
					Map returnMap = new HashMap();
					
		//		{
		//			 "name" : "ITSM작업해제",
		//			 "description" : "ITSM작업해제",
		//			 "userId" : "sjyoon",
		//			 "targetResource" : ["111","112"],
		//			 "triggers" : [{"startTime" : 2019110612001212,
		//			               "timeMin" : 120,
		//			               "intervalType" : "ONCE"}
		//			 			]
		//		}
					
					Map<String, Object> inJsonEmsTimeMap = new HashMap<String, Object>();
					
					inJsonEmsTimeMap.put("startTime", startTime);  //시작시간
					inJsonEmsTimeMap.put("timeMin", procChangeMap.get("MONITOR_TERMINATE_MIN"));    //알람막을시간(분) 종료시간-시작시간
					inJsonEmsTimeMap.put("intervalType", "ONCE"); //ONCE고정
					// 요청 기본정보
					ArrayList resourceList = new ArrayList();
					for(int i=0;i<emsList.size();i++){
						if(!(emsList.get(i).get("EMS_ID")==null)){
							resourceList.add(emsList.get(i).get("EMS_ID"));
						}
					}
					Map<String, Object> inJsonEmsMap = new HashMap<String, Object>();
					inJsonEmsMap.put("name", procChangeMap.get("TITLE")); //픽스
					inJsonEmsMap.put("userId", "admin"); //사용자
					//inJsonEmsMap.put("userId", procChangeMap.get("WORKER")); //사용자
					inJsonEmsMap.put("targetResource", resourceList);
					ArrayList triggersList = new ArrayList();
					triggersList.add(inJsonEmsTimeMap);
					inJsonEmsMap.put("triggers", triggersList);
					String body = JsonMapper.toJson(inJsonEmsMap);		
					
					String sUrl = "";
					//sUrl = "http://ems.lotte.center/rest/v1/common/maintenancejobs";
					sUrl = NkiaApplicationPropertiesMap.getProperty("EMS.maint");
					URL postUrl = new URL(sUrl);
					HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
					conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
					conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
					conn.setConnectTimeout(3000);
					conn.setAllowUserInteraction(false);
					OutputStream os = conn.getOutputStream();
					os.write(body.getBytes());
					os.flush();
					
					BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
					String output;
					String responseJson = "";
					while ((output = br.readLine()) != null) {
//						System.out.println(output);
						logger.info(output);
						responseJson += output;
					}
					conn.disconnect();
					boolean successFlag = true;
					try {
						returnMap= JsonMapper.fromJson(responseJson, Map.class);
						successFlag = (boolean)((Map)returnMap.get("data")).get("result");
						//Gson gson = new Gson();
						//resultVO = gson.fromJson(responseJson, ChangeVO.class);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(!successFlag){ //실패시 알림톡 발송
						
						//알림톡 발송로직 들어가야하는곳
						//1. 발송인원 셀렉트
						List<HashMap> userList = customizeUbitDAO.emsFailAlarm(paramMap);
						String workStrDt= "";
						String workEndDt= "";
										
						String content = "관제 알람입니다.\n▶고객사 : "+userList.get(0).get("CUSTOMER_NM")+"\n▶등급 : INFO \n▶일시 : "+
										userList.get(0).get("MONITOR_TERMINATE_STR_DT")+" ~ " +userList.get(0).get("MONITOR_TERMINATE_END_DT") + 
										"\n▶장비명 : "+userList.get(0).get("CONF_COUNT") + "\n▶이벤트명 : 관제 해지 예약 오류\n▶프로세스ID : " + srId + "\n▶제목 : " + 
										userList.get(0).get("TITLE");
						//2. 발송내용체크
							//2-1 유형에따라 템플릿코드 셋팅 - CAB참석자 정보에 맞춰서 수정필요
						String templteCode = "LMSG_20170918180306775180";
						String senderKey = "6ee324bb9d52dfe380906819ec2d76420aac386e";
						String sendPhone = "0226264200";
		
					/*	
						//2-2  유형에따라 rebody 앞에 prefix 붙여줌   [자바스크립트에서 진행]
		//				String rebody = "";
				/*		String content = "ITSM 업무 알림입니다. \n";
						content += "▶고객사 : 변경요청\n";
						content += "▶등급 : 등급";
						content += "▶";
						content += "▶제목 :" +(String)processDetail.get("TITLE")+"\n";
						content += "업무 진행 바랍니다.";
						smsInfo.put("CONTENT", content);*/
						//3. 알림톡테이블 등록
						String hp_no = "";
						for(HashMap userData: userList){
							hp_no = (String) userData.get("HP_NO");
							hp_no = hp_no.replace("-", "");
							userData.put("TO_PHONE", hp_no); // 받는사람전화번호
							userData.put("MSG_BODY", content); //알림톡 메세지
							userData.put("RE_BODY", "["+srId+"]"+"관제 해지 예약 오류"); //실패시 보내는 대체 문자메세지
							userData.put("TEMPLATE_CODE", templteCode); //장애전파 템플릿코드
							userData.put("SENDER_KEY", senderKey); // 센더키
							userData.put("SEND_PHONE", sendPhone); // 보내는사람
							smsSendDAO.insertTalkList(userData);
							smsgHistDAO.insertTalkListHistory(userData);
						}
					}
				}
				
				///EMS 작업연계
				List<Map<String, Object>> snapshotList = new ArrayList();
				snapshotList =  customizeUbitDAO.selectSnapshotDetail(paramMap);
				Map returnMap = new HashMap();
				/* 예상템플릿		{
					"taskId": "1234", //srId
					"flag": "plan",
					"resourceInfo":[
					    {
					    		"resourceId": 37,
					    		"resourceType": "Server",
					    		"requestType": "1,2,4"
					    }
					]
		
				} */
				for(int z=0;z <snapshotList.size();z++){
					Map<String, Object> resourceMap = new HashMap<String, Object>();
					resourceMap.put("resourceId", snapshotList.get(z).get("EMS_ID"));  //EMS ID 숫자형
					resourceMap.put("resourceType", "SERVER");    //"SERVER"고정
					resourceMap.put("requestType",  snapshotList.get(z).get("SNAPSHOT")); 
					// 요청 기본정보
					Map<String, Object> inJsonEmsMap = new HashMap<String, Object>();
					inJsonEmsMap.put("taskId", srId); //SRID
					//inJsonEmsMap.put("userId", paramMap.get("user_id")); //사용자
					inJsonEmsMap.put("flag", "plan"); //plan /work  < 작업전 후
					ArrayList resourceList = new ArrayList();
					resourceList.add(resourceMap);
					inJsonEmsMap.put("resourceInfo", resourceList);
					String body = JsonMapper.toJson(inJsonEmsMap);
					
					/*
					//body = JsonMapper.toJson(body);
					String sUrl = "";
					//sUrl = "http://tems.lotte.center/rest/v1/itsm/snapshot";
					//sUrl = "http://10.231.128.93:8723/rest/v1/itsm/snapshot";
					sUrl = NkiaApplicationPropertiesMap.getProperty("EMS.snapshot");
					URL postUrl = new URL(sUrl);
					HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
					conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
					conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
					conn.setConnectTimeout(3000);
					conn.setAllowUserInteraction(false);
					OutputStream os = conn.getOutputStream();
					os.write(body.getBytes());
					os.flush();
					
					BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
					String output;
					String responseJson = "";
					while ((output = br.readLine()) != null) {
						System.out.println(output);
						responseJson += output;
					}
					conn.disconnect();
					*/
					PreChangeResultThreadExecutor snapshot = new PreChangeResultThreadExecutor() ;
					snapshot.setBody(body);
					Thread t = new Thread(snapshot);
					t.start();
				}
			}
			//알림자설정에 등록된 유저에게 알림톡 발송
			
			HashMap<String, Object> procChangeOperMap = new HashMap<String, Object>();
			procChangeOperMap = (HashMap<String, Object>) customizeUbitDAO.selectProcChangeOperAppm(paramMap);
			
			Map processDetailMap = postWorkDAO.selectProcessDetail(srId);

			String changeTyCdNm = (String) procChangeMap.get("CHANGE_TY_CD_NM");
			String changeCategoryCd = (String) procChangeMap.get("CHANGE_CATEGORY_CD_NM");
			String title = (String) processDetailMap.get("TITLE");		
			String workStrDt= (String) procChangeMap.get("PLAN_STR_DT");
			String workEndDt= (String) procChangeMap.get("PLAN_END_DT");
			String operUserNm = (String) procChangeOperMap.get("OPER_USER_NM");
			String workDt = workStrDt+" ~ "+workEndDt;
			
			List<HashMap> userList = new ArrayList();
			userList =  customizeUbitDAO.workExpectedUserlist(paramMap);
			
			//작업시작, 완료, 취소에 따른 세팅값
			String content= "UBIT센터 작업 사전안내  알림 문자입니다.\n▶작업구분 : " + changeTyCdNm
							+"\n▶작업범주 : " +changeCategoryCd
							+"\n▶작업내용  : "+title
							+"\n▶작업시간 : "+workDt
							+"\n▶작업자 : "+operUserNm
							+"\n일정 참고 부탁드립니다.";
			
			//2. 발송내용체크
			//2-1 유형에따라 템플릿코드 셋팅 - CAB참석자 정보에 맞춰서 수정필요
			String templteCode = "LMSG_20170911172320836200";
			String senderKey = "ef834c839e4a9c83ac3e027b0ed782a7545014d4";;
			String sendPhone = "0226264210";

			//3. 알림톡테이블 등록
			String hp_no = "";
			for(HashMap userData: userList){
				hp_no = (String) userData.get("HP_NO");
				hp_no = hp_no.replace("-", "");
				userData.put("TO_PHONE", hp_no); // 받는사람전화번호
				userData.put("MSG_BODY", content); //알림톡 메세지
				userData.put("RE_BODY", "UBIT센터 작업 사전안내 알림 문자입니다."); //실패시 보내는 대체 문자메세지
				userData.put("TEMPLATE_CODE", templteCode); //장애전파 템플릿코드
				userData.put("SENDER_KEY", senderKey); // 센더키
				userData.put("SEND_PHONE", sendPhone); // 보내는사람
				userData.put("USER_NM", userData.get("USER_NM")); 
				smsSendDAO.insertTalkList(userData);
				smsgHistDAO.insertTalkListHistory(userData);
			}

		}catch (Exception e) {
			
		}
	
		
	}
}
