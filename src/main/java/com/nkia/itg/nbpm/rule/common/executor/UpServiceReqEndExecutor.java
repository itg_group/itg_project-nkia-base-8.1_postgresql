package com.nkia.itg.nbpm.rule.common.executor;

import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class UpServiceReqEndExecutor implements Executor {

	@Override
	public void execute(long procId) throws NkiaException {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		Map paramMap = commonDAO.selectUpServiceTaskInfo(procId);
		if(paramMap != null) {
			NbpmClient nbpmCommonClient = (NbpmClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient");
			
			NbpmProcessVO processVO = new NbpmProcessVO();
			processVO.setSr_id((String)paramMap.get("SR_ID"));
			processVO.setTask_id(Long.valueOf((String)paramMap.get("TASK_ID")));
			processVO.setWorker_user_id((String)paramMap.get("WORKER_USER_ID"));
			
			nbpmCommonClient.completeTask(processVO);
		}
	}
}
