package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class MultiConfmNrmltExecutor  implements Executor {

	/**
	 * 작업요청, 서버폐기/서비스 종료요청 
	 * 
	 * 부서승인(MULTI_CONFM) 승인인 경우
	 * 다음 순서의 부서승인(MULTI_CONFM) 생성 or 임시태스크 진행
	 * 
	 * 2019-03-26 dhkim
	 */
	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");

		String srId = commonDAO.selectSrId(procId);
		Map paramMap = postWorkDAO.selectProcessDetail(srId);
		
		List multiUserList = new ArrayList();
		
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("task_name",  "REQUST_REGIST");
		searchMap.put("processid",  (String)paramMap.get("PROCESSID"));
		searchMap.put("event_name", "MULTI_ORDER_PROC");
		searchMap.put("not_task_group_id", "Y");
		
		multiUserList = postWorkDAO.selectMultiTempUsers(searchMap);
		
		if(multiUserList.size() > 0){
			Map multiUserMap = new HashMap();
			multiUserMap = (Map)multiUserList.get(0);
			multiUserMap.put("sr_id", srId);
			multiUserMap = WebUtil.lowerCaseMapKey(multiUserMap);
			multiUserMap.put("nbpm_processInstanceId", String.valueOf(paramMap.get("PROC_ID")));
				
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(multiUserMap);
			processVO.setWorker_user_id((String)multiUserMap.get("worker_user_id"));
			
			nbpmCommonClient.registerUser(processVO, multiUserMap);
			Map result = nbpmCommonClient.createMultiTask(processVO, multiUserMap);
			
			//task_group_id 정보 추가
			searchMap.put("task_order", (String)multiUserMap.get("task_order"));
			searchMap.put("task_group_id", result.get("task_group_id"));
			postWorkDAO.updateMultiTemp(searchMap);
		
		}else{
			//임시태스크 진행
			searchMap.put("task_name", "TEMP_");
			List insertList = new ArrayList();
			insertList = commonDAO.selectTempTaskConfirm(searchMap);
			
			// 조건,
			// PROC_MASTER.PROC_ID = TASK.PROCESSINSTANCEID
			// TASK.TASK_NAME like 'TEMP_WAIT_%'
			// TASK.STATUS = 'Reserved' 
			// WORK_USER_ID = 프로세스 권한 그룹에서 <릴리즈요청 - 연계요청>의 지정 담당자(RELS_ROLE_OPETR_05)
			if(insertList.size() > 0){
				for(int i=0; i<insertList.size(); i++){
					Map processInfoMap = new HashMap();
					processInfoMap = (Map) insertList.get(i);
										
					Map insertMap = new HashMap();
					insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
					insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
					insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
					insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
					insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
					insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
				//	insertMap.put("work_type", String.valueOf(NbpmConstraints.WORKTYPE_NORMAL_PROC)); 
				//	insertMap.put("nbpm_comment", "");
					
					nbpmCommonClient.workTaskForRule(insertMap);
					 
				}
			}
		}
	}
}