package com.nkia.itg.nbpm.rule.common.executor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Clob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.workflow.kedb.dao.KedbDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;


public class ProblemKedbRegistExecutor implements Executor {

	private static final Logger logger = LoggerFactory.getLogger(KedbRegistExecutor.class);
	
	@Override
	public void execute(long procId) throws Exception {

		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		KedbDAO kedbDAO = (KedbDAO)NkiaApplicationContext.getCtx().getBean("kedbDao"); 
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = userVO.getUser_id();
		
		String kedbId = kedbDAO.selectKedbId(new HashMap());		
		String srId = commonDAO.selectSrId(procId);
		
		Map processDetail = customizeUbitDAO.selectKedbInsertData(srId);
		
		Clob contentData = (Clob)processDetail.get("CONTENT");
		String contentString = "";
		Reader in = null;
		StringWriter strWriter = new StringWriter();
		try {
			in = contentData.getCharacterStream();
			IOUtils.copy(in, strWriter);
			contentString = strWriter.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
					strWriter.close();
				} catch (IOException e) {
					logger.debug("execute Exception : " + e.getMessage());
				}
			}
		}
		if(contentString != null) {
			processDetail.put("contentStr", contentString);
		} else {
			processDetail.put("contentStr", "");
		}
		
		Map srMap = new HashMap();
		
		//상위요청이 장애일경우만 응급처리내용 가져옴
		String upSrId = (String) processDetail.get("UP_SR_ID");
		
		Map upProcessDetail = postWorkDAO.selectProcessDetail(upSrId);	
		
		if(upProcessDetail != null){
			String upReqType = (String) upProcessDetail.get("REQ_TYPE");
			if(!upSrId.equals("") && upReqType.equals("INCIDENT")){
				srMap.put("tmpr_solut_mthd_cn", (String) customizeUbitDAO.selectKedbInsertDataIncident(upSrId).get("EMRGNCY_PROC_CONTENT"));  // 임시해결안 장애요청일경우만
			}			
		}
		
		srMap.put("kedbId", kedbId);
		srMap.put("kedb_titl", 			(String)processDetail.get("TITLE")); 	     // 제목
		srMap.put("kedb_cn",   			(String)processDetail.get("contentStr"));    // 내용
		srMap.put("inc_type_cd",  (String)processDetail.get("INC_TYPE_CD"));   // 장애원인유형
		srMap.put("root_cause_cn", 		(String)processDetail.get("CAUSE_CONTENT"));  	 // 원인내용
		srMap.put("solut_mthd_cn", 		(String)processDetail.get("PREVENTION_COMMENT"));   // 재발방지대책방안
		srMap.put("login_user_id",   	loginUserId); 								 // 수정자
		
		kedbDAO.insertKedb(srMap);
		
	}
}
