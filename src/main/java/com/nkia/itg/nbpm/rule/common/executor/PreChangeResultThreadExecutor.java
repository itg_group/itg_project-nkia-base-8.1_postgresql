package com.nkia.itg.nbpm.rule.common.executor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;


public class PreChangeResultThreadExecutor implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(PreChangeResultThreadExecutor.class);
	
	private String body;
	
	public void setBody(String Body) {
		this.body = Body;
	}
	public void restAPI() {
		try {
			String sUrl = "";
			sUrl = NkiaApplicationPropertiesMap.getProperty("EMS.snapshot");
			URL postUrl;
			postUrl = new URL(sUrl);
			HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
			conn.setConnectTimeout(3000);
			conn.setAllowUserInteraction(false);
			OutputStream os = conn.getOutputStream();
			os.write(body.getBytes());
			os.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			String responseJson = "";
			while ((output = br.readLine()) != null) {
//				System.out.println(output);
				logger.info(output);
				responseJson += output;
			}
			conn.disconnect();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			restAPI();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}
}
