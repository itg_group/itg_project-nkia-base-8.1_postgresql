/*
 * @(#)RuleManager.java              2014. 2. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.rule.common.invoker;

import com.nkia.itg.nbpm.rule.common.executor.Executor;

public interface RuleManager {
	
	public Executor getExecutor(String type);
}
