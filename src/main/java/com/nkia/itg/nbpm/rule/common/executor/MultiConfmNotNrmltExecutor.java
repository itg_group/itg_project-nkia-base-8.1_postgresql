package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

public class MultiConfmNotNrmltExecutor  implements Executor {

	/**
	 * 작업요청, 서버폐기/서비스 종료요청 
	 * 
	 * 부서승인(MULTI_CONFM) 승인인 경우
	 * 다음 순서의 부서승인(MULTI_CONFM) 생성 or 임시태스크 진행
	 * 
	 * 2019-03-26 dhkim
	 */
	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");

		String srId = commonDAO.selectSrId(procId);
		
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("task_name",  "TEMP_");

		List insertList = new ArrayList();
		insertList = commonDAO.selectTempTaskConfirm(searchMap);
		
		// 조건,
		// PROC_MASTER.PROC_ID = TASK.PROCESSINSTANCEID
		// TASK.TASK_NAME like 'TEMP_WAIT_%'
		// TASK.STATUS = 'Reserved' 
		if(insertList.size() > 0){
			for(int i=0; i<insertList.size(); i++){
				Map processInfoMap = new HashMap();
				processInfoMap = (Map) insertList.get(i);
									
				Map insertMap = new HashMap();
				insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
				insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
				insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
				insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
				insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
				insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
				insertMap.put("work_type", String.valueOf(NbpmConstraints.WORKTYPE_RETURN_PROC)); 
			//	insertMap.put("nbpm_comment", "");
				
				nbpmCommonClient.workTaskForRule(insertMap);
			}
		}
	}
}