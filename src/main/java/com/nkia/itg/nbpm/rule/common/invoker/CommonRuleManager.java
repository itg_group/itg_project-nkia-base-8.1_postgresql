package com.nkia.itg.nbpm.rule.common.invoker;

import com.nkia.itg.nbpm.rule.common.executor.AllCreateMultiTaskExecutor;
import com.nkia.itg.nbpm.rule.common.executor.ChangeEndExecutor;
import com.nkia.itg.nbpm.rule.common.executor.CreateFirstMultiConfmExecutor;
import com.nkia.itg.nbpm.rule.common.executor.EmailSendExecutor;
import com.nkia.itg.nbpm.rule.common.executor.Executor;
import com.nkia.itg.nbpm.rule.common.executor.KedbRegistExecutor;
import com.nkia.itg.nbpm.rule.common.executor.MultiConfmNotNrmltExecutor;
import com.nkia.itg.nbpm.rule.common.executor.MultiConfmNrmltExecutor;
import com.nkia.itg.nbpm.rule.common.executor.UpServiceReqEndExecutor;

public class CommonRuleManager implements RuleManager {

	@Override
	public Executor getExecutor(String type) {
		Executor executor = null;
		if("COMMON_EMAIL_SENDER".equals(type)) {
			executor = new EmailSendExecutor();			
		}
		if("UP_SERVICE_REQ_END".equals(type)) {
			executor = new UpServiceReqEndExecutor();			
		}
		if("KEDB_APPLY_RULE".equals(type)) {
			executor = new KedbRegistExecutor();			
		}
		if("FIRST_MULTI_CONFM".equals(type)) { //순차 멀티생성
			executor = new CreateFirstMultiConfmExecutor();			
		}
		if("MULTI_CONFM_NRMLT".equals(type)) { //순차 정상종료
			executor = new MultiConfmNrmltExecutor();			
		}
		if("MULTI_CONFM_NOT_NRMLT".equals(type)) { //순차 반려종료
			executor = new MultiConfmNotNrmltExecutor();			
		}
		if("CHANGE_END".equals(type)) { //대기TASK종료 화인
			executor = new ChangeEndExecutor();			
		}
		if("MULTI_ALL_CNFIRM".equals(type)) { //일괄 멀티
			executor = new AllCreateMultiTaskExecutor();			
		}
		return executor;
	}

}
