package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class NosDcoReturnEndExecutor  implements Executor {

	/**
	 * 병렬 승인시 반려종료처리
	 * 
	 * 
	 * 2019-03-22 dhkim
	 */
	@Override
	public void execute(long procId) throws Exception {
/*		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");

		String srId = commonDAO.selectSrId(procId);
		Map paramMap = postWorkDAO.selectProcessDetail(srId);
		
		List multiUserList = new ArrayList();
		
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
	
		//DCO 승인, NOS 승인
		searchMap.put("task_name", "_CONFM");
		List insertList = new ArrayList();
		insertList = commonDAO.selectTempTaskConfirm(searchMap);
		
		// 조건,
		// PROC_MASTER.PROC_ID = TASK.PROCESSINSTANCEID
		// TASK.TASK_NAME like 'TEMP_WAIT_%'
		// TASK.STATUS = 'Reserved' 
		if(insertList.size() > 0){

//			for(int i=0; i<insertList.size(); i++){
				Map processInfoMap = new HashMap();
				processInfoMap = (Map) insertList.get(0);
									
//				Map insertMap = new HashMap();
//				insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
//				insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
//				insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
//				insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
//				insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
//				insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
//			//	insertMap.put("work_type", String.valueOf(NbpmConstraints.WORKTYPE_NORMAL_PROC)); 
//			//	insertMap.put("nbpm_comment", "");
//				
//				nbpmCommonClient.workTaskForRule(insertMap);
//				
				String taskId = String.valueOf(processInfoMap.get("ID"));
				
				NbpmProcessVO processVO = new NbpmProcessVO();
				processVO.setSr_id((String)processInfoMap.get("SR_ID"));
				processVO.setTask_id(Long.parseLong(taskId));
				processVO.setWorker_user_id((String)processInfoMap.get("WORKER_USER_ID"));
				
				nbpmCommonClient.completeTask(processVO);
			}
//		}
 */
 
	}
}