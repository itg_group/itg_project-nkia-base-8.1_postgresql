package com.nkia.itg.nbpm.rule.common.invoker;

import com.nkia.itg.nbpm.rule.common.executor.CapNotiAlarmExecutor;
import com.nkia.itg.nbpm.rule.common.executor.CenterHappyCallExecutor;
import com.nkia.itg.nbpm.rule.common.executor.ChangeEndExecutor;
import com.nkia.itg.nbpm.rule.common.executor.ChangePlanAlarmExecutor;
import com.nkia.itg.nbpm.rule.common.executor.ChangeTempCnfirmExecutor;
import com.nkia.itg.nbpm.rule.common.executor.CreateFirstMultiConfmExecutor;
import com.nkia.itg.nbpm.rule.common.executor.Executor;
import com.nkia.itg.nbpm.rule.common.executor.HappyCallExecutor;
import com.nkia.itg.nbpm.rule.common.executor.MultiConfmNotNrmltExecutor;
import com.nkia.itg.nbpm.rule.common.executor.MultiConfmNrmltExecutor;
import com.nkia.itg.nbpm.rule.common.executor.NosDcoReturnEndExecutor;
import com.nkia.itg.nbpm.rule.common.executor.PreChangeResultExecutor;
import com.nkia.itg.nbpm.rule.common.executor.ProblemKedbRegistExecutor;
import com.nkia.itg.nbpm.rule.common.executor.ServiceRegistTempExecutor;
import com.nkia.itg.nbpm.rule.common.executor.ServiceReturnEndAlarmExecutor;
import com.nkia.itg.nbpm.rule.common.executor.TempHistEndExecutor;

public class UbitRuleManager implements RuleManager {

	@Override
	public Executor getExecutor(String type) {
		Executor executor = null;
		//작업요청 - 최초멀티승인자티켓생성
		if("CHANGE_TEMP_CNFIRM".equals(type)) {
			executor = new ChangeTempCnfirmExecutor();			
		}
		if("HAPPYCALL_MAIL".equals(type)) {
			executor = new HappyCallExecutor();			
		}
		if("CHANGE_PLAN_ALARM".equals(type)) {
			executor = new ChangePlanAlarmExecutor();			
		}
		if("CAP_NOTI_ALARM".equals(type)) {
			executor = new CapNotiAlarmExecutor();			
		}
		if("PRE_CHANGE_RESULT".equals(type)) {
			executor = new PreChangeResultExecutor();			
		}
		if("CENTER_HAPPYCALL_MAIL".equals(type)) {
			executor = new CenterHappyCallExecutor();			
		}
		if("PROBLEM_KEDB_REGIST".equals(type)) {
			executor = new ProblemKedbRegistExecutor();			
		}		
		if("SERVICE_RETURN_END_ALARM".equals(type)) {
			executor = new ServiceReturnEndAlarmExecutor();			
		}
		if("SERVICE_REGIST_TEMP".equals(type)) {
			executor = new ServiceRegistTempExecutor();			
		}
		return executor;
	}

}
