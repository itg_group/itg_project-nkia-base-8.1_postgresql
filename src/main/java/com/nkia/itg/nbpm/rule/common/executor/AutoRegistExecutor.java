package com.nkia.itg.nbpm.rule.common.executor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

public class AutoRegistExecutor implements Executor {

	/**
	 * 요청등록에서 연계요청시 하위요청은 
	 * 임시 태스크로 이동후 메인요청 등록시 강제진행
	 * 
	 */
	//연관장애 SR_ID의 작업상태값 체크하여 임시TASK, 또는 요청종료 
	@Override
	public void execute(long procId) throws NkiaException,SQLException {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		SmsSendDAO smsSendDAO = (SmsSendDAO)NkiaApplicationContext.getCtx().getBean("smsSendDAO");
		SmsgHistDAO smsgHistDAO = (SmsgHistDAO)NkiaApplicationContext.getCtx().getBean("smsgHistDAO");
		
		String sr_id = commonDAO.selectSrId(procId);
		//연관장애의 작업상태값
		String rel_inc_work_state = commonDAO.selectRelIncWorkState(sr_id); 
		
	}
}
