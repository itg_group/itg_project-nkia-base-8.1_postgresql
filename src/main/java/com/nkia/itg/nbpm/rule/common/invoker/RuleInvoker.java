package com.nkia.itg.nbpm.rule.common.invoker;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.rule.common.executor.Executor;


public class RuleInvoker {
	
	protected long procId;
	private String site;
	
	private Executor executor;
	
	public RuleInvoker(String site, long procId) {
		this.site = site;
		this.procId = procId;
	}
	
	public void setExecutor(String type) {
		//사이트 별로 매니저를 분기하여 실행 할 수 있도록 함
		RuleManager manager = null;
		if("COMMON".equals(site)) {
			manager = new CommonRuleManager();
			executor = manager.getExecutor(type);
		}
		if("BASE".equals(site)) {
			manager = new BaseRuleManager();
			executor = manager.getExecutor(type);
		}if("UBIT".equals(site)) {
			manager = new UbitRuleManager();
			executor = manager.getExecutor(type);
		}
	}
	
	public void invoke() throws JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, SQLException, IOException, MessagingException, Exception {
		executor.execute(this.procId);
	}
}
