package com.nkia.itg.nbpm.rule.common.invoker;

import com.nkia.itg.nbpm.rule.common.executor.AutoRegistExecutor;
import com.nkia.itg.nbpm.rule.common.executor.CheckIncRelExecutor;
import com.nkia.itg.nbpm.rule.common.executor.Executor;
import com.nkia.itg.nbpm.rule.common.executor.UpIncRelStatusExecutor;


public class BaseRuleManager implements RuleManager {

	@Override
	public Executor getExecutor(String type) {
		Executor executor = null;
		//작업요청 - 최초멀티승인자티켓생성
		if("CHECK_INC_REL".equals(type)) {
			executor = new CheckIncRelExecutor();			
		}
		
		if("UP_INC_REL_STATUS".equals(type)) {
			executor = new UpIncRelStatusExecutor();			
		}
		
		if("ATUO_REGIST".equals(type)) {
			executor = new AutoRegistExecutor();			
		}
		
		return executor;
	}

}
