/*
 * @(#)CommonRuleDAO.java              2014. 2. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.rule.common.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

@Repository("commonRuleDAO")
public class CommonRuleDAO extends NbpmAbstractDAO {

	public String selectSrId(long procId) throws SQLException, NkiaException {
		return (String) selectByPk("CommonRuleDAO.selectSrId", procId);
	}
	
	public Map selectUserInfo(String userId) throws SQLException, NkiaException {
		return (Map) selectByPk("CommonRuleDAO.selectUserInfo", userId);
	}

	public List searchSendMailTargetList(String srId) throws SQLException, NkiaException {
		return list("CommonRuleDAO.searchSendMailTargetList", srId);
	}
	
	public List searchSendMailHistoryList(Map param) throws SQLException, NkiaException {
		return list("CommonRuleDAO.searchSendMailHistoryList", param);
	}
	
	public String selectLastTaskName(String srId) throws SQLException, NkiaException {
		return (String) selectByPk("CommonRuleDAO.selectLastTaskName", srId);
	}

	public void insertProcEmailSendHis(Map param) throws SQLException, NkiaException {
		this.getSqlMapClientTemplate().insert("CommonRuleDAO.insertProcEmailSendHis", param);
	}

	public void deleteProcEmailSend(String srId) throws SQLException, NkiaException{
		this.getSqlMapClientTemplate().delete("CommonRuleDAO.deleteProcEmailSend", srId);
	}

	public Map selectUpServiceTaskInfo(long procId) {
		return (Map) selectByPk("CommonRuleDAO.selectUpServiceTaskInfo", procId);
	}
	// 카카오 뱅크 
	public Map selectSrChangeDetailInfo(String srId) throws SQLException, NkiaException {
		return (Map) selectByPk("CommonRuleDAO.selectSrChangeDetailInfo", srId);
	}

	public HashMap selectCrMasterInfo(long procId) throws SQLException, NkiaException {
		return (HashMap) selectByPk("CommonRuleDAO.selectCrMasterInfo", procId);
	}

	public String selectCrRequstTy(String srId) throws SQLException, NkiaException {
		return (String) selectByPk("CommonRuleDAO.selectCrRequstTy", srId);
	}

	public HashMap selectCrDetailInfo(String srId) throws SQLException, NkiaException {
		return (HashMap) selectByPk("CommonRuleDAO.selectCrDetailInfo", srId);
	}
	
	public HashMap selectSrDetailInfo(String srId) throws SQLException, NkiaException {
		return (HashMap) selectByPk("CommonRuleDAO.selectSrDetailInfo", srId);
	}
	
	public Map searchHappyCallDataList(String srId) throws SQLException, NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("CommonRuleDAO.searchHappyCallDataList", srId, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnMap();
		
	}
	
	public Map searchApprovalDataList(Map param) throws SQLException, NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("CommonRuleDAO.searchApprovalDataList", param, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnMap();
		
	}

	//이메일 수신자 정보 조회
	public List searchEmailUserList(String userId) {
		return list("CommonRuleDAO.searchEmailUserList", userId);
	}
	

	public HashMap selectOperWdtbDetail(String srId) throws SQLException {
		return (HashMap) selectByPk("CommonRuleDAO.selectOperWdtbDetail", srId);
	}

	public String selectUpSrId(String srId) throws SQLException {
		return (String) selectByPk("CommonRuleDAO.selectUpSrId", srId);
	}
	
	public List selectLowerSrId(String srId) throws SQLException {
		return list("CommonRuleDAO.selectLowerSrId", srId);
	}
	
	//연관장애 리스트
	public List selectRelIncSrId(String srId) throws SQLException {
		return list("CommonRuleDAO.selectRelIncSrId", srId);
	}
	
	public String selectRelIncWorkState(String srId) throws SQLException {
		return (String) selectByPk("CommonRuleDAO.selectRelIncWorkState", srId);
	}
	
	public List selectIncInfo(String srId) throws SQLException {
		return list("CommonRuleDAO.selectIncInfo", srId);
	}
	
	// 임시태스크 확인
	public List selectTempTaskConfirm(Map param) throws SQLException {
		return list("CommonRuleDAO.selectTempTaskConfirm", param);
	}
		
	// 멀티태스크 확인
	public int selectIngMutilTaskCount(Map param) throws SQLException {
		return (Integer)selectByPk("CommonRuleDAO.selectIngMutilTaskCount", param);
	}

	public List selectSrChangeDevelopper(String srId) throws SQLException {
		return list("CommonRuleDAO.selectSrChangeDevelopper", srId);
	}

	public List selectProcRelConf(Map param) throws SQLException {
		return list("CommonRuleDAO.selectProcRelConf", param);
	}
	
	// Task 상태 업데이트 (-> Exited)
	public void updateTaskStatus(Map paramMap) throws SQLException, Exception {
		update("CommonRuleDAO.updateTaskStatus", paramMap);
	}

	public String changeEndUpsrId(String srId) throws SQLException, Exception {
		return (String) selectByPk("CommonRuleDAO.changeEndUpsrId", srId);
	}

	public int selectEndCount(String upSrId) throws SQLException, Exception {
		return (Integer)selectByPk("CommonRuleDAO.selectEndCount", upSrId);
	}
	
	public int selectChangeEndCount(Map upSrId) throws SQLException, Exception {
		return (Integer)selectByPk("CommonRuleDAO.selectChangeEndCount", upSrId);
	}
	
	// 승인메일시 승인상태 확인
	public List updateApprovalList(Map param) throws SQLException {
		return list("CommonRuleDAO.updateApprovalList", param);
	}
			
	public void updateServiceEndInfo(String upSrId) throws NkiaException {
		update("CommonRuleDAO.updateServiceEndInfo", upSrId);
	}
}
