package com.nkia.itg.nbpm.rule.common.executor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

import egovframework.com.cmm.EgovMessageSource;


public class CapNotiAlarmExecutor implements Executor {

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="smsSendDAO")
	private SmsSendDAO smsSendDAO;
	
	@Override
	public void execute(long procId) throws Exception {

		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		SmsSendDAO smsSendDAO = (SmsSendDAO)NkiaApplicationContext.getCtx().getBean("smsSendDAO");
		SmsgHistDAO smsgHistDAO = (SmsgHistDAO)NkiaApplicationContext.getCtx().getBean("smsgHistDAO");
		
		String srId = commonDAO.selectSrId(procId);
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sr_id", srId);
		
		HashMap<String, Object> procChangeMap = new HashMap<String, Object>();
		procChangeMap = (HashMap<String, Object>) customizeUbitDAO.selectProcChangeRule(paramMap);
		
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String conferencePlace = (String) procChangeMap.get("CONFERENCE_PLACE");
		String conferenceDt= transFormat.format(procChangeMap.get("CONFERENCE_DT"));
		String cabName= (String) procChangeMap.get("CAB_NAME");
		String cabContent= (String) procChangeMap.get("CAB_CONTENT");;
		String content= "ITSM 업무 알림입니다.\n"
					 +"▶프로세스 정보 : 변경관리\n"
				     +"▶ID : " +srId+"\n"
					 +"▶알림내용 : CAB소집 요청\n"
				     +"▶회의장소 : "+conferencePlace+"\n"
					 +"▶회의일시 : "+conferenceDt+"\n"
				     +"▶제목 : "+cabName+"\n"
					 +"▶CAB내용 : "+cabContent;

		boolean checkLogic = true;
		
		//String resultMsg = messageSource.getMessage("msg.common.result.00004");
		//알림톡 발송로직 들어가야하는곳
		//1. 발송인원 셀렉트
		List<Map<String, Object>> userList = customizeUbitDAO.selectAttendeesList(paramMap);
		//2. 발송내용체크
			//2-1 유형에따라 템플릿코드 셋팅 - CAB참석자 정보에 맞춰서 수정필요
		String templteCode = "LMSG_20200317133022746749";
		String senderKey = "ef834c839e4a9c83ac3e027b0ed782a7545014d4";
		String sendPhone = "0226264210";
	
		//2-2  유형에따라 rebody 앞에 prefix 붙여줌   [자바스크립트에서 진행]
//		String rebody = "";
		//3. 알림톡테이블 등록	
		String hp_no = "";
		for(Map<String, Object> userData: userList){			
			hp_no = (String) userData.get("HP_NO");
			hp_no = hp_no.replace("-", "");
			userData.put("TO_PHONE", hp_no); // 받는사람전화번호
			userData.put("MSG_BODY", content); //알림톡 메세지
			userData.put("RE_BODY", "CAB소집요청"); //실패시 보내는 대체 문자메세지
			userData.put("TEMPLATE_CODE", templteCode); //장애전파 템플릿코드
			userData.put("SENDER_KEY", senderKey); // 센더키
			userData.put("SEND_PHONE", sendPhone); // 보내는사람
			userData.put("USER_NM", userData.get("USER_NM")); 
			smsSendDAO.insertTalkList(userData);
			smsgHistDAO.insertTalkListHistory(userData);
		}
		//4. 히스토리테이블등록
		//smsGrpDAO.insertMessageListHistory(paramMap);
		
	}
}
