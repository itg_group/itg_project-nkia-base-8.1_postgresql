package com.nkia.itg.nbpm.rule.common.executor;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.workflow.kedb.dao.KedbDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

public class KedbRegistExecutor implements Executor {
	
	private static final Logger logger = LoggerFactory.getLogger(KedbRegistExecutor.class);
	
	@Override
	public void execute(long procId) throws NkiaException, SQLException {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		KedbDAO kedbDAO = (KedbDAO)NkiaApplicationContext.getCtx().getBean("kedbDao"); 
		NbpmCommonDAO nbpmCommonDAO = (NbpmCommonDAO)NkiaApplicationContext.getCtx().getBean("nbpmCommonDAO");
		Map srMap = new HashMap();
		List insertList = new ArrayList();
		String srId = commonDAO.selectSrId(procId);
		
		srMap.put("sr_id", srId);
		srMap.put("selectQuerykey", "selectPROC_PROBLEM");
		
		insertList = nbpmCommonDAO.searchProcessSubGrid(srMap);
		Map procMasterMap = (HashMap)commonDAO.selectSrDetailInfo(srId);
		
		Clob contentData = (Clob)procMasterMap.get("CONTENT");
		String contentString = "";
		Reader in = null;
		StringWriter strWriter = new StringWriter();
		try {
			in = contentData.getCharacterStream();
			IOUtils.copy(in, strWriter);
			contentString = strWriter.toString();
		} catch (Exception e) {
			logger.error("error");
		} finally {
			if(in != null) {
				try {
					in.close();
					strWriter.close();
				} catch (IOException e) {
					logger.debug("execute Exception : " + e.getMessage());
				}
			}
		}
		if(contentString != null) {
			procMasterMap.put("contentStr", contentString);
		} else {
			procMasterMap.put("contentStr", "");
		}
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = userVO.getUser_id();
		
		if(insertList.size() > 0){
			for(int i=0; i<insertList.size(); i++){
				Map kedbMap = new HashMap();
				List relList = new ArrayList();
				kedbMap = (Map) insertList.get(i);
				
				String kedbId = kedbDAO.selectKedbId(new HashMap());
				srMap.put("kedbId", kedbId);
				srMap.put("kedb_titl", 			(String)procMasterMap.get("TITLE")); 	     // 제목
				srMap.put("kedb_cn",   			(String)procMasterMap.get("contentStr"));    // 내용
				srMap.put("probm_cause_ty_cd",  (String)kedbMap.get("PROBM_CAUSE_TY_CD"));   // 장애원인유형
				srMap.put("tmpr_solut_mthd_cn", (String)kedbMap.get("TMPR_SOLUT_MTHD_CN"));  // 임시해결안
				srMap.put("root_cause_cn", 		(String)kedbMap.get("ROOT_CAUSE_CN"));  	 // 근본원인
				srMap.put("solut_mthd_cn", 		(String)kedbMap.get("SOLUT_MTHD_CN"));   	 // 해결방안 
				srMap.put("opert_cn", 			(String)kedbMap.get("OPERT_CN"));   	 	 // 작업내용
				srMap.put("atch_file_id",  		(String)procMasterMap.get("ATCH_FILE_ID") ); // 첨부파일ID
				srMap.put("login_user_id",   	loginUserId); 								 // 수정자
				srMap.put("problem_id",     	srId); 										 // 문제SR_ID
				
				kedbDAO.insertKedb(srMap);
				
				// 구성정보
				//주석처리-base 기준에 KEDB에는 구성정보가 없음 2019.04.16
		/*		relList = commonDAO.selectProcRelConf(srMap);
				if(relList.size() > 0){
					for(int j=0; j<relList.size(); j++){
						Map relMap = new HashMap();
						relMap = (Map) relList.get(j);
						relMap.put("kedb_id", kedbId);
						Map relInsertMap = WebUtil.lowerCaseMapKey(relMap);
						kedbDAO.insertKedbRelConf(relInsertMap);
					}
				}*/
			}
		}
	}
}
