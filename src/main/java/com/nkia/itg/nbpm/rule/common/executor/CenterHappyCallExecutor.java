package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;


public class CenterHappyCallExecutor implements Executor {
	private static final Logger logger = LoggerFactory.getLogger(CenterHappyCallExecutor.class);
	
	@Override
	public void execute(long procId) throws Exception {

		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");

		String srId = commonDAO.selectSrId(procId);
		
		Map dataMap = new HashMap();
		Map processDetail = commonDAO.searchHappyCallDataList(srId);
		
		logger.info("정상종료");
		//System.out.println("정상종료");
		/*//작업명
		String templateId = "STATIS_NOTI_MAIL"; //만족도템플릿ID
		
		//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
//		List targetUserList = new ArrayList();
//		targetUserList.add(processDetail.get("REQ_USER_ID"));
//		targetUserList.add(processDetail.get("REQ_USER_ID"));
		List targetUserList = commonDAO.searchEmailUserList((String)processDetail.get("REQ_USER_ID"));
		dataMap.put("TO_USER_LIST", targetUserList);
	
		try {
			//메일 데이터 셋팅
			Map mailInfoMap = new HashMap();
			mailInfoMap.put("KEY","PROCESS");
			mailInfoMap.put("TEMPLATE_ID", templateId);
			mailInfoMap.put("DATA", dataMap);
			mailInfoMap.put("ADD_DATA", processDetail);
			
			EmailSetData emailSender = new EmailSetData(mailInfoMap);
			emailSender.sendMail();
//			processVO.setEmailSended("Y");
		} catch(Exception e){
			throw new NkiaException(e);
		}
		*/
//		//메일 발송이 되면 메일 발송 여부를 업데이트 한다.
//		AbstractNbpmCommand emailSendedCommand = new EmailSendedCommand(processVO);		
//		emailSendedCommand.execute();
		
		
	}
}
