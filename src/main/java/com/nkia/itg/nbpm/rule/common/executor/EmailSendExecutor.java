package com.nkia.itg.nbpm.rule.common.executor;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

public class EmailSendExecutor implements Executor {

	@Override
	public void execute(long procId) throws NkiaException,JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, MessagingException, SQLException {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		
		String srId = commonDAO.selectSrId(procId);
		Map processDetail = commonDAO.selectSrDetailInfo(srId);
		
		String nodeName = commonDAO.selectLastTaskName(srId);
		
		//발송 대상 조회
		List targetList = commonDAO.searchSendMailTargetList(srId);
		String templateName = "";
		
		//발송 데이터
		Map alarmMap = new HashMap();
		alarmMap.put("KEY", "PROCESS");
		alarmMap.put("TEMPLATE_ID", templateName);
		
		Map dataMap = new HashMap();
		dataMap.put("NODENAME", nodeName);
		dataMap.put("TO_USER_LIST", targetList);
		
		alarmMap.put("DATA", dataMap);
		alarmMap.put("ADD_DATA", processDetail);
		//메일 발송 선언
		EmailSetData emailSender = new EmailSetData(alarmMap);
		emailSender.sendMail();
		
		for(int i = 0 ; i < targetList.size() ; i++) {
			Map targetMap = (Map)targetList.get(i);
			Iterator keySet = targetMap.keySet().iterator();
			Map insertMap = new HashMap();
			while(keySet.hasNext()) {
				String key = (String)keySet.next();
				insertMap.put(key.toLowerCase(), (String)targetMap.get(key));
			}
			commonDAO.insertProcEmailSendHis(insertMap);
		}
		//발송 후 이력테이블에 데이터 입력
		//삭제
		commonDAO.deleteProcEmailSend(srId);
	}

}
