package com.nkia.itg.nbpm.rule.common.executor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

public class CheckIncRelExecutor implements Executor {

	/**
	 * 요청등록에서 연계요청시 하위요청은 
	 * 임시 태스크로 이동후 메인요청 등록시 강제진행
	 * @throws Exception 
	 * 
	 */
	//현재 TASK의 SR_ID를 REL_INC_ID로 사용하는 요청 리스트 불러와서 임시TASK에 들어있는경우 강제진행 
	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		SmsSendDAO smsSendDAO = (SmsSendDAO)NkiaApplicationContext.getCtx().getBean("smsSendDAO");
		SmsgHistDAO smsgHistDAO = (SmsgHistDAO)NkiaApplicationContext.getCtx().getBean("smsgHistDAO");
		
		String sr_id = commonDAO.selectSrId(procId);
		
		//해당요청을 상위요청으로 삼는 SR번호 리스트
		List RelIncSrList = new ArrayList();
		RelIncSrList = commonDAO.selectRelIncSrId(sr_id);
		Map srMap = new HashMap();
		
		if(RelIncSrList.size() > 0){
			srMap.put("proc_auth_id", "SYSTEM");
			srMap.put("task_name", "TEMP_REL_INC");
			for(int i=0; i<RelIncSrList.size(); i++){
				Map RelIncSrMap = new HashMap();
				RelIncSrMap = (Map) RelIncSrList.get(i);
				srMap.put("sr_id", (String)RelIncSrMap.get("SR_ID"));
				
				List insertList = new ArrayList();
				insertList = commonDAO.selectTempTaskConfirm(srMap);
				
				if(insertList.size() > 0){
					for(int j=0; j<insertList.size(); j++){
						Map processInfoMap = new HashMap();
						processInfoMap = (Map) insertList.get(j);
											
						Map insertMap = new HashMap();
						insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
						insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
						insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
						insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
						insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
						insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
					//	insertMap.put("nbpm_comment", "");
						
						nbpmCommonClient.workTaskForRule(insertMap);
					}
				}
			
			}
		}	
	}
}
