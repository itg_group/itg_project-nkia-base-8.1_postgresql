package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class CreateFirstMultiConfmExecutor  implements Executor {

	/**
	 * 작업요청, 서버폐기/서비스 종료요청 
	 * 
	 * 요청등록에서 최초의 부서승인(MULTI_CONFM)태스크 생성
	 * [요청등록] -> 임시 태스크 -> [DCO 승인] or [NOS 승인]
	 * 
	 * 2019-03-22 dhkim
	 */
	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		
		String srId = commonDAO.selectSrId(procId);
		Map paramMap = postWorkDAO.selectProcessDetail(srId);
		
		List multiUserList = new ArrayList();
		
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("event_name", "MULTI_ORDER_PROC");
		//최초 멀티 생성전에 temp 테이블의 해당 sr_id의  
		//task_group_id를 null로 수정
		postWorkDAO.updateMultiTemp(searchMap);
		
		searchMap.put("task_name",  "REQUST_REGIST");
		searchMap.put("processid",  (String)paramMap.get("PROCESSID"));
		//searchMap.put("task_order", "0");
		
		multiUserList = postWorkDAO.selectMultiTempUsers(searchMap);
		
		if(multiUserList.size() > 0){
			Map multiUserMap = new HashMap();
			multiUserMap = (Map)multiUserList.get(0);
			multiUserMap.put("sr_id", srId);
			multiUserMap = WebUtil.lowerCaseMapKey(multiUserMap);
			multiUserMap.put("nbpm_processInstanceId", String.valueOf(paramMap.get("PROC_ID")));
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(multiUserMap);
			processVO.setWorker_user_id((String)multiUserMap.get("worker_user_id"));
						
			//최초의 멀티태스크생성
			nbpmCommonClient.registerUser(processVO, multiUserMap);
			Map result = nbpmCommonClient.createMultiTask(processVO, multiUserMap);
			
			//task_group_id 정보 추가
			searchMap.put("task_order", (String)multiUserMap.get("task_order"));
			searchMap.put("task_group_id", result.get("task_group_id"));
			postWorkDAO.updateMultiTemp(searchMap);
		}
	}
}