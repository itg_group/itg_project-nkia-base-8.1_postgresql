package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

public class TempHistEndExecutor  implements Executor {

	/**
	 * 작업요청 - HIST 처리(TEMP_HIST) 진행
	 * 
	 * HIST, (NW or 그외) 작업이 모두 있는 경우,
	 * [HIST 처리] 대기모드임. 
	 * [HIST 처리] -> [결과확인]
	 * 
	 * 2019-03-26 dhkim
	 */
	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");

		String srId = commonDAO.selectSrId(procId);
		
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("task_name", "TEMP_");
		List insertList = new ArrayList();
		insertList = commonDAO.selectTempTaskConfirm(searchMap);
		
		// 조건,
		// PROC_MASTER.PROC_ID = TASK.PROCESSINSTANCEID
		// TASK.TASK_NAME like 'TEMP_%'
		// TASK.STATUS = 'Reserved' 
		if(insertList.size() > 0){
			for(int i=0; i<insertList.size(); i++){
				Map processInfoMap = new HashMap();
				processInfoMap = (Map) insertList.get(i);
									
				Map insertMap = new HashMap();
				insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
				insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
				insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
				insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
				insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
				insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
			//	insertMap.put("nbpm_comment", "");
				
				nbpmCommonClient.workTaskForRule(insertMap);
			}
		}
	}
}