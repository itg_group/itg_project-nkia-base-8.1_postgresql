package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class AllCreateMultiTaskExecutor  implements Executor {

	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		String srId = commonDAO.selectSrId(procId);
		Map paramMap = postWorkDAO.selectProcessDetail(srId);
		
		List multiUserList = new ArrayList();
			
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("event_name", "MULTI_ALL_PROC");
		//최초 멀티 생성전에 temp 테이블의 해당 sr_id의  
		//task_group_id를 null로 수정	
		postWorkDAO.updateMultiTemp(searchMap);
		
		searchMap.put("task_name",  "REQUST_REGIST");
		searchMap.put("processid",  (String)paramMap.get("PROCESSID"));
		
		multiUserList = postWorkDAO.selectMultiTempUsers(searchMap);
		
		if(multiUserList.size() > 0){
			for(int i=0; i<multiUserList.size(); i++){
				Map multiUserMap = new HashMap();
				multiUserMap = (Map)multiUserList.get(i);
				multiUserMap.put("sr_id", srId);
				multiUserMap = WebUtil.lowerCaseMapKey(multiUserMap);
				multiUserMap.put("nbpm_processInstanceId", String.valueOf(paramMap.get("PROC_ID")));
			
				NbpmProcessVO processVO = NbpmUtil.setProcessParameter(multiUserMap);
				processVO.setWorker_user_id((String)multiUserMap.get("worker_user_id"));
						
				//멀티태스크생성
				nbpmCommonClient.registerUser(processVO, multiUserMap);
				Map result = nbpmCommonClient.createMultiTask(processVO, multiUserMap);
			
				//task_group_id 정보 추가
				searchMap.put("task_order", (String)multiUserMap.get("task_order"));
				searchMap.put("task_group_id", result.get("task_group_id"));
				postWorkDAO.updateMultiTemp(searchMap);
			}
		}
	}



}