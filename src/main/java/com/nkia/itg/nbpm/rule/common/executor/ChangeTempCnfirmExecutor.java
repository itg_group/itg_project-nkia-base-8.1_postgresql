package com.nkia.itg.nbpm.rule.common.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

public class ChangeTempCnfirmExecutor implements Executor {

	/**
	 * 
	 * 연계 요청 중일때 부모 요청(변경 요청)의 상태는 
	 * 임시 태스크.
	 * 
	 * 연계 요청이 완료되면,
	 * 부모 요청(서비스 요청) 태스트 이동
	 * 임시 태스크 -> 요청종료
	 * 
	 */
	@Override
	public void execute(long procId) throws Exception {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		SmsSendDAO smsSendDAO = (SmsSendDAO)NkiaApplicationContext.getCtx().getBean("smsSendDAO");
		SmsgHistDAO smsgHistDAO = (SmsgHistDAO)NkiaApplicationContext.getCtx().getBean("smsgHistDAO");
		
		String sr_id = commonDAO.selectSrId(procId);
		/*Map srMap = new HashMap();
		srMap.put("sr_id", sr_id);
		srMap.put("req_type", "SERVICE");*/
		
		// 01. 상위 연계 요청이 있는지 
		// 02. 해당 상위 연계요청의 프로세스 타입이 CHANGE인지 확인
		String upSrId = commonDAO.selectUpSrId(sr_id);
		Map notEndCountMap = new HashMap();
		notEndCountMap.put("sr_id", sr_id);
		notEndCountMap.put("up_sr_id", upSrId);
		
		Map srMap = commonDAO.selectSrDetailInfo(upSrId);
		
		//int notEndCount = commonDAO.selectEndCount(upSrId);
		//남은연계요청중 변경관리의 완료여부에 따라 상위TASK 진행
		int notEndCount = commonDAO.selectChangeEndCount(notEndCountMap);
		
		if(upSrId != null && notEndCount == 0 && srMap.get("REQ_TYPE").equals("SERVICE")){
			Map upSrMap = new HashMap();
			upSrMap.put("sr_id", upSrId);
			upSrMap.put("proc_auth_id", "SYSTEM");
			upSrMap.put("task_name", "TEMP_LINK_CHANGE");
			
			if(upSrId != null){
				List insertList = new ArrayList();
				insertList = commonDAO.selectTempTaskConfirm(upSrMap);
				
				// 조건,
				// PROC_MASTER.PROC_ID = TASK.PROCESSINSTANCEID
				// TASK.TASK_NAME like 'TEMP_WAIT_%'
				// TASK.STATUS = 'Reserved' 
				// WORK_USER_ID = 프로세스 권한 그룹에서 <변경요청 - 연계요청>의 지정 담당자(CHANGE_ROLE_OPETR_05)
				if(insertList.size() > 0){
					for(int i=0; i<insertList.size(); i++){
						Map processInfoMap = new HashMap();
						processInfoMap = (Map) insertList.get(i);
											
						Map insertMap = new HashMap();
						insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
						insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
						insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
						insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
						insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
						insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
					//	insertMap.put("nbpm_comment", "");
						
						nbpmCommonClient.workTaskForRule(insertMap);
						commonDAO.updateServiceEndInfo(upSrId);
					}
						//상위요청 요청자에게 메일 보내기
						
						Map dataMap = new HashMap();
						Map processDetail = commonDAO.searchHappyCallDataList(upSrId);
						//작업명
						String templateId = "STATIS_NOTI_MAIL"; //만족도템플릿ID
						
						//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
						//List targetUserList = new ArrayList();
						//targetUserList.add(processDetail.get("REQ_USER_ID"));
						//targetUserList.add(processDetail.get("REQ_USER_ID"));
						List targetUserList = commonDAO.searchEmailUserList((String)processDetail.get("REQ_USER_ID"));
						dataMap.put("TO_USER_LIST", targetUserList);
						
						try {
						//메일 데이터 셋팅
						Map mailInfoMap = new HashMap();
						mailInfoMap.put("KEY","PROCESS");
						mailInfoMap.put("TEMPLATE_ID", templateId);
						mailInfoMap.put("DATA", dataMap);
						mailInfoMap.put("ADD_DATA", processDetail);
						
						EmailSetData emailSender = new EmailSetData(mailInfoMap);
						emailSender.sendMail();
						//processVO.setEmailSended("Y");
						} catch(Exception e){
						throw new NkiaException(e);
						}
						
						//요청자에게 알림톡
						Map paramMap = postWorkDAO.selectProcessDetail(upSrId);
						
						//SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						
						//String content= "ITSM 업무 알림입니다.\n▶프로세스 정보 : 서비스요청\n▶ID - " +upSrId+"\n▶알림내용 : 서비스요청 종료 알림\n▶제목 : "+(String)paramMap.get("TITLE");
						String content= "ITSM 종료 알림입니다.\n"
							      + "▶프로세스 정보 : 서비스요청\n"
							      +	"▶ID : " +upSrId + "\n"
							      + "▶알림내용 : 서비스요청 종료 알림\n"
							      + "▶신청유형 : " + (String)paramMap.get("REQUST_TYPE_NM") + "\n"
								  + "▶유형분류 : " + (String)paramMap.get("REQ_DOC_NM") + "\n"
							      + "▶제목 : "+(String)paramMap.get("TITLE");
						
						//String resultMsg = messageSource.getMessage("msg.common.result.00004");
						//알림톡 발송로직 들어가야하는곳
						//1. 발송인원 셀렉트
						Map userMap = customizeUbitDAO.selectReqUserHpNo((String)paramMap.get("REQ_USER_ID"));
						//2. 발송내용체크
							//2-1 유형에따라 템플릿코드 셋팅
						String templteCode = "LMSG_20200317133022746749";
						String senderKey = "ef834c839e4a9c83ac3e027b0ed782a7545014d4";
						String sendPhone = "0226264210";
					
						//2-2  유형에따라 rebody 앞에 prefix 붙여줌   [자바스크립트에서 진행]
//						String rebody = "";
						//3. 알림톡테이블 등록	
						String hp_no = "";
								
						hp_no = (String)userMap.get("HP_NO");
						hp_no = hp_no.replace("-", "");
						
						HashMap<String, Object> sendData = new HashMap<String, Object>();
						sendData.put("TO_PHONE", hp_no); // 받는사람전화번호
						sendData.put("MSG_BODY", content); //알림톡 메세지
						sendData.put("RE_BODY", "["+upSrId+"] 서비스요청종료"); //실패시 보내는 대체 문자메세지
						sendData.put("TEMPLATE_CODE", templteCode); //장애전파 템플릿코드
						sendData.put("SENDER_KEY", senderKey); // 센더키
						sendData.put("SEND_PHONE", sendPhone); // 보내는사람
						sendData.put("USER_NM", userMap.get("USER_NM")); 
						smsSendDAO.insertTalkList(sendData);
						smsgHistDAO.insertTalkListHistory(sendData);
//						//메일 발송이 되면 메일 발송 여부를 업데이트 한다.
//						AbstractNbpmCommand emailSendedCommand = new EmailSendedCommand(processVO);		
//						emailSendedCommand.execute();
				}
				
				
			}
		}
	}	
}
