package com.nkia.itg.nbpm.noti;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.directwebremoting.Container;
import org.directwebremoting.ScriptSession;
import org.directwebremoting.ServerContextFactory;
import org.directwebremoting.extend.ScriptSessionManager;
import org.directwebremoting.impl.DefaultScriptSession;
import org.directwebremoting.proxy.ScriptProxy;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;

public class NbpmPushSender {
	private static NbpmPushSender instance;
	
	public static NbpmPushSender getInstance(){
		if( instance == null ){
			instance = new NbpmPushSender();
		}
		return instance;
	}
	
	public NbpmPushSender() {
		
	}
	
	public boolean sendServiceDesk( Map paramMap ) throws NkiaException {
		Container container = ServerContextFactory.get().getContainer();
		ScriptSessionManager manager = container.getBean(ScriptSessionManager.class);
		Collection<ScriptSession> sdeskSessions = manager.getScriptSessionsByPage(BaseConstraints.CONTEXT + "/itg/index.do");
		
		ArrayList<DefaultScriptSession> sessionList = new ArrayList();
		ArrayList<DefaultScriptSession> mySessionList = new ArrayList();
		
		String userId = (String)paramMap.get("userId");
		String hpNo = (String)paramMap.get("hpNo");
		String srId = (String)paramMap.get("srId");
			
		if(sdeskSessions != null && !sdeskSessions.isEmpty()) {
			Iterator sessionsIter = sdeskSessions.iterator();
			while(sessionsIter.hasNext()) {
				DefaultScriptSession sesseion = (DefaultScriptSession)sessionsIter.next();
				sessionList.add(sesseion);
			}
		}
		
		long maxLastAccessTime = 0;
		for(int i = 0 ; i < sessionList.size() ; i++) {
			DefaultScriptSession sesseion = sessionList.get(i);
			UserVO userVO = (UserVO)sesseion.getAttribute("userVO");
			if(userVO != null) {	//userVO가 없으면 알림을 보내지 않는다.
				String sessionUserId = userVO.getUser_id();
				if(userId.equals(sessionUserId)) {
					long lastAccessTime = sesseion.getLastAccessedTime();
					if(maxLastAccessTime < lastAccessTime) {
						maxLastAccessTime = lastAccessTime;
					}
					mySessionList.add(sesseion);
				}				
			}
		}

		DefaultScriptSession sendSession = null;
		for(int i = 0 ; i < mySessionList.size() ; i++) {
			DefaultScriptSession session = mySessionList.get(i);
			if(maxLastAccessTime == session.getLastAccessedTime()) {
				sendSession = session;
			}
			

		}
		//UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		//String userId = userVO.getUser_id();
		Map result = new HashMap();
		result.put("sr_id", srId);
		result.put("push_type",NbpmConstraints.PUSH_TYPE_SERVICEDESK);
		result.put("hp_no", hpNo);
		
		if(sendSession != null) {
			ScriptProxy sdeskProxy = new ScriptProxy(sendSession);
			sdeskProxy.addFunctionCall("controlNbpmPush", result);
		}
		
		return true;
	}

	public void sendTaskAlram(Map paramMap) {
		Container container = ServerContextFactory.get().getContainer();
		ScriptSessionManager manager = container.getBean(ScriptSessionManager.class);
		String url = "";
		Collection<ScriptSession> sdeskSessions = manager.getScriptSessionsByPage(BaseConstraints.CONTEXT + url);
		
		String msg = getMessage(paramMap);
	}

	private String getMessage(Map paramMap) {
		// TODO LBASS
		return null;
	}
}
