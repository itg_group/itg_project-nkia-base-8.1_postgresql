/*
 * @(#)PushPortalProcess.java              2013. 6. 13.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.noti;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.common.NbpmConstraints;

@Repository("nbpmPushManager")
public class NbpmPushManager {
	
	private NbpmPushSender nbpmPushSender;
	
	public void setNbpmPushSender(NbpmPushSender nbpmPushSender) {
		this.nbpmPushSender = nbpmPushSender;
	}
	
	public void notify(String msg){
	}
	
	public void setUserId(String userId){
	}
	
	public void pushTargetUser(Map paramMap){
		try {
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String push_type = "";
			
			if(paramMap.get("push_type") != null) {
				push_type = (String)paramMap.get("push_type");
				
				if(NbpmConstraints.PUSH_TYPE_SERVICEDESK.equals(push_type)) {
					nbpmPushSender.getInstance().sendServiceDesk( paramMap );
				} else if(NbpmConstraints.PUSH_TYPE_ALRAM.equals(push_type)) {
					nbpmPushSender.getInstance().sendTaskAlram( paramMap );
				}
			}
			
		} catch (Exception e) {
			new NkiaException(e);
		}
	}
}
