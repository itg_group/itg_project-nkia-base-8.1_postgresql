package com.nkia.itg.nbpm.noti;

import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;

public class NbpmPushThread extends Thread {
	
	public Map param = null;
	
	public NbpmPushThread(Map param) {
		this.param = param;
	}
	
	public void run() {
		try {
			NbpmPushManager pushManager = (NbpmPushManager) NkiaApplicationContext.getCtx().getBean("nbpmPushManager");
			pushManager.pushTargetUser(this.param);
		} catch (Exception e) {
			new NkiaException(e);
		}
	}	
}
