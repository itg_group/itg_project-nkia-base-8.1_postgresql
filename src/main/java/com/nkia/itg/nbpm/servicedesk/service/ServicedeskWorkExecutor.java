package com.nkia.itg.nbpm.servicedesk.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public interface ServicedeskWorkExecutor {

	public Map chageReqType(NbpmProcessVO processVO, Map paramMap) throws NkiaException ;
	
	//@@20130711 김도원 START
	//상담이력 기능 추가	
	public boolean checkExistSrId(HashMap param) throws NkiaException;

	public void updateCounselingHistory(HashMap param) throws NkiaException, SQLException;
	//@@20130711 김도원 END
	
	public void sendSoftPhone(ModelMap paramMap, UserVO userVO, String remoteAddr) throws NkiaException;
	
	public void changeWaitMode(ModelMap paramMap, UserVO userVO, String remoteAddr) throws NkiaException;
	
	public void changeBusyMode(ModelMap paramMap, UserVO userVO, String remoteAddr) throws NkiaException;

	public void changeSDeskUser(NbpmProcessVO processVO, ModelMap paramMap) throws NkiaException;

	public boolean checkCounsellerInfo(Map paramMap) throws NkiaException;

	public boolean changeCounsellerInfo(HashMap param) throws NkiaException;

	public boolean checkTaskRole(HashMap param) throws NkiaException;
}
