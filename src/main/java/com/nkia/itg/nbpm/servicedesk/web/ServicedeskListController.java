/*
 * @(#)ServicedeskController.java              2013. 5. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.servicedesk.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.servicedesk.service.ServicedeskListExecutor;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ServicedeskListController {
	
	@Resource(name="servicedeskCommonListExecutor")
	private ServicedeskListExecutor servicedeskListExecutor;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@RequestMapping(value="/itg/nbpm/servicedesk/list/goServicedeskTab.do")
	public String goServicedeskTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/servicedesk/servicedeskTab.nvf";

		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = userVO.getUser_id();
		paramMap.put("sd_list_type", request.getParameter("sd_list_type"));
		paramMap.put("login_user_id", userId);
		
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/servicedesk/list/goServicedeskMain.do")
	public String goServicedeskMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/servicedesk/servicedeskMain.nvf";
		try {
			String userId = request.getParameter("user_id");
			if(userId == null || "".equals(userId)) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				userId = userVO.getUser_id();
			}
			paramMap.put("sd_list_type", request.getParameter("sd_list_type"));
			paramMap.put("login_user_id", userId);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/servicedesk/list/searchServicedeskList.do")
	public @ResponseBody ResultVO searchServicedeskList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try{
			
			int totalCount = 0;
			
			String sdListType = (String)paramMap.get("sd_list_type");
			
			PagingUtil.getFristEndNum(paramMap);
			paramMap.put("rejectedText", messageSource.getMessage("res.label.nbpm.reject"));
			
			if(NbpmConstraints.LIST_TYPE_READY.equals(sdListType)) {
				totalCount = servicedeskListExecutor.searchReadyServicedeskListCount(paramMap);
				resultList = servicedeskListExecutor.searchReadyServicedeskList(paramMap);
			} else if(NbpmConstraints.LIST_TYPE_ING.equals(sdListType)) {
				totalCount = servicedeskListExecutor.searchIngServicedeskListCount(paramMap);
				resultList = servicedeskListExecutor.searchIngServicedeskList(paramMap);
			} else if(NbpmConstraints.LIST_TYPE_COMPLETE.equals(sdListType)) {
				totalCount = servicedeskListExecutor.searchCompleteServicedeskListCount(paramMap);
				resultList = servicedeskListExecutor.searchCompleteServicedeskList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
