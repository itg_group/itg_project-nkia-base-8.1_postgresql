/*
 * @(#)ServicedeskCommonExecutor.java              2013. 5. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.servicedesk.service.impl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.servicedesk.dao.ServicedeskDAO;
import com.nkia.itg.nbpm.servicedesk.service.ServicedeskListExecutor;

@Service("servicedeskCommonListExecutor")
public class ServicedeskCommonListExecutor implements ServicedeskListExecutor {

	@Resource(name = "servicedeskDAO")
	private ServicedeskDAO servicedeskDAO;
	
	//@@20130711 김도원 START
	//요청처리 DAO 추가
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	//@@20130711 김도원 END
	
	public int searchServicedeskListCount(ModelMap paramMap) throws NkiaException, SQLException {
		return servicedeskDAO.searchServicedeskListCount(paramMap);
	}

	public List searchServicedeskList(ModelMap paramMap) throws NkiaException, SQLException {
		return servicedeskDAO.searchServicedeskList(paramMap);
	}
	
	public int searchReadyServicedeskListCount(ModelMap paramMap)
			throws NkiaException, SQLException {
		return servicedeskDAO.searchReadyServicedeskListCount(paramMap);
	}

	public List searchReadyServicedeskList(ModelMap paramMap) throws NkiaException, SQLException { 
		return servicedeskDAO.searchReadyServicedeskList(paramMap);
	}

	public int searchIngServicedeskListCount(ModelMap paramMap) throws NkiaException, SQLException {
		return servicedeskDAO.searchIngServicedeskListCount(paramMap);
	}

	public List searchIngServicedeskList(ModelMap paramMap) throws NkiaException, SQLException {
		return servicedeskDAO.searchIngServicedeskList(paramMap);
	}

	public int searchCompleteServicedeskListCount(ModelMap paramMap) throws NkiaException, SQLException {
		return servicedeskDAO.searchCompleteServicedeskListCount(paramMap);
	}

	public List searchCompleteServicedeskList(ModelMap paramMap) throws NkiaException, SQLException {
		return servicedeskDAO.searchCompleteServicedeskList(paramMap);
	}
}
