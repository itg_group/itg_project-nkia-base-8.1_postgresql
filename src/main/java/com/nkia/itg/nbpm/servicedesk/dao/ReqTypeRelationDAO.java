package com.nkia.itg.nbpm.servicedesk.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository("reqTypeRelationDAO")
public class ReqTypeRelationDAO {
	
	private final String[] PROC_ACCOUNT = {"PROC_ACCOUNT","PROC_ACCOUNT_SUB"};
	private final String[] PROC_AVAIL = {"PROC_AVAIL"};
	private final String[] PROC_BACKUP = {"PROC_BACKUP","PROC_BACKUP_SUB"};
	private final String[] PROC_CAPACITY = {"PROC_CAPACITY"};
	private final String[] PROC_CATALOG = {"PROC_CATALOG"};
	private final String[] PROC_CHANGE = {"PROC_CHANGE"};
	private final String[] PROC_CI = {"PROC_CI"};
	private final String[] PROC_CLOUD = {"PROC_CLOUD","PROC_CLOUD_SUB"};
	private final String[] PROC_DISCONTENT = {"PROC_DISCONTENT"};
	//private final String[] PROC_FINANCE = {"PROC_FINANCE"};
	private final String[] PROC_GENERAL = {"PROC_GENERAL"};
	private final String[] PROC_INCDNT = {"PROC_INCDNT"};
	private final String[] PROC_INSTALL = {"PROC_INSTALL"};
	private final String[] PROC_JOB = {"PROC_JOB","PROC_JOB_SUB"};
	private final String[] PROC_PERFORM = {"PROC_PERFORM"};
	private final String[] PROC_PROBLEM = {"PROC_PROBLEM"};
	private final String[] PROC_RECOVERY = {"PROC_RECOVERY"};
	private final String[] PROC_RELEASE = {"PROC_RELEASE"};
	private final String[] PROC_SLA = {"PROC_SLA"};
	private final String[] PROC_STATUS = {"PROC_STATUS","PROC_STATUS_SUB"};
	private final String[] PROC_TECH = {"PROC_TECH","PROC_TECH_SUB_EQUIP","PROC_TECH_SUB_USER"};

	public List searchReqTypeTableList(String reqType) {
		List resultList = null;
		if(reqType != null) {
			if(("account".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_ACCOUNT);
			} else if(("avail".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_AVAIL);
			} else if(("backup".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_BACKUP);
			} else if(("capacity".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_CAPACITY);
			} else if(("catalog".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_CATALOG);
			} else if(("change".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_CHANGE);
			} else if(("ci".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_CI);
			} else if(("cloud".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_CLOUD);
			} else if(("discontent".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_DISCONTENT);
			} else if(("general".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_GENERAL);
			} else if(("incident".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_INCDNT);
			} else if(("install".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_INSTALL);
			} else if(("job".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_JOB);
			} else if(("perform".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_PERFORM);
			} else if(("problem".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_PROBLEM);
			} else if(("recovery".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_RECOVERY);
			} else if(("release".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_RELEASE);
			} else if(("sla".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_SLA);
			} else if(("status".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_STATUS);
			} else if(("tech".toUpperCase()).equals(reqType.toUpperCase())) {
				resultList = setTableList(PROC_TECH);
			}
		}
		return resultList;
	}

	private List setTableList(String[] reqTypeArray) {
		List resultList = new ArrayList();
		for(int i = 0 ; i < reqTypeArray.length ; i++) {
			resultList.add(reqTypeArray[i]);	
		}
		return resultList;
	}

}
