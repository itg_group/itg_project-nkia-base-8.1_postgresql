/*
 * @(#)ServiceDeskExecutor.java              2013. 5. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.servicedesk.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ServicedeskListExecutor {

	public int searchReadyServicedeskListCount(ModelMap paramMap) throws NkiaException, SQLException;
	
	public List searchReadyServicedeskList(ModelMap paramMap) throws NkiaException, SQLException;

	public int searchServicedeskListCount(ModelMap paramMap) throws NkiaException, SQLException;

	public List searchServicedeskList(ModelMap paramMap) throws NkiaException, SQLException;
	
	public int searchIngServicedeskListCount(ModelMap paramMap) throws NkiaException, SQLException;

	public List searchIngServicedeskList(ModelMap paramMap) throws NkiaException, SQLException;

	public int searchCompleteServicedeskListCount(ModelMap paramMap) throws NkiaException, SQLException;

	public List searchCompleteServicedeskList(ModelMap paramMap) throws NkiaException, SQLException;

}
