package com.nkia.itg.nbpm.servicedesk.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.common.NbpmTransactionManager;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.ChangeWorkerCmd;
import com.nkia.itg.nbpm.executor.StartProcessCmd;
import com.nkia.itg.nbpm.executor.TerminationProcessCmd;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.servicedesk.dao.ReqTypeRelationDAO;
import com.nkia.itg.nbpm.servicedesk.dao.ServicedeskDAO;
import com.nkia.itg.nbpm.servicedesk.service.ServicedeskWorkExecutor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

@Service("servicedeskCommonWorkExecutor")
public class ServicedeskCommonWorkExecutor implements ServicedeskWorkExecutor {

	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
		
	@Resource(name = "servicedeskDAO")
	private ServicedeskDAO servicedeskDAO;
	
	@Resource(name = "reqTypeRelationDAO")
	private ReqTypeRelationDAO reqTypeRelationDAO;
	
	@Override
	public Map chageReqType(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		Map resultMap = new HashMap();
		try {
			//nsm.begin();
				
			String srId = (String)paramMap.get("srId");
			String reqType = (String)paramMap.get("reqType");
			
			Map srData = postWorkDAO.selectProcessDetail(srId);
			
			if (srData != null) {
				Map paramMapForVersionInfo = new HashMap();
				paramMapForVersionInfo.put("process_type", reqType);
				paramMapForVersionInfo.put("startTaskName", NbpmConstraints.TASK_NAME_REQUEST);
				NbpmProcessVO nbpmVO = postWorkDAO.selectFinalVersionProcessInfo(paramMapForVersionInfo);
				nbpmVO.setProcessInstanceId((Long)srData.get("PROC_ID"));
				
				AbstractNbpmCommand nbpmCommad = new TerminationProcessCmd(nbpmVO);
				nbpmCommad.execute();
				
				nbpmVO.setProcessInstanceId(0);
				nbpmVO.setReq_user_id((String)srData.get("REQ_USER_ID"));
				AbstractNbpmCommand startCommad = new StartProcessCmd(nbpmVO);
				resultMap = (Map)startCommad.execute();
			}
			
			paramMap.put("procId", resultMap.get("processInstanceId"));
			postWorkDAO.updateReqTypeForInit(paramMap);
			List reqTableList = reqTypeRelationDAO.searchReqTypeTableList(reqType);
			for(int i = 0 ; i < reqTableList.size() ; i++) {
				String tableName = (String)reqTableList.get(i);
				Map deleteMap = new HashMap();
				deleteMap.put("sr_id", srId);
				deleteMap.put("table_name", tableName);
				servicedeskDAO.deleteProcDetailForChangeReqType(deleteMap);				
			}
			
			resultMap.put("srId", srId);
			
			//nsm.commit();
		} catch(Exception e) {
			//nsm.rollback();
			throw new NkiaException(e);
		}
		
		return resultMap;
	}
	
	//@@20130711 김도원 START
	//상담이력 기능 추가
	public void updateCounselingHistory(HashMap param) throws NkiaException, SQLException {
		
		int count = servicedeskDAO.selectCounselingHistoryCount(param);
		if(count > 0) {
			servicedeskDAO.updateCounselingHistory(param);			
		} else {
			servicedeskDAO.insertCounselingHistory(param);
		}
	}

	public boolean checkExistSrId(HashMap param) throws NkiaException {
		boolean result = false;
		String srId = (String)param.get("sr_id");
		int srCount = postWorkDAO.selectExistSrId(srId);
		if(srCount > 0) {
			result = true;
		}
		return result;
	}
	//@@20130711 김도원 END

//	public void sendSoftPhone(ModelMap paramMap, UserVO userVO, String remoteAddr) throws NkiaException {
//		SoftPhoneClient softPhoneClient = new SoftPhoneClient();
//		softPhoneClient.startClient();
//	}
//
//	public void changeWaitMode(ModelMap paramMap, UserVO userVO, String remoteAddr) throws NkiaException {
//		SoftPhoneManager provider = new SoftPhoneManager();
//		provider.changeWaitMode(userVO, remoteAddr);
//	}
//
//	public void changeBusyMode(ModelMap paramMap, UserVO userVO, String remoteAddr) throws NkiaException {
//		SoftPhoneManager provider = new SoftPhoneManager();
//		provider.changeBusyMode(userVO, remoteAddr);
//	}

	@Override
	public void changeSDeskUser(NbpmProcessVO processVO, ModelMap paramMap) throws NkiaException {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {
			//nsm.begin();
			
			AbstractNbpmCommand changeWorkerCommad = new ChangeWorkerCmd(processVO);
			changeWorkerCommad.execute();
			
			Map operInfo = new HashMap();
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(processVO.getSr_id() != null) operInfo.put("sr_id", processVO.getSr_id());			
			operInfo.put("oper_type", "servicedesk");
			operInfo.put("oper_user_id", processVO.getWorker_user_id());
			operInfo.put("select_type", "SINGLE");
			postWorkDAO.deleteProcessOrerator(operInfo);
			postWorkDAO.insertProcessOrerator(operInfo);
			
			//nsm.commit();
		} catch(Exception e) {
			//nsm.rollback();
			throw new NkiaException(e);
		}
	}

	@Override
	public boolean checkCounsellerInfo(Map paramMap) throws NkiaException {
		boolean result = false;
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		Map resultMap = new HashMap();
		try {
			//nsm.begin();
			
			paramMap.put("oper_type", NbpmConstraints.SERVICEDESK_ROLE);
			String cnslrUserId = servicedeskDAO.selectCounsellerInfo(paramMap);
			String curCnslrUserId = (String)paramMap.get("cnslr_user_id");
			
			//상담원 정보가 없을 경우에는 상담원 정보를 등록 한다.)
			if(!(cnslrUserId != null && !"".equals(cnslrUserId))) {
				String srId = (String)paramMap.get("sr_id");
				int taskId = (Integer)paramMap.get("task_id");
				
			  	Map operInfoMap = new HashMap();
			    operInfoMap.put("oper_user_id", curCnslrUserId);
			    operInfoMap.put("oper_type", NbpmConstraints.SERVICEDESK_ROLE);
			    operInfoMap.put("sr_id", srId);
				
				postWorkDAO.insertProcessOrerator(operInfoMap);
				
				NbpmProcessVO pVO = new NbpmProcessVO();
				pVO.setTask_id((long)(taskId));
				pVO.setWorker_user_id(curCnslrUserId);
				AbstractNbpmCommand changeWorkerCmd = new ChangeWorkerCmd(pVO);			
				changeWorkerCmd.execute();
				
				result = true;
			} else {
				if(cnslrUserId.equals(curCnslrUserId)) {
					result = true;
				} else {
					result = false;
				}
			}
			
			//nsm.commit();
		} catch(Exception e) {
			//nsm.rollback();
			throw new NkiaException(e);
		}
		
		return result;
	}

	@Override
	public boolean changeCounsellerInfo(HashMap paramMap) throws NkiaException {
		boolean result = false;
		Map resultMap = new HashMap();
		try {
			//nsm.begin();
			
			paramMap.put("oper_type", NbpmConstraints.SERVICEDESK_ROLE);
			String curCnslrUserId = (String)paramMap.get("cnslr_user_id");
			
			String srId = (String)paramMap.get("sr_id");
			int taskId = (Integer)paramMap.get("task_id");
			
		  	Map operInfoMap = new HashMap();
		    operInfoMap.put("oper_user_id", curCnslrUserId);
		    operInfoMap.put("oper_type", NbpmConstraints.SERVICEDESK_ROLE);
		    operInfoMap.put("sr_id", srId);
			
		    postWorkDAO.deleteProcessOrerator(operInfoMap);
			postWorkDAO.insertProcessOrerator(operInfoMap);
			
			NbpmProcessVO pVO = new NbpmProcessVO();
			pVO.setTask_id((long)(taskId));
			pVO.setWorker_user_id(curCnslrUserId);
			AbstractNbpmCommand changeWorkerCmd = new ChangeWorkerCmd(pVO);			
			changeWorkerCmd.execute();
			
			result = true;

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return result;
	}

	@Override
	public boolean checkTaskRole(HashMap param) throws NkiaException {
		boolean result = false;
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {
			//nsm.begin();
			
			String roleId = servicedeskDAO.selectTaskRoleInfo(param);
			if(NbpmConstraints.SERVICEDESK_ROLE.equals(roleId)) {
				result = true;
			}
			
			//nsm.commit();
		} catch(Exception e) {
			//nsm.rollback();
			throw new NkiaException(e);
		}
		
		return result;
	}

	@Override
	public void sendSoftPhone(ModelMap paramMap, UserVO userVO,
			String remoteAddr) throws NkiaException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeWaitMode(ModelMap paramMap, UserVO userVO,
			String remoteAddr) throws NkiaException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeBusyMode(ModelMap paramMap, UserVO userVO,
			String remoteAddr) throws NkiaException {
		// TODO Auto-generated method stub
		
	}
}
