/*
 * @(#)ServicedeskDAO.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.servicedesk.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("servicedeskDAO")
public class ServicedeskDAO extends NbpmAbstractDAO {
	
	public int searchServicedeskListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("ServicedeskDAO.searchServicedeskListCount", paramMap);
	}

	public List searchServicedeskList(ModelMap paramMap) throws SQLException {
		return list("ServicedeskDAO.searchServicedeskList", paramMap);
	}
	
	public int searchReadyServicedeskListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("ServicedeskDAO.searchReadyServicedeskListCount", paramMap);
	}

	public List searchReadyServicedeskList(ModelMap paramMap) throws SQLException {
		return list("ServicedeskDAO.searchReadyServicedeskList", paramMap);
	}

	public int searchIngServicedeskListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("ServicedeskDAO.searchIngServicedeskListCount", paramMap);
	}

	public List searchIngServicedeskList(ModelMap paramMap) throws SQLException {
		return list("ServicedeskDAO.searchIngServicedeskList", paramMap);
	}

	public int searchCompleteServicedeskListCount(ModelMap paramMap) throws SQLException {
		return (Integer)selectByPk("ServicedeskDAO.searchCompleteServicedeskListCount", paramMap);
	}

	public List searchCompleteServicedeskList(ModelMap paramMap) throws SQLException {
		return list("ServicedeskDAO.searchCompleteServicedeskList", paramMap);
	}

	//@@20130711 김도원 START
	//상담이력 기능 추가
	public void updateCounselingHistory(HashMap param) throws SQLException {
		this.getSqlMapClient().update("ServicedeskDAO.updateCounselingHistory", param);
	}
	
	public String selectCounselingHistory(Map param) throws SQLException {
		return (String)selectByPk("ServicedeskDAO.selectCounselingHistory", param);
	}
	
	public int selectCounselingHistoryCount(HashMap param) throws SQLException {
		return (Integer)selectByPk("ServicedeskDAO.selectCounselingHistoryCount", param);
	}
	
	public void insertCounselingHistory(HashMap param) throws SQLException {
		this.getSqlMapClient().insert("ServicedeskDAO.insertCounselingHistory", param);
	}
	//@@20130711 김도원 END
	
	public void deleteProcDetailForChangeReqType(Map paramMap) throws SQLException {
		this.getSqlMapClient().delete("ServicedeskDAO.deleteProcDetailForChangeReqType", paramMap);
	}

	public String selectCounsellerInfo(Map param) throws SQLException {
		return (String)selectByPk("ServicedeskDAO.selectCounsellerInfo", param);
	}

	public String selectTaskRoleInfo(HashMap param) throws SQLException {
		return (String)selectByPk("ServicedeskDAO.selectTaskRoleInfo", param);
	}
}
