package com.nkia.itg.nbpm.servicedesk.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.jbpm.task.service.PermissionDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.servicedesk.service.ServicedeskWorkExecutor;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ServicedeskWorkController {

	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name="servicedeskCommonWorkExecutor")
	private ServicedeskWorkExecutor servicedeskWorkExecutor;
	
	@RequestMapping(value="/itg/nbpm/servicedesk/work/connectSoftPhone.do")
	public @ResponseBody ResultVO connectSoftPhone(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			servicedeskWorkExecutor.sendSoftPhone(paramMap, userVO, request.getRemoteAddr());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/servicedesk/work/changeWaitMode.do")
	public @ResponseBody ResultVO changeWaitMode(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			servicedeskWorkExecutor.changeWaitMode(paramMap, userVO, request.getRemoteAddr());
		} catch(Exception e) {
			String message = "상태변경실패";
			throw new NkiaException(e, message);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/servicedesk/work/changeBusyMode.do")
	public @ResponseBody ResultVO changeBusyMode(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			servicedeskWorkExecutor.changeBusyMode(paramMap, userVO, request.getRemoteAddr());
		} catch(Exception e) {
			String message = "상태변경실패";
			throw new NkiaException(e, message);
		}
		return resultVO;
	}
	
	/**
	 * 요청의 요청유형을 변경한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/servicedesk/work/chageReqType.do")
	public @ResponseBody ResultVO chageReqType(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			
			Map result = servicedeskWorkExecutor.chageReqType(processVO, paramMap);
		
			resultVO.setResultMsg("요청유형이 초기화 되었습니다.");
			resultVO.setResultMap((HashMap) result);
			
		} catch(PermissionDeniedException e) {
			message = "담당자가 아닙니다.\n확인 후 처리바랍니다.";
			throw new NkiaException(e, message);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/servicedesk/work/updateCounselingHistory.do")
	public @ResponseBody ResultVO updateCounselingHistory(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			HashMap param = new HashMap();
			param.put("sr_id", paramMap.get("sr_id"));

			boolean isExist = servicedeskWorkExecutor.checkExistSrId(param);
			
			if(isExist) {
				param.put("upd_user_id", loginUserId);
				param.put("counseling_content", paramMap.get("counseling_content"));
				servicedeskWorkExecutor.updateCounselingHistory(param);
				resultVO.setResultMsg("등록되었습니다.");
			} else {
				resultVO.setResultMsg("요청정보가 저장되지 않았습니다.\n요청을 임시저장 또는 등록 후에 저장하여 주십시오.");
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/servicedesk/work/checkCounsellerInfo.do")
	public @ResponseBody ResultVO checkCounsellerInfo(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		
		final int DO_NOT_WORK_STATE = 0; 	//작업 할 수 없는 상태
		final int REGISTER = 1;				//상담원 등록
		final int ALREADY_REGISTER = 2;		//상담원 정보 이미 등록
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			HashMap param = new HashMap();
			param.put("sr_id", paramMap.get("sr_id"));
			param.put("cnslr_user_id", paramMap.get("cnslr_user_id"));
			param.put("task_id", paramMap.get("task_id"));

			
			boolean isWorkable = servicedeskWorkExecutor.checkTaskRole(param);
			if(isWorkable) {
				boolean isReceipt = servicedeskWorkExecutor.checkCounsellerInfo(param);				
				if(isReceipt) {
					resultVO.setSuccess(true);
					resultVO.setResultInt(REGISTER);
				} else {
					resultVO.setSuccess(true);
					resultVO.setResultInt(ALREADY_REGISTER);
					resultVO.setResultMsg("이미 접수된 요청입니다. 재접수 하시겠습니까?");
				}
			} else {
				resultVO.setSuccess(true);
				resultVO.setResultInt(DO_NOT_WORK_STATE);
			}
			
		} catch(Exception e) {			
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/servicedesk/work/changeCounsellerInfo.do")
	public @ResponseBody ResultVO changeCounsellerInfo(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			HashMap param = new HashMap();
			param.put("sr_id", paramMap.get("sr_id"));
			param.put("cnslr_user_id", paramMap.get("cnslr_user_id"));
			param.put("task_id", paramMap.get("task_id"));

			boolean success = servicedeskWorkExecutor.changeCounsellerInfo(param);
			resultVO.setSuccess(success);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
