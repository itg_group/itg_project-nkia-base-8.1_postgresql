/*
 * @(#)GetTaskCommentListCmd.java              2014. 4. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.List;

import org.jbpm.task.Comment;

/**
 * 타스크의 의견 정보를 조회한다.
 * @author Kim Do-Won
 *
 */
public class GetTaskCommentListCmd extends AbstractNbpmCommand {

	private long taskId = 0;

	public GetTaskCommentListCmd(long taskId) {
		this.taskId = taskId;
	}

	public List execute() {
		List<Comment> commentList = null;
		commentList = taskClient.getTask(taskId).getTaskData().getComments();		
		return commentList;
	}

}
