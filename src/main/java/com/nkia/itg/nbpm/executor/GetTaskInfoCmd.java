package com.nkia.itg.nbpm.executor;

import java.util.HashMap;

import org.jbpm.task.Task;

/**
 * 타스크의 정보를 조회한다.
 * @author Kim Do-Won
 *
 */
public class GetTaskInfoCmd extends AbstractNbpmCommand {

	private long taskId;
	
	public GetTaskInfoCmd(long taskId) {
		this.taskId = taskId;
	}
	
	@Override
	public Object execute() {
		HashMap resultMap = new HashMap();
		taskClient.connect();
		Task task = taskClient.getTask(taskId);
		resultMap.put("worker_user_id", task.getTaskData().getActualOwner().getId());
		resultMap.put("task_name", task.getTaskData().getTask_name());
		
		return resultMap;
	}
}
