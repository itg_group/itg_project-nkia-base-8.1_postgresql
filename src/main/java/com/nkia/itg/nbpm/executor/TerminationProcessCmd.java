package com.nkia.itg.nbpm.executor;

import java.util.HashMap;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 프로세스를 종료한다.
 * @author Kim Do-Won
 *
 */
public class TerminationProcessCmd extends AbstractNbpmCommand {
	private static final long serialVersionUID = 1L;
	private NbpmProcessVO processVO = null;

	public NbpmProcessVO getProcessVO() {
		return processVO;
	}

	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public TerminationProcessCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;	
	}


	@Override
	public Object execute() {
		//프로세스를 종료시킨다.
		HashMap resultMap = new HashMap();
		loadedKsession.abortProcessInstance(processVO.getProcessInstanceId());
		
		return resultMap;
	}

}
