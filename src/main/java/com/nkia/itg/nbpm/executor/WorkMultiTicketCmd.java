package com.nkia.itg.nbpm.executor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.drools.runtime.process.NodeInstance;
import org.drools.runtime.process.WorkflowProcessInstance;
import org.jbpm.ruleflow.instance.RuleFlowProcessInstance;
import org.jbpm.task.Status;
import org.jbpm.task.Task;
import org.jbpm.task.User;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.workflow.core.node.HumanTaskNode;
import org.jbpm.workflow.instance.node.WorkItemNodeInstance;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class WorkMultiTicketCmd extends AbstractNbpmCommand {
	
	private NbpmProcessVO processVO = null;
	private String eventName = null;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}


	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}

	public WorkMultiTicketCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}
	
	public WorkMultiTicketCmd(NbpmProcessVO pVO, String eventName) {
		this.processVO = pVO;
		this.eventName = eventName;
	}
	
	@Override
	public Object execute() {
		HashMap resultMap = new HashMap();
				
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance) loadedKsession.getProcessInstance(processVO.getProcessInstanceId());
		processInstance.signalEvent(eventName, null);
		
		List<Status> status = new ArrayList<Status>();
		status.add(Status.Reserved);
		status.add(Status.Ready);
		status.add(Status.Created);
		
		long processInstanceOne = processInstance.getId();
		long taskId = 0;
		String taskName = "";
		//타스크를 조회하고 담당자 ID가 MULTIUSER인 타스크를 찾는다.
		List<TaskSummary> waitTasks = taskClient.getTasksByStatusByProcessId(processInstanceOne, status,"en-UK");
		for (int i = 0; i < waitTasks.size(); i++) {
			TaskSummary task = waitTasks.get(i);
			User actorId = task.getActualOwner();
			if (actorId != null) {
				if ("MULTIUSER".equals(actorId.getId())) {
					taskId = task.getId();
				}
			}
		}
		
		if (taskId > 0) {
			//찾은 타스크의 담당자 ID를 그리드에서 선택한 담당자 ID로 변경한다.
			taskClient.claim(taskId, processVO.getWorker_user_id());
			//타스크의 영문명과 노드 ID를 업데이트 한다.(이 로직이 API들에서 반복되는데 삭제를 검토해볼 필요는 있을 듯)
			Collection nodeCollection = ((RuleFlowProcessInstance) processInstance).getNodeInstances();
			Iterator nodeIterator = nodeCollection.iterator();
			while (nodeIterator.hasNext()) {
				NodeInstance nodeInst = (NodeInstance) nodeIterator.next();
				if (nodeInst instanceof WorkItemNodeInstance) {
					HashMap nodeMap = new HashMap();
					HumanTaskNode node = (HumanTaskNode) nodeInst.getNode();
					
					Map nodeMetaMap = node.getMetaData();
					String nodeId = (String) nodeMetaMap.get("UniqueId");
					taskName = (String) node.getWork().getParameter("TaskName");
					Task task = taskClient.getTask(taskId);
					task.getTaskData().setNodeId(nodeId);
					task.getTaskData().setTask_name(taskName);
				}
			}
		}
		resultMap.put("task_id", taskId);
		resultMap.put("task_name", taskName);
		return resultMap;
	}

}
