/*
 * @(#)CheckTasUserkCmd.java              2013. 4. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.HashMap;

import org.jbpm.task.Task;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 담당자정보를 조회하는 클래스이다. - 클래스 이름이 오타가 있음 수정해야 할 듯 함.
 * @author Kim Do-Won
 *
 */
public class CheckTasUserkCmd extends AbstractNbpmCommand {
	private NbpmProcessVO processVO = null;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}

	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public CheckTasUserkCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}

	public Object execute() {
		HashMap resultMap = new HashMap();
		String result = "";
		if(processVO.getTask_id() != 0) {
			Task task = taskClient.getTask(processVO.getTask_id());
			//타스크의 담당자 ID를 조회한다.
			result = task.getTaskData().getActualOwner().getId();
		}
		return result;
	}
}
