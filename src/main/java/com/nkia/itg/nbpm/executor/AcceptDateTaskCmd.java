/*
 * @(#)AcceptDateTaskCmd.java              2013. 6. 7.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.Date;

import org.jbpm.task.Task;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 대기함에서 타스크를 확인할 때 동작한다.
 * @author Kim Do-Won
 *
 */
public class AcceptDateTaskCmd extends AbstractNbpmCommand {
	private NbpmProcessVO processVO = null;
	
	public AcceptDateTaskCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}

	public Object execute() {
		//접수 일시를 업데이트 한다.
		Task task = taskClient.getTask(processVO.getTask_id());
		if(!(task.getTaskData().getAcceptTime() != null && !"".equals(task.getTaskData().getAcceptTime()))) {
			task.getTaskData().setAcceptTime(new Date());			
		}
		//readed 데이터를 Y로 업데이트 한다.
		if(!"Y".equals(task.getTaskData().getReaded())) {
			task.getTaskData().setReaded("Y");
		}
		return null;
	}
}
