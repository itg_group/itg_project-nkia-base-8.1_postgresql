package com.nkia.itg.nbpm.executor;

import org.jbpm.task.Status;
import org.jbpm.task.Task;
import org.jbpm.task.service.FaultData;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 진행중인 타스크들의 상태를 Exited로 변경한다.
 * @author Kim Do-Won
 *
 */
public class ExitTaskCmd extends AbstractNbpmCommand {
	private static final long serialVersionUID = 1L;
	private NbpmProcessVO processVO = null;

	public NbpmProcessVO getProcessVO() {
		return processVO;
	}

	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public ExitTaskCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;	
	}


	@Override
	public Object execute() {
		FaultData faultData = new FaultData();
		faultData.setFaultName("강제종료");
		Task t = taskClient.getTask(processVO.getTask_id());
		if(!(Status.Completed.equals(t.getTaskData().getStatus())|| Status.Exited.equals(t.getTaskData().getStatus()))) {
			//taskClient.exit(processVO.getTask_id(), "Administrator");
			loadedKsession.getWorkItemManager().abortWorkItem(t.getTaskData().getWorkItemId());			
		}
		return null;
	}
}
