/*
 * @(#)WorkRuleTaslCmd.java              2014. 4. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.drools.runtime.process.NodeInstance;
import org.drools.runtime.process.ProcessInstance;
import org.drools.runtime.rule.Activation;
import org.drools.runtime.rule.AgendaFilter;
import org.jbpm.ruleflow.instance.RuleFlowProcessInstance;
import org.jbpm.workflow.instance.node.RuleSetNodeInstance;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class WorkRuleTaskCmd extends AbstractNbpmCommand {
	
	private NbpmProcessVO processVO = null;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}

	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}

	public WorkRuleTaskCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}
	
	@Override
	public Object execute() {
		HashMap resultMap = new HashMap();
			
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		String processId = "";		
		if(processVO.getProcessId() != null) {
			processId = processVO.getProcessId();
		}
		long processInstanceOne = processVO.getProcessInstanceId();
		ProcessInstance processInstance = loadedKsession.getProcessInstance(processInstanceOne);
		
		//프로세스 인스턴스 ID를 통해 생성된 노드정보를 확인하고 노드가 룰타스크 노드일 경우에 만 룰을 실행한다.
		if(processInstance != null) {
			Collection nodeCollection = ((RuleFlowProcessInstance)processInstance).getNodeInstances();
			Iterator nodeIterator = nodeCollection.iterator();
			if(nodeCollection != null) {
				while(nodeIterator.hasNext()) {
					NodeInstance nodeInst = (NodeInstance)nodeIterator.next();
					if(nodeInst instanceof RuleSetNodeInstance) {
						AgendaFilter aFilter = new AgendaFilter() {
							int i = 1;
							String currentRuleName = "";
							@Override
							public boolean accept(Activation agendaItem) {
								String nextRuleName = agendaItem.getRule().getName();
								boolean result = false;
								if(!currentRuleName.equals(nextRuleName) ) {
									currentRuleName = agendaItem.getRule().getName();
									result = true;
								}
								return result;
							}
						};
						//룰 실행
						loadedKsession.fireAllRules(aFilter);
					}
				}
			}
		}
		
		return resultMap;
	}

}