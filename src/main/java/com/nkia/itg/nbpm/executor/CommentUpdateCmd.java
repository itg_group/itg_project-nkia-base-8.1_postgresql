package com.nkia.itg.nbpm.executor;

import java.util.Date;
import java.util.HashMap;

import org.jbpm.task.Comment;

import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
/**
 * 타스크의 의견 정보를 수정한다.(사용안함)
 * @author Kim Do-Won
 *
 */
public class CommentUpdateCmd extends AbstractNbpmCommand {

	private NbpmProcessVO processVO = null;
	private long commentId = 0;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}


	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public CommentUpdateCmd(NbpmProcessVO pVO, long commentId) {
		this.processVO = pVO;
		this.commentId = commentId;
	}

	public Object execute() {
		HashMap resultMap = new HashMap();
		if(commentId != 0) {
			taskClient.deleteComment(processVO.getTask_id(), commentId);				
		}
		
		Date commentDate = new Date();  
		Comment comment = new Comment();
		String commentText = processVO.getComment();
		if(processVO.getComment() != null && !"".equals(processVO.getComment())) {
			commentText = processVO.getComment();
			commentText = StringUtil.changeLineAlignmentForHtml(commentText);
		}
		comment.setAddedAt(commentDate);
		comment.setText(commentText);
		taskClient.addComment(processVO.getTask_id(), comment);
		
		return resultMap;
	}

}
