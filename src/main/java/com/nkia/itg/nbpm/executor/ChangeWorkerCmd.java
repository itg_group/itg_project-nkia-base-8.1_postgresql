/*
 * @(#)ChangeWorkerCmd.java              2013. 4. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.HashMap;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 담당자 변경을 위한 클래스
 * @author Kim Do-Won
 *
 */
public class ChangeWorkerCmd extends AbstractNbpmCommand {
	
	private NbpmProcessVO processVO = null;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}

	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public ChangeWorkerCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}

	public Object execute() {
		HashMap resultMap = new HashMap();
		//담당자를 변경하는 API이다. 종료된 요청에 사용할 시에 바꿀 수 없는 상태라는 에러가 표시 될 수 있다.
		taskClient.claim(processVO.getTask_id(), processVO.getWorker_user_id());
		
		return null;
	}

}
