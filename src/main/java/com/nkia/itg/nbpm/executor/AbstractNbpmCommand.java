package com.nkia.itg.nbpm.executor;

import java.io.Serializable;

import org.drools.runtime.StatefulKnowledgeSession;
import org.jbpm.task.service.local.LocalTaskService;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.util.NbpmThreadManager;


/**
 * 엔진의 API를 호출하는 클래스들은 이 클래스를 상속 받아 execute를 구현하여야 한다.
 * @author Kim Do-Wom
 *
 */
@SuppressWarnings("serial")
public abstract class AbstractNbpmCommand implements Serializable {
	
	protected StatefulKnowledgeSession loadedKsession = (StatefulKnowledgeSession)NbpmThreadManager.getTxMap().get("session");
	protected LocalTaskService taskClient = (LocalTaskService)NbpmThreadManager.getTxMap().get("taskClient");
	
	protected PostWorkDAO getNbpmProcessDAO() {
		PostWorkDAO nbpmProcessDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("nbpmProcessDAO");
		return nbpmProcessDAO;
	}
	
	public abstract Object execute();
  
}
