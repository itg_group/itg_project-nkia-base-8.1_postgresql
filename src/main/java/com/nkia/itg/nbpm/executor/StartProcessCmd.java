package com.nkia.itg.nbpm.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.drools.runtime.process.ProcessInstance;
import org.jbpm.task.Status;
import org.jbpm.task.Task;
import org.jbpm.task.query.TaskSummary;

import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 프로세스를 시작시킨다. 요청유형을 변경할 경우 사용된다.(메가센터에서만 사용됨)
 * @author Kim Do-Won
 *
 */
public class StartProcessCmd extends AbstractNbpmCommand {
	private static final long serialVersionUID = 1L;
	
	private NbpmProcessVO processVO = null;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}


	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public StartProcessCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}


	@Override
	public Object execute() {
		HashMap resultMap = new HashMap();
		long processInstanceId = 0;
		HashMap params = new HashMap();
		List<Status> status = new ArrayList<Status>();
		status.add(Status.Reserved);
		status.add(Status.Ready);
		status.add(Status.Created);
		
		params.put("loopCount", 0);
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		String user_id = "";
		if(processVO.getReq_user_id() != null) {
			user_id = processVO.getReq_user_id();
		}
		
		params.put(NbpmConstraints.REQUESTOR_ROLE, user_id);
		params.put(NbpmConstraints.SERVICEDESK_ROLE, NbpmConstraints.SERVICEDESK_ROLE);
		
		ProcessInstance processInstance = loadedKsession.startProcess(processVO.getProcessId(), params);
		processInstanceId = processInstance.getId();
		
		List<TaskSummary> startTasks = taskClient.getTasksByStatusByProcessId(processInstanceId, status,  "en-UK");
		long start_task_id = startTasks.get(0).getId();
		
		Task startTask = taskClient.getTask(start_task_id);
		startTask.getTaskData().setTask_name(NbpmConstraints.TASK_NAME_REQUEST);
		
		resultMap.put("taskId", start_task_id);
		resultMap.put("processInstanceId", processInstanceId);
	
		return resultMap;
	}
}