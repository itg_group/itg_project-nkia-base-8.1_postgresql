/*
 * @(#)AfterProcessCmd.java              2013. 3. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.drools.runtime.process.NodeInstance;
import org.drools.runtime.process.ProcessInstance;
import org.jbpm.ruleflow.instance.RuleFlowProcessInstance;
import org.jbpm.task.Status;
import org.jbpm.task.Task;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.workflow.core.node.HumanTaskNode;
import org.jbpm.workflow.instance.node.WorkItemNodeInstance;

import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 타스크 처리 후 후처리를 위한 클래스이다.
 * @author Kim Do-Won
 *
 */
public class AfterWorkCmd extends AbstractNbpmCommand {
	
	private NbpmProcessVO processVO = null;
	public List roleList = null;
	public List nodeList = null;
	
	public AfterWorkCmd(NbpmProcessVO pVO, List roleList, List nodeList) {
		this.processVO = pVO;
		if(roleList != null) {
			this.roleList = roleList;			
		} else {
			this.roleList = new ArrayList();
		}
		
		if(nodeList != null) {
			this.nodeList = nodeList;			
		} else {
			this.nodeList = new ArrayList();
		}
	}

	@Override
	public Object execute() {
		HashMap resultMap = new HashMap();
		List<HashMap<String, String>> operList = new ArrayList<HashMap<String, String>>();;
		
		long processInstanceOne = processVO.getProcessInstanceId();
		ProcessInstance processInstance = loadedKsession.getProcessInstance(processInstanceOne);
		
		ArrayList nodeTaskList = new ArrayList();
		ArrayList subProcessList = new ArrayList();
		
		List<Status> status = new ArrayList<Status>();
		status.add(Status.Reserved);
		status.add(Status.Ready);
		status.add(Status.Created);
		
		//프로세스 인스턴스 ID와 타스크의 상태 값을 조건으로 타스크를 조회한다.
		List<TaskSummary> tasks = taskClient.getTasksByStatusByProcessId(processInstanceOne, status,  "en-UK");
		RuleFlowProcessInstance ruleInstance = (RuleFlowProcessInstance)processInstance;
		
		//현재 생성된 Task의 Node정보를 수집한다.
		if(ruleInstance != null) {
			addNodeTaskInfo(processInstance, tasks, nodeTaskList, subProcessList, nodeList);
		}
		
		//수집한 Node 정보를 통해 Task에 NodeId 세팅 
		updateTasksNodeId(nodeTaskList);
		
		//아직 종료되지 않은 Task 정보를 결과 정보에 담아서 Return
		if(nodeTaskList.size() > 0) {
			ArrayList workerId = new ArrayList();
			ArrayList taskId = new ArrayList();
			ArrayList nodeTaskId = new ArrayList();
			ArrayList nodeNames = new ArrayList();
			for(int i = 0 ; i < nodeTaskList.size() ; i++) {
				Map nodeMap = (Map)nodeTaskList.get(i);
				for(int j = 0 ; j < tasks.size(); j++) {
					String nodeId = (String)nodeMap.get("nodeId");
					String nodeName = (String)nodeMap.get("nodeName");
					Task task = taskClient.getTask(tasks.get(j).getId());
					String taskInNodeId = task.getTaskData().getNodeId();
					if(nodeId.equals(taskInNodeId)) {
						if(task.getTaskData().getActualOwner() != null) {
							workerId.add(task.getTaskData().getActualOwner().getId());							
						} else {
							workerId.add("");
						}
						taskId.add(task.getId());
						nodeTaskId.add(nodeId);
						nodeNames.add(nodeName);
					}
				}
			}
			//종료되지 않은 타스크들의 담당자 정보
			resultMap.put(NbpmConstraints.NBPM_PARAM_WORKERIDS, workerId);
			//현재 요청의 상태 (ING = 진행 중)
			resultMap.put(NbpmConstraints.NBPM_PARAM_STATE, "ING");
			//프로세스 인스턴스 ID
			resultMap.put(NbpmConstraints.NBPM_PARAM_PROCID, processInstanceOne);
			//종료되지 않은 타스크들의 ID 정보
			resultMap.put(NbpmConstraints.NBPM_PARAM_NEXTTASKIDS, taskId);
			//종료되지 않은 타스크들의 NodeId정보 (현재 사용하지 않을 것으로 예상)
			resultMap.put(NbpmConstraints.NBPM_PARAM_NEXTNODEIDS, nodeTaskId);
			//종료되지 않은 타스크들의 Node의 한글명(업무명)
			resultMap.put(NbpmConstraints.NBPM_PARAM_NEXTNODENAMES, nodeNames);
			//서브프로세스 정보(현재 연계 프로세스의 매커니즘이 변경되어 이것은 사용하지 않는다.)
			resultMap.put(NbpmConstraints.NBPM_PARAM_SUBPROCESS, subProcessList);
		} else {
			//현재 요청의 상태 (END = 종료)
			resultMap.put("state", "END");
			//서브프로세스 정보(현재 연계 프로세스의 매커니즘이 변경되어 이것은 사용하지 않는다.)
			resultMap.put(NbpmConstraints.NBPM_PARAM_SUBPROCESS, subProcessList);
			//프로세스 인스턴스 ID
			resultMap.put(NbpmConstraints.NBPM_PARAM_PROCID, processInstanceOne);
		}
		return resultMap;
	}
	
	private void addNodeTaskInfo(ProcessInstance processInstance, List<TaskSummary> tasks
			, ArrayList nodeTaskList, ArrayList subProcessList, List nodeList) {
		//WorkTaskCmd에 이미 실행한 로직으로 필요성을 검토해보아야 함
		Collection nodeCollection = ((RuleFlowProcessInstance)processInstance).getNodeInstances();
		Iterator nodeIterator = nodeCollection.iterator();
		while(nodeIterator.hasNext()) {
			NodeInstance nodeInst = (NodeInstance)nodeIterator.next();
			if(nodeInst instanceof WorkItemNodeInstance) {
				for(int i = 0 ; i < tasks.size() ; i++) {
					HashMap nodeMap = new HashMap();
					long workItemId = ((WorkItemNodeInstance) nodeInst).getWorkItemId();
					Task task = taskClient.getTask(tasks.get(i).getId());
					if(workItemId == task.getTaskData().getWorkItemId()) {
						HumanTaskNode node = (HumanTaskNode)nodeInst.getNode();
						Map nodeMetaMap = node.getMetaData();
						String taskName = (String)node.getWork().getParameter("TaskName");
						String nodeId = (String)nodeMetaMap.get("UniqueId");

						String multiYn = null;
						
						//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
						if(nodeList != null) {
							if(nodeList.size() > 0) {
								for(int j = 0 ; j < nodeList.size() ; j++) {
									Map nodeDetailMap = (Map)nodeList.get(j);
									String nodeDetailNodeId = (String)nodeDetailMap.get("NODEID");
									if(nodeDetailNodeId.equals(nodeId)) {
										multiYn = (String)nodeDetailMap.get("MULTI_YN");
									}
								}
							}
						}
						
						nodeMap.put("multiYn", multiYn);
						nodeMap.put("taskId", tasks.get(i).getId());
						nodeMap.put("taskName", taskName);
						nodeMap.put("nodeId", nodeId);
						nodeMap.put("nodeName", node.getName());
						nodeMap.put("workItemId", workItemId);
						
						//String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
						String roleId = (String)node.getWork().getParameter("ActorId");
						roleId = roleId.replaceAll("#", "");
						roleId = roleId.replaceAll("[{]", "");
						roleId = roleId.replaceAll("[}]", "");
						
						nodeMap.put("roleId", roleId);
						nodeTaskList.add(nodeMap);
					}
				}
			}
		}
	}


	private void updateTasksNodeId(ArrayList nodeTaskList) {
		//타스크의 정보를 수정한다. 
		for(int i = 0 ; i < nodeTaskList.size() ; i++) {
			Map nodeTask = (Map)nodeTaskList.get(i);
			String roleId = (String)nodeTask.get("roleId");
			Task task = taskClient.getTask((Long)nodeTask.get("taskId"));
			task.getTaskData().setTask_name((String)nodeTask.get("taskName"));
			task.getTaskData().setNodeId(String.valueOf(nodeTask.get("nodeId")));
			
			//그룹 선택 여부에 대한 데이터처리이다.
			for(int j = 0 ; j < roleList.size() ; j++) {
				Map<String, String> roleInfo = (Map<String, String>)roleList.get(j);
				String grpSlctYn = roleInfo.get("GRP_SLCT_YN");
				String operUserRoleId= roleInfo.get("ROLE_ID");
				if(roleId.equals(operUserRoleId)) {
					if("Y".equals(grpSlctYn)) {
						task.getTaskData().setGrp_slct_yn("Y");	
					} else {
						task.getTaskData().setGrp_slct_yn("N");
					}
					break;
				} 
			}
			
			
		}	
	}

}
