package com.nkia.itg.nbpm.executor;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.jbpm.task.Comment;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 타스크의 의견 정보를 수정한다.(사용안함)
 * @author Kim Do-Won
 *
 */
public class TaskCommentUpdateCmd extends AbstractNbpmCommand {

	private NbpmProcessVO processVO = null;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}


	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public TaskCommentUpdateCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}

	public Object execute() {
		//타스크의 의견 정보를 업데이트한다.(삭제 후 재등록)
		HashMap resultMap = new HashMap();
		List<Comment> commentList = taskClient.getTask(processVO.getTask_id()).getTaskData().getComments();
		
		if(commentList.size() > 0) {
			Comment comment = commentList.get(0);
			taskClient.deleteComment(processVO.getTask_id(), comment.getId());				
		}
		
		Date commentDate = new Date();  
		Comment comment = new Comment();
		String commentText = processVO.getComment();
		if(processVO.getComment() != null && !"".equals(processVO.getComment())) {
			commentText = processVO.getComment();
		}
		comment.setAddedAt(commentDate);
		comment.setText(commentText);
		comment.setTask_name(processVO.getTask_name());
		taskClient.addComment(processVO.getTask_id(), comment);
			 
		return resultMap;
	}

}
