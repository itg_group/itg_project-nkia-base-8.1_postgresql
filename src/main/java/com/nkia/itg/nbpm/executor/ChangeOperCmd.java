/*
 * @(#)AcceptDateTaskCmd.java              2013. 6. 7.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.HashMap;

import org.jbpm.task.Task;
import org.jbpm.task.User;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 프로세스 담당자 or 담당자그룹 변경을 위한 클래스
 * @author Kim DaHee
 *
 */
public class ChangeOperCmd extends AbstractNbpmCommand {
	private NbpmProcessVO processVO = null;
	
	public ChangeOperCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}

	public Object execute() {
		HashMap resultMap = new HashMap();
		Task task = taskClient.getTask(processVO.getTask_id());
		
		// 그룹으로 변경시
		// 01. createdby_id 데이터를 그룹 ID로 업데이트
		if("Y".equals(processVO.getGrp_slct_yn())){
			task.getTaskData().setCreatedBy(new User(processVO.getWorker_user_id()));
		}
		
		// 그룹X(단일) -> 그룹으로 변경시
		// 02. grp_slct_yn 데이터를 Y로 업데이트  
		if("Y".equals(processVO.getGrp_slct_yn()) && (task.getTaskData().getGrp_slct_yn() == null || "N".equals(task.getTaskData().getGrp_slct_yn())) ) {
			task.getTaskData().setGrp_slct_yn("Y");
		}
		
		//담당자를 변경하는 API이다. 종료된 요청에 사용할 시에 바꿀 수 없는 상태라는 에러가 표시 될 수 있다.
		taskClient.claim(processVO.getTask_id(), processVO.getWorker_user_id());
		
		return null;
	}
}
