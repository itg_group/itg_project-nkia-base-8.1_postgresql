package com.nkia.itg.nbpm.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.task.Status;
import org.jbpm.task.Task;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.task.service.FaultData;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 지정된 타스크를 강제종료시킨다.(Exited 상태로 변경 - 정상 종료는 Completed)
 * @author Kim Do-Won
 *
 */
public class TerminationTasksCmd extends AbstractNbpmCommand {
	private static final long serialVersionUID = 1L;
	private static final Map<String, Boolean> FILTER = new HashMap<String, Boolean>();
	private static final List<Status> STATUS = new ArrayList<Status>();
	static {
		//예외 필터 객체 초기화
		FILTER.put("RESULT_CNFIRM", true);
		FILTER.put("TEST_CNFIRM", true);
		FILTER.put("SCRTY_CNFIRM", true);
		//Task 검색을 위한 Status 초기화
		STATUS.add(Status.Reserved);
		STATUS.add(Status.Ready);
		STATUS.add(Status.Created);			
	}
	private NbpmProcessVO processVO = null;

	public NbpmProcessVO getProcessVO() {
		return processVO;
	}

	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}


	public TerminationTasksCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;	
	}


	@Override
	public Object execute() {
 		exitedTask(taskClient.getTasksByStatusByProcessId(processVO.getProcessInstanceId(), STATUS,  "en-UK"));
		return null;
	}


	private void exitedTask(List<TaskSummary> tasks) {
		//남아있는 타스크들을 수집하고 타스크들을 종료시킨다.
		List<TaskSummary> exitedTarget = new ArrayList<TaskSummary>();
		for(int i = 0 ; i < tasks.size() ; i++) {
			TaskSummary task = tasks.get(i);
			if(!(FILTER.containsKey(task.getName()))) {
				exitedTarget.add(task);
			}
		}
		
		if(exitedTarget.size() > 0) {
			for(int i = 0 ; i < exitedTarget.size() ; i ++) {
				TaskSummary task = exitedTarget.get(i);
				FaultData faultData = new FaultData();
				faultData.setFaultName("강제종료");
				taskClient.exit(task.getId(), "Administrator");
				Task t = taskClient.getTask(task.getId());
				loadedKsession.getWorkItemManager().abortWorkItem(t.getTaskData().getWorkItemId());	
			}
			exitedTask(taskClient.getTasksByStatusByProcessId(processVO.getProcessInstanceId(), STATUS,  "en-UK"));
		}
	}
}
