package com.nkia.itg.nbpm.executor;

import java.util.HashMap;

import org.jbpm.task.Task;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 타스크를 완료시킨다.
 * @author Kim Do-Won
 *
 */
public class CompleteTaskCmd extends AbstractNbpmCommand {
	
	private NbpmProcessVO processVO = null;
	
	public CompleteTaskCmd(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}
	
	@Override
	public Object execute() {
		HashMap resultMap = new HashMap();
		taskClient.connect();
		taskClient.start(processVO.getTask_id(), "Administrator");
		taskClient.completeWithResults(processVO.getTask_id(), "Administrator", new HashMap());
		Task work_tasks = taskClient.getTask(processVO.getTask_id());
		loadedKsession.getWorkItemManager().completeWorkItem(work_tasks.getTaskData().getWorkItemId(), new HashMap());
		
		return resultMap;
	}

}
