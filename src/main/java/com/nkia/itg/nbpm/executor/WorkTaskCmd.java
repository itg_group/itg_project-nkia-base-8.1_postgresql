/*
 * @(#)WorkStartTaskCmd.java              2013. 3. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.executor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.drools.runtime.process.NodeInstance;
import org.drools.runtime.process.ProcessInstance;
import org.drools.runtime.process.WorkItem;
import org.jbpm.ruleflow.instance.RuleFlowProcessInstance;
import org.jbpm.task.Comment;
import org.jbpm.task.Status;
import org.jbpm.task.Task;
import org.jbpm.task.User;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.workflow.core.node.HumanTaskNode;
import org.jbpm.workflow.instance.node.WorkItemNodeInstance;

import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.common.NbpmRuntimeContainer;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.nbpm.vo.NbpmRuleVO;

//@@20130719 김도원 START
//1.사용하지 않는 변수 및 주석을 제거
//2.소스의 복잡도 및 가독성 문제로 리펙토링을 실시하였고 멀티프로세스 관련 로직을 추가함
//@@20130719 김도원 END
public class WorkTaskCmd extends AbstractNbpmCommand {
	
	public NbpmProcessVO processVO = null;
	public List roleList = null;
	
	public NbpmProcessVO getProcessVO() {
		return processVO;
	}


	public void setProcessVO(NbpmProcessVO processVO) {
		this.processVO = processVO;
	}

	public WorkTaskCmd(NbpmProcessVO pVO, List roleList) {
		this.processVO = pVO;
		if(roleList != null) {
			this.roleList = roleList;			
		} else {
			this.roleList = new ArrayList();
		}
	}
	
	@Override
	public Object execute() {
		HashMap resultMap = new HashMap();
		
		taskClient.connect();
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		String processId = "";
		if(processVO.getProcessId() != null) {
			processId = processVO.getProcessId();
		}
		long processInstanceOne = processVO.getProcessInstanceId();
		long task_id = processVO.getTask_id();
		String workerUserId = processVO.getWorker_user_id();
		ProcessInstance processInstance = null;
		HashMap processParam = new HashMap();
		
		//타스크 검색을 위해 검색조건이 되는  정보를 세팅한다.
		List<Status> status = new ArrayList<Status>();
		status.add(Status.Reserved);
		status.add(Status.Ready);
		status.add(Status.Created);
		status.add(Status.Exited);
		
		Map expansionParamMap = processVO.getExpansionParamMap();
		
		if(NbpmConstraints.TASK_NAME_START.equals(processVO.getTask_name())) {
			//요청 등록일 경우
			Map<String, Object> params = new HashMap<String, Object>();
			
			//등록되어 있는 담당자 정보를 통해 타스크에 전달할 담당자 파라메터 정보를 세팅한다.
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(roleList != null) {
				if(roleList.size() > 0) {
					for(int i = 0 ; i < roleList.size() ; i++) {
						Map role = (Map)roleList.get(i);
						params.put((String)role.get("ROLE_ID"), (String)role.get("OPER_USER_ID"));
					}
				}
			}			
			
			if(processVO.getReq_user_id() != null && !"".equals(processVO.getReq_user_id())) {
				workerUserId = processVO.getReq_user_id();					
			}
			
			//요청자 및 상담원 파라메터 정보를 세팅한다.
			params.put(NbpmConstraints.REQUESTOR_ROLE, workerUserId);
			params.put(NbpmConstraints.SERVICEDESK_ROLE, NbpmConstraints.SERVICEDESK_ROLE);
			
			processParam.put("RESULT_" + NbpmConstraints.REQUESTOR_ROLE, workerUserId);
			processParam.put("RESULT_" + NbpmConstraints.SERVICEDESK_ROLE, NbpmConstraints.SERVICEDESK_ROLE);
			
			//엔진 API를 사용하여 프로세스인스턴스를 생성
			processInstance = loadedKsession.createProcessInstance(processId, params);
			processInstanceOne = processInstance.getId();
			
			//엔진 API를 사용하여 프로세스 인스턴스를 시작 시킨다. -> 첫번째 타스크가 생성된다.
			processInstance = loadedKsession.startProcessInstance(processInstanceOne);
			processInstanceOne = processInstance.getId();
			
			//생성된 타스크를 조회한다.
			List<TaskSummary> startTasks = taskClient.getTasksByStatusByProcessId(processInstanceOne, status,  "en-UK");
			long start_task_id = startTasks.get(0).getId();
			
			//TASK 테이블의 NodeId 정보를 갱신한다. -> 현재 사용이 여부가 모호하다 삭제 가능한지 검토 필요함
			//프로세스 인스턴스의 활성화된 Node 정보를 불러온다
			/*
			 * Node와 Task는 성격이 다르다. Node는 프로세스 디자이너에서 그린 Task의 세부정보가 담겨있다. 디자이너에서 생성한 ID나 타스크의 한글명 등
			 * Task는 TASK 테이블의 DB 정보이다.
			 */
			Collection nodeCollection = ((RuleFlowProcessInstance)processInstance).getNodeInstances();
			Iterator nodeIterator = nodeCollection.iterator();
			while(nodeIterator.hasNext()) {
				//활성화 된 노드(타스크와 맵핑된다.) 정보를 조회하여 노드에서 가져온 추가정보를 업데이트 한다. 
				NodeInstance nodeInst = (NodeInstance)nodeIterator.next();
				if(nodeInst instanceof WorkItemNodeInstance) {
					HashMap nodeMap = new HashMap();
					HumanTaskNode node = (HumanTaskNode)nodeInst.getNode();
					Map nodeMetaMap = node.getMetaData();
					//Node에서 데이터를 가져와 기타 정보들을 세팅한다.
					String nodeId = (String)nodeMetaMap.get("UniqueId");
					String taskName = (String)node.getWork().getParameter("TaskName");
					Task task = taskClient.getTask(start_task_id);
					task.getTaskData().setNodeId(nodeId);
					task.getTaskData().setTask_name(taskName);
				}
			}
			
			task_id = start_task_id;
		} else {
			//등록이 아닐 경우에는 기존의 프로세스 인스턴스 정보를 로딩한다.
			processInstance = loadedKsession.getProcessInstance(processInstanceOne);
		}
		//화면에서 전달 받은 추가로 정의 된 파라메터들을 세팅
		setAllWorkItemParam(processInstance, processParam, expansionParamMap);
		
		//현재 타스크 정보를 조회
		Task currentTask = taskClient.getTask(task_id);
		
		//work_type이 반려, 반려종료일 경우 
		String workType= processVO.getWorkType();
		if(NbpmConstraints.WORKTYPE_RETURN_PROC.equals(workType) || NbpmConstraints.WORKTYPE_RETURN_END_PROC.equals(workType) ){
			currentTask.getTaskData().setRejected("Y");
		}
		
		//타스크의 담당자 정보를 조회
		String actualOwnerid = currentTask.getTaskData().getActualOwner().getId();
		//담당자가 servicedesk일 경우에는 데이터 추가 세팅(상담원 타스크는 디폴트 User가 servicedesk가 되며 접수 시 변경된다.
		if(NbpmConstraints.SERVICEDESK_ROLE.indexOf(actualOwnerid) > -1) {
			processParam.put("RESULT_" + NbpmConstraints.SERVICEDESK_ROLE, workerUserId);
		}
		//타스크를 시작시킨다.
		taskClient.start(task_id, workerUserId);
		//Task를 완료시키고 담당자 정보를 세팅한다.
		processParam.put("RESULT_WORKTYPE", processVO.getWorkType());
		setOperUserProcessParamInfo(processParam);
		
		NbpmRuleVO ruleVO = new NbpmRuleVO();
		ruleVO.setProcessInstanceId(processInstanceOne);
		ruleVO.setProcess_type(processVO.getProcess_type());
		loadedKsession.insert(ruleVO);
		
		//화면에서 넘어온 의견 정보를 등록한다.
		Comment comment = createComment(workerUserId);
		taskClient.addComment(task_id, comment);
		
		//타스크를 완료 시킨다.
		taskClient.completeWithResults(task_id, workerUserId, processParam);
		
		Task work_tasks = taskClient.getTask(task_id);
		//워크 아이템을 완료 시킨다.
		loadedKsession.getWorkItemManager().completeWorkItem(work_tasks.getTaskData().getWorkItemId(), processParam);
		resultMap.put(NbpmConstraints.NBPM_PARAM_PROCID, processInstanceOne);
		
		return resultMap;
	}


	private void setOperUserProcessParamInfo(Map processParam) {
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		String multiYn = "";
		if(processVO.getMulti_yn() != null) {
			multiYn = processVO.getMulti_yn();
		}
		
		String processTaskGroupId = processVO.getTask_group_id();

		if(roleList != null) {
			if(roleList.size() > 0) {
				for(int i = 0 ; i < roleList.size() ; i++) {
					Map role = (Map)roleList.get(i);
					HashMap operUserMap = new HashMap();
					String operType = (String)role.get("ROLE_ID");
					String operUserId = (String)role.get("OPER_USER_ID");
					
					if("Y".equals(multiYn)) {
						Long taskGroupId = (Long)role.get("TASK_GROUP_ID");
						String taskGroupIdStr = String.valueOf(taskGroupId);
						if(processTaskGroupId.equals(taskGroupIdStr)) {
							if(!"".equals(operUserId) && operUserId != null && !"null".equals(operUserId)) {
								processParam.put("RESULT_" + operType, operUserId);											
							}
						}
					} else {
						if(!"".equals(operUserId) && operUserId != null && !"null".equals(operUserId)) {
							processParam.put("RESULT_" + operType, operUserId);											
						}
					}
				}	
			}
		}
				
	}


	private Comment createComment(String workerId) {
		Comment comment = new Comment();
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		String commentText = "";		
		if(processVO.getComment() != null) {
			commentText = processVO.getComment();
		}
		String taskName = "";
		if(processVO.getComment() != null && !"".equals(processVO.getComment())) {
			commentText = processVO.getComment();
			//commentText = StringUtil.changeLineAlignmentForHtml(commentText);
			taskName = this.processVO.getTask_name();
		} else {
			if("start".equals(processVO.getTask_name())) {
				commentText = NbpmRuntimeContainer.getResources("res.label.nbpm.sdesk.00011");
				taskName = NbpmConstraints.REQUESTOR_ROLE;
			}
		}
		comment.setText(commentText);
		
		Date commentDate = new Date();  
		comment.setAddedAt(commentDate);
		comment.setAddedBy(new User("SYSTEM"));
		comment.setTask_name(taskName);
		return comment;
	}


	private void setAllWorkItemParam(ProcessInstance processInstance, Map processParam, Map expantionMap) {		
		if(processInstance != null) {
			Collection nodeCollection = ((RuleFlowProcessInstance)processInstance).getNodeInstances();
			Iterator nodeIterator = nodeCollection.iterator();
			while(nodeIterator.hasNext()) {
				NodeInstance nodeInst = (NodeInstance)nodeIterator.next();
				if(nodeInst instanceof WorkItemNodeInstance) {	
					WorkItem workItem = ((WorkItemNodeInstance) nodeInst).getWorkItem();
					
					setExpantionParam(nodeInst, expantionMap, processParam);
					
					setWorkItemParam(workItem, processParam);
				}
			}
		}
	}


	private void setWorkItemParam(WorkItem workItem, Map processParam) {
		Map workItemParam = workItem.getParameters();
		Iterator workItemParamKeySet = workItemParam.keySet().iterator();
		while(workItemParamKeySet.hasNext()) {
			String key = (String)workItemParamKeySet.next();
			String value = (String)workItemParam.get(key);
			if(value != null && !"".equals(value)) {
				processParam.put(key, value);
			}
		}
	}


	private void setExpantionParam(NodeInstance nodeInst, Map expantionMap, Map processParam) {
		if(expantionMap != null) {
			Iterator expantionMapKeySet =  expantionMap.keySet().iterator();
			while(expantionMapKeySet.hasNext()) {
				String key = (String)expantionMapKeySet.next();
				//화면 입력 값
				String value = (String)expantionMap.get(key);
				//프로세스 런타임에 저장되어 있는 값 
				String refValue = (String)((WorkItemNodeInstance) nodeInst).getVariable(key);
				if(value != null && !"".equals(value)) {
					processParam.put("RESULT_" + key, value);
				} else {
					processParam.put("RESULT_" + key, refValue);
				}
			}					
		}
	}
}
