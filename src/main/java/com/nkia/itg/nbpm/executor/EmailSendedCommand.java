package com.nkia.itg.nbpm.executor;

import org.jbpm.task.Task;

import com.nkia.itg.nbpm.vo.NbpmProcessVO;

/**
 * 타스크의 메일 발송 여부를 Y로 업데이트한다.
 * @author Kim Do-Won
 *
 */
public class EmailSendedCommand extends AbstractNbpmCommand {
	private NbpmProcessVO processVO = null;
	
	public EmailSendedCommand(NbpmProcessVO pVO) {
		this.processVO = pVO;
	}

	public Object execute() {
		Task task = taskClient.getTask(processVO.getTask_id());
		//task.getTaskData().setEmailSended("Y");
		task.getTaskData().setEmailSended(processVO.getEmailSended());
		return null;
	}
}
