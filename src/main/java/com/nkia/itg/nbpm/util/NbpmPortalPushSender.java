package com.nkia.itg.nbpm.util;

import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsPortalBean;
import com.nkia.itg.interfaces.client.WsPortalClient;

public class NbpmPortalPushSender implements Runnable {

	public Map processData = null;
	private String workerId = null;
	private String taskName = null;
	
	public NbpmPortalPushSender(Map processDetail, String workerId, String taskName) {
		this.processData = processDetail;
		this.workerId = workerId;
		this.taskName = taskName;
	}
	
	public void sendPush() throws NkiaException {
		String sr_id = (String)processData.get("SR_ID");
		
		WsPortalClient wcpe = new WsPortalClient(this.taskName);
		WsPortalBean wsPortalBean = new WsPortalBean();

		wsPortalBean.setSr_id(sr_id);
		wsPortalBean.setWorker_id(workerId);

		wcpe.sendData(wsPortalBean);
		wcpe.getResultMsg();
	}
	
	public void run() {	
		try {
			sendPush();
		} catch (Exception e) {
			new NkiaException(e);
		}	
	}
}
