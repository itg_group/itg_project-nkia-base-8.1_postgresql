package com.nkia.itg.nbpm.util;

import java.util.Map;


public class NbpmThreadManager {
	
	private static ThreadLocal<Map> threadLocal = new ThreadLocal<Map>();
	
	
	public static void setTxMap(Map nsm) {
		if(threadLocal.get() == null) {
			threadLocal.set(nsm);
		}
	}
	
	public static Map getTxMap() {
		return threadLocal.get();
	}
	
	public static void deleteSessionManager() {
		threadLocal.remove();
	}
}
