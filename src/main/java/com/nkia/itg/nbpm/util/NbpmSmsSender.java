package com.nkia.itg.nbpm.util;

import java.util.HashMap;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.SmsSendService;

public class NbpmSmsSender implements Runnable {

	public Map processData = null;
	public Map smsInfo = null;
	
	public NbpmSmsSender(Map processDetail, Map smsInfo) {
		this.processData = processDetail;
		this.smsInfo = smsInfo;
	}

	private Map setSmsInfo() {
		Map returnMap = new HashMap();
		returnMap.putAll(smsInfo);
		return returnMap;
	}	
	
	private Map setProcessDetail() {
		Map returnMap = new HashMap();
		returnMap.putAll(processData);
		return returnMap;
	}	
	
	public void sendSms() throws NkiaException {

		SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
		Map sendInfo = setSmsInfo();
		Map procInfo = setProcessDetail();
		/*
		 * 프로시저를 통해서 등록하는 경우
		 */
		smsSendService.insertTalkList(sendInfo);
		
		/*
		 * table에 등록 하는 경우 
		 */
		//smsSendService.insertSmsSend(sendInfo);
		
		/*
		 * 전송 이력을 남길경우 
		 */
		//smsSendService.insertSmsgHist(sendInfo);

	}
	
	public void run() {	
		try {
			sendSms();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}

}
