package com.nkia.itg.nbpm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.nkia.itg.base.application.exception.NkiaException;
 
public class DeployXMLPaser {
	
	public static HashMap build(File xml) throws NkiaException, JDOMException, IOException {
		HashMap resultMap = new HashMap();
		ArrayList taskList = new ArrayList();
		ArrayList propertyList = new ArrayList();
		//@@20130719 김도원 START
		//서브프로세스 정보를 담을 List
		ArrayList calledElementList = new ArrayList();
		//@@20130719 김도원 END
		
		SAXBuilder sax = new SAXBuilder();
		//sax.setFeature("http://apache.org/xml/features/disallow-doctype-decl",true); //취약성점검추가 20210111
		FileInputStream fis = new FileInputStream(xml);
		InputStreamReader is = new InputStreamReader(fis, "EUC-KR"); 
		Document getDoc = sax.build(is);
		Namespace ns = Namespace.getNamespace("http://www.omg.org/spec/BPMN/20100524/MODEL");
		Element rootNode = getDoc.getRootElement();
		Element processNode = rootNode.getChild("process", ns);
		
		List properties = processNode.getChildren("property", ns);
		for(int i = 0 ; i < properties.size() ; i++) {
			Element property = (Element) properties.get(i);
			String propertyId = property.getAttributeValue("id");
			propertyList.add(propertyId);
		}
		
		Namespace droolsNs = Namespace.getNamespace("http://www.jboss.org/drools");
		
		//@@20130719 김도원 START
		//서브프로세스 정보를 분석한다.
		List callActivityList = processNode.getChildren("callActivity", ns);
		for(int i = 0 ; i < callActivityList.size() ; i++) {
			Element callActivity = processNode.getChild("callActivity", ns);
			HashMap callActivityMap = new HashMap();
			String calledElement = callActivity.getAttributeValue("calledElement");
			calledElementList.add(calledElement);
		}
		//@@20130719 김도원 END
		
		//시작노드를 먼저 담는다.
		List startList = processNode.getChildren("startEvent", ns);
		for(int i = 0 ; i < startList.size() ; i++) {
			Element startNode = (Element)startList.get(i);
			HashMap startNodeMap = new HashMap();
			String startNodeId = startNode.getAttributeValue("id");
			String startNodeName = "START";
			String taskName = startNode.getAttributeValue("taskName", droolsNs);
			startNodeMap.put("nodeId", startNodeId);
			startNodeMap.put("nodeName", startNodeName);
			startNodeMap.put("taskName", startNodeName);
			taskList.add(startNodeMap);
		}
		
		
		//프로세스노드를 담는다.
		List userTaskList = processNode.getChildren("userTask", ns);
		for(int i = 0 ; i < userTaskList.size() ; i++) {
			HashMap nodeMap = new HashMap();
			Element userTask = (Element) userTaskList.get(i);
			String nodeId = userTask.getAttributeValue("id");
			String nodeName = userTask.getAttributeValue("name");
			String taskName = userTask.getAttributeValue("taskName", droolsNs);
			String multiYn = userTask.getAttributeValue("multiyn", droolsNs);
			String email = userTask.getAttributeValue("email", droolsNs);
			String sms = userTask.getAttributeValue("sms", droolsNs);
			//nodeId = nodeId.replaceAll("_", "");
			nodeMap.put("nodeId", nodeId);
			nodeMap.put("nodeName", nodeName);
			nodeMap.put("taskName", taskName);
			if(multiYn != null && !"".equals(multiYn)) {
				nodeMap.put("multiYn", multiYn);				
			} else {
				nodeMap.put("multiYn", "N");
			}
			nodeMap.put("email", email);
			nodeMap.put("sms", sms);
			
			Element potentialOwner = userTask.getChild("potentialOwner", ns);
			Element resourceAssignmentExpression = potentialOwner.getChild("resourceAssignmentExpression", ns);
			Element formalExpression = resourceAssignmentExpression.getChild("formalExpression", ns);
			String role_id = formalExpression.getValue();
			
			role_id = role_id.replaceAll("#", "");
			role_id = role_id.replaceAll("[{]", "");
			role_id = role_id.replaceAll("[}]", "");
			
			nodeMap.put("role_id", role_id);
			taskList.add(nodeMap);
		}
		
		//종료노드를 담는다.
		List endList = processNode.getChildren("endEvent", ns);
		for(int i = 0 ; i < endList.size() ; i++) {
			HashMap endNodeMap= new HashMap();
			Element endNode = (Element)endList.get(i);
			String endNodeId = endNode.getAttributeValue("id");
			String endNodeName = endNode.getAttributeValue("name");
			String taskName = endNode.getAttributeValue("taskName", droolsNs);
			
			//@@20130726 김도원 START
			//종료 노드를 처리할때 프로세스 강제종료 노드를 따로 구별하기 위한 기능추가
			List terminateEventList = endNode.getChildren("terminateEventDefinition", ns);
			if(terminateEventList != null && terminateEventList.size() > 0) {
				endNodeMap.put("taskName", "TEND");				
			} else {
				endNodeMap.put("taskName", "END");
			}
			//endNodeId = endNodeId.replaceAll("_", "");
			endNodeMap.put("nodeId", endNodeId);
			endNodeMap.put("nodeName", "END");
			//@@20130726 김도원 END
			taskList.add(endNodeMap);
		}
		
		fis.close();
		is.close();
		
		//@@20130719 김도원 START
		//서브프로세스 정보를 담아 결과에 담는다.
		resultMap.put("calledElementList", calledElementList);
		//@@20130719 김도원 START
		resultMap.put("propertyList", propertyList);
		resultMap.put("taskList", taskList);
		
		return resultMap;
	}
	
	/*
	public static class MapConverter extends AbstractCollectionConverter implements Converter {
		public MapConverter(Mapper mapper) {
			super(mapper);
		}
		
		public boolean canConvert(Class type) {
			return type.equals(java.util.HashMap.class) || type.equals(java.util.Hashtable.class)  || (type.getName().equals("java.util.LinkedHashMap") || type.getName().equals("sun.font.AttributeMap"));
		}
		
		public void marshal(Object value, HierarchicalStreamWriter writer , MarshallingContext context) {
			AbstractMap map = (AbstractMap) value;
			for (Object obj : map.entrySet()) {
				Entry ety = (Entry) obj;
				writer.startNode(ety.getKey().toString());
				writer.setValue(ety.getValue().toString());
				writer.endNode();
			}
			
		}
		
		public Object unmarshal(HierarchicalStreamReader reader , UnmarshallingContext arg1) {
			Map current = new HashMap();
			Map elements = new HashMap();
			try {
				for (; reader.hasMoreChildren(); reader.moveUp()) {
					reader.moveDown();
					
					if (reader.hasMoreChildren() == true) {
						//재귀호출
						elements.put(reader.getNodeName(), unmarshal(reader, arg1));
					} else {
						String tempValue = reader.getValue();
						String tempKey = reader.getNodeName();
						if (tempValue != null) {
							tempValue = tempValue.trim().replaceAll("\n", "");
						}
						
						Object gettingValue = elements.get(tempKey);
						if(gettingValue != null) {
							if(gettingValue instanceof List) {
								((List) gettingValue).add(tempValue);
								elements.put(tempKey,gettingValue);
							}
							else if(gettingValue instanceof String) {
								List tempList = new ArrayList();
								tempList.add(gettingValue);
								tempList.add(tempValue);
								elements.put(tempKey,tempList);
							}
						} else {
							
							Iterator attr = reader.getAttributeNames();
							while(attr.hasNext()) {
								//System.out.println(attr.next());
								String attrName = (String) attr.next();
								System.out.println(attrName);
								System.out.println(reader.getAttribute(attrName));
								elements.put(attrName, reader.getAttribute(attrName)); 
								//System.out.println("++++   " + attr.next());
							}
							elements.put(reader.getNodeName(), tempValue);
							System.out.println();
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return elements;
		}
	}
	*/
	
}