/*
 * @(#)NbpmUtil.java              2013. 3. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class NbpmUtil {

	private final static String PAGE_FILE_EXTENSION = ".nvf";
	
	public static String parseForwardPage(String processType,
			String subReqType, String version, String taskName)
			throws NkiaException {
		
		String taskFileName = new String();
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(taskName.toUpperCase() != null) taskFileName = taskName.toUpperCase();
		
		StringBuilder sb = new StringBuilder();
		sb.append(NbpmConstraints.NBPM_PATH + "/");
		sb.append(NkiaApplicationPropertiesMap.getProperty("Nbpm.itsm.site") + "/");
		sb.append(processType);
		if (subReqType != null && subReqType.length() > 0) {
			sb.append("_" + subReqType);
		}
		sb.append("-" + version + "/");
		sb.append(taskFileName + PAGE_FILE_EXTENSION);
		return sb.toString();
	}
	
	public static String parseForwardPage(String processType, String version,
			String taskName) throws NkiaException {
		return parseForwardPage(processType, null, version, taskName);
	}

	public static NbpmProcessVO setProcessParameter(Map<String, Object> paramMap) {
		String sr_id = (String)paramMap.get("sr_id");
		String processId = (String)paramMap.get("nbpm_processId");
		String processInstanceId = (String)paramMap.get("nbpm_processInstanceId");
		String task_id = (String)paramMap.get("nbpm_task_id");
		String task_name = (String)paramMap.get("nbpm_task_name");
		String process_nm = (String)paramMap.get("nbpm_process_nm");
		String process_type = (String)paramMap.get("nbpm_process_type");
		List operList = (List)paramMap.get("nbpm_operList");
		List itemList = (List)paramMap.get("nbpm_itemList");
		String version = (String)paramMap.get("nbpm_version");
		String comment = (String)paramMap.get("nbpm_comment");
		String workType = (String)paramMap.get("workType");
		String reqUesrId = (String)paramMap.get("req_user_id");
		String workerUserId = (String)paramMap.get("workerUserId");
		List gridMapList = (List)paramMap.get("gridMapList");
		List gridFieldList = (List)paramMap.get("gridFieldList");
		
		//멀티티켓을 위한 데이터로 멀티티켓이 아닌경우 세팅할 필요없음
		String multiYn = (String)paramMap.get("multi_yn");
		String taskGroupId = (String)paramMap.get("task_group_id");
		
		NbpmProcessVO processVO = new NbpmProcessVO();
		
		if(processInstanceId != null && !"".equals(processInstanceId)) {
			processVO.setProcessInstanceId(Long.parseLong(processInstanceId));
		}
		
		if(task_id != null && !"".equals(task_id)) {
			processVO.setTask_id(Long.parseLong(task_id));
		}
		
		//processVO.setRole_id(role_id);
		processVO.setSr_id(sr_id);
		processVO.setProcessId(processId);
		processVO.setTask_name(task_name);
		processVO.setOperList(operList);
		processVO.setWorker_user_id(workerUserId);
		processVO.setItemList(itemList);
		processVO.setProcess_name(process_nm);
		processVO.setProcess_type(process_type);
		processVO.setVersion(version);
		processVO.setWorkType(workType);
		processVO.setComment(comment);
		processVO.setReq_user_id(reqUesrId);
		processVO.setGridMapList(gridMapList);
		processVO.setGridFieldList(gridFieldList);
		processVO.setMulti_yn(multiYn);
		processVO.setTask_group_id(taskGroupId);
		
		return processVO;
	}

	public static String getCommentHistory(List taskCommentList) {
		StringBuffer commentHistory = new StringBuffer();
		if(taskCommentList != null) {
			for(int i = 0 ; i < taskCommentList.size() ; i++) {
				HashMap taksComment = (HashMap)taskCommentList.get(i);
				String workUserNm = (String)taksComment.get("WORK_USER_NM");
				String workDate = (String)taksComment.get("WORK_DATE");
				String nodeName = (String)taksComment.get("NODENAME");
				String text = (String)taksComment.get("TEXT");
			}
		}
		return commentHistory.toString();
	}

	public static NbpmProcessVO setRegisterForData(NbpmProcessVO processVO, String reqUserId, String loginUserId) {
		processVO.setReq_user_id(reqUserId);
		processVO.setWorker_user_id(loginUserId);
		processVO.setTask_name("start");
		processVO.setWorkType("comp");
		
		List operList = new ArrayList();
		LinkedHashMap requestor = new LinkedHashMap();
		requestor.put("oper_type", NbpmConstraints.SERVICEDESK_ROLE);
		requestor.put("oper_user_id", loginUserId);
		operList.add(requestor);
		
		processVO.setOperList(operList);
		return processVO;
	}

	
	public static HashMap<String, Object> getDataMapForInsertPage(
			NbpmProcessVO processVO, UserVO loginUserInfo, String relationPop, String up_sr_id) {
		
		HashMap<String, Object> dataMap = getDataMapForTaskPage(processVO, loginUserInfo);
		dataMap.put("nbpm_node_name", "요청");
		dataMap.put("nbpm_task_name", NbpmConstraints.TASK_NAME_START);
		dataMap.put("relationPop", Boolean.parseBoolean(relationPop));
		
		if(up_sr_id != null) {
			dataMap.put("up_sr_id", up_sr_id);
			dataMap.put("req_user_id", loginUserInfo.getUser_id());
			dataMap.put("req_user_nm", loginUserInfo.getUser_nm());
/*			//UBIT sub_req_type 추가
			dataMap.put("sub_req_type", processVO.getSub_req_type());*/
		}
		
		return dataMap;
	}
	
	public static HashMap<String, Object> getDataMapForTaskPage(
			NbpmProcessVO processVO, UserVO loginUserInfo) {
		
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("nbpm_user_id", loginUserInfo.getUser_id());
		dataMap.put("nbpm_user_nm", loginUserInfo.getUser_nm());
		if(processVO.getNodename() != null) dataMap.put("nbpm_node_name", processVO.getNodename());
		dataMap.put("nbpm_task_name", processVO.getTask_name());
		dataMap.put("nbpm_task_id", processVO.getTask_id());
		dataMap.put("nbpm_process_type", processVO.getProcess_type());
		dataMap.put("nbpm_processId", processVO.getProcessId());
		dataMap.put("nbpm_processInstanceId", processVO.getProcessInstanceId());
		dataMap.put("nbpm_version", processVO.getVersion());
		dataMap.put("req_type", processVO.getProcess_type());
		dataMap.put("sr_id", processVO.getSr_id());
		dataMap.put("sub_req_type", processVO.getSub_req_type());
		dataMap.put("multi_yn", processVO.getMulti_yn());
		dataMap.put("task_group_id", processVO.getTask_group_id());
		dataMap.put("sanctn_line_slct_yn", processVO.getSanctn_line_slct_yn());
		dataMap.put("faq_yn", processVO.getFaq_yn());
		dataMap.put("process_statis_yn", processVO.getProcess_statis_yn());
		dataMap.put("current_worker_id", processVO.getCurrent_worker_id());
		
		return dataMap;
	}
}
