package com.nkia.itg.nbpm.edit.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("deleteDAO")
public class DeleteDAO extends NbpmAbstractDAO {
	public int searchDeleteListCount(Map paramMap) {
		return (Integer)selectByPk("DeleteDAO.searchDeleteListCount", paramMap);
	}

	public List searchDeleteList(Map paramMap) {
		return list("DeleteDAO.searchDeleteList", paramMap);
	}

	public Map selectDeleteDetail(Map paramMap) throws SQLException {
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("DeleteDAO.selectDeleteDetail", paramMap, handler);
		return handler.getReturnMap();
	}
}
