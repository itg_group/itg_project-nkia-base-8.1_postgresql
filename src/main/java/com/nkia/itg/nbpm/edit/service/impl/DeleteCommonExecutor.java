package com.nkia.itg.nbpm.edit.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.edit.dao.DeleteDAO;
import com.nkia.itg.nbpm.edit.service.DeleteExecutor;

@Service("deleteCommonExecutor")
public class DeleteCommonExecutor implements DeleteExecutor {

	@Resource(name = "deleteDAO")
	private DeleteDAO deleteDAO;
	
	@Override
	public int searchDeleteListCount(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return deleteDAO.searchDeleteListCount(paramMap);
	}

	@Override
	public List searchDeleteList(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return deleteDAO.searchDeleteList(paramMap);
	}

	@Override
	public Map selectDeleteDetail(Map paramMap) throws NkiaException, SQLException {
		// TODO Auto-generated method stub
		return deleteDAO.selectDeleteDetail(paramMap);
	}

}
