package com.nkia.itg.nbpm.edit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.edit.dao.ChargerBndeChangeDAO;
import com.nkia.itg.nbpm.edit.service.ChargerBndeChangeService;

/*
 * 1. 변수명 _를 사용하지 않도록 통일
 * 2. 상수는 private로 선언
 * 3. 상수를 삭제하고 isApply 파라메터를 받아 분기 처리 하도록 수정 가능 할듯
 * 4. 사용하지 않는 변수 및 로직은 삭제
 * 5. Exception 처리 확인
 */


@Service("chargerBndeChangeService")
public class ChargerBndeChangeServiceImpl implements ChargerBndeChangeService{

	private static final Logger logger = LoggerFactory.getLogger(ChargerBndeChangeServiceImpl.class);
	
	@Resource(name = "chargerBndeChangeDAO")
	public ChargerBndeChangeDAO chargerBndeChangeDAO;
	
	@Resource(name = "nbpmCommonDAO")
	public NbpmCommonDAO nbpmCommonDAO;

	@Override
	public int searchChargerBndeChangeListCount(Map paramMap) throws NkiaException {
		return chargerBndeChangeDAO.searchChargerBndeChangeListCount(paramMap);
	}

	@Override
	public List searchChargerBndeChangeList(Map paramMap) throws NkiaException {
		return chargerBndeChangeDAO.searchChargerBndeChangeList(paramMap); 
	}

	@Override
	public int searchChargerBndeChangeMapngChargerListCount(Map paramMap) throws NkiaException {
		return chargerBndeChangeDAO.searchChargerBndeChangeMapngChargerListCount(paramMap);
	}

	@Override
	public List searchChargerBndeChangeMapngChargerList(Map paramMap) throws NkiaException {
		return chargerBndeChangeDAO.searchChargerBndeChangeMapngChargerList(paramMap); 
	}
	
	@Override
	public void registChargerBndeChange(Map paramMap) throws NkiaException {
		String bndeChangeStat = (String)paramMap.get("bnde_change_stat");
		String updUserId = (String)paramMap.get("upd_user_id");
		boolean isApply = (Boolean) paramMap.get("isApply");
		
		String bnde_change_id = chargerBndeChangeDAO.selectBndeChangeId();
		paramMap.put("bnde_change_id", bnde_change_id);
		chargerBndeChangeDAO.insertUserBatchEdit(paramMap);
		
		insertChargerBndeChangeMapng(updUserId, paramMap);
		
		if( isApply ){
			insertChargerBndeChange(updUserId, paramMap);
		}
	}
	
	@Override
	public void updateChargerBndeChange(Map paramMap) throws NkiaException {
		String bndeChangeStat = (String)paramMap.get("bnde_change_stat");
		String updUserId = (String)paramMap.get("upd_user_id");
		boolean isApply = (Boolean) paramMap.get("isApply");
		
		chargerBndeChangeDAO.updateUserBatchEdit(paramMap);
		chargerBndeChangeDAO.deleteUserBatchEditMapngUser(paramMap);
			
		insertChargerBndeChangeMapng(updUserId, paramMap);
		
		if( isApply ){
			insertChargerBndeChange(updUserId, paramMap);
		}
	}
	
	public void insertChargerBndeChangeMapng(String updUserId, Map paramMap){
		List users = (List)paramMap.get("userList");
		final int LENGTH = users.size();
		
		for(int i=0; i < LENGTH; i++){
			Map insertRowMap = new HashMap();
			Map tmpMap = (HashMap)users.get(i);
			
			insertRowMap.put("bnde_change_id",  paramMap.get("bnde_change_id") );
			insertRowMap.put("before_charger_id", tmpMap.get("before_charger_id") );
			insertRowMap.put("after_charger_id", tmpMap.get("after_charger_id") );
			insertRowMap.put("upd_user_id", updUserId );
			
			chargerBndeChangeDAO.insertUserBatchEditMapngUser(insertRowMap);
		}
	}
	
	public void insertChargerBndeChange(String updUserId, Map paramMap) throws NkiaException{
		String bndeChangeId = (String)paramMap.get("bnde_change_id");
		List users = (List)paramMap.get("userList");
		final int LENGTH = users.size();
		
		for(int i=0; i < LENGTH; i++){
			Map userMap = new HashMap();
			Map tmpMap = (HashMap)users.get(i);
			String beforeChargerId = (String)tmpMap.get("before_charger_id");
			String afterChargerId = (String)tmpMap.get("after_charger_id");
			userMap.put("before_charger_id", beforeChargerId );
			
			List chargerReqList = chargerBndeChangeDAO.searchChargerReqList(userMap);
			int chargerSize = chargerReqList.size();
			if(chargerSize > 0){
				// 담당자 변경 처리DAO
				for(int j=0; j < chargerSize; j++){
					HashMap updateMap = new HashMap();
					Map listMap = (HashMap)chargerReqList.get(j);
					updateMap.put("before_charger_id", beforeChargerId);
					updateMap.put("after_charger_id", afterChargerId);
					updateMap.put("sr_id", listMap.get("SR_ID"));
					updateMap.put("task_id", listMap.get("TASK_ID"));
					updateMap.put("task_name", listMap.get("TASK_NAME"));
					updateMap.put("role_id", listMap.get("ROLE_ID"));
					updateMap.put("bnde_change_id", bndeChangeId);
					updateMap.put("upd_user_id", updUserId);
					// nbpmCommonDAO 용 담당자명
					updateMap.put("actualowner_id", beforeChargerId);
					updateMap.put("user_id", afterChargerId);
					
					nbpmCommonDAO.updateClaimTask(updateMap);
					nbpmCommonDAO.updateClaimProcOper(updateMap);
					
					chargerBndeChangeDAO.insertChargerEditListHistory(updateMap);
				}
			}
			List chargerOperOnlyList = chargerBndeChangeDAO.searchOperOnlyList(userMap);
			int chargerOperOnlySize = chargerOperOnlyList.size();
			if(chargerOperOnlySize > 0){
				// 담당자 변경 처리DAO
				for(int j=0; j < chargerOperOnlySize; j++){
					HashMap updateMap = new HashMap();
					Map listMap = (HashMap)chargerOperOnlyList.get(j);
					updateMap.put("before_charger_id", beforeChargerId);
					updateMap.put("after_charger_id", afterChargerId);
					updateMap.put("sr_id", listMap.get("SR_ID"));
					updateMap.put("task_id", listMap.get("TASK_ID"));
					updateMap.put("task_name", listMap.get("TASK_NAME"));
					updateMap.put("role_id", listMap.get("ROLE_ID"));
					updateMap.put("bnde_change_id", bndeChangeId);
					updateMap.put("upd_user_id", updUserId);
					// nbpmCommonDAO 용 담당자명
					updateMap.put("actualowner_id", beforeChargerId);
					updateMap.put("user_id", afterChargerId);

					nbpmCommonDAO.updateClaimProcOperAll(updateMap);
					
					chargerBndeChangeDAO.insertChargerEditListHistory(updateMap);
				}
			}
		}
	}
	
	@Override
	public int searchChargerHistoryListCount(Map paramMap) throws NkiaException {
		return chargerBndeChangeDAO.searchChargerHistoryListCount(paramMap);
	}

	@Override
	public List searchChargerHistoryList(Map paramMap) throws NkiaException {
		return chargerBndeChangeDAO.searchChargerHistoryList(paramMap); 
	}
	
}
