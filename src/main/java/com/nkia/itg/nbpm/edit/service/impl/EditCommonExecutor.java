/*
 * @(#)MyListServiceImpl.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.edit.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.edit.dao.EditDAO;
import com.nkia.itg.nbpm.edit.service.EditExecutor;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.ExitTaskCmd;
import com.nkia.itg.nbpm.executor.TerminationProcessCmd;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

@Service("editCommonExecutor")
public class EditCommonExecutor implements EditExecutor {

	@Resource(name = "editDAO")
	private EditDAO editDAO;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name = "nbpmCommonDAO")
	private NbpmCommonDAO nbpmCommonDAO;

	@Override
	public int searchEditListCount(Map paramMap) {
		return editDAO.searchEditListCount(paramMap);
	}

	@Override
	public List searchEditList(Map paramMap) {
		return editDAO.searchEditList(paramMap);
	}

	@Override
	public Map updateSrData(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		try {
			Map originSrData = (Map)paramMap.get("originSrData");
			Iterator updateDataKeySet = paramMap.keySet().iterator();
			List updateList = new ArrayList();
			List itemList = new ArrayList();
			List operList = new ArrayList();
			
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(processVO.getItemList() != null) itemList = processVO.getItemList();
			if(processVO.getOperList() != null) operList = processVO.getOperList();
//			List itemList = processVO.getItemList();
//			List operList = processVO.getOperList();
			
			String srId = (String)paramMap.get("sr_id");
			String queryKey = (String)paramMap.get("queryKey");
			
			//화면에서 전달 받은 grid 데이터를 등록한다.
			if(processVO.getGridMapList() != null && processVO.getGridMapList().size() > 0) {
				updateGridMapData(processVO);
			}
			
			//화면에서 전달 받은 grid 필드의 데이터를 등록한다.
			if(processVO.getGridFieldList() != null && processVO.getGridFieldList().size() > 0){
				updateGridFieldData(processVO);
			}
			
			while(updateDataKeySet.hasNext()){
				String updateDataKey = (String)updateDataKeySet.next();
				String originDataKey = updateDataKey.toUpperCase();
				Object uTempVal = paramMap.get(updateDataKey);
				Object oTempVal = originSrData.get(originDataKey);
				if(uTempVal instanceof String) {
					if(oTempVal instanceof String) {
						String uVal = (String)uTempVal;
						String oVal = (String)oTempVal;
						
						if(updateDataKey.indexOf("_dt") != -1) {
							uVal = StringUtil.removeSpecialLetters(uVal);
							oVal = StringUtil.removeSpecialLetters(oVal);
						}
						
						if(uVal != null && !uVal.equals(oVal)) {
							Map updateHistory = new HashMap();
							updateHistory.put("sr_id", srId);
							updateHistory.put("column_id", originDataKey);
							
							if(StringUtil.getLength(uVal) < 4000 && StringUtil.getLength(oVal) < 4000) {
								updateHistory.put("new_value", uVal);
								updateHistory.put("old_value", oVal);
							}
							int seq = editDAO.selectModifyHistorySeq(srId);
							updateHistory.put("upd_user_id", processVO.getWorker_user_id());
							updateHistory.put("seq", seq);
							editDAO.insertModifyHistory(updateHistory);
						}
					}
				}
			}
			
			updateOperatorInfo(processVO, operList);
			updateProcessItemInfo(processVO, itemList);
			
			if(queryKey != null) {
				String[] querySet = queryKey.split(",");
				String checkQuery = null;
				String insertQuery = null;
				String updateQuery = null;
				for(int i = 0 ; i < querySet.length ; i++) {
					if(querySet[i].indexOf("select") != -1) {
						checkQuery = querySet[i];						
					} else if(querySet[i].indexOf("insert") != -1) {
						insertQuery = querySet[i];
					} else if(querySet[i].indexOf("update") != -1) {
						updateQuery = querySet[i];						
					}
				}
				
				paramMap.put("upd_user_id", processVO.getWorker_user_id());
				postWorkDAO.updateProcessMaster(paramMap);
				postWorkDAO.excuteProcessDetail(updateQuery, paramMap);
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return null;
	}
	
	@Override
	public Map forceQuitSrData(Map paramMap) throws NkiaException {
		try {
			/*
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			AbstractNbpmCommand workCommand = new ExitTaskCmd(processVO);
			workCommand.execute();
			*/
			List taskList = new ArrayList();
			//proc_master 상태값 업데이트			
			postWorkDAO.forceQuitProcMaster(paramMap);
			
			//task 테이블 수정목록 조회
			taskList = postWorkDAO.forceQuitTaskList(paramMap);
			
			for(int i = 0 ; taskList != null && i<taskList.size() ; i++){
 				Map taskId = (Map)taskList.get(i);			
				taskId = WebUtil.lowerCaseMapKey(taskId);

				//task status 업데이트
				postWorkDAO.forceQuitTask(taskId);
			}			
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return null;
	}
	
	private void updateProcessItemInfo(NbpmProcessVO processVO, List itemList) throws SQLException, Exception {
		if(itemList != null && 0 < itemList.size()){
			
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(processVO.getSr_id() != null) postWorkDAO.deleteProcessItem(processVO.getSr_id());			
			
			for(int i = 0 ; itemList != null && i < itemList.size() ; i++) {
				Map itemInfo = (Map)itemList.get(i);
				itemInfo.put("sr_id", processVO.getSr_id());
				postWorkDAO.insertProcessItem(itemInfo);
			}
		}
	}

	private void updateOperatorInfo(NbpmProcessVO processVO, List operList) throws SQLException, Exception {
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(operList != null) {
			if(operList.size() > 0) {
				for(int i = 0 ; operList != null && i < operList.size() ; i++) {
					Map operInfo = (Map)operList.get(i);
					
					if(processVO.getSr_id() != null) {
						operInfo.put("sr_id", processVO.getSr_id());
						postWorkDAO.deleteProcessOrerator(operInfo);
					}					
				}
			}
		}		
		
		for(int i = 0 ; operList != null && i < operList.size() ; i++) {
			Map operInfo = (Map)operList.get(i);
			Object tempOperUserId = operInfo.get("oper_user_id");
			if(tempOperUserId instanceof String) {
				operInfo.put("sr_id", processVO.getSr_id());
				postWorkDAO.insertProcessOrerator(operInfo);
			} else if(tempOperUserId instanceof List) {
				//담당자가 다수일 경우 디자이너의 스크립트를 실행하기 위해 workerCount를 세팅한다.
				List operUserIdList = (List)tempOperUserId;
				for(int j = 0 ; j < operUserIdList.size() ; j++) {
					Map multiOperUser = new HashMap();
					String operUserId = (String)operUserIdList.get(j);
					multiOperUser.put("oper_type", operInfo.get("oper_type"));
					multiOperUser.put("sr_id", processVO.getSr_id());
					multiOperUser.put("oper_user_id", operUserId);
					multiOperUser.put("select_type", operInfo.get("select_type"));
					postWorkDAO.insertProcessOrerator(multiOperUser);
				}
			}
		}
	}	

	@Override
	public void deleteSrData(ModelMap paramMap, String loginUserId) throws NkiaException {
		try {
			String srId = (String)paramMap.get("sr_id");
			List tableNmList = nbpmCommonDAO.searchProcRelTable(paramMap);
			for(int i = 0 ; i < tableNmList.size() ; i++) {
				Map tableNmMap = (Map)tableNmList.get(i);
				paramMap.put("tableNm", tableNmMap.get("TABLE_NAME"));
				postWorkDAO.deleteProcTableData(paramMap);
			}
			
			paramMap.put("upd_user_id", loginUserId);
			Map deleteData = editDAO.selectProcMasterForHistory(paramMap);
			Map paramData = new HashMap();
			if(deleteData != null) {
				Iterator<String> keySet = deleteData.keySet().iterator();
				while(keySet.hasNext()) {
					String mapKey = keySet.next();
					paramData.put(mapKey.toLowerCase(), deleteData.get(mapKey));						
				}
			}
			
			paramData.put("delete_reason", paramMap.get("delete_reason"));
			editDAO.insertProcDeleteHistory(paramData);
			postWorkDAO.deleteProcessOrerator(paramData);
			postWorkDAO.deleteProcessItem(srId);
			postWorkDAO.deleteProcMaster(paramData);
			
			NbpmProcessVO nbpmVO = new NbpmProcessVO();
			
			if(deleteData != null) {
				Object procId = deleteData.get("PROC_ID");
				if (procId != null && procId instanceof Long) {
					nbpmVO.setProcessInstanceId((Long)procId);
					AbstractNbpmCommand nbpmCommad = new TerminationProcessCmd(nbpmVO);
					nbpmCommad.execute();
				}
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}

	@Override
	public boolean isExistParentProcess(ModelMap paramMap) throws NkiaException {
		boolean result = false;
		int resultCnt = editDAO.selectParentProcess(paramMap);
		if(resultCnt > 0) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}
	
	private void updateGridMapData(NbpmProcessVO processVO) throws NkiaException {
		List gridMapList = new ArrayList();
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(processVO.getGridMapList() != null) gridMapList = processVO.getGridMapList();
		
		if(gridMapList.size() > 0) {
			for(int i = 0 ; i < gridMapList.size() ; i++) {
				Map gridDataMap = (Map)gridMapList.get(i);
				String gridInsertQueryKey = (String)gridDataMap.get("gridInsertQueryKey");
				String gridDeleteQueryKey = (String)gridDataMap.get("gridDeleteQueryKey");
				List gridDataList = (List)gridDataMap.get("gridData");
				HashMap paramMap = new HashMap();
				paramMap.put("sr_id", processVO.getSr_id());
				paramMap.put("task_group_id", processVO.getTask_group_id());
				if(gridInsertQueryKey != null && !"".equals(gridInsertQueryKey)) {
					nbpmCommonDAO.deleteGridData(gridDeleteQueryKey, paramMap);
					for(int j = 0 ; j < gridDataList.size() ; j++) {
						Map gridData = (Map)gridDataList.get(j);
						gridData.put("sr_id", processVO.getSr_id());
						gridData.put("upd_user_id", processVO.getWorker_user_id());
						nbpmCommonDAO.insertGridData(gridInsertQueryKey, gridData);
					}
				}
			}
		}
		
	}
	
	private void updateGridFieldData(NbpmProcessVO processVO) throws NkiaException {
		List gridFielList = new ArrayList();
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(processVO.getGridFieldList() != null) gridFielList = processVO.getGridFieldList();
		
		if(gridFielList.size() > 0) {
			for(int i = 0 ; i < gridFielList.size() ; i++) {
				Map dataMap = (Map) gridFielList.get(i);
				String gridInsertQueryKey = (String)dataMap.get("insertQuerykey");
				String gridDeleteQueryKey = (String)dataMap.get("deleteQuerykey");
				List gridList = (List)dataMap.get("gridList");
				
				if(!gridInsertQueryKey.isEmpty() && !gridDeleteQueryKey.isEmpty()){
					
					HashMap paramMap = new HashMap();
					paramMap.put("sr_id", processVO.getSr_id());
					paramMap.put("task_group_id", processVO.getTask_group_id());
					nbpmCommonDAO.deleteGridData(gridDeleteQueryKey, paramMap);
					
					for(int j = 0 ; j <gridList.size(); j++){
						Map gridDataList = (Map)gridList.get(j);
						if(gridInsertQueryKey != null && !"".equals(gridInsertQueryKey)) {
							gridDataList = WebUtil.lowerCaseMapKey(gridDataList);
							gridDataList.put("sr_id", processVO.getSr_id());
							gridDataList.put("upd_user_id", processVO.getWorker_user_id());
							nbpmCommonDAO.insertGridData(gridInsertQueryKey, gridDataList);
						}
					}
				}	
			}
		}
		
		return;
	}

}
