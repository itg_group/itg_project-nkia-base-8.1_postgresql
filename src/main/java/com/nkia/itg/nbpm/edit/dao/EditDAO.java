package com.nkia.itg.nbpm.edit.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("editDAO")
public class EditDAO extends NbpmAbstractDAO {
	
	public int searchEditListCount(Map paramMap) {
		return (Integer)selectByPk("EditDAO.searchEditListCount", paramMap);
	}

	public List searchEditList(Map paramMap) {
		return list("EditDAO.searchEditList", paramMap);
	}

	public Map updateSrData(Map paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	public int selectModifyHistorySeq(String srId) {
		return (Integer)selectByPk("EditDAO.selectModifyHistorySeq", srId);
	}

	public void insertModifyHistory(Map updateHistory) throws SQLException {
		getSqlMapClient().insert("EditDAO.insertModifyHistory", updateHistory);
	}

	public Map selectProcMasterForHistory(Map paramMap) throws SQLException {
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("EditDAO.selectProcMasterForHistory", paramMap, handler);
		return handler.getReturnMap();
	}
	
	public void insertProcDeleteHistory(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().insert("EditDAO.insertProcDeleteHistory", paramMap);
	}

	public int selectParentProcess(ModelMap paramMap) {
		return (Integer)selectByPk("EditDAO.selectParentProcess", paramMap);
	}
}
