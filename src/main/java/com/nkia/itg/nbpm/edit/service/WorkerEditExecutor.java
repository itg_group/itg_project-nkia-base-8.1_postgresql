package com.nkia.itg.nbpm.edit.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface WorkerEditExecutor {
	
	public int searchWorkerEditListCount(Map paramMap);

	public List searchWorkerEditList(Map paramMap);

	public Map updateWorkerData(Map paramMap) throws NkiaException;
	
	public Map updateOperData(Map paramMap) throws NkiaException;

	public List searchWorkerDetailList(ModelMap paramMap);

	public void sendEmail(Map paramMap) throws NkiaException;
	
	public void processForcedProgress(Map paramMap) throws NkiaException;

}
