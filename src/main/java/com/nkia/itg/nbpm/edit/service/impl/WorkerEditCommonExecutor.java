package com.nkia.itg.nbpm.edit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.service.SmsSendService;
import com.nkia.itg.certification.dao.CertificationDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.edit.dao.WorkerEditDAO;
import com.nkia.itg.nbpm.edit.service.WorkerEditExecutor;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.ChangeOperCmd;
import com.nkia.itg.nbpm.executor.ChangeWorkerCmd;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.util.NbpmPortalPushSender;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.email.service.impl.EmailInfoServiceImpl;
import com.nkia.itg.system.user.service.UserService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("workerEditCommonExecutor")
public class WorkerEditCommonExecutor implements WorkerEditExecutor {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailInfoServiceImpl.class);

	@Resource(name = "workerEditDAO")
	private WorkerEditDAO workerEditDAO;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name="certificationDAO")
	public CertificationDAO certificationDAO;

	@Resource(name = "userService")
	private UserService userService;
	
	@Override
	public int searchWorkerEditListCount(Map paramMap) {
		return workerEditDAO.searchWorkerEditListCount(paramMap);
	}

	@Override
	public List searchWorkerEditList(Map paramMap) {
		return workerEditDAO.searchWorkerEditList(paramMap);
	}

	@Override
	public Map updateWorkerData(Map paramMap) throws NkiaException {
		try {
			String workerUserId = (String)paramMap.get("after_worker_id");
			NbpmProcessVO pVO = new NbpmProcessVO();
			int taskId = (Integer)paramMap.get("id");
			pVO.setTask_id((long)taskId);
			pVO.setWorker_user_id(workerUserId);
			AbstractNbpmCommand changeWorkerCmd = new ChangeWorkerCmd(pVO);			
			changeWorkerCmd.execute();
			int count = workerEditDAO.selectWorkerDataCount(paramMap);
			if(count > 0) {
				workerEditDAO.updateWorkerData(paramMap);				
			} else {
				workerEditDAO.insertWorkerData(paramMap);
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return null;
	}
	

	@Override
	public Map updateOperData(Map paramMap) throws NkiaException {
		try {
			String workerUserId = (String)paramMap.get("after_worker_id");
			String grpSlctYn = (String)paramMap.get("grp_slct_yn");
			NbpmProcessVO pVO = new NbpmProcessVO();
			int taskId = (Integer)paramMap.get("id");
			pVO.setTask_id((long)taskId);
			pVO.setWorker_user_id(workerUserId);
			pVO.setGrp_slct_yn(grpSlctYn);
			AbstractNbpmCommand changeOperCmd = new ChangeOperCmd(pVO);			
			changeOperCmd.execute();
			int count = workerEditDAO.selectWorkerDataCount(paramMap);
			if(count > 0) {
				workerEditDAO.updateWorkerData(paramMap);				
			} else {
				workerEditDAO.insertWorkerData(paramMap);
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return null;
	}
	
	
	@Override
	public List searchWorkerDetailList(ModelMap paramMap) {
		return workerEditDAO.searchWorkerDetailList(paramMap);
	}
	
	
	/**
	 * 이메일 보내기
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void sendEmail(Map paramMap) throws NkiaException {
		
//		try {
//			Map alarmMap = new HashMap();
//			
//			//알림 메일
//			alarmMap.put("KEY", "PROCESS");
//			alarmMap.put("TEMPLATE_ID", "EMPTY_TEMPLATE");
//			
//			// 1. 메일수신자셋팅(LIST로)
//			Map dataMap = new HashMap();
//			ModelMap modelMap = new ModelMap();
//			modelMap.put("user_id", (String) paramMap.get("after_worker_id"));
//			List userList = userService.searchUserListForMail(modelMap);
//			dataMap.put("TO_USER_LIST", userList);
//			dataMap.put("TITLE", "[알림] 프로세스 담당자 변경");
//			dataMap.put("CONTENT", "<br/>프로세스 담당자가 변경되었습니다.<br/><br/><br/>확인바랍니다.<br/><br/>");
//			alarmMap.put("DATA", dataMap);
//
//			EmailSetData emailSender = new EmailSetData(alarmMap);
//			emailSender.sendMail();
//		} catch (Exception e) {
//			throw new NkiaException(e);
//		}
		
	try {
			Map processDetail = postWorkDAO.selectProcessDetail((String)paramMap.get("sr_id"));
			NbpmProcessVO processVO = new NbpmProcessVO();
			Number n = (Number)paramMap.get("id");
			String processId = postWorkDAO.selectProcessIdTaskId(n.longValue());
			processVO.setProcessId(processId);
			processVO.setTask_id(n.longValue());
			
			//다음 task의 알림 확인
			Map nextTaskAlarm = postWorkDAO.selectNextTaskAlamInfo_workedit(processVO);
			String emailSend = ""; //이메일발송여부
			String smsSend = ""; //문자발송여부
			String grpSlctYn = ""; //그룹여부
			String emailSendGrpYn = ""; //(담당자, 처리자 등의) 그룹에 메일 발송 여부
			String nodeName = ""; //작업명
			String personalYn = "";//개인발송여부
			
			if(nextTaskAlarm != null){
				emailSend = (String)nextTaskAlarm.get("EMAIL_SEND"); //이메일발송여부
				smsSend = (String)nextTaskAlarm.get("SMS_SEND"); //문자발송여부
				grpSlctYn = (String)nextTaskAlarm.get("GRP_SLCT_YN"); //그룹여부
				emailSendGrpYn = (String)nextTaskAlarm.get("EMAIL_SEND_GRP_YN"); //() 그룹에 메일 발송 여부  
				nodeName = (String)nextTaskAlarm.get("NODENAME"); //작업명
				personalYn = (String)nextTaskAlarm.get("PERSONAL_YN"); //그룹 발송 여부
			}
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginId = userVO.getUser_id();
			
			//SMS
			if (smsSend.equals(EmailEnum.COMMON_Y.getString())) {
				//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
				List alamList = postWorkDAO.searchNbpmAlamSmsList_workedit(processVO);
				if("Y".equals(grpSlctYn)){//실제 데이터가 그룹인지 아닌지를파악 // 실제데이터가 그룹일때
					if("Y".equals(personalYn)){ //그룹발송여부 확인 "Y"그룹  "N" 발송안함
						for (int j = 0; j < alamList.size(); j++) {
							Map alamInfo = (Map)alamList.get(j);
							if("Y".equals(alamInfo.get("ALARM_YN"))){ //사용자정보에 그룹이관 알람수신여부가 Y일때만 발송
								userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
								loginId = userVO.getUser_id();
								if(!loginId.equals((String)alamInfo.get("TARGET_USER_ID"))){ //로그인ID랑 보내는사람이랑 다를때만 발송
									nodeName = (String)alamInfo.get("NODENAME"); //절차단계
									String userId = (String)alamInfo.get("TARGET_USER_ID"); //수신자ID
									//프로세스 작업 상태 관리에서 문자 송신 여부가 활성화 되어 있으면 문자를 보낸다.
									ModelMap modelMap = new ModelMap();
									modelMap.put("userId", userId);
									Map userInfoMap = certificationDAO.selectUserInfo(modelMap);
									
									Map smsInfo = new HashMap();
									smsInfo.put("nodeName", nodeName);
									String toPhone = (String)(userInfoMap.get("HP_NO") == null ? "-" : userInfoMap.get("HP_NO"));
									smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
									String content = "ITSM 업무 알림입니다. \n";
									content += "▶프로세스 정보 : "+ (String)processDetail.get("REQ_TYPE_NM") + "\n";
									content += "▶ID : " + (String)processDetail.get("SR_ID")+ "\n";
									content += "▶알림내용 : " + (String)alamInfo.get("NODENAME") + "\n";
									if("SERVICE".equals((String)processDetail.get("REQ_TYPE"))){
										content += "▶신청유형 : " + (String)processDetail.get("REQUST_TYPE_NM") + "\n";
										content += "▶유형분류 : " + (String)processDetail.get("REQ_DOC_NM") + "\n";
									}
									content += "▶고객사 : " + (String)processDetail.get("MAIL_CUSTOMER_ID_NM") + "\n";
									content += "▶요청자 : " + (String)processDetail.get("REQ_USER_NM") + "\n";
									content += "▶제목 : " +(String)processDetail.get("TITLE")+"\n";
									smsInfo.put("MSG_BODY", content);
									smsInfo.put("RE_BODY", "["+(String)processDetail.get("SR_ID")+"]"+(String)processDetail.get("REQ_TYPE_NM")+"_업무가 배정되었습니다.");
									smsInfo.put("TEMPLATE_CODE", "LMSG_20200317133022746749"); //ITSM업무알림 템플릿코드
									smsInfo.put("SENDER_KEY", "ef834c839e4a9c83ac3e027b0ed782a7545014d4"); // 센더키
									smsInfo.put("SEND_PHONE", "0226264210"); // 보내는사람
									smsInfo.put("USER_NM", (String)alamInfo.get("TARGET_USER_NM")); 
									SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
									smsSendService.insertTalkList(smsInfo);
								}
							}
						}
					}
				}else{
					for (int j = 0; j < alamList.size(); j++) {
						Map alamInfo = (Map)alamList.get(j);
						userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
						loginId = userVO.getUser_id();
						if(!loginId.equals((String)alamInfo.get("TARGET_USER_ID"))){ //로그인ID랑 보내는사람이랑 다를때만 발송
							nodeName = (String)alamInfo.get("NODENAME"); //절차단계
							String userId = (String)alamInfo.get("TARGET_USER_ID"); //수신자ID
							//프로세스 작업 상태 관리에서 문자 송신 여부가 활성화 되어 있으면 문자를 보낸다.
							ModelMap modelMap = new ModelMap();
							modelMap.put("userId", userId);
							Map userInfoMap = certificationDAO.selectUserInfo(modelMap);
							
							Map smsInfo = new HashMap();
							smsInfo.put("nodeName", nodeName);
							String toPhone = (String)(userInfoMap.get("HP_NO") == null ? "-" : userInfoMap.get("HP_NO"));
							smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
							String content = "ITSM 업무 알림입니다. \n";
							content += "▶프로세스 정보 : "+ (String)processDetail.get("REQ_TYPE_NM") + "\n";
							content += "▶ID : " + (String)processDetail.get("SR_ID")+ "\n";
							content += "▶알림내용 : " + (String)alamInfo.get("NODENAME") + "\n";
							if("SERVICE".equals((String)processDetail.get("REQ_TYPE"))){
								content += "▶신청유형 : " + (String)processDetail.get("REQUST_TYPE_NM") + "\n";
								content += "▶유형분류 : " + (String)processDetail.get("REQ_DOC_NM") + "\n";
							}
							content += "▶고객사 : " + (String)processDetail.get("MAIL_CUSTOMER_ID_NM") + "\n";
							content += "▶요청자 : " + (String)processDetail.get("REQ_USER_NM") + "\n";
							content += "▶제목 : " +(String)processDetail.get("TITLE")+"\n";
							smsInfo.put("MSG_BODY", content);
							smsInfo.put("RE_BODY", "["+(String)processDetail.get("SR_ID")+"]"+(String)processDetail.get("REQ_TYPE_NM")+"_업무가 배정되었습니다.");
							smsInfo.put("TEMPLATE_CODE", "LMSG_20200317133022746749"); //ITSM업무알림 템플릿코드
							smsInfo.put("SENDER_KEY", "ef834c839e4a9c83ac3e027b0ed782a7545014d4"); // 센더키
							smsInfo.put("SEND_PHONE", "0226264210"); // 보내는사람
							smsInfo.put("USER_NM", (String)alamInfo.get("TARGET_USER_NM"));
							SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
							smsSendService.insertTalkList(smsInfo);
						}
					}
				}
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
	
public void processForcedProgress(Map paramMap) throws NkiaException {
	NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient");
	CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
	
	try {
		List insertList = new ArrayList();
		insertList = commonDAO.selectTempTaskConfirm(paramMap);
		
		if(insertList.size() > 0){
			for(int j=0; j<insertList.size(); j++){
				Map processInfoMap = new HashMap();
				processInfoMap = (Map) insertList.get(j);
									
				Map insertMap = new HashMap();
				insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
				insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
				insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
				insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
				insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
				insertMap.put("workerUserId", String.valueOf(processInfoMap.get("WORK_USER_ID"))); 
			//	insertMap.put("nbpm_comment", "");
				
				nbpmCommonClient.workTaskForRule(insertMap);
			}
		}
		/*
		Map insertMap = new HashMap();
		insertMap.put("nbpm_task_id", String.valueOf(paramMap.get("nbpm_task_id")));
		insertMap.put("sr_id", String.valueOf(paramMap.get("sr_id")));
		insertMap.put("nbpm_processId", String.valueOf(paramMap.get("nbpm_process_id")));
		insertMap.put("nbpm_processInstanceId", String.valueOf(paramMap.get("nbpm_process_instance_id")));
		insertMap.put("nbpm_process_type", String.valueOf(paramMap.get("nbpm_process_type")));
		insertMap.put("workerUserId", String.valueOf(paramMap.get("worker_user_id"))); 
	//	insertMap.put("nbpm_comment", "");
		
		nbpmCommonClient.workTaskForRule(insertMap);
	*/} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
}