package com.nkia.itg.nbpm.edit.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.edit.service.ChargerBndeChangeService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ChargerBndeChangeController {
	
	@Resource(name = "chargerBndeChangeService")
	private ChargerBndeChangeService chargerBndeChangeService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 프로세스 담당자 일괄 수정 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/goChargerBndeChangeTab.do")
	public String goUserSelectPopMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/nbpm/edit/chargerBndeChangeTab.nvf";
		return forwarPage;
	}

	/**
	 * 프로세스 담당자 일괄 변경 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/searchChargerBndeChangeList.do")  
	public @ResponseBody ResultVO searchChargerBndeChangeList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = chargerBndeChangeService.searchChargerBndeChangeListCount(paramMap);
			List resultList = chargerBndeChangeService.searchChargerBndeChangeList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 담당자 일괄 변경 사용자 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/searchChargerBndeChangeMapngChargerList.do")  
	public @ResponseBody ResultVO searchChargerBndeChangeMapngChargerList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
		
			List resultList = chargerBndeChangeService.searchChargerBndeChangeMapngChargerList(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 담당자 일괄 변경 - 목록 등록, 등록적용
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/registChargerBndeChange.do")
	public @ResponseBody ResultVO registChargerBndeChange(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String USER_ID = userVO.getUser_id();
			paramMap.put("upd_user_id", USER_ID);
			chargerBndeChangeService.registChargerBndeChange(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 담당자 일괄 변경 - 수정, 수정적용
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/updateChargerBndeChange.do")
	public @ResponseBody ResultVO updateChargerBndeChange(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String USER_ID = userVO.getUser_id();
			paramMap.put("upd_user_id", USER_ID);
			chargerBndeChangeService.updateChargerBndeChange(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 담당자 일괄 변경이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/searchChargerHistoryList.do")  
	public @ResponseBody ResultVO searchChargerHistoryList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = chargerBndeChangeService.searchChargerHistoryListCount(paramMap);
			List resultList = chargerBndeChangeService.searchChargerHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
