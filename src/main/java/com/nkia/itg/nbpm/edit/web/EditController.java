/*
 * @(#)MyListController.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.edit.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.edit.service.EditExecutor;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.system.service.SystemService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class EditController {
	
	@Resource(name="editCommonExecutor")
	private EditExecutor editExecutor;
	
	@Resource(name = "systemService")
	private SystemService systemService;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/nbpm/edit/goEditTab.do")
	public String goEditTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/nbpm/edit/editTab.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/edit/goEditMain.do")
	public String goEditMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/edit/editMain.nvf";
		return forwarPage;
	}
	
	//@@20130711 김도원 END			
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/searchEditList.do")
	public @ResponseBody ResultVO searchEditList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			
			totalCount = editExecutor.searchEditListCount(paramMap);
			resultList = editExecutor.searchEditList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	@RequestMapping(value="/itg/nbpm/edit/updateSrData.do")
	public @ResponseBody ResultVO updateSrData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			processCommonExecutor.registerUser(processVO, paramMap);
			
			String taskStatus = processCommonExecutor.selectTaskStatus(paramMap);
			if("Recall".equals(taskStatus)) {
				paramMap.put("task_status","Reserved");
				paramMap.put("task_id", paramMap.get("nbpm_task_id"));
				processCommonExecutor.updateTaskStatus(paramMap);
			}
			
			editExecutor.updateSrData(processVO, paramMap);
			resultVO.setResultMsg("수정되었습니다."); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//프로세스 강제종료
	@RequestMapping(value="/itg/nbpm/edit/forceQuitSrData.do")
	public @ResponseBody ResultVO forceQuitSrData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
//		String srId = (String)paramMap.get("sr_id");
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			editExecutor.forceQuitSrData(paramMap);
			
			resultVO.setResultMsg("종료되었습니다."); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/nbpm/edit/checkDeleteSrData.do")
	public @ResponseBody ResultVO checkDeleteSrData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			HashMap resultMap = new HashMap();
			boolean result =  editExecutor.isExistParentProcess(paramMap);
			
			resultMap.put("isExist", result);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg("상위연계요청이 존재합니다.\n삭제하시겠습니까?"); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
		
	
	@RequestMapping(value="/itg/nbpm/edit/deleteSrData.do")
	public @ResponseBody ResultVO deleteSrData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			editExecutor.deleteSrData(paramMap, loginUserId);
			resultVO.setResultMsg("삭제되었습니다."); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/edit/deleteSrDataList.do")
	public @ResponseBody ResultVO deleteSrDataList(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		
		List tasks = (List)paramMap.get("tasks");
		
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			for(int i = 0 ; i < tasks.size() ; i++){
				Map task = (Map)tasks.get(i);
				ModelMap param = new ModelMap();
				param.put("sr_id", String.valueOf(task.get("sr_id")));
				param.put("process_type", String.valueOf(task.get("process_type")));
				param.put("title", String.valueOf(task.get("title")));
				param.put("work_state", String.valueOf(task.get("work_state")));
				param.put("req_user_id", String.valueOf(task.get("req_user_id")));
				param.put("req_user_nm", String.valueOf(task.get("req_user_nm")));
				param.put("reg_dt", String.valueOf(task.get("reg_dt")));
				param.put("req_dt", String.valueOf(task.get("req_dt")));
				param.put("delete_reason", String.valueOf(task.get("delete_reason")));
				editExecutor.deleteSrData(param, loginUserId);
			}
			resultVO.setResultMsg("삭제되었습니다."); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
