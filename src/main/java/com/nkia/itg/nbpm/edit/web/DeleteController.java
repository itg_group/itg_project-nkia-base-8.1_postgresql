package com.nkia.itg.nbpm.edit.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.edit.service.DeleteExecutor;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class DeleteController {
	
	@Resource(name="deleteCommonExecutor")
	private DeleteExecutor deleteExecutor;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/nbpm/goDeleteTab.do")
	public String goDeleteTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/nbpm/edit/deleteTab.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/goDeleteMain.do")
	public String goDeleteMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/edit/deleteMain.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/goDeleteDetailPage.do")
	public String goDeleteDetailPage(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String sr_id = request.getParameter("sr_id");
		paramMap.put("sr_id", sr_id);
		String forwarPage = "/itg/nbpm/edit/deleteDetail.nvf";
		return forwarPage;
	}
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchDeleteList.do")
	public @ResponseBody ResultVO searchDeleteList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			
			totalCount = deleteExecutor.searchDeleteListCount(paramMap);
			resultList = deleteExecutor.searchDeleteList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	@RequestMapping(value="/itg/nbpm/selectDeleteDetail.do")
	public @ResponseBody ResultVO selectDeleteDetail(@RequestBody ModelMap paramMap) throws NkiaException {
		Map resultMap = null;
		ResultVO result = new ResultVO();
		try{
			resultMap = deleteExecutor.selectDeleteDetail(paramMap);
			result.setResultMap((HashMap)resultMap);
			result.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
}
