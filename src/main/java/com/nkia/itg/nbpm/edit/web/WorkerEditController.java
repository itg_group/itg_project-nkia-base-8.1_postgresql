package com.nkia.itg.nbpm.edit.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.edit.service.WorkerEditExecutor;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkerEditController {
	
	@Resource(name="workerEditCommonExecutor")
	private WorkerEditExecutor workerEditExecutor;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/nbpm/edit/goWorkerEditTab.do")
	public String goMyHistoryTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/nbpm/edit/workerEditTab.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/edit/goWorkerEditMain.do")
	public String goMyHistoryListMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/edit/workerEditMain.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/nbpm/edit/goWorkerDetailPage.do")
	public String goWorkerDetailPage(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/edit/workerEditDetail.nvf";
		String srId = request.getParameter("sr_id");
		paramMap.put("srId", srId);
		return forwarPage;
	}	
	
	@RequestMapping(value="/itg/nbpm/edit/goWorkerForcedProgress.do")
	public String goWorkerForcedProgress(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/nbpm/edit/workerForcedProgress.nvf";
		return forwarPage;
	}
	
		
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/searchWorkerEditList.do")
	public @ResponseBody ResultVO searchWorkerEditList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			
			totalCount = workerEditExecutor.searchWorkerEditListCount(paramMap);
			resultList = workerEditExecutor.searchWorkerEditList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/searchWorkerDetailList.do")
	public @ResponseBody ResultVO searchWorkerDetailList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = workerEditExecutor.searchWorkerDetailList(paramMap);
			
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}	
	
	@RequestMapping(value="/itg/nbpm/edit/updateWorkerData.do")
	public @ResponseBody ResultVO updateWorkerData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			paramMap.put("upd_user_id", loginUserId);
			NbpmProcessVO processVO = new NbpmProcessVO();
			String workerUserId = (String)paramMap.get("after_worker_id");
			processVO.setWorker_user_id(workerUserId);
			processCommonExecutor.registerUser(processVO, paramMap);
			
			workerEditExecutor.updateWorkerData(paramMap);
			
			//알림 이메일 발송 기능 추가
			if(((String)paramMap.get("email_send")).equals("Y")){
				workerEditExecutor.sendEmail(paramMap);
				resultVO.setResultMsg("수정되었습니다. \n 알림 이메일 발송을 하였습니다.");
			}else{
				resultVO.setResultMsg("수정되었습니다.");
			}
			
			//resultVO.setResultMsg("수정되었습니다."); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 프로세스 담당자(그룹)변경
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/edit/updateOperData.do")
	public @ResponseBody ResultVO updateOperData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			paramMap.put("upd_user_id", loginUserId);
			NbpmProcessVO processVO = new NbpmProcessVO();
			String workerUserId = (String)paramMap.get("after_worker_id");
			processVO.setWorker_user_id(workerUserId);
			paramMap.put("after_worker_id", workerUserId); 
			
			String grpSlctYn = (String)paramMap.get("grp_slct_yn");
			processVO.setGrp_slct_yn(grpSlctYn);
			processCommonExecutor.registerUser(processVO, paramMap);
			
			workerEditExecutor.updateOperData(paramMap);
			
			//알림 이메일 발송 기능 추가
			if(((String)paramMap.get("email_send")).equals("Y")){
				workerEditExecutor.sendEmail(paramMap);
				resultVO.setResultMsg("수정되었습니다. \n 알림을 하였습니다.");
			}else{
				resultVO.setResultMsg("수정되었습니다.");
			}
			
			//resultVO.setResultMsg("수정되었습니다."); 
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 테스트 이메일 보내기
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/nbpm/edit/sendEmail.do")
	public @ResponseBody ResultVO  sendTestEmail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			workerEditExecutor.sendEmail(paramMap);
			resultVO.setResultMsg("TEST");
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 타스크 강제진행
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/nbpm/edit/processForcedProgress.do")
	public @ResponseBody ResultVO  processForcedProgress(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			workerEditExecutor.processForcedProgress(paramMap);
			resultVO.setResultMsg("실행되었습니다.");
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

}
