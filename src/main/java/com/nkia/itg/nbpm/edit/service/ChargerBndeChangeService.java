package com.nkia.itg.nbpm.edit.service;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ChargerBndeChangeService {

	int searchChargerBndeChangeListCount(Map paramMap) throws NkiaException;
	
	List searchChargerBndeChangeList(Map paramMap) throws NkiaException;
	
	int searchChargerBndeChangeMapngChargerListCount(Map paramMap) throws NkiaException;
	
	List searchChargerBndeChangeMapngChargerList(Map paramMap) throws NkiaException;
	
	void registChargerBndeChange(Map paramMap) throws NkiaException;
	
	void updateChargerBndeChange(Map paramMap) throws NkiaException;

	int searchChargerHistoryListCount(Map paramMap) throws NkiaException;
	
	List searchChargerHistoryList(Map paramMap) throws NkiaException;
	
}



