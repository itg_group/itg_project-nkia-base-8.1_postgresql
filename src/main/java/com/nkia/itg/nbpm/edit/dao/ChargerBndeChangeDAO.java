package com.nkia.itg.nbpm.edit.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("chargerBndeChangeDAO")
public class ChargerBndeChangeDAO extends EgovComAbstractDAO{

	public String selectBndeChangeId() {
		return (String) selectByPk("ChargerBndeChangeDAO.selectBndeChangeId", null);
	}

	public int searchChargerBndeChangeListCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerBndeChangeDAO.searchChargerBndeChangeListCount", paramMap);
	}
	
	public List searchChargerBndeChangeList(Map paramMap) {
		return this.list("ChargerBndeChangeDAO.searchChargerBndeChangeList", paramMap);
	}
	
	public int searchChargerBndeChangeMapngChargerListCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerBndeChangeDAO.searchChargerBndeChangeMapngChargerListCount", paramMap);
	}
	
	public List searchChargerBndeChangeMapngChargerList(Map paramMap) {
		return this.list("ChargerBndeChangeDAO.searchChargerBndeChangeMapngChargerList", paramMap);
	}
	
	public void insertUserBatchEdit(Map paramMap) {
		insert("ChargerBndeChangeDAO.insertUserBatchEdit", paramMap);
	}

	public void insertUserBatchEditMapngUser(Map paramMap) {
		insert("ChargerBndeChangeDAO.insertUserBatchEditMapngUser", paramMap);
	}

	public void updateUserBatchEdit(Map paramMap) {
		update("ChargerBndeChangeDAO.updateUserBatchEdit", paramMap);
	}

	public void deleteUserBatchEditMapngUser(Map paramMap) {
		delete("ChargerBndeChangeDAO.deleteUserBatchEditMapngUser", paramMap);
	}

	public List searchChargerReqList(Map paramMap) {
		return this.list("ChargerBndeChangeDAO.searchChargerReqList", paramMap);
	}
	public List searchOperOnlyList(Map paramMap) {
		return this.list("ChargerBndeChangeDAO.searchOperOnlyList", paramMap);
	}
	public void insertChargerEditListHistory(Map paramMap) {
		insert("ChargerBndeChangeDAO.insertChargerEditListHistory", paramMap);
	}

	public int searchChargerHistoryListCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerBndeChangeDAO.searchChargerHistoryListCount", paramMap);
	}
	
	public List searchChargerHistoryList(Map paramMap) {
		return this.list("ChargerBndeChangeDAO.searchChargerHistoryList", paramMap);
	}
}
