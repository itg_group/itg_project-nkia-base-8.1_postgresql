/*
 * @(#)MyListService.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.edit.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;


public interface EditExecutor {
	
	public int searchEditListCount(Map paramMap);

	public List searchEditList(Map paramMap);

	public Map updateSrData(NbpmProcessVO processVO, Map paramMap) throws NkiaException;
	
	public Map forceQuitSrData(Map paramMap) throws NkiaException;

	public void deleteSrData(ModelMap paramMap, String loginUserId) throws NkiaException;

	public boolean isExistParentProcess(ModelMap paramMap) throws NkiaException;
}
