package com.nkia.itg.nbpm.edit.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("workerEditDAO")
public class WorkerEditDAO extends NbpmAbstractDAO {

	public List searchWorkerEditList(Map paramMap) {
		return list("WorkerEditDAO.searchWorkerEditList", paramMap);
	}

	public int searchWorkerEditListCount(Map paramMap) {
		return (Integer)selectByPk("WorkerEditDAO.searchWorkerEditListCount", paramMap);
	}

	public List searchWorkerDetailList(ModelMap paramMap) {
		return list("WorkerEditDAO.searchWorkerDetailList", paramMap);
	}

	public void updateWorkerData(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().update("WorkerEditDAO.updateWorkerData", paramMap);
	}

	public int selectWorkerDataCount(Map paramMap) throws SQLException, Exception {
		return (Integer)selectByPk("WorkerEditDAO.selectWorkerDataCount", paramMap);
	}

	public void insertWorkerData(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().update("WorkerEditDAO.insertWorkerData", paramMap);
	}

}
