package com.nkia.itg.nbpm.edit.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DeleteExecutor {

	public int searchDeleteListCount(Map paramMap) throws NkiaException;

	public List searchDeleteList(Map paramMap) throws NkiaException;

	public Map selectDeleteDetail(Map paramMap) throws NkiaException, SQLException;

}
