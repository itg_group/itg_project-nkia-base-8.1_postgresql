package com.nkia.itg.nbpm.preprocessor.impl;

import java.util.List;
import java.util.Map;

import com.nkia.itg.nbpm.preprocessor.PreProcessor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class CommonPreProcessor implements PreProcessor {
	
	public void workPreProcessing(NbpmProcessVO processVO, Map paramMap) {
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(processVO.getProcess_type() != null) {
			if("MNTNCE".equals(processVO.getProcess_type())) {
				String procCn = processVO.getComment();
				List gridMapList = processVO.getGridMapList();
				for(int i = 0 ; i < gridMapList.size() ; i++) {
					Map gridDataMap = (Map)gridMapList.get(i);
					List gridDataList = (List)gridDataMap.get("gridData");
					for(int j = 0 ; j < gridDataList.size() ; j++) {
						Map gridData = (Map)gridDataList.get(j);
						String chckCnText = (String)gridData.get("chck_cn");
						if(!(chckCnText != null && !"".equals(chckCnText) && !"null".equals(chckCnText))) {
							chckCnText = procCn;
						}
						gridData.put("sr_id", processVO.getSr_id());
						gridData.put("task_group_id", processVO.getTask_group_id());
						gridData.put("proc_result_cn", chckCnText);
						gridData.put("upd_user_id", processVO.getWorker_user_id());
					}
				}
			}
		}
		
	}

}
