package com.nkia.itg.nbpm.preprocessor;

import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.preprocessor.impl.CommonPreProcessor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class PreProcessExcutor {

	
	public void workPreProcessing(NbpmProcessVO processVO, Map paramMap) throws NkiaException {

		PreProcessor processor = null;
		
		if("POC".equals(NbpmConstraints.PROJECT_NAME)) {
			processor = new CommonPreProcessor();
			processor.workPreProcessing(processVO, paramMap);
		}
	}

}
