package com.nkia.itg.nbpm.preprocessor;

import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public interface PreProcessor {
	
	public void workPreProcessing(NbpmProcessVO processVO, Map paramMap) throws NkiaException;
}
