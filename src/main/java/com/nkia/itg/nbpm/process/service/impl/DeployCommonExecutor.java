/*
 * @(#)DeployExecutor.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.process.service.impl;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsWorkFlowDeployBean;
import com.nkia.itg.nbpm.common.NbpmTransactionManager;
import com.nkia.itg.nbpm.process.dao.DeployDAO;
import com.nkia.itg.nbpm.process.service.DeployExecutor;
import com.nkia.itg.nbpm.util.Base64;
import com.nkia.itg.nbpm.util.DeployXMLPaser;

@Service("deployCommonExecutor")
public class DeployCommonExecutor implements DeployExecutor {
	
	@Resource(name = "deployDAO")
	private DeployDAO deployDAO;
	
	public void createIndexingFile(ArrayList fileList, ModelMap modelMap,
			String rootPath) throws NkiaException, IOException {
		
		StringBuffer strBuf = new StringBuffer();
		String version = (String)modelMap.get("version");
		String process_type = (String)modelMap.get("process_type");
		
		for(int i = 0 ; i < fileList.size() ; i++) {
			String fileName = (String)fileList.get(i);
			String beforeName = "";
			String afterName = "";
			int extInt = fileName.indexOf(".nvf");
			beforeName = fileName.substring(0, extInt);
			afterName = new String(Base64.encodeBase64(beforeName.getBytes("euc-kr"), false),"euc-kr");
			strBuf.append(beforeName + " : " + afterName);
			strBuf.append("\n");
		}
		
		File newFile = new File(rootPath + "/itg/nbpm/" + process_type + "-" + version + "/mappingList.txt");
		if(!newFile.exists()) {
			newFile.createNewFile();				
		}
		
		BufferedOutputStream prrocessout = new BufferedOutputStream(
				new FileOutputStream(newFile));
		
		prrocessout.write(strBuf.toString().getBytes());
		prrocessout.close();
	}
	
	public List searchFinalVesions() throws NkiaException, SQLException {
		// TODO Auto-generated method stub
		return deployDAO.searchFinalVesions();
	}

	public void deployProcess(WsWorkFlowDeployBean wsWorkFlowDeployBean)
			throws NkiaException, IOException {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		DataInputStream in = null;
		try {
			//nsm.begin();
			String data = wsWorkFlowDeployBean.getData();
			String processType = wsWorkFlowDeployBean.getProcessType();
			String processName = wsWorkFlowDeployBean.getProcessName();
			String currentVersion = deployDAO.selectFinalVersion(processType);
			String finalVersion = "";
			if(currentVersion != null && !"".equals(currentVersion)) {
				finalVersion = String.valueOf(Integer.parseInt(currentVersion) + 1);				
			} else {
				finalVersion = "1";
			}
			
			//@@20130719 김도원 START
			//멀티프로세스(서브프로세스)의 process id를 최신으로 가져오기 위한 로직을 추가함.
			String filename = wsWorkFlowDeployBean.getFname()+ "_v_" + finalVersion + "." + wsWorkFlowDeployBean.getFext();
			String processId = processType + "_v_" + finalVersion;
			data = data.replaceAll("[$]PROCESSID[$]", processId);
			
			String bpm_file_upload_path = BaseConstraints.NBPM_BPMN2FILE_PATH;
			File deployFile = createXmlFile(bpm_file_upload_path, data);
			
			Map deployInfoMap = DeployXMLPaser.build(deployFile);
			
			List calledElementList = (List)deployInfoMap.get("calledElementList");
			List propertyList = (List)deployInfoMap.get("propertyList");
			List taskList = (List)deployInfoMap.get("taskList");
			
			String result = null;
			
	        byte[] buffer = new byte[(int)deployFile.length()];
	      //@@20130719 김도원 END
	        in = new DataInputStream(new FileInputStream(deployFile));
	        in.readFully(buffer);
	        result = new String(buffer, "MS949");
	        
	        in.close();

	        List currentNodeDetailList = null;
	       // if(!"1".equals(finalVersion)) {
	        currentNodeDetailList = deployDAO.searchNbpmNodeWorkState(processType);
	       // }
	        
			HashMap param = new HashMap();
			param.put("process_type", processType);
			param.put("process_nm", processName);
	        param.put("blob_value", result.getBytes());
	        param.put("file_nm", filename);
	        param.put("processId", processId);
	        param.put("version", finalVersion);
			int deployId = 0;
			if("test".equals(processType)) {
				deployDAO.updateTestProcess(param);
			} else {
				deployId = (Integer)deployDAO.selectNewDeployId();
				param.put("deployId", deployId);
				deployDAO.insertNewProcess(param);
			}
			
			for(int i = 0 ; i < taskList.size() ; i++) {
				Map node = (Map)taskList.get(i);
				String taskName = (String)node.get("taskName");
				String nodeName = (String)node.get("nodeName");
				String multiYn = (String)node.get("multiYn");
				if(currentNodeDetailList != null) {
					for(int j = 0 ; j < currentNodeDetailList.size() ; j++) {
						Map curNodeMap = (Map)currentNodeDetailList.get(j);
						String curTaskName = (String)curNodeMap.get("TASK_NAME");
						String curWorkState = (String)curNodeMap.get("WORK_STATE");
						String curProcState = (String)curNodeMap.get("PROC_STATE");
						String curWorkDtUpdateYn = (String)curNodeMap.get("WORK_DT_UPDATE_YN");
						
						String curEmailSendYn = (String)curNodeMap.get("EMAIL_SEND");
						String curSmsSendYn = (String)curNodeMap.get("SMS_SEND");
						String curEmailTemplateId = (String)curNodeMap.get("EMAIL_TEMPLATE_ID");
						String curSmsTemplateId = (String)curNodeMap.get("SMS_TEMPLATE_ID");
						String curEmailSendGrpYn = (String)curNodeMap.get("EMAIL_SEND_GRP_YN");
						String curBatchProcYn = (String)curNodeMap.get("BATCH_PROC_YN");
						String curSanctnLineSlctYn = (String)curNodeMap.get("SANCTN_LINE_SLCT_YN");
						String curFaqYn = (String)curNodeMap.get("FAQ_YN");
						String curProcessStatisYn = (String)curNodeMap.get("PROCESS_STATIS_YN");
						String curHideFlowYn = (String)curNodeMap.get("HIDE_FLOW_YN");
						String curPersonalYn = (String)curNodeMap.get("PERSONAL_YN");
						
						if(taskName != null && curTaskName != null && taskName.equals(curTaskName)) {
							node.put("work_state", curWorkState);
							node.put("proc_state", curProcState);
							node.put("work_dt_update_yn", curWorkDtUpdateYn);							
							node.put("email_send", curEmailSendYn);
							node.put("sms_send", curSmsSendYn);
							node.put("email_template_id", curEmailTemplateId);
							node.put("sms_template_id", curSmsTemplateId);
							node.put("email_send_grp_yn", curEmailSendGrpYn);
							node.put("batch_proc_yn", curBatchProcYn);
							node.put("sanctn_line_slct_yn", curSanctnLineSlctYn);
							node.put("faq_yn", curFaqYn);
							node.put("process_statis_yn", curProcessStatisYn);
							node.put("hide_flow_yn", curHideFlowYn);
							node.put("personal_yn", curPersonalYn);
						}
					}
				}
				node.put("deployId", deployId);
				node.put("processId", processId);
				node.put("version", finalVersion);
				node.put("multi_yn", multiYn);
				
				deployDAO.insertNbpmNodeDetail(node);
			}
			
			for(int i = 0 ; i < taskList.size() ; i++) {
				Map node = (Map)taskList.get(i);
				node.put("deployId", deployId);
				node.put("processId", processId);
				String emailYn = (String)node.get("email");
				if("Y".equals(emailYn)) {
					node.put("alam_type", "EMAIL");
					node.put("target_type", "S");
					deployDAO.insertNbpmNodeAlam(node);
				}
				String smsYn = (String)node.get("sms");
				if("Y".equals(smsYn)) {
					node.put("alam_type", "SMS");
					node.put("target_type", "S");
					deployDAO.insertNbpmNodeAlam(node);
				}
			}
			
			
			for(int i = 0 ; i < propertyList.size() ; i++) {
				Map propertyMap = new HashMap();
				String property = (String)propertyList.get(i);
				propertyMap.put("deployId", deployId);
				propertyMap.put("processId", processId);
				propertyMap.put("division_vailable", property);
				deployDAO.insertNbpmProcessVariable(propertyMap);
			}
			
			//nsm.commit();
		} catch (Exception e) {
			throw new NkiaException(e);
			//nsm.rollback();			
		} finally {
			if(in != null){
				in.close();
			}
			//nsm.close();
		}
		
	}

	//@@20130719 김도원 START
	//소스의 재사용성을 위해 반복되는 구문을 메소드로 추출함
	private File createXmlFile(String filename, String data) throws NkiaException, IOException {
		File xmlFile = null;
		OutputStream outStream = null;
		
		try {
			xmlFile = new File(filename);			
			
			outStream = new FileOutputStream(xmlFile);
			// 읽어들일 버퍼크기를 메모리에 생성
			byte[] buf = new byte[1024];
			int len = 0;
			// 끝까지 읽어들이면서 File 객체에 내용들을 쓴다
			outStream.write(data.getBytes());

			/*
		PrintWriter printWriter = new PrintWriter(xmlFile);
		printWriter.write(data);
		printWriter.close();
			 */			
		} finally {
			if (outStream != null) {
				try {
					// Stream 객체를 모두 닫는다.
					outStream.close();
				} catch (Exception e) {
					throw new NkiaException(e);
				}
			}
		}
		return xmlFile;
	}
	//@@20130719 김도원 START

	@Override
	public void deleteProcess(String processId) throws NkiaException {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {
			//nsm.begin();
			long deployId = deployDAO.searchDeployId(processId);
			deployDAO.deleteProcessVariable(deployId);
			deployDAO.deleteProcessNodeDetail(deployId);
			//deployDAO.deleteProcessNodeAlam(deployId);
			deployDAO.deleteProcessDeploy(deployId);
			//nsm.commit();
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	}

	@Override
	public void deleteAllVersionProcess(String processType) throws NkiaException {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {
			//nsm.begin();
			List deployIds = deployDAO.searchDeployIds(processType);
			for(int i = 0 ; i < deployIds.size() ; i++) {
				long deployId = (Long)deployIds.get(i);
				deployDAO.deleteProcessVariable(deployId);
				deployDAO.deleteProcessNodeDetail(deployId);
				deployDAO.deleteProcessNodeAlam(deployId);
				deployDAO.deleteProcessDeploy(deployId);
			}
			//nsm.commit();
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	}
}
