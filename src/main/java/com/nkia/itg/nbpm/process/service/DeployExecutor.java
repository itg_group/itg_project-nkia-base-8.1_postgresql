/*
 * @(#)ProcessDeployExecutor.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.process.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsWorkFlowDeployBean;

public interface DeployExecutor {

	public void createIndexingFile(ArrayList fileList, ModelMap modelMap, String rootPath) throws NkiaException, IOException;
	
	public List searchFinalVesions() throws NkiaException, SQLException;

	public void deployProcess(WsWorkFlowDeployBean wsDeployBean) throws NkiaException, IOException;
	
	public void deleteProcess(String processId) throws NkiaException;
	
	public void deleteAllVersionProcess(String processType) throws NkiaException;
}
