/*
 * @(#)DeployDAO.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.process.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("deployDAO")
public class DeployDAO extends NbpmAbstractDAO {
	
	public void insertNewProcess(Map param) throws NkiaException, SQLException {
		this.getSqlMapClient().insert("NbpmDAO.insertNewProcess", param);
	}

	public void updateTestProcess(Map param) throws NkiaException, SQLException {
		this.getSqlMapClient().update("NbpmDAO.updateTestProcess", param);
	}
	
	public List searchDeployList() throws SQLException {
		return list("NbpmDAO.searchDeployList", null);
	}
	
	public List searchFinalVesions() throws SQLException {
		return list("NbpmDAO.searchFinalVesions", null);
	}
	
	public void insertNbpmNodeDetail(Map paramMap) throws SQLException {
		this.getSqlMapClient().insert("NbpmDAO.insertNbpmNodeDetail", paramMap);
	}
	
	public Integer selectNewDeployId() throws SQLException {
		return (Integer)selectByPk("NbpmDAO.selectNewDeployId", null);
	}

	public void insertNbpmProcessVariable(Map paramMap) throws SQLException {
		this.getSqlMapClient().insert("NbpmDAO.insertNbpmProcessVariable", paramMap);
	}

	public String selectFinalVersion(String param) throws SQLException {
		return (String)selectByPk("NbpmDAO.selectFinalVersion",  param);
	}

	public void insertNbpmNodeAlam(Map paramMap) throws SQLException {
		this.getSqlMapClient().insert("NbpmDAO.insertNbpmNodeAlam", paramMap);
	}

	//@@20130719 김도원 START
	//deploy시에 서브 프로세스 ID를 최신버전을 조회하는 기능 추가 
	public String selectSubProcessId(String subProcessType) throws SQLException {
		return (String)selectByPk("NbpmDAO.selectSubProcessId", subProcessType);
	}
	//@@20130719 김도원 END

	public void deleteProcessDeploy(long deployId) throws SQLException {
		this.getSqlMapClient().delete("NbpmDAO.deleteProcessDeploy", deployId);
	}
	
	public void deleteProcessNodeAlam(long deployId) throws SQLException {
		//this.getSqlMapClient().delete("NbpmDAO.deleteProcessNodeAlam", deployId);
	}
	
	public void deleteProcessNodeDetail(long deployId) throws SQLException {
		this.getSqlMapClient().delete("NbpmDAO.deleteProcessNodeDetail", deployId);
	}
	
	public void deleteProcessVariable(long deployId) throws SQLException {
		this.getSqlMapClient().delete("NbpmDAO.deleteProcessVariable", deployId);
	}

	public List searchDeployIds(String processType) throws SQLException {
		return list("NbpmDAO.searchDeployIds", processType);
	}

	public long searchDeployId(String processId) throws SQLException {
		Object result = selectByPk("NbpmDAO.searchDeployId",  processId);
		if(result == null) {
			result = (long)0;
		}
		return (Long)result;
	}

	public List searchNbpmNodeWorkState(String curruentId) throws SQLException {
		return list("NbpmDAO.searchNbpmNodeWorkState", curruentId);
	}

	public List searchFinalProcessList() throws SQLException {
		return list("NbpmDAO.searchFinalProcessList", null);
	}

	public Map selectBpmnXmlData(String processId) throws SQLException {
		return (Map)selectByPk("NbpmDAO.selectBpmnXmlData", processId);
	}
	
}
