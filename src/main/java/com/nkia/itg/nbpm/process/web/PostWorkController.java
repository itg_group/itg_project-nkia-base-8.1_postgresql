package com.nkia.itg.nbpm.process.web;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.jbpm.task.service.PermissionDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.common.service.NbpmCommonService;
import com.nkia.itg.nbpm.excpetion.NbpmRuleException;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.system.service.SystemService;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class PostWorkController {
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "nbpmCommonService")
	private NbpmCommonService nbpmCommonService;
	
	@Resource(name = "systemService")
	private SystemService systemService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	/**
	 * 요청의 상세정보를 조회한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/selectProcessDetail.do")
	public @ResponseBody ResultVO selectProcessDetail(@RequestBody ModelMap paramMap) throws NkiaException {
		Map resultMap = null;
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			//paramMap <== SR_ID만 세팅되어 있음
			String sr_id = (String)paramMap.get("sr_id");
			List queryKey = (List)paramMap.get("queryKey");
			List subQueryKey = (List)paramMap.get("subQueryKey");
			
			String taskId = (String)paramMap.get("task_id");
			String taskGroupId = (String)paramMap.get("task_group_id");
			String reqType = (String)paramMap.get("req_type");
			String reqUserId = (String)paramMap.get("req_user_id");
			String reqPath = (String)paramMap.get("req_path");
			String requestPop = (String)paramMap.get("request_pop");
			
			HashMap param = new HashMap();
			param.put("sr_id", sr_id);
			param.put("req_type", reqType);
			param.put("req_user_id", reqUserId);
			param.put("req_path", reqPath);
			param.put("request_pop", requestPop);
			if(taskId != null && !"".equals(taskId)) {
				param.put("task_id", Long.parseLong(taskId));
			} else {
				param.put("task_id", 0);
			}
			if(taskGroupId != null && !"".equals(taskGroupId)) {
				param.put("task_group_id", taskGroupId);
			} else {
				param.put("task_group_id", "0");
			}
			
			param.put("queryKey", queryKey);
			param.put("subQueryKey", subQueryKey);
			
			resultMap = processCommonExecutor.selectProcessDetail(param, userVO);
			resultVO.setResultMap((HashMap) resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 요청의 상세정보를 조회한다.외부접속용
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/selectProcessDetail_EC.do")
	public @ResponseBody ResultVO selectProcessDetail_EC(@RequestBody ModelMap paramMap) throws NkiaException {
		Map resultMap = null;
		ResultVO resultVO = new ResultVO();
		try {
			//UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			//paramMap <== SR_ID만 세팅되어 있음
			String sr_id = (String)paramMap.get("sr_id");
			List queryKey = (List)paramMap.get("queryKey");
			List subQueryKey = (List)paramMap.get("subQueryKey");
			
			String taskId = (String)paramMap.get("task_id");
			String taskGroupId = (String)paramMap.get("task_group_id");
			String reqType = (String)paramMap.get("req_type");
			String reqUserId = (String)paramMap.get("req_user_id");
			String reqPath = (String)paramMap.get("req_path");
			String requestPop = (String)paramMap.get("request_pop");
			
			HashMap param = new HashMap();
			param.put("sr_id", sr_id);
			param.put("req_type", reqType);
			param.put("req_user_id", reqUserId);
			param.put("req_path", reqPath);
			param.put("request_pop", requestPop);
			if(taskId != null && !"".equals(taskId)) {
				param.put("task_id", Long.parseLong(taskId));
			} else {
				param.put("task_id", 0);
			}
			if(taskGroupId != null && !"".equals(taskGroupId)) {
				param.put("task_group_id", taskGroupId);
			} else {
				param.put("task_group_id", "0");
			}
			
			param.put("queryKey", queryKey);
			param.put("subQueryKey", subQueryKey);
			
			resultMap = processCommonExecutor.selectProcessDetail_EC(param);
			resultVO.setResultMap((HashMap) resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 상위 연계 요청의 상세정보를 조회한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/selectParentProcessDetail.do")
	public @ResponseBody ResultVO selectParentProcessDetail(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		Map<String, Object> resultMap = null;
		try {
			paramMap.put("login_user_id", userVO.getUser_id());
			paramMap.put("login_user_nm", userVO.getUser_nm());
			resultMap = processCommonExecutor.selectParentProcessDetail(paramMap);
			resultVO.setResultMap((HashMap<String, Object>) resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 연계 요청페이지로 이동한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/goChildInsertPage.do")
	public String goChildInsertPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";
		try {

			String processType = request.getParameter("process_type");
			String up_sr_id = request.getParameter("up_sr_id");
			String subReqType = request.getParameter("sub_req_type");
			String relationPop = request.getParameter("relationPop");
			String subTyCd = request.getParameter("sub_ty_cd");
			/*String setData = request.getParameter("setData");
			if(setData != null){
				 setData = setData.replaceAll("&quot;", "\"");
				 setData = setData.replaceAll("&lt;", "<");
				 setData = setData.replaceAll("&gt;", ">");
				 setData = setData.replaceAll("&apos;", "\'");
			}*/
			
			NbpmProcessVO processVO = processCommonExecutor.selectFinalVersionProcessInfo(processType);
			
			String sr_id = processCommonExecutor.createSrId();
			processVO.setSr_id(sr_id);
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			HashMap<String, Object> dataMap = NbpmUtil.getDataMapForInsertPage(processVO, userVO, relationPop, up_sr_id);
			
			dataMap.put("sub_req_type", subReqType);
			dataMap.put("sub_ty_cd", subTyCd);
			//dataMap.put("setData", setData);
			paramMap.put("dataMap", dataMap);

			String version = processVO.getVersion();
			String requestPage = NbpmConstraints.REQUEST_PAGE.toUpperCase();
			//forwardPath = NbpmUtil.parseForwardPage(processType, version, requestPage);
			forwardPath = NbpmUtil.parseForwardPage(processType, subReqType, version, requestPage);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return forwardPath;
	}
	/*@RequestMapping(value="/itg/nbpm/goChildInsertPage.do")
	public String goChildInsertPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";
		try {

			String processType = request.getParameter("process_type");
			//UBIT커스텀 하나의 프로세스에 sub_req_type을 사용하여 여러 폼으로 나누는 경우 프로세스 type 수정 
			String processPage = request.getParameter("process_type");
			
			boolean processPageType = processPage.startsWith("SERVICE");
			if(processPageType){
				processType = "SERVICE";
			}
			String up_sr_id = request.getParameter("up_sr_id");
			String subReqType = request.getParameter("sub_req_type");
			String relationPop = request.getParameter("relationPop");
			
			NbpmProcessVO processVO = processCommonExecutor.selectFinalVersionProcessInfo(processType);
			
			String sr_id = processCommonExecutor.createSrId();
			processVO.setSr_id(sr_id);
			processVO.setSub_req_type(subReqType);
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			HashMap<String, Object> dataMap = NbpmUtil.getDataMapForInsertPage(processVO, userVO, relationPop, up_sr_id);
			
			paramMap.put("dataMap", dataMap);

			String version = processVO.getVersion();
			String requestPage = NbpmConstraints.REQUEST_PAGE.toUpperCase();
			forwardPath = NbpmUtil.parseForwardPage(processPage, version, requestPage);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return forwardPath;
	}*/
	
	/**
	 * 요청의 등록 페이지로 이동한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/goInsertPage.do")
	public String goInsertPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";
		try {
			//2018.08.09 메뉴접근검증 추가
			if (!urlAccessCheckService.urlAuthCheck(request)) {
				return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
			}
			String subTyCd = request.getParameter("sub_ty_cd");
			String customerId = request.getParameter("customer_id");
			String processType = request.getParameter("process_type");
			String subReqType = request.getParameter("sub_req_type");
			String relationPop = request.getParameter("relationPop");
			String sr_id = request.getParameter("sr_id");
			
			NbpmProcessVO processVO = processCommonExecutor.selectFinalVersionProcessInfo(processType);
			
			if(sr_id == null || "".equals(sr_id)) {
				sr_id = processCommonExecutor.createSrId();
				processVO.setSr_id(sr_id);
			}
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
       		HashMap<String, Object> dataMap = NbpmUtil.getDataMapForInsertPage(processVO, userVO, relationPop, null);
			
       		paramMap.put("processVO", processVO);
       		
       		dataMap.put("sub_req_type", subReqType);
       		
       		dataMap.put("customer_id", customerId);
       		
       		dataMap.put("sub_ty_cd", subTyCd);
       		
       		paramMap.put("dataMap", dataMap);
       		
			String version = processVO.getVersion();
			String requestPage = NbpmConstraints.REQUEST_PAGE.toUpperCase();
			//forwardPath = NbpmUtil.parseForwardPage(processType, version, requestPage);
			forwardPath = NbpmUtil.parseForwardPage(processType, subReqType, version, requestPage);
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
		return forwardPath;
	}
	
	/**
	 * 요청의 상세페이지로 이동한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/goTaskPage.do")
	public String goTaskPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String nbpm_task_id = request.getParameter("nbpm_task_id");
			String claimYn = request.getParameter("claim_yn");
			String listType = request.getParameter("list_type");
			
			if("READY".equals(listType)){
				if(userId != null && !"".equals(userId)) {
					int count = postWorkDAO.isExistNbpmUser(userId);
					if(count == 0) {
						postWorkDAO.insertNbpmUser(userId);
					}
				}
			}else if(listType == null){
				listType = "";
			}
			
			paramMap.put("nbpm_task_id", nbpm_task_id);
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			//타스크 페이지에 필요한 데이터를 수집
			NbpmProcessVO processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			
			if("READY".equals(listType)){
				if(userId.equals(processVO.getCurrent_worker_id())) {
					//데이터를 확인 하였으므로 접수일 시 및 Readed 를 업데이트 한다.
					processCommonExecutor.updateAcceptDate(paramVO);
				}
			}
			//페이지에 대한 사용자의 권한을 체크한다.
			boolean authFlag = checkUserPageAuth(processVO, userVO);
			if (authFlag && !listType.equals("ING")) {
				//포워딩하려는 페이지 경로를 획득한다.
				forwardPath = NbpmUtil.parseForwardPage(
						processVO.getProcess_type(),
						processVO.getSub_req_type(), processVO.getVersion(),
						processVO.getTask_name());
			} else {
				//권한이 없을 경우 뷰전용 페이지로 이동한다.
				processVO.setTask_name(NbpmConstraints.END_VIEW_PAGE);
				forwardPath = NbpmUtil.parseForwardPage(
						processVO.getProcess_type(),
						processVO.getSub_req_type(), processVO.getVersion(),
						NbpmConstraints.END_VIEW_PAGE.toUpperCase());
			}
			
			paramMap.put("processVO", processVO);
			paramMap.put("user_id", userId);
			
			//프로세스 화면 자동화에서 사용하기 위한 dataMpa 데이터를 세팅함.
			HashMap<String, Object> dataMap = NbpmUtil.getDataMapForTaskPage(processVO, userVO);
			dataMap.put("claim_yn", claimYn);
			dataMap.put("sub_req_type", processVO.getSub_req_type());
			dataMap.put("sub_ty_cd", processVO.getReq_doc_id());
			paramMap.put("dataMap", dataMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
		return forwardPath;
	}	
	
	/**
	 * UBIT 센터출입대기합  커스텀
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/goCenterTaskPage.do")
	public String goCenterTaskPage(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String nbpm_task_id = request.getParameter("nbpm_task_id");
			String claimYn = request.getParameter("claim_yn");
			
			paramMap.put("nbpm_task_id", nbpm_task_id);
			NbpmProcessVO paramVO = NbpmUtil.setProcessParameter(paramMap);
			//타스크 페이지에 필요한 데이터를 수집
			NbpmProcessVO processVO = processCommonExecutor.selectTaskPageInfo(paramVO);
			
			if(userId.equals(processVO.getCurrent_worker_id())) {
				//데이터를 확인 하였으므로 접수일 시 및 Readed 를 업데이트 한다.
				processCommonExecutor.updateAcceptDate(paramVO);
			}
			
			if(processVO.getWork_state().equals("END")){
				processVO.setTask_name("END_VIEW");
			}

			forwardPath = NbpmUtil.parseForwardPage(
					processVO.getProcess_type(),
					processVO.getSub_req_type(), processVO.getVersion(),
					processVO.getTask_name());
			
			paramMap.put("processVO", processVO);
			paramMap.put("user_id", userId);
			
			//프로세스 화면 자동화에서 사용하기 위한 dataMpa 데이터를 세팅함.
			HashMap<String, Object> dataMap = NbpmUtil.getDataMapForTaskPage(processVO, userVO);
			dataMap.put("claim_yn", claimYn);
			dataMap.put("sub_req_type", processVO.getSub_req_type());
			dataMap.put("sub_ty_cd", processVO.getReq_doc_id());
			paramMap.put("dataMap", dataMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
		return forwardPath;
	}	
	
	/**
	 * 요청의 상세페이지 이동 시 로그인 사용자의 권한의 체크한다. 
	 * @param processVO
	 * @param userVO
	 * @return
	 */
	private boolean checkUserPageAuth(NbpmProcessVO processVO, UserVO userVO) {
		boolean result = false;
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(processVO.getStatus() != null && processVO.getCurrent_worker_id() != null) {
			if(!"Completed".equals(processVO.getStatus()) && userVO.getUser_id().equals(processVO.getCurrent_worker_id())) {
				result = true;
			}
		}		
		
		return result;
	}
	
	
	/**
	 * 요청을 임시등록한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/registerTemporarily.do")
	public @ResponseBody ResultVO registerTemporarily(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			
			Map result = processCommonExecutor.registerTemporarily(processVO, paramMap);
			resultVO.setResultMsg("등록되었습니다.");
			resultVO.setResultMap((HashMap) result); 
			
		} catch(PermissionDeniedException e) {
			message = "담당자가 아닙니다.\n확인 후 처리바랍니다.";
			throw new NkiaException(message);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
				
	}
		
	/**
	 * 요청의 처리내용을 임시저장한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/processTemporarily.do")
	public @ResponseBody ResultVO processTemporarily(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			
			Map result = processCommonExecutor.processTemporarily(processVO, paramMap);
			resultVO.setResultMsg("등록되었습니다.");
			resultVO.setResultMap((HashMap) result); 
			
		} catch(PermissionDeniedException e) {
			message = "담당자가 아닙니다.\n확인 후 처리바랍니다.";
			throw new NkiaException(message);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 요청을 처리한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/processRequest.do")
	public @ResponseBody ResultVO processRequest(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			//타스크의 상태가 처리 가능한 상태인지 확인한다.(이미 처리된 것은 아닌지, 본인이 담당자가 맞는지)
			boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
			if(workCheck) {
				//화면에서 넘어온 담당자 정보 중 없는 BPM User 테이블에 데이터가 있는지 체크하여 등록한다.
				processCommonExecutor.registerUser(processVO, paramMap);
				//요청을 처리한다.
				Map result = processCommonExecutor.executeTask(processVO, paramMap);
				//새로 생성된 타스크들의 알림을 실행한다.
				processCommonExecutor.notifyTask(processVO, result);			
				resultVO.setResultMsg("등록되었습니다.");
				resultVO.setResultMap((HashMap) result);
			} else {
				HashMap result = new HashMap();
				result.put("selfWork", false);
				resultVO.setResultMap((HashMap) result);
				resultVO.setResultMsg("처리상태 확인이 필요합니다.\n목록을 갱신하고 재시도 하여 주세요");				
			}
			resultVO.setSuccess(true);
		} catch(PermissionDeniedException e) {
			throw new NkiaException("담당자가 아닙니다.\n확인 후 처리바랍니다.");
		} catch(NbpmRuleException e) {
			resultVO.setResultMsg(ExceptionUtils.getRootCause(e).getMessage());
			resultVO.setSuccess(false);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 하위 요청을 등록한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/registerChildProcess.do")
	public @ResponseBody ResultVO registerChildProcess(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			
			processCommonExecutor.registerUser(processVO, paramMap);
			Map result = processCommonExecutor.registerChildProcess(processVO, paramMap);		
			
			resultVO.setResultMsg("등록되었습니다.");
			resultVO.setResultMap((HashMap) result);
			
		} catch(PermissionDeniedException e) {
			message = "담당자가 아닙니다.\n확인 후 처리바랍니다.";
			resultVO.setSuccess(false);
			resultVO.setResultMsg(message);
		} catch(Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 서비스데스크에서 요청을 접수한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/process/acceptGroupAssignTask.do")
	public @ResponseBody ResultVO acceptGroupAssignTask(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			String taskId = (String)paramMap.get("task_id");
			NbpmProcessVO processVO = new NbpmProcessVO();
			processVO.setTask_id(Long.parseLong(taskId));
			
			// dhk 2017-05-16  그룹 티켓에서 접수시 담당자 지정 
			processVO.setWorker_user_id(loginUserId);
			
			boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
			if(workCheck) {
				processCommonExecutor.registerUser(processVO, paramMap);
				processCommonExecutor.changeTaskWorker(processVO.getTask_id(), loginUserId);
				processCommonExecutor.updateRceptDt(paramMap);
				resultVO.setSuccess(true);
			} else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg("처리상태 확인이 필요합니다.\n목록을 갱신하고 재시도 하여 주세요");
			}
			
		} catch(PermissionDeniedException e) {
			message = "담당자가 아닙니다.\n확인 후 처리바랍니다.";
			resultVO.setSuccess(false);
			resultVO.setResultMsg(message);
		} catch(Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/process/releaseGroupAssignTask.do")
	public @ResponseBody ResultVO releaseGroupAssignTask(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			String taskId = (String)paramMap.get("task_id");
			NbpmProcessVO processVO = new NbpmProcessVO();
			processVO.setTask_id(Long.parseLong(taskId));
			
			boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
			if(workCheck) {
				processCommonExecutor.registerUser(processVO, paramMap);
				processCommonExecutor.releaseTaskWorker(processVO.getTask_id(), loginUserId);
				resultVO.setSuccess(true);
			} else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg("처리상태 확인이 필요합니다.\n목록을 갱신하고 재시도 하여 주세요");
			}
			
		} catch(PermissionDeniedException e) {
			message = "담당자가 아닙니다.\n확인 후 처리바랍니다.";
			resultVO.setSuccess(false);
			resultVO.setResultMsg(message);
		} catch(Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 요청을 연관장애로 등록한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/processRelInc.do")
	public @ResponseBody ResultVO processRelInc(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);

			processCommonExecutor.registerUser(processVO, paramMap);
			Map result = processCommonExecutor.processRelInc(processVO, paramMap);
			
			resultVO.setResultMsg("등록되었습니다.");
			resultVO.setResultMap((HashMap) result);
			
		} catch(PermissionDeniedException e) {
			message = "담당자가 아닙니다.\n확인 후 처리바랍니다.";
			resultVO.setSuccess(false);
			resultVO.setResultMsg(message);
		} catch(Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
		
	/**
	 * 요청화면을 미리보기한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/goPreviewDesign.do")
	public String goPreviewDesign(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		String forwardPath = "";
		try {
			HashMap dataMap = new HashMap();
			dataMap.put("preview", true);
			dataMap.put("nbpm_process_type", "preview"+File.separator+(String)request.getParameter("preview_date"));
			paramMap.put("dataMap", dataMap);
			forwardPath = NbpmConstraints.NBPM_PATH + File.separator + "preview" + File.separator + (String)request.getParameter("preview_date") + File.separator + (String)request.getParameter("preview_name");
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
		return forwardPath;
	}
	
	/**
	 * 요청을 일선처리한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/firstCompRequest.do")
	public @ResponseBody ResultVO firstCompRequest(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			
			boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
			if(workCheck) {
				processCommonExecutor.registerUser(processVO, paramMap);
				paramMap.put("first_yn", "Y");
				Map result = processCommonExecutor.executeTask(processVO, paramMap);
				processCommonExecutor.notifyTask(processVO, result);
				
				resultVO.setResultMsg("등록되었습니다.");
				resultVO.setResultMap((HashMap) result);
			} else {
				HashMap result = new HashMap();
				result.put("selfWork", false);
				resultVO.setResultMap((HashMap) result); 
				resultVO.setResultMsg("처리상태 확인이 필요합니다.\n목록을 갱신하고 재시도 하여 주세요");				
			}
			
		} catch(PermissionDeniedException e) {
			throw new NkiaException("담당자가 아닙니다.\n확인 후 처리바랍니다.");
		} catch(NbpmRuleException e) {
			resultVO.setSuccess(true);
			resultVO.setResultMsg(ExceptionUtils.getRootCause(e).getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 요청을 반려종료한다.
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/rejectEndProc.do")
	public @ResponseBody ResultVO rejectEndProc(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			
			boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
			if(workCheck) {	
				processCommonExecutor.registerUser(processVO, paramMap);
				Map result = processCommonExecutor.executeTask(processVO, paramMap);
				processCommonExecutor.notifyTask(processVO, result);
				
				resultVO.setResultMsg("등록되었습니다.");
				resultVO.setResultMap((HashMap) result);
			} else {
				HashMap result = new HashMap();
				result.put("selfWork", false);
				resultVO.setResultMap((HashMap) result); 
				resultVO.setResultMsg("처리상태 확인이 필요합니다.\n목록을 갱신하고 재시도 하여 주세요");				
			}
			
		} catch(PermissionDeniedException e) {
			throw new NkiaException("담당자가 아닙니다.\n확인 후 처리바랍니다.");
		} catch(NbpmRuleException e) {
			resultVO.setSuccess(true);
			resultVO.setResultMsg(ExceptionUtils.getRootCause(e).getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/createMultiTask.do")
	public @ResponseBody ResultVO createMultiTask(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id((String)paramMap.get("worker_user_id"));
			
			processCommonExecutor.registerUser(processVO, paramMap);
			Map result = processCommonExecutor.createMultiTask(processVO, paramMap);
			processCommonExecutor.notifyTask(processVO, result);
			String resultMsg = (String)result.get("result");
			
			if("fail".equals(resultMsg)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg("이미 생성되었습니다.");
				resultVO.setResultMap((HashMap) result);			
			} else {
				resultVO.setSuccess(true);
				resultVO.setResultMsg("등록되었습니다.");
				resultVO.setResultMap((HashMap) result);			
			}
			
		} catch(PermissionDeniedException e) {
			throw new NkiaException(e);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티 작업자를 조회한다.
	 * (일괄발행을 위한 정보)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchMultiTaskGrid.do")
	public @ResponseBody ResultVO searchMultiTaskGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try{
			//String eventName = (String)paramMap.get("event_name");
//			resultList = nbpmCommonService.searchMultiTaskGrid(paramMap);
			
//			if(resultList.size() == 0){
//				Map searchMap = new HashMap();
//				searchMap.put("sr_id", (String)paramMap.get("sr_id"));
				//searchMap.put("task_name", (String)paramMap.get("nbpm_task_name"));
				//searchMap.put("processid", (String)paramMap.get("nbpm_processId"));
				//searchMap.put("event_name", eventName);
				
				resultList = processCommonExecutor.selectMultiUsers(paramMap);		
//			}
	
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티 작업자를 추가한다. 
	 * (일괄발행을 위한 정보)
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/insertMultiTempInfo.do")
	public @ResponseBody ResultVO insertMultiTempInfo(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			processCommonExecutor.insertMultiTempInfo(paramMap);
			resultVO.setSuccess(true);
			resultVO.setResultMsg("등록되었습니다.");			
			
		} catch(PermissionDeniedException e) {			
			throw new NkiaException(e);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티 작업자를 삭제한다. 
	 * (일괄발행을 위한 정보)
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/deleteMultiTempInfo.do")
	public @ResponseBody ResultVO deleteMultiTempInfo(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			processCommonExecutor.deleteMultiTempInfo(paramMap);
			resultVO.setSuccess(true);
			resultVO.setResultMsg("삭제되었습니다.");			
			
		} catch(PermissionDeniedException e) {			
			throw new NkiaException(e);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/cancelMultiTask.do")
	public @ResponseBody ResultVO cancelMultiTask(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id((String)paramMap.get("worker_user_id"));
			
			processCommonExecutor.cancelMultiTask(processVO, paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("삭제되었습니다.");			
			
		} catch(PermissionDeniedException e) {			
			throw new NkiaException(e);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/registerSanctionLineInfo.do")
	public @ResponseBody ResultVO registerSanctionLineInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();

			processCommonExecutor.registerSanctionLineInfo(paramMap);
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/process/selectSelfProcessAuth.do")
	public @ResponseBody ResultVO selectSelfProcessAuth(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			String processType = (String)paramMap.get("process_type");
			String operType = (String)paramMap.get("oper_type");
			String userId = userVO.getUser_id();
			
			boolean isProcAuth = processCommonExecutor.selectProcAuth(userId, processType, operType);
			if(isProcAuth) {
				Map<String, String> userInfo = new HashMap<String, String>();
				userInfo.put("userId", userId);
				userInfo.put("userNm", userVO.getUser_nm());
				resultVO.setSuccess(true);
				resultVO.setResultMap(userInfo);
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 지정 정보를 조회한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/searchReqChargerInfo.do")
	public @ResponseBody ResultVO searchReqChargerInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = null;
		resultMap = new HashMap();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String sr_id = (String)paramMap.get("sr_id");
			String processType = (String)paramMap.get("process_type");
			
       		// 담당자 지정 로직 추가
       		paramMap.put("sr_id", sr_id);
       		paramMap.put("process_type", processType);
       		
       		List resultList = processCommonExecutor.registerReqCharGerInfo(paramMap);
       		resultMap.put("resultList", resultList);
       		
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자유형별 담당자그룹 매핑을 위한 조회
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/process/selectGrpAppoint.do")
	public @ResponseBody ResultVO selectGrpAppoint(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Map resultMap = null;
			
			String processType = (String)paramMap.get("process_type");
			String operType = (String)paramMap.get("oper_type");
			resultMap = nbpmCommonService.selectGrpAppoint(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMap((HashMap) resultMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 회수 처리 - 1안
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	
	@RequestMapping(value="/itg/nbpm/processRecall.do")
	public @ResponseBody ResultVO processRecall(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			
			//String curTaskId = String.valueOf(paramMap.get("nbpm_task_id"));
			
			//타스크의 상태가 처리 가능한 상태인지 확인한다.(이미 처리된 것은 아닌지, 본인이 담당자가 맞는지)
			boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
			if(workCheck) {
				//화면에서 넘어온 담당자 정보 중 없는 BPM User 테이블에 데이터가 있는지 체크하여 등록한다.
				//processCommonExecutor.registerUser(processVO, paramMap);
				//요청을 처리한다.
				Map result = processCommonExecutor.executeTask(processVO, paramMap);
				processCommonExecutor.hideRecallTask(paramMap);
				//새로 생성된 타스크들의 알림을 실행한다.
				//processCommonExecutor.notifyTask(processVO, result);			
				resultVO.setResultMsg("등록되었습니다.");
				resultVO.setResultMap((HashMap) result);
			} 
			resultVO.setSuccess(true);
		} catch(PermissionDeniedException e) {
			throw new NkiaException("담당자가 아닙니다.\n확인 후 처리바랍니다.");
		} catch(NbpmRuleException e) {
			resultVO.setResultMsg(ExceptionUtils.getRootCause(e).getMessage());
			resultVO.setSuccess(false);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 회수 처리 - 2안
	 * @param paramMap
	 * @param request
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/processRecallModify.do")
	public @ResponseBody ResultVO processRecallModify(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			processVO.setWorker_user_id(loginUserId);
			
			Map dataMap = WebUtil.lowerCaseMapKey(paramMap);
			dataMap.put("task_status","Recall");
			processCommonExecutor.updateTaskStatus(dataMap);
						
			resultVO.setResultMsg("등록되었습니다.");
			
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
