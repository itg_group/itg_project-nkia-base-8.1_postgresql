package com.nkia.itg.nbpm.process.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

@Repository("postWorkDAO")
public class PostWorkDAO extends NbpmAbstractDAO {
	
	public String selectFinalVersion(String process_name) {
		String result = (String)selectByPk("NbpmDAO.selectFinalVersion", process_name);
		return result;
	}

	public NbpmProcessVO selectFinalVersionProcessInfo(Map paramMap) {
		return (NbpmProcessVO)selectByPk("NbpmDAO.selectFinalVersionProcessInfo", paramMap);
	}
	
	public void insertProcessMaster(Map paramMap) {
		insert("NbpmDAO.insertProcessMaster", paramMap);
	}

	public void updateProcessMaster(Map paramMap) {
		update("NbpmDAO.updateProcessMaster", paramMap);
	}

	public Map selectProcessDetailInfo(String queryKey, Map paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("ProcDetail." + queryKey, paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}	
		return handler.getReturnMap();
	}
	
	public void insertProcessOrerator(Map operInfo) {
		insert("NbpmDAO.insertProcessOrerator", operInfo);
		
	}

	public void updateProcessOrerator(Map operInfo) {
		update("NbpmDAO.updateProcessOrerator", operInfo);
	}

	public List searchProcessOrerator(Map operInfo) {
		return list("NbpmDAO.searchProcessOrerator", operInfo);
	}

	public List searchRelConf(Map itemInfo) {
		return list("NbpmDAO.searchRelConf", itemInfo);
	}
		
	public String createSrId() {
		return (String)selectByPk("NbpmDAO.createSrId", null);
	}
	
	public String createSrId(String prefix) {
		return (String)selectByPk("NbpmDAO.createPrefixId", prefix);
	}

	public NbpmProcessVO selectTaskPageInfo(NbpmProcessVO paramVO) {
		return (NbpmProcessVO)selectByPk("NbpmDAO.selectTaskPageInfo", paramVO);
	}

	public void updateWorkState(Map paramMap) {
		update("NbpmDAO.updateWorkState", paramMap);
	}
	
	public void updateEndWorkState(Map paramMap) {
		update("NbpmDAO.updateEndWorkState", paramMap);
	}

	public List searchRoleList(NbpmProcessVO processVO) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("NbpmDAO.searchRoleList", processVO, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
	}

	public void deleteProcessOrerator(Map operInfo) {
		delete("NbpmDAO.deleteProcessOrerator", operInfo);
	}

	
	public void excuteProcessDetail(String queryKey, Map paramMap) {
		update("ProcDetail." + queryKey, paramMap);
	}

	public Map selectProcessDetail(String sr_id) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("NbpmDAO.selectProcessDetail", sr_id, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnMap();
	}

	public List selectProcessOperList(Map paramMap) {
		return list("NbpmDAO.selectProcessOperList", paramMap);
	}

	public List searchTaskCommentList(String sr_id) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("NbpmDAO.searchTaskCommentList", sr_id, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
	}

	public void updateUpDupSrId(Map paramMap) {
		update("NbpmDAO.updateUpDupSrId", paramMap);
	}

	public void updateEndDuplication(Map paramMap) {
		update("NbpmDAO.updateEndDuplication", paramMap);
	}

	public void insertProcessItem(Map itemInfo) {
		insert("NbpmDAO.insertProcessItem", itemInfo);
	}
	
	public void deleteProcessItem(String srId) {
		insert("NbpmDAO.deleteProcessItem", srId);
	}
	
	public List searchNbpmProcessVariable(String processId) {
		return list("NbpmDAO.searchNbpmProcessVariable", processId);
	}

	public void updateProcMasterProcId(Map paramMap) {
		update("NbpmDAO.updateProcMasterProcId", paramMap);
	}
	
	public List searchNbpmAlamList(NbpmProcessVO processVO) {
		return list("NbpmDAO.searchNbpmAlamList", processVO);
	}

	public String selectNextNodeName(NbpmProcessVO processVO) {
		return (String)selectByPk("NbpmDAO.selectNextNodeName", processVO);
	}

	public String selectLastTaskId(String srId) {
		return (String)selectByPk("NbpmDAO.selectLastTaskId", srId);
	}

	public List searchWorkerTaskList(String sr_id) {
		return list("NbpmDAO.searchWorkerTaskList", sr_id);
	}

	public List searchViewerTaskList(String sr_id) {
		return list("NbpmDAO.searchViewerTaskList", sr_id);
	}

	public Map selectCurrentWorkState(NbpmProcessVO processVO) {
		return (Map)selectByPk("NbpmDAO.selectCurrentWorkState", processVO);
	}

	public int selectExistSrId(String srId) {
		return (Integer)selectByPk("NbpmDAO.selectExistSrId", srId);
	}

	public NbpmProcessVO selectRequestPageInfo(NbpmProcessVO paramVO) {
		return (NbpmProcessVO)selectByPk("NbpmDAO.selectRequestPageInfo", paramVO);
	}

	public int selectExistSrIdInDetailTable(String checkQuery, Map paramMap) {
		return (Integer)selectByPk("ProcDetail." + checkQuery, paramMap);
	}
	
	public long selectCommentId(NbpmProcessVO processVO) {
		return (Integer)selectByPk("NbpmDAO.selectCommentId", processVO);
	}

	public String searchTaskCurrentComment(Map param) {
		return (String)selectByPk("NbpmDAO.searchTaskCurrentComment", param);
	}

	public void updateChildProcMasterProcId(Map param) {
		update("NbpmDAO.updateChildProcMasterProcId", param);
	}

	public String selectProcessId(long subProcessInstId) {
		return (String)selectByPk("NbpmDAO.selectProcessId", subProcessInstId);
	}

	public Map selectProcessInfo(NbpmProcessVO processVO) {
		return (Map)selectByPk("NbpmDAO.selectProcessInfo", processVO);
	}
	
	public void deleteProcMaster(Map paramMap) {
		delete("NbpmDAO.deleteProcMaster", paramMap);
	}

	public void deleteProcDetail(String queryKey, Map paramMap) {
		delete("ProcDetail." + queryKey, paramMap);
	}

	public Map selectParentProcessDetail(String upSrId) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("NbpmDAO.selectParentProcessDetail", upSrId, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnMap();
	}

	public void updateParentRelationField(Map paramMap) {
		update("NbpmDAO.updateParentRelationField", paramMap);
	}

	public void updateReqTypeForInit(Map srData) {
		update("NbpmDAO.updateReqTypeForInit", srData);
	}

	public void deleteProcDetailForChangeReqType(Map paramMap) {
		delete("NbpmDAO.deleteProcDetailForChangeReqType", paramMap);
	}
	
	public List searchNodeList(NbpmProcessVO processVO) {
		return list("NbpmDAO.searchNodeList", processVO);
	}

	public List searchRelIncList(String sr_id) {
		return list("NbpmDAO.searchRelIncList", sr_id);
	}

	public NbpmProcessVO checkParentProcessWorkState(Map params) {
		return (NbpmProcessVO)selectByPk("NbpmDAO.checkParentProcessWorkState", params);
	}

	public int checkTaskStatus(NbpmProcessVO processVO) {
		return (Integer)selectByPk("NbpmDAO.checkTaskStatus", processVO);
	}
	
	public void deleteProcTableData(Map paramMap) {
		delete("NbpmDAO.deleteProcTableData", paramMap);
	}	

	public String selectCurrentTaskName(String srId) {
		return (String)selectByPk("NbpmDAO.selectCurrentTaskName", srId);
	}

	public void insertMultiTaskInfo(Map paramMap) {
		insert("NbpmDAO.insertMultiTaskInfo", paramMap);
	}

	public void insertMultiTaskTableInfo(Map paramMap) {
		insert("NbpmDAO.insertMultiTaskTableInfo", paramMap);
		
	}

	public List searchUncompletedTask(long procInstId) {
		return list("NbpmDAO.searchUncompletedTask", procInstId);
	}

	public int selectMultiTicketCnt(Map paramMap) {
		return (Integer)selectByPk("NbpmDAO.selectMultiTicketCnt", paramMap);
	}
	
	public String selectTaskNodeName(Map paramMap) {
		return (String)selectByPk("NbpmDAO.selectTaskNodeName", paramMap);
	}
	
	public int isExistNbpmUser(String userId) {
		return (Integer)selectByPk("NbpmDAO.isExistNbpmUser", userId);
	}

	public void insertNbpmUser(String userId) {
		insert("NbpmDAO.insertNbpmUser", userId);
	}

	public List selectOldTaskList(long processInstanceId) {
		return (List)list("NbpmDAO.selectOldTaskList", processInstanceId);
	}
	
	public List selectNewTaskList(List oldTaskList) {
		return list("NbpmDAO.selectNewTaskList", oldTaskList);
	}

	public Long selectMultiTaskInfo(Long taskId) {
		return (Long)selectByPk("NbpmDAO.selectMultiTaskInfo", taskId);
	}

	public void updateTaskGroupInfo(Map paramMap) {
		update("NbpmDAO.updateTaskGroupInfo", paramMap);
	}

	public void updateOperUserInfo(Map operInfo) {
		update("NbpmDAO.updateOperUserInfo", operInfo);
		
	}

	public int selectOperUserInfo(Map operInfo) {
		return (Integer)selectByPk("NbpmDAO.selectOperUserInfo", operInfo);
	}

	public Map selectTaskForClaim(long taskId) {
		return (Map)selectByPk("NbpmDAO.selectTaskForClaim", taskId);
	}

	public int selectOwnProcAuthCount(Map<String, String> paramMap) {
		return (Integer)selectByPk("NbpmDAO.selectOwnProcAuthCount", paramMap);
	}

	public void deleteMultiTaskProcOper(Map param) {
		delete("NbpmDAO.deleteMultiTaskProcOper", param);
	}

	public void deleteCanceledTask(Map param) {
		delete("NbpmDAO.deleteCanceledTask", param);
	}
	
	public void deleteMultiTaskInfo(Map param) {
		delete("NbpmDAO.deleteMultiTaskInfo", param);
	}	
	
	public void insertGrpNbpmUser(String userId) {
		this.insert("NbpmDAO.insertGrpNbpmUser", userId);
	}
	
	public List searchReqCharGerInfo(Map paramMap) throws NkiaException {
		return list("NbpmDAO.searchReqCharGerInfo", paramMap);
	}
	
	public List selectMultiTempUsers(Map paramMap) throws NkiaException {
		return list("NbpmDAO.selectMultiTempUsers", paramMap);
	}
	
	/**
	 * PROC_MASTER 정보 조회 - SR_ID, PROC_ID, REQ_TYPE, REQ_USER_ID 
	 * @param sr_id
	 * @return
	 */
	public Map selectSimpleProcessDetail(String sr_id) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("NbpmDAO.selectSimpleProcessDetail", sr_id, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnMap();
	}
	
	//알림 정보 조회
	public Map selectNextTaskAlamInfo(NbpmProcessVO processVO) {
		return (Map)selectByPk("NbpmDAO.selectNextTaskAlamInfo", processVO);
	}
	
	//알림 정보 조회(담당자변경)
	public Map selectNextTaskAlamInfo_workedit(NbpmProcessVO processVO) {
		return (Map)selectByPk("NbpmDAO.selectNextTaskAlamInfo_workedit", processVO);
	}
	
	//sms 수신자 정보 조회
	public List searchNbpmAlamSmsList(NbpmProcessVO processVO) {
		return list("NbpmDAO.searchNbpmAlamSmsList", processVO);
	}
	
	//sms 수신자 정보 조회(담당자변경)
	public List searchNbpmAlamSmsList_workedit(NbpmProcessVO processVO) {
		return list("NbpmDAO.searchNbpmAlamSmsList_workedit", processVO);
	}
	
	//이메일 수신자 정보 조회
	public List searchNbpmAlamEmailList_workedit(NbpmProcessVO processVO) {
		return list("NbpmDAO.searchNbpmAlamEmailList_workedit", processVO);
	}
	
	//이메일 수신자 정보 조회
	public List searchNbpmAlamEmailList(NbpmProcessVO processVO) {
		return list("NbpmDAO.searchNbpmAlamEmailList", processVO);
	}
	
	public List selectMultiUsers(Map paramMap) throws NkiaException {
		return list("NbpmDAO.selectMultiUsers", paramMap);
	}
	
	public int selectMultiUserCnt(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("NbpmDAO.selectMultiUserCnt", paramMap);
	}

	public void deleteMultiTempInfo(Map paramMap) throws NkiaException {
		delete("NbpmDAO.deleteMultiTempInfo", paramMap);
	}

	public void insertMultiTempInfo(Map paramMap) throws NkiaException {
		insert("NbpmDAO.insertMultiTempInfo", paramMap);
	}

	public void updateMultiTemp(Map paramMap) throws NkiaException {
		update("NbpmDAO.updateMultiTemp", paramMap);
	}
	
	public Map selectRceptInfo(ModelMap paramMap) {
		return (Map)selectByPk("NbpmDAO.selectRceptInfo", paramMap);
	}
	
	public void updateRceptDt(ModelMap paramMap) throws NkiaException {
		update("NbpmDAO.updateRceptDt", paramMap);
	}
	public String selectProcessIdTaskId(long id) {
		return (String)selectByPk("NbpmDAO.selectProcessIdTaskId", id);
	}
	public List searchWorkerTaskListEndEdit(String sr_id) {
		return list("NbpmDAO.searchWorkerTaskListEndEdit", sr_id);
	}

	public List searchViewerTaskListEndEdit(String sr_id) {
		return list("NbpmDAO.searchViewerTaskListEndEdit", sr_id);
	}	
	
	public void forceQuitProcMaster(Map paramMap) {
		update("NbpmDAO.forceQuitProcMaster", paramMap);
	}

	public List forceQuitTaskList(Map paramMap) {
		return list("NbpmDAO.forceQuitTaskList", paramMap);
	}	
	
	public void forceQuitTask(Map paramMap) {
		update("NbpmDAO.forceQuitTask", paramMap);
	}
	
	public String selectReservedTaskId(Map paramMap) {
		return (String)selectByPk("NbpmDAO.selectReservedTaskId", paramMap);
	}
	
	public void updateRecallTaskHide(Map paramMap) {
		update("NbpmDAO.updateRecallTaskHide", paramMap);
	}
	
	public Map selectPreTaskId(Map paramMap) {
		return (Map)selectByPk("NbpmDAO.selectPreTaskId", paramMap);
	}
	
	public void updateNextTaskInfo(Map paramMap) {
		update("NbpmDAO.updateNextTaskInfo", paramMap);
	}
	
	public void updateRecallTaskInfo(Map paramMap) {
		update("NbpmDAO.updateRecallTaskInfo", paramMap);
	}
	
	public void updateTaskStatus(Map paramMap) {
		update("NbpmDAO.updateTaskStatus", paramMap);
	}
	
	public String selectTaskStatus(Map paramMap) {
		return (String)selectByPk("NbpmDAO.selectTaskStatus", paramMap);
	}
}