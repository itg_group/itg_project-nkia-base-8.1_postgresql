package com.nkia.itg.nbpm.process.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jbpm.task.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.SmsSendService;
import com.nkia.itg.certification.dao.CertificationDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.customize.ubit.service.CustomizeUbitService;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.excpetion.NbpmMultiTaskException;
import com.nkia.itg.nbpm.excpetion.NbpmRuleException;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.AcceptDateTaskCmd;
import com.nkia.itg.nbpm.executor.AfterWorkCmd;
import com.nkia.itg.nbpm.executor.ChangeWorkerCmd;
import com.nkia.itg.nbpm.executor.CheckTasUserkCmd;
import com.nkia.itg.nbpm.executor.EmailSendedCommand;
import com.nkia.itg.nbpm.executor.ExitTaskCmd;
import com.nkia.itg.nbpm.executor.GetTaskInfoCmd;
import com.nkia.itg.nbpm.executor.WorkMultiTicketCmd;
import com.nkia.itg.nbpm.executor.WorkRuleTaskCmd;
import com.nkia.itg.nbpm.executor.WorkTaskCmd;
import com.nkia.itg.nbpm.preprocessor.PreProcessExcutor;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.rule.common.invoker.RuleInvoker;
import com.nkia.itg.nbpm.servicedesk.dao.ServicedeskDAO;
import com.nkia.itg.nbpm.util.NbpmPortalPushSender;
import com.nkia.itg.nbpm.util.NbpmSmsSender;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.code.dao.CommonCodeDAO;
import com.nkia.itg.system.user.dao.UserDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("processCommonExecutor")
public class PostWorkCommonExecutor implements PostWorkExecutor {
	
	private static final Logger logger = LoggerFactory.getLogger(PostWorkCommonExecutor.class);
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name = "userDAO")
	public UserDAO userDAO;
	
	@Resource(name = "servicedeskDAO")
	private ServicedeskDAO servicedeskDAO;
	
	@Resource(name = "nbpmCommonDAO")
	private NbpmCommonDAO nbpmCommonDAO;
	
	@Resource(name = "commonCodeDAO")
	public CommonCodeDAO commonCodeDAO;

	@Resource(name="certificationDAO")
	public CertificationDAO certificationDAO;
	
	@Resource(name="customizeUbitService")
	private CustomizeUbitService customizeUbitService;
	
	
	/* 
	 * 임시 등록을 실시한다. JBPM과의 연동을 하지 않는다.
	 */
	public Map registerTemporarily(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		Map resultMap = new HashMap();
		Map returnMap = new HashMap();
		try {
			
			//데이터 처리를 위한 변수 세팅
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String task_name = "";
			if(processVO.getTask_name() != null) {
				task_name = processVO.getTask_name();
			}
			
			List itemList = processVO.getItemList();
			List operList = processVO.getOperList();
			String queryKey = (String)paramMap.get("queryKey");
			
			//SR_ID 기준으로 데이터가 있는지 확인(임시등록은 몇번이고 가능하다.)
			String srId = processVO.getSr_id();
			boolean isExist = checkExistSrId(srId);
			
			//기존에 등록된 데이터가 없는 경우
			if(!isExist) {
				//PROC_MASTER 데이터 등록
				paramMap.put("reg_user_id", processVO.getWorker_user_id());
				postWorkDAO.insertProcessMaster(paramMap);
				
				//PROC_OPER 테이블에 요청자정보 등록
				Map requestorInfo = new HashMap();
				requestorInfo.put("oper_user_id", processVO.getWorker_user_id());
				requestorInfo.put("sr_id", processVO.getSr_id());
				requestorInfo.put("oper_type", NbpmConstraints.REQUESTOR_ROLE);
				requestorInfo.put("select_type", "SINGLE");
				postWorkDAO.insertProcessOrerator(requestorInfo);
				
				//담당자 정보가 추가로 있다면 등록
				for(int i = 0 ; operList != null && i < operList.size() ; i++) {
					Map operInfo = (Map)operList.get(i);
					operInfo.put("sr_id",processVO.getSr_id());
					postWorkDAO.insertProcessOrerator(operInfo);
				}
				
				//연계 Item(Conf, Service) 정보 등록
				for(int i = 0 ; itemList != null && i < itemList.size() ; i++) {
					Map itemInfo = (Map)itemList.get(i);
					itemInfo.put("sr_id", processVO.getSr_id());
					postWorkDAO.insertProcessItem(itemInfo);
				}
			} else {
				//데이터가 있을 경우에는 PROC_MASTER를 수정한다.
				paramMap.put("upd_user_id", processVO.getWorker_user_id());
				postWorkDAO.updateProcessMaster(paramMap);
				//재지정된 담당자정보를 수정하기 위해 실행한다.
				checkReassignOperUser(operList);
				//담당자 정보 수정
				updateOperatorInfo(processVO, operList);
				//Item(Conf, Service) 정보 수정
				updateProcessItemInfo(processVO, itemList);
			}
			
			//상세 테이블 (PROC_CHANGE, PROC_SERVICE)
			if(queryKey != null) {
				String[] querySet = queryKey.split(",");
				String checkQuery = null;
				String insertQuery = null;
				String updateQuery = null;
				for(int i = 0 ; i < querySet.length ; i++) {
					if(querySet[i].indexOf("select") != -1) {
						checkQuery = querySet[i];						
					} else if(querySet[i].indexOf("insert") != -1) {
						insertQuery = querySet[i];
					} else if(querySet[i].indexOf("update") != -1) {
						updateQuery = querySet[i];						
					}
				}
				//상세 테이블에 SR_ID 데이터가 있는지 확인
				if(checkQuery != null && !"".equals(checkQuery)) {
					isExist = checkExistSrIdInDetailTable(checkQuery, paramMap);					
				}
				if(isExist) {
					//데이터가 있을 경우 - 업데이트 쿼리 실행
					if(updateQuery != null && !"".equals(updateQuery)) {
						postWorkDAO.excuteProcessDetail(updateQuery, paramMap);							
					}
				} else {
					//데이터가 없을 경우 - 등록 쿼리 실행
					if(insertQuery != null && !"".equals(insertQuery)) {
						postWorkDAO.excuteProcessDetail(insertQuery, paramMap);						
					}
				}
			}
			
			//grid 성 데이터를 등록한다.
			if(processVO.getGridMapList() != null && processVO.getGridMapList().size() > 0) {
				updateGridMapData(processVO);
			}
			
			//화면에서 전달 받은 grid 필드의 데이터를 등록한다.
			if(processVO.getGridFieldList() != null && processVO.getGridFieldList().size() > 0){
				updateGridFieldData(processVO);
			}
			
			//단계가 등록 단계 이면 작업 상태를 임시저장으로 등록함 - 이것은 임시등록(요청등록단계)과 임시저장(진행중인단계)을 하나로 쓰기 위해 작성된 구문으로 보임
			//현재 임시 저장을 따로 분리하기 때문에 삭제되어도 무방할 것으로 보임
			if(NbpmConstraints.TASK_NAME_START.equals(task_name)) {
				HashMap param = new HashMap();
				param.put("sr_id", processVO.getSr_id());
				param.put("work_state", NbpmConstraints.WORK_STATE_TEMP);
				postWorkDAO.updateWorkState(param);
			}

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return returnMap;
	}	
	
	@Override
	/*
	 * 임시 저장 기능 - 등록이 요청등록 단계라면 진행 중인 단계에서는 임시저장이 가능하다.(데이터 등록 로직등이 필요해지기 때문에 분리)
	 * 대부분의 로직은 임시등록과 비슷하나 등록을 위한 분기가 없다.
	 */
	public Map processTemporarily(NbpmProcessVO processVO, Map paramMap)
			throws NkiaException {
		Map resultMap = new HashMap();
		Map returnMap = new HashMap();
		try {
			
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String task_name = "";
			if(processVO.getTask_name() != null) {
				task_name = processVO.getTask_name();
			}
			List itemList = processVO.getItemList();
			List operList = processVO.getOperList();
			String queryKey = (String)paramMap.get("queryKey");
			
			String srId = processVO.getSr_id();
			
			paramMap.put("upd_user_id", processVO.getWorker_user_id());
			postWorkDAO.updateProcessMaster(paramMap);
			checkReassignOperUser(operList);
			updateOperatorInfo(processVO, operList);
			updateProcessItemInfo(processVO, itemList);
			
			if(queryKey != null) {
				String[] querySet = queryKey.split(",");
				String checkQuery = null;
				String insertQuery = null;
				String updateQuery = null;
				for(int i = 0 ; i < querySet.length ; i++) {
					if(querySet[i].indexOf("select") != -1) {
						checkQuery = querySet[i];						
					} else if(querySet[i].indexOf("insert") != -1) {
						insertQuery = querySet[i];
					} else if(querySet[i].indexOf("update") != -1) {
						updateQuery = querySet[i];						
					}
				}
				if(updateQuery != null && !"".equals(updateQuery)) {
					postWorkDAO.excuteProcessDetail(updateQuery, paramMap);							
				}
			}
			
			//grid성 데이터를 등록한다.
			if(processVO.getGridMapList() != null && processVO.getGridMapList().size() > 0) {
				updateGridMapData(processVO);
			}
			
			//화면에서 전달 받은 grid 필드의 데이터를 등록한다.
			if(processVO.getGridFieldList() != null && processVO.getGridFieldList().size() > 0){
				updateGridFieldData(processVO);
			}

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return returnMap;
	}
		
	/**
	 * TODO
	 * @param processVO 프로세스에 필요한 정보
	 * @param paramMap : 
	 * 				nbpm_task_name
	 * 			  , nbpm_user_id
	 * 			  , nbpm_process_type
	 * 			  , nbpm_processId
	 * 			  , nbpm_processInstanceId
	 * 			  , nbpm_version
	 * 			  , nbpm_operList
	 * @throws NkiaException
	 */
	public Map executeTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		Map returnMap = new HashMap();
		try {
			//멀티 타스크들의 그룹 데이터를 확인하기 위해 요청 처리 전 현재 활성 중인 타스크가 있는지 조회 한다.
			List oldTaskList = postWorkDAO.selectOldTaskList(processVO.getProcessInstanceId());
			//타스크를 처리한다.
			returnMap = workTask(processVO, paramMap);
			if("Y".equals(StringUtil.parseString(processVO.getMulti_yn()))) {
				//처리되고 아직 활성화 되어 있는 타스크들의 아이디
				List nextTaskList = (List)returnMap.get(NbpmConstraints.NBPM_PARAM_NEXTTASKIDS);
				//이전 (OLD) 타스크와 처리 후 전달받은 타스크를 비교하여 최종적으로 신규 생성된 타스크를 분리한다. 
				List newTaskList = getNewTaskList(oldTaskList, nextTaskList);
				//현재 처리된 타스크가 멀티 타스크일 경우 새로 생긴 타스크는 처리 된 타스크와 같은 그룹의 타스크이므로 그룹 ID를 세팅한다.
				if(processVO != null){
					updateMultiTaskGroupInfo(processVO, newTaskList);
				}
			}

			//연관 장애 타스크가 있는지 확인한다.
//			List relIncList = searchRelIncList(processVO.getSr_id());
//			for(int i = 0 ; i < relIncList.size() ; i++) {
//				NbpmProcessVO relSr = (NbpmProcessVO)relIncList.get(i);
//				relSr.setWorker_user_id(NbpmConstraints.REL_INC_ROLE);
//				Map param = new HashMap();
//				param.put("sr_id", relSr.getSr_id());
//				//있다면 처리 한다.
//				workTask(relSr, param);
//			}
		}  catch (NbpmRuleException e) {
			throw new NbpmRuleException(e);
		}  catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return returnMap;
	}
	
	private List getNewTaskList(List oldTaskList, List nextTaskList) {
		List resultList = new ArrayList();
		//이중 포문을 작성하여 이전 정보를 확인하여 신규로 작성된 타스크가 무엇인지 찾는다.
		for(int i = 0 ; i < nextTaskList.size() ; i++) {
			boolean isAdd = true;
			String nextTaskId = String.valueOf(nextTaskList.get(i));
			for(int j = 0 ; j < oldTaskList.size() ; j++) {
				Map taskInfo = (Map)oldTaskList.get(j);
				String taskId = String.valueOf(taskInfo.get("ID"));
				if(taskId.equals(nextTaskId)) {
					isAdd = false;
				}
			}
			if(isAdd){
				resultList.add(Long.valueOf(nextTaskId));				
			}
		}
		return resultList;
	}

	private void updateMultiTaskGroupInfo(NbpmProcessVO processVO, List newTaskList) throws NkiaException {
		//새롭게 생긴 타스크 중 멀티 타스크가 있을 경우에는 타스크 그룹 정보를 업데이트 한다.
		Map paramMap = new HashMap();
		if(newTaskList != null && newTaskList.size() > 0) {
			for(int i = 0 ; i < newTaskList.size() ; i++) {
				Long taskId = (Long)newTaskList.get(i);
				//타스크 정보를 통해 멀티타크스 정보를 조회하고 타스크 그룹 정보를 세팅하는 과정이다.
				Long multiTaskId = postWorkDAO.selectMultiTaskInfo(taskId);
				if(multiTaskId != null && multiTaskId > 0) {
					AbstractNbpmCommand cmd = new GetTaskInfoCmd(multiTaskId);
					Map taskInfo = (Map)cmd.execute();
					//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
					if(processVO.getTask_group_id() != null) {
						paramMap.put("task_group_id", processVO.getTask_group_id());
					}
					paramMap.put("task_id", multiTaskId);
					paramMap.put("worker_user_id", taskInfo.get("worker_user_id"));
					paramMap.put("task_name", taskInfo.get("task_name"));
					postWorkDAO.updateTaskGroupInfo(paramMap);
				} else {
					paramMap.put("task_group_id", processVO.getTask_group_id());
					paramMap.put("task_id", null);
					paramMap.put("worker_user_id", "");
					paramMap.put("task_name", NbpmConstraints.NBPM_STATE_END);
					postWorkDAO.updateTaskGroupInfo(paramMap);
				}
			}
		} else {
			paramMap.put("task_group_id", processVO.getTask_group_id());
			paramMap.put("task_id", null);
			paramMap.put("worker_user_id", "");
			paramMap.put("task_name", NbpmConstraints.NBPM_STATE_END);
			postWorkDAO.updateTaskGroupInfo(paramMap);
		}
		
	}

	public Map workTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		Map resultMap = new HashMap();
		try {
			
			//엔진 실행 커스터마이즈 된 데이터를 세팅하고 싶을 경우 사용한다.
			PreProcessExcutor preProcessExcutor = new PreProcessExcutor();
			preProcessExcutor.workPreProcessing(processVO, paramMap); 
			
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String task_name = "";
			if(processVO.getTask_name() != null) {
				task_name = processVO.getTask_name();
			}
			List itemList = processVO.getItemList();
			List operList = processVO.getOperList();
			String queryKey = (String)paramMap.get("queryKey");
			
			//프로세스에서 사용하기로 정의된 변수 명들을 조회하고 화면에서 넘어온 데이터를 비교하여 일치하는게 있다면 값을 세팅한다.
			List extParamNameList = postWorkDAO.searchNbpmProcessVariable(processVO.getProcessId());
			HashMap extParamMap = new HashMap();
			for(int i = 0 ; i < extParamNameList.size() ; i++) {
				String paramName = (String)extParamNameList.get(i);
				if(paramName != null && !"".equals(paramName)) {
					String paramValue = (String)paramMap.get(paramName.toLowerCase());
					extParamMap.put((String)extParamNameList.get(i), paramValue);					
				}
			}

			//UBIT 요청등록에서 연계요청 할 경우 임시Task로 이동
			if(paramMap.get("up_sr_id") != null && !paramMap.get("up_sr_id").equals("")){ //상위요청이 있을경우
				int upSrCnt = customizeUbitService.countProcMasterUpSrId(paramMap); //PROC_MASTER 에 등록된 테이블인지 확인
				Map taskMap = customizeUbitService.selectTaskNameUpSrId(paramMap);
				
				if(upSrCnt == 0){ //PROC_MASTER에 등록되지 않은경우 => 임시TASK
					//regist_param N으로 업데이트
					extParamMap.put("REGIST_PARAM", "N"); //REGIST_PARM이 N인경우 임시TASK로 이동, Y인경우 정상진행(Y인경우 하위요청이 임시TASK에 있으면 강제진행)
				}else{ //PROC_MASTER에 등록되어 있는 경우
					if(taskMap != null){
						if(taskMap.get("TASK_NAME").equals("REQUST_REGIST") || taskMap.get("TASK_NAME")==null){ //상위요청의 현재 TASK_NAME이 null이거나 REQUST_REGIST 인경우
							extParamMap.put("REGIST_PARAM", "N");
						}else{
							extParamMap.put("REGIST_PARAM", "Y");
						}						
					}else{
						extParamMap.put("REGIST_PARAM", "N");
					}
				}
			}else{
				extParamMap.put("REGIST_PARAM", "Y");
			}

			processVO.setExpansionParamMap(extParamMap);
			
			//기 등록된 SR_ID가 있는지 조회한다.
			boolean isExist = checkExistSrId(processVO.getSr_id());
			
			if(!isExist) {
				paramMap.put("reg_user_id", processVO.getWorker_user_id());
				//PROC_MASTER정보 등록
				postWorkDAO.insertProcessMaster(paramMap);
				
				//요청자 정보를 담당자 정보 테이블에 맞는 형태로 가공한다.
				Map requestorInfo = new HashMap();
				requestorInfo.put("oper_user_id", processVO.getReq_user_id());
				requestorInfo.put("sr_id", processVO.getSr_id());
				requestorInfo.put("oper_type", NbpmConstraints.REQUESTOR_ROLE);
				requestorInfo.put("select_type", "SINGLE");
				operList.add(requestorInfo);
				
			} else {
				//PROC_MASTER 정보 수정
				paramMap.put("upd_user_id", processVO.getWorker_user_id());
				postWorkDAO.updateProcessMaster(paramMap);
				//재지정된 담당자 정보를 체크한다.
				checkReassignOperUser(operList);
			}
			
			//담당자 정보를 업데이트 한다.
			updateOperatorInfo(processVO, operList);
			
			//2018-06-22 molee
			//일괄승인일 때 연관구성이 업데이트 되지 않도록 조건 처리
			String myList_doBatchProcess = "";
			if (paramMap.get("myList_doBatchProcess") != null) {
				//일괄승인
				myList_doBatchProcess = (String) paramMap.get("myList_doBatchProcess");
			} else {
				myList_doBatchProcess = "N";
			}
			
			if ("N".equals(myList_doBatchProcess)) {
				//연관 구성이나 업무서비스를 업데이트 한다.
				updateProcessItemInfo(processVO, itemList);
			}
			
			//화면에서 넘어온 쿼리키가 있을 경우 상세 테이블 데이터를 업데이트한다.
			if (queryKey != null) {
				updateProcDetailTable(queryKey, paramMap);
			}
			
			//화면에서 전달 받은 grid 데이터를 등록한다.
			if (processVO.getGridMapList() != null && processVO.getGridMapList().size() > 0) {
				updateGridMapData(processVO);
			}
			
			//화면에서 전달 받은 grid 필드의 데이터를 등록한다.
			if (processVO.getGridFieldList() != null && processVO.getGridFieldList().size() > 0) {
				updateGridFieldData(processVO);
			}
			
			//현재 등록되어 있는 역할자 정보를 조회한다.
			List roleList = postWorkDAO.searchRoleList(processVO);
			//프로세스의 전체 타스크 정보를 조회한다.
			List nodeList = postWorkDAO.searchNodeList(processVO);
			
			//타스크 처리를 위한 엔진 처리
			AbstractNbpmCommand workCommand = new WorkTaskCmd(processVO, roleList);				
			Map workResult = (Map)workCommand.execute();
			
			//프로세스 인스턴스 ID를 PROC_MASTER에 세팅한다.
			Long procInstId = (Long)workResult.get(NbpmConstraints.NBPM_PARAM_PROCID);
			if(procInstId != null){
				paramMap.put("proc_id", procInstId);
				if(NbpmConstraints.TASK_NAME_START.equals(task_name)) {
					postWorkDAO.updateProcMasterProcId(paramMap);
				}
				processVO.setProcessInstanceId(procInstId);
			}
			
			//엔진의  Rule Task를 실행한다.
			try {
				AbstractNbpmCommand workRuleTaskCommand = new WorkRuleTaskCmd(processVO);				
				workRuleTaskCommand.execute();			
			} catch (NbpmRuleException e) {
				throw new NbpmRuleException(e);
			} catch (Exception e) {
				throw new NbpmRuleException(e);
			}
			
			//엔진의  후 처리를 위한 엔진 호출
			AbstractNbpmCommand afterWorkCommand = new AfterWorkCmd(processVO, roleList, nodeList);				
			resultMap = (Map)afterWorkCommand.execute();
			String state = (String)resultMap.get(NbpmConstraints.NBPM_PARAM_STATE);
		
			if(NbpmConstraints.NBPM_STATE_ING.equals(state)) {
				//상태가 아직 진행 중일 경우
				ArrayList nextNodeIds = (ArrayList)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTNODEIDS);
				if(nextNodeIds != null){
					//상태를 업데이트 한다.
					updateProcMasterWorkState(processVO , nextNodeIds);
				}
				//알림과 다음 페이지 (자신이 담당자일 경우) 포워딩을 위해 다음 Task에 대한 정보를 세팅한다.
				setNextTaskInfoMap(resultMap, processVO);
			} else if(NbpmConstraints.NBPM_STATE_END.equals(state)) {
				//요청이 종료되었을 때
				paramMap.put("sr_id", processVO.getSr_id());
				paramMap.put("upd_user_id", processVO.getWorker_user_id());
				
				paramMap.put("reject_end_code", NbpmConstraints.PROC_STATE_REJECT_END);
				
				//반려 종료로 요청이 종료되었을 시를 판단한다.(로직상 반려종료와 동시에 요청이 끝날 경우에는 PROC_STATE 정보가 없기 때문에 추가로 판단하는 로직을 추가한다).
				if(NbpmConstraints.WORKTYPE_RETURN_END_PROC.equals(processVO.getWorkType())) {
					paramMap.put("proc_state", NbpmConstraints.PROC_STATE_REJECT_END);
				} else {
					paramMap.put("proc_state", NbpmConstraints.PROC_STATE_NRMLT);
				}
				
				postWorkDAO.updateEndWorkState(paramMap);
				
			}
		}catch (NbpmRuleException e) {
			throw new NbpmRuleException(e);
		}catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultMap;
	}

	@Deprecated
	private void registerChildProcess(Map childSrData, String workerUserId, long ProcInstId) throws SQLException, Exception {
		throw new Exception("Deprecated method");
	}
	
	@Override
	public Map registerChildProcess(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		Map returnMap = new HashMap();
		try {
			returnMap = workTask(processVO, paramMap);
			String updateTable = (String)paramMap.get("updateTable");
			if(updateTable != null && !"".equals(updateTable)) {
				postWorkDAO.updateParentRelationField(paramMap);
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return returnMap;
	}
		
	public Map checkTaskUser(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		String currentUserId = "";
		String currentUserNm = "";
		Map resultMap = new HashMap();
		try {
			//nsm.begin();
			AbstractNbpmCommand checkTasUserkCommand = new CheckTasUserkCmd(processVO);
			currentUserId = (String)checkTasUserkCommand.execute();
			if(NbpmConstraints.SERVICEDESK_ROLE.equals(currentUserId)) {
				AbstractNbpmCommand changeWorkerCommad = new ChangeWorkerCmd(processVO);
				changeWorkerCommad.execute();
				
				//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
				Map operInfo = new HashMap();
				if(processVO.getSr_id() != null) {
					operInfo.put("sr_id", processVO.getSr_id());
				}
				
				operInfo.put("oper_type", NbpmConstraints.SERVICEDESK_ROLE);
				operInfo.put("oper_user_id", processVO.getWorker_user_id());
				
				postWorkDAO.insertProcessOrerator(operInfo);
				
				resultMap.put("result", true);
			} else if(currentUserId.equals(processVO.getWorker_user_id())) {
				resultMap.put("result", true);
			} else {				
				currentUserNm = userDAO.selectUserName(currentUserId);				
				resultMap.put("result", false);
				resultMap.put("currentUserId", currentUserId);
				resultMap.put("currentUserNm", currentUserNm);
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultMap;
	}
		
	public String createSrId() throws NkiaException {
		String sr_id = postWorkDAO.createSrId();
		return sr_id;
	}

	public NbpmProcessVO selectTaskPageInfo(NbpmProcessVO paramVO) throws NkiaException {
		return postWorkDAO.selectTaskPageInfo(paramVO);
	}

	public Map selectProcessDetail(Map param, UserVO userVO) throws NkiaException, SQLException {
		String sr_id = (String)param.get("sr_id");
		List queryKey = (List)param.get("queryKey");
		List subQueryKey = (List)param.get("subQueryKey");
		String requestPop = (String)param.get("request_pop");
		String taskGroupId = (String)param.get("task_group_id");
		
		String userId = userVO.getUser_id();
		param.put("user_id", userId);
		Map masterData = postWorkDAO.selectProcessDetail(sr_id);
		
		if (masterData != null) {
			for(int i = 0 ; queryKey != null && i < queryKey.size() ; i++) {
				Map detailData = postWorkDAO.selectProcessDetailInfo((String)queryKey.get(i), param);
				if(detailData != null) {
					Iterator keySet = detailData.keySet().iterator();
					while(keySet.hasNext()) {
						String key = (String)keySet.next();
						masterData.put(key, detailData.get(key));
					}
				}
			}
			
			for (int i = 0; subQueryKey != null && i < subQueryKey.size(); i++) {
				String subQeury = (String)subQueryKey.get(i);
				if (subQeury != null && !"".equals(subQeury) && !"null".equals(subQeury)) {
					Map detailData = postWorkDAO.selectProcessDetailInfo((String)subQueryKey.get(i), param);
					if (detailData != null) {
						Iterator keySet = detailData.keySet().iterator();
						while(keySet.hasNext()) {
							String key = (String)keySet.next();
							masterData.put(key, detailData.get(key));
						}
					}				
				}
			}
		}
		Map operSearchMap = new HashMap();
		operSearchMap.put("sr_id", sr_id);
		operSearchMap.put("task_group_id", taskGroupId);
		
		List operList = postWorkDAO.selectProcessOperList(operSearchMap);
		
		List taskCommentList = postWorkDAO.searchTaskCommentList(sr_id);
		List commentHistory = new ArrayList();
		for(int i = 0 ; taskCommentList != null && i < taskCommentList.size(); i++) {
			Map commentMap = new HashMap();
			commentMap.putAll(((HashMap)taskCommentList.get(i)));
			String comment = (String)commentMap.get("TEXT");
			if(comment != null) {
				//개행문자 -> <br>변경
				comment = StringUtil.changeLineAlignmentForHtml(comment);
				commentMap.put("TEXT", comment);
				commentHistory.add(commentMap);
			}
		}
		//@@20130711 김도원 START
		//현재 수정 중인 의견 정보 조회 기능 추가
		String taskCurrentCommentList = postWorkDAO.searchTaskCurrentComment(param);
		String counselingHistory = servicedeskDAO.selectCounselingHistory(param);
		String reqUserId = "";
		if(masterData != null) {
			masterData.put("operList", operList);
			masterData.put("commentHistory", commentHistory);
			masterData.put("nbpm_commentList", taskCommentList);
			masterData.put("nbpm_comment", taskCurrentCommentList);
			//개행문자 -> <br>변경
			//masterData.put("counseling_content", StringUtil.changeLineAlignmentForHtml(counselingHistory));
			masterData.put("counseling_content", counselingHistory);
			
			reqUserId = (String)masterData.get("REQ_USER_ID");
		} else {
			masterData = new HashMap();
			Map codeParam = new HashMap();
			codeParam.put("code_grp_id", "REQ_TYPE");
			codeParam.put("code_id", StringUtil.parseString(param.get("req_type")));
			Map codeDetail = commonCodeDAO.selectCodeDetailData(codeParam);
			masterData.put("REQ_TYPE", StringUtil.parseString(param.get("req_type")));
			masterData.put("REQ_TYPE_NM", codeDetail.get("CODE_NM"));
			masterData.put("REQ_PATH", StringUtil.parseString(param.get("req_path")));
			masterData.put("SR_ID", sr_id);
			
			reqUserId = StringUtil.parseString(param.get("req_user_id"));
			masterData.put("REG_USER_ID", userId);
			masterData.put("REG_USER_NM", userVO.getUser_nm());
		}
		if( !"Y".equals(requestPop) && ("".equals(reqUserId) || reqUserId == null)) {
			reqUserId = userId;
		}
		//@@20130711 김도원 END
		UserVO reqUserVO = userDAO.selectUserInfo(reqUserId);
		if(reqUserVO != null) {
			String userDept = reqUserVO.getDetailInfo();
			masterData.put("REQ_USER_DETAIL", userDept);
			masterData.put("REQ_USER_NM", reqUserVO.getUser_nm());
			masterData.put("REQ_USER_ID", reqUserId);
			//UBIT커스텀 고객사정보 셋팅 20191024 
			/*masterData.put("CUSTOMER_ID_REQ_USER", reqUserVO.getCustomer_id());
			masterData.put("CUSTOMER_NM_REQ_USER", reqUserVO.getCustomer_nm());*/
			masterData.put("INSIDE_YN", reqUserVO.getInside_yn());
		}
		
		return masterData;
	}
	public Map selectProcessDetail_EC(Map param) throws NkiaException, SQLException {
		String sr_id = (String)param.get("sr_id");
		List queryKey = (List)param.get("queryKey");
		List subQueryKey = (List)param.get("subQueryKey");
		String requestPop = (String)param.get("request_pop");
		String taskGroupId = (String)param.get("task_group_id");
		
		String userId = "ExternalConnection";
		param.put("user_id", userId);
		Map masterData = postWorkDAO.selectProcessDetail(sr_id);
		
		if (masterData != null) {
			for(int i = 0 ; queryKey != null && i < queryKey.size() ; i++) {
				Map detailData = postWorkDAO.selectProcessDetailInfo((String)queryKey.get(i), param);
				if(detailData != null) {
					Iterator keySet = detailData.keySet().iterator();
					while(keySet.hasNext()) {
						String key = (String)keySet.next();
						masterData.put(key, detailData.get(key));
					}
				}
			}
			
			for (int i = 0; subQueryKey != null && i < subQueryKey.size(); i++) {
				String subQeury = (String)subQueryKey.get(i);
				if (subQeury != null && !"".equals(subQeury) && !"null".equals(subQeury)) {
					Map detailData = postWorkDAO.selectProcessDetailInfo((String)subQueryKey.get(i), param);
					if (detailData != null) {
						Iterator keySet = detailData.keySet().iterator();
						while(keySet.hasNext()) {
							String key = (String)keySet.next();
							masterData.put(key, detailData.get(key));
						}
					}				
				}
			}
		}
		Map operSearchMap = new HashMap();
		operSearchMap.put("sr_id", sr_id);
		operSearchMap.put("task_group_id", taskGroupId);
		
		List operList = postWorkDAO.selectProcessOperList(operSearchMap);
		
		List taskCommentList = postWorkDAO.searchTaskCommentList(sr_id);
		List commentHistory = new ArrayList();
		for(int i = 0 ; taskCommentList != null && i < taskCommentList.size(); i++) {
			Map commentMap = new HashMap();
			commentMap.putAll(((HashMap)taskCommentList.get(i)));
			String comment = (String)commentMap.get("TEXT");
			if(comment != null) {
				//개행문자 -> <br>변경
				comment = StringUtil.changeLineAlignmentForHtml(comment);
				commentMap.put("TEXT", comment);
				commentHistory.add(commentMap);
			}
		}
		//@@20130711 김도원 START
		//현재 수정 중인 의견 정보 조회 기능 추가
		String taskCurrentCommentList = postWorkDAO.searchTaskCurrentComment(param);
		String counselingHistory = servicedeskDAO.selectCounselingHistory(param);
		String reqUserId = "";
		if(masterData != null) {
			masterData.put("operList", operList);
			masterData.put("commentHistory", commentHistory);
			masterData.put("nbpm_commentList", taskCommentList);
			masterData.put("nbpm_comment", taskCurrentCommentList);
			//개행문자 -> <br>변경
			//masterData.put("counseling_content", StringUtil.changeLineAlignmentForHtml(counselingHistory));
			masterData.put("counseling_content", counselingHistory);
			
			reqUserId = (String)masterData.get("REQ_USER_ID");
		} else {
			masterData = new HashMap();
			Map codeParam = new HashMap();
			codeParam.put("code_grp_id", "REQ_TYPE");
			codeParam.put("code_id", StringUtil.parseString(param.get("req_type")));
			Map codeDetail = commonCodeDAO.selectCodeDetailData(codeParam);
			masterData.put("REQ_TYPE", StringUtil.parseString(param.get("req_type")));
			masterData.put("REQ_TYPE_NM", codeDetail.get("CODE_NM"));
			masterData.put("REQ_PATH", StringUtil.parseString(param.get("req_path")));
			masterData.put("SR_ID", sr_id);
			
			reqUserId = StringUtil.parseString(param.get("req_user_id"));
			masterData.put("REG_USER_ID", userId);
			masterData.put("REG_USER_NM", "외부접속");
		}
		if( !"Y".equals(requestPop) && ("".equals(reqUserId) || reqUserId == null)) {
			reqUserId = userId;
		}
		//@@20130711 김도원 END
		UserVO reqUserVO = userDAO.selectUserInfo(reqUserId);
		if(reqUserVO != null) {
			String userDept = reqUserVO.getDetailInfo();
			masterData.put("REQ_USER_DETAIL", userDept);
			masterData.put("REQ_USER_NM", reqUserVO.getUser_nm());
			masterData.put("REQ_USER_ID", reqUserId);
		}
		
		return masterData;
	}
	public NbpmProcessVO selectFinalVersionProcessInfo(String processType)
			throws NkiaException {
		Map paramMapForVersionInfo = new HashMap();
		NbpmProcessVO nbpmProcessVO = new NbpmProcessVO();
		
		paramMapForVersionInfo.put("process_type", processType);
		paramMapForVersionInfo.put("startTaskName", NbpmConstraints.TASK_NAME_REQUEST);
		
		nbpmProcessVO = postWorkDAO.selectFinalVersionProcessInfo(paramMapForVersionInfo);
		return nbpmProcessVO;
	}


	@Override
	public int searchRelConfListCount(Map paramMap) throws NkiaException {
		return nbpmCommonDAO.searchRelConfListCount(paramMap);
	}


	@Override
	public List searchRelConfList(Map paramMap) throws NkiaException {
		String itemType = (String)paramMap.get("item_type");
		List dataList = new ArrayList();
		if( itemType.equalsIgnoreCase("CI") ){
			dataList = nbpmCommonDAO.searchRelConfCiList(paramMap);
		}else if( itemType.equalsIgnoreCase("SERVICE") ){
			dataList = nbpmCommonDAO.searchRelConfServiceList(paramMap);
		}
		return dataList;
	}

	@Override
	public void updateAcceptDate(NbpmProcessVO nbpmProcessVO) throws NkiaException {
		try {
			AbstractNbpmCommand userRegisterCommand = new AcceptDateTaskCmd(nbpmProcessVO);
			userRegisterCommand.execute();
		} catch(Exception e) {
			throw new NkiaException(e);
		} finally {
			
		}
	}
	
	@Override
	public String selectLastTaskId(String srId) throws NkiaException {
		return postWorkDAO.selectLastTaskId(srId);
	}

	//@@20130711 김도원 START
	//현황, 요청이력 페이지 등에서 상세페이지를 조회하기 위해 추가된 메소드 
	@Override
	public NbpmProcessVO selectRequestPageInfo(NbpmProcessVO paramVO) throws NkiaException {
		return postWorkDAO.selectRequestPageInfo(paramVO);
	}
	//@@20130711 김도원 END

	@Override
	public void notifyTask(NbpmProcessVO processVO, Map result) throws NkiaException {
	
		try {
			
			ArrayList nextTaskIds = (ArrayList)result.get(NbpmConstraints.NBPM_PARAM_NEXTTASKIDS);
			ArrayList nextNodeIds = (ArrayList)result.get(NbpmConstraints.NBPM_PARAM_NEXTNODEIDS);
			
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String beforeTaskId = "";
			if(processVO.getTask_name() != null) {
				beforeTaskId = processVO.getTask_name();
			}
			
			//프로세스의 상세정보를 조회한다.
			Map processDetail = postWorkDAO.selectProcessDetail(processVO.getSr_id());
			
			for (int i=0; nextTaskIds != null && i < nextTaskIds.size(); i++) {
				processVO.setTask_id((Long)nextTaskIds.get(i));
				processVO.setNext_node_id((String)nextNodeIds.get(i));
				
				//다음 task의 알림 확인
				Map nextTaskAlarm = postWorkDAO.selectNextTaskAlamInfo(processVO);
				String emailSend = ""; //이메일발송여부
				String smsSend = ""; //문자발송여부
				String grpSlctYn = ""; //그룹여부
				String emailSendGrpYn = ""; //(담당자, 처리자 등의) 그룹에 메일 발송 여부
				String nodeName = ""; //작업명
				String personalYn = "";//개인발송여부
				
				if(nextTaskAlarm != null){
					emailSend = (String)nextTaskAlarm.get("EMAIL_SEND"); //이메일발송여부
					smsSend = (String)nextTaskAlarm.get("SMS_SEND"); //문자발송여부
					grpSlctYn = (String)nextTaskAlarm.get("GRP_SLCT_YN"); //그룹여부
					emailSendGrpYn = (String)nextTaskAlarm.get("EMAIL_SEND_GRP_YN"); //() 그룹에 메일 발송 여부  
					nodeName = (String)nextTaskAlarm.get("NODENAME"); //작업명
					personalYn = (String)nextTaskAlarm.get("PERSONAL_YN"); //그룹 발송 여부
				}
				UserVO userVO = null;
				String loginId = "";
				if("Y".equals((String)result.get("email_yn"))){
					loginId = (String)result.get("workerUserId");
				}else{
					userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					loginId = userVO.getUser_id();
				}
				//E-MAIL
				// 프로세스 작업 상태 관리에서 메일 송신 여부가 활성화 되어 있으면 알림메일를 보낸다.
				if (emailSend.equals(EmailEnum.COMMON_Y.getString())) {
					List targetUserList = postWorkDAO.searchNbpmAlamEmailList(processVO);
					if("Y".equals(grpSlctYn)){//실제 데이터가 그룹인지 아닌지를파악 // 실제데이터가 그룹일때
						if("Y".equals(personalYn)){ //그룹발송여부 확인 "Y"그룹  "N" 발송안함
							
							Map dataMap = new HashMap();
							
							//작업명
							dataMap.put("NODENAME", nodeName);
							String templateId = (String)nextTaskAlarm.get("EMAIL_TEMPLATE_ID"); //템플릿ID
		
							//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
							
							if("APPROVAL_MAIL".equals(templateId)|| "APPROVAL_ONLY_MAIL".equals(templateId)){
								processDetail.put("TASK_ID", (Long)nextTaskIds.get(i));
								processDetail.put("TASK_NAME", (String)nextTaskAlarm.get("TASK_NAME"));
								Map targetUserMap = (Map)targetUserList.get(0);
								processDetail.put("TARGET_USER_ID", (String)targetUserMap.get("TARGET_USER_ID"));
								dataMap.put("TO_USER_LIST", targetUserList);
							}
							
							//승인메일이아닐경우 자기자신은 발송안함
							List userList = new ArrayList(); 
							for(int k=0; k < targetUserList.size();k++){
								Map userListMap = (Map)targetUserList.get(k);
								if(!loginId.equals(userListMap.get("TARGET_USER_ID"))){
									if("Y".equals(userListMap.get("ALARM_YN"))){
										userList.add(targetUserList.get(k));
									}
								}
							}
							targetUserList = userList;
							dataMap.put("TO_USER_LIST", targetUserList);
							
							
							if(targetUserList.size() >0){
								boolean emailSendFlag = false; //이메일발송
								//그룹여부
								if (!"Y".equals(grpSlctYn)) {
									emailSendFlag = true;
								} else {
									//그룹에도 메일 발송할지 여부
									if (emailSendGrpYn != null && emailSendGrpYn.equals(EmailEnum.COMMON_Y.getString())) {
										emailSendFlag = true;
									}
								}
								
								//메일 발송  그룹여부를 앞단에서 체크하기때문에 이곳은 주석처리
								//if (emailSendFlag) {
								//	try {
										//메일 데이터 셋팅
										Map mailInfoMap = new HashMap();
										mailInfoMap.put("KEY","PROCESS");
										mailInfoMap.put("TEMPLATE_ID", templateId);
										mailInfoMap.put("DATA", dataMap);
										mailInfoMap.put("ADD_DATA", processDetail);
										
										EmailSetData emailSender = new EmailSetData(mailInfoMap);
										emailSender.sendMail();
										processVO.setEmailSended("Y");
								//	} catch(Exception e){
								//		throw new NkiaException(e);
								//	}
								//} else {
								//	processVO.setEmailSended("N");
								//}
								
								//메일 발송이 되면 메일 발송 여부를 업데이트 한다.
								AbstractNbpmCommand emailSendedCommand = new EmailSendedCommand(processVO);		
								emailSendedCommand.execute();
							}
						}
					}else{//실제 데이터가 그룹인지 아닌지를파악 // 실제데이터가 단일일때
						Map dataMap = new HashMap();
						
						//작업명
						dataMap.put("NODENAME", nodeName);
						String templateId = (String)nextTaskAlarm.get("EMAIL_TEMPLATE_ID"); //템플릿ID
	
						//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
						
						if("APPROVAL_MAIL".equals(templateId)|| "APPROVAL_ONLY_MAIL".equals(templateId)){
							processDetail.put("TASK_ID", (Long)nextTaskIds.get(i));
							processDetail.put("TASK_NAME", (String)nextTaskAlarm.get("TASK_NAME"));
							Map targetUserMap = (Map)targetUserList.get(0);
							processDetail.put("TARGET_USER_ID", (String)targetUserMap.get("TARGET_USER_ID"));
							dataMap.put("TO_USER_LIST", targetUserList);
						}
						List userList = new ArrayList(); 
						for(int k=0; k < targetUserList.size();k++){
							Map userListMap = (Map)targetUserList.get(k);
							if(!loginId.equals(userListMap.get("TARGET_USER_ID"))){
								userList.add(targetUserList.get(k));
							}
						}
						targetUserList = userList;
						if(targetUserList.size() >0){
							dataMap.put("TO_USER_LIST", targetUserList);
							//승인메일이아닐경우 자기자신은 발송안함
							boolean emailSendFlag = false; //이메일발송
							//그룹여부
							if (!"Y".equals(grpSlctYn)) {
								emailSendFlag = true;
							} else {
								//그룹에도 메일 발송할지 여부
								if (emailSendGrpYn != null && emailSendGrpYn.equals(EmailEnum.COMMON_Y.getString())) {
									emailSendFlag = true;
								}
							}
							//메일 발송
							if (emailSendFlag) {
								try {
									//메일 데이터 셋팅
									Map mailInfoMap = new HashMap();
									mailInfoMap.put("KEY","PROCESS");
									mailInfoMap.put("TEMPLATE_ID", templateId);
									mailInfoMap.put("DATA", dataMap);
									mailInfoMap.put("ADD_DATA", processDetail);
									
									EmailSetData emailSender = new EmailSetData(mailInfoMap);
									emailSender.sendMail();
									processVO.setEmailSended("Y");
								} catch(Exception e){
									throw new NkiaException(e);
								}
							} else {
								processVO.setEmailSended("N");
							}
							//메일 발송이 되면 메일 발송 여부를 업데이트 한다.
							AbstractNbpmCommand emailSendedCommand = new EmailSendedCommand(processVO);		
							emailSendedCommand.execute();
						}
					}
				}
				
				//SMS
				if (smsSend.equals(EmailEnum.COMMON_Y.getString())) {
					//생성된 타스크 데이터를 기준으로 알림을 위한 데이터를 수집한다.
					List alamList = postWorkDAO.searchNbpmAlamSmsList(processVO);
					if("SM_APPROVAL".equals(beforeTaskId) && "REQUST_RCEPT".equals((String)nextTaskAlarm.get("TASK_NAME"))){ //이전이 긴급결재이고 바뀐
						for (int j = 0; j < alamList.size(); j++) {
							Map alamInfo = (Map)alamList.get(j);
							if(!loginId.equals((String)alamInfo.get("TARGET_USER_ID"))){ //로그인ID랑 보내는사람이랑 다를때만 발송
								nodeName = (String)alamInfo.get("NODENAME"); //절차단계
								String userId = (String)alamInfo.get("TARGET_USER_ID"); //수신자ID
								//프로세스 작업 상태 관리에서 문자 송신 여부가 활성화 되어 있으면 문자를 보낸다.
								ModelMap modelMap = new ModelMap();
								modelMap.put("userId", userId);
								Map userInfoMap = certificationDAO.selectUserInfo(modelMap);
								
								Map smsInfo = new HashMap();
								smsInfo.put("nodeName", nodeName);
								String toPhone = (String)(userInfoMap.get("HP_NO") == null ? "-" : userInfoMap.get("HP_NO"));
								smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
								String content = "ITSM 업무 알림입니다. \n";
								content += "▶프로세스 정보 : "+ (String)processDetail.get("REQ_TYPE_NM") + "\n";
								content += "▶ID : " + (String)processDetail.get("SR_ID")+ "\n";
								content += "▶알림내용 : " + (String)alamInfo.get("NODENAME") + "\n";
								if("SERVICE".equals((String)processDetail.get("REQ_TYPE"))){
									content += "▶신청유형 : " + (String)processDetail.get("REQUST_TYPE_NM") + "\n";
									content += "▶유형분류 : " + (String)processDetail.get("REQ_DOC_NM") + "\n";
								}
								content += "▶고객사 : " + (String)processDetail.get("MAIL_CUSTOMER_ID_NM") + "\n";
								content += "▶요청자 : " + (String)processDetail.get("REQ_USER_NM") + "\n";
								content += "▶제목 : " +(String)processDetail.get("TITLE");
								smsInfo.put("MSG_BODY", content);
								smsInfo.put("RE_BODY", "["+(String)processDetail.get("SR_ID")+"]"+(String)processDetail.get("REQ_TYPE_NM")+"_업무가 배정되었습니다.");
								smsInfo.put("TEMPLATE_CODE", "LMSG_20200317133022746749"); //ITSM업무알림 템플릿코드
								smsInfo.put("SENDER_KEY", "ef834c839e4a9c83ac3e027b0ed782a7545014d4"); // 센더키
								smsInfo.put("SEND_PHONE", "0226264210"); // 보내는사람
								smsInfo.put("USER_NM", (String)alamInfo.get("TARGET_USER_NM")); 
								SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
								smsSendService.insertTalkList(smsInfo);
							}
						}
					}else{
						if("Y".equals(grpSlctYn)){//실제 데이터가 그룹인지 아닌지를파악 // 실제데이터가 그룹일때
							if("Y".equals(personalYn)){ //그룹발송여부 확인 "Y"그룹  "N" 발송안함
								for (int j = 0; j < alamList.size(); j++) {
									Map alamInfo = (Map)alamList.get(j);
									if("Y".equals(alamInfo.get("ALARM_YN"))){ //사용자정보에 그룹이관 알람수신여부가 Y일때만 발송
										if(!loginId.equals((String)alamInfo.get("TARGET_USER_ID"))){ //로그인ID랑 보내는사람이랑 다를때만 발송
											nodeName = (String)alamInfo.get("NODENAME"); //절차단계
											String userId = (String)alamInfo.get("TARGET_USER_ID"); //수신자ID
											//프로세스 작업 상태 관리에서 문자 송신 여부가 활성화 되어 있으면 문자를 보낸다.
											ModelMap modelMap = new ModelMap();
											modelMap.put("userId", userId);
											Map userInfoMap = certificationDAO.selectUserInfo(modelMap);
											
											Map smsInfo = new HashMap();
											smsInfo.put("nodeName", nodeName);
											String toPhone = (String)(userInfoMap.get("HP_NO") == null ? "-" : userInfoMap.get("HP_NO"));
											smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
											String content = "ITSM 업무 알림입니다. \n";
											content += "▶프로세스 정보 : "+ (String)processDetail.get("REQ_TYPE_NM") + "\n";
											content += "▶ID : " + (String)processDetail.get("SR_ID")+ "\n";
											content += "▶알림내용 : " + (String)alamInfo.get("NODENAME") + "\n";
											if("SERVICE".equals((String)processDetail.get("REQ_TYPE"))){
												content += "▶신청유형 : " + (String)processDetail.get("REQUST_TYPE_NM") + "\n";
												content += "▶유형분류 : " + (String)processDetail.get("REQ_DOC_NM") + "\n";
											}
											content += "▶고객사 : " + (String)processDetail.get("MAIL_CUSTOMER_ID_NM") + "\n";
											content += "▶요청자 : " + (String)processDetail.get("REQ_USER_NM") + "\n";
											content += "▶제목 : " +(String)processDetail.get("TITLE");
											smsInfo.put("MSG_BODY", content);
											smsInfo.put("RE_BODY", "["+(String)processDetail.get("SR_ID")+"]"+(String)processDetail.get("REQ_TYPE_NM")+"_업무가 배정되었습니다.");
											smsInfo.put("TEMPLATE_CODE", "LMSG_20200317133022746749"); //ITSM업무알림 템플릿코드
											smsInfo.put("SENDER_KEY", "ef834c839e4a9c83ac3e027b0ed782a7545014d4"); // 센더키
											smsInfo.put("SEND_PHONE", "0226264210"); // 보내는사람
											smsInfo.put("USER_NM", (String)alamInfo.get("TARGET_USER_NM")); 
											SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
											smsSendService.insertTalkList(smsInfo);
										}
									}
								}
							}
						}else{
							for (int j = 0; j < alamList.size(); j++) {
								Map alamInfo = (Map)alamList.get(j);
								if(!loginId.equals((String)alamInfo.get("TARGET_USER_ID"))){ //로그인ID랑 보내는사람이랑 다를때만 발송
									nodeName = (String)alamInfo.get("NODENAME"); //절차단계
									String userId = (String)alamInfo.get("TARGET_USER_ID"); //수신자ID
									//프로세스 작업 상태 관리에서 문자 송신 여부가 활성화 되어 있으면 문자를 보낸다.
									ModelMap modelMap = new ModelMap();
									modelMap.put("userId", userId);
									Map userInfoMap = certificationDAO.selectUserInfo(modelMap);
									
									Map smsInfo = new HashMap();
									smsInfo.put("nodeName", nodeName);
									String toPhone = (String)(userInfoMap.get("HP_NO") == null ? "-" : userInfoMap.get("HP_NO"));
									smsInfo.put("TO_PHONE", toPhone.replaceAll("-", ""));
									String content = "ITSM 업무 알림입니다. \n";
									content += "▶프로세스 정보 : "+ (String)processDetail.get("REQ_TYPE_NM") + "\n";
									content += "▶ID : " + (String)processDetail.get("SR_ID")+ "\n";
									content += "▶알림내용 : " + (String)alamInfo.get("NODENAME") + "\n";
									if("SERVICE".equals((String)processDetail.get("REQ_TYPE"))){
										content += "▶신청유형 : " + (String)processDetail.get("REQUST_TYPE_NM") + "\n";
										content += "▶유형분류 : " + (String)processDetail.get("REQ_DOC_NM") + "\n";
									}
									content += "▶고객사 : " + (String)processDetail.get("MAIL_CUSTOMER_ID_NM") + "\n";
									content += "▶요청자 : " + (String)processDetail.get("REQ_USER_NM") + "\n";
									content += "▶제목 : " +(String)processDetail.get("TITLE");
									smsInfo.put("MSG_BODY", content);
									smsInfo.put("RE_BODY", "["+(String)processDetail.get("SR_ID")+"]"+(String)processDetail.get("REQ_TYPE_NM")+"_업무가 배정되었습니다.");
									smsInfo.put("TEMPLATE_CODE", "LMSG_20200317133022746749"); //ITSM업무알림 템플릿코드
									smsInfo.put("SENDER_KEY", "ef834c839e4a9c83ac3e027b0ed782a7545014d4"); // 센더키
									smsInfo.put("SEND_PHONE", "0226264210"); // 보내는사람
									smsInfo.put("USER_NM", (String)alamInfo.get("TARGET_USER_NM"));
									SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
									smsSendService.insertTalkList(smsInfo);
								}
							}
						}
					}
				}
			}
			
			//포탈과 연동을 할 경우에는 포탈에 푸시를 보낸다.
			if("true".equals(NkiaApplicationPropertiesMap.getProperty("nbpm.push.portal"))) {
				ArrayList nextWorkerIds = (ArrayList)result.get(NbpmConstraints.NBPM_PARAM_WORKERIDS);
				for(int i = 0 ; nextWorkerIds != null && i < nextWorkerIds.size() ; i ++ ) {
					NbpmPortalPushSender pushSender = new NbpmPortalPushSender(processDetail, (String)nextWorkerIds.get(i), processVO.getTask_name());
					Thread t = new Thread(pushSender);
					t.start();
				}
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}

	@Override
	public Map<String, Object> selectParentProcessDetail(Map<String, Object> param) throws NkiaException {
		String srId = (String)param.get("sr_id");
		String processType = (String)param.get("process_type");
		String upSrId = (String)param.get("parent_sr_id");
		/*
		//UBIT커스텀 하나의 프로세스에 sub_req_type을 사용하여 여러 폼으로 나누는 경우 프로세스 type 수정 
		String processPage = (String)param.get("process_type");
		boolean processPageType = processPage.startsWith("SERVICE");
		if(processPageType){
			processType = "SERVICE";
		}*/
		Map masterData = postWorkDAO.selectParentProcessDetail(upSrId);
		
		// 카카오 커스텀 - 릴리즈 요청일 경우, 요청자를 로그인한 사람으로 받음.
		String kakaoCustomReqType = "RELS";
		
		if(masterData == null) {
			masterData = new HashMap();			
		}
		
		masterData.put("REQ_TYPE", processType);
		
		Map codeParam = new HashMap();
		codeParam.put("code_grp_id", "REQ_TYPE");
		codeParam.put("code_id", processType);
		Map codeDetail = commonCodeDAO.selectCodeDetailData(codeParam);
		masterData.put("REQ_TYPE_NM", codeDetail.get("CODE_NM"));
		
		masterData.put("UP_SR_ID", upSrId);
		masterData.put("SR_ID", srId);
		
		/*//UBIT커스텀 변경연계요청시 서비스요청내용 가져오게
		NbpmCommonDAO nbpmCommonDAO = (NbpmCommonDAO)NkiaApplicationContext.getCtx().getBean("nbpmCommonDAO");
		
		String queryKey = "selectPROC_SERVICE";
		Map processMap = new HashMap();
		processMap.put("sr_id", upSrId);
		
		//proc_service 데이터 값이 있을경우 masterdata에 추가 
		List processList = nbpmCommonDAO.searchGridDataList(queryKey, processMap);
		if(processList.size() > 0){
			for(int i=0; i<processList.size(); i++){
				masterData = (Map)processList.get(i);
				masterData = WebUtil.lowerCaseMapKey(masterData);
			}
		}
		
		if(processType.equals("CHANGE")){
			masterData.put();
		}*/
		
		//@@20130711 김도원 END
		// 20160618 DJ SCREAM
		Object reqUser = masterData.get("REQ_USER_ID");
		String reqUserId = "";
//		if(reqUser == null || processType.equals(kakaoCustomReqType) ) {
			reqUserId = (String)param.get("login_user_id");
			String reqUserNm = (String)param.get("login_user_nm");
			masterData.put("REQ_USER_ID", reqUserId);
			masterData.put("REQ_USER_NM", reqUserNm);
//		} else {
//			reqUserId = reqUser.toString();
//		}
		// 20160618 DJ SCREAM END
		UserVO reqUserVO = userDAO.selectUserInfo(reqUserId);
		String userDept = reqUserVO.getDetailInfo();
		masterData.put("REQ_USER_DETAIL", userDept);
		masterData.put("INSIDE_YN", reqUserVO.getInside_yn());
		return masterData;
	}

	@Override
	public Map processRelInc(NbpmProcessVO processVO, ModelMap paramMap)
			throws NkiaException {
		Map returnMap = new HashMap();
		try {
			returnMap = workTask(processVO, paramMap);
			
			String relIncSrId = (String)paramMap.get("rel_inc_sr_id");
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String srId = "";
			if(processVO.getSr_id() != null) {
				srId = processVO.getSr_id();
			}
			
			HashMap params = new HashMap();
			params.put("relIncSrId", relIncSrId);
			params.put("srId", srId);
			
			NbpmProcessVO resultVO = checkParentProcessWorkState(params);
			if(resultVO != null) {
				int relIncIdx = -1;
				List nextTaskIds = (List)returnMap.get(NbpmConstraints.NBPM_PARAM_NEXTTASKIDS);
				List workerIds = (List)returnMap.get(NbpmConstraints.NBPM_PARAM_WORKERIDS);
				for(int i = 0 ; i < workerIds.size() ; i++) {
					String workerId = (String)workerIds.get(i);
					if(NbpmConstraints.REL_INC_ROLE.equals(workerId)) {
						relIncIdx = i;
					}
				}
				if(relIncIdx > -1) {
					resultVO.setTask_id((Long)nextTaskIds.get(relIncIdx));
					resultVO.setProcessId(processVO.getProcessId());
					resultVO.setWorker_user_id(NbpmConstraints.REL_INC_ROLE);
					resultVO.setWorkType("");
					returnMap = workTask(resultVO, paramMap);
				}
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return returnMap;
	}

	@Override
	public boolean checkTaskStatus(NbpmProcessVO processVO) throws NkiaException {
		boolean result = true;
		int count = postWorkDAO.checkTaskStatus(processVO);
		if(count > 0) {
			result = false;
		}
		return result;
	}	
	
	private void updateProcDetailTable(String queryKey, Map paramMap) throws SQLException, Exception {
		String[] querySet = queryKey.split(",");
		String checkQuery = null;
		String insertQuery = null;
		String updateQuery = null;
		for(int i = 0 ; i < querySet.length ; i++) {
			if(querySet[i].indexOf("count") != -1) {
				checkQuery = querySet[i];						
			} else if(querySet[i].indexOf("insert") != -1) {
				insertQuery = querySet[i];
			} else if(querySet[i].indexOf("update") != -1) {
				updateQuery = querySet[i];						
			}
		}
		boolean isExist = true;
		if(checkQuery != null && !"".equals(checkQuery)) {
			isExist = checkExistSrIdInDetailTable(checkQuery, paramMap);					
		}
		if(isExist && updateQuery != null) {
			//데이터가 있을 경우
			postWorkDAO.excuteProcessDetail(updateQuery, paramMap);
		} else {
			//데이터가 없을 경우
			if(insertQuery != null && !"".equals(insertQuery)) {
				postWorkDAO.excuteProcessDetail(insertQuery, paramMap);						
			}
		}
	}	
	
	private void checkReassignOperUser(List operList) {
		ArrayList<Integer> removeIndex = new ArrayList<Integer>();
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(operList != null) {
			for(int i = 0 ; operList != null && i < operList.size() ; i++) {
				Map checkOperInfo = (Map)operList.get(i);
				String checkOperType = (String)checkOperInfo.get("oper_type");
				if(checkOperType.indexOf("re_") != -1) {
					checkOperType = checkOperType.replace("re_", "");
					for(int j = 0 ; j < operList.size() ; j++) {
						Map operInfo = (Map)operList.get(j);
						String operType = (String)operInfo.get("oper_type");
						if(checkOperType.equals(operType)) {
							removeIndex.add(j);
						}
					}
					checkOperInfo.put("oper_type", checkOperType);
				}
			}
		}
		
		for(int i = 0 ; i < removeIndex.size() ; i++) {
			operList.remove(removeIndex.get(i));
		}
	}	
	
	private void setNextTaskInfoMap(Map resultMap, NbpmProcessVO processVO) throws SQLException, Exception {
		ArrayList nextNodeIds = (ArrayList)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTNODEIDS);		

		List workerIds = (List)resultMap.get(NbpmConstraints.NBPM_PARAM_WORKERIDS);
		List taskIds = (List)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTTASKIDS);
		List nodeNames = (List)resultMap.get(NbpmConstraints.NBPM_PARAM_NEXTNODENAMES);
		
		boolean selfWork = false;
		long taskId = 0;
		String nodeId = "";
		String nodeName = "";
		for(int i = 0 ; i < workerIds.size() ; i++) {
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(processVO.getWorker_user_id() != null) {
				if(processVO.getWorker_user_id().equals((String)workerIds.get(i))) {
					selfWork = true;
					taskId = (Long)taskIds.get(i);
					nodeId = (String)nextNodeIds.get(i);
					nodeName = (String)nodeNames.get(i);
				}
			}			
		}
		
		resultMap.put("selfWork", selfWork);
		resultMap.put("nextTaskId", taskId);
		resultMap.put("nextNodeName", nodeName);
		
		resultMap.put("sr_id", processVO.getSr_id());
	}	

	private void updateProcMasterWorkState(NbpmProcessVO processVO, ArrayList nextNodeIds) throws SQLException, Exception {
		if(nextNodeIds != null) {
			for(int i = 0 ; i < nextNodeIds.size() ; i++){
				processVO.setNext_node_id((String)nextNodeIds.get(i));
				Map workStateMap = postWorkDAO.selectCurrentWorkState(processVO);
				String workState= (String)workStateMap.get("WORK_STATE");
				String procState= (String)workStateMap.get("PROC_STATE");
				String workDtUpdateYn = (String)workStateMap.get("WORK_DT_UPDATE_YN");
				if((!"".equals(workState) && workState != null)) {
					if(!workState.equals(NbpmConstraints.WORK_STATE_NON)) {
						HashMap param = new HashMap();
						param.put("sr_id", processVO.getSr_id());
						param.put("work_state", workState);
						param.put("work_dt_update_yn", workDtUpdateYn);
						param.put("reject_end_code", NbpmConstraints.PROC_STATE_REJECT_END);
						//WORKTYPE 코드 상수로 등록할 것
						if(NbpmConstraints.WORKTYPE_RETURN_END_PROC.equals(StringUtil.parseString(processVO.getWorkType()))) {
							param.put("proc_state", NbpmConstraints.PROC_STATE_REJECT_END);
						} else {
							param.put("proc_state", procState);
						}
						
						postWorkDAO.updateWorkState(param);
					}
				}
			}
		}
		
	}

	private void updateGridMapData(NbpmProcessVO processVO) throws NkiaException {
		List gridMapList = new ArrayList();
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(processVO.getGridMapList() != null) gridMapList = processVO.getGridMapList();
		
		if(gridMapList != null) {
			for(int i = 0 ; i < gridMapList.size() ; i++) {
				Map gridDataMap = (Map)gridMapList.get(i);
				String gridInsertQueryKey = (String)gridDataMap.get("gridInsertQueryKey");
				String gridDeleteQueryKey = (String)gridDataMap.get("gridDeleteQueryKey");
				List gridDataList = (List)gridDataMap.get("gridData");
				HashMap paramMap = new HashMap();
				paramMap.put("sr_id", processVO.getSr_id());
				paramMap.put("task_group_id", processVO.getTask_group_id());
				if(gridInsertQueryKey != null && !"".equals(gridInsertQueryKey)) {
					nbpmCommonDAO.deleteGridData(gridDeleteQueryKey, paramMap);
					for(int j = 0 ; j < gridDataList.size() ; j++) {
						Map gridData = (Map)gridDataList.get(j);
						gridData.put("sr_id", processVO.getSr_id());
						gridData.put("upd_user_id", processVO.getWorker_user_id());
						nbpmCommonDAO.insertGridData(gridInsertQueryKey, gridData);
					}
				}
			}
		}		
	}	
	
	private void updateGridFieldData(NbpmProcessVO processVO) throws NkiaException {
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		List gridFielList = new ArrayList();
		
		if(processVO.getGridFieldList() != null) gridFielList = processVO.getGridFieldList();
		
		if(gridFielList != null) {
			for(int i = 0 ; i < gridFielList.size() ; i++) {
				Map dataMap = (Map) gridFielList.get(i);
				String gridInsertQueryKey = (String)dataMap.get("insertQuerykey");
				String gridDeleteQueryKey = (String)dataMap.get("deleteQuerykey");
				List gridList = (List)dataMap.get("gridList");
				
				if(!gridInsertQueryKey.isEmpty() && !gridDeleteQueryKey.isEmpty()){
					
					HashMap paramMap = new HashMap();
					paramMap.put("sr_id", processVO.getSr_id());
					paramMap.put("task_group_id", processVO.getTask_group_id());
					nbpmCommonDAO.deleteGridData(gridDeleteQueryKey, paramMap);
					
					for(int j = 0 ; j <gridList.size(); j++){
						Map gridDataList = (Map)gridList.get(j);
						if(gridInsertQueryKey != null && !"".equals(gridInsertQueryKey)) {
							gridDataList = WebUtil.lowerCaseMapKey(gridDataList);
							gridDataList.put("sr_id", processVO.getSr_id());
							gridDataList.put("upd_user_id", processVO.getWorker_user_id());
							nbpmCommonDAO.insertGridData(gridInsertQueryKey, gridDataList);
						}
					}
				}
			}
		}
		
	}	
	
	private void updateProcessItemInfo(NbpmProcessVO processVO, List itemList) throws SQLException, Exception {
		if(itemList != null && 0 < itemList.size()){
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(processVO.getSr_id() != null) {
				postWorkDAO.deleteProcessItem(processVO.getSr_id());							
			}
			for(int i = 0 ; itemList != null && i < itemList.size() ; i++) {
				Map itemInfo = (Map)itemList.get(i);
				itemInfo.put("sr_id", processVO.getSr_id());
				postWorkDAO.insertProcessItem(itemInfo);
			}
		}
	}

	private void updateOperatorInfo(NbpmProcessVO processVO, List operList) throws SQLException, Exception {
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(operList != null) {
			for(int i = 0 ; operList != null && i < operList.size() ; i++) {
				Map operInfo = (Map)operList.get(i);
				
				if(processVO.getSr_id() != null) {
					operInfo.put("sr_id", processVO.getSr_id());
					postWorkDAO.deleteProcessOrerator(operInfo);
				}
				
			}
		}		
		
		for(int i = 0 ; operList != null && i < operList.size() ; i++) {
			Map operInfo = (Map)operList.get(i);
			operInfo.put("sr_id", processVO.getSr_id());
			postWorkDAO.insertProcessOrerator(operInfo);
		}
	}	

	private boolean checkExistSrIdInDetailTable(String checkQuery, Map paramMap) throws SQLException, Exception {
		boolean result = false;
		int count = postWorkDAO.selectExistSrIdInDetailTable(checkQuery, paramMap);
		if(count > 0) {
			result = true;
		}
		return result;
	}


	private boolean checkExistSrId(String srId) throws SQLException, Exception {
		boolean result = false;
		int count = postWorkDAO.selectExistSrId(srId);
		if(count > 0) {
			result = true;
		}
		return result;
	}
	//@@20130711 김도원 END	
	
	private NbpmProcessVO checkParentProcessWorkState(Map params) {
		NbpmProcessVO resultVO = postWorkDAO.checkParentProcessWorkState(params);
		return resultVO;
	}
	
	private List searchRelIncList(String sr_id) {
		List relIncList = postWorkDAO.searchRelIncList(sr_id);
		return relIncList;
	}

	@Override
	public String selectCurrentTaskName(String srId) throws NkiaException {
		return postWorkDAO.selectCurrentTaskName(srId);
	}
	
	
	public void changeTaskWorker(long taskId, String worker_id) throws NkiaException {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		Map returnMap = new HashMap();
		try {
			//nsm.begin();
			
			Map taskInfo = postWorkDAO.selectTaskForClaim(taskId);
			
			HashMap paramMap = new HashMap();
			paramMap.put("user_id", worker_id);
			paramMap.put("sr_id", taskInfo.get("SR_ID"));
			paramMap.put("actualowner_id", taskInfo.get("ACTUALOWNER_ID"));
			paramMap.put("role_id", (String)taskInfo.get("ROLE_ID"));
			paramMap.put("task_id", taskId);
			
			
			nbpmCommonDAO.updateClaimTask(paramMap);
			
			paramMap.put("grp_slct_yn", "N");

			String operType = (String)taskInfo.get("OPER_TYPE");
			boolean isExistProcOperInfo = operType != null && !"".equals(operType) ? true : false;
			if(isExistProcOperInfo) {
				nbpmCommonDAO.updateClaimProcOper(paramMap);
			} else {
				paramMap.put("oper_type", (String)taskInfo.get("ROLE_ID"));
			    paramMap.put("oper_user_id", worker_id);
				postWorkDAO.insertProcessOrerator(paramMap);
			}
			
			//nsm.commit();
		} catch(Exception e) {
			throw new NkiaException(e);
			//nsm.rollback();
		} finally {
			//nsm.close();
		}
	}
	
	public void releaseTaskWorker(long taskId, String loginUserId) throws NkiaException {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		Map returnMap = new HashMap();
		try {
			//nsm.begin();
			Map taskInfo = postWorkDAO.selectTaskForClaim(taskId);
			
			HashMap paramMap = new HashMap();
			paramMap.put("user_id", taskInfo.get("CREATEDBY_ID"));
			paramMap.put("sr_id", taskInfo.get("SR_ID"));
			paramMap.put("actualowner_id", loginUserId);
			paramMap.put("role_id", taskInfo.get("OPER_TYPE"));
			paramMap.put("task_id", taskId);
			
			
			nbpmCommonDAO.updateClaimTask(paramMap);
			
			paramMap.put("grp_slct_yn", "Y");
			nbpmCommonDAO.updateClaimProcOper(paramMap);
			
			//nsm.commit();
		} catch(Exception e) {
			throw new NkiaException(e);
			//nsm.rollback();
		} finally {
			//nsm.close();
		}
	}
	
	@Override
	public void updateProcInfo(NbpmProcessVO processVO, ModelMap paramMap) throws SQLException, Exception {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {
			//nsm.begin();
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String task_name = "";
			if(processVO.getTask_name() != null) {
				task_name = processVO.getTask_name();
			}
			List itemList = processVO.getItemList();
			List operList = processVO.getOperList();
			String queryKey = (String)paramMap.get("queryKey");
			
			paramMap.put("upd_user_id", processVO.getWorker_user_id());
			postWorkDAO.updateProcessMaster(paramMap);
			//재지정된 담당자정보를 수정하기 위해 실행한다.
			checkReassignOperUser(operList);
			
			//담당자 정보 Update start
			updateOperatorInfo(processVO, operList);
			//담당자 정보 Update end
			
			//Item(Conf, Service) 정보 Update start
			updateProcessItemInfo(processVO, itemList);
			//Item(Conf, Service) 정보 Update end
			
			if(queryKey != null) {
				updateProcDetailTable(queryKey, paramMap);
			}
			
			//grid성 데이터를 등록한다.
			if(processVO.getGridMapList() != null && processVO.getGridMapList().size() > 0) {
				updateGridMapData(processVO);
			}
			
			//화면에서 전달 받은 grid 필드의 데이터를 등록한다.
			if(processVO.getGridFieldList() != null && processVO.getGridFieldList().size() > 0){
				updateGridFieldData(processVO);
			}
			//nsm.commit();
		} catch(Exception e) {
			throw new NkiaException(e);
			//nsm.rollback();
		} finally {
			//nsm.close();
		}
	}

	@Override
	public void executeRuleClass(NbpmProcessVO processVO, String ruleName) throws SQLException, Exception {
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {
			//nsm.begin();		
			RuleInvoker invoker = new RuleInvoker("skh", processVO.getProcessInstanceId());
			invoker.setExecutor(ruleName);
			invoker.invoke();
			//nsm.commit();
		} catch(Exception e) {
			throw new NkiaException(e);
			//nsm.rollback();
		} finally {
			//nsm.close();
		}
	}
	
	@Override
	public Map createMultiTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		Map resultMap = new HashMap();
		//NbpmTransactionManager nsm = new NbpmTransactionManager();
		try {		
			//nsm.begin();
			String eventName = (String) paramMap.get("event_name");

			AbstractNbpmCommand workCommand = new WorkMultiTicketCmd(processVO, eventName);
			Map workResult = (Map) workCommand.execute();

			long task_id = (Long) workResult.get("task_id");
			String taskName = (String) workResult.get("task_name");
			paramMap.put("task_id", task_id);
			paramMap.put("task_group_id", task_id);
			paramMap.put("task_name", taskName);
			String nodename = null;
			int cnt = postWorkDAO.selectMultiTicketCnt(paramMap);
			if (cnt > 0) {
				throw new NbpmMultiTaskException("Task is exist");
			} else {
				postWorkDAO.insertMultiTaskInfo(paramMap);
				postWorkDAO.insertMultiTaskTableInfo(paramMap);

				String operType = StringUtil.parseString(paramMap.get("oper_type"));
				Map workerUserInfo = new HashMap();
				
				if(processVO.getWorker_user_id() != null) workerUserInfo.put("oper_user_id", processVO.getWorker_user_id());			
				workerUserInfo.put("sr_id", processVO.getSr_id());
				workerUserInfo.put("oper_type", operType);
				workerUserInfo.put("task_group_id", task_id);
				postWorkDAO.insertProcessOrerator(workerUserInfo);
				
				nodename = postWorkDAO.selectTaskNodeName(paramMap);
			}
			resultMap.put("result", "success");
			resultMap.put("task_name", taskName);
			resultMap.put("task_id", task_id);
			resultMap.put("task_group_id", task_id);
			resultMap.put("nodename", nodename);

			//nsm.commit();				
		} catch(NbpmMultiTaskException e) {
			//new NkiaException(e);
			//nsm.rollback();
			resultMap.put("result", "fail");
			throw new NkiaException(e);
		} catch(Exception e) {
			throw new NkiaException(e);
			//nsm.rollback();
		} finally {
			//nsm.close();
		}
		return resultMap;
	}
	
	@Override
	public void registerUser(NbpmProcessVO processVO, Map paramMap)
			throws NkiaException {
		//NbpmTransactionManager tm = new NbpmTransactionManager();
		try {
			//tm.begin();
			//shshin
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			String requestor = "";
			if(processVO.getReq_user_id() != null) {
				requestor = processVO.getReq_user_id();
			}
			if(requestor != null && !"".equals(requestor)) {
				int count = postWorkDAO.isExistNbpmUser(requestor);
				if(count == 0) {
					postWorkDAO.insertNbpmUser(requestor);
				}
			}
			
			String loginId = processVO.getWorker_user_id();
			if(loginId != null && !"".equals(loginId)) {
				int count = postWorkDAO.isExistNbpmUser(loginId);
				if(count == 0) {
					postWorkDAO.insertNbpmUser(loginId);
				}
			}
			for(int i = 0 ; processVO.getOperList() != null && i < processVO.getOperList().size(); i++) {
				Object operUserId = processVO.getOperList().get(i).get("oper_user_id");
				String grp_slct_yn =  processVO.getOperList().get(i).get("grp_slct_yn");
				if(operUserId instanceof String) {
					if(operUserId != null && !"".equals((String)operUserId)) {
						//freechang 17.05.10 담당자 그룹으로 지정시  ORGANIZATIONALENTITY table에 등록
						/*
						if("Y".equals(grp_slct_yn))
						{
							postWorkDAO.insertGrpNbpmUser((String)operUserId);
						}else
						{
						*/
							int count = postWorkDAO.isExistNbpmUser((String)operUserId);
							if(count == 0) {
								postWorkDAO.insertNbpmUser((String)operUserId);
							}
//						}
					}
				} else if(operUserId instanceof List) {
					for(int j = 0 ; j < ((List)operUserId).size() ; j++) {
						String userId = (String)((List)operUserId).get(j);
						if(userId != null && !"".equals(userId)) {
							int count = postWorkDAO.isExistNbpmUser(userId);
							if(count == 0) {
								postWorkDAO.insertNbpmUser(userId);
							}
						}
					}
				}
			}			
			//tm.commit();
		} catch(Exception e) {
			//tm.rollback();
			throw new NkiaException(e);
		} finally {
			//tm.close();
		}
	}

	@Deprecated
	public Map executeAccept(NbpmProcessVO processVO, Map paramMap)
			throws NkiaException {
		throw new NkiaException("Deprecated method");
	}

	@Override
	public void registerSanctionLineInfo(Map paramMap) throws NkiaException {
		List operUserList = (List)paramMap.get("operUserList");
		String srId = String.valueOf(paramMap.get("srId"));
		if(operUserList != null) {
			for(int i = 0 ; i < operUserList.size() ; i++) {
				Map operInfo = (Map)operUserList.get(i);
				operInfo.put("sr_id", srId);
				operInfo = WebUtil.lowerCaseMapKey(operInfo);
				int count = postWorkDAO.selectOperUserInfo(operInfo);
				operInfo.put("oper_user_id", StringUtil.parseString(operInfo.get("sanction_id")));
				if(count > 0) {
					postWorkDAO.updateOperUserInfo(operInfo);
				} else {
					postWorkDAO.insertProcessOrerator(operInfo);
				}
				
			}
		}
	}

	@Override
	public boolean selectProcAuth(String userId, String processType,
			String operType) throws NkiaException {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("processType", processType);
		paramMap.put("operType", operType);
		paramMap.put("userId", userId);
		
		int count = postWorkDAO.selectOwnProcAuthCount(paramMap);
		
		return count > 0 ? true : false;
	}

	@Override
	public void registerUser(List<User> systemUserList) throws NkiaException {
		for(int i = 0 ; i < systemUserList.size() ; i ++) {
			User user = systemUserList.get(i);
			String userId = user.getId();
			if(userId != null && !"".equals(userId)) {
				int count = postWorkDAO.isExistNbpmUser(userId);
				if(count == 0) {
					postWorkDAO.insertNbpmUser(userId);
				}
			}
		}
		
	}

	@Override
	public void cancelMultiTask(NbpmProcessVO processVO, ModelMap paramMap) {
		Map resultMap = new HashMap();
		try {
			AbstractNbpmCommand workCommand = new ExitTaskCmd(processVO);
			workCommand.execute();			
		} catch (RuntimeException e) {
			logger.info("Task(id : " + processVO.getTask_id() + ") is Complete");
		}
		
		Map param = new HashMap();
		param.put("task_group_id", processVO.getTask_group_id());
		postWorkDAO.deleteMultiTaskProcOper(param);
		postWorkDAO.deleteCanceledTask(param);
		postWorkDAO.deleteMultiTaskInfo(param);
	}
    
	public List registerReqCharGerInfo(Map paramMap) throws NkiaException {
		String srId = String.valueOf(paramMap.get("sr_id"));
		String processType = String.valueOf(paramMap.get("process_type"));
		
		List resultList = postWorkDAO.searchReqCharGerInfo(paramMap);	
		
		return resultList;
	}
	
	@Override
	public List selectMultiUsers(Map paramMap) throws NkiaException {
		List resultList = new ArrayList();
		resultList = postWorkDAO.selectMultiUsers(paramMap);
		return resultList;
	}

	@Override
	public void insertMultiTempInfo(Map paramMap) throws NkiaException {
		List multiUserList = (List)paramMap.get("multiUsers");
		
		postWorkDAO.deleteMultiTempInfo(paramMap);

		for(int i = 0 ; i < multiUserList.size() ; i++) {
			Map multiInfoMap = paramMap;
			Map multiUser = (Map)multiUserList.get(i);
			multiInfoMap.put("task_order", String.valueOf(i));
			multiInfoMap.put("worker_user_id", String.valueOf(multiUser.get("worker_user_id")));

			postWorkDAO.insertMultiTempInfo(multiInfoMap);
		}
		
	}
	
	@Override
	public void deleteMultiTempInfo(Map paramMap) throws NkiaException {
		postWorkDAO.deleteMultiTempInfo(paramMap);
	}
	
	@Override
	public void updateRceptDt(ModelMap paramMap) throws NkiaException {
		Map rceptInfo = postWorkDAO.selectRceptInfo(paramMap);
		String status = (String) rceptInfo.get("STATUS");
		String taskName = (String) rceptInfo.get("TASK_NAME");
		String srId = (String) rceptInfo.get("SR_ID");
		paramMap.put("srId", srId);
		
		if("Reserved".equals(status) && "REQUST_RCEPT".equals(taskName)){
			postWorkDAO.updateRceptDt(paramMap);			
		}
	}
	
	@Override
	public void hideRecallTask(Map paramMap) throws NkiaException {
		//절차흐름도 회수태스크 숨김처리
		Map preTaskInfo = WebUtil.lowerCaseMapKey(postWorkDAO.selectPreTaskId(paramMap)); //이전 태스크정보
		
		String preTaskId = String.valueOf(preTaskInfo.get("id")); 
		String curTaskId = String.valueOf(paramMap.get("nbpm_task_id")); //현재 회수되어야하는 타스크
		String nextTaskId = String.valueOf(postWorkDAO.selectReservedTaskId(paramMap)); //회수 후 이동해야할 태스크 (이후)
		
		Map hideTaskMap = new HashMap(); 
		hideTaskMap.put("curTaskId",curTaskId);
		hideTaskMap.put("preTaskId",preTaskId);
		
		preTaskInfo.put("nextTaskId",nextTaskId);
		preTaskInfo.put("recallType","PROCESS"); //회수방식
		
		postWorkDAO.updateRecallTaskHide(hideTaskMap);
		postWorkDAO.updateNextTaskInfo(preTaskInfo); //이전단계 종료시간 
	}
	
	@Override
	public void updateTaskStatus(Map paramMap) throws NkiaException { //프로세스 회수 수정방식 - task테이블 업데이트
		postWorkDAO.updateTaskStatus(paramMap); 
		
		System.out.println(paramMap.get("task_status"));//Reserved, Recall
		
		if("Reserved".equals(paramMap.get("task_status"))) {
			Map preTaskInfo = WebUtil.lowerCaseMapKey(postWorkDAO.selectPreTaskId(paramMap)); //회수 후 이동해야할 태스크(이전)
			preTaskInfo.put("recallType","MODIFY"); //회수방식
			preTaskInfo.put("nextTaskId",preTaskInfo.get("id")); //이후 Task nbpm_processInstanceId
			postWorkDAO.updateNextTaskInfo(preTaskInfo);
			
			Map taskInfo = new HashMap(); //흐릅도 이전Task 종료시간, 이후Task 시작시간 수정
			taskInfo.put("recallTaskId",paramMap.get("nbpm_task_id")); //이후 Task
			postWorkDAO.updateRecallTaskInfo(taskInfo);
		}
	}
	
	@Override
	public String selectTaskStatus(Map paramMap) throws NkiaException {
		return postWorkDAO.selectTaskStatus(paramMap); 
	}	
	
}
