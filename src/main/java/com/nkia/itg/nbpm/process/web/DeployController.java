package com.nkia.itg.nbpm.process.web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.process.service.DeployExecutor;
import com.nkia.itg.nbpm.util.Base64;

@Controller
public class DeployController {
	
	private boolean test = true;
	
	@Resource(name="deployCommonExecutor")
	private DeployExecutor deployCommonExecutor;
	
	@RequestMapping(value="/itg/nbpm/goDeployTestPage.do")
	public String goDeployTestPage(ModelMap paramMap) throws NkiaException, SQLException {
		String forwarPage = "/itg/nbpm/deployTestPage.nvf";
		List resultList = deployCommonExecutor.searchFinalVesions();
		
		paramMap.put("resultList", resultList);
		return forwarPage;
	}	
	
	@RequestMapping(value="/itg/nbpm/goPageNameEnDeCodePage.do")
	public String goPageNameEnDeCodePage(ModelMap paramMap) throws NkiaException {
		
		return "/itg/nbpm/changePageName.nvf";
	}
	
	@RequestMapping(value="/itg/nbpm/decodePageName.do")
	public @ResponseBody Map decodePageName(@RequestBody ModelMap paramMap) throws NkiaException {
		HashMap resultMap = new HashMap();
		try{
			String str = (String)paramMap.get("text");
			byte[] result = Base64.decodeBase64(str.getBytes());
			resultMap.put("decodeName", result.toString());			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultMap;
	}
	
	@RequestMapping(value="/itg/nbpm/encodePageName.do")
	public @ResponseBody Map encodePageName(@RequestBody ModelMap paramMap) throws NkiaException {
		HashMap resultMap = new HashMap();
		try{
			String str = (String)paramMap.get("text");
			resultMap.put("encodeName", Base64.encodeBase64(str.getBytes()));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultMap;
	}
	
	
	@RequestMapping(value="/itg/nbpm/goNbpmSamplePage.do")
	public String goNbpmSamplePage(ModelMap paramMap) throws NkiaException {
		return "/itg/nbpm/nbpmSample.nvf";
	}
	
}
