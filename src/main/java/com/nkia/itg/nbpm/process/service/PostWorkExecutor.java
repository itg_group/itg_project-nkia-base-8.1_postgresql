/*
 * @(#)NbpmDeployService.java              2013. 3. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.nbpm.process.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jbpm.task.User;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public interface PostWorkExecutor {

	/**
	 * 엔진을 실행한다.
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map executeTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	/**
	 * 작업을 처리한다.
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map workTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException;
	
	/**
	 * @return
	 * @throws NkiaException
	 */
	public String createSrId() throws NkiaException;

	/**
	 * @param paramVO
	 * @return
	 * @throws NkiaException
	 */
	public NbpmProcessVO selectTaskPageInfo(NbpmProcessVO paramVO) throws NkiaException;

	/**
	 * @param param
	 * @param userVO
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */
	public Map selectProcessDetail(Map param, UserVO userVO) throws NkiaException, SQLException;

	public Map selectProcessDetail_EC(Map param) throws NkiaException, SQLException;
	/**
	 * @param process_type
	 * @return
	 * @throws NkiaException
	 */
	public NbpmProcessVO selectFinalVersionProcessInfo(String paramMap) throws NkiaException;
	
	/**
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map checkTaskUser(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchRelConfListCount(Map paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchRelConfList(Map paramMap) throws NkiaException;

	/**
	 * @param paramVO
	 * @throws NkiaException
	 */
	public void updateAcceptDate(NbpmProcessVO paramVO) throws NkiaException;

	/**
	 * @param srId
	 * @return
	 * @throws NkiaException
	 */
	public String selectLastTaskId(String srId) throws NkiaException;

	/**
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map registerTemporarily(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	/**
	 * @param paramVO
	 * @return
	 * @throws NkiaException
	 */
	public NbpmProcessVO selectRequestPageInfo(NbpmProcessVO paramVO) throws NkiaException;
	
	/**
	 * @param processVO
	 * @param result
	 * @throws NkiaException
	 */
	public void notifyTask(NbpmProcessVO processVO, Map result) throws NkiaException;
	
	/**
	 * @param param
	 * @return
	 * @throws NkiaException
	 */
	public Map<String, Object> selectParentProcessDetail(Map<String, Object> param) throws NkiaException;

	/**
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map registerChildProcess(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	/**
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map processTemporarily(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	/**
	 * @param processVO
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Map processRelInc(NbpmProcessVO processVO, ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param processVO
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void registerUser(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	public boolean checkTaskStatus(NbpmProcessVO processVO) throws NkiaException;

	public String selectCurrentTaskName(String srId) throws NkiaException;

	public void updateProcInfo(NbpmProcessVO processVO, ModelMap paramMap) throws SQLException, Exception;

	public void executeRuleClass(NbpmProcessVO processVO, String string) throws SQLException, Exception;
	
	public Map createMultiTask(NbpmProcessVO processVO, Map paramMap) throws NkiaException;
	
	public void cancelMultiTask(NbpmProcessVO processVO, ModelMap paramMap);
	@Deprecated
	public Map executeAccept(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	public void registerSanctionLineInfo(Map paramMap) throws NkiaException;

	public void changeTaskWorker(long taskId, String worker_id) throws NkiaException;

	public void releaseTaskWorker(long task_id, String loginUserId) throws NkiaException;

	public boolean selectProcAuth(String userId, String processType,
			String operType) throws NkiaException;

	public void registerUser(List<User> systemUserList) throws NkiaException;
	
	public List registerReqCharGerInfo(Map paramMap) throws NkiaException;
	
	public List selectMultiUsers(Map searchMap) throws NkiaException;

	public void insertMultiTempInfo(Map paramMap) throws NkiaException;

	public void deleteMultiTempInfo(Map paramMap) throws NkiaException;
	
	public void updateRceptDt(ModelMap paramMap) throws NkiaException;

	public void hideRecallTask(Map paramMap) throws NkiaException;
	
	public void updateTaskStatus(Map paramMap) throws NkiaException;
	
	public String selectTaskStatus(Map paramMap) throws NkiaException;
}
