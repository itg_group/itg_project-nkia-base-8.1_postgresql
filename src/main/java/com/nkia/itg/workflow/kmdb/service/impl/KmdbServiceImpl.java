package com.nkia.itg.workflow.kmdb.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.workflow.kmdb.dao.KmdbDAO;
import com.nkia.itg.workflow.kmdb.service.KmdbService;

@Service("kmdbService")
public class KmdbServiceImpl implements KmdbService {

	@Resource(name = "kmdbDAO")
	private KmdbDAO kmdbDAO;

	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public List searchCategoryTree(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.searchCategoryTree(paramMap);
	}	
	
	public int searchKmdbRequestListCount(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.searchKmdbRequestListCount(paramMap);
	}

	public List searchKmdbRequestList(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.searchKmdbRequestList(paramMap);
	}
	
	public int searchKmdbApprovalListCount(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.searchKmdbApprovalListCount(paramMap);
	}

	public List searchKmdbApprovalList(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.searchKmdbApprovalList(paramMap);
	}
	
	public void insertKmdb(ModelMap paramMap) throws NkiaException {
		kmdbDAO.insertKmdb(paramMap);
	}	
	
	public void updateKmdb(ModelMap paramMap) throws NkiaException {
		kmdbDAO.updateKmdb(paramMap);
	}	
	
	public void updateKmdbStateChange(ModelMap paramMap) throws NkiaException {
		kmdbDAO.updateKmdbStateChange(paramMap);
	}	
	
	public void useYnChange(ModelMap paramMap) throws NkiaException {
		kmdbDAO.useYnChange(paramMap);
	}	
	
	public void updateCnt(ModelMap paramMap) throws NkiaException {
		kmdbDAO.updateCnt(paramMap);
	}
	
	public void updateKmdbReaded(ModelMap paramMap) throws NkiaException {
		kmdbDAO.updateKmdbReaded(paramMap);
	}
	
	public void deleteKmdb(ModelMap paramMap) throws NkiaException, IOException {
		kmdbDAO.deleteKmdb(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}		
	}
	
	public HashMap selectKmdb(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.selectKmdb(paramMap);
	}

	@Override
	public int searchKmdbMainListCount(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.searchKmdbMainListCount(paramMap);
	}

	@Override
	public List searchKmdbMainList(ModelMap paramMap) throws NkiaException {
		return kmdbDAO.searchKmdbMainList(paramMap);
	}

	@Override
	public void approvalKmdb(ModelMap paramMap) throws NkiaException {
		String kmdbId = (String)paramMap.get("kmdb_id");
		if(!"".equals(kmdbId) && kmdbId != null) {
			kmdbDAO.updateKmdb(paramMap);
		} else {
			kmdbDAO.insertKmdb(paramMap);			
		}
	}
}
