package com.nkia.itg.workflow.kmdb.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.kmdb.service.CategoryService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class CategoryController {
	@Resource(name="categoryService")
	private CategoryService categoryService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 카테고리 관리 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/categoryManager.do")
	public String categoryManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/workflow/kmdb/categoryManager.nvf";
		return forwarPage;	
	}
	
	/**
	 * 멀티 코드 트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/searchMultiTree.do")
	public @ResponseBody ResultVO searchMultiTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = categoryService.searchCategoryDataForTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			Integer expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티코드 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/searchCodeDataList.do")
	public @ResponseBody ResultVO searchCodeDataList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = categoryService.searchCodeDataList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 하위 코드 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/searchCodeDataForChild.do")
	public @ResponseBody ResultVO  searchCodeDataForChild(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			
			if(!initFlag) {
				resultList = categoryService.searchCodeDataForChild(paramMap);	
				totalCount = categoryService.searchCodeDataForChildCnt(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티코드 정보 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/selectCategoryDetailData.do")
	public @ResponseBody ResultVO selectCategoryDetailData(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = categoryService.selectCategoryDetailData(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 멀티 코드 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/insertCategoryInfo.do")
	public @ResponseBody ResultVO insertCategoryInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			categoryService.insertCategoryInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 멀티 코드 삭제 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/deleteCategoryInfo.do")
	public @ResponseBody ResultVO deleteCategoryInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			categoryService.deleteCategoryInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 멀티 코드 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/updateCategoryInfo.do")
	public @ResponseBody ResultVO updateCategoryInfo(@RequestBody ModelMap paramMap)throws NkiaException {	
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			categoryService.updateCategoryInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 코드 아이디 중복 체크
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/selectCodeIdComp.do")
	public @ResponseBody ResultVO selectCodeIdComp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			String checkValue = categoryService.selectCodeIdComp(paramMap);
			resultVO.setResultString(checkValue);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 카테고리 하위 리스트 순서 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kmdb/updateCodeOrder.do")
	public @ResponseBody ResultVO updateCodeOrder(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	categoryService.updateCodeOrder(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
}
