package com.nkia.itg.workflow.kmdb.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface CategoryService { 
	
	public List searchCategoryDataForTree(ModelMap paramMap) throws NkiaException;
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException;

	public List searchCodeDataForChild(ModelMap paramMap) throws NkiaException;
	
	public int searchCodeDataForChildCnt(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectCategoryDetailData(ModelMap paramMap) throws NkiaException;
	
	public void insertCategoryInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteCategoryInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateCategoryInfo(ModelMap paramMap) throws NkiaException;
	
	public String selectCodeIdComp(ModelMap paramMap) throws NkiaException;
	
	public void updateCodeOrder(ModelMap paramMap) throws NkiaException;
}
