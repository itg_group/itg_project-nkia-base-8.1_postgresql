package com.nkia.itg.workflow.kmdb.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.workflow.kmdb.dao.CategoryDAO;
import com.nkia.itg.workflow.kmdb.service.CategoryService;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

	@Resource(name = "categoryDAO")
	private CategoryDAO categoryDAO;

	public List searchCategoryDataForTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return categoryDAO.searchCategoryDataForTree(paramMap);
		
	}

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return categoryDAO.searchCodeDataList(paramMap);
	}
	
	public List searchCodeDataForChild(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return categoryDAO.searchCodeDataForChild(paramMap);
	}
	
	public int searchCodeDataForChildCnt(ModelMap paramMap) throws NkiaException{
		return categoryDAO.searchCodeDataForChildCnt(paramMap);
	}
	
	public HashMap selectCategoryDetailData(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return categoryDAO.selectCategoryDetailData(paramMap);
	}
	
	public void insertCategoryInfo(ModelMap paramMap) throws NkiaException {
		categoryDAO.insertCategoryInfo(paramMap);
	}
	public void deleteCategoryInfo(ModelMap paramMap) throws NkiaException {
		categoryDAO.deleteCategoryInfo(paramMap);
	}
	
	public void updateCategoryInfo(ModelMap paramMap) throws NkiaException {
		categoryDAO.updateCategoryInfo(paramMap);
		categoryDAO.updateCategoryDeptUseAll(paramMap);
	}
	
	public String selectCodeIdComp(ModelMap paramMap) throws NkiaException {
		return categoryDAO.selectCodeIdComp(paramMap);
	}

	public void updateCodeOrder(ModelMap paramMap) throws NkiaException {
		categoryDAO.updateCodeOrder(paramMap);
	}
}
