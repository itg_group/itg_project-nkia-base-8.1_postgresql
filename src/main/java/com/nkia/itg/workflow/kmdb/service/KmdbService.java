package com.nkia.itg.workflow.kmdb.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface KmdbService { 

	public List searchKmdbApprovalList(ModelMap paramMap) throws NkiaException;

	public int searchKmdbApprovalListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchKmdbRequestList(ModelMap paramMap) throws NkiaException;

	public int searchKmdbRequestListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchCategoryTree(ModelMap paramMap) throws NkiaException;
	
	public void insertKmdb(ModelMap paramMap) throws NkiaException;
	
	public void updateKmdb(ModelMap paramMap) throws NkiaException;
	
	public void updateKmdbStateChange(ModelMap paramMap) throws NkiaException;
	
	public void useYnChange(ModelMap paramMap) throws NkiaException;
	
	public void updateCnt(ModelMap paramMap) throws NkiaException;
	
	public void updateKmdbReaded(ModelMap paramMap) throws NkiaException;
	
	public void deleteKmdb(ModelMap paramMap) throws NkiaException, IOException;
	
	public HashMap selectKmdb(ModelMap paramMap) throws NkiaException;

	public int searchKmdbMainListCount(ModelMap paramMap) throws NkiaException;

	public List searchKmdbMainList(ModelMap paramMap) throws NkiaException;

	public void approvalKmdb(ModelMap paramMap) throws NkiaException;
}
