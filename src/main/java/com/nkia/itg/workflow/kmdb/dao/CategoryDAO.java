package com.nkia.itg.workflow.kmdb.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("categoryDAO")
public class CategoryDAO  extends EgovComAbstractDAO {
	
	public List searchCategoryDataForTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("categoryDAO.searchCategoryDataForTree", paramMap);
		return resultList;
	}

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("categoryDAO.searchCodeDataList", paramMap);
		return resultList;
	}
	
	public List searchCodeDataForChild(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("categoryDAO.searchCodeDataForChild", paramMap);
		return resultList;
	}
	
	public int searchCodeDataForChildCnt(ModelMap paramMap) throws NkiaException{
		return (Integer)selectByPk("categoryDAO.searchCodeDataForChildCnt", paramMap);
	}
	
	public HashMap selectCategoryDetailData(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("categoryDAO.selectCategoryDetailData", paramMap);
	}
	public void insertCategoryInfo(ModelMap paramMap) throws NkiaException {
		this.update("categoryDAO.insertCategoryInfo", paramMap);
	}
	
	public void deleteCategoryInfo(ModelMap paramMap) throws NkiaException {
		this.update("categoryDAO.deleteCategoryInfo", paramMap);
	}
	
	public void updateCategoryInfo(ModelMap paramMap) throws NkiaException {
		this.update("categoryDAO.updateCategoryInfo", paramMap);
	}
	
	public void updateCategoryDeptUseAll(ModelMap paramMap) throws NkiaException {
		this.update("categoryDAO.updateCategoryDeptUseAll", paramMap);
	}
	
	public String selectCodeIdComp(ModelMap paramMap) throws NkiaException {
		return (String)selectByPk("categoryDAO.selectCodeIdComp", paramMap);
	}
	
	public void updateCodeOrder(ModelMap paramMap) throws NkiaException {
		this.update("categoryDAO.updateCodeOrder", paramMap);
	}
}
