package com.nkia.itg.workflow.kmdb.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("kmdbDAO")
public class KmdbDAO  extends EgovComAbstractDAO {

	public List searchCategoryTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("kmdbDAO.searchCategoryTree", paramMap);
		return resultList;
	}		
	
	public int searchKmdbApprovalListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) this.selectByPk("kmdbDAO.searchKmdbApprovalListCount", paramMap);
	}

	public List searchKmdbApprovalList(ModelMap paramMap) throws NkiaException {
		return this.list("kmdbDAO.searchKmdbApprovalList", paramMap);
	}
	
	public int searchKmdbRequestListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) this.selectByPk("kmdbDAO.searchKmdbRequestListCount", paramMap);
	}

	public List searchKmdbRequestList(ModelMap paramMap) throws NkiaException {
		return this.list("kmdbDAO.searchKmdbRequestList", paramMap);
	}	
	
	public void insertKmdb(ModelMap paramMap) throws NkiaException {
		this.insert("kmdbDAO.insertKmdb", paramMap);
	}
	
	public void updateKmdb(ModelMap paramMap) throws NkiaException {
		this.update("kmdbDAO.updateKmdb", paramMap);
	}
	
	public void updateKmdbStateChange(ModelMap paramMap) throws NkiaException {
		this.update("kmdbDAO.updateKmdbStateChange", paramMap);
	}
	
	public void useYnChange(ModelMap paramMap) throws NkiaException {
		this.update("kmdbDAO.useYnChange", paramMap);
	}
	
	public void updateCnt(ModelMap paramMap) throws NkiaException {
		this.update("kmdbDAO.updateCnt", paramMap);
	}
	
	public void deleteKmdb(ModelMap paramMap) throws NkiaException {
		this.delete("kmdbDAO.deleteKmdb", paramMap);
	}
	
	public HashMap selectKmdb(ModelMap paramMap) throws NkiaException{
		return (HashMap)selectByPk("kmdbDAO.selectKmdb", paramMap);
	}

	public int searchKmdbMainListCount(ModelMap paramMap) throws NkiaException{
		return (Integer) this.selectByPk("kmdbDAO.searchKmdbMainListCount", paramMap);
	}

	public List searchKmdbMainList(ModelMap paramMap) {
		return this.list("kmdbDAO.searchKmdbMainList", paramMap);
	}
	
	public void updateKmdbReaded(ModelMap paramMap) throws NkiaException {
		this.update("kmdbDAO.updateKmdbReaded", paramMap);
	}
}
