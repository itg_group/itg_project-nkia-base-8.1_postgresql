package com.nkia.itg.workflow.kmdb.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.kmdb.service.KmdbService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class KmdbController {
	@Resource(name="kmdbService")
	private KmdbService kmdbService;
	
/********************
 *   KMDB 관리화면 호출
********************/
	
	/**
	 * message 호출
	 */	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * KMDB 관리 - 관리화면 탭생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbManager.do")
	public String kmdbManager(ModelMap paramMap) throws NkiaException{
		return "/itg/workflow/kmdb/manage/kmdbManagerTab.nvf";
	}
	
	/**
	 * KMDB 관리 - 관리화면 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbManagerList.do")
	public String kmdbManagerList(ModelMap paramMap) throws NkiaException{
		return "/itg/workflow/kmdb/manage/kmdbManagerList.nvf";
	}
	
	/**
	 * KMDB 조회 - 조회화면 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbMain.do")
	public String kmdbMain(ModelMap paramMap) throws NkiaException{
		return "/itg/workflow/kmdb/main/kmdbMainTab.nvf";
	}
	
	/**
	 * KMDB 관리 - 신규등록 페이지로 이동
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbNewInput.do")
	public String kmdbNewInput(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		return "/itg/workflow/kmdb/manage/kmdbManagerEdit.nvf";
	}

	/**
	 * KMDB 관리 - 데이터 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbEdit.do")
	public String kmdbEdit(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		
		String KMDB_ID = (request.getParameter("KMDB_ID") != null)?request.getParameter("KMDB_ID"): "";
		paramMap.put("KMDB_ID", KMDB_ID);
		
		return "/itg/workflow/kmdb/manage/kmdbManagerEdit.nvf";
	}

/********************
 *   KMDB 요청화면 호출
********************/
	
	/**
	 * KMDB 요청 - 조회화면 탭생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbRequestor.do")
	public String kmdbRequestor(ModelMap paramMap) throws NkiaException{
		return "/itg/workflow/kmdb/request/kmdbRequestorTab.nvf";
	}	
	
	/**
	 * KMDB 조회 - 조회화면 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbMainList.do")
	public String kmdbMainList(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		return "/itg/workflow/kmdb/main/kmdbMainList.nvf";
	}		
	
	/**
	 * KMDB 요청 - 조회화면 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbRequestorList.do")
	public String kmdbRequestorList(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
				
		return "/itg/workflow/kmdb/request/kmdbRequestorList.nvf";
	}	
	
	/**
	 * KMDB 요청 - 신규등록 페이지로 이동
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbReqNewInput.do")
	public String kmdbReqNewInput(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		return "/itg/workflow/kmdb/request/kmdbRequestorEdit.nvf";
	}

	/**
	 * KMDB 요청 - 데이터 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbRequestorEdit.do")
	public String kmdbRequestorEdit(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		
		String KMDB_ID = (request.getParameter("KMDB_ID") != null)?request.getParameter("KMDB_ID"): "";
		paramMap.put("KMDB_ID", KMDB_ID);
		
		return "/itg/workflow/kmdb/request/kmdbRequestorEdit.nvf";
	}
	
	/**
	 * KMDB 조회 - 데이터 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/kmdbDetail.do")
	public String kmdbDetail(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		
		String KMDB_ID = (request.getParameter("KMDB_ID") != null)?request.getParameter("KMDB_ID"): "";
		paramMap.put("KMDB_ID", KMDB_ID);
		
		return "/itg/workflow/kmdb/main/kmdbDetail.nvf";
	}	
/********************
 *   KMDB 공통사용
********************/
	
	/**
	 * 
	 * 카테고리_트리생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/searchCategoryTree.do")
	public @ResponseBody ResultVO searchCategoryTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		TreeVO treeVO = null;
		List resultList = new ArrayList();
		try{
			resultList = kmdbService.searchCategoryTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * KMDB 관리화면 리스트 그리드 출력
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/searchKmdbRequestList.do")
	public @ResponseBody ResultVO searchKmdbRequestList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			totalCount = kmdbService.searchKmdbRequestListCount(paramMap);
			resultList = kmdbService.searchKmdbRequestList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * KMDB 관리화면 리스트 그리드 출력
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/searchKmdbApprovalList.do")
	public @ResponseBody ResultVO searchKmdbApprovalList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			totalCount = kmdbService.searchKmdbApprovalListCount(paramMap);
			resultList = kmdbService.searchKmdbApprovalList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * KMDB 조회화면 리스트 그리드 출력
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kmdb/searchKmdbMainList.do")
	public @ResponseBody ResultVO searchKmdbMainList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			totalCount = kmdbService.searchKmdbMainListCount(paramMap);
			resultList = kmdbService.searchKmdbMainList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * kmdb 관리 - 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/workflow/kmdb/insertKmdb.do")
	public @ResponseBody ResultVO insertKmdb(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
	    	kmdbService.insertKmdb(paramMap);
	    	resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/workflow/kmdb/approvalKmdb.do")
	public @ResponseBody ResultVO approvalKmdb(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
	    	kmdbService.approvalKmdb(paramMap);
	    	resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * kmdb 관리 - 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/workflow/kmdb/updateKmdb.do")
	public @ResponseBody ResultVO updateKmdb(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}
			kmdbService.updateKmdb(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * kmdb 관리 - 상태변경
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/workflow/kmdb/updateKmdbStateChange.do")
	public @ResponseBody ResultVO updateKmdbStateChange(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}
			kmdbService.updateKmdbStateChange(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * kmdb 관리 - 사용여부 변경
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/workflow/kmdb/useYnChange.do")
	public @ResponseBody ResultVO useYnChange(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}
			kmdbService.useYnChange(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * kmdb 관리 - 조회수 증가
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/workflow/kmdb/updateCnt.do")
	public @ResponseBody ResultVO updateCnt(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			kmdbService.updateCnt(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * kmdb 관리 - 읽기상태 변경
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/workflow/kmdb/updateKmdbReaded.do")
	public @ResponseBody ResultVO updateKmdbReaded(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			kmdbService.updateKmdbReaded(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * kmdb 관리 - 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/workflow/kmdb/deleteKmdb.do")
	public @ResponseBody ResultVO deleteKmdb(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}			
			kmdbService.deleteKmdb(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * kmdb 관리 - 선택된 데이터 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/workflow/kmdb/selectKmdb.do")
	public @ResponseBody ResultVO selectKmdb(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = kmdbService.selectKmdb(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
}
