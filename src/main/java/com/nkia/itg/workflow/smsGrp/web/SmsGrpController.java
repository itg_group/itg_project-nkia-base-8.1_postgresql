package com.nkia.itg.workflow.smsGrp.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.smsGrp.service.SmsGrpService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class SmsGrpController {

	@Resource(name = "smsGrpService")
	private SmsGrpService smsGrpService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	/**
	 * 문자메시지 그룹 등록 페이지 이동
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/goSmsGrp.do")
	public String goSmsGrp(ModelMap paramMap) throws NkiaException {
		
		return "/itg/workflow/smsGrp/smsGrp.nvf";
	}
	
	/**
	 * 문자메시지 발송 페이지 이동
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/goSmsSend.do")
	public String goSmsSend(ModelMap paramMap) throws NkiaException {
		
		String sendHpNo = NkiaApplicationPropertiesMap.getProperty("Globals.Sms.FromNo");
		// 문자메시지 발신 번호 셋팅 
		paramMap.put("sendHpNo", sendHpNo);
		
		return "/itg/workflow/smsGrp/smsSend.nvf";
	}
	
	/**
	 * 문자메시지 그룹 등록 페이지 이동
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/goSmsMessage.do")
	public String goSmsMessage(ModelMap paramMap) throws NkiaException {
		
		return "/itg/workflow/smsGrp/smsMessage.nvf";
	}
	
	/**
	 * 문자메시지 그룹 등록 페이지 이동
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value = "/itg/workflow/smsGrp/goSmsMessageHistory.do")
	public String goSmsMessageHistory(ModelMap paramMap) throws NkiaException {
		
		return "/itg/workflow/smsGrp/smsMessageHistory.nvf";
	}
	
	/**
	 * 문자메시지 그룹 트리 생성
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchSmsGrpTree.do")
	public @ResponseBody
	ResultVO searchSmsGrpTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		TreeVO treeVO = null;
		List resultList = null;

		try {
			resultList = smsGrpService.searchSmsGrpTree(paramMap);
			String up_node_id = (String) paramMap.get("up_node_id");
			Integer expand_level = (Integer) paramMap.get("expandLevel");

			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id,
					true, expand_level);
			resultVO.setTreeVO(treeVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 트리를 클릭 하였을 때 상세정보 검색
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/selectSmsGrpTree.do")
	public @ResponseBody
	ResultVO selectSmsGrpTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		HashMap<String, String> resultMap = null;
		
		try {
			resultMap = smsGrpService.selectSmsGrpTree(paramMap);
			resultVO.setResultMap(resultMap);
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 모드변경시 전체 카운트 검색
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/selectSmsGrpTreeCnt.do")
	public @ResponseBody
	ResultVO selectSmsGrpTreeCnt(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		int totalCount; 
		try {
			totalCount = smsGrpService.selectSmsGrpTreeCnt(paramMap);
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	
	/**
	 * 문자메시지 그룹 등록 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/insertSmsGrp.do")
	public @ResponseBody
	ResultVO insertSmsGrp(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {

			smsGrpService.insertSmsGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 문자메시지 그룹 수정 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/updateSmsGrp.do")
	public @ResponseBody
	ResultVO updateSmsGrp(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {

			smsGrpService.updateSmsGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 그룹 삭제 전 하위 등록인원이 있는지 체크 
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/selectGrpUserCnt.do")
	public @ResponseBody
	ResultVO selectGrpUserCnt(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		int totalCount;
		try {
			
			totalCount = smsGrpService.selectGrpUserCnt(paramMap);
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	
	
	/**
	 * 문자메시지 그룹 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/deleteSmsGrp.do")
	public @ResponseBody
	ResultVO deleteSmsGrp(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {

			smsGrpService.deleteSmsGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 부서 및 사용자 트리 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchDeptUserTree.do")
	public @ResponseBody
	ResultVO searchDeptUserTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		TreeVO treeVO = null;
		List resultList = null;

		try {
			resultList = smsGrpService.searchDeptUserTree(paramMap);
			String up_node_id = (String) paramMap.get("up_node_id");
			Integer expand_level = (Integer) paramMap.get("expandLevel");

			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id,
					true, expand_level, false);
			resultVO.setTreeVO(treeVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * SMS 그룹에 등록된 사용자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchSmsGrpUserList.do")
	public @ResponseBody
	ResultVO searchSmsGrpUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;

		try {
			resultList = smsGrpService.searchSmsGrpUserList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * SMS 그룹에 사용자 등록하기
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/insertSmsGrpUser.do")
	public @ResponseBody
	ResultVO insertSmsGrpUser(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			smsGrpService.insertSmsGrpUser(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * SMS 그룹에 등록된 사용자 삭제 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/deleteSmsGrpUser.do")
	public @ResponseBody
	ResultVO deleteSmsGrpUser(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			smsGrpService.deleteSmsGrpUser(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	// 문자메시지 그룹관리 종료
	//------------------------------------------------------------//
	// 문자메시지 발송 시작
	
	/**
	 * SMS 사용자 그리드 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchSmsUserList.do")
	public @ResponseBody
	ResultVO searchSmsUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;
		int totalCount;
		try {
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = smsGrpService.searchSmsUserListCount(paramMap);
			resultList = smsGrpService.searchSmsUserList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * SMS 그룹 그리드 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchSmsGrpList.do")
	public @ResponseBody
	ResultVO searchSmsGrpList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;
		int totalCount;
		try {
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = smsGrpService.searchSmsGrpListCount(paramMap);
			resultList = smsGrpService.searchSmsGrpList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * SMS 발송 대상 그리드 첫 로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchSmsSendList.do")
	public @ResponseBody
	ResultVO searchSmsSendList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;
		try {
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 입력받은 전화번호를 가지고 사용자 목록에서 검색하기
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchHpNoUserList.do")
	public @ResponseBody
	ResultVO searchHpNoUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;
		int totalCount;
		try {
			
			totalCount = smsGrpService.searchHpNoUserListCnt(paramMap);
			resultList = smsGrpService.searchHpNoUserList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 부서별 사용자 그리드 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchDeptUserGrid.do")
	public @ResponseBody ResultVO searchDeptUserGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;
		int totalCount;
		try {
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = smsGrpService.searchDeptUserGridCnt(paramMap);
			resultList = smsGrpService.searchDeptUserGrid(paramMap);
			gridVO.setRows(resultList);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 부서별 사용자 검색폼에서 사용할 부서 콤보박스 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchDeptComboBox.do")
	public @ResponseBody ResultVO searchDeptComboBox(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;

		try {
			resultList = smsGrpService.searchDeptComboBox(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 문자메시지 그룹 유저 수정 - 2014.07.30 JY Jeong
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/updateSmsGrpUserCustom.do")
	public @ResponseBody
	ResultVO updateSmsGrpUserCustom(@RequestBody ModelMap paramMap)
			throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {

			smsGrpService.updateSmsGrpUserCustom(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
			
		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 장애전파 등록되된 리스트
	 * @param paramMap end_yn - 상황종료 여부
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchMessageList.do")
	public @ResponseBody
	ResultVO searchMessageList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;

		try {
			resultList = smsGrpService.searchMessageList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 장애전파 등록되된 리스트
	 * @param paramMap end_yn - 상황종료 여부
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchMessageListHistory.do")
	public @ResponseBody ResultVO searchMessageListHistory(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;

		try {
			resultList = smsGrpService.searchMessageListHistory(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 장애전파 등록되된 리스트
	 * @param paramMap end_yn - 상황종료 여부
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchMessageListHistoryExcelDown.do")
	public @ResponseBody ResultVO searchMessageListHistoryExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		List resultList = null;
		Map excelParam = (Map) paramMap.get("excelParam");
		Map param =  (Map) paramMap.get("param");
		
		try {
			resultList = smsGrpService.searchMessageListHistoryExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	/**
	 * 장애전파 등록유저된 리스트
	 * @param paramMap end_yn - 상황종료 여부
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchMessageUserList.do")
	public @ResponseBody
	ResultVO searchMessageUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;

		try {
			resultList = smsGrpService.searchMessageUserList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	/**
	 * 장애전파 그룹  등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/smsGrp/insertMessageList.do")
	public @ResponseBody ResultVO insertMessageList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = smsGrpService.insertMessageList(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 장애전파 그룹  수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/smsGrp/updateMessageList.do")
	public @ResponseBody ResultVO updateMessageList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = smsGrpService.updateMessageList(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 장애전파 상황종료
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/smsGrp/updateEndMessage.do")
	public @ResponseBody ResultVO updateEndMessage(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = smsGrpService.updateEndMessage(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 장애전파 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/smsGrp/deleteMessageList.do")
	public @ResponseBody ResultVO deleteMessageList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = smsGrpService.deleteMessageList(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 알림메세지발송
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/smsGrp/insertSendMessage.do")
	public @ResponseBody ResultVO insertSendMessage(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = smsGrpService.insertSendMessage(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 장애전파 전체 리스트
	 * @param paramMap end_yn - 상황종료 여부
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchMessageAllHistory.do")
	public @ResponseBody ResultVO searchMessageAllHistory(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;

		try {
			resultList = smsGrpService.searchMessageAllHistory(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 장애전파 전체 리스트
	 * @param paramMap end_yn - 상황종료 여부
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/smsGrp/searchMessageAllHistoryExcelDown.do")
	public @ResponseBody ResultVO searchMessageAllHistoryExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		List resultList = null;
		List sheetNameList = null;
		Map excelParam = (Map) paramMap.get("excelParam");
		Map param =  (Map) paramMap.get("param");
		
		try {
			//sheetNameList = smsGrpService.searchMessageAllHistoryExcelDownSheetName(param);
			resultList = smsGrpService.searchMessageAllHistoryExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			//excelMap = excelMaker.makeExcelFileTab(excelParam, resultList,sheetNameList);
			
			resultVO.setResultMap(excelMap);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
}
