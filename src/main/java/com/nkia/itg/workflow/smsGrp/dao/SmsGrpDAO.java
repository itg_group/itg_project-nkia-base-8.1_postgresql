package com.nkia.itg.workflow.smsGrp.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("smsGrpDAO")
public class SmsGrpDAO extends EgovComAbstractDAO{

	public List searchSmsGrpTree(ModelMap paramMap) throws NkiaException {

		return list("smsGrpDAO.searchSmsGrpTree", paramMap);
	}

	public HashMap<String, String> selectSmsGrpTree(ModelMap paramMap) throws NkiaException {

		return (HashMap<String, String>) selectByPk("smsGrpDAO.selectSmsGrpTree", paramMap);
	}
	
	public String selectSmsGrpId(ModelMap paramMap) throws NkiaException {

		return (String) selectByPk("smsGrpDAO.selectSmsGrpId", paramMap);
	}
	

	public int selectSmsGrpTreeCnt(ModelMap paramMap) throws NkiaException{

		return (Integer) selectByPk("smsGrpDAO.selectSmsGrpTreeCnt", paramMap);
	}
	
	
	public void insertSmsGrp(ModelMap paramMap) throws NkiaException{

		insert("smsGrpDAO.insertSmsGrp", paramMap);
	}

	public void updateSmsGrp(ModelMap paramMap) throws NkiaException{
		
		update("smsGrpDAO.updateSmsGrp", paramMap);
	}

	public int selectGrpUserCnt(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("smsGrpDAO.selectGrpUserCnt", paramMap);
	}
	
	public void deleteSmsGrp(ModelMap paramMap) throws NkiaException{

		delete("smsGrpDAO.deleteSmsGrp", paramMap);
	}

	public List searchDeptUserTree(ModelMap paramMap) throws NkiaException {

		return list("smsGrpDAO.searchDeptUserTree", paramMap);
	}

	public List searchSmsGrpUserList(ModelMap paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchSmsGrpUserList", paramMap);
	}

	public int selectSmsGrpUserCnt(Map param) throws NkiaException {

		return (Integer) selectByPk("smsGrpDAO.selectSmsGrpUserCnt", param);
	}

	public void insertSmsGrpUser(Map param) throws NkiaException {
		
		insert("smsGrpDAO.insertSmsGrpUser", param);
	}

	public void deleteSmsGrpUser(Map param) throws NkiaException{

		delete("smsGrpDAO.deleteSmsGrpUser", param);
	}

	public void deleteSmsGrpUserAll(ModelMap paramMap) throws NkiaException{
		delete("smsGrpDAO.deleteSmsGrpUserAll", paramMap);
	}

	public int searchSmsUserListCount(ModelMap paramMap) {

		return (Integer) selectByPk("smsGrpDAO.searchSmsUserListCount", paramMap);
	}
	
	public List searchSmsUserList(ModelMap paramMap) throws NkiaException{

		return list("smsGrpDAO.searchSmsUserList", paramMap);
	}
	
	public int searchSmsGrpListCount(ModelMap paramMap) throws NkiaException{

		return (Integer) selectByPk("smsGrpDAO.searchSmsGrpListCount", paramMap);
	}

	public List searchSmsGrpList(ModelMap paramMap) throws NkiaException{

		return list("smsGrpDAO.searchSmsGrpList", paramMap);
	}
	
	public int searchHpNoUserListCnt(ModelMap paramMap) throws NkiaException {

		return (Integer) selectByPk("smsGrpDAO.searchHpNoUserListCnt", paramMap);
	}
	
	public List searchHpNoUserList(ModelMap paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchHpNoUserList",  paramMap);
	}

	public int searchDeptUserTotalCnt(ModelMap paramMap) throws NkiaException{

		return (Integer) selectByPk("smsGrpDAO.searchDeptUserTotalCnt", paramMap);
	}
	
	public int searchDeptUserGridCnt(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("smsGrpDAO.searchDeptUserGridCnt", paramMap);
	}
	
	public List searchDeptUserGrid(ModelMap paramMap) throws NkiaException {

		return list("smsGrpDAO.searchDeptUserGrid", paramMap);
	}

	public List searchDeptComboBox(ModelMap paramMap) throws NkiaException {

		return list("smsGrpDAO.searchDeptComboBox", paramMap);
	}

	public void insertSmsGrpUserCustom(Map param) throws NkiaException {
		
		insert("smsGrpDAO.insertSmsGrpUserCustom", param);
	}

	public void deleteSmsGrpUserCustom(Map param) throws NkiaException{

		delete("smsGrpDAO.deleteSmsGrpUserCustom", param);
	}
	
	public void updateSmsGrpChildUseYn(HashMap paramMap) throws NkiaException{
		
		update("smsGrpDAO.updateSmsGrpChildUseYn", paramMap);
	}
	
	public List searchMessageList(ModelMap paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchMessageList", paramMap);
	}
	
	public List searchMessageListHistory(ModelMap paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchMessageListHistory", paramMap);
	}
	
	public List searchMessageListHistoryExcelDown(Map paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchMessageListHistory", paramMap);
	}
	
	public List searchMessageUserList(ModelMap paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchMessageUserList", paramMap);
	}

	public String selectSpreadId(ModelMap paramMap) throws NkiaException {

		return (String) selectByPk("smsGrpDAO.selectSpreadId", paramMap);
	}
	public void insertMessageList(Object object) throws NkiaException {
		this.insert("smsGrpDAO.insertMessageList", object);
	}
	public void updateMessageList(Object object) throws NkiaException {
		update("smsGrpDAO.updateMessageList", object);
	}
	public void insertMessageUserList(Object object) throws NkiaException {
		this.insert("smsGrpDAO.insertMessageUserList", object);
	}
	public void updateEndMessage(HashMap paramMap) throws NkiaException{
		
		update("smsGrpDAO.updateEndMessage", paramMap);
	}
	public void updateActiveFalse(HashMap paramMap) throws NkiaException{
		
		update("smsGrpDAO.updateActiveFalse", paramMap);
	}

	public void deleteMessageList(HashMap paramMap) throws NkiaException{

		delete("smsGrpDAO.deleteMessageList", paramMap);
	}
	
	public void deleteMessageUserList(HashMap paramMap) throws NkiaException{

		delete("smsGrpDAO.deleteMessageUserList", paramMap);
	}
	public void insertMessageListHistory(Object object) throws NkiaException {
		this.insert("smsGrpDAO.insertMessageListHistory", object);
	}
	public List sendMessageUserList(ModelMap paramMap) throws NkiaException {
		
		return list("smsGrpDAO.sendMessageUserList", paramMap);
	}
	public List searchMessageAllHistory(ModelMap paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchMessageAllHistory", paramMap);
	}
	
	public List searchMessageAllHistoryExcelDown(Map paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchMessageAllHistoryExcelDown", paramMap);
	}
	
	public List searchMessageAllHistoryExcelDownSheetName(Map paramMap) throws NkiaException {
		
		return list("smsGrpDAO.searchMessageAllHistoryExcelDownSheetName", paramMap);
	}
}
