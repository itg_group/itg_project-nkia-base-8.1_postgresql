package com.nkia.itg.workflow.smsGrp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SmsGrpService {

	public List searchSmsGrpTree(ModelMap paramMap) throws NkiaException;

	public HashMap<String, String> selectSmsGrpTree(ModelMap paramMap) throws NkiaException;

	public int selectSmsGrpTreeCnt(ModelMap paramMap) throws NkiaException;

	public void insertSmsGrp(ModelMap paramMap) throws NkiaException;

	public void updateSmsGrp(ModelMap paramMap) throws NkiaException;

	public void deleteSmsGrp(ModelMap paramMap) throws NkiaException;

	public List searchDeptUserTree(ModelMap paramMap) throws NkiaException;

	public List searchSmsGrpUserList(ModelMap paramMap) throws NkiaException;

	public void insertSmsGrpUser(ModelMap paramMap) throws NkiaException;

	public int selectGrpUserCnt(ModelMap paramMap) throws NkiaException;
	
	public void deleteSmsGrpUser(ModelMap paramMap) throws NkiaException;

	public int searchSmsUserListCount(ModelMap paramMap) throws NkiaException;

	public List searchSmsUserList(ModelMap paramMap) throws NkiaException;

	public List searchSmsGrpList(ModelMap paramMap) throws NkiaException;

	public int searchSmsGrpListCount(ModelMap paramMap) throws NkiaException;

	public int searchHpNoUserListCnt(ModelMap paramMap) throws NkiaException;

	public List searchHpNoUserList(ModelMap paramMap) throws NkiaException;

	public int searchDeptUserTotalCnt(ModelMap paramMap) throws NkiaException;
	
	public int searchDeptUserGridCnt(ModelMap paramMap) throws NkiaException;
	
	public List searchDeptUserGrid(ModelMap paramMap) throws NkiaException;

	public List searchDeptComboBox(ModelMap paramMap) throws NkiaException; 
	
	// 문자메시지 그룹 유저 수정 - 2014.07.30 JY Jeong
	public void updateSmsGrpUserCustom(ModelMap paramMap) throws NkiaException;
	
	public List searchMessageList(ModelMap paramMap) throws NkiaException;
	
	public List searchMessageListHistory(ModelMap paramMap) throws NkiaException;
	
	public List searchMessageListHistoryExcelDown(Map paramMap) throws NkiaException;
	
	public List searchMessageUserList(ModelMap paramMap) throws NkiaException;
	
	public HashMap<String, String> insertSendMessage(ModelMap paramMap) throws NkiaException;

	public HashMap<String, String> insertMessageList(ModelMap paramMap) throws NkiaException;
	
	public HashMap<String, String> updateMessageList(ModelMap paramMap) throws NkiaException;
	
	public HashMap<String, String> updateEndMessage(ModelMap paramMap) throws NkiaException;
	
	public HashMap<String, String> deleteMessageList(ModelMap paramMap) throws NkiaException;
	
	public List searchMessageAllHistory(ModelMap paramMap) throws NkiaException;
	
	public List searchMessageAllHistoryExcelDown(Map paramMap) throws NkiaException;
	
	public List searchMessageAllHistoryExcelDownSheetName(Map paramMap) throws NkiaException;

}
