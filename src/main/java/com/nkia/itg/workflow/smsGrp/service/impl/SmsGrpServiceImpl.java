package com.nkia.itg.workflow.smsGrp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.smsGrp.dao.SmsGrpDAO;
import com.nkia.itg.workflow.smsGrp.service.SmsGrpService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("smsGrpService")
public class SmsGrpServiceImpl implements SmsGrpService{

	@Resource(name="smsGrpDAO")
	private SmsGrpDAO smsGrpDAO;
	
	@Resource(name="smsSendDAO")
	private SmsSendDAO smsSendDAO;
	
	@Resource(name="smsgHistDAO")
	private SmsgHistDAO smsgHistDAO; 
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	
	public List searchSmsGrpTree(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchSmsGrpTree(paramMap);
	}

	public HashMap<String, String> selectSmsGrpTree(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.selectSmsGrpTree(paramMap);
	}
	
	public int selectSmsGrpTreeCnt(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.selectSmsGrpTreeCnt(paramMap);
	}
	

	public void insertSmsGrp(ModelMap paramMap) throws NkiaException {
		
		// ID 생성 
		String smsGrpId = smsGrpDAO.selectSmsGrpId(paramMap);
		paramMap.put("SMS_GRP_ID", smsGrpId);
		
		// 로그인한 세션유저정보 받아오기
		UserVO user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramMap.put("REG_USER_ID", user.getUser_id());
		
		// 등록 
		smsGrpDAO.insertSmsGrp(paramMap);
	}

	
	public void updateSmsGrp(ModelMap paramMap) throws NkiaException {
		
		// 로그인한 세션유저정보 받아오기
		UserVO user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramMap.put("UPD_USER_ID", user.getUser_id());
		
		String UseYn = (String)paramMap.get("USE_YN");
		String SmsGrpId = (String)paramMap.get("SMS_GRP_ID");
		
		// 수정
		smsGrpDAO.updateSmsGrp(paramMap);
		
		// 자식들의 사용유무도 부모와 같이 수정
		HashMap childMap = new HashMap();
		childMap.put("SMS_GRP_ID", SmsGrpId);
		childMap.put("USE_YN", UseYn);
		
		smsGrpDAO.updateSmsGrpChildUseYn(childMap);
	}
	
	// 그룹 내부에 사용자가 등록되어있는지 체크 합니다. 
	public int selectGrpUserCnt(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.selectGrpUserCnt(paramMap);
	}
	
	
	public void deleteSmsGrp(ModelMap paramMap) throws NkiaException {
		
		// 그룹 내부의 유저 삭제
		smsGrpDAO.deleteSmsGrpUserAll(paramMap);
		// 삭제
		smsGrpDAO.deleteSmsGrp(paramMap);
	}

	public List searchDeptUserTree(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchDeptUserTree(paramMap);
	}

	public List searchSmsGrpUserList(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchSmsGrpUserList(paramMap);
	}

	public void insertSmsGrpUser(ModelMap paramMap) throws NkiaException {

		List arrayList = (List) paramMap.get("arrayList");
		// 로그인한 세션유저정보 받아오기
		UserVO user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		// 그룹안에 있는 유저들 전부 삭제 합니다. 
		smsGrpDAO.deleteSmsGrpUserAll(paramMap);
		
		for (int i = 0; i < arrayList.size(); i++) {
			
			Map param = new HashMap();
			param.putAll((Map)arrayList.get(i));
			param.put("REG_USER_ID", user.getUser_id());
			smsGrpDAO.insertSmsGrpUser(param);
		}
	}

	public void deleteSmsGrpUser(ModelMap paramMap) throws NkiaException {

		List arrayList = (List) paramMap.get("arrayList");
		for (int i = 0; i < arrayList.size(); i++) {
					
			Map param = new HashMap();
			param.putAll((Map)arrayList.get(i));
			// 삭제 실행 (그룹안의 유저 단위 삭제)
			smsGrpDAO.deleteSmsGrpUser(param);
			
		}
	}

	public int searchSmsUserListCount(ModelMap paramMap) throws NkiaException {
		
		return smsGrpDAO.searchSmsUserListCount(paramMap);
		
	}

	public List searchSmsUserList(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchSmsUserList(paramMap);
	}

	public int searchSmsGrpListCount(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchSmsGrpListCount(paramMap);
	}
	
	public List searchSmsGrpList(ModelMap paramMap) throws NkiaException {
	
		return smsGrpDAO.searchSmsGrpList(paramMap);
	}

	public int searchHpNoUserListCnt(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchHpNoUserListCnt(paramMap);
	}
	
	
	public List searchHpNoUserList(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchHpNoUserList(paramMap);
	}

	public int searchDeptUserTotalCnt(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchDeptUserTotalCnt(paramMap);
	}
	
	public int searchDeptUserGridCnt(ModelMap paramMap) throws NkiaException {
		return smsGrpDAO.searchDeptUserGridCnt(paramMap);
	}
	
	public List searchDeptUserGrid(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchDeptUserGrid(paramMap);
	}

	public List searchDeptComboBox(ModelMap paramMap) throws NkiaException {
		
		return smsGrpDAO.searchDeptComboBox(paramMap);
	}

	// 문자메시지 그룹 유저 수정 - 2014.07.30 JY Jeong
	public void updateSmsGrpUserCustom(ModelMap paramMap) throws NkiaException {

		List dataList = (List) paramMap.get("dataList");
		UserVO user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		// 그룹 내 유저들 전부 삭제 후,
		smsGrpDAO.deleteSmsGrpUserCustom(paramMap);
		
		// 새로 입력한 유저 리스트 수 만큼 반복해서 DB에 insert.
		for (int i = 0; i < dataList.size(); i++) {
			
			Map param = new HashMap();
			param.putAll((Map)dataList.get(i));
			param.put("REG_USER_ID", user.getUser_id());
			smsGrpDAO.insertSmsGrpUserCustom(param);
		}
	}
	@Override
	public List searchMessageList(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchMessageList(paramMap);
	}
	@Override
	public List searchMessageListHistory(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchMessageListHistory(paramMap);
	}
	
	@Override
	public List searchMessageListHistoryExcelDown(Map paramMap) throws NkiaException {

		return smsGrpDAO.searchMessageListHistoryExcelDown(paramMap);
	}
	@Override
	public List searchMessageUserList(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchMessageUserList(paramMap);
	}
	
	@Override
	public HashMap<String, String> insertSendMessage(ModelMap paramMap) throws NkiaException {
		boolean checkLogic = true;
		HashMap<String, String> resultMap = new HashMap<String, String>();
		HashMap<String, String> checkMap = new HashMap<String, String>();
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		//알림톡 발송로직 들어가야하는곳
		//1. 발송인원 셀렉트
		List<HashMap> userList = smsGrpDAO.sendMessageUserList(paramMap);
		//2. 발송내용체크
			//2-1 유형에따라 템플릿코드 셋팅
		String templteCode = "";
		String senderKey = "";
		String sendPhone = "";
		if("INCIDENT".equals(userList.get(0).get("SPREAD_TYPE"))){
			//장애일경우
			templteCode = "LMSG_20200417102957072710";
			senderKey = "0cede52df082258ff06b59c92dffff975844c35f";
			sendPhone = "0226264271";
			
		}else{
			//재해,모의훈련,프로젝트일경우
			templteCode = "LMSG_20191024170021026942";
			senderKey = "0cede52df082258ff06b59c92dffff975844c35f";
			sendPhone = "0226264271";
		}
		//2-2  유형에따라 rebody 앞에 prefix 붙여줌   [자바스크립트에서 진행]
//		String rebody = "";
		//3. 알림톡테이블 등록		
		for(HashMap userData: userList){
			userData.put("TO_PHONE", userData.get("HP_NO")); // 받는사람전화번호
			userData.put("MSG_BODY", paramMap.get("CONTENT")); //알림톡 메세지
			userData.put("RE_BODY", paramMap.get("MESSAGECONTENT")); //실패시 보내는 대체 문자메세지
			userData.put("TEMPLATE_CODE", templteCode); //장애전파 템플릿코드
			userData.put("SENDER_KEY", senderKey); // 센더키
			userData.put("SEND_PHONE", sendPhone); // 보내는사람
			userData.put("USER_NM", userData.get("USER_NM"));
			userData.put("upd_user_id", paramMap.get("upd_user_id"));
			smsSendDAO.insertTalkList(userData);
			smsgHistDAO.insertTalkListHistory(userData);
		}
		//4. 히스토리테이블등록
		smsGrpDAO.insertMessageListHistory(paramMap);
		resultMap.put("RESULT_MSG", resultMsg);
		return resultMap;
	}

	@Override
	public HashMap<String, String> insertMessageList(ModelMap paramMap) throws NkiaException {
		boolean checkLogic = true;
		HashMap<String, String> resultMap = new HashMap<String, String>();
		HashMap<String, String> checkMap = new HashMap<String, String>();
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		
		String spread_id = smsGrpDAO.selectSpreadId(paramMap);
		paramMap.put("SPREAD_ID", spread_id);
		if("Y".equals(paramMap.get("ACTIVE_YN").toString())){
			smsGrpDAO.updateActiveFalse(paramMap);
		}
		smsGrpDAO.insertMessageList(paramMap);
		
		// 세부유형 건 삭제
		ArrayList array_node = (ArrayList) paramMap.get("userInfo");
		ArrayList array_grp_node = (ArrayList) paramMap.get("grpInfo");
		for(int i = 0; i <array_node.size(); i++){
			Map userMap = (Map) array_node.get(i);
			userMap.put("SPREAD_ID", spread_id);
			userMap.put("UPD_USER_ID", paramMap.get("upd_user_id").toString());
			smsGrpDAO.insertMessageUserList(userMap);
		}
		resultMap.put("RESULT_MSG", resultMsg);
		return resultMap;
	}
	
	public HashMap<String, String> updateMessageList(ModelMap paramMap) throws NkiaException {
		boolean checkLogic = true;
		HashMap<String, String> resultMap = new HashMap<String, String>();
		HashMap<String, String> checkMap = new HashMap<String, String>();
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		if("Y".equals(paramMap.get("ACTIVE_YN").toString())){
			smsGrpDAO.updateActiveFalse(paramMap);
		}
		smsGrpDAO.updateMessageList(paramMap);
		
		// 세부유형 건 삭제
		smsGrpDAO.deleteMessageUserList(paramMap);
		ArrayList array_node = (ArrayList) paramMap.get("userInfo");
		for(int i = 0; i <array_node.size(); i++){
			Map userMap = (Map) array_node.get(i);
			userMap.put("SPREAD_ID", paramMap.get("SPREAD_ID").toString());
			userMap.put("UPD_USER_ID", paramMap.get("upd_user_id").toString());
			smsGrpDAO.insertMessageUserList(userMap);
		}

		resultMap.put("RESULT_MSG", resultMsg);
		return resultMap;
	}
	@Override
	public HashMap<String, String> updateEndMessage(ModelMap paramMap) throws NkiaException {
		HashMap<String, String> resultMap = new HashMap<String, String>();
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		
		
		// 세부유형 건 삭제
		HashMap<String,String> userMap = new HashMap<String, String>();
		ArrayList array_node = (ArrayList) paramMap.get("spreadInfo");
		userMap.put("UPD_USER_ID", paramMap.get("upd_user_id").toString());
		for( int i = 0; i < array_node.size(); i++ ){
			 userMap.put("SPREAD_ID", array_node.get(i).toString());
			smsGrpDAO.updateEndMessage(userMap);
		}
		
		resultMap.put("RESULT_MSG", resultMsg);
		return resultMap;
	}
	
	
	public HashMap<String, String> deleteMessageList(ModelMap paramMap) throws NkiaException {
		HashMap<String, String> resultMap = new HashMap<String, String>();
		String resultMsg = "완료되었습니다.";
		
		
		// 세부유형 건 삭제
		HashMap<String,String> userMap = new HashMap<String, String>();
		ArrayList array_node = (ArrayList) paramMap.get("spreadInfo");
		for( int i = 0; i < array_node.size(); i++ ){
			 userMap.put("SPREAD_ID", array_node.get(i).toString());
			 smsGrpDAO.deleteMessageList(userMap);
			smsGrpDAO.deleteMessageUserList(userMap);
		}
		
		resultMap.put("RESULT_MSG", resultMsg);
		return resultMap;
	}
	
	public List searchMessageAllHistory(ModelMap paramMap) throws NkiaException {

		return smsGrpDAO.searchMessageAllHistory(paramMap);
	}
	
	public List searchMessageAllHistoryExcelDown(Map paramMap) throws NkiaException {

		return smsGrpDAO.searchMessageAllHistoryExcelDown(paramMap);
	}
	
	public List searchMessageAllHistoryExcelDownSheetName(Map paramMap) throws NkiaException {

		return smsGrpDAO.searchMessageAllHistoryExcelDownSheetName(paramMap);
	}
}
