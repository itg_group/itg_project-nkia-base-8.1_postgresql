/*
 * @(#)workCheckListService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckList.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface WorkCheckListService {

	public Map<String, Object> searchWorkCheckList(Map<String, Object> paramMap) throws Exception;

	public List<Map<String, Object>> searchWorkCheckListList(Map<String, Object> paramMap) throws Exception;

	public int searchWorkCheckListCount(Map<String, Object> paramMap) throws Exception;
	
	public void insertWorkCheckList(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkCheckList(Map<String, Object> paramMap) throws Exception;
	
	public void insertWorkCheckDtlList(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkCheckDtlList(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkCheckGridList(Map<String, Object> paramMap) throws Exception;
	
	public void deleteWorkCheckList(Map<String, Object> paramMap) throws Exception;
	

	public List<Map<String, Object>> searchWorkSrCheckList(Map<String, Object> paramMap) throws Exception;

	public void saveWorkSrCheckList(Map<String, Object> paramMap) throws Exception;

	public List<Map<String, Object>> selectWorkCheckGroupList(Map<String, Object> paramMap) throws Exception;

	public HashMap selectWorkCheckListGrpInfo(Map paramMap) throws Exception;

	public void insertWorkCheckListGrp(Map paramMap) throws Exception;

	public void deleteWorkCheckListGrp(Map paramMap) throws Exception;

	public void updateWorkCheckListGrp(Map paramMap) throws Exception;

	public String selectWorkCheckGroupNewId(Map paramMap) throws Exception;
	
	public List<Map<String, Object>> searchVersion(Map<String, Object> paramMap) throws Exception;
	
	public int selectWorkCheckGroupCount(Map<String, Object> paramMap) throws Exception;
}
