/*
 * @(#)workCheckListServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckList.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckList.dao.WorkCheckListDAO;
import com.nkia.itg.workflow.workCheckList.service.WorkCheckListService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("workCheckListService")
public class WorkCheckListServiceImpl implements WorkCheckListService{
	
	private static final Logger logger = LoggerFactory.getLogger(WorkCheckListServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "workCheckListDAO")
	public WorkCheckListDAO workCheckListDAO;

	public Map<String, Object> searchWorkCheckList(Map<String, Object> paramMap) throws Exception {
		return workCheckListDAO.searchWorkCheckList(paramMap);
	}
	
	public List<Map<String, Object>> searchWorkCheckListList(Map<String, Object> paramMap) throws Exception {
		return workCheckListDAO.searchWorkCheckListList(paramMap);
	}
	
	public int searchWorkCheckListCount(Map<String, Object> paramMap) throws Exception {
		return workCheckListDAO.searchWorkCheckListCount(paramMap);
	}
	
	public void insertWorkCheckList(Map<String, Object> paramMap) throws Exception {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}
		
		String newId = workCheckListDAO.selectModelNewId(paramMap);
		paramMap.put("check_list_id", newId);
		workCheckListDAO.insertWorkCheckList(paramMap);
	}

	public void updateWorkCheckList(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 수동지표 계약수정
		workCheckListDAO.updateWorkCheckList(paramMap);
	}
	
	public void insertWorkCheckDtlList(Map<String, Object> paramMap) throws Exception {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}
		
		String newId = workCheckListDAO.selectModelNewId(paramMap);
		paramMap.put("check_list_id", newId);
		workCheckListDAO.insertWorkCheckDtlList(paramMap);
	}
	
	public void updateWorkCheckDtlList(Map<String, Object> paramMap) throws Exception {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}
				
		workCheckListDAO.updateWorkCheckDtlList(paramMap);
	}	

	public void updateWorkCheckGridList(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		//String name = (String)paramMap.get("INPUT1_NAME");
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");
		for(Map<String, Object> map: list){
			if("".equals(map.get("check_list_id"))){
				throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 체크목록아이디"); // 필수 입력 항목을 확인 바랍니다.
			}
			//if("".equals(map.get("check_info"))){
			//	throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 점검내역"); // 필수 입력 항목을 확인 바랍니다.
			//}
		}
		
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			mPs = new HashMap<String, Object>();
			mPs.put("check_list_id", map.get("check_list_id")); // PK 체크목록 아이디
			mPs.put("check_info", map.get("check_info")); // 점검내역
			mPs.put("login_user_id", login_user_id);
			workCheckListDAO.updateWorkCheckList(mPs); 
		}
	}
	
	public void deleteWorkCheckList(Map<String, Object> paramMap) throws Exception {
		workCheckListDAO.deleteWorkCheckList(paramMap);
	}
	
	public List<Map<String, Object>> searchWorkSrCheckList(Map<String, Object> paramMap) throws Exception {
		return workCheckListDAO.searchWorkSrCheckListList(paramMap);
	}
	
	public void saveWorkSrCheckList(Map<String, Object> paramMap) throws Exception {

		if(!paramMap.containsKey("sr_id")){
			throw new NkiaException("필수항목 정보가 없습니다. sr_id ");
		}
		if(!paramMap.containsKey("INPUT1_LIST")){
			throw new NkiaException("필수항목 정보가 없습니다. INPUT1_LIST ");
		}
		
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		List<Map<String, Object>> input1List = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");
		for(int i=0; i<input1List.size(); i++){
			Map<String, Object> map = input1List.get(i);
			map.put("sr_id", paramMap.get("sr_id"));
			map.put("login_user_id", login_user_id);
			if(null == workCheckListDAO.searchWorkSrCheckList(map)){
				workCheckListDAO.insertWorkSrCheckList(map);
			}else{
				workCheckListDAO.updateWorkSrCheckList(map);
			}
		}
	}
	
	public List<Map<String, Object>> selectWorkCheckGroupList(Map<String, Object> paramMap) throws Exception {
		return workCheckListDAO.selectWorkCheckGroupList(paramMap);
	}

	@Override
	public HashMap selectWorkCheckListGrpInfo(Map paramMap) throws Exception {
		return workCheckListDAO.selectWorkCheckListGrpInfo(paramMap);
	}

	@Override
	public String selectWorkCheckGroupNewId(Map paramMap) throws Exception {
		String newId = workCheckListDAO.selectWorkCheckGroupNewId(paramMap);
		return newId;
	}
	
	@Override
	public void insertWorkCheckListGrp(Map paramMap) throws Exception {
		workCheckListDAO.insertWorkCheckListGrp(paramMap);
		
		List checkList = (List) paramMap.get("list");
		//deleteWorkCheckList(paramMap);
		Map searchMap = new HashMap();
		searchMap.put("check_group_cd", (String)paramMap.get("check_group_cd"));
		String version = workCheckListDAO.selectMaxVersion(searchMap);
		if(checkList.size() > 0){
			for(int i=0; i<checkList.size(); i++){
				Map listMap = (Map)checkList.get(i);
				listMap = WebUtil.lowerCaseMapKey(listMap);
				listMap.put("check_group_cd", (String)paramMap.get("check_group_cd"));
				listMap.put("display_no", i);
				listMap.put("version", version);
				insertWorkCheckDtlList(listMap);
			}
		}
		
	}

	@Override
	public void deleteWorkCheckListGrp(Map paramMap) throws Exception {
		workCheckListDAO.deleteWorkCheckListGrp(paramMap);
		deleteWorkCheckList(paramMap);
	}

	@Override
	public void updateWorkCheckListGrp(Map paramMap) throws Exception {
		workCheckListDAO.updateWorkCheckListGrp(paramMap);
		
		List checkList = (List) paramMap.get("list");
		Map searchMap = new HashMap();
		searchMap.put("check_group_cd", (String)paramMap.get("check_group_cd"));
		String version = workCheckListDAO.selectMaxVersion(searchMap);
		//deleteWorkCheckList(paramMap);
		if(checkList.size() > 0){
			for(int i=0; i<checkList.size(); i++){
				Map listMap = (Map)checkList.get(i);
				listMap = WebUtil.lowerCaseMapKey(listMap);
				listMap.put("check_group_cd", (String)paramMap.get("check_group_cd"));
				listMap.put("display_no", i);
				listMap.put("version", version);
				listMap.put("check_list_id", ((Map)checkList.get(i)).get("CHECK_LIST_ID"));
							
				if((((Map)checkList.get(i)).get("CHECK_LIST_ID"))==null){				
					insertWorkCheckDtlList(listMap);
				}else{				
					updateWorkCheckDtlList(listMap);
				}
			}
		}
	}
	public List<Map<String, Object>> searchVersion(Map<String, Object> paramMap) throws Exception {
		return workCheckListDAO.searchVersion(paramMap);
	}

	@Override
	public int selectWorkCheckGroupCount(Map<String, Object> paramMap)
			throws Exception {
		// TODO Auto-generated method stub
		return workCheckListDAO.selectWorkCheckGroupCount(paramMap);
	}
}