/*
 * @(#)WorkCheckListController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckList.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckList.service.WorkCheckListService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkCheckListController {
	
	@Resource(name = "workCheckListService")
	private WorkCheckListService workCheckListService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	// 체크리스트 기본 탭
	@RequestMapping(value="/itg/workflow/workCheckList/goWorkCheckListBaseTab.do")
	public String goWorkCheckListBaseTab(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		String forwarPage = "/itg/workflow/workCheckList/workCheckListBaseTab.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 체크리스트 관리
	@RequestMapping(value="/itg/workflow/workCheckList/goWorkCheckListMng.do")
	public String goWorkCheckListMng(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		String forwarPage = "/itg/workflow/workCheckList/workCheckListMng.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}

	// 체크리스트 목록
	@RequestMapping(value="/itg/workflow/workCheckList/goWorkCheckListList.do")
	public String goWorkCheckListList(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		String forwarPage = "/itg/workflow/workCheckList/workCheckListList.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 체크리스트 상세 팝업
	@RequestMapping(value="/itg/workflow/workCheckList/goWorkCheckListDetail.do")
	public String goWorkCheckListDetailPop(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		String forwarPage = "/itg/workflow/workCheckList/workCheckListDetail.nvf";
		
		// 신규등록인 경우 체크리스트그룹 ID 가져오기
		if("new".equals((String)reqMap.get("mode"))){
			String newId = workCheckListService.selectWorkCheckGroupNewId(paramMap);
			reqMap.put("check_group_cd", newId);
		}
		
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	/**
	 * 체크리스트 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workCheckList/searchWorkCheckList.do")
	public @ResponseBody ResultVO searchWorkCheckList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			Map <String , Object> resultMap = workCheckListService.searchWorkCheckList(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workCheckList/searchWorkCheckListList.do")
	public @ResponseBody ResultVO searchWorkCheckListList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			totalCount = workCheckListService.searchWorkCheckListCount(paramMap);
			List<Map<String, Object>> resultList = workCheckListService.searchWorkCheckListList(paramMap);		
		
			gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 체크리스트 전체 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workCheckList/searchWorkCheckAllList.do")
	public @ResponseBody ResultVO searchWorkCheckAllList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = workCheckListService.searchWorkCheckListList(paramMap);		
			//totalCount = functionPointService.selectModuleSelPopCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 체크리스트 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckList/searchWorkCheckListExcelDown.do")
	public @ResponseBody ResultVO searchWorkCheckListExcelDown(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map<String, Object> excelParam = (Map<String, Object>) paramMap.get("excelParam");
			Map<String, Object> param = (Map<String, Object>) paramMap.get("param");
			List<Map<String, Object>> resultList = workCheckListService.searchWorkCheckListList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 - 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workCheckList/insertWorkCheckList.do")
	public @ResponseBody ResultVO insertWorkCheckList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{		
			workCheckListService.insertWorkCheckList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 체크리스트 - 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckList/updateWorkCheckList.do")
	public @ResponseBody ResultVO updateWorkCheckList(@RequestBody ModelMap paramMap)throws Exception  {
		ResultVO resultVO = new ResultVO();
		try{			
			workCheckListService.updateWorkCheckList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 체크리스트 - 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckList/updateWorkCheckGridList.do")
	public @ResponseBody ResultVO updateWorkCheckGridList(@RequestBody ModelMap paramMap)throws Exception  {
		ResultVO resultVO = new ResultVO();
		try{			
			workCheckListService.updateWorkCheckGridList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 -  삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckList/deleteWorkCheckList.do")
	public @ResponseBody ResultVO deleteWorkCheckList(@RequestBody ModelMap paramMap)throws Exception  {
		ResultVO resultVO = new ResultVO();
		try{
			workCheckListService.deleteWorkCheckList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

    /**
	 * 체크리스트 윈도우팝업을 오픈한다.
	 * @exception Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckList/openWindowPopup.do")
	public String openWindowPopup(HttpServletRequest request, @RequestParam Map<String, Object> reqMap, ModelMap paramMap) throws Exception {
		if (null == reqMap.get("popupId") || "".equals(reqMap.get("popupId"))) {
			throw new NkiaException("popupId 는 필수입력입니다.");
		}
		String popupId = StringUtil.replaceNull(String.valueOf(reqMap.get("popupId")), "");
		paramMap.put("reqMap", reqMap);
		String forwarPage = "/itg/workflow/workCheckList/popup/" + popupId + ".nvf";
		return forwarPage;
	}
	
	/**
	 * 요청 체크리스트 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workCheckList/searchWorkSrCheckList.do")
	public @ResponseBody ResultVO searchWorkSrCheckList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			//int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = workCheckListService.searchWorkSrCheckList(paramMap);		
			//totalCount = workCheckListService.searchWorkCheckListCount(paramMap);
			
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 요청 체크리스트 -  저장
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckList/saveWorkSrCheckList.do")
	public @ResponseBody ResultVO saveWorkSrCheckList(@RequestBody ModelMap paramMap)throws Exception  {
		ResultVO resultVO = new ResultVO();
		try{
			workCheckListService.saveWorkSrCheckList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 체크리스트 그룹 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workCheckList/selectWorkCheckGroupList.do")
	public @ResponseBody ResultVO selectWorkCheckGroupList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = workCheckListService.selectWorkCheckGroupList(paramMap);	
			totalCount = workCheckListService.selectWorkCheckGroupCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 *  체크리스트 그룹 리스트 접보 출력
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckList/selectWorkCheckListGrpInfo.do")
	public @ResponseBody ResultVO selectWorkCheckListGrpInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = workCheckListService.selectWorkCheckListGrpInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 그룹 등록
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckList/insertWorkCheckListGrp.do")
	public @ResponseBody ResultVO insertWorkCheckListGrp(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String UPD_USER_ID = userVO.getUser_id();
			paramMap.put("ins_user_id", UPD_USER_ID);
			
			workCheckListService.insertWorkCheckListGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
		
	/**
	 * 체크리스트 그룹 삭제
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckList/deleteWorkCheckListGrp.do")
	public @ResponseBody ResultVO deleteWorkCheckListGrp(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			workCheckListService.deleteWorkCheckListGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 그룹 수정
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckList/updateWorkCheckListGrp.do")
	public @ResponseBody ResultVO updateWorkCheckListGrp(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String UPD_USER_ID = userVO.getUser_id();
			paramMap.put("upd_user_id", UPD_USER_ID);
			
			workCheckListService.updateWorkCheckListGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 체크리스트 이력버전
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckList/searchVersion.do")
	public @ResponseBody ResultVO searchVersion(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			
			List resultList = workCheckListService.searchVersion(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
}
