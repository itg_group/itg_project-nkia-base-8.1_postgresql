/*
 * @(#)WorkCheckListDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckList.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workCheckListDAO")
public class WorkCheckListDAO extends EgovComAbstractDAO{

	public Map<String, Object> searchWorkCheckList(Map<String, Object> paramMap) throws Exception{
		return (Map<String, Object>)selectByPk("workCheckListDAO.searchWorkCheckList", paramMap);
	}
	
	public List<Map<String, Object>> searchWorkCheckListList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckListDAO.searchWorkCheckListList", paramMap);
	}
	
	public int searchWorkCheckListCount(Map<String, Object> paramMap)throws Exception {
		return (Integer)selectByPk("workCheckListDAO.searchWorkCheckListCount", paramMap);
	}
	
	public String selectModelNewId(Map<String, Object> param) throws Exception {
		return (String)selectByPk("workCheckListDAO.selectWorkCheckListNewId", param);
	}
	
	public String selectMaxVersion(Map<String, Object> param) throws Exception {
		return (String)selectByPk("workCheckListDAO.selectMaxVersion", param);
	}

	public void insertWorkCheckList(Map<String, Object> paramMap) throws Exception {
		this.insert("workCheckListDAO.insertWorkCheckList", paramMap);
	}
	
	public void updateWorkCheckList(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckListDAO.updateWorkCheckList", paramMap);
	}
	
	public void insertWorkCheckDtlList(Map<String, Object> paramMap) throws Exception {
		this.insert("workCheckListDAO.insertWorkCheckDtlList", paramMap);
	}
	
	public void updateWorkCheckDtlList(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckListDAO.updateWorkCheckDtlList", paramMap);
	}
	
	public void deleteWorkCheckList(Map<String, Object> paramMap) throws Exception {
		this.delete("workCheckListDAO.deleteWorkCheckList", paramMap);
	}
	
	public Map<String, Object> searchWorkSrCheckList(Map<String, Object> paramMap) throws Exception{
		return (Map<String, Object>)selectByPk("workCheckListDAO.searchWorkSrCheckList", paramMap);
	}
	
	public List<Map<String, Object>> searchWorkSrCheckListList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckListDAO.searchWorkSrCheckListList", paramMap);
	}
	
	public void insertWorkSrCheckList(Map<String, Object> paramMap) throws Exception {
		this.insert("workCheckListDAO.insertWorkSrCheckList", paramMap);
	}
	
	public void updateWorkSrCheckList(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckListDAO.updateWorkSrCheckList", paramMap);
	}

	public List<Map<String, Object>> selectWorkCheckGroupList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckListDAO.selectWorkCheckGroupList", paramMap);
	}
	
	public int selectWorkCheckGroupCount(Map<String, Object> paramMap) throws Exception{
		return (Integer)selectByPk("workCheckListDAO.selectWorkCheckGroupCount", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkReqDocChkgroupList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckListDAO.selectWorkReqDocChkgroupList", paramMap);
	}
	
	public void insertWorkReqDocChkgroup(Map<String, Object> paramMap) throws Exception {
		this.insert("workCheckListDAO.insertWorkReqDocChkgroup", paramMap);
		this.insert("workCheckListDAO.insertWorkReqDocChkListDefault", paramMap);
	}
	
	public void deleteWorkReqDocChkgroupByReqDocIdList(Map<String, Object> paramMap) throws Exception {
		this.delete("workCheckListDAO.deleteWorkReqDocChkgroupByReqDocIdList", paramMap);
		this.delete("workCheckListDAO.deleteWorkReqDocChkListByReqDocIdList", paramMap);
	}

	public HashMap selectWorkCheckListGrpInfo(Map paramMap) throws Exception {
		return (HashMap) selectByPk("workCheckListDAO.selectWorkCheckListGrpInfo", paramMap);
	}
	
	public String selectWorkCheckGroupNewId(Map<String, Object> param) throws Exception {
		return (String)selectByPk("workCheckListDAO.selectWorkCheckGroupNewId", param);
	}

	public void insertWorkCheckListGrp(Map paramMap) throws Exception {
		this.insert("workCheckListDAO.insertWorkCheckListGrp", paramMap);
	}

	public void deleteWorkCheckListGrp(Map paramMap) throws Exception {
		this.delete("workCheckListDAO.deleteWorkCheckListGrp", paramMap);
	}

	public void updateWorkCheckListGrp(Map paramMap) {
		this.update("workCheckListDAO.updateWorkCheckListGrp", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkReqDocChkList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckListDAO.selectWorkReqDocChkList", paramMap);
	}
	public void insertWorkReqDocChkList(Map<String, Object> paramMap) throws Exception {
		this.insert("workCheckListDAO.insertWorkReqDocChkList", paramMap);
	}
	public void deleteWorkReqDocChkListByReqDocIdListId(Map<String, Object> paramMap) throws Exception {
		this.insert("workCheckListDAO.deleteWorkReqDocChkListByReqDocIdListId", paramMap);
	}
	public List<Map<String, Object>> searchVersion(Map<String, Object> paramMap) throws Exception{
		return list("workCheckListDAO.searchVersion", paramMap);
	}
	
}

