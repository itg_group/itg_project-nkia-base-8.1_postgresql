/*
 * @(#)WorkCheckRequestController.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckRequest.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.excpetion.NbpmRuleException;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.user.service.UserService;
import com.nkia.itg.workflow.charger.constraint.ChargerGrpEnum;
import com.nkia.itg.workflow.workCheckRequest.service.WorkCheckRequestService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkCheckRequestController {
	
	@Resource(name = "workCheckRequestService")
	private WorkCheckRequestService workCheckRequestService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "userService")
	private UserService userService;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	/**
	 * 자가점검 일괄요청등록 관리
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/goWorkCheckRequest.do")
	public String goWorkCheckResult(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
						
		paramMap.put("cust_id", "037");
		paramMap.put("position", "팀장");
		
		List<HashMap> mngrInfo = userService.searchUserList(paramMap);
		
		if(mngrInfo != null && mngrInfo.size() > 0){
			paramMap.put("req_manager", mngrInfo.get(0).get("USER_ID"));
			paramMap.put("req_manager_nm", mngrInfo.get(0).get("USER_NM"));
		}
		String processType = StringUtil.parseString(request.getParameter("process_type"));		
		paramMap.put("req_type", processType);	
		
		String forwarPage = "/itg/workflow/workCheckRequest/workCheckRequest.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
			
	
	/**
	 * 자가점검 달력목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckRequest/selectWorkCheckRequestMonths.do")
	public @ResponseBody ResultVO selectWorkCheckResultMonths(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = workCheckRequestService.selectWorkCheckRequestMonths(paramMap);				
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		
	
	
	/**
	 * 자가점검 요청목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckRequest/selectWorkCheckRequestList.do")
	public @ResponseBody ResultVO selectWorkCheckRequestList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();			
			int totalCount = 0;
			
			totalCount = workCheckRequestService.selectWorkCheckRequestCnt(paramMap);
			resultList = workCheckRequestService.selectWorkCheckRequestList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
			return resultVO;
	}		
		
	
	/**
	 * 자가점검 일괄요청 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/selectWorkCheckRequestInfo.do")
	public @ResponseBody ResultVO selectWorkCheckAtchInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) workCheckRequestService.selectWorkCheckRequestInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
				
	
	/**
	 * 타겟부서 목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckRequest/selectWorkCheckTargetList.do")
	public @ResponseBody ResultVO selectWorkCheckTargetList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();			
			
			resultList = workCheckRequestService.selectWorkCheckTargetList(paramMap);		
			
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
			return resultVO;
	}		
	

	/**
	 * 자가점검 요청목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckRequest/selectWorkCheckRequestSEQ.do")
	public @ResponseBody ResultVO selectWorkCheckRequestSEQ(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		
		try{
			String request_seq = "";
			request_seq = workCheckRequestService.selectWorkCheckRequestSEQ(paramMap);
			resultVO.setResultString(request_seq);
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
			return resultVO;
	}	
	
	/**
	 * 자가점검 일괄요청 등록
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/insertWorkCheckRequestInfo.do")
	public @ResponseBody ResultVO insertWorkCheckRequestInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		try {	
			HashMap req_info = (HashMap) paramMap.get("req_info");
			ArrayList<HashMap> targetGrid = (ArrayList<HashMap>) paramMap.get("targetGrid");
			String request_seq = workCheckRequestService.selectWorkCheckRequestSEQ(paramMap);	//아이디 생성
			req_info.put("check_request_id", request_seq);
			
			workCheckRequestService.insertWorkCheckRequestInfo(req_info);						
						
			if(targetGrid.size() > 0){
				for(int i=0; i<targetGrid.size(); i++){
					HashMap tmpMap = targetGrid.get(i);
					tmpMap.put("check_request_id", request_seq);					
					tmpMap.put("cust_id", tmpMap.get("CUST_ID"));
					tmpMap.put("target_user_id", tmpMap.get("TARGET_USER_ID"));
					tmpMap.put("appr_user_id", tmpMap.get("APPR_USER_ID"));
					tmpMap.put("ins_user_id", userVO.getUser_id());
					
					workCheckRequestService.insertWorkCheckRequestTargetInfo(tmpMap);		
				}
			}
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}			
	
	/**
	 * 자가점검 일괄요청 부서등록(미사용)
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/insertWorkCheckRequestTargetInfo.do")
	public @ResponseBody ResultVO insertWorkCheckRequestTargetInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		try {			
			HashMap req_info = (HashMap) paramMap.get("req_info");
			ArrayList<HashMap> targetGrid = (ArrayList<HashMap>) paramMap.get("targetGrid");
			
			workCheckRequestService.insertWorkCheckRequestTargetInfo(req_info);						
						
			if(targetGrid.size() > 0){
				for(int i=0; i<targetGrid.size(); i++){
					HashMap tmpMap = targetGrid.get(i);
//					tmpMap.put("check_request_target_id", StringUtil.parseString(tmpMap.get("CHECK_REQUEST_TARGET_ID")));					
					tmpMap.put("cust_id", tmpMap.get("CUST_ID"));
					tmpMap.put("target_user_id", tmpMap.get("TARGET_USER_ID"));
					tmpMap.put("appr_user_id", tmpMap.get("APPR_USER_ID"));
					tmpMap.put("ins_user_id", userVO.getUser_id());
					
					workCheckRequestService.insertWorkCheckRequestTargetInfo(tmpMap);		
				}
			}
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}			
	
	
	/**
	 * 자가점검 일괄요청 수정
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/updateWorkCheckRequestInfo.do")
	public @ResponseBody ResultVO updateWorkCheckRequestInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		try {			
			HashMap req_info = (HashMap) paramMap.get("req_info");
			ArrayList<HashMap> targetGrid = (ArrayList<HashMap>) paramMap.get("targetGrid");
			
			workCheckRequestService.updateWorkCheckRequestInfo(req_info);
				
			if(targetGrid.size() > 0){
				workCheckRequestService.deleteWorkCheckRequestTargetInfo(req_info);
				for(int i=0; i<targetGrid.size(); i++){
					HashMap tmpMap = targetGrid.get(i);
					
					if(!"".equals(StringUtil.parseString(tmpMap.get("CHECK_REQUEST_ID")))){
						//이미 등록된 데이터
						tmpMap.put("check_request_target_id", StringUtil.parseString(tmpMap.get("CHECK_REQUEST_TARGET_ID")));	
						tmpMap.put("check_request_id", StringUtil.parseString(tmpMap.get("CHECK_REQUEST_ID")));	
						tmpMap.put("cust_id", tmpMap.get("CUST_ID"));
						tmpMap.put("target_user_id", tmpMap.get("TARGET_USER_ID"));
						tmpMap.put("appr_user_id", tmpMap.get("APPR_USER_ID"));
						tmpMap.put("ins_user_id", userVO.getUser_id());
						
						workCheckRequestService.insertWorkCheckRequestTargetInfo(tmpMap);	
					} else {
						//추가된 경우
						tmpMap.put("check_request_id", StringUtil.parseString(req_info.get("check_request_id")));					
						tmpMap.put("cust_id", tmpMap.get("CUST_ID"));
						tmpMap.put("target_user_id", tmpMap.get("TARGET_USER_ID"));
						tmpMap.put("appr_user_id", tmpMap.get("APPR_USER_ID"));
						tmpMap.put("ins_user_id", userVO.getUser_id());
						
						workCheckRequestService.insertWorkCheckRequestTargetInfo(tmpMap);	
					}
						
				}
			}
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	
	/**
	 * 자가점검 일괄요청 부서수정(미사용)
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/updateWorkCheckRequestTargetInfo.do")
	public @ResponseBody ResultVO updateWorkCheckRequestTargetInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();

		try {			
			
			workCheckRequestService.updateWorkCheckRequestTargetInfo(paramMap);
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	

	/**
	 * 자가점검 일괄요청 삭제
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/deleteWorkCheckRequestInfo.do")
	public @ResponseBody ResultVO deleteWorkCheckRequestInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();

		try {			
			
			workCheckRequestService.deleteWorkCheckRequestInfo(paramMap);						
			workCheckRequestService.deleteWorkCheckRequestTargetList(paramMap);
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	
	/**
	 * 자가점검 타겟부서 삭제
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/deleteWorkCheckRequestTargetInfo.do")
	public @ResponseBody ResultVO deleteWorkCheckRequestTargetInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();

		try {			
						
			workCheckRequestService.deleteWorkCheckRequestTargetInfo(paramMap);
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	/**
	 * 타겟그룹 목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckRequest/selectWorkCheckTargetGroupList.do")
	public @ResponseBody ResultVO selectWorkCheckTargetGroupList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();			
			
			resultList = workCheckRequestService.selectWorkCheckTargetGroupList(paramMap);		
			
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}		
			return resultVO;
	}			
	
	/**
	 * 담당자 그룹목록 트리 호출  
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/searchAllChargerGrpTree.do")
	public @ResponseBody ResultVO searchAllChargerGrpTree(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			String grpUserId = String.valueOf(paramMap.get("grp_user_id"));
			String operType = String.valueOf(paramMap.get("oper_type"));
/*			
			//개인화 메뉴인지 관리자용 메뉴인지 확인
			String adminYn = NkiaApplicationPropertiesMap.getProperty("Globals.ChargerGrp.AdminYN");
			
			//그룹관리 등록 관리자ID
			String adminId = NkiaApplicationPropertiesMap.getProperty("Globals.ChargerGrp.AdminID");
			
			//개인화 메뉴인 경우
			if(adminYn != null && "N".equals(adminYn)){
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				String loginUserId = userVO.getUser_id();
				paramMap.put("grp_user_id", adminId);
				paramMap.put("personal_user_id", loginUserId);
				
			}else{
				paramMap.put("grp_user_id", adminId);
			
			}
*/
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String GRP_USER_ID = userVO.getUser_id();
			
			// [담당자 관리]에서 호출하는 것인지, 담당자 검색을 통해 호출하는 것인지 판별
			if(!"null".equals(grpUserId)){
				// [담당자 관리]에서 호출하는 경우, 개인화 혹은 서비스 데스크용으로 나뉨
				paramMap.put("grp_user_id", grpUserId);				
				
			}else{
			    // 담당자 검색을 통해 호출하는 경우, 해당 테스크의 롤에 따라 개인화 혹은 서비스 데스크용으로 나뉨
				if(operType.equals(ChargerGrpEnum.SERVICEDESK_ROLE.getMessage())){
					grpUserId = ChargerGrpEnum.SERVICEDESK.getMessage();
					
				}else{
					grpUserId = GRP_USER_ID;
					
				}
				paramMap.put("grp_user_id", grpUserId);				
			}
			
			List resultList = workCheckRequestService.searchAllChargerGrpTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자가점검, 개선과제 일괄 요청 등록  
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckRequest/insertBatchRequstRegist.do")
	public @ResponseBody ResultVO insertBatchRequstRegist(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		try {
			HashMap reqInfo = (HashMap) paramMap.get("reqInfo");	//일괄요청등록에서 가져온 form의 값
			String reqType = (String) paramMap.get("reqType");
			String queryKey = "insertPROC_" + reqType;
			String nextOperId = (String) paramMap.get("nextOperId");			
			List registList = new ArrayList();	//점검결과를 등록할 각 부서 담당자의 배열 변수.
			
//			String curDate = DateUtil.getDateFormat("yyyy-MM-ddHHmm");
			
			/*
			 *  role_opetr_04 : 부서담당자
			 *  role_opetr_05 : 부서책임자
			 *  role_opetr_06 : 부서관리자
			 *  role_opetr_07 : 정보보호팀>보안담당자
			 *  role_opetr_08 : 정보보호팀>보안관리자
			 * 
			 * */
			if("WORK_CHK".equals(reqType) || "IMPRO_ASSIGN".equals(reqType)){
				//WORK_CHK : 자가점검 일괄요청 등록
				//IMPRO_ASSIGN : 개선과제 일괄요청 등록
				
				//요청부서 목록의 수만큼 반복
				if(paramMap.get("target_dept_grid") != null ){
					registList = (List) paramMap.get("target_dept_grid");	
					
					for(int i=0;i<registList.size();i++) {
						Map tempMap = (Map) registList.get(i);
						
				   		String srId = processCommonExecutor.createSrId();
				   		NbpmProcessVO processInfo = processCommonExecutor.selectFinalVersionProcessInfo(reqType);
						
						NbpmProcessVO processVO = new NbpmProcessVO();
						Map param = new HashMap();
						processVO.setSr_id(srId);
						param.put("sr_id", srId);															
						
						//역할자 list 정의
						List nbpmOperList = new ArrayList();
							
						//다음 담당자 ROLE 지정
						//요청받을 그룹 목록
						
						//점검담당자  & 점검 관리자
						//target_dept_grid -> 요청부서 목록
						if(paramMap.get("target_dept_grid") != null ){
							//점검 담당자 정보
							if(StringUtil.parseString(tempMap.get("target_user_id")) != null && StringUtil.parseString(tempMap.get("target_user_id")) != ""){
								LinkedHashMap grpOperInfo = new LinkedHashMap();							
								grpOperInfo.put("select_type", "SINGLE");		
								grpOperInfo.put("oper_type", nextOperId);						
								grpOperInfo.put("oper_user_id", tempMap.get("target_user_id"));
								grpOperInfo.put("grp_slct_yn", "N");
								param.put("grpOperInfo", grpOperInfo);			
								
								nbpmOperList.add(grpOperInfo);
							}
							
							
							//점검 관리자 정보
							if(StringUtil.parseString(tempMap.get("appr_user_id")) != null && StringUtil.parseString(tempMap.get("appr_user_id")) != ""){
								LinkedHashMap grpOperInfo = new LinkedHashMap();							
								grpOperInfo.put("select_type", "SINGLE");		
								grpOperInfo.put("oper_type", "ROLE_OPETR_06");																				
								grpOperInfo.put("oper_user_id", tempMap.get("appr_user_id"));
								grpOperInfo.put("grp_slct_yn", "N");
								param.put("grpOperInfo", grpOperInfo);			
								
								nbpmOperList.add(grpOperInfo);
							}							

						}
						
						//보안관리자
						if(paramMap.get("role_opetr_08_id") != null && !"".equals(StringUtil.parseString(paramMap.get("role_opetr_08_id")))){
							LinkedHashMap grpOperInfo = new LinkedHashMap();	
							
							grpOperInfo.put("select_type", "SINGLE");		
							grpOperInfo.put("oper_type", "ROLE_OPETR_08");	
							grpOperInfo.put("oper_user_id", StringUtil.parseString(paramMap.get("role_opetr_08_id")));
							grpOperInfo.put("grp_slct_yn", "N");
							
							nbpmOperList.add(grpOperInfo);
							
						}
						
						//등록자 정보					
						processVO.setReq_user_id(userVO.getUser_id());						
						processVO.setOperList(nbpmOperList);
						
						String procVersion = processInfo.getVersion();
						processVO.setProcessId(processInfo.getProcessId());
						processVO.setProcess_name(processInfo.getProcess_name());
						processVO.setProcess_type(processInfo.getProcess_type());						
						processVO.setTask_name("start");
						processVO.setVersion(procVersion);
						processVO.setWorkType("NRMLT_PROC");
						processVO.setReq_user_id(processVO.getReq_user_id());
						processVO.setWorker_user_id(processVO.getReq_user_id());
						
						//요청내용에 들어갈 데이터 세팅				
						param.put("queryKey", queryKey);
						param.put("title",  reqInfo.get("title"));
						param.put("req_type", reqType);
						param.put("content", reqInfo.get("contents"));
						param.put("ins_user_id", userVO.getUser_id());	
						param.put("req_user_id", processVO.getReq_user_id());
						
						param.put("check_request_id", reqInfo.get("check_request_id"));	//일괄요청정보 ID(proc_work_chk 또는 proc_impro_assign에 저장)
						param.put("target_cust_id", tempMap.get("target_cust_id")); //요청부서 ID(proc_work_chk 또는 proc_impro_assign에 저장)
						
						String req_dt = StringUtil.parseString(reqInfo.get("req_dt"));
						param.put("req_dt", req_dt);
						param.put("nbpm_processId", processVO.getProcessId());
						//완료요청일(요청일 + 7일)
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						Date req_dt_date = dateFormat.parse(req_dt);
					
						Calendar cal = Calendar.getInstance();
						cal.setTime(req_dt_date);
						cal.add(Calendar.DATE, 7);
						
						String hope_dt = dateFormat.format(cal.getTime());		
						
						param.put("hope_dt", hope_dt);						
						param.put("proc_ver", procVersion);
						
						//화면에서 넘어온 담당자 정보 중 없는 BPM User 테이블에 데이터가 있는지 체크하여 등록한다.
						processCommonExecutor.registerUser(processVO, param);
						//요청 처리를 한다.
						Map result = processCommonExecutor.executeTask(processVO, param);
						//새로 생성된 타스크들의 알림을 실행한다.
						processCommonExecutor.notifyTask(processVO, result);
					}
				}
			}
			
			
//			//해당 일괄요청 건의 SR생성여부를 Y로 업데이트
//			reqInfo.put("upd_user_id", userVO.getUser_id());
//			
//			reqInfo.put("srreq_yn", "Y");
//			workCheckRequestService.updateWorkCheckRequestInfo(reqInfo);
			
			resultVO.setSuccess(true);
		} catch (NbpmRuleException e) {
			throw new NbpmRuleException(e);
		}catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
