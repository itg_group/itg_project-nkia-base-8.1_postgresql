/*
 * @(#)workCheckRequestServiceImpl.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckRequest.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckRequest.dao.WorkCheckRequestDAO;
import com.nkia.itg.workflow.workCheckRequest.service.WorkCheckRequestService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("workCheckRequestService")
public class WorkCheckRequestServiceImpl implements WorkCheckRequestService{
	
	private static final Logger logger = LoggerFactory.getLogger(WorkCheckRequestServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "workCheckRequestDAO")
	public WorkCheckRequestDAO workCheckRequestDAO;
	
	
	public List<Map<String, Object>> selectWorkCheckRequestMonths(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		}	
		return workCheckRequestDAO.selectWorkCheckRequestMonths(paramMap);
	}		
	
	public int selectWorkCheckRequestCnt(ModelMap paramMap) throws Exception {
		return workCheckRequestDAO.selectWorkCheckRequestCnt(paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckRequestList(Map<String, Object> paramMap) throws Exception {
		return workCheckRequestDAO.selectWorkCheckRequestList(paramMap);
	}			
	
	public Map selectWorkCheckRequestInfo(ModelMap paramMap) throws Exception {				
		Map atchInfo = workCheckRequestDAO.selectWorkCheckRequestInfo(paramMap);		
		return atchInfo;
	}	
	
	public List<Map<String, Object>> selectWorkCheckTargetList(Map<String, Object> paramMap) throws Exception {
		return workCheckRequestDAO.selectWorkCheckTargetList(paramMap);
	}		
	
	public String selectWorkCheckRequestSEQ(ModelMap paramMap) throws Exception {
		return workCheckRequestDAO.selectWorkCheckRequestSEQ(paramMap);
	}	
	
	public void insertWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}				
		workCheckRequestDAO.insertWorkCheckRequestInfo(paramMap);
	}	
	
	public void insertWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}				
		workCheckRequestDAO.insertWorkCheckRequestTargetInfo(paramMap);
	}	

	public void updateWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}				
		workCheckRequestDAO.updateWorkCheckRequestInfo(paramMap);
	}		
	
	public void updateWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}				
		workCheckRequestDAO.updateWorkCheckRequestTargetInfo(paramMap);
	}			
	
	public void deleteWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}				
		workCheckRequestDAO.deleteWorkCheckRequestInfo(paramMap);
	}		
	
	public void deleteWorkCheckRequestTargetList(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}				
		workCheckRequestDAO.deleteWorkCheckRequestTargetList(paramMap);
	}			
	
	public void deleteWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}				
		workCheckRequestDAO.deleteWorkCheckRequestTargetInfo(paramMap);
	}		
	
	public List<Map<String, Object>> selectWorkCheckTargetGroupList(Map<String, Object> paramMap) throws Exception {
		return workCheckRequestDAO.selectWorkCheckTargetGroupList(paramMap);
	}		
	
	@Override
	public List searchAllChargerGrpTree(Map paramMap) throws Exception {
		return workCheckRequestDAO.searchAllChargerGrpTree(paramMap);
	}

	@Override
	public void updateWorkCheckRequestSrreqYn(Map<String, Object> paramMap)
			throws Exception {
		// TODO Auto-generated method stub
		workCheckRequestDAO.updateWorkCheckRequestSrreqYn(paramMap);
	}
	

}