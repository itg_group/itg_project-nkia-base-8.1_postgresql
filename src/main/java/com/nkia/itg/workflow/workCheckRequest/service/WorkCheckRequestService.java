/*
 * @(#)workCheckListRequestService.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckRequest.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

public interface WorkCheckRequestService {

	public List<Map<String, Object>> selectWorkCheckRequestMonths(Map<String, Object> paramMap) throws Exception;
	
	public int selectWorkCheckRequestCnt(ModelMap paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckRequestList(Map<String, Object> paramMap) throws Exception;	
				
	public Map selectWorkCheckRequestInfo(ModelMap paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckTargetList(Map<String, Object> paramMap) throws Exception;
	
	public String selectWorkCheckRequestSEQ(ModelMap paramMap) throws Exception;

	public void insertWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception;
	
	public void insertWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception;	
	
	public void updateWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception;
	
	public void deleteWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception;
		
	public void deleteWorkCheckRequestTargetList(Map<String, Object> paramMap) throws Exception;
	
	public void deleteWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckTargetGroupList(Map<String, Object> paramMap) throws Exception;
	
	public List searchAllChargerGrpTree(Map paramMap) throws Exception;
	
	public void updateWorkCheckRequestSrreqYn(Map<String, Object> paramMap) throws Exception;
}
