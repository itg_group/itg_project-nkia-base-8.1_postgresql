/*
 * @(#)WorkCheckRequestDAO.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckRequest.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workCheckRequestDAO")
public class WorkCheckRequestDAO extends EgovComAbstractDAO{

	
	public List<Map<String, Object>> selectWorkCheckRequestMonths(Map<String, Object> paramMap) throws Exception{
		return list("workCheckRequestDAO.selectWorkCheckRequestMonths", paramMap);
	}
	
	public int selectWorkCheckRequestCnt(ModelMap paramMap) throws Exception {
		return (Integer)selectByPk("workCheckRequestDAO.selectWorkCheckRequestCnt", paramMap);
	}	
	
	public List<Map<String, Object>> selectWorkCheckRequestList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckRequestDAO.selectWorkCheckRequestList", paramMap);
	}
	
	public Map selectWorkCheckRequestInfo(ModelMap paramMap) throws Exception {
		return (Map) this.selectByPk("workCheckRequestDAO.selectWorkCheckRequestInfo", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckTargetList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckRequestDAO.selectWorkCheckTargetList", paramMap);
	}	

	public String selectWorkCheckRequestSEQ(ModelMap paramMap) throws Exception {
		return (String)selectByPk("workCheckRequestDAO.selectWorkCheckRequestSEQ", paramMap);
	}	
	
	public void insertWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckRequestDAO.insertWorkCheckRequestInfo", paramMap);
	}	
	
	public void insertWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckRequestDAO.insertWorkCheckRequestTargetInfo", paramMap);
	}		
	
	public void updateWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckRequestDAO.updateWorkCheckRequestInfo", paramMap);
	}
	
	public void updateWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckRequestDAO.updateWorkCheckRequestTargetInfo", paramMap);
	}	
	
	public void deleteWorkCheckRequestInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckRequestDAO.deleteWorkCheckRequestInfo", paramMap);
	}
	
	public void deleteWorkCheckRequestTargetList(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckRequestDAO.deleteWorkCheckRequestTargetList", paramMap);
	}	
	
	public void deleteWorkCheckRequestTargetInfo(Map<String, Object> paramMap) throws Exception {
		this.delete("workCheckRequestDAO.deleteWorkCheckRequestTargetInfo", paramMap);
	}		
	
	public List<Map<String, Object>> selectWorkCheckTargetGroupList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckRequestDAO.selectWorkCheckTargetGroupList", paramMap);
	}	
	
	public List searchAllChargerGrpTree(Map paramMap) {
		return this.list("workCheckRequestDAO.searchAllChargerGrpTree", paramMap);
	}
	
	public void updateWorkCheckRequestSrreqYn(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckRequestDAO.updateWorkCheckRequestSrreqYn", paramMap);
	}	
	
}

