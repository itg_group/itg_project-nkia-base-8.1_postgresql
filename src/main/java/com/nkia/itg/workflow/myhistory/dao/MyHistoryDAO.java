/*
 * @(#)MyListDAO.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.myhistory.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("myHistoryDAO")
public class MyHistoryDAO extends EgovComAbstractDAO {

	public List searchMyHistoryList(Map paramMap) {
		return list("MyHistoryDAO.searchMyHistoryList", paramMap);
	}
	
	public Integer searchMyHistoryListCount(Map paramMap) {
		return (Integer) selectByPk("MyHistoryDAO.searchMyHistoryListCount", paramMap);
	}
	
}
