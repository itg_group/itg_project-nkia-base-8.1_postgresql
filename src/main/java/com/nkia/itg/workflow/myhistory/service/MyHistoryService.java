/*
 * @(#)MyListService.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.myhistory.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface MyHistoryService {
	
	public int searchMyHistoryListCount(ModelMap paramMap);

	public List searchMyHistoryList(ModelMap paramMap);
	
	public void deleteTempSrData(Map paramMap) throws NkiaException;
}
