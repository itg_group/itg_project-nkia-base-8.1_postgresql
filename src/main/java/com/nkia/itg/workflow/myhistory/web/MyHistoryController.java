/*
 * @(#)MyListController.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.myhistory.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.myhistory.service.MyHistoryService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class MyHistoryController {
	
	@Resource(name="myHistoryService")
	private MyHistoryService myHistoryService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/workflow/myhistory/goMyHistoryTab.do")
	public String goMyHistoryTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/workflow/myhistory/myHistoryTab.nvf";
		if(null != request.getParameter("portal_param")){
			paramMap.put("portal_param", request.getParameter("portal_param"));
		}
		
		String req_statis_chk = (String) request.getParameter("req_statis_chk"); 
		if (req_statis_chk != null && !"".equals(req_statis_chk) ) {
			paramMap.put("req_statis_chk", "Y");
		}
		
		String proc_state_search = (String) request.getParameter("proc_state_search");
		if (proc_state_search != null && !"".equals(proc_state_search)) {
			paramMap.put("proc_state_search", proc_state_search);
		}
		
		String search_user_type = (String) request.getParameter("search_user_type");
		if (search_user_type != null && !"".equals(search_user_type)) {
			paramMap.put("search_user_type", search_user_type);
		}
		
		String main_search_yn = (String) request.getParameter("main_search_yn");
		if (main_search_yn != null && !"".equals(main_search_yn) ) {
			paramMap.put("main_search_yn", main_search_yn);
		} else {
			paramMap.put("main_search_yn", "N");
		}
		
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/workflow/myhistory/goMyHistoryListMain.do")
	public String goMyHistoryListMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException, ParseException {
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String login_user_nm = userVO.getUser_nm();
		
		String forwarPage = "/itg/workflow/myhistory/myHistoryMain.nvf";
		
		if(null != request.getParameter("portal_param")){
			paramMap.put("portal_param", request.getParameter("portal_param"));
		}
		
		String req_statis_chk = (String) request.getParameter("req_statis_chk"); 
		if (req_statis_chk != null && !"".equals(req_statis_chk) ) {
			paramMap.put("req_statis_chk", req_statis_chk);
		} else {
			paramMap.put("req_statis_chk", "N");
		}
		
		String proc_state_search = (String) request.getParameter("proc_state_search");
		if (proc_state_search != null && !"".equals(proc_state_search)) {
			paramMap.put("proc_state_search", proc_state_search);
		}
		
		String search_user_type = (String) request.getParameter("search_user_type");
		if (search_user_type != null && !"".equals(search_user_type)) {
			paramMap.put("search_user_type", search_user_type);
		}
		
		//이번달 종료 상태인 요청건 조회
		String main_search_yn = (String) request.getParameter("main_search_yn");
		if (main_search_yn != null && !"".equals(main_search_yn) ) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar currentCalendar = Calendar.getInstance();
			//이번달 첫날
			int year = currentCalendar.get(Calendar.YEAR);
			int month = currentCalendar.get(Calendar.MONTH) + 1;
			String date = "01";
			
			String startDay = Integer.toString(year) + "-" +
	                 ((month<10) ? "0" + Integer.toString(month) : Integer.toString(month)) + "-" +
	                 date;
			Date convertSDay = sdf.parse(startDay);
			startDay = sdf.format(convertSDay);
			
			//이번달 마지막날 
	        int endDay = currentCalendar.getActualMaximum(Calendar.DAY_OF_MONTH );
	        
	        String lastDay = Integer.toString(year) + "-" +
	                 ((month<10) ? "0" + Integer.toString(month) : Integer.toString(month)) + "-" +
	                 Integer.toString(endDay);
			Date convertEDay = sdf.parse(lastDay);
			lastDay = sdf.format(convertEDay);

	        paramMap.put("end_dt_startDate", startDay);
	        paramMap.put("end_dt_endDate", lastDay);
			paramMap.put("main_search_yn", main_search_yn);
		} else {
			paramMap.put("main_search_yn", "N");
		}
		
		return forwarPage;
	}
	
		
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/myhistory/searchMyHistoryList.do")
	public @ResponseBody ResultVO searchMyHistoryList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			
			totalCount = myHistoryService.searchMyHistoryListCount(paramMap);
			List resultList = myHistoryService.searchMyHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 임시저장 상태의 테스크 삭제
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/myhistory/deleteTempSrData.do")
	public @ResponseBody ResultVO deleteTempSrData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			//임시저장 상태의 태스크 삭제
			myHistoryService.deleteTempSrData(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
