/*
 * @(#)MyListServiceImpl.java              2013. 5. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.myhistory.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.workflow.myhistory.dao.MyHistoryDAO;
import com.nkia.itg.workflow.myhistory.service.MyHistoryService;

@Service("myHistoryService")
public class MyHistoryServiceImpl implements MyHistoryService {

	@Resource(name = "myHistoryDAO")
	private MyHistoryDAO myHistoryDAO;
	
	@Resource(name = "nbpmCommonDAO")
	private NbpmCommonDAO nbpmCommonDAO;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;

	@Override
	public int searchMyHistoryListCount(ModelMap paramMap) {
		// TODO Auto-generated method stub
		return myHistoryDAO.searchMyHistoryListCount(paramMap);
	}

	@Override
	public List searchMyHistoryList(ModelMap paramMap) {
		// TODO Auto-generated method stub
		return myHistoryDAO.searchMyHistoryList(paramMap);
	}
	
	@Override
	public void deleteTempSrData(Map paramMap) throws NkiaException {
		try {
			List tasks = (List)paramMap.get("tasks");
			
			for(int i = 0 ; i < tasks.size() ; i++) {
				Map task = (Map)tasks.get(i);
				Map processInfoMap = new HashMap();
				
				String srId =  String.valueOf(task.get("sr_id"));
				processInfoMap.put("sr_id", srId);
				processInfoMap.put("process_type", String.valueOf(task.get("req_type")));
				
				List tableNmList = nbpmCommonDAO.searchProcRelTable(processInfoMap);
				for(int j = 0 ; j < tableNmList.size() ; j++) {
					Map tableNmMap = (Map)tableNmList.get(j);
					processInfoMap.put("tableNm", tableNmMap.get("TABLE_NAME"));
					postWorkDAO.deleteProcTableData(processInfoMap);
				}
				postWorkDAO.deleteProcessOrerator(processInfoMap);
				postWorkDAO.deleteProcessItem(srId);
				postWorkDAO.deleteProcMaster(processInfoMap);
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}

}
