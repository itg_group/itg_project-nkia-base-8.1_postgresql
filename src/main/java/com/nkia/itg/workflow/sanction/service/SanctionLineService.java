package com.nkia.itg.workflow.sanction.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SanctionLineService {

	public List searchAllSanctionLineTree(Map paramMap) throws NkiaException;

	public HashMap selectSanctionLineProcInfo(Map paramMap) throws NkiaException;

	public HashMap selectSanctionLineInfo(Map paramMap) throws NkiaException;

	public int searchSanctionLineProcAuthListCount(Map paramMap) throws NkiaException;

	public List searchSanctionLineProcAuthList(Map paramMap) throws NkiaException;

	public void insertSanctionLine(Map paramMap) throws NkiaException;

	public void deleteSanctionLine(Map paramMap) throws NkiaException;

	public void updateSanctionLine(Map paramMap) throws NkiaException;

	public void deleteSanctionAuth(Map paramMap) throws NkiaException;

	public void insertSanctionLineAuth(Map paramMap) throws NkiaException;
}
