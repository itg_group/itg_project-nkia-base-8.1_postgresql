package com.nkia.itg.workflow.sanction.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("sanctionLineDAO")
public class SanctionLineDAO extends EgovComAbstractDAO{
	
	public List searchAllSanctionLineTree(Map paramMap) throws NkiaException{
		return this.list("SanctionLineDAO.searchAllSanctionLineTree", paramMap);
	}

	public HashMap selectSanctionLineProcInfo(Map paramMap) {
		return (HashMap) selectByPk("SanctionLineDAO.selectSanctionLineProcInfo", paramMap);
	}

	public HashMap selectSanctionLineInfo(Map paramMap) {
		return (HashMap) selectByPk("SanctionLineDAO.selectSanctionLineInfo", paramMap);
	}

	public String selectLineId() {
		return (String) selectByPk("SanctionLineDAO.selectLineId", null);
	}

	public void insertSanctionLine(Map paramMap) {
		insert("SanctionLineDAO.insertSanctionLine", paramMap);
	}

	public void deleteSanctionLine(Map paramMap) {
		delete("SanctionLineDAO.deleteSanctionLine", paramMap);
	}

	public void deleteSanctionLineProcAuth(Map paramMap) {
		delete("SanctionLineDAO.deleteSanctionLineProcAuth", paramMap);
	}

	public void updateSanctionLine(Map paramMap) {
		update("SanctionLineDAO.updateSanctionLine", paramMap);
	}

	public void updateUseYn(Map paramMap) {
		update("SanctionLineDAO.updateUseYn", paramMap);
	}

	public int searchSanctionLineProcAuthListCount(Map paramMap) {
		return (Integer) this.selectByPk("SanctionLineDAO.searchSanctionLineProcAuthListCount", paramMap);
	}

	public List searchSanctionLineProcAuthList(Map paramMap) {
		return this.list("SanctionLineDAO.searchSanctionLineProcAuthList", paramMap);
	}

	public void insertSanctionLineAuth(Map paramMap) {
		insert("SanctionLineDAO.insertSanctionLineAuth", paramMap);
	}
}
