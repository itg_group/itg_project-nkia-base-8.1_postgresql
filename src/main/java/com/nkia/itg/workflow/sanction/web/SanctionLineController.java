package com.nkia.itg.workflow.sanction.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.sanction.service.SanctionLineService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
@Controller
public class SanctionLineController {

		@Resource(name = "sanctionLineService")
		private SanctionLineService sanctionLineService;

		@Resource(name = "egovMessageSource")
		private EgovMessageSource messageSource;
	   
		@Resource(name = "urlAccessCheckService")
		private UrlAccessCheckService urlAccessCheckService;
		
		
	   /**
		 * 결재라인 관리 페이지 호출
	     * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/goSanctionLineManager.do")
		public String goUserSelectPopMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
			//2018.08.09 메뉴접근검증 추가
			if (!urlAccessCheckService.urlAuthCheck(request)) {
				return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
			}
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String USER_ID = userVO.getUser_id();
			paramMap.put("user_id", USER_ID);
			
			String forwarPage = "/itg/workflow/sanction/sanctionLineManager.nvf";
			return forwarPage;
		}
		
		/**
		 * 결재라인 트리 호출  
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/searchAllSanctionLineTree.do")
		public @ResponseBody ResultVO searchAllSanctionLineTree(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try{
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				final String USER_ID = userVO.getUser_id();
				paramMap.put("user_id", USER_ID);
				
				List resultList = sanctionLineService.searchAllSanctionLineTree(paramMap);
				String up_node_id = (String)paramMap.get("up_node_id");
				int expand_level = (Integer)paramMap.get("expandLevel");
				TreeVO treeVO = JsonUtil.changeTreeMapToJson(resultList, up_node_id, true, expand_level);
				resultVO.setTreeVO(treeVO);
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 결재라인 정보 호출(요청)
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/selectSanctionLineProcInfo.do")
		public @ResponseBody ResultVO selectSanctionLineProcInfo(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			HashMap resultMap = new HashMap();
			
			try {
				resultMap = sanctionLineService.selectSanctionLineProcInfo(paramMap);
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 결재라인 정보 호출
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/selectSanctionLineInfo.do")
		public @ResponseBody ResultVO selectSanctionLineInfo(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			HashMap resultMap = new HashMap();
			
			try {
				resultMap = sanctionLineService.selectSanctionLineInfo(paramMap);
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		/**
		 * 결재라인 등록
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/insertSanctionLine.do")
		public @ResponseBody ResultVO insertSanctionLine(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try {
				sanctionLineService.insertSanctionLine(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			} catch (Exception e) {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
				throw new NkiaException(e);
			}
			return resultVO;
		}
			
		/**
		 * 결재라인 삭제
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/deleteSanctionLine.do")
		public @ResponseBody ResultVO deleteSanctionLine(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			HashMap resultMap = new HashMap();
			
			try {
				sanctionLineService.deleteSanctionLine(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			} catch (Exception e) {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 결재라인 수정
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/updateSanctionLine.do")
		public @ResponseBody ResultVO updateSanctionLine(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			
			try {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				final String UPD_USER_ID = userVO.getUser_id();
				paramMap.put("upd_user_id", UPD_USER_ID);
				
				sanctionLineService.updateSanctionLine(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			} catch (Exception e) {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 결재라인 요청 권한자 그리드 호출  
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/searchSanctionLineProcAuth.do")  
		public @ResponseBody ResultVO searchSanctionLineProcAuth(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try{
				GridVO gridVO = new GridVO();
				int totalCount = 0;
				PagingUtil.getFristEndNum(paramMap);
			
				totalCount = sanctionLineService.searchSanctionLineProcAuthListCount(paramMap);
				List resultList = sanctionLineService.searchSanctionLineProcAuthList(paramMap);
				
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
				gridVO.setRows(resultList);
				resultVO.setGridVO(gridVO);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 결재라인 결재자 삭제
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/deleteSanctionAuth.do")
		public @ResponseBody ResultVO deleteSanctionAuth(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			HashMap resultMap = new HashMap();
			
			try {
				sanctionLineService.deleteSanctionAuth(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			} catch (Exception e) {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 결재라인 결재자 등록
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/workflow/sanction/insertSanctionLineAuth.do")
		public @ResponseBody ResultVO insertSanctionLineAuth(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try {
				sanctionLineService.insertSanctionLineAuth(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			} catch (Exception e) {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
				throw new NkiaException(e);
			}
			return resultVO;
		}
}
