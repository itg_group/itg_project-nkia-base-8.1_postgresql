package com.nkia.itg.workflow.sanction.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.workflow.charger.service.impl.ChargerGrpServiceImpl;
import com.nkia.itg.workflow.sanction.dao.SanctionLineDAO;
import com.nkia.itg.workflow.sanction.service.SanctionLineService;

@Service("sanctionLineService")
public class SanctionLineServiceImpl implements SanctionLineService{
	
	private static final Logger logger = LoggerFactory.getLogger(ChargerGrpServiceImpl.class);
	
	@Resource(name = "sanctionLineDAO")
	public SanctionLineDAO sanctionLineDAO;

	@Override
	public List searchAllSanctionLineTree(Map paramMap) throws NkiaException {
		return sanctionLineDAO.searchAllSanctionLineTree(paramMap);
	}

	@Override
	public HashMap selectSanctionLineProcInfo(Map paramMap) throws NkiaException {
		return sanctionLineDAO.selectSanctionLineProcInfo(paramMap);
	}

	@Override
	public HashMap selectSanctionLineInfo(Map paramMap) throws NkiaException {
		return sanctionLineDAO.selectSanctionLineInfo(paramMap);
	}


	@Override
	public void insertSanctionLine(Map paramMap) throws NkiaException {
		String line_id = sanctionLineDAO.selectLineId();
		
		paramMap.put("line_id", line_id);
		sanctionLineDAO.insertSanctionLine(paramMap);
	}

	@Override
	public void deleteSanctionLine(Map paramMap) throws NkiaException {
		sanctionLineDAO.deleteSanctionLine(paramMap);
		sanctionLineDAO.deleteSanctionLineProcAuth(paramMap);
	}

	@Override
	public void updateSanctionLine(Map paramMap) throws NkiaException {
		sanctionLineDAO.updateSanctionLine(paramMap);
		
		if((paramMap.get("leafFalg")).equals("N")){
			sanctionLineDAO.updateUseYn(paramMap);
		}
	}

	@Override
	public int searchSanctionLineProcAuthListCount(Map paramMap) throws NkiaException {
		return sanctionLineDAO.searchSanctionLineProcAuthListCount(paramMap);
	}

	@Override
	public List searchSanctionLineProcAuthList(Map paramMap) throws NkiaException {
		return sanctionLineDAO.searchSanctionLineProcAuthList(paramMap);
	}

	@Override
	public void deleteSanctionAuth(Map paramMap) throws NkiaException {
		sanctionLineDAO.deleteSanctionLineProcAuth(paramMap);
	}

	@Override
	public void insertSanctionLineAuth(Map paramMap) throws NkiaException {
		sanctionLineDAO.insertSanctionLineAuth(paramMap);
	}

}
