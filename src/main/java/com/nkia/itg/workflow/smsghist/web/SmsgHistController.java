package com.nkia.itg.workflow.smsghist.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.workflow.smsghist.service.SmsgHistService;

@Controller
public class SmsgHistController {

	@Resource(name="smsgHistService")
	private SmsgHistService smsgHistService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@RequestMapping(value="/itg/workflow/smsghist/goSmsgHistManager.do")
	public String goSmsgHistManager(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		return "/itg/workflow/smsgHist/smsgHistManager.nvf";
	}
	
	@RequestMapping(value="/itg/workflow/smsghist/goSmsgHistManager_user.do")
	public String goSmsgHistManager_user(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		return "/itg/workflow/smsgHist/smsgHistManager_user.nvf";
	}
	
	@RequestMapping(value="/itg/workflow/smsghist/searchSmsgHistList.do")
	public @ResponseBody ResultVO searchKmdbRequestList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			totalCount = smsgHistService.searchSmsgHistListCount(paramMap);
			resultList = smsgHistService.searchSmsgHistList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/workflow/smsghist/searchSmsgHistExcelDown.do")
	public @ResponseBody ResultVO searchSmsgHistExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		List resultList = new ArrayList();
		ModelMap excelMap = new ModelMap();
		try{
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			resultList = smsgHistService.searchSmsgHistList(paramMap);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
