package com.nkia.itg.workflow.smsghist.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SmsgHistService {
	
	public List searchSmsgHistList(Map paramMap) throws NkiaException;

	public int searchSmsgHistListCount(Map paramMap) throws NkiaException;
}
