package com.nkia.itg.workflow.smsghist.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.workflow.smsghist.service.SmsgHistService;

@Service("smsgHistService")
public class SmsgHistServiceImpl implements SmsgHistService {

	@Resource(name="smsgHistDAO")
	private SmsgHistDAO smsgHistDAO;
	
	public List searchSmsgHistList(Map paramMap) throws NkiaException {
		return smsgHistDAO.searchSmsgHistList(paramMap);
	}

	@Override
	public int searchSmsgHistListCount(Map paramMap) throws NkiaException {
		return smsgHistDAO.searchSmsgHistListCount(paramMap);
	}
}
