package com.nkia.itg.workflow.delayProc.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DelayProcService {
	
	public int searchDelayProcListCount(ModelMap paramMap);
		
	public List searchDelayProcList(ModelMap paramMap);
	
	public void sendEvent(ModelMap paramMap) throws NkiaException;
}
