package com.nkia.itg.workflow.delayProc.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("delayProcDao")
public class DelayProcDAO  extends EgovComAbstractDAO {
	
	public int searchDelayProcListCount(ModelMap paramMap) {
		return (Integer)selectByPk("delayProcDao.searchDelayProcListCount", paramMap);
	}
	
	public List searchDelayProcList(ModelMap paramMap) {
		return list("delayProcDao.searchDelayProcList", paramMap);
	}
	
	public void insertSendHist(HashMap srInfoMap){
		insert("delayProcDao.insertSendHist", srInfoMap);
	}
	
	public HashMap searchProcDetail(HashMap paramMap) {
		return (HashMap) selectByPk("delayProcDao.searchProcDetail", paramMap);
	}
}
