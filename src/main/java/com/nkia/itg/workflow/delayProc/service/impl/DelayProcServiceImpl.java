package com.nkia.itg.workflow.delayProc.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.workflow.delayProc.dao.DelayProcDAO;
import com.nkia.itg.workflow.delayProc.service.DelayProcService;



@Service("delayProcService")
public class DelayProcServiceImpl implements DelayProcService{
	private static final Logger logger = LoggerFactory.getLogger(DelayProcServiceImpl.class);
	
	@Resource(name = "delayProcDao")
	public DelayProcDAO delayProcDao;
	
	public int searchDelayProcListCount(ModelMap paramMap) {
		return delayProcDao.searchDelayProcListCount(paramMap);
	}

	public List searchDelayProcList(ModelMap paramMap) {
		return delayProcDao.searchDelayProcList(paramMap);
	}
	
	public void sendEvent(ModelMap paramMap) throws NkiaException {
		List sendList = (List) paramMap.get("sendList");
		HashMap srInfoMap = new HashMap();
		Map alarmMap = new HashMap();
		
		try {
			
			for (int i=0; i<sendList.size(); i++) {
				//메일발신 내부테이블 이력기능
				srInfoMap = (HashMap) sendList.get(i);
				delayProcDao.insertSendHist(srInfoMap);
				
				HashMap procDetail = delayProcDao.searchProcDetail(srInfoMap);
				
				String title = "[처리지연] " + (String) procDetail.get("TITLE");
				String content = "지연중인 아래 요청건에 대하여 확인 바랍니다.<br>" +
						"접수번호(SR_ID) : " + (String) srInfoMap.get("SR_ID") + "<br>" +
						"요청제목 : " + (String) procDetail.get("TITLE");
				alarmMap.put("KEY", "PROCESS");
				alarmMap.put("TEMPLATE_ID", "EMPTY_TEMPLATE");
				//알림 메일
				Map dataMap = new HashMap();
				// 1. 메일수신자셋팅(LIST로)
				List user = new ArrayList();
				user.add(procDetail);
				dataMap.put("TO_USER_LIST", user);
				// 2. 제목, 내용
				dataMap.put("TITLE", title);
				dataMap.put("CONTENT", content);
				alarmMap.put("DATA", dataMap);
				
				EmailSetData emailSender = new EmailSetData(alarmMap);
				emailSender.sendMail();
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
	}
}
