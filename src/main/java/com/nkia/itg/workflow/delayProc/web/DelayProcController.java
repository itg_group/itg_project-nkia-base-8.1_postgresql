/*
 * @(#)KedbController.java              2013. 5. 22.
 *
 * @eungyu@nakia.co.kr
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.delayProc.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.workflow.delayProc.service.DelayProcService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class DelayProcController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "delayProcService")
	private DelayProcService delayProcService;
	
	/**
	 * message 호출
	 */	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 지연요청프로세스 탭화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/delayProc/delayProcTab.do")
	public String delayProcTab(HttpServletRequest request, ModelMap paramMap) {
		return "/itg/workflow/delayProc/delayProcTab.nvf";
	}
	
	/**
	 * 지연요청프로세스 조회화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/delayProc/delayProcMain.do")
	public String delayProcList(HttpServletRequest request, ModelMap paramMap) {
		return "/itg/workflow/delayProc/delayProcMain.nvf";
	}
	
	/**
	 * 지연요청 리스트 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/delayProc/delayProcList.do")
	public @ResponseBody ResultVO selcetCheckListMainList(@RequestBody ModelMap paramMap) {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		int totalCount = 0;
		PagingUtil.getFristEndNum(paramMap);
		
		totalCount = delayProcService.searchDelayProcListCount(paramMap);
		List resultList = delayProcService.searchDelayProcList(paramMap);
		
		gridVO.setTotalCount(totalCount);
		gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
		gridVO.setRows(resultList);
		resultVO.setGridVO(gridVO);
		return resultVO;
	}
		
	@RequestMapping(value="/itg/workflow/delayProc/sendEvent.do")
	public @ResponseBody ResultVO sendEvent(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		delayProcService.sendEvent(paramMap);
		resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		return resultVO;
	}
}
