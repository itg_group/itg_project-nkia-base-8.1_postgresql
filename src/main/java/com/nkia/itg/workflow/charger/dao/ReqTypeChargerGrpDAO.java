package com.nkia.itg.workflow.charger.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("reqTypeChargerGrpDAO")
public class ReqTypeChargerGrpDAO extends EgovComAbstractDAO {

	public List searchAllReqTypeChargerGrpTree(Map paramMap) {
		return this.list("ReqTypeChargerGrpDAO.searchAllReqTypeChargerGrpTree", paramMap);
	}
	
	public HashMap selectReqTypeChargerGrpInfo(Map paramMap) {
		return (HashMap) selectByPk("ReqTypeChargerGrpDAO.selectReqTypeChargerGrpInfo", paramMap);
	}
	
	public List selectProcAuthCode(Map paramMap) {
		return this.list("ReqTypeChargerGrpDAO.selectProcAuthCode", paramMap);
	}
	
	public int searchReqTypeChargerGrpUserListCount(Map paramMap) {
		return (Integer) this.selectByPk("ReqTypeChargerGrpDAO.searchReqTypeChargerGrpUserListCount", paramMap);
	}

	public List searchReqTypeChargerGrpUserList(Map paramMap) {
		return this.list("ReqTypeChargerGrpDAO.searchReqTypeChargerGrpUserList", paramMap);
	}

	public String selectReqTypeGrpId() {
		return (String) selectByPk("ReqTypeChargerGrpDAO.selectReqTypeGrpId", null);
	}

	public void insertReqTypeChargerGrp(Map paramMap) {
		insert("ReqTypeChargerGrpDAO.insertReqTypeChargerGrp", paramMap);
	}

	public void updateReqTypeChargerGrp(Map paramMap) {
		update("ReqTypeChargerGrpDAO.updateReqTypeChargerGrp", paramMap);
	}

	public void deleteReqTypeChargerGrp(Map paramMap) {
		delete("ReqTypeChargerGrpDAO.deleteReqTypeChargerGrp", paramMap);
	}

	public void deleteReqTypeChargerUsers(Map paramMap) {
		delete("ReqTypeChargerGrpDAO.deleteReqTypeChargerUsers", paramMap);
	}

	public void deleteReqTypeChargerGrpCharger(Map paramMap) {
		delete("ReqTypeChargerGrpDAO.deleteReqTypeChargerGrpCharger", paramMap);
	}

	public void insertReqTypeChargerGrpCharger(Map paramMap) {
		insert("ReqTypeChargerGrpDAO.insertReqTypeChargerGrpCharger", paramMap);
	}

	public HashMap searchReqTypeChargerGrpId(Map paramMap) {
		return (HashMap) selectByPk("ReqTypeChargerGrpDAO.searchReqTypeChargerGrpId", paramMap);
	}

	public int searchReqTypeChargerGrpCount(Map paramMap) {
		return (Integer) this.selectByPk("ReqTypeChargerGrpDAO.searchReqTypeChargerGrpCount", paramMap);
	}
}
