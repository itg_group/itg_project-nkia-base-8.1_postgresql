package com.nkia.itg.workflow.charger.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.charger.service.ReqTypeChargerGrpService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ReqTypeChargerGrpController {
	
	@Resource(name = "reqTypeChargerGrpService")
	private ReqTypeChargerGrpService reqTypeChargerGrpService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 세부요청 담당자 그룹관리페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/goReqTypeChargerGrpManager.do")
	public String goReqTypeChargerGrpManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/workflow/charger/reqTypeChargerGrpManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 세부요청 담당자 그룹목록 트리 호출  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchAllReqTypeChargerGrpTree.do")
	public @ResponseBody ResultVO searchAllReqTypeChargerGrpTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = reqTypeChargerGrpService.searchAllReqTypeChargerGrpTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 정보 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/selectReqTypeChargerGrpInfo.do")
	public @ResponseBody ResultVO selectReqTypeChargerGrpInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = reqTypeChargerGrpService.selectReqTypeChargerGrpInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 정보 호출 - 프로세스 권한 불러오기
	 * 코브콤보(프로세스 권한)의 id는 동일하게, value, title은 SYS_PROC_AUTH 권한명을 불러온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/selectProcAuthCode.do")
	public @ResponseBody ResultVO selectProcAuthCode(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO resultGridVO = new GridVO();
		try{
			resultGridVO.setRows(reqTypeChargerGrpService.selectProcAuthCode(paramMap));
			resultVO.setGridVO(resultGridVO);
			resultVO.setSuccess(true);
		}catch(NkiaException e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 사용자 그리드 호출 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchReqTypeChargerGrpUser.do")  
	public @ResponseBody ResultVO searchReqTypeChargerGrpUser(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				PagingUtil.getFristEndNum(paramMap);
			}
		
			totalCount = reqTypeChargerGrpService.searchReqTypeChargerGrpUserListCount(paramMap);
			List resultList = reqTypeChargerGrpService.searchReqTypeChargerGrpUserList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/insertReqTypeChargerGrp.do")
	public @ResponseBody ResultVO insertReqTypeChargerGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String GRP_USER_ID = userVO.getUser_id();
			paramMap.put("grp_user_id", GRP_USER_ID);
			
			reqTypeChargerGrpService.insertReqTypeChargerGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/updateReqTypeChargerGrp.do")
	public @ResponseBody ResultVO updateReqTypeChargerGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String UPD_USER_ID = userVO.getUser_id();
			paramMap.put("upd_user_id", UPD_USER_ID);
			
			reqTypeChargerGrpService.updateReqTypeChargerGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/deleteReqTypeChargerGrp.do")
	public @ResponseBody ResultVO deleteReqTypeChargerGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			reqTypeChargerGrpService.deleteReqTypeChargerGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 사용자 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/deleteReqTypeChargerGrpCharger.do")
	public @ResponseBody ResultVO deleteReqTypeChargerGrpCharger(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			reqTypeChargerGrpService.deleteReqTypeChargerGrpCharger(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요쳥 담당자 그룹 사용자 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/insertReqTypeChargerGrpCharger.do")
	public @ResponseBody ResultVO insertReqTypeChargerGrpCharger(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String REGIST_USER_ID = userVO.getUser_id();
			paramMap.put("regist_user_id", REGIST_USER_ID);
			
			reqTypeChargerGrpService.insertReqTypeChargerGrpCharger(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 ID 조회 - 요청등록시 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchReqTypeChargerGrpId.do")
	public @ResponseBody ResultVO searchReqTypeChargerGrpId(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = reqTypeChargerGrpService.searchReqTypeChargerGrpId(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 세부요청 담당자 그룹 Count - 같은 그룹은 하나만 등록가능(검증)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchReqTypeChargerGrpCount.do")
	public @ResponseBody ResultVO searchReqTypeChargerGrpCount(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		int grpCount = 0;
		
		try {
			grpCount = reqTypeChargerGrpService.searchReqTypeChargerGrpCount(paramMap);
			resultVO.setResultInt(grpCount);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
