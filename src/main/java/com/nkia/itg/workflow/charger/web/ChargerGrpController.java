package com.nkia.itg.workflow.charger.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.charger.constraint.ChargerGrpEnum;
import com.nkia.itg.workflow.charger.service.ChargerGrpService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ChargerGrpController {
	
	@Resource(name = "chargerGrpService")
	private ChargerGrpService chargerGrpService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	
	/**
	 * 담당자 그룹관리페이지 호출(운영업무관리메뉴)
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/goChargerGrpManager.do")
	public String goChargerGrpManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
/*		
		//개인화 메뉴인지 관리자용 메뉴인지 확인
		String adminYn = NkiaApplicationPropertiesMap.getProperty("Globals.ChargerGrp.AdminYN");
		
		//그룹관리 등록 관리자ID
		String adminId = NkiaApplicationPropertiesMap.getProperty("Globals.ChargerGrp.AdminID");
		
		//개인화 메뉴인 경우
		if(adminYn != null && "N".equals(adminYn)){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String GRP_USER_ID = userVO.getUser_id();
			paramMap.put("grp_user_id", GRP_USER_ID);
		
		}else{
			paramMap.put("grp_user_id", adminId);
		
		}
*/		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String GRP_USER_ID = userVO.getUser_id();
		paramMap.put("grp_user_id", GRP_USER_ID);
		
		//장애이벤트 담당자 그룹 사용여부
		String incidentGrpUse = NkiaApplicationPropertiesMap.getProperty("Globals.Incident.Grpid");
		if( incidentGrpUse != null && !"".equals(incidentGrpUse)){
			paramMap.put("incidentGrpUse", incidentGrpUse);
		}else{
			paramMap.put("incidentGrpUse", "N");
		}
				
		String forwarPage = "/itg/workflow/charger/chargerGrpManager.nvf";
		return forwarPage;
	}
	@RequestMapping(value="/itg/workflow/charger/goChargerGrpServiceSubReqManager.do")
	public String goChargerGrpServiceSubReqManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/workflow/charger/chargerGrpServiceSubReqManager.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/workflow/charger/goSubReqAdminManager.do")
	public String goSubReqAdminManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/workflow/charger/subReqAdminManager.nvf";
		return forwarPage;
	}
	@RequestMapping(value="/itg/workflow/charger/goSearchSubReqCustomerManager.do")
	public String goSearchSubReqCustomerManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/workflow/charger/searchSubReqCustomerManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 담당자 그룹관리페이지 호출(서비스데스크메뉴)
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/goChargerGrpManagerForSRDSK.do")
	public String goChargerGrpManagerForSRDSK(ModelMap paramMap) throws NkiaException {
		
		String GRP_USER_ID = ChargerGrpEnum.SERVICEDESK.getMessage(); 
		paramMap.put("grp_user_id", GRP_USER_ID);
		
		//장애이벤트 담당자 그룹 사용여부
		String incidentGrpUse = NkiaApplicationPropertiesMap.getProperty("Globals.Incident.Grpid");
		if( incidentGrpUse != null && !"".equals(incidentGrpUse)){
			paramMap.put("incidentGrpUse", incidentGrpUse);
		}else{
			paramMap.put("incidentGrpUse", "N");
		}
			
		String forwarPage = "/itg/workflow/charger/chargerGrpManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 담당자 그룹목록 트리 호출  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchAllChargerGrpTree.do")
	public @ResponseBody ResultVO searchAllChargerGrpTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			String grpUserId = String.valueOf(paramMap.get("grp_user_id"));
			String operType = String.valueOf(paramMap.get("oper_type"));
/*			
			//개인화 메뉴인지 관리자용 메뉴인지 확인
			String adminYn = NkiaApplicationPropertiesMap.getProperty("Globals.ChargerGrp.AdminYN");
			
			//그룹관리 등록 관리자ID
			String adminId = NkiaApplicationPropertiesMap.getProperty("Globals.ChargerGrp.AdminID");
			
			//개인화 메뉴인 경우
			if(adminYn != null && "N".equals(adminYn)){
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				String loginUserId = userVO.getUser_id();
				paramMap.put("grp_user_id", adminId);
				paramMap.put("personal_user_id", loginUserId);
				
			}else{
				paramMap.put("grp_user_id", adminId);
			
			}
*/
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String GRP_USER_ID = userVO.getUser_id();
			
			// [담당자 관리]에서 호출하는 것인지, 담당자 검색을 통해 호출하는 것인지 판별
			if(!"null".equals(grpUserId)){
				// [담당자 관리]에서 호출하는 경우, 개인화 혹은 서비스 데스크용으로 나뉨
				paramMap.put("grp_user_id", grpUserId);				
				
			}else{
			    // 담당자 검색을 통해 호출하는 경우, 해당 테스크의 롤에 따라 개인화 혹은 서비스 데스크용으로 나뉨
				if(operType.equals(ChargerGrpEnum.SERVICEDESK_ROLE.getMessage())){
					grpUserId = ChargerGrpEnum.SERVICEDESK.getMessage();
					
				}else{
					grpUserId = GRP_USER_ID;
					
				}
				paramMap.put("grp_user_id", grpUserId);				
			}
			
			List resultList = chargerGrpService.searchAllChargerGrpTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 그룹목록 트리 호출  (카운트없음)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchChargerGrpTree.do")
	public @ResponseBody ResultVO searchChargerGrpTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	
			List resultList = chargerGrpService.searchChargerGrpTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자정보에서 사용될 세부유형정보 트리 호출 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchSubReqTree.do")
	public @ResponseBody ResultVO  searchSubReqTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			List resultList = chargerGrpService.searchSubReqTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 담당자정보에서 사용될 고객사정보 트리 호출 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchCustomerTree.do")
	public @ResponseBody ResultVO  searchCustomerTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			List resultList = chargerGrpService.searchCustomerTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	/**
	 * 담당자 그룹 정보 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/selectChargerGrpInfo.do")
	public @ResponseBody ResultVO selectChargerGrpInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = chargerGrpService.selectChargerGrpInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 그룹ID 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/selectGrpId.do")
	public @ResponseBody ResultVO selectGrpId(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = chargerGrpService.selectGrpId();
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 담당자 그룹 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/insertChargerGrp.do")
	public @ResponseBody ResultVO insertChargerGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			chargerGrpService.insertChargerGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
		
	/**
	 * 담당자 그룹 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/deleteChargerGrp.do")
	public @ResponseBody ResultVO deleteChargerGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			chargerGrpService.deleteChargerGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 그룹 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/updateChargerGrp.do")
	public @ResponseBody ResultVO updateChargerGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String UPD_USER_ID = userVO.getUser_id();
			paramMap.put("upd_user_id", UPD_USER_ID);
			
			chargerGrpService.updateChargerGrp(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 그룹 사용자 그리드 호출  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchChargerGrpUser.do")  
	public @ResponseBody ResultVO searchChargerGrpUser(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				PagingUtil.getFristEndNum(paramMap);
			}
		
			totalCount = chargerGrpService.searchGrpUserListCount(paramMap);
			List resultList = chargerGrpService.searchGrpUserList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자 그룹 사용자 종합 그리드 호출  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchChargerGrpUserTotal.do")  
	public @ResponseBody ResultVO searchChargerGrpUserTotal(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				PagingUtil.getFristEndNum(paramMap);
			}
		
			totalCount = chargerGrpService.searchGrpUserListTotalCount(paramMap);
			List resultList = chargerGrpService.searchGrpUserListTotal(paramMap);
			
			gridVO.setTotalCount(totalCount);
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	/**
	 * 담당자 그룹 사용자 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/deleteChargerGrpCharger.do")
	public @ResponseBody ResultVO deleteChargerGrpCharger(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			chargerGrpService.deleteChargerGrpCharger(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 그룹 사용자 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/insertChargerGrpCharger.do")
	public @ResponseBody ResultVO insertChargerGrpCharger(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String UPD_USER_ID = userVO.getUser_id();
			paramMap.put("regist_user_id", UPD_USER_ID);
			
			chargerGrpService.insertChargerGrpCharger(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 담당자 트리 검색-트리명으로 검색
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchTreeDataGroup.do") 
	public @ResponseBody ResultVO searchTreeDataGroup(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			List resultList = chargerGrpService.searchTreeDataGroup(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 트리 검색-사용자명으로 검색
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchTreeDataUser.do") 
	public @ResponseBody ResultVO searchTreeDataUser(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			List resultList = chargerGrpService.searchTreeDataUser(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서 사용자 그리드 호출 (사용자 선택팝업)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchDeptUserGrid.do")
	public @ResponseBody ResultVO searchDeptUserGrid(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = chargerGrpService.searchDeptUserGridListCount(paramMap);
			List resultList = chargerGrpService.searchDeptUserGridList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서트리팝업 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchStaticAllDeptTree.do")
	public @ResponseBody ResultVO searchStaticAllDeptTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			List resultList = chargerGrpService.searchStaticAllDeptTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			//TreeVO treeVO = JsonUtil.changeTreeMapListToJson(resultList, up_node_id, true, expand_level);
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 그룹 부서 그리드 호출  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchChargerGrpDept.do")  
	public @ResponseBody ResultVO searchChargerGrpDept(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				PagingUtil.getFristEndNum(paramMap);
			}
		
			totalCount = chargerGrpService.searchGrpDeptListCount(paramMap);
			List resultList = chargerGrpService.searchGrpDeptList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자 그룹 사용자그룹 그리드 호출  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/searchChargerGrpUserGrp.do")  
	public @ResponseBody ResultVO searchChargerGrpUserGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				PagingUtil.getFristEndNum(paramMap);
			}
		
			totalCount = chargerGrpService.searchGrpUserGrpListCount(paramMap);
			List resultList = chargerGrpService.searchGrpUserGrpList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			if(paramMap.containsKey(paramMap.get(BaseEnum.PAGER_START_PARAM.getString()))){
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자 그룹 사용자 삭제(등록 취소할 경우)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/deleteChargerUsers.do")
	public @ResponseBody ResultVO deleteChargerUsers(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			chargerGrpService.deleteChargerUsers(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자그룹 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="itg/workflow/charger/checkReqCustomer.do")
	public @ResponseBody ResultVO checkReqCustomer(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	String returnMsg = chargerGrpService.checkReqCustomer(paramMap);
	    	resultVO.setResultMsg(returnMsg);
	    	
	    	
	    	

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자그룹 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="itg/workflow/charger/updateReqCustomer.do")
	public @ResponseBody ResultVO updateReqCustomer(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
    		HashMap<String, String> resultMap = chargerGrpService.updateReqCustomer(paramMap);
    		resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));
    	} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자그룹 삭제
     * @param paramMap
	 * @return
	 * @
	 */       
	@RequestMapping(value="/itg/workflow/charger/deleteReqCustomer.do")
	public @ResponseBody ResultVO deleteReqCustomer(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	chargerGrpService.deleteReqCustomer(paramMap);
	    	resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자그룹별 조회화면 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchSubReqCustomerList.do")
	public @ResponseBody ResultVO searchSubReqCustomerList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			List resultList = new ArrayList();
			PagingUtil.getFristEndNum(paramMap);
			boolean registSW = (boolean) paramMap.get("registSW");
			if(registSW){
				totalCount = chargerGrpService.searchSubReqCustomerListCount(paramMap);
				resultList = chargerGrpService.searchSubReqCustomerList(paramMap);
			}else{
				totalCount = chargerGrpService.searchUnSubReqCustomerListCount(paramMap);
				resultList = chargerGrpService.searchUnSubReqCustomerList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 담당자그룹 담당자 별 조회화면 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchManagerList.do")
	public @ResponseBody ResultVO searchManagerList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = new ArrayList();
			resultList = chargerGrpService.searchManagerList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	
	
	@RequestMapping(value="/itg/workflow/charger/subReqAdminList.do")
	public @ResponseBody ResultVO subReqAdminList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			
			List resultList = chargerGrpService.subReqAdminList(paramMap);
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 세부유형별 관리자수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/updateSubReqAdmin.do")
	public @ResponseBody ResultVO updateSubReqAdmin(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = chargerGrpService.updateSubReqAdmin(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 세부유형별 관리자삭제
     * @param paramMap
	 * @return
	 * @
	 */       
	@RequestMapping(value="/itg/workflow/charger/deleteSubReqAdmin.do")
	public @ResponseBody ResultVO deleteSubReqAdmin(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = chargerGrpService.deleteSubReqAdmin(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자그룹관리 권한 조회 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchChargerAuth.do")
	public @ResponseBody ResultVO searchChargerAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = chargerGrpService.searchChargerAuth(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자그룹관리 권한 조회 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/charger/searchChargerAuthPop.do")
	public @ResponseBody ResultVO searchChargerAuthPop(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = chargerGrpService.searchChargerAuthPop(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 담당자 그룹 관리 - 권한등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/insertChargerAuth.do")
	public @ResponseBody ResultVO insertChargerAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramMap.put("upd_user_id", userVO.getUser_id());
		try {
			chargerGrpService.insertChargerAuth(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
		
	/**
	 * 담당자 그룹 관리 - 권한삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/deleteChargerAuth.do")
	public @ResponseBody ResultVO deleteChargerAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramMap.put("upd_user_id", userVO.getUser_id());
		try {
			chargerGrpService.deleteChargerAuth(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
