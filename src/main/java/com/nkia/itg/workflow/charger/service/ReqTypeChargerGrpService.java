package com.nkia.itg.workflow.charger.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;


public interface ReqTypeChargerGrpService {

	List searchAllReqTypeChargerGrpTree(Map paramMap) throws NkiaException;

	HashMap selectReqTypeChargerGrpInfo(Map paramMap) throws NkiaException;
	
	List selectProcAuthCode(Map paramMap) throws NkiaException;
	
	int searchReqTypeChargerGrpUserListCount(Map paramMap) throws NkiaException;

	List searchReqTypeChargerGrpUserList(Map paramMap) throws NkiaException;
	
	void insertReqTypeChargerGrp(Map paramMap) throws NkiaException;

	void updateReqTypeChargerGrp(Map paramMap) throws NkiaException;

	void deleteReqTypeChargerGrp(Map paramMap) throws NkiaException;

	void deleteReqTypeChargerGrpCharger(Map paramMap) throws NkiaException;

	void insertReqTypeChargerGrpCharger(Map paramMap) throws NkiaException;

	HashMap searchReqTypeChargerGrpId(Map paramMap) throws NkiaException;

	int searchReqTypeChargerGrpCount(Map paramMap) throws NkiaException;
	
}
