package com.nkia.itg.workflow.charger.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

public interface ChargerGrpService {
	
	public List searchAllChargerGrpTree(Map paramMap) throws NkiaException;
	
	public List searchChargerGrpTree(Map paramMap) throws NkiaException;
	
	public List searchSubReqTree(Map paramMap) throws NkiaException;
	
	public List searchCustomerTree(Map paramMap) throws NkiaException;
	
	public HashMap selectChargerGrpInfo(Map paramMap) throws NkiaException;
	
	public HashMap selectGrpId() throws NkiaException;
	
	public void insertChargerGrp(Map paramMap) throws NkiaException;
	
	public void deleteChargerGrp(Map paramMap) throws NkiaException;
	
	public void updateChargerGrp(Map paramMap) throws NkiaException;
	
	public void deleteChargerGrpCharger(Map paramMap) throws NkiaException;

	public List searchDeptUser(Map paramMap) throws NkiaException;

	public void insertChargerGrpCharger(Map paramMap) throws NkiaException;

	public int searchGrpUserListCount(Map paramMap)throws NkiaException;

	public List searchGrpUserList(Map paramMap)throws NkiaException;
	
	public int searchGrpUserListTotalCount(Map paramMap)throws NkiaException;

	public List searchGrpUserListTotal(Map paramMap)throws NkiaException;


	public List searchTreeDataGroup(Map paramMap)throws NkiaException;

	public List searchTreeDataUser(Map paramMap)throws NkiaException;

	public int searchDeptUserGridListCount(Map paramMap)throws NkiaException;

	public List searchDeptUserGridList(Map paramMap)throws NkiaException;

	public List searchStaticAllDeptTree(Map paramMap)throws NkiaException;

	public int searchGrpDeptListCount(Map paramMap) throws NkiaException;

	public List searchGrpDeptList(Map paramMap) throws NkiaException;
	
	public int searchGrpUserGrpListCount(Map paramMap) throws NkiaException;

	public List searchGrpUserGrpList(Map paramMap) throws NkiaException;

	public void deleteChargerUsers(Map paramMap) throws NkiaException;
	
	public String checkReqCustomer(ModelMap paramMap) throws NkiaException;
	
	public HashMap<String, String> updateReqCustomer(ModelMap paramMap) throws NkiaException;
	
	public void deleteReqCustomer(ModelMap paramMap) throws NkiaException;
	
	public int searchSubReqCustomerListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchSubReqCustomerList(ModelMap paramMap) throws NkiaException;
	
	public List searchManagerList(ModelMap paramMap) throws NkiaException;
	
	public int searchUnSubReqCustomerListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchUnSubReqCustomerList(ModelMap paramMap) throws NkiaException;
	
	public List searchReqTypeList(ModelMap paramMap) throws NkiaException;
	
	public List subReqAdminList(ModelMap paramMap) throws NkiaException;
	
	public HashMap<String, String> updateSubReqAdmin(ModelMap paramMap) throws NkiaException;
	
	public HashMap<String, String> deleteSubReqAdmin(ModelMap paramMap) throws NkiaException;
	
	public List searchChargerAuth(Map paramMap) throws NkiaException;
	
	public List searchChargerAuthPop(Map paramMap) throws NkiaException;
	
	public void insertChargerAuth(Map paramMap) throws NkiaException;
	
	public void deleteChargerAuth(Map paramMap) throws NkiaException;
}
