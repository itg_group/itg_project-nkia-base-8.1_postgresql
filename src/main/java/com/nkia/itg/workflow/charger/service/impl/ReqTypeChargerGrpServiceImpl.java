package com.nkia.itg.workflow.charger.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.workflow.charger.dao.ReqTypeChargerGrpDAO;
import com.nkia.itg.workflow.charger.service.ReqTypeChargerGrpService;


@Service("reqTypeChargerGrpService")
public class ReqTypeChargerGrpServiceImpl implements ReqTypeChargerGrpService{
	
	private static final Logger logger = LoggerFactory.getLogger(ReqTypeChargerGrpServiceImpl.class);
	
	@Resource(name = "reqTypeChargerGrpDAO")
	public ReqTypeChargerGrpDAO reqTypeChargerGrpDAO;

	@Override
	public List searchAllReqTypeChargerGrpTree(Map paramMap) throws NkiaException {
		return reqTypeChargerGrpDAO.searchAllReqTypeChargerGrpTree(paramMap);
	}

	@Override
	public HashMap selectReqTypeChargerGrpInfo(Map paramMap) throws NkiaException {
		return reqTypeChargerGrpDAO.selectReqTypeChargerGrpInfo(paramMap);
	}

	@Override
	public int searchReqTypeChargerGrpUserListCount(Map paramMap) throws NkiaException {
		return reqTypeChargerGrpDAO.searchReqTypeChargerGrpUserListCount(paramMap);
	}

	@Override
	public List searchReqTypeChargerGrpUserList(Map paramMap) throws NkiaException {
		return reqTypeChargerGrpDAO.searchReqTypeChargerGrpUserList(paramMap);
	}
	
	@Override
	public List selectProcAuthCode(Map paramMap) throws NkiaException {
		return reqTypeChargerGrpDAO.selectProcAuthCode(paramMap);
	}
	
	@Override
	public void insertReqTypeChargerGrp(Map paramMap) throws NkiaException {
		String grp_id = reqTypeChargerGrpDAO.selectReqTypeGrpId();
		
		paramMap.put("grp_id", grp_id);
		paramMap = WebUtil.lowerCaseMapKey(paramMap);
		reqTypeChargerGrpDAO.insertReqTypeChargerGrp(paramMap);
		
	}

	@Override
	public void updateReqTypeChargerGrp(Map paramMap) throws NkiaException {
		paramMap = WebUtil.lowerCaseMapKey(paramMap);
		reqTypeChargerGrpDAO.updateReqTypeChargerGrp(paramMap);
	}

	@Override
	public void deleteReqTypeChargerGrp(Map paramMap) throws NkiaException {
		reqTypeChargerGrpDAO.deleteReqTypeChargerGrp(paramMap);
		reqTypeChargerGrpDAO.deleteReqTypeChargerUsers(paramMap);
	}

	@Override
	public void deleteReqTypeChargerGrpCharger(Map paramMap) throws NkiaException {
		List Users = (List) paramMap.get("charger_id");
		final int LENGTH = Users.size();
		
		for(int i=0; i < LENGTH; i++){
			Map deleteRowMap = new HashMap();
			
			deleteRowMap.put("grp_id", paramMap.get("grp_id"));
			deleteRowMap.put("charger_id", Users.get(i) );
			
			reqTypeChargerGrpDAO.deleteReqTypeChargerGrpCharger(deleteRowMap);
		}
	}

	@Override
	public void insertReqTypeChargerGrpCharger(Map paramMap) throws NkiaException {
		reqTypeChargerGrpDAO.deleteReqTypeChargerUsers(paramMap);
		
		List Users = (List) paramMap.get("charger_id");
		final int LENGTH = Users.size();
		
		for(int i=0; i<LENGTH; i++){
			Map insertMap = new HashMap();
			
			insertMap.put("regist_user_id", paramMap.get("regist_user_id"));
			insertMap.put("grp_id", paramMap.get("grp_id"));
			insertMap.put("charger_id", Users.get(i));
			insertMap.put("user_yn", paramMap.get("user_yn"));
			
			reqTypeChargerGrpDAO.insertReqTypeChargerGrpCharger(insertMap);
		}
	}

	@Override
	public HashMap searchReqTypeChargerGrpId(Map paramMap) throws NkiaException {
		return reqTypeChargerGrpDAO.searchReqTypeChargerGrpId(paramMap);
	}

	@Override
	public int searchReqTypeChargerGrpCount(Map paramMap) throws NkiaException {
		paramMap = WebUtil.lowerCaseMapKey(paramMap);
		return reqTypeChargerGrpDAO.searchReqTypeChargerGrpCount(paramMap);
	}
	
}
