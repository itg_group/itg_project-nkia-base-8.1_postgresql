package com.nkia.itg.workflow.charger.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class UserSelectPopController {

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	/**
	 * 템플릿 메인 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/charger/userSelectPopMain.do")
	public String goUserSelectPopMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		
		String grp_id = request.getParameter("grp_id");
		paramMap.put("grp_id", grp_id);
		
		String forwarPage = "/itg/workflow/charger/userSelectPopMain.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/workflow/charger/goUserSelectTab1.do")
	public String goUserSelectTab1(HttpServletRequest request) throws NkiaException {
		String forwarPage = "/itg/workflow/charger/userSelectTab1.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/workflow/charger/goUserSelectTab2.do")
	public String goUserSelectTab2(HttpServletRequest request) throws NkiaException {
		String forwarPage = "/itg/workflow/charger/userSelectTab2.nvf";
		return forwarPage;
	}
	
}
