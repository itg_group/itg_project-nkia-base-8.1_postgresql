package com.nkia.itg.workflow.charger.constraint;

/**
 * 사용자별 담당자 관리 페이지에 접속하는데 필요한 상수들을 정의함
 * @author dhkim
 *
 */
public enum ChargerGrpEnum {
	
	SERVICEDESK("SRDSK"),
	SERVICEDESK_ROLE("CNSLR");
		
	private String message;
	
	ChargerGrpEnum(String message){
		this.message = message;
    }
	
	public String getMessage() {
		return this.message;
	}
}
