package com.nkia.itg.workflow.charger.service.impl;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.security.CipherRSA;
import com.nkia.itg.base.application.util.security.CipherSHA;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.charger.dao.ChargerGrpDAO;
import com.nkia.itg.workflow.charger.service.ChargerGrpService;

import egovframework.com.cmm.EgovMessageSource;


@Service("chargerGrpService")
public class ChargerGrpServiceImpl implements ChargerGrpService{
	
	private static final Logger logger = LoggerFactory.getLogger(ChargerGrpServiceImpl.class);
	
	@Resource(name = "chargerGrpDAO")
	public ChargerGrpDAO chargerGrpDAO;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Override
	public List searchAllChargerGrpTree(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchAllChargerGrpTree(paramMap);
	}
	
	@Override
	public List searchChargerGrpTree(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchChargerGrpTree(paramMap);
	}
	
	@Override
	public List searchSubReqTree(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchSubReqTree(paramMap);
	}
	
	@Override
	public List searchCustomerTree(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchCustomerTree(paramMap);
	}
	
	@Override
	public HashMap selectChargerGrpInfo(Map paramMap) {
		return chargerGrpDAO.selectChargerGrpInfo(paramMap);
	}

	@Override	
	public HashMap selectGrpId() throws NkiaException {
		Map resultMap = new HashMap();
		String grp_id = (String)chargerGrpDAO.selectGrpId();
		resultMap.put("grp_id", grp_id);
		
		return (HashMap) resultMap;
	}

	@Override
	
	public void insertChargerGrp(Map paramMap) {
		chargerGrpDAO.insertChargerGrp(paramMap);
	}
	
	@Override
	public void deleteChargerGrp(Map paramMap) throws NkiaException {
		chargerGrpDAO.deleteChargerGrp(paramMap);
		chargerGrpDAO.deleteChargerUsers(paramMap);
		chargerGrpDAO.deleteChargerToReq(paramMap);
		chargerGrpDAO.deleteChargerToCustomer(paramMap);
	}
	@Override
	public void updateChargerGrp(Map paramMap) {
		chargerGrpDAO.updateChargerGrp(paramMap);
		
		if((paramMap.get("leafFalg")).equals("N")){
			chargerGrpDAO.updateUseYn(paramMap);
		}
	}

	@Override
	public void deleteChargerGrpCharger(Map paramMap) { 
		List Users = (List) paramMap.get("charger_id");
		final int LENGTH = Users.size();
		
		for(int i=0; i < LENGTH; i++){
			Map deleteRowMap = new HashMap();
			
			deleteRowMap.put("grp_id", paramMap.get("grp_id"));
			deleteRowMap.put("charger_id", Users.get(i) );
			
			chargerGrpDAO.deleteChargerGrpCharger(deleteRowMap);
		}
	}

	@Override
	public List searchDeptUser(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchDeptUser(paramMap);
	}

	@Override
	public void insertChargerGrpCharger(Map paramMap) throws NkiaException {
		chargerGrpDAO.deleteChargerUsers(paramMap);
		
		List Users = (List) paramMap.get("charger_id");
		final int LENGTH = Users.size();
		
		for(int i=0; i<LENGTH; i++){
			Map insertMap = new HashMap();
			
			insertMap.put("regist_user_id", paramMap.get("regist_user_id"));
			insertMap.put("grp_id", paramMap.get("grp_id"));
			insertMap.put("charger_id", Users.get(i));
			insertMap.put("user_yn", paramMap.get("user_yn"));
			
			chargerGrpDAO.insertChargerGrpCharger(insertMap);
		}
	}

	@Override
	public int searchGrpUserListCount(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpUserListCount(paramMap);
	}

	@Override
	public List searchGrpUserList(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpUserList(paramMap); 
	}

	@Override
	public int searchGrpUserListTotalCount(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpUserListTotalCount(paramMap);
	}

	@Override
	public List searchGrpUserListTotal(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpUserListTotal(paramMap); 
	}

	@Override
	public List searchTreeDataGroup(Map paramMap) throws NkiaException {		
		return chargerGrpDAO.searchTreeDataGroup(paramMap);
	}

	@Override
	public List searchTreeDataUser(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchTreeDataUser(paramMap); 
	}

	@Override
	public int searchDeptUserGridListCount(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchDeptUserGridListCount(paramMap);
	}

	@Override
	public List searchDeptUserGridList(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchDeptUserGridList(paramMap);
	}
	
	public List searchStaticAllDeptTree(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchStaticAllDeptTree(paramMap);
	}
	
	@Override
	public int searchGrpDeptListCount(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpDeptListCount(paramMap);
	}

	@Override
	public List searchGrpDeptList(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpDeptList(paramMap); 
	}
	@Override
	public int searchGrpUserGrpListCount(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpUserGrpListCount(paramMap);
	}

	@Override
	public List searchGrpUserGrpList(Map paramMap) throws NkiaException {
		return chargerGrpDAO.searchGrpUserGrpList(paramMap); 
	}
	@Override
	public void deleteChargerUsers(Map paramMap) throws NkiaException {
		chargerGrpDAO.deleteChargerUsers(paramMap);
	}
	public String checkReqCustomer(ModelMap paramMap) throws NkiaException {
		String returnMsg = "";
		HashMap<String, String> checkMap = new HashMap<String, String>();
		

		ArrayList checkreq_node = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("reqInfo")).get("req_id");
		ArrayList checkcustomer_node = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("customerInfo")).get("customer_id");
		for( int i = 0; i < checkreq_node.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("reqInfo")).put("req_id", checkreq_node.get(i));
			int checkReq = chargerGrpDAO.checkReq(paramMap.get("reqInfo"));
			if(checkReq > 0){
				for( int j = 0; j < checkcustomer_node.size(); j++ ){
					((HashMap<String, Object>) paramMap.get("customerInfo")).put("customer_id", checkcustomer_node.get(j));
					((HashMap<String, Object>) paramMap.get("customerInfo")).put("req_id", checkreq_node.get(i).toString());
					int checkCustomer = chargerGrpDAO.checkCustomer(paramMap.get("customerInfo"));
					if(checkCustomer > 0){
						checkMap.put("req_id", checkreq_node.get(i).toString());
						checkMap.put("customer_id", checkcustomer_node.get(j).toString());
						checkMap.put("center_id", ((LinkedHashMap<String, Object>)paramMap.get("reqInfo")).get("center_id").toString());
						returnMsg = chargerGrpDAO.resultString(checkMap);
						return returnMsg;
					}
				}
			}
		}
		return returnMsg;
	}
	public HashMap<String, String> updateReqCustomer(ModelMap paramMap) throws NkiaException {
		boolean checkLogic = true;
		HashMap<String, String> resultMap = new HashMap<String, String>();
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
			
		// 세부유형 삭제
		chargerGrpDAO.deleteReq(paramMap.get("reqInfo"));
		
		// 세부유형 등록
		ArrayList array_node = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("reqInfo")).get("req_id");
		((HashMap<String, Object>) paramMap.get("reqInfo")).put("upd_user_id", paramMap.get("upd_user_id"));
		for( int i = 0; i < array_node.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("reqInfo")).put("req_id", array_node.get(i));
			chargerGrpDAO.insertReq(paramMap.get("reqInfo"));
		}
		
		// 고객사 삭제
		chargerGrpDAO.deleteCustomer(paramMap.get("customerInfo"));
		
		// 고객사 등록
		ArrayList array_customer = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("customerInfo")).get("customer_id");
		((HashMap<String, Object>) paramMap.get("customerInfo")).put("upd_user_id", paramMap.get("upd_user_id"));
		for( int i = 0; i < array_customer.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("customerInfo")).put("customer_id", array_customer.get(i));
			chargerGrpDAO.insertCustomer(paramMap.get("customerInfo"));
		}
		// 요청유형별 이력관리
		Map tempMap = new HashMap();
		tempMap.put("upd_user_id", paramMap.get("upd_user_id"));
		tempMap.put("grp_id", ((HashMap<String, Object>) paramMap.get("reqInfo")).get("user_grp_id"));
		tempMap.put("center_id", ((HashMap<String, Object>) paramMap.get("reqInfo")).get("center_id"));
		chargerGrpDAO.insertReqHistory(tempMap);
		
		resultMap.put("RESULT_MSG", resultMsg);
		return resultMap;
	}
	public void deleteReqCustomer(ModelMap paramMap) throws NkiaException {
		// 삭제 로직
		chargerGrpDAO.deleteReq(paramMap);
		chargerGrpDAO.deleteCustomer(paramMap);
	}

	
	public List searchManagerList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = chargerGrpDAO.searchManagerList(paramMap); 
		return resultList;
	}

	
	public int searchSubReqCustomerListCount(ModelMap paramMap) throws NkiaException {
		return chargerGrpDAO.searchSubReqCustomerListCount(paramMap);
	}
	
	public List searchSubReqCustomerList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = chargerGrpDAO.searchSubReqCustomerList(paramMap); 
		return resultList;
	}
	
	public int searchUnSubReqCustomerListCount(ModelMap paramMap) throws NkiaException {
		return chargerGrpDAO.searchUnSubReqCustomerListCount(paramMap);
	}
	
	public List searchUnSubReqCustomerList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = chargerGrpDAO.searchUnSubReqCustomerList(paramMap); 
		return resultList;
	}
	@Override
	public List searchReqTypeList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return chargerGrpDAO.searchReqTypeList(paramMap);
	}
	
	@Override 
	public List subReqAdminList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return chargerGrpDAO.subReqAdminList(paramMap);
	}
	@Override 
	public HashMap<String, String> updateSubReqAdmin(ModelMap paramMap) throws NkiaException {
		boolean checkLogic = true;
		HashMap<String, String> resultMap = new HashMap<String, String>();
		HashMap<String, String> checkMap = new HashMap<String, String>();


		// 세부유형 삭제
		// 세부유형 건 삭제 후 등록
		ArrayList array_node = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("reqInfo")).get("req_id");
		((HashMap<String, Object>) paramMap.get("reqInfo")).put("upd_user_id", paramMap.get("upd_user_id"));
		((HashMap<String, Object>) paramMap.get("reqInfo")).put("admin_id", paramMap.get("admin_id"));
		((HashMap<String, Object>) paramMap.get("reqInfo")).put("grp_yn", paramMap.get("grp_yn"));
		for( int i = 0; i < array_node.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("reqInfo")).put("req_id", array_node.get(i));
			chargerGrpDAO.deleteNodeSubReqAdmin(paramMap.get("reqInfo"));
			chargerGrpDAO.insertSubReqAdmin(paramMap.get("reqInfo"));
			chargerGrpDAO.insertSubReqAdminHistory(paramMap.get("reqInfo"));
		}
		
		resultMap.put("RESULT_MSG", "완료되었습니다.");
		return resultMap;
	}
	@Override 
	public HashMap<String, String> deleteSubReqAdmin(ModelMap paramMap) throws NkiaException {
		boolean checkLogic = true;
		HashMap<String, String> resultMap = new HashMap<String, String>();
		HashMap<String, String> checkMap = new HashMap<String, String>();


		
		// 세부유형 건 삭제
		ArrayList array_node = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("reqInfo")).get("req_id");
		((HashMap<String, Object>) paramMap.get("reqInfo")).put("admin_id", paramMap.get("admin_id"));
		for( int i = 0; i < array_node.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("reqInfo")).put("req_id", array_node.get(i));
			
			chargerGrpDAO.deleteNodeSubReqAdmin(paramMap.get("reqInfo"));
		}
		resultMap.put("RESULT_MSG", "완료 되었습니다.");
		return resultMap;
	}
	@Override
	public List searchChargerAuth(Map paramMap) {
		return chargerGrpDAO.searchChargerAuth(paramMap);
	}

	@Override
	public List searchChargerAuthPop(Map paramMap) {
		return chargerGrpDAO.searchChargerAuthPop(paramMap);
	}

	@Override
	public void insertChargerAuth(Map paramMap) {
		HashMap resultMap = new HashMap();
		ArrayList array_node = (ArrayList) (paramMap.get("proc_auth_id"));
		for( int i = 0; i < array_node.size(); i++ ){
			resultMap.put("proc_auth_id", array_node.get(i).toString());
			resultMap.put("grp_id", paramMap.get("grp_id"));
			resultMap.put("upd_user_id", paramMap.get("upd_user_id"));
			chargerGrpDAO.insertChargerAuth(resultMap);
		}
		//chargerGrpDAO.insertChargerAuth(paramMap);
	}
	
	@Override
	public void deleteChargerAuth(Map paramMap) {
		HashMap resultMap = new HashMap();
		ArrayList array_node = (ArrayList) (paramMap.get("proc_auth_id"));
		for( int i = 0; i < array_node.size(); i++ ){
			resultMap.put("proc_auth_id", array_node.get(i).toString());
			resultMap.put("grp_id", paramMap.get("grp_id"));
			resultMap.put("upd_user_id", paramMap.get("upd_user_id"));
			chargerGrpDAO.deleteChargerAuth(resultMap);
		}
		
	}

}
