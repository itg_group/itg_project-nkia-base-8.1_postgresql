package com.nkia.itg.workflow.charger.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("chargerGrpDAO")
public class ChargerGrpDAO extends EgovComAbstractDAO {
	
	public List searchAllChargerGrpTree(Map paramMap) {
		return this.list("ChargerGrpDAO.searchAllChargerGrpTree", paramMap);
	}
	
	public List searchChargerGrpTree(Map paramMap) {
		return this.list("ChargerGrpDAO.searchChargerGrpTree", paramMap);
	}
	
	public List searchSubReqTree(Map paramMap) {
		return this.list("ChargerGrpDAO.searchSubReqTree", paramMap);
	}
	
	public List searchCustomerTree(Map paramMap) {
		return this.list("ChargerGrpDAO.searchCustomerTree", paramMap);
	}
	
	public HashMap selectChargerGrpInfo(Map paramMap) {
		return (HashMap) selectByPk("ChargerGrpDAO.selectChargerGrpInfo", paramMap);
	}
	
	public void insertChargerGrp(Map paramMap) {
		insert("ChargerGrpDAO.insertChargerGrp", paramMap);
	}
	
	public void deleteChargerGrp(Map paramMap) {
		delete("ChargerGrpDAO.deleteChargerGrp", paramMap);
	}

	public void updateChargerGrp(Map paramMap) {
		update("ChargerGrpDAO.updateChargerGrp", paramMap);
	}

	public void updateUseYn(Map paramMap) {
		update("ChargerGrpDAO.updateUseYn", paramMap);
	}
	
	public void deleteChargerGrpCharger(Map paramMap) {
		delete("ChargerGrpDAO.deleteChargerGrpCharger", paramMap);
	}
	
	public void deleteChargerUsers(Map paramMap) {
		delete("ChargerGrpDAO.deleteChargerUsers", paramMap);
	}
	
	public void deleteChargerToReq(Map paramMap) {
		delete("ChargerGrpDAO.deleteChargerToReq", paramMap);
	}
	
	public void deleteChargerToCustomer(Map paramMap) {
		delete("ChargerGrpDAO.deleteChargerToCustomer", paramMap);
	}

	public List searchDeptUser(Map paramMap) {
		return this.list("ChargerGrpDAO.searchDeptUser", paramMap);
	}

	public String selectGrpId() {
		return (String) selectByPk("ChargerGrpDAO.selectGrpId", null);
	}
	
	public void insertChargerGrpCharger(Map paramMap) {
		insert("ChargerGrpDAO.insertChargerGrpCharger", paramMap);
	}

	public int searchGrpUserListCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerGrpDAO.searchGrpUserListCount", paramMap);
	}

	public List searchGrpUserList(Map paramMap) {
		return this.list("ChargerGrpDAO.searchGrpUserList", paramMap);
	}
	
	public int searchGrpUserListTotalCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerGrpDAO.searchGrpUserListTotalCount", paramMap);
	}

	public List searchGrpUserListTotal(Map paramMap) {
		return this.list("ChargerGrpDAO.searchGrpUserListTotal", paramMap);
	}

	
	public List searchTreeDataGroup(Map paramMap) {
		return this.list("ChargerGrpDAO.searchTreeDataGroup", paramMap);
	}

	public List searchTreeDataUser(Map paramMap) {
		return this.list("ChargerGrpDAO.searchTreeDataUser", paramMap);
	}

	public int searchDeptUserGridListCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerGrpDAO.searchDeptUserGridListCount", paramMap);
	}

	public List searchDeptUserGridList(Map paramMap) {
		return this.list("ChargerGrpDAO.searchDeptUserGridList", paramMap);
	}

	public List searchStaticAllDeptTree(Map paramMap) {
		return this.list("ChargerGrpDAO.searchStaticAllDeptTree", paramMap);
	}
	
	public int searchGrpDeptListCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerGrpDAO.searchGrpDeptListCount", paramMap);
	}

	public List searchGrpDeptList(Map paramMap) {
		return this.list("ChargerGrpDAO.searchGrpDeptList", paramMap);
	}
	
	public int searchGrpUserGrpListCount(Map paramMap) {
		return (Integer) this.selectByPk("ChargerGrpDAO.searchGrpUserGrpListCount", paramMap);
	}

	public List searchGrpUserGrpList(Map paramMap) {
		return this.list("ChargerGrpDAO.searchGrpUserGrpList", paramMap);
	}
	public void deleteReq(Object object) throws NkiaException {
		delete("ChargerGrpDAO.deleteReq", object);
	}
	
	public void deleteCustomer(Object object) throws NkiaException {
		delete("ChargerGrpDAO.deleteCustomer", object);
	}
	
	public void insertReq(Object object) throws NkiaException {
		this.insert("ChargerGrpDAO.insertReq", object);
	}
	
	public void insertCustomer(Object object) throws NkiaException {
		this.insert("ChargerGrpDAO.insertCustomer", object);
	}
	
	public int searchManagerListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("ChargerGrpDAO.searchManagerListCount", paramMap);
	}

	public List searchManagerList(ModelMap paramMap) throws NkiaException {
		return list("ChargerGrpDAO.searchManagerList", paramMap);
	}
	
	public int searchSubReqCustomerListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("ChargerGrpDAO.searchSubReqCustomerListCount", paramMap);
	}

	public List searchSubReqCustomerList(ModelMap paramMap) throws NkiaException {
		return list("ChargerGrpDAO.searchSubReqCustomerList", paramMap);
	}
	
	public int searchUnSubReqCustomerListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("ChargerGrpDAO.searchUnSubReqCustomerListCount", paramMap);
	}

	public List searchUnSubReqCustomerList(ModelMap paramMap) throws NkiaException {
		return list("ChargerGrpDAO.searchUnSubReqCustomerList", paramMap);
	}
	public int checkReq(Object object) {
		return (Integer) this.selectByPk("ChargerGrpDAO.checkReq", object);
	}
	public int checkCustomer(Object object) {
		return (Integer) this.selectByPk("ChargerGrpDAO.checkCustomer", object);
	}
	public String resultString(Object object) {
		return (String) selectByPk("ChargerGrpDAO.resultString", object);
	}
	public List searchReqTypeList(ModelMap paramMap) throws NkiaException {
		return list("ChargerGrpDAO.searchReqTypeList", paramMap);
	}
	public List subReqAdminList(HashMap paramMap) throws NkiaException {
		return list("ChargerGrpDAO.subReqAdminList", paramMap);
	}
	public void deleteSubReqAdmin(Object object) throws NkiaException {
		delete("ChargerGrpDAO.deleteSubReqAdmin", object);
	}
	public void insertSubReqAdmin(Object object) throws NkiaException {
		this.insert("ChargerGrpDAO.insertSubReqAdmin", object);
	}
	public void insertSubReqAdminHistory(Object object) throws NkiaException {
		this.insert("ChargerGrpDAO.insertSubReqAdminHistory", object);
	}

	public void deleteNodeSubReqAdmin(Object object) throws NkiaException {
		delete("ChargerGrpDAO.deleteNodeSubReqAdmin", object);
	}
	public List searchChargerAuth(Map paramMap) {
		return this.list("ChargerGrpDAO.searchChargerAuth", paramMap);
	}
	public List searchChargerAuthPop(Map paramMap) {
		return this.list("ChargerGrpDAO.searchChargerAuthPop", paramMap);
	}
	public void insertChargerAuth(Map paramMap) {
		insert("ChargerGrpDAO.insertChargerAuth", paramMap);
	}
	public void deleteChargerAuth(Map paramMap) {
		delete("ChargerGrpDAO.deleteChargerAuth", paramMap);
	}
	public void insertReqHistory(Map paramMap) {
		insert("ChargerGrpDAO.insertReqHistory", paramMap);
	}
}
