/*
 * @(#)WorkCheckResultDAO.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckResult.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workCheckResultDAO")
public class WorkCheckResultDAO extends EgovComAbstractDAO{

	
	public List<Map<String, Object>> selectWorkCheckResultMonths(Map<String, Object> paramMap) throws Exception{
		return list("workCheckResultDAO.selectWorkCheckResultMonths", paramMap);
	}
	
	public int selectWorkCheckResultCnt(ModelMap paramMap) throws Exception {
		return (Integer)selectByPk("workCheckResultDAO.selectWorkCheckResultCnt", paramMap);
	}	
	
	public List<Map<String, Object>> selectWorkCheckResultList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckResultDAO.selectWorkCheckResultList", paramMap);
	}
	
	public void insertAutoWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckResultDAO.insertAutoWorkCheckResultInfo", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckNoResultList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckResultDAO.selectWorkCheckNoResultList", paramMap);
	}

	public Map selectWorkCheckAtchInfo(ModelMap paramMap) throws Exception {
		return (Map) this.selectByPk("workCheckResultDAO.selectWorkCheckAtchInfo", paramMap);
	}
	
	public void insertWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckResultDAO.insertWorkCheckResultInfo", paramMap);
	}	
	
	public void updateWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckResultDAO.updateWorkCheckResultInfo", paramMap);
	}	
	
	public void deleteWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckResultDAO.deleteWorkCheckResultInfo", paramMap);
	}	
	
	public int checkWorkCheckAtchInfo(ModelMap paramMap) throws Exception {
		return (Integer)selectByPk("workCheckResultDAO.checkWorkCheckAtchInfo", paramMap);
	}
	
	public void insertWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckResultDAO.insertWorkCheckAtchInfo", paramMap);
	}	
	
	public void updateWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckResultDAO.updateWorkCheckAtchInfo", paramMap);
	}		

	public void deleteWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckResultDAO.deleteWorkCheckAtchInfo", paramMap);
	}		

	public List<Map<String, Object>> selectWorkCheckCategory(Map<String, Object> paramMap) throws Exception{
		return list("workCheckResultDAO.selectWorkCheckCategory", paramMap);
	}	
}

