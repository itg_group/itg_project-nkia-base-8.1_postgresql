/*
 * @(#)workCheckListResultService.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckResult.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

public interface WorkCheckResultService {

	public List<Map<String, Object>> selectWorkCheckResultMonths(Map<String, Object> paramMap) throws Exception;
	
	public boolean selectUserInfo(ModelMap paramMap) throws Exception;
	
	public int selectWorkCheckResultCnt(ModelMap paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckResultList(Map<String, Object> paramMap) throws Exception;
	
	public void insertAutoWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckNoResultList(Map<String, Object> paramMap) throws Exception;
				
	public Map selectWorkCheckAtchInfo(ModelMap paramMap) throws Exception;
	
	public void insertWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception;
	
	public void deleteWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception;
	
	public int checkWorkCheckAtchInfo(ModelMap paramMap) throws Exception;
	
	public void insertWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception;	
	
	public void deleteWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception;	
	
	public List<Map<String, Object>> selectWorkCheckCategory(Map<String, Object> paramMap) throws Exception;
	
}
