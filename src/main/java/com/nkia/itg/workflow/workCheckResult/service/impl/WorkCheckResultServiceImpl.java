/*
 * @(#)workCheckResultServiceImpl.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckResult.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckResult.dao.WorkCheckResultDAO;
import com.nkia.itg.workflow.workCheckResult.service.WorkCheckResultService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

import org.springframework.ui.ModelMap;

@Service("workCheckResultService")
public class WorkCheckResultServiceImpl implements WorkCheckResultService{
	
	private static final Logger logger = LoggerFactory.getLogger(WorkCheckResultServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "workCheckResultDAO")
	public WorkCheckResultDAO workCheckResultDAO;
	
	
	public List<Map<String, Object>> selectWorkCheckResultMonths(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();			
		}	
		return workCheckResultDAO.selectWorkCheckResultMonths(paramMap);
	}		
	
	public boolean selectUserInfo(ModelMap paramMap) throws Exception {		
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		
		List userGrpAuth = null;		
		UserVO userVO = null;		
		String userAuth = "";
		Boolean result = false;
		
		
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();			
		userGrpAuth = userVO.getGrpAuthList();
			
			
		for(int j=0; j<userGrpAuth.size(); j++){
			Map userGrpAuthMap =(HashMap)userGrpAuth.get(j);
			userAuth = (String) userGrpAuthMap.get("USER_GRP_ID");
			if(userAuth != null && !userAuth.isEmpty()){
				
				if(userAuth.equals("NKIA_ADMIN")){					
					result = true;				
				}
			}
		}		
			
		return result;
	}	

	
	public int selectWorkCheckResultCnt(ModelMap paramMap) throws Exception {
		return workCheckResultDAO.selectWorkCheckResultCnt(paramMap);
	}
	
	public void insertAutoWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());			
		}		
		
		workCheckResultDAO.insertAutoWorkCheckResultInfo(paramMap);
	}	
	
	public List<Map<String, Object>> selectWorkCheckResultList(Map<String, Object> paramMap) throws Exception {		
		return workCheckResultDAO.selectWorkCheckResultList(paramMap);
	}	
	
	public List<Map<String, Object>> selectWorkCheckNoResultList(Map<String, Object> paramMap) throws Exception {
		return workCheckResultDAO.selectWorkCheckNoResultList(paramMap);
	}
	
	public Map selectWorkCheckAtchInfo(ModelMap paramMap) throws Exception {		
		
		Map atchInfo = workCheckResultDAO.selectWorkCheckAtchInfo(paramMap);
		
		return atchInfo;
	}		
	
	public void insertWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());			
		}		
		
		workCheckResultDAO.insertWorkCheckResultInfo(paramMap);
	}	

	public void updateWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}		
		
		workCheckResultDAO.updateWorkCheckResultInfo(paramMap);
	}		
	
	public void deleteWorkCheckResultInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}		
		
		workCheckResultDAO.deleteWorkCheckResultInfo(paramMap);
	}		
	
	public int checkWorkCheckAtchInfo(ModelMap paramMap) throws Exception {
		return workCheckResultDAO.checkWorkCheckAtchInfo(paramMap);
	}	
	
	public void insertWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}		
		
		workCheckResultDAO.insertWorkCheckAtchInfo(paramMap);
	}	

	public void updateWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}		
		
		workCheckResultDAO.updateWorkCheckAtchInfo(paramMap);
	}	
	
	public void deleteWorkCheckAtchInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}		
		
		workCheckResultDAO.deleteWorkCheckAtchInfo(paramMap);
	}			
	
	public List<Map<String, Object>> selectWorkCheckCategory(Map<String, Object> paramMap) throws Exception {		
		return workCheckResultDAO.selectWorkCheckCategory(paramMap);
	}		
	
}