/*
 * @(#)WorkCheckResultController.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckResult.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.dept.service.DeptService;
import com.nkia.itg.system.dept.web.DeptController;
import com.nkia.itg.workflow.workCheckResult.service.WorkCheckResultService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkCheckResultController {
	
	@Resource(name = "workCheckResultService")
	private WorkCheckResultService workCheckResultService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "deptService")
	private DeptService deptService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 체크리스트 결과 관리
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workCheckResult/goWorkCheckResult.do")
	public String goWorkCheckResult(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		
		String mode = StringUtil.parseString(reqMap.get("mode"));
		paramMap.put("mode", mode);
		
		String pop_view_type = StringUtil.parseString(request.getParameter("pop_view_type"));		
		paramMap.put("pop_view_type", pop_view_type);
		
		String yyyy = StringUtil.parseString(request.getParameter("yyyy"));
		paramMap.put("yyyy", yyyy);
		
		String mm = StringUtil.parseString(request.getParameter("mm"));
		paramMap.put("mm", mm);
		
		String target_cust_id = StringUtil.parseString(request.getParameter("target_cust_id"));
		paramMap.put("target_cust_id", target_cust_id);
		
		if(!"".equals(target_cust_id)){
			paramMap.put("cust_id", target_cust_id);
			HashMap custInfo = deptService.searchDeptBasicInfo(paramMap);
			
			paramMap.put("target_cust_nm", StringUtil.parseString(custInfo.get("CUST_NM")));
		}
		
		String forwarPage = "/itg/workflow/workCheckResult/workCheckResult.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
			
	
	/**
	 * 체크리스트 달력목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckResult/selectWorkCheckResultMonths.do")
	public @ResponseBody ResultVO selectWorkCheckResultMonths(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = workCheckResultService.selectWorkCheckResultMonths(paramMap);				
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		
	
	/**
	 * 유저정보 반환
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/workCheckResult/selectUserInfo.do")
	public @ResponseBody ResultVO selectUserInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
			
		try {							
			resultVO.setResultBoolean(workCheckResultService.selectUserInfo(paramMap));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}	
	
	/**
	 * 체크리스트 결과목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckResult/selectWorkCheckResultList.do")
	public @ResponseBody ResultVO selectWorkCheckResultList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = null;
			int resultCnt =0;
			resultCnt = workCheckResultService.selectWorkCheckResultCnt(paramMap);

			if(resultCnt<=0){
				workCheckResultService.insertAutoWorkCheckResultInfo(paramMap);
			}
			
			resultList = workCheckResultService.selectWorkCheckResultList(paramMap);
							
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		
		
	
	/**
	 * 체크리스트 의견,첨부파일 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/workCheckResult/selectWorkCheckAtchInfo.do")
	public @ResponseBody ResultVO selectWorkCheckAtchInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) workCheckResultService.selectWorkCheckAtchInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
				

	/**
	 * 체크리스트 결과목록 등록
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckResult/insertWorkCheckResultInfo.do")
	public @ResponseBody ResultVO insertWorkCheckResultInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();

		try {			
			
			workCheckResultService.insertWorkCheckResultInfo(paramMap);						
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}			
	
	
	/**
	 * 체크리스트 결과목록 수정
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckResult/updateWorkCheckResultInfo.do")
	public @ResponseBody ResultVO updateWorkCheckResultInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();

		try {			
			
			workCheckResultService.updateWorkCheckResultInfo(paramMap);						
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	/**
	 * 체크리스트 결과목록 삭제
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckResult/deleteWorkCheckResultInfo.do")
	public @ResponseBody ResultVO deleteWorkCheckResultInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();

		try {			
			
			workCheckResultService.deleteWorkCheckResultInfo(paramMap);
			workCheckResultService.deleteWorkCheckAtchInfo(paramMap);	
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}			
	
	/**
	 * 체크리스트 의견,첨부파일 등록/수정
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckResult/mergeWorkCheckAtchInfo.do")
	public @ResponseBody ResultVO insertWorkCheckAtchInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		int atchCnt = 0;
		try {			
			
			atchCnt = workCheckResultService.checkWorkCheckAtchInfo(paramMap);
			
			if(atchCnt>0){
				workCheckResultService.updateWorkCheckAtchInfo(paramMap);
			}else{
				workCheckResultService.insertWorkCheckAtchInfo(paramMap);
			}
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	

	/**
	 * 체크리스트 결과목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckResult/selectWorkCheckCategory.do")
	public @ResponseBody ResultVO selectWorkCheckCategory(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = null;
			
			resultList = workCheckResultService.selectWorkCheckCategory(paramMap);
							
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		
		
	/**
	 * 점검결과등록 단계에서 [등록] 버튼 눌렀을 때, 미입력한 점검결과가 있는지 체크하는 메소드
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckResult/selectWorkChkResultNoResult.do")
	public @ResponseBody ResultVO selectWorkChkResultNoResult(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
			
		try {				
			List resultList = null;
			int resultCnt =0;
			resultCnt = workCheckResultService.selectWorkCheckResultCnt(paramMap);

			if(resultCnt<=0){
				workCheckResultService.insertAutoWorkCheckResultInfo(paramMap);
			}
			
			resultList = workCheckResultService.selectWorkCheckResultList(paramMap);
							
			int cnt = 0;
			if(resultList.size() > 0){
				for(int i=0; i<resultList.size(); i++){
					HashMap tmpMap = (HashMap) resultList.get(i);
					if("00".equals(StringUtil.parseString(tmpMap.get("RESULT_CD"))) || "".equals(StringUtil.parseString(tmpMap.get("RESULT_CD")))) {
						cnt ++;
						break;
					}
				}
			}
			
			resultVO.setResultInt(cnt);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}
}
