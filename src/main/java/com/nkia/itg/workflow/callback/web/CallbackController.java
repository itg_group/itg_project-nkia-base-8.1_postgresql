package com.nkia.itg.workflow.callback.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.callback.service.CallbackService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class CallbackController {
	
	@Resource(name = "callbackService")
	private CallbackService callbackService;	
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	/**
	 * 부서관리페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/calback/goCallbackManager.do")
	public String goProcessWorkStateManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/workflow/calback/callbackManager.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/workflow/callback/searchCallBackList.do")
	public @ResponseBody ResultVO searchCallBackList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			
			int totalCount = callbackService.searchCallBackListCount(paramMap);
			List resultList = callbackService.searchCallBackList(paramMap);
			
			GridVO gridVO = new GridVO();
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/process/sendSoftPhone.do")
	public @ResponseBody ResultVO sendSoftPhone(@RequestBody ModelMap paramMap, HttpServletRequest request)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			callbackService.sendSoftPhone(paramMap, userVO, request.getRemoteAddr());
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/workflow/callback/updateCallBackYn.do")
	public @ResponseBody ResultVO updateCallBackYn(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			callbackService.updateCallBackYn(paramMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
