package com.nkia.itg.workflow.callback.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("callbackDAO")
public class CallbackDAO extends EgovComAbstractDAO{
	public List searchCallBackList(Map paramMap) {
		return list("CallbackDAO.searchCallBackList", paramMap);
	}

	public int searchCallBackListCount(Map paramMap) {
		return (Integer)selectByPk("CallbackDAO.searchCallBackListCount", paramMap);
	}

	public Map selectCallBackDetail(Map paramMap) {
		return (Map)selectByPk("CallbackDAO.selectCallBackDetail", paramMap);
	}

	public Map selectUserFromHpNo(String hpNo) {
		return (Map)selectByPk("CallbackDAO.selectUserFromHpNo", hpNo);
	}

	public void insertCallback(Map insertMap) {
		insert("CallbackDAO.insertCallback", insertMap);
	}
	
	public void updateCallBackYn(Map paramMap) {
		insert("CallbackDAO.updateCallBackYn", paramMap);
	}
}
