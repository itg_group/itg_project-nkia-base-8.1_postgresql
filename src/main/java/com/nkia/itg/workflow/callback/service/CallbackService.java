package com.nkia.itg.workflow.callback.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;


public interface CallbackService {

	public List searchCallBackList(Map paramMap) throws NkiaException;

	public int searchCallBackListCount(Map paramMap) throws NkiaException;

	public Map selectCallBackDetail(Map paramMap) throws NkiaException;

	public void sendSoftPhone(Map paramMap, UserVO userVO, String ip) throws NkiaException;

	public void insertCallback(Map insertMap) throws NkiaException;
	
	public void updateCallBackYn(Map paramMap) throws NkiaException;

}
