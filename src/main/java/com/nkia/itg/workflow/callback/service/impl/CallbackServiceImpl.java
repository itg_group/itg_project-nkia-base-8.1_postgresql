package com.nkia.itg.workflow.callback.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.callback.dao.CallbackDAO;
import com.nkia.itg.workflow.callback.service.CallbackService;

@Service("callbackService")
public class CallbackServiceImpl implements CallbackService {
	
	@Resource(name = "callbackDAO")
	public CallbackDAO callbackDAO;
	
	@Override
	public List searchCallBackList(Map paramMap) throws NkiaException {
		return callbackDAO.searchCallBackList(paramMap);
	}

	@Override
	public int searchCallBackListCount(Map paramMap) throws NkiaException {
		return callbackDAO.searchCallBackListCount(paramMap);
	}

	@Override
	public Map selectCallBackDetail(Map paramMap) throws NkiaException {
		return callbackDAO.selectCallBackDetail(paramMap);
	}

//	@Override
//	public void sendSoftPhone(Map paramMap, UserVO userVO, String remoteIp) throws NkiaException {
//		String telNo = (String)paramMap.get("hp_no");
//		SoftPhoneManager provider = new SoftPhoneManager();
//		provider.sendCallback(telNo, userVO, remoteIp);
//	}

	@Override
	public void insertCallback(Map insertMap) throws NkiaException {
		String inBoundId = (String)insertMap.get("in_bound_id");
		String hpNo = (String)insertMap.get("hp_no");
		String sdeskUserId = (String)insertMap.get("sdesk_user_id");
		
		Map userInfo = callbackDAO.selectUserFromHpNo(hpNo);
		callbackDAO.insertCallback(insertMap);
	}
	
	@Override
	public void updateCallBackYn(Map paramMap) throws NkiaException {
		callbackDAO.updateCallBackYn(paramMap);
	}

	@Override
	public void sendSoftPhone(Map paramMap, UserVO userVO, String ip)
			throws NkiaException {
		// TODO Auto-generated method stub
		
	}

}
