package com.nkia.itg.workflow.template.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.workflow.template.dao.TemplateDAO;
import com.nkia.itg.workflow.template.service.TemplateService;

@Service("templateService")
public class TemplateServiceImpl implements TemplateService{

	@Resource(name="templateDAO")
	private TemplateDAO templateDAO;
	
	
	public List searchTemplateList(ModelMap paramMap) throws NkiaException {

		return templateDAO.searchTemplateList(paramMap);
	}
	
}
