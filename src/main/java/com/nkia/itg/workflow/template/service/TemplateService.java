package com.nkia.itg.workflow.template.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface TemplateService {

	public List searchTemplateList(ModelMap paramMap) throws NkiaException;

}
