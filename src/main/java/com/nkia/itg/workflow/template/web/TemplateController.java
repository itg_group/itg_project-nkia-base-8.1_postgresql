package com.nkia.itg.workflow.template.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.workflow.template.service.TemplateService;

@Controller
public class TemplateController {

	@Resource(name = "templateService")
	private TemplateService templateService;
	
	/**
	 * SMS 그룹에 등록된 사용자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/workflow/template/searchTemplateList.do")
	public @ResponseBody
	ResultVO searchSmsGrpUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;

		try {
			resultList = templateService.searchTemplateList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}
	
}
