package com.nkia.itg.workflow.template.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("templateDAO")
public class TemplateDAO extends EgovComAbstractDAO{

	public List searchTemplateList(ModelMap paramMap) throws NkiaException {
		return list("templateDAO.searchTemplateList", paramMap);
	}


}
