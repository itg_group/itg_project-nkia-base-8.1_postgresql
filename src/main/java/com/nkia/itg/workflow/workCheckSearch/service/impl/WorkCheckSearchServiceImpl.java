/*
 * @(#)workCheckSearchServiceImpl.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckSearch.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckSearch.dao.WorkCheckSearchDAO;
import com.nkia.itg.workflow.workCheckSearch.service.WorkCheckSearchService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("workCheckSearchService")
public class WorkCheckSearchServiceImpl implements WorkCheckSearchService{
	
	private static final Logger logger = LoggerFactory.getLogger(WorkCheckSearchServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "workCheckSearchDAO")
	public WorkCheckSearchDAO workCheckSearchDAO;
	
	
	public List<Map<String, Object>> selectWorkCheckResultMonths(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		}	
		return workCheckSearchDAO.selectWorkCheckResultMonths(paramMap);
	}		
	
	public int selectWorkCheckResultCnt(ModelMap paramMap) throws Exception {
		return workCheckSearchDAO.selectWorkCheckResultCnt(paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckResultList(Map<String, Object> paramMap) throws Exception {
		return workCheckSearchDAO.selectWorkCheckResultList(paramMap);
	}	
	
	public List<Map<String, Object>> selectWorkCheckNoResultList(Map<String, Object> paramMap) throws Exception {
		return workCheckSearchDAO.selectWorkCheckNoResultList(paramMap);
	}
	
	public Map selectWorkCheckAtchInfo(ModelMap paramMap) throws Exception {		
		
		Map atchInfo = workCheckSearchDAO.selectWorkCheckAtchInfo(paramMap);
		
		return atchInfo;
	}		
	
		
	
}