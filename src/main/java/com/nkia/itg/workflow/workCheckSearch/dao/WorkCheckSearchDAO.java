/*
 * @(#)WorkCheckSearchDAO.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckSearch.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workCheckSearchDAO")
public class WorkCheckSearchDAO extends EgovComAbstractDAO{

	
	public List<Map<String, Object>> selectWorkCheckResultMonths(Map<String, Object> paramMap) throws Exception{
		return list("workCheckSearchDAO.selectWorkCheckResultMonths", paramMap);
	}
	
	public int selectWorkCheckResultCnt(ModelMap paramMap) throws Exception {
		return (Integer)selectByPk("workCheckSearchDAO.selectWorkCheckResultCnt", paramMap);
	}	
	
	public List<Map<String, Object>> selectWorkCheckResultList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckSearchDAO.selectWorkCheckResultList", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckNoResultList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckSearchDAO.selectWorkCheckNoResultList", paramMap);
	}

	public Map selectWorkCheckAtchInfo(ModelMap paramMap) throws Exception {
		return (Map) this.selectByPk("workCheckSearchDAO.selectWorkCheckAtchInfo", paramMap);
	}
	

	
}

