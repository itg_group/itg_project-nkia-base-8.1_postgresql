/*
 * @(#)workCheckListSearchService.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckSearch.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

public interface WorkCheckSearchService {

	public List<Map<String, Object>> selectWorkCheckResultMonths(Map<String, Object> paramMap) throws Exception;
	
	public int selectWorkCheckResultCnt(ModelMap paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckResultList(Map<String, Object> paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckNoResultList(Map<String, Object> paramMap) throws Exception;
				
	public Map selectWorkCheckAtchInfo(ModelMap paramMap) throws Exception;

	
}
