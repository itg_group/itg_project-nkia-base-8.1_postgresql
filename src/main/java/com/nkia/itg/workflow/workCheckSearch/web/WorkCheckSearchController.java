/*
 * @(#)WorkCheckSearchController.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckSearch.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckSearch.service.WorkCheckSearchService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkCheckSearchController {
	
	@Resource(name = "workCheckSearchService")
	private WorkCheckSearchService workCheckSearchService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 체크리스트 결과 관리
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workCheckSearch/goWorkCheckSearch.do")
	public String goWorkCheckSearch(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		String forwarPage = "/itg/workflow/workCheckSearch/workCheckSearch.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
			
	
	/**
	 * 체크리스트 달력목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckSearch/selectWorkCheckResultMonths.do")
	public @ResponseBody ResultVO selectWorkCheckResultMonths(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = workCheckSearchService.selectWorkCheckResultMonths(paramMap);				
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		
	
	
	/**
	 * 체크리스트 결과목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckSearch/selectWorkCheckResultList.do")
	public @ResponseBody ResultVO selectWorkCheckResultList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = null;
			int resultCnt =0;
			resultCnt = workCheckSearchService.selectWorkCheckResultCnt(paramMap);

			if(resultCnt>0){
				resultList = workCheckSearchService.selectWorkCheckResultList(paramMap);
			}else{
				resultList = workCheckSearchService.selectWorkCheckNoResultList(paramMap);
			}
							
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		
		
	
	/**
	 * 체크리스트 의견,첨부파일 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/workCheckSearch/selectWorkCheckAtchInfo.do")
	public @ResponseBody ResultVO selectWorkCheckAtchInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) workCheckSearchService.selectWorkCheckAtchInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
				

	

	
	
	
}
