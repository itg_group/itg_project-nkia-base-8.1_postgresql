/*
 * @(#)WorkReqDocDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workReqDoc.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workReqDocDAO")
public class WorkReqDocDAO extends EgovComAbstractDAO{

	public Map<String, Object> searchWorkReqDoc(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("workReqDocDAO.searchWorkReqDoc", paramMap);
	}
	
	public List<Map<String, Object>> searchWorkReqDocList(Map<String, Object> paramMap) throws NkiaException{
		return list("workReqDocDAO.searchWorkReqDocList", paramMap);
	}
	
	public int searchWorkReqDocCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("workReqDocDAO.searchWorkReqDocCount", paramMap);
	}
	
	public String selectModelNewId(Map<String, Object> param) throws NkiaException {
		return (String)selectByPk("workReqDocDAO.selectWorkReqDocNewId", param);
	}
	
	/*public void insertWorkReqDoc(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqDocDAO.insertWorkReqDoc", paramMap);
	}
	
	public void updateWorkReqDoc(Map<String, Object> paramMap) throws NkiaException {
		this.update("workReqDocDAO.updateWorkReqDoc", paramMap);
	}
	
	public void deleteWorkReqDoc(Map<String, Object> paramMap) throws NkiaException {
		this.delete("workReqDocDAO.deleteWorkReqDoc", paramMap);
	}*/
	
	public List searchWorkReqDocListTree(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("workReqDocDAO.searchWorkReqDocListTree", paramMap);
		return resultList;
	}
	
	public Map<String, Object> selectWorkReqDoc(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("workReqDocDAO.selectWorkReqDoc", paramMap);
	}
	
	public List selectWorkReqDocGrid(ModelMap paramMap) throws NkiaException{
		return list("workReqDocDAO.selectWorkReqDocGrid", paramMap);
	}

	public int selectWorkReqDocGridCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("workReqDocDAO.selectWorkReqDocGridCount", paramMap);
	}
	
	public void insertWorkReqDocCustomer(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqDocDAO.insertWorkReqDocCustomer", paramMap);
	}
	
	public void updateWorkReqDocCustomer(Map<String, Object> paramMap) throws NkiaException {
		this.update("workReqDocDAO.updateWorkReqDocCustomer", paramMap);
	}
	
	public Map<String, Object> selectWorkReqDocCustomer(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("workReqDocDAO.selectWorkReqDocCustomer", paramMap);
	}
	
	public void deleteWorkReqDocCustomer(Map<String, Object> paramMap) throws NkiaException {
		this.delete("workReqDocDAO.deleteWorkReqDocCustomer", paramMap);
	}
	
	public int selectWorkReqDocOrderGridCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer) this.selectByPk("workReqDocDAO.selectWorkReqDocOrderGridCount", paramMap);
		return count;
	}
	
	public List selectWorkReqDocOrderGrid(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("workReqDocDAO.selectWorkReqDocOrderGrid", paramMap);
		return resultList;
	}
	
	public HashMap selectWorkReqDocBasicInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("workReqDocDAO.selectWorkReqDocBasicInfo", paramMap);
	}
	
	public void insertWorkReqDoc(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqDocDAO.insertWorkReqDoc", paramMap);
	}
	
	public void updateWorkReqDoc(Map<String, Object> paramMap) throws NkiaException {
		this.update("workReqDocDAO.updateWorkReqDoc", paramMap);
	}
	
	public void deleteWorkReqDoc(Map<String, Object> paramMap) throws NkiaException {
		this.delete("workReqDocDAO.deleteWorkReqDoc", paramMap);
	}
	
	public void updateWorkReqDocOrder(Map<String, Object> paramMap) throws NkiaException {
		this.update("workReqDocDAO.updateWorkReqDocOrder", paramMap);
	}
	
	/*public HashMap selectCustomerDeleteList(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("workReqDocDAO.selectCustomerDeleteList", paramMap);
	}*/
	
	public List selectCustomerDeleteList(Map<String, Object> paramMap) throws NkiaException{
		return list("workReqDocDAO.selectCustomerDeleteList", paramMap);
	}
	
	public void deleteCustomerList(Map<String, Object> paramMap) throws NkiaException {
		this.delete("workReqDocDAO.deleteCustomerList", paramMap);
	}
}

