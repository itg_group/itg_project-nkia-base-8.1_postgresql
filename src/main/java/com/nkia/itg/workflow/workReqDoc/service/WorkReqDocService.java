/*
 * @(#)workReqDocService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workReqDoc.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface WorkReqDocService {

	public Map<String, Object> searchWorkReqDoc(Map<String, Object> paramMap) throws Exception;

	public List<Map<String, Object>> searchWorkReqDocList(Map<String, Object> paramMap) throws Exception;

	public int searchWorkReqDocCount(Map<String, Object> paramMap) throws Exception;
	
	/*public void insertWorkReqDoc(Map<String, Object> paramMap) throws NkiaException;
	
	public void updateWorkReqDoc(Map<String, Object> paramMap) throws NkiaException;
	
	public void deleteWorkReqDoc(Map<String, Object> paramMap) throws NkiaException;*/
	
	public List<Map<String, Object>> searchWorkReqEvidTrblList(Map<String, Object> paramMap) throws Exception;
	
	public int searchWorkReqEvidTrblCount(Map<String, Object> paramMap) throws Exception;
	
	public List<Map<String, Object>> searchWorkReqEvidPrbmList(Map<String, Object> paramMap) throws Exception;
	
	public int searchWorkReqEvidPrbmCount(Map<String, Object> paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkReqDocChkgroupList(Map<String, Object> paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkReqDocChkList(Map<String, Object> paramMap) throws Exception;
	
	public void insertCheckListGroupInsert(String jsonData) throws Exception;
	
	public void WorkReqDocCheckListUseY(String jsonData) throws Exception;
	
	public void WorkReqDocCheckListUseN(String jsonData) throws Exception;
	
	public void insertWorkReqEvidTrbl(List<HashMap> list) throws Exception;
	
	public void insertWorkReqEvidPrbm(List<HashMap> list) throws Exception;
	
	public void mergeDeptInfo(List<HashMap> list) throws Exception;
	
	public void mergeUserInfo(List<HashMap> list) throws Exception;
	
	public void updateSSLCustId(Map<String, Object> paramMap) throws Exception;
	
	public List searchWorkReqDocListTree(Map paramMap) throws Exception;
	
	public Map<String, Object> selectWorkReqDoc(Map<String, Object> paramMap) throws Exception;
	
	public List selectWorkReqDocGrid(ModelMap paramMap) throws Exception;

	public int selectWorkReqDocGridCount(ModelMap paramMap) throws Exception;
	
	public void insertWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception;
	
	public Map<String, Object> selectWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception;
	
	public void deleteWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception;
	
	public Map selectRequstRegistDefault(Map paramMap) throws Exception;
	
	public int selectWorkReqDocOrderGridCount(ModelMap paramMap) throws Exception;
	
	public List selectWorkReqDocOrderGrid(ModelMap paramMap) throws Exception;
	
	public HashMap selectWorkReqDocBasicInfo(ModelMap paramMap) throws Exception;
	
	public void insertWorkReqDoc(ModelMap paramMap) throws Exception;
	
	public void updateWorkReqDoc(Map<String, Object> paramMap) throws Exception;
	
	public void deleteWorkReqDoc(Map<String, Object> paramMap) throws Exception;
}
