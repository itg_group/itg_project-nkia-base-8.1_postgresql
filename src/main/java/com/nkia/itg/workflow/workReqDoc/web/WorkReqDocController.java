/*
 * @(#)WorkReqDocController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workReqDoc.web;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.user.service.UserService;
import com.nkia.itg.workflow.workReqDoc.service.WorkReqDocService;

import egovframework.com.cmm.EgovMessageSource;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkReqDocController {
	
	@Resource(name = "workReqDocService")
	private WorkReqDocService workReqDocService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "userService")
	private UserService userService;
	
	// 작업요청양식 기본 탭
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkReqDocBaseTab.do")
	public String goWorkReqDocBaseTab(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workReqDocBaseTab.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}

	// 작업요청양식 관리
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkReqDocMng.do")
	public String goWorkReqDocMng(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workReqDocMng.nvf";

		//내부요청인 경우
		//로그인한 사람의 부서를 운영팀으로 기본세팅함
		String inRequestYn = (String) reqMap.get("in_request_yn");
		if(inRequestYn != null && "Y".contains(inRequestYn)){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String LOGIN_USER_ID = userVO.getUser_id();
			String cust_id = userVO.getCust_id();
			reqMap.put("in_cust_id", cust_id);
		}
		
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}

	// 고객사별 양식 관리
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkReqDocMngTree.do")
	public String goWorkReqDocMngTree(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workReqDocMngTree.nvf";

		//내부요청인 경우
		//로그인한 사람의 부서를 운영팀으로 기본세팅함
		String inRequestYn = (String) reqMap.get("in_request_yn");
		if(inRequestYn != null && "Y".contains(inRequestYn)){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String LOGIN_USER_ID = userVO.getUser_id();
			String cust_id = userVO.getCust_id();
			reqMap.put("in_cust_id", cust_id);
		}
		
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 작업요청양식 관리
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkReqDocManager.do")
	public String goWorkReqDocManager(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workReqDocManager.nvf";

		/*//내부요청인 경우
		//로그인한 사람의 부서를 운영팀으로 기본세팅함
		String inRequestYn = (String) reqMap.get("in_request_yn");
		if(inRequestYn != null && "Y".contains(inRequestYn)){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			final String LOGIN_USER_ID = userVO.getUser_id();
			String cust_id = userVO.getCust_id();
			reqMap.put("in_cust_id", cust_id);
		}*/
		
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 작업요청양식 목록
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkReqDocList.do")
	public String goWorkReqDocList(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workReqDocList.nvf";
		
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 작업요청양식 상세
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkReqDocDetail.do")
	public String goWorkReqDocDetail(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workReqDocDetail.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 정기작업 관리
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkRegularMng.do")
	public String goWorkRegularMng(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workRegularMng.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 정기작업 목록
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkRegularList.do")
	public String goWorkRegularList(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workRegularList.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	// 정기작업 상세
	@RequestMapping(value="/itg/workflow/workReqDoc/goWorkRegularDetail.do")
	public String goWorkRegularDetail(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/workflow/workReqDoc/workRegularDetail.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	
	/**
	 * 작업요청양식 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/searchWorkReqDoc.do")
	public @ResponseBody ResultVO searchWorkReqDoc(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Map <String , Object> resultMap = workReqDocService.searchWorkReqDoc(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 리스트 트리 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/searchWorkReqDocListTree.do")
	public @ResponseBody ResultVO searchWorkReqDocListTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = workReqDocService.searchWorkReqDocListTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/searchWorkReqDocList.do")
	public @ResponseBody ResultVO searchWorkReqDocList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = workReqDocService.searchWorkReqDocList(paramMap);		
			totalCount = workReqDocService.searchWorkReqDocCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 작업요청양식 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/searchWorkReqDocExcelDown.do")
	public @ResponseBody ResultVO searchWorkReqDocExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map<String, Object> excelParam = (Map<String, Object>) paramMap.get("excelParam");
			Map<String, Object> param = (Map<String, Object>) paramMap.get("param");
			List<Map<String, Object>> resultList = workReqDocService.searchWorkReqDocList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 - 등록
     * @param paramMap
	 * @return
	 * @
	 *//*		
	@RequestMapping(value="/itg/workflow/workReqDoc/insertWorkReqDoc.do")
	public @ResponseBody ResultVO insertWorkReqDoc(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			workReqDocService.insertWorkReqDoc(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	*//**
	 * 작업요청양식 - 수정 
     * @param paramMap
	 * @return
	 * @
	 *//*	
	@RequestMapping(value="/itg/workflow/workReqDoc/updateWorkReqDoc.do")
	public @ResponseBody ResultVO updateWorkReqDoc(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			workReqDocService.updateWorkReqDoc(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
			resultVO.setResultMap(paramMap);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	*//**
	 * 작업요청양식 -  삭제
     * @param paramMap
	 * @return
	 * @
	 *//*	
	@RequestMapping(value="/itg/workflow/workReqDoc/deleteWorkReqDoc.do")
	public @ResponseBody ResultVO deleteWorkReqDoc(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			workReqDocService.deleteWorkReqDoc(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}*/
	
	/**
	 * 요청근거상세 팝업(장애관리) - 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/searchWorkReqEvidTrblList.do")
	public @ResponseBody ResultVO searchWorkReqEvidTrblList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = workReqDocService.searchWorkReqEvidTrblList(paramMap);		
			totalCount = workReqDocService.searchWorkReqEvidTrblCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 요청근거상세 팝업(문제/개선관리) - 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/searchWorkReqEvidPrbmList.do")
	public @ResponseBody ResultVO searchWorkReqEvidPrbmList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = workReqDocService.searchWorkReqEvidPrbmList(paramMap);		
			totalCount = workReqDocService.searchWorkReqEvidPrbmCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 그룹 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/selectWorkReqDocChkgroupList.do")
	public @ResponseBody ResultVO selectWorkReqDocChkgroupList(@RequestBody ModelMap paramMap) throws NkiaException {
		
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = workReqDocService.selectWorkReqDocChkgroupList(paramMap);
		
			gridVO.setTotalCount(resultList.size());
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 레크리스트 그룹 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/insertCheckListGroupInsert.do", produces = "application/json", consumes = "application/json")
	public @ResponseBody ResultVO insertCheckListGroupInsert(@RequestBody String jsonData, HttpServletRequest request)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			workReqDocService.insertCheckListGroupInsert(jsonData);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/selectWorkReqDocChkList.do")
	public @ResponseBody ResultVO selectWorkReqDocChkList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = workReqDocService.selectWorkReqDocChkList(paramMap);		
			//totalCount = functionPointService.selectModuleSelPopCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 사용
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/WorkReqDocCheckListUseY.do", produces = "application/json", consumes = "application/json")
	public @ResponseBody ResultVO WorkReqDocCheckListUseYN(@RequestBody String jsonData, HttpServletRequest request)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			workReqDocService.WorkReqDocCheckListUseY(jsonData);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 사용안함
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/WorkReqDocCheckListUseN.do", produces = "application/json", consumes = "application/json")
	public @ResponseBody ResultVO WorkReqDocCheckListUseN(@RequestBody String jsonData, HttpServletRequest request)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			workReqDocService.WorkReqDocCheckListUseN(jsonData);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 트리선택시 요청양식관리 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/selectWorkReqDoc.do")
	public @ResponseBody ResultVO selectWorkReqDoc(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Map <String , Object> resultMap = workReqDocService.selectWorkReqDoc(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 고객사별 요청양식 리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/selectWorkReqDocGrid.do")
	public @ResponseBody ResultVO selectWorkReqDocGrid(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = workReqDocService.selectWorkReqDocGrid(paramMap);		
			totalCount = workReqDocService.selectWorkReqDocGridCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 등록된 데이터 있는지 확인
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/checkWorkReqDocToCustomer.do")
	public @ResponseBody ResultVO checkWorkReqDocToCustomer(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			int totalCount = 0;
			totalCount = workReqDocService.selectWorkReqDocGridCount(paramMap);
						
			resultVO.setResultInt(totalCount);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 - 고객사양식 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/insertWorkReqDocCustomer.do")
	public @ResponseBody ResultVO insertWorkReqDocCustomer(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			workReqDocService.insertWorkReqDocCustomer(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 - 고객사양식 수정
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workReqDoc/updateWorkReqDocCustomer.do")
	public @ResponseBody ResultVO updateWorkReqDocCustomer(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			workReqDocService.updateWorkReqDocCustomer(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업양식조회
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/selectWorkReqDocCustomer.do")
	public @ResponseBody ResultVO selectWorkReqDocCustomer(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			
			Map <String , Object> resultMap = workReqDocService.selectWorkReqDocCustomer(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 -  삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workReqDoc/deleteWorkReqDocCustomer.do")
	public @ResponseBody ResultVO deleteWorkReqDocCustomer(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			workReqDocService.deleteWorkReqDocCustomer(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 요청등록 호출시 요청등록 기본값 세팅
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/selectRequstRegistDefault.do")
	public @ResponseBody ResultVO selectRequstRegistDefault(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		String reqUserId = "";
		try {
			
			//요청내용, 도움말, 완료희망일시, 요청부서승인자
			resultMap = workReqDocService.selectRequstRegistDefault(paramMap);
			
			reqUserId = (String)paramMap.get("req_user_id");
			/*//고객사 정보
			UserVO reqUserVO = userDAO.selectUserInfo(reqUserId);
			
			if(reqUserVO != null) {
				String userDept = reqUserVO.getDetailInfo();				
				resultMap.put("customer_id", reqUserVO.getCustomer_id());
				resultMap.put("customer_nm", reqUserVO.getCustomer_nm());
			}
			//대외고객사의 경우 서비스대상고객사 셋팅
			int foreign_customer = customizeUbitService.selectForeignCustomer(reqUserId);
			String foreign_customer_yn = "N";
			if(foreign_customer>0){
				resultMap.put("service_trget_customer_id", reqUserVO.getCustomer_id());
				resultMap.put("service_trget_customer_id_nm", reqUserVO.getCustomer_nm());
				foreign_customer_yn = "Y";
			}
			resultMap.put("foreign_customer_yn", foreign_customer_yn);*/
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위부서리스트
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/selectWorkReqDocOrderGrid.do")
	public @ResponseBody ResultVO selectWorkReqDocOrderGrid(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			List resultList = workReqDocService.selectWorkReqDocOrderGrid(paramMap);		
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 양식 기본정보
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/selectWorkReqDocBasicInfo.do")
	public @ResponseBody ResultVO selectWorkReqDocBasicInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = workReqDocService.selectWorkReqDocBasicInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업양식 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/insertWorkReqDoc.do")
	public @ResponseBody ResultVO insertWorkReqDoc(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	
	    	workReqDocService.insertWorkReqDoc(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 - 수정 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/updateWorkReqDoc.do")
	public @ResponseBody ResultVO updateWorkReqDoc(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			workReqDocService.updateWorkReqDoc(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
			resultVO.setResultMap(paramMap);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 -  삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/deleteWorkReqDoc.do")
	public @ResponseBody ResultVO deleteWorkReqDoc(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			workReqDocService.deleteWorkReqDoc(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 작업요청양식 - 로그인유저 customer id 확인
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workReqDoc/selectLoginCustomerId.do")
	public @ResponseBody ResultVO selectLoginCustomerId(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if( isAuthenticated ){
				userVO = userService.selectUserInfo(userVO.getUser_id());
			}
			
			Map resultMap = new HashMap();
			
			resultMap.put("customer_id",userVO.getCustomer_id());
			resultMap.put("customer_nm",userVO.getCustomer_nm());
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
