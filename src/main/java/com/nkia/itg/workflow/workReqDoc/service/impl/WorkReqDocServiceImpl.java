/*
 * @(#)workReqDocServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workReqDoc.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.process.dao.ProcessTemplateDAO;
import com.nkia.itg.workflow.workCheckList.dao.WorkCheckListDAO;
import com.nkia.itg.workflow.workReqDoc.dao.WorkReqDocDAO;
import com.nkia.itg.workflow.workReqDoc.dao.WorkReqEvidDAO;
import com.nkia.itg.workflow.workReqDoc.service.WorkReqDocService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("workReqDocService")
public class WorkReqDocServiceImpl implements WorkReqDocService{
	
	private static final Logger logger = LoggerFactory.getLogger(WorkReqDocServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "workReqDocDAO")
	public WorkReqDocDAO workReqDocDAO;

	@Resource(name = "workCheckListDAO")
	public WorkCheckListDAO workCheckListDAO;

	@Resource(name = "processTemplateDAO")
	public ProcessTemplateDAO processTemplateDAO;
	
	@Resource(name = "workReqEvidDAO")
	public WorkReqEvidDAO workReqEvidDAO;
	
	public Map<String, Object> searchWorkReqDoc(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> resultMap = workReqDocDAO.searchWorkReqDoc(paramMap);
		/*List<Map<String, Object>> check_group_cd_arryList = workCheckListDAO.selectWorkReqDocChkgroupList(paramMap);
		String check_group_cd_list = "";
		String check_group_nm_list = "";
		for(Map<String, Object> map: check_group_cd_arryList){
			if(!"".equals(check_group_cd_list)){
				check_group_cd_list += ", ";
				check_group_nm_list += ", ";
			}
			check_group_cd_list += map.get("CHECK_GROUP_CD");
			check_group_nm_list += map.get("CHECK_GROUP_NM");
		}
		resultMap.put("check_group_cd_list", check_group_cd_list);
		resultMap.put("check_group_nm_list", check_group_nm_list);*/
		return resultMap;
	}
	
	public List<Map<String, Object>> searchWorkReqDocList(Map<String, Object> paramMap) throws Exception {
		return workReqDocDAO.searchWorkReqDocList(paramMap);
	}
	
	public int searchWorkReqDocCount(Map<String, Object> paramMap) throws Exception {
		return workReqDocDAO.searchWorkReqDocCount(paramMap);
	}
	
	/*public void insertWorkReqDoc(Map<String, Object> paramMap) throws Exception {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}

		// 요청양식 변경
		String newId = workReqDocDAO.selectModelNewId(paramMap);
		paramMap.put("req_doc_id", newId);
		workReqDocDAO.insertWorkReqDoc(paramMap);
		
		//체크그룹 삭제
		//workCheckListDAO.deleteWorkReqDocChkgroupByReqDocIdList(paramMap);
		
		// 서식 변경
		//processTemplateDAO.deleteTemplateReqDoc(paramMap); // 요청문서관련 서식 삭제
		//processTemplateDAO.insertTemplateReqDoc(paramMap); // 요청문서관련 서식 등록
	}

	public void updateWorkReqDoc(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 요청양식 변경
		workReqDocDAO.updateWorkReqDoc(paramMap);
		
		// 서식 변경
		processTemplateDAO.deleteTemplateReqDoc(paramMap); // 요청문서관련 서식 삭제
		processTemplateDAO.insertTemplateReqDoc(paramMap); // 요청문서관련 서식 등록
	}
	
	public void deleteWorkReqDoc(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		// 요청양식 변경
		workReqDocDAO.deleteWorkReqDoc(paramMap);
		
		//체크그룹 삭제
		//workCheckListDAO.deleteWorkReqDocChkgroupByReqDocIdList(paramMap);

		// 첨부파일 변경
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
		
		// 서식 저변경장
		//processTemplateDAO.deleteTemplateReqDoc(paramMap); // 요청문서관련 서식 등록
	}*/
	
	public List searchWorkReqDocListTree(Map paramMap) throws Exception {
		return workReqDocDAO.searchWorkReqDocListTree(paramMap);
	}	
	
	public Map<String, Object> selectWorkReqDoc(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> resultMap = workReqDocDAO.selectWorkReqDoc(paramMap);

		return resultMap;
	}
	
	@Override
	public List<Map<String, Object>> searchWorkReqEvidTrblList(Map<String, Object> paramMap) throws Exception {
		return workReqEvidDAO.searchWorkReqEvidTrblList(paramMap);
	}

	@Override
	public int searchWorkReqEvidTrblCount(Map<String, Object> paramMap) throws Exception {
		return workReqEvidDAO.searchWorkReqEvidTrblCount(paramMap);
	}

	@Override
	public List<Map<String, Object>> searchWorkReqEvidPrbmList(Map<String, Object> paramMap) throws Exception {
		return workReqEvidDAO.searchWorkReqEvidPrbmList(paramMap);
	}

	@Override
	public int searchWorkReqEvidPrbmCount(Map<String, Object> paramMap) throws Exception {
		return workReqEvidDAO.searchWorkReqEvidPrbmCount(paramMap);
	}

	@Override
	public void insertWorkReqEvidTrbl(List<HashMap> list) throws Exception, SQLException {
		
		if(list.size() > 0) {
			Map<String, Object> paramMap = new HashMap();
			//가져온 데이터를 insert하기전에 모두 지운다
			workReqEvidDAO.deleteWorkReqEvidTrbl(paramMap);
			
			workReqEvidDAO.getSqlMapClient().startBatch();
			workReqEvidDAO.getSqlMapClient().startTransaction();
			
			for (int i = 0; i < list.size(); i++) {
				HashMap vo = list.get(i);
				workReqEvidDAO.getSqlMapClient().insert("workReqEvidDAO.insertWorkReqEvidTrbl", vo);
			}
		}
		workReqEvidDAO.getSqlMapClient().executeBatch();
		workReqEvidDAO.getSqlMapClient().commitTransaction();
		workReqEvidDAO.getSqlMapClient().endTransaction();
		
	}

	@Override
	public void insertWorkReqEvidPrbm(List<HashMap> list) throws Exception, SQLException {
		
		if(list.size() > 0) {
			Map<String, Object> paramMap = new HashMap();
			//가져온 데이터를 insert하기전에 모두 지운다
			workReqEvidDAO.deleteWorkReqEvidPrbm(paramMap);
			
			workReqEvidDAO.getSqlMapClient().startBatch();
			workReqEvidDAO.getSqlMapClient().startTransaction();
			
			for (int i = 0; i < list.size(); i++) {
				HashMap vo = list.get(i);
				workReqEvidDAO.getSqlMapClient().insert("workReqEvidDAO.insertWorkReqEvidPrbm", vo);
			}
		}
		workReqEvidDAO.getSqlMapClient().executeBatch();
		workReqEvidDAO.getSqlMapClient().commitTransaction();
		workReqEvidDAO.getSqlMapClient().endTransaction();
		
	}

	@Override
	public List<Map<String, Object>> selectWorkReqDocChkgroupList(Map<String, Object> paramMap) throws Exception {
		List<Map<String, Object>> check_group_cd_arryList = workCheckListDAO.selectWorkReqDocChkgroupList(paramMap);
		return check_group_cd_arryList;
	}

	@Override
	public void insertCheckListGroupInsert(String jsonData) throws Exception {
		UserVO userVO = new UserVO();
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		}
		JSONArray jsonArray = JSONArray.fromObject(jsonData);
		
		for (int i = 0; i < jsonArray.size(); i++) {
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			JSONObject obj = JSONObject.fromObject(jsonArray.get(i));
			paramMap.put("req_doc_id", obj.get("REQ_DOC_ID"));
			paramMap.put("check_group_cd", obj.get("CHECK_GROUP_CD"));
			paramMap.put("login_user_id", userVO.getUser_id());
			
			if(obj.get("PARAM_TYPE").equals("INSERT")) {
				//체크그룹 삭제
				workCheckListDAO.deleteWorkReqDocChkgroupByReqDocIdList(paramMap);
				workCheckListDAO.insertWorkReqDocChkgroup(paramMap);
			}else if(obj.get("PARAM_TYPE").equals("DELETE")) {
				//체크그룹 삭제
				workCheckListDAO.deleteWorkReqDocChkgroupByReqDocIdList(paramMap);
			}
		}
		
	}

	@Override
	public List<Map<String, Object>> selectWorkReqDocChkList(Map<String, Object> paramMap) throws Exception {
		List<Map<String, Object>> list = workCheckListDAO.selectWorkReqDocChkList(paramMap);
		return list;
	}

	@Override
	public void WorkReqDocCheckListUseY(String jsonData) throws Exception {
		UserVO userVO = new UserVO();
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		}
		JSONArray jsonArray = JSONArray.fromObject(jsonData);
		
		for (int i = 0; i < jsonArray.size(); i++) {
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			JSONObject obj = JSONObject.fromObject(jsonArray.get(i));
			paramMap.put("req_doc_id", obj.get("REQ_DOC_ID"));
			paramMap.put("check_group_cd", obj.get("CHECK_GROUP_CD"));
			paramMap.put("check_list_id", obj.get("CHECK_LIST_ID"));
			paramMap.put("login_user_id", userVO.getUser_id());
			
			workCheckListDAO.deleteWorkReqDocChkListByReqDocIdListId(paramMap);
			workCheckListDAO.insertWorkReqDocChkList(paramMap);
		}
		
	}
	
	@Override
	public void WorkReqDocCheckListUseN(String jsonData) throws Exception {
		UserVO userVO = new UserVO();
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		}
		JSONArray jsonArray = JSONArray.fromObject(jsonData);
		
		for (int i = 0; i < jsonArray.size(); i++) {
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			JSONObject obj = JSONObject.fromObject(jsonArray.get(i));
			paramMap.put("req_doc_id", obj.get("REQ_DOC_ID"));
			paramMap.put("check_group_cd", obj.get("CHECK_GROUP_CD"));
			paramMap.put("check_list_id", obj.get("CHECK_LIST_ID"));
			paramMap.put("login_user_id", userVO.getUser_id());
			
			workCheckListDAO.deleteWorkReqDocChkListByReqDocIdListId(paramMap);
		}
		
	}
	
	@Override
	public void mergeDeptInfo(List<HashMap> list) throws Exception, SQLException {
		
		if(list.size() > 0) {
		//	Map<String, Object> paramMap = new HashMap();
			//가져온 데이터를 insert하기전에 모두 지운다
		//	workReqEvidDAO.deleteSqmsIfDeptInfo(paramMap);
			
			workReqEvidDAO.getSqlMapClient().startBatch();
			workReqEvidDAO.getSqlMapClient().startTransaction();
			
			for (int i = 0; i < list.size(); i++) {
				HashMap vo = list.get(i);
				workReqEvidDAO.mergeDeptInfo(vo);
			}
		}
		workReqEvidDAO.getSqlMapClient().executeBatch();
		workReqEvidDAO.getSqlMapClient().commitTransaction();
		workReqEvidDAO.getSqlMapClient().endTransaction();
		
	}
	
	@Override
	public void mergeUserInfo(List<HashMap> list) throws Exception, SQLException {
		if(list.size() > 0) {
		//	Map<String, Object> paramMap = new HashMap();
			//가져온 데이터를 insert하기전에 모두 지운다
		//	workReqEvidDAO.deleteSqmsIfUserInfo(paramMap);
			
			workReqEvidDAO.getSqlMapClient().startBatch();
			workReqEvidDAO.getSqlMapClient().startTransaction();
			
			for (int i = 0; i < list.size(); i++) {
				HashMap vo = list.get(i);
				//사용자 정보 SYNC
				workReqEvidDAO.mergeUserInfo(vo);
				
				//신규 사용자 기본 권한(COMM_USER:요청자) 등록
				workReqEvidDAO.insertUserGrpInfo(vo);
				
			}
		}
		workReqEvidDAO.getSqlMapClient().executeBatch();
		workReqEvidDAO.getSqlMapClient().commitTransaction();
		workReqEvidDAO.getSqlMapClient().endTransaction();
	}

	@Override
	public void updateSSLCustId(Map<String, Object> paramMap) throws Exception {
		workReqEvidDAO.updateSSLCustId(paramMap);
	}
	
	public List selectWorkReqDocGrid(ModelMap paramMap) throws Exception {		
		return workReqDocDAO.selectWorkReqDocGrid(paramMap);
	}

	public int selectWorkReqDocGridCount(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return workReqDocDAO.selectWorkReqDocGridCount(paramMap);
	}
	
	public void insertWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}

		// 요청양식 변경
		/*String newId = workReqDocDAO.selectModelNewId(paramMap);
		paramMap.put("req_doc_id", newId);*/
		workReqDocDAO.insertWorkReqDocCustomer(paramMap);
		
		//체크그룹 삭제
		//workCheckListDAO.deleteWorkReqDocChkgroupByReqDocIdList(paramMap);
		
		// 서식 변경
		//processTemplateDAO.deleteTemplateReqDoc(paramMap); // 요청문서관련 서식 삭제
		//processTemplateDAO.insertTemplateReqDoc(paramMap); // 요청문서관련 서식 등록
	}

	public void updateWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 요청양식 변경
		workReqDocDAO.updateWorkReqDocCustomer(paramMap);
		
		// 서식 변경
		processTemplateDAO.deleteTemplateReqDoc(paramMap); // 요청문서관련 서식 삭제
		processTemplateDAO.insertTemplateReqDoc(paramMap); // 요청문서관련 서식 등록
	}
	
	public Map<String, Object> selectWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> resultMap = workReqDocDAO.selectWorkReqDocCustomer(paramMap);

		return resultMap;
	}
	
	public void deleteWorkReqDocCustomer(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		// 요청양식 변경
		workReqDocDAO.deleteWorkReqDocCustomer(paramMap);
				
	}
	
	@Override
	public Map selectRequstRegistDefault(Map paramMap) throws Exception {
		
		Map returnMap = new HashMap();
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = userVO.getUser_id();
		String custId = userVO.getCust_id();
		paramMap.put("login_user_id", userId);
		paramMap.put("login_cust_id", custId);
		
		//요청양식 정보(도움말, 요청내용, 완료희망일시)
		Map workReqDocMap = workReqDocDAO.selectWorkReqDocCustomer(paramMap);
		if(workReqDocMap != null) {
			Iterator keySet = workReqDocMap.keySet().iterator();
			while(keySet.hasNext()) {
				String key = (String)keySet.next();
				returnMap.put(key, workReqDocMap.get(key));
			}
		}
		returnMap = WebUtil.lowerCaseMapKey(returnMap);		
		
		return returnMap;
	}
	
	public int selectWorkReqDocOrderGridCount(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return workReqDocDAO.selectWorkReqDocOrderGridCount(paramMap);
	}
	
	public List selectWorkReqDocOrderGrid(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return workReqDocDAO.selectWorkReqDocOrderGrid(paramMap);
	}
	
	public HashMap selectWorkReqDocBasicInfo(ModelMap paramMap) throws Exception {
		return workReqDocDAO.selectWorkReqDocBasicInfo(paramMap);
	}
	
	public void insertWorkReqDoc(ModelMap paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}

		// 요청양식 변경
		String newId = workReqDocDAO.selectModelNewId(paramMap);
		paramMap.put("req_doc_id", newId);
		workReqDocDAO.insertWorkReqDoc(paramMap);
		
		//체크그룹 삭제
		//workCheckListDAO.deleteWorkReqDocChkgroupByReqDocIdList(paramMap);
		
		// 서식 변경
		//processTemplateDAO.deleteTemplateReqDoc(paramMap); // 요청문서관련 서식 삭제
		//processTemplateDAO.insertTemplateReqDoc(paramMap); // 요청문서관련 서식 등록
		
	}
	
	public void updateWorkReqDoc(Map<String, Object> paramMap) throws Exception {
		HashMap workReqDocInfo = (HashMap)paramMap.get("workReqDocInfo");     //부서정보 업데이트
		List orderWorkReqDocList = (List)paramMap.get("orderWorkReqDocList");    //하위부서  리스트
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 요청양식 변경
		workReqDocDAO.updateWorkReqDoc(workReqDocInfo);
				
		//하위부서 순서변경
		if(orderWorkReqDocList != null){
			
			for(int i = 0; i<orderWorkReqDocList.size(); i++){
				HashMap orderWorkReqDocData = (HashMap)orderWorkReqDocList.get(i);
				
				orderWorkReqDocData.put("req_doc_id", orderWorkReqDocData.get("REQ_DOC_ID"));
				orderWorkReqDocData.put("display_no", i+1);
				orderWorkReqDocData.put("upd_user_id", paramMap.get("upd_user_id"));
				
				workReqDocDAO.updateWorkReqDocOrder(orderWorkReqDocData);				
			}
		}
	}
	
	public void deleteWorkReqDoc(Map<String, Object> paramMap) throws Exception {
		List selectCustomerDeleteList = workReqDocDAO.selectCustomerDeleteList(paramMap);
		
		for(int i = 0 ; i<selectCustomerDeleteList.size(); i++){
			HashMap selectCustomerDeleteData = (HashMap)selectCustomerDeleteList.get(i);
			selectCustomerDeleteData.put("req_doc_id", selectCustomerDeleteData.get("REQ_DOC_ID"));
			workReqDocDAO.deleteCustomerList(selectCustomerDeleteData);
		}
		workReqDocDAO.deleteWorkReqDoc(paramMap);		
	}
	
}