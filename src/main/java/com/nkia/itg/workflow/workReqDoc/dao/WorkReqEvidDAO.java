package com.nkia.itg.workflow.workReqDoc.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workReqEvidDAO")
public class WorkReqEvidDAO extends EgovComAbstractDAO{
	
	public List<Map<String, Object>> searchWorkReqEvidTrblList(Map<String, Object> paramMap) throws NkiaException {
		return list("workReqEvidDAO.searchWorkReqEvidTrblList", paramMap);
	}

	public int searchWorkReqEvidTrblCount(Map<String, Object> paramMap) throws NkiaException {
		return (Integer)selectByPk("workReqEvidDAO.searchWorkReqEvidTrblCount", paramMap);
	}

	public List<Map<String, Object>> searchWorkReqEvidPrbmList(Map<String, Object> paramMap) throws NkiaException {
		return list("workReqEvidDAO.searchWorkReqEvidPrbmList", paramMap);
	}

	public int searchWorkReqEvidPrbmCount(Map<String, Object> paramMap) throws NkiaException {
		return (Integer)selectByPk("workReqEvidDAO.searchWorkReqEvidPrbmCount", paramMap);
	}
	
	public void insertWorkReqEvidTrbl(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqEvidDAO.insertWorkReqEvidTrbl", paramMap);
	}
	
	public void insertWorkReqEvidPrbm(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqEvidDAO.insertWorkReqEvidPrbm", paramMap);
	}
	
	public int deleteWorkReqEvidTrbl(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("workReqEvidDAO.deleteWorkReqEvidTrbl", paramMap);
	}
	
	public int deleteWorkReqEvidPrbm(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("workReqEvidDAO.deleteWorkReqEvidPrbm", paramMap);
	}
	
	public int deleteSqmsIfDeptInfo(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("workReqEvidDAO.deleteSqmsIfDeptInfo", paramMap);
	}
	
	public int deleteSqmsIfUserInfo(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("workReqEvidDAO.deleteSqmsIfUserInfo", paramMap);
	}
	
	public void mergeDeptInfo(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqEvidDAO.mergeDeptInfo", paramMap);
	}
	
	public void mergeUserInfo(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqEvidDAO.mergeUserInfo", paramMap);
	}
	
	public void insertUserGrpInfo(Map<String, Object> paramMap) throws NkiaException {
		this.insert("workReqEvidDAO.insertUserGrpInfo", paramMap);
	}
	
	public int updateSSLCustId(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("workReqEvidDAO.updateSSLCustId", paramMap);
	}
	
}
