/*
 * @(#)WorkCheckMapngDAO.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMapng.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workCheckMapngDAO")
public class WorkCheckMapngDAO extends EgovComAbstractDAO{

	public List<Map<String, Object>> selectWorkCheckGroupList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckMapngDAO.selectWorkCheckGroupList", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckMapngDAO.selectWorkCheckList", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckMapngList(Map<String, Object> paramMap) throws Exception{
		return list("workCheckMapngDAO.selectWorkCheckMapngList", paramMap);
	}	
	
	public int checkWorkCheckMapngCount(ModelMap paramMap) throws Exception {
		return (Integer)selectByPk("workCheckMapngDAO.checkWorkCheckMapngCount", paramMap);
	}

	public void insertWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckMapngDAO.insertWorkCheckMapngInfo", paramMap);
	}	
	
	public void updateWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckMapngDAO.updateWorkCheckMapngInfo", paramMap);
	}
	
	public void deleteWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception {
		this.update("workCheckMapngDAO.deleteWorkCheckMapngInfo", paramMap);
	}	
	
	
	
}

