/*
 * @(#)workCheckMapngServiceImpl.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMapng.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckMapng.dao.WorkCheckMapngDAO;
import com.nkia.itg.workflow.workCheckMapng.service.WorkCheckMapngService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("workCheckMapngService")
public class WorkCheckMapngServiceImpl implements WorkCheckMapngService{
	
	private static final Logger logger = LoggerFactory.getLogger(WorkCheckMapngServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "workCheckMapngDAO")
	public WorkCheckMapngDAO workCheckMapngDAO;
	
	public List<Map<String, Object>> selectWorkCheckGroupList(Map<String, Object> paramMap) throws Exception {
		return workCheckMapngDAO.selectWorkCheckGroupList(paramMap);
	}
	
	
	public List<Map<String, Object>> selectWorkCheckList(Map<String, Object> paramMap) throws Exception {
		return workCheckMapngDAO.selectWorkCheckList(paramMap);
	}

	
	public List<Map<String, Object>> selectWorkCheckMapngList(Map<String, Object> paramMap) throws Exception {
		return workCheckMapngDAO.selectWorkCheckMapngList(paramMap);
	}
	
	
	public int checkWorkCheckMapngCount(ModelMap paramMap) throws Exception {
		return workCheckMapngDAO.checkWorkCheckMapngCount(paramMap);
	}
		
	public void insertWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}		
		
		workCheckMapngDAO.insertWorkCheckMapngInfo(paramMap);
	}	
	
	public void updateWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}		
		
		workCheckMapngDAO.updateWorkCheckMapngInfo(paramMap);
	}	
	
	public void deleteWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
		}		
		
		workCheckMapngDAO.deleteWorkCheckMapngInfo(paramMap);
	}	
	
}