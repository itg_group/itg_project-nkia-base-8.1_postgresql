/*
 * @(#)WorkCheckMapngController.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMapng.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckMapng.service.WorkCheckMapngService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkCheckMapngController {
	
	@Resource(name = "workCheckMapngService")
	private WorkCheckMapngService workCheckMapngService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 체크리스트 맵핑 관리
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workCheckMapng/goWorkCheckMapng.do")
	public String goWorkCheckListMng(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		String forwarPage = "/itg/workflow/workCheckMapng/workCheckMapng.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
	

	/**
	 * 체크리스트 그룹 목록 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/workCheckMapng/selectWorkCheckGroupList.do")
	public @ResponseBody ResultVO selectWorkCheckGroupList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = workCheckMapngService.selectWorkCheckGroupList(paramMap);				
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}	
	
	
	/**
	 * 체크리스트 목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckMapng/selectWorkCheckList.do")
	public @ResponseBody ResultVO selectWorkCheckList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = workCheckMapngService.selectWorkCheckList(paramMap);				
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		
	
	
	/**
	 * 체크리스트 맵핑목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckMapng/selectWorkCheckMapngList.do")
	public @ResponseBody ResultVO selectWorkCheckMapngList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = workCheckMapngService.selectWorkCheckMapngList(paramMap);				
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}			

	
	/**
	 * 체크리스트 맵핑목록 등록/수정
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckMapng/updateWorkCheckMapngInfo.do")
	public @ResponseBody ResultVO updateWorkCheckMapngInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		int mapngCnt = 0;
		try {			
			
			mapngCnt = workCheckMapngService.checkWorkCheckMapngCount(paramMap);
			
			if(mapngCnt>0){
				workCheckMapngService.updateWorkCheckMapngInfo(paramMap);
			}else{
				workCheckMapngService.insertWorkCheckMapngInfo(paramMap);
			}
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	
	/**
	 * 체크리스트 맵핑목록 삭제
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/workflow/workCheckMapng/deleteWorkCheckMapngInfo.do")
	public @ResponseBody ResultVO deleteWorkCheckMapngInfo(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		int mapngCnt = 0;
		try {			
			
			workCheckMapngService.deleteWorkCheckMapngInfo(paramMap);
						
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	
	
}
