/*
 * @(#)workCheckListMapngService.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMapng.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

public interface WorkCheckMapngService {

	public List<Map<String, Object>> selectWorkCheckGroupList(Map<String, Object> paramMap) throws Exception;
	
	public List<Map<String, Object>> selectWorkCheckList(Map<String, Object> paramMap) throws Exception;
			
	public List<Map<String, Object>> selectWorkCheckMapngList(Map<String, Object> paramMap) throws Exception;
	
	public int checkWorkCheckMapngCount(ModelMap paramMap) throws Exception;
	
	public void insertWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception;
	
	public void updateWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception;
		
	public void deleteWorkCheckMapngInfo(Map<String, Object> paramMap) throws Exception;
	
	
}
