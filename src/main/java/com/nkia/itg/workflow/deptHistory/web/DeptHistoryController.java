package com.nkia.itg.workflow.deptHistory.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.deptHistory.service.DeptHistoryService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller 
public class DeptHistoryController {

	@Resource(name="deptHistoryService")
	private DeptHistoryService deptHistoryService;
	
	@RequestMapping(value="/itg/workflow/deptHistory/goDeptHistoryTab.do")
	public String goDeptHistoryTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/workflow/deptHistory/deptHistoryTab.nvf";
		if(null != request.getParameter("portal_param")){
			paramMap.put("portal_param", request.getParameter("portal_param"));
		}
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/workflow/deptHistory/goDeptHistoryMain.do")
	public String goDeptHistoryMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/workflow/deptHistory/deptHistoryMain.nvf";
		if(null != request.getParameter("portal_param")){
			paramMap.put("portal_param", request.getParameter("portal_param"));
		}
		return forwarPage;
	}
	
		
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/deptHistory/searchDeptHistoryList.do")
	public @ResponseBody ResultVO searchDeptHistoryList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			String custId = userVO.getCust_id();
			String custClass = userVO.getCust_class();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			paramMap.put("cust_id", custId);
			paramMap.put("cust_class", custClass);
			
			totalCount = deptHistoryService.searchDeptHistoryListCount(paramMap);
			List resultList = deptHistoryService.searchDeptHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
