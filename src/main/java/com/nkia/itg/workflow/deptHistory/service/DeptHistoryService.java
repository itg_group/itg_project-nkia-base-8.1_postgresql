package com.nkia.itg.workflow.deptHistory.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DeptHistoryService {

	public int searchDeptHistoryListCount(Map paramMap) throws NkiaException;;

	public List searchDeptHistoryList(Map paramMap) throws NkiaException;

}
