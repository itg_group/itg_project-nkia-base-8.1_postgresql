package com.nkia.itg.workflow.deptHistory.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("deptHistoryDAO")
public class DeptHistoryDAO extends EgovComAbstractDAO {

	public Integer searchDeptHistoryListCount(Map paramMap) {
		return (Integer)selectByPk("DeptHistoryDAO.searchDeptHistoryListCount", paramMap);
	}

	public List searchDeptHistoryList(Map paramMap) {
		return list("DeptHistoryDAO.searchDeptHistoryList", paramMap);
	}

}
