package com.nkia.itg.workflow.deptHistory.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.workflow.deptHistory.dao.DeptHistoryDAO;
import com.nkia.itg.workflow.deptHistory.service.DeptHistoryService;

@Service("deptHistoryService")
public class DeptHistoryServiceImpl implements DeptHistoryService{

	@Resource(name = "deptHistoryDAO")
	private DeptHistoryDAO deptHistoryDAO;
	
	@Override
	public int searchDeptHistoryListCount(Map paramMap) throws NkiaException {
		return deptHistoryDAO.searchDeptHistoryListCount(paramMap);
	}

	@Override
	public List searchDeptHistoryList(Map paramMap) throws NkiaException {
		return deptHistoryDAO.searchDeptHistoryList(paramMap);
	}

}
