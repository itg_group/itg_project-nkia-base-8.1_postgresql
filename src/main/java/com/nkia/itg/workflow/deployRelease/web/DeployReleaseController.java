package com.nkia.itg.workflow.deployRelease.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.workflow.deployRelease.service.DeployReleaseService;

@Controller
public class DeployReleaseController {

	
	@Resource(name="deployReleaseService")
	private DeployReleaseService deployReleaseService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 릴리즈배포 탭 페이지 이동 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/deployRelease/goDeployReleaseTab.do")
	public String goDeployReleaseTab(ModelMap paramMap) throws NkiaException{
		
		return "/itg/workflow/deployRelease/deployReleaseTab.nvf";
	}
	
	/**
	 * 릴리즈배포 탭 페이지 이후 실제 페이지 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/deployRelease/goDeployReleaseMain.do")
	public String goDeployReleaseMain(ModelMap paramMap) throws NkiaException{
		
		return "/itg/workflow/deployRelease/deployReleaseMain.nvf";
	}
	
	/**
	 * 릴리즈 배포 현황 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/deployRelease/searchDeployReleaseList.do")
	public @ResponseBody ResultVO searchDeployReleaseList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO =  new GridVO();
			int totalCount = 0;
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			// 토탈 카운트
			totalCount = deployReleaseService.searchDeployReleaseListCnt(paramMap);
			// 리스트 
			List resultList = deployReleaseService.searchDeployReleaseList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 릴리즈 배포관리 엑셀관리
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/deployRelease/searchExcelDown.do")
    public @ResponseBody ResultVO searchExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = deployReleaseService.searchExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
