package com.nkia.itg.workflow.deployRelease.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.workflow.deployRelease.dao.DeployReleaseDAO;
import com.nkia.itg.workflow.deployRelease.service.DeployReleaseService;

@Service("deployReleaseService")
public class DeployReleaseServiceImpl implements DeployReleaseService {

	@Resource(name="deployReleaseDAO")
	private DeployReleaseDAO deployReleaseDAO;

	public int searchDeployReleaseListCnt(ModelMap paramMap) throws NkiaException {
		return deployReleaseDAO.searchDeployReleaseListCnt(paramMap);
	}

	public List searchDeployReleaseList(ModelMap paramMap) throws NkiaException {
		
		return deployReleaseDAO.searchDeployReleaseList(paramMap);
		
	}

	public List searchExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = deployReleaseDAO.searchExcelDown(paramMap);
		return resultList;
	}

}
