package com.nkia.itg.workflow.deployRelease.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DeployReleaseService {

	public int searchDeployReleaseListCnt(ModelMap paramMap) throws NkiaException;

	public List searchDeployReleaseList(ModelMap paramMap) throws NkiaException;

	public List searchExcelDown(Map paramMap) throws NkiaException;


}
