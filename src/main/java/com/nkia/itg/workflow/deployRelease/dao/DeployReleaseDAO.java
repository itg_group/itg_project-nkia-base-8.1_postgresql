package com.nkia.itg.workflow.deployRelease.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("deployReleaseDAO")
public class DeployReleaseDAO extends EgovComAbstractDAO {

	public int searchDeployReleaseListCnt(ModelMap paramMap) throws NkiaException {
		
		return (Integer) selectByPk("deployReleaseDAO.searchDeployReleaseListCnt", paramMap);
	}

	public List searchDeployReleaseList(ModelMap paramMap) throws NkiaException {
		
		return list("deployReleaseDAO.searchDeployReleaseList", paramMap);
	}

	public List searchExcelDown(Map paramMap) throws NkiaException {

		return list("deployReleaseDAO.searchDeployReleaseList", paramMap);
	}

}
