package com.nkia.itg.workflow.kedb.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface KedbService {
	
	public List searchKedbList(ModelMap paramMap) throws NkiaException, SQLException;

	public int searchKedbListCount(ModelMap paramMap) throws NkiaException;
	
	public String insertKedb(ModelMap paramMap) throws NkiaException;
	
	public void updateKedb(ModelMap paramMap) throws NkiaException;
	
	public void deleteKedb(ModelMap paramMap) throws NkiaException, IOException;
	
	public List searchCategory(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchKedbInfo(ModelMap paramMap) throws NkiaException;
}
