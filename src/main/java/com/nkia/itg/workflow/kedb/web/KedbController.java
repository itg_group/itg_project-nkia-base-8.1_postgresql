/*
 * @(#)KedbController.java              2013. 5. 22.
 *
 * @eungyu@nakia.co.kr
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.kedb.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.kedb.service.KedbService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class KedbController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "kedbService")
	private KedbService kedbService;
	
	/**
	 * message 호출
	 */	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;

	
	/**
	 * KEDB관리 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kedb/kedbManager.do")
	public String kedb(ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String viewType = request.getParameter("view_type");
		paramMap.put("view_type", viewType);
		String forwarPage = "/itg/workflow/kedb/kedbManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 분류체계관리 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/kedb/kedbSearchManager.do")
	public String kedbSearch(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/workflow/kedb/kedbSearchManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 분류체계 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kedb/searchKedbList.do")
	public @ResponseBody ResultVO searchKedbList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = kedbService.searchKedbListCount(paramMap);
			List resultList = kedbService.searchKedbList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			 throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * KEDB 관리 - 등록
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/kedb/insertKedb.do")
	public @ResponseBody ResultVO insertKedb(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = new HashMap();
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
	    	String kedbId = kedbService.insertKedb(paramMap);
	    	resultMap.put("kedbId", kedbId);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * KEDB 관리 - 수정
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/kedb/updateKedb.do")
	public @ResponseBody ResultVO updateKedb(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
	    	kedbService.updateKedb(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	
	/**
	 * KEDB 관리 - 삭제
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/kedb/deleteKedb.do")
	public @ResponseBody ResultVO deleteKedb(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}			
			kedbService.deleteKedb(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 오류유형_트리생성
	 * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/workflow/kedb/searchCategory.do")
	public @ResponseBody ResultVO searchCategory(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = kedbService.searchCategory(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 사용자정보 수정시 비밀번호 복호화를 위하여 단건 조회 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/kedb/searchKedbInfo.do")
	public @ResponseBody ResultVO searchKedbInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = kedbService.searchKedbInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
}
