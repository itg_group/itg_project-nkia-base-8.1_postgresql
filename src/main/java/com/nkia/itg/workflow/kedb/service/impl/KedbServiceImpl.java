package com.nkia.itg.workflow.kedb.service.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.system.system.service.impl.SystemServiceImpl;
import com.nkia.itg.workflow.kedb.dao.KedbDAO;
import com.nkia.itg.workflow.kedb.service.KedbService;

@Service("kedbService")
public class KedbServiceImpl implements KedbService{
	
	private static final Logger logger = LoggerFactory.getLogger(KedbServiceImpl.class);
	
	@Resource(name = "kedbDao")
	public KedbDAO kedbDao;

	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public int searchKedbListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return kedbDao.searchKedbListCount(paramMap);
	}

	public List searchKedbList(ModelMap paramMap) throws NkiaException, SQLException {
		// TODO Auto-generated method stub
		return kedbDao.searchKedbList(paramMap);
	}

	public String insertKedb(ModelMap paramMap) throws NkiaException {
		String kedbId = kedbDao.selectKedbId(paramMap);
		paramMap.put("kedbId", kedbId);
		kedbDao.insertKedb(paramMap);
		return kedbId;
	}
	
	public void updateKedb(ModelMap paramMap) throws NkiaException {
		kedbDao.updateKedb(paramMap);
	}
	
	public void deleteKedb(ModelMap paramMap) throws NkiaException, IOException {
		kedbDao.deleteKedb(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}		
	}
	
	public List searchCategory(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return kedbDao.searchCategory(paramMap);
	}
	
	public HashMap searchKedbInfo(ModelMap paramMap) throws NkiaException {
		return kedbDao.searchKedbInfo(paramMap);
	}
}
