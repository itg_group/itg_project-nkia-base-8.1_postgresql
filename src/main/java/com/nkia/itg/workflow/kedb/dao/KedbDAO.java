package com.nkia.itg.workflow.kedb.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("kedbDao")
public class KedbDAO  extends EgovComAbstractDAO {

	public int searchKedbListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("kedbDao.searchKedbListCount", paramMap);
		return count;
	}

	public List searchKedbList(ModelMap paramMap) throws NkiaException, SQLException {
		// TODO Auto-generated method stub
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("kedbDao.searchKedbList", paramMap, handler);
		return handler.getReturnList();
	}
	
	public String selectKedbId(ModelMap paramMap) throws NkiaException {
		String kedbId = (String) this.selectByPk("kedbDao.selectKedbId", paramMap);
		return kedbId;
	}	
	
	public void insertKedb(ModelMap paramMap) throws NkiaException {
		this.insert("kedbDao.insertKedb", paramMap);
	}
	
	public void updateKedb(ModelMap paramMap) throws NkiaException {
		this.update("kedbDao.updateKedb", paramMap);
	}	
	
	public void deleteKedb(ModelMap paramMap) throws NkiaException {
		this.delete("kedbDao.deleteKedb", paramMap);
	}	
	
	public List searchCategory(ModelMap paramMap) throws NkiaException {
		return this.list("kedbDao.searchCategory", paramMap);
	}
	
	public HashMap searchKedbInfo(ModelMap paramMap) throws NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		super.getSqlMapClientTemplate().queryWithRowHandler("kedbDao.searchKedbInfo", paramMap, handler);
		return (HashMap)handler.getReturnMap();
		//return (HashMap) selectByPk("kedbDao.searchKedbInfo", paramMap);
	}
	
	public String selectKedbId(Map paramMap) throws NkiaException {
		String kedbId = (String) this.selectByPk("kedbDao.selectKedbId", paramMap);
		return kedbId;
	}	
	
	public void insertKedb(Map paramMap) throws NkiaException {
		this.insert("kedbDao.insertKedb", paramMap);
	}
	
	public void insertKedbRelConf(Map paramMap) throws NkiaException {
		this.insert("kedbDao.insertKedbRelConf", paramMap);
	}
}
