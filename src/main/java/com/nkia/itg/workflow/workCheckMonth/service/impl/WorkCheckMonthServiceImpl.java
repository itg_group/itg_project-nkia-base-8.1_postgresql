/*
 * @(#)workCheckMonthServiceImpl.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMonth.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckMonth.dao.WorkCheckMonthDAO;
import com.nkia.itg.workflow.workCheckMonth.service.WorkCheckMonthService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("workCheckMonthService")
public class WorkCheckMonthServiceImpl implements WorkCheckMonthService{
	
	private static final Logger logger = LoggerFactory.getLogger(WorkCheckMonthServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "workCheckMonthDAO")
	public WorkCheckMonthDAO workCheckMonthDAO;

	
	public List<Map<String, Object>> selectWorkCheckMonthList(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}		
		return workCheckMonthDAO.selectWorkCheckMonthList(paramMap);
	}		
		
	public List<Map<String, Object>> selectWorkCheckMonthDtlList(Map<String, Object> paramMap) throws Exception {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("ins_user_id", userVO.getUser_id());
		}		
		return workCheckMonthDAO.selectWorkCheckMonthDtlList(paramMap);
	}			
}