/*
 * @(#)WorkCheckMonthDAO.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMonth.dao;

import java.sql.CallableStatement;
import java.util.HashMap; 
import java.util.List;
import java.util.Map;
 
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("workCheckMonthDAO")
public class WorkCheckMonthDAO extends EgovComAbstractDAO{

		
	public List<Map<String, Object>> selectWorkCheckMonthList(Map<String, Object> paramMap) throws Exception{
		
		return list("workCheckMonthDAO.selectWorkCheckMonthList", paramMap);
	}
	
	public List<Map<String, Object>> selectWorkCheckMonthDtlList(Map<String, Object> paramMap) throws Exception{
		
		return list("workCheckMonthDAO.selectWorkCheckMonthDtlList", paramMap);
	}	
		
}

