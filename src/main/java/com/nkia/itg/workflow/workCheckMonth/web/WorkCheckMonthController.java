/*
 * @(#)WorkCheckMonthController.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMonth.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.excel.ExcelMakerByPoi;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.workflow.workCheckMonth.service.WorkCheckMonthService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.utl.fcc.service.EgovStringUtil;

@Controller
public class WorkCheckMonthController {
	
	@Resource(name = "workCheckMonthService")
	private WorkCheckMonthService workCheckMonthService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	@Resource(name = "excelMakerByPoi")
	private ExcelMakerByPoi excelMakerByPoi;
	
	
	/**
	 * 체크리스트 결과 관리
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/workCheckMonth/goWorkCheckMonth.do")
	public String goWorkCheckMonth(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap)throws Exception{
		String forwarPage = "/itg/workflow/workCheckMonth/workCheckMonth.nvf";
		paramMap.put("reqMap", reqMap);
		return forwarPage;
	}
			

	/**
	 * 체크리스트 결과목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckMonth/selectWorkCheckResultMonthList.do")
	public @ResponseBody ResultVO selectWorkCheckResultMonthList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = null;
			
			Map map = new HashMap();
			String result_yyyymm = paramMap.get("result_yyyymm").toString();
									
			paramMap.put("result_yyyymm", result_yyyymm.replaceAll("-",""));
			
			resultList = workCheckMonthService.selectWorkCheckMonthList(paramMap);
							
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}		

	/**
	 * 체크리스트 결과목록 출력 엑셀다운로드
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/workflow/workCheckMonth/selectWorkCheckResultMonthExcelDown.do")
	public @ResponseBody ResultVO selectWorkCheckResultMonthExcelDown(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map<String, Object> excelParam = (Map<String, Object>) paramMap.get("excelParam");
			Map<String, Object> param = (Map<String, Object>) paramMap.get("param");
			
			String result_yyyymm = param.get("result_yyyymm").toString();
			param.put("result_yyyymm", result_yyyymm.replaceAll("-",""));
			
			List resultList = workCheckMonthService.selectWorkCheckMonthList(param);
			String poi_yn = EgovStringUtil.isNullToString(param.get("poi_yn"));
			if("Y".equals(poi_yn)){
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultList);
			}else{
				excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			}
			resultVO.setResultMap(excelMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("엑셀다운로드 되었습니다"); // 조회되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 체크리스트 결과목록 출력
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/workCheckMonth/selectWorkCheckResultMonthDtlList.do")
	public @ResponseBody ResultVO selectWorkCheckResultMonthDtlList(@RequestBody ModelMap paramMap) throws Exception {	
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
			
		try {				
			List resultList = null;
						
			String result_yyyymm = paramMap.get("result_yyyymm").toString();
												
			paramMap.put("result_yyyymm", result_yyyymm.replaceAll("-",""));
			
			resultList = workCheckMonthService.selectWorkCheckMonthDtlList(paramMap);
							
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}			
			return resultVO;
	}			
	
}
