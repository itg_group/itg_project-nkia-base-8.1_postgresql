/*
 * @(#)workCheckListSearchService.java              2020. 11. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.workflow.workCheckMonth.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

public interface WorkCheckMonthService {

	public List<Map<String, Object>> selectWorkCheckMonthList(Map<String, Object> paramMap) throws Exception;

	public List<Map<String, Object>> selectWorkCheckMonthDtlList(Map<String, Object> paramMap) throws Exception;
}
