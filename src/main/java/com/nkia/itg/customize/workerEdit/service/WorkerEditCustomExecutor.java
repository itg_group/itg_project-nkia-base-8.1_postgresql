package com.nkia.itg.customize.workerEdit.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public interface WorkerEditCustomExecutor {
	
	public int searchWorkerEditListCount(Map paramMap)throws NkiaException, SQLException, Exception;

	public List searchWorkerEditList(Map paramMap)throws NkiaException, SQLException, Exception;
	
	public List searchWorkerWorkList(Map paramMap)throws NkiaException, SQLException, Exception;

	public List searchWorkerDisuseList(Map paramMap) throws NkiaException, SQLException, Exception;

	public void updateWorkerData(NbpmProcessVO processVO, Map paramMap) throws NkiaException;
	
	public void updateMultiTaskData(NbpmProcessVO processVO, Map paramMap)throws NkiaException;

	public void updateApprovalInfo(Map paramMap)throws NkiaException, SQLException, Exception;

	public void deleteApprovalInfo(Map paramMap) throws NkiaException, SQLException, Exception;

	public void deleteMultiTaskData(NbpmProcessVO processVO, Map paramMap) throws NkiaException;

	
	
}
