package com.nkia.itg.customize.workerEdit.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.customize.workerEdit.dao.WorkerEditCustomDAO;
import com.nkia.itg.customize.workerEdit.service.WorkerEditCustomExecutor;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.ChangeWorkerCmd;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.email.service.impl.EmailInfoServiceImpl;

@Service("workerEditCustomCommonExecutor")
public class WorkerEditCustomCommonExecutor implements WorkerEditCustomExecutor {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailInfoServiceImpl.class);

	@Resource(name = "workerEditCustomDAO")
	private WorkerEditCustomDAO workerEditCustomDAO;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "commonRuleDAO")
	private CommonRuleDAO commonRuleDAO;
	
	@Resource(name = "nbpmCommonClient")
	private NbpmCommonClient nbpmCommonClient;
	
	@Override
	public int searchWorkerEditListCount(Map paramMap) throws SQLException, Exception {
		return workerEditCustomDAO.searchWorkerEditListCount(paramMap);
	}

	@Override
	public List searchWorkerEditList(Map paramMap) throws SQLException, Exception {
		return workerEditCustomDAO.searchWorkerEditList(paramMap);
	}
	
	@Override
	public void updateWorkerData(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		try {
			
			AbstractNbpmCommand changeWorkerCmd = new ChangeWorkerCmd(processVO);			
			changeWorkerCmd.execute();
			paramMap.put("grp_slct_yn", "N");
			int count = workerEditCustomDAO.selectWorkerDataCount(paramMap);
			if(count > 0) {
				workerEditCustomDAO.updateWorkerData(paramMap);				
			} else {
				workerEditCustomDAO.insertWorkerData(paramMap);
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
	
	@Override
	public List searchWorkerWorkList(Map paramMap) throws SQLException, Exception {
		
		int endCheckCount = 0;
		endCheckCount = workerEditCustomDAO.searchEndCheckCount(paramMap);
		if(endCheckCount > 0){
			paramMap.put("sr_id", "-");
		}
		
		return workerEditCustomDAO.searchWorkerWorkList(paramMap);
	}
	

	@Override
	public List searchWorkerDisuseList(Map paramMap) throws NkiaException, SQLException, Exception {
		return workerEditCustomDAO.searchWorkerDisuseList(paramMap);
	}

	@Override
	public void updateMultiTaskData(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		
		//Task 정보변경
		long taskId = processVO.getTask_id();
		paramMap.put("id", taskId);
		updateWorkerData(processVO, paramMap);
		
		//멀티태스크 정보 변경
		Map multiInfoMap = new HashMap();
		multiInfoMap.put("task_group_id", taskId);
		multiInfoMap.put("worker_user_id", (String)processVO.getWorker_user_id());
		multiInfoMap.put("task_name", (String)paramMap.get("task_name"));
		postWorkDAO.updateTaskGroupInfo(multiInfoMap);
	}
	
	@Override
	public void updateApprovalInfo(Map paramMap) throws SQLException, Exception {
		
		String gridType = (String)paramMap.get("grid_id");
		if("WorkerWorkList".equals(gridType)){
			workerEditCustomDAO.updateProcHpAprvlInfoTemp(paramMap);
		}
		
		//이력 정보 저장
		String afterWorkerId = (String)paramMap.get("after_worker_id");
		if(afterWorkerId != null && "workerEdit".equals(afterWorkerId)){
		
		}else{
			workerEditCustomDAO.insertProcAprvlInfoHis(paramMap);	
		}
	}
	
	@Override
	public void deleteApprovalInfo(Map paramMap) throws SQLException, Exception {
		
		String gridType = (String)paramMap.get("grid_id");
		if("WorkerWorkList".equals(gridType)){
			workerEditCustomDAO.deleteProcHpAprvlInfoTemp(paramMap);
			
		}
		
		//이력 정보 저장
		workerEditCustomDAO.insertProcAprvlInfoHis(paramMap);
	}
	
	@Override
	public void deleteMultiTaskData(NbpmProcessVO processVO, Map paramMap) throws NkiaException {
		
		try{
			//다음 태스크 진행
			long taskId = processVO.getTask_id();
			paramMap.put("id", taskId);
			
			//Task 정보변경
			workerEditCustomDAO.updateTaskStatus(paramMap);	

			paramMap.put("task_group_id", taskId);
			postWorkDAO.deleteMultiTaskProcOper(paramMap);
			//postWorkDAO.deleteCanceledTask(paramMap);
			postWorkDAO.deleteMultiTaskInfo(paramMap);
			
		}catch(Exception e){
			throw new NkiaException(e);
		}
		
	}

	
}
