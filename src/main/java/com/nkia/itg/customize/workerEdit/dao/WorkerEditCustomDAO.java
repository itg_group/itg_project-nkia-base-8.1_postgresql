package com.nkia.itg.customize.workerEdit.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("workerEditCustomDAO")
public class WorkerEditCustomDAO extends NbpmAbstractDAO {

	public List searchWorkerEditList(Map paramMap) throws SQLException, Exception {
		return list("WorkerEditCustomDAO.searchWorkerEditList", paramMap);
	}

	public int searchWorkerEditListCount(Map paramMap) throws SQLException, Exception {
		return (Integer)selectByPk("WorkerEditCustomDAO.searchWorkerEditListCount", paramMap);
	}
	
	public int searchEndCheckCount(Map paramMap) throws SQLException, Exception {
		return (Integer)selectByPk("WorkerEditCustomDAO.searchEndCheckCount", paramMap);
	}

	public List searchWorkerWorkList(Map paramMap) throws SQLException, Exception {
		return list("WorkerEditCustomDAO.searchWorkerWorkList", paramMap);
	}
	
	public List searchWorkerDisuseList(Map paramMap) throws SQLException, Exception {
		return list("WorkerEditCustomDAO.searchWorkerDisuseList", paramMap);
	}
	
	public int selectTaskCount(Map paramMap) throws SQLException, Exception {
		return (Integer)selectByPk("WorkerEditCustomDAO.selectTaskCount", paramMap);
	}
	
	public void updateWorkerData(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().update("WorkerEditCustomDAO.updateWorkerData", paramMap);
	}

	public int selectWorkerDataCount(Map paramMap) throws SQLException, Exception {
		return (Integer)selectByPk("WorkerEditCustomDAO.selectWorkerDataCount", paramMap);
	}

	public void insertWorkerData(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().update("WorkerEditCustomDAO.insertWorkerData", paramMap);
	}

	public void updateTaskStatus(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().update("WorkerEditCustomDAO.updateTaskStatus", paramMap);
	}

	public void updateProcHpAprvlInfoTemp(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().update("WorkerEditCustomDAO.updateProcHpAprvlInfoTemp", paramMap);
	}

	public void deleteProcHpAprvlInfoTemp(Map paramMap) throws SQLException, Exception {
		delete("WorkerEditCustomDAO.deleteProcHpAprvlInfoTemp", paramMap);
	}
	
	public void insertProcAprvlInfoHis(Map paramMap) throws SQLException, Exception {
		this.getSqlMapClient().update("WorkerEditCustomDAO.insertProcAprvlInfoHis", paramMap);
	}

}
