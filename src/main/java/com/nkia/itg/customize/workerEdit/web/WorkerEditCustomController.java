package com.nkia.itg.customize.workerEdit.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.customize.workerEdit.service.WorkerEditCustomExecutor;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class WorkerEditCustomController {
	
	@Resource(name="workerEditCustomCommonExecutor")
	private WorkerEditCustomExecutor workerEditCustomExecutor;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;

	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "nbpmCommonClient")
	private NbpmCommonClient nbpmCommonClient;
	
	@RequestMapping(value="/itg/customize/workerEdit/workerEditCustomTab.do")
	public String goMyHistoryTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/customize/workerEdit/workerEditCustomTab.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/customize/workerEdit/goWorkerEditCustomMain.do")
	public String goMyHistoryListMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/customize/workerEdit/workerEditCustomMain.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/customize/workerEdit/goWorkerCustomDetailPage.do")
	public String goWorkerDetailPage(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/customize/workerEdit/workerEditCustomDetail.nvf";
		String srId = request.getParameter("sr_id");
		String reqType = request.getParameter("req_type");
		paramMap.put("srId", srId);
		paramMap.put("reqType", reqType);
		return forwarPage;
	}	
	
		
	/**
	 * 대한항공 - 작업요청, 서버생성폐기요청 
	 * 부서 승인 수정 및 삭제(작업요청만 삭제가능)
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/workerEdit/searchWorkerEditList.do")
	public @ResponseBody ResultVO searchWorkerEditList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			
			totalCount = workerEditCustomExecutor.searchWorkerEditListCount(paramMap);
			resultList = workerEditCustomExecutor.searchWorkerEditList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	/**
	 * 작업요청 - 부서승인(멀티)
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/workerEdit/searchWorkerWorkList.do")
	public @ResponseBody ResultVO searchWorkerWorkList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = workerEditCustomExecutor.searchWorkerWorkList(paramMap);
			
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
	
	/**
	 * 서버생성폐기요청 - 부서승인(단일)
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/workerEdit/searchWorkerDisuseList.do")
	public @ResponseBody ResultVO searchWorkerDisuseList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		ResultVO result = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = workerEditCustomExecutor.searchWorkerDisuseList(paramMap);
			
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return result;
	}
		
	/**
	 * [작업요청,서버생성폐기요청] 부서승인자 수정
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/workerEdit/updateWorkerData.do")
	public @ResponseBody ResultVO updateWorkerData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			paramMap.put("upd_user_id", loginUserId);
			
			NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
			String workerUserId = (String)paramMap.get("after_worker_id");
			processVO.setWorker_user_id(workerUserId);
			
			Object checkTaskId = paramMap.get("nbpm_task_id");

			if(checkTaskId != null && !"".equals(checkTaskId)) {
				
				//타스크의 상태가 처리 가능한 상태인지 확인한다.(이미 처리된 것은 아닌지, 본인이 담당자가 맞는지)
				boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
				if(workCheck) {
					//task, proc_multitask_info 정보수정
					processCommonExecutor.registerUser(processVO, paramMap);
					workerEditCustomExecutor.updateMultiTaskData(processVO, paramMap);
					workerEditCustomExecutor.updateApprovalInfo(paramMap);
					resultVO.setResultMsg("담당자 변경이 완료 되었습니다."); 
				}else{
					resultVO.setResultMsg("처리상태 확인이 필요합니다.\n목록을 갱신하고 재시도 하여 주세요."); 
				}
			} else {
				paramMap.put("task_id", 0);
				workerEditCustomExecutor.updateApprovalInfo(paramMap);
				resultVO.setResultMsg("담당자 변경이 완료 되었습니다."); 
			}
			
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [작업요청] 부서승인 삭제
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/workerEdit/deleteWorkerData.do")
	public @ResponseBody ResultVO deleteWorkerData(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String message = "";
		try {
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			paramMap.put("upd_user_id", loginUserId);
		
			Object checkTaskId = paramMap.get("nbpm_task_id");

			if(checkTaskId != null && !"".equals(checkTaskId)) {
				
				NbpmProcessVO processVO = NbpmUtil.setProcessParameter(paramMap);
				processVO.setWorker_user_id(loginUserId);
				processVO.setWorkType(NbpmConstraints.WORKTYPE_NORMAL_PROC);
				
				//타스크의 상태가 처리 가능한 상태인지 확인한다.(이미 처리된 것은 아닌지, 본인이 담당자가 맞는지)
				boolean workCheck = processCommonExecutor.checkTaskStatus(processVO);
				if(workCheck) {
					paramMap.put("after_worker_id", "workerEdit");
					workerEditCustomExecutor.updateApprovalInfo(paramMap);
					processCommonExecutor.workTask(processVO,paramMap);
					
					//task(정보수정 Exited), proc_multitask_info 삭제
					workerEditCustomExecutor.deleteMultiTaskData(processVO, paramMap);
					paramMap.remove("after_worker_id");
					paramMap.put("worker_user_id", "workerEdit");
					
					workerEditCustomExecutor.deleteApprovalInfo(paramMap);
					resultVO.setResultMsg("담당자 삭제가 완료되었습니다."); 
				}else{
					resultVO.setResultMsg("처리상태 확인이 필요합니다.\n목록을 갱신하고 재시도 하여 주세요."); 
				}
			} else {
				paramMap.put("task_id", 0);
				workerEditCustomExecutor.deleteApprovalInfo(paramMap);
				resultVO.setResultMsg("담당자 삭제가 완료되었습니다."); 
			}
			
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
