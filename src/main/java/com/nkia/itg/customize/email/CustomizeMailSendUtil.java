package com.nkia.itg.customize.email;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationEmailMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.AbstractEmailSendJob;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.SmtpCallEmailSendJob;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.system.email.service.EmailHistoryService;


public class CustomizeMailSendUtil extends AbstractEmailSendJob {
	
	private static final Logger logger = LoggerFactory.getLogger(SmtpCallEmailSendJob.class);
	private EmailVO emailVO;
	
	public void setMailVO(EmailVO emailVO) {
		this.emailVO = emailVO;
	}

	/**
	 * 메일 전송
	 * @param mailMap
	 * @return boolean
	 */
	public void sendMail() throws NkiaException {
		
		boolean sendFlag = false;
		
		SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss", java.util.Locale.KOREA);
		
		try {
			//메일 로직 구현
				
			// 메일 전송 이력 등록
			insertSendMailHistory("SUCCESS", emailVO, "");
			
			sendFlag = true;
			
		} catch (NkiaException addr_e) {  //예외처리 주소를 입력하지 않을 경우
			//JOptionPane.showMessageDialog(null, "메일을 입력해주세요", "메일주소입력", JOptionPane.ERROR_MESSAGE);
			//addr_e.printStackTrace();
			//StringWriter sw = new StringWriter();
			//PrintWriter pw = new PrintWriter(sw);
			new NkiaException(addr_e); 
			// 메일 전송 이력 등록
			insertSendMailHistory("FAIL", emailVO, addr_e.toString());
		} catch (Exception e) {
			new NkiaException(e);
			// 메일 전송 이력 등록
			insertSendMailHistory("FAIL", emailVO, e.toString());
		}
	}
	
	// 메일발송 이력 관리
	public void insertSendMailHistory(String status, EmailVO emailVO, String errMsg) throws NkiaException {
		emailVO.setStatus(status);
		emailVO.setErr_msg(errMsg);
		
		EmailHistoryService emailHistoryService = (EmailHistoryService) NkiaApplicationContext.getCtx().getBean("emailHistoryService");
		emailHistoryService.insertMailSendHistory(emailVO);
	}
	

	@Override
	public void run() {
		try {
			sendMail();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}

}
