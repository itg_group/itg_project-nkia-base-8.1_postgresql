package com.nkia.itg.customize.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("kairCustomDAO")
public class KairCustomDAO extends EgovComAbstractDAO {
	
	public Map selectDeptConfmer(String req_Id) throws NkiaException {
		return(Map)selectByPk("KairCustomDAO.selectDeptConfmer", req_Id);
	}

	public HashMap selectHistId(Map paramMap) {
		return(HashMap)selectByPk("KairCustomDAO.selectHistId", paramMap);
	}
	
	/*public Map selectWorkReqDoc(Map paramMap) {
		return (Map)selectByPk("KairCustomDAO.selectWorkReqDoc", paramMap);
	}*/
}
