package com.nkia.itg.customize.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.customize.dao.KairCustomDAO;
import com.nkia.itg.customize.service.KairCustomService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;


@Service("kairCustomService")
public class KairCustomServiceImpl implements KairCustomService{
	
	private static final Logger logger = LoggerFactory.getLogger(KairCustomServiceImpl.class);
	
	@Resource(name = "kairCustomDAO")
	public KairCustomDAO kairCustomDAO;
	
	/**
	 * 요청자의 부서의 부서장 조회
	 * 
	 * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	@Override
	public Map selectDeptConfmer(String req_Id) throws NkiaException {
		return kairCustomDAO.selectDeptConfmer(req_Id);
	}

	@Override
	public HashMap selectHistId(Map paramMap) throws NkiaException {
		return kairCustomDAO.selectHistId(paramMap);
	}
	
	/*@Override
	public Map selectRequstRegistDefault(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = userVO.getUser_id();
		String custId = userVO.getCust_id();
		paramMap.put("login_user_id", userId);
		paramMap.put("login_cust_id", custId);
		
		//요청양식 정보(도움말, 요청내용, 완료희망일시)
		Map workReqDocMap = kairCustomDAO.selectWorkReqDoc(paramMap);
		if(workReqDocMap != null) {
			Iterator keySet = workReqDocMap.keySet().iterator();
			while(keySet.hasNext()) {
				String key = (String)keySet.next();
				returnMap.put(key, workReqDocMap.get(key));
			}
		}
		returnMap = WebUtil.lowerCaseMapKey(returnMap);

		//요청작업(CSR) - (요청작업데이터)ROLE_OPETR_02 = '사전검토자(ROLE_OPETR_02)', (요청작업데이터)ROLE_OPETR_01 = '작업관리자(ROLE_OPETR_03)' 
		Map roleOper = kairCustomDAO.selectpProcessRoleOper(paramMap);
		if(roleOper != null){
			
			//요청작업(CSR) - 작업관리자
			if(!"".equals((String)roleOper.get("ROLE_OPETR_01_USER_ID"))){
				returnMap.put("ROLE_OPETR_03", (String)roleOper.get("ROLE_OPETR_01_USER_NM"));
				returnMap.put("ROLE_OPETR_03_user_id", (String)roleOper.get("ROLE_OPETR_01_USER_ID"));
				returnMap.put("ROLE_OPETR_03_grp_select_yn", (String)roleOper.get("ROLE_OPETR_01_GRP_SELECT_YN"));
			}
			
			//요청작업(CSR) - 사전관리자
			
			if(!"".equals((String)roleOper.get("ROLE_OPETR_02_USER_ID"))){
				returnMap.put("ROLE_OPETR_02", (String)roleOper.get("ROLE_OPETR_02_USER_NM"));
				returnMap.put("ROLE_OPETR_02_user_id", (String)roleOper.get("ROLE_OPETR_02_USER_ID"));
				returnMap.put("ROLE_OPETR_02_grp_select_yn", (String)roleOper.get("ROLE_OPETR_02_GRP_SELECT_YN"));
			}
			
			//요청작업(CSR) - 보안검증담당자
			
			if(!"".equals((String)roleOper.get("ROLE_OPETR_21_USER_ID"))){
				returnMap.put("ROLE_OPETR_21", (String)roleOper.get("ROLE_OPETR_21_USER_NM"));
				returnMap.put("ROLE_OPETR_21_user_id", (String)roleOper.get("ROLE_OPETR_21_USER_ID"));
				returnMap.put("ROLE_OPETR_21_grp_select_yn", (String)roleOper.get("ROLE_OPETR_21_GRP_SELECT_YN"));
			}
		}
		
		return returnMap;
	}*/
}
