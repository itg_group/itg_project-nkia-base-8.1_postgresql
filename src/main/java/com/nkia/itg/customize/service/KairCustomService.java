package com.nkia.itg.customize.service;

import java.util.HashMap;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;


public interface KairCustomService {
	
	/**
	 * 요청자의 부서의 부서장 조회
	 * @param req_Id
	 * @return
	 * @throws NkiaException
	 */
	public Map selectDeptConfmer(String req_Id) throws NkiaException;
	
	/**
	 * HIST 시스템의 ID를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectHistId(Map paramMap) throws NkiaException;
	
	//public Map selectRequstRegistDefault(Map paramMap) throws NkiaException;
}
