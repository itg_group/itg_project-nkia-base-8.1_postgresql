package com.nkia.itg.customize.ubit.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;
import com.nkia.itg.nbpm.common.dao.NbpmAbstractDAO;

@Repository("CustomizeUbitDAO")
public class CustomizeUbitDAO extends NbpmAbstractDAO {
	public List<Map<String, Object>> searchWorkStep(Map<String, Object> paramMap) throws NkiaException{
		return list("CustomizeUbitDAO.searchWorkStep", paramMap);
	}
	
	public Map setRceptGrp(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.setRceptGrp", paramMap);
	}
	
	public Map setMngGrp(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.setMngGrp", paramMap);
	}
	
	public Map selectWorkReqDoc(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectWorkReqDoc", paramMap);
	}
	
	public List<Map<String, Object>> changeReview(Map<String, Object> paramMap) throws NkiaException{
		return list("CustomizeUbitDAO.changeReview", paramMap);
	}
	
	public List<Map<String, Object>> changeReviewHis(Map<String, Object> paramMap) throws NkiaException{
		return list("CustomizeUbitDAO.changeReviewHis", paramMap);
	}
	
	public void changeReviewInsert(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeReviewInsert", paramMap);
	}
	
	public void changeReviewUpdate(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeReviewUpdate", paramMap);
	}
	
	public void changeReviewDelete(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeReviewDelete", paramMap);
	}
	
	public void changeReviewHistoryInsert(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeReviewHistoryInsert", paramMap);
	}
	
	public void changeHistoryInsert(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeHistoryInsert", paramMap);
	}
	
	public void changeReviewItemInit(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeReviewItemInit", paramMap);
	}
	
	public Map countAttendees(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.countAttendees", paramMap);
	}
	
	public List<Map<String, Object>> selectAttendeesInit(Map<String, Object> paramMap) {
		return list("CustomizeUbitDAO.selectAttendeesInit", paramMap);
	}
	
	public List<Map<String, Object>> selectAttendeesList(Map<String, Object> paramMap) {
		return list("CustomizeUbitDAO.selectAttendeesList", paramMap);
	}

	public Map selectProcChangeRule(Map<String, Object> paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectProcChangeRule", paramMap);
	}
	
	public List<Map<String, Object>> selectProcRelConfEmsId(Map<String, Object> paramMap) {
		return list("CustomizeUbitDAO.selectProcRelConfEmsId", paramMap);
	}
	public int selectProcRelConfEmsIdCount(Map paramMap) {
		return (Integer)selectByPk("CustomizeUbitDAO.selectProcRelConfEmsIdCount", paramMap);
	}
	public Map selectProcChangeOperAppm(Map<String, Object> paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectProcChangeOperAppm", paramMap);
	}
	
	public int selectPreReqLoadCount(Map paramMap) {
		return (Integer)selectByPk("CustomizeUbitDAO.selectPreReqLoadCount", paramMap);
	}

	public List selectPreReqLoad(Map paramMap) {
		return list("CustomizeUbitDAO.selectPreReqLoad", paramMap);
	}
	
	public List selectPreReqLoadGuestGrid(Map paramMap) {
		return list("CustomizeUbitDAO.selectPreReqLoadGuestGrid", paramMap);
	}
	
	public List selectPreReqLoadWorkGrid(Map paramMap) {
		return list("CustomizeUbitDAO.selectPreReqLoadWorkGrid", paramMap);
	}
	
	public void centerGuestInUpdate(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.centerGuestInUpdate", paramMap);
	}
	
	public void centerGuestOutUpdate(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.centerGuestOutUpdate", paramMap);
	}
	
	public int searchCenterListCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("CustomizeUbitDAO.searchCenterListCount", paramMap);
	}

	public List searchCenterList(Map paramMap) throws NkiaException {
		return list("CustomizeUbitDAO.searchCenterList", paramMap);
	}
	
	public Map emrgncyDtValid(Map<String, Object> paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.emrgncyDtValid", paramMap);
	}
	
	public List searchSnapshot(HashMap paramMap) throws NkiaException {
		return list("CustomizeUbitDAO.searchSnapshot", paramMap);
	}
	public void insertSnapshot(Map<String, Object> paramMap) throws NkiaException {
		this.insert("CustomizeUbitDAO.insertSnapshot", paramMap);
	}
	
	public void deleteSnapshot(Map<String, Object> paramMap) throws NkiaException {
		this.delete("CustomizeUbitDAO.deleteSnapshot", paramMap);
	}
	
	public void changeUpdateSnapshot(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeUpdateSnapshot", paramMap);
	}
	
	public Map selectDiff(Map paramMap) {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("CustomizeUbitDAO.selectDiff", paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnMap();
	}
	
	public List selectChecklistDetail(ModelMap paramMap) throws NkiaException {
		return list("CustomizeUbitDAO.selectChecklistDetail", paramMap);
	}
	public void insertChecklist(Map<String, Object> paramMap) throws NkiaException {
		this.insert("CustomizeUbitDAO.insertChecklist", paramMap);
	}
	
	public void deleteChecklist(Map<String, Object> paramMap) throws NkiaException {
		this.delete("CustomizeUbitDAO.deleteChecklist", paramMap);
	}
	public List selectChecklist(Map<String, Object> paramMap) {
		return list("CustomizeUbitDAO.selectChecklist", paramMap);
	}
	
	public Map problemUpSrType(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.problemUpSrType", paramMap);
	}
	
	public Map selectIncTypeCd(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectIncTypeCd", paramMap);
	}
	
	public Map selectKedbInsertData(String sr_id) {
		return (Map)selectByPk("CustomizeUbitDAO.selectKedbInsertData", sr_id);
	}
	
	public Map selectKedbInsertDataIncident(String sr_id) {
		return (Map)selectByPk("CustomizeUbitDAO.selectKedbInsertDataIncident", sr_id);
	}
	
	public Map setChangeManager(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.setChangeManager", paramMap);
	}
	
	public Map selectUpServiceInfo(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectUpServiceInfo", paramMap);
	}
	
	public Map checkProcMasterCount(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.checkProcMasterCount", paramMap);
	}
	
	public Map checkCustomer(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.checkCustomer", paramMap);
	}
	public List searchSnapshotDetail(ModelMap paramMap) throws NkiaException {
		return list("CustomizeUbitDAO.searchSnapshotDetail", paramMap);
	}
	public List searchSnapshotDetailMemory(ModelMap paramMap) throws NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("CustomizeUbitDAO.searchSnapshotDetailMemory", paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
		//return list("CustomizeUbitDAO.searchSnapshotDetailMemory", paramMap);
	}
	public List searchSnapshotDetailDisk(ModelMap paramMap) throws NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("CustomizeUbitDAO.searchSnapshotDetailDisk", paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
		//return list("CustomizeUbitDAO.searchSnapshotDetailDisk", paramMap);
	}
	public List searchSnapshotDetailDB(ModelMap paramMap) throws NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		try {
			this.getSqlMapClient().queryWithRowHandler("CustomizeUbitDAO.searchSnapshotDetailDB", paramMap, handler);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return handler.getReturnList();
		//return list("CustomizeUbitDAO.searchSnapshotDetailDB", paramMap);
	}
	public List workExpectedUserlist(Map<String, Object> paramMap) {
		return list("CustomizeUbitDAO.workExpectedUserlist", paramMap);
	}
	public List<Map<String, Object>> selectSnapshotDetail(Map<String, Object> paramMap) {
		return list("CustomizeUbitDAO.selectSnapshotDetail", paramMap);
	}
	public List emsFailAlarm(Map<String, Object> paramMap) {
		return list("CustomizeUbitDAO.emsFailAlarm", paramMap);
	}
	public void deleteSnapshotDetail(Map paramMap) throws NkiaException {
		this.delete("CustomizeUbitDAO.deleteSnapshotDetail", paramMap);
	}
	public void updateSnapshotMaster(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.updateSnapshotMaster", paramMap);
	}
	
	public Map selectReqUserHpNo(String userId) {
		return (Map)selectByPk("CustomizeUbitDAO.selectReqUserHpNo", userId);
	}
	
	public void updateLinkRelEndYn(String srId) throws NkiaException {
		this.update("CustomizeUbitDAO.updateLinkRelEndYn", srId);
	}
	
	public Map countLinkChangeEnd(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.countLinkChangeEnd", paramMap);
	}
	
	public Map selectReturnYN(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectReturnYN", paramMap);
	}
	
	public Map selectExtendHopeDt(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectExtendHopeDt", paramMap);
	}
	
	public void changeSridEnddueDt(Map<String, Object> paramMap) throws NkiaException {
		this.update("CustomizeUbitDAO.changeSridEnddueDt", paramMap);
	}
	
	public int selectMyReqLoadCount(Map paramMap) {
		return (Integer)selectByPk("CustomizeUbitDAO.selectMyReqLoadCount", paramMap);
	}

	public List selectMyReqLoad(Map paramMap) {
		return list("CustomizeUbitDAO.selectMyReqLoad", paramMap);
	}
	
	public int countProcMasterUpSrId(Map paramMap) {
		return (Integer)selectByPk("CustomizeUbitDAO.countProcMasterUpSrId", paramMap);
	}
	
	public Map selectTaskNameUpSrId(Map paramMap) {
		return (Map)selectByPk("CustomizeUbitDAO.selectTaskNameUpSrId", paramMap);
	}
	
	public int selectForeignCustomer(String userId) {
		return (Integer)selectByPk("CustomizeUbitDAO.selectForeignCustomer", userId);
	}
	public int selectSnapShotBeforeOnlyCount(Map paramMap) {
		return (Integer)selectByPk("CustomizeUbitDAO.selectSnapShotBeforeOnlyCount", paramMap);
	}
}
