/*
 * @(#)NbpmInterfaceService.java              2013. 3. 29.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.customize.ubit.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;


public interface CustomizeUbitService {
	public List<Map<String, Object>> searchWorkStep(Map<String, Object> paramMap) throws NkiaException;
	
	public Map setRceptGrp(Map paramMap) throws NkiaException;
	
	public Map setMngGrp(Map paramMap) throws NkiaException;
	
	public Map selectRequstRegistDefault(Map paramMap) throws NkiaException;
	
	public Map emsAlarmStop(Map paramMap) throws NkiaException;
	
	public List<Map<String, Object>> changeReview(Map<String, Object> paramMap) throws NkiaException;
	
	public List<Map<String, Object>> changeReviewHis(Map<String, Object> paramMap) throws NkiaException;
	
	public void changeReviewInsert(Map<String, Object> paramMap) throws NkiaException;
	
	public void changeReviewUpdate(Map<String, Object> paramMap) throws NkiaException;
	
	public Map countAttendees(Map paramMap) throws NkiaException;
	
	public List<Map<String, Object>> selectAttendeesInit(Map<String, Object> paramMap) throws NkiaException;
	
	public HashMap<String, String> workStartAlarm(Map<String, Object> paramMap) throws NkiaException;
	
	public int selectPreReqLoadCount(Map paramMap) throws NkiaException;

	public List selectPreReqLoad(Map paramMap) throws NkiaException;
	
	public List selectPreReqLoadGuestGrid(Map paramMap) throws NkiaException;
	
	public List selectPreReqLoadWorkGrid(Map paramMap) throws NkiaException;
	
	public void centerGuestInUpdate(Map<String, Object> paramMap) throws NkiaException;
	
	public void centerGuestOutUpdate(Map<String, Object> paramMap) throws NkiaException;
	
	public int searchCenterListCount(Map paramMap) throws NkiaException;

	public List searchCenterList(Map paramMap) throws NkiaException;
	
	public Map emrgncyDtValid(Map paramMap) throws NkiaException;
	
	public List searchSnapshot(ModelMap paramMap) throws NkiaException;
	
	public void insertSnapshot(ModelMap paramMap) throws NkiaException;
	
	public List<Map<String, Object>> searchIncidentEmsList(Map<String, Object> paramMap) throws NkiaException;
	
	public Map selectDiff(Map paramMap) throws NkiaException;
	
	public List selectChecklistDetail(ModelMap paramMap) throws NkiaException;
	
	public void insertChecklist(ModelMap paramMap) throws NkiaException;
	
	public List selectChecklist(Map paramMap) throws NkiaException;
	
	public Map problemUpSrType(Map paramMap) throws NkiaException;
	
	public Map setChangeManager(Map paramMap) throws NkiaException;
	
	public Map checkProcMasterCount(Map paramMap) throws NkiaException;
	
	public List searchSnapshotDetail(ModelMap paramMap) throws NkiaException;
	
	public List searchSnapshotDetailMemory(ModelMap paramMap) throws NkiaException;
	
	public List searchSnapshotDetailDisk(ModelMap paramMap) throws NkiaException;
	
	public List searchSnapshotDetailDB(ModelMap paramMap) throws NkiaException;
	
	public Map checkCustomer(Map paramMap) throws NkiaException;
	
	public void deleteSnapshotDetail(ModelMap paramMap) throws NkiaException;
	
	public void snapshotRestful(ModelMap paramMap) throws NkiaException;
	
	public Map countLinkChangeEnd(Map paramMap) throws NkiaException;
	
	public Map selectReturnYN(Map paramMap) throws NkiaException;
	
	public int selectMyReqLoadCount(Map paramMap) throws NkiaException;

	public List selectMyReqLoad(Map paramMap) throws NkiaException;	
	
	public int countProcMasterUpSrId(Map paramMap) throws NkiaException;
	
	public Map selectTaskNameUpSrId(Map paramMap) throws NkiaException;
	
	public int selectForeignCustomer(String userId) throws NkiaException;
	
	public Map changeAssetRequiredValid(Map paramMap) throws NkiaException;
	
}
