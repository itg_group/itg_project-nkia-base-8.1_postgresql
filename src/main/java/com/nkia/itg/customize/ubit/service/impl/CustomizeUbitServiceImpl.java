/*
 * @(#)NbpmInterfaceServiceImpl.java              2013. 3. 29.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.customize.ubit.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.intergration.polestar.dao.IntergrationPolestarSmsDAO;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.customize.ubit.dao.CustomizeUbitDAO;
import com.nkia.itg.customize.ubit.service.CustomizeUbitService;
import com.nkia.itg.itam.automation.dao.AssetAutomationDAO;
import com.nkia.itg.nbpm.common.dao.NbpmCommonDAO;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("customizeUbitService")
public class CustomizeUbitServiceImpl implements CustomizeUbitService {

	@Resource(name = "CustomizeUbitDAO")
	private CustomizeUbitDAO CustomizeUbitDAO;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name = "nbpmCommonDAO")
	private NbpmCommonDAO nbpmCommonDAO;
	
	@Resource(name = "assetAutomationDAO")
	private AssetAutomationDAO assetAutomationDAO;
	
	@Resource(name="smsSendDAO")
	private SmsSendDAO smsSendDAO;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "intergrationPolestarSmsDAO")
	private IntergrationPolestarSmsDAO intergrationPolestarSmsDAO;
	
	@Resource(name="smsgHistDAO")
	private SmsgHistDAO smsgHistDAO; 

	public List<Map<String, Object>> searchWorkStep(Map<String, Object> paramMap) throws NkiaException {
		return CustomizeUbitDAO.searchWorkStep(paramMap);
	}
	
	public Map setRceptGrp(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.setRceptGrp(paramMap);
	}
		
	public Map setMngGrp(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.setMngGrp(paramMap);
	}
	
	@Override
	public Map selectRequstRegistDefault(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = userVO.getUser_id();
		String custId = userVO.getCust_id();
		paramMap.put("login_user_id", userId);
		paramMap.put("login_cust_id", custId);
		
		//요청양식 정보(도움말, 요청내용, 완료희망일시)
		Map workReqDocMap = CustomizeUbitDAO.selectWorkReqDoc(paramMap);
		if(workReqDocMap != null) {
			Iterator keySet = workReqDocMap.keySet().iterator();
			while(keySet.hasNext()) {
				String key = (String)keySet.next();
				returnMap.put(key, workReqDocMap.get(key));
			}
		}
		returnMap = WebUtil.lowerCaseMapKey(returnMap);

		//요청작업(CSR) - (요청작업데이터)ROLE_OPETR_02 = '사전검토자(ROLE_OPETR_02)', (요청작업데이터)ROLE_OPETR_01 = '작업관리자(ROLE_OPETR_03)' 
		/*Map roleOper = kairCustomDAO.selectpProcessRoleOper(paramMap);
		if(roleOper != null){
			
			//요청작업(CSR) - 작업관리자
			if(!"".equals((String)roleOper.get("ROLE_OPETR_01_USER_ID"))){
				returnMap.put("ROLE_OPETR_03", (String)roleOper.get("ROLE_OPETR_01_USER_NM"));
				returnMap.put("ROLE_OPETR_03_user_id", (String)roleOper.get("ROLE_OPETR_01_USER_ID"));
				returnMap.put("ROLE_OPETR_03_grp_select_yn", (String)roleOper.get("ROLE_OPETR_01_GRP_SELECT_YN"));
			}
			
			//요청작업(CSR) - 사전관리자
			
			if(!"".equals((String)roleOper.get("ROLE_OPETR_02_USER_ID"))){
				returnMap.put("ROLE_OPETR_02", (String)roleOper.get("ROLE_OPETR_02_USER_NM"));
				returnMap.put("ROLE_OPETR_02_user_id", (String)roleOper.get("ROLE_OPETR_02_USER_ID"));
				returnMap.put("ROLE_OPETR_02_grp_select_yn", (String)roleOper.get("ROLE_OPETR_02_GRP_SELECT_YN"));
			}
			
			//요청작업(CSR) - 보안검증담당자
			
			if(!"".equals((String)roleOper.get("ROLE_OPETR_21_USER_ID"))){
				returnMap.put("ROLE_OPETR_21", (String)roleOper.get("ROLE_OPETR_21_USER_NM"));
				returnMap.put("ROLE_OPETR_21_user_id", (String)roleOper.get("ROLE_OPETR_21_USER_ID"));
				returnMap.put("ROLE_OPETR_21_grp_select_yn", (String)roleOper.get("ROLE_OPETR_21_GRP_SELECT_YN"));
			}
		}*/
		
		return returnMap;
	}
	/**
	 * param userid,시작일시,ㅈ
	 */
	public Map emsAlarmStop(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
////		{
////			 "name" : "ITSM작업해제",
////			 "description" : "ITSM작업해제",
////			 "userId" : "sjyoon",
////			 "targetResource" : ["111","112"],
////			 "triggers" : [{"startTime" : 2019110612001212,
////			               "timeMin" : 120,
////			               "intervalType" : "ONCE"}
////			 			]
////		}
//
//		Map<String, Object> inJsonEmsTimeMap = new HashMap<String, Object>();
//		Date emsDate = new Date();
//		emsDate.getTime();
//		long milli = emsDate.getTime() + 10000;
//		inJsonEmsTimeMap.put("startTime", milli);  //시작시간
//		inJsonEmsTimeMap.put("timeMin", 10);    //알람막을시간(분) 종료시간-시작시간
//		inJsonEmsTimeMap.put("intervalType", "ONCE"); //ONCE고정
//		// 요청 기본정보
//		ArrayList resourceList = new ArrayList();
//		//for(int i=0;i<리소스ID만큼;ㅑ++){
//			resourceList.add("9062");
//		//}
//		Map<String, Object> inJsonEmsMap = new HashMap<String, Object>();
//		inJsonEmsMap.put("name", "변경요청작업"); //픽스
//		//inJsonEmsMap.put("userId", paramMap.get("user_id")); //사용자
//		inJsonEmsMap.put("userId", "admin"); //사용자
//		inJsonEmsMap.put("targetResource", resourceList);
//		ArrayList triggersList = new ArrayList();
//		triggersList.add(inJsonEmsTimeMap);
//		inJsonEmsMap.put("triggers", triggersList);
//		String body = JsonMapper.toJson(inJsonEmsMap);
//		
//		
//		//body = JsonMapper.toJson(body);
//		
//		String sUrl = "";
//		//sUrl = "http://ems.lotte.center/rest/v1/common/maintenancejobs";
//		sUrl = NkiaApplicationPropertiesMap.getProperty("EMS.maint");
//		URL postUrl = new URL(sUrl);
//		HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
//		conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
//		conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
//		conn.setRequestMethod("POST");
//		conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
//		conn.setConnectTimeout(3000);
//		conn.setAllowUserInteraction(false);
//		OutputStream os = conn.getOutputStream();
//		os.write(body.getBytes());
//		os.flush();
//		
//		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
//		String output;
//		String responseJson = "";
//		while ((output = br.readLine()) != null) {
//			responseJson += output;
//		}
//		conn.disconnect();
//		
//
//
//
//		
//		try {
//			returnMap= JsonMapper.fromJson(responseJson, Map.class);
//			//Gson gson = new Gson();
//			//resultVO = gson.fromJson(responseJson, ChangeVO.class);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
		
		
		
		return returnMap;
	}
	
	public List<Map<String, Object>> changeReview(Map<String, Object> paramMap) throws NkiaException {
		return CustomizeUbitDAO.changeReview(paramMap);
	}
	
	public List<Map<String, Object>> changeReviewHis(Map<String, Object> paramMap) throws NkiaException {
		return CustomizeUbitDAO.changeReviewHis(paramMap);
	}
	

	public void changeReviewInsert(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		CustomizeUbitDAO.changeReviewDelete(paramMap);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("INPUT_LIST");
				
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			mPs = new HashMap<String, Object>();
			mPs.put("sr_id", paramMap.get("sr_id"));
			mPs.put("attendees_list", paramMap.get("attendees_list")); 
			mPs.put("review_list", map.get("REVIEW_LIST")); 
			mPs.put("review_result", map.get("REVIEW_RESULT"));
			mPs.put("remarks", map.get("REMARKS")); 
			mPs.put("login_user_id", login_user_id);
			CustomizeUbitDAO.changeReviewInsert(mPs); 
		}
	}
	
	public void changeReviewUpdate(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("INPUT_LIST");
				
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			mPs = new HashMap<String, Object>();
			mPs.put("sr_id", paramMap.get("sr_id")); 
			mPs.put("review_list", map.get("REVIEW_LIST")); 
			mPs.put("review_result", map.get("REVIEW_RESULT"));
			mPs.put("remarks", map.get("REMARKS")); 
			mPs.put("login_user_id", login_user_id);
			CustomizeUbitDAO.changeReviewUpdate(mPs); 
		}
	}
	
	@Override
	public Map countAttendees(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
		//요청양식 정보(도움말, 요청내용, 완료희망일시)
		returnMap = CustomizeUbitDAO.countAttendees(paramMap);
		
		return returnMap;
	}
	
	@Override
	public List<Map<String, Object>> selectAttendeesInit(Map<String, Object> paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectAttendeesInit(paramMap);
	}
	
	public HashMap<String, String> workStartAlarm(Map<String, Object> paramMap) throws NkiaException {
		
		//CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		//NbpmCommonClient nbpmCommonClient = (NbpmCommonClient)NkiaApplicationContext.getCtx().getBean("nbpmCommonClient"); 
		PostWorkDAO postWorkDAO = (PostWorkDAO)NkiaApplicationContext.getCtx().getBean("postWorkDAO");
		CustomizeUbitDAO customizeUbitDAO = (CustomizeUbitDAO)NkiaApplicationContext.getCtx().getBean("CustomizeUbitDAO");
		
		String srId = (String) paramMap.get("sr_id");
		String popUpType = (String) paramMap.get("popUpType");
		
		boolean checkLogic = true;
		HashMap<String, String> resultMap = new HashMap<String, String>();
//		HashMap<String, String> checkMap = new HashMap<String, String>();
//		String resultMsg = messageSource.getMessage("msg.common.result.00004");
//		
//		HashMap<String, Object> procChangeMap = new HashMap<String, Object>();
//		procChangeMap = (HashMap<String, Object>) customizeUbitDAO.selectProcChangeRule(paramMap);
//		
//		HashMap<String, Object> procChangeOperMap = new HashMap<String, Object>();
//		procChangeOperMap = (HashMap<String, Object>) customizeUbitDAO.selectProcChangeOperAppm(paramMap);
//		
//		Map processDetailMap = postWorkDAO.selectProcessDetail(srId);
//		
//		SimpleDateFormat transFormatt = new SimpleDateFormat("yyyyMMddHHmm");
//		
//		String changeTyCdNm = (String) procChangeMap.get("CHANGE_TY_CD_NM");
//		String changeCategoryCd = (String) procChangeMap.get("CHANGE_CATEGORY_CD_NM");
//		String title = (String) processDetailMap.get("TITLE");		
//		//String workStrDt= transFormat.format(procChangeMap.get("WORK_STR_DT"));
//		//String workeEndDt= transFormat.format(procChangeMap.get("WORK_END_DT"));
//		String workStrDt= (String) paramMap.get("workStrDt");
//		String workEndDt= (String) paramMap.get("workEndDt");
//		String operUserNm = (String) procChangeOperMap.get("OPER_USER_NM");
//		String workState = "";
//		String workDt = "";
//		//알림톡 발송로직 들어가야하는곳
//		//1. 발송인원 셀렉트
//		Date transWorkStrDt;
//		Date transWorkEndDt;
//		List<HashMap> userList = new ArrayList();
//		if("workExpected".equals(popUpType)){
//			userList =  customizeUbitDAO.workExpectedUserlist(paramMap);
//		}else{
//			userList =  (List<HashMap>) paramMap.get("user_list");
//			workStrDt = workStrDt.replace("-", "");
//			workEndDt = workEndDt.replace("-", "");
//			transWorkStrDt = transFormatt.parse(workStrDt);
//			transWorkEndDt = transFormatt.parse(workEndDt);
//			workStrDt = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(transWorkStrDt);
//			workEndDt = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(transWorkEndDt);
//
//		}
//		
//		//작업시작, 완료, 취소에 따른 세팅값
//		if(popUpType.equals("workStart") && "workStart".equals(popUpType)){
//			workState = "시작";
//			workDt = workStrDt+" ~";
//		}else if(popUpType.equals("workEnd") && "workEnd".equals(popUpType)){
//			workState = "완료";
//			workDt = workStrDt+" ~ "+workEndDt;
//		}else if(popUpType.equals("workCancel") && "workCancel".equals(popUpType)){
//			workState = "취소";
//			workDt = workStrDt+" ~";
//		}else if(popUpType.equals("workExpected") && "workExpected".equals(popUpType)){
//			workState = "사전안내";
//			workDt = workStrDt+" ~ "+workEndDt;
//		}
//		String content = "";
//		String contentWorker = "";
//		if("workExpected".equals(popUpType)){
//			content= "UBIT센터 작업 "+workState+" 알림 문자입니다.\n▶작업구분 : "+changeTyCdNm+"\n▶작업범주 : " +changeCategoryCd+"\n▶작업내용  : "+title+"\n▶작업시간 : "+workDt+"\n▶작업자 : "+operUserNm+"\n일정 참고 부탁드립니다.";
//			contentWorker= "UBIT센터 작업 "+workState+" 알림 문자입니다.\n▶작업구분 : "+changeTyCdNm+"\n▶작업범주 : " +changeCategoryCd+"\n▶작업내용  : "+title+"\n▶작업시간 : "+workDt+"\n▶작업자 : "+operUserNm+"\n일정 참고 부탁드립니다.";
//		}else{
//			content= "UBIT센터 작업 "+workState+" 알림 문자입니다.\n▶작업구분 : "+changeTyCdNm+"\n▶작업범주 : " +changeCategoryCd+"\n▶작업내용  : "+title+"\n▶작업시간 : "+workDt+"\n▶작업자 : "+operUserNm;
//		}
//		
//		//2. 발송내용체크
//			//2-1 유형에따라 템플릿코드 셋팅 - CAB참석자 정보에 맞춰서 수정필요
//		String templteCode = "LMSG_20170911172320836200";
//		String senderKey = "ef834c839e4a9c83ac3e027b0ed782a7545014d4";;
//		String sendPhone = "0226264210";
//
//	/*	
//		//2-2  유형에따라 rebody 앞에 prefix 붙여줌   [자바스크립트에서 진행]
////		String rebody = "";
///*		String content = "ITSM 업무 알림입니다. \n";
//		content += "▶고객사 : 변경요청\n";
//		content += "▶등급 : 등급";
//		content += "▶";
//		content += "▶제목 :" +(String)processDetail.get("TITLE")+"\n";
//		content += "업무 진행 바랍니다.";
//		smsInfo.put("CONTENT", content);*/
//		//3. 알림톡테이블 등록
//		String hp_no = "";
//		for(HashMap userData: userList){
//			hp_no = (String) userData.get("HP_NO");
//			hp_no = hp_no.replace("-", "");
//			userData.put("TO_PHONE", hp_no); // 받는사람전화번호
////			if("workExpected".equals(popUpType)){
////				if("2".equals(userData.get("TARGET"))){
////					userData.put("MSG_BODY", contentWorker); //알림톡 메세지
////				}else{
////					userData.put("MSG_BODY", content); //알림톡 메세지
////				}
////			}else{
////				userData.put("MSG_BODY", content); //알림톡 메세지
////			}
//			userData.put("MSG_BODY", content); //알림톡 메세지
//			userData.put("RE_BODY", "UBIT센터 작업 "+workState+" 알림 문자입니다."); //실패시 보내는 대체 문자메세지
//			userData.put("TEMPLATE_CODE", templteCode); //장애전파 템플릿코드
//			userData.put("SENDER_KEY", senderKey); // 센더키
//			userData.put("SEND_PHONE", sendPhone); // 보내는사람
//			userData.put("USER_NM", userData.get("USER_NM")); 
//			smsSendDAO.insertTalkList(userData);
//			smsgHistDAO.insertTalkListHistory(userData);
//		}
//		//4. 히스토리테이블등록
//		//smsGrpDAO.insertMessageListHistory(paramMap);
//		
//		resultMap.put("RESULT_MSG", resultMsg);
		return resultMap;
	}
	
	public int selectPreReqLoadCount(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectPreReqLoadCount(paramMap);
	}

	public List selectPreReqLoad(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectPreReqLoad(paramMap);
	}
	
	public List selectPreReqLoadGuestGrid(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectPreReqLoadGuestGrid(paramMap);
	}
	
	public List selectPreReqLoadWorkGrid(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectPreReqLoadWorkGrid(paramMap);
	}
	
	public void centerGuestInUpdate(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> updateMap = null;

		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("ROWS");
				
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			updateMap = new HashMap<String, Object>();
			updateMap.put("sr_id", map.get("SR_ID"));
			updateMap.put("seq", map.get("SEQ")); 
			updateMap.put("card_number", map.get("CARD_NUMBER")); 
			//updateMap.put("login_user_id", login_user_id);
			CustomizeUbitDAO.centerGuestInUpdate(updateMap); 
		}
	}
	
	public void centerGuestOutUpdate(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> updateMap = null;
	
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("ROWS");
				
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			updateMap = new HashMap<String, Object>();
			updateMap.put("sr_id", map.get("SR_ID"));
			updateMap.put("seq", map.get("SEQ")); 
			//updateMap.put("login_user_id", login_user_id);
			CustomizeUbitDAO.centerGuestOutUpdate(updateMap); 
		}
	}
	
	public int searchCenterListCount(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.searchCenterListCount(paramMap);
	}

	public List searchCenterList(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.searchCenterList(paramMap);
	}
	
	public Map emrgncyDtValid(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.emrgncyDtValid(paramMap);
	}
	
	public List searchSnapshot(ModelMap paramMap) throws NkiaException {
		ArrayList temp = (ArrayList)paramMap.get("gridData");
		ArrayList tempList = new ArrayList();
		for(int i=0;i<temp.size();i++){
			tempList.add(((Map)temp.get(i)).get("CONF_ID"));
		}
		HashMap param = new HashMap();
		param.put("conf_id",tempList);
		param.put("sr_id",(String)paramMap.get("srId"));
		List resultList =CustomizeUbitDAO.searchSnapshot(param); 
		return resultList;
	}
	
	public void insertSnapshot(ModelMap paramMap) throws NkiaException {
		Map<String, Object> snapshotParam = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		CustomizeUbitDAO.deleteSnapshot(paramMap);
		
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("SNAPSHOT_LIST");
				
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			snapshotParam = new HashMap<String, Object>();
			snapshotParam.put("SR_ID", paramMap.get("SR_ID"));
			snapshotParam.put("CONF_ID", map.get("CONF_ID")); 
			snapshotParam.put("SERVER_SNAPSHOT", map.get("SERVER_SNAPSHOT")); 
			snapshotParam.put("SERVER_CONFIG", map.get("SERVER_CONFIG"));
			snapshotParam.put("NETWORK_CONFIG", map.get("NETWORK_CONFIG")); 
			snapshotParam.put("DB_CONFIG", map.get("DB_CONFIG"));
			snapshotParam.put("WAS_CONFIG", map.get("WAS_CONFIG"));
			snapshotParam.put("UPD_USER_ID", login_user_id);
			CustomizeUbitDAO.insertSnapshot(snapshotParam); 
		}
		CustomizeUbitDAO.changeUpdateSnapshot(paramMap);
	}
	
	public List<Map<String, Object>> searchIncidentEmsList(Map<String, Object> paramMap) throws NkiaException {
		return intergrationPolestarSmsDAO.searchIncidentEmsList(paramMap);
	}
	
	public Map selectDiff(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectDiff(paramMap);
	}
	
	
	public List selectChecklistDetail(ModelMap paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectChecklistDetail(paramMap); 
	}
	
	public void insertChecklist(ModelMap paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		CustomizeUbitDAO.deleteChecklist(paramMap);
		if(null != paramMap.get("SERVERINFO")){
			Map<String, Object> checklistParam = new HashMap<String, Object>();
			checklistParam.put("CHECKLIST_TYPE", ((Map)paramMap.get("SERVERINFO")).get("TYPE"));
			checklistParam.put("SR_ID", paramMap.get("SR_ID"));
			checklistParam.put("UPD_USER_ID", login_user_id);
			checklistParam.put("CHECK_GROUP_CD", ((Map)paramMap.get("SERVERINFO")).get("CHECK_GROUP_CD"));
			checklistParam.put("VERSION", ((Map)paramMap.get("SERVERINFO")).get("VERSION"));
			CustomizeUbitDAO.insertChecklist(checklistParam);
		}
		if(null != paramMap.get("NETWORKINFO")){
			Map<String, Object> checklistParam = new HashMap<String, Object>();
			checklistParam.put("CHECKLIST_TYPE", ((Map)paramMap.get("NETWORKINFO")).get("TYPE"));
			checklistParam.put("SR_ID", paramMap.get("SR_ID"));
			checklistParam.put("UPD_USER_ID", login_user_id);
			checklistParam.put("CHECK_GROUP_CD", ((Map)paramMap.get("NETWORKINFO")).get("CHECK_GROUP_CD"));
			checklistParam.put("VERSION", ((Map)paramMap.get("NETWORKINFO")).get("VERSION"));
			CustomizeUbitDAO.insertChecklist(checklistParam);
		}
		if(null != paramMap.get("DBINFO")){
			Map<String, Object> checklistParam = new HashMap<String, Object>();
			checklistParam.put("CHECKLIST_TYPE", ((Map)paramMap.get("DBINFO")).get("TYPE"));
			checklistParam.put("SR_ID", paramMap.get("SR_ID"));
			checklistParam.put("UPD_USER_ID", login_user_id);
			checklistParam.put("CHECK_GROUP_CD", ((Map)paramMap.get("DBINFO")).get("CHECK_GROUP_CD"));
			checklistParam.put("VERSION", ((Map)paramMap.get("DBINFO")).get("VERSION"));
			CustomizeUbitDAO.insertChecklist(checklistParam);
		}
				
		
	}
	public List selectChecklist(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectChecklist(paramMap);
	}
	
	public Map problemUpSrType(Map paramMap) throws NkiaException {
		Map<String, Object> upSrType = null;
		upSrType = CustomizeUbitDAO.problemUpSrType(paramMap);
		
		Map<String, Object> resultMap = null;
		//resultMap = CustomizeUbitDAO.selectIncTypeCd(paramMap);
		if(upSrType!=null){
			if(upSrType.get("REQ_TYPE").equals("INCIDENT")){
				resultMap = CustomizeUbitDAO.selectIncTypeCd(paramMap);		
				resultMap.put("upSrType", upSrType.get("REQ_TYPE"));
				
				return resultMap;
			}else{
				return upSrType;					
			}
		}else{
			return resultMap;
		}
		
		
	}

	public Map setChangeManager(Map paramMap) throws NkiaException {
		Map<String, Object> upSrType = null;
		upSrType = CustomizeUbitDAO.problemUpSrType(paramMap);
		
		Map<String, Object> resultMap = null;
		//resultMap = CustomizeUbitDAO.selectIncTypeCd(paramMap);
		if(upSrType!=null){
			if(upSrType.get("REQ_TYPE").equals("SERVICE")){
				resultMap = CustomizeUbitDAO.selectUpServiceInfo(paramMap);		
				resultMap.put("center_id", resultMap.get("CENTER_ID"));
				resultMap.put("req_id", resultMap.get("REQ_DOC_ID"));
				resultMap = CustomizeUbitDAO.setMngGrp(resultMap);

				if(resultMap != null){
					resultMap.put("UP_SR_REQ_TYPE", upSrType.get("REQ_TYPE"));
				}else{
					resultMap = new HashMap();
					resultMap.put("OPER_MNGE_01_ID", "");
					resultMap.put("GRP_SELECT_YN", "");
					resultMap.put("UP_SR_REQ_TYPE", upSrType.get("REQ_TYPE"));
				}
				//서비스요청의 서비스대상고객사  - 고객사로 바꿔서 put
				String upSrId = (String) paramMap.get("upSrId");
				Map processDetailMap = postWorkDAO.selectProcessDetail(upSrId);					
				resultMap.put("CUSTOMER_ID", (String)processDetailMap.get("SERVICE_TRGET_CUSTOMER_ID"));
				resultMap.put("CUSTOMER_NM", (String)processDetailMap.get("SERVICE_TRGET_CUSTOMER_ID_NM"));
			}else{
				resultMap = new HashMap();
				resultMap.put("OPER_MNGE_01_ID", "");
				resultMap.put("GRP_SELECT_YN", "");
				resultMap.put("UP_SR_REQ_TYPE", upSrType.get("REQ_TYPE"));
			}
		}	
		return resultMap;
		
	}		
	
	public Map checkProcMasterCount(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
		//요청양식 정보(도움말, 요청내용, 완료희망일시)
		returnMap = CustomizeUbitDAO.checkProcMasterCount(paramMap);
		
		return returnMap;
	}
	public List searchSnapshotDetail(ModelMap paramMap) throws NkiaException {
		return CustomizeUbitDAO.searchSnapshotDetail(paramMap);
	}
	public List searchSnapshotDetailMemory(ModelMap paramMap) throws NkiaException {
		return CustomizeUbitDAO.searchSnapshotDetailMemory(paramMap);
	}
	public List searchSnapshotDetailDisk(ModelMap paramMap) throws NkiaException {
		return CustomizeUbitDAO.searchSnapshotDetailDisk(paramMap);
	}
	public List searchSnapshotDetailDB(ModelMap paramMap) throws NkiaException {
		return CustomizeUbitDAO.searchSnapshotDetailDB(paramMap);
	}

	public Map checkCustomer(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
		returnMap = CustomizeUbitDAO.checkCustomer(paramMap);
		
		return returnMap;
	}
	public void deleteSnapshotDetail(ModelMap paramMap) throws NkiaException {
		ArrayList temp = (ArrayList)paramMap.get("gridData");
		ArrayList tempList = new ArrayList();
		for(int i=0;i<temp.size();i++){
			tempList.add(((Map)temp.get(i)).get("CONF_ID"));
		}
		HashMap param = new HashMap();
		param.put("conf_id",tempList);
		param.put("sr_id",(String)paramMap.get("srId"));
		CustomizeUbitDAO.deleteSnapshotDetail(param);
		CustomizeUbitDAO.updateSnapshotMaster(param);
	}
	
	public void snapshotRestful(ModelMap paramMap) throws NkiaException {
		List<Map<String, Object>> snapshotList = new ArrayList();
		snapshotList =  CustomizeUbitDAO.selectSnapshotDetail(paramMap);
		for(int z=0;z <snapshotList.size();z++){
			Map<String, Object> resourceMap = new HashMap<String, Object>();
			resourceMap.put("resourceId", snapshotList.get(z).get("EMS_ID"));  //EMS ID 숫자형
			resourceMap.put("resourceType", "SERVER");    //"SERVER"고정
			resourceMap.put("requestType",  snapshotList.get(z).get("SNAPSHOT")); 
			// 요청 기본정보
			Map<String, Object> inJsonEmsMap = new HashMap<String, Object>();
			inJsonEmsMap.put("taskId", paramMap.get("sr_id")); //SRID
			inJsonEmsMap.put("flag", "work"); //plan /work  < 작업전 후
			ArrayList resourceList = new ArrayList();
			resourceList.add(resourceMap);
			inJsonEmsMap.put("resourceInfo", resourceList);
			String body = JsonMapper.toJson(inJsonEmsMap);
			
			
//			//body = JsonMapper.toJson(body);
//			
//			String sUrl = "";
//			//sUrl = "http://tems.lotte.center/rest/v1/itsm/snapshot";
//			//sUrl = "http://10.231.128.93:8723/rest/v1/itsm/snapshot";
//			sUrl = NkiaApplicationPropertiesMap.getProperty("EMS.snapshot");
//			URL postUrl = new URL(sUrl);
//			HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
//			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
//			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
//			conn.setRequestMethod("POST");
//			conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
//			conn.setConnectTimeout(3000);
//			conn.setAllowUserInteraction(false);
//			OutputStream os = conn.getOutputStream();
//			os.write(body.getBytes());
//			os.flush();
//			
//			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
//			String output;
//			String responseJson = "";
//			while ((output = br.readLine()) != null) {
//				System.out.println(output);
//				responseJson += output;
//			}
//			conn.disconnect();
//			PreChangeResultThreadExecutor snapshot = new PreChangeResultThreadExecutor() ;
//			snapshot.setBody(body);
//			Thread t = new Thread(snapshot);
//			t.start();
		}
	}
	
	@Override
	public Map countLinkChangeEnd(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
		returnMap = CustomizeUbitDAO.countLinkChangeEnd(paramMap);
		
		return returnMap;
	}
	
	@Override
	public Map selectReturnYN(Map paramMap) throws NkiaException {
		
		Map returnMap = new HashMap();
		
		returnMap = CustomizeUbitDAO.selectReturnYN(paramMap);
		
		return returnMap;
	}
	@Override
	public int selectMyReqLoadCount(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectMyReqLoadCount(paramMap);
	}

	@Override
	public List selectMyReqLoad(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectMyReqLoad(paramMap);
	}
	
	@Override
	public int countProcMasterUpSrId(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.countProcMasterUpSrId(paramMap);
	}
	
	public Map selectTaskNameUpSrId(Map paramMap) throws NkiaException {
		return CustomizeUbitDAO.selectTaskNameUpSrId(paramMap);
	}
	
	@Override
	public int selectForeignCustomer(String userId) throws NkiaException {
		return CustomizeUbitDAO.selectForeignCustomer(userId);
	}
	
	@Override
	public Map changeAssetRequiredValid(Map paramMap) throws NkiaException {
		//Map workReqDocMap = CustomizeUbitDAO.selectWorkReqDoc(paramMap);
		return CustomizeUbitDAO.selectWorkReqDoc(paramMap);
	}
}
