/*
 * @(#)NbpmProviderController.java              2013. 6. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.customize.ubit.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.customize.ubit.service.CustomizeUbitService;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.user.dao.UserDAO;
import com.nkia.itg.base.application.util.excel.ExcelMaker;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class CustomizeUbitController {

	@Resource(name="customizeUbitService")
	private CustomizeUbitService customizeUbitService;
	
	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "userDAO")
	public UserDAO userDAO;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	 /**
	 * 체크리스트 윈도우팝업을 오픈한다.
	 * @exception Exception
	 */
	@RequestMapping(value="/itg/customize/ubit/openWindowPopup.do")
	public String openWindowPopup(HttpServletRequest request, @RequestParam Map<String, Object> reqMap, ModelMap paramMap) throws NkiaException {
		if (null == reqMap.get("popupId") || "".equals(reqMap.get("popupId"))) {
			throw new NkiaException("popupId 는 필수입력입니다.");
		}
		String popupId = StringUtil.replaceNull(String.valueOf(reqMap.get("popupId")), "");
		paramMap.put("reqMap", reqMap);
		String forwarPage = "/itg/workflow/workReqDoc/popup/" + popupId + ".nvf";
		return forwarPage;
	}	

	/**
	 * 작업절차 그리드 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/customize/ubit/searchWorkStep.do")
	public @ResponseBody ResultVO searchWorkStep(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = customizeUbitService.searchWorkStep(paramMap);		
			//totalCount = workCheckListService.searchWorkCheckListCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자그룹 셋팅
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/setRceptGrp.do")
	public @ResponseBody ResultVO setRceptGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try {
			resultMap = customizeUbitService.setRceptGrp(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 관리자그룹 셋팅
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/setMngGrp.do")
	public @ResponseBody ResultVO setMngGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try {
			resultMap = customizeUbitService.setMngGrp(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 요청등록 호출시 요청등록 기본값 세팅
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/selectRequstRegistDefault.do")
	public @ResponseBody ResultVO selectRequstRegistDefault(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		String reqUserId = "";
		try {
			
			//요청내용, 도움말, 완료희망일시, 요청부서승인자
			resultMap = customizeUbitService.selectRequstRegistDefault(paramMap);
			
			reqUserId = (String)paramMap.get("req_user_id");
			//고객사 정보
			UserVO reqUserVO = userDAO.selectUserInfo(reqUserId);
			
			if(reqUserVO != null) {
				String userDept = reqUserVO.getDetailInfo();				
				resultMap.put("customer_id", reqUserVO.getCustomer_id());
				resultMap.put("customer_nm", reqUserVO.getCustomer_nm());
			}
			//대외고객사의 경우 서비스대상고객사 셋팅
			int foreign_customer = customizeUbitService.selectForeignCustomer(reqUserId);
			String foreign_customer_yn = "N";
			if(foreign_customer>0){
				resultMap.put("service_trget_customer_id", reqUserVO.getCustomer_id());
				resultMap.put("service_trget_customer_id_nm", reqUserVO.getCustomer_nm());
				foreign_customer_yn = "Y";
			}
			resultMap.put("foreign_customer_yn", foreign_customer_yn);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경관리시 EMS에 변경작업시간동안 정지 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/ubit/emsAlramStop.do")
	public @ResponseBody ResultVO emsAlramStop(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try {
			
			//요청내용, 도움말, 완료희망일시, 요청부서승인자
			resultMap = customizeUbitService.emsAlarmStop(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 심의항목 그리드 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/customize/ubit/changeReview.do")
	public @ResponseBody ResultVO changeReview(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
						
			List<Map<String, Object>> resultList = customizeUbitService.changeReview(paramMap);		
		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 심의항목 이력 그리드 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/customize/ubit/changeReviewHis.do")
	public @ResponseBody ResultVO changeReviewHis(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
						
			List<Map<String, Object>> resultList = customizeUbitService.changeReviewHis(paramMap);		
		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 심의항목 그리드 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/customize/ubit/changeReviewInsert.do")
	public @ResponseBody ResultVO changeReviewInsert(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			customizeUbitService.changeReviewInsert(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 심의항목 그리드 저장
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/customize/ubit/changeReviewUpdate.do")
	public @ResponseBody ResultVO changeReviewUpdate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			customizeUbitService.changeReviewUpdate(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 심의항목 그리드 저장
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/customize/ubit/countAttendees.do")
	public @ResponseBody ResultVO countAttendees(@RequestBody ModelMap paramMap) throws NkiaException {		
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try{
			resultMap = customizeUbitService.countAttendees(paramMap);
			resultVO.setResultMap(resultMap);	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 참석자 초기리스트 (통보그룹)
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/customize/ubit/selectAttendeesInit.do")
	public @ResponseBody ResultVO selectAttendeesInit(@RequestBody ModelMap paramMap) throws NkiaException {		
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try{
			GridVO gridVO = new GridVO();			
			List<Map<String, Object>> resultList = customizeUbitService.selectAttendeesInit(paramMap);		
		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 알림대상자설정 -  작업시작 알림톡
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/customize/ubit/workStartAlarm.do")
	public @ResponseBody ResultVO workStartAlarm(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try{
			resultMap = customizeUbitService.workStartAlarm(paramMap);
			resultVO.setResultMap(resultMap);
			//resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//센터 출입요청 - 이전요청 가져오기
	@RequestMapping(value="/itg/customize/ubit/selectPreReqLoad.do")
	public @ResponseBody ResultVO selectPreReqLoad(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			paramMap.put("user_id", userId);
			
			totalCount = customizeUbitService.selectPreReqLoadCount(paramMap);
			resultList = customizeUbitService.selectPreReqLoad(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//센터출입요청 출입자정보 그리드셋팅
	@RequestMapping(value="/itg/customize/ubit/selectPreReqLoadGuestGrid.do")
	public @ResponseBody ResultVO selectPreReqLoadGuestGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			resultList = customizeUbitService.selectPreReqLoadGuestGrid(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//센터출입요청 작업절차 그리드셋팅
		@RequestMapping(value="/itg/customize/ubit/selectPreReqLoadWorkGrid.do")
		public @ResponseBody ResultVO selectPreReqLoadWorkGrid(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			ResultVO resultVO = new ResultVO();
			GridVO gridVO = new GridVO();
			try{
				resultList = customizeUbitService.selectPreReqLoadWorkGrid(paramMap);
				gridVO.setRows(resultList);
				
				resultVO.setGridVO(gridVO);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 센터출입요청 - 출/퇴실처리 출입자정보그리드 출입버튼
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/centerGuestInUpdate.do")
		public @ResponseBody ResultVO centerGuestInUpdate(@RequestBody ModelMap paramMap) throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try{
				customizeUbitService.centerGuestInUpdate(paramMap);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 센터출입요청 - 출/퇴실처리 출입자정보그리드 퇴실버튼
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/centerGuestOutUpdate.do")
		public @ResponseBody ResultVO centerGuestOutUpdate(@RequestBody ModelMap paramMap) throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try{
				customizeUbitService.centerGuestOutUpdate(paramMap);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 센터출입요청 - 센터출입대기함
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/goCenterListTab.do")
		public String goCenterListTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
			String forwarPage = "/itg/nbpm/customize/UBIT/nvf/centerListTab.nvf";
			
			return forwarPage;
		}
		
		@RequestMapping(value="/itg/customize/ubit/goCenterListMain.do")
		public String goMyListMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
			String forwarPage = "/itg/nbpm/customize/UBIT/nvf/centerListMain.nvf";
			
			return forwarPage;
		}
			
		@RequestMapping(value="/itg/customize/ubit/searchCenterList.do")
			public @ResponseBody ResultVO searchCenterList(@RequestBody ModelMap paramMap) throws NkiaException {
				List resultList = new ArrayList();
				GridVO gridVO = new GridVO();
				ResultVO result = new ResultVO();
				try{
					UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					String userId = userVO.getUser_id();
					String custId = userVO.getCust_id();
					
					int totalCount = 0;
					
					paramMap.put("user_id", userId);
					paramMap.put("cust_id", custId);
					PagingUtil.getFristEndNum(paramMap);
					
					totalCount = customizeUbitService.searchCenterListCount(paramMap);
					resultList = customizeUbitService.searchCenterList(paramMap);
					
					gridVO.setTotalCount(totalCount);
					gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
					gridVO.setRows(resultList);
					
					result.setGridVO(gridVO);
					result.setSuccess(true); 
					
				} catch(Exception e) {
					throw new NkiaException(e);
				}
				return result;
			}
		
		@RequestMapping(value="/itg/customize/ubit/centerListExcelDown.do")
		public @ResponseBody ResultVO centerListExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
			String login_user_id = "";
			UserVO userVO = new UserVO();
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			List resultList = new ArrayList();
			ResultVO resultVO = new ResultVO();
			ModelMap excelMap = new ModelMap();
			
			Map excelParam = (Map) paramMap.get("excelParam");
			Map temp = (Map) paramMap.get("param");
			
			try{
				//page=1, start=0, limit=10 있는 경우, 
				resultList = customizeUbitService.searchCenterList(temp);
				excelMap = excelMaker.makeExcelFile(excelParam, resultList);				
				resultVO.setResultMap(excelMap);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
			
		}	
		
		/**
		 * 서비스요청 - 긴급요청일 시간 validation
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/emrgncyDtValid.do")
		public @ResponseBody ResultVO emrgncyDtValid(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try {
				resultMap = customizeUbitService.emrgncyDtValid(paramMap);
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 변경요청 - 스냅샷설정조회
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/searchSnapshot.do")
		public @ResponseBody ResultVO searchSnapshot(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				resultList = customizeUbitService.searchSnapshot(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		
		/**
		 * 변경요청 - 스냅샷설정저장
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/insertSnapshot.do")
		public @ResponseBody ResultVO insertSnapshot(@RequestBody ModelMap paramMap) throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try{
				customizeUbitService.insertSnapshot(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));	
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 장애요청 관련이벤트 그리드
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/searchIncidentEmsList.do")
		public @ResponseBody ResultVO searchIncidentEmsList(@RequestBody ModelMap paramMap) throws NkiaException {		
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try{
				GridVO gridVO = new GridVO();			
				List<Map<String, Object>> resultList = customizeUbitService.searchIncidentEmsList(paramMap);		
			
				gridVO.setRows(resultList);
				
				resultVO.setGridVO(gridVO);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 스냅샷 비교페이지 호출
	     * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/customize/ubit/snapshotDiff.do")
		public String goChargerGrpManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
			String forwarPage = "/itg/customize/diff.nvf";
			String sr_id = request.getParameter("srId");
			String rtype = request.getParameter("rtype");
			String attr = request.getParameter("attr");
			String instance = request.getParameter("instance");
			instance = instance.replaceAll("\\\\", "//");
			String ems_id = request.getParameter("ems_id");
			paramMap.addAttribute("sr_id",sr_id);
			paramMap.addAttribute("attr",attr);
			paramMap.addAttribute("rtype",rtype);
			paramMap.addAttribute("instance",instance);
			paramMap.addAttribute("ems_id",ems_id);
			return forwarPage;
		}
		
		/**
		 * 담당자 그룹관리페이지 호출(운영업무관리메뉴)
	     * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/customize/ubit/selectDiff.do")
		public @ResponseBody ResultVO selectDiff(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try {
				resultMap = customizeUbitService.selectDiff(paramMap);
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}		
		
		/**
		 * 변경요청 - 체크리스트조회
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/selectChecklistDetail.do")
		public @ResponseBody ResultVO selectChecklistDetail(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				resultList = customizeUbitService.selectChecklistDetail(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		/**
		 * 변경요청 - 체크리스트등록
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/insertChecklist.do")
		public @ResponseBody ResultVO insertChecklist(@RequestBody ModelMap paramMap) throws NkiaException {
			ResultVO resultVO = new ResultVO();
			try{
				customizeUbitService.insertChecklist(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));	
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		/**
		 * 변경관리 - 체크리스트조회
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/selectChecklist.do")
		public @ResponseBody ResultVO selectChecklist(@RequestBody ModelMap paramMap)throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				resultList = customizeUbitService.selectChecklist(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		
		/**
		 * 문제관리 상위요청이 장애일경우 유형값 셋팅
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/customize/ubit/problemUpSrType.do")
		public @ResponseBody ResultVO problemUpSrType(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try {
				resultMap = customizeUbitService.problemUpSrType(paramMap);
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 변경관리 관리자 셋팅(변경관리 상위요청이 서비스요청인경우)
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/customize/ubit/setChangeManager.do")
		public @ResponseBody ResultVO setChangeManager(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try {
				resultMap = customizeUbitService.setChangeManager(paramMap);
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 연계요청 - 하위요청이 등록된요청인지 확인
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */	
		@RequestMapping(value="/itg/customize/ubit/checkProcMasterCount.do")
		public @ResponseBody ResultVO checkProcMasterCount(@RequestBody ModelMap paramMap) throws NkiaException {		
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try{
				resultMap = customizeUbitService.checkProcMasterCount(paramMap);
				resultVO.setResultMap(resultMap);	
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		/**
		 * 로그인유저 고객사에 따른 서비스상세요청트리 변경 - 내부, 외부 고객사인지 체크
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */	
		@RequestMapping(value="/itg/customize/ubit/checkCustomer.do")
		public @ResponseBody ResultVO checkCustomer(@RequestBody ModelMap paramMap) throws NkiaException {		
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try{
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				String userId = userVO.getUser_id();
				
				paramMap.put("userId", userId);
				resultMap = customizeUbitService.checkCustomer(paramMap);
				resultVO.setResultMap(resultMap);	
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 변경요청 - 스냅샷설정상세조회
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/searchSnapshotDetail.do")
		public @ResponseBody ResultVO searchSnapshotDetail(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				resultList = customizeUbitService.searchSnapshotDetail(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		/**
		 * 변경요청 - 스냅샷설정상세조회 -MEMORY
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/searchSnapshotDetailMemory.do")
		public @ResponseBody ResultVO searchSnapshotDetailMemory(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				resultList = customizeUbitService.searchSnapshotDetailMemory(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		
		/**
		 * 변경요청 - 스냅샷설정상세조회 -DISK
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/searchSnapshotDetailDisk.do")
		public @ResponseBody ResultVO searchSnapshotDetailDisk(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				resultList = customizeUbitService.searchSnapshotDetailDisk(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		/**
		 * 변경요청 - 스냅샷설정상세조회 -DB
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/searchSnapshotDetailDB.do")
		public @ResponseBody ResultVO searchSnapshotDetailDB(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				resultList = customizeUbitService.searchSnapshotDetailDB(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		/**
		 * 변경요청 - 스냅샷설정삭제로직
	     * @param paramMap
		 * @return
		 * @
		 */	
		@RequestMapping(value="/itg/customize/ubit/deleteSnapshotDetail.do")
		public @ResponseBody ResultVO deleteSnapshotDetail(@RequestBody ModelMap paramMap) throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				customizeUbitService.deleteSnapshotDetail(paramMap);
				gridVO.setRows(resultList);
				result.setGridVO(gridVO);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		/**
		 * 변경관리 - 스냅샷 작업후 restful 호출
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/snapshotRestful.do")
		public @ResponseBody ResultVO snapshotRestful(@RequestBody ModelMap paramMap)throws NkiaException {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			ResultVO result = new ResultVO();
			try{
				customizeUbitService.snapshotRestful(paramMap);
				result.setSuccess(true); 
				
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return result;
		}
		
		/**
		 * 서비스요청 하위연계 완료상태 조회
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/countLinkChangeEnd.do")
		public @ResponseBody ResultVO countLinkChangeEnd(@RequestBody ModelMap paramMap) throws NkiaException {		
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try{
				resultMap = customizeUbitService.countLinkChangeEnd(paramMap);
				resultVO.setResultMap(resultMap);	
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 서비스요청 요청등록 화면 반려된건지 아닌지 확인(반려인경우 취소종료 버튼 추가)
	     * @param paramMap
		 * @return
		 * @
		 */		
		@RequestMapping(value="/itg/customize/ubit/selectReturnYN.do")
		public @ResponseBody ResultVO selectReturnYN(@RequestBody ModelMap paramMap) throws NkiaException {		
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			try{
				resultMap = customizeUbitService.selectReturnYN(paramMap);
				resultVO.setResultMap(resultMap);	
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		
		//마감시간변경 요청 - 내 요청 가져오기
		@RequestMapping(value="/itg/customize/ubit/selectMyReqLoad.do")
		public @ResponseBody ResultVO selectMyReqLoad(@RequestBody ModelMap paramMap) throws NkiaException {
			
			
			List resultList = new ArrayList();
			ResultVO resultVO = new ResultVO();
			GridVO gridVO = new GridVO();
			
			try{
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				String userId = "";
				userId = userVO.getUser_id();
				
				int totalCount = 0;
				PagingUtil.getFristEndNum(paramMap);
				paramMap.put("login_user_id", userId);
				
				totalCount = customizeUbitService.selectMyReqLoadCount(paramMap);
				resultList = customizeUbitService.selectMyReqLoad(paramMap);
				
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
				gridVO.setRows(resultList);
				
				resultVO.setGridVO(gridVO);
			} catch(Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
		
		/**
		 * 내 고객사 버튼 셋팅
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/customize/ubit/selectCustomerInfo.do")
		public @ResponseBody ResultVO selectCustomerInfo(@RequestBody ModelMap paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			String reqUserId = "";
			try {
				reqUserId = (String)paramMap.get("req_user_id");
				UserVO reqUserVO = userDAO.selectUserInfo(reqUserId);
				
				if(reqUserVO != null) {
					String userDept = reqUserVO.getDetailInfo();				
					resultMap.put("customer_id", reqUserVO.getCustomer_id());
					resultMap.put("customer_nm", reqUserVO.getCustomer_nm());
				}
				
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}		
		
		/**
		 * 변경요청등록시 장비반입인경우 구성정보 필수해제
		 * @param paramMap
		 * @return
		 * @throws NkiaException
		 */
		@RequestMapping(value="/itg/customize/ubit/changeAssetRequiredValid.do")
		public @ResponseBody ResultVO changeAssetRequiredValid(@RequestBody Map paramMap)throws NkiaException {
			ResultVO resultVO = new ResultVO();
			Map resultMap = new HashMap();
			String reqUserId = "";
			try {
				resultMap = customizeUbitService.changeAssetRequiredValid(paramMap);
				resultVO.setResultMap(resultMap);
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			return resultVO;
		}
}
