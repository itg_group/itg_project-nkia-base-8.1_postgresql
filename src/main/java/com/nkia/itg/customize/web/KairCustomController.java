package com.nkia.itg.customize.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.customize.service.KairCustomService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class KairCustomController {
	
	@Resource(name = "kairCustomService")
	private KairCustomService kairCustomService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@RequestMapping(value="/itg/customize/searchDeptConfmer.do")
	public @ResponseBody ResultVO searchDeptConfmer(@RequestBody ModelMap paramMap, HttpServletRequest request) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			String req_Id = (String)paramMap.get("req_id");
			Map resultMap = kairCustomService.selectDeptConfmer(req_Id);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * HIST 시스템의 ID를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/customize/selectHistId.do")
	public @ResponseBody ResultVO selectHistId(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = kairCustomService.selectHistId(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 요청등록 호출시 요청등록 기본값 세팅
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/customize/selectRequstRegistDefault.do")
	public @ResponseBody ResultVO selectRequstRegistDefault(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map resultMap = new HashMap();
		try {
			
			//요청내용, 도움말, 완료희망일시, 요청부서승인자
			resultMap = kairCustomService.selectRequstRegistDefault(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}*/
}
