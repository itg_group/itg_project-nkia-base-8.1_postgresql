/*
 * @(#)HomeController.java              2013. 5. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.home.web;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.service.CertificationService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.home.service.HomeService;
import com.nkia.itg.nbpm.mylist.service.MyListExecutor;
import com.nkia.itg.nbpm.servicedesk.service.ServicedeskListExecutor;
import com.nkia.itg.system.board.service.DownBoardService;
import com.nkia.itg.system.board.service.NoticeBoardService;
import com.nkia.itg.system.dept.service.DeptService;
import com.nkia.itg.system.process.service.ProcessAuthService;
import com.nkia.itg.system.system.service.SystemService;
import com.nkia.itg.workflow.kedb.service.KedbService;
import com.nkia.itg.workflow.myhistory.service.MyHistoryService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class HomeController {
	
	@Resource(name = "noticeboardService")
	private NoticeBoardService boardService;
	
	@Resource(name = "downboardService")
	private DownBoardService dnboardService;
	
	@Resource(name="myListCommonExecutor")
	private MyListExecutor myListExecutor;
	
	@Resource(name="servicedeskCommonListExecutor")
	private ServicedeskListExecutor servicedeskListExecutor;
		
	@Resource(name="myHistoryService")
	private MyHistoryService myHistoryService;
	
	@Resource(name="certificationService")
	private CertificationService certificationService;
	
	@Resource(name = "processAuthService")
	private ProcessAuthService processAuthService;
	
	@Resource(name = "kedbService")
	private KedbService kedbService;
	
	@Resource(name = "systemService")
	private SystemService systemService;
	
	@Resource(name = "homeService")
	private HomeService homeService;
	
	@Resource(name = "deptService")
	private DeptService deptService;

	/**
	 * 
	 * Home
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/goHome.do")
	public String goHome(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "";
		String isServicedesk = request.getParameter("isServicedesk");
		paramMap.put("isServicedesk", isServicedesk);
		
		if("true".equals(isServicedesk)) {
			forwarPage = NkiaApplicationPropertiesMap.getProperty("Globals.Servicedesk.Url");
		} else {
			forwarPage = NkiaApplicationPropertiesMap.getProperty("Globals.Home.Url");
		}
		
		return "redirect:" + forwarPage;
	}
	
	/**
	 * 
	 * MyHome
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value="/itg/home/goMyHomeMain.do")
	public String goMyHomeMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException, SQLException {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("user_id", userVO.getUser_id());
    		paramMap.put("cust_id", userVO.getCust_id());
    	}
		
    	List<HashMap> processAuthList = processAuthService.searchUserProcessAuthList(paramMap);
		
		paramMap.put("first_num", 1);
		paramMap.put("last_num", 6);
		
		// 공지사항 리스트 
		List<HashMap> noticeList = (ArrayList<HashMap>) boardService.searchBoardList(paramMap);
		List<HashMap> downList = (ArrayList<HashMap>) dnboardService.searchBoardList(paramMap);
		
		paramMap.put("last_num", 9999999);
		
		//요청함 수량
		int readyCount = myListExecutor.searchReadyMyListCount(paramMap);
		int ingCount = myListExecutor.searchIngMyListCount(paramMap);
		int completeCount = myListExecutor.searchCompleteMyListCount(paramMap);
		boolean isServiceDesk = false;

		for(int i = 0; i < processAuthList.size(); i++){
			if(processAuthList.get(i).get("proc_auth_id").equals("SERVICEDESK")){
				isServiceDesk = true;
			}
		}
		
		List<HashMap> readyList;
		List<HashMap> obstacleList;
		
		if(isServiceDesk){
			
			readyCount = servicedeskListExecutor.searchReadyServicedeskListCount(paramMap);
			ingCount = servicedeskListExecutor.searchIngServicedeskListCount(paramMap);
			completeCount = servicedeskListExecutor.searchCompleteServicedeskListCount(paramMap);
			
			//대기함 리스트
			
			paramMap.put("last_num", 6);
			readyList = (ArrayList<HashMap>) servicedeskListExecutor.searchReadyServicedeskList(paramMap);
			
			paramMap.put("req_type","INCDNT");
			
			//장애 리스트
			obstacleList = (ArrayList<HashMap>) servicedeskListExecutor.searchReadyServicedeskList(paramMap);
		} else {
			
			readyCount = myListExecutor.searchReadyMyListCount(paramMap);
			ingCount = myListExecutor.searchIngMyListCount(paramMap);
			completeCount = myListExecutor.searchCompleteMyListCount(paramMap);
			
			//대기함 리스트
			
			paramMap.put("last_num", 6);
			readyList = (ArrayList<HashMap>) myListExecutor.searchReadyMyList(paramMap);
			
			paramMap.put("req_type","INCDNT");

			//장애 리스트
			obstacleList = (ArrayList<HashMap>) myListExecutor.searchReadyMyList(paramMap);
		}
		
		//KEDB 리스트
		paramMap.put("codeGrpId", "GRP_ID");
		paramMap.put("codeId", "PROBM_CAUSE_TY_CD");
		paramMap.put("categoryCodeId", "PROBM_CAUSE_TY_CD");
		List<HashMap> kedbList = kedbService.searchKedbList(paramMap); 
		
		request.setAttribute("rsl", noticeList);
		request.setAttribute("dnl", downList);
		request.setAttribute("readyList", readyList);
		request.setAttribute("obstacleList", obstacleList);
		request.setAttribute("readyCnt",readyCount);
		request.setAttribute("ingCnt",ingCount);
		request.setAttribute("completeCnt",completeCount);
		request.setAttribute("kedbList", kedbList);
		request.setAttribute("isServiceDesk", isServiceDesk);

		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		if( userVO != null ){
			paramMap.put("user_id", userVO.getUser_id());
			paramMap.put("USER_ID", userVO.getUser_id());
			paramMap.put("USER_NM", userVO.getUser_nm());
			paramMap.put("USER_AUTH", userVO.getGrpAuthList());
			paramMap.put("USER_SYSAUTH", systemService.searchUserSystemAuthList(paramMap));
		} else {
			paramMap.put("USER_ID", "");
			paramMap.put("USER_NM", "");
			paramMap.put("USER_AUTH", new ArrayList());
			paramMap.put("USER_SYSAUTH", new ArrayList());
			paramMap.put("USER_PUSH", "N");
		}
		
		String reftenceDay = NkiaApplicationPropertiesMap.getProperty("Nbpm.Process.ReferenceDay");
		paramMap.put("reftenceDay", reftenceDay);

		//당일 프로세스현황
		List<HashMap> myProcessList = (ArrayList<HashMap>) homeService.selectMyProcessList(paramMap);
		request.setAttribute("myProcessList", myProcessList);

		// 화면저장값
		HashMap mainStateInfo = (HashMap) homeService.selectMainStateList(paramMap);
		if(mainStateInfo == null){
			String defaultState = NkiaApplicationPropertiesMap.getProperty("User.default.UserState.Val");
			request.setAttribute("mainStateList", defaultState);
		}else{
			request.setAttribute("mainStateList", mainStateInfo.get("USER_STATE"));
		}

		// 나의할일 리스트 초기 페이징셋팅
		paramMap.put("startNum", 1);
		paramMap.put("endNum", 30);
		
		List<HashMap> myTodoList = (ArrayList<HashMap>) homeService.selectMyTodoList(paramMap);
		request.setAttribute("myTodoList", myTodoList);
		
		String forwardPage = null;
		String site = NkiaApplicationPropertiesMap.getProperty("Globals.itsm.site");
		if( site != null && !"".equals(site)){
			forwardPage = "/itg/customize/" + site + "/main.nvf";
    	}else{
    		forwardPage = "/itg/customize/polestar_itg/main.nvf";
    	}
		
		return forwardPage;
	}
	/**
	 * 나의작업함
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/home/myJobList.do")
	public @ResponseBody ResultVO myJobList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			if( userVO != null ){
				paramMap.put("user_id", userVO.getUser_id());
				paramMap.put("USER_ID", userVO.getUser_id());
				paramMap.put("CUST_ID", userVO.getCust_id());
				paramMap.put("USER_NM", userVO.getUser_nm());
				paramMap.put("USER_AUTH", userVO.getGrpAuthList());
				paramMap.put("USER_SYSAUTH", systemService.searchUserSystemAuthList(paramMap));
			} else {
				paramMap.put("USER_ID", "");
				paramMap.put("CUST_ID", "");
				paramMap.put("USER_NM", "");
				paramMap.put("USER_AUTH", new ArrayList());
				paramMap.put("USER_SYSAUTH", new ArrayList());
				paramMap.put("USER_PUSH", "N");
			}
			
			String reftenceDay = NkiaApplicationPropertiesMap.getProperty("Nbpm.Process.ReferenceDay");
			paramMap.put("reftenceDay", reftenceDay);
			
			//나의작업함
			List<HashMap> myJobList = (ArrayList<HashMap>) homeService.selectMyJobList(paramMap);
			gridVO.setRows(myJobList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 나의요청함
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/home/myReqList.do")
	public @ResponseBody ResultVO myReqList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
		
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			if( userVO != null ){
				paramMap.put("user_id", userVO.getUser_id());
				paramMap.put("USER_ID", userVO.getUser_id());
				paramMap.put("CUST_ID", userVO.getCust_id());
				paramMap.put("USER_NM", userVO.getUser_nm());
				paramMap.put("USER_AUTH", userVO.getGrpAuthList());
				paramMap.put("USER_SYSAUTH", systemService.searchUserSystemAuthList(paramMap));
			} else {
				paramMap.put("USER_ID", "");
				paramMap.put("CUST_ID", "");
				paramMap.put("USER_NM", "");
				paramMap.put("USER_AUTH", new ArrayList());
				paramMap.put("USER_SYSAUTH", new ArrayList());
				paramMap.put("USER_PUSH", "N");
			}
			
			//나의요청함
			List<HashMap> myReqList = (ArrayList<HashMap>) homeService.selectMyReqList(paramMap);
			gridVO.setRows(myReqList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 나의할일리스트
     * @param paramMap
	 * @return
	 * @basicCompList
	 */
	@RequestMapping(value="/itg/home/myTodoList.do")
	public @ResponseBody ResultVO myTodoList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
		
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			if( userVO != null ){
				paramMap.put("user_id", userVO.getUser_id());
				paramMap.put("USER_ID", userVO.getUser_id());
				paramMap.put("CUST_ID", userVO.getCust_id());
				paramMap.put("USER_NM", userVO.getUser_nm());
				paramMap.put("USER_AUTH", userVO.getGrpAuthList());
				paramMap.put("USER_SYSAUTH", systemService.searchUserSystemAuthList(paramMap));
			} else {
				paramMap.put("USER_ID", "");
				paramMap.put("CUST_ID", "");
				paramMap.put("USER_NM", "");
				paramMap.put("USER_AUTH", new ArrayList());
				paramMap.put("USER_SYSAUTH", new ArrayList());
				paramMap.put("USER_PUSH", "N");
			}
			int pageNo = (int) paramMap.get("pageNo"); //클릭 페이징넘버
			int countPerPage = 10; //현재화면 보여지는 갯수
			
			int startNum = ((pageNo - 1) * countPerPage) + 1; 
			int endNum = startNum + countPerPage - 1;
			
			paramMap.put("startNum", startNum);
			paramMap.put("endNum", endNum);
			
			List resultList = homeService.selectMyTodoList(paramMap);
			totalCount = homeService.selectMyTodoListCnt(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setPage( String.valueOf((int)paramMap.get("pageNo")));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 나의요청함
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/home/noticeList.do")
	public @ResponseBody ResultVO noticeList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
		
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			if( userVO != null ){
				paramMap.put("user_id", userVO.getUser_id());
				paramMap.put("USER_ID", userVO.getUser_id());
				paramMap.put("CUST_ID", userVO.getCust_id());
				paramMap.put("USER_NM", userVO.getUser_nm());
				paramMap.put("USER_AUTH", userVO.getGrpAuthList());
				paramMap.put("USER_SYSAUTH", systemService.searchUserSystemAuthList(paramMap));
			} else {
				paramMap.put("USER_ID", "");
				paramMap.put("CUST_ID", "");
				paramMap.put("USER_NM", "");
				paramMap.put("USER_AUTH", new ArrayList());
				paramMap.put("USER_SYSAUTH", new ArrayList());
				paramMap.put("USER_PUSH", "N");
			}
			//공지사항
			List<HashMap> notiList = (ArrayList<HashMap>) homeService.selectNotiList(paramMap);
			gridVO.setRows(notiList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	/**
	 * 나의요청함
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/home/processCnt.do")
	public @ResponseBody ResultVO processCnt(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
		
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			if( userVO != null ){
				paramMap.put("user_id", userVO.getUser_id());
				paramMap.put("USER_ID", userVO.getUser_id());
				paramMap.put("CUST_ID", userVO.getCust_id());
				paramMap.put("USER_NM", userVO.getUser_nm());
				paramMap.put("USER_AUTH", userVO.getGrpAuthList());
				paramMap.put("USER_SYSAUTH", systemService.searchUserSystemAuthList(paramMap));
			} else {
				paramMap.put("USER_ID", "");
				paramMap.put("CUST_ID", "");
				paramMap.put("USER_NM", "");
				paramMap.put("USER_AUTH", new ArrayList());
				paramMap.put("USER_SYSAUTH", new ArrayList());
				paramMap.put("USER_PUSH", "N");
			}
			//당일 프로세스현황
			List<HashMap> myProcessList = (ArrayList<HashMap>) homeService.selectMyProcessList(paramMap);
			gridVO.setRows(myProcessList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}		

	/**
	 * 나의할일리스트
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/home/basicCompList.do")
	public @ResponseBody ResultVO basicCompList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			GridVO gridVO = new GridVO();
			//컴포넌트 기본정보
			HashMap resultMap = new HashMap();
			List resultList  = homeService.selectMainCompList(paramMap);
			resultMap.put("resultList", resultList);
			resultVO.setResultMap(resultMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	/**
	 * 메인화면 커스텀 저장
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/home/saveCustomMain.do")
	public @ResponseBody ResultVO searchNoticeBoardPopupList(@RequestBody ModelMap paramMap)throws NkiaException {
		
		ResultVO resultVO = new ResultVO();
		try {
			String userId = "";
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		userId = userVO.getUser_id();
	    		paramMap.put("user_id", userId);
	    	}
	    	if(userId != ""){
	    		//delete insert구조
	    		homeService.saveCustomMain(paramMap);
	    	}
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
