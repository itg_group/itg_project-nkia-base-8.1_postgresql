/*
 * @(#)CertificationServiceImpl.java              2013. 4. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.home.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.home.dao.HomeDAO;
import com.nkia.itg.home.service.HomeService;

@Service("homeService")
public class HomeServiceImpl implements HomeService {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeServiceImpl.class);

	@Resource(name="homeDAO")
	public HomeDAO homeDAO;
	
	public List selectMyJobList(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectMyJobList(paramMap);
	}
	
	public List selectMyReqList(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectMyReqList(paramMap);
	}
	
	public List selectMyProcessList(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectMyProcessList(paramMap);
	}
	
	public List selectNotiList(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectNotiList(paramMap);
	}
	
	public List selectMainCompList(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectMainCompList(paramMap);
	}
	/*public List selectStatisList(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectStatisList(paramMap); 
	}*/
	public Map selectMainStateList(ModelMap paramMap) throws NkiaException {
		return (Map)homeDAO.selectMainStateList(paramMap);
	}
		
	public Map selectStatisInfo(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectStatisInfo(paramMap);
	}
	
	public List selectMyTodoList(ModelMap paramMap) throws NkiaException {
		return homeDAO.selectMyTodoList(paramMap);
	}
	
	public int selectMyTodoListCnt(ModelMap paramMap) throws NkiaException{
		return homeDAO.selectMyTodoListCnt(paramMap);
	}
	
	public void saveCustomMain(ModelMap paramMap) throws NkiaException{
		homeDAO.deleteCustomMain(paramMap);
		homeDAO.saveCustomMain(paramMap);
	}
}
