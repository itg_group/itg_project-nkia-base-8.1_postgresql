/*
 * @(#)HomeService.java              2013. 4. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.home.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface HomeService {

	public List selectMyJobList(ModelMap paramMap) throws NkiaException;

	public List selectMyReqList(ModelMap paramMap) throws NkiaException;

	public List selectMyProcessList(ModelMap paramMap) throws NkiaException;

	public List selectNotiList(ModelMap paramMap) throws NkiaException;
	
	public List selectMainCompList(ModelMap paramMap) throws NkiaException;
	
	public Map selectMainStateList(ModelMap paramMap) throws NkiaException;
	
//	public List selectStatisList(ModelMap paramMap) throws NkiaException;
	
	public Map selectStatisInfo(ModelMap paramMap) throws NkiaException;
	
	public List selectMyTodoList(ModelMap paramMap) throws NkiaException;

	public int selectMyTodoListCnt(ModelMap paramMap) throws NkiaException;
	
	public void saveCustomMain(ModelMap paramMap) throws NkiaException;

}
