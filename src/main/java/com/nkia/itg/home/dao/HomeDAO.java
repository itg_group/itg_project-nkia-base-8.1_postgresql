/*
 * @(#)HomeDAO.java              2018. 3. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.home.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("homeDAO")
public class HomeDAO extends EgovComAbstractDAO{

	public List selectMyJobList(Map paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("HomeDAO.selectMyJobList", paramMap);
		return resultList;
	}

	public List selectMyReqList(ModelMap paramMap) {
		List resultList = null;
		resultList = list("HomeDAO.selectMyReqList", paramMap);
		return resultList;
	}

	public List selectMyProcessList(ModelMap paramMap) {
		List resultList = null;
		resultList = list("HomeDAO.selectMyProcessList", paramMap);
		return resultList;
	}
	
	public List selectNotiList(ModelMap paramMap) {
		List resultList = null;
		resultList = list("HomeDAO.selectNotiList", paramMap);
		return resultList;
	}

	public List selectMainCompList(ModelMap paramMap) {
		return list("HomeDAO.selectMainCompList", paramMap);
	}
	
	public Map selectMainStateList(ModelMap paramMap) {
		return(Map)selectByPk("HomeDAO.selectMainStateList", paramMap);
	}
	
//	public List selectStatisList(ModelMap paramMap) {
//		List resultList = null;
//		resultList = list("HomeDAO.selectStatisList", paramMap);
//		return resultList;
//	}
	
	public Map selectStatisInfo(ModelMap paramMap) throws NkiaException {
		return (Map) this.selectByPk("HomeDAO.selectStatisInfo", paramMap);
	}
	
	public List selectMyTodoList(ModelMap paramMap) {
		List resultList = null;
		resultList = list("HomeDAO.selectMyTodoList", paramMap);
		return resultList;
	}

	public int selectMyTodoListCnt(ModelMap paramMap) {
		return (Integer) selectByPk("HomeDAO.selectMyTodoListCnt", paramMap);
	}
	public void deleteCustomMain(ModelMap paramMap) {
		delete("HomeDAO.deleteCustomMain", paramMap);
	}		
	public void saveCustomMain(ModelMap paramMap) {
		insert("HomeDAO.saveCustomMain", paramMap);
	}	
}
 