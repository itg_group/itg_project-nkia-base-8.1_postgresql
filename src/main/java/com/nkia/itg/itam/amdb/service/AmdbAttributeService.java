/**
 * 
 */
package com.nkia.itg.itam.amdb.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 1.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface AmdbAttributeService {

	/**
	 * @param paramMap
	 * @return
	 */
	public int searchAmAttributeGridListCount(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 * @return
	 */
	public List searchAmAttributeGridList(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 */
	public void updateAmAttributeInfo(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 */
	public void deleteAmAttributeInfo(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 */
	public void insertAmAttributeInfo(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertUserGroupInfo(ModelMap paramMap) throws NkiaException;
	
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public int selectAttributeFieldNm(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAttributeInfo(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectUserGroupCnt(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteUserGroupInfo(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public List selectPopupList(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectGenTableList(ModelMap paramMap) throws NkiaException;

	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectGenTableCnt(ModelMap paramMap) throws NkiaException;
}
