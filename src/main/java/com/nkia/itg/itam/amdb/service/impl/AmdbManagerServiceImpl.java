/**
 * 
 */
package com.nkia.itg.itam.amdb.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.amdb.dao.AmdbManagerDAO;
import com.nkia.itg.itam.amdb.service.AmdbManagerService;
import com.nkia.itg.itam.opms.util.OpmsUtil;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("amdbManagerService")
public class AmdbManagerServiceImpl implements AmdbManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(AmdbManagerServiceImpl.class);
	
	@Resource(name = "amdbManagerDAO")
	public AmdbManagerDAO amdbManagerDAO;
	  
	@Resource(name = "opmsUtil")
	public OpmsUtil opmsUtil;
	
	
	
	@Override
	public List selectClassEntityTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectClassEntityTree(paramMap);
	}

	@Override
	public List selectEntityList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectEntityList(paramMap);
	}

	@Override
	public List selectTblList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectTblList(paramMap);
	}

	@Override
	public List selectTblSelList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectTblSelList(paramMap);
	}

	@Override
	public List selectTblJoinList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectTblJoinList(paramMap);
	}

	@Override
	public List selectColList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectColList(paramMap);
	}

	@Override
	public List selectColSelList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectColSelList(paramMap);
	}

	@Override
	public void insertEntityInfo(ModelMap paramMap) throws NkiaException {
		amdbManagerDAO.inserEntityInfo(paramMap);
	}
	
	@Override
	public void generateEntityId(ModelMap paramMap) throws NkiaException {
		paramMap.put("entity_id", amdbManagerDAO.generateEntityId());
	}
	
	@Override
	public void insertTblInfo(ModelMap paramMap) throws NkiaException {
		
		List tblArray 		= (ArrayList)paramMap.get("sel_tbl_grid");
		String entity_id	= (String)paramMap.get("entity_id");
		String set_type		= (String)paramMap.get("set_type");
		
		int cnt = amdbManagerDAO.selectTblCount(paramMap);
		if(cnt>0){
			amdbManagerDAO.deleteTblInfo(paramMap);
		}
		for(int i=0;i<tblArray.size();i++){
			HashMap map = (HashMap)tblArray.get(i);
			map.put("ENTITY_ID", entity_id);
			map.put("SET_TYPE", set_type);
			map.put("SORT", i);
			amdbManagerDAO.insertTblInfo(map);
		}
	}
	
	@Override
	public void insertJoinInfo(ModelMap paramMap) throws NkiaException {
		List tblArray 		= (ArrayList)paramMap.get("sel_join_grid");
		String entity_id	= (String)paramMap.get("entity_id");
		String set_type		= (String)paramMap.get("set_type");
		
		int cnt = amdbManagerDAO.selectJoinCount(paramMap);
		if(cnt>0){
			amdbManagerDAO.deleteJoinInfo(paramMap);
		}
		for(int i=0;i<tblArray.size();i++){
			HashMap map = (HashMap)tblArray.get(i);
			map.put("ENTITY_ID", entity_id);
			map.put("SET_TYPE", set_type);
			map.put("SORT", i);
			amdbManagerDAO.insertJoinInfo(map);
		}
	}
	
	@Override
	public void insertColInfo(ModelMap paramMap) throws NkiaException {
		
		List tblArray 		= (ArrayList)paramMap.get("sel_col_grid");
		String entity_id	= (String)paramMap.get("entity_id");
		
		int cnt = amdbManagerDAO.selectColCount(paramMap);
		if(cnt>0){
			amdbManagerDAO.deleteColInfo(paramMap);
		}
		for(int i=0;i<tblArray.size();i++){
			HashMap map = (HashMap)tblArray.get(i);
			map.put("ENTITY_ID", entity_id);
			map.put("SORT", i);
			amdbManagerDAO.insertColInfo(map);
		}
	}

	@Override
	public void updateChangeOrder(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List tblArray 		= (ArrayList)paramMap.get("grp_grid");
		
		for(int i=0;i<tblArray.size();i++){
			HashMap map = (HashMap)tblArray.get(i);
			map.put("SORT_ORDER", i+1);
			amdbManagerDAO.updateChangeOrder(map);
		}
	}

	@Override
	public HashMap selectGroupInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectGroupInfo(paramMap);
	}

	@Override
	public List selectInstallList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectInstallList(paramMap);
	}

	@Override
	public List selectInstallSelList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectInstallSelList(paramMap);
	}

	@Override
	public List selectRelList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List returnList = new ArrayList();
		
		//hmsong cmdb 개선 작업 Start
		//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		String confAdminYn = new String();
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		paramMap.put("confAdminYn", confAdminYn); 
		
		if("Y".equals(confAdminYn)) {
			// 구성용도를 관계정보에 매핑하게 됨. 구성용도는 멀티코드에서 관리합니다.
			paramMap.put("map_code_id", "CONF_TYPE"); 
		} else {
			// 기존 ITSM 방법. 자산분류체계를 관계정보에 매핑하게 됨. 관계정보에 매핑할 자산분류체계는 AM_REL_CODE 코드에서 정의된 것만 매핑 가능합니다.
			paramMap.put("map_code_id", "AM_REL_CODE"); 
		}
		
		returnList = amdbManagerDAO.selectRelList(paramMap);
		
		return returnList;
	}

	@Override
	public List selectRelSelList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbManagerDAO.selectRelSelList(paramMap);
	}

	@Override
	public void insertInstallInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List tblArray 		= (ArrayList)paramMap.get("sel_install_grid");
		String entity_id	= (String)paramMap.get("entity_id");
		
		int cnt = amdbManagerDAO.selectIntallCount(paramMap);
		if(cnt>0){
			amdbManagerDAO.deleteIntallInfo(paramMap);
		}
		for(int i=0;i<tblArray.size();i++){
			HashMap map = (HashMap)tblArray.get(i);
			map.put("ENTITY_ID", entity_id);
			map.put("SORT", i);
			amdbManagerDAO.insertInstallInfo(map);
		}
	}

	@Override
	public void insertRelInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		List tblArray 		= (ArrayList)paramMap.get("sel_rel_grid");
		String entity_id	= (String)paramMap.get("entity_id");
		
		int cnt = amdbManagerDAO.selectRelCount(paramMap);
		if(cnt>0){
			amdbManagerDAO.deleteRelInfo(paramMap);
		}
		for(int i=0;i<tblArray.size();i++){
			HashMap map = (HashMap)tblArray.get(i);
			map.put("ENTITY_ID", entity_id);
			map.put("SORT", i);
			amdbManagerDAO.insertRelInfo(map);
		}
	}

	@Override
	public void deleteAllEntityInfo(ModelMap paramMap) throws NkiaException {
		
		// TODO Auto-generated method stub
		List nodeList = amdbManagerDAO.selectChildNodeList(paramMap);
		
		if(nodeList != null){
			for(int i=0;i<nodeList.size();i++){
				HashMap map = (HashMap)nodeList.get(i);
				map.put("entity_id", (String)map.get("ENTITY_ID"));
				amdbManagerDAO.deleteEntityInfo(map);
				amdbManagerDAO.deleteTblInfo(map);
				amdbManagerDAO.deleteJoinInfo(map);
				amdbManagerDAO.deleteColInfo(map);
				amdbManagerDAO.deleteIntallInfo(map);
				amdbManagerDAO.deleteRelInfo(map);
			}
		}
	}

	@Override
	public void updateEntityInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		
		amdbManagerDAO.updateEntityInfo(paramMap);
		
		String use_yn = (String)paramMap.get("use_yn");
		
		if("N".equals(use_yn)){
			List nodeList = amdbManagerDAO.selectChildNodeList(paramMap);
			if(nodeList.size()>1){
				for(int i=1;i<nodeList.size();i++){
					HashMap map = (HashMap)nodeList.get(i);
					map.put("entity_id", (String)map.get("ENTITY_ID"));
					map.put("use_yn",use_yn);
					amdbManagerDAO.updateEntityUseYn(map);
				}
			}
		}
	}

	@Override
	public HashMap selectCompCodeCnt(ModelMap paramMap) throws NkiaException {
		
		// TODO Auto-generated method stub
		List result = amdbManagerDAO.selectCompCodeCnt(paramMap);
		
		String resultMsg = "";
		if(result != null){
			for(int i=0;i<result.size();i++){
				HashMap map = (HashMap)result.get(i);
				resultMsg += "[ " + (String)map.get("ENTITY_NM_PATH") + " ]" + "<br>";
			}
			
			paramMap.put("resultMsg", resultMsg);
			paramMap.put("resultCnt", result.size());
		}

		return paramMap;
	}
	
	@Override
	public List selectEntityCopyList(HashMap paramMap)throws NkiaException{
		return amdbManagerDAO.selectEntityCopyList(paramMap);
	}
	
	@Override
	public int selectEntityCopyCnt(HashMap paramMap)throws NkiaException{
		return amdbManagerDAO.selectEntityCopyCnt(paramMap);
	}
	
	@Override
	public List selectCopyEntityTree(HashMap paramMap)throws NkiaException{
		return amdbManagerDAO.selectCopyEntityTree(paramMap);
	}

	@Override
	public void insertCopyEntity(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		
		String classId = StringUtil.parseString(paramMap.get("class_id"));
		String taClassId = StringUtil.parseString(paramMap.get("target_class_id"));
		String userId = StringUtil.parseString(paramMap.get("user_id"));
		
		//정보가 존재한다면 삭제한다.
		deleteCopyAllEntityInfo(taClassId);
		
		//복사에 해다하는 속성들을 가져온다.
		List<HashMap> targetEntityList = amdbManagerDAO.selectCopyEntityList(paramMap);
		
		//기존 id와 새로생성된 id가 맵핑되어있는 리스트를 만든다.
		List<HashMap> idList = selectCopyIdGentList(targetEntityList);
		
		//엔터티 정보를 복사해서 넣는다..
		for(int i=0 ; i<targetEntityList.size() ; i++){
			
			HashMap dataMap = targetEntityList.get(i);
			//키를 소문자로 전환해준다..
			dataMap = opmsUtil.replaceRowerKey(dataMap);
			String upEntityId = StringUtil.parseString(dataMap.get("up_entity_id"));
			String entityId = StringUtil.parseString(dataMap.get("entity_id"));
			
			dataMap.put("user_id", userId);
			
			if(upEntityId.equalsIgnoreCase(classId)){
				//class_id 와 같다면 ... taclass_id 로 넣어준다..
				dataMap.put("up_entity_id", taClassId);
				//entity_id 를 비교해서 entity_id 에 새로 생성된 id를 넣어준다
				compareIdSet(idList,entityId,dataMap,"entity_id");
			}else{
				//entity_id 를 비교해서 entity_id 에 새로 생성된 id를 넣어준다
				compareIdSet(idList,entityId,dataMap,"entity_id");
				//up_entity_id를 entity_id 에서 찾아서 해당하는 새로운 id를 up_entity_id에 넣어준다..
				compareIdSet(idList,upEntityId,dataMap,"up_entity_id");
			}
			
			//속성 정보를 넣는다.
			amdbManagerDAO.inserEntityInfo(dataMap);
		}
		//맵핑정보등을 복사해서 넣는다.
		insertCopyEntityOption(idList);
	}
	
	@Override
	public List selectCopyIdGentList(List<HashMap> targetEntityList)throws NkiaException{
		
		List<HashMap> idList = new ArrayList<HashMap>();
		
		//id를 생성 하는 로직..
		for(int i=0 ; i<targetEntityList.size() ; i++){
			HashMap dataMap = targetEntityList.get(i);
			
			dataMap = opmsUtil.replaceRowerKey(dataMap);
			HashMap idMap = new HashMap();
			String ta_entity_id = amdbManagerDAO.generateEntityId();
			String upEntityId = StringUtil.parseString(dataMap.get("up_entity_id"));
			String entityId = StringUtil.parseString(dataMap.get("entity_id"));
			
			idMap.put("entity_id", entityId);
			idMap.put("up_entity_id", upEntityId);
			idMap.put("ta_entity_id", ta_entity_id);
			
			idList.add(idMap);
		}
		return idList;
	}
	
	@Override
	public void compareIdSet(List<HashMap> idList,String compareIds,HashMap dataMap,String setKey)throws NkiaException{
		
		//비교할 아이디와 id를 비교해서 setKey 로 Map에 넣어준다.
		for(int j=0;j<idList.size();j++){
			HashMap idMap = idList.get(j);
			String ids = StringUtil.parseString(idMap.get("entity_id"));
			if(compareIds.equalsIgnoreCase(ids)){
				dataMap.put(setKey,idMap.get("ta_entity_id"));
				break;
			}
		}
	}
	
	@Override
	public void insertCopyEntityOption(List<HashMap> idList)throws NkiaException{
		for(int i=0;i<idList.size();i++){
			HashMap dataMap = idList.get(i);
			//속성 맵핑정보 등록
			amdbManagerDAO.inserCopyAttrMapping(dataMap);
			//설치정보 등록
			amdbManagerDAO.inserCopyCompMapping(dataMap);
			//연계장비속성 등록
			amdbManagerDAO.inserCopyRelMapping(dataMap);
			//테이블 정보 등록
			amdbManagerDAO.inserCopyTblMapping(dataMap);
			//조인 정보 등록
			amdbManagerDAO.inserCopyTblJoin(dataMap);
		}
	}
	
	@Override
	public void deleteCopyAllEntityInfo(String class_id)throws NkiaException{
		
		//존재하면.. 삭제
		int cnt = amdbManagerDAO.selectCopyEntityCnt(class_id);
		
		//모든 정보 삭제
		if(cnt > 0){
			//entity 정보 삭제
			amdbManagerDAO.deleteCopyEntityInfo(class_id);
			//속성 맵핑정보 삭제
			amdbManagerDAO.deleteCopyAttrMapping(class_id);
			//설치 정보 설정 삭제
			amdbManagerDAO.deleteCopyCompMapping(class_id);
			//연계구성장비정보 삭제
			amdbManagerDAO.deleteCopyRelMapping(class_id);
			//테이블 맵핑정보 삭제
			amdbManagerDAO.deleteCopyTblMapping(class_id);
			//테이블 조인 정보 삭제
			amdbManagerDAO.deleteCopyTblJoin(class_id);
		}
	}
	
	@Override
	public void insertBtnInfo(ModelMap paramMap) throws NkiaException {
		List tblArray = (ArrayList)paramMap.get("sel_btn_grid");
		String entity_id = (String)paramMap.get("entity_id");
		
		int cnt = amdbManagerDAO.selectBtnCount(paramMap);
		if( cnt > 0 ){
			amdbManagerDAO.deleteBtnInfo(paramMap);
		}
		for( int i = 0; i < tblArray.size(); i++){
			HashMap map = (HashMap)tblArray.get(i);
			map.put("ENTITY_ID", entity_id);
			map.put("SORT", i);
			amdbManagerDAO.insertBtnInfo(map);
		}
	}
	
	@Override
	public List searchEntityBtnList(ModelMap paramMap) throws NkiaException {
		return amdbManagerDAO.searchEntityBtnList(paramMap);
	}

	
	/* *ITAM 뷰테이블 생성* Start */
	
	@Override
	public Integer makeItamViewtableListSelectCount(ModelMap paramMap) throws NkiaException {
		return amdbManagerDAO.makeItamViewtableListSelectCount(paramMap);
	}

	@Override
	public List makeItamViewtableListSelect(ModelMap paramMap) throws NkiaException {
		return amdbManagerDAO.makeItamViewtableListSelect(paramMap);
	}

	// PARAM : class_kind , class_type
	@Override
	public HashMap makeItamViewtableDetailRecord(ModelMap paramMap) throws NkiaException {
		HashMap mapResult = amdbManagerDAO.makeItamViewtableDetailRecord(paramMap);
		if ( mapResult != null ) {
			mapResult.put("MAKE_QUERY", StringUtil.parseString(mapResult.get("MAKE_QUERY1")) 
										+ StringUtil.parseString(mapResult.get("MAKE_QUERY2")) 
										+ StringUtil.parseString(mapResult.get("MAKE_QUERY3"))
						);
		}
		return mapResult;
	}

	@Override
	public boolean makeItamViewtableDetailSave(ModelMap paramMap) throws NkiaException {
		boolean bResult = true;
		
		// MAKE_QUERY > MAKE_QUERY1~3 ( CLOB 대신 처리 함 )
		String strQry1 = "", strQry2 = "", strQry3 = "";
		String [] arrFnlQry = new String [3];
		arrFnlQry[0] = "";
		arrFnlQry[1] = "";
		arrFnlQry[2] = "";
		
		String [] arrQry = StringUtil.parseString(paramMap.get("MAKE_QUERY")).split("\n");
		Integer nGrp = 0;
		
		for ( int r = 0 ; r < arrQry.length ; r++) {
			if ( (arrFnlQry[nGrp] + "\n" + arrQry[r]).getBytes().length > 3900 ) {
				nGrp++;
			}
			arrFnlQry[nGrp] += ( r == 0 ? "" : "\n" ) + arrQry[r];
		}
		
		paramMap.put("MAKE_QUERY1",arrFnlQry[0]);
		paramMap.put("MAKE_QUERY2",arrFnlQry[1]);
		paramMap.put("MAKE_QUERY3",arrFnlQry[2]);
		
		// 업데이트일자 적용 여부
		String strUpddtUpdateYn = StringUtil.parseString(paramMap.get("UPD_DT_NOUPDATE"));
		if ( strUpddtUpdateYn.equals("") == true ) {strUpddtUpdateYn = "N";}
		paramMap.put("UPD_DT_NOUPDATE",strUpddtUpdateYn);
		
		amdbManagerDAO.makeItamViewtableDetailSave(paramMap);
		
		return bResult;
	}

	@Override
	public HashMap makeItamViewtableDetailMakeQuery(ModelMap paramMap) throws NkiaException {
		HashMap mapResult = new HashMap();
		String strFinalQuery = "";
		String strField = "";
		String strFrom = "";
		String strBatchYn = StringUtil.parseString(paramMap.get("batch_work_yn"));
		String strBatchRemakeQueryYn = StringUtil.parseString(paramMap.get("AUTO_REMAKE_YN"));
		String strTempClassKind = StringUtil.parseString(paramMap.get("class_kind"));
		String strTempClassType = StringUtil.parseString(paramMap.get("class_type"));
		String strAllColumnList = ""; // 컬럼 목록 : 자동추출 + 커스텀
		
		// 배치처리(1/2) : DB에 저장된 값을 로딩하여 사용.
		if ( strBatchYn.equals("Y") == true ) {
			HashMap mapInfo = makeItamViewtableDetailRecord(paramMap);
			strBatchRemakeQueryYn = StringUtil.parseString(mapInfo.get("AUTO_REMAKE_YN"));
			if ( mapInfo != null ) {
				Iterator iInfo = mapInfo.keySet().iterator();
				while ( iInfo.hasNext() ) {
					String strKey = StringUtil.parseString(iInfo.next()); 
					paramMap.put(strKey,mapInfo.get(strKey));
				}
			}
		}
		
		// 컬럼 자동추출
		if ( StringUtil.parseString(paramMap.get("AUTO_COLMAKE_YN")).equals("Y") == true ) {
			String strException = StringUtil.parseString(paramMap.get("FIELD_EXCEPTION_AUTO")).trim().replace(" ","");
			if ( strException.equals("") == false ) { strException = "'" + strException.replace(",", "','") + "'";}
			paramMap.put("FIELD_EXCEPTION_AUTO_FOR_IN", strException);
			
			// 임의추가된 CLASS_TYPE( ROOT , HW , JOIN 등 ) 구분
			paramMap.put("custom_class_type_yn", "N");
			paramMap.put("field_name_header", "");
			if ( strTempClassType.equals("ROOT") == true || strTempClassType.equals("HW") == true || strTempClassType.equals("JOIN") == true ) {
				paramMap.put("custom_class_type_yn", "Y");
				if ( strTempClassType.equals("JOIN") == true ) {
					if ( strTempClassKind.equals("ASSET") == true ) { paramMap.put("field_name_header", "AR_"); }
					else if ( strTempClassKind.equals("CONF") == true ) { paramMap.put("field_name_header", "CR_"); }
				}
			}

			List listResult = amdbManagerDAO.makeItamViewtableDetailMakeQuery(paramMap);
			
			if ( listResult != null ) {
				boolean bStartField = false;
				boolean bStartFrom = false;
				for ( int i = 0 ; i < listResult.size(); i++) {
					HashMap mapLine = (HashMap) listResult.get(i);
					String strLine = StringUtil.parseString(mapLine.get("QRYSTR"));
					if ( strLine.trim().toUpperCase().equals("--FIELDINFO_START--") == true ) { bStartField = true; continue;}
					if ( bStartField == true ) {
						if ( strLine.trim().toUpperCase().equals("--TABLEINFO_START--") == true ) { bStartFrom = true;	continue;}
						if ( bStartFrom == true ) { strFrom += ( strFrom.equals("") == true ? "" : "\n" ) + strLine;}
						else {strField += ( strField.equals("") == true ? "" : "\n" ) + strLine;}
					}
					if ( strLine.trim().toUpperCase().indexOf("--COLUMN_LIST--") > -1 ) {
						strAllColumnList = strLine.trim().replace("--COLUMN_LIST--", "").replace(" ", "") + ","; // 비교를 위해서 콤마로 시작하고 콤마로 끝나야 함.
					}
				} // for i
			} // if ( listResult != null ) {
			
		}
		
		// 최종쿼리 생성 Start
		String strCustomField = StringUtil.parseString(paramMap.get("FIELD_CUSTOM")).replace("\n", "\n\t\t\t");
		String strCustomFrom = StringUtil.parseString(paramMap.get("TABLE_CUSTOM")).replace("\n", "\n\t\t\t");
		String strCustomWhere = StringUtil.parseString(paramMap.get("WHERE_CUSTOM")).replace("\n", "\n\t\t\t");
		String strAddAssetField = "";
		String strAddAssetFrom = "";
		
		// 컬럼목록 추가
		String [] arrstrCustomField = strCustomField.split("\n\t\t\t");
		for ( int c = 0 ; c < arrstrCustomField.length ; c++) {
			boolean bAlias = false;
			String [] arrTempLine = arrstrCustomField[c].split(" ");
			for ( int w = 0 ; w < arrTempLine.length ; w++) {
				String strTempWord = StringUtil.parseString(arrTempLine[w]).trim();
				if ( strTempWord.equals("") == true ) {continue;}
				if ( strTempWord.equals("AS") == true ) {bAlias=true;continue;}
				if ( bAlias == true ) {strAllColumnList += strTempWord + ",";bAlias=false;}
			}
		}

		// Start - CONF 뷰테이블(JOIN) 생성시 자동으로 ASSET_STATISTIC_ROOT 중에서 AM_ASSET 관련 컬럼을 추가해줌.(그 외에는 사용자정의로 추가 필요)
		//*
		if ( strTempClassKind.equals("CONF") == true && strTempClassType.equals("JOIN") == false
				&& ( strFrom.indexOf("AM_CONF") > -1 || strCustomFrom.indexOf("AM_CONF") > -1 ) 
				//&& strFrom.indexOf("AM_ASSET") == -1 && strCustomFrom.indexOf("AM_ASSET") == -1 
				) {
			ModelMap mapAssetParam = new ModelMap();
			mapAssetParam.put("class_kind","ASSET");
			mapAssetParam.put("class_type","ROOT");
			HashMap mapAssetRootView = makeItamViewtableDetailRecord(mapAssetParam);
			String strAssetRootQuery = StringUtil.parseString(mapAssetRootView.get("MAKE_QUERY"));
			if ( strAssetRootQuery != null ) {
				String strPosition = "";
				String [] arrAssetRootQuery = strAssetRootQuery.split("\n");
				for ( int r = 0 ; r < arrAssetRootQuery.length ; r++) {
					if ( arrAssetRootQuery[r] != null ) {
						if ( strPosition.equals("") == true && arrAssetRootQuery[r].trim().indexOf("SELECT") == 0  ) { strPosition = "FIELD"; continue;}
						if ( strPosition.equals("FIELD") == true ) {
							if ( arrAssetRootQuery[r].trim().length() >= 4 ) {
								if ( arrAssetRootQuery[r].trim().indexOf("FROM") == 0 ) { strPosition = "TABLE"; break;	}
							}
							if ( arrAssetRootQuery[r].indexOf("AM_ASSET.") > -1 ) {
								// 컬럼 겹치는지 확인
								boolean bAlias = false;
								boolean bDupCheck = false;
								String [] arrTempLine = arrAssetRootQuery[r].split(" ");
								for ( int w = 0 ; w < arrTempLine.length ; w++) {
									String strTempWord = StringUtil.parseString(arrTempLine[w]).trim();
									if ( strTempWord.equals("") == true ) {continue;}
									if ( strTempWord.equals("AS") == true ) {bAlias=true;continue;}
									if ( bAlias == true ) {
										bAlias=false;
										if ( strAllColumnList.indexOf(strTempWord + ",") > -1 ) {bDupCheck=true;break;}
									}
								} // for w			
								if ( bDupCheck == false ) {
									strAddAssetField += "\n" + arrAssetRootQuery[r];
								}
							} // if 							
						} // if ( strPosition.equals("FIELD") == true ) {
					} // if not null
				} // for r
				if ( strAddAssetField.equals("") == false ) {
					// 쿼리 추출시 자동 추가
					// strAddAssetFrom = "\n			LEFT OUTER JOIN AM_ASSET ON AM_CONF.ASSET_ID = AM_ASSET.ASSET_ID"; 
				}
			}
		}
		//*/
		// End - CONF 뷰테이블(JOIN 제외)의 경우 AM_ASSET이 없는 경우 ASSET_STATISTIC_ROOT 정보에서 AM_ASSET 관련 속성만 추가함
		
		strFinalQuery += "CREATE OR REPLACE VIEW " + StringUtil.parseString(paramMap.get("VIEW_TABLE_ID")) + " AS " ;
		strFinalQuery += "\n	SELECT " + "\n			SYSDATE AS DATE_" + strTempClassKind + " \n" 
				+ strField  + (strField.equals("") == true ? "" : "\n" ) 
				+ (strCustomField.equals("") == true ? "" : "\t\t\t" ) + strCustomField + strAddAssetField;
		strFinalQuery += "\n		FROM " + "\n" 
				+ strFrom  + (strFrom.equals("") == true ? "" : "\n" ) 
				+ (strCustomFrom.equals("") == true ? "" : "\t\t\t" ) + strCustomFrom + strAddAssetFrom;
		strFinalQuery += "\n		WHERE 1=1 " + "\n" 
				+ (strCustomWhere.equals("") == true ? "" : "\t\t\t" ) + strCustomWhere ;
		mapResult.put("final_query",strFinalQuery);
		// 최종쿼리 생성 End
		
		// 배치처리(2/2) : 신규 생성된 뷰테이블쿼리를 다시 저장함.(쿼리를 재생성한 경우)
		if ( strBatchYn.equals("Y") == true ) {
			if ( strBatchRemakeQueryYn.equals("Y") == false ) {
				// paramMap.MAKE_QUERY 그대로 사용(DB 로딩 정보 그대로 변경 없음)
				paramMap.put("UPD_DT_NOUPDATE", "Y"); // 수정이력은 업데이트 하지 않음
			}else{
				paramMap.put("UPD_DT_NOUPDATE", "N");
				paramMap.put("MAKE_QUERY", strFinalQuery);
				makeItamViewtableDetailSave(paramMap);
			}
		}		
		
		// mapResult : nvf 에서 ajax 리턴값으로 사용
		// paramMap : 배치적용에서 사용하는 값.
		return mapResult;
	} // makeItamViewtableDetailMakeQuery

	@Override
	public void makeItamViewtableDetailCreateViewtable(ModelMap paramMap) throws NkiaException {
		amdbManagerDAO.makeItamViewtableDetailCreateViewtable(paramMap); // 뷰테이블 생성
		amdbManagerDAO.makeItamViewtableDetailCreateViewtableApplydt(paramMap); // 적용일 업데이트
		amdbManagerDAO.makeItamViewtableDetailCreateViewtableHistory(paramMap); // 뷰테이블쿼리가 변경된 경우 이력 남김.
	}

	@Override
	public Integer makeItamViewtableHistorySelectCount(ModelMap paramMap) throws NkiaException {
		return amdbManagerDAO.makeItamViewtableHistorySelectCount(paramMap);
	}

	@Override
	public List makeItamViewtableHistorySelect(ModelMap paramMap) throws NkiaException {
		List listResult = amdbManagerDAO.makeItamViewtableHistorySelect(paramMap);
		List listReturn = new ArrayList();
		
		if ( listResult != null ) {
			for ( int r = 0 ; r < listResult.size(); r ++){
				HashMap mapRecord = (HashMap) listResult.get(r);
				if ( mapRecord != null ) {
					mapRecord.put("MAKE_QUERY", StringUtil.parseString(mapRecord.get("MAKE_QUERY1")) 
												+ StringUtil.parseString(mapRecord.get("MAKE_QUERY2")) 
												+ StringUtil.parseString(mapRecord.get("MAKE_QUERY3"))
								);
				}
				listReturn.add(mapRecord);
			}
		} else {
			listReturn = null;
		}

		return listReturn;
	} // makeItamViewtableHistorySelect
	
	/* *ITAM 뷰테이블 생성* End */
}
