package com.nkia.itg.itam.amdb.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.amdb.dao.AmdbServiceDAO;
import com.nkia.itg.itam.amdb.service.AmdbServiceService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("amdbServiceService")
public class AmdbServiceServiceImpl implements AmdbServiceService{
	private static final Logger logger = LoggerFactory.getLogger(AmdbServiceServiceImpl.class);
	
	@Resource(name = "amdbServiceDao")
	public AmdbServiceDAO amdbServiceDao;

	public List searchAmdbServiceAllTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchAmdbServiceAllTree(paramMap);
	}
	
	public List searchAmdbServiceAllTreeMap(ModelMap paramMap) throws NkiaException {
		return amdbServiceDao.searchAmdbServiceAllTreeMap(paramMap);
	}
	
	public HashMap selectAmdbServiceBaseInfo(ModelMap paramMap) throws NkiaException {
		return amdbServiceDao.selectAmdbServiceBaseInfo(paramMap);
	}
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchCodeDataList(paramMap);
	}

	public List searchAmdbServiceGridList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchAmdbServiceGridList(paramMap);
	}
	
	public List searchAmdbServiceLowServiceList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchAmdbServiceLowServiceList(paramMap);
	}


	public void updateAmdbServiceInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		
		int mapSize = paramMap.size();
		String service_id = null;
		
		for(int i=0; i<mapSize; i++){
			
			String mapIndex = toString().valueOf(i);
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
			if(i == 0){
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
		    	if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		infoData.put("login_user_id", userVO.getUser_id());
		    	}
		    	service_id = (String) infoData.get("service_id");
				amdbServiceDao.updateAmdbServiceInfo(infoData);
				amdbServiceDao.deleteAmdbServiceUser(infoData);
				amdbServiceDao.deleteAmdbServiceRelConf(infoData);
				amdbServiceDao.insertAmdbServiceRelConf(infoData);
			}else{
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
		    	if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		infoData.put("login_user_id", userVO.getUser_id());
		    	}
		    	infoData.put("service_id", service_id);
		    	amdbServiceDao.insertAmdbServiceUser(infoData);
			}
		}
	}
	
	public void updateServiceOrder(HashMap oldOrderMap) throws NkiaException {

		String up_service_id = (String) oldOrderMap.get("up_service_id");
		List oldOrderList = (List) oldOrderMap.get("oldOrderList");
		
		//select new order list
		List newOrderList = amdbServiceDao.searchAmdbServiceTreeList(up_service_id);
		
		// comparing old one with new one and reorder the list
		for(int i = 0; i < oldOrderList.size(); i++){
			HashMap oldElement = (HashMap) oldOrderList.get(i);
			
			for(int k = 0; k < newOrderList.size(); k++){
				HashMap newElement = (HashMap) newOrderList.get(k);
				
				int oldDisp = Integer.valueOf(oldElement.get("display_no").toString());
				String oldServiceId = oldElement.get("id").toString();
				int newDisp = Integer.valueOf(newElement.get("DISPLAY_NO").toString());
				String newServiceId = newElement.get("SERVICE_ID").toString();
				
				if( oldServiceId.equals(newServiceId) && oldDisp != newDisp){
					HashMap paramMap = new HashMap();
					paramMap.put("new_order", newDisp);
					paramMap.put("old_order", oldDisp);
					paramMap.put("service_id",oldServiceId);
					paramMap.put("up_service_id",up_service_id);
					
					if(oldDisp > newDisp){ 
						
						amdbServiceDao.updateAmdbServiceTreeUpward(paramMap);
						
					}else{
						
						amdbServiceDao.updateAmdbServiceTreeDownward(paramMap);
						
					}
				}
			}
		}
	}
	

	public void updateAmdbServiceLowService(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbServiceDao.updateAmdbServiceLowService(paramMap);
	}
	
	public void deleteAmdbServiceUser(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbServiceDao.deleteAmdbServiceUser(paramMap);
	}

	public void insertAmdbServiceInfo(ModelMap paramMap) throws NkiaException {
		int mapSize = paramMap.size();
		String upCustId = null;
		for(int i=0; i<mapSize; i++){
			String mapIndex = toString().valueOf(i);
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
			if(i == 0){
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
		    	if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		infoData.put("login_user_id", userVO.getUser_id());
		    	}
		    	
		    	int custCnt = amdbServiceDao.selectCustCount(infoData);
		    	
		    	if(custCnt == 0){
		    		String newServiceId = amdbServiceDao.selectServiceId(infoData);
		    		infoData.put("up_service_id", newServiceId);
		    		amdbServiceDao.insertServiceCust(infoData);
		    	}
		    	
		    	amdbServiceDao.insertAmdbServiceInfo(infoData);
				amdbServiceDao.insertAmdbServiceRelConf(infoData);		    	
			}else{
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
		    	if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		infoData.put("login_user_id", userVO.getUser_id());
		    	}
		    	amdbServiceDao.insertAmdbServiceUser(infoData);
			}
		}	
	}
	
	public void insertAmdbServiceUser(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbServiceDao.insertAmdbServiceUser(paramMap);
	}

	public void deleteAmdbServiceInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbServiceDao.deleteAmdbServiceRel(paramMap);
		amdbServiceDao.deleteAmdbServiceUser(paramMap);
		amdbServiceDao.deleteAmdbServiceInfo(paramMap);
	}
	
	public HashMap selectServiceCust(ModelMap paramMap) throws NkiaException {
		return amdbServiceDao.selectServiceCust(paramMap);
	}
	
	public void insertServiceCust(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbServiceDao.insertServiceCust(paramMap);
	}	
	
	
	public List searchServiceList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchServiceList(paramMap);
	}

	public int searchServiceListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchServiceListCount(paramMap);
	}
	
	public List searchServiceToRelConfList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchServiceToRelConfList(paramMap);
	}
	
	public int searchServiceToRelConfListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchServiceToRelConfListCount(paramMap);
	}
	
	public List searchServiceConfList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchServiceConfList(paramMap);
	}
	
	public int searchServiceConfListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbServiceDao.searchServiceConfListCount(paramMap);
	}
	
	public List searchServiceCodeList(ModelMap paramMap) throws NkiaException {
		return amdbServiceDao.searchServiceCodeList(paramMap);
	}
	
	public List searchServiceCodeList(Map paramMap) throws NkiaException {
		return amdbServiceDao.searchServiceCodeList(paramMap);
	}

	public int searchServiceCodeListCount(ModelMap paramMap) throws NkiaException {
		return amdbServiceDao.searchServiceCodeListCount(paramMap);
	}
}
