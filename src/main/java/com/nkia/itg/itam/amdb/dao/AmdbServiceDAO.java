package com.nkia.itg.itam.amdb.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("amdbServiceDao")
public class AmdbServiceDAO  extends EgovComAbstractDAO {

	public List searchAmdbServiceAllTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("amdbServiceDao.searchAmdbServiceAllTree", paramMap);
		return resultList;
	}
	
	public List searchAmdbServiceAllTreeMap(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("amdbServiceDao.searchAmdbServiceAllTreeMap", paramMap);
		return resultList;
	}

	public HashMap selectAmdbServiceBaseInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("amdbServiceDao.selectAmdbServiceBaseInfo", paramMap);
	}	

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("amdbServiceDao.searchCodeDataList", paramMap);
		return resultList;
	}

	public List searchAmdbServiceGridList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("amdbServiceDao.searchAmdbServiceGridList", paramMap);
		return resultList;
	}
	
	public List searchAmdbServiceLowServiceList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("amdbServiceDao.searchAmdbServiceLowServiceList", paramMap);
		return resultList;
	}
	
	public List searchAmdbServiceTreeList(String param) throws NkiaException {
		List resultList = null;
		resultList = this.list("amdbServiceDao.searchAmdbServiceTreeList", param);
		return resultList;
	}

	public void updateAmdbServiceInfo(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		update("amdbServiceDao.updateAmdbServiceInfo", infoData);
	}
	
	public void updateAmdbServiceLowService(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		update("amdbServiceDao.updateAmdbServiceLowService", infoData);
	}
	
	public void updateAmdbServiceTreeUpward(HashMap infoData) throws NkiaException {
		update("amdbServiceDao.updateAmdbServiceTreeUpward", infoData);
	}
	
	public void updateAmdbServiceTreeDownward(HashMap infoData) throws NkiaException {
		update("amdbServiceDao.updateAmdbServiceTreeDownward", infoData);
	}

	public void deleteAmdbServiceUser(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		delete("amdbServiceDao.deleteAmdbServiceUser", infoData);
	}
	
	public String selectCustId(HashMap infoData) throws NkiaException{
		String upCustId = (String) this.selectByPk("amdbServiceDao.selectCustId", infoData);
		return upCustId;
	}
	
	public void insertAmdbServiceInfo(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		insert("amdbServiceDao.insertAmdbServiceInfo", infoData);
	}
	
	public void insertAmdbServiceUser(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		insert("amdbServiceDao.insertAmdbServiceUser", infoData);
	}

	public void deleteAmdbServiceInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("amdbServiceDao.deleteAmdbServiceInfo", paramMap);
	}
	
	public void deleteAmdbServiceRel(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("amdbServiceDao.deleteAmdbServiceRel", paramMap);
	}

	public HashMap selectServiceCust(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("amdbServiceDao.selectServiceCust", paramMap);
	}	
	
	public void insertServiceCust(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		insert("amdbServiceDao.insertServiceCust", infoData);
	}
	
	public List searchServiceList(ModelMap paramMap) throws NkiaException{
		return list("amdbServiceDao.searchServiceList", paramMap);
	}
	
	public int searchServiceListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("amdbServiceDao.searchServiceListCount", paramMap);
	}
	
	public int selectCustCount(HashMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("amdbServiceDao.selectCustCount", paramMap);
	}
	
	public String selectServiceId(HashMap infoData) throws NkiaException {
		return (String)selectByPk("amdbServiceDao.selectServiceId", infoData);
	}
	
	public List searchServiceToRelConfList(ModelMap paramMap) throws NkiaException{
		return list("amdbServiceDao.searchServiceToRelConfList", paramMap);
	}
	
	public int searchServiceToRelConfListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("amdbServiceDao.searchServiceToRelConfListCount", paramMap);
	}
	
	public List searchServiceConfList(ModelMap paramMap) throws NkiaException{
		return list("amdbServiceDao.searchServiceConfList", paramMap);
	}
	
	public int searchServiceConfListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("amdbServiceDao.searchServiceConfListCount", paramMap);
	}
	
	public void deleteAmdbServiceRelConf(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		delete("amdbServiceDao.deleteAmdbServiceRelConf", infoData);
	}	
	
	public void insertAmdbServiceRelConf(HashMap infoData) throws NkiaException {
		// TODO Auto-generated method stub
		insert("amdbServiceDao.insertAmdbServiceRelConf", infoData);
	}
	
	public List searchServiceCodeList(ModelMap paramMap) throws NkiaException{
		return list("amdbServiceDao.searchServiceCodeList", paramMap);
	}
	
	public List searchServiceCodeList(Map paramMap) throws NkiaException{
		return list("amdbServiceDao.searchServiceCodeList", paramMap);
	}
	
	public int searchServiceCodeListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("amdbServiceDao.searchServiceCodeListCount", paramMap);
	}
}
