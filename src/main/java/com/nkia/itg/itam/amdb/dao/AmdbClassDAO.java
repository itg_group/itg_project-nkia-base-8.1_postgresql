package com.nkia.itg.itam.amdb.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("amdbClassDao")
public class AmdbClassDAO  extends EgovComAbstractDAO {

	public List searchAmClassAllTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("amdbClassDao.searchAmClassAllTree", paramMap);
		return resultList;
	}

	public List selectAmClassBaseInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("amdbClassDao.selectAmClassBaseInfo", paramMap);
		return resultList;
	}

	public int searchAmClassGridListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("amdbClassDao.searchAmClassGridListCount", paramMap);
		return count;
	}

	public List searchAmClassGridList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("amdbClassDao.searchAmClassGridList", paramMap);
		return resultList;
	}

	public void updateAmClassInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		update("amdbClassDao.updateAmClassInfo", paramMap);
	}
	
	public void updateAssetServiceLife(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		update("amdbClassDao.updateAssetServiceLife", paramMap);
	}

	public void updateAmClassOrder(Map param) throws NkiaException {
		// TODO Auto-generated method stub
		update("amdbClassDao.updateAmClassOrder", param);
	}

	public void insertAmClassInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("amdbClassDao.insertAmClassInfo", paramMap);
	}

	public void deleteAmClassInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("amdbClassDao.deleteAmClassInfo", paramMap);
	}

	/**
	 * @param paramMap
	 * @return
	 */
	public List searchAmClassCheckTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("amdbClassDao.searchAmClassCheckTree", paramMap);
		return resultList;
	}

	public int selectAssetCount(HashMap paramMap) throws NkiaException{
		return (Integer)selectByPk("amdbClassDao.selectAssetCount", paramMap);
	}	
}
