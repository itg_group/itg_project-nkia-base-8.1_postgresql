/**
 * 
 */
package com.nkia.itg.itam.amdb.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 1.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("amdbAttributeDao")
public class AmdbAttributeDAO extends EgovComAbstractDAO {

	/**
	 * @param paramMap
	 * @return
	 */
	public int searchAmAttributeGridListCount(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer) selectByPk("amdbAttributeDao.searchAmAttributeGridListCount", paramMap);
	}

	/**
	 * @param paramMap
	 * @return
	 */
	public List searchAmAttributeGridList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("amdbAttributeDao.searchAmAttributeGridList", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void updateAmAttributeInfo(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		update("amdbAttributeDao.updateAmAttributeInfo", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void deleteAmAttributeInfo(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("amdbAttributeDao.deleteAmAttributeInfo", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void insertAmAttributeInfo(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("amdbAttributeDao.insertAmAttributeInfo", paramMap);
	}
	
	public int selectAttributeFieldNm(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("amdbAttributeDao.selectAttributeFieldNm", paramMap);
	}

	public HashMap selectAttributeInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("amdbAttributeDao.selectAttributeInfo", paramMap);
	}
	
	public int selectUserGroupCnt(HashMap paramMap)throws NkiaException {
		return (Integer)selectByPk("amdbAttributeDao.selectUserGroupCnt", paramMap);
	}
	
	public void deleteUserGroupInfo(HashMap paramMap)throws NkiaException {
		delete("amdbAttributeDao.deleteUserGroupInfo", paramMap);
	}
	
	public void insertUserGroupInfo(HashMap paramMap)throws NkiaException {
		insert("amdbAttributeDao.insertUserGroupInfo", paramMap);
	}
	
	public List selectPopupList(ModelMap paramMap) throws NkiaException {
		return list("amdbAttributeDao.selectPopupList",paramMap);
	}
	
	public List selectGenTableList(ModelMap paramMap) throws NkiaException {
		return list("amdbAttributeDao.selectGenTableList",paramMap);
	}
	
	public int selectGenTableCnt(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("amdbAttributeDao.selectGenTableCnt", paramMap);
	}
	
	public List selectEmsCollectColList(HashMap paramMap) throws NkiaException {
		return list("amdbAttributeDao.selectEmsCollectColList",paramMap);
	}
}
