





/**
 * 
 */
package com.nkia.itg.itam.amdb.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 12.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("amdbTableDao")
public class AmdbTableDAO extends EgovComAbstractDAO{

	/**
	 * @param paramMap
	 * @return
	 */
	public int selectAmTblGridCount(ModelMap paramMap) throws NkiaException{
		// TODO Auto-generated method stub
		return (Integer)selectByPk("amdbTableDao.selectAmTblGridCount", paramMap);
	}

	/**
	 * @param paramMap
	 * @return
	 */
	public List selectAmTblGridList(ModelMap paramMap) throws NkiaException{
		// TODO Auto-generated method stub
		return list("amdbTableDao.selectAmTblGridList", paramMap);
	}

	/**
	 * @param paramMap
	 * @return
	 */
	public List selectAmAttrGridList(ModelMap paramMap) throws NkiaException{
		return this.list("amdbTableDao.selectAmAttrGridList", paramMap);
	}

	/**
	 * @param paramMap
	 * @return
	 */
	public List<HashMap> selectAmAttrSelGridList(ModelMap paramMap) throws NkiaException{
		// TODO Auto-generated method stub
		return list("amdbTableDao.selectAmAttrSelGridList", paramMap);
	}

	/**
	 * @param tableInfo
	 */
	public void insertGenTableInfo(HashMap tableInfo)throws NkiaException{
		// TODO Auto-generated method stub
		insert("amdbTableDao.insertGenTableInfo", tableInfo);
	}
	

	public void insertGenTblAttrMapping(HashMap dataMap)throws NkiaException{
		insert("amdbTableDao.insertGenTblAttrMapping", dataMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteGenTableInfo(HashMap paramMap)throws NkiaException{
		// TODO Auto-generated method stub
		insert("amdbTableDao.deleteGenTableInfo", paramMap);
	}

	/**
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void deleteGenAttrMapping(HashMap paramMap)throws NkiaException{
		insert("amdbTableDao.deleteGenAttrMapping", paramMap);
	}
	
	/**
	 * @param tableInfo
	 */
	public void updateGenTableInfo(HashMap tableInfo) throws NkiaException{
		// TODO Auto-generated method stub
		update("amdbTableDao.updateGenTableInfo", tableInfo);
	}
	
	/**
	 * 테이블 명 검증
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectGenTableNm(HashMap paramMap) throws NkiaException{
		return (Integer)selectByPk("amdbTableDao.selectGenTableNm", paramMap);
	}
	
	
	/**
	 * 테이블 정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectGenTableInfo(HashMap paramMap)throws NkiaException{
		return (HashMap)selectByPk("amdbTableDao.selectGenTableInfo", paramMap);
	}
	
	/**
	 * 테이블 스페이스를 가져온다
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectUserTableSpace(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("amdbTableDao.selectUserTableSpace", paramMap);
	}
	
	/**
	 * 실제 테이블이 존재하는지 확인한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectTblSchemaCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbTableDao.selectTblSchemaCnt", paramMap);
	}
	
	/**
	 * 테이블 생성
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void createTableScript(HashMap paramMap)throws NkiaException{
		update("amdbTableDao.createTableScript", paramMap);
	}
	
	/**
	 * 코멘트 생성
	 * @param commentSql
	 * @throws NkiaException
	 */
	public void createExcuteScript(String commentSql)throws NkiaException{
		update("amdbTableDao.createExcuteScript", commentSql);
	}
	
	/**
	 * 제약 조건 생성
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void createConstScript(HashMap paramMap)throws NkiaException{
		update("amdbTableDao.createConstScript", paramMap);
	}
	
	/**
	 * index생성
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void createIndexScript(HashMap paramMap)throws NkiaException{
		update("amdbTableDao.createIndexScript", paramMap);
	}
	
	
	/**
	 * 테이블 생성 여부 생성
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateGenYn(HashMap paramMap)throws NkiaException{
		update("amdbTableDao.updateGenYn", paramMap);
	}
	
	/**
	 * 컬럼 생성 여부
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateGenYnCols(HashMap paramMap)throws NkiaException{
		update("amdbTableDao.updateGenYnCols", paramMap);
	}
	
	
	/**
	 * 
	 * @throws NkiaException
	 */
	public void deleteTempTbl()throws NkiaException{
		delete("amdbTableDao.deleteTempTbl", null);
	}
	
	/**
	 * 
	 * @throws NkiaException
	 */
	public void isnertTempTbl()throws NkiaException{
		insert("amdbTableDao.isnertTempTbl", null);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmRelationTblList(HashMap paramMap)throws NkiaException{
		return list("amdbTableDao.selectAmRelationTblList", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectScriptInfo(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("amdbTableDao.selectScriptInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void createBackupTable(HashMap paramMap)throws NkiaException{
		insert("amdbTableDao.createBackupTable", paramMap);
	}
	
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertBackupHistory(HashMap paramMap)throws NkiaException{
		insert("amdbTableDao.insertBackupHistory", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void dropAmTable(String tblNm)throws NkiaException{
		insert("amdbTableDao.dropAmTable", tblNm);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void migrationTable(HashMap paramMap)throws NkiaException{
		insert("amdbTableDao.migrationTable", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public int selectDropValidation(String tblNm)throws NkiaException{
		return (Integer)selectByPk("amdbTableDao.selectDropValidation", tblNm);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAttributeCombo(HashMap paramMap)throws NkiaException{
		return list("amdbTableDao.selectAttributeCombo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAlterList(HashMap paramMap)throws NkiaException{
		return list("amdbTableDao.selectAlterList", paramMap);
	}
	
	/**
	 * 
	 * @param query
	 * @throws NkiaException
	 */
	public void executeAlterQuery(String query)throws NkiaException{
		insert("amdbTableDao.executeAlterQuery", query);
	}
	
	/**
	 * 
	 * @param gen_table_nm
	 * @throws NkiaException
	 */
	public void dropConstraints(String gen_table_nm)throws NkiaException{
		insert("amdbTableDao.dropConstraints", gen_table_nm);
	}
	
	/**
	 * 
	 * @param gen_table_nm
	 * @throws NkiaException
	 */
	public void dropIndex(String gen_table_nm)throws NkiaException{
		insert("amdbTableDao.dropIndex", gen_table_nm);
	}
	
	//hmsong cmdb 개선 작업 Start
	/**
	 * 테이블 매핑 시 필수 컬럼의 정보를 검토해주는 쿼리.
	 * 
	 * @param List
	 * @throws NkiaException
	 */
	public List selectNotNullTblCode(HashMap paramMap) throws NkiaException {
		return list("amdbTableDao.selectNotNullTblCode", paramMap);
	}
	//hmsong cmdb 개선 작업 End
}
