/**
 * 
 */
package com.nkia.itg.itam.amdb.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.amdb.service.AmdbManagerService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * @version 1.0 2013. 4. 8.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class AmdbManagerController {
	
	@Resource(name = "amdbManagerService")
	private AmdbManagerService amdbManagerService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 관리 그룹 설정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbManager.do")
	public String amdbManager(ModelMap paramMap) throws NkiaException {
		return "/itg/itam/amdb/amdbManager.nvf";
	}
	

	/**
	 * 관리 그룹 설정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbManagerEntity.do")
	public String amdbManagerEntity(HttpServletRequest req, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(req)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		//hmsong cmdb 개선 작업 Start
		//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		String confAdminYn = new String();
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		paramMap.put("confAdminYn", confAdminYn);
		//hmsong cmdb 개선 작업 End				
		return "/itg/itam/amdb/amdbManagerEntity.nvf";
	}
	
	/**
	 * 현황 설정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbManagerList.do")
	public String amdbManagerList(HttpServletRequest req,ModelMap paramMap) throws NkiaException {
		return "/itg/itam/amdb/amdbManagerList.nvf";
	}

	@RequestMapping(value="/itg/itam/amdb/selectClassEntityTree.do")
	public @ResponseBody ResultVO selectClassEntityTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			TreeVO treeVO = new TreeVO();
			
			//hmsong cmdb 개선 작업 Start
			//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
			String confAdminYn = new String();
			if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
				if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
					confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
				}
			}			
			paramMap.put("confAdminYn", confAdminYn);
			//hmsong cmdb 개선 작업 End
			
			List result 		= amdbManagerService.selectClassEntityTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			
			treeVO = JsonUtil.changeTreeVOToJson(result, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectEntityList.do")
	public @ResponseBody ResultVO selectEntityList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectEntityList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectTblList.do")
	public @ResponseBody ResultVO selectTblList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectTblList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectTblSelList.do")
	public @ResponseBody ResultVO selectTblSelList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectTblSelList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectTblJoinList.do")
	public @ResponseBody ResultVO selectTblJoinList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectTblJoinList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectColList.do")
	public @ResponseBody ResultVO selectColList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectColList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectColSelList.do")
	public @ResponseBody ResultVO selectColSelList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectColSelList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/insertEntityInfo.do")
	public @ResponseBody ResultVO insertEntityInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			paramMap.put("set_type","D");
			
			paramMap.put("user_id", userVO.getUser_id());
			//id 생성
			amdbManagerService.generateEntityId(paramMap);
			//그룹정보입력
			amdbManagerService.insertEntityInfo(paramMap);
			//테이블 정보 입력
			amdbManagerService.insertTblInfo(paramMap);
			//join정보 입력
			amdbManagerService.insertJoinInfo(paramMap);
			//속성 정보 입력
			amdbManagerService.insertColInfo(paramMap);
			
			amdbManagerService.insertInstallInfo(paramMap);
			
			amdbManagerService.insertRelInfo(paramMap);
			// 버튼 정보 입력
			amdbManagerService.insertBtnInfo(paramMap);
			
			resultVO.setResultMap(paramMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/updateChangeOrder.do")
	public @ResponseBody ResultVO updateChangeOrder(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			//id 생성
			amdbManagerService.updateChangeOrder(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/itam/amdb/selectGroupInfo.do")
	public @ResponseBody ResultVO selectGroupInfo(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = amdbManagerService.selectGroupInfo(paramMap);
			resultVO.setResultMap(resultMap);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectInstallList.do")
	public @ResponseBody ResultVO selectInstallList(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectInstallList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectInstallSelList.do")
	public @ResponseBody ResultVO selectInstallSelList(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectInstallSelList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectRelList.do")
	public @ResponseBody ResultVO selectRelList(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectRelList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectRelSelList.do")
	public @ResponseBody ResultVO selectRelSelList(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectRelSelList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/deleteEntityInfo.do")
	public @ResponseBody ResultVO deleteEntityInfo(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			
			GridVO gridVO = new GridVO();
			amdbManagerService.deleteAllEntityInfo(paramMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/updateEntityInfo.do")
	public @ResponseBody ResultVO updateEntityInfo(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			paramMap.put("set_type","D");
			paramMap.put("user_id", userVO.getUser_id());
			
			amdbManagerService.updateEntityInfo(paramMap);
			//테이블 정보 입력
			amdbManagerService.insertTblInfo(paramMap);
			//join정보 입력
			amdbManagerService.insertJoinInfo(paramMap);
			//속성 정보 입력
			amdbManagerService.insertColInfo(paramMap);
			
			amdbManagerService.insertInstallInfo(paramMap);
			
			amdbManagerService.insertRelInfo(paramMap);
			// 버튼 정보 입력
			amdbManagerService.insertBtnInfo(paramMap);
			
			resultVO.setResultMap(paramMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectCompCodeCnt.do")
	public @ResponseBody ResultVO selectCompCodeCnt(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			HashMap result = amdbManagerService.selectCompCodeCnt(paramMap);
			resultVO.setResultMap(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectEntityCopyList.do")
	public @ResponseBody ResultVO selectEntityCopyList(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.selectEntityCopyList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectCopyEntityTree.do")
	public @ResponseBody ResultVO selectCopyEntityTree(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = new TreeVO();
			List result 		= amdbManagerService.selectCopyEntityTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			
			treeVO = JsonUtil.changeTreeVOToJson(result, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/insertCopyEntity.do")
	public @ResponseBody ResultVO insertCopyEntity(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			amdbManagerService.insertCopyEntity(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/searchEntityBtnList.do")
	public @ResponseBody ResultVO searchEntityBtnList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbManagerService.searchEntityBtnList(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 분류체계별 속성관리 _ webix
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbManagerEntity_webix.do")
	public String amdbManagerEntity_webix(HttpServletRequest req, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(req)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		return "/itg/itam/amdb/amdbManagerEntity.nvf";
	}
	
	
	
	// ##################################################################################################################################################
	// ## *ITAM 뷰테이블 생성*
	
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableTab.do")
	public String makeItamViewtableTab(HttpServletRequest req, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(req)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		return "/itg/itam/amdb/makeItamViewtableTab.nvf";
	}
	
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableList.do")
	public String makeItamViewtableList(HttpServletRequest req, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(req)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		return "/itg/itam/amdb/makeItamViewtableList.nvf";
	}	

	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableListSelect.do")
	public @ResponseBody ResultVO makeItamViewtableListSelect(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		Integer resultCount = 0;
		try{
			PagingUtil.getFristEndNum(paramMap);
			
			resultCount = amdbManagerService.makeItamViewtableListSelectCount(paramMap);
			List result = amdbManagerService.makeItamViewtableListSelect(paramMap);
			
			gridVO.setTotalCount( ( resultCount == null ? 0 : resultCount ) );
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableDetail.do")
	public String makeItamViewtableDetail(HttpServletRequest req, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(req)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String strViewMiddleName = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Globals.Cmdb.ViewTable.Make.MiddleName") );
		paramMap.put("view_table_middle_name", strViewMiddleName);
		
		paramMap.put("class_kind", StringUtil.parseString(req.getParameter("class_kind")));
		paramMap.put("class_type", StringUtil.parseString(req.getParameter("class_type")));

		return "/itg/itam/amdb/makeItamViewtableDetail.nvf";
	}	
	
	// PARAM : class_kind , class_type
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableDetailRecord.do")
	public @ResponseBody ResultVO makeItamViewtableDetailRecord(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			HashMap result = amdbManagerService.makeItamViewtableDetailRecord(paramMap);
			resultVO.setResultMap(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableDetailSave.do")
	public @ResponseBody ResultVO makeItamViewtableDetailSave(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap mapSave = new ModelMap();
			Iterator it = paramMap.keySet().iterator();
			while ( it.hasNext() ) {
				String strKey = StringUtil.parseString(it.next());
				//if ( strKey.equals("FIELD_CUSTOM") || strKey.equals("TABLE_CUSTOM") || strKey.equals("WHERE_CUSTOM") || strKey.equals("MAKE_QUERY")) {
				if (  false ) {
					mapSave.put(strKey, StringUtil.parseString(paramMap.get(strKey)).replaceAll("'", "''"));
				}else {
					mapSave.put(strKey, paramMap.get(strKey));
				}
			}
			
			boolean bResult = amdbManagerService.makeItamViewtableDetailSave(mapSave);
			resultVO.setSuccess(bResult);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableDetailMakeQuery.do")
	public @ResponseBody ResultVO makeItamViewtableDetailMakeQuery(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap mapResult = amdbManagerService.makeItamViewtableDetailMakeQuery(paramMap);
			resultVO.setResultMap(mapResult);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableDetailCreateViewtable.do")
	public @ResponseBody ResultVO makeItamViewtableDetailCreateViewtable(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		paramMap.put("batch_work_yn", "Y");
		
		try{
			amdbManagerService.makeItamViewtableDetailSave(paramMap);
			amdbManagerService.makeItamViewtableDetailCreateViewtable(paramMap);
			resultVO.setSuccess(true);
			resultVO.setResultMsg("");
		} catch(NkiaException e) {
			resultVO.setSuccess(true);
			resultVO.setResultMsg("뷰테이블 생성 오류");
			//throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableDetailBatchCreateViewtable.do")
	public @ResponseBody ResultVO makeItamViewtableDetailBatchCreateViewtable(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		ModelMap mapBatch = new ModelMap();
		
		try{
			List listRows = (List) paramMap.get("batch_rows");
			if ( listRows != null ) {
				for ( int r = 0 ; r < listRows.size() ; r++) {
					HashMap mapRow = (HashMap) listRows.get(r);
					
					mapBatch.put("class_kind", StringUtil.parseString(mapRow.get("class_kind")));
					mapBatch.put("class_type", StringUtil.parseString(mapRow.get("class_type")));
					mapBatch.put("batch_work_yn", "Y");
					
					amdbManagerService.makeItamViewtableDetailMakeQuery(mapBatch);
					amdbManagerService.makeItamViewtableDetailCreateViewtable(mapBatch);
				} // for r
			} // if 
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("");
		} catch(NkiaException | ClassCastException e) {
			resultVO.setSuccess(true);
			resultVO.setResultMsg("["+StringUtil.parseString(mapBatch.get("class_kind"))+","+StringUtil.parseString(mapBatch.get("class_type"))+"] 뷰테이블 생성 오류");
			//throw new NkiaException(e);
		}
		return resultVO;
	}			
	
	@RequestMapping(value="/itg/itam/amdb/makeItamViewtableHistorySelect.do")
	public @ResponseBody ResultVO makeItamViewtableHistorySelect(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		Integer resultCount = 0;
		try{
			PagingUtil.getFristEndNum(paramMap);
			
			resultCount = amdbManagerService.makeItamViewtableHistorySelectCount(paramMap);
			List result = amdbManagerService.makeItamViewtableHistorySelect(paramMap);
			
			gridVO.setTotalCount( ( resultCount == null ? 0 : resultCount ) );
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	// ## *ITAM 뷰테이블 생성*
	// ##################################################################################################################################################
	
}
