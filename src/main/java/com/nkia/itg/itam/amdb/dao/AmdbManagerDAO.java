/**
 * 
 */
package com.nkia.itg.itam.amdb.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("amdbManagerDAO")
public class AmdbManagerDAO extends EgovComAbstractDAO {
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectClassEntityTree(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectClassEntityTree",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectEntityList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTblList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectTblList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTblSelList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectTblSelList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTblJoinList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectTblJoinList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectColList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectColList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectColSelList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectColSelList",paramMap);
	}
	
	/**
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public String generateEntityId()throws NkiaException{
		return (String)selectByPk("amdbManagerDAO.generateEntityId",null);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectJoinCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectJoinCount", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectTblCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectTblCount", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectColCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectColCount", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteJoinInfo(HashMap paramMap)throws NkiaException{
		delete("amdbManagerDAO.deleteJoinInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteTblInfo(HashMap paramMap)throws NkiaException{
		delete("amdbManagerDAO.deleteTblInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteColInfo(HashMap paramMap)throws NkiaException{
		delete("amdbManagerDAO.deleteColInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void inserEntityInfo(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.inserEntityInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertJoinInfo(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.insertJoinInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertTblInfo(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.insertTblInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertColInfo(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.insertColInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateChangeOrder(HashMap paramMap)throws NkiaException{
		update("amdbManagerDAO.updateChangeOrder", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectGroupInfo(HashMap paramMap)throws NkiaException{
		return (HashMap)selectByPk("amdbManagerDAO.selectGroupInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectInstallList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectInstallList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectInstallSelList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectInstallSelList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectRelList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelSelList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectRelSelList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectIntallCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectIntallCount", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteIntallInfo(HashMap paramMap)throws NkiaException{
		delete("amdbManagerDAO.deleteIntallInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertInstallInfo(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.insertInstallInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectRelCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectRelCount", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteRelInfo(HashMap paramMap)throws NkiaException{
		delete("amdbManagerDAO.deleteRelInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertRelInfo(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.insertRelInfo", paramMap);
	}
	
	/**
	 * 
	 * @param map
	 * @throws NkiaException
	 */
	public void deleteEntityInfo(HashMap map)throws NkiaException{
		delete("amdbManagerDAO.deleteEntityInfo", map);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildNodeList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectChildNodeList", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateEntityInfo(HashMap paramMap)throws NkiaException{
		update("amdbManagerDAO.updateEntityInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateEntityUseYn(HashMap paramMap)throws NkiaException{
		update("amdbManagerDAO.updateEntityUseYn", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectCompCodeCnt(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectCompCodeCnt", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityCopyList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectEntityCopyList", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectEntityCopyCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectEntityCopyCnt", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectCopyEntityTree(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectCopyEntityTree", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectCopyEntityList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.selectCopyEntityList",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void inserCopyAttrMapping(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.inserCopyAttrMapping",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void inserCopyCompMapping(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.inserCopyCompMapping",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void inserCopyRelMapping(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.inserCopyRelMapping",paramMap);
	}
	

	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void inserCopyTblMapping(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.inserCopyTblMapping",paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void inserCopyTblJoin(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.inserCopyTblJoin",paramMap);
	}
	
	/**
	 * 
	 * @param class_id
	 * @return
	 * @throws NkiaException
	 */
	public int selectCopyEntityCnt(String class_id)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectCopyEntityCnt", class_id);
	}
	
	/**
	 * 
	 * @param class_id
	 * @return
	 * @throws NkiaException
	 */
	public void deleteCopyEntityInfo(String class_id)throws NkiaException{
		delete("amdbManagerDAO.deleteCopyEntityInfo",class_id);
	}
	
	/**
	 * 
	 * @param class_id
	 * @return
	 * @throws NkiaException
	 */
	public void deleteCopyAttrMapping(String class_id)throws NkiaException{
		delete("amdbManagerDAO.deleteCopyAttrMapping",class_id);
	}

	/**
	 * 
	 * @param class_id
	 * @return
	 * @throws NkiaException
	 */
	public void deleteCopyCompMapping(String class_id)throws NkiaException{
		delete("amdbManagerDAO.deleteCopyCompMapping",class_id);
	}
	
	/**
	 * 
	 * @param class_id
	 * @return
	 * @throws NkiaException
	 */
	public void deleteCopyRelMapping(String class_id)throws NkiaException{
		delete("amdbManagerDAO.deleteCopyRelMapping",class_id);
	}

	/**
	 * 
	 * @param class_id
	 * @return
	 * @throws NkiaException
	 */
	public void deleteCopyTblMapping(String class_id)throws NkiaException{
		delete("amdbManagerDAO.deleteCopyTblMapping",class_id);
	}

	/**
	 * 
	 * @param class_id
	 * @return
	 * @throws NkiaException
	 */
	public void deleteCopyTblJoin(String class_id)throws NkiaException{
		delete("amdbManagerDAO.deleteCopyTblJoin",class_id);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectBtnCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("amdbManagerDAO.selectBtnCount", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteBtnInfo(HashMap paramMap)throws NkiaException{
		delete("amdbManagerDAO.deleteBtnInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBtnInfo(HashMap paramMap)throws NkiaException{
		insert("amdbManagerDAO.insertBtnInfo", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchEntityBtnList(HashMap paramMap)throws NkiaException{
		return list("amdbManagerDAO.searchEntityBtnList",paramMap);
	}

	
	/* *ITAM 뷰테이블 생성* Start */
	
	public Integer makeItamViewtableListSelectCount(ModelMap paramMap) {
		return (Integer)selectByPk("amdbManagerDAO.makeItamViewtableListSelectCount", paramMap);
	}

	public List makeItamViewtableListSelect(ModelMap paramMap) {
		return list("amdbManagerDAO.makeItamViewtableListSelect",paramMap);
	}

	public HashMap makeItamViewtableDetailRecord(ModelMap paramMap) {
		return (HashMap)selectByPk("amdbManagerDAO.makeItamViewtableDetailRecord", paramMap);
	}

	public void makeItamViewtableDetailDelete(ModelMap paramMap) {
		delete("amdbManagerDAO.makeItamViewtableDetailDelete", paramMap);
	}

	public void makeItamViewtableDetailSave(ModelMap paramMap) {
		insert("amdbManagerDAO.makeItamViewtableDetailSave", paramMap);
	}

	public List makeItamViewtableDetailMakeQuery(ModelMap paramMap) {
		return list("amdbManagerDAO.makeItamViewtableDetailMakeQuery",paramMap);
	}

	public void makeItamViewtableDetailCreateViewtable(ModelMap paramMap) throws NkiaException {
		try {
			insert("amdbManagerDAO.makeItamViewtableDetailCreateViewtable", paramMap);
		}
		catch ( Exception e ) {
			throw new NkiaException(e);
		}
	}

	public void makeItamViewtableDetailCreateViewtableApplydt(ModelMap paramMap) {
		insert("amdbManagerDAO.makeItamViewtableDetailCreateViewtableApplydt", paramMap);		
	}

	public void makeItamViewtableDetailCreateViewtableHistory(ModelMap paramMap) {
		insert("amdbManagerDAO.makeItamViewtableDetailCreateViewtableHistory", paramMap);
	}

	public Integer makeItamViewtableHistorySelectCount(ModelMap paramMap) {
		return (Integer)selectByPk("amdbManagerDAO.makeItamViewtableHistorySelectCount", paramMap);
	}

	public List makeItamViewtableHistorySelect(ModelMap paramMap) {
		return list("amdbManagerDAO.makeItamViewtableHistorySelect",paramMap);
	}

	/* *ITAM 뷰테이블 생성* End */

}
