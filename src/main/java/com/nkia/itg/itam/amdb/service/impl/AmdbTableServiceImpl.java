/**
 * 
 */
package com.nkia.itg.itam.amdb.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.amdb.dao.AmdbTableDAO;
import com.nkia.itg.itam.amdb.service.AmdbTableService;

import egovframework.com.utl.fcc.service.EgovStringUtil;

/**
 * @version 1.0 2013. 4. 12.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("amdbTableService")
public class AmdbTableServiceImpl implements AmdbTableService{

	private static final Logger logger = LoggerFactory.getLogger(AmdbTableServiceImpl.class);
	
	@Resource(name = "amdbTableDao")
	public AmdbTableDAO amdbTableDao;
	
	public int selectAmTblGridCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbTableDao.selectAmTblGridCount(paramMap);
	}  
	
	public List selectAmTblGridList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbTableDao.selectAmTblGridList(paramMap);
	}

	public List selectAmAttrGridList(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return amdbTableDao.selectAmAttrGridList(paramMap);
	}

	public List selectAmAttrSelGridList(ModelMap paramMap)
			throws NkiaException {
		// TODO Auto-generated method stub
		return amdbTableDao.selectAmAttrSelGridList(paramMap);
	}

	public void insertGenTableInfo(ModelMap paramMap) throws NkiaException {
		String gen_table_nm = (String)paramMap.get("gen_table_nm");
		// TODO Auto-generated method stub
		List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("sel_grid_data");
		
		for(int i=0;i<paramList.size();i++){
			HashMap dataMap = paramList.get(i);
			dataMap.put("gen_table_nm", gen_table_nm);
			amdbTableDao.insertGenTblAttrMapping(dataMap);
		}
		amdbTableDao.insertGenTableInfo(paramMap);
	}
	
	public void deleteGenTblInfo(ModelMap paramMap) throws NkiaException{
		amdbTableDao.deleteGenTableInfo(paramMap);
		amdbTableDao.deleteGenAttrMapping(paramMap);
	}
	
	public void updateGenTableInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		String old_gen_table_nm = (String)paramMap.get("old_gen_table_nm");
		String gen_table_nm		= (String)paramMap.get("gen_table_nm");
		
		paramMap.put("gen_table_nm", old_gen_table_nm);
		amdbTableDao.deleteGenAttrMapping(paramMap);
		
		// TODO Auto-generated method stub
		List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("sel_grid_data");
		
		for(int i=0;i<paramList.size();i++){
			HashMap dataMap = paramList.get(i);
			dataMap.put("gen_table_nm", gen_table_nm);
			dataMap.put("sort", i);
			amdbTableDao.insertGenTblAttrMapping(dataMap);
		}
		
		paramMap.put("gen_table_nm", gen_table_nm);
		updateGenTableOnlyInfo(paramMap);
	}
	
	public void updateGenTableOnlyInfo(ModelMap paramMap)throws NkiaException{
		amdbTableDao.updateGenTableInfo(paramMap);
	}
	
	
	public void createAmTable(ModelMap paramMap)throws NkiaException {
		//
		try{
			int tblCnt = amdbTableDao.selectTblSchemaCnt(paramMap);
			
			if(tblCnt == 0){
				List<HashMap> attrList 		= amdbTableDao.selectAmAttrSelGridList(paramMap);
				
				getTableSpaceNm(paramMap,"INDEX","I");
				getTableSpaceNm(paramMap,"TABLE","D");
				//테이블 생성
				createTableScript(paramMap,attrList);
				
				amdbTableDao.createTableScript(paramMap);
				//COMMENT 생성
				createCommentScript(paramMap,attrList);
				paramMap.put("resultFlag", "TS");
			}else{
				//테이블이 존재하는경우
				paramMap.put("resultFlag", "TE");
			}
		}catch(Exception e){
			paramMap.put("resultFlag", "TF");
			throw new NkiaException(e);
		}
	}
	
	public int selectGenTableNm(ModelMap paramMap) throws NkiaException {

		return amdbTableDao.selectGenTableNm(paramMap);
	}

	public HashMap selectGenTableInfo(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbTableDao.selectGenTableInfo(paramMap);
	}
	
	public void createTableScript(ModelMap paramMap,List<HashMap> attrList)throws NkiaException{
		
			StringBuffer tblSql 	= new StringBuffer();
			StringBuffer constSql	= new StringBuffer();
			StringBuffer indexSql	= new StringBuffer();
			
			//String required_yn 	= "";
			String ispk 		= "";
			String comma		= "";
			String colId		= "";
			String colType		= "";
			String colSet		= "";
			String column		= "";
			String index		= "";
			String constr		= "";
			//컬럼정보
			for(int i=0;i<attrList.size();i++){
				
				HashMap attrMap = attrList.get(i);
				//required_yn 	= (String)attrMap.get("REQUIRED_YN");
				ispk 			= StringUtil.parseString(attrMap.get("PK_YN"));
				colId			= StringUtil.parseString(attrMap.get("COL_ID"));
				colType			= StringUtil.parseString(attrMap.get("COL_TYPE"));
				
				if(i == 0){
					comma		= "";
				}else{
					comma		= ",";
				}
				
				if(ispk.equalsIgnoreCase("Y")){
					constSql.append(comma+colId);
					indexSql.append(comma+colId+" ASC");
				}
				
				if("VARCHAR2".equals(colType) || "VARCHAR".equals(colType) || "CHAR".equals(colType)){
					colSet = " "+colType + "(" +attrMap.get("COL_LEN")+" BYTE)\n";
				}else if("NUMBER".equals(colType) || "DATE".equals(colType)){
					colSet = " "+colType;
				}
				tblSql.append(comma+colId+colSet);
			}
			
			constr 	= constSql.toString();
			index	= indexSql.toString();
			column	= tblSql.toString();
			
			if(constr.startsWith(",")){
				constr 	= constr.substring(1,constr.length());
				index 	= index.substring(1,index.length());
			}
			
			paramMap.put("colInfo"	, column);
			paramMap.put("indexInfo", index);
			paramMap.put("constInfo", constr);
	}
	
	public void createCommentScript(ModelMap paramMap,List<HashMap> attrList)throws NkiaException{
		
		String gen_table_nm 	= (String)paramMap.get("gen_table_nm");
		String gen_table_nm_log = (String)paramMap.get("gen_table_nm_log");
		String commentSql		= "";
		
		commentSql = "COMMENT ON TABLE "+gen_table_nm+" IS '"+gen_table_nm_log+"'";
		amdbTableDao.createExcuteScript(commentSql);
		
		for(int i=0;i<attrList.size();i++){
			HashMap attrMap = (HashMap)attrList.get(i);
			commentSql = "COMMENT ON COLUMN "+gen_table_nm+"."+attrMap.get("COL_ID")+" IS '"+attrMap.get("COL_NAME")+"'";
			amdbTableDao.createExcuteScript(commentSql);
		}
	}

	public void getTableSpaceNm(ModelMap paramMap,String seg_type,String spc_prifix)throws NkiaException{
		paramMap.put("seg_type"	, seg_type);
		paramMap.put("tbl_spc"	, spc_prifix);
		String indexTablspace 	= amdbTableDao.selectUserTableSpace(paramMap);
		paramMap.put(seg_type.toLowerCase()+"Tablspace", indexTablspace);
	}
	
	
	public void updateGenYn(ModelMap paramMap)throws NkiaException{
		String resultFlag = (String)paramMap.get("resultFlag");
		String alterFlag = (String)paramMap.get("alterFlag");
		
		if("TS".equalsIgnoreCase(resultFlag) 
			|| "Y".equalsIgnoreCase(alterFlag)){
			
			amdbTableDao.updateGenYn(paramMap);
			amdbTableDao.updateGenYnCols(paramMap);
		}
	}
	
	public void convertToLobSource() throws NkiaException {
		// TODO Auto-generated method stub
		amdbTableDao.deleteTempTbl();
		amdbTableDao.isnertTempTbl();
	}

	public List selectAmRelationTblList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbTableDao.selectAmRelationTblList(paramMap);
	}
	
	public String selectScriptInfo(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		String gen_table_nm = (String)paramMap.get("gen_table_nm");
		
		String result 		= EgovStringUtil.checkHtmlView(amdbTableDao.selectScriptInfo(paramMap));
		result = StringUtil.replaceAll(result, gen_table_nm, "<font style='color:red;font-weight:bold;font-style:oblique;'>"+gen_table_nm+"</font>");
		return result;
	}
	
	public void createBackupTable(ModelMap paramMap)throws NkiaException{
		try{
			HashMap dataMap 	= new HashMap();
			String gen_table_nm = (String)paramMap.get("gen_table_nm");
			String backupTblNm  = gen_table_nm+"_"+DateUtil.getDateFormat();
			
			paramMap.put("back_table_nm", backupTblNm);
			dataMap.put("gen_table_nm"	, backupTblNm);
			
			//백업 테이블 존재 여부 확인.
			int backTblCnt 	= amdbTableDao.selectTblSchemaCnt(dataMap);
			int genTblCnt 	= amdbTableDao.selectTblSchemaCnt(paramMap);
			
			if(backTblCnt == 0 && genTblCnt == 1){
				amdbTableDao.createBackupTable(paramMap);
				amdbTableDao.insertBackupHistory(paramMap);
				paramMap.put("backFlag", "BS");
			}else if(genTblCnt == 0){
				//실제 테이블이 존재하지 않는경우
				paramMap.put("backFlag", "BZ");
			}else{
				paramMap.put("backFlag", "BF");
			}
		}catch(Exception e){
			paramMap.put("backFlag", "BF");
			throw new NkiaException(e);
		}
	}
	
	public void dropAmTable(ModelMap paramMap)throws NkiaException {
		try{
			String tblNm = (String)paramMap.get("gen_table_nm");
			int cnt 		= amdbTableDao.selectDropValidation(tblNm);
			int genTblCnt 	= amdbTableDao.selectTblSchemaCnt(paramMap);
			
			//자산 테이블 관리에서 관리되는 테이블 만 삭제 할수 있게 한다.  파라메터 변조나 변수 변환등으로 다른 테이블을 삭제 할수도 있음...
			if(cnt > 0 && genTblCnt > 0){
				amdbTableDao.dropAmTable(tblNm);
				paramMap.put("dropFlag", "DS");
			}else if(genTblCnt == 0){
				paramMap.put("dropFlag", "DZ");
			}else{
				paramMap.put("dropFlag", "DN");
			}
		}catch(Exception e){
			paramMap.put("dropFlag", "DF");
			throw new NkiaException(e);
		}
	}

	public void migrationTable(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		try{
			
			StringBuffer curColSb	= new StringBuffer();
			StringBuffer preColSb	= new StringBuffer();
			List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("mod_grid_data");
			String backFlag = (String)paramMap.get("backFlag");
			String curCol	= "";
			String preCol	= "";
			String comma 	= "";
			
			if("BS".equals(backFlag)){
				for(int i=0;i<paramList.size();i++){
					
					HashMap dataMap 	= (HashMap)paramList.get(i);
					curCol 	= (String)dataMap.get("COL_ID");
					preCol 	= (String)dataMap.get("BK_COL_ID");
					
					if(i == 0){
						comma		= "";
					}else{
						comma		= ",";
					}
					
					curColSb.append(comma+curCol);
					
					if(preCol != null && !"".equals(preCol)){
						preColSb.append(comma+preCol);
					}else{
						preColSb.append(comma+"NULL");
					}
				}
				
				paramMap.put("curColSb", curColSb.toString());
				paramMap.put("preColSb", preColSb.toString());
				
				amdbTableDao.migrationTable(paramMap);
				paramMap.put("migrFlag", "MS");
			}else{
				paramMap.put("migrFlag", "MZ");
			}
		}catch(Exception e){
			paramMap.put("migrFlag", "MF");
			throw new NkiaException(e);
		}
	}
	
	public void dropConstsIndex(ModelMap paramMap)throws NkiaException{
		try{
			String gen_table_nm = StringUtil.parseString(paramMap.get("gen_table_nm"));
			
			amdbTableDao.dropConstraints(gen_table_nm);
			amdbTableDao.dropIndex(gen_table_nm);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void creteConstsIndex(ModelMap paramMap)throws NkiaException{
		try{
			
			String indexInfo = (String)paramMap.get("indexInfo");
			
			if(indexInfo != null && !"".equals(indexInfo)){
				amdbTableDao.createIndexScript(paramMap);
				amdbTableDao.createConstScript(paramMap);
				paramMap.put("constFlag", "IS");
			}else{
				paramMap.put("constFlag", "IZ");
			}
		}catch(Exception e){
			paramMap.put("constFlag", "IF");
//			throw new NkiaException(e);
		}
	}

	@Override
	public List selectAttributeCombo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbTableDao.selectAttributeCombo(paramMap);
	}

	@Override
	public void alterAmTable(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		try{
			List<HashMap> currList = (ArrayList<HashMap>)paramMap.get("sel_grid_data");
			List<HashMap> prevList	= amdbTableDao.selectAmAttrSelGridList(paramMap);
			
			paramMap.put("selectList", currList);
			paramMap.put("notInList", prevList);
			paramMap.put("selType", "A");
			
			List<HashMap> alterAddList = amdbTableDao.selectAlterList(paramMap);
			
			paramMap.put("selectList", prevList);
			paramMap.put("notInList", currList);
			paramMap.put("selType", "D");
			
			List<HashMap> alterDropList = amdbTableDao.selectAlterList(paramMap);
			
			//alter drop logic
			if(alterAddList.size() > 0 || alterDropList.size() > 0 ){
				
				executeAlterQuery(alterAddList,paramMap,"A");
				
				// 결합인덱스 제거 필요때문에, drop 컬럼 실행 전에 index부터 제거
//				dropConstsIndex(paramMap);
				executeAlterQuery(alterDropList,paramMap,"D");
				
				createCommentScript(paramMap,alterAddList);
				
				getTableSpaceNm(paramMap,"INDEX","I");
				getTableSpaceNm(paramMap,"TABLE","D");
				
				createTableScript(paramMap,currList);
				
				paramMap.put("alterFlag", "AS");
			}else{
				paramMap.put("alterFlag", "AN");
			}
		}catch(Exception e){
			paramMap.put("alterFlag", "AF");
			throw new NkiaException(e);
		}
	}
	
	public void executeAlterQuery(List<HashMap> attrList,HashMap paramMap,String type)throws NkiaException{
		
		try{
			String gen_table_nm_log = StringUtil.parseString(paramMap.get("gen_table_nm"));
			
			for(int i=0;i<attrList.size();i++){
				HashMap attrMap = attrList.get(i);
				StringBuffer sb = new StringBuffer();
				String colId = StringUtil.parseString(attrMap.get("COL_ID"));
				String colType = StringUtil.parseString(attrMap.get("COL_TYPE"));
				String colLen = StringUtil.parseString(attrMap.get("COL_LEN"));
				
				if("A".equals(type)){
					sb.append("ALTER TABLE "+gen_table_nm_log+" ADD ");
					if("VARCHAR2".equals(colType) || "VARCHAR".equals(colType) || "CHAR".equals(colType)){
						sb.append(colId + " "+colType + "(" +colLen+" BYTE)");
					}else if("NUMBER".equals(colType) || "DATE".equals(colType)){
						sb.append(colId + " "+colType);
					}
				}else{
					sb.append("ALTER TABLE "+gen_table_nm_log+" DROP COLUMN " + colId);
				}
				
				amdbTableDao.executeAlterQuery(sb.toString());
			}
		}catch(Exception e){
			throw new NkiaException(e);
		}
	}

	//hmsong cmdb 개선 작업 Start
	@Override
	public List selectNotNullTblCode(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List returnList = new ArrayList();

		if((paramMap != null) && (paramMap.get("code_grp_id_list") != null)) {
			returnList = amdbTableDao.selectNotNullTblCode(paramMap);
		}
		
		return returnList;
	}
	//hmsong cmdb 개선 작업 End
}
