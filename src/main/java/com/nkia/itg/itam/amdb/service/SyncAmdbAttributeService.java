/**
 * 
 */
package com.nkia.itg.itam.amdb.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 1.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface SyncAmdbAttributeService {
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List selectSyncClassEntityTree(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 * @return
	 */
	public List searchTargetClassList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List selectDetailTargetTree(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchGroupList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchChildEntityList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchTblJoinList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingTblList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingAttrList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingBtnList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingCompList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingRelList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 */
	public void updateCopyEntity(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 */
	public void updateSyncMappingTable(ModelMap paramMap) throws NkiaException;
	
	// 컴포넌트 복사
	public void insertCopyComponent(ModelMap paramMap) throws NkiaException;

}
