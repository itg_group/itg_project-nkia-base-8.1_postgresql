/**
 * 
 */
package com.nkia.itg.itam.amdb.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.ItgBaseService;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.amdb.service.AmdbTableService;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * @version 1.0 2013. 4. 12.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class AmdbTableController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "amdbTableService")
	private AmdbTableService amdbTableService;
	
	@Resource(name = "commonCodeService")
	private CommonCodeService commonCodeService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "itgBaseService")
	private ItgBaseService itgBaseService;
	/**
	 * 테이블 관리 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbTable.do")
	public String amdbManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		//hmsong cmdb 개선 작업 Start
		/*
		 * 목적 : CMDB 구조 유지를 위한 필수 속성 로딩(임의 수정 금지)
		 * NOT_NULL_ALL : 모든 테이블에 필수로 들어가야 할 컬럼리스트를 정의한 공통코드
		 * NOT_NULL_{TABLE_NAME} : {TABLE_NAME} 테이블에 필수로 들어가야 할 컬럼리스트를 정의한 공통코드
		    . 프라퍼티 ( Globals.Itg.Cmdb.NotNullTable ) 에 정의된 공통코드만 로딩함.
		    . EX : NOT_NULL_AM_ASSET , NOT_NULL_AM_CONF 

		 **/
		String notNullColumn_CodeGrpId = new String();	//notNullColumn_CodeGrpId - not null 속성 정의
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.NotNullTable") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.NotNullTable")))) {
				notNullColumn_CodeGrpId = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.NotNullTable"));
			}
		}
		paramMap.put("notNullColumn_CodeGrpId", notNullColumn_CodeGrpId);
		//hmsong cmdb 개선 작업 End
		
		return "/itg/itam/amdb/amdbTableMng.nvf";
	}
	
	/**
	 * 테이블 관리 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchAmTblGridList.do")
	public @ResponseBody ResultVO searchAmTblGridList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			commonCodeService.getSearchList(paramMap,"tblSearchKey","tblSearchValue","AM_TBL_SEARCH_TYPE");
			
			int totalCount 	= amdbTableService.selectAmTblGridCount(paramMap);
			List resultList	= amdbTableService.selectAmTblGridList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 관리항목 미포함 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	
	@RequestMapping(value="/itg/itam/amdb/selectAmAttrGridList.do")
	public @ResponseBody ResultVO searchAmNotContainTableGridList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = amdbTableService.selectAmAttrGridList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 관리항목 포함 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectAmAttrSelGridList.do")
	public @ResponseBody ResultVO searchAmInContainTableGridList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = amdbTableService.selectAmAttrSelGridList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	
	
	
	/**
	 * 테이블 관리 신규 테이블 입력
	 * 테이블 관리 입력한 테이블명으로 테이블 생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertGenTableInfo.do")
	public @ResponseBody ResultVO insertGenTableInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			amdbTableService.insertGenTableInfo(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테이블 관리 신규 테이블 입력
	 * 테이블 관리 입력한 테이블명으로 테이블 생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/deleteGenTblInfo.do")
	public @ResponseBody ResultVO deleteGenTblInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			amdbTableService.deleteGenTblInfo(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 테이블 관리 신규 테이블 입력
	 * 테이블 관리 입력한 테이블명으로 테이블 생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/updateGenTableInfo.do")
	public @ResponseBody ResultVO updateGenTableInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			amdbTableService.updateGenTableInfo(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테이블 관리 신규 테이블 입력
	 * 테이블 관리 입력한 테이블명으로 테이블 생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/updateGenTableOnlyInfo.do")
	public @ResponseBody ResultVO updateGenTableOnlyInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			amdbTableService.updateGenTableOnlyInfo(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	

	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectGenTableNm.do")
	public @ResponseBody ResultVO selectGenTableNm(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			int result = amdbTableService.selectGenTableNm(paramMap);
			resultVO.setResultInt(result);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectGenTableInfo.do")
	public @ResponseBody ResultVO selectGenTableInfo(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			HashMap result = amdbTableService.selectGenTableInfo(paramMap);
			resultVO.setResultMap(result);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 테이블 관리 신규 테이블 입력
	 * 테이블 관리 입력한 테이블명으로 테이블 생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/createAmTable.do")
	public @ResponseBody ResultVO createAmTable(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			

			amdbTableService.updateGenTableInfo(paramMap);
			amdbTableService.createAmTable(paramMap);
			String resultFlag = (String)paramMap.get("resultFlag");
			
			if("TS".equals(resultFlag)){
				//제약조건 생성 backup 때문에 분리 테이블 만들고 추후생성할수 있도로 service 분리
				amdbTableService.creteConstsIndex(paramMap);
				//생성 여부 update 처리후 rollback 위해 tranjection 을 끊어 놓는다.
				paramMap.put("gen_yn", "Y");
				paramMap.put("use_yn", "Y");
				amdbTableService.updateGenYn(paramMap);
			}
			resultVO.setResultMap(paramMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/itam/amdb/selectAmRelationObjList.do")
	public @ResponseBody ResultVO selectAmRelationTblList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			amdbTableService.convertToLobSource();
			List resultList = amdbTableService.selectAmRelationTblList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectScriptInfo.do")
	public @ResponseBody ResultVO selectScriptInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			String scriptInfo = amdbTableService.selectScriptInfo(paramMap);
			resultVO.setResultString(scriptInfo);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테이블 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/alterAmTable.do")
	public @ResponseBody ResultVO alterAmTable(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			String flag = "";
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			
			//백업 테이블 생성
			amdbTableService.createBackupTable(paramMap);
			
			String backflag = (String)paramMap.get("backFlag");
			
			//백업 테이블 생성 S : 성공 , BZ : 실제 테이블 존재하지않음 , BF : 생성 실패
			if("BS".equals(backflag) || "BZ".equals(backflag)){
				
				//amdbTableService.dropAmTable(paramMap);
				//String dropFlag = (String)paramMap.get("dropFlag");
				amdbTableService.alterAmTable(paramMap);
				String alterFlag = (String)paramMap.get("alterFlag");
				
				if("AS".equalsIgnoreCase(alterFlag)){ 
					//
					amdbTableService.updateGenTableInfo(paramMap);					
					//테이블 생성 S : 성공 , F : 실패  
					//amdbTableService.migrationTable(paramMap);
					
//					amdbTableService.creteConstsIndex(paramMap);
					
				}
			}
			resultVO.setResultMap(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테이블 드랍
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/dropAmTable.do")
	public @ResponseBody ResultVO dropAmTable(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			String flag = "";
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			
			//백업 테이블 생성
			amdbTableService.createBackupTable(paramMap);
			
			String backflag = (String)paramMap.get("backFlag");
			//백업 테이블 생성 S : 성공 , BZ : 실제 테이블 존재하지않음 , BF : 생성 실패
			if("BS".equals(backflag) || "BZ".equals(backflag)){
				
				//테이블 드랍
				amdbTableService.dropAmTable(paramMap);
				
				String dropFlag = (String)paramMap.get("dropFlag");
				
				//드랍 테이블 생성 S : 성공 , DZ : 실제 테이블 존재하지않음 , DN : 자산 관리 테이블 아님 , DF : 실패  
				if("DS".equals(dropFlag) || "DZ".equals(dropFlag)){
					String info_del_yn = (String)paramMap.get("info_del_yn");
					
					//설정 데이터 포함 삭제
					if("Y".equals(info_del_yn)){
						amdbTableService.deleteGenTblInfo(paramMap);
					}else{
						//설정 데이터 삭제 안함
						//테이블 생성 시 필수 데이터 초기화
						paramMap.put("gen_yn", "N");
						paramMap.put("gen_dt", "");
						paramMap.put("gen_user", "");
						amdbTableService.updateGenTableInfo(paramMap);
					}
				}
			}
			resultVO.setResultMap(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectAttributeCombo.do")
	public @ResponseBody ResultVO selectAttributeCombo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List result = amdbTableService.selectAttributeCombo(paramMap);
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//hmsong cmdb 개선 작업 Start
	/**
	 * 관리항목 포함 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectNotNullTblCode.do")
	public @ResponseBody ResultVO selectNotNullTblCode(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = amdbTableService.selectNotNullTblCode(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	//hmsong cmdb 개선 작업 End
}	