/**
 * 
 */
package com.nkia.itg.itam.amdb.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface AmdbManagerService {
	
	public List selectClassEntityTree(ModelMap paramMap) throws NkiaException;
	
	public List selectEntityList(ModelMap paramMap)throws NkiaException;

	public List selectTblList(ModelMap paramMap)throws NkiaException;
	
	public List selectTblSelList(ModelMap paramMap)throws NkiaException;
	
	public List selectTblJoinList(ModelMap paramMap)throws NkiaException;
	
	public List selectColList(ModelMap paramMap)throws NkiaException;
	
	public List selectColSelList(ModelMap paramMap)throws NkiaException;
	
	public void insertEntityInfo(ModelMap paramMap)throws NkiaException;

	public void generateEntityId(ModelMap paramMap) throws NkiaException;
	
	public void insertTblInfo(ModelMap paramMap) throws NkiaException;
	
	public void insertJoinInfo(ModelMap paramMap) throws NkiaException;
	
	public void insertColInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateChangeOrder(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectGroupInfo(ModelMap paramMap) throws NkiaException;
	
	public List selectInstallList(ModelMap paramMap)throws NkiaException;
	
	public List selectInstallSelList(ModelMap paramMap)throws NkiaException;
	
	public List selectRelList(ModelMap paramMap)throws NkiaException;
	
	public List selectRelSelList(ModelMap paramMap)throws NkiaException;
	
	public void insertInstallInfo(ModelMap paramMap)throws NkiaException;
	
	public void insertRelInfo(ModelMap paramMap)throws NkiaException;
	
	public void deleteAllEntityInfo(ModelMap paramMap)throws NkiaException;
	
	public void updateEntityInfo(ModelMap paramMap)throws NkiaException;
	
	public HashMap selectCompCodeCnt(ModelMap paramMap)throws NkiaException;
	
	public List selectEntityCopyList(HashMap paramMap)throws NkiaException;
	
	public int selectEntityCopyCnt(HashMap paramMap)throws NkiaException;
	
	public List selectCopyEntityTree(HashMap paramMap)throws NkiaException;
	
	public void insertCopyEntity(HashMap paramMap)throws NkiaException;

	public void insertCopyEntityOption(List<HashMap> idList) throws NkiaException;

	public void compareIdSet(List<HashMap> idList, String compareIds, HashMap dataMap, String setKey) throws NkiaException;

	public List selectCopyIdGentList(List<HashMap> targetEntityList) throws NkiaException;

	public void deleteCopyAllEntityInfo(String class_id) throws NkiaException;
	
	public void insertBtnInfo(ModelMap paramMap)throws NkiaException;
	
	public List searchEntityBtnList(ModelMap paramMap) throws NkiaException;

	/* *ITAM 뷰테이블 생성* Start */
	public Integer makeItamViewtableListSelectCount(ModelMap paramMap) throws NkiaException;
	public List makeItamViewtableListSelect(ModelMap paramMap) throws NkiaException;
	public HashMap makeItamViewtableDetailRecord(ModelMap paramMap) throws NkiaException;
	public boolean makeItamViewtableDetailSave(ModelMap paramMap) throws NkiaException;
	public HashMap makeItamViewtableDetailMakeQuery(ModelMap paramMap) throws NkiaException;
	public void makeItamViewtableDetailCreateViewtable(ModelMap paramMap) throws NkiaException;
	public Integer makeItamViewtableHistorySelectCount(ModelMap paramMap)throws NkiaException;
	public List makeItamViewtableHistorySelect(ModelMap paramMap)throws NkiaException;
	/* *ITAM 뷰테이블 생성* End */





	
}
