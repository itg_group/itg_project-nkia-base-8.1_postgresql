package com.nkia.itg.itam.amdb.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AmdbClassService {

	public List searchAmClassAllTree(ModelMap paramMap) throws NkiaException;

	public List selectAmClassBaseInfo(ModelMap paramMap) throws NkiaException;

	public int searchAmClassGridListCount(ModelMap paramMap) throws NkiaException;

	public List searchAmClassGridList(ModelMap paramMap) throws NkiaException;

	public void updateAmClassInfo(ModelMap paramMap) throws NkiaException;

	public void updateAmClassOrder(ModelMap paramMap) throws NkiaException;

	public void insertAmClassInfo(ModelMap paramMap) throws NkiaException;

	public void deleteAmClassInfo(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 * @return
	 */
	public List searchAmClassCheckTree(ModelMap paramMap) throws NkiaException;

	public int selectAssetCount(ModelMap paramMap) throws NkiaException;
}
