/**
 * @version 1.0 2013. 4. 1.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
package com.nkia.itg.itam.amdb.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.amdb.service.AmdbAttributeService;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AmdbAttributeController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "amdbAttributeService")
	private AmdbAttributeService amdbAttributeService;
	
	@Resource(name = "commonCodeService")
	private CommonCodeService commonCodeService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 속성관리 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbAttribute.do")
	public String amdbAttribute(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/amdb/amdbAttributeList.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/amdb/amdbAttribute_webix.do")
	public String amdbAttributeWebix(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/amdb/amdbAttributeList_webix.nvf";
		return forwarPage;
	}
	
	/**
	 * 속성관리 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchAmAttributeGridList.do")
	public @ResponseBody ResultVO searchAmAttributeGridList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			commonCodeService.getSearchList(paramMap,"attrSearchKey","attrSearchValue","ATTR_SEARCH_KEY");
			int totalCount 	= amdbAttributeService.searchAmAttributeGridListCount(paramMap);
			List resultList	= amdbAttributeService.searchAmAttributeGridList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 속성관리 기본정보 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/updateAmAttributeInfo.do")
	public @ResponseBody ResultVO updateAmAttributeInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			amdbAttributeService.updateAmAttributeInfo(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 속성관리 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/deleteAmAttributeInfo.do")
	public @ResponseBody ResultVO deleteAmAttributeInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			amdbAttributeService.deleteAmAttributeInfo(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 속성관리 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertAmAttributeInfo.do")
	public @ResponseBody ResultVO insertAmAttributeInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			amdbAttributeService.insertAmAttributeInfo(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectAttributeFieldNm.do")
	public @ResponseBody ResultVO selectAttributeFieldNm(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			int result = amdbAttributeService.selectAttributeFieldNm(paramMap);
			resultVO.setResultInt(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectAttributeInfo.do")
	public @ResponseBody ResultVO selectAttributeInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap result = amdbAttributeService.selectAttributeInfo(paramMap);
			resultVO.setResultMap(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectPopupList.do")
	public @ResponseBody ResultVO selectPopupList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList	= amdbAttributeService.selectPopupList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/selectGenTableList.do")
	public @ResponseBody ResultVO selectGenTableList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			int totalCount 	= amdbAttributeService.selectGenTableCnt(paramMap);
			List resultList	= amdbAttributeService.selectGenTableList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
