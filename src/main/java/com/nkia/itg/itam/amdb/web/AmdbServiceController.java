/*
 * @(#)AmdbServiceController.java              2013. 5. 22.
 *
 * @eungyu@nakia.co.kr
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.amdb.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.amdb.service.AmdbServiceService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AmdbServiceController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "amdbServiceService")
	private AmdbServiceService amdbServiceService;
	
	/**
	 * message 호출
	 */	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 분류체계관리 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbService.do")
	public String amdbService(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/amdb/amdbServiceMng.nvf";
		return forwarPage;
	}
	
	/**
	 * 사용자별 서비스관리 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/amdbServiceByUser.do")
	public String amdbServiceByUser(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/amdb/amdbServiceMngByUser.nvf";
		return forwarPage;
	}

	/**
	 * 서비스정보_트리생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchAmdbServiceAllTree.do")
	public @ResponseBody ResultVO searchAmdbServiceAllTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		TreeVO treeVO = null;
		List resultList = new ArrayList();
		try{
			resultList = amdbServiceService.searchAmdbServiceAllTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스정보_트리생성(맵)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/amdb/searchAmdbServiceAllTreeMap.do")
	public @ResponseBody ResultVO searchAmdbServiceAllTreeMap(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = amdbServiceService.searchAmdbServiceAllTreeMap(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeMapToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티코드 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchCodeDataList.do")
	public @ResponseBody ResultVO searchCodeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = null;
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try{
			resultList = amdbServiceService.searchCodeDataList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}	
	
	/**
	 * 서비스_상세정보
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectAmdbServiceBaseInfo.do")
	public @ResponseBody ResultVO selectAmdbServiceBaseInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = amdbServiceService.selectAmdbServiceBaseInfo(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스 사용자 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchAmdbServiceGridList.do")
	public @ResponseBody ResultVO searchAmdbServiceGridList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = amdbServiceService.searchAmdbServiceGridList(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 서비스 리스트 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchAmdbServiceLowServiceList.do")
	public @ResponseBody ResultVO searchAmdbServiceLowServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = amdbServiceService.searchAmdbServiceLowServiceList(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스 기본정보 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/updateAmdbServiceInfo.do")
	public @ResponseBody ResultVO updateAmdbServiceInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap oldOrderMap = new HashMap();
			oldOrderMap.put("oldOrderList", paramMap.get("oldOrderList"));
			oldOrderMap.put("up_service_id", paramMap.get("selUpServiceId"));
			paramMap.remove("oldOrderList");
			paramMap.remove("selUpServiceId");
			amdbServiceService.updateAmdbServiceInfo(paramMap);
			if(oldOrderMap.get(oldOrderMap) != null){
				amdbServiceService.updateServiceOrder(oldOrderMap);
			}
			
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위서비스목록 순서변경
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/updateAmdbServiceLowService.do")
	public @ResponseBody ResultVO updateAmdbServiceLowService(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			amdbServiceService.updateAmdbServiceLowService(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스관리 기본정보 입력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertAmdbServiceInfo.do")
	public @ResponseBody ResultVO insertAmdbServiceInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			amdbServiceService.insertAmdbServiceInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스관리 담당자정보 입력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertAmdbServiceUser.do")
	public @ResponseBody ResultVO insertAmdbServiceUser(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			amdbServiceService.insertAmdbServiceUser(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/deleteAmdbServiceInfo.do")
	public @ResponseBody ResultVO deleteAmdbServiceInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			amdbServiceService.deleteAmdbServiceInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스 담당자 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/deleteAmdbServiceUser.do")
	public @ResponseBody ResultVO deleteAmdbServiceUser(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			amdbServiceService.deleteAmdbServiceUser(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 추가된 부서조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/amdb/selectServiceCust.do")
	public @ResponseBody ResultVO selectServiceCust(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = amdbServiceService.selectServiceCust(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.itam.service.00008"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서추가
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertServiceCust.do")
	public @ResponseBody ResultVO insertServiceCust(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			amdbServiceService.insertServiceCust(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 서비스 그리드 생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/amdb/searchServiceList.do")
	public @ResponseBody ResultVO searchServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = amdbServiceService.searchServiceList(paramMap);		
			totalCount = amdbServiceService.searchServiceListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 연계 구성정보 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/amdb/searchServiceToRelConfList.do")
	public @ResponseBody ResultVO searchServiceToRelConfList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
//			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
//			PagingUtil.getFristEndNum(paramMap);
			
			resultList = amdbServiceService.searchServiceToRelConfList(paramMap);		
//			totalCount = amdbServiceService.searchServiceToRelConfListCount(paramMap);
			
//			gridVO.setTotalCount(totalCount);
//			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 구성연계팝업 - 구성리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/amdb/searchServiceConfList.do")
	public @ResponseBody ResultVO searchServiceConfList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Boolean isAuthenticated = false;
		
		try{
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("user_id", userVO.getUser_id());
	    	}
			
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
//			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
//			PagingUtil.getFristEndNum(paramMap);
			
			resultList = amdbServiceService.searchServiceConfList(paramMap);		
//			totalCount = amdbServiceService.searchServiceConfListCount(paramMap);
			
//			gridVO.setTotalCount(totalCount);
//			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스 코드 그리드 생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/amdb/searchServiceCodeList.do")
	public @ResponseBody ResultVO searchServiceCodeList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			if(!StringUtils.isEmpty(paramMap.get("page"))) {
				//@@ 페이징 관련 내용 
				PagingUtil.getFristEndNum(paramMap);
				totalCount = amdbServiceService.searchServiceCodeListCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}

			resultList = amdbServiceService.searchServiceCodeList(paramMap);		
			
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스 코드 조회 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/amdb/searchServiceCodeListExcelDown.do")
    public @ResponseBody ResultVO searchServiceCodeListExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = amdbServiceService.searchServiceCodeList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
