/**
 * @version 1.0 2014. 6. 16.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jyjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
package com.nkia.itg.itam.amdb.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("syncAmdbAttributeDAO")
public class SyncAmdbAttributeDAO extends EgovComAbstractDAO {
	
	/**
	 * @param paramMap
	 * @return
	 */
	public String generateEntityId() throws NkiaException{
		return (String)selectByPk("amdbManagerDAO.generateEntityId", null);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSyncClassEntityTree(HashMap paramMap)throws NkiaException{
		return list("syncAmdbAttributeDAO.selectSyncClassEntityTree",paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List selectConnectByEntityIdList(HashMap paramMap) throws NkiaException {
		
		List resultList = new ArrayList();
		List dataList = new ArrayList();
		dataList = list("syncAmdbAttributeDAO.selectConnectByEntityIdList", paramMap);
		
		for(int i=0; i<dataList.size(); i++) {
			HashMap dataMap = new HashMap();
			
			dataMap = (HashMap)dataList.get(i);
			String id = (String)dataMap.get("ENTITY_ID");
			
			resultList.add(id);
		}
		
		return resultList;
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List selectChildEntityIdList(HashMap paramMap) throws NkiaException {
		
		List resultList = new ArrayList();
		List dataList = new ArrayList();
		dataList = list("syncAmdbAttributeDAO.selectChildEntityIdList", paramMap);
		
		for(int i=0; i<dataList.size(); i++) {
			HashMap dataMap = new HashMap();
			
			dataMap = (HashMap)dataList.get(i);
			String id = (String)dataMap.get("ENTITY_ID");
			
			resultList.add(id);
		}
		
		return resultList;
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchCopyTargetClassList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchCopyTargetClassList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchSyncTargetClassList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchSyncTargetClassList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchNotGroupCompTargetClassList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchNotGroupCompTargetClassList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchInGroupCompTargetClassList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchInGroupCompTargetClassList", paramMap);
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 */
	public List selectDetailTargetTree(HashMap paramMap)throws NkiaException{
		return list("syncAmdbAttributeDAO.selectDetailTargetTree",paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchGroupList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchGroupList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchTblJoinList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchTblJoinList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingTblList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchMappingTblList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchChildEntityList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchChildEntityList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingAttrList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchMappingAttrList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingBtnList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchMappingBtnList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingCompList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchMappingCompList", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingRelList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchMappingRelList", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void deleteSyncAmEntity(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("syncAmdbAttributeDAO.deleteSyncAmEntity", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void insertSyncAmEntity(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertSyncAmEntity", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void deleteSyncTblJoin(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("syncAmdbAttributeDAO.deleteSyncTblJoin", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void insertSyncTblJoin(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertSyncTblJoin", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void deleteSyncTblMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("syncAmdbAttributeDAO.deleteSyncTblMapping", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void insertSyncTblMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertSyncTblMapping", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void deleteSyncAttrMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("syncAmdbAttributeDAO.deleteSyncAttrMapping", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void insertSyncAttrMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertSyncAttrMapping", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void deleteSyncCompMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("syncAmdbAttributeDAO.deleteSyncCompMapping", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void insertSyncCompMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertSyncCompMapping", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void deleteSyncRelMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("syncAmdbAttributeDAO.deleteSyncRelMapping", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void insertSyncRelMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertSyncRelMapping", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void deleteSyncBtnMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		delete("syncAmdbAttributeDAO.deleteSyncBtnMapping", paramMap);
	}

	/**
	 * @param paramMap
	 */
	public void insertSyncBtnMapping(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertSyncBtnMapping", paramMap);
	}
	
	/**
	 * @param paramMap
	 */
	public void insertCopyComponent(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertCopyComponent", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingCompListSub(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchMappingCompListSub", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List searchMappingRelListSub(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("syncAmdbAttributeDAO.searchMappingRelListSub", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public void insertMappingCompListSub(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertMappingCompListSub", paramMap);
	}
	
	/**
	 * @param paramMap
	 * @return
	 */
	public void insertMappingRelListSub(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		insert("syncAmdbAttributeDAO.insertMappingRelListSub", paramMap);
	}
	
}
