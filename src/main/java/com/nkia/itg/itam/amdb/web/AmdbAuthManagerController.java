package com.nkia.itg.itam.amdb.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.itam.amdb.service.AmdbAuthManagerService;

@Controller
public class AmdbAuthManagerController {
	
	@Resource(name="amdbAuthManagerService")
	private AmdbAuthManagerService amdbAuthManagerService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@RequestMapping(value="/itg/itam/amdb/AmdbAuthManager.do")
	public String amdbManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		return "/itg/itam/amdb/amdbAuthManager.nvf";
	}

	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectUserGroupTree.do")
	public @ResponseBody ResultVO selectUserGroupTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List result = amdbAuthManagerService.selectUserGroupTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(result, up_node_id, false, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 
	 * [분류체계별 권한관리]
	 * 원장 그룹목록 treeGrid
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectEntityGrpTreeList.do")
	public @ResponseBody ResultVO selectEntityGrpTreeList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			
			List resultList = amdbAuthManagerService.selectEntityGrpTreeList(paramMap);
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * [분류체계별 권한관리]
	 * 원장 그룹목록 Grid
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectEntityGrpList.do")
	public @ResponseBody ResultVO selectEntityGrpList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			List resultList = amdbAuthManagerService.selectEntityGrpList(paramMap);
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * [분류체계별 권한관리] 엑셀다운로드
	 * 원장 그룹목록 Grid
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectEntityGrpExcelDown.do")
	public @ResponseBody ResultVO selectEntityGrpExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");
			
			List resultList = amdbAuthManagerService.selectEntityGrpList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectGrpAttrGrid.do")
	public @ResponseBody ResultVO selectGrpAttrGrid(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = amdbAuthManagerService.selectGrpAttrGrid(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectGrpAttrAuthSelGrid.do")
	public @ResponseBody ResultVO selectGrpAttrSelGrid(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = amdbAuthManagerService.selectGrpAttrAuthSelGrid(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/amdb/insertGrpAuth.do")
	public @ResponseBody ResultVO insertGrpAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			amdbAuthManagerService.insertGrpAuth(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 전체권한설정 저장
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertAllGrpAuth.do")
	public @ResponseBody ResultVO insertAllGrpAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			amdbAuthManagerService.insertAllGrpAuth(paramMap);
			resultVO.setSuccess(true);
		} catch (Exception e) {
			resultVO.setSuccess(false);
		}
		return resultVO;
	}
	
	/**
	 * 속성 부분권한설정 저장
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertPartGrpAuth.do")
	public @ResponseBody ResultVO insertPartGrpAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			amdbAuthManagerService.insertPartGrpAuth(paramMap);
			resultVO.setSuccess(true);
		} catch (Exception e) {
			resultVO.setSuccess(false);
		}
		return resultVO;
	}
	
	
	/**
	 * 권한설정 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/deleteGrpAuth.do")
	public @ResponseBody ResultVO deleteGrpAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			amdbAuthManagerService.deleteGrpAuth(paramMap);
			resultVO.setSuccess(true);
		} catch (Exception e) {
			resultVO.setSuccess(false);
		}
		return resultVO;
	}
	
	
}
