package com.nkia.itg.itam.amdb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AmdbServiceService {

	public List searchAmdbServiceAllTree(ModelMap paramMap) throws NkiaException;
	
	public List searchAmdbServiceAllTreeMap(ModelMap paramMap) throws NkiaException;

	public HashMap selectAmdbServiceBaseInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException;

	public List searchAmdbServiceGridList(ModelMap paramMap) throws NkiaException;
	
	public List searchAmdbServiceLowServiceList(ModelMap paramMap) throws NkiaException;

	public void updateAmdbServiceInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateAmdbServiceLowService(ModelMap paramMap) throws NkiaException;

	public void deleteAmdbServiceUser(ModelMap paramMap) throws NkiaException;

	public void insertAmdbServiceInfo(ModelMap paramMap) throws NkiaException;
	
	public void insertAmdbServiceUser(ModelMap paramMap) throws NkiaException;

	public void deleteAmdbServiceInfo(ModelMap paramMap) throws NkiaException;

	public HashMap selectServiceCust(ModelMap paramMap) throws NkiaException;
	
	public void insertServiceCust(ModelMap paramMap) throws NkiaException;
	
	public List searchServiceList(ModelMap paramMap) throws NkiaException;

	public int searchServiceListCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchServiceToRelConfList(ModelMap paramMap) throws NkiaException;
	
	public int searchServiceToRelConfListCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchServiceConfList(ModelMap paramMap) throws NkiaException;
	
	public int searchServiceConfListCount(ModelMap paramMap) throws NkiaException;	
	
	public void updateServiceOrder(HashMap oldOrderMap) throws NkiaException;
	
	public List searchServiceCodeList(Map paramMap) throws NkiaException;
	
	public List searchServiceCodeList(ModelMap paramMap) throws NkiaException;

	public int searchServiceCodeListCount(ModelMap paramMap) throws NkiaException;	
	
}
