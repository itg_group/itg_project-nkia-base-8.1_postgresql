/**
 * @version 1.0 2014. 6. 16.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jyjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
package com.nkia.itg.itam.amdb.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.amdb.dao.SyncAmdbAttributeDAO;
import com.nkia.itg.itam.amdb.service.SyncAmdbAttributeService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("syncAmdbAttributeService")
public class SyncAmdbAttributeServiceImpl implements SyncAmdbAttributeService{

	private static final Logger logger = LoggerFactory.getLogger(SyncAmdbAttributeServiceImpl.class);
	
	@Resource(name = "syncAmdbAttributeDAO")
	public SyncAmdbAttributeDAO syncAmdbAttributeDAO;
	
	@Override
	public List selectSyncClassEntityTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.selectSyncClassEntityTree(paramMap);
	}
	
	public List searchTargetClassList(ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		String u_type = (String)paramMap.get("u_type");
		String is_in_group_comp = (String)paramMap.get("is_in_group_comp");
		
		//hmsong cmdb 개선 작업 Start
		//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		String confAdminYn = new String();
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		paramMap.put("confAdminYn", confAdminYn);
		
		if("SYNC".equals(u_type)) {
			resultList = syncAmdbAttributeDAO.searchSyncTargetClassList(paramMap);
		} else if("COPY".equals(u_type)) {
			resultList = syncAmdbAttributeDAO.searchCopyTargetClassList(paramMap);
		} else if("COMP".equals(u_type)) {
			if("N".equals(is_in_group_comp)) {
				resultList = syncAmdbAttributeDAO.searchNotGroupCompTargetClassList(paramMap);
			} else {
				resultList = syncAmdbAttributeDAO.searchInGroupCompTargetClassList(paramMap);
			}
		} else {
			resultList = null;
		}
		return resultList;
	}
	
	public List selectDetailTargetTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.selectDetailTargetTree(paramMap);
	}
	
	public List searchGroupList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchGroupList(paramMap);
	}
	
	public List searchChildEntityList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchChildEntityList(paramMap);
	}
	
	public List searchTblJoinList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchTblJoinList(paramMap);
	}
	
	public List searchMappingTblList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchMappingTblList(paramMap);
	}
	
	public List searchMappingAttrList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchMappingAttrList(paramMap);
	}
	
	public List searchMappingBtnList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchMappingBtnList(paramMap);
	}
	
	public List searchMappingCompList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchMappingCompList(paramMap);
	}
	
	public List searchMappingRelList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return syncAmdbAttributeDAO.searchMappingRelList(paramMap);
	}
	
	public void updateCopyEntity(ModelMap paramMap) throws NkiaException {
		
		// 삭제 리스트 만들기 시작
		List del_id_list = new ArrayList();
		List del_root_id_list = new ArrayList();
		
		del_root_id_list = (List)paramMap.get("entity_id_list");
		
		for(int a=0; a<del_root_id_list.size(); a++) {
			
			HashMap delMap = new HashMap();
			List tempList = new ArrayList();
			
			String del_id = (String)del_root_id_list.get(a);
			
			delMap.put("base_entity_id", del_id);
			tempList = syncAmdbAttributeDAO.selectConnectByEntityIdList(delMap);
			
			for(int b=0; b<tempList.size(); b++) {
				del_id_list.add((String)tempList.get(b));
			}
		}
		// 삭제 리스트 만들기 끝
		
		
		// 로직 시작
		UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String user_id = userVO.getUser_id();
		
		List class_id_list = new ArrayList();
		class_id_list = (List)paramMap.get("entity_id_list");
		
		String tree_class_id = (String)paramMap.get("class_id");
		
		// 변경 대상 분류 수 만큼 반복
		for(int i=0; i<class_id_list.size(); i++) {
			
			HashMap keyMap = new HashMap();
			
			String class_id = (String)class_id_list.get(i);
			keyMap.put("fixed_entity_id", tree_class_id);
			
			List group_id_list = new ArrayList();
			group_id_list = syncAmdbAttributeDAO.selectChildEntityIdList(keyMap);
			
			// 선택 트리에서 가져온 Group ID 리스트 만큼 반복하면서 entity id를 새로 딴 후 insert 해줌
			for(int j=0; j<group_id_list.size(); j++) {
				
				HashMap entityMap = new HashMap();
				
				String new_id = syncAmdbAttributeDAO.generateEntityId();
				String curr_id = (String)group_id_list.get(j);
				
				entityMap.put("user_id", user_id);
				entityMap.put("entity_id", new_id);
				entityMap.put("up_entity_id", class_id);
				entityMap.put("fixed_entity_id", curr_id);
				
				// 그룹 등록
				syncAmdbAttributeDAO.insertSyncAmEntity(entityMap);
				
				List child_id_list = new ArrayList();
				child_id_list = syncAmdbAttributeDAO.selectChildEntityIdList(entityMap);
				
				// 하위 엔터티 리스트 수 만큼 반복
				for(int k=0; k<child_id_list.size(); k++) {
					
					HashMap childMap = new HashMap();
					
					String child_new_id = syncAmdbAttributeDAO.generateEntityId();
					String child_curr_id = (String)child_id_list.get(k);
					
					childMap.put("user_id", user_id);
					childMap.put("entity_id", child_new_id);
					childMap.put("up_entity_id", new_id);
					childMap.put("fixed_entity_id", child_curr_id);
					
					// 하위 엔터티 등록
					syncAmdbAttributeDAO.insertSyncAmEntity(childMap);
					// 하위 엔터티들의 매핑정보 등록
					syncAmdbAttributeDAO.insertSyncTblJoin(childMap);
					syncAmdbAttributeDAO.insertSyncTblMapping(childMap);
					syncAmdbAttributeDAO.insertSyncAttrMapping(childMap);
					syncAmdbAttributeDAO.insertSyncCompMapping(childMap);
					syncAmdbAttributeDAO.insertSyncRelMapping(childMap);
					syncAmdbAttributeDAO.insertSyncBtnMapping(childMap);
					
				} // k 끝
				
			} // j 끝
			
		} // i 끝
		
		// 기존정보 삭제 시작
		for(int f=0; f<del_id_list.size(); f++) {
			
			HashMap deleteMap = new HashMap();
			
			String del_entity_id = (String)del_id_list.get(f);
			deleteMap.put("entity_id", del_entity_id);
			
			syncAmdbAttributeDAO.deleteSyncAmEntity(deleteMap);
			syncAmdbAttributeDAO.deleteSyncTblJoin(deleteMap);
			syncAmdbAttributeDAO.deleteSyncTblMapping(deleteMap);
			syncAmdbAttributeDAO.deleteSyncAttrMapping(deleteMap);
			syncAmdbAttributeDAO.deleteSyncCompMapping(deleteMap);
			syncAmdbAttributeDAO.deleteSyncRelMapping(deleteMap);
			syncAmdbAttributeDAO.deleteSyncBtnMapping(deleteMap);
		}
		// 기존정보 삭제 끝
		
	}
	
	public void updateSyncMappingTable(ModelMap paramMap) throws NkiaException {
		
		List entity_id_list = new ArrayList();
		entity_id_list = (List)paramMap.get("entity_id_list");
		
		for(int i=0; i<entity_id_list.size(); i++) {
			
			String entity_id = (String)entity_id_list.get(i);
			paramMap.put("entity_id", entity_id);
			
			syncAmdbAttributeDAO.deleteSyncTblJoin(paramMap);
			syncAmdbAttributeDAO.insertSyncTblJoin(paramMap);
			
			syncAmdbAttributeDAO.deleteSyncTblMapping(paramMap);
			syncAmdbAttributeDAO.insertSyncTblMapping(paramMap);
			
			syncAmdbAttributeDAO.deleteSyncAttrMapping(paramMap);
			syncAmdbAttributeDAO.insertSyncAttrMapping(paramMap);
			
			syncAmdbAttributeDAO.deleteSyncCompMapping(paramMap);
			syncAmdbAttributeDAO.insertSyncCompMapping(paramMap);
			
			syncAmdbAttributeDAO.deleteSyncRelMapping(paramMap);
			syncAmdbAttributeDAO.insertSyncRelMapping(paramMap);
			
			syncAmdbAttributeDAO.deleteSyncBtnMapping(paramMap);
			syncAmdbAttributeDAO.insertSyncBtnMapping(paramMap);
		}
	}
	
	
	// 컴포넌트 복사
	public void insertCopyComponent(ModelMap paramMap) throws NkiaException {
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String user_id = userVO.getUser_id();
		
		List entity_id_list = new ArrayList();
		entity_id_list = (List)paramMap.get("entity_id_list");
		
		String fixed_entity_id = (String)paramMap.get("class_id");
		//String is_in_group_comp = (String)paramMap.get("is_in_group_comp");
		
		for(int i=0; i<entity_id_list.size(); i++) {
			
			String entity_id = "";
			String up_entity_id = "";
			
			entity_id = syncAmdbAttributeDAO.generateEntityId();
			up_entity_id = (String)entity_id_list.get(i);
			
			HashMap dataMap = new HashMap();
			
			dataMap.put("entity_id", entity_id);
			dataMap.put("up_entity_id", up_entity_id);
			dataMap.put("fixed_entity_id", fixed_entity_id);
			dataMap.put("user_id", user_id);
			
			// 컴포넌트 복사 후
			syncAmdbAttributeDAO.insertCopyComponent(dataMap);
			
			// 컴포넌트 관련 연계정보 복사
			List compMappingTblData = new ArrayList();
			List relMappingTblData = new ArrayList();
			
			compMappingTblData = syncAmdbAttributeDAO.searchMappingCompListSub(dataMap);
			relMappingTblData = syncAmdbAttributeDAO.searchMappingRelListSub(dataMap);
			
			// AM_ENTITY_COMP_MAPPING 테이블에 복사
			for(int x=0; x<compMappingTblData.size(); x++) {
				HashMap resultMap = null;
				resultMap = (HashMap)compMappingTblData.get(x);
				
				HashMap innerMap = new HashMap();
				innerMap.put("new_entity_id", entity_id);
				innerMap.put("install_code", resultMap.get("INSTALL_CODE"));
				innerMap.put("sort", resultMap.get("SORT"));
				
				syncAmdbAttributeDAO.insertMappingCompListSub(innerMap);
			}
			
			// AM_ENTITY_REL_MAPPING 테이블에 복사
			for(int y=0; y<relMappingTblData.size(); y++) {
				HashMap resultMap = null;
				resultMap = (HashMap)relMappingTblData.get(y);
				
				HashMap innerMap = new HashMap();
				innerMap.put("new_entity_id", entity_id);
				innerMap.put("rel_code", resultMap.get("REL_CODE"));
				innerMap.put("sort", resultMap.get("SORT"));
				
				syncAmdbAttributeDAO.insertMappingRelListSub(innerMap);
			}
		}
	}
	
	
}
