package com.nkia.itg.itam.amdb.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AmdbAuthManagerService {
	
	public List selectUserGroupTree(ModelMap paramMap) throws NkiaException;
	
	public List selectEntityGrpTreeList(ModelMap paramMap) throws NkiaException;

	public List selectGrpAttrGrid(ModelMap paramMap) throws NkiaException;
	
	public List selectGrpAttrAuthSelGrid(HashMap paramMap) throws NkiaException;
	
	public void insertGrpAuth(HashMap paramMap) throws NkiaException;

	public List selectEntityGrpList(HashMap paramMap) throws NkiaException;

	public void insertAllGrpAuth(HashMap paramMap) throws NkiaException;

	public void insertPartGrpAuth(ModelMap paramMap) throws NkiaException;

	public void deleteGrpAuth(ModelMap paramMap) throws NkiaException;

}
