/*
 * @(#)SyncAmdbController.java              2014. 6. 16.
 *
 * @jyjeong@nkia.co.kr
 * Copyright 2014 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.amdb.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.itam.amdb.service.SyncAmdbAttributeService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class SyncAmdbAttributeController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "syncAmdbAttributeService")
	private SyncAmdbAttributeService syncAmdbAttributeService;
	
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 분류체계 속성 동기화 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/goSyncAmdbAttribute.do")
	public String goSyncAmdbAttribute(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		//hmsong cmdb 개선 작업 Start
		//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		String confAdminYn = new String();
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		paramMap.put("confAdminYn", confAdminYn);
		//hmsong cmdb 개선 작업 End
		String forwarPage = "/itg/itam/amdb/syncAmdbAttribute.nvf";
		return forwarPage;
	}
	
	/**
	 * 분류체계 트리 및 엔터티 목록 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectSyncClassEntityTree.do")
	public @ResponseBody ResultVO selectSyncClassEntityTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			TreeVO treeVO = new TreeVO();
			//hmsong cmdb 개선 작업 Start
			//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
			String confAdminYn = new String();
			if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
				if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
					confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
				}
			}			
			paramMap.put("confAdminYn", confAdminYn);
			//hmsong cmdb 개선 작업 End
			List result 		= syncAmdbAttributeService.selectSyncClassEntityTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			
			treeVO = JsonUtil.changeTreeVOToJson(result, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 동기화 대상 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchTargetClassList.do")
	public @ResponseBody ResultVO searchTargetClassList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			//hmsong cmdb 개선 작업 Start
			//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
			String confAdminYn = new String();
			if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
				if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
					confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
				}
			}			
			paramMap.put("confAdminYn", confAdminYn);
			//hmsong cmdb 개선 작업 End
			resultList = syncAmdbAttributeService.searchTargetClassList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 상세 엔터티 목록 트리 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/selectDetailTargetTree.do")
	public @ResponseBody ResultVO selectDetailTargetTree(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = new TreeVO();
			List result = syncAmdbAttributeService.selectDetailTargetTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			
			treeVO = JsonUtil.changeTreeVOToJson(result, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 그룹 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchGroupList.do")
	public @ResponseBody ResultVO searchGroupList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchGroupList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 개체 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchChildEntityList.do")
	public @ResponseBody ResultVO searchChildEntityList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchChildEntityList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테이블 조인정보 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchTblJoinList.do")
	public @ResponseBody ResultVO searchTblJoinList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchTblJoinList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 매핑 테이블 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchMappingTblList.do")
	public @ResponseBody ResultVO searchMappingTblList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchMappingTblList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 매핑 속성 테이블 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchMappingAttrList.do")
	public @ResponseBody ResultVO searchMappingAttrList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchMappingAttrList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 매핑 버튼 테이블 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchMappingBtnList.do")
	public @ResponseBody ResultVO searchMappingBtnList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchMappingBtnList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 매핑 컴포넌트 테이블 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchMappingCompList.do")
	public @ResponseBody ResultVO searchMappingCompList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchMappingCompList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 매핑 연관장비 테이블 리스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/searchMappingRelList.do")
	public @ResponseBody ResultVO searchMappingRelList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = syncAmdbAttributeService.searchMappingRelList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 속성 복사, 매핑테이블 업데이트 (삭제 후 삽입)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/updateCopyEntity.do")
	public @ResponseBody ResultVO updateCopyEntity(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			syncAmdbAttributeService.updateCopyEntity(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 동기화 시, 매핑테이블 업데이트 (삭제 후 삽입)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/updateSyncMappingTable.do")
	public @ResponseBody ResultVO updateSyncMappingTable(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			syncAmdbAttributeService.updateSyncMappingTable(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 컴포넌트 복사, 엔터티 테이블 삽입
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/insertCopyComponent.do")
	public @ResponseBody ResultVO insertCopyComponent(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			syncAmdbAttributeService.insertCopyComponent(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
