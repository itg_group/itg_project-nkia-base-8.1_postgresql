package com.nkia.itg.itam.amdb.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.amdb.dao.AmdbAuthManagerDao;
import com.nkia.itg.itam.amdb.service.AmdbAuthManagerService;

@Service("amdbAuthManagerService")
public class AmdbAuthManagerServiceImpl implements AmdbAuthManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(AmdbAuthManagerServiceImpl.class);
	
	@Resource(name = "amdbAuthManagerDao")
	public AmdbAuthManagerDao amdbAuthManagerDao;
	

	@Override
	public List selectUserGroupTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAuthManagerDao.selectUserGroupTree(paramMap);
	}

	@Override 
	public List selectEntityGrpTreeList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAuthManagerDao.selectEntityGrpTreeList(paramMap);
	}
	
	@Override
	public List selectGrpAttrGrid(ModelMap paramMap) throws NkiaException {
		return amdbAuthManagerDao.selectGrpAttrGrid(paramMap);
	}
	
	
	public List selectGrpAttrAuthSelGrid(HashMap paramMap) throws NkiaException{
		return amdbAuthManagerDao.selectGrpAttrAuthSelGrid(paramMap);
	}


	@Override
	public void insertGrpAuth(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("grp_auth_list");
		

		//설정 count
		int cnt = amdbAuthManagerDao.selectGrpAuthCnt(paramMap);
		
		if(cnt>0){	// 설정이 있는경우 삭제후 재입력
			amdbAuthManagerDao.deleteGrpAuth(paramMap);
		}
		
		if(paramList != null){
			for(int i=0;i<paramList.size();i++){
				
				HashMap map = paramList.get(i);
				//그룹 id
				String 	entity_id 	= (String)map.get("id");
				//사용자 권한 id
				String 	user_grp_id = (String)map.get("user_grp_id");
				
				//읽기 권한 체크
				boolean	rChecked	= StringUtil.parseBoolean(map.get("param_003"));
				//수정 권한 체크
				boolean	wChecked	= StringUtil.parseBoolean(map.get("param_004"));
				
				boolean	rMChecked	= StringUtil.parseBoolean(map.get("param_009"));
				boolean	wMChecked	= StringUtil.parseBoolean(map.get("param_010"));
				
				//그룹유형  (component,manual)  현재는 두가지
				String 	entity_type = (String)map.get("param_005");
				
				//일기 및 수정 권한 상세 설정 (col_id::auth_type^) 형식으로 설정된 상세 정보가 들어갑니다.
				String 	rMDetailParam= (String)map.get("param_006");
				String 	wMDetailParam= (String)map.get("param_007");
				
				//컴포넌트의 경우 상세 컴포넌트 유형 (담당자, 파일 첨부, 등등)
				String  comp_code 	= (String)map.get("param_008");
				
				//입력을 위한 dataMap
				HashMap dataMap 	= new HashMap();
				//검색을 위한 맵
				HashMap searchMap 	= new HashMap();
				
				searchMap.put("entity_id"	, entity_id);
				searchMap.put("user_grp_id"	, user_grp_id);
				
				if(rMChecked){ // 읽기 권한 설정(부분설정)
					if("MANUAL".equals(entity_type)){
						if(rMDetailParam != null){//상세 설정이 있는경우
							String[]  paramArr = rMDetailParam.split("\\^");
							for(int j=0;j<paramArr.length;j++){
								String[]  paramIds = paramArr[j].split("::");
								dataMap.put("col_comp_id", paramIds[0]);
								dataMap.put("auth_type", paramIds[1]);
								dataMap.put("entity_id", entity_id);
								dataMap.put("user_grp_id", user_grp_id);
								amdbAuthManagerDao.insertGrpAuth(dataMap);
							}
						}else{//설정이 없는경우 (체크만 한경우)
							searchMap.put("display_type", "R");
							List<HashMap> rAuthList = amdbAuthManagerDao.selectGrpAttrGrid(searchMap);
							
							for(int j=0;j<rAuthList.size();j++){
								HashMap rAuthMap = rAuthList.get(j);
								String col_id = (String)rAuthMap.get("COL_ID");
								/*if(wMDetailParam != null){//수정 상세설정이 있는경우 
									String[] paramArr = wMDetailParam.split("\\^");
									int cCnt = 0;
									
									for(int k=0;k<paramArr.length;k++){
										String comp_col_id = paramArr[k].split("::")[0];
										if(comp_col_id.equals(col_id)){
											cCnt ++;
											break;
										}
									}
									if(cCnt == 0){
										dataMap.put("col_comp_id", col_id);
										dataMap.put("auth_type", "R");
										dataMap.put("entity_id", entity_id);
										dataMap.put("user_grp_id", user_grp_id);
										
										amdbAuthManagerDao.insertGrpAuth(dataMap);
									}
								}else{*///수정 상세설정이 없는 경우
									dataMap.put("col_comp_id", col_id);
									dataMap.put("auth_type", "R");
									dataMap.put("entity_id", entity_id);
									dataMap.put("user_grp_id", user_grp_id);
									
									amdbAuthManagerDao.insertGrpAuth(dataMap);
								//}
							}
							
							if(!wMChecked){	// 읽기 권한만 단일 지정한경우.. 수정 관한 속성도 읽기 속성으로 이동
								searchMap.put("display_type", "U");
								List<HashMap> wAuthList = amdbAuthManagerDao.selectGrpAttrGrid(searchMap);
								
								for(int j=0;j<wAuthList.size();j++){
									HashMap wAuthMap = wAuthList.get(j);
									String col_id = (String)wAuthMap.get("COL_ID");
									dataMap.put("col_comp_id", col_id);
									dataMap.put("auth_type", "R");
									dataMap.put("entity_id", entity_id);
									dataMap.put("user_grp_id", user_grp_id);
									
									amdbAuthManagerDao.insertGrpAuth(dataMap);
								}
							}
						}
					}
				}
				
				if(wMChecked){ // 쓰기 권한 설정
					if("MANUAL".equals(entity_type)){
						if(wMDetailParam != null){//상세 설정이 있는경우
							String[]  paramArr = wMDetailParam.split("\\^");
							for(int j=0;j<paramArr.length;j++){
								String[]  paramIds = paramArr[j].split("::");
								dataMap.put("col_comp_id", paramIds[0]);
								dataMap.put("auth_type", paramIds[1]);
								dataMap.put("entity_id", entity_id);
								dataMap.put("user_grp_id", user_grp_id);
								
								amdbAuthManagerDao.insertGrpAuth(dataMap);
							}
						}else{
							//설정이 없는경우 (체크만 한경우)
							searchMap.put("display_type", "U");
							List<HashMap> wAuthList = amdbAuthManagerDao.selectGrpAttrGrid(searchMap);
							
							for(int j=0;j<wAuthList.size();j++){
								HashMap wAuthMap = wAuthList.get(j);
								String col_id = (String)wAuthMap.get("COL_ID");
								/*if(rMDetailParam != null){ //쓰기 권한에 상세 설정이 있는경우 , (현재는 필요없음)
									String[] paramArr = rMDetailParam.split("\\^"); 
									int cCnt = 0;
									
									for(int k=0;k<paramArr.length;k++){
										String comp_col_id = paramArr[k].split("::")[0];
										if(comp_col_id.equals(col_id)){
											cCnt ++;
											break;
										}
									}
									if(cCnt == 0){
										dataMap.put("col_comp_id", col_id);
										dataMap.put("auth_type", "U");
										dataMap.put("entity_id", entity_id);
										dataMap.put("user_grp_id", user_grp_id);
										
										amdbAuthManagerDao.insertGrpAuth(dataMap);
									}
								}else{	*///읽기 상세 설정이 없는경우.
									dataMap.put("col_comp_id", col_id);
									dataMap.put("auth_type", "U");
									dataMap.put("entity_id", entity_id);
									dataMap.put("user_grp_id", user_grp_id);
									amdbAuthManagerDao.insertGrpAuth(dataMap);
								//}
							}
						}
					}
				}
				
				//일괄 속성 지정
				if(rChecked){//(읽기)
					if("MANUAL".equals(entity_type)){
						dataMap.put("col_comp_id", "ALL");
						dataMap.put("auth_type", "R");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}else if("COMPONENT".equals(entity_type)){
						dataMap.put("col_comp_id", comp_code);
						dataMap.put("auth_type", "R");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}
				}
				
				if(wChecked){//(쓰기)
					if("MANUAL".equals(entity_type)){
						dataMap.put("col_comp_id", "ALL");
						dataMap.put("auth_type", "U");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}else if("COMPONENT".equals(entity_type)){
						dataMap.put("col_comp_id", comp_code);
						dataMap.put("auth_type", "U");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}
				}
			}
		}
	}
	
	/**
	 * 분류체계별 권한
	 * 전체 권한 설정 저장
	 *  : 전체 읽기/전체 수정
	 */
	@Override
	public void insertAllGrpAuth(HashMap paramMap) throws NkiaException {
		
		List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("grp_auth_list");
		
		if(paramList != null){
			
			// 기존 권한 삭제
			amdbAuthManagerDao.deleteGrpAuth(paramMap);

			for(int i=0;i<paramList.size();i++){
				
				HashMap map = paramList.get(i);
				//그룹 id
				String 	entity_id 	= (String)map.get("ENTITY_ID");
				//사용자 권한 id
				String 	user_grp_id = (String)map.get("USER_GRP_ID");
				
				//읽기 권한 체크
				boolean	rChecked	= StringUtil.parseBoolean(map.get("ALL_READ"));
				//수정 권한 체크
				boolean	wChecked	= StringUtil.parseBoolean(map.get("ALL_UPD"));
				
				//그룹유형  (component,manual)  현재는 두가지
				String 	entity_type = (String)map.get("ENTITY_TYPE");
				
				//컴포넌트의 경우 상세 컴포넌트 유형 (담당자, 파일 첨부, 등등)
				String  comp_code 	= (String)map.get("COMP_CODE");
				
				//입력을 위한 dataMap
				HashMap dataMap 	= new HashMap();
				//검색을 위한 맵
				HashMap searchMap 	= new HashMap();
				
				searchMap.put("entity_id"	, entity_id);
				searchMap.put("user_grp_id"	, user_grp_id);
				
				//일괄 속성 지정
				if(rChecked){//(읽기)
					if("MANUAL".equals(entity_type)){
						dataMap.put("col_comp_id", "ALL");
						dataMap.put("auth_type", "R");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}else if("COMPONENT".equals(entity_type)){
						dataMap.put("col_comp_id", comp_code);
						dataMap.put("auth_type", "R");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}
				}
				
				if(wChecked){//(쓰기)
					if("MANUAL".equals(entity_type)){
						dataMap.put("col_comp_id", "ALL");
						dataMap.put("auth_type", "U");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}else if("COMPONENT".equals(entity_type)){
						dataMap.put("col_comp_id", comp_code);
						dataMap.put("auth_type", "U");
						dataMap.put("entity_id", entity_id);
						dataMap.put("user_grp_id", user_grp_id);
						amdbAuthManagerDao.insertGrpAuth(dataMap);
					}
				}
			}
		}
	}
	

	/**
	 * 분류체계별 권한관리
	 * 원장 그룹 권한목록 조회
	 */
	@Override
	public List selectEntityGrpList(HashMap paramMap) throws NkiaException {
		return amdbAuthManagerDao.selectEntityGrpList(paramMap);
	}

	/**
	 * 분류체계별 권한
	 * 상세속성 권한 설정 저장
	 *  : 부분 읽기/부분 수정
	 */
	@Override
	public void insertPartGrpAuth(ModelMap paramMap) throws NkiaException {
		
		String entity_id 	= (String)paramMap.get("entity_id");
		String user_grp_id = (String)paramMap.get("user_grp_id");
		List updAuthAttrList= (List) paramMap.get("updAuthAttrList");		// 수정 속성 목록
		List viewAuthAttrList= (List) paramMap.get("viewAuthAttrList");		// 읽기 속성 목록
		
		// 기존 권한 삭제
		amdbAuthManagerDao.deleteGrpAuth(paramMap);
		
		// 읽기속성 저장
		if(viewAuthAttrList != null && !viewAuthAttrList.isEmpty()){
			for(int j=0;j<viewAuthAttrList.size();j++){
				HashMap dataMap = new HashMap();
				HashMap attrMap = (HashMap) viewAuthAttrList.get(j);
				dataMap.put("col_comp_id", attrMap.get("COL_ID").toString());
				dataMap.put("auth_type", "R");
				dataMap.put("entity_id", entity_id);
				dataMap.put("user_grp_id", user_grp_id);
				amdbAuthManagerDao.insertGrpAuth(dataMap);
			}
		}
		
		// 수정속성 저장
		if(updAuthAttrList != null && !updAuthAttrList.isEmpty()){
			for(int j=0;j<updAuthAttrList.size();j++){
				HashMap dataMap = new HashMap();
				HashMap attrMap = (HashMap) updAuthAttrList.get(j);
				dataMap.put("col_comp_id", attrMap.get("COL_ID").toString());
				dataMap.put("auth_type", "U");
				dataMap.put("entity_id", entity_id);
				dataMap.put("user_grp_id", user_grp_id);
				amdbAuthManagerDao.insertGrpAuth(dataMap);
			}
		}
		
		
		
	}

	@Override
	public void deleteGrpAuth(ModelMap paramMap) throws NkiaException {
		
		List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("grp_auth_list");
		
		if(paramList != null){
			// 기존 권한 삭제
			amdbAuthManagerDao.deleteGrpAuth(paramMap);
			
		}
	}

}
