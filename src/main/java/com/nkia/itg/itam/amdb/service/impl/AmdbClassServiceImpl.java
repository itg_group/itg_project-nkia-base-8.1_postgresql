package com.nkia.itg.itam.amdb.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.itam.amdb.dao.AmdbClassDAO;
import com.nkia.itg.itam.amdb.service.AmdbClassService;

@Service("amdbClassService")
public class AmdbClassServiceImpl implements AmdbClassService{
	private static final Logger logger = LoggerFactory.getLogger(AmdbClassServiceImpl.class);
	
	@Resource(name = "amdbClassDao")
	public AmdbClassDAO amdbClassDao;

	public List searchAmClassAllTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbClassDao.searchAmClassAllTree(paramMap);
	}

	public List selectAmClassBaseInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbClassDao.selectAmClassBaseInfo(paramMap);
	}

	public int searchAmClassGridListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbClassDao.searchAmClassGridListCount(paramMap);
	}

	public List searchAmClassGridList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbClassDao.searchAmClassGridList(paramMap);
	}

	public void updateAmClassInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbClassDao.updateAmClassInfo(paramMap);
//		amdbClassDao.updateAssetServiceLife(paramMap); // 수정시 내용년수 변경
	}

	public void updateAmClassOrder(ModelMap paramMap) throws NkiaException {
		ArrayList gridList = (ArrayList) paramMap.get("gridList");
		if(gridList != null){
			for(int i = 0; i < gridList.size(); i++){
				Map gridMap = (Map) gridList.get(i);
				Map param = WebUtil.lowerCaseMapKey(gridMap);
				
				amdbClassDao.updateAmClassOrder(param);
			}
		}
	}

	public void insertAmClassInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbClassDao.insertAmClassInfo(paramMap);
	}

	public void deleteAmClassInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbClassDao.deleteAmClassInfo(paramMap);
	}

	/* (non-Javadoc)
	 * @see com.nkia.itg.itam.amdb.service.AmdbClassService#searchAmClassCheckTree(org.springframework.ui.ModelMap)
	 */
	@Override
	public List searchAmClassCheckTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbClassDao.searchAmClassCheckTree(paramMap);
	}

	@Override
	public int selectAssetCount(ModelMap paramMap) throws NkiaException {
		return amdbClassDao.selectAssetCount(paramMap);
	}

}
