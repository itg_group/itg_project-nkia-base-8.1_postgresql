package com.nkia.itg.itam.amdb.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("amdbAuthManagerDao")
public class AmdbAuthManagerDao extends EgovComAbstractDAO{
	
	public List selectUserGroupTree(HashMap paramMap) throws NkiaException {
		return list("amdbAuthManagerDAO.selectUserGroupTree",null);
	}
	
	public List selectEntityGrpTreeList(HashMap paramMap) throws NkiaException {
		return list("amdbAuthManagerDAO.selectEntityGrpTreeList", paramMap);
	}
	
	public List selectGrpAttrGrid(HashMap paramMap) throws NkiaException {
		return list("amdbAuthManagerDAO.selectGrpAttrGrid", paramMap);
	}
	
	public List selectGrpAttrAuthSelGrid(HashMap paramMap) throws NkiaException {
		return list("amdbAuthManagerDAO.selectGrpAttrAuthSelGrid", paramMap);
	}
	
	public int selectGrpAuthCnt(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("amdbAuthManagerDAO.selectGrpAuthCnt", paramMap);
	}
	
	public void deleteGrpAuth(HashMap paramMap) throws NkiaException {
		delete("amdbAuthManagerDAO.deleteGrpAuth", paramMap);
	}
	
	public void insertGrpAuth(HashMap paramMap) throws NkiaException {
		insert("amdbAuthManagerDAO.insertGrpAuth", paramMap);
	}

	public List selectEntityGrpList(HashMap paramMap) throws NkiaException {
		return list("amdbAuthManagerDAO.selectEntityGrpList", paramMap);
	}

	public void insertGrpAuth2(HashMap dataMap) throws NkiaException {
		List aa = list("amdbAuthManagerDAO.insertGrpAuth2", dataMap);
	}

}
