/**
 * 
 */
package com.nkia.itg.itam.amdb.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 12.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface AmdbTableService {

	/**
	 * @param paramMap
	 * @return
	 */
	public int selectAmTblGridCount(ModelMap paramMap) throws NkiaException;
	
	/**
	 * @param paramMap
	 * @return
	 */
	public List selectAmTblGridList(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 * @return
	 */
	public List selectAmAttrGridList(ModelMap paramMap) throws NkiaException;

	/**
	 * @param paramMap
	 * @return
	 */
	public List selectAmAttrSelGridList(ModelMap paramMap) throws NkiaException;

	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertGenTableInfo(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteGenTblInfo(ModelMap paramMap) throws NkiaException;
	
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateGenTableInfo(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateGenTableOnlyInfo(ModelMap paramMap)throws NkiaException;
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void createAmTable(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectGenTableNm(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 테이블 정보
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectGenTableInfo(HashMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @param attrList
	 * @throws NkiaException
	 */
	public void createTableScript(ModelMap paramMap,List<HashMap> attrList)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @param attrList
	 * @throws NkiaException
	 */
	public void createCommentScript(ModelMap paramMap,List<HashMap> attrList)throws NkiaException;

	/**
	 * 
	 * @param paramMap
	 * @param seg_type
	 * @param spc_prifix
	 * @throws NkiaException
	 */
	public void getTableSpaceNm(ModelMap paramMap,String seg_type,String spc_prifix)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateGenYn(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @throws NkiaException
	 */
	public void convertToLobSource()throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAmRelationTblList(HashMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectScriptInfo(HashMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void createBackupTable(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param tblNm
	 * @throws NkiaException
	 */
	public void dropAmTable(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param tblNm
	 * @throws NkiaException
	 */
	public void migrationTable(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void creteConstsIndex(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public List selectAttributeCombo(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void alterAmTable(ModelMap paramMap)throws NkiaException;
	
	
	/**
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void dropConstsIndex(ModelMap paramMap)throws NkiaException;
	
	//hmsong cmdb 개선 작업 Start
	/**
	 * 테이블 매핑 시 필수 컬럼의 정보를 검토해주는 쿼리.
	 * @param paramMap
	 * @throws NkiaException
	 */
	public List selectNotNullTblCode(ModelMap paramMap)throws NkiaException;
	//hmsong cmdb 개선 작업 End
}
