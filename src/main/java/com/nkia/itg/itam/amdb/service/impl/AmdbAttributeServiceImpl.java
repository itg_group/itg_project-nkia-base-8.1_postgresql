/**
 * 
 */
package com.nkia.itg.itam.amdb.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.amdb.dao.AmdbAttributeDAO;
import com.nkia.itg.itam.amdb.service.AmdbAttributeService;

/**
 * @version 1.0 2013. 4. 1.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */

@Service("amdbAttributeService")
public class AmdbAttributeServiceImpl implements AmdbAttributeService{

	private static final Logger logger = LoggerFactory.getLogger(AmdbAttributeServiceImpl.class);
	
	@Resource(name = "amdbAttributeDao")
	public AmdbAttributeDAO amdbAttributeDao;
	
	/* (non-Javadoc)
	 * @see com.nkia.itg.itam.amdb.service.AmdbAttributeService#goAmAttributeGridListCount(org.springframework.ui.ModelMap)
	 */
	public int searchAmAttributeGridListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAttributeDao.searchAmAttributeGridListCount(paramMap);
	}

	/* (non-Javadoc)
	 * @see com.nkia.itg.itam.amdb.service.AmdbAttributeService#goAmAttributeGridList(org.springframework.ui.ModelMap)
	 */
	public List searchAmAttributeGridList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAttributeDao.searchAmAttributeGridList(paramMap);
	}

	/* (non-Javadoc)
	 * @see com.nkia.itg.itam.amdb.service.AmdbAttributeService#goAmAttributeInfoUpdate(org.springframework.ui.ModelMap)
	 */
	public void updateAmAttributeInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbAttributeDao.updateAmAttributeInfo(paramMap);
		
	}

	/* (non-Javadoc)
	 * @see com.nkia.itg.itam.amdb.service.AmdbAttributeService#goAmAttributeInfoDelete(org.springframework.ui.ModelMap)
	 */
	public void deleteAmAttributeInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbAttributeDao.deleteAmAttributeInfo(paramMap);
	}

	/* (non-Javadoc)
	 * @see com.nkia.itg.itam.amdb.service.AmdbAttributeService#goAmAttributeInfoInsert(org.springframework.ui.ModelMap)
	 */
	public void insertAmAttributeInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbAttributeDao.insertAmAttributeInfo(paramMap);
	}

	@Override
	public int selectAttributeFieldNm(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAttributeDao.selectAttributeFieldNm(paramMap);
		
	}

	@Override
	public HashMap selectAttributeInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAttributeDao.selectAttributeInfo(paramMap);
	}

	@Override
	public void insertUserGroupInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		
		String col_id = (String)paramMap.get("col_id");
		String display_type = (String)paramMap.get("display_type");
		
		List<HashMap> paramList = (ArrayList<HashMap>)paramMap.get("userGrpList");
		
		int cnt = amdbAttributeDao.selectUserGroupCnt(paramMap);
		
		if(cnt>0){
			amdbAttributeDao.deleteUserGroupInfo(paramMap);
		}
		
		for(int i=0;i<paramList.size();i++){
			HashMap map = paramList.get(i);
			map.put("user_grp_id", (String)map.get("id"));
			map.put("col_id", col_id);
			map.put("auth_type", display_type);
			amdbAttributeDao.insertUserGroupInfo(map);
		}
	}

	@Override
	public int selectUserGroupCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAttributeDao.selectUserGroupCnt(paramMap);
	}

	@Override
	public void deleteUserGroupInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		amdbAttributeDao.deleteUserGroupInfo(paramMap);
	}

	@Override
	public List selectPopupList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return amdbAttributeDao.selectPopupList(paramMap);
	}

	@Override
	public List selectGenTableList(ModelMap paramMap) throws NkiaException {
		return amdbAttributeDao.selectGenTableList(paramMap);
	}
	
	@Override
	public int selectGenTableCnt(ModelMap paramMap) throws NkiaException {
		return amdbAttributeDao.selectGenTableCnt(paramMap);
	}
	
}
