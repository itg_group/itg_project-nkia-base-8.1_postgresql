/*
 * @(#)AutoServiceImpl.java              2013. 11. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.auto.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.auto.dao.AutoDAO;
import com.nkia.itg.itam.auto.service.AutoService;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;
import com.nkia.itg.itam.opms.service.OpmsDetailService;
import com.nkia.itg.itam.opms.util.OpmsUtil;

@Service("autoService")
public class AutoServiceImpl implements AutoService{
	
	@Resource(name="autoDAO")
	private AutoDAO autoDAO;
	
	@Resource(name="opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;
	
	@Resource(name="opmsDetailService")
	private OpmsDetailService opmsDetailService;
	
	@Resource(name="opmsUtil")
	private OpmsUtil opmsUtil;
	
	/**
	 * 
	 * TODO 불일치 리스트를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
/*	@Override
	public List selectDifConfList(HashMap paramMap)throws NkiaException{
		return autoDAO.selectDifConfList(paramMap);
	}

	*//**
	 * 
	 * TODO 불일치 리스트 갯수를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public int selectDifConfCnt(HashMap paramMap)throws NkiaException{
		return autoDAO.selectDifConfCnt(paramMap);
	}

	*//**
	 * 
	 * TODO 권한이있는지 여부와 설치 정보의 경우 item리스트를 가져온다.
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 *//*
	@Override
	public void selectDifAuthList(HashMap paramMap) throws NkiaException {
		
		// TODO Auto-generated method stub
		List<HashMap> compItem = autoDAO.selectInstallItemList(paramMap);
		List<HashMap> compItemComp = new ArrayList<HashMap>();  
		for(int i=0;i<compItem.size();i++){
			HashMap installMap = compItem.get(i);
			String install_code = StringUtil.parseString(installMap.get("INSTALL_CODE"));
			//해당 컬럼 리스트를 가져온다
			List<HashMap> installColList = opmsDetailDAO.selectOpmsInstallColumnList(install_code);
			//자산쪽그리드 헤더 정보
			installMap.put("formItems"+install_code+"AM", opmsUtil.getGridResourceByList(installColList,"COL_ID","COL_NM"));
			//ems쪽 그리드 헤더정보
			installMap.put("formItems"+install_code+"EMS", opmsUtil.getGridResourceByList(installColList,"EMS_COL_ID","COL_NM"));
			//비교 대상 컬럼을 지정한다.
			installMap.put("compareItems"+install_code, opmsUtil.getInstallCompareData(installColList));

			compItemComp.add(installMap);
		}
		
		//array형식으로 변환
		JSONArray itemArray = JSONArray.fromObject(compItemComp);
		
		//스크립트에서 array로 바로 사용하기 위해 변환한다.여기서만 예외 replace all 한번더한다.
		String installItem = StringUtil.replaceAll(opmsUtil.replaceAllquotes(opmsUtil.replaceValueQuotes(itemArray.toString())),"'',","',");
		
		//권한이 있는 EMS 연계속성중 불일치 여부가 있는지 가져온다.	
		int auth_cnt = autoDAO.selectManualAuthCnt(paramMap);
		
		paramMap.put("tot_auth_cnt", auth_cnt+compItem.size());
		paramMap.put("auth_cnt", auth_cnt);
		paramMap.put("installItem", installItem);
	}

	
	*//**
	 * 
	 * TODO 속성 비교 리스트 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectAutoInfoCompareList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return autoDAO.selectAutoInfoCompareList(paramMap);
	}
	
	
	*//**
	 * 
	 * TODO 속성 비교 리스트 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectAutoAmInsInfoList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		
		String insType = StringUtil.parseString(paramMap.get("ins_type"));
		String listType = StringUtil.parseString(paramMap.get("list_type"));
		HashMap intstallTblInfo = opmsDetailDAO.selectOpmsInstallInfo(insType);
		
		if("EMS".equalsIgnoreCase(listType)){
			//EMS 테이블에서 정보를 가져온다.
			intstallTblInfo.put("EMS_SHOW_YN", "Y");
		}else{
			intstallTblInfo.put("EMS_SHOW_YN", "N");
		}
		//설치 정보마다 셋팅된 컬럼 정보를 가져온다.
		List<HashMap> intstallColList = opmsDetailDAO.selectOpmsInstallColumnList(insType);
		
		opmsUtil.paramSync(paramMap, intstallTblInfo);
		//select 쿼리를 만든다.
		String query = opmsUtil.createAutoInstallQuery(intstallTblInfo,intstallColList);
		
		return opmsDetailDAO.excuteQueryList(query);
	}

	*//**
	 * 
	 * TODO 설치 정보 수정
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 *//*
	@Override
	public void modifyInstallInfo(HashMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		
		//자산 이력관리 테이블의 seq id를 생성한다.
		String chgSeq = opmsDetailService.selectAssetChgHisSeq(paramMap);
		paramMap.put("chg_his_seq", chgSeq);
		
		opmsDetailService.insertPrevDataHis(paramMap);
		
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		String asset_id = StringUtil.parseString(paramMap.get("asset_id"));
		
		List<String> submitIdList = (List<String>)paramMap.get("submitIdArray");
		
		if(submitIdList != null){
			
			for(int i=0; i<submitIdList.size(); i++){
				
				String gridsId = submitIdList.get(i);
				HashMap gridsDataMap = (HashMap)paramMap.get(gridsId);
				String gridsType = StringUtil.parseString(gridsDataMap.get("gridsType"));
				List<HashMap> gridsData = (List<HashMap>)gridsDataMap.get("gridsData");
				
				if(gridsData != null){
					if("ATTR".equalsIgnoreCase(gridsType)){
						for(int j=0;j<gridsData.size();j++){
							HashMap gridsDetailData = gridsData.get(j);
							String gen_table_nm = StringUtil.parseString(gridsDetailData.get("GEN_TABLE_NM"));
							//리퀘스트에서 들어온 데이터 동기화..
							opmsUtil.paramSync(paramMap, gridsDetailData);
							gridsDetailData.put("gen_table_nm", gen_table_nm);
							//수정이력 비교해서 수정된것만  Y로 변경
							updateAutoChgHisModYn(gridsDetailData);
							//asset_id,conf_id 를 가져오기 위한 parameter
							gridsDetailData.put("sel_type", "UKEYID");
							List<HashMap> confAssetList = opmsDetailDAO.selectTypeColumn(gridsDetailData);
							String query = opmsUtil.createAutoInstallUpdateQuery(gridsDetailData,confAssetList);
							opmsDetailDAO.excuteCudQuery(query);
						}
					}else{
						//설치 기본정보를 가져온다.
						HashMap intstallTblInfo = opmsDetailDAO.selectOpmsInstallInfo(gridsType);
						String  tableNm = StringUtil.parseString(intstallTblInfo.get("TABLE_NM"));
						String emsShowYn =  StringUtil.parseString(intstallTblInfo.get("EMS_SHOW_YN"));
						//설치 정보마다 셋팅된 컬럼 정보를 가져온다.
						List<HashMap> intstallColList = opmsDetailDAO.selectOpmsInstallColumnList(gridsType);
						
						intstallTblInfo.put("ins_type", gridsType);
						intstallTblInfo.put("conf_id", conf_id);
						List<HashMap> prevDataList = opmsDetailService.selectOpmsInstallInfoList(intstallTblInfo);
						
						if(emsShowYn.equalsIgnoreCase("N")){
							for(int j=0;j<prevDataList.size();j++){
								
								HashMap preDataMap = (HashMap)prevDataList.get(j);
								HashMap hisDataMap = new HashMap();
								hisDataMap.put("conf_id"			, conf_id);
								hisDataMap.put("asset_id"			, asset_id);
								hisDataMap.put("chg_his_seq"		, chgSeq);
								hisDataMap.put("seq"				, j+1);
							
								for(int k=0;k<intstallColList.size();k++){
									
									HashMap installColMap = intstallColList.get(k);
									
									String colId = StringUtil.parseString(installColMap.get("COL_ID"));
									String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
									String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
									String curVal = "";
									String preVal = "";
									
									if(gridsData != null){
										if(gridsData.size() == prevDataList.size()){
											HashMap currentDataMap = gridsData.get(j);
											preVal = StringUtil.parseString(preDataMap.get(colId));
											curVal = StringUtil.parseString(currentDataMap.get(colId));
											if(preVal.equals(curVal)){
												hisDataMap.put("chg_yn"			, "N");
											}else{
												hisDataMap.put("chg_yn"			, "Y");
											}
										}else{
											hisDataMap.put("chg_yn"			, "Y");
										}
										hisDataMap.put("chg_table"		,tableNm);
										hisDataMap.put("chg_column_nm"	, colNm);
										hisDataMap.put("chg_column_id"		, colId);
									}else{
										hisDataMap.put("chg_yn"			, "Y");
										hisDataMap.put("chg_table"		,tableNm);
										hisDataMap.put("chg_column_nm"	, colNm);
										hisDataMap.put("chg_column_id"		, colId);
									}
									opmsDetailDAO.updateInsAmAssetChgHisModYn(hisDataMap);
								}
							}
						}
						
						//파라메터 대소문자 관련해서 문제가 있습니다...
						intstallTblInfo.put("CONF_ID", conf_id);
						opmsDetailService.deleteInstallInfo(intstallTblInfo);
						for(int j=0;j<gridsData.size();j++){
							HashMap curInstallMap = gridsData.get(j);
							curInstallMap.put("CONF_ID", conf_id);
							curInstallMap.put("TABLE_NM", tableNm);
							//등록한다..
							opmsDetailService.insertInstallInfo(curInstallMap,intstallColList);
						}
					}
				}
			}
		}
		//AM_AUTO Table 데이터 갱신을 위해 변경된 구성ID의 값을 제거 한다.
		autoDAO.deleteAmAuto(paramMap);
		
		//AM_AUTO Table 데이터 갱신을 위해 변경된 구성ID의 값을 새로 등록한다.
		autoDAO.insertAmAuto(paramMap);
	}
	
	*//**
	 * 
	 * TODO form Data 형식 수정여부 update
	 * 
	 * @param gridsDetailData
	 * @throws NkiaException
	 *//*
	public void updateAutoChgHisModYn(HashMap gridsDetailData)throws NkiaException{
		
		HashMap hisDataMap = new HashMap();
		
		String conf_id = StringUtil.parseString(gridsDetailData.get("conf_id"));
		String asset_id = StringUtil.parseString(gridsDetailData.get("asset_id"));
		String chg_his_seq = StringUtil.parseString(gridsDetailData.get("chg_his_seq"));
		String chg_table = StringUtil.parseString(gridsDetailData.get("gen_table_nm"));
		String chg_column_id = StringUtil.parseString(gridsDetailData.get("COL_ID"));
		String curValue = StringUtil.parseString(gridsDetailData.get("EMS_VALUE"));
		
		
		hisDataMap.put("conf_id"			, conf_id);
		hisDataMap.put("asset_id"			, asset_id);
		hisDataMap.put("chg_his_seq"	, chg_his_seq);
		hisDataMap.put("seq"				, "1");
		hisDataMap.put("chg_table"		, chg_table);
		hisDataMap.put("chg_column_id", chg_column_id);
		hisDataMap.put("curValue", curValue);
		
		opmsDetailDAO.updateAmAssetChgHisModYn(hisDataMap);
	}*/
}

