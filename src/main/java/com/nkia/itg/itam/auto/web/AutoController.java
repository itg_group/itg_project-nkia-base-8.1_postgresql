/*
 * @(#)AutoController.java              2013. 11. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.auto.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.auto.service.AutoService;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AutoController {
	
	@Resource(name = "autoService")
	private AutoService autoService;
	
	@Resource(name = "commonCodeService")
	private CommonCodeService commonCodeService;
	
	/*
	
	*//**
	 * 
	 * TODO 구성점검 탭화면
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/itam/auto/autoMainTab.do")
	public String amdbAttribute(ModelMap paramMap) throws NkiaException {
		return "/itg/itam/auto/autoMainTab.nvf";
	}
	
	*//**
	 * 
	 * TODO 구성점검 리스트 화면
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/itam/auto/autoMainList.do")
	public String autoMainList(ModelMap paramMap) throws NkiaException {
		return "/itg/itam/auto/autoMainList.nvf";
	}
	
	*//**
	 * 
	 * TODO 불일치 자원 리스트를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/itam/auto/selectDifConfList.do")
	public @ResponseBody ResultVO selectDifConfList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		
		try{
			GridVO gridVO = new GridVO();
			
			UserVO userVO			= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String user_id			= userVO.getUser_id();
			paramMap.put("user_id"		, user_id);
			
			PagingUtil.getFristEndNum(paramMap);
			//전체 검색인경우 key 설정
			commonCodeService.getSearchList(paramMap,"searchKey","searchValue","CIPOP_SEARCH_TYPE");
			//불일치 리스트 건수
			int totalCount 	= autoService.selectDifConfCnt(paramMap);
			//불일치 리스트
			List resultList	= autoService.selectDifConfList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	*//**
	 * 
	 * TODO 불일치 자원 리스트를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/itam/auto/seletDiffInfo.do")
	public String seletDiffInfo(HttpServletRequest request,ModelMap paramMap) throws NkiaException {
		UserVO userVO			= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String user_id			= userVO.getUser_id();
		String ems_id 		= StringUtil.parseString(request.getParameter("ems_id"));
		String conf_id 			= StringUtil.parseString(request.getParameter("conf_id"));
		String asset_id 		= StringUtil.parseString(request.getParameter("asset_id"));
		String class_id 		= StringUtil.parseString(request.getParameter("class_id"));
		String class_type 		= StringUtil.parseString(request.getParameter("class_type"));
		String logical_yn 		= StringUtil.parseString(request.getParameter("logical_yn"));
		String entity_class_id 	= StringUtil.parseString(request.getParameter("entity_class_id"));
		String tangible_asset_yn= StringUtil.parseString(request.getParameter("tangible_asset_yn"));
		
		paramMap.put("user_id"		, user_id);
		paramMap.put("ems_id"		, ems_id);
		paramMap.put("conf_id"		, conf_id);
		paramMap.put("asset_id"		, asset_id);
		paramMap.put("class_id"		, class_id);
		paramMap.put("class_type"	, class_type);
		paramMap.put("logical_yn"	, logical_yn);
		paramMap.put("entity_id"	, entity_class_id);
		paramMap.put("entity_class_id"	, entity_class_id);
		paramMap.put("tangible_asset_yn", tangible_asset_yn);
		
		autoService.selectDifAuthList(paramMap);
		
		return "/itg/itam/auto/autoMainDetail.nvf";
	}
	
	*//**
	 * 
	 * TODO 속성 비교 리스트 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/itam/auto/selectAutoInfoCompareList.do")
	public @ResponseBody ResultVO selectAutoInfoCompareList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			
			UserVO userVO			= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String user_id			= userVO.getUser_id();
			paramMap.put("user_id"		, user_id);
			//불일치 리스트
			List resultList	= autoService.selectAutoInfoCompareList(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	*//**
	 * 
	 * TODO 속성 비교 리스트 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/itam/auto/selectAutoAmInsInfoList.do")
	public @ResponseBody ResultVO selectAutoAmInsInfoList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			UserVO userVO			= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String user_id			= userVO.getUser_id();
			
			paramMap.put("user_id"		, user_id);
			//불일치 리스트
			List resultList	= autoService.selectAutoAmInsInfoList(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	*//**
	 * 
	 * TODO 설치 정보 수정 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@RequestMapping(value="/itg/itam/auto/modifyInstallInfo.do")
	public @ResponseBody ResultVO modifyInstallInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO			= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String user_id			= userVO.getUser_id();
			paramMap.put("user_id"		, user_id);
			
			autoService.modifyInstallInfo(paramMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}*/
}
