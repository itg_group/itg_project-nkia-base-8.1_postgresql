/*
 * @(#)AutoDAO.java              2013. 11. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.auto.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("autoDAO")
public class AutoDAO extends EgovComAbstractDAO{
	
	/**
	 * 
	 * TODO 불일치 리스트를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectDifConfList(HashMap paramMap)throws NkiaException{
		return list("autoDAO.selectDifConfList", paramMap);
	}

	/**
	 * 
	 * TODO 불일치 리스트 갯수를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectDifConfCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("autoDAO.selectDifConfCnt", paramMap);
	}
	
	/**
	 * 
	 * TODO 권한이 있는 EMS 연계속성중 불일치 여부가 있는지 가져온다.
	 * 		
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectManualAuthCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("autoDAO.selectManualAuthCnt", paramMap);
	}
	
	/**
	 * 
	 * TODO 설치 정보 item 을 가져온다. .수정권한이 있을때만.. item을 가져노다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectInstallItemList(HashMap paramMap)throws NkiaException{
		return list("autoDAO.selectInstallItemList", paramMap);
	}
	
	/**
	 * 
	 * TODO 속성 비교 리스트 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAutoInfoCompareList(HashMap paramMap)throws NkiaException{
		return list("autoDAO.selectAutoInfoCompareList", paramMap);
	}
	
	/**
	 * 
	 * TODO AM_AUTO Table 변경 구성 갱신을 위한 데이터 Delete
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteAmAuto(HashMap paramMap)throws NkiaException{
		this.delete("autoDAO.deleteAmAuto", paramMap);
	}
	
	/**
	 * 
	 * TODO AM_AUTO Table 변경 구성 갱신을 위한 데이터 Insert
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAmAuto(HashMap paramMap)throws NkiaException{
		this.insert("autoDAO.insertAmAuto", paramMap);
	}
	
}
