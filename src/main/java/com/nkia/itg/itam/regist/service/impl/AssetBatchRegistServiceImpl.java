package com.nkia.itg.itam.regist.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.automation.core.AssetAutomationComponents;
import com.nkia.itg.itam.automation.dao.AssetAutomationDAO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.itam.automation.ui.WebixUIHandler;
import com.nkia.itg.itam.introduction.service.TempAssetService;
import com.nkia.itg.itam.regist.service.AssetBatchRegistService;

@Service("assetBatchRegistService")
public class AssetBatchRegistServiceImpl implements AssetBatchRegistService{

	@Resource(name = "webixUIHandler")
	private WebixUIHandler webixUIHandler;
	
	@Resource(name = "assetAutomationComponents")
	private AssetAutomationComponents assetAutomationComponents;
	
	@Resource(name = "assetAutomationDAO")
	private AssetAutomationDAO assetAutomationDAO;
	
	@Resource(name = "assetAutomationService")
	private AssetAutomationService assetAutomationService;
	
	@Resource(name = "tempAssetService")
	private TempAssetService tempAssetService;
	
	@Resource(name = "jsonUtil")
	private JsonUtil jsonUtil;  
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	/**
	 * 정보수집서 데이터 등록 및 수정
	 * @param dataMap
	 * @throws NkiaException
	 */
	@Override
	public void insertOpmsInfoCollection(HashMap dataMap)throws NkiaException{
		String user_id 				= StringUtil.parseString(dataMap.get("user_id"));
		String class_id 			= StringUtil.parseString(dataMap.get("class_id"));
		String class_type 			= StringUtil.parseString(dataMap.get("class_type"));
		String entity_class_id 		= StringUtil.parseString(dataMap.get("entity_class_id"));
		String tangible_asset_yn 	= StringUtil.parseString(dataMap.get("tangible_asset_yn"));
		String file_id	 			= StringUtil.parseString(dataMap.get("atch_file_id"));
		String regist_type	 		= StringUtil.parseString(dataMap.get("regist_type"));
		String physi_asset_id	 	= StringUtil.parseString(dataMap.get("physi_asset_id"));
		String physi_conf_id	 	= StringUtil.parseString(dataMap.get("physi_conf_id"));
		String title	 			= StringUtil.parseString(dataMap.get("info_title"));
		Integer infoCnt	 			= StringUtil.parseInteger(dataMap.get("info_cnt"));
		String project_no	 		= StringUtil.parseString(dataMap.get("project_no"));
		String view_type	 		= StringUtil.parseString(dataMap.get("view_type"));
		String durable_year	 		= StringUtil.parseString(dataMap.get("durable_year"));
		List formItemIds			= (List)dataMap.get("formItemIds");
		
		String assetTempId = "";
		String confTempId = "";
		if (view_type.equals("infmodifiy")) {
			assetTempId = (String) dataMap.get("asset_id");
			confTempId = (String) dataMap.get("conf_id");
		}
		
		assetAutomationService.selectClassInfo(dataMap);	
		
		String temp_seq 	 = StringUtil.parseString(dataMap.get("temp_seq"));		
		
		//각그룹에 해당되는 테이블 리스트 목록을 가져온다.
		//List<HashMap> entityTblList = assetAutomationDAO.selectEntityManualTable(dataMap);
		
		if(view_type.equals("info")){			
			String tempSeq = tempAssetService.createTempSeq();
			temp_seq = tempSeq;
			HashMap insSeqMap = new HashMap();
			insSeqMap.put("ins_user_id"			, user_id);
			insSeqMap.put("class_id"		, class_id);
			insSeqMap.put("temp_seq"		, tempSeq);
			insSeqMap.put("title"			, title);
			insSeqMap.put("project_no"		, project_no);
			insSeqMap.put("class_type"		, class_type);
			insSeqMap.put("temp_div"		, "INFO");
			
			tempAssetService.insertAmTempSeq(insSeqMap);
			
		}else if(view_type.equals("infmodifiy")){
			HashMap dataParam = new HashMap();
			dataParam.put("ASSET_TEMP_ID", assetTempId);
			dataParam.put("CONF_TEMP_ID", confTempId);
			dataParam.put("ins_type", "UPDATE");
			/** DATA 제거 **/
			tempAssetService.deleteAmTempData(dataParam);
			/** MAPPING DATA 상태값 변경 REGIST -> REGIST_UPD **/
			tempAssetService.updateAmTempMapping(dataParam);
		}

		if(infoCnt.equals(0)){
			infoCnt =  1;
		}
		
		// 일괄등록 요청상태
		boolean isCnfirmMenu = tempAssetService.checkCnfirmMenu();
		String tempAssetStatus;
		if(isCnfirmMenu){
			tempAssetStatus = "REGIST";
		}else{
			tempAssetStatus = "CONFIRM";
		}
		
		// 정보수집 갯수를 입력한 만큼 insert 함
		for(int l=0; l <infoCnt; l++){
			
			
			if(view_type.equals("info")){
				// 신규 자산id 생성
				confTempId = tempAssetService.createConfTempId();
				assetTempId = tempAssetService.createAssetTempId();

				HashMap mappingData = new HashMap();
				mappingData.put("temp_seq"			, temp_seq);
				mappingData.put("asset_temp_id"		, assetTempId);
				mappingData.put("conf_temp_id"		, confTempId);
				mappingData.put("status"			, tempAssetStatus);
				
				tempAssetService.insertAmTempMapping(mappingData);
			}
			
			for(int i = 0; i < formItemIds.size(); i++){
				HashMap getDataMap 	= (HashMap)formItemIds.get(i);
				
				String entity_id	= StringUtil.parseString(getDataMap.get("entity_id"));
				String comp_id		= StringUtil.parseString(getDataMap.get("comp_id"));
				String type 		= StringUtil.parseString(getDataMap.get("type"));
				String entity_type	= StringUtil.parseString(getDataMap.get("entity_type"));				
				
				//각그룹에 해당되는 테이블 리스트 목록을 가져온다.
				List<HashMap> tblList =	assetAutomationDAO.selectTblList(getDataMap);
				tblList.add(dataMap);
				
				HashMap insDataMap = new HashMap();
				insDataMap.put("asset_temp_id"		, assetTempId);
				insDataMap.put("conf_temp_id"		, confTempId);
				
				for(int j=0;j<tblList.size();j++){
					HashMap tblDataMap = tblList.get(j);
					String gen_table_nm = StringUtil.parseString(tblDataMap.get("GEN_TABLE_NM"));
					dataMap.put("GEN_TABLE_NM", gen_table_nm);
					
					List<HashMap> colList = assetAutomationDAO.selectTabAllColumnsList(dataMap);
					
					for(int k=0;k<colList.size();k++){
						HashMap colDataMap = colList.get(k);
						
						String mkCompNm		= StringUtil.parseString(colDataMap.get("MK_COMP_NM"));
						String colId		= StringUtil.parseString(colDataMap.get("COL_ID"));
						String colNm		= StringUtil.parseString(colDataMap.get("COL_NAME"));
						String genTableNm	= StringUtil.parseString(colDataMap.get("GEN_TABLE_NM"));
						String htmlType 	= StringUtil.parseString(colDataMap.get("HTML_TYPE"));
						
						insDataMap.put("ins_table_nm"		, genTableNm);
						insDataMap.put("ins_column_id"		, colId);
						insDataMap.put("ins_column_nm"		, colNm);
						insDataMap.put("user_id"			, user_id);
						insDataMap.put("view_type"			, view_type);
						
						if("F".equalsIgnoreCase(type)){
							HashMap currentData = (HashMap)dataMap.get(comp_id);
							String value = StringUtil.parseString(currentData.get(mkCompNm));
							
							if("DATE".equalsIgnoreCase(htmlType)){
								value = StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.replaceAll(value, "-", "")," ",""),":","");
							}else if("COMBO".equalsIgnoreCase("HTML_TYPE") 
									|| "CHECK".equalsIgnoreCase("HTML_TYPE") 
									|| "RADIO".equalsIgnoreCase("HTML_TYPE")){
								insDataMap.put("code_yn"	, "Y");
							}
							
							if(!"".equals(value)){
								insDataMap.put("value", value);
								int dataCnt	 = assetAutomationDAO.selectDataInsertYn(insDataMap);
								if(dataCnt == 0){
									tempAssetService.insertAmTempData(insDataMap);
								}
							}
						}else if("SL".equalsIgnoreCase(type)){
							if("MANAGER".equalsIgnoreCase(entity_type)){
								List<HashMap> currentData	= (List<HashMap>)dataMap.get(entity_id);
								
								if(currentData != null){
									for(int h=0;h<currentData.size();h++){
										HashMap curMngMap = currentData.get(h);
										curMngMap.put("asset_temp_id", assetTempId);
										curMngMap.put("conf_temp_id", confTempId);
										curMngMap.put("cust_id", curMngMap.get("CUST_ID"));
										curMngMap.put("oper_user_id", curMngMap.get("USER_ID"));
										curMngMap.put("main_yn_val", curMngMap.get("MAIN_YN_VAL"));
										curMngMap.put("oper_user_type", curMngMap.get("OPER_USER_TYPE"));
										curMngMap.put("upd_user_id", user_id);
										int dataCnt	 = assetAutomationDAO.selectOpmsInfoOperInsertYn(curMngMap);
										if(dataCnt == 0){
											curMngMap.put("seq", dataCnt+1);
											assetAutomationDAO.insertOpmsInfoOper(curMngMap);												
										}
									}
								}									
							}else if("REL_SERVICE".equalsIgnoreCase(entity_type)){
								List<HashMap> currentData	= (List<HashMap>)dataMap.get(entity_id);

								if(currentData != null){
									for(int h=0;h<currentData.size();h++){
										HashMap curMngMap = currentData.get(h);
										insDataMap.put("service_id", curMngMap.get("SERVICE_ID"));
										insDataMap.put("cust_id", curMngMap.get("CUST_ID"));
										int dataCnt	 = assetAutomationDAO.selectOpmsInfoServiceInsertYn(insDataMap);
										if(dataCnt == 0){
											assetAutomationDAO.insertOpmsInfoService(insDataMap);												
										}
									}
								}
							}
						}else if("TL".equalsIgnoreCase(type)){
							if("REL_INFRA".equalsIgnoreCase(entity_type)){
								List<HashMap> currentDataList = (List<HashMap>)dataMap.get(comp_id);

								if(currentDataList != null){
									//현제는 구현사항이 없음...
									if("LOGSV".equalsIgnoreCase(comp_id)){
									//현제는 구현상황이 없음..
									} else if("HA".equalsIgnoreCase(comp_id)){
										
									} else if("VM".equalsIgnoreCase(comp_id)){
										
									} else if("RS".equalsIgnoreCase(comp_id)) {
										for(int h=0;h<currentDataList.size();h++){
											HashMap curMngMap = currentDataList.get(h);
											insDataMap.put("service_id", curMngMap.get("SERVICE_ID"));
											insDataMap.put("cust_id", curMngMap.get("CUST_ID"));
											int dataCnt	 = assetAutomationDAO.selectOpmsInfoServiceInsertYn(insDataMap);
											if(dataCnt == 0){
												assetAutomationDAO.insertOpmsInfoService(insDataMap);												
											}
										}
									}else{
										for(int h=0;h<currentDataList.size();h++){
											HashMap curRelConfMap = currentDataList.get(h);
											curRelConfMap.put("CLASS_TYPE", comp_id);
											curRelConfMap.put("MAIN_CONF_ID", confTempId);
											curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
											curRelConfMap.put("UPD_USER_ID", user_id);
											int dataCnt	 = assetAutomationDAO.selectOpmsInfoRelConfInsertYn(curRelConfMap);
											if(dataCnt == 0){												
												assetAutomationDAO.insertOpmsInfoRelConf(curRelConfMap);
											}
										}
									}
								}
							}else if("INSTALL".equalsIgnoreCase(entity_type)){
								
								dataMap.put("ins_type", comp_id);
								HashMap installInfo = assetAutomationDAO.selectOpmsInstallInfo(comp_id);
								List<HashMap> installColList = assetAutomationDAO.selectOpmsInstallColumnList(comp_id);
								List<HashMap> currentDataList = (List<HashMap>)dataMap.get(comp_id);
								
								String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
								String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
								String tableNm	= StringUtil.parseString(installInfo.get("INFO_TABLE_NM"));						
					
								if(emsShowYn.equalsIgnoreCase("N")){
									//파라메터 대소문자 관련해서 문제가 있습니다...
									installInfo.put("TABLE_NM", tableNm);
									installInfo.put("CONF_ID", confTempId);
									installInfo.put("COL_NM", "CONF_TEMP_ID");
									assetAutomationService.deleteInstallInfo(installInfo);
									
									if(currentDataList != null){
										for(int h=0;h<currentDataList.size();h++){
											HashMap curInstallMap = currentDataList.get(h);
											curInstallMap.put("CONF_ID", confTempId);
											curInstallMap.put("TABLE_NM", tableNm);
											int dataCnt	 = assetAutomationDAO.selectOpmsInfoInstallInsertYn(curInstallMap);
											if(dataCnt == 0){													
												assetAutomationService.insertInstallInfo(curInstallMap,installColList);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			// 분류체계 테이블 임의값 등록(분류체계 테이블 미등록 방지)
			tempAssetService.mustInsertClassTable(class_type, assetTempId, confTempId, user_id);
		}
	}
}
