package com.nkia.itg.itam.regist.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.service.ExcelFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.introduction.service.TempAssetService;
import com.nkia.itg.itam.regist.dao.AssetExcelRegistDAO;
import com.nkia.itg.itam.regist.service.AssetExcelRegistService;
import com.nkia.itg.itam.statistics.dao.AssetStatisticsDAO;
import com.sun.org.apache.xml.internal.resolver.helpers.Debug;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("assetExcelRegistService")
public class AssetExcelRegistServiceImpl implements AssetExcelRegistService{
	
	@Resource(name="assetExcelRegistDAO")
	private AssetExcelRegistDAO assetExcelRegistDAO;

	@Resource(name = "assetStatisticsDAO")
	public AssetStatisticsDAO assetStatisticsDAO;
	
	@Resource(name = "tempAssetService")
	private TempAssetService tempAssetService;
	
	@Resource(name = "excelFileService")
	private ExcelFileService excelFileService;
	
	/**
	 * 엑셀파일의 sheet를 읽는다.
	 * @param storeFileName
	 * @param fileStoreCours
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	private XSSFSheet getExcelFileSeet(String storeFileName, String fileStoreCours, int sheetNum) throws NkiaException, IOException{
		
		FileInputStream fis = null;
		XSSFSheet sheet = null;
		
		try {
			String storeFilePath = fileStoreCours + storeFileName;
			File file = new File(storeFilePath);
			fis = new FileInputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(sheetNum);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e){
					throw new NkiaException(e);
				}
			}
		}
		
		return sheet;
	}
	
	
	/**
	 * 엑셀 템플릿 파일을 parsing하여 반환 (.xlsx 용)
	 * @throws IOException 
	 */
	@Override
	public HashMap parsingAssetExcelForXlsx(
				HashMap paramMap,
				String storeFileName,
				String orignlFileName, 
				String fileStoreCours,
				String fileExtsn, 
				String fileContentType) throws NkiaException, IOException{
		
		HashMap resultMap = new HashMap();
		
		// 엑셀파일의 0번째 sheet 를 가져온다
		XSSFSheet sheet = getExcelFileSeet(storeFileName, fileStoreCours, 0);
		
		// 헤더 정보 
		HashMap headerInfoMap = getHeaderInfoMapForXlsx(paramMap, sheet);
		
		if(headerInfoMap.size() > 0) {
			
			List<HashMap> excelHeaderList = (List) headerInfoMap.get("excelHeaderList");	// 헤더 정보
			HashMap gridResourceMap = (HashMap) headerInfoMap.get("gridResourceMap");	// grid resource 정보
			int lastCellNum = (Integer) headerInfoMap.get("lastCellNum");					// 마지막 cell 번호
			
			// 데이터 정보
			List<HashMap> dataList = getExcelDataMapForXlsx(paramMap, sheet, excelHeaderList, lastCellNum);
			
			resultMap.put("resultList", dataList);
			resultMap.put("gridResourceMap", gridResourceMap);
		}
			
		return resultMap;
	}
	
	/**
	 * 엑셀 템플릿 파일을 parsing하여 반환 (.xls 용)
	 * @throws IOException 
	 */
	@Override
	public HashMap parsingAssetExcelForXls(
				HashMap paramMap,
				String storeFileName,
				String orignlFileName, 
				String fileStoreCours,
				String fileExtsn, 
				String fileContentType) throws NkiaException, IOException{
		
		HashMap resultMap = new HashMap();
		
		// 엑셀파일의 0번째 sheet 를 가져온다
		HSSFSheet sheet = getExcelFileSeetForXls(storeFileName, fileStoreCours, 0);
		
		// 헤더 정보 
		HashMap headerInfoMap = getHeaderInfoMapForXls(paramMap, sheet);
		
		if(headerInfoMap.size() > 0) {
			
			List<HashMap> excelHeaderList = (List) headerInfoMap.get("excelHeaderList");	// 헤더 정보
			HashMap gridResourceMap = (HashMap) headerInfoMap.get("gridResourceMap");	// grid resource 정보
			int lastCellNum = (Integer) headerInfoMap.get("lastCellNum");					// 마지막 cell 번호
			
			// 데이터 정보
			List<HashMap> dataList = getExcelDataMapForXls(paramMap, sheet, excelHeaderList, lastCellNum);
			
			resultMap.put("resultList", dataList);
			resultMap.put("gridResourceMap", gridResourceMap);
		}
			
		return resultMap;
	}
	
	private List<HashMap> getExcelDataMapForXls(HashMap paramMap, HSSFSheet sheet, List<HashMap> excelHeaderList, int lastCellNum) {
	
		int HEADER_DATA_START_ROW_NUMBER = 5;	// 엑셀 자산 데이터 시작 행
//		int lastRownum = sheet.getPhysicalNumberOfRows();		// 데이터 마지막 row
		int assetCount = (Integer) paramMap.get("last_row_num");
		int lastRownum = assetCount ; // 데이터 마지막 row
		
		// 요청 분류체계의 공통코드 목록 (코드값 조회용)
		List<HashMap> commCodeList = assetExcelRegistDAO.selectCommCodeListForClassType(paramMap);
		List<HashMap> popupCodeList = selectPopupCodeList();
		
		DataFormatter formatter = new DataFormatter();
		List<HashMap> dataList = new ArrayList();
		
		// row 반복문
		for(int i = (HEADER_DATA_START_ROW_NUMBER -1) ; i < lastRownum; i++) {
		
			HashMap dataMap = new HashMap();
			HSSFRow dataRow = sheet.getRow(i);
			if(dataRow != null) {
				
				// cell 반복문
				for(int c = 0; c < lastCellNum; c++) {
					
					// 엑셀 헤더
					HashMap headerMap = excelHeaderList.get(c);
					HSSFCell dataCell = dataRow.getCell(c);
					// 값
					String dataCellValue = StringUtil.parseString(formatter.formatCellValue(dataCell)).trim();
					
					if(c == 0 ) {
						// 행번호 컬럼 값 추가(엑셀에 없으므로 따로 추가해준다)
						dataMap.put("EMPTY.ROW_NUM", (i+1));
					}
					
					// dataMap
					getExcelDataMapCommon(dataMap, headerMap, commCodeList, popupCodeList, dataCellValue);
				}
			}
			
			
			if(dataMap != null && dataMap.size() > 0) {
				dataList.add(dataMap);
			}
			
		}
		return dataList;
	}


	private HSSFSheet getExcelFileSeetForXls(String storeFileName, String fileStoreCours, int sheetNum) throws NkiaException, IOException {
		
		FileInputStream fis = null;
		HSSFSheet sheet = null;
		
		try {
			String storeFilePath = fileStoreCours + storeFileName;
			File file = new File(storeFilePath);
			fis = new FileInputStream(file);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			sheet = workbook.getSheetAt(sheetNum);
		} finally {
			if (fis != null){
				try {
					fis.close();
				} catch (Exception e) {
					throw new NkiaException(e);
				}
			}
		}
		
		return sheet;
	}
	
	/**
	 * 엑셀의 헤더정보 파싱 ( .xls용 )
	 * @param paramMap
	 * @param sheet
	 * @return
	 */
	private HashMap getHeaderInfoMapForXls(HashMap paramMap, HSSFSheet sheet) {
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		// 헤더 정보를 만든다.
		int HEADER_ID_START_ROW_NUMBER = 3;		// 엑셀 헤더ID 시작 행
		int HEADER_NM_START_ROW_NUMBER = 4;		// 엑셀 헤더명 시작 행 
		HSSFRow headerIdRow = sheet.getRow(HEADER_ID_START_ROW_NUMBER - 1);
		HSSFRow headerNmRow = sheet.getRow(HEADER_NM_START_ROW_NUMBER - 1);
		
		if(headerIdRow != null) {
			
			int lastCellNum = headerIdRow.getLastCellNum();
			String classType = (String) paramMap.get("class_type");
		
			
			/**  엑셀의 헤더Cell을 List 객체에 따로 담아준다. */
			List headerList = new ArrayList();
			for(int i = 0; i < lastCellNum; i++) {
			
				HSSFCell headerIdCell = headerIdRow.getCell(i);
				HSSFCell headerNmCell = headerNmRow.getCell(i);
				
				String headerId = headerIdCell.getStringCellValue().trim();	// 형태 : [ 테이블명.컬럼ID ]
				String headerNm = headerNmCell.getStringCellValue().trim();
				
				HashMap headerMap = new HashMap();
				headerMap.put("headerId", headerId);
				headerMap.put("headerNm", headerNm);
				
				headerList.add(headerMap);
			}

			resultMap = getHeaderInfoMapCommon(headerList, classType);
			resultMap.put("lastCellNum", lastCellNum);
		}
		return resultMap;
	}
	
	
	/**
	 * 데이터를 파싱하여 List 로 return
	 * @param paramMap
	 * @param sheet
	 * @param excelHeaderList
	 * @param lastCellNum
	 * @return
	 * @throws NkiaException 
	 */
	private List getExcelDataMapForXlsx(HashMap paramMap, XSSFSheet sheet, List<HashMap> excelHeaderList, int lastCellNum) throws NkiaException {
		
		int HEADER_DATA_START_ROW_NUMBER = 5;	// 엑셀 자산 데이터 시작 행
//		int lastRownum = sheet.getPhysicalNumberOfRows();		// 데이터 마지막 row
		int assetCount = (Integer) paramMap.get("last_row_num"); // 엑셀 마지막 행번호
		int lastRownum = assetCount ; // 데이터 마지막 row
		
		// 요청 분류체계의 공통코드 목록 (코드값 조회용)
		List<HashMap> commCodeList = assetExcelRegistDAO.selectCommCodeListForClassType(paramMap);
		// List<HashMap> popupCodeList = selectPopupCodeList();
		List<HashMap> popupCodeList = selectPopupCodeListByPopupMng(); // 엑셀템플릿 자동 생성(By Poi)
		
		DataFormatter formatter = new DataFormatter();
		List<HashMap> dataList = new ArrayList();
		
		// row 반복문
		for(int i = (HEADER_DATA_START_ROW_NUMBER -1) ; i < lastRownum; i++) {
		
			HashMap dataMap = new HashMap();
			XSSFRow dataRow = sheet.getRow(i);
			if(dataRow != null) {
				
				// cell 반복문
				for(int c = 0; c < lastCellNum; c++) {
					
					// 엑셀 헤더
					HashMap headerMap = excelHeaderList.get(c);
					XSSFCell dataCell = dataRow.getCell(c);
					// 값
					String dataCellValue = StringUtil.parseString(formatter.formatCellValue(dataCell)).trim();
					
					if(c == 0 ) {
						// 행번호 컬럼 값 추가(엑셀에 없으므로 따로 추가해준다)
						dataMap.put("EMPTY.ROW_NUM", (i+1));
					}
					
					// dataMap
					getExcelDataMapCommon(dataMap, headerMap, commCodeList, popupCodeList, dataCellValue);
				}
			}
			
			
			if(dataMap != null && dataMap.size() > 0) {
				dataList.add(dataMap);
			}
			
		}
		return dataList;
	}

	
	/**
	 * 데이터를 파싱 공통 로직
	 * @param dataMap
	 * @param headerMap
	 * @param commCodeList
	 * @param popupCodeList
	 * @param dataCellValue
	 */
	private void getExcelDataMapCommon(
				HashMap dataMap, 
				HashMap headerMap,
				List<HashMap> commCodeList,
				List<HashMap> popupCodeList,
				String dataCellValue) {
		
		// 엑셀 헤더
		String column_id = (String) headerMap.get("column_id");
		String column_html_type = (String) headerMap.get("column_html_type");
		String column_col_type = (String) headerMap.get("column_col_type");
		
		// 숫자형의 경우 값에서 콤마 문자를 제거
		if("NUMBER".equals(column_col_type)){
			dataCellValue = dataCellValue.replace(",", "");
		}else if("DATE".equals(column_col_type)){
			dataCellValue = getChgDateFormat(dataCellValue, "yyyy-MM-dd");
		}
		
		dataMap.put((String)headerMap.get("tableCols"), dataCellValue);
		
		// 코드 속성의 코드값 조회
		if(column_html_type != null) {
			if( "SELECT".equals(column_html_type) || "RADIO".equals(column_html_type) ){	//공통코드
				String sysCode = (String) headerMap.get("sysCode");
				String CodeNm = "";
				
				// 공통코드가 URL요청일 경우
				if(StringUtils.isEmpty(sysCode)) {
					sysCode = column_id;
				}
				
				for(HashMap codeData : commCodeList){
					String codeGrpId = (String) codeData.get("CODE_GRP_ID");
					String CodeId = (String) codeData.get("CODE_ID");
					if(codeGrpId.equals(sysCode) && CodeId.equals(dataCellValue)){
						CodeNm = (String) codeData.get("CODE_NM");
						break;
					}
					// 엑셀템플릿 자동 생성(By Poi) -- 코드명으로 비교하고, 코드ID 값과 코드명 값을 넣어줌.
					else if ( codeGrpId.equals(sysCode) && StringUtil.parseString(codeData.get("CODE_NM")).equals(dataCellValue) ){
						CodeNm = StringUtil.parseString(codeData.get("CODE_NM"));
						dataMap.put(StringUtil.parseString(headerMap.get("tableCols")), StringUtil.parseString(codeData.get("CODE_ID")));
						break;
					}
				}
				dataMap.put("EMPTY." + column_id + "_NM", CodeNm);
				
			}else if(column_html_type.matches("POPUP.*")) {									// 팝업 속성
				String popupCode = (String) headerMap.get("popupCode");
				List<HashMap> popupValueList = new ArrayList(); 
				for(HashMap popupData : popupCodeList) {
					if(popupCode.equals(popupData.get("popupCode"))) {
						popupValueList = (List) popupData.get("resultList");
						break;
					}
				}
				if(!popupValueList.isEmpty()) {
					String CodeNm = "";
					for(HashMap codeData : popupValueList){
						String CodeId = (String) codeData.get("CODE_ID");
						if(dataCellValue.equals(CodeId)) {
							CodeNm = (String) codeData.get("CODE_NM");
							break;
						}
						// 엑셀템플릿 자동 생성(By Poi) -- 코드명으로 비교하고, 코드ID 값과 코드명 값을 넣어줌.
						else if ( dataCellValue.equals(codeData.get("CODE_ID_NM")) == true ){
							CodeNm = StringUtil.parseString(codeData.get("CODE_ID_NM"));
							dataMap.put(StringUtil.parseString(headerMap.get("tableCols")), StringUtil.parseString(codeData.get("CODE_ID")));
							break;
						}
					}
					dataMap.put("EMPTY." + column_id + "_NM", CodeNm);
				}
			}
		}
	}
	
	
	/**
	 * header 정보를 담아준다
	 * excelHeaderList : 엑셀 헤더 정보
	 * gridResourceMap : 그리드 resource 정보 
	 * @param paramMap
	 * @param sheet
	 * @return
	 */
	private HashMap<String, Object> getHeaderInfoMapForXlsx(HashMap paramMap, XSSFSheet sheet){
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		// 헤더 정보를 만든다.
		int HEADER_ID_START_ROW_NUMBER = 3;		// 엑셀 헤더ID 시작 행
		int HEADER_NM_START_ROW_NUMBER = 4;		// 엑셀 헤더명 시작 행 
		XSSFRow headerIdRow = sheet.getRow(HEADER_ID_START_ROW_NUMBER - 1);
		XSSFRow headerNmRow = sheet.getRow(HEADER_NM_START_ROW_NUMBER - 1);
		
		if(headerIdRow != null) {
			
			int lastCellNum = headerIdRow.getLastCellNum();
			String classType = (String) paramMap.get("class_type");
		
			
			/**  엑셀의 헤더Cell을 List 객체에 따로 담아준다. */
			List headerList = new ArrayList();
			for(int i = 0; i < lastCellNum; i++) {
			
				XSSFCell headerIdCell = headerIdRow.getCell(i);
				XSSFCell headerNmCell = headerNmRow.getCell(i);
				
				String headerId = headerIdCell.getStringCellValue().trim();	// 형태 : [ 테이블명.컬럼ID ]
				String headerNm = headerNmCell.getStringCellValue().trim();
				
				HashMap headerMap = new HashMap();
				headerMap.put("headerId", headerId);
				headerMap.put("headerNm", headerNm);
				
				headerList.add(headerMap);
			}

			resultMap = getHeaderInfoMapCommon(headerList, classType);
			resultMap.put("lastCellNum", lastCellNum);
			
		}
		return resultMap;
	}
	
	
	/**
	 * Grid 헤더 정보 추출
	 * @param headerList
	 * @param classType
	 * @return
	 */
	private HashMap getHeaderInfoMapCommon(List<HashMap> headerList, String classType){
		HashMap resultMap = new HashMap();
		List<HashMap<String, String>> excelHeaderList = new ArrayList<HashMap<String, String>>();
		HashMap gridResourceMap = new HashMap();
		
		// 화면 그리드 reousrce 정의
		StringBuffer gridHeader = new StringBuffer("EMPTY.ROW_NUM");
		StringBuffer gridText = new StringBuffer("행번호");	
		StringBuffer gridWidth = new StringBuffer("70");
		StringBuffer gridFlex = new StringBuffer("na");
		StringBuffer gridSortable = new StringBuffer("na");
		StringBuffer gridAlign = new StringBuffer("center");
		StringBuffer gridXtype = new StringBuffer("na");
		
	
		// 요청 분류체계의 속성 정보 목록 조회
		List<HashMap> allAttrInfoList = assetExcelRegistDAO.selectArrtInfoList(classType);
					
		for(HashMap headerMap : headerList) {
			String headerId = (String) headerMap.get("headerId");
			String headerNm = (String) headerMap.get("headerNm");

			String[] tableCols = headerId.split("\\.");
			String colId = tableCols[1];
			String colHthmlType = "";
			String colType = "";
			
			/**
			 *  엑셀 헤더 목록 정의
			 */
			HashMap<String, String> excelHeaderMap = new HashMap<String, String>();
			excelHeaderMap.put("tableCols", headerId);
			excelHeaderMap.put("table", tableCols[0]);
			excelHeaderMap.put("column_id", tableCols[1]);
			excelHeaderMap.put("column_value", headerNm);
			
			for(HashMap attrInfoMap : allAttrInfoList) {
				String attrId = (String) attrInfoMap.get("COL_ID");
				
				// 속성의 col_type, html_type 정보를 담는다
				if(colId.equals(attrId)) {
					colHthmlType = attrInfoMap.get("HTML_TYPE").toString();
					colType = attrInfoMap.get("COL_TYPE").toString();
					excelHeaderMap.put("column_html_type", colHthmlType);
					excelHeaderMap.put("column_col_type", colType);
					excelHeaderMap.put("sysCode", (String) attrInfoMap.get("SYS_CODE"));
					excelHeaderMap.put("popupCode", (String) attrInfoMap.get("POPUP_CODE"));
					
				}
			}
			excelHeaderList.add(excelHeaderMap);

			/**
			 *  화면 grid 의 resource 정의
			 */
			gridHeader.append("," + headerId);
			gridText.append("," + headerNm);
			gridWidth.append("," + (headerNm.length() * 20 ));
			if("CONF_NM".equals(colId)){
				gridFlex.append("," + "1");
			}else{
				gridFlex.append("," + "na");
			}
			gridSortable.append("," + "na");
			gridAlign.append("," + "left");
			if("NUMBER".equals(colType)){
				gridXtype.append("," + "na");
			}else{
				gridXtype.append("," + "na");
			}
			
			// 코드 속성이면 코드명 추가
			if ("SELECT".equals(colHthmlType) || "RADIO".equals(colHthmlType) || colHthmlType.matches("POPUP.*")) {
				headerNm = headerNm + "(코드명)";
				gridHeader.append("," + "EMPTY." + colId + "_NM");
				gridText.append("," + headerNm);
				gridWidth.append("," + (headerNm.length() * 20 ));
				gridFlex.append("," + "na");
				gridSortable.append("," + "na");
				gridAlign.append("," + "left");
				gridXtype.append("," + "na");
			}
			
		}
		
		gridResourceMap.put("gridHeader", gridHeader.toString());
		gridResourceMap.put("gridText", gridText.toString());
		gridResourceMap.put("gridWidth", gridWidth.toString());
		gridResourceMap.put("gridFlex", gridFlex.toString());
		gridResourceMap.put("gridSortable", gridSortable.toString());
		gridResourceMap.put("gridAlign", gridAlign.toString());
		gridResourceMap.put("gridXtype", gridXtype.toString());
		
		resultMap.put("excelHeaderList", excelHeaderList);
		resultMap.put("gridResourceMap", gridResourceMap);
		
		return resultMap;
	}
	

	private List<HashMap> selectPopupCodeList() {
		List popupCodeList = new ArrayList();
		
		// 제조사 목록 조회
		HashMap dataMap = new HashMap();
		List vendorList = assetExcelRegistDAO.selectVendorList();
		dataMap.put("popupCode", "VENDOR");
		dataMap.put("resultList", vendorList);
		popupCodeList.add(dataMap);
		
		// 위치 목록 조회
		dataMap = new HashMap();
		List locList = assetExcelRegistDAO.selectLocList();
		dataMap.put("popupCode", "LOCATION");
		dataMap.put("resultList", locList);
		popupCodeList.add(dataMap);
		
		// 분류체계 목록 조회
		dataMap = new HashMap();
		List classList = assetExcelRegistDAO.selectclassList();
		dataMap.put("popupCode", "CLASS");
		dataMap.put("resultList", classList);
		popupCodeList.add(dataMap);
		
		// 부서 목록 조회
		dataMap = new HashMap();
		List custList = assetExcelRegistDAO.selectCustList();
		dataMap.put("popupCode", "CUST");
		dataMap.put("resultList", custList);
		popupCodeList.add(dataMap);
		
		// 고객사 목록 조회
		dataMap = new HashMap();
		List customerList = assetExcelRegistDAO.selectCustomerList();
		dataMap.put("popupCode", "CUSTOMER");
		dataMap.put("resultList", customerList);
		popupCodeList.add(dataMap);
		
		// 사용자 목록 조회
		dataMap = new HashMap();
		List userList = assetExcelRegistDAO.selectUserList();
		dataMap.put("popupCode", "USER");
		dataMap.put("resultList", userList);
		popupCodeList.add(dataMap);
		
		return popupCodeList;
	} // selectPopupCodeList
	
	/* 엑셀템플릿 자동 생성(By Poi) */
	private List<HashMap> selectPopupCodeListByPopupMng() throws NkiaException {
		List popupCodeList = new ArrayList();

		// 엑셀콤보서식 출력을 위한 팝업코드 목록 조회(AM_POPUP_MNG.POPUP_CODE_QRY_OPT => 구분자 : 더블콤마(,,) , 파라미터구분자 : ==
		HashMap mapQueryParam = new HashMap();
		List listPopupCodeQuery = null;
		try {
			listPopupCodeQuery = excelFileService.selectPopupCodeListQueryForExcel(mapQueryParam);
			if ( listPopupCodeQuery != null ) {
				for ( int pq = 0 ; pq < listPopupCodeQuery.size() ; pq ++) {
					String strPopupCodeIndividualQuery = "";
					
					HashMap mapPopupCode = (HashMap) listPopupCodeQuery.get(pq);
					if ( StringUtil.parseString(mapPopupCode.get("POPUP_CODE_QRY")).trim().equals("") == false ) {
						strPopupCodeIndividualQuery = StringUtil.parseString(mapPopupCode.get("POPUP_CODE_QRY")).trim();
						String strTempPCQOption = StringUtil.parseString(mapPopupCode.get("POPUP_CODE_QRY_OPT")).trim();
						if ( strTempPCQOption.equals("") == false ) {
							String [] arrTemp1 = strTempPCQOption.split(",,");
							if ( arrTemp1 != null ) {
								for ( int po = 0 ; po < arrTemp1.length ; po++) {
									if ( arrTemp1[po].trim().equals("") == false ) {
										
										String [] arrTemp2 = arrTemp1[po].trim().split("==");
										String strTempOption = arrTemp2[0];
										for ( int poo = 1 ; poo < arrTemp2.length ; poo++) {
											if ( arrTemp2[poo].trim().equals("") == false && StringUtil.parseString(mapQueryParam.get(arrTemp2[poo].trim())).equals("")== false) { 
												strTempOption = strTempOption.replace("{"+(poo-1)+"}", StringUtil.parseString(mapQueryParam.get(arrTemp2[poo].trim())) );
											}
										}
										if ( arrTemp2.length == 1 || strTempOption.equals(arrTemp2[0]) == false ) { // 파라미터가 있는 경우 파라미터 값이 존재하는 경우만 적용
											strPopupCodeIndividualQuery += " AND " + strTempOption;
										}
										/*
										if ( arrTemp2.length >= 2 && mapQueryParam.get(arrTemp2[1].trim()) != null) { // 파라미터 값이 존재하는 경우만 적용
											strPopupCodeIndividualQuery += " AND " + arrTemp2[0].trim() + " = '" + StringUtil.parseString(mapQueryParam.get(arrTemp2[1].trim()))+ "'";
										}
										*/
									}
								} // for po
							}
						}
					}
					//popup_code_individual_query
					List listPopupCodeList = null;
					if ( strPopupCodeIndividualQuery.trim().equals("") == false ) {
						mapQueryParam.put("popup_code_individual_query", strPopupCodeIndividualQuery);
						listPopupCodeList = excelFileService.selectPopupCodeListDynamic(mapQueryParam);
					}
					HashMap dataMap = new HashMap();
					dataMap.put("popupCode", StringUtil.parseString(mapPopupCode.get("POPUP_CODE")) );
					dataMap.put("resultList", listPopupCodeList);
					popupCodeList.add(dataMap);
					
				} // for pq
			} // if ( listPopupCodeQuery != null ) {			
		} catch (Exception e) {
			throw new NkiaException(e);
		} // 파라미터는 NULL 이어도 무방함.
		
		return popupCodeList;
	} // selectPopupCodeListByPopupMng
	
	/**
	 * 엑셀 업로드 자산 validation
	 * @throws IOException 
	 */
	@Override
	public HashMap validateAssetExcelListData(String classType, List classIdList, List assetList) throws NkiaException, IOException {
		
		// 업로드 자산 내에서 중복 validation 실행
		String infraTableNm = "AM_INFRA";
		if("SW".equals(classType)){
			infraTableNm = "AM_SW";
		}
		// 중복 체크 할 컬럼
		String[] checkColumns = {};
		HashMap resultMap = checkDuplicateAsset(classType, assetList, checkColumns);
		
		// 데이터 타입 및 일반 validation
		boolean isSuccess = (boolean) resultMap.get("isSuccess");
		if(isSuccess) {
			resultMap = validateAssetListData(classType, classIdList, assetList);
		}
		
		return resultMap;
	}
	
	
	/**
	 * 엑셀업로드 자산 내의 중복데이터 확인
	 * checkColumns : 엑셀의 헤더id [ 테이블명.컬럼명]
	 * @param classType
	 * @param assetList
	 * @param resultMap
	 * @return
	 */
	public HashMap checkDuplicateAsset(String classType, List<HashMap> assetList, String[] checkColumns){
		List duplicateAssetList = new ArrayList();
		boolean isSuccess = true;
		StringBuffer resultMsg = new StringBuffer("중복된 구성 데이터가 존재합니다.");
		
		
		if(checkColumns != null) {
			for(int i = 0; i < checkColumns.length; i++ ){
				String checkColumnNm = checkColumns[i];
				String columnId = checkColumnNm.split("\\.")[1];
				HashMap columnInfo = assetExcelRegistDAO.selectAttrInfo(columnId);
				String columnDesc = (String) columnInfo.get("COL_NAME");
				
				List checkedValues = new ArrayList();	// validation 체크 한 값을 담을 List(이미 체크된 값을 또 확인하지 않기 위해)
				for(HashMap dataMap : assetList) {
					int duplicateCnt = 0;
					String checkColumnVal = "";
					checkColumnVal = StringUtil.parseString(dataMap.get(checkColumnNm));
					
					if(!"".equals(checkColumnVal)){	// validation 체크하지 않은 값에 대해서만 확인하기 위해 사용 
						for(int gridIdx = 0; gridIdx < assetList.size(); gridIdx++){
							HashMap tempMap = assetList.get(gridIdx);
							String compareCoumnlVal = (String) tempMap.get(checkColumnNm);
							if(!checkedValues.contains(compareCoumnlVal)){
								if(compareCoumnlVal.equals(checkColumnVal)){
									duplicateCnt++;
									if(duplicateCnt > 1){
										HashMap dupleDataMap = new HashMap();
										dupleDataMap.put("IDX", gridIdx);
										dupleDataMap.put("COLUMN", checkColumnNm);
										duplicateAssetList.add(dupleDataMap);
									}
								}
							}
						}
						
						// 중복건이 존재 할 경우
						if(duplicateCnt > 1){
							isSuccess = false;
							resultMsg.append("\n - " + columnDesc + " : " + checkColumnVal);
						}
						// 체크 한 값
						checkedValues.add(checkColumnVal);
					}
				}// end assetList for
			}// end checkColumns for
			
		}
		HashMap resultMap = new HashMap();
		resultMap.put("isSuccess", isSuccess);
		if(!isSuccess) {
			resultMap.put("resultMsg", resultMsg.toString());
			resultMap.put("resultList", duplicateAssetList);
		}
		
		return resultMap;
	}

	/**
	 * 자산 데이터 검증
	 * @throws IOException 
	 */
	@Override
	public HashMap validateAssetListData(String classType, List<String> searchClassIdList, List<HashMap> assetList) throws NkiaException, IOException {
		HashMap resultMap = new HashMap();
		boolean isSuccess = true;		// validation 성공여부
		StringBuffer resultMsg = new StringBuffer();	// 결과메세지
		List validationReusltList = new ArrayList();	// 
		
		// 자산 중복 체크 컬럼
		String duplicateCheckColumns = NkiaApplicationPropertiesMap.getProperty("Globals.Asset.Duplicate.Check.Columns");
		String[] duplicateCheckColArr = {};
		
		if(!StringUtils.isEmpty(duplicateCheckColumns)) {
			duplicateCheckColArr = duplicateCheckColumns.split(",");
		}
		
		// validation 을 위한 속성 정보 목록 생성
		HashMap tempAssetData = assetList.get(0);
		tempAssetData.remove("id");
		List<HashMap> attrInfoList = new ArrayList();		// 속성 정보 목록
		Iterator tempIter = (Iterator) tempAssetData.keySet().iterator();
		while(tempIter.hasNext()){
			String tableCols = (String) tempIter.next();	// 형태 : [ 테이블ID.컬럼ID ]
			String[] tableColsAttr = tableCols.split("\\.");
			String columnId = tableColsAttr[1];
			
			// key의 속성 정보 조회하여 List 에 담아준다
			HashMap attrInfo = assetExcelRegistDAO.selectAttrInfo(columnId);
			if(attrInfo != null) {
				attrInfoList.add(attrInfo);
			}
		}
		
		/** 자산 데이터의 속성별 validation */
		int idx = 0;
		HashMap validFalseDataMap = new HashMap();
		
		// 분류체계 검증을 위한 class_id 목록 조회
		List<String> classIdList = new ArrayList();
		if(classType != null){
			HashMap paramMap = new HashMap<>();
			paramMap.put("class_type", classType);
			paramMap.put("class_id_list", searchClassIdList);
			classIdList = assetExcelRegistDAO.selectClassIdList(paramMap);
		}
		
		// 센터위치 검증을 위한 코드 목록 조회
		HashMap paramMap = new HashMap<>();
		List<String> locationCdList = assetExcelRegistDAO.selectLocCdList(paramMap);
				
		for(HashMap assetData : assetList) {
			int validFailCnt = 0;
			
			assetData.remove("id");		// grid 속성 id 제거
			int rowNum = (Integer) assetData.get("EMPTY.ROW_NUM");	// 엑셀 행번호
			
			Iterator iter = (Iterator) assetData.keySet().iterator();
			while(iter.hasNext()){
				validFalseDataMap = new HashMap();
				String tableCols = (String) iter.next();	// 형태 : [ 테이블ID.컬럼ID ]
				String[] tableColsAttr = tableCols.split("\\.");
				String tableId = tableColsAttr[0];
				String columnId = tableColsAttr[1];		// 그리드의 컬럼 ID
				String columnValue = StringUtil.parseString(assetData.get(tableCols));
				
				// 컴포넌트 구성 데이터
				if("COMP".equals(tableId)) {
					
				}else{
					// 속성 항목
				
					/** 분류체계 확인 */
					if("CLASS_ID".equals(columnId)){
						if(!classIdList.contains(columnValue)){
							isSuccess = false;
							resultMsg.append(rowNum + "번행의 분류체계가 잘못 선택되었습니다. \n");
							validFalseDataMap.put("IDX", idx);
							validFalseDataMap.put("COLUMN", tableCols);
							validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
							validFailCnt++;
						}
					} else if("LOCATION_CD".equals(columnId)){
						if(!locationCdList.contains(columnValue)){
							isSuccess = false;
							resultMsg.append(rowNum + "번행의 센터위치가 잘못 선택되었습니다. \n");
							validFalseDataMap.put("IDX", idx);
							validFalseDataMap.put("COLUMN", tableCols);
							validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
							validFailCnt++;
						}
					} else {
						/** 속성정보 참조하여 validation [필수값, 데이터타입, 코드값유효] */
						for(HashMap attrInfo : attrInfoList) {
							String attrColName = (String) attrInfo.get("COL_NAME");
							if(columnId.equals(attrInfo.get("COL_ID"))) {
								
								/** 필수값 validation */
								if("Y".equals(attrInfo.get("NOT_NULL"))) {
									if("".equals(columnValue) || columnValue == null) {
										isSuccess = false;
										resultMsg.append(rowNum + " 번행의 " + attrColName + " 은(는) 필수입력입니다. \n");
										validFalseDataMap.put("IDX", idx);
										validFalseDataMap.put("COLUMN", tableCols);
										validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
										validFailCnt++;
										
									}
								}
								
								if(!"".equals(columnValue) && columnValue != null) {
									/** 데이터 타입 validation */
									String colType = (String) attrInfo.get("COL_TYPE");
									if( "DATE".equals(colType) ) {
										if(!isDataFormatCheck(colType, columnValue)) {
											isSuccess = false;
											resultMsg.append(rowNum + " 번행의 " + attrColName + " 날짜 형식이 잘못되었습니다. \n");
											validFalseDataMap.put("IDX", idx);
											validFalseDataMap.put("COLUMN", tableCols);
											validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
											validFailCnt++;
										}
									} else if( "NUMBER".equals(colType) ) {
										if(!isDataFormatCheck(colType, columnValue)) {
											isSuccess = false;
											resultMsg.append(rowNum + " 번행의 " + attrColName + " 은(는) 숫자만 입력가능합니다. \n");
											validFalseDataMap.put("IDX", idx);
											validFalseDataMap.put("COLUMN", tableCols);
											validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
											validFailCnt++;
										}
									}else if ( colType.startsWith("VARCHAR") || colType.startsWith("CHAR") ) {
										
										/** 코드값 유효 validation */
										String htmlType = (String) attrInfo.get("HTML_TYPE");
										if(htmlType.matches("POPUP.*") || "SELECT".equals(htmlType) || "RADIO".equals(htmlType)) {
											
											String tempColNm = "";
											if("SELECT".equals(htmlType) || "RADIO".equals(htmlType) ) {
												tempColNm = attrColName + "(공통코드 : " + attrInfo.get("SYS_CODE") + ")";
											}else if (htmlType.matches("POPUP.*")) {
												tempColNm = attrColName;
											}
											
											// 코드값은 입력되었는데, 조회된 코드값이 없는 경우
											if(!"".equals(columnValue) && columnValue != null) {
												String codeNmKey = "EMPTY." + columnId + "_NM";
												if(assetData.containsKey(codeNmKey)) {
													String codeNmValue = (String) assetData.get(codeNmKey);
													if("".equals(codeNmValue) || codeNmValue == null) {
														isSuccess = false;
														resultMsg.append(rowNum + " 번행의 " +tempColNm  + " 코드값이 잘못되었습니다. \n");
														validFalseDataMap.put("IDX", idx);
														validFalseDataMap.put("COLUMN", tableCols);
														validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
														validFailCnt++;
													}
												}
											}
										}
										
										/** 문자열 길이 validation */
										int colLen = Integer.parseInt( String.valueOf(attrInfo.get("COL_LEN")));
										int valueLength = columnValue.getBytes(BaseConstraints.APP_DATABASE_CHARSET).length;
										if(colLen < valueLength) {
											isSuccess = false;
											resultMsg.append(rowNum + " 번행의 " + attrColName + " 이 작성 가능한 길이를 초과하였습니다. [ " + valueLength + "/" + colLen + " ]\n");
											validFalseDataMap.put("IDX", idx);
											validFalseDataMap.put("COLUMN", tableCols);
											validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
											validFailCnt++;
										}
										
										/** 제약조건 validation */
										String constType = (String) attrInfo.get("CONST_TYPE");
										
										// 숫자 제약조건
										if("number".equals(constType)) {
											if(!isDataFormatCheck("NUMBER", columnValue)) {
												isSuccess = false;
												resultMsg.append(rowNum + " 번행의 " + attrColName + " 은(는) 숫자만 입력가능합니다. \n");
												validFalseDataMap.put("IDX", idx);
												validFalseDataMap.put("COLUMN", tableCols);
												validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
												validFailCnt++;
											}
										}
										
									}
								}
								
							}
						}
					}// 속성항목 VALIDATION 체크 END
				}
			}// 컬럼 반복문 END
			
			// 고객사, 용도, 호스트명 중복 체크 시작
			// 1. 엑셀 파일내 중복 체크
			String originData = "";
			String targetData = "";
			
			for (int i=0; i<duplicateCheckColArr.length; i++) {
				if(assetData.containsKey(duplicateCheckColArr[i])) {
					originData += assetData.get(duplicateCheckColArr[i]).toString();
				}
			}
			
			int checkRowIdx = 0;
			
			for(HashMap checkData : assetList) {
				targetData = "";
				
				if(idx == checkRowIdx) continue;
				
				for (int i=0; i<duplicateCheckColArr.length; i++) {
					if(checkData.containsKey(duplicateCheckColArr[i])) {
						targetData += checkData.get(duplicateCheckColArr[i]).toString();
					}
				}
				
				if(originData.equals(targetData)) {
					isSuccess = false;
					resultMsg.append(rowNum + " 번행의 " + " 자산이 다른행의 자산과 중복됩니다.(호스트명 중복) \n");
					
					iter = (Iterator) assetData.keySet().iterator();
					
					while(iter.hasNext()){
						String tableCols = (String) iter.next();
						
						for (int i=0; i<duplicateCheckColArr.length; i++) {
							if(tableCols.equals((duplicateCheckColArr[i]))) {
								validFalseDataMap = new HashMap();
								validFalseDataMap.put("IDX", idx);
								validFalseDataMap.put("COLUMN", tableCols);
								validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
								validFailCnt++;
							}
						}
					}
				}
				
				checkRowIdx++;
			}
			
			// 2. 이미 등록된 자산인지 체크
			HashMap duplicateParamMap = new HashMap();
			duplicateParamMap.put("class_type", classType);
			
			for (int i=0; i<duplicateCheckColArr.length; i++) {
				String[] paramColsArr = duplicateCheckColArr[i].split("\\.");
				
				if(paramColsArr.length == 2) {
					if(assetData.containsKey(duplicateCheckColArr[i])) {
						String paramVal = assetData.get(duplicateCheckColArr[i]).toString();
						duplicateParamMap.put(paramColsArr[1].toLowerCase(), paramVal);
					}
				}
			}
			
			int duplicateCnt = assetStatisticsDAO.selectAssetDuplicateCount(duplicateParamMap);
			
			if(duplicateCnt > 0) {
				isSuccess = false;
				resultMsg.append(rowNum + " 번행의 " + " 자산이 이미등록 되어 있습니다.(호스트명 중복) \n");
				
				iter = (Iterator) assetData.keySet().iterator();
				
				while(iter.hasNext()){
					String tableCols = (String) iter.next();
					
					for (int i=0; i<duplicateCheckColArr.length; i++) {
						if(tableCols.equals(duplicateCheckColArr[i])) {
							validFalseDataMap = new HashMap();
							validFalseDataMap.put("IDX", idx);
							validFalseDataMap.put("COLUMN", tableCols);
							validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
							validFailCnt++;
						}
					}
				}
			}
			// 고객사, 용도, 호스트명 중복 체크 끝
			
			idx++;
			
			if(validFailCnt > 0) {
				resultMsg.append("\n");
			}
		}// ROW 반복문 END
		
		if(isSuccess) {
			resultMsg.append("검증 성공하였습니다.");
		}
		resultMap.put("isSuccess", isSuccess);
		resultMap.put("resultMsg", resultMsg.toString());
		resultMap.put("resultList", validationReusltList);
		return resultMap;
	}

	/**
	 * 업로드 자산 임시테이블 등록
	 */
	@Override
	public void registTempAssetList(HashMap paramMap) throws NkiaException {

		HashMap paramData = (HashMap) paramMap.get("paramData");
		List<HashMap> assetList = (List) paramMap.get("assetList");
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = "";
		if(userVO != null) {
			userId = (String)userVO.getUser_id();
		}
		
		/** 시퀀스 테이블 등록 */
		String tempSeq = tempAssetService.createTempSeq();
		paramData.put("temp_seq", tempSeq);
		paramData.put("temp_div", "EXCEL");
		paramData.put("ins_user_id", userId);
		tempAssetService.insertAmTempSeq(paramData);
		
		/** 일괄등록 검토메뉴의 사용여부에 따라, 임시자산 저장의 상태값을 별도로 한다. 
		 * 2019-04-16 좌은진
		 *  */
		boolean isCnfirmMenu = tempAssetService.checkCnfirmMenu();
		String tempAssetStatus;	// 일괄등록 요청상태 (공통코드 : OPMSINFO_STATE)
		if(isCnfirmMenu){
			tempAssetStatus = "REGIST";
		}else{
			tempAssetStatus = "CONFIRM";
		}

long totStart = System.currentTimeMillis();		
		for(HashMap assetMap : assetList) {
			assetMap.remove("id");
			
			HashMap assetDataMap = new HashMap(); 		// 일반 속성 데이터 Map
			HashMap compOperMap = new HashMap();// 컴포넌트 운영자정보 데이터 Map
			
			/** 시퀀스 매핑 테이블 등록 */
			// 임시 자산ID 생성
			String assetTempId = tempAssetService.createAssetTempId();
			String confTempId = assetTempId;
			
			
			// 매핑 테이블 등록
			assetDataMap.put("status", tempAssetStatus);		
			assetDataMap.put("temp_seq", tempSeq);
			assetDataMap.put("asset_temp_id", assetTempId);
			assetDataMap.put("conf_temp_id", confTempId);
			tempAssetService.insertAmTempMapping(assetDataMap);
			
			/**
			 * 데이터 항목 반복문
			 * 속성 : 반복문 내에서 임시테이블 등록
			 * 컴포넌트 데이터 : 데이터 따로 Map에 담아준 뒤 하단에서 따로 임시테이블 등록 처리
			 */
			
			List<HashMap> colDataList = new ArrayList();
			Iterator iter = (Iterator) assetMap.keySet().iterator();
			while(iter.hasNext()){
				String tableCols = (String) iter.next();	// 형태 : [ 테이블ID.컬럼ID ]
				String[] tableColsAttr = tableCols.split("\\.");
				String tableId = tableColsAttr[0];
				
				// 컴포넌트 항목과 일반 속성 항목을 구분하여 데이터 저장
				if("COMP".equals(tableId)) {
					
					/** 컴포넌트 항목 임시테이블 등록 */
					String compTableId = tableColsAttr[1];		// 컴포넌트 데이터 임시테이블명
					String compColumnId = tableColsAttr[2];		// 컴포넌트 데이터 컬럼명
					String columnValue = StringUtil.parseString(assetMap.get(tableCols));	// 컴포넌트 데이터 값
					
					// 운영자정보 임시데이터 등록
					if("AM_TEMP_OPER_DATA".equals(compTableId)) {
						compOperMap.put(compColumnId, columnValue);
					}
					
				} else {
					/** 일반 속성 항목 임시테이블 등록 */
					String columnId = tableColsAttr[1];
					String columnValue = StringUtil.parseString(assetMap.get(tableCols));
					
					if(!"EMPTY".equals(tableId) && !columnValue.equals("") && !"ASSET_STATE".equals(columnId)) {	// ASSET_STATE 는 하단에서 고정으로 입력함.
						HashMap tempMap = new HashMap();
						tempMap.put("asset_temp_id", assetTempId);
						tempMap.put("conf_temp_id", confTempId);
						tempMap.put("ins_table_nm", tableId);
						tempMap.put("ins_column_id", columnId);
						tempMap.put("col_value", columnValue);
						tempMap.put("ins_user_id", userId);
						colDataList.add(tempMap);
						
					}
				}
			} // 컬럼 반복문 END
			
			/**  수동추가 컬럼 등록 (필요시 아래와같은 형식으로 사용) */
			// 자산상태를 'ACTIVE' 로 고정 등록
			HashMap tempMap = new HashMap();
			tempMap.put("asset_temp_id", assetTempId);
			tempMap.put("conf_temp_id", confTempId);
			tempMap.put("ins_table_nm", "AM_ASSET");
			tempMap.put("ins_column_id", "ASSET_STATE");
			tempMap.put("col_value", "ACTIVE");
			tempMap.put("ins_user_id", userId);
			colDataList.add(tempMap);
			
			// 컬럼정보 등록
			HashMap tempParamMap = new HashMap();
			tempParamMap.put("paramList", colDataList);
			tempAssetService.insertAmTempData(tempParamMap);
			
			// 분류체계 테이블 임의값 등록(분류체계 테이블 미등록 방지)
			String classType = (String) paramData.get("class_type");
			tempAssetService.mustInsertClassTable(classType, assetTempId, confTempId, userId);
			
			//운영자 컴포넌트 정보 등록
			if(compOperMap != null && compOperMap.size() > 0) {
				String operUSerType = (String) compOperMap.get("OPER_USER_TYPE");	// 담당 구분
				String operUSerId = (String) compOperMap.get("OPER_USER_ID");	// 담당자 ID
				
				if(operUSerType != null && !"".equals(operUSerType)) {
					String[] operUSerTypeArr = operUSerType.split(",");
					String[] operUSerIdArr = operUSerId.split(",");
					for(int i = 0; i < operUSerTypeArr.length; i++) {
						try {
							HashMap paramDataMap = new HashMap();
							paramDataMap.put("seq", i);
							paramDataMap.put("conf_temp_id", confTempId);
							paramDataMap.put("oper_user_type", operUSerTypeArr[i]);
							paramDataMap.put("oper_user_id", operUSerIdArr[i]);
							paramDataMap.put("upd_user_id", userId);
							assetExcelRegistDAO.insertAmTempOperData(paramDataMap);
						} catch (ArrayIndexOutOfBoundsException e) {
							// 배열 크기 오류 날 경우 성공한데까지만 등록
							break;
						}
					}
				}
			}
			
		}
long totEnd = System.currentTimeMillis();
	}
	
	/**
	 * 데이터 타입 체크
	 * @param dataType [DATE, NUMBER]
	 * @param value
	 * @return
	 */
	private boolean isDataFormatCheck(String dataType, String value) {
		boolean result = true;
		
		if("DATE".equals(dataType)) {
			SimpleDateFormat dateFormatParser1 = new SimpleDateFormat("MM/dd/yy");
			try{
				dateFormatParser1.setLenient(false);
				dateFormatParser1.parse(value);
			}catch(Exception e1) {
				try {
					SimpleDateFormat dateFormatParser2 = new SimpleDateFormat("yyyy-MM-dd");
					dateFormatParser2.setLenient(false);
					dateFormatParser2.parse(value);
				} catch (Exception e2) {
					try {
						SimpleDateFormat dateFormatParser3 = new SimpleDateFormat("yyyyMMdd");
						dateFormatParser3.setLenient(false);
						dateFormatParser3.parse(value);
					} catch (Exception e3) {
						result = false;
					}
					
				}
			}
		} else if("NUMBER".equals(dataType)) {
			try {
				Double.parseDouble(value);
			} catch (NumberFormatException e) {
				result = false;
			}
		}
		
		return result;
	}
	
	/**
	 * 날짜 형식 변경
	 * @param dateValue
	 * @param toFormat
	 * @return
	 */
	private String getChgDateFormat(String dateValue, String toFormat){
		String resultValue = "";
		String fromFormat = "";
		
		// 기본 데이트포멧
		if(toFormat == null) {
			toFormat = "yyyy-MM-dd";
		}
		
		// 기존 데이트포멧 찾기
		SimpleDateFormat dateFormatParser1 = new SimpleDateFormat("MM/dd/yy");
		try{
			dateFormatParser1.parse(dateValue);
			fromFormat = "MM/dd/yy";
		}catch(Exception e1) {
			try {
				SimpleDateFormat dateFormatParser2 = new SimpleDateFormat("yyyy-MM-dd");
				dateFormatParser2.parse(dateValue);
				fromFormat = "yyyy-MM-dd";
			} catch (Exception e2) {
				try {
					SimpleDateFormat dateFormatParser3 = new SimpleDateFormat("yyyyMMdd");
					dateFormatParser3.parse(dateValue);
					fromFormat = "yyyyMMdd";
				} catch (Exception e3) {
					fromFormat = "";
				}
				
			}
		}
		
		// 데이트 포맷 변경
		try {
			SimpleDateFormat fFormat = new SimpleDateFormat(fromFormat);
			SimpleDateFormat tFormat = new SimpleDateFormat(toFormat);
			Date chageDate = fFormat.parse(dateValue);
			
			resultValue = tFormat.format(chageDate);
		
		} catch (ParseException e) {
			resultValue = dateValue;
			new NkiaException(e);
		}
		return resultValue;
	}
	
	


	/**
	 * 검증 결과 파일 다운로드
	 */
	@Override
	public boolean validResultFileDownload(HttpServletRequest request, HttpServletResponse response, String fileName, String filePath) {
		boolean isSuccess = false;
		
		String resultText = request.getParameter("resultText");
		
		// 파일 다운로드 전 파일 저장
		String fullPath = null;
		
		File directory = new File(filePath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}
		
		fullPath = filePath+File.separator+fileName;
		FileWriter fw = null;
		BufferedWriter bw = null;
		
		try {
			
			fw = new FileWriter(fullPath);
            bw = new BufferedWriter(fw);
            bw.write(resultText);
            bw.close();
            
            // 파일 다운로드 
            AtchFileUtil atchFileUtil = new AtchFileUtil();
            isSuccess = atchFileUtil.createOutputFile(request, response, fileName, fileName, filePath, null);

		} catch (Exception e) {
			new NkiaException(e);
		}finally {
			try {
				if(bw != null) {
					bw.close();
				}
			} catch (Exception e2) {
				new NkiaException(e2);
			}
		}
		
		return isSuccess;
	}


	/* 엑셀템플릿 자동 생성(By Poi) */
	@Override
	public List searchAssetClsssListForPoi(ModelMap paramMap) throws NkiaException {
		return assetExcelRegistDAO.searchAssetClsssListForPoi(paramMap);
	}

}
