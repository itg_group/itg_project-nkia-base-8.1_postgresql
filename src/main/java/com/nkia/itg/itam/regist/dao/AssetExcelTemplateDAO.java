package com.nkia.itg.itam.regist.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("assetExcelTemplateDAO")
public class AssetExcelTemplateDAO extends EgovComAbstractDAO {

	public List searchExcelTemplateList(HashMap paramMap) {
		return list("AssetExcelTemplateDAO.searchExcelTemplateList", paramMap);
	}

	public int searchExcelTemplateListCount(HashMap paramMap) {
		return (Integer) selectByPk("AssetExcelTemplateDAO.searchExcelTemplateListCount", paramMap);
	}

	public void updateExcelTemplate(ModelMap paramMap) {
		update("AssetExcelTemplateDAO.updateExcelTemplate", paramMap);
	}

	public HashMap selectAssetTemplateAtchFileId(HashMap paramMap) {
		return (HashMap) selectByPk("AssetExcelTemplateDAO.selectAssetTemplateAtchFileId", paramMap);
	}

	public List searchClassList(HashMap paramMap) {
		return list("AssetExcelTemplateDAO.searchClassList", paramMap);
	}

	public List searchModelList(HashMap paramMap) {
		return list("AssetExcelTemplateDAO.searchModelList", paramMap);
	}

	public List searchCodeList(HashMap paramMap) {
		return list("AssetExcelTemplateDAO.searchCodeList", paramMap);
	}

	public List searchLocList(HashMap paramMap) {
		return list("AssetExcelTemplateDAO.searchLocList", paramMap);
	}

	public List searchCustList(HashMap paramMap) {
		return list("AssetExcelTemplateDAO.searchCustList", paramMap);
	}

	public List selectBatchRegistAttrList(ModelMap paramMap) {
		return list("AssetExcelTemplateDAO.selectBatchRegistAttrList", paramMap);
	}

	public void deleteExcelTemplate(ModelMap paramMap) {
		delete("AssetExcelTemplateDAO.deleteExcelTemplate", paramMap);
	}
	
	public void insertExcelTemplate(ModelMap paramMap) {
		insert("AssetExcelTemplateDAO.insertExcelTemplate", paramMap);
	}


}
