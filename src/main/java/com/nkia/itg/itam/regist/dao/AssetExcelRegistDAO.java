package com.nkia.itg.itam.regist.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("assetExcelRegistDAO")
public class AssetExcelRegistDAO extends EgovComAbstractDAO {


	public List selectArrtInfoList(String classType) {
		return list("AssetExcelRegistDAO.selectArrtInfoList", classType);
	}
	
	public String executeSqlGetCodeNmValue(String getCodeFunction) {
		return (String) selectByPk("AssetExcelRegistDAO.executeSqlGetCodeNmValue", getCodeFunction);
	}

	public List<String> selectClassIdList(HashMap paramMap) {
		return list("AssetExcelRegistDAO.selectClassIdList", paramMap);
	}

	public HashMap selectAttrInfo(String columnId) {
		return (HashMap) selectByPk("AssetExcelRegistDAO.selectAttrInfo", columnId);
	}

	// 운영자 임시테이블 데이터 등록
	public void insertAmTempOperData(HashMap paramDataMap) {
		insert("AssetExcelRegistDAO.insertAmTempOperData", paramDataMap);
	}

	public List<HashMap> selectCommCodeListForClassType(HashMap paramMap) {
		return list("AssetExcelRegistDAO.selectCommCodeListForClassType", paramMap);
	}

	public List selectVendorList() {
		return list("AssetExcelRegistDAO.selectVendorList", null);
	}

	// 위치 목록 조회
	public List selectLocList() {
		return list("AssetExcelRegistDAO.selectLocList", null);
	}

	public List selectclassList() {
		return list("AssetExcelRegistDAO.selectclassList", null);
	}

	public List selectCustList() {
		return list("AssetExcelRegistDAO.selectCustList", null);
	}
	
	public List selectCustomerList() {
		return list("AssetExcelRegistDAO.selectCustomerList", null);
	}
	
	public List selectUserList() {
		return list("AssetExcelRegistDAO.selectUserList", null);
	}

	public void insertAmTempSeq(HashMap paramData) {
		// TODO Auto-generated method stub
		
	}

	public List<String> selectLocCdList(HashMap paramMap) {
		return list("AssetExcelRegistDAO.selectLocCdList", null);
	}

	/* 엑셀템플릿 자동 생성(By Poi) */
	public List searchAssetClsssListForPoi(ModelMap paramMap) {
		return list("AssetExcelRegistDAO.searchAssetClsssListForPoi", paramMap);
	}
}
