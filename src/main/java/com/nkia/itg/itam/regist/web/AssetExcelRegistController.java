package com.nkia.itg.itam.regist.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.file.FileUtils;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.regist.service.AssetExcelRegistService;

@Controller
public class AssetExcelRegistController {

	@Resource(name="assetExcelRegistService")
	private AssetExcelRegistService assetExcelRegistService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	

	/**
	 * 자산 일괄 등록 화면 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goRegistAssetExcelTempData.do")
	public String goRegistAssetExcelTempData(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/regist/registAssetExcelTempData.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산 일괄 등록 화면 호출(롯데UBIT용)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goRegistAssetExcelTempDataNew.do")
	public String goRegistAssetExcelTempDataNew(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		// 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		// 엑셀템플릿 자동 생성(By Poi)
		String forwarPage = "";
		String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim();
		if ( excelLib.equals("POI") == true ) {
			forwarPage = "/itg/itam/regist/registAssetExcelTempData_poi.nvf";
		}
		else {
			forwarPage = "/itg/itam/regist/registAssetExcelTempData_new.nvf";
		}
		
		return forwarPage;
	}
	
	/**
	 * 자산 일괄 등록 화면 호출(엑셀템플릿 자동 생성(By Poi))
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goRegistAssetExcelTempDataByPoi.do", method = RequestMethod.GET)
	public String goRegistAssetExcelTempDataByPoi(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		// 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		// 엑셀템플릿 자동 생성(By Poi)
		String forwarPage = "";
		String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim();
		if ( excelLib.equals("POI") == true ) {
			forwarPage = "/itg/itam/regist/registAssetExcelTempData_poi.nvf";
		}
		else {
			forwarPage = "/itg/itam/regist/registAssetExcelTempData_new.nvf";
		}
		
		return forwarPage;
	}	
	
	/**
	 * 자산 일괄 등록 화면 호출(롯데UBIT용)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goRegistAssetExcelTempDataMng.do")
	public String goRegistAssetExcelTempDataMng(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		// 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/regist/registAssetExcelTempDataMng.nvf";
		return forwarPage;
	}
	
	/**
	 * 업로드 된 엑셀템플릿 파일을 파싱
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/parsingExcelTemplateData.do")
	public @ResponseBody ResultVO parsingExcelTemplateData(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			// 업로드된 파일 정보 조회
			AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO();
			atchFileDetailVO.setAtch_file_id((String) paramMap.get("atch_file_id"));
			atchFileDetailVO.setFile_idx("SINGLE_FILE");
			atchFileDetailVO = atchFileService.selectAtchFileDetail(atchFileDetailVO);
			String extsn = atchFileDetailVO.getFile_extsn();
			
			HashMap resultMap = new HashMap();
			if("xlsx".equals(extsn)){
				resultMap = assetExcelRegistService.parsingAssetExcelForXlsx(
						paramMap,
						atchFileDetailVO.getStore_file_name(),
						atchFileDetailVO.getOrignl_file_name(),
						atchFileDetailVO.getFile_store_cours(),
						atchFileDetailVO.getFile_extsn(),
						atchFileDetailVO.getFile_content_type()
						);
			}else if("xls".equals(extsn)){
				resultMap = assetExcelRegistService.parsingAssetExcelForXls(
						paramMap,
						atchFileDetailVO.getStore_file_name(),
						atchFileDetailVO.getOrignl_file_name(),
						atchFileDetailVO.getFile_store_cours(),
						atchFileDetailVO.getFile_extsn(),
						atchFileDetailVO.getFile_content_type()
						);
			}
			
			resultVO.setResultMap(resultMap);
			GridVO gridVO = new  GridVO();
			List resultList = (List) resultMap.get("resultList");
			gridVO.setRows(resultList);
			gridVO.setTotalCount(resultList.size());
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 업로드 된 엑셀템플릿 파일을 파싱
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/getListForGrid.do")
	public @ResponseBody ResultVO getListForGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			GridVO gridVO = new  GridVO();
			List resultList = (List) paramMap.get("paramList");
			gridVO.setRows(resultList);
			gridVO.setTotalCount(resultList.size());
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [자산 일괄 등록]
	 * 업로드 자산 검증 validation
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/validateExcelTemplateData.do")
	public @ResponseBody ResultVO validateExcelTemplateData(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			String classType = (String) paramMap.get("class_type");
			List classIdList = (List) paramMap.get("class_id_list");
			List assetList = (List) paramMap.get("assetList");
			
			HashMap resultMap = assetExcelRegistService.validateAssetExcelListData(classType, classIdList, assetList);
			resultVO.setResultMap(resultMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	return resultVO;
	}
	
	/**
	 * [자산 일괄 등록]
	 * 업로드 자산 등록 (임시테이블 저장)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/registTempAssetList.do")
	public @ResponseBody ResultVO registTempAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			assetExcelRegistService.registTempAssetList(paramMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	return resultVO;
	}
	
	/**
	 * 자산 검증 결과 팝업
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/selectValidResultPop.do")
	public String selectValidResultPop(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/regist/selectValidResultPop.nvf";
		return forwarPage;
	}	
	
	/**
	 * 검증 결과 파일 다운로드
	 * @param paramMap
	 * @return 
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/itam/regist/validResultFileDownload.do")
	public  void validResultFileDownload(HttpServletRequest request, HttpServletResponse response) throws NkiaException, IOException {
			
		String fileName = "검증결과"+"_"+System.currentTimeMillis()+".txt";
		String filePath = BaseConstraints.getExcelFileTempPath();
		
		boolean isSuccess = assetExcelRegistService.validResultFileDownload(request, response, fileName, filePath);
		if( !isSuccess ){
			PrintWriter out = response.getWriter();
			response.setContentType("text/html; charset=utf-8");
			out.println("FileNotFound");
		} else {
			String deleteFlag = NkiaApplicationPropertiesMap.getProperty("Globals.ExcelFile.DeleteYN");
			if("Y".equals(deleteFlag)) {
				FileUtils.deleteFile(filePath+fileName);
			}
		}
			
	}	
	
	/**
	 * 분류체계목록 조회(엑셀템플릿 자동 생성(By Poi))
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchAssetClsssListForPoi.do", method = RequestMethod.POST)
	public @ResponseBody ResultVO searchAssetClsssListForPoi(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = new ArrayList();

			resultList = assetExcelRegistService.searchAssetClsssListForPoi(paramMap);			
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
}
