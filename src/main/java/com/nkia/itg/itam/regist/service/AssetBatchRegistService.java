package com.nkia.itg.itam.regist.service;

import java.util.HashMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AssetBatchRegistService {

	/**
	 * 정보수집서 데이터 임시저장
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertOpmsInfoCollection(HashMap dataMap) throws NkiaException;
	
}
