package com.nkia.itg.itam.regist.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AssetExcelRegistService {

	// 엑셀 데이터를 읽고 그리드에 출력하도록 파싱
	HashMap parsingAssetExcelForXlsx(
			HashMap paramMap,
			String storeFileName,
			String orignlFileName, 
			String fileStoreCours,
			String fileExtsn, 
			String fileContentType) throws NkiaException, IOException;


	/**
	 * 엑셀 업로드 데이터 검증
	 * @param classType
	 * @param assetList
	 * @return
	 * @throws IOException 
	 */
	HashMap validateAssetExcelListData(String classType, List classIdList, List assetList)  throws NkiaException, IOException;

	
	/**
	 * 업로드 자산 내에 데이터 중복확인
	 * @param classType
	 * @param assetList
	 * @param checkColumns	: 중복체크할 컬럼 [테이블명.컬럼명]
	 * @return
	 */
	HashMap checkDuplicateAsset(String classType, List<HashMap> assetList, String[] checkColumns);
	
	
	// 엑셀 업로드 데이터 검증
	HashMap validateAssetListData(String classType, List<String> searchClassIdList, List<HashMap> assetList) throws NkiaException, IOException;

	// 자산 데이터 임시테이블 등록
	void registTempAssetList(HashMap paramMap) throws NkiaException;

	//검증 결과 파일 다운로드
	boolean validResultFileDownload(HttpServletRequest request, HttpServletResponse response, String fileName, String filePath);

	HashMap parsingAssetExcelForXls(HashMap paramMap, String storeFileName,
			String orignlFileName, String fileStoreCours, String fileExtsn,
			String fileContentType) throws NkiaException, IOException;


	/* 엑셀템플릿 자동 생성(By Poi) */
	public List searchAssetClsssListForPoi(ModelMap paramMap) throws NkiaException;

}
