package com.nkia.itg.itam.regist.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AssetExcelTemplateService {

	List searchExcelTemplateList(HashMap paramMap);
	
	int searchExcelTemplateListCount(HashMap paramMap);

	void updateExcelTemplate(ModelMap paramMap) throws NkiaException;

	HashMap selectAssetTemplateAtchFileId(HashMap paramMap);

	List searchClassList(HashMap paramMap);

	List searchModelList(HashMap paramMap);

	List searchCodeList(HashMap paramMap);

	List searchLocList(HashMap paramMap);

	List searchCustList(HashMap paramMap);

	List selectBatchRegistAttrList(ModelMap paramMap);
}
