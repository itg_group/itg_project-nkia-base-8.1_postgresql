package com.nkia.itg.itam.regist.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.itam.regist.service.AssetBatchRegistService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AssetBatchRegistController {

	@Resource(name="assetBatchRegistService")
	private AssetBatchRegistService assetBatchRegistService;
	
	@Resource(name="assetAutomationService")
	private AssetAutomationService assetAutomationService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 정보수집서 등록 화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goAssetBatchRegist.do")
	public String opmsInfoManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/regist/assetBatchRegist.nvf";
		return forwarPage;
	}
	
	/**
	 * 정보수집서 등록 화면(롯데UBIT용)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goAssetBatchRegistNew.do")
	public String opmsInfoManagerNew(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/regist/assetBatchRegist_new.nvf";
		return forwarPage;
	}
	
	/**
	 * 정보수집서 등록
	 * 임시자산 수정
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/insertOpmsInfoCollection.do")
	public @ResponseBody ResultVO insertOpmsInfoCollection(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			/*if(assetAutomationService.isDuplicateAsset(paramMap)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"호스트명"}));
			} else {*/
				UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
				assetBatchRegistService.insertOpmsInfoCollection(paramMap);
			/*}*/
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
}
