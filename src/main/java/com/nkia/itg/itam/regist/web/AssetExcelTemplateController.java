package com.nkia.itg.itam.regist.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.excel.ExcelMakerByPoi;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.regist.service.AssetExcelTemplateService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AssetExcelTemplateController {

	@Resource(name="assetExcelTemplateService")
	private AssetExcelTemplateService assetExcelTemplateService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "excelMakerByPoi")
	private ExcelMakerByPoi excelMakerByPoi;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 자산 템플릿 관리 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goAssetExcelTemplateMng.do")
	public String goAssetExcelTemplateMng(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/regist/assetExcelTemplateMng.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산 템플릿 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchExcelTemplateList.do")
	public @ResponseBody ResultVO searchExcelTemplateList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			List resultList = new ArrayList();

			if(!StringUtils.isEmpty(paramMap.get("page"))) {
				//@@ 페이징 관련 내용 
				PagingUtil.getFristEndNum(paramMap);
				totalCount = assetExcelTemplateService.searchExcelTemplateListCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}

			resultList = assetExcelTemplateService.searchExcelTemplateList(paramMap);			
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 엑셀 템플릿 파일 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/updateExcelTemplate.do")
	public @ResponseBody ResultVO updateExcelTemplate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("UPD_USER_ID", userVO.getUser_id());
			assetExcelTemplateService.updateExcelTemplate(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 자산 템플릿 다운로드 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/regist/goAssetExcelTemplateDownload.do")
	public String goAssetExcelTemplateDown(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/regist/assetExcelTemplateDownload.nvf";
		return forwarPage;
	}
	
	/**
	 * [자산 템플릿 다운로드]
	 * 첨부파일 id 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/selectAssetTemplateAtchFileId.do")
	public @ResponseBody ResultVO selectAssetTemplateAtchFileId(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			HashMap resultMap = new HashMap();
			resultMap = assetExcelTemplateService.selectAssetTemplateAtchFileId(paramMap);
			
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 분류체계 코드 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchClassList.do")
	public @ResponseBody ResultVO searchClassList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			GridVO gridVO = new GridVO();
			List resultList = assetExcelTemplateService.searchClassList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 모델 코드 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchModelList.do")
	public @ResponseBody ResultVO searchModelList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			GridVO gridVO = new GridVO();
			List resultList = assetExcelTemplateService.searchModelList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 설치위치 코드 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchLocList.do")
	public @ResponseBody ResultVO searchLocList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			GridVO gridVO = new GridVO();
			List resultList = assetExcelTemplateService.searchLocList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공통코드 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchCodeList.do")
	public @ResponseBody ResultVO searchCodeList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetExcelTemplateService.searchCodeList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;

	}

	/**
	 * 부서코드 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchCustList.do")
	public @ResponseBody ResultVO searchCustList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetExcelTemplateService.searchCustList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;

	}	
	
	/**
	 * 분류체계 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchClassListExcelDown.do")
	public @ResponseBody ResultVO searchClassListExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");

			List resultList = assetExcelTemplateService.searchClassList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 공통코드 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchCodeListExcelDown.do")
	public @ResponseBody ResultVO searchCodeListExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");

			List resultList = assetExcelTemplateService.searchCodeList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
		
	/**
	 * 설치위치 코드 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchLocListExcelDown.do")
	public @ResponseBody ResultVO searchLocListExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");

			List resultList = assetExcelTemplateService.searchLocList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 부서 코드 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/regist/searchCustListExcelDown.do")
	public @ResponseBody ResultVO searchCustListExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");
	
			List resultList = assetExcelTemplateService.searchCustList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
	
			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 일괄등록용 속성 목록 조회
	 * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/itam/regist/selectBatchRegistAttrList.do")
	public @ResponseBody ResultVO selectBatchRegistAttrList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetExcelTemplateService.selectBatchRegistAttrList(paramMap);

			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
			HashMap resultMap = new HashMap();
			resultMap.put("resultList", resultList);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/regist/basicAssetTemplateExcelDown.do")
	public @ResponseBody ResultVO basicAssetTemplateExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = (List) param.get("resultList");
			excelMap = excelMaker.makeExcelFileForBatchEdit(excelParam, resultList);

			resultVO.setResultMap(excelMap);
	
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/*엑셀템플릿 자동 생성(By Poi) - 일괄등록템플릿 생성*/
	@RequestMapping(value="/itg/itam/regist/basicAssetTemplateExcelDownByPoi.do", method = RequestMethod.POST)
	public @ResponseBody ResultVO basicAssetTemplateExcelDownByPoi(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = (List) param.get("resultList");
			excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultList);

			resultVO.setResultMap(excelMap);
	
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}	
	
}
