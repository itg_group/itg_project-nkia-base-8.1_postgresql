package com.nkia.itg.itam.regist.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.regist.dao.AssetExcelTemplateDAO;
import com.nkia.itg.itam.regist.service.AssetExcelTemplateService;

@Service("assetExcelTemplateService")
public class AssetExcelTemplateServiceImpl implements AssetExcelTemplateService{
	
	@Resource(name="assetExcelTemplateDAO")
	private AssetExcelTemplateDAO assetExcelTemplateDAO;

	@Override
	public List searchExcelTemplateList(HashMap paramMap) {
		return assetExcelTemplateDAO.searchExcelTemplateList(paramMap);
	}

	@Override
	public int searchExcelTemplateListCount(HashMap paramMap) {
		return assetExcelTemplateDAO.searchExcelTemplateListCount(paramMap);
	}

	@Override
	public void updateExcelTemplate(ModelMap paramMap) throws NkiaException {
		assetExcelTemplateDAO.deleteExcelTemplate(paramMap);
		assetExcelTemplateDAO.insertExcelTemplate(paramMap);
		
	}

	@Override
	public HashMap selectAssetTemplateAtchFileId(HashMap paramMap) {
		// TODO Auto-generated method stub
		return assetExcelTemplateDAO.selectAssetTemplateAtchFileId(paramMap);
	}

	@Override
	public List searchClassList(HashMap paramMap) {
		// TODO Auto-generated method stub
		return assetExcelTemplateDAO.searchClassList(paramMap);
	}

	@Override
	public List searchModelList(HashMap paramMap) {
		// TODO Auto-generated method stub
		return assetExcelTemplateDAO.searchModelList(paramMap);
	}

	@Override
	public List searchCodeList(HashMap paramMap) {
		// TODO Auto-generated method stub
		return assetExcelTemplateDAO.searchCodeList(paramMap);
	}

	@Override
	public List searchLocList(HashMap paramMap) {
		// TODO Auto-generated method stub
		return assetExcelTemplateDAO.searchLocList(paramMap);
	}

	@Override
	public List searchCustList(HashMap paramMap) {
		// TODO Auto-generated method stub
		return assetExcelTemplateDAO.searchCustList(paramMap);
	}

	@Override
	public List selectBatchRegistAttrList(ModelMap paramMap) {
		return assetExcelTemplateDAO.selectBatchRegistAttrList(paramMap);
	}

}
