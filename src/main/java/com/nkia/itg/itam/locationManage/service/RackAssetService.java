/*
 * @(#)rackAssetService.java              2013. 7. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface RackAssetService {

	public List searchRackAssetList(ModelMap paramMap) throws NkiaException;

	public int searchRackAssetListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchRackAsset(ModelMap paramMap) throws NkiaException;
	
	public List searchRackAssetExcelDown(Map paramMap) throws NkiaException;
}
