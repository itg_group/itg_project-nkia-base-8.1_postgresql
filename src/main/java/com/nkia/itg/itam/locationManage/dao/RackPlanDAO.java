/*
 * @(#)RackPlanDAO.java              2013. 7. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("rackPlanDAO")
public class RackPlanDAO extends EgovComAbstractDAO{
	
	public List searchLocationPlan(ModelMap paramMap) throws NkiaException{
		return list("RackPlanDAO.searchLocationPlan", paramMap);
	}
	
	public HashMap selectType(ModelMap paramMap) throws NkiaException{
		return (HashMap)selectByPk("RackPlanDAO.selectType", paramMap);
	}
	
	public HashMap selectRackInfo(ModelMap paramMap) throws NkiaException{
		return (HashMap)selectByPk("RackPlanDAO.selectRackInfo", paramMap);
	}
	
	public void insertRackFloorInfo(ModelMap paramMap) throws NkiaException {
		this.insert("RackPlanDAO.insertRackFloorInfo", paramMap);
	}
	
	public void insertRackFloorDivInfo(HashMap info) throws NkiaException {
		this.insert("RackPlanDAO.insertRackFloorDivInfo", info);
	}
	
	public void deleteRackFloorDivInfo(HashMap assetInfo) throws NkiaException {
		this.delete("RackPlanDAO.deleteRackFloorDivInfo", assetInfo);
	}
	
	public void updateRackFloorInfo(ModelMap paramMap) throws NkiaException {
		this.update("RackPlanDAO.updateRackFloorInfo", paramMap);
	}
	
	public void updateRackFloorAssetDel(ModelMap paramMap) throws NkiaException {
		this.update("RackPlanDAO.updateRackFloorAssetDel", paramMap);
	}
	
	public void deleteRackFloorAssetIsNull(ModelMap paramMap) throws NkiaException {
		this.delete("RackPlanDAO.deleteRackFloorAssetIsNull", paramMap);
	}
	
	public void updateType(ModelMap paramMap) throws NkiaException {
		this.update("RackPlanDAO.updateType", paramMap);
	}
	
	public void deleteRackAsset(ModelMap paramMap) throws NkiaException {
		this.delete("RackPlanDAO.deleteRackAsset", paramMap);
	}
	
	public List searchRackPop(ModelMap paramMap) throws NkiaException{
		return list("RackPlanDAO.searchRackPop", paramMap);
	}

	public int searchRackPopCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("RackPlanDAO.searchRackPopCount", paramMap);
	}	
	
	public List searchRackPlanInfo(ModelMap paramMap) throws NkiaException{
		return list("RackPlanDAO.searchRackPlanInfo", paramMap);
	}
	
	public List searchRackFloorAssetList(ModelMap paramMap) throws NkiaException{
		return list("RackPlanDAO.searchRackFloorAssetList", paramMap);
	}
	
	public int searchRackFloorAssetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("RackPlanDAO.searchRackFloorAssetListCount", paramMap);
	}	
	
	public List searchAssetLocationList(ModelMap paramMap) throws NkiaException{
		return list("RackPlanDAO.searchAssetLocationList", paramMap);
	}
	
	public int searchAssetLocationListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("RackPlanDAO.searchAssetLocationListCount", paramMap);
	}	
	
	public List searchRackLocList(ModelMap paramMap) throws NkiaException{
		return list("RackPlanDAO.searchRackLocList", paramMap);
	}
	
	public int searchRackLocListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("RackPlanDAO.searchRackLocListCount", paramMap);
	}
	
	public HashMap selectLocationName(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("RackPlanDAO.selectLocationName", paramMap);
	}	
}
