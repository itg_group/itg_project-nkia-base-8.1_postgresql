/*
 * @(#)supplyServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.locationManage.dao.RackAssetDAO;
import com.nkia.itg.itam.locationManage.service.RackAssetService;

@Service("rackAssetService")
public class RackAssetServiceImpl implements RackAssetService{
	
	private static final Logger logger = LoggerFactory.getLogger(RackAssetServiceImpl.class);
	
	@Resource(name = "rackAssetDAO")
	public RackAssetDAO rackAssetDAO;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public List searchRackAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackAssetDAO.searchRackAssetList(paramMap);
	}

	public int searchRackAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackAssetDAO.searchRackAssetListCount(paramMap);
	}
	
	public List searchRackAsset(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackAssetDAO.searchRackAsset(paramMap);
	}
	
	public List searchRackAssetExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = rackAssetDAO.searchRackAssetExcelDown(paramMap); 
		return resultList;
	}
	
}
