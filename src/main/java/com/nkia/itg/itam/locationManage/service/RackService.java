/*
 * @(#)rackService.java              2013. 7. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface RackService {

	public List searchRackList(ModelMap paramMap) throws NkiaException;

	public int searchRackListCount(ModelMap paramMap) throws NkiaException;
	
	public void insertRackInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateRackInfo(ModelMap paramMap) throws NkiaException;
	
	public int deleteRackInfo(ModelMap paramMap) throws NkiaException, IOException;
	
	public HashMap selectRackAsset(ModelMap paramMap) throws NkiaException;
	
	public int searchAssetListCount(ModelMap paramMap) throws NkiaException;

	public List searchAssetList(ModelMap paramMap) throws NkiaException;
	
	public List searchExcelDown(Map paramMap) throws NkiaException;
	
}
