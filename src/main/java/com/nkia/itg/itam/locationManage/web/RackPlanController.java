/*
 * @(#)RackPlanController.java              2013. 7. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.locationManage.service.RackPlanService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class RackPlanController {
	
	@Resource(name = "rackPlanService")
	private RackPlanService rackPlanService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 구역 상세설정 -  구역 상세설정 탭생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/locationManage/rackPlan.do")
	public String rackPlan(ModelMap paramMap) throws NkiaException{
		return "/itg/itam/locationManage/rackPlanManage/rackPlanTab.nvf";
	}
	
	/**
	 * RACK별 자산위치 -  RACK별 자산위치 탭생성
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/locationManage/rackPlanCheck.do")
	public String rackPlanCheck(ModelMap paramMap) throws NkiaException{
		return "/itg/itam/locationManage/rackPlanCheck/rackPlanCheckTab.nvf";
	}
	
	/**
	 * 구역 상세설정 - 구역 상세설정 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/locationManage/rackPlanManager.do")
	public String rackPlanManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/locationManage/rackPlanManage/rackPlanManager.nvf";
		return forwarPage;
	}
	
	/**
	 * RACK별 자산위치 - RACK별 자산위치 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/locationManage/rackPlanCheckMng.do")
	public String rackPlanCheckMng(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/locationManage/rackPlanCheck/rackPlanCheckMng.nvf";
		return forwarPage;
	}
	
	/**
	 * 구역 상세설정 - 랙 자산정보조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/locationManage/rackInfoTab.do")
	public String rackInfoTab(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		
		String RACK_PLAN_CODE = (request.getParameter("RACK_PLAN_CODE") != null)?request.getParameter("RACK_PLAN_CODE"): "";
		String RACK_MOUNT_CNT = (request.getParameter("RACK_MOUNT_CNT") != null)?request.getParameter("RACK_MOUNT_CNT"): "";
		paramMap.put("RACK_PLAN_CODE", RACK_PLAN_CODE);
		paramMap.put("RACK_MOUNT_CNT", RACK_MOUNT_CNT);
		
		return "/itg/itam/locationManage/rackPlanManage/rackInfoTab.nvf";
	}
	
	/**
	 * RACK별 자산위치 - 랙 자산정보조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/locationManage/rackInfoCheckTab.do")
	public String rackInfoCheckTab(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		
		String RACK_PLAN_CODE = (request.getParameter("RACK_PLAN_CODE") != null)?request.getParameter("RACK_PLAN_CODE"): "";
		String RACK_MOUNT_CNT = (request.getParameter("RACK_MOUNT_CNT") != null)?request.getParameter("RACK_MOUNT_CNT"): "";
		paramMap.put("RACK_PLAN_CODE", RACK_PLAN_CODE);
		paramMap.put("RACK_MOUNT_CNT", RACK_MOUNT_CNT);
		
		return "/itg/itam/locationManage/rackPlanCheck/rackInfoCheckTab.nvf";
	}
	
	/**
	 * 구역 상세설정 - 좌표생성 데이터 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchLocationPlan.do")
	public @ResponseBody ResultVO searchLocationPlan(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();

			resultList = rackPlanService.searchLocationPlan(paramMap);		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 구역 상세설정 - 선택된 상면위치 타입조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/locationManage/selectType.do")
	public @ResponseBody ResultVO selectType(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = rackPlanService.selectType(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 구역 상세설정 - 선택된 랙 정보조회
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/locationManage/selectRackInfo.do")
	public @ResponseBody ResultVO selectRackInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = rackPlanService.selectRackInfo(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 구역 상세설정 - 선택된 상면위치 타입 수정
	 * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/locationManage/updateType.do")
	public @ResponseBody ResultVO updateType(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}			
			rackPlanService.updateType(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 구역 상세설정 - 선택된 상면의 층에 자산정보를 수정
	 * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/locationManage/updateRackFloorInfo.do")
	public @ResponseBody ResultVO updateRackFloorInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}			
			rackPlanService.updateRackFloorInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 구역 상세설정 - 선택된 상면의 층에 선택한 자산을 제거
	 * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/locationManage/updateRackFloorAssetDel.do")
	public @ResponseBody ResultVO updateRackFloorAssetDel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}			
			rackPlanService.updateRackFloorAssetDel(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 구역 상세설정 - 미등록 랙 조회 팝업
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchRackPop.do")
	public @ResponseBody ResultVO searchRackPop(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = rackPlanService.searchRackPop(paramMap);		
			totalCount = rackPlanService.searchRackPopCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 구역 상세설정 - 랙 등록 자산조회(층, 모델명)
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchRackPlanInfo.do")
	public @ResponseBody ResultVO searchRackPlanInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = rackPlanService.searchRackPlanInfo(paramMap);		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 구역 상세설정 - 선택 층 자산리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchRackFloorAssetList.do")
	public @ResponseBody ResultVO searchRackFloorAssetList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = rackPlanService.searchRackFloorAssetList(paramMap);		
			totalCount = rackPlanService.searchRackFloorAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자산 위치선택 팝업
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchAssetLocationList.do")
	public @ResponseBody ResultVO searchAssetLocationList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = rackPlanService.searchAssetLocationList(paramMap);		
			totalCount = rackPlanService.searchAssetLocationListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 랙 선택 팝업
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchRackLocList.do")
	public @ResponseBody ResultVO searchRackLocList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = rackPlanService.searchRackLocList(paramMap);		
			totalCount = rackPlanService.searchRackLocListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 위치주소 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/locationManage/selectLocationName.do")
	public @ResponseBody ResultVO selectAmdbServiceBaseInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = rackPlanService.selectLocationName(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
