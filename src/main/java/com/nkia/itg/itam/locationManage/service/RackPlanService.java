/*
 * @(#)rackPlanService.java              2013. 7. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface RackPlanService {

	public List searchLocationPlan(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectType(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectRackInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateType(ModelMap paramMap) throws NkiaException;
	
	public void updateRackFloorInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateRackFloorAssetDel(ModelMap paramMap) throws NkiaException;
	
	public List searchRackPop(ModelMap paramMap) throws NkiaException;

	public int searchRackPopCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchRackPlanInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchRackFloorAssetList(ModelMap paramMap) throws NkiaException;

	public int searchRackFloorAssetListCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchAssetLocationList(ModelMap paramMap) throws NkiaException;
	
	public int searchAssetLocationListCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchRackLocList(ModelMap paramMap) throws NkiaException;
	
	public int searchRackLocListCount(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectLocationName(ModelMap paramMap) throws NkiaException;
	
}
