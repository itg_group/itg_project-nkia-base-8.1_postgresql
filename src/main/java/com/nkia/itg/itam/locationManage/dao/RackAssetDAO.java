/*
 * @(#)RackAssetDAO.java              2013. 7. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("rackAssetDAO")
public class RackAssetDAO extends EgovComAbstractDAO{
	
	public List searchRackAssetList(ModelMap paramMap) throws NkiaException{
		return list("RackAssetDAO.searchRackAssetList", paramMap);
	}

	public int searchRackAssetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("RackAssetDAO.searchRackAssetListCount", paramMap);
	}
	
	public List searchRackAsset(ModelMap paramMap) throws NkiaException{
		return list("RackAssetDAO.searchRackAsset", paramMap);
	}
	
	public List searchRackAssetExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("RackAssetDAO.searchRackAsset", paramMap);
		return resultList;
	}		
}
