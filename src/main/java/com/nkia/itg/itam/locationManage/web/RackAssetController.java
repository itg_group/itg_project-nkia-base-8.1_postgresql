/*
 * @(#)RackAssetController.java              2013. 7. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.locationManage.service.RackAssetService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class RackAssetController {
	
	@Resource(name = "rackAssetService")
	private RackAssetService rackAssetService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	/**
	 * RACK정보 현황 - RACK정보 현황 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/locationManage/rackAssetManager.do")
	public String rackAssetManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/locationManage/rackAssetManager.nvf";
		return forwarPage;
	}
	
	/**
	 * RACK정보 현황 - 랙 자산정보 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchRackAssetList.do")
	public @ResponseBody ResultVO searchRackAssetList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = rackAssetService.searchRackAssetList(paramMap);		
			totalCount = rackAssetService.searchRackAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * RACK정보 현황 - 선택된 랙의 자산정보 조회
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/locationManage/searchRackAsset.do")
	public @ResponseBody ResultVO searchRackAsset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();

			resultList = rackAssetService.searchRackAsset(paramMap);		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * RACK정보 현황 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/locationManage/searchRackAssetExcelDown.do")
    public @ResponseBody ResultVO searchRackAssetExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = new ArrayList();
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			resultList = rackAssetService.searchRackAssetExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
}
