/*
 * @(#)RackDAO.java              2013. 7. 17.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("rackDAO")
public class RackDAO extends EgovComAbstractDAO{
	
	public List searchRackList(ModelMap paramMap) throws NkiaException{
		return list("RackDAO.searchRackList", paramMap);
	}

	public int searchRackListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("RackDAO.searchRackListCount", paramMap);
	}
	
	public void insertRackInfo(ModelMap paramMap) throws NkiaException {
		this.insert("RackDAO.insertRackInfo", paramMap);
	}	
	
	public void deleteOverRackMount(ModelMap paramMap) throws NkiaException {
		this.delete("RackDAO.deleteOverRackMount", paramMap);
	}	
	
	public void updateRackInfo(ModelMap paramMap) throws NkiaException {
		this.update("RackDAO.updateRackInfo", paramMap);
	}	
	
	public void deleteRackInfo(ModelMap paramMap) throws NkiaException {
		this.delete("RackDAO.deleteRackInfo", paramMap);
	}
	
	public int searchUseRack(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		int rackCnt = (Integer)selectByPk("RackDAO.searchUseRack", paramMap); 
		return rackCnt;
	}
	
	public HashMap selectRackAsset(ModelMap paramMap) throws NkiaException{
		return (HashMap)selectByPk("RackDAO.selectRackAsset", paramMap);
	}
	
	public String selectAssetId(HashMap assetId) throws NkiaException{
		String id = (String) this.selectByPk("RackDAO.selectAssetId", assetId);
		return id;
	}
	
	public void updateOldAssetLocation(ModelMap paramMap) throws NkiaException{
		this.update("RackDAO.updateOldAssetLocation", paramMap);
	}
	
	public void updateAssetLocation(ModelMap paramMap) throws NkiaException{
		this.update("RackDAO.updateAssetLocation", paramMap);
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("RackDAO.searchAssetList", paramMap);
		return resultList;
	}
	
	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("RackDAO.searchAssetListCount", paramMap);
		return count;
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("RackDAO.searchRackList", paramMap);
		return resultList;
	}	
}
