/*
 * @(#)RackPlanServiceImpl.java              2013. . .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.locationManage.dao.RackPlanDAO;
import com.nkia.itg.itam.locationManage.service.RackPlanService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("rackPlanService")
public class RackPlanServiceImpl implements RackPlanService{
	
	private static final Logger logger = LoggerFactory.getLogger(RackPlanServiceImpl.class);
	
	@Resource(name = "rackPlanDAO")
	public RackPlanDAO rackPlanDAO;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public List searchLocationPlan(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchLocationPlan(paramMap);
	}
	
	public HashMap selectType(ModelMap paramMap) throws NkiaException {
		return rackPlanDAO.selectType(paramMap);
	}
	
	public HashMap selectRackInfo(ModelMap paramMap) throws NkiaException {
		return rackPlanDAO.selectRackInfo(paramMap);
	}

	public void updateRackFloorInfo(ModelMap paramMap) throws NkiaException {
		//if(paramMap.get("floor_cnt")!= null){
			int mapSize = paramMap.size();
			String login_user_id = null;
			String rack_plan_code = null;
			Integer floor_cnt = null;
			Integer rack_floor = null;
			for(int i=0; i<mapSize; i++){
				
				String mapIndex = toString().valueOf(i);
		    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
				//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
				if(i == 0){
					HashMap assetInfo = new HashMap();
					assetInfo = (HashMap) paramMap.get(mapIndex);
			    	if(isAuthenticated) {
			    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			    		login_user_id = userVO.getUser_id();
			    		assetInfo.put("login_user_id", login_user_id);
			    	}
			    	rack_plan_code = (String) assetInfo.get("rack_plan_code");
			    	floor_cnt = (Integer) assetInfo.get("floor_cnt");
			    	rack_floor = (Integer) assetInfo.get("rack_floor");
			    	
					for(int k=0; k<floor_cnt; k++){
						assetInfo.put("rack_floor_del", rack_floor-k);
						if((rack_floor-k) > 0 ){
							rackPlanDAO.deleteRackFloorDivInfo(assetInfo);												
						}
					}    	
				}else{
					HashMap info = new HashMap();
					info = (HashMap) paramMap.get(mapIndex);
					if(info != null){
						for(int k=0; k<floor_cnt; k++){
							info.put("rack_plan_code", rack_plan_code);
							info.put("login_user_id", login_user_id);
							info.put("rack_floor", rack_floor-k);
							info.put("rack_floor_div", i);
							if((rack_floor-k) > 0 ){
								rackPlanDAO.insertRackFloorDivInfo(info);												
							}
						}
					}
				}
			}
/*		}else{
			rackPlanDAO.updateRackFloorAssetDel(paramMap);			
		}*/
	}
	
	public void updateRackFloorAssetDel(ModelMap paramMap) throws NkiaException {
		rackPlanDAO.updateRackFloorAssetDel(paramMap);
		if((String)paramMap.get("asset_id") == null){
			rackPlanDAO.deleteRackFloorAssetIsNull(paramMap);			
		}
	}
	
	public void updateType(ModelMap paramMap) throws NkiaException {
		rackPlanDAO.updateType(paramMap);
		if(("rack").equals((String)paramMap.get("rack_plan_type"))){
			for(int i = 1; i <= (Integer)paramMap.get("mount_cnt"); i++){
				paramMap.put("rack_floor", i);
				paramMap.put("rack_floor_div", 1);
				rackPlanDAO.insertRackFloorInfo(paramMap);
			}
		}else{
			rackPlanDAO.deleteRackAsset(paramMap);			
		}
	}
	
	public List searchRackPop(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchRackPop(paramMap);
	}

	public int searchRackPopCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchRackPopCount(paramMap);
	}	
	
	public List searchRackPlanInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchRackPlanInfo(paramMap);
	}
	
	public List searchRackFloorAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchRackFloorAssetList(paramMap);
	}
	
	public int searchRackFloorAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchRackFloorAssetListCount(paramMap);
	}	
	
	public List searchAssetLocationList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchAssetLocationList(paramMap);
	}
	
	public int searchAssetLocationListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchAssetLocationListCount(paramMap);
	}	
	
	public List searchRackLocList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchRackLocList(paramMap);
	}
	
	public int searchRackLocListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackPlanDAO.searchRackLocListCount(paramMap);
	}
	
	public HashMap selectLocationName(ModelMap paramMap) throws NkiaException {
		return rackPlanDAO.selectLocationName(paramMap);
	}
}
