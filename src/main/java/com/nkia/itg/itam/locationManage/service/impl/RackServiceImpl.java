/*
 * @(#)RackServiceImpl.java              2013. 7. 16.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.locationManage.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.itam.locationManage.dao.RackDAO;
import com.nkia.itg.itam.locationManage.service.RackService;

@Service("rackService")
public class RackServiceImpl implements RackService{
	
	private static final Logger logger = LoggerFactory.getLogger(RackServiceImpl.class);
	
	@Resource(name = "rackDAO")
	public RackDAO rackDAO;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public List searchRackList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackDAO.searchRackList(paramMap);
	}

	public int searchRackListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackDAO.searchRackListCount(paramMap);
	}
	
	public void insertRackInfo(ModelMap paramMap) throws NkiaException {
		rackDAO.insertRackInfo(paramMap);
	}	
	
	public void updateRackInfo(ModelMap paramMap) throws NkiaException {
		rackDAO.deleteOverRackMount(paramMap);
		rackDAO.updateRackInfo(paramMap);
	}	
	
	public int deleteRackInfo(ModelMap paramMap) throws NkiaException, IOException {
		int rackCnt = rackDAO.searchUseRack(paramMap);
		if(rackCnt == 0){
			rackDAO.deleteRackInfo(paramMap);
			if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
				AtchFileVO atchFileVO = new AtchFileVO();
				atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
				atchFileService.deleteRegistAtchFile(atchFileVO);
			}
		}
		return rackCnt;
	}
	
	public HashMap selectRackAsset(ModelMap paramMap) throws NkiaException {
		return rackDAO.selectRackAsset(paramMap);
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackDAO.searchAssetList(paramMap);
	}
	
	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return rackDAO.searchAssetListCount(paramMap);
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {		
		List resultList = null;
		resultList = rackDAO.searchExcelDown(paramMap); 
		return resultList;
	}
}
