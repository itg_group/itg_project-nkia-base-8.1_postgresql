/*
 * @(#)maintServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.maint.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.maint.dao.MaintDAO;
import com.nkia.itg.itam.maint.service.MaintService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("maintService")
public class MaintServiceImpl implements MaintService{
	
	private static final Logger logger = LoggerFactory.getLogger(MaintServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "maintDAO")
	public MaintDAO maintDAO;
	
	public List searchMaint(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaint(paramMap);
	}

	public int searchMaintCount(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintCount(paramMap);
	}
	
	public List searchMaintAsset(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintAsset(paramMap);
	}
	
	public void insertMaint(ModelMap paramMap) throws NkiaException {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 유지보수 계약등록
		maintDAO.insertMaint(paramMap);
		// 유지보수 자산등록
		insertMaintAsset(paramMap);
	}

	public void updateMaint(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 유지보수 계약수정
		maintDAO.updateMaint(paramMap);
		// 유지보수 자산등록
		insertMaintAsset(paramMap);
	}
	
	@SuppressWarnings({ "rawtypes"})
	private void insertMaintAsset(ModelMap paramMap) throws NkiaException {
		List dataArray = (ArrayList)paramMap.get("asset_data");
		if( dataArray != null && dataArray.size() > 0 ){
			// 자산 데이터 삭제
			maintDAO.deleteMaintToAsset(paramMap);
			
			// 자산 데이터 등록
			HashMap assetData = null;
			for( int i = 0; i < dataArray.size(); i++ ){
				assetData = (HashMap)dataArray.get(i);
				paramMap.put("asset_id", (String)assetData.get("asset_id"));
				
				if(0 < maintDAO.selectMaintToAssetByAssetCount(paramMap)){
					throw new NkiaException("자산번호:" + (String)assetData.get("asset_id") + ", 구성명:" + (String)assetData.get("conf_nm") + "(은)는 이미 다른 유지보수계약에 연결되어 있습니다. ");
				}
				
				maintDAO.insertMaintToAsset(paramMap);
			}
		}
	}
	
	public void deleteMaint(ModelMap paramMap) throws NkiaException, IOException {
		maintDAO.deleteMaintToAsset(paramMap);
		maintDAO.deleteMaint(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	public List searchAsset(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return maintDAO.searchAsset(paramMap);
	}

	public int searchAssetCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return maintDAO.searchAssetCount(paramMap);
	}
	
	public void insertMaintToAsset(ModelMap paramMap) throws NkiaException {
		maintDAO.insertMaintToAsset(paramMap);
	}	
	
	public void deleteMaintToAsset(ModelMap paramMap) throws NkiaException {
		maintDAO.deleteMaintToAsset(paramMap);
	}
	
	public List searchMaintExcelDown(Map paramMap) throws NkiaException {
		return maintDAO.searchMaintExcelDown(paramMap);
	}
	
	public void insertHis(ModelMap paramMap) throws NkiaException {
		maintDAO.insertHis(paramMap);
	}	
	
	public List searchMaintHis(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintHis(paramMap);
	}
	
	public int searchMaintHisCount(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintHisCount(paramMap);
	}
	
	public List searchMaintTargetList(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintTargetList(paramMap);
	}
	
	public int searchMaintTargetListCount(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintTargetListCount(paramMap);
	}
	
	public List searchMaintTargetAssetList(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintTargetAssetList(paramMap);
	}
	
	public int searchMaintTargetAssetListCount(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintTargetAssetListCount(paramMap);
	}
	
	public List searchMaintTargetAssetSaveList(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintTargetAssetSaveList(paramMap);
	}
	
	public int searchMaintTargetAssetSaveListCount(ModelMap paramMap) throws NkiaException {
		return maintDAO.searchMaintTargetAssetSaveListCount(paramMap);
	}
	
	public void insertMaintTargetAsset(ModelMap paramMap) throws NkiaException {
		
		String maintTargetId = maintDAO.selectMaintTargetId(paramMap);
		paramMap.put("maint_target_id", maintTargetId);
		
		String userId = (String) paramMap.get("login_user_id");
		HashMap assetList = (HashMap) paramMap.get("gridParam");

		maintDAO.insertMaintTarget(paramMap);
		
		for(int i=0; i < assetList.size(); i++){
			String key = String.valueOf(i);
			String assetId = (String) assetList.get(key);
			HashMap assetInfo = new HashMap();
			
			assetInfo.put("asset_id", assetId);
			assetInfo.put("maint_target_id", maintTargetId);
			assetInfo.put("login_user_id", userId);
			
			maintDAO.insertMaintTargetAsset(assetInfo);
		}
	}
	
	public void updateMaintTargetAsset(ModelMap paramMap) throws NkiaException {

		String userId = (String) paramMap.get("login_user_id");
		HashMap assetList = (HashMap) paramMap.get("gridParam");
		
		maintDAO.updateMaintTarget(paramMap);
		maintDAO.deleteMaintTargetAsset(paramMap);
		
		for(int i=0; i < assetList.size(); i++){
			String key = String.valueOf(i);
			String assetId = (String) assetList.get(key);
			HashMap assetInfo = new HashMap();
			
			assetInfo.put("asset_id", assetId);
			assetInfo.put("maint_target_id", paramMap.get("maint_target_id"));
			assetInfo.put("login_user_id", userId);
			
			maintDAO.insertMaintTargetAsset(assetInfo);
		}
	}
	
	public void deleteMaintTargetAsset(ModelMap paramMap) throws NkiaException {
		maintDAO.deleteMaintTargetAsset(paramMap);
		maintDAO.deleteMaintTarget(paramMap);
	}
	
	public List searchMaintTargetExcelDown(Map paramMap) throws NkiaException {
		return maintDAO.searchMaintTargetExcelDown(paramMap);
	}	
	
	/**
	 * 작업기록 등록
	 * @param paramMap
	 * @return
	 */	
	public void insertMaintJob(HashMap paramMap) throws NkiaException {
		
		List assetInfoList = (List)paramMap.get("asset_list");
		String maintJobId = maintDAO.createMaintJobId();
		
		if(assetInfoList != null && assetInfoList.size() > 0) {
			
			for(int i=0; i<assetInfoList.size(); i++) {
				HashMap assetMap = new HashMap();
				HashMap dataMap = new HashMap();
				
				assetMap = (HashMap)assetInfoList.get(i);
				dataMap.putAll(paramMap);
				
				dataMap.put("maint_job_id", maintJobId);
				dataMap.put("asset_id", assetMap.get("ASSET_ID"));
				dataMap.put("conf_id", assetMap.get("CONF_ID"));
				
				maintDAO.insertMaintJob(dataMap);
			}
		}
	}
	
	/**
	 * 작업기록 목록
	 * @param paramMap
	 * @return
	 */	
	public List selectMaintJobListByConfId(HashMap paramMap) throws NkiaException {
		return maintDAO.selectMaintJobListByConfId(paramMap);
	}
	
}