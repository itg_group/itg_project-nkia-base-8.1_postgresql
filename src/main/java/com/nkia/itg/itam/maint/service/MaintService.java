/*
 * @(#)maintService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.maint.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface MaintService {

	public List searchMaint(ModelMap paramMap) throws NkiaException;

	public int searchMaintCount(ModelMap paramMap) throws NkiaException;
	
	public List searchMaintAsset(ModelMap paramMap) throws NkiaException;

	public void insertMaint(ModelMap paramMap) throws NkiaException;
	
	public void updateMaint(ModelMap paramMap) throws NkiaException;
	
	public void deleteMaint(ModelMap paramMap) throws NkiaException, IOException;
	
	public List searchAsset(ModelMap paramMap) throws NkiaException;
	
	public int searchAssetCount(ModelMap paramMap) throws NkiaException;
	
	public void deleteMaintToAsset(ModelMap paramMap) throws NkiaException;
	
	public void insertMaintToAsset(ModelMap paramMap) throws NkiaException;
	
	public List searchMaintExcelDown(Map paramMap) throws NkiaException;
	
	public void insertHis(ModelMap paramMap) throws NkiaException;
	
	public List searchMaintHis(ModelMap paramMap) throws NkiaException;
	
	public int searchMaintHisCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchMaintTargetList(ModelMap paramMap) throws NkiaException;
	
	public int searchMaintTargetListCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchMaintTargetAssetList(ModelMap paramMap) throws NkiaException;
	
	public int searchMaintTargetAssetSaveListCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchMaintTargetAssetSaveList(ModelMap paramMap) throws NkiaException;
	
	public int searchMaintTargetAssetListCount(ModelMap paramMap) throws NkiaException;	
	
	public void insertMaintTargetAsset(ModelMap paramMap) throws NkiaException;
	
	public void updateMaintTargetAsset(ModelMap paramMap) throws NkiaException;
	
	public void deleteMaintTargetAsset(ModelMap paramMap) throws NkiaException;
	
	public List searchMaintTargetExcelDown(Map paramMap) throws NkiaException;
	
	/**
	 * 작업기록 등록
	 * @param paramMap
	 * @return
	 */	
	public void insertMaintJob(HashMap paramMap) throws NkiaException;
	
	/**
	 * 작업기록 목록
	 * @param paramMap
	 * @return
	 */	
	public List selectMaintJobListByConfId(HashMap paramMap) throws NkiaException;
}
