/*
 * @(#)MaintDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.maint.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("maintDAO")
public class MaintDAO extends EgovComAbstractDAO{
	
	public List searchMaint(ModelMap paramMap) throws NkiaException{
		return list("MaintDAO.searchMaint", paramMap);
	}

	public int searchMaintCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("MaintDAO.searchMaintCount", paramMap);
	}
	
	public List searchMaintAsset(ModelMap paramMap) throws NkiaException{
		return list("MaintDAO.searchMaintAsset", paramMap);
	}
	
	public void insertMaint(HashMap maintInfo) throws NkiaException {
		this.insert("MaintDAO.insertMaint", maintInfo);
	}
	
	public void updateMaint(HashMap maintInfo) throws NkiaException {
		this.update("MaintDAO.updateMaint", maintInfo);
	}
	
	public void deleteMaint(ModelMap paramMap) throws NkiaException {
		this.delete("MaintDAO.deleteMaint", paramMap);
	}
	
	public List searchAsset(ModelMap paramMap) throws NkiaException{
		return list("MaintDAO.searchAsset", paramMap);
	}

	public int searchAssetCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("MaintDAO.searchAssetCount", paramMap);
	}
	
	public void deleteMaintToAsset(HashMap maintInfo) throws NkiaException {
		delete("MaintDAO.deleteMaintToAsset", maintInfo);
	}
	
	public void insertMaintToAsset(HashMap assetInfo) throws NkiaException {
		insert("MaintDAO.insertMaintToAsset", assetInfo);
	}
	
	public List searchMaintExcelDown(Map paramMap) throws NkiaException {
		return list("MaintDAO.searchMaintExcelDown", paramMap);
	}
	
	public void insertHis(ModelMap paramMap) throws NkiaException {
		insert("MaintDAO.insertHis", paramMap);
	}	
	
	public List searchMaintHis(ModelMap paramMap) throws NkiaException{
		return list("MaintDAO.searchMaintHis", paramMap);
	}
	
	public int searchMaintHisCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("MaintDAO.searchMaintHisCount", paramMap);
	}	
	
	public List searchMaintTargetList(ModelMap paramMap) throws NkiaException{
		return list("MaintDAO.searchMaintTargetList", paramMap);
	}
	
	public int searchMaintTargetListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("MaintDAO.searchMaintTargetListCount", paramMap);
	}	
	
	public List searchMaintTargetAssetList(ModelMap paramMap) throws NkiaException{
		return list("MaintDAO.searchMaintTargetAssetList", paramMap);
	}
	
	public int searchMaintTargetAssetListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("MaintDAO.searchMaintTargetAssetListCount", paramMap);
	}	
	
	public List searchMaintTargetAssetSaveList(ModelMap paramMap) throws NkiaException{
		return list("MaintDAO.searchMaintTargetAssetSaveList", paramMap);
	}
	
	public int searchMaintTargetAssetSaveListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("MaintDAO.searchMaintTargetAssetSaveListCount", paramMap);
	}	

	public int selectMaintToAssetByAssetCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("MaintDAO.selectMaintToAssetByAssetCount", paramMap);
	}	
	
	public void insertMaintTarget(ModelMap paramMap) throws NkiaException {
		insert("MaintDAO.insertMaintTarget", paramMap);
	}	
	
	public void updateMaintTarget(ModelMap paramMap) throws NkiaException {
		update("MaintDAO.updateMaintTarget", paramMap);
	}	
	
	public void insertMaintTargetAsset(HashMap assetInfo) throws NkiaException {
		insert("MaintDAO.insertMaintTargetAsset", assetInfo);
	}	
	
	public String selectMaintTargetId(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (String)selectByPk("MaintDAO.selectMaintTargetId", paramMap);
	}
	
	public void updateMaintTargetAsset(ModelMap paramMap) throws NkiaException {
		update("MaintDAO.updateMaintTargetAsset", paramMap);
	}
	
	public void deleteMaintTarget(ModelMap paramMap) throws NkiaException {
		delete("MaintDAO.deleteMaintTarget", paramMap);
	}
	
	public void deleteMaintTargetAsset(ModelMap paramMap) throws NkiaException {
		delete("MaintDAO.deleteMaintTargetAsset", paramMap);
	}
	
	public List searchMaintTargetExcelDown(Map paramMap) throws NkiaException {
		return list("MaintDAO.searchMaintTargetAssetSaveList", paramMap);
	}	
	
	/**
	 * 작업기록 ID생성
	 * @return
	 * @throws NkiaException
	 */
	public String createMaintJobId()throws NkiaException{
		return (String)selectByPk("MaintDAO.createMaintJobId", null);
	}
	/**
	 * 작업기록 등록
	 * @param paramMap
	 * @return
	 */	
	public void insertMaintJob(HashMap paramMap) throws NkiaException {
		this.insert("MaintDAO.insertMaintJob", paramMap);
	}
	
	/**
	 * 작업기록 목록
	 * @param paramMap
	 * @return
	 */	
	public List selectMaintJobListByConfId(HashMap paramMap) throws NkiaException {
		return list("MaintDAO.selectMaintJobListByConfId", paramMap);
	}
}