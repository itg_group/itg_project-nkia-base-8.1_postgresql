package com.nkia.itg.itam.opms.web;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.opms.service.OpmsListService;

@Controller
public class OpmsListController {
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsListController.class);
	
	@Resource(name = "opmsListService")
	private OpmsListService opmsListService;
	
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/amdb/opmsMainTab.do")
	public String opmsMainTab(ModelMap paramMap) throws NkiaException {
		return "/itg/itam/opms/opmsMainTab.nvf";
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/goOpmsMainPage.do")
	public String goOpmsMainPage(ModelMap paramMap) throws NkiaException {
		return "/itg/itam/opms/opmsMainPage.nvf";
	}
	
}
