package com.nkia.itg.itam.opms.service.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.itam.opms.dao.OpmsListDAO;
import com.nkia.itg.itam.opms.service.OpmsListService;

@Service("opmsListService")
public class OpmsListServiceImpl implements OpmsListService{
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsListServiceImpl.class);
	
	@Resource(name="opmsListDAO")
	private OpmsListDAO omsListDAO;
}
