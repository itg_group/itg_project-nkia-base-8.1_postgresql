
/**
 * 
 * @version 1.0 2013. 11. 18.
 * @author <a href="mailto:gunbum@nkia.co.kr"> Gun-bum Kim
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
package com.nkia.itg.itam.opms.service.impl;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;
import com.nkia.itg.itam.opms.dao.OpmsVmDAO;
import com.nkia.itg.itam.opms.service.OpmsVmService;
import com.nkia.itg.itam.opms.util.OpmsUtil;

@Service("opmsVmService")
public class OpmsVmServiceImpl implements OpmsVmService{

	@Resource(name="opmsUtil")
	private OpmsUtil opmsUtil;
	
	@Resource(name="opmsVmDAO")
	private OpmsVmDAO opmsVmDAO;
	
	@Resource(name="opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;
	
	/**
	 * 
	 * TODO vm등록
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public boolean registVmAsset(HashMap vmDataMap) throws NkiaException {
		try{
		String asset_id = StringUtil.parseString("asset_id");
		
		//자산아이디로 물리장비의 기본 정보를 가져온다.
		HashMap assetBaseInfo = opmsUtil.replaceRowerKey(opmsVmDAO.selectPhysiInfoByAssetId(vmDataMap));
		//conf_id를 새로 생성한다.
		String conf_id = opmsDetailDAO.createConfId(assetBaseInfo);
		assetBaseInfo.put("conf_id", conf_id);
		//데이터를 assetBaseInfo 로 동기화한다.
		opmsUtil.paramSync(vmDataMap, assetBaseInfo);
		
		//am_infra 등록
		opmsVmDAO.insertVmAmInfra(assetBaseInfo);
		//am_sv_logic 등록
		opmsVmDAO.insertVmAmSvLogic(assetBaseInfo);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return true;
	}

	/**
	 * 
	 * TODO vm 정보수정
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public boolean updateVmInfo(HashMap vmDataMap) throws NkiaException {
		
		try{
			//am_infra 수정
			opmsVmDAO.updateVmAmInfra(vmDataMap);
			//am_sv_logic 수정
			opmsVmDAO.updateVmAmSvLogic(vmDataMap);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return true;
	}

	/**
	 * 
	 * TODO vm상태수정
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public boolean updateVmInfoState(HashMap vmDataMap) throws NkiaException {
		try{
			//am_infra 수정
			opmsVmDAO.updateVmAmInfra(vmDataMap);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return true;
	}
}
