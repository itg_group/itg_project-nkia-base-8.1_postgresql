/*
 * @(#)OpmsTreeVO.java              2013. 10. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.opms.vo;

import com.nkia.itg.base.vo.TreeVO;

public class OpmsTreeVO extends TreeVO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String logical_yn;
	private String class_type;
	private String durable_year;
	private String conf_type_grp;	//hmsong cmdb 개선 작업 : 차후 구성용도의 그룹핑을 위해 만들어놓음
	
	public void setLogical_yn(String logical_yn) {
		this.logical_yn = logical_yn;
	}
	
	public String getLogical_yn() {
		return logical_yn;
	}
	
	public void setClass_type(String class_type) {
		this.class_type = class_type;
	}
	
	public String getClass_type() {
		return class_type;
	}
	
	public void setDurable_year(String durable_year) {
		this.durable_year = durable_year;
	}
	
	public String getDurable_year() {
		return durable_year;
	}

	//hmsong cmdb 개선 작업 : 차후 구성용도의 그룹핑을 위해 만들어놓음 Start
	public String getConf_type_grp() {
		return conf_type_grp;
	}

	public void setConf_type_grp(String conf_type_grp) {
		this.conf_type_grp = conf_type_grp;
	}
	//hmsong cmdb 개선 작업 : 차후 구성용도의 그룹핑을 위해 만들어놓음 End

}
