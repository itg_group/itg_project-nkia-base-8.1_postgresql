package com.nkia.itg.itam.opms.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;
import com.nkia.itg.itam.opms.dao.OpmsInterfaceDAO;
import com.nkia.itg.itam.opms.service.OpmsInterfaceService;
import com.nkia.itg.itam.opms.util.OpmsUtil;

@Service("opmsInterfaceService")
public class OpmsInterfaceServiceImpl implements OpmsInterfaceService{

	@Resource(name="opmsUtil")
	private OpmsUtil opmsUtil;
	
	@Resource(name="opmsInterfaceDAO")
	private OpmsInterfaceDAO opmsInterfaceDAO;
	
	@Resource(name="opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;
	
	/**
	 * 
	 * Xeus -> ITG 가상화 등록
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public boolean registVmAssetFromXeus(HashMap vmDataMap) throws NkiaException {
		try{
		String asset_id = StringUtil.parseString("asset_id");
		
		//자산아이디로 물리장비의 기본 정보를 가져온다.
		HashMap assetBaseInfo = opmsUtil.replaceRowerKey(opmsInterfaceDAO.selectPhysiInfoByAssetId(vmDataMap));
		//conf_id를 새로 생성한다.
		String conf_id = opmsDetailDAO.createConfId(assetBaseInfo);
		assetBaseInfo.put("conf_id", conf_id);
		//데이터를 assetBaseInfo 로 동기화한다.
		opmsUtil.paramSync(vmDataMap, assetBaseInfo);
		
		//am_infra 등록
		opmsInterfaceDAO.insertVmAmInfra(assetBaseInfo);
		//am_sv_logic 등록
		opmsInterfaceDAO.insertVmAmSvLogic(assetBaseInfo);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return true;
	}

	/**
	 * 
	 * Xeus -> ITG 가상화 스펙변경
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public boolean updateVmInfoFromXeus(HashMap vmDataMap) throws NkiaException {
		
		try{
			//am_infra 수정
			opmsInterfaceDAO.updateVmAmInfra(vmDataMap);
			//am_sv_logic 수정
			opmsInterfaceDAO.updateVmAmSvLogic(vmDataMap);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return true;
	}

	/**
	 * 
	 * Xeus -> ITG 가상화 상태변경
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public boolean updateVmInfoStateFromXeus(HashMap vmDataMap) throws NkiaException {
		try{
			//am_infra 수정
			opmsInterfaceDAO.updateVmAmInfra(vmDataMap);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return true;
	}
	
	/**
	 * 
	 * EMS -> ITG 자산등록
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void registPhysiInfoAssetFromEms(HashMap paramMap) throws NkiaException {
		
		//분류체계타입 조회
		String class_type = opmsInterfaceDAO.selectClassType(paramMap);
		paramMap.put("class_type", class_type);
			
		String asset_id = opmsDetailDAO.createAssetId(paramMap);
		String conf_id = opmsDetailDAO.createConfId(paramMap);
		
		paramMap.put("asset_id", asset_id);
		paramMap.put("conf_id", conf_id);
		
		HashMap extraParamMap = (HashMap) paramMap.get("extra_params");
		extraParamMap.put("asset_id", asset_id);
		extraParamMap.put("conf_id", conf_id);
		
		//AM_ASSET 등록
		opmsInterfaceDAO.insertAmAsset(paramMap);
		//AM_INFRA 등록
		opmsInterfaceDAO.insertPhysiAmInfra(paramMap);
	}
	
	/**
	 * 
	 * ITAM VM 목록
	 * 
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchVmList() throws NkiaException {
		return opmsInterfaceDAO.searchVmList();
	}
}
