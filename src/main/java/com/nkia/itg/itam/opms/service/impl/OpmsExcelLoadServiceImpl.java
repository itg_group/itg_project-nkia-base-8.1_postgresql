package com.nkia.itg.itam.opms.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;
import com.nkia.itg.itam.opms.dao.OpmsExcelLoadDAO;
import com.nkia.itg.itam.opms.service.OpmsExcelLoadService;

@Service("opmsExcelLoadService")
public class OpmsExcelLoadServiceImpl implements OpmsExcelLoadService{
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsExcelLoadServiceImpl.class);
	public static final String hyphen = "-";
	
	@Resource(name = "opmsExcelLoadDAO")
	private OpmsExcelLoadDAO opmsExcelLoadDAO;
	
	@Resource(name = "opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;

	@Override
	public void insertExcelData(List<HashMap> resultMap, ModelMap formData) throws NkiaException {
		HashMap params = formData;
		params.put("class_type", "XL");
		String conf_temp_id = opmsDetailDAO.createConfTempId(params);
		String asset_temp_id = opmsDetailDAO.createAssetTempId(params);
		
		params.put("conf_temp_id", conf_temp_id);			// 임시구성ID 생성
		params.put("asset_temp_id", asset_temp_id);			// 임시자산ID 생성
		params.put("status", "REQUEST");
		opmsDetailDAO.insertOpmsInfoMapping(params);		// mapping table Insert
		
		
		HashMap confNameCode = new HashMap();
		for(int i=0; i < resultMap.size(); i++){
			HashMap rowData = new HashMap();
			rowData = resultMap.get(i);
			
			rowData.put("conf_temp_id", conf_temp_id);			// 임시구성ID 생성
			rowData.put("asset_temp_id", asset_temp_id);
			
			if(rowData.get("ins_table_nm").equals("AM_CONF_NM_CODE")){
				confNameCode.put(rowData.get("ins_column_id"), rowData.get("value"));
			}else{
				opmsDetailDAO.insertOpmsInfoData(rowData);					
			}
		}
	}
	
	public String createTempSeq() throws NkiaException {
		return opmsDetailDAO.createTempSeq(null);
	}
	
	public void insertOpmsInfoSeq(ModelMap paramMap) throws NkiaException {
		opmsDetailDAO.insertOpmsInfoSeq(paramMap);
	}	

	@Override
	public void insertLogicExcelData(List<HashMap> resultMap) throws NkiaException {
		HashMap dataMap = new HashMap();
		dataMap.put("class_type", "SV");
		
		String logicConfId = opmsDetailDAO.createConfId(dataMap);
		
		HashMap infraData = new HashMap();			
		HashMap svLogicData = new HashMap();
		
		infraData.put("CONF_ID", logicConfId);
		infraData.put("TANGIBLE_ASSET_YN", "N");
		
		svLogicData.put("CONF_ID", logicConfId);
		
		String PHYSI_CONF_ID = null; 
		
		for(int i=0; i < resultMap.size(); i++){
			HashMap rowData = resultMap.get(i);
			if(rowData.get("ins_table_nm").equals("AM_INFRA")){
				infraData.put(rowData.get("col_id"), rowData.get("value"));
			}else if(rowData.get("ins_table_nm").equals("AM_SV_LOGIC")){
				svLogicData.put(rowData.get("col_id"), rowData.get("value"));					
				if("PHYSI_CONF_ID".equals(rowData.get("col_id"))){
					PHYSI_CONF_ID = (String) rowData.get("value");
				}
			}
		}

		List<HashMap> physi = (List<HashMap>) opmsExcelLoadDAO.searchPhysiData(PHYSI_CONF_ID);
		
		HashMap physiData = physi.get(0);
		infraData.put("ASSET_ID", physiData.get("ASSET_ID"));
		infraData.put("CLASS_ID", physiData.get("CLASS_ID"));
		infraData.put("CLASS_TYPE", physiData.get("CLASS_TYPE"));
		infraData.put("CONF_TYPE", physiData.get("CONF_TYPE"));
		
		opmsExcelLoadDAO.insertLogicInfraData(infraData);
		opmsExcelLoadDAO.insertSVLogicData(svLogicData);
	}
}
