/*
 * @(#)OpmsInterfaceDAO.java              2013. 11. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.opms.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Component("opmsInterfaceDAO")
public class OpmsInterfaceDAO extends EgovComAbstractDAO{

	/**
	 * 
	 * TODO 자산아이디로 물리장비의  기본정보를 가져온다. 
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectPhysiInfoByAssetId(HashMap dataMap)throws NkiaException{
		return (HashMap)selectByPk("OpmsInterfaceDAO.selectPhysiInfoByAssetId", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_INFRA 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertVmAmInfra(HashMap dataMap)throws NkiaException{
		insert("OpmsInterfaceDAO.insertVmAmInfra", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_SV_LOGIC 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertVmAmSvLogic(HashMap dataMap)throws NkiaException{
		insert("OpmsInterfaceDAO.insertVmAmSvLogic", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_INFRA 에 수정
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateVmAmInfra(HashMap dataMap)throws NkiaException{
		update("OpmsInterfaceDAO.updateVmAmInfra", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_SV_LOGIC 에수정
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateVmAmSvLogic(HashMap dataMap)throws NkiaException{
		update("OpmsInterfaceDAO.updateVmAmSvLogic", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_ASSET 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertAmAsset(HashMap dataMap)throws NkiaException{
		insert("OpmsInterfaceDAO.insertAmAsset", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_INFRA 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertPhysiAmInfra(HashMap dataMap)throws NkiaException{
		insert("OpmsInterfaceDAO.insertPhysiAmInfra", dataMap);
	} 
	
	/**
	 * 
	 * TODO 자산 AM_INFRA 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */	
	public String selectClassType(HashMap paramMap) throws NkiaException {
		return (String) selectByPk("OpmsInterfaceDAO.selectClassType",paramMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_SV_PHYSI 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertVmAmSvPhysi(HashMap dataMap)throws NkiaException{
		insert("OpmsInterfaceDAO.insertVmAmSvPhysi", dataMap);
	}
	
	/**
	 * 
	 * ITAM 자산등록
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List searchVmList()throws NkiaException{
		return list("opmsVmDAO.searchVmList", null);
	}
}
