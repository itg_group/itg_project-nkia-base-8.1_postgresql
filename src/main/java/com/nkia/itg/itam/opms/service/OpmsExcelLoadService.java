package com.nkia.itg.itam.opms.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface OpmsExcelLoadService {

	public void insertExcelData(List<HashMap> excelData, ModelMap formData) throws NkiaException;

	public String createTempSeq() throws NkiaException;

	public void insertOpmsInfoSeq(ModelMap formData) throws NkiaException;
	
	public void insertLogicExcelData(List<HashMap> excelData) throws NkiaException;
	
}
