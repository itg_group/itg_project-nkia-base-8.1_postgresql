package com.nkia.itg.itam.opms.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("opmsExcelLoadDAO")
public class OpmsExcelLoadDAO extends EgovComAbstractDAO{
	
	// 엑셀 데이터 ROW 별로 INSERT
	public void insertExcelData(HashMap resultMap) throws NkiaException {
		this.insert("OpmsExcelLoadDAO.insertExcelData", resultMap);
	}

	// 분류체계ID 로 분류체계 타입을 구해온다.
	public String selectClassType(HashMap resultMap)throws NkiaException{
		return (String)selectByPk("OpmsExcelLoadDAO.selectClassType", resultMap);
	}
	
	// 
	public List searchPhysiData(String PHYSI_CONF_ID)throws NkiaException{
		return list("OpmsExcelLoadDAO.searchPhysiData",PHYSI_CONF_ID);
	}
	
	// AM_INFRA table Data Insert
	public void insertLogicInfraData(HashMap paramMap)throws NkiaException{
		insert("OpmsExcelLoadDAO.insertLogicInfraData",paramMap);
	}
	
	// AM_SV_LOGIC table Data Insert
	public void insertSVLogicData(HashMap paramMap)throws NkiaException{
		insert("OpmsExcelLoadDAO.insertSVLogicData",paramMap);
	}
	
}

