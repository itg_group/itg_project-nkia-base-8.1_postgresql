/*
 * @(#)OpmsVmDAO.java              2013. 11. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.opms.dao;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Component("opmsVmDAO")
public class OpmsVmDAO extends EgovComAbstractDAO{

	/**
	 * 
	 * TODO 자산아이디로 물리장비의  기본정보를 가져온다. 
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectPhysiInfoByAssetId(HashMap dataMap)throws NkiaException{
		return (HashMap)selectByPk("opmsVmDAO.selectPhysiInfoByAssetId", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_INFRA 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertVmAmInfra(HashMap dataMap)throws NkiaException{
		insert("opmsVmDAO.insertVmAmInfra", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_SV_LOGIC 에 등록
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertVmAmSvLogic(HashMap dataMap)throws NkiaException{
		insert("opmsVmDAO.insertVmAmSvLogic", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_INFRA 에 수정
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateVmAmInfra(HashMap dataMap)throws NkiaException{
		update("opmsVmDAO.updateVmAmInfra", dataMap);
	}
	
	/**
	 * 
	 * TODO 자산 AM_SV_LOGIC 에수정
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateVmAmSvLogic(HashMap dataMap)throws NkiaException{
		update("opmsVmDAO.updateVmAmSvLogic", dataMap);
	}
}
