/*
 * @(#)OpmsVmService.java              2013. 11. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.opms.service;

import java.util.HashMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface OpmsVmService {
	
	/**
	 * 
	 * TODO vm등록
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	public boolean registVmAsset(HashMap vmDataMap)throws NkiaException;
	
	/**
	 * 
	 * TODO vm 정보수정
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	public boolean updateVmInfo(HashMap vmDataMap)throws NkiaException;
	
	/**
	 * 
	 * TODO vm상태수정
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	public boolean updateVmInfoState(HashMap vmDataMap)throws NkiaException;
}
