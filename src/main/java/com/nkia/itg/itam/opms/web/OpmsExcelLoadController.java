package com.nkia.itg.itam.opms.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.opms.service.OpmsExcelLoadService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.EgovWebUtil;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class OpmsExcelLoadController {
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsExcelLoadController.class);

	
	@Resource(name = "opmsExcelLoadService")
	private OpmsExcelLoadService opmsExcelLoadService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 자산 엑셀정보 등록화면 로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/opms/opmsExcelLoad.do")
	public String opmsExcelLoadManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/opms/opmsExcelLoad.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * 정보수집서 - 엑셀업로드
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws JSONException 
	 * @
	 */
	@RequestMapping(value = "/itg/itam/opms/assetExcelDataLoad.do")
	public void assetExcelDataLoad(HttpServletRequest request, HttpServletResponse response)throws NkiaException, IOException, JSONException  {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
        
        
		try {
			//Globals.FileStore.Path
			String file_upload_deault_path = BaseConstraints.getFileUploadDefaultPath();
			// 파일등록
			MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
			
			String project_no = mptRequest.getParameter("project_no");
			String info_title = mptRequest.getParameter("info_title");

			String tempSeqId = opmsExcelLoadService.createTempSeq();
			
			ModelMap formData = new ModelMap(); 
			
			formData.put("project_no", project_no);
			formData.put("title", info_title);
			formData.put("temp_seq", tempSeqId);
			formData.put("info_type", "EXCEL");
			
			try {
			    File cFile = new File(EgovWebUtil.filePathBlackList(file_upload_deault_path));
			    
			    cFile.setExecutable(false, true);
			    cFile.setReadable(true);
			    cFile.setWritable(false, true);
			    
			    if (!cFile.exists()){
			    	cFile.mkdir();
			    }
			} catch (Exception e) {
				throw new NkiaException(e);
			}
			
			Map result = null;
			Iterator<String> fileIter = mptRequest.getFileNames();
			String fileName = null;
			while (fileIter.hasNext()) {
				fileName = (String) fileIter.next();
				MultipartFile mFile = mptRequest.getFile( fileName );
				if (mFile.getSize() > 0) {
					// 개별 최대 파일 Check
					result = EgovFileMngUtil.uploadFile(mFile, file_upload_deault_path);
				}
			}
			
			String uploadFilePath = file_upload_deault_path;
			if (result != null) {
				String uploadFileName = (String) result.get("uploadFileName");
				String uploadDir = uploadFileName.substring(0, 6) + "/";
				
				//저장된 파일경로 설정
				uploadFilePath = file_upload_deault_path + uploadDir + uploadFileName;
			}
			
			XSSFWorkbook work = new XSSFWorkbook(new FileInputStream(uploadFilePath));
			int sheetNum = work.getNumberOfSheets();
			
			// 사용자 정보 조회 후 SEQ 테이블 데이터에 삽입
			String user_id = "";
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(); 
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		user_id = userVO.getUser_id();
	    		formData.put("user_id", user_id);
	    	}
	    	// SEQ 테이블 데이터 INSERT
	    	opmsExcelLoadService.insertOpmsInfoSeq(formData);
	    	
			// 첫번째 시트를 불러온다.
			//for(int s=0; s < 5; s++){
				XSSFSheet sheet = work.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();				
		
			//	for(int i=2; i < rows; i++){
				for(int i=4; i < rows; i++){
					
					List<HashMap> excelData = new ArrayList<HashMap>();
					
					XSSFRow row = sheet.getRow(i);
					XSSFRow colRow = sheet.getRow(1);
					
					if(row != null){
						int cells = row.getLastCellNum();
						//String cellValue = "";
						//분류체계가 선택이 안되어있으면 수행하지 않음.
						XSSFCell class_name = row.getCell(0);
						
						if(class_name != null){
							for(int j=1; j < cells; j++){
								ModelMap map = new ModelMap();
								XSSFCell cell = row.getCell(j);
								XSSFCell colInfo = colRow.getCell(j);
								String colSt = null;
								String tableName = null;
								String colName	 = null;
								
								if( colInfo != null ){
									colSt = colInfo.getStringCellValue();
									if(!"".equals(colSt)){
									//	tableName = colSt.split("__")[0];
									//	colName	 = colSt.split("__")[1];
										
										tableName = colSt.split("@")[0];
										colName	 = colSt.split("@")[1];
										
										map.put("ins_table_nm", tableName);
										if(cell != null){
											if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC){
												if(HSSFDateUtil.isCellDateFormatted(cell)){
													SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
													//cellValue = formatter.format(cell.getDateCellValue());
													map.put("ins_column_id", colName);
													map.put("value", formatter.format(cell.getDateCellValue()));
												}else{
													int dVal = (int) cell.getNumericCellValue();
													map.put("ins_column_id", colName);
													map.put("value", dVal);
												}
											}else{
												//cellValue = cell.getStringCellValue();
												map.put("ins_column_id", colName);
												map.put("value", cell.getStringCellValue());									
											}
											map.put("user_id", user_id);
											map.put("ins_column_nm", "");
											excelData.add(map);
										}
									}
								}
							}
							if( excelData.size() > 0 && !excelData.get(0).get("value").equals("")){
								opmsExcelLoadService.insertExcelData(excelData, formData);
							}
							
						}else{
							break;
						}
						
					}
				}
			//}
			//sets success to true
			returnData.put("success", true);
			returnData.put("resultMsg", "엑셀업로드가 완료되었습니다.");
	        
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			returnData.put("resultMsg", "엑셀업로드에 실패하였습니다.");
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
			throw new NkiaException(e);
		}
	}
	
	
	/**
	 * 
	 * 논리 가상화 등록 - 엑셀 업로드
	 * @param request
	 * @param response
	 * @
	 */
	@RequestMapping(value = "/itg/itam/opms/logicExcelDataLoad.do")
	public @ResponseBody ResultVO logicExcelDataLoad(HttpServletRequest request, HttpServletResponse response) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			//Globals.FileStore.Path
			String file_upload_deault_path = BaseConstraints.getFileUploadDefaultPath();
			
			// 파일등록
			MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;

		    File cFile = new File(EgovWebUtil.filePathBlackList(file_upload_deault_path));
		    
		    cFile.setExecutable(false, true);
		    cFile.setReadable(true);
		    cFile.setWritable(false, true);
		    
		    if (!cFile.exists()){
		    	cFile.mkdir();
		    }

			Map result = null;
			Iterator<String> fileIter = mptRequest.getFileNames();
			String fileName = null;
			while (fileIter.hasNext()) {
				fileName = (String) fileIter.next();
				MultipartFile mFile = mptRequest.getFile( fileName );
				if (mFile.getSize() > 0) {
					// 개별 최대 파일 Check
					result = EgovFileMngUtil.uploadFile(mFile, file_upload_deault_path);
				}
			}
			
			String uploadFilePath = file_upload_deault_path;
			if (result != null) {
				String uploadFileName = (String) result.get("uploadFileName");
				String uploadDir = uploadFileName.substring(0, 6) + "/";
				
				//저장된 파일경로 설정
				uploadFilePath = file_upload_deault_path + uploadDir + uploadFileName;
			}

			XSSFWorkbook work = new XSSFWorkbook(new FileInputStream(uploadFilePath));
			int sheetNum = work.getNumberOfSheets();

			// 첫번째 시트를 불러온다.
			XSSFSheet sheet = work.getSheetAt(0);
			int rows = sheet.getPhysicalNumberOfRows();				
	
			for(int i=2; i < rows; i++){
				
				List<HashMap> excelData = new ArrayList<HashMap>();
				
				XSSFRow row = sheet.getRow(i);
				XSSFRow colRow = sheet.getRow(1);
				
				if(row != null){
					int cells = row.getLastCellNum();
					//String cellValue = "";
					//물리서버 ID가 입력되지 않으면 수행하지않는다.
					XSSFCell physiID = row.getCell(0);
					if(physiID != null){
						
						for(int j=0; j < cells; j++){
							
							ModelMap map = new ModelMap();
							
							XSSFCell cell = row.getCell(j);
							XSSFCell colInfo = colRow.getCell(j);
							String colSt = null;
							String tableName = null;
							String colName	 = null;
							
							if( colInfo != null ){

								colSt = colInfo.getStringCellValue();
								
								if(!"".equals(colSt)){
									tableName = colSt.split("__")[0];
									colName	 = colSt.split("__")[1];										
									map.put("ins_table_nm", tableName);

									if(cell != null){
										if(cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC){
											if(HSSFDateUtil.isCellDateFormatted(cell)){
												SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
												//cellValue = formatter.format(cell.getDateCellValue());
												map.put("col_id", colName);
												map.put("value", formatter.format(cell.getDateCellValue()));
											}else{
												int dVal = (int) cell.getNumericCellValue();
												map.put("col_id", colName);
												map.put("value", dVal);
											}
										}else{
											//cellValue = cell.getStringCellValue();
											map.put("col_id", colName);
											map.put("value", cell.getStringCellValue());									
										}											
										excelData.add(map);
									}
								}
							}
						}
						opmsExcelLoadService.insertLogicExcelData(excelData);
					}				
				}
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
}
