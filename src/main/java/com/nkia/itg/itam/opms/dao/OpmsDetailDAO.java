package com.nkia.itg.itam.opms.dao;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("opmsDetailDAO")
public class OpmsDetailDAO extends EgovComAbstractDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsDetailDAO.class);
	
	/**
	 * 분류별 속성 정보 (사용자 권한과 분류해당 item들을 가져온다.)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectClassEntityInfo(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectClassEntityInfo",paramMap);
	}
	
	/**
	 * 논리서버 연관 장비 정보 등록
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertLogSvRelConf(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertLogSvRelConf",paramMap);
	}
	/**
	 * 
	 * TODO 
	 * 사용자 지정에 해당되는 속성(컬럼정보) 들을 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectManualAttrList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectManualAttrList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 사용자의 해당 item들에 대한 권한을 체크한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectCompAuthList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectCompAuthList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 사용자 지정 중에서 권한 설정을 전체 읽기나 전체 수정으로 한경우
	 * 해당 item의 속성정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectMnualAttrListByType(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectMnualAttrListByType",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 컴포넌트중 연계정보 그룹 정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsCompSetupList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsCompSetupList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 컴포넌트중 유지보수 정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectMaintFormDataList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectMaintFormDataList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 설치정보 리스트를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsCompInstallList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsCompInstallList",paramMap);
	}
	
	/**
	 * 선택된 담당자 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelMngUserList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsSelMngUserList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 담당자 선택 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsMngUserCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsMngUserCnt",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsMngUserList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsMngUserList",paramMap);
	}
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		return list("opmsDetailDAO.searchCodeDataList", paramMap);
	}
	
	/**
	 * 자자산 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelChildList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsSelChildList",paramMap);
	}
	
	/**
	 * 구매정보 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelPurInfoList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsSelPurInfoList",paramMap);
	}
	/**
	 * 구매정보 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsDepreciationInfoList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsDepreciationInfoList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 선택된 사용자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelUserList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsSelUserList",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 사용자 선택 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsUserCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsUserCnt",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 사용자 선택 팝업 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsUserList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsUserList",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 사용자 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteUser(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.deleteUser",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 사용자 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertUser(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertUser",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 소프트웨어 라이센스 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSwLicenceList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsSwLicenceList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 소프트웨어 라이센스 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateSwLicence(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.updateSwLicence",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 이중화 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelHaGrpList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsRelHaGrpList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 논리 연계장비 정보
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsReLogConfList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsReLogConfList",paramMap);
	}
	
	/**
	 * 연관 장비 정보 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelConfList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsRelConfList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 구성 분류별 트리가 나온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelConfClassTree(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectRelConfClassTree",paramMap);
	}
	
	/**
	 * 연계 장비 정보 팝업 리스트 카운트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsConfCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsConfCnt",paramMap);
	}

	/**
	 * 
	 * TODO
	 * 연계장비 정보 팝업 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsConfList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("opmsDetailDAO.selectOpmsConfList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 연관 서비스 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("opmsDetailDAO.selectOpmsRelServiceList",paramMap);
	}
	
	/**
	 * 연관 서비스 선택 팝업 리스트 카운트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsServiceCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsServiceCnt",paramMap);
	}

	/**
	 * 연관 서비스 선택 팝업 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("opmsDetailDAO.selectOpmsServiceList",paramMap);
	}
	
	/**
	 * 연관 서비스 리스트 팝업 부서 검색조건 콤보 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectServiceCustList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("opmsDetailDAO.selectServiceCustList",paramMap);
	}
	
	/**
	 * 테이블 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTblList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("opmsDetailDAO.selectTblList",paramMap);
	}
	
	/**
	 * 조인 정보
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectJoinList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("opmsDetailDAO.selectJoinList",paramMap);
	}
	
	/**
	 * hashMap 리턴타입 쿼리 실행
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public HashMap excuteQuery(String query)throws NkiaException{
		return (HashMap)selectByPk("opmsDetailDAO.excuteQuery",query);
	}
	
	/**
	 * 수정 권한 갯수
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectUpdateAuthCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectUpdateAuthCnt",paramMap);	
	}

	/**
	 * 가져오고싶은 타입 별로 컬럼을 가져온다.
	 * SKEYID	: 여러테이블 중에서 검색조건 ..  conf_id,asset_id를 가져온다.
	 * UKEYID	: 해당 테이블 중에서 검색조건 ..  conf_id,asset_id를 가져온다.
	 * UPDID	: 디폴트로 들어가는 컬럼 upd_user_id,upd_dt 존재 여부 판단 컬럼을 가져온다.
	 * COL_ID 	: 실재 스크마 정보에서 존재하는 컬럼들만을 가져온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTypeColumn(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectTypeColumn",paramMap);
	}

	/**
	 * 새로 생성된 수정테이블 seq 정보를 입력한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
//	public void insertChgHisSeq(HashMap paramMap)throws NkiaException{
//		insert("opmsDetailDAO.insertChgHisSeq",paramMap);
//	}
	
	/**
	 * 이력정보 테이블의 id를 생성한다. 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectAssetChgHisSeq(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.selectAssetChgHisSeq",paramMap);
	}
	
	/**
	 * 속성 이력정보를 입력한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
//	public void insertAmAssetChgHis(HashMap paramMap)throws NkiaException{
//		insert("opmsDetailDAO.insertAmAssetChgHis",paramMap);
//	}
	
	/**
	 * 속성이력정보에 같은 속성 카운트를 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
//	public int selectAmAssetChgHisCnt(HashMap paramMap)throws NkiaException{
//		return (Integer)selectByPk("opmsDetailDAO.selectAmAssetChgHisCnt",paramMap);
//	}
	
	/**
	 * 파일 id를 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectatchFileId(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.selectatchFileId",paramMap);
	}
	
	/**
	 * 파일 id를 수정한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateAtchFileId(HashMap paramMap)throws NkiaException{
		update("opmsDetailDAO.updateAtchFileId",paramMap);
	}
	
	/**
	 * 담당자 정보 이력테이블에 입력한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
//	public void insertOperHist(HashMap paramMap)throws NkiaException{
//		insert("opmsDetailDAO.insertOperHist",paramMap);
//	}
	
	/**
	 * 담당자 정보를 등록한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertOper(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertOper",paramMap);
	}
	/**
	 * 담당자 정보를 등록한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateCust(HashMap dataMap)throws NkiaException{
		update("opmsDetailDAO.updateCust", dataMap);
	}
	
	
	/**
	 * 담당자를 삭제한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteOper(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.deleteOper",paramMap);
	}
	
	/**
	 * 연관 서비스 이력정보를 입략힌다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
//	public void insertServiceHist(HashMap paramMap)throws NkiaException{
//		delete("opmsDetailDAO.insertServiceHist",paramMap);
//	}
	
	/**
	 * 연관 서비스 입략힌다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertService(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertService",paramMap);
	}
	
	/**
	 * 연관서비스 정보 삭제
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteService(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.deleteService",paramMap);
	}
	
	/**
	 * 이중화 이력정보를 등록한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertHaHist(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.insertHaHist",paramMap);
	}
	
	/**
	 * 논리 수정 이력정보를 넣는다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
//	public void insertLogSvHist(HashMap paramMap)throws NkiaException{
//		delete("opmsDetailDAO.insertLogSvHist",paramMap);
//	}
	
	/**
	 * 연관장비 정보 이력을 넣는다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
//	public void insertRelConfHist(HashMap paramMap)throws NkiaException{
//		delete("opmsDetailDAO.insertRelConfHist",paramMap);
//	}
//	
	/**
	 * 연관 장비 정보 삭제
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteRelConf(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.deleteRelConf",paramMap);
	}
	
	/**
	 * 연관 장비 정보 등록
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertRelConf(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertRelConf",paramMap);
	}
	
	/**
	 * 연관 장비 사용률 등록
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateRelConfUseRate(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.updateLogSvUseRate",paramMap);
	}
	
	/**
	 * 설치 정보 테이블 정보 를 가져온다.
	 * TODO
	 * 
	 * @param ins_type
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectOpmsInstallInfo(String ins_type)throws NkiaException{
		return (HashMap)selectByPk("opmsDetailDAO.selectOpmsInstallInfo", ins_type);
	}
	
	/**
	 * 설치 정보 테이블 관련 속성을 가져온다.
	 * TODO
	 * 
	 * @param ins_type
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInstallColumnList(String ins_type)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsInstallColumnList", ins_type);
	}
	
	/**
	 * 설치 정보 테이블 관련 속성을 가져온다.
	 * TODO
	 * 
	 * @param ins_type
	 * @return
	 * @throws NkiaException 
	 */
	public List selectOpmsInstallColumnListByAdTarget(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsInstallColumnListByAdTarget", paramMap);
	}
	
	/**
	 * 결과값이 리스트 형태인 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public List excuteQueryList(String query)throws NkiaException{
		return list("opmsDetailDAO.excuteQueryList", query);
	}
	
	/**
	 * 등록,삭제,수정 등의 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @throws NkiaException
	 */
	public void excuteCudQuery(String query)throws NkiaException{
		delete("opmsDetailDAO.excuteCudQuery", query);
	}
	
	/**
	 * 결과 값이 숫자유형의 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public int excuteQueryInt(String query)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.excuteQueryInt", query);
	}
	
	/**
	 * 장비 명명 정보를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectOpmsConfNmCodeInfo(HashMap dataMap)throws NkiaException{
		return (HashMap) selectByPk("opmsDetailDAO.selectOpmsConfNmCodeInfo", dataMap);
	}
	/**
	 * 유지보수 정보를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectOpmsMaintFormInfo(HashMap dataMap)throws NkiaException{
		return (HashMap) selectByPk("opmsDetailDAO.selectOpmsMaintFormInfo", dataMap);
	}
	
	
	
	/**
	 * 결과 값이 String 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public String excuteQueryString(String query)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.excuteQueryString", query);
	}
	
	/**
	 * 장비 명명정보 카운트를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsConfNmCodeCnt(HashMap dataMap)throws NkiaException{
		return (Integer) selectByPk("opmsDetailDAO.selectOpmsConfNmCodeCnt", dataMap);
	}
	
	/**
	 * 장비 명명 정보를 등록한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertOpmsConfNmCode(HashMap dataMap)throws NkiaException{
		insert("opmsDetailDAO.insertOpmsConfNmCode", dataMap);
	}
	
	/**
	 * 장비 명명정보를 수엊ㅇ한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateOpmsConfNmCode(HashMap dataMap)throws NkiaException{
		update("opmsDetailDAO.updateOpmsConfNmCode", dataMap);
	}
	
	/**
	 * 해당 관리그룹에 속하는 테이블 모두를 가져온다..
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityManualTable(HashMap dataMap)throws NkiaException{
		return list("opmsDetailDAO.selectEntityManualTable",dataMap);
	}
	
	/**
	 * 해당 테이블에 속하는 모든 테이블을 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTabAllColumnsList(HashMap dataMap)throws NkiaException{
		return list("opmsDetailDAO.selectTabAllColumnsList",dataMap);
	}
	
	/**
	 * 속성이력의 경우 미리등록된 정보에 수정한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateAmAssetChgHisModYn(HashMap dataMap)throws NkiaException{
		update("opmsDetailDAO.updateAmAssetChgHisModYn",dataMap);
	}
	
	/**
	 * ems_id를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectEmsIds(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.selectEmsIds", dataMap);
	}
	
	/**
	 * dca_id를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectDcaIds(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.selectDcaIds", dataMap);
	}
	
	/**
	 * 분류체계 정보를 가졍노다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectClassInfo(HashMap dataMap)throws NkiaException{
		return (HashMap)selectByPk("opmsDetailDAO.selectClassInfo", dataMap);
	}
	
	/**
	 * 자산 id를 생성 한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createAssetId(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.createAssetId", dataMap);
	}
	
	/**
	 * 자산 id를 생성 한다. (도입일이 있을 경우)
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createAssetIdExistIntroDt(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.createAssetIdExistIntroDt", dataMap);
	}
	
	
	/**
	 * 구성id를 생성한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createConfId(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.createConfId", dataMap);
	}
	
	/**
	 * 구성id를 생성한다. (도입일이 있을 경우)
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createConfIdExistIntroDt(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.createConfIdExistIntroDt", dataMap);
	}
	
	/**
	 * 기본적인 필수값들을 update한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBaseDataInfo(HashMap paramMap)throws NkiaException{
		update("opmsDetailDAO.updateBaseDataInfo",paramMap);
	}
	
	/**
	 * 논리 구성에서 기본적인 필수값을 update한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBaseLogSvInfo(HashMap paramMap)throws NkiaException{
		update("opmsDetailDAO.updateBaseLogSvInfo",paramMap);
	}
	
	/**
	 * 타입별 분류리스트를 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectConfClassTree(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectConfClassTree",paramMap);
	}
	
	/**
	 * 수정이력관리 그룹을 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectModViewCompList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectModViewCompList",paramMap);
	}
	
	/**
	 * 
	 * 가상서버(클라우드와 연계된 가상서버) 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsVmList(ModelMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsVmList",paramMap);
	}
	
	/**
	 * 
	 * 속성 수정 이력정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsFormModList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsFormModList",paramMap);
	}
	
	/**
	 * 
	 * 담당자 수정 이력 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOperModHistoryList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOperModHistoryList",paramMap);
	}
	
	/**
	 * 
	 * 연관서비스 수정 이력 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectServiceModHistoryList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectServiceModHistoryList",paramMap);
	}
	
	/**
	 * 
	 * 연관장비 수정이력
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectConfModHistoryList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectConfModHistoryList",paramMap);
	}
	
	/**
	 * 
	 * 설치정보 수정이력
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectInstallModHistoryList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectInstallModHistoryList",paramMap);
	}
	
	/**
	 * 속성별 상세 이력 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectFormAttrModList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectFormAttrModList",paramMap);
	}
	
	/**
	 * 
	 * TODO 담당자 상세 수정 이력정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOperModDetailList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOperModDetailList",paramMap);
	}
	
	/**
	 * 
	 * TODO 서비스 수정이력 상세
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelServiceModDetailList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectRelServiceModDetailList",paramMap);
	}
	
	/**
	 * 
	 * TODO 연관 구성 장비 수정이력 상세 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelConfHisDetailList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectRelConfHisDetailList",paramMap);
	}
	
	/**
	 * 
	 * TODO 설치정보 수정이력 상세 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectInstallHisDetailList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectInstallHisDetailList",paramMap);
	}
	

	/**
	 * 
	 * TODO 정보수집서 생성 
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createTempSeq(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.createTempSeq", dataMap);
	}
	
	public String createConfTempId(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.createConfTempId", dataMap);
	}
	
	public String createAssetTempId(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.createAssetTempId", dataMap);
	}
	
	public void insertOpmsInfoData(HashMap dataMap)throws NkiaException{
		insert("opmsDetailDAO.insertOpmsInfoData",dataMap);
	}
	
	public void insertOpmsInfoSeq(HashMap dataMap)throws NkiaException{
		insert("opmsDetailDAO.insertOpmsInfoSeq",dataMap);
	}	
	
	public void insertOpmsInfoMapping(HashMap dataMap)throws NkiaException{
		insert("opmsDetailDAO.insertOpmsInfoMapping",dataMap);
	}
	
	/**
	 * 정보수집서 데이터 insert 유무 확인
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectDataInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectDataInsertYn",paramMap);	
	}
	
	public int selectOpmsInfoServiceInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsInfoServiceInsertYn",paramMap);	
	}
	
	public int selectOpmsInfoRelConfInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsInfoRelConfInsertYn",paramMap);	
	}
	
	public int selectOpmsInfoInstallInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsInfoInstallInsertYn",paramMap);	
	}
	
	/**
	 * 정보수집서 연관 서비스 등록.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertOpmsInfoService(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertOpmsInfoService",paramMap);
	}
	
	/**
	 * 정보수집서 연관장비 정보.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertOpmsInfoRelConf(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.insertOpmsInfoRelConf",paramMap);
	}	
	
	/**
	 * 
	 * TODO 컴포넌트 리스트를 모두 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityCompList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectEntityCompList",paramMap);
	}
	
	/**
	 * 
	 * TODO 히스토리테이블에 그대로 넣는다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyOperHis(HashMap dataMap) throws NkiaException{
		insert("opmsDetailDAO.insertCopyOperHis",dataMap);
	}
	
	/**
	 * 
	 * TODO 서비스 정보를 히스토리테이블에 그대로 넣는다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyServiceHis(HashMap dataMap) throws NkiaException{
		insert("opmsDetailDAO.insertCopyServiceHis",dataMap);
	}
	
	/**
	 * 
	 * TODO 연관구성 정보 대상 구성정보 리스트 -20150810 정정윤 추가
	 * 
	 * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List selectCopyRelConfTargetList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectCopyRelConfTargetList",paramMap);
	}
	
	/**
	 * 
	 * TODO 연관구성 정보를 히스토리테이블에 그대로 넣는다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyRelConfHis(HashMap dataMap) throws NkiaException{
		insert("opmsDetailDAO.insertCopyRelConfHis",dataMap);
	}
	
	public void updateOperHist(HashMap dataMap) throws NkiaException{
		update("opmsDetailDAO.updateOperHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateRelServiceHist(HashMap dataMap) throws NkiaException{
		update("opmsDetailDAO.updateRelServiceHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateRelConfHist(HashMap dataMap) throws NkiaException{
		update("opmsDetailDAO.updateRelConfHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateHaHist(HashMap dataMap) throws NkiaException{
		update("opmsDetailDAO.updateHaHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateLogSvHist(HashMap dataMap) throws NkiaException{
		update("opmsDetailDAO.updateLogSvHist",dataMap);
	}
	
	
	/**
	 * 
	 * TODO 이중화 연관
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyHaHist(HashMap dataMap) throws NkiaException{
		insert("opmsDetailDAO.insertCopyHaHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 논리 연관
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyLogSvHist(HashMap dataMap) throws NkiaException{
		insert("opmsDetailDAO.insertCopyLogSvHist",dataMap);
	}
	
	/**
	 * 
	 * TODO update 여부만 변경
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
/*	public void updateInsAmAssetChgHisModYn(HashMap dataMap) throws NkiaException{
		update("opmsDetailDAO.updateInsAmAssetChgHisModYn",dataMap);
	}*/
	
	/**
	 * 
	 * TODO
	 * 분류체계별 속성관리에서 버튼 컴퍼넌트 목록을 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsBtnTree(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsBtnTree",paramMap);
	}
	
	/**
	 * 분류체계별 속성관리에서 엔티티에 대한 버튼목록을 가져온다
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityBtnInfo(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectEntityBtnInfo",paramMap);
	}
	/**
	 * 모델속성을 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchModelTreeComp(ModelMap paramMap) throws NkiaException {
		return  list("opmsDetailDAO.searchModelTreeComp", paramMap);
		
	}	
	/**
	 * 업무분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchWorkTreeComp(ModelMap paramMap) throws NkiaException {
		return  list("opmsDetailDAO.searchWorkTreeComp", paramMap);
		
	}

	/**
	 * HISOS를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchHISOSTreeComp(ModelMap paramMap) throws NkiaException {
		return  list("opmsDetailDAO.searchHISOSTreeComp", paramMap);
		
	}

	public int selectAssetHisByAttribute(HashMap paramMap)throws NkiaException {
		return (Integer)selectByPk("opmsDetailDAO.selectAssetHisByAttribute", paramMap);
	}	
	
	/**
	 * 설치된 소프트웨어 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInsSwList(HashMap paramMap) throws NkiaException {
		return  list("opmsDetailDAO.selectOpmsInsSwList", paramMap);
		
	}
	
	/**
	 * 설치 소프트웨어와 맵핑되는 소프트웨어 옵션 전체 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOptionMappingList(HashMap paramMap) throws NkiaException {
		return  list("opmsDetailDAO.selectOptionMappingList", paramMap);
		
	}
	
	/**
	 * 설치 소프트웨어와 맵핑되는 선택되어있던 소프트웨어 옵션 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSelectedOptionList(HashMap paramMap) throws NkiaException {
		return  list("opmsDetailDAO.selectSelectedOptionList", paramMap);
		
	}
	
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 목록 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteInsSw(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.deleteInsSw",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 목록 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertInsSw(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertInsSw",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteInsSwOption(HashMap paramMap)throws NkiaException{
		delete("opmsDetailDAO.deleteInsSwOption",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertInsSwOption(HashMap paramMap)throws NkiaException{
		insert("opmsDetailDAO.insertInsSwOption",paramMap);
	}
	/**
	 * 유지보수 폼 정보를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAssetMaintInfo(HashMap paramMap)throws NkiaException{
		return (HashMap)selectByPk("opmsDetailDAO.selectAssetMaintInfo", paramMap);
	}
	/**
	 * 
	 * TODO 유지보수 기본 정보 입력
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertAssetMaintBaseInfo(HashMap dataMap) throws NkiaException{
		insert("opmsDetailDAO.insertAssetMaintBaseInfo",dataMap);
	}
	
	public void updateAssetMaintBaseInfo(HashMap dataMap) throws NkiaException{
		update("opmsDetailDAO.updateAssetMaintBaseInfo",dataMap);
	}
	
	/**
	 * 유지보수 폼 정보를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectMaintYN(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("opmsDetailDAO.selectMaintYN", paramMap);
	}
	
	public int selectMaintCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("opmsDetailDAO.selectMaintCnt", paramMap);
	}
	
	public int selectMotherListCnt(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("opmsDetailDAO.selectMotherListCnt",paramMap);
	}

	public List selectMotherList(HashMap paramMap) throws NkiaException {
		return list("opmsDetailDAO.selectMotherList",paramMap);
	}
	/**
	 * 선택한 자자산의 모자산을 업데이트 한다. 
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateMotherInfo(HashMap paramMap)throws NkiaException{
		update("opmsDetailDAO.updateMotherInfo",paramMap);
	}

	public List selectAmIntroDtGroupBy(ModelMap paramMap) throws NkiaException {
		return list("opmsDetailDAO.selectAmIntroDtGroupBy",paramMap);
	}
	
	/**
	 * 연계 논리구성 정보 (단일 데이터.. 자바에서 반복문으로 돌면서 꺼내와야함)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectRelLocSv(HashMap paramMap)throws NkiaException{
		return (HashMap)selectByPk("opmsDetailDAO.selectRelLocSv", paramMap);
	}
	
	/**
	 * 자품 변경 이력 시퀀스 따옴
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectChildChgHisSeq(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("opmsDetailDAO.selectChildChgHisSeq",paramMap);
	}
	
	/**
	 * 자품 변경 이력 수정횟수 따옴
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectChildChgHisCnt(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("opmsDetailDAO.selectChildChgHisCnt",paramMap);
	}
	
	/**
	 * 자품 변경 이력 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertChildChgHis(HashMap dataMap) throws NkiaException{
		insert("opmsDetailDAO.insertChildChgHis",dataMap);
	}
	
	/**
	 * 자품 변경 이력 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisList(HashMap paramMap) throws NkiaException{
		return list("opmsDetailDAO.selectChildChgHisList",paramMap);
	}
	
	/**
	 * 자품 변경 이력 상세 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisDetail(HashMap paramMap) throws NkiaException{
		return list("opmsDetailDAO.selectChildChgHisDetail",paramMap);
	}
	
	/**
	 * 논리서버 이력 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
//	public List selectOpmsLogicHisList(HashMap paramMap)throws NkiaException{
//		return list("opmsDetailDAO.selectOpmsLogicHisList",paramMap);
//	}
	
	/**
	 * 자산 등록시 클래스 명
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchClassNm(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("opmsDetailDAO.searchClassNm", paramMap);
	}

	/**
	 * 자산등록자 및 자산등록일 입력
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateInsUserInfo(HashMap paramMap)throws NkiaException{
		update("opmsDetailDAO.updateInsUserInfo",paramMap);
	}

	public int selectOpmsInfoOperInsertYn(HashMap curMngMap) {
		return (Integer)selectByPk("opmsDetailDAO.selectOpmsInfoOperInsertYn", curMngMap);	
	}

	public void insertOpmsInfoOper(HashMap curMngMap) {
		insert("opmsDetailDAO.insertOpmsInfoOper", curMngMap);
	}	
		
	/**
	 * 장애 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProcHist(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.searchProcHist",paramMap);
	}
	
	/**
	 * 변경 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchChangeProcHist(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.searchChangeProcHist",paramMap);
	}
	
	/**
	 * 문제 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProblemProcHist(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.searchProblemProcHist",paramMap);
	}
	
	/**
	 * 서비스 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchServiceProcHist(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.searchServiceProcHist",paramMap);
	}

	public List selectIfPcSwList(ModelMap paramMap) {
		return list("opmsDetailDAO.selectIfPcSwList", paramMap);
	}
	
	/**
	 * 하위 논리 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelLogicList(HashMap paramMap)throws NkiaException{
		return list("opmsDetailDAO.selectOpmsRelLogicList",paramMap);
	}
	
	/**
	 * 가용성, 기밀성, 무결성 값 합산하여 중요도 값 업데이트
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateImportanceByAvCnIn(HashMap paramMap)throws NkiaException{
		update("opmsDetailDAO.updateImportanceByAvCnIn", paramMap);
	}
	
	/**
	 * 
	 * TODO 이력조회 - 장비별 요청현황 조회
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */		
	public List searchConfReqStateList(ModelMap paramMap) throws NkiaException{
		return list("opmsDetailDAO.searchConfReqStateList", paramMap);
	}
	
	public int searchConfReqStateListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("opmsDetailDAO.searchConfReqStateListCount", paramMap);
	}
}
