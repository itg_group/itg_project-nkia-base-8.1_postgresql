/*
 * @(#)OpmsInterfaceService.java              2013. 11. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.opms.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface OpmsInterfaceService {
	
	/**
	 * 
	 * Xeus -> ITG 가상화 등록
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	public boolean registVmAssetFromXeus(HashMap vmDataMap)throws NkiaException;
	
	/**
	 * 
	 * Xeus -> ITG 가상화 스펙변경
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	public boolean updateVmInfoFromXeus(HashMap vmDataMap)throws NkiaException;
	
	/**
	 * 
	 * Xeus -> ITG 가상화 상태변경
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	public boolean updateVmInfoStateFromXeus(HashMap vmDataMap)throws NkiaException;
	
	/**
	 * 
	 * EMS -> ITG 자산등록
	 * 
	 * @param vmDataMap
	 * @return
	 * @throws NkiaException
	 */
	public void registPhysiInfoAssetFromEms(HashMap vmDataMap)throws NkiaException;
	
	/**
	 * 
	 * ITAM VM 목록
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List searchVmList()throws NkiaException;

}
