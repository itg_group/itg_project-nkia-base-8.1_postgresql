package com.nkia.itg.itam.opms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;
import com.nkia.itg.itam.opms.service.OpmsDetailService;
import com.nkia.itg.itam.opms.util.OpmsUtil;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * 자산 원장 자동화
 * @version 1.0 2013. 10. 30.
 * @author <a href="mailto:gunbum@nkia.co.kr"> Gun-bum kim
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("opmsDetailService")
public class OpmsDetailServiceImpl implements OpmsDetailService{
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsDetailServiceImpl.class);
	
	@Resource(name = "opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;
	
	@Resource(name = "opmsUtil")
	private OpmsUtil opmsUtil;
	
	@Resource(name = "jsonUtil")
	private JsonUtil jsonUtil;  
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
/*	@Override
	public void selectClassEntitySetupInfo(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		paramMap.put("level", "2");
		//상위 탭 item들을 가져온다.
		List<HashMap> entityList = opmsDetailDAO.selectClassEntityInfo(paramMap);			

		
		//저장버튼 보이고 안보이고 수정 권한 갯수를 가져온다.
		int authCnt	 = opmsDetailDAO.selectUpdateAuthCnt(paramMap);
		
		//ems_id 연계되는지 안되는지..
		String ems_id = opmsDetailDAO.selectEmsIds(paramMap);
		
		//dca_id 연계되는지 안되는지..
		String dca_id = opmsDetailDAO.selectDcaIds(paramMap);
		
		JSONArray tabArray		= new JSONArray();
		JSONArray formArray		= new JSONArray();
		JSONArray dataArray		= new JSONArray();
		JSONArray manualBtnList = null;
		
		if(entityList != null && entityList.size()>0){
			
			for(int i=0;i<entityList.size();i++){				
				
				HashMap entityMap 		= entityList.get(i);
				String entity_type 		= (String) entityMap.get("ENTITY_TYPE");
				String entity_id 			= (String) entityMap.get("ENTITY_ID");
				String compType		 	= (String) entityMap.get("COMP_TYPE");
				String result				= "";

				opmsUtil.paramSync(paramMap,entityMap);
				entityMap.put("entity_id", entity_id);
				entityMap.put("ems_id", ems_id);
				entityMap.put("dca_id", dca_id);
				
				if(entity_type.equalsIgnoreCase("GROUP")){
					
					JSONArray panelItem = new JSONArray();
					//하위 컴포넌트 리스트를 가져온다.
					List<HashMap> childEntityList =  opmsDetailDAO.selectClassEntityInfo(entityMap);
					
					for(int j=0;j<childEntityList.size();j++){
						
						HashMap childMap 	= childEntityList.get(j);
						
						String child_entity_type 	= (String) childMap.get("ENTITY_TYPE");
						String child_entity_id 		= (String) childMap.get("ENTITY_ID");
						
						//request 에서 넘어온 정보를 다시 세팅해준다.
						opmsUtil.paramSync(paramMap,childMap);
						
						childMap.put("entity_id", child_entity_id);
						childMap.put("ems_id", ems_id);
						childMap.put("dca_id", dca_id);
						
						//사용자 지정폼
						if(child_entity_type.equalsIgnoreCase("MANUAL")){
							
							//해당 그룹에 관련된 속성리스트
							List<HashMap> attrList = opmsDetailDAO.selectManualAttrList(childMap);
							//All 인경우 다시 속성 리스트를 가져온다.권한 설정도 동시에 이루어짐..
							attrList = selectOpmsMnualAllList(attrList,childMap);
							//그룹패널의 버튼리스트를 구해온다.
							manualBtnList = new JSONArray();
							manualBtnList.addAll(opmsUtil.createPanelBtnComp(opmsDetailDAO.selectEntityBtnInfo(childMap), paramMap));	
							
					
							if(attrList != null && attrList.size()>0){
								//히든필드
								List<HashMap> hiddenList = selectOpmsHiddenList(attrList,childMap);
								//form 생성
								result = opmsUtil.createEditorFormComp(attrList,hiddenList,childMap,formArray,manualBtnList);
								//form 데이터를 가져온다.
								selectMnualCompData(attrList,hiddenList,childMap,dataArray);
							}
						}else if(child_entity_type.equalsIgnoreCase("COMPONENT")){
							
							//권한을 가져온다.
							List<HashMap> authList = opmsDetailDAO.selectCompAuthList(childMap);
							//권한이 수정권한인지 검증
							selectOpmsCompAuth(authList,childMap);
							//연관장비와,, 설치정보의 경우.. 상세 리스트를 가져와야한다.
							List<HashMap> attrList = selectOpmsCompSetupList(childMap);
							
							//컴포넌트 중에 예외 처리 다른 핸들링이 필요한경우 .....
							exceptionCompHandler(attrList,childMap,dataArray);
							//컴포넌트 생성
							result = opmsUtil.createComponent(attrList,childMap,formArray);
						}
						if(!"".equals(result)){
							//패널 item에 등록
							panelItem.add(result);
						}
					}
					
					//패널 item 생성
					result = opmsUtil.createPanelComp(entityMap,panelItem);
					
				}else if(entity_type.equalsIgnoreCase("MANUAL")){
					
					//해당 그룹에 관련된 속성리스트
					List<HashMap> attrList = opmsDetailDAO.selectManualAttrList(entityMap);
					//All 인경우 다시 속성 리스트를 가져온다.권한 설정도 동시에 이루어짐..
					attrList = selectOpmsMnualAllList(attrList,entityMap);
					
					//그룹패널의 버튼리스트를 구해온다.
					manualBtnList = new JSONArray();
					
					if(attrList != null && attrList.size()>0){
						//hidden 속성 리스트
						List<HashMap> hiddenList = selectOpmsHiddenList(attrList,entityMap);
						//form 생성
						result = opmsUtil.createEditorFormComp(attrList,hiddenList,entityMap,formArray,manualBtnList);
						//form 데이터를 가져온다.
						selectMnualCompData(attrList,hiddenList,entityMap,dataArray);
					}
				}else if(entity_type.equalsIgnoreCase("COMPONENT")){
					//컴포넌트 권한 리스트
					List<HashMap> authList = opmsDetailDAO.selectCompAuthList(entityMap);
					//권한 설정
					selectOpmsCompAuth(authList,entityMap);
					//연관장비와,, 설치정보의 경우.. 상세 리스트를 가져와야한다.
					List<HashMap> attrList = selectOpmsCompSetupList(entityMap);
					//컴포넌트 중에 예외 처리 다른 핸들링이 필요한경우 ..... 
					exceptionCompHandler(attrList,entityMap,dataArray);
					//컴포넌트 생성
					result = opmsUtil.createComponent(attrList,entityMap,formArray);
				}
				if(!"".equals(result)){
					//탭 array add
					tabArray.add(result);
				}
			}
		}
		//tab 정보
		paramMap.put("tabItem"		, opmsUtil.replaceAllquotes(tabArray.toString()));
		//데이터 id array
		paramMap.put("formItemIds"	, formArray.toString());
		//form 데이터 array
		paramMap.put("formData"		, dataArray.toString());
		//수정 권한 갯수
		paramMap.put("authCnt"		, authCnt);
	}
	
	@Override
	public void exceptionCompHandler(List<HashMap> attrList,HashMap entityMap,JSONArray dataArray)throws NkiaException{
		
		String entityId = StringUtil.parseString(entityMap.get("ENTITY_ID"));
		String compType = StringUtil.parseString(entityMap.get("COMP_TYPE"));
		JSONObject dataObj = new JSONObject();
		
		if("FILE".equalsIgnoreCase(compType)){//파일
			//파일 인경우 파일 id를 가져온다.
			String file_id = opmsDetailDAO.selectatchFileId(entityMap);
			entityMap.put("atch_file_id", file_id);
		}else if("INSTALL".equalsIgnoreCase(compType)){
			//설치 정보인경우 해당 관련 속성을 가져온다..
			for(int k=0;k<attrList.size();k++){
				HashMap attrDataMap = attrList.get(k);
				String code_id = StringUtil.parseString(attrDataMap.get("CODE_ID"));
				//설정된 설치 정보 리스트
				List installColList = opmsDetailDAO.selectOpmsInstallColumnList(code_id);
				entityMap.put(code_id, installColList);
			}
		}else if("CONFNMCODE".equalsIgnoreCase(compType)){
			//장비 명명정보는 manual form 형식이 아니어서 데이터를 동적으로 가져오지 못한다.. 
			HashMap confNmCode = opmsDetailDAO.selectOpmsConfNmCodeInfo(entityMap);
			//장비 명명 정보를 가져온다.
			dataObj.put(entityId, confNmCode);
			dataArray.add(dataObj);
		}else if("MAINT".equalsIgnoreCase(compType)){
			HashMap maintData = opmsDetailDAO.selectOpmsMaintFormInfo(entityMap);
			//장비 명명 정보를 가져온다.
			dataObj.put(entityId, maintData);
			dataArray.add(dataObj);
		}
	}
	
	@Override
	public List selectOpmsHiddenList(List<HashMap> attrList,HashMap entityMap)throws NkiaException{
		//auth_type : u: 수정 ,r: 읽기 , n: 히든
		entityMap.put("auth_type", "N");
		entityMap.put("attrList"	, attrList);
		return opmsDetailDAO.selectMnualAttrListByType(entityMap);
	}
	
	@Override
	public List selectOpmsMnualAllList(List<HashMap> attrList,HashMap entityMap)throws NkiaException{
		
		//속성리스트의 첮번재 데이터를 가져온다 .. col_id all 로 들어간경우를 위함
		if(attrList != null){
			HashMap flagMap 		= attrList.get(0);
			String flagAuth 		= (String)flagMap.get("COL_ID");
			//String entity_type 	= (String)entityMap.get("ENTITY_TYPE"); 
			String authType = "R";
			String compareAuthType = "";
			
			//권한 핸들링 ..   all 인경우 하나라도 수정권한(U) 가지고 있으면 수정권한이 있는걸로 판단한다.
			for(int i=0;i<attrList.size();i++){
				HashMap attrMap  = (HashMap)attrList.get(i);
				compareAuthType = (String) attrMap.get("AUTH_TYPE");
				if(compareAuthType.equals("U")){
					authType = compareAuthType;
					break;
				}
			}
			
			if("ALL".equalsIgnoreCase(flagAuth)){
					//manual 인 경우 전체 읽기 나 전체 수정으로 권한을 받으면 속성 리스트를 다시 가져온다. 
					entityMap.put("auth_type", authType);
					attrList = new ArrayList<HashMap>();
					attrList = opmsDetailDAO.selectMnualAttrListByType(entityMap);
			}
			entityMap.put("AUTH_TYPE", authType);
		}
		return attrList;
	}
	
	@Override
	public HashMap selectOpmsCompAuth(List<HashMap> attrList,HashMap entityMap)throws NkiaException{
		String authType = "R";
		String compareAuthType = "";
		//컴포넌트 권한 핸들러
		for(int i=0;i<attrList.size();i++){
			HashMap attrMap  = (HashMap)attrList.get(i);
			compareAuthType = (String) attrMap.get("AUTH_TYPE");
			
			if(compareAuthType.equals("U")){
				authType = compareAuthType;
				break;
			}
		}
		entityMap.put("AUTH_TYPE", authType);
		return entityMap;
	}
	
	@Override
	public List selectOpmsCompSetupList(HashMap entityMap)throws NkiaException{
		String compType = (String)entityMap.get("COMP_TYPE");
		List attrList = new ArrayList<HashMap>();
		if("REL_INFRA".equalsIgnoreCase(compType)){
			//연관 장비 정보에서 맵핍된 분류 유형을 가져온다.
			attrList = opmsDetailDAO.selectOpmsCompSetupList(entityMap);
		}else if("INSTALL".equalsIgnoreCase(compType)){
			//설치 정보에서 맵핑된 분류를 가져온다.
			attrList = opmsDetailDAO.selectOpmsCompInstallList(entityMap);
		}else if("UPDATE".equalsIgnoreCase(compType)){
			//수정이력 인경우 사용자가 볼수있는 그룹 리스트를 가져온다.
			attrList = opmsDetailDAO.selectModViewCompList(entityMap);
		}else if("MAINT".equalsIgnoreCase(compType)){
			//유지보수인 경우 관련 폼 데이터를 가져온다.
			attrList = opmsDetailDAO.selectMaintFormDataList(entityMap);
		}
		return attrList;
	}

	@Override
	public List selectOpmsSelMngUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//담당자 리스트
		return opmsDetailDAO.selectOpmsSelMngUserList(paramMap);
	}
	
	

	@Override
	public int selectOpmsMngUserCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsMngUserCnt(paramMap);
	}
	
	@Override
	public List selectOpmsMngUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsMngUserList(paramMap);
	}
	
	@Override
	public List selectOpmsSwLicenceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//소프트웨어 라이센스 리스트
		return opmsDetailDAO.selectOpmsSwLicenceList(paramMap);
	}
	
	@Override
	public List selectOpmsSelUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//사용자 리스트
		return opmsDetailDAO.selectOpmsSelUserList(paramMap);
	}
	
	@Override
	public int selectOpmsUserCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsUserCnt(paramMap);
	}
	
	@Override
	public List selectOpmsUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsUserList(paramMap);
	}
	@Override
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchCodeDataList(paramMap);
	}
	@Override
	public List selectOpmsSelChildList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//자자산리스트
		return opmsDetailDAO.selectOpmsSelChildList(paramMap);
	}
	@Override
	public List selectOpmsSelPurInfoList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//구매정보리스트
		return opmsDetailDAO.selectOpmsSelPurInfoList(paramMap);
	}
	@Override
	public List selectOpmsDepreciationInfoList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//구매정보리스트
		return opmsDetailDAO.selectOpmsDepreciationInfoList(paramMap);
	}
	
	@Override
	public List selectOpmsRelConfList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		String class_type = (String)paramMap.get("class_type");
		List relConfList = null;
		
		if("HA".equalsIgnoreCase(class_type)){ 
			//이중화의 경우 
			relConfList = opmsDetailDAO.selectOpmsRelHaGrpList(paramMap);
		}else if("LOGSV".equalsIgnoreCase(class_type)){
			//연관 장비에서 논리 연계정보인경우
			relConfList = opmsDetailDAO.selectOpmsReLogConfList(paramMap);
		}else{
			//나머지.. 등등
			relConfList = opmsDetailDAO.selectOpmsRelConfList(paramMap);
		}
		
		return relConfList;
	}
	
	@Override
	public List selectRelConfClassTree(HashMap paramMap)throws NkiaException{
		return opmsDetailDAO.selectRelConfClassTree(paramMap);
	}

	@Override
	public int selectOpmsConfCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsConfCnt(paramMap);
	}

	@Override
	public List selectOpmsConfList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsConfList(paramMap);
	}
	
	@Override
	public List selectOpmsRelServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsRelServiceList(paramMap);
	}

	@Override
	public int selectOpmsServiceCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsServiceCnt(paramMap);
	}

	@Override
	public List selectOpmsServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsServiceList(paramMap);
	}
	
	@Override
	public List selectServiceCustList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectServiceCustList(paramMap);
	}
	

	@Override
	public List selectOpmsInstallInfoList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//insType : 설치 분류 정보 
		String insType = (String)paramMap.get("ins_type");
		List insInfoList = null;
		
		HashMap intstallTblInfo = opmsDetailDAO.selectOpmsInstallInfo(insType);
		//String emsShowYn = StringUtil.parseString(intstallTblInfo.get("EMS_SHOW_YN"));
		//설치 정보마다 셋팅된 컬럼 정보를 가져온다.
		List<HashMap> intstallColList = opmsDetailDAO.selectOpmsInstallColumnList(insType);
		
		opmsUtil.paramSync(paramMap, intstallTblInfo);
		//select 쿼리를 만든다.
		String query = opmsUtil.createInstallQuery(intstallTblInfo,intstallColList, false);
		//정보리스트를 가져온다.
		insInfoList = opmsDetailDAO.excuteQueryList(query);
		
		return insInfoList;
	}
	
	@Override
	public HashMap selectMnualCompData(List attrList,List hiddenList,HashMap entityMap,JSONArray dataArray)throws NkiaException{
		
		JSONObject dataObj = new JSONObject();
		String entity_id = (String)entityMap.get("ENTITY_ID");
		
		//그룹에 해당되는 테이블 리스트
		List<HashMap> tblList 	=	opmsDetailDAO.selectTblList(entityMap);
		//join정보 
		List<HashMap> joinList	=  opmsDetailDAO.selectJoinList(entityMap);
		
		
		entityMap.put("tblList", tblList);
		entityMap.put("sel_type", "SKEYID");
		//conf_id,asset_id 리스트를 가져온다.
		List<HashMap> assetConfIdList	=  opmsDetailDAO.selectTypeColumn(entityMap);
		
		//select 쿼리를 만든다..
		String query = null;
		if(entityMap.get("view_type").equals("infmodifiy")){
			query = opmsUtil.createOpmsInfoSelectQuery(attrList,hiddenList,tblList,joinList,assetConfIdList,entityMap);
		}else{
			query = opmsUtil.createSelectQuery(attrList,hiddenList,tblList,joinList,assetConfIdList,entityMap);
		}
		
		HashMap resultMap  = opmsDetailDAO.excuteQuery(query);
		
		if(dataArray != null){
			dataObj.put(entity_id, resultMap);
			dataArray.add(dataObj);
		}
		
		return resultMap;
	}
	
	@Override
	public void updateMnualCompData(List attrList,List hiddenList,HashMap entityMap,HashMap currentData)throws NkiaException{
		
		JSONObject dataObj = new JSONObject();
		String entity_id = (String)entityMap.get("ENTITY_ID");
		
		//해당 컴포넌트에 맵핑된 테이블 리스트를 가져온다.
		List<HashMap> tblList 	=	opmsDetailDAO.selectTblList(entityMap);
		
		entityMap.put("tblList", tblList);
		entityMap.put("attrList",attrList);
		
		for(int i=0;i<tblList.size();i++){
			
			HashMap tblMap = tblList.get(i);
			entityMap.put("gen_table_nm", tblMap.get("GEN_TABLE_NM"));
			
			//update key정보를 가져온다.
			entityMap.put("sel_type", "UKEYID");
			List<HashMap> assetConfIdList	=  opmsDetailDAO.selectTypeColumn(entityMap);
			
			//upd_user_id,upd_dt 가 존재하는지 존재하면 리스트를 가져온다.
			entityMap.put("sel_type", "UPDID");
			List<HashMap> updUserDtList		=  opmsDetailDAO.selectTypeColumn(entityMap);
			
			//실제 테이블에 존재하는 컬럼 리스트를 가져온다.
			entityMap.put("sel_type", "COLID");
			List<HashMap> tblColIdList	=  opmsDetailDAO.selectTypeColumn(entityMap);
			
			String query = "";
			//해당 id로 카운트를 가져온다.
			query = opmsUtil.createCountQuery(assetConfIdList,entityMap);
			
			int cnt = 0;
			
			if(!"".equalsIgnoreCase(query)){
				//카운트 실행쿼리
				cnt = opmsDetailDAO.excuteQueryInt(query);
			}
			
			if(!"".equalsIgnoreCase(query)){
				if(cnt == 0){
					//데이터가 없으면 insert 쿼리를 만든다
					query = opmsUtil.createInsertQuery(attrList,assetConfIdList,updUserDtList,tblColIdList,entityMap,currentData);
				}else{
					//데이터가 있으면 update 쿼리를 만든다.
					query = opmsUtil.createUpdateQuery(attrList,assetConfIdList,updUserDtList,tblColIdList,entityMap,currentData);
				}
				//쿼리 실행
				opmsDetailDAO.excuteCudQuery(query);
			}
		}
	}
	
	@Override
	public void updateOpmsDetail(HashMap paramMap)throws NkiaException{
		
		String conf_id	 	= StringUtil.parseString(paramMap.get("conf_id"));
		String asset_id 	= StringUtil.parseString(paramMap.get("asset_id"));
		String asset_id_hi	= StringUtil.parseString(paramMap.get("asset_id_hi"));
		String entity_class_id 	= StringUtil.parseString(paramMap.get("entity_class_id"));
		String user_id 		= StringUtil.parseString(paramMap.get("user_id"));
		String class_id 	= StringUtil.parseString(paramMap.get("class_id"));
		String class_type 	= StringUtil.parseString(paramMap.get("class_type"));

		
		String file_id	 	= StringUtil.parseString(paramMap.get("atch_file_id"));
		List formItemIds	= (List)paramMap.get("formItemIds");
		
		if(formItemIds != null){
			
			//자산 이력관리 테이블의 seq id를 생성한다.
			String chgSeq = selectAssetChgHisSeq(paramMap);
			paramMap.put("chg_his_seq", chgSeq);
			
			//등록및 수정전에 과거 데이터를 모두 넣어준다.
			insertPrevDataHis(paramMap);
			
			for(int i=0;i<formItemIds.size();i++){
				
				HashMap dataMap = (HashMap)formItemIds.get(i);
				
				String entity_id	= StringUtil.parseString(dataMap.get("entity_id"));
				String comp_id		= StringUtil.parseString(dataMap.get("comp_id"));
				String type 			= StringUtil.parseString(dataMap.get("type"));
				String entity_type= StringUtil.parseString(dataMap.get("entity_type"));
				
				dataMap.put("asset_id", asset_id);
				dataMap.put("asset_id_hi", asset_id_hi);
				dataMap.put("conf_id", conf_id);
				dataMap.put("class_id", class_id);
				dataMap.put("user_id", user_id);
				dataMap.put("chg_his_seq", chgSeq);
				dataMap.put("entity_class_id", entity_class_id);
				dataMap.put("ENTITY_ID", entity_id);
				
				//권한이 있는지를 판단한다.
				int authCnt	 = opmsDetailDAO.selectUpdateAuthCnt(dataMap);
				
				//manual component
				if("F".equalsIgnoreCase(type)){
					//현제 update 데이터
					HashMap currentData 		= (HashMap)paramMap.get(comp_id);
					//해당 속성 리스트를 가져온다.
					List<HashMap> attrList 		= opmsDetailDAO.selectManualAttrList(dataMap);
					//권한 및 all인경우 해당 속성 리스트를 가져온다.
					attrList 							= selectOpmsMnualAllList(attrList,dataMap);
					//숨김 속성리스트를 가져온다.
					List<HashMap> hiddenList 	= selectOpmsHiddenList(attrList,dataMap);
					//히스토리 테이블에서 해당 데이터와 지금 현제 데이터를 비교한다.
					compareDetailData(attrList,dataMap,currentData);
					
					if(authCnt>0){
						//권한이 있으면 update or insert
						updateMnualCompData(attrList,hiddenList,dataMap,currentData);
					}
					
					//single 리스트
				}else if("SL".equalsIgnoreCase(type)){ // 싱글 리스트
					//담당자
					if("MANAGER".equalsIgnoreCase(entity_type)){
						
						//과거데이터를 가져온다
						List<HashMap> prevData		= selectOpmsSelMngUserList(paramMap);
						//현제 수정될 데이터
						List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
						
						//비교로직
						for(int j=0;j<prevData.size();j++){
							HashMap managerMap = prevData.get(j);
							String preUserId	= StringUtil.parseString(managerMap.get("USER_ID"));
							String preServId	= StringUtil.parseString(managerMap.get("SERVICE_ID"));
							
							managerMap.put("ASSET_ID", asset_id);
							managerMap.put("CONF_ID", conf_id);
							managerMap.put("CHG_HIS_SEQ", chgSeq);
							
							int compareCnt = 0;
							if(currentData != null){
								if(currentData.size() != prevData.size()){
									//두데이터의 사이즈가 다른경우 모두 수정된걸로 간주
									managerMap.put("CHG_YN", "Y");
								}else{
									//같은 경우 두개 의 데이터를 user_id로 비교
									for(int k=0;k<currentData.size();k++){
										HashMap curMngMap = currentData.get(k);
										String curUserId	= StringUtil.parseString(curMngMap.get("USER_ID"));
										if(preUserId.equalsIgnoreCase(curUserId)){
											compareCnt++;
											break;
										}
									}
									
									if(compareCnt == 0){
										// 없으면 수정
										managerMap.put("CHG_YN", "Y");
									}else{
										// 있으면 수정
										managerMap.put("CHG_YN", "N");
									}
								}
							}else{
								//현제 데이터가 없으면 .. 과거에 없어도.. 수정으로 간주 어차피 과거도 데이터가 없으면 수정이력에는 안들어감.. 
								//과거데이터가 있다면.. 수정으로 들억간ㄷ..
								managerMap.put("CHG_YN", "Y");
							}
							//담당자 이력관리 테이블
							opmsDetailDAO.updateOperHist(managerMap);
						}
						//수정자가 권한이 있는경우
						if(authCnt>0){
							//기존 데이터 삭제
							opmsDetailDAO.deleteOper(dataMap);
							if(currentData != null){
								for(int j=0;j<currentData.size();j++){
									//등록
									HashMap curMngMap = currentData.get(j);
									curMngMap.put("SEQ", j+1);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									curMngMap.put("ASSET_ID", asset_id);
									opmsDetailDAO.insertOper(curMngMap);
									if(j==currentData.size()){
										opmsDetailDAO.updateCust(curMngMap);
									}
								}
							}
						}
					}
					if("USER".equalsIgnoreCase(entity_type)){
						
						//과거데이터를 가져온다
						List<HashMap> prevData		= selectOpmsSelUserList(paramMap);
						//현제 수정될 데이터
						List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
						
						//비교로직
						for(int j=0;j<prevData.size();j++){
							HashMap managerMap = prevData.get(j);//이전 데이터
							String preUpServId	= StringUtil.parseString(managerMap.get("UP_CODE"));//비교하기 위한 값들
							String preServId	= StringUtil.parseString(managerMap.get("SUB_CODE"));//비교하기 위한 값들
							String preDnServId	= StringUtil.parseString(managerMap.get("FAB_CODE"));//비교하기 위한 값들
							
							managerMap.put("CONF_ID", conf_id);
							managerMap.put("ASSET_ID", asset_id);
							managerMap.put("CHG_HIS_SEQ", chgSeq);
							
							int compareCnt = 0;
							if(currentData != null){
								if(currentData.size() != prevData.size()){
									//두데이터의 사이즈가 다른경우 모두 수정된걸로 간주
									managerMap.put("CHG_YN", "Y");
								}else{
									//같은 경우 두개 의 데이터를 SERVICE_ID와 USER_ID로 비교
									for(int k=0;k<currentData.size();k++){
										HashMap curMngMap = currentData.get(k);
										String curUpServId	= StringUtil.parseString(curMngMap.get("UP_CODE"));//대분류, 중분류 추가
										String curServId	= StringUtil.parseString(curMngMap.get("SUB_CODE"));//대분류, 중분류 추가
										String curDnServId	= StringUtil.parseString(curMngMap.get("FAB_CODE"));//대분류, 중분류 추가										
										if(preUpServId.equalsIgnoreCase(curUpServId) && preServId.equalsIgnoreCase(curServId) && preDnServId.equalsIgnoreCase(curDnServId)){
											compareCnt++;
											break;
										}
									}
									
									if(compareCnt == 0){
										// 수정값 존재
										managerMap.put("CHG_YN", "Y");
									}else{
										// 수정된 값 없음
										managerMap.put("CHG_YN", "N");
									}
								}
							}else{
								//현제 데이터가 없으면 .. 과거에 없어도.. 수정으로 간주 어차피 과거도 데이터가 없으면 수정이력에는 안들어감.. 
								//과거데이터가 있다면.. 수정으로 들억간ㄷ..
								managerMap.put("CHG_YN", "Y");
							}
							//사용자 이력관리 테이블
							opmsDetailDAO.updateRelServiceHist(managerMap);//SERVICE_REL_HIS에 업데이트
						} //이력관련 데이터 변경 확인 및 이력 업데이트
						//수정자가 권한이 있는경우
						if(authCnt>0){
							//기존 데이터 삭제
							opmsDetailDAO.deleteUser(dataMap); //TODO 기존 데이터 삭제 쿼리 작성
							if(currentData != null){
								for(int j=0;j<currentData.size();j++){
									//등록
									HashMap curMngMap = currentData.get(j);
									curMngMap.put("SEQ", j+1);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									opmsDetailDAO.insertUser(curMngMap); //TODO 업데이트 쿼리 작성
								}
							}
						}
					}//연관 서비스 리스트
					else if("REL_SERVICE".equalsIgnoreCase(entity_type)){
						
						
						List<HashMap> prevData	= selectOpmsRelServiceList(paramMap);
						List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
						
						for(int j=0;j<prevData.size();j++){
							
							HashMap preServiceMap = prevData.get(j);
							String preServiceId	= StringUtil.parseString(preServiceMap.get("SERVICE_ID"));
							
							preServiceMap.put("CONF_ID", conf_id);
							preServiceMap.put("ASSET_ID", asset_id);
							preServiceMap.put("CHG_HIS_SEQ", chgSeq);
							
							int compareCnt = 0;
							if(currentData != null){
								if(currentData.size() != prevData.size()){
									preServiceMap.put("CHG_YN", "Y");
								}else{
									for(int k=0;k<currentData.size();k++){
										HashMap curServiceMap = currentData.get(k);
										String curServiceId	= StringUtil.parseString(curServiceMap.get("SERVICE_ID"));
										if(preServiceId.equalsIgnoreCase(curServiceId)){
											compareCnt++;
											break;
										}
									}
									if(compareCnt == 0){
										preServiceMap.put("CHG_YN", "Y");
									}else{
										preServiceMap.put("CHG_YN", "N");
									}
								}
							}else{
								preServiceMap.put("CHG_YN", "Y");
							}
							opmsDetailDAO.updateRelServiceHist(preServiceMap);
						}
						if(authCnt>0){
							opmsDetailDAO.deleteService(dataMap);
							if(currentData != null){
								for(int j=0;j<currentData.size();j++){
									HashMap curMngMap = currentData.get(j);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									opmsDetailDAO.insertService(curMngMap);
								}
							}
						}
					}
					else if("INSTALL_SW".equalsIgnoreCase(entity_type)){
						
						// update Opms
						// 수정될 데이터
						List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
						
						// 현재 데이터의 OPTION_ID_LIST 따로 빼 놓는다
						if(authCnt>0){
							
							//기존 데이터 삭제
							opmsDetailDAO.deleteInsSwOption(dataMap); // 해당 conf_id의 옵션 전체 삭제
							opmsDetailDAO.deleteInsSw(dataMap); //TODO 기존 데이터 삭제 쿼리 작성
							
							if(currentData != null){
								
								for(int j=0;j<currentData.size();j++){
									
									//등록
									HashMap curMngMap = currentData.get(j);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									
									opmsDetailDAO.insertInsSw(curMngMap); // 설치 소프트웨어 목록을 insert 해준다.
									
									List curOptionIdList = null;
									
									// OPTION_ID_LIST Key 가 존재한다면
									if( curMngMap.get("OPTION_ID_LIST") != null ){
										
										// OPTION_ID_LIST의 데이터가 리스트형 일 때
										if( curMngMap.get("OPTION_ID_LIST") instanceof ArrayList){
											
											// 해당 옵션ID 들로 리스트를 만들어 주고
											curOptionIdList = (ArrayList)curMngMap.get("OPTION_ID_LIST");
											
											// 만들어진 리스트에 값이 들어 있다면
											if( curOptionIdList != null && curOptionIdList.size() > 0 ){
												
												// 해당 리스트의 길이(설치 소프트웨어가 가지고 있는 옵션의 수)만큼 다시 insert 해준다.
												for(int z=0; z<curOptionIdList.size(); z++) {
													
													curMngMap.put("OPTION_ID", curOptionIdList.get(z));
													opmsDetailDAO.insertInsSwOption(curMngMap);
												}
											}
										}
									} 
									
								}
							}
						}
						
					}
				}
				else if("TL".equalsIgnoreCase(type)){//tab 리스트
					//연관 구성장비
					if("REL_INFRA".equalsIgnoreCase(entity_type)){
						dataMap.put("class_type", comp_id);
						List<HashMap> prevDataList = selectOpmsRelConfList(dataMap);
						List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
						String keyId = "";
						boolean compareFlag = true;
						for(int j=0;j<prevDataList.size();j++){
							HashMap prevDataMap = prevDataList.get(j);
							//나중에 관리 수정도 가능할지도 몰라서 flag로 빼놓습니다.
							if("HA".equalsIgnoreCase(comp_id)){
								keyId = "HA_GRP_ID";
								compareFlag = false;
							}else if("LOGSV".equalsIgnoreCase(comp_id)){
								keyId = "CONF_ID";
								compareFlag = true;
							}else{
								keyId = "CONF_ID";
								compareFlag = true;
							} 
							
							String preCompareId	= StringUtil.parseString(prevDataMap.get(keyId));
							
							prevDataMap.put("CHG_HIS_CONF_ID", conf_id);
							prevDataMap.put("CHG_HIS_ASSET_ID", asset_id);
							prevDataMap.put("CHG_HIS_SEQ", chgSeq);
							
							if(compareFlag){
								int compareCnt = 0;
								if(currentDataList != null){
									if(currentDataList.size() != prevDataList.size()){
										//두데이터의 사이즈가 다른경우 모두 수정된걸로 간주
										prevDataMap.put("CHG_YN", "Y");
									} else {
										//논리구성 수정인 경우 사용율 비교
										if("LOGSV".equalsIgnoreCase(comp_id)){
											for(int k=0;k<currentDataList.size();k++){
												HashMap curDataMap = currentDataList.get(k);
												HashMap preMap = opmsDetailDAO.selectRelLocSv(curDataMap);
												String logConfId = StringUtil.parseString(curDataMap.get("CONF_ID"));
												String logUseRate = StringUtil.parseString(curDataMap.get("USE_RATE"));
												String preUseRate = StringUtil.parseString(preMap.get("USE_RATE"));
												if(preUseRate.equalsIgnoreCase(logUseRate)){
													compareCnt++;
													break;
												}
												String curCompareId	= StringUtil.parseString(curDataMap.get(keyId));
												if(preCompareId.equalsIgnoreCase(curCompareId)){
													compareCnt++;
													break;
												}
											}
											if(compareCnt == 0){
												prevDataMap.put("CHG_YN", "Y");
											}else{
												prevDataMap.put("CHG_YN", "N");
											}
										} else {
											for(int k=0;k<currentDataList.size();k++){
												HashMap curDataMap = currentDataList.get(k);
												String curCompareId	= StringUtil.parseString(curDataMap.get(keyId));
												if(preCompareId.equalsIgnoreCase(curCompareId)){
													compareCnt++;
													break;
												}
											}
											if(compareCnt == 0){
												prevDataMap.put("CHG_YN", "Y");
											}else{
												prevDataMap.put("CHG_YN", "N");
											}
										}
									}
								}else{
									prevDataMap.put("CHG_YN", "Y");
								}
							}else{
								prevDataMap.put("CHG_YN", "N");
							}
							//HA 와 LOGSV 는 이력만 쌓습니다.
							if("HA".equalsIgnoreCase(comp_id)){
								//opmsDetailDAO.updateHaHist(prevDataMap);
							}else if("LOGSV".equalsIgnoreCase(comp_id)){
								//opmsDetailDAO.updateLogSvHist(prevDataMap);
							}else{
								opmsDetailDAO.updateRelConfHist(prevDataMap);
								
								// 20150810 정정윤 추가 - 연관장비 이력 등록 시, 상대방 구성에도 수정이력 쌓이도록
								if("Y".equalsIgnoreCase((String)prevDataMap.get("CHG_YN"))){
									List targetConfList = opmsDetailDAO.selectCopyRelConfTargetList(dataMap);
									
									for(int x=0; x<targetConfList.size(); x++) {
										HashMap targetMap = new HashMap();
										HashMap subDataMap = new HashMap();
										targetMap = (HashMap)targetConfList.get(x);
										
										subDataMap.put("CHG_HIS_ASSET_ID", targetMap.get("ASSET_ID"));
										subDataMap.put("CHG_HIS_CONF_ID", targetMap.get("CONF_ID"));
										subDataMap.put("CONF_ID", conf_id);
										subDataMap.put("CHG_HIS_SEQ", chgSeq);
										subDataMap.put("CHG_YN", prevDataMap.get("CHG_YN"));
										subDataMap.put("TARGET_CONF_ID", prevDataMap.get("CONF_ID"));
										
										opmsDetailDAO.updateRelConfHist(subDataMap);
									}
								}
							}
						}
						if(authCnt>0){
							//파라메터 대소문자 관련해서 문제가 있습니다...
							dataMap.put("main_class_type", class_type);
							
							if(currentDataList != null){
								//현제는 구현사항이 없음... > 이제생김.
								if("LOGSV".equalsIgnoreCase(comp_id)){
									for(int j=0;j<currentDataList.size();j++){
										HashMap curRelConfMap = currentDataList.get(j);
//										curRelConfMap.put("CONF_ID", conf_id);
										curRelConfMap.put("conf_id", curRelConfMap.get("CONF_ID"));
										curRelConfMap.put("use_rate", curRelConfMap.get("USE_RATE"));
										opmsDetailDAO.updateRelConfUseRate(curRelConfMap);
									}
								}else if("HA".equalsIgnoreCase(comp_id)){
							
								}else{
									opmsDetailDAO.deleteRelConf(dataMap);
									for(int j=0;j<currentDataList.size();j++){
										HashMap curRelConfMap = currentDataList.get(j);
										curRelConfMap.put("CLASS_TYPE", comp_id);
										curRelConfMap.put("MAIN_CONF_ID", conf_id);
										curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
										opmsDetailDAO.insertRelConf(curRelConfMap);
									}
								}
							} else {
								opmsDetailDAO.deleteRelConf(dataMap);
							}
						}
					}//설치 정보
					else if("INSTALL".equalsIgnoreCase(entity_type)){
						
						dataMap.put("ins_type", comp_id);
						HashMap installInfo = opmsDetailDAO.selectOpmsInstallInfo(comp_id);
						List<HashMap> installColList = opmsDetailDAO.selectOpmsInstallColumnList(comp_id);
						
						List<HashMap> prevDataList = selectOpmsInstallInfoList(dataMap);
						List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
						
						String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
						String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
						String dcaShowYn =  StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
						String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
						String tableNm	= StringUtil.parseString(installInfo.get("TABLE_NM"));
						
						if(emsShowYn.equalsIgnoreCase("N") && dcaShowYn.equalsIgnoreCase("N")){
							for(int j=0;j<prevDataList.size();j++){
								
								HashMap preDataMap = (HashMap)prevDataList.get(j);
								HashMap hisDataMap = new HashMap();
								
								hisDataMap.put("conf_id"			, conf_id);
								hisDataMap.put("asset_id"			, asset_id);
								hisDataMap.put("chg_his_seq"		, chgSeq);
								hisDataMap.put("seq"				, j+1);
							
								for(int k=0;k<installColList.size();k++){
									
									HashMap installColMap = installColList.get(k);
									
									String colId = StringUtil.parseString(installColMap.get("COL_ID"));
									String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
									String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
									String curVal = "";
									String preVal = "";
									
									if(currentDataList != null){
										if(currentDataList.size() == prevDataList.size()){
											HashMap currentDataMap = currentDataList.get(j);
											preVal = StringUtil.parseString(preDataMap.get(colId));
											curVal = StringUtil.parseString(currentDataMap.get(colId));
											if(preVal.equals(curVal)){
												hisDataMap.put("chg_yn"			, "N");
											}else{
												hisDataMap.put("chg_yn"			, "Y");
											}
										}else{
											hisDataMap.put("chg_yn"			, "Y");
										}
										hisDataMap.put("chg_table"		,tableNm);
										hisDataMap.put("chg_column_nm"	, colNm);
										hisDataMap.put("chg_column_id"		, colId);
									}else{
										hisDataMap.put("chg_yn"			, "Y");
										hisDataMap.put("chg_table"		,tableNm);
										hisDataMap.put("chg_column_nm"	, colNm);
										hisDataMap.put("chg_column_id"		, colId);
									}
									opmsDetailDAO.updateInsAmAssetChgHisModYn(hisDataMap);
								}
							}
						}
						if(authCnt>0 && emsShowYn.equalsIgnoreCase("N") && dcaShowYn.equalsIgnoreCase("N")){
							//파라메터 대소문자 관련해서 문제가 있습니다...
							if( !tableNm.equalsIgnoreCase("IF_DCA_SV_INSTALLED_SW_V") ){
								installInfo.put("CONF_ID", conf_id);
								deleteInstallInfo(installInfo);
								if(currentDataList != null){
									for( int j=0; j<currentDataList.size(); j++ ){
										HashMap curInstallMap = currentDataList.get(j);
										curInstallMap.put("CONF_ID", conf_id);
										curInstallMap.put("TABLE_NM", tableNm);
										insertInstallInfo(curInstallMap,installColList);
									}
								}
							}
							
						}
					}
				}//커스텀 폼 
				else if("CF".equalsIgnoreCase(type)){
					//장비 명명정보 메가 에만 있음.... 
					if("CONFNMCODE".equalsIgnoreCase(entity_type)){
						
						HashMap prevDataMap = opmsDetailDAO.selectOpmsConfNmCodeInfo(dataMap);
						HashMap curDataMap = (HashMap)paramMap.get(comp_id);
						
						if(prevDataMap != null){
							
							List<HashMap> attrList = opmsUtil.getConfNmCodeResourceList();
							
							for(int j=0;j<attrList.size();j++){
								HashMap headerMap = attrList.get(j);
								String key = StringUtil.parseString(headerMap.get("COL_ID"));
								String label = StringUtil.parseString(headerMap.get("COL_NAME"));
								String preValue = StringUtil.parseString(prevDataMap.get(key));
								String curValue = StringUtil.parseString(curDataMap.get(key));
									
								HashMap hisDataMap = new HashMap();
								hisDataMap.put("conf_id"			, conf_id);
								hisDataMap.put("asset_id"			, asset_id);
								hisDataMap.put("chg_his_seq"	, chgSeq);
								hisDataMap.put("seq"				, "1");
								hisDataMap.put("chg_table"		, "AM_CONF_NM_CODE");
								hisDataMap.put("chg_column_id" , key);
								hisDataMap.put("chg_yn"		, "N");
								
								if(!preValue.equalsIgnoreCase(curValue)){
									hisDataMap.put("chg_yn"		, "Y");
								}
								
								opmsDetailDAO.updateInsAmAssetChgHisModYn(hisDataMap);
							}
						}
						if(authCnt>0){
							//파라메터 대소문자 관련해서 문제가 있습니다...
							int cnt = opmsDetailDAO.selectOpmsConfNmCodeCnt(dataMap);
							if(curDataMap != null){
								curDataMap.put("ASSET_ID", asset_id);
								curDataMap.put("USER_ID", user_id);
								if(cnt>0){
									opmsDetailDAO.updateOpmsConfNmCode(curDataMap);
								}else{
									opmsDetailDAO.insertOpmsConfNmCode(curDataMap);
								}
							}
						}
					}
					if("MAINT".equalsIgnoreCase(entity_type)){
						//TODO am_asset_maint update , am_asset_maint_his에 insert
						//과거 데이터와 화면 데이터 비교 -> 변경사항 있으면 his에 insert, maint에 update
						//hmms업데이트 이후 수정 가능한지 확인.
						
						HashMap prevDataMap = opmsDetailDAO.selectOpmsConfNmCodeInfo(dataMap); //테이블 데이터 조회
						HashMap curDataMap = (HashMap)paramMap.get(comp_id);//현재 폼의 데이트를 가져오기
						
						if(prevDataMap != null){
							
							List<HashMap> attrList = opmsUtil.getConfNmCodeResourceList();
							
							for(int j=0;j<attrList.size();j++){
								HashMap headerMap = attrList.get(j);
								String key = StringUtil.parseString(headerMap.get("COL_ID"));
								String label = StringUtil.parseString(headerMap.get("COL_NAME"));
								String preValue = StringUtil.parseString(prevDataMap.get(key));
								String curValue = StringUtil.parseString(curDataMap.get(key));
									
								HashMap hisDataMap = new HashMap();
								hisDataMap.put("conf_id"			, conf_id);
								hisDataMap.put("asset_id"			, asset_id);
								hisDataMap.put("chg_his_seq"	, chgSeq);
								hisDataMap.put("seq"				, "1");
								hisDataMap.put("chg_table"		, "AM_ASSET_MAINT");
								hisDataMap.put("chg_column_id" , key);
								hisDataMap.put("chg_yn"		, "N");
								
								if(!preValue.equalsIgnoreCase(curValue)){
									hisDataMap.put("chg_yn"		, "Y");
								}
								
								opmsDetailDAO.updateInsAmAssetChgHisModYn(hisDataMap);
							}
						}
						if(authCnt>0){
							//파라메터 대소문자 관련해서 문제가 있습니다...
							int cnt = opmsDetailDAO.selectOpmsConfNmCodeCnt(dataMap);//
							if(curDataMap != null){
								curDataMap.put("ASSET_ID", asset_id);
								curDataMap.put("USER_ID", user_id);
								if(cnt>0){
									opmsDetailDAO.updateOpmsConfNmCode(curDataMap);
								}else{
									opmsDetailDAO.insertOpmsConfNmCode(curDataMap);
								}
							}
						}
					}
				
				
				}
			}
			if(!"".equalsIgnoreCase(file_id)){
				opmsDetailDAO.updateAtchFileId(paramMap);
			}
			
			// 2016.12.08.정정윤 추가 - 가용성, 기밀성, 무결성 값 합산하여 중요도 값 업데이트
			opmsDetailDAO.updateImportanceByAvCnIn(paramMap);
						
			
		}
	}
	
	// SK하이닉스 자산저장 후 예외처리 부분
	private void executeExceptionProcBySkh(String asset_id, String conf_id, String chgSeq)throws NkiaException {
		//TODO AM_ASSET_MALIST에 인서트 쿼리 추가
		HashMap params = new HashMap();
		params.put("asset_id", asset_id);
		params.put("conf_id", conf_id);
		params.put("chg_his_seq", chgSeq);
		params.put("chg_column_id", "GET_DT");
		
		// 장비설치일자 비교해서 변경이 되었으면 Maint호출해주는 부분이 들어오면 됩니다.	
		int fieldChgCnt = opmsDetailDAO.selectAssetHisByAttribute(params);
		
		// WARRANTY가 변경됐을 때
		params.put("chg_column_id", "WARRANTY"); 
		int fieldChgCnt2= opmsDetailDAO.selectAssetHisByAttribute(params);
		
		String MaintYN = opmsDetailDAO.selectMaintYN(params);
		int MaintCnt = opmsDetailDAO.selectMaintCnt(params);
		if( (fieldChgCnt == 1 || fieldChgCnt2 == 1) && MaintYN.equalsIgnoreCase("Y")){ //유지보수 대상이면서 동시에 취득일자 업데이트 되었을 때
			if(MaintCnt < 1){
				opmsDetailDAO.insertAssetMaintBaseInfo(params);//유지보수 기본정보 입력 TO AM_ASSET_MAINT
			}else{
				opmsDetailDAO.updateAssetMaintBaseInfo(params);//유지보수 기본정보 입력 TO AM_ASSET_MAINT
			}
		}
	}

	@Override
	public String selectAssetChgHisSeq(HashMap dataMap)throws NkiaException{
		opmsDetailDAO.insertChgHisSeq(dataMap);
		return opmsDetailDAO.selectAssetChgHisSeq(dataMap);
	}
	
	*//**
	 * entity_type == > manual 히스토리 값과 비교.. update전에 미리 데이터를 넣어놓습니다.. 
	 * @param attrList
	 * @param dataMap
	 * @param currentData
	 * @throws NkiaException
	 *//*
	@Override
	public void compareDetailData(List<HashMap> attrList,HashMap dataMap,HashMap currentData)throws NkiaException{
		
		String chg_his_seq 	= StringUtil.parseString(dataMap.get("chg_his_seq"));
		String conf_id 			= StringUtil.parseString(dataMap.get("conf_id"));
		String asset_id 		= StringUtil.parseString(dataMap.get("asset_id"));
		String user_id 			= StringUtil.parseString(dataMap.get("user_id"));
		
		for(int i=0;i<attrList.size();i++){
			
			HashMap colDataMap = attrList.get(i);
			
			String mkCompNm	= StringUtil.parseString(colDataMap.get("MK_COMP_NM"));
			String colId			= StringUtil.parseString(colDataMap.get("COL_ID"));
			String genTableNm= StringUtil.parseString(colDataMap.get("GEN_TABLE_NM"));
			String curValue	= StringUtil.parseString(currentData.get(mkCompNm));
			String defaultVal 	= StringUtil.parseString(colDataMap.get("DEFAULT_VALUE"));
			
			if("".equals(defaultVal)){
				HashMap hisDataMap = new HashMap();
				
				hisDataMap.put("conf_id"			, conf_id);
				hisDataMap.put("asset_id"			, asset_id);
				hisDataMap.put("chg_his_seq"	, chg_his_seq);
				hisDataMap.put("seq"				, "1");
				hisDataMap.put("chg_table"		, genTableNm);
				hisDataMap.put("chg_column_id", colId);
				hisDataMap.put("curValue", curValue);
				
				opmsDetailDAO.updateAmAssetChgHisModYn(hisDataMap);
			}
		}
	}
	
	*//**
	 * 설치 정보 가져온다.
	 * @param dataMap
	 * @throws NkiaException
	 *//*
	@Override
	public void deleteInstallInfo(HashMap dataMap)throws NkiaException{
		String query = opmsUtil.createInstallDelQuery(dataMap);
		opmsDetailDAO.excuteCudQuery(query);
	}
	
	*//**
	 * 설치 정보 등록
	 * @param dataMap
	 * @param installColList
	 * @throws NkiaException
	 *//*
	@Override
	public void insertInstallInfo(HashMap dataMap,List<HashMap> installColList)throws NkiaException{
		String query = opmsUtil.createInstallInsQuery(dataMap,installColList);
		opmsDetailDAO.excuteCudQuery(query);
	}
	
	*//**
	 * 그룹에 관련된 테이블의 데이터를 히스토리 테이블에 모두 넣어준다.. 
	 * @param dataMap
	 * @throws NkiaException
	 *//*
	@Override
	public void insertPrevDataHis(HashMap dataMap)throws NkiaException{
		
		String chg_his_seq 	= StringUtil.parseString(dataMap.get("chg_his_seq"));
		String conf_id 			= StringUtil.parseString(dataMap.get("conf_id"));
		String asset_id 		= StringUtil.parseString(dataMap.get("asset_id"));
		String user_id 			= StringUtil.parseString(dataMap.get("user_id"));
		
		//각그룹에 해당되는 테이블 리스트 목록을 가져온다.
		List<HashMap> entityTblList = opmsDetailDAO.selectEntityManualTable(dataMap);
		
		if(entityTblList != null){
			for(int i=0;i<entityTblList.size();i++){
				
				List<HashMap> tblList = new ArrayList();
				
				HashMap tblDataMap = entityTblList.get(i);
				
				String gen_table_nm = StringUtil.parseString(tblDataMap.get("GEN_TABLE_NM"));
				dataMap.put("GEN_TABLE_NM", gen_table_nm);
				
				List<HashMap> colList = opmsDetailDAO.selectTabAllColumnsList(dataMap);
				
				tblList.add(dataMap);
				
				dataMap.put("tblList", tblList);
				dataMap.put("sel_type", "SKEYID");
				
				List<HashMap> assetConfIdList	=  opmsDetailDAO.selectTypeColumn(dataMap);
				
				String query = opmsUtil.createSelectQuery(colList,null,tblList,null,assetConfIdList,dataMap);
				
				HashMap resultMap  = opmsDetailDAO.excuteQuery(query);
				
				for(int j=0;j<colList.size();j++){
					HashMap colDataMap = colList.get(j);
					
					String mkCompNm	= StringUtil.parseString(colDataMap.get("MK_COMP_NM"));
					String colId			= StringUtil.parseString(colDataMap.get("COL_ID"));
					String colNm		= StringUtil.parseString(colDataMap.get("COL_NAME"));
					String hystoryYn 	= StringUtil.parseString(colDataMap.get("HISTORY_YN"));
					String genTableNm= StringUtil.parseString(colDataMap.get("GEN_TABLE_NM"));
					String htmlType 	= StringUtil.parseString(colDataMap.get("HTML_TYPE"));
					String defaultVal 	= StringUtil.parseString(colDataMap.get("DEFAULT_VALUE"));
					
					if(resultMap != null){
						String value		= StringUtil.parseString(resultMap.get(mkCompNm));
						
						if("".equals(defaultVal)){
							HashMap hisDataMap = new HashMap();
							
							hisDataMap.put("conf_id"			, conf_id);
							hisDataMap.put("asset_id"			, asset_id);
							hisDataMap.put("user_id"			, user_id);
							hisDataMap.put("chg_his_seq"	, chg_his_seq);
							hisDataMap.put("chg_cud"			, "U");
							hisDataMap.put("code_yn"			, "N");
							hisDataMap.put("chg_yn"			, "N");
							hisDataMap.put("seq"				, "1");
							hisDataMap.put("chg_table"		, genTableNm);
							hisDataMap.put("chg_column_nm", colNm);
							hisDataMap.put("chg_column_id", colId);
							hisDataMap.put("chg_prev_value", value);
							
							if("DATE".equalsIgnoreCase(htmlType)){
								value = StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.replaceAll(value, "-", "")," ",""),":","");
							}else if("COMBO".equalsIgnoreCase("HTML_TYPE") 
									|| "CHECK".equalsIgnoreCase("HTML_TYPE") 
									|| "RADIO".equalsIgnoreCase("HTML_TYPE")){
								hisDataMap.put("code_yn"	, "Y");
							}
							
							int cnt = opmsDetailDAO.selectAmAssetChgHisCnt(hisDataMap);
							
							if(cnt == 0){
								opmsDetailDAO.insertAmAssetChgHis(hisDataMap);
							}
						}
					}
				}
			}
		}
		
		
		List<HashMap> componentList = opmsDetailDAO.selectEntityCompList(dataMap);
		
		if(componentList != null){
			for(int i=0;i<componentList.size();i++){
				
				HashMap compInfo = componentList.get(i);
				String compCode = StringUtil.parseString(compInfo.get("COMP_CODE"));
				
				opmsUtil.replaceAddRowerKey(compInfo);
				
				if ("MANAGER".equalsIgnoreCase(compCode)) {
					opmsDetailDAO.insertCopyOperHis(dataMap);
				} else if ("REL_SERVICE".equalsIgnoreCase(compCode)) {
					opmsDetailDAO.insertCopyServiceHis(dataMap);
				} else if ("USER".equalsIgnoreCase(compCode)) {
					opmsDetailDAO.insertCopyServiceHis(dataMap);
				} else if ("REL_INFRA".equalsIgnoreCase(compCode)) {
					//opmsDetailDAO.insertCopyHaHist(dataMap);
					//opmsDetailDAO.insertCopyLogSvHist(dataMap);
					opmsDetailDAO.insertCopyRelConfHis(dataMap);
					
					// 20150810 정정윤 추가 - 연관장비 이력 등록 시, 상대방 구성에도 수정이력 쌓이도록
					List targetConfList = opmsDetailDAO.selectCopyRelConfTargetList(dataMap);
					
					for(int j=0; j<targetConfList.size(); j++) {
						HashMap targetMap = new HashMap();
						HashMap subDataMap = new HashMap();
						targetMap = (HashMap)targetConfList.get(j);
						
						subDataMap.put("asset_id", targetMap.get("ASSET_ID"));
						subDataMap.put("conf_id", targetMap.get("CONF_ID"));
						subDataMap.put("chg_his_seq", dataMap.get("chg_his_seq"));
						subDataMap.put("user_id", dataMap.get("user_id"));
						subDataMap.put("cur_rel_conf_id", dataMap.get("conf_id"));
						subDataMap.put("target_chg_his_seq", dataMap.get("chg_his_seq"));
						
						opmsDetailDAO.insertCopyRelConfHis(subDataMap);
						opmsDetailDAO.insertChgHisSeq(subDataMap);
					}
					
				} else if ("INSTALL".equalsIgnoreCase(compCode)) {
					
					List<HashMap> installList = opmsDetailDAO.selectOpmsCompInstallList(compInfo);
					
					for(int j=0;j<installList.size();j++){
						HashMap installComp = installList.get(j);
						String comp_id = StringUtil.parseString(installComp.get("CODE_ID"));
						
						HashMap installInfo = opmsDetailDAO.selectOpmsInstallInfo(comp_id);
						
						opmsUtil.paramSync(dataMap, installInfo);
						
						installInfo.put("ins_type", comp_id);
						List<HashMap> installColList = opmsDetailDAO.selectOpmsInstallColumnList(comp_id);
						List<HashMap> prevDataList = selectOpmsInstallInfoList(installInfo);
						
						String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
						String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
						String dcaShowYn =  StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
						String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
						String tableNm	= StringUtil.parseString(installInfo.get("TABLE_NM"));
						
						if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
							//ems로 다이렉트로 보여줄경우..
							for(int k=0;k<prevDataList.size();k++){
								
								HashMap preDataMap = (HashMap)prevDataList.get(k);
								HashMap hisDataMap = new HashMap();
								
								hisDataMap.put("conf_id"			, conf_id);
								hisDataMap.put("asset_id"			, asset_id);
								hisDataMap.put("user_id"			, user_id);
								hisDataMap.put("chg_his_seq"		, chg_his_seq);
								hisDataMap.put("chg_cud"			, "U");
								hisDataMap.put("seq"				, j+1);
								
								for(int l=0;l<installColList.size();l++){
									HashMap installColMap = installColList.get(l);
									
									String colId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
									String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
									
									hisDataMap.put("code_yn"		, "N");
									hisDataMap.put("chg_yn"			, "N");
									hisDataMap.put("chg_table"		, emsTblNm);
									hisDataMap.put("chg_column_nm"	, colNm);
									hisDataMap.put("chg_column_id"	, colId);
									hisDataMap.put("chg_prev_value"	, preDataMap.get(colId));
								}
								opmsDetailDAO.insertAmAssetChgHis(hisDataMap);
							}
						}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
							//DCA로 다이렉트로 보여줄경우..
							for(int k=0;k<prevDataList.size();k++){
								
								HashMap preDataMap = (HashMap)prevDataList.get(k);
								HashMap hisDataMap = new HashMap();
								
								hisDataMap.put("conf_id"			, conf_id);
								hisDataMap.put("asset_id"			, asset_id);
								hisDataMap.put("user_id"			, user_id);
								hisDataMap.put("chg_his_seq"		, chg_his_seq);
								hisDataMap.put("chg_cud"			, "U");
								hisDataMap.put("seq"				, j+1);
								
								for(int l=0;l<installColList.size();l++){
									HashMap installColMap = installColList.get(l);
									
									String colId = StringUtil.parseString(installColMap.get("DCA_COL_ID"));
									String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
									
									hisDataMap.put("code_yn"		, "N");
									hisDataMap.put("chg_yn"			, "N");
									hisDataMap.put("chg_table"		, emsTblNm);
									hisDataMap.put("chg_column_nm"	, colNm);
									hisDataMap.put("chg_column_id"	, colId);
									hisDataMap.put("chg_prev_value"	, preDataMap.get(colId));
								}
								opmsDetailDAO.insertAmAssetChgHis(hisDataMap);
							}
						}else{
							for(int k=0;k<prevDataList.size();k++){
								
								HashMap preDataMap = (HashMap)prevDataList.get(k);
								HashMap hisDataMap = new HashMap();
								
								hisDataMap.put("conf_id"			, conf_id);
								hisDataMap.put("asset_id"			, asset_id);
								hisDataMap.put("user_id"			, user_id);
								hisDataMap.put("chg_his_seq"		, chg_his_seq);
								hisDataMap.put("chg_cud"			, "U");
								hisDataMap.put("seq"				, k+1);
							
								for(int l=0;l<installColList.size();l++){
									
									HashMap installColMap = installColList.get(l);
									
									String colId = StringUtil.parseString(installColMap.get("COL_ID"));
									String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
									String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
									String curVal = "";
									String preVal = "";
									
									if("combo".equalsIgnoreCase(xtype)){
										hisDataMap.put("code_yn"			, "Y");
									}else{
										hisDataMap.put("code_yn"			, "N");
									}
									
									hisDataMap.put("chg_yn"			, "N");
									hisDataMap.put("chg_table"		, tableNm);
									hisDataMap.put("chg_column_nm"	, colNm);
									hisDataMap.put("chg_column_id"	, colId);
									hisDataMap.put("chg_prev_value"	, preDataMap.get(colId));
									
									opmsDetailDAO.insertAmAssetChgHis(hisDataMap);
								}
							}
						}
						dataMap.put("PRE_INS_"+comp_id, installInfo);
					}
					
				} else if("CONFNMCODE".equalsIgnoreCase(compCode)){
					
					HashMap prevDataMap = opmsDetailDAO.selectOpmsConfNmCodeInfo(dataMap);
					
					if(prevDataMap != null){
						
						List<HashMap> attrList = opmsUtil.getConfNmCodeResourceList();
						
						for(int j=0;j<attrList.size();j++){
							HashMap headerMap = attrList.get(j);
							String key = StringUtil.parseString(headerMap.get("COL_ID"));
							String label = StringUtil.parseString(headerMap.get("COL_NAME"));
							String preValue = StringUtil.parseString(prevDataMap.get(key));
								
							HashMap hisDataMap = new HashMap();
							hisDataMap.put("conf_id"			, conf_id);
							hisDataMap.put("asset_id"			, asset_id);
							hisDataMap.put("user_id"			, user_id);
							hisDataMap.put("chg_his_seq"		, chg_his_seq);
							hisDataMap.put("chg_cud"			, "U");
							hisDataMap.put("code_yn"			, "N");
							hisDataMap.put("seq"				, "1");
							hisDataMap.put("chg_table"		, "AM_CONF_NM_CODE");
							hisDataMap.put("chg_column_nm",label);
							hisDataMap.put("chg_column_id" , key);
							hisDataMap.put("chg_prev_value", preValue);
							hisDataMap.put("chg_yn"		, "N");
							
							opmsDetailDAO.insertAmAssetChgHis(hisDataMap);
						}
					}
				} 
			}
		}
		
	}
	
	@Override
	public void registOpmsDetail(HashMap paramMap)throws NkiaException{
		
		String user_id 		= StringUtil.parseString(paramMap.get("user_id"));
		String class_id 	= StringUtil.parseString(paramMap.get("class_id"));
		String entity_class_id 	= StringUtil.parseString(paramMap.get("entity_class_id"));
		String tangible_asset_yn 	= StringUtil.parseString(paramMap.get("tangible_asset_yn"));
		String file_id	 	= StringUtil.parseString(paramMap.get("atch_file_id"));
		String regist_type	 	= StringUtil.parseString(paramMap.get("regist_type"));
		String physi_asset_id	 	= StringUtil.parseString(paramMap.get("physi_asset_id"));
		String physi_conf_id	 	= StringUtil.parseString(paramMap.get("physi_conf_id"));
		
		selectClassInfo(paramMap); //클래스 정보
		String class_type = StringUtil.parseString(paramMap.get("class_type"));
		
		if("SV".equals(class_type) && "N".equalsIgnoreCase(tangible_asset_yn)){
			paramMap.put("class_type", "SL");
		}
		createConfId(paramMap); //구성ID 생성
		paramMap.put("class_type", class_type);
		
		if("PHYSI".equalsIgnoreCase(regist_type)){
			createAssetId(paramMap); //자산ID 생성
		}
		
		String asset_id = StringUtil.parseString(paramMap.get("asset_id"));
		String asset_id_hi = StringUtil.parseString(paramMap.get("asset_id_hi"));
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		List formItemIds	= (List)paramMap.get("formItemIds");
		
		if(formItemIds != null){
		
			for(int i=0;i<formItemIds.size();i++){
				
				HashMap dataMap = (HashMap)formItemIds.get(i);
				
				String entity_id	= StringUtil.parseString(dataMap.get("entity_id"));
				String comp_id		= StringUtil.parseString(dataMap.get("comp_id"));
				String type 		= StringUtil.parseString(dataMap.get("type"));
				String entity_type= StringUtil.parseString(dataMap.get("entity_type"));
				
				dataMap.put("asset_id", asset_id);
				dataMap.put("asset_id_hi", asset_id_hi);
				dataMap.put("conf_id", conf_id);
				dataMap.put("class_id", class_id);
				dataMap.put("entity_class_id", entity_class_id);
				dataMap.put("tangible_asset_yn", tangible_asset_yn);
				dataMap.put("user_id", user_id);
				dataMap.put("physi_conf_id", physi_conf_id);
				dataMap.put("ENTITY_ID", entity_id);
				
				
				int authCnt	 = opmsDetailDAO.selectUpdateAuthCnt(dataMap);
				
				if("F".equalsIgnoreCase(type)){
					
					HashMap currentData 		= (HashMap)paramMap.get(comp_id);
					// 논리서버 등록의 경우 PHYSI_CONF_ID  값 수동 세팅
					if("SV".equals(class_type) && "N".equalsIgnoreCase(tangible_asset_yn)){
						if(currentData.containsKey("PHYSI_CONF_ID")){
							currentData.put("PHYSI_CONF_ID", physi_conf_id);
						}
					}
					
					List<HashMap> attrList 		= opmsDetailDAO.selectManualAttrList(dataMap);
					attrList 					= selectOpmsMnualAllList(attrList,dataMap);
					List<HashMap> hiddenList 	= selectOpmsHiddenList(attrList,dataMap);
					
					//compareDetailData(attrList,dataMap,currentData);
					
					if(authCnt>0){
						updateMnualCompData(attrList,hiddenList,dataMap,currentData);
					}
					
				}else if("SL".equalsIgnoreCase(type)){ // 싱글 리스트
					if("MANAGER".equalsIgnoreCase(entity_type)){
						List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
						if(authCnt>0){
							opmsDetailDAO.deleteOper(dataMap);
							if(currentData != null){
								for(int j=0;j<currentData.size();j++){
									HashMap curMngMap = currentData.get(j);
									curMngMap.put("SEQ", j+1);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									*//**
									 * 정/부 여부 삭제하고 담당구분(OPER_USER_TYPE) 추가 -2016.12.20 좌은진
									 *//*
//									if(curMngMap.get("MAIN_YN").toString() == "true"){
//										curMngMap.put("MAIN_YN", "Y");
//									}else{
//										curMngMap.put("MAIN_YN", "N");
//									}
									curMngMap.put("OPER_USER_TYPE", curMngMap.get("OPER_USER_TYPE"));
									opmsDetailDAO.insertOper(curMngMap);
								}
							}
						}
					}else if("REL_SERVICE".equalsIgnoreCase(entity_type)){
						List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
						if(authCnt>0){
							opmsDetailDAO.deleteService(dataMap);
							if(currentData != null){
								for(int j=0;j<currentData.size();j++){
									HashMap curMngMap = currentData.get(j);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									opmsDetailDAO.insertService(curMngMap);
								}
							}
						}
					}else if("INSTALL_SW".equalsIgnoreCase(entity_type)){
						// regist Opms
						// 수정될 데이터
						List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
						
						// 현재 데이터의 OPTION_ID_LIST 따로 빼 놓는다
						if(authCnt>0){
							
							//기존 데이터 삭제
							opmsDetailDAO.deleteInsSwOption(dataMap); // 해당 conf_id의 옵션 전체 삭제
							opmsDetailDAO.deleteInsSw(dataMap); //TODO 기존 데이터 삭제 쿼리 작성
							
							if(currentData != null){
								
								for(int j=0;j<currentData.size();j++){
									
									//등록
									HashMap curMngMap = currentData.get(j);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									
									opmsDetailDAO.insertInsSw(curMngMap); // 설치 소프트웨어 목록을 insert 해준다.
									
									List curOptionIdList = null;
									
									// OPTION_ID_LIST Key 가 존재한다면
									if( curMngMap.get("OPTION_ID_LIST") != null ){
										
										// OPTION_ID_LIST의 데이터가 리스트형 일 때
										if( curMngMap.get("OPTION_ID_LIST") instanceof ArrayList){
											
											// 해당 옵션ID 들로 리스트를 만들어 주고
											curOptionIdList = (ArrayList)curMngMap.get("OPTION_ID_LIST");
											
											// 만들어진 리스트에 값이 들어 있다면
											if( curOptionIdList != null && curOptionIdList.size() > 0 ){
												
												// 해당 리스트의 길이(설치 소프트웨어가 가지고 있는 옵션의 수)만큼 다시 insert 해준다.
												for(int z=0; z<curOptionIdList.size(); z++) {
													
													curMngMap.put("OPTION_ID", curOptionIdList.get(z));
													opmsDetailDAO.insertInsSwOption(curMngMap);
												}
											}
										}
									} 
									
								}
							}
						}
					}
				}else if("TL".equalsIgnoreCase(type)){//tab 리스트
					
					if("REL_INFRA".equalsIgnoreCase(entity_type)){
						dataMap.put("class_type", comp_id);
						if(comp_id.equals("LOGSV")){
							if(!physi_conf_id.isEmpty()){
								dataMap.put("physi_conf_id", physi_conf_id);
								opmsDetailDAO.insertLogSvRelConf(dataMap);
							}
						}else{
							List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
							if(authCnt>0){
								//파라메터 대소문자 관련해서 문제가 있습니다...
								dataMap.put("main_class_type", class_type);
								opmsDetailDAO.deleteRelConf(dataMap);
								if(currentDataList != null){
									for(int j=0;j<currentDataList.size();j++){
										HashMap curRelConfMap = currentDataList.get(j);
										curRelConfMap.put("MAIN_CONF_ID", conf_id);
										curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
										opmsDetailDAO.insertRelConf(curRelConfMap);
									}
								}
							}
							
						}
					}else if("INSTALL".equalsIgnoreCase(entity_type)){
						
						dataMap.put("ins_type", comp_id);
						HashMap installInfo = opmsDetailDAO.selectOpmsInstallInfo(comp_id);
						List<HashMap> installColList = opmsDetailDAO.selectOpmsInstallColumnList(comp_id);
						List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
						
						String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
						String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
						String dcaShowYn =  StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
						String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
						String tableNm	= StringUtil.parseString(installInfo.get("TABLE_NM"));						
						
						if(authCnt>0 && emsShowYn.equalsIgnoreCase("N") && dcaShowYn.equalsIgnoreCase("N")){
							//파라메터 대소문자 관련해서 문제가 있습니다...
							installInfo.put("CONF_ID", conf_id);
							deleteInstallInfo(installInfo);
							
							if(currentDataList != null){
								for(int j=0;j<currentDataList.size();j++){
									HashMap curInstallMap = currentDataList.get(j);
									curInstallMap.put("CONF_ID", conf_id);
									curInstallMap.put("TABLE_NM", tableNm);
									insertInstallInfo(curInstallMap,installColList);
								}
							}
						}
					}
				}else if("CF".equalsIgnoreCase(type)){
					if("CONFNMCODE".equalsIgnoreCase(entity_type)){
						HashMap curDataMap = (HashMap)paramMap.get(comp_id);
						if(authCnt>0){
							//파라메터 대소문자 관련해서 문제가 있습니다...
							int cnt = opmsDetailDAO.selectOpmsConfNmCodeCnt(dataMap);
							if(curDataMap != null){
								curDataMap.put("ASSET_ID", asset_id);
								curDataMap.put("USER_ID", user_id);
								if(cnt>0){
									opmsDetailDAO.updateOpmsConfNmCode(curDataMap);
								}else{
									opmsDetailDAO.insertOpmsConfNmCode(curDataMap);
								}
							}
						}
					}
				}
			}
			// 2016.12.08.정정윤 추가 - 가용성, 기밀성, 무결성 값 합산하여 중요도 값 업데이트
			opmsDetailDAO.updateImportanceByAvCnIn(paramMap);
			
			exceptionInsertHandler(paramMap);
		}
	}
	
	@Override
	public void exceptionInsertHandler(HashMap paramMap)throws NkiaException{
		opmsDetailDAO.updateAtchFileId(paramMap);
		opmsDetailDAO.updateBaseDataInfo(paramMap);
		opmsDetailDAO.updateBaseLogSvInfo(paramMap);
	}
	
	@Override
	public void createConfId(HashMap paramMap)throws NkiaException{
		String conf_id = opmsDetailDAO.createConfId(paramMap);
		paramMap.put("conf_id", conf_id);
	}
	
	@Override
	public void createAssetId(HashMap paramMap)throws NkiaException{
		String asset_id = opmsDetailDAO.createAssetId(paramMap);
		paramMap.put("asset_id", asset_id);
	}
	
	@Override
	public void selectClassInfo(HashMap paramMap)throws NkiaException{
		HashMap classInfoMap = opmsDetailDAO.selectClassInfo(paramMap);
		String classType = StringUtil.parseString(classInfoMap.get("CLASS_TYPE"));
		paramMap.put("class_type", classType);
	}
	
	@Override
	public List selectConfClassTree(HashMap paramMap)throws NkiaException{
		return opmsDetailDAO.selectConfClassTree(paramMap);
	}

	*//**
	 * 
	 * 가상서버(클라우드와 연계된 가상서버) 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectOpmsVmList(ModelMap paramMap) throws NkiaException {
		return opmsDetailDAO.selectOpmsVmList(paramMap);
	}

	*//**
	 * 폼 관련 속성 이력정보를 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectOpmsFormModList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		String comp_type = StringUtil.parseString(paramMap.get("comp_type")); 
		List<HashMap> attrList = new ArrayList<HashMap>();
		List<HashMap> tblList = new ArrayList<HashMap>();
		
		//장비 명명 정보
		if("CONFNMCODE".equalsIgnoreCase(comp_type)){
			//컴포넌트 폼으로 만들어진 겁니다.. 메가센터에만 있습니다.
			attrList	= opmsUtil.getConfNmCodeResourceList();
			HashMap tblMap = new HashMap();
			tblMap.put("GEN_TABLE_NM", "AM_CONF_NM_CODE");
			tblList.add(tblMap);
		}else{//매뉴얼폼..
			attrList	=opmsDetailDAO.selectManualAttrList(paramMap);
			//All 인경우 다시 속성 리스트를 가져온다.권한 설정도 동시에 이루어짐..
			attrList	=selectOpmsMnualAllList(attrList,paramMap);
			//테이블 리스트를 가져온다.
			tblList		=opmsDetailDAO.selectTblList(paramMap);
		}
		
		paramMap.put("attrList", attrList);
		paramMap.put("tblList", tblList);
		
		return opmsDetailDAO.selectOpmsFormModList(paramMap);
		
		
	}


	*//**
	 * 
	 * TODO 컴포넌트 탭그리드 나 싱글 그리드를 모두 모아서 데이터를 보여준다..
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectOpmsCompModList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List<HashMap> compList = (List<HashMap>)paramMap.get("compItems");
		List<HashMap> modHistoryList = new ArrayList<HashMap>();
		if(compList != null){
			for(int i=0;compList.size()>i;i++){
				HashMap compMap = compList.get(i);
				opmsUtil.paramSync(paramMap, compMap);
				selectOpmsCompTypeModList(modHistoryList,compMap);
			}
		}
		return modHistoryList;
	}
	
	*//**
	 * 
	 * TODO 커포넌트 별로 수정이력을 모아서 넣는다..
	 * @param modHistoryList
	 * @param compMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectOpmsCompTypeModList(List<HashMap> modHistoryList,HashMap compMap)throws NkiaException{
		
		String entity_id = StringUtil.parseString(compMap.get("ENTITY_ID"));
		String comp_type = StringUtil.parseString(compMap.get("COMP_TYPE"));
		
		compMap.put("entity_id", entity_id);
		
		List<HashMap> resultList = new ArrayList<HashMap>();
		
		if ("MANAGER".equalsIgnoreCase(comp_type)) {
			//담당자 수정이력을 가져옫나.
			resultList = opmsDetailDAO.selectOperModHistoryList(compMap);
			opmsUtil.addTargetList(resultList,modHistoryList);
		} else if ("USER".equalsIgnoreCase(comp_type)) {
			//연관 서비스 수정리스트를 가져온다.
			resultList = opmsDetailDAO.selectServiceModHistoryList(compMap);
			opmsUtil.addTargetList(resultList,modHistoryList);
		} else if ("REL_SERVICE".equalsIgnoreCase(comp_type)) {
			//연관 서비스 수정리스트를 가져온다.
			resultList = opmsDetailDAO.selectServiceModHistoryList(compMap);
			opmsUtil.addTargetList(resultList,modHistoryList);
		} else if ("REL_INFRA".equalsIgnoreCase(comp_type)) {
			//설정된 리스트를 가져온다.
			List<HashMap> attrList = selectOpmsCompSetupList(compMap);
			
			for(int i=0;i<attrList.size();i++){
				//해당 분류별 수정이력을 가져온다.
				HashMap attrMap = attrList.get(i);
				String class_type = StringUtil.parseString(attrMap.get("CODE_ID"));
				String class_type_nm = StringUtil.parseString(attrMap.get("CODE_NM"));
				
				compMap.put("CLASS_TYPE", class_type);
				compMap.put("CLASS_TYPE_NM", class_type_nm);
				
				if("LOGSV".equalsIgnoreCase(class_type)){
					//논리는 따로 관리합니다.
				}else if("HA".equalsIgnoreCase(class_type)){
					//이중화는 사용하지 않음
				}else{
					//서버, 네트워크, 보안, 백업, 스토리지 등.. am_conf_rel 에 들어가는 이력 리스트
					resultList = opmsDetailDAO.selectConfModHistoryList(compMap);
				}
				opmsUtil.addTargetList(resultList,modHistoryList);
			}
		} else if ("INSTALL".equalsIgnoreCase(comp_type)) {
			//설치정보 설정 정보를 가져온다.
			List<HashMap> attrList = selectOpmsCompSetupList(compMap);
			
			for(int i=0;i<attrList.size();i++){
				
				HashMap attrMap = attrList.get(i);
				//설치 분류 유형
				String ins_type = StringUtil.parseString(attrMap.get("CODE_ID"));
				//설치 분류 명
				String ins_type_nm = StringUtil.parseString(attrMap.get("CODE_NM"));
				//테이블 명 .. ems 연계 정보로 보여줄때는.. ems 테이블로 보여준다..
				String table_nm = StringUtil.parseString(attrMap.get("TABLE_NM"));
				
				compMap.put("COMP_ID", ins_type);
				compMap.put("COMP_NM", ins_type_nm);
				compMap.put("TABLE_NM", table_nm);
				
				//수정이력 리스트를 가져온다.
				resultList = opmsDetailDAO.selectInstallModHistoryList(compMap);
				
				opmsUtil.addTargetList(resultList,modHistoryList);
			}
		}
		
		return modHistoryList;
	}
	
	*//**
	 * 속성별 상세 이력 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectFormAttrModList(HashMap paramMap)throws NkiaException{
		// TODO Auto-generated method stub
		//모든키를 소문자로 변환해서 추가한다.
		opmsUtil.replaceAddRowerKey(paramMap);
		
		String comp_type = StringUtil.parseString(paramMap.get("COMP_TYPE")); 
		List<HashMap> attrList = new ArrayList<HashMap>();
		List<HashMap> tblList = new ArrayList<HashMap>();
		//장비 명명 정보
		if("CONFNMCODE".equalsIgnoreCase(comp_type)){
			//컴포넌트 폼으로 만들어진 겁니다.. 메가센터에만 있습니다.
			attrList	= opmsUtil.getConfNmCodeResourceList();
			HashMap tblMap = new HashMap();
			tblMap.put("GEN_TABLE_NM", "AM_CONF_NM_CODE");
			tblList.add(tblMap);
		}else{//매뉴얼폼..
			attrList	=opmsDetailDAO.selectManualAttrList(paramMap);
			//All 인경우 다시 속성 리스트를 가져온다.권한 설정도 동시에 이루어짐..
			attrList	=selectOpmsMnualAllList(attrList,paramMap);
			//테이블 리스트를 가져온다.
			tblList		=opmsDetailDAO.selectTblList(paramMap);
		}
		paramMap.put("attrList", attrList);
		paramMap.put("tblList", tblList);
		
		return opmsDetailDAO.selectFormAttrModList(paramMap);
	}

	@Override
	public JSONArray selectCompDataColumn(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		String comp_type = StringUtil.parseString(paramMap.get("COMP_TYPE"));
		String tab_type = StringUtil.parseString(paramMap.get("TAB_TYPE"));
		
		JSONArray defaultColArr = opmsUtil.getGridResource("grid.itam.compModCompHisList");
		JSONArray detailColArr = new JSONArray();
		JSONArray returnColArr = new JSONArray();
		
		if("MANAGER".equalsIgnoreCase(comp_type)){
			//담당자
			detailColArr = opmsUtil.getGridResource("grid.itam.managerList");
		}else if("REL_SERVICE".equalsIgnoreCase(comp_type)){
			//연관서비스
			detailColArr = opmsUtil.getGridResource("grid.itam.relService");
		}else if("USER".equalsIgnoreCase(comp_type)){
			//연관서비스
			detailColArr = opmsUtil.getGridResource("grid.itam.userList");
		}else if("REL_INFRA".equalsIgnoreCase(comp_type)){
			//연관 장비
			detailColArr = opmsUtil.getGridResource("grid.itam.relConf"+tab_type);
		}else if("INSTALL".equalsIgnoreCase(comp_type)){
			//설치정보
			List installColList = opmsDetailDAO.selectOpmsInstallColumnList(tab_type);
			detailColArr = opmsUtil.getGridResourceByList(installColList,"COL_ID","COL_NM");
		}
		
		returnColArr = opmsUtil.getMergeGridResource(defaultColArr,detailColArr,"[REPLACE_COL_DAT]");
		
		return returnColArr;
	}

	@Override
	public List selectCompDetailModList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		
		opmsUtil.replaceAddRowerKey(paramMap);
		
		String comp_type = StringUtil.parseString(paramMap.get("COMP_TYPE"));
		String tab_type = StringUtil.parseString(paramMap.get("TAB_TYPE"));
		
		List<HashMap> resultList = new ArrayList<HashMap>();
		
		if("MANAGER".equalsIgnoreCase(comp_type)){
			//담당자 수정이력
			resultList = opmsDetailDAO.selectOperModDetailList(paramMap);
		}else if("REL_SERVICE".equalsIgnoreCase(comp_type)){
			//연관 서비스 수정이력
			resultList = opmsDetailDAO.selectRelServiceModDetailList(paramMap);
		}else if("USER".equalsIgnoreCase(comp_type)){
			//연관 서비스 수정이력
			resultList = opmsDetailDAO.selectRelServiceModDetailList(paramMap);
		}else if("REL_INFRA".equalsIgnoreCase(comp_type)){
			//연관 구성장비 수정이력
			resultList = opmsDetailDAO.selectRelConfHisDetailList(paramMap);
		}else if("INSTALL".equalsIgnoreCase(comp_type)){
			//해당 컬럼 리스트를 가져온다.
			List installColList = opmsDetailDAO.selectOpmsInstallColumnList(tab_type);
			//설치 정보 이력을 가져오기 위한 쿼리를 만든다.
			String installColQuery = opmsUtil.createInstallHisQuery(installColList);
			paramMap.put("installColQuery", installColQuery);
			
			//수정이력 리스트를 가져온다.
			resultList = opmsDetailDAO.selectInstallHisDetailList(paramMap);
		}
		
		return resultList;
	}

	
	*//**
	 * 정보수집서 데이터 등록 및 수정
	 * @param dataMap
	 * @throws NkiaException
	 *//*
	@Override
	public void insertOpmsInfoCollection(HashMap dataMap)throws NkiaException{
		String user_id 				= StringUtil.parseString(dataMap.get("user_id"));
		String class_id 			= StringUtil.parseString(dataMap.get("class_id"));
		String class_type 			= StringUtil.parseString(dataMap.get("class_type"));
		String entity_class_id 		= StringUtil.parseString(dataMap.get("entity_class_id"));
		String tangible_asset_yn 	= StringUtil.parseString(dataMap.get("tangible_asset_yn"));
		String file_id	 			= StringUtil.parseString(dataMap.get("atch_file_id"));
		String regist_type	 		= StringUtil.parseString(dataMap.get("regist_type"));
		String physi_asset_id	 	= StringUtil.parseString(dataMap.get("physi_asset_id"));
		String physi_conf_id	 	= StringUtil.parseString(dataMap.get("physi_conf_id"));
		String title	 			= StringUtil.parseString(dataMap.get("info_title"));
		Integer infoCnt	 			= StringUtil.parseInteger(dataMap.get("info_cnt"));
		String project_no	 		= StringUtil.parseString(dataMap.get("project_no"));
		String view_type	 		= StringUtil.parseString(dataMap.get("view_type"));
		String durable_year	 		= StringUtil.parseString(dataMap.get("durable_year"));
		List formItemIds			= (List)dataMap.get("formItemIds");
		String classTableNm = null;
		createTempSeq(dataMap);
		selectClassInfo(dataMap);	
		
		String temp_seq 	 = StringUtil.parseString(dataMap.get("temp_seq"));		
		
		//각그룹에 해당되는 테이블 리스트 목록을 가져온다.
		//List<HashMap> entityTblList = opmsDetailDAO.selectEntityManualTable(dataMap);
		
		if(view_type.equals("info")){			
			HashMap insSeqMap = new HashMap();
			insSeqMap.put("user_id"			, user_id);
			insSeqMap.put("class_id"		, class_id);
			insSeqMap.put("temp_seq"		, temp_seq);
			insSeqMap.put("title"			, title);
			insSeqMap.put("project_no"		, project_no);
			
			opmsDetailDAO.insertOpmsInfoSeq(insSeqMap);
		}else if(view_type.equals("infmodifiy")){
			ModelMap dataParam = new ModelMap();
			dataParam.put("asset_id", dataMap.get("asset_id"));
			dataParam.put("conf_id", dataMap.get("conf_id"));
			*//** DATA 제거 **//*
			opmsInfoDAO.deleteOpmsInfoData(dataParam);
			*//** MAPPING DATA 상태값 변경 REQUEST -> UPDATE **//*
			opmsInfoDAO.updateOpmsInfoMappingData(dataParam);
		}

		if(infoCnt.equals(0)){
			infoCnt =  1;
		}
		// 정보수집 갯수를 입력한 만큼 insert 함
		for(int l=0; l <infoCnt; l++){
			if(view_type.equals("info")){
				createConfTempId(dataMap);
				if("PHYSI".equalsIgnoreCase(regist_type)){
					createAssetTempId(dataMap);
				}				
			}else if(view_type.equals("infmodifiy")){
				dataMap.put("asset_temp_id", dataMap.get("asset_id"));
				dataMap.put("conf_temp_id", dataMap.get("conf_id"));
			}
			String conf_temp_id	 = StringUtil.parseString(dataMap.get("conf_temp_id"));
			String asset_temp_id = StringUtil.parseString(dataMap.get("asset_temp_id"));
			
			// MAPPING 테이블 등록
			if(view_type.equals("info")){
				HashMap mappingData = new HashMap();
				mappingData.put("temp_seq"			, temp_seq);
				mappingData.put("asset_temp_id"		, asset_temp_id);
				mappingData.put("conf_temp_id"		, conf_temp_id);
				mappingData.put("status"			, "REQUEST");
				
				opmsDetailDAO.insertOpmsInfoMapping(mappingData);
			}
			
			// 분류체계id 입력..
			HashMap classMap =  new HashMap();
			if(view_type.equals("info")){
				classMap.put("asset_temp_id"	, asset_temp_id);
				classMap.put("conf_temp_id"		, conf_temp_id);
			}else if(view_type.equals("infmodifiy")){
				classMap.put("asset_temp_id"	, dataMap.get("asset_id"));
				classMap.put("conf_temp_id"		, dataMap.get("conf_id"));							
			}
			classMap.put("ins_table_nm", "AM_INFRA");
			if(class_type.equals("SW") || dataMap.get("class_type").equals("SW")){
				classMap.put("ins_table_nm", "AM_SW");
			}
			classMap.put("ins_column_id", "CLASS_ID");
			classMap.put("ins_column_nm", "분류체계ID");
			classMap.put("value", class_id);
			classMap.put("user_id", user_id);
			opmsDetailDAO.insertOpmsInfoData(classMap);
			// 분류체계id 입력 끝..
			
			// 내용년수 입력..
			HashMap serviceLifeMap =  new HashMap();
			if(view_type.equals("info")){
				serviceLifeMap.put("asset_temp_id"		, asset_temp_id);
				serviceLifeMap.put("conf_temp_id"		, conf_temp_id);
			}else if(view_type.equals("infmodifiy")){
				serviceLifeMap.put("asset_temp_id"		, dataMap.get("asset_id"));
				serviceLifeMap.put("conf_temp_id"		, dataMap.get("conf_id"));							
			}				
			serviceLifeMap.put("ins_table_nm", "AM_ASSET");
			serviceLifeMap.put("ins_column_id", "SERVICE_LIFE");
			serviceLifeMap.put("ins_column_nm", "내용년수");
			serviceLifeMap.put("value", durable_year);
			serviceLifeMap.put("user_id", user_id);
			opmsDetailDAO.insertOpmsInfoData(serviceLifeMap);
			// 내용년수 입력 끝..
			
			for(int i = 0; i < formItemIds.size(); i++){
				HashMap getDataMap 	= (HashMap)formItemIds.get(i);
				
				String entity_id	= StringUtil.parseString(getDataMap.get("entity_id"));
				String comp_id		= StringUtil.parseString(getDataMap.get("comp_id"));
				String type 		= StringUtil.parseString(getDataMap.get("type"));
				String entity_type	= StringUtil.parseString(getDataMap.get("entity_type"));				
				
				//각그룹에 해당되는 테이블 리스트 목록을 가져온다.
				List<HashMap> tblList =	opmsDetailDAO.selectTblList(getDataMap);
				tblList.add(dataMap);
				
				HashMap insDataMap = new HashMap();
				if(view_type.equals("info")){
					insDataMap.put("asset_temp_id"		, asset_temp_id);
					insDataMap.put("conf_temp_id"		, conf_temp_id);
				}else if(view_type.equals("infmodifiy")){
					insDataMap.put("asset_temp_id"		, dataMap.get("asset_id"));
					insDataMap.put("conf_temp_id"		, dataMap.get("conf_id"));							
				}
				
				for(int j=0;j<tblList.size();j++){
					HashMap tblDataMap = tblList.get(j);
					String gen_table_nm = StringUtil.parseString(tblDataMap.get("GEN_TABLE_NM"));
					dataMap.put("GEN_TABLE_NM", gen_table_nm);
					if(!"AM_ASSET".equals(gen_table_nm) && !"AM_INFRA".equals(gen_table_nm)){
						classTableNm = gen_table_nm;
					}
					
					List<HashMap> colList = opmsDetailDAO.selectTabAllColumnsList(dataMap);
					
					for(int k=0;k<colList.size();k++){
						HashMap colDataMap = colList.get(k);
						
						String mkCompNm		= StringUtil.parseString(colDataMap.get("MK_COMP_NM"));
						String colId		= StringUtil.parseString(colDataMap.get("COL_ID"));
						String colNm		= StringUtil.parseString(colDataMap.get("COL_NAME"));
						String genTableNm	= StringUtil.parseString(colDataMap.get("GEN_TABLE_NM"));
						String htmlType 	= StringUtil.parseString(colDataMap.get("HTML_TYPE"));
						
						insDataMap.put("ins_table_nm"		, genTableNm);
						insDataMap.put("ins_column_id"		, colId);
						insDataMap.put("ins_column_nm"		, colNm);
						insDataMap.put("user_id"			, user_id);
						insDataMap.put("view_type"			, view_type);
						
						if("F".equalsIgnoreCase(type)){
							HashMap currentData = (HashMap)dataMap.get(comp_id);
							String value = StringUtil.parseString(currentData.get(mkCompNm));
							
							if("DATE".equalsIgnoreCase(htmlType)){
								value = StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.replaceAll(value, "-", "")," ",""),":","");
							}else if("COMBO".equalsIgnoreCase("HTML_TYPE") 
									|| "CHECK".equalsIgnoreCase("HTML_TYPE") 
									|| "RADIO".equalsIgnoreCase("HTML_TYPE")){
								insDataMap.put("code_yn"	, "Y");
							}
							
							if(!"".equals(value)){
								insDataMap.put("value", value);
								int dataCnt	 = opmsDetailDAO.selectDataInsertYn(insDataMap);
								if(dataCnt == 0){
									opmsDetailDAO.insertOpmsInfoData(insDataMap);									
								}
							}
						}else if("SL".equalsIgnoreCase(type)){
							if("MANAGER".equalsIgnoreCase(entity_type)){
								List<HashMap> currentData	= (List<HashMap>)dataMap.get(entity_id);
								
								if(currentData != null){
									for(int h=0;h<currentData.size();h++){
										HashMap curMngMap = currentData.get(h);
										curMngMap.put("asset_temp_id", asset_temp_id);
										curMngMap.put("conf_temp_id", conf_temp_id);
										curMngMap.put("cust_id", curMngMap.get("CUST_ID"));
										curMngMap.put("oper_user_id", curMngMap.get("USER_ID"));
										curMngMap.put("main_yn_val", curMngMap.get("MAIN_YN_VAL"));
										curMngMap.put("upd_user_id", user_id);
										curMngMap.put("oper_user_type", curMngMap.get("OPER_USER_TYPE"));
										int dataCnt	 = opmsDetailDAO.selectOpmsInfoOperInsertYn(curMngMap);
										if(dataCnt == 0){
											curMngMap.put("seq", dataCnt+1);
											opmsDetailDAO.insertOpmsInfoOper(curMngMap);
										}
									}
								}									
							}else if("REL_SERVICE".equalsIgnoreCase(entity_type)){
								List<HashMap> currentData	= (List<HashMap>)dataMap.get(entity_id);

								if(currentData != null){
									for(int h=0;h<currentData.size();h++){
										HashMap curMngMap = currentData.get(h);
										insDataMap.put("service_id", curMngMap.get("SERVICE_ID"));
										insDataMap.put("cust_id", curMngMap.get("CUST_ID"));
										int dataCnt	 = opmsDetailDAO.selectOpmsInfoServiceInsertYn(insDataMap);
										if(dataCnt == 0){
											opmsDetailDAO.insertOpmsInfoService(insDataMap);												
										}
									}
								}
							}
						}else if("TL".equalsIgnoreCase(type)){
							if("REL_INFRA".equalsIgnoreCase(entity_type)){
								List<HashMap> currentDataList = (List<HashMap>)dataMap.get(comp_id);

								if(currentDataList != null){
									//현제는 구현사항이 없음...
									if("LOGSV".equalsIgnoreCase(comp_id)){
									//현제는 구현상황이 없음..
									}else if("HA".equalsIgnoreCase(comp_id)){
										
									}else{
										for(int h=0;h<currentDataList.size();h++){
											HashMap curRelConfMap = currentDataList.get(h);
											curRelConfMap.put("CLASS_TYPE", comp_id);
											curRelConfMap.put("MAIN_CONF_ID", conf_temp_id);
											curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
											curRelConfMap.put("UPD_USER_ID", user_id);
											int dataCnt	 = opmsDetailDAO.selectOpmsInfoRelConfInsertYn(curRelConfMap);
											if(dataCnt == 0){												
												opmsDetailDAO.insertOpmsInfoRelConf(curRelConfMap);
											}
										}
									}
								}
							}else if("INSTALL".equalsIgnoreCase(entity_type)){
								
								dataMap.put("ins_type", comp_id);
								HashMap installInfo = opmsDetailDAO.selectOpmsInstallInfo(comp_id);
								List<HashMap> installColList = opmsDetailDAO.selectOpmsInstallColumnList(comp_id);
								List<HashMap> currentDataList = (List<HashMap>)dataMap.get(comp_id);
								
								String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
								String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
								String tableNm	= StringUtil.parseString(installInfo.get("INFO_TABLE_NM"));						
					
								if(emsShowYn.equalsIgnoreCase("N")){
									//파라메터 대소문자 관련해서 문제가 있습니다...
									installInfo.put("TABLE_NM", tableNm);
									installInfo.put("CONF_ID", conf_temp_id);
									installInfo.put("COL_NM", "CONF_TEMP_ID");
									deleteInstallInfo(installInfo);
									
									if(currentDataList != null){
										for(int h=0;h<currentDataList.size();h++){
											HashMap curInstallMap = currentDataList.get(h);
											curInstallMap.put("CONF_ID", conf_temp_id);
											curInstallMap.put("TABLE_NM", tableNm);
											int dataCnt	 = opmsDetailDAO.selectOpmsInfoInstallInsertYn(curInstallMap);
											if(dataCnt == 0){													
												insertInstallInfo(curInstallMap,installColList);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			// CONF_ID 입력(분류체계테이블 데이터 누락 방지용)
			HashMap confIdMap =  new HashMap();
			if(classTableNm != null){
				serviceLifeMap.put("asset_temp_id"		, asset_temp_id);
				serviceLifeMap.put("conf_temp_id"		, conf_temp_id);
				serviceLifeMap.put("ins_table_nm", classTableNm);
				serviceLifeMap.put("ins_column_id", "");
				serviceLifeMap.put("ins_column_nm", "");
				serviceLifeMap.put("user_id", user_id);
				opmsDetailDAO.insertOpmsInfoData(serviceLifeMap);
			}	
		}
	}
	
	
	@Override
	public void createTempSeq(HashMap paramMap)throws NkiaException{
		String temp_seq = opmsDetailDAO.createTempSeq(paramMap);
		paramMap.put("temp_seq", temp_seq);
	}
	
	@Override
	public void createConfTempId(HashMap paramMap)throws NkiaException{
		String conf_temp_id = opmsDetailDAO.createConfTempId(paramMap);
		paramMap.put("conf_temp_id", conf_temp_id);
	}
	
	@Override
	public void createAssetTempId(HashMap paramMap)throws NkiaException{
		String asset_temp_id = opmsDetailDAO.createAssetTempId(paramMap);
		paramMap.put("asset_temp_id", asset_temp_id);
	}	
	
	@Override
	public List selectOpmsBtnTree(HashMap paramMap)throws NkiaException{
		return opmsDetailDAO.selectOpmsBtnTree(paramMap);
	}
	*//**
	 * 모델속성을 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List searchModelTreeComp(ModelMap paramMap) throws NkiaException {
		return opmsDetailDAO.searchModelTreeComp(paramMap);
	}
	*//**
	 * 업무분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List searchWorkTreeComp(ModelMap paramMap) throws NkiaException {
		return opmsDetailDAO.searchWorkTreeComp(paramMap);
		
	}
	
	*//**
	 * HISOS를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List searchHISOSTreeComp(ModelMap paramMap) throws NkiaException {
		return opmsDetailDAO.searchHISOSTreeComp(paramMap);
		
	}
	
	*//**
	 * 설치된 소프트웨어 목록 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List selectOpmsInsSwList(HashMap paramMap) throws NkiaException {
		
		// 설치된 SW리스트
		List resultList = new ArrayList();
		
		// 선택한 1개의 SW에 맵핑된 옵션 테이블
		List optionList = new ArrayList();
		
		// 값을 새로 세팅해서 리턴할 리스트 
		List returnList = new ArrayList();
		
		// 먼저 설치된 SW 정보를 받아오고
		resultList = opmsDetailDAO.selectOpmsInsSwList(paramMap);
		
		// 1개의 SW마다 로직 실행
		for(int i=0; i<resultList.size(); i++) {
			
			HashMap returnMap = new HashMap();
			List optionIdList = new ArrayList();
			returnMap = (HashMap)resultList.get(i);
			
			// SW가 가지고 있는 옵션 테이블 조회
			optionList = selectSelectedOptionList(returnMap);
			returnMap.put("OPTION_ID_LIST", "");
			
			// 옵션테이블에 루프 돌려서
			for(int j=0; j<optionList.size(); j++) {
				
				HashMap optionMap = new HashMap();
				optionMap = (HashMap)optionList.get(j);
				
				// 옵션ID값만 가져와서 맵에 세팅
				optionIdList.add((String)optionMap.get("OPTION_ID"));
				returnMap.put("OPTION_ID_LIST", optionIdList);
			}
			
			// 리턴할 리스트에 맵 담아줌
			returnList.add(returnMap);
		}
		
		return returnList;
	}
	
	*//**
	 * 설치 소프트웨어와 맵핑되는 소프트웨어 옵션 전체 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public List selectOptionMappingList(HashMap paramMap) throws NkiaException {
		return opmsDetailDAO.selectOptionMappingList(paramMap);
		
	}
	
	*//**
	 * 설치 소프트웨어와 맵핑되는 선택되어있던 소프트웨어 옵션 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public List selectSelectedOptionList(HashMap paramMap) throws NkiaException {
		return  opmsDetailDAO.selectSelectedOptionList(paramMap);
		
	}

	*//**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public void deleteInsSwOption(HashMap paramMap)throws NkiaException{
		opmsDetailDAO.deleteInsSwOption(paramMap);
	}
	*//**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public void insertInsSwOption(HashMap paramMap)throws NkiaException{
		opmsDetailDAO.insertInsSwOption(paramMap);
	}
	
	*//**
	 * 유지보수 정보 폼 데이터를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public HashMap selectAssetMaintInfo(HashMap paramMap) throws NkiaException{
		return opmsDetailDAO.selectAssetMaintInfo(paramMap);
	}
	
	*//**
	 * 모자산 변경 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*	
	public int selectMotherListCnt(HashMap paramMap) throws NkiaException {
		return opmsDetailDAO.selectMotherListCnt(paramMap);
	}
	*//**
	 * 모자산 변경 팝업 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public List selectMotherList(HashMap paramMap) throws NkiaException {
		return opmsDetailDAO.selectMotherList(paramMap);
	}
	*//**
	 * 모자산 변경 정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public void updateMotherInfo(HashMap paramMap) throws NkiaException {
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String User_Id = (String)userVO.getUser_id();
		
		int paramsize = paramMap.size();
		HashMap param = null;
		boolean isSelSeq = false;
		int seq = 0;
		
		for( int i=0; i < paramsize; i++ ){
			
			param = (HashMap) paramMap.get(String.valueOf(i));
			
			// 이력테이블 insert용 시퀀스 세팅
			if(!isSelSeq) {
				String a_asset_id_hi = (String) param.get("ASSET_ID_HI");
				param.put("a_asset_id_hi", a_asset_id_hi);
				
				seq = opmsDetailDAO.selectChildChgHisSeq(param);
				isSelSeq = true;
			}
			
			HashMap assetMap = new HashMap();
			assetMap.put("asset_id", (String) param.get("ASSET_ID"));
			
			param.put("seq"				, seq);
			param.put("asset_id"		, (String) param.get("ASSET_ID"));
			param.put("a_asset_id_hi"	, (String) param.get("ASSET_ID_HI"));
			param.put("b_asset_id_hi"	, (String) param.get("ORI_ASSET_ID"));
			param.put("conf_nm"			, (String) param.get("CONF_NM"));
			param.put("user_id"			, User_Id);
			param.put("chg_cnt"			, opmsDetailDAO.selectChildChgHisCnt(assetMap));
			
			// 이력테이블에 insert
			insertChildChgHis(param);
			// 모자산 정보 변경 update
			opmsDetailDAO.updateMotherInfo(param);
		}
	}

	@Override
	public List selectAmIntroDtGroupBy(ModelMap paramMap) throws NkiaException {
		return opmsDetailDAO.selectAmIntroDtGroupBy(paramMap);
	} 
	
	*//**
	 * 자품 변경 이력 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public void insertChildChgHis(HashMap dataMap) throws NkiaException{
		
		//int seq = opmsDetailDAO.selectChildChgHisSeq(dataMap);
		
		//UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		//String userId = (String)userVO.getUser_id();
		
		//dataMap.put("seq", seq);
		//dataMap.put("user_id", userId);
		
		opmsDetailDAO.insertChildChgHis(dataMap);
	}
	
	*//**
	 * 자품 변경 이력 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public List selectChildChgHisList(HashMap paramMap) throws NkiaException{
		List resultList = opmsDetailDAO.selectChildChgHisList(paramMap);
		List newList = new ArrayList();
		
		for(int i=0; i<resultList.size(); i++) {
			HashMap dataMap = new HashMap();
			dataMap = (HashMap)resultList.get(i);
			String asset_id = (String)dataMap.get("ASSET_ID_VIEW");
			String asset_id_view = asset_id.substring(0,14) + "-" + asset_id.substring(14);
			dataMap.put("ASSET_ID_VIEW", asset_id_view);
			newList.add(dataMap);
		}
		
		return newList;
	}
	
	*//**
	 * 자품 변경 이력 상세 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public List selectChildChgHisDetail(HashMap paramMap) throws NkiaException{
		return (List)opmsDetailDAO.selectChildChgHisDetail(paramMap);
	}
	
	@Override
	public List selectOpmsLogicHisList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//논리서버 이력 리스트
		return opmsDetailDAO.selectOpmsLogicHisList(paramMap);
	}
	
	*//**
	 * 자산등록시 클래스 명
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	public HashMap searchClassNm(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchClassNm(paramMap);
	}
	
	*//**
	 * 프로세스 장애이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List searchProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchProcHist(paramMap);
	}

	*//**
	 * 프로세스 변경이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List searchChangeProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchChangeProcHist(paramMap);
	}
	
	*//**
	 * 프로세스 문제이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List searchProblemProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchProblemProcHist(paramMap);
	}

	*//**
	 * 프로세스 서비스이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*
	@Override
	public List searchServiceProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchServiceProcHist(paramMap);
	}

	*//**
	 *  DCA 연계 PC S/W 목록
	 *//*
	@Override
	public List selectIfPcSwList(ModelMap paramMap) {
		return opmsDetailDAO.selectIfPcSwList(paramMap);
	}
	
	*//**
	 * 하위 논리 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 *//*
	@Override
	public List selectOpmsRelLogicList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.selectOpmsRelLogicList(paramMap);
	}

	*//**
	 * 이력조회 - 장비별 요청현황조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 *//*		
	public List searchConfReqStateList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchConfReqStateList(paramMap);
	}
	
	public int searchConfReqStateListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return opmsDetailDAO.searchConfReqStateListCount(paramMap);
	}*/
}
