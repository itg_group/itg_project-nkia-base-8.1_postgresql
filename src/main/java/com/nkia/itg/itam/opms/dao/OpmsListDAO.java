package com.nkia.itg.itam.opms.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("opmsListDAO")
public class OpmsListDAO extends EgovComAbstractDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsListDAO.class);
	
}
