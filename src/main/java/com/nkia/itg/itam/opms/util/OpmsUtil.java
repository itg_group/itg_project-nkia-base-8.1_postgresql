package com.nkia.itg.itam.opms.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Component("opmsUtil")
public class OpmsUtil {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	

	private static final String lBraket = "(";
	private static final String rBraket = ")";
	private static final String semicolon = ";";
	private static final String colon = ":";
	private static final String rBrbraket = "}";
	private static final String lBrbraket = "{";
	private static final String singleQuotes = "'";
	private static final String doubleQuotes = "\"";
	private static final String comma = ",";
	private static final String dot = ".";

	private static int columnSize = 2;
	private static final int rowSpanOne = 1;				// rowSpan 1
	private static final int tdHeight = 25;				// tdHeight
	// 컨테이너 안에서는 텍스트필드의 width값이 퍼센트가 익스플로러에서 잘 안먹어서 int값으로 줘야함
	private static final int poc_textPopWidth = 290;
	private static final String popupPrefix = "_POP_NM";

	private static final String dayDateType = "YYYY-MM-DD";
	private static final String hourDateType = "HH24";
	private static final String minDateType = "MI";
	private static final String timeDateType = "YYYY-MM-DD HH24:MI";
	
	private static final String numberFieldStyle = "{'text-align': 'right'}";

	// function name
	private static final String text = "createTextFieldComp";
	private static final String hidden = "createHiddenFieldComp";
	private static final String textarea = "createTextAreaFieldComp";
	private static final String radio = "createCodeRadioComp";
	private static final String check = "createGroupCodeCheckComp";
	private static final String combo = "createCodeComboBoxComp";
	private static final String container = "createUnionFieldContainer";
	private static final String label = "createLabelComp";
	private static final String button = "";
	private static final String eiditorForm = "createEditorFormComp";
	private static final String popOpenBtn = "createPopBtnComp";
	private static final String popClearBtn = "createClearBtnComp";
	private static final String popInfoBtn = "createPopInfoBtnComp";
	private static final String panelBtn = "createPanelBtnComp";
	private static final String numberText = "createNumberUnitFieldComp";

	private static final String fieldInitFn = "function(){containerInitField(this);}";
	private static final String vPanel = "createVerticalPanel";
	private static final String timeStamp = "createDateTimeStamp";
	private static final String date = "createDateFieldComp";
	private static final String tabPanel = "createTabComponent";

	// component
	private static final String manager = "createManagerList";//운영자정보 컴포넌트 생성메소드
	private static final String child = "createChildList";//자자산 리스트 컴포넌트 생성메소드
	private static final String user = "createUserList";//사용자정보 컴포넌트 생성메소드	
	private static final String purinfo = "createPurInfoList";//구매정보 컴포넌트 생성메소드	
	private static final String commng = "createComMngInfoList";//업체관리자정보 컴포넌트 생성메소드	
	private static final String depre = "createDepreciationInfoList"; //감가상각정보 컴포넌트 생성메소드
	private static final String maint = "createMaintForm"; //유지보수 입력폼 컴포넌트 생성메소드
	private static final String file = "createFileForm";
	private static final String infra = "createRelInfra";
	private static final String infraUpdown = "createUpDownRelInfra";
	private static final String infraSelUpdown = "createSelUpDownRelInfra";
	
	private static final String service = "createRelService";
	private static final String install = "createInstallInfo";
	private static final String confcode = "createConfNmCode";
	private static final String update = "createUpdateHistory";
	
	private static final String insSw = "createInstalledSwList";//설치된 소프트웨어 리스트(기존 컴포넌트에서 따로 뺌)
	private static final String swLicence = "createSwLicenceList";//할당 라이선스 리스트
	private static final String logicHis = "createLogicHisList";//논리서버 이력
	
	public static final String incidentHist = "createIncidentReqHist";//프로세스-장애이력
	public static final String changeHist = "createChangeReqHist";//프로세스-변경이력
	public static final String problemHist = "createProblemReqHist";//프로세스-문제이력
	public static final String serviceHist = "createServiceReqHist";//프로세스-서비스이력
	
	private static final String pcSw = "createPcSwList";//DCA 연계 SW 정보
	
	private static final String relLogic = "createRelLogicList";//하위 논리서버 리스트
	private static final String maintJob = "createMaintJobHistory";// 작업기록 이력 리스트

	/**
	 * Manual item 생성 로직... TODO
	 * 
	 * @param dataList
	 * @param paramMap
	 * @return
	 */
	public String createEditorFormComp(List<HashMap> dataList, List<HashMap> hiddenList, HashMap entityMap, JSONArray formArray, JSONArray btnArray)throws NkiaException {

		JSONArray editorItemArray = new JSONArray();
		JSONArray editorHiddenArray = new JSONArray();
		JSONObject editorIdsObj = new JSONObject();
		
		columnSize = StringUtil.parseInteger(entityMap.get("COL_SIZE"));
		String ems_id = StringUtil.parseString(entityMap.get("ems_id"));
		String dca_id = StringUtil.parseString(entityMap.get("dca_id"));

		for (int i = 0; i < dataList.size(); i++) {

			HashMap dataMap = dataList.get(i);

			String htmlType = (String) dataMap.get("HTML_TYPE");
			String unit = (String) dataMap.get("UNIT");
			String result = "";
			String textFieldItem = "";
			String unitItem = "";
			
			// Field Colspan
			int colspan = dataMap.get("COLSPAN") != null ? ((BigDecimal)dataMap.get("COLSPAN")).intValue() : 1;
			
			dataMap.put("EMS_ID", ems_id);
			dataMap.put("DCA_ID", dca_id);

			JSONObject editorItem = new JSONObject();
			JSONArray containerItem = new JSONArray();

			// 텍스트
			if ("TEXT".equalsIgnoreCase(htmlType)) {
				if (!StringUtil.isNull(unit)) {
					textFieldItem = createTextComp(dataMap);
					unitItem = createLabelComp(dataMap);

					containerItem.add(textFieldItem);
					containerItem.add(unitItem);
					
					result = createPocCustomContainerComp(dataMap, containerItem);
				} else {
					result = createTextComp(dataMap);
				}
				editorItem = createFormItemArray(result, colspan, rowSpanOne, tdHeight);

			} else if ("TEXTAREA".equalsIgnoreCase(htmlType)) { // 텍스트 박스
				result = createTextAreaComp(dataMap);
				editorItem = createFormItemArray(result, colspan, rowSpanOne, tdHeight);

			} else if ("DATE".equalsIgnoreCase(htmlType)) {// 날짜

				result = createDateFieldComp(dataMap);
				editorItem = createFormItemArray(result, colspan, rowSpanOne, tdHeight);

			} else if ("SELECT".equalsIgnoreCase(htmlType)) {// 콤보

				result = createCodeComboBoxComp(dataMap, entityMap);
				editorItem = createFormItemArray(result, colspan, rowSpanOne, tdHeight);

			} else if ("RADIO".equalsIgnoreCase(htmlType)) {// 라디오

				result = createCodeRadioComp(dataMap);
				editorItem = createFormItemArray(result, colspan, rowSpanOne, tdHeight);

			} else if ("CHECK".equalsIgnoreCase(htmlType)) {// 체크

				result = createCodeCheckComp(dataMap);
				editorItem = createFormItemArray(result, colspan, rowSpanOne, tdHeight);

			} else if ("POPUP".equalsIgnoreCase(htmlType)) {// 팝업
				result = createPopupContainer(dataMap, entityMap);
				editorItem = createFormItemArray(result, colspan, rowSpanOne, tdHeight);
			}
			
			//form items 생성
			editorItemArray.add(editorItem);
		}

		if (hiddenList != null) {
			String result = "";
			for (int i = 0; i < hiddenList.size(); i++) {
				HashMap dataMap = hiddenList.get(i);
				result = createHiddenFieldComp(dataMap);
				editorHiddenArray.add(result);
			}
		}
		editorIdsObj.put("entity_id", entityMap.get("ENTITY_ID"));
		editorIdsObj.put("comp_id", entityMap.get("ENTITY_ID"));
		editorIdsObj.put("up_id", entityMap.get("UP_ENTITY_ID"));
		editorIdsObj.put("type", "F");
		editorIdsObj.put("entity_type", "MANUAL");

		formArray.add(editorIdsObj);
		return createEditorForm(editorItemArray, editorHiddenArray, entityMap, btnArray);
	}

	public String createComponent(List<HashMap> attrList, HashMap entityMap, JSONArray formArray) throws NkiaException{

		String entityId = (String) entityMap.get("ENTITY_ID");
		String compType = (String) entityMap.get("COMP_TYPE");
		String result = "";
		JSONObject editorIdsObj = new JSONObject();

		if ("MANAGER".equalsIgnoreCase(compType)) {
			result = createManagerList(entityMap);
		} else if ("CHILD".equalsIgnoreCase(compType)) {
			result = createChildList(entityMap);	
		} else if ("USER".equalsIgnoreCase(compType)) {
			result = createUserList(entityMap);	
		} else if ("PUR_INFO".equalsIgnoreCase(compType)) {
			result = createPurInfoList(entityMap);	
		} else if ("COM_MNG".equalsIgnoreCase(compType)) {
			result = createComMngList(entityMap);	
		} else if ("DEPRECIATION".equalsIgnoreCase(compType)) {
			result = createDepreciationInfoList(entityMap);
		} else if ("MAINT".equalsIgnoreCase(compType)) {
			result = createMaintForm(attrList, entityMap);
		} else if ("FILE".equalsIgnoreCase(compType)) {
			result = createFileForm(entityMap);
		} else if ("REL_SERVICE".equalsIgnoreCase(compType)) {
			result = createRelService(entityMap);
		} else if ("REL_INFRA".equalsIgnoreCase(compType) && attrList != null && attrList.size() > 0) {
			result = createRelInfra(attrList, entityMap);
		} else if ("INSTALL".equalsIgnoreCase(compType) && attrList != null && attrList.size() > 0) {
			result = createInstallInfo(attrList, entityMap);
		} else if("CONFNMCODE".equalsIgnoreCase(compType)){
			result = createConfNmCode(entityMap);
		} else if("UPDATE".equalsIgnoreCase(compType) && attrList != null && attrList.size() > 0){
			result = createUpdateHistory(attrList,entityMap);
		} else if("INSTALL_SW".equalsIgnoreCase(compType)) {
			result = createInstalledSw(attrList,entityMap);
		} else if("SW_LICENCE".equalsIgnoreCase(compType)) {
			result = createSwLicence(attrList,entityMap);
		} else if("LOGIC_HIS".equalsIgnoreCase(compType)) {
			result = createLogicHis(attrList,entityMap);
		} else if("INCIDENT_REQ".equalsIgnoreCase(compType)){
			result = createIncidentHist(attrList,entityMap);
		} else if("CHANGE_REQ".equalsIgnoreCase(compType)){
			result = createChangeHist(attrList,entityMap);
		} else if("PROBLEM_REQ".equalsIgnoreCase(compType)){
			result = createProblemHist(attrList,entityMap);
		} else if("SERVICE_REQ".equalsIgnoreCase(compType)){
			result = createServiceReqHist(attrList,entityMap);
		} else if("PC_SW".equalsIgnoreCase(compType)){
			result = createPcSwList(attrList,entityMap);
		}else if("REL_LOGIC".equalsIgnoreCase(compType)) {
			result = createRelLogicList(attrList,entityMap);
//		} else if("MAINT_JOB".equalsIgnoreCase(compType) && attrList != null && attrList.size() > 0){
		} else if("MAINT_JOB".equalsIgnoreCase(compType) ){
			// 2016.04.07.정정윤 추가 - 자산원장호출-신규등록시 작업기록(유지보수)이력 탭은 생성하지 않는다.
			String viewType = (String) entityMap.get("view_type");
			if(null != viewType && !"".equals(viewType) && (!"regist".equals(viewType) && !"info".equals(viewType))) {
				result = createMaintJobHistory(attrList,entityMap);
			} else {
				result = "";
			}
		}
		//싱글 리스트
		if ("MANAGER".equalsIgnoreCase(compType) || "REL_SERVICE".equalsIgnoreCase(compType) || "CHILD".equalsIgnoreCase(compType)
				|| "USER".equalsIgnoreCase(compType) || "PUR_INFO".equalsIgnoreCase(compType) || "COM_MNG".equalsIgnoreCase(compType)
				|| "INSTALL_SW".equalsIgnoreCase(compType) || "DEPRECIATION".equalsIgnoreCase(compType) || "SW_LICENCE".equalsIgnoreCase(compType)
				|| "LOGIC_HIS".equalsIgnoreCase(compType)
				|| "REL_LOGIC".equalsIgnoreCase(compType)) {
			editorIdsObj.put("entity_id", entityId);
			editorIdsObj.put("comp_id", entityId);
			editorIdsObj.put("up_id", entityMap.get("UP_ENTITY_ID"));
			editorIdsObj.put("type", "SL");
			editorIdsObj.put("entity_type", compType);
			formArray.add(editorIdsObj); 
		} else if("CONFNMCODE".equalsIgnoreCase(compType) || "MAINT".equalsIgnoreCase(compType)){
			//컴포넌트 폼
			editorIdsObj.put("entity_id", entityId);
			editorIdsObj.put("comp_id", entityId);
			editorIdsObj.put("up_id", entityMap.get("UP_ENTITY_ID"));
			editorIdsObj.put("type", "CF");
			editorIdsObj.put("entity_type", compType);
			formArray.add(editorIdsObj); 
		}else {
			//탭리스트
			if (!"FILE".equalsIgnoreCase(compType) 
				&& !"UPDATE".equalsIgnoreCase(compType)) {
				if (attrList != null && attrList.size() > 0) {
					for (int i = 0; i < attrList.size(); i++) {
						HashMap dataMap = attrList.get(i);
						editorIdsObj.put("entity_id", entityId);
						editorIdsObj.put("comp_id", dataMap.get("CODE_ID"));
						editorIdsObj.put("up_id", entityMap.get("UP_ENTITY_ID"));
						editorIdsObj.put("type", "TL");
						editorIdsObj.put("entity_type", compType);
						formArray.add(editorIdsObj);
					}
				}
			}
		}
		return result;
	}

	/**
	 * editor 폼 생성 TODO
	 * 
	 * @param jsonArray
	 * @param paramMap
	 * @return
	 */
	public String createEditorForm(JSONArray editorItemArray, JSONArray editorHiddenArray, HashMap entityMap, JSONArray btnArray) throws NkiaException{

		JSONObject editorFormProp = new JSONObject();

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");

		editorFormProp.put("id", attachQuotes(id));
		editorFormProp.put("title", attachQuotes(title));
		editorFormProp.put("editOnly", true);
		editorFormProp.put("columnSize", columnSize);
		editorFormProp.put("border", true);
		editorFormProp.put("tableProps", editorItemArray.toString());
		if( btnArray != null && btnArray.size() > 0 ){
			editorFormProp.put("formBtns", btnArray.toString());
		}

		if (editorHiddenArray.size() > 0) {
			editorFormProp.put("hiddenFields", editorHiddenArray.toString());
		}

		return eiditorForm + lBraket + replaceAllquotes(editorFormProp.toString()) + rBraket;
	}

	/**
	 * 에디터 폼 item 생성 TODO
	 * 
	 * @param str
	 * @param colspan
	 * @param rowSpan
	 * @param tdHeight
	 * @return
	 */
	public JSONObject createFormItemArray(String str, int colspan, int rowSpan, int tdHeight) throws NkiaException{
		JSONObject formItemProp = new JSONObject();

		formItemProp.put("colspan", colspan);
		formItemProp.put("rowSpan", rowSpan);
		formItemProp.put("tdHeight", tdHeight);
		formItemProp.put("item", str);

		return formItemProp;
	}

	/**
	 * 텍스트
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createTextComp(HashMap dataMap)throws NkiaException {

		JSONObject textfieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String padding = (String) dataMap.get("PADDING");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String view_type = (String) dataMap.get("view_type");
		String col_id = dataMap.containsKey("COL_ID") ? (String) dataMap.get("COL_ID") : "";
		
		int maxFieldValue = StringUtil.parseInteger(dataMap.get("MAX_VALUE"));
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		
		boolean hideLabel = StringUtil.parseBoolean(dataMap.get("HIDE_LABEL"));
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		boolean popupText = StringUtil.parseBoolean(dataMap.get("POPUP_TEXT"), "Y");

		textfieldProp.put("label", attachQuotes(label));
		textfieldProp.put("name", attachQuotes(name));
		
		if( dataMap.containsKey("COLSPAN") ){
			int colspan = ((BigDecimal)dataMap.get("COLSPAN")).intValue();
			if( colspan > 1 ){
				int width = BaseConstraints.getDefaultFieldWidth() * colspan;
				textfieldProp.put("width", width);
			}
		}
		
		if (!StringUtil.isNull(padding)) {
			textfieldProp.put("padding", attachQuotes(padding));
		}
		if (!StringUtil.isNull(vtype)) {
			textfieldProp.put("vtype", attachQuotes(vtype));
			if( vtype.equalsIgnoreCase("number") ){
				textfieldProp.put("fieldStyle", attachQuotes(numberFieldStyle));
			}
		}
		if (maxFieldValue > 0) {
			textfieldProp.put("maxFieldValue", maxFieldValue);
		}
		if(maxLength>0){
			textfieldProp.put("maxLength", maxLength);
		}
		if (hideLabel) {
			textfieldProp.put("hideLabel", true);
		}
		if (notNull) {
			if (readOnly) {
				textfieldProp.put("readOnly", true);
				if (popupText) {
					textfieldProp.put("notNull", true);
					textfieldProp.put("editMode", true);
				} else {
					textfieldProp.put("notNull", false);
				}
			} else {
				textfieldProp.put("readOnly", false);
				textfieldProp.put("notNull", true);
				textfieldProp.put("editMode", true);
			}
		} else {
			if (readOnly) {
				textfieldProp.put("readOnly", true);
			}else{
				textfieldProp.put("editMode", true);
				if("LOC_XY".equals(name)) {
					textfieldProp.put("readOnly", true);
				}else {
					textfieldProp.put("readOnly", false);
					
				}
			}
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			textfieldProp.put("emsYn", true);
			textfieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			textfieldProp.put("dcaYn", true);
			textfieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			textfieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			textfieldProp.put("colExpression", attachQuotes(colExpression));
		}
		
		/**
		 *  NH 기능 적용
		 * number 항목에 ',' 찍어주기 위해 numberText 를 사용할 수 있도록 추가  - 2017.01.03 좌은진 
		 */
		String resultType = text;
		
		if (!StringUtil.isNull(vtype)) {
			textfieldProp.put("vtype", attachQuotes(vtype));
			if( vtype.equalsIgnoreCase("number") ) {
				resultType = numberText;
				textfieldProp.put("defaultZeroVal", false);
				textfieldProp.put("fieldStyle", attachQuotes(numberFieldStyle));
			} else if ( vtype.equalsIgnoreCase("numberDot") ) {
				textfieldProp.put("fieldStyle", attachQuotes(numberFieldStyle));
			}
		}

		return resultType + lBraket + textfieldProp.toString() + rBraket;
	}

	/**
	 * 텍스트 박스
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createTextAreaComp(HashMap dataMap) throws NkiaException{

		JSONObject textAreafieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");

		textAreafieldProp.put("label", attachQuotes(label));
		textAreafieldProp.put("name", attachQuotes(name));
		
		if( dataMap.containsKey("COLSPAN") ){
			int colspan = ((BigDecimal)dataMap.get("COLSPAN")).intValue();
			if( colspan > 1 ){
				int width = BaseConstraints.getDefaultFieldWidth() * colspan;
				textAreafieldProp.put("width", width);
			}
		}

		if (!StringUtil.isNull(vtype)) {
			textAreafieldProp.put("vtype", attachQuotes(vtype));
		}
		if (notNull) {
			if (readOnly) {
				textAreafieldProp.put("readOnly", true);
				textAreafieldProp.put("notNull", false);
			} else {
				textAreafieldProp.put("readOnly", false);
				textAreafieldProp.put("editMode", true);
				textAreafieldProp.put("notNull", true);
			}
		} else {
			if (readOnly) {
				textAreafieldProp.put("readOnly", true);
			}
		}
		if(maxLength>0){
			textAreafieldProp.put("maxLength", maxLength);
		}
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			textAreafieldProp.put("emsYn", true);
			textAreafieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			textAreafieldProp.put("dcaYn", true);
			textAreafieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			textAreafieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		return textarea + lBraket + textAreafieldProp.toString() + rBraket; 
	}

	/**
	 * 콤보박스
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createCodeComboBoxComp(HashMap dataMap, HashMap entityMap) throws NkiaException{

		JSONObject codeCombofieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String code_grp_id = (String) dataMap.get("SYS_CODE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		
		boolean hideLabel = StringUtil.parseBoolean(dataMap.get("HIDE_LABEL"));
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		String viewType = (String)entityMap.get("view_type");

		codeCombofieldProp.put("label", attachQuotes(label));
		codeCombofieldProp.put("name", attachQuotes(name));
		codeCombofieldProp.put("code_grp_id", attachQuotes(code_grp_id));
		boolean isAttachChoice = true;

		if (hideLabel) {
			codeCombofieldProp.put("hideLabel", true);
		}
		if (notNull) {
			if (readOnly) {
				codeCombofieldProp.put("readOnly", true);
				codeCombofieldProp.put("notNull", false);
			} else {
				String colId = (String) dataMap.get("COL_ID");
				if("ASSET_STATE".equals(colId)) {
					//codeCombofieldProp.put("readOnly", true); 일괄수정 시 운영상태 비활성화 제거
					if( "regist".equals(viewType) || "info".equals(viewType) ) {
						codeCombofieldProp.put("defaultValue", "'INTRO'");
					}
				}else{
					codeCombofieldProp.put("readOnly", false);
				}
				
				// 2016.12.08 좌은진 추가 - 가용성, 기밀성, 무결성, 중요도 필드 전체선택 란 없앰
				if("AVALTY".equals(colId) || "CONFTY".equals(colId) || "INTGTY".equals(colId) || "IMPORTANCE".equals(colId)) {
					isAttachChoice = false;
				}
				
				codeCombofieldProp.put("attachChoice", isAttachChoice);
				codeCombofieldProp.put("editMode", true);
				codeCombofieldProp.put("notNull", true);
			}
		} else {
			if (readOnly) {
				codeCombofieldProp.put("readOnly", true);
			}
		}
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			codeCombofieldProp.put("emsYn", true);
			codeCombofieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			codeCombofieldProp.put("dcaYn", true);
			codeCombofieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			codeCombofieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		return combo + lBraket + codeCombofieldProp.toString() + rBraket;
	}

	/**
	 * 라디오
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createCodeRadioComp(HashMap dataMap)throws NkiaException {

		JSONObject codeRadiofieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String code_grp_id = (String) dataMap.get("SYS_CODE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		
		boolean hideLabel = StringUtil.parseBoolean(dataMap.get("HIDE_LABEL"));
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");

		codeRadiofieldProp.put("label", attachQuotes(label));
		codeRadiofieldProp.put("name", attachQuotes(name));
		codeRadiofieldProp.put("code_grp_id", attachQuotes(code_grp_id));

		if (hideLabel) {
			codeRadiofieldProp.put("hideLabel", true);
		}
		if (notNull) {
			if (readOnly) {
				codeRadiofieldProp.put("readOnly", true);
				codeRadiofieldProp.put("notNull", false);
			} else {
				codeRadiofieldProp.put("readOnly", false);
				codeRadiofieldProp.put("editMode", true);
				codeRadiofieldProp.put("notNull", true);
			}
		} else {
			if (readOnly) {
				codeRadiofieldProp.put("readOnly", true);
			}
		}
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			codeRadiofieldProp.put("emsYn", true);
			codeRadiofieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			codeRadiofieldProp.put("dcaYn", true);
			codeRadiofieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			codeRadiofieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		return radio + lBraket + codeRadiofieldProp.toString() + rBraket;
	}

	/**
	 * 체크 박스
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createCodeCheckComp(HashMap dataMap) throws NkiaException{

		JSONObject codeCheckfieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String code_grp_id = (String) dataMap.get("SYS_CODE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		
		boolean hideLabel = StringUtil.parseBoolean(dataMap.get("HIDE_LABEL"));
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");

		codeCheckfieldProp.put("label", attachQuotes(label));
		codeCheckfieldProp.put("name", attachQuotes(name));
		codeCheckfieldProp.put("code_grp_id", attachQuotes(code_grp_id));

		if (hideLabel) {
			codeCheckfieldProp.put("hideLabel", true);
		}
		if (notNull) {
			if (readOnly) {
				codeCheckfieldProp.put("readOnly", true);
				codeCheckfieldProp.put("notNull", false);
			} else {
				codeCheckfieldProp.put("readOnly", false);
				codeCheckfieldProp.put("editMode", true);
				codeCheckfieldProp.put("notNull", true);
			}
		} else {
			if (readOnly) {
				codeCheckfieldProp.put("readOnly", true);
			}
		}
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			codeCheckfieldProp.put("emsYn", true);
			codeCheckfieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			codeCheckfieldProp.put("dcaYn", true);
			codeCheckfieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			codeCheckfieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		return check + lBraket + codeCheckfieldProp.toString() + rBraket;
	}

	/**
	 * 팝업 초기화버튼 TODO
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createPopupClearBtnComp(HashMap dataMap)throws NkiaException {

		JSONObject popupBtnClrProp = new JSONObject();
		String entityId = (String) dataMap.get("ENTITY_ID");
		String name = (String) dataMap.get("MK_COMP_NM");
		String colId = (String) dataMap.get("COL_ID");
		popupBtnClrProp.put("id", attachQuotes(entityId + "_" + name + "_PopClrBtn"));
		
		/** 2014.03.20 정정윤추가 - 모델 팝업 버튼 초기화 하면 제조사 팝업 버튼도 초기화 되게 함 **/
		// 원장화면과 일괄수정 원장화면이 다르므로 opmsDetail.nvf 와 batchEditDetail.nvf 에
		// 각각 thisForm 이라는 변수를 선언해서 현재 폼을 그곳에 담아줌.
		// MODEL_ID 팝업 초기화 버튼을 눌렀을 때 VENDOR_ID 값 또한 초기화 될 수 있도록 (둘은 상호 관계)
		// 핸들러를 걸어준다.
		if("MODEL_ID".equals(colId)) {
			String vendorId = "manualForm.findField('VENDOR_ID')";
			String vendorIdPop = "manualForm.findField('VENDOR_ID_POP_NM')";
			String initFunc = "function(){containerInitField(this);containerInitField("+vendorIdPop+");containerInitField("+vendorId+");}";
			popupBtnClrProp.put("handler", initFunc);
		// SW_GROUP_ID 초기화 버튼 클릭시 옵션필드도 초기화
		} else if("SW_GROUP_ID".equals(colId)) {
			String swGroupOptionId = "specForm.findField('SW_GROUP_OPTION_ID')";
			//String swGroupOptionIdView = "specForm.findField('SW_GROUP_OPTION_ID_VIEW')";
			String initFunc = "function(){containerInitField(this);containerInitField("+swGroupOptionId+");}";
			popupBtnClrProp.put("handler", initFunc);
		} else {
			popupBtnClrProp.put("handler", fieldInitFn);
		}
		

		return popClearBtn + lBraket + popupBtnClrProp.toString() + rBraket;
	}

	/**
	 * 팝업 열기 버튼
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createPopupBtnComp(HashMap dataMap, HashMap entityMap)throws NkiaException {

		JSONObject popupBtnProp = new JSONObject();

		String entityId = (String) dataMap.get("ENTITY_ID");
		String name = (String) dataMap.get("MK_COMP_NM");
		String handler = (String) dataMap.get("POPUP_CODE");
		String classId = (String) entityMap.get("class_id");
		String classType = (String) entityMap.get("class_type");

		popupBtnProp.put("id", attachQuotes(entityId + "_" + name + "_PopOpBtn"));
		popupBtnProp.put("handler", "function(){openAmPopup(this,'" + handler + "','" + name + "','" + name + popupPrefix + "','I','"+classId+"','"+classType+"')}");

		return popOpenBtn + lBraket + popupBtnProp.toString() + rBraket;
	}

	/**
	 * 팝업 열기 버튼
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createPopupInfoComp(HashMap dataMap, HashMap entityMap)throws NkiaException {

		JSONObject popupBtnProp = new JSONObject();

		String entityId = (String) dataMap.get("ENTITY_ID");
		String name = (String) dataMap.get("MK_COMP_NM");
		String handler = (String) dataMap.get("POPUP_CODE");
		String classId = (String) entityMap.get("class_id");
		String classType = (String) entityMap.get("class_type");

		popupBtnProp.put("id", attachQuotes(entityId + "_" + name + "_PopInfoBtn"));
		popupBtnProp.put("handler", "function(){openAmPopup(this,'" + handler + "','" + name + "','" + name + popupPrefix + "','V','"+classId+"','"+classType+"')}");

		return popInfoBtn + lBraket + popupBtnProp.toString() + rBraket;
	}

	/**
	 * 
	 * TODO 팝업생성
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createPopupContainer(HashMap dataMap, HashMap entityMap) throws NkiaException{
		JSONArray popupArray = new JSONArray();

		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		String entityId = (String) dataMap.get("ENTITY_ID");
		String name = (String) dataMap.get("MK_COMP_NM");
		String showType = (String) dataMap.get("POPUP_SHOW_TYPE");
		String popupFn = (String) dataMap.get("POPUP_NM_FN");
		boolean popupDetailYn = StringUtil.parseBoolean(dataMap.get("POPUP_DETAIL_YN"), "Y");
		
		// 2014.03.20 정정윤 추가 - 제조사는 모델의 선택값에 따라서 움직여야 하므로 팝업선택버튼이 없어야 한다
		// 2014.03.26 정정윤 추가 - SW옵션은 SW그룹의 선택값에 따라서 움직여야 하므로 팝업선택버튼이 없어야 한다
		// 2014.03.28 정정윤 추가 - 부서는 운영자정보 선택값에 따라서 바뀌어야 하므로 팝업선택버튼이 없어야 한다
		String colId = (String) dataMap.get("COL_ID");
		if("SW_GROUP_OPTION_ID".equals(colId) || "LOC_XY".equals(colId)) {
			readOnly = true;
		}
		
		// 팝업의 경우 텍스트 박스는 그냥 만들어준다..
		// 기준컬럼이 히든으로 들어간다..
		HashMap textDataMap = new HashMap();

		textDataMap.put("ENTITY_ID", entityId);
		if( dataMap.containsKey("COLSPAN") ){
			textDataMap.put("COLSPAN", dataMap.get("COLSPAN"));
		}
		textDataMap.put("MK_COMP_NM", name + popupPrefix);
		textDataMap.put("COL_NAME", dataMap.get("COL_NAME"));
		textDataMap.put("NOT_NULL", StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y"));
		textDataMap.put("POPUP_TEXT", "Y");
		textDataMap.put("AUTH_TYPE", "R");

		String text = "";
		String idText = "";
		String opBnts = "";
		String infoBnts = "";
		String clrBnts = "";

		// id만 출력
		if ("ID".equalsIgnoreCase(showType)) {
			dataMap.put("AUTH_TYPE", "R");
			idText = createTextComp(dataMap);
			popupArray.add(idText);

		}// 명칭만 출력
		else if ("NAME".equalsIgnoreCase(showType)) {
			text = createTextComp(textDataMap);
			idText = createHiddenFieldComp(dataMap);
			popupArray.add(text);
			popupArray.add(idText);

		}// id와 명칭 모두 출력
		else if ("IDNAME".equalsIgnoreCase(showType)) {
			
			int id_width = (int)( poc_textPopWidth / 2 );
			int txt_width = (int)( poc_textPopWidth / 2);

			textDataMap.put("PADDING", "0 0 0 5");

			dataMap.put("AUTH_TYPE", "R");
			textDataMap.put("WIDTH", txt_width);

			textDataMap.put("COL_NAME", "");
			
			idText = createTextComp(dataMap);
			text = createTextComp(textDataMap);

			popupArray.add(idText);
			popupArray.add(text);

		}// null인경우 명칭만 출력한다.
		else if (showType == null || "".equals(showType)) {
			text = createTextComp(textDataMap);
			idText = createHiddenFieldComp(dataMap);
			popupArray.add(text);
			popupArray.add(idText);
		}

		// -- 2016.12.21 시작
		if (popupDetailYn) {
			//2016.11.30 좌은진 - 물리구성아이디 인 경우, 물리서버 상세 보기 팝업버튼 구성
			if("PHYSI_CONF_ID".equals(colId) && !"regist".equalsIgnoreCase((String) entityMap.get("view_type"))){
				infoBnts = createPopupInfoComp(dataMap, entityMap);
				popupArray.add(infoBnts);
			}
		}
		if (!readOnly) {
			if(!"PHYSI_CONF_ID".equals(colId)){
				opBnts = createPopupBtnComp(dataMap, entityMap);
				popupArray.add(opBnts);
				clrBnts = createPopupClearBtnComp(dataMap);
				popupArray.add(clrBnts);
			}
		}
		// !-- -- 2016.12.21 끝
		return createPocCustomContainerComp(dataMap, popupArray);
	}

	/**
	 * 
	 * TODO 히든 필드 생성
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createHiddenFieldComp(HashMap dataMap) throws NkiaException{
		JSONObject hiddenProp = new JSONObject();
		String name = (String) dataMap.get("MK_COMP_NM");
		hiddenProp.put("name", attachQuotes(name));
		return hidden + lBraket + hiddenProp.toString() + rBraket;
	}
	
	/**
	 * 
	 * TODO 커스텀 컨테이너 생성 - 2015.06.23.정정윤
	 * 
	 * @param dataMap
	 * @param itemArray
	 * @return
	 */
	public String createPocCustomContainerComp(HashMap dataMap, JSONArray itemArray) throws NkiaException{
		JSONObject containerProp = new JSONObject();

		String name = (String) dataMap.get("MK_COMP_NM");
		String entityId = (String) dataMap.get("ENTITY_ID");

		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");

		containerProp.put("id", attachQuotes(entityId + "_" + name + "_Container"));
		containerProp.put("items", itemArray.toString());

		if (notNull) {
			if (readOnly) {
				containerProp.put("readOnly", true);
				containerProp.put("notNull", false);
			} else {
				containerProp.put("readOnly", false);
				containerProp.put("editMode", true);
				containerProp.put("notNull", true);
			}
		} else {
			if (readOnly) {
				containerProp.put("readOnly", true);
			}
		}
		if( dataMap.containsKey("AM_VALUE") ){
			containerProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		return container + lBraket + containerProp.toString() + rBraket;
	}

	/**
	 * 
	 * TODO 컨테이너 생성
	 * 
	 * @param dataMap
	 * @param itemArray
	 * @return
	 */
	public String createContainerComp(HashMap dataMap, JSONArray itemArray) throws NkiaException{
		JSONObject containerProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String entityId = (String) dataMap.get("ENTITY_ID");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");

		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");

		containerProp.put("id", attachQuotes(entityId + "_" + name + "_Container"));
		containerProp.put("label", attachQuotes(label));
		containerProp.put("items", itemArray.toString());

		if (notNull) {
			if (readOnly) {
				containerProp.put("readOnly", true);
				containerProp.put("notNull", false);
			} else {
				containerProp.put("readOnly", false);
				containerProp.put("editMode", true);
				containerProp.put("notNull", true);
			}
		} else {
			if (readOnly) {
				containerProp.put("readOnly", true);
			}
		}
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			containerProp.put("emsYn", true);
			containerProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			containerProp.put("dcaYn", true);
			containerProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			containerProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		return container + lBraket + containerProp.toString() + rBraket;
	}

	/**
	 * 라벨 생성 TODO
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createLabelComp(HashMap dataMap) throws NkiaException{
		JSONObject labelProp = new JSONObject();
		String unit = (String) dataMap.get("UNIT");
		labelProp.put("text", attachQuotes(unit));
		return label + lBraket + labelProp.toString() + rBraket;
	}

	/**
	 * 
	 * TODO 날짜 필드 생성
	 * 
	 * @param dataMap
	 * @return
	 */
	public String createDateFieldComp(HashMap dataMap) throws NkiaException{

		JSONObject dateProp = new JSONObject();
		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String dateType = (String) dataMap.get("DATE_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String dateTypeProp = "";

		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");

		dateProp.put("label", attachQuotes(label));
		dateProp.put("name", attachQuotes(name));

		if (dayDateType.equalsIgnoreCase(dateType)) {
			dateTypeProp = "D";
		} else {
			dateTypeProp = "DHM";
		}

		dateProp.put("dateType", attachQuotes(dateTypeProp));

		dataMap.put("D_TYPE", dateTypeProp);
		String defaultDateTime = createDateTimeStamp(dataMap);

		if (notNull) {
			if (readOnly) {
				dateProp.put("readOnly", true);
				dateProp.put("notNull", false);
			} else {
				dateProp.put("readOnly", false);
				dateProp.put("editMode", true);
				dateProp.put("notNull", true);
			}
		} else {
			if (readOnly) {
				dateProp.put("readOnly", true);
			}
		}
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			dateProp.put("emsYn", true);
			dateProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			dateProp.put("dcaYn", true);
			dateProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			dateProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		return date + lBraket + dateProp.toString() + rBraket;
	}

	/**
	 * 
	 * TODO 시간 을 가져오는 컴포넌트 생성
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createDateTimeStamp(HashMap dataMap) throws NkiaException{

		JSONObject timeStampProp = new JSONObject();
		String dateType = (String) dataMap.get("D_TYPE");

		timeStampProp.put("year", 0);
		timeStampProp.put("month", 0);
		timeStampProp.put("day", 0);
		timeStampProp.put("hour", 0);
		timeStampProp.put("min", 0);

		return timeStamp + lBraket + attachQuotes(dateType) + "," + timeStampProp.toString() + rBraket;
	}

	/**
	 * 패널을 만든다. TODO
	 * 
	 * @param entityMap
	 * @param jsonArray
	 * @return
	 */
	public String createPanelComp(HashMap entityMap, JSONArray jsonArray)throws NkiaException {
		JSONObject panelProp = new JSONObject();

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");

		panelProp.put("id", attachQuotes(id));
		panelProp.put("title", attachQuotes(title));
		panelProp.put("panelTools", entityMap.get("panelTools"));
		
		JSONArray verticalArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		for( int i = 0; i < jsonArray.size(); i++ ){
			jsonObject.put("width", "'100%'");
			jsonObject.put("items", jsonArray.getString(i));
			verticalArray.add(replaceAllquotes(jsonObject.toString()));
		}		
		panelProp.put("panelItems", verticalArray.toString());

		return vPanel + lBraket + replaceAllquotes(panelProp.toString()) + rBraket;
	}

	/**
	 * 탭을 만든다. TODO
	 * 
	 * @param dataMap
	 * @param tabItems
	 * @return
	 */
	public String createTabComponent(HashMap dataMap, JSONArray tabItems)throws NkiaException {

		JSONObject tabProp = new JSONObject();
		String id = (String) dataMap.get("asset_id");
		tabProp.put("id", attachQuotes(id));
		tabProp.put("items", tabItems.join(",").toString());

		return tabPanel + lBraket + tabProp.toString() + rBraket;
	}

	/**
	 * 담당자 리스트를 만든다. TODO
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createManagerList(HashMap entityMap) throws NkiaException {
		JSONObject managerProp = new JSONObject();
		JSONArray resource = getGridResource("grid.itam.managerList"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");
		String entity_mng_type = (String) entityMap.get("entity_mng_type");	//hmsong 속성이 ASSET속성인지, CONF속성인지 구분하는 구분자
		
		managerProp.put("id", attachQuotes(id));
		managerProp.put("title", attachQuotes(title));
		managerProp.put("conf_id", attachQuotes(confId));
		managerProp.put("authType", attachQuotes(authType));
		managerProp.put("gridFormItem",resource.toString());
		managerProp.put("view_type",attachQuotes(view_type));
		managerProp.put("entity_mng_type",attachQuotes(entity_mng_type));
		return manager + lBraket + replaceAllquotes(managerProp.toString()) + rBraket;
	}
	
	/**
	 * 자자산 리스트를 만든다. TODO
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createChildList(HashMap entityMap) throws NkiaException {
		JSONObject childProp = new JSONObject();
		JSONArray resource = getGridResource("grid.itam.childList"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");
		String asset_id = (String) entityMap.get("asset_id");

		childProp.put("id", attachQuotes(id));
		childProp.put("title", attachQuotes(title));
		childProp.put("conf_id", attachQuotes(confId));
		childProp.put("authType", attachQuotes(authType));
		childProp.put("gridFormItem",resource.toString());
		childProp.put("view_type",attachQuotes(view_type));
		childProp.put("ori_asset_id",attachQuotes(asset_id));
		return child + lBraket + replaceAllquotes(childProp.toString()) + rBraket;
	}
	
	/**
	 * 구매정보 리스트를 만든다. TODO
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createPurInfoList(HashMap entityMap) throws NkiaException {

		JSONObject purinfoProp = new JSONObject();
		
	//	gridFormItem
		//
		
		JSONArray resource = getGridResource("grid.itam.purinfoList");  

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String view_type = (String) entityMap.get("view_type");

		purinfoProp.put("id", attachQuotes(id));
		purinfoProp.put("title", attachQuotes(title));
		purinfoProp.put("conf_id", attachQuotes(confId));
		purinfoProp.put("asset_id", attachQuotes(assetId));
		purinfoProp.put("authType", attachQuotes(authType));
		purinfoProp.put("gridFormItem",resource.toString());
		purinfoProp.put("view_type",attachQuotes(view_type));
		return purinfo + lBraket + replaceAllquotes(purinfoProp.toString()) + rBraket;
	}
	
	/**
	 * 감가상각 정보 리스트를 만든다. TODO
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createDepreciationInfoList(HashMap entityMap) throws NkiaException {
		JSONObject depreciationinfoProp = new JSONObject();
		JSONArray resource = getGridResource("grid.itam.DepreciationList");

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String view_type = (String) entityMap.get("view_type");

		depreciationinfoProp.put("id", attachQuotes(id));
		depreciationinfoProp.put("title", attachQuotes(title));
		depreciationinfoProp.put("conf_id", attachQuotes(confId));
		depreciationinfoProp.put("asset_id", attachQuotes(assetId));
		depreciationinfoProp.put("authType", attachQuotes(authType));
		depreciationinfoProp.put("gridFormItem",resource.toString());
		depreciationinfoProp.put("view_type",attachQuotes(view_type));
		return depre + lBraket + replaceAllquotes(depreciationinfoProp.toString()) + rBraket;
	}
	/**
	 * 사용자 리스트를 만든다. TODO
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createUserList(HashMap entityMap) throws NkiaException {
		JSONObject userProp = new JSONObject();
		JSONArray resource = getGridResource("grid.itam.userList"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");

		userProp.put("id", attachQuotes(id));
		userProp.put("title", attachQuotes(title));
		userProp.put("conf_id", attachQuotes(confId));
		userProp.put("authType", attachQuotes(authType));
		userProp.put("gridFormItem",resource.toString());
		userProp.put("view_type",attachQuotes(view_type));
		return user + lBraket + replaceAllquotes(userProp.toString()) + rBraket;
	}
	/**
	 * 업체관리자정보 리스트를 만든다. TODO
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createComMngList(HashMap entityMap) throws NkiaException {
		JSONObject commngProp = new JSONObject();
		JSONArray resource = getGridResource("grid.itam.commngList"); //TODO 업체정보관리 그리드 메시지 만들 것

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");

		commngProp.put("id", attachQuotes(id));
		commngProp.put("title", attachQuotes(title));
		commngProp.put("conf_id", attachQuotes(confId));
		commngProp.put("authType", attachQuotes(authType));
		commngProp.put("gridFormItem",resource.toString());
		commngProp.put("view_type",attachQuotes(view_type));
		return commng + lBraket + replaceAllquotes(commngProp.toString()) + rBraket;
	}
	


	/**
	 * 파일 콤포넌트 생성 TODO
	 * 
	 * @param entityMap
	 * @return
	 */
	public String createFileForm(HashMap entityMap)throws NkiaException {
		JSONObject fileProp = new JSONObject();
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String fileId = (String) entityMap.get("atch_file_id");

		fileProp.put("id", attachQuotes(id));
		fileProp.put("title", attachQuotes(title));
		fileProp.put("conf_id", attachQuotes(confId));
		fileProp.put("asset_Id", attachQuotes(assetId));
		fileProp.put("authType", attachQuotes(authType));
		fileProp.put("atch_file_id", attachQuotes(fileId));

		return file + lBraket + replaceAllquotes(fileProp.toString()) + rBraket;
	}

	/**
	 * 연관구성장비 TODO
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 */
	public String createRelInfra(List attrList, HashMap entityMap) throws NkiaException{

		JSONObject infraProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String view_type = (String) entityMap.get("view_type");
		
		infraProp.put("id", attachQuotes(id));
		infraProp.put("title", attachQuotes(title));
		infraProp.put("conf_id", attachQuotes(confId));
		infraProp.put("asset_id", attachQuotes(assetId));
		infraProp.put("authType", attachQuotes(authType));
		infraProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		infraProp.put("view_type",attachQuotes(view_type));
		
		for(int i=0;i<attrList.size();i++){
			HashMap attrMap = (HashMap)attrList.get(i);
			String codeId = StringUtil.parseString(attrMap.get("CODE_ID"));
			infraProp.put("gridFormItem"+codeId,getGridResource("grid.itam.relConf"+codeId));
		}

		return infra + lBraket + replaceAllquotes(infraProp.toString()) + rBraket;
	}

	/**
	 * 
	 * TODO 연관 서비스 그리드 생성
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createRelService(HashMap entityMap) throws NkiaException{

		JSONObject serviceProp = new JSONObject();

		JSONArray resource = getGridResource("grid.itam.relService"); 
	
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String view_type = (String) entityMap.get("view_type");
		
		serviceProp.put("id", attachQuotes(id));
		serviceProp.put("title", attachQuotes(title));
		serviceProp.put("conf_id", attachQuotes(confId));
		serviceProp.put("asset_id", attachQuotes(assetId));
		serviceProp.put("authType", attachQuotes(authType));
		serviceProp.put("gridFormItem",resource.toString());
		serviceProp.put("view_type",attachQuotes(view_type));

		return service + lBraket + replaceAllquotes(serviceProp.toString()) + rBraket;
	}

	/**
	 * 설치 스펙 정보 TODO
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 */
	public String createInstallInfo(List attrList, HashMap entityMap)throws NkiaException {

		JSONObject installProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String view_type = (String) entityMap.get("view_type");

		installProp.put("id", attachQuotes(id));
		installProp.put("title", attachQuotes(title));
		installProp.put("conf_id", attachQuotes(confId));
		installProp.put("asset_id", attachQuotes(assetId));
		installProp.put("authType", attachQuotes(authType));
		installProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		installProp.put("view_type",attachQuotes(view_type));
		
		setInstallColInfo(installProp,attrList,entityMap);

		return install + lBraket + replaceAllquotes(installProp.toString()) + rBraket;
	}
	
	/**
	 * 유지보수 폼 TODO
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 */
	public String createMaintForm(List attrList, HashMap entityMap)throws NkiaException {

		JSONObject maintProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String view_type = (String) entityMap.get("view_type");

		maintProp.put("id", attachQuotes(id));
		maintProp.put("title", attachQuotes(title));
		maintProp.put("conf_id", attachQuotes(confId));
		maintProp.put("asset_id", attachQuotes(assetId));
		maintProp.put("authType", attachQuotes(authType));
		maintProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		maintProp.put("view_type",attachQuotes(view_type));
		return maint + lBraket + replaceAllquotes(maintProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 설치 컬럼 정보 입력한다.
	 * 
	 * @param installProp
	 * @param attrList
	 * @param entityMap
	 * @throws NkiaException
	 */
	public void setInstallColInfo(JSONObject installProp,List<HashMap> attrList,HashMap entityMap)throws NkiaException{
		for(int i=0;i<attrList.size();i++){
			HashMap attrMap = (HashMap)attrList.get(i);
			String codeId 	= StringUtil.parseString(attrMap.get("CODE_ID"));
			List<HashMap> installColList = (List<HashMap>)entityMap.get(codeId);
			
			for(int j=0;j<installColList.size();j++){
				HashMap colDataMap = installColList.get(j);
				String idxCol 	= StringUtil.parseString(colDataMap.get("IDX_COL"));
				String colNm 	= StringUtil.parseString(colDataMap.get("COL_ID"));
				if("Y".equalsIgnoreCase(idxCol)){
					installProp.put("idx_col_"+codeId, attachQuotes(colNm));
				}
			}
			installProp.put("gridFormItem"+codeId,getGridResourceByList(installColList,"COL_ID","COL_NM"));
		}
	}


	
	/**
	 * 
	 * TODO 장비 이력 컴포넌트 생성
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createUpdateHistory(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject updateProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);
		
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String classType = (String) entityMap.get("class_type");
		String viewType = (String) entityMap.get("view_type");
		String tanYn = (String) entityMap.get("tangible_asset_yn");
		String entity_mng_type = (String) entityMap.get("entity_mng_type"); //hmsong 속성이 ASSET속성인지, CONF속성인지 구분하는 구분자

		updateProp.put("id", attachQuotes(id));
		updateProp.put("title", attachQuotes(title));
		updateProp.put("conf_id", attachQuotes(confId));
		updateProp.put("asset_id", attachQuotes(assetId));
		updateProp.put("authType", attachQuotes(authType));
		updateProp.put("class_type", attachQuotes(classType));
		updateProp.put("view_type", attachQuotes(viewType));
		updateProp.put("tangible_asset_yn", attachQuotes(tanYn));
		updateProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		updateProp.put("entity_mng_type", attachQuotes(entity_mng_type));
		return update + lBraket + replaceAllquotes(updateProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 설치된 소프트웨어 컴포넌트 생성
	 * 20140225 정정윤 추가
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createInstalledSw(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject insSwProp = new JSONObject();
		
		JSONArray resource = getGridResource("grid.itam.oper.installedSw"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");

		insSwProp.put("id", attachQuotes(id));
		insSwProp.put("title", attachQuotes(title));
		insSwProp.put("conf_id", attachQuotes(confId));
		insSwProp.put("authType", attachQuotes(authType));
		insSwProp.put("gridFormItem",resource.toString());
		insSwProp.put("view_type",attachQuotes(view_type));
		
		return insSw + lBraket + replaceAllquotes(insSwProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 할당 라이선스 목록 그리드 컴포넌트 생성
	 * 20140303 정정윤 추가
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createSwLicence(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject swLicenceProp = new JSONObject();
		
		JSONArray resource = getGridResource("grid.itam.sam.assignedSw"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");

		swLicenceProp.put("id", attachQuotes(id));
		swLicenceProp.put("title", attachQuotes(title));
		swLicenceProp.put("conf_id", attachQuotes(confId));
		swLicenceProp.put("authType", attachQuotes(authType));
		swLicenceProp.put("gridFormItem",resource.toString());
		swLicenceProp.put("view_type",attachQuotes(view_type));
		
		return swLicence + lBraket + replaceAllquotes(swLicenceProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 논리서버 이력 목록 그리드 컴포넌트 생성
	 * 20140513 정정윤 추가
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createLogicHis(List attrList, HashMap entityMap)throws NkiaException{
		
		JSONObject logicHisProp = new JSONObject();
		
		JSONArray resource = getGridResource("grid.itam.opms.logicHis"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");

		logicHisProp.put("id", attachQuotes(id));
		logicHisProp.put("title", attachQuotes(title));
		logicHisProp.put("conf_id", attachQuotes(confId));
		logicHisProp.put("authType", attachQuotes(authType));
		logicHisProp.put("gridFormItem",resource.toString());
		logicHisProp.put("view_type",attachQuotes(view_type));
		
		return logicHis + lBraket + replaceAllquotes(logicHisProp.toString()) + rBraket;
	}

	/**
	 * Json Object 변환시 파람 값에 더블 쿼츠 붙는거 바꾼다. TODO
	 * 
	 * @param str
	 * @return
	 */
	public String replaceAllquotes(String str) throws NkiaException{
		return StringUtil.replaceAll(StringUtil.replaceAll(str, "\\\\", ""), "\"", "");
	}

	/**
	 * List를 Json Array로 바로 변환시에 파람 값에 더블 쿼츠 붙는거 바꾼다. TODO
	 * 
	 * @param str
	 * @return
	 */
	public String replaceValueQuotes(String str) throws NkiaException{
		return StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.replaceAll(str, ":\"", ":'\""), "\",", "\"',"), "\"}", "'\"}");
	}

	/**
	 * 
	 * TODO 쿼츠 붙이기..
	 * 
	 * @param str
	 * @return
	 */
	public String attachQuotes(String str) throws NkiaException{
		if (str != null) {
			if (str.indexOf("'") > 0) {
				str = doubleQuotes + str + doubleQuotes;
			} else if (str.indexOf("\"") > 0) {
				str = singleQuotes + str + singleQuotes;
			} else {
				str = singleQuotes + str + singleQuotes;
			}
		}
		return str;
	}

	/**
	 * 
	 * TODO 두개의 맵을 서로 싱크
	 * 
	 * @param baseMap
	 * @param targetMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap paramSync(HashMap baseMap, HashMap targetMap)throws NkiaException {
		Iterator iter = baseMap.keySet().iterator();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			targetMap.put(key, baseMap.get(key));
		}
		return targetMap;
	}

	/**
	 * 
	 * TODO SELECT 쿼리 생성
	 * 
	 * @param attrList
	 * @param hiddenList
	 * @param tblList
	 * @param joinList
	 * @param assetConfIdList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createSelectQuery(List<HashMap> attrList, List<HashMap> hiddenList, List<HashMap> tblList, List<HashMap> joinList,
			List<HashMap> assetConfIdList, HashMap entityMap)throws NkiaException {

		StringBuffer queryText = new StringBuffer();
		String conf_id = (String) entityMap.get("conf_id");
		String asset_id = (String) entityMap.get("asset_id");
		String class_id = (String) entityMap.get("class_id");
		String class_type = (String) entityMap.get("class_type");
		
		if(tblList != null && tblList.size()>0){
			queryText.append("SELECT\n");
			
			if(attrList != null){
				if (attrList.size() > 0) {
					for (int i = 0; i < attrList.size(); i++) {
		
						HashMap dataMap = attrList.get(i);
						boolean isAppend = true;
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						String popupFn = StringUtil.parseString(dataMap.get("POPUP_NM_FN"));
		
						StringBuilder tempText = new StringBuilder();
						if (i > 0) {
							tempText.append(comma);
						}
						
						if (htmlType.equalsIgnoreCase("TEXT")) {
							tempText.append(table + dot + colId + " AS " + name);
							//queryText.append(table + dot + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("TEXTAREA")) { // 텍스트 박스
							tempText.append(table + dot + colId + " AS " + name);
							//queryText.append(table + dot + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
							String dateType = (String) dataMap.get("DATE_TYPE");
							if (dateType.equals(timeDateType)) {
								tempText.append("TO_CHAR(" + table + dot + colId + ",'" + timeDateType + "')" + " AS " + name);
								tempText.append(comma + "TO_CHAR(" + table + dot + colId + ",'" + dayDateType + "')" + " AS " + name + "_DAY");
								tempText.append(comma + "TO_CHAR(" + table + dot + colId + ",'" + hourDateType + "')" + " AS " + name + "_HOUR");
								tempText.append(comma + "TO_CHAR(" + table + dot + colId + ",'" + minDateType + "')" + " AS " + name + "_MIN");
							} else {
								tempText.append("TO_CHAR(" + table + dot + colId + ",'" + dayDateType + "')" + " AS " + name);
								tempText.append(comma + "TO_CHAR(" + table + dot + colId + ",'" + dayDateType + "')" + " AS " + name + "_DAY");
							}
						} else if (htmlType.equalsIgnoreCase("SELECT")) {// 콤보
							tempText.append(table + dot + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("RADIO")) {// 라디오
							tempText.append(table + dot + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("CHECK")) {// 체크
							tempText.append(table + dot + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("POPUP")) {// 팝업
							tempText.append(table + dot + colId + " AS " + name);
							if (!"".equalsIgnoreCase(popupFn)) {
								tempText.append("\n");
								tempText.append(comma);
								String popupColIds = table + dot + colId;
								
								// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
								popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
								
								popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
								tempText.append(popupFn + " AS " + name + popupPrefix);
							}
						} else {
							isAppend = false;
						}
						if(isAppend) {
							queryText.append(tempText.toString());
							queryText.append("\n");							
						}
					}
				}
			}
			
			if(hiddenList != null){
				if (hiddenList.size() > 0) {
					for (int i = 0; i < hiddenList.size(); i++) {
		
						HashMap dataMap = hiddenList.get(i);
		
						String htmlType = (String) dataMap.get("HTML_TYPE");
						String colId = (String) dataMap.get("COL_ID");
						String name = (String) dataMap.get("MK_COMP_NM");
						String table = (String) dataMap.get("GEN_TABLE_NM");
		
						if (attrList != null && attrList.size() > 0) {
							queryText.append(comma);
						} else {
							if (i > 0) {
								queryText.append(comma);
							}
						}
						if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
							String dateType = (String) dataMap.get("DATE_TYPE");
							if (dateType.equals(timeDateType)) {
								queryText.append("TO_CHAR(" + table + dot + colId + ",'" + timeDateType + "')" + " AS " + name + "\n");
							} else {
								queryText.append("TO_CHAR(" + table + dot + colId + ",'" + dayDateType + "')" + " AS " + name + "\n");
							}
						} else {
							queryText.append(table + dot + colId + " AS " + name + "\n");
						}
					}
				}
			}
	
			queryText.append("FROM\n");
			
			if (tblList.size() > 0) {
				for (int i = 0; i < tblList.size(); i++) {
					HashMap dataMap = tblList.get(i);
					String table = (String) dataMap.get("GEN_TABLE_NM");
					if (i > 0) {
						queryText.append(comma);
					}
					queryText.append(table + "\n");
				}
			}
	
			queryText.append("WHERE\n");
	
			if(joinList != null){
				if (joinList.size() > 0) {
					for (int i = 0; i < joinList.size(); i++) {
		
						HashMap dataMap = joinList.get(i);
						String left = (String) dataMap.get("LEFT_COL_ID");
						String right = (String) dataMap.get("RIGHT_COL_ID");
		
						if (i > 0) {
							queryText.append("AND ");
						}
						queryText.append(left + " = " + right + "\n");
					}
					queryText.append("AND ");
				}
			}
	
			int attatchAsset = 0;
			
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				String tabl_nm = (String) dataMap.get("GEN_TABLE_NM");
				if ("ASSET_ID".equalsIgnoreCase(col_id)) {
					queryText.append(tabl_nm + "." + col_id + "='" + asset_id + "'" + "\n");
					attatchAsset = 1;
					break;
				}
			}
	
			int attatchConf = 0;
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				String tabl_nm = (String) dataMap.get("GEN_TABLE_NM");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					if (attatchAsset > 0) {
						queryText.append("AND ");
					}
					queryText.append(tabl_nm + "." + col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
				
			}
	
			// 검색 조건이 없어서 에러날경우 대비
			if (attatchConf == 0 && attatchAsset == 0) {
				queryText.append(" ROWNUM=1\n");
			}
		}
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO 카운트 쿼리 생성
	 * 
	 * @param assetConfIdList
	 * @param entityMap
	 * @return
	 */
	public String createCountQuery(List<HashMap> assetConfIdList,HashMap entityMap){
		
		StringBuffer queryText = new StringBuffer();
		String conf_id = StringUtil.parseString(entityMap.get("conf_id"));
		String asset_id = StringUtil.parseString(entityMap.get("asset_id"));
		String tblNm = StringUtil.parseString(entityMap.get("gen_table_nm"));
		
		if(assetConfIdList != null && assetConfIdList.size()>0){
			
			queryText.append("SELECT COUNT(1) FROM " + tblNm + "\n");
			// 조건절시작
			queryText.append("WHERE\n");
			
			int attatchAsset = 0;
			//자산아이디가 있을경우..
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				String tabl_nm = (String) dataMap.get("GEN_TABLE_NM");
				if ("ASSET_ID".equalsIgnoreCase(col_id)) {
					queryText.append(col_id + "='" + asset_id + "'" + "\n");
					attatchAsset = 1;
					break;
				}
			}

			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				String tabl_nm = (String) dataMap.get("GEN_TABLE_NM");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					if (attatchAsset > 0) {
						queryText.append("AND ");
					}
					queryText.append(col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
		}
		return queryText.toString();
	}
	

	/**
	 * 
	 * TODO 원장 자동화 수정 쿼리 생성
	 * 
	 * @param attrList
	 * @param assetConfIdList
	 * @param updUserDtList
	 * @param tblColIdList
	 * @param entityMap
	 * @param currentData
	 * @return
	 * @throws NkiaException
	 */
	public String createUpdateQuery(List<HashMap> attrList
								  ,List<HashMap> assetConfIdList
								  ,List<HashMap> updUserDtList
								  ,List<HashMap> tblColIdList
								  ,HashMap entityMap
								  ,HashMap currentData) throws NkiaException{

		StringBuffer queryText = new StringBuffer();
		String conf_id = StringUtil.parseString(entityMap.get("conf_id"));
		String asset_id = StringUtil.parseString(entityMap.get("asset_id"));
		String user_id = StringUtil.parseString(entityMap.get("user_id"));
		String asset_id_hi = StringUtil.parseString(entityMap.get("asset_id_hi"));
		String div_hierachy;
		if(asset_id_hi == null || "".equals(asset_id_hi)) {
			div_hierachy = "M";
		} else {
			div_hierachy = "S";
		}
		
		String tblNm = StringUtil.parseString(entityMap.get("gen_table_nm"));
		int cnt = 0;
		
		queryText.append("UPDATE " + tblNm + " SET\n");
		
				
		if(tblColIdList != null){
			for(int i=0;i<tblColIdList.size();i++){
				
				HashMap tblColDataMap = tblColIdList.get(i);
				String tblUpdateAbleCol = StringUtil.parseString(tblColDataMap.get("COL_ID"));
				
				if (attrList != null && attrList.size() > 0) {
					
					for (int j = 0; j < attrList.size(); j++) {
						
						HashMap dataMap = attrList.get(j);
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						//String popupFn = StringUtil.parseString(dataMap.get("POPUP_NM_FN"));
						String authType = StringUtil.parseString(dataMap.get("AUTH_TYPE"));
						String defaultVal = StringUtil.parseString(dataMap.get("DEFAULT_VAL"));
						String dateType = StringUtil.parseString(dataMap.get("DATE_TYPE"));
							
						if (!"ASSET_ID".equalsIgnoreCase(colId)
						 && !"CONF_ID".equalsIgnoreCase(colId)) {
							
							if(tblUpdateAbleCol.equalsIgnoreCase(colId)){
								
								//디폴트값이 없을때..
								if ("".equalsIgnoreCase(defaultVal)) {
									if (cnt > 0) {
										queryText.append(comma);
									}
									//날짜인경우..
									if("DATE".equalsIgnoreCase(htmlType)){
										queryText.append( colId + " = TO_DATE('"+StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.parseString(currentData.get(name)), "-", "")," ","")+"','"+dateType+"')");
									}else{
										if(null == currentData.get(name)){
											queryText.append( colId + " = '" + "" + "'");
										}else{
											queryText.append( colId + " = '"+StringUtil.replaceSingleQuotation(StringUtil.parseString(currentData.get(name)))+"'");
										}
									}
									queryText.append("\n");
								} else {//디폴트값이 있지만 upd_user_id와 upd_dt 는 따로 세팅 외의 컬럼을 넣기 위해..
									int compareCnt = 0;
									for(int k=0;k<updUserDtList.size();k++){
										HashMap updDataMap = updUserDtList.get(k);
										String updColId = StringUtil.parseString(updDataMap.get("COL_ID"));
										if(updColId.equalsIgnoreCase(colId)){
											compareCnt++;
										}
									}
									if(compareCnt == 0){//upd_user_id,upd_dt 가 아닌경우.. 
										if (cnt > 0) {
											queryText.append(comma);
										}
										if ("USER_ID".equalsIgnoreCase(defaultVal)) {
											queryText.append(colId + " =  '" + user_id+ "'");
										} else if ("SYSDATE".equalsIgnoreCase(defaultVal)) {
											queryText.append(colId + " =  SYSDATE");
										} else {
											queryText.append(colId + " =  ''");
										}
										queryText.append("\n");
									}
								}
								cnt++;
							}
						}
					}
				}
			}
		}
		
		if(updUserDtList != null){ //upd_user_id 와 upd_dt 를 세팅한다.
			for(int i=0;i<updUserDtList.size();i++){
				HashMap updColDataMap = updUserDtList.get(i);
				String updColId = StringUtil.parseString(updColDataMap.get("COL_ID"));
				if(attrList != null && tblColIdList.size() > 0){
					queryText.append(comma);
				}else{
					if(i>0){
						queryText.append(comma);
					}
				}
				if("UPD_USER_ID".equals(updColId)){
					queryText.append(updColId+" = '"+user_id+"'\n");
				}else if("UPD_DT".equals(updColId)){
					queryText.append(updColId+" = SYSDATE \n");
				}
				cnt++;
			}
		}
		
		// 조건절시작
		queryText.append("WHERE\n");
		
		int attatchAsset = 0;
		//자산아이디가 있을경우..
		for (int i = 0; i < assetConfIdList.size(); i++) {
			HashMap dataMap = assetConfIdList.get(i);
			String col_id = (String) dataMap.get("COL_ID");
			if ("ASSET_ID".equalsIgnoreCase(col_id)) {
				queryText.append(col_id + "='" + asset_id + "'" + "\n");
				attatchAsset = 1;
				break;
			}
		}

		int attatchConf = 0;
		//구성 id가 있을경우
		for (int i = 0; i < assetConfIdList.size(); i++) {
			HashMap dataMap = assetConfIdList.get(i);
			String col_id = (String) dataMap.get("COL_ID");
			if ("CONF_ID".equalsIgnoreCase(col_id)) {
				if (attatchAsset > 0) {
					queryText.append("AND ");
				}
				queryText.append(col_id + "='" + conf_id + "'\n");
				attatchConf = 1;
				break;
			}
		}
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO 원장 자동화 등록 쿼리 생성
	 * 
	 * @param attrList
	 * @param assetConfIdList
	 * @param updUserDtList
	 * @param tblColIdList
	 * @param entityMap
	 * @param currentData
	 * @return
	 * @throws NkiaException
	 */
	public String createInsertQuery(List<HashMap> attrList, List<HashMap> assetConfIdList, List<HashMap> updUserDtList, List<HashMap> tblColIdList,
			HashMap entityMap, HashMap currentData) throws NkiaException {

		StringBuffer queryText = new StringBuffer();
		StringBuffer colQueryText = new StringBuffer();
		StringBuffer valQueryText = new StringBuffer();
		
		String conf_id = StringUtil.parseString(entityMap.get("conf_id"));
		String asset_id = StringUtil.parseString(entityMap.get("asset_id"));
		String user_id = StringUtil.parseString(entityMap.get("user_id"));
		String tblNm = StringUtil.parseString(entityMap.get("gen_table_nm"));
		String asset_id_hi = StringUtil.parseString(entityMap.get("asset_id_hi"));
		String div_hierachy;
		if(asset_id_hi == null || "".equals(asset_id_hi)) {
			div_hierachy = "M";
		} else {
			div_hierachy = "S";
		}
		
		int cnt = 0;

		if (tblColIdList != null) {
			
			for (int i = 0; i < tblColIdList.size(); i++) {

				HashMap tblColDataMap = tblColIdList.get(i);
				String tblUpdateAbleCol = StringUtil.parseString(tblColDataMap.get("COL_ID"));

				if (attrList.size() > 0) {
					for (int j = 0; j < attrList.size(); j++) {
							
						HashMap dataMap = attrList.get(j);
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						String authType = StringUtil.parseString(dataMap.get("AUTH_TYPE"));
						String defaultVal = StringUtil.parseString(dataMap.get("DEFAULT_VAL"));
						String dateType = StringUtil.parseString(dataMap.get("DATE_TYPE"));
						
						if (tblUpdateAbleCol.equalsIgnoreCase(colId)) {
							// 디폴트값이 없을때..
							if ("".equalsIgnoreCase(defaultVal)) {
								if (cnt > 0) {
									colQueryText.append(comma);
									valQueryText.append(comma);
								}
								// 날짜인경우..
								if ("DATE".equalsIgnoreCase(htmlType)) {
									colQueryText.append(colId);
									valQueryText.append("TO_DATE('"+ StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.parseString(currentData.get(name)), "-", ""), " ", "")+ "','" + dateType + "')");
								} else {
									colQueryText.append(colId);
									valQueryText.append("'" + StringUtil.replaceSingleQuotation(StringUtil.parseString(currentData.get(name))) + "'");
								}
								colQueryText.append("\n");
								valQueryText.append("\n");
							} else {// 디폴트값이 있지만 upd_user_id와 upd_dt 는 따로 세팅
									// 외의 컬럼을 넣기 위해..
								int compareCnt = 0;
								for (int k = 0; k < updUserDtList.size(); k++) {
									HashMap updDataMap = updUserDtList.get(k);
									String updColId = StringUtil.parseString(updDataMap.get("COL_ID"));
									if (updColId.equalsIgnoreCase(colId)) {
										compareCnt++;
									}
								}
								if (compareCnt == 0) {// upd_user_id,upd_dt 가 아닌경우..
									if (cnt > 0) {
										colQueryText.append(comma);
										valQueryText.append(comma);
									}
									if ("USER_ID".equalsIgnoreCase(defaultVal)) {
										colQueryText.append(colId);
										valQueryText.append("'" + user_id + "'");
									} else if ("SYSDATE".equalsIgnoreCase(defaultVal)) {
										colQueryText.append(colId);
										valQueryText.append("SYSDATE");
									} else {
										colQueryText.append(colId);
										valQueryText.append("''");
									}
									colQueryText.append("\n");
									valQueryText.append("\n");
								}
							}
							cnt++;
						}
					}
				}
			}
		}
		
		if (updUserDtList != null) { // upd_user_id 와 upd_dt 를 세팅한다.
			for (int i = 0; i < updUserDtList.size(); i++) {
				
				HashMap updColDataMap = updUserDtList.get(i);
				String updColId = StringUtil.parseString(updColDataMap.get("COL_ID"));
				
				if (cnt > 0) {
					colQueryText.append(comma);
					valQueryText.append(comma);
				} else {
					if (i > 0) {
						colQueryText.append(comma);
						valQueryText.append(comma);
					}
				}
				if ("UPD_USER_ID".equals(updColId)) {
					colQueryText.append(updColId);
					valQueryText.append("'" + user_id + "'");
				} else if ("UPD_DT".equals(updColId)) {
					colQueryText.append(updColId);
					valQueryText.append("SYSDATE");
				}
				colQueryText.append("\n");
				valQueryText.append("\n");
				cnt++;
			}
		}

		// 자산아이디가 있을경우..
		for (int i = 0; i < assetConfIdList.size(); i++) {
			HashMap dataMap = assetConfIdList.get(i);
			String updColId = (String) dataMap.get("COL_ID");
			
			if ("ASSET_ID".equalsIgnoreCase(updColId)) {
				if (cnt> 0) {
					colQueryText.append(comma);
					valQueryText.append(comma);
				}
				colQueryText.append(updColId+ "\n");
				valQueryText.append("'" + asset_id + "'" + "\n");
				cnt++;
				break;
			}
		}

		int attatchConf = 0;
		// 구성 id가 있을경우
		for (int i = 0; i < assetConfIdList.size(); i++) {
			HashMap dataMap = assetConfIdList.get(i);
			String updColId = (String) dataMap.get("COL_ID");
			if ("CONF_ID".equalsIgnoreCase(updColId)) {
				if (cnt> 0){
					colQueryText.append(comma);
					valQueryText.append(comma);
				}
				colQueryText.append(updColId+ "\n");
				valQueryText.append("'" + conf_id + "'" + "\n");
				cnt++;
				break;
			}
		}
		
		if(colQueryText != null){
			queryText.append("INSERT INTO " + tblNm +" \n")
							.append("(\n")
							.append(colQueryText.toString()+" \n")
							.append(") VALUES (\n")
							.append(valQueryText.toString())
							.append(")");
			
		}
		
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO prefix 정보
	 * 
	 * @param resource_prefix
	 * @return
	 * @throws NkiaException
	 */
	public JSONArray getGridResource(String resource_prefix) throws NkiaException{
		
		JSONArray jsonArray = new JSONArray();
		
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".text")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".header")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".width")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".align")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".flex")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".sortable")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".xtype")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".tpl")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".hidden")+"'");
		
		return jsonArray;
	}
	
	/**
	 * 
	 * TODO 리스트 형태를 그리드 헤더 정보에 맞게 변환
	 * 
	 * @param collList
	 * @param col_id
	 * @param col_nm
	 * @return
	 * @throws NkiaException
	 */
	public JSONArray getGridResourceByList(List<HashMap> collList,String col_id,String col_nm) throws NkiaException{
		
		JSONArray jsonArray = new JSONArray();
		
		String text = "";
		String header = "";
		String width = "";
		String align = "";
		String flex = "";
		String sortable = "";
		String xtype = "";
		String tpl = "";
		String hidden = "";
		String attachComma = "";
		
		for(int i=0;i<collList.size();i++){
			
			HashMap colDataMap = collList.get(i);
			if(i > 0 ){
				attachComma = comma;
			}
			
			text 	+= attachComma + StringUtil.parseString(colDataMap.get(col_nm));
			header 	+= attachComma + StringUtil.parseString(colDataMap.get(col_id));
			width 	+= attachComma + StringUtil.parseString(colDataMap.get("WIDTH"));
			align 	+= attachComma + StringUtil.parseString(colDataMap.get("ALIGN"));
			flex 	+= attachComma + StringUtil.parseString(colDataMap.get("FLEX"));
			sortable+= attachComma + StringUtil.parseString(colDataMap.get("SORTABLE"));
			xtype 	+= attachComma + StringUtil.parseString(colDataMap.get("XTYPE"));
			tpl 	+= attachComma + StringUtil.parseString(colDataMap.get("TPL"));
			hidden 	+= attachComma + StringUtil.parseString(colDataMap.get("HIDDEN"));
		}
		
		jsonArray.add("'"+text+"'");
		jsonArray.add("'"+header+"'");
		jsonArray.add("'"+width+"'");
		jsonArray.add("'"+align+"'");
		jsonArray.add("'"+flex+"'");
		jsonArray.add("'"+sortable+"'");
		jsonArray.add("'"+xtype+"'");
		jsonArray.add("'"+tpl+"'");
		jsonArray.add("'"+hidden+"'");
		
		return jsonArray;
	}
	
	public JSONArray getInstallCompareData(List<HashMap> collList) throws NkiaException{
		
		JSONArray jsonArray = new JSONArray();
		
		for(int i=0;i<collList.size();i++){
			
			HashMap colMap = collList.get(i);
			String compareCol = StringUtil.parseString(colMap.get("COMPARE_COL"));
			
			if("Y".equals(compareCol)){
				String col_id = StringUtil.parseString(colMap.get("COL_ID"));
				String ems_col_id = StringUtil.parseString(colMap.get("EMS_COL_ID"));
				String dca_col_id = StringUtil.parseString(colMap.get("DCA_COL_ID"));
				
				JSONObject compareData = new JSONObject();
				
				compareData.put("COL_ID", col_id);
				compareData.put("EMS_COL_ID", ems_col_id);
				compareData.put("DCA_COL_ID", dca_col_id);
				
				jsonArray.add(compareData);
			}
		}
		
		return jsonArray;
	}
	
	
	/**
	 * 
	 * TODO 헤더와 텍스트만 리턴
	 * 
	 * @param resource_prefix
	 * @return
	 * @throws NkiaException
	 */
	public HashMap getGridHeaderInfo(String resource_prefix)throws NkiaException{
		
		HashMap dataMap =new HashMap();
		
		String text = messageSource.getMessage(resource_prefix + ".text");
		String header = messageSource.getMessage(resource_prefix + ".header");
		
		String[] textArr = text.split(",");
		String[] headerArr = header.split(",");
		
		for(int i=0;i<textArr.length;i++){
			dataMap.put(textArr[i], headerArr[i]);
		}
		
		return dataMap;
	}
	
	/**
	 * 
	 * TODO 설치 정보 쿼리
	 * 
	 * @param installInfo
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallQuery(HashMap installInfo,List<HashMap> installColList, boolean isAdCompareData)throws NkiaException{
		
		StringBuffer installQuery = new StringBuffer();
		StringBuffer adCompareQuery = new StringBuffer();
		  
		String confId   = StringUtil.parseString(installInfo.get("conf_id"));
		String emsId   = StringUtil.parseString(installInfo.get("ems_id"));
		String dcaId   = StringUtil.parseString(installInfo.get("dca_id"));
		String tableNm = StringUtil.parseString(installInfo.get("TABLE_NM"));
		String viewType = StringUtil.parseString(installInfo.get("view_type"));	//@@ 고은규 수정
		if("info".equals(viewType) || "infmodify".equals(viewType)){				//@@ 고은규 수정
			tableNm = StringUtil.parseString(installInfo.get("INFO_TABLE_NM"));		//@@ 고은규 수정
		}
		String emsShowYn = StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
		String emsYn = StringUtil.parseString(installInfo.get("EMS_YN"));
		String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
		String dcaShowYn = StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
		String dcaYn = StringUtil.parseString(installInfo.get("DCA_YN"));
		String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
		String orderColumn = "";
		
		installQuery.append("SELECT\n");
		
		if( isAdCompareData ){
			adCompareQuery.append("SELECT\n");
		}
		
		for(int i=0;i<installColList.size();i++){
			
			HashMap installColMap = installColList.get(i);
			
			String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
			String dcaColId = StringUtil.parseString(installColMap.get("DCA_COL_ID"));
			String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
			
			if(i>0){
				installQuery.append(comma);
				if( isAdCompareData ){
					adCompareQuery.append(comma);
				}
			}
			
			if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
				installQuery.append(emsColId + "\n");
			}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
				installQuery.append(dcaColId + "\n");
			}else{
				installQuery.append(colId + "\n");
			}	
			
			if( isAdCompareData ){
				adCompareQuery.append(colId + "\n");
			}
			
			if("Y".equalsIgnoreCase(orderCol)){
				if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
					orderColumn = emsColId;
				}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
					orderColumn = dcaColId;
				}else{
					orderColumn = colId;
				}
			}
		}
		
		installQuery.append("FROM\n");
		if( isAdCompareData ){
			adCompareQuery.append("FROM\n");
		}
		
		if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
			installQuery.append(emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' \n");
		}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
			installQuery.append(dcaTblNm + "\n")
			.append("WHERE \n")
			.append("DEVICE_ID = '" + dcaId + "' \n");
			if( isAdCompareData ){
				adCompareQuery.append(tableNm + "\n")
				.append("WHERE \n")
				.append("CONF_ID = '" + confId + "'\n");
				
				installQuery.append("MINUS "+ adCompareQuery.toString() );
			}
		}else{
			installQuery.append(tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "'\n");
		}
		
		if( !"".equalsIgnoreCase(orderColumn) ){
			installQuery.append("ORDER BY " + orderColumn + " \n");
		}
		
		return installQuery.toString();
	}
	
	/**
	 * 
	 * TODO 설치정보 삭제 쿼리
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallDelQuery(HashMap dataMap)throws NkiaException{
		StringBuffer sb = new StringBuffer();
		String tableNm = StringUtil.parseString(dataMap.get("TABLE_NM"));
		String confId = StringUtil.parseString(dataMap.get("CONF_ID"));
		sb.append("DELETE FROM " + tableNm + " WHERE CONF_ID = '" +confId+ "'");
		return sb.toString();
	}
	
	/**
	 * 
	 * TODO 설치정보 등록 쿼리 
	 * 
	 * @param dataMap
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallInsQuery(HashMap dataMap,List<HashMap> installColList)throws NkiaException{
		
		StringBuffer querySb = new StringBuffer();
		StringBuffer colSb = new StringBuffer();
		StringBuffer insSb = new StringBuffer();
		
		String tableNm = StringUtil.parseString(dataMap.get("TABLE_NM"));
		String confId = StringUtil.parseString(dataMap.get("CONF_ID"));
		boolean confIdFlag = false;
		
		for(int i=0;i<installColList.size();i++){
			HashMap installColMap = installColList.get(i);
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			if("CONF_ID".equals(colId)){
				confIdFlag = true;
				break;
			}
		}
		
		if(!confIdFlag){
			colSb.append("CONF_ID"); 
			insSb.append("'"+confId+"'"); 
		}
		
		int roopCnt = 0;
		for(int i=0;i<installColList.size();i++){
			
			HashMap installColMap = installColList.get(i);
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			
			if(!confIdFlag){
				colSb.append(comma);
				insSb.append(comma); 
			}else{
				if(i>0){
					colSb.append(comma);
					insSb.append(comma); 
				}
			}
			colSb.append(colId + "\n");
			insSb.append("'"+StringUtil.parseString(dataMap.get(colId))+"'\n"); 
		}
		
		querySb.append("INSERT INTO " + tableNm + "( \n").append(colSb).append(") VALUES (").append(insSb + ")");
		return querySb.toString(); 	
	}
	
	/**
	 * 
	 * TODO 장비 명명 규칙 폼 생성
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createConfNmCode(HashMap entityMap) throws NkiaException{

		JSONObject serviceProp = new JSONObject();

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");

		serviceProp.put("id", attachQuotes(id));
		serviceProp.put("title", attachQuotes(title));
		serviceProp.put("conf_id", attachQuotes(confId));
		serviceProp.put("assetId", attachQuotes(assetId));
		serviceProp.put("authType", attachQuotes(authType));

		return confcode + lBraket + replaceAllquotes(serviceProp.toString()) + rBraket;
	}
	
	/**
	 * map 의 키셋을 rowercase 로 바꿔준다.
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap replaceRowerKey(HashMap dataMap)throws NkiaException{
		HashMap returnMap = new HashMap();
		if(dataMap != null){
			Iterator iter = dataMap.keySet().iterator();
			
			while (iter.hasNext()) {
				String key = (String) iter.next();
				String value = StringUtil.parseString(dataMap.get(key));
				returnMap.put(key.toLowerCase(), value);
			}
		}
		return returnMap;
	}
	
	
	/**
	 * map 의 키셋을 rowercase 로 바꿔준다.
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap replaceAddRowerKey(HashMap dataMap)throws NkiaException{
		Iterator iter = dataMap.keySet().iterator();
		HashMap returnMap = new HashMap();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			returnMap.put(key.toLowerCase(), dataMap.get(key));
		}
		Iterator iter1 = returnMap.keySet().iterator();
		while (iter1.hasNext()) {
			String key = (String) iter1.next();
			dataMap.put(key, returnMap.get(key));
		}
		return dataMap;
	}
	
	
	/**
	 * 
	 * TODO
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List getConfNmCodeResourceList()throws NkiaException{
		
		List headerList = new ArrayList();
		try{
		String header = messageSource.getMessage("grid.itam.confnmcode.header");
		String text = messageSource.getMessage("grid.itam.confnmcode.text");
		String code = messageSource.getMessage("grid.itam.confnmcode.code");
		
		String[] headerArr = header.split(",");
		String[] textArr = text.split(",");
		String[] codeArr = code.split(",");
		
		for(int i=0 ;i<headerArr.length;i++){
			
			HashMap dataMap = new HashMap();
			dataMap.put("COL_ID", headerArr[i]);
			dataMap.put("COL_NAME", textArr[i]);
			
			String sysCode = codeArr[i];
			
			if(!"NA".equalsIgnoreCase(sysCode)){
				dataMap.put("SYS_CODE", codeArr[i]);
				dataMap.put("HTML_TYPE", "SELECT");
			}else{
				dataMap.put("SYS_CODE", null);
				dataMap.put("HTML_TYPE", "TEXT");
			}
			dataMap.put("POPUP_CODE", null);
			dataMap.put("DATE_TYPE", null);
			
			headerList.add(dataMap);
		}
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return headerList;
	}
	
	/**
	 * 
	 * 베이스 리스트의 데이터를 타겟 리스트에 넣어준다.
	 * TODO
	 * 
	 * @param baseList
	 * @param targetList
	 * @return
	 * @throws NkiaException
	 */
	public List addTargetList(List<HashMap> baseList,List<HashMap> targetList)throws NkiaException{
		if(baseList != null){
			for(int i=0;i<baseList.size();i++){
				HashMap baseMap = baseList.get(i);
				targetList.add(baseMap);
			}
		}
		return targetList;
	}
	
	public JSONArray getMergeGridResource(JSONArray baseColArr,JSONArray inColArr,String prefix)throws NkiaException{
		
		JSONArray returnArray = new JSONArray();
		
		for(int i=0;i<baseColArr.size();i++){
			String baseCol = (String)baseColArr.get(i);
			String inCol = (String)inColArr.get(i);
			baseCol =  StringUtil.replaceAll(baseCol.replace(prefix,inCol),"'","");
			returnArray.add(baseCol);
		}
		
		return returnArray;
	}
	
	/**
	 * 
	 * TODO 설치 정보 이력 생성 쿼리
	 * 
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallHisQuery(List<HashMap> installColList)throws NkiaException{
		StringBuffer sb = new StringBuffer();
		if(installColList != null){
			for(int i=0;i<installColList.size();i++){
				HashMap installColMap = installColList.get(i);
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				sb.append(comma + "MAX(DECODE(CHG_COLUMN_ID,'"+colId+"',CHG_PREV_VALUE,null)) AS " + colId + "\n");
			}
		}
		return sb.toString();
	}

	/**
	 * 
	 * TODO 정보수집서 조회 쿼리 생성
	 * 
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createOpmsInfoSelectQuery(List<HashMap> attrList, List<HashMap> hiddenList, List<HashMap> tblList, List<HashMap> joinList,
			List<HashMap> assetConfIdList, HashMap entityMap)throws NkiaException {

		StringBuffer queryText = new StringBuffer();
		String conf_id = (String) entityMap.get("conf_id");
		String asset_id = (String) entityMap.get("asset_id");
		String class_id = (String) entityMap.get("class_id");
		String class_type = (String) entityMap.get("class_type");
		
		if(tblList != null && tblList.size()>0){
			queryText.append("SELECT\n");
			queryText.append("ASSET_TEMP_ID\n");
			queryText.append(comma + "CONF_TEMP_ID\n");
			if(attrList != null){
				if (attrList.size() > 0) {
					for (int i = 0; i < attrList.size(); i++) {
		
						HashMap dataMap = attrList.get(i);
						
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						String popupFn = StringUtil.parseString(dataMap.get("POPUP_NM_FN"));
		

						queryText.append(comma);

						if (htmlType.equalsIgnoreCase("TEXT")) {
							queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
						} else if (htmlType.equalsIgnoreCase("TEXTAREA")) { // 텍스트 박스
							queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
						} else if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
							String dateType = (String) dataMap.get("DATE_TYPE");
							if (dateType.equals(timeDateType)) {
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+dayDateType+"'),'"+timeDateType+"'),null),null)) AS " + name);
								queryText.append(comma + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+dayDateType+"'),'"+dayDateType+"'),null),null)) AS " + name + "_DAY");
								queryText.append(comma + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+dayDateType+"'),'"+hourDateType+"'),null),null)) AS " + name + "_HOUR");
								queryText.append(comma + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+dayDateType+"'),'"+minDateType+"'),null),null)) AS " + name + "_MIN");
							} else {
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+dayDateType+"'),'"+dayDateType+"'),null),null)) AS " + name);
								queryText.append(comma + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+ dayDateType+"'),'"+ dayDateType+"'),null),null)) AS " + name+ "_DAY");
							}
						} else if (htmlType.equalsIgnoreCase("SELECT")) {// 콤보
							queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
						} else if (htmlType.equalsIgnoreCase("RADIO")) {// 라디오
							queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
						} else if (htmlType.equalsIgnoreCase("CHECK")) {// 체크
							queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
						} else if (htmlType.equalsIgnoreCase("POPUP")) {// 팝업
							queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
							if (!"".equalsIgnoreCase(popupFn)) {
								queryText.append("\n");
								queryText.append(comma);
								String popupColIds = "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null))";
								
								// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
								popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
								
								popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
								queryText.append(popupFn + " AS " + name + popupPrefix);
							}
						}
						queryText.append("\n");
					}
				}
			}
			/*
			if(hiddenList != null){
				if (hiddenList.size() > 0) {
					for (int i = 0; i < hiddenList.size(); i++) {
		
						HashMap dataMap = hiddenList.get(i);
		
						String htmlType = (String) dataMap.get("HTML_TYPE");
						String colId = (String) dataMap.get("COL_ID");
						String name = (String) dataMap.get("MK_COMP_NM");
						String table = (String) dataMap.get("GEN_TABLE_NM");
		
						if (attrList.size() > 0) {
							queryText.append(comma);
						} else {
							if (i > 0) {
								queryText.append(comma);
							}
						}
						if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
							String dateType = (String) dataMap.get("DATE_TYPE");
							if (dateType.equals(timeDateType)) {
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+ dayDateType+"'),'"+ timeDateType+"'),null),null)) AS " + name);
							} else {
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+ dayDateType+"'),'"+ dayDateType+"'),null),null)) AS " + name);
							}
						} else {
							queryText.append(table + dot + colId + " AS " + name + "\n");
						}
					}
				}
			}
			*/
			queryText.append("\n FROM (\n");
			queryText.append("SELECT ASSET_TEMP_ID\n");
			queryText.append(comma + "CONF_TEMP_ID\n");
			queryText.append(comma + "COLUMN_ID\n");
			queryText.append(comma + "TABLE_NM\n");
			queryText.append(comma + "COL_VALUE\n");
			queryText.append("FROM AM_TEMP_DATA\n");
			queryText.append("WHERE CONF_TEMP_ID = '"+conf_id+"'\n");
			queryText.append("  AND ASSET_TEMP_ID = '"+asset_id+"'\n");
			queryText.append(" )\n");
			queryText.append("GROUP BY ASSET_TEMP_ID, CONF_TEMP_ID");
		}
		return queryText.toString();
	}	
	
	/**
	 * 
	 * TODO 설치 속성 업데이트 쿼리를 생성한다.
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public String createAutoInstallUpdateQuery(HashMap paramMap,List<HashMap> assetConfIdList, String adTarget)throws NkiaException{
		
		StringBuffer queryText = new StringBuffer();
		
		String asset_id = StringUtil.parseString(paramMap.get("asset_id"));
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		String table_nm = StringUtil.parseString(paramMap.get("gen_table_nm"));
		String am_col_id = StringUtil.parseString(paramMap.get("COL_ID"));
		String emsValue = StringUtil.parseString(paramMap.get("EMS_VALUE"));
		String dcaValue = StringUtil.parseString(paramMap.get("DCA_VALUE"));
		
		
		if(!"".equals(table_nm) && !"".equals(am_col_id)){
			
			queryText.append("UPDATE " + table_nm + " SET \n");
			if( adTarget.equals("EMS") ){
				queryText.append(am_col_id + " = '"+emsValue+"' \n");
			}else if( adTarget.equals("DCA") ){
				queryText.append(am_col_id + " = '"+dcaValue+"' \n");
			}
			
			// 조건절시작
			queryText.append("WHERE\n");
			
			int attatchAsset = 0;
			//자산아이디가 있을경우..
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("ASSET_ID".equalsIgnoreCase(col_id)) {
					queryText.append(col_id + "='" + asset_id + "'" + "\n");
					attatchAsset = 1;
					break;
				}
			}
	
			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					if (attatchAsset > 0) {
						queryText.append("AND ");
					}
					queryText.append(col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
		}
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO 설치 속성 업데이트 쿼리를 생성한다.
	 * @return 
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public ArrayList createPanelBtnComp(List<HashMap> dataList, HashMap paramMap)throws NkiaException{
		ArrayList createBtnComp = new ArrayList();
		String btn = new String();
		for(int i=0; i<dataList.size(); i++ ){
			HashMap dataMap = dataList.get(i);
			String btn_id = (String) dataMap.get("BTN_ID");
			String btn_nm = (String) dataMap.get("BTN_NM");
			String conf_id = (String) paramMap.get("conf_id");
			String class_type = (String) paramMap.get("class_type");
			String tangible_asset_yn = (String) paramMap.get("tangible_asset_yn");
			
			JSONObject btnProp = new JSONObject();
			
			btnProp.put("command", attachQuotes(btn_id) );
			btnProp.put("btn_nm", attachQuotes(btn_nm) );
			btnProp.put("conf_id", attachQuotes(conf_id));
			btnProp.put("class_type", attachQuotes(class_type));
			btnProp.put("tangible_asset_yn", attachQuotes(tangible_asset_yn));
			
			btn = panelBtn + lBraket + btnProp.toString() + rBraket;
			createBtnComp.add(btn);
			
		}
		
		return createBtnComp;
	}
	
	/**
	 * 
	 * TODO 설치 정보 쿼리
	 * 
	 * @param installInfo
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createAutoInstallQuery(HashMap installInfo,List<HashMap> installColList)throws NkiaException{
		
		StringBuffer sb = new StringBuffer();
		
		String confId   = StringUtil.parseString(installInfo.get("conf_id"));
		String emsId   = StringUtil.parseString(installInfo.get("ems_id"));
		String tableNm = StringUtil.parseString(installInfo.get("TABLE_NM"));
		String viewType = StringUtil.parseString(installInfo.get("view_type"));	//@@ 고은규 수정
		if("info".equals(viewType) || "infmodify".equals(viewType)){				//@@ 고은규 수정
			tableNm = StringUtil.parseString(installInfo.get("INFO_TABLE_NM"));		//@@ 고은규 수정
		}
		String emsShowYn = StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
		String emsYn = StringUtil.parseString(installInfo.get("EMS_YN"));
		String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
		String orderColumn = "";
		
		sb.append("SELECT\n");
		
		for(int i=0;i<installColList.size();i++){
			
			HashMap installColMap = installColList.get(i);
			
			String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
			String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
			//	String sysCode = StringUtil.parseString(installColMap.get("SYS_CODE"));
			
			if(i>0){
				sb.append(comma);
			}
			
			if("Y".equalsIgnoreCase(emsShowYn)){
				sb.append(emsColId + "\n");
			}else{
				sb.append(colId + "\n");
			}	
			
			if("Y".equalsIgnoreCase(orderCol)){
				if("Y".equalsIgnoreCase(emsShowYn)){
					orderColumn = emsColId;
				}else{
					orderColumn = colId;
				}
			}
		}
		if(!"Y".equalsIgnoreCase(emsShowYn)){
			sb.append(comma)
			.append("GUBUN\n");
		}
		sb.append("FROM\n");
		
		if("Y".equalsIgnoreCase(emsShowYn)){
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(comma);
				}
				sb.append(emsColId + "\n");
				
			}
			sb.append("FROM " + emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' \n");
			sb.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(comma);
				}
				sb.append(colId + " AS "+ emsColId + "\n");
				
			}
			
			sb.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "') \n");
			
		}else{
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(comma);
				}
				sb.append(colId + "\n");
				
			}
			sb.append(comma)
			.append("'D' AS GUBUN \n")
			.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "' \n")
			.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(comma);
				}
				sb.append(emsColId + " AS "+ colId + "\n");
				
			}
			sb.append(comma)
			.append("'D' AS GUBUN \n")
			.append("FROM " + emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' \n")
			.append(" UNION \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(comma);
				}
				sb.append(colId + "\n");
				
			}
			sb.append(comma)
			.append("'S' AS GUBUN \n")
			.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "' \n")
			.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(comma);
				}
				sb.append(colId + "\n");
				
			}
			sb.append(comma)
			.append("'S' AS GUBUN \n")
			.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "' \n")
			.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(comma);
				}
				sb.append(emsColId + " AS "+ colId + "\n");
				
			}
			sb.append(comma)
			.append("'S' AS GUBUN \n")
			.append("FROM " + emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' )\n )\n )\n");			
			
		}
		
		if("Y".equalsIgnoreCase(orderColumn) && !"".equalsIgnoreCase(orderColumn)){
		//if(!"".equals(orderColumn)){
			/*if("INDX".equals(orderColumn)){
				sb.append("ORDER BY TO_NUMBER("+orderColumn+") \n");				
			}else{
				sb.append("ORDER BY " + orderColumn +" \n");								
			}*/
			sb.append("ORDER BY " + orderColumn +" \n");
		}
		
		return sb.toString();
	}
	
	/**
	 * 
	 * TODO 설치 속성 업데이트 쿼리를 생성한다.
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public String createAutoInstallUpdateQuery(HashMap paramMap,List<HashMap> assetConfIdList)throws NkiaException{
		
		StringBuffer queryText = new StringBuffer();
		
		String asset_id = StringUtil.parseString(paramMap.get("asset_id"));
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		String table_nm = StringUtil.parseString(paramMap.get("gen_table_nm"));
		String am_col_id = StringUtil.parseString(paramMap.get("COL_ID"));
		String emsValue = StringUtil.parseString(paramMap.get("EMS_VALUE"));
		
		
		if(!"".equals(table_nm) && !"".equals(am_col_id)){
			
			queryText.append("UPDATE " + table_nm + " SET \n");
			queryText.append(am_col_id + " = '"+emsValue+"' \n");
			// 조건절시작
			queryText.append("WHERE\n");
			
			int attatchAsset = 0;
			//자산아이디가 있을경우..
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("ASSET_ID".equalsIgnoreCase(col_id)) {
					queryText.append(col_id + "='" + asset_id + "'" + "\n");
					attatchAsset = 1;
					break;
				}
			}
	
			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					if (attatchAsset > 0) {
						queryText.append("AND ");
					}
					queryText.append(col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
		}
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO 장애 이력 컴포넌트 생성
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createIncidentHist(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject updateProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);
		
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");

		updateProp.put("id", attachQuotes(id));
		updateProp.put("title", attachQuotes(title));
		updateProp.put("conf_id", attachQuotes(confId));
		updateProp.put("asset_id", attachQuotes(assetId));
		updateProp.put("authType", attachQuotes(authType));
		updateProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		return incidentHist + lBraket + replaceAllquotes(updateProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 변경 이력 컴포넌트 생성
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createChangeHist(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject updateProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);
		
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");

		updateProp.put("id", attachQuotes(id));
		updateProp.put("title", attachQuotes(title));
		updateProp.put("conf_id", attachQuotes(confId));
		updateProp.put("asset_id", attachQuotes(assetId));
		updateProp.put("authType", attachQuotes(authType));
		updateProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		return changeHist + lBraket + replaceAllquotes(updateProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 문제 이력 컴포넌트 생성
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createProblemHist(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject updateProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);
		
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");

		updateProp.put("id", attachQuotes(id));
		updateProp.put("title", attachQuotes(title));
		updateProp.put("conf_id", attachQuotes(confId));
		updateProp.put("asset_id", attachQuotes(assetId));
		updateProp.put("authType", attachQuotes(authType));
		updateProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		return problemHist + lBraket + replaceAllquotes(updateProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 서비스요청 이력 컴포넌트 생성
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createServiceReqHist(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject updateProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);
		
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");

		updateProp.put("id", attachQuotes(id));
		updateProp.put("title", attachQuotes(title));
		updateProp.put("conf_id", attachQuotes(confId));
		updateProp.put("asset_id", attachQuotes(assetId));
		updateProp.put("authType", attachQuotes(authType));
		updateProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		return serviceHist + lBraket + replaceAllquotes(updateProp.toString()) + rBraket;
	}
	
	/**
	 * DCA 연계PC S/W정보 리스트
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createPcSwList(List attrList, HashMap entityMap) throws NkiaException {
		JSONObject pcSwProp = new JSONObject();
		JSONArray resource = getGridResource("grid.itam.opms.pcSwList"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");

		pcSwProp.put("id", attachQuotes(id));
		pcSwProp.put("title", attachQuotes(title));
		pcSwProp.put("conf_id", attachQuotes(confId));
		pcSwProp.put("authType", attachQuotes(authType));
		pcSwProp.put("gridFormItem",resource.toString());
		pcSwProp.put("view_type",attachQuotes(view_type));
		return pcSw + lBraket + replaceAllquotes(pcSwProp.toString()) + rBraket;
	}
	
	/**
	 * 하위 논리 리스트
	 * 
	 * @param entityMap
	 * @return
	 * @throws NkiaException 
	 */
	public String createRelLogicList(List attrList, HashMap entityMap) throws NkiaException {
		JSONObject logicProp = new JSONObject();
		JSONArray resource = getGridResource("grid.itam.opms.rel.logic"); 

		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String view_type = (String) entityMap.get("view_type");

		logicProp.put("id", attachQuotes(id));
		logicProp.put("title", attachQuotes(title));
		logicProp.put("conf_id", attachQuotes(confId));
		logicProp.put("authType", attachQuotes(authType));
		logicProp.put("gridFormItem",resource.toString());
		logicProp.put("view_type",attachQuotes(view_type));
		return relLogic + lBraket + replaceAllquotes(logicProp.toString()) + rBraket;
	}
	
	/**
	 * 
	 * TODO 팝업용 DB Function에서 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
	 * 
	 * @param popupColIds
	 * @param colId
	 * @param asset_id
	 * @param conf_id
	 * @param class_id
	 * @param class_type
	 * @return String
	 * @throws NkiaException
	 */
	private String getAddOtherDataParams(
			String popupColIds,
			String colId,
			String asset_id,
			String conf_id,
			String class_id,
			String class_type
		) throws NkiaException {
		
		String result = popupColIds;
		
		if(colId != null && !colId.equals("")) {
			
			if("SW_GROUP_OPTION_ID".equals(colId)) {
				// 소프트웨어 그룹 옵션의 경우 인자값이 2개가 들어가야 하므로 추가 해준다 -20140326 정정윤
				result = result + ", '" + asset_id + "'"; 
			} else if("DIV_WORK".equals(colId)) {
				// 업무 분류의 경우 인자값이 2개 들어가야하므로 추가해준다. 20161128 좌은진
				result = "'DIV_WORK'," + result; 
			}
			
		}
		
		return result;
	}
	
	/**
	 * 
	 * TODO 작업기록(유지보수) 이력 컴포넌트 생성 -2016.04.07.정정윤
	 * NH기능 적용 - 2016.12.30좌은진 
	 * 
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public String createMaintJobHistory(List attrList, HashMap entityMap)throws NkiaException{
		JSONObject updateProp = new JSONObject();
		JSONArray itemArray = JSONArray.fromObject(attrList);
		
		String id = (String) entityMap.get("ENTITY_ID");
		String title = (String) entityMap.get("ENTITY_NM");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String confId = (String) entityMap.get("conf_id");
		String assetId = (String) entityMap.get("asset_id");
		String classType = (String) entityMap.get("class_type");
		String viewType = (String) entityMap.get("view_type");
		
		if(null != confId && !"".equals(confId)) {
			classType = confId.substring(0, 2);
		}

		updateProp.put("id", attachQuotes(id));
		updateProp.put("title", attachQuotes(title));
		updateProp.put("conf_id", attachQuotes(confId));
		updateProp.put("asset_id", attachQuotes(assetId));
		updateProp.put("authType", attachQuotes(authType));
		updateProp.put("class_type", attachQuotes(classType));
		updateProp.put("view_type", attachQuotes(viewType));
		
		//작업기록 등록 팝업 -> 등록자 필요
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = userVO.getUser_id();
		String userNm = userVO.getUser_nm();
		updateProp.put("user_id", attachQuotes(userId));
		updateProp.put("user_nm", attachQuotes(userNm));
		
		updateProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		
		return maintJob + lBraket + replaceAllquotes(updateProp.toString()) + rBraket;
	}
}
