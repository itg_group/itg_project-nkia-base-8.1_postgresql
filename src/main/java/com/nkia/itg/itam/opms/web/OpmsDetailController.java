package com.nkia.itg.itam.opms.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelRackInfo;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import net.sf.json.JSONArray;

@Controller
public class OpmsDetailController {
	
	private static final Logger logger = LoggerFactory.getLogger(OpmsDetailController.class);
	
	@Resource(name = "assetAutomationService")
	private AssetAutomationService assetAutomationService;
	
	@Resource(name = "commonCodeService")
	private CommonCodeService commonCodeService;
	
	@Resource(name="excelRackInfo")
	private ExcelRackInfo excelRackInfo;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 분류별 설정정보를 가져와서 화면을 그려주고 해당 데이터를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/opmsDetailInfo.do")
	public String opmsMainTab(HttpServletRequest request,ModelMap paramMap) throws NkiaException {
		
		UserVO userVO		= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String user_id			= userVO.getUser_id();
		String conf_id 			= StringUtil.parseString(request.getParameter("conf_id"));
		String asset_id 		= StringUtil.parseString(request.getParameter("asset_id"));
		String class_id 		= StringUtil.parseString(request.getParameter("class_id"));
		String class_type 	= StringUtil.parseString(request.getParameter("class_type"));
		String view_type 		= StringUtil.parseString(request.getParameter("view_type"));
		String entity_class_id 		= StringUtil.parseString(request.getParameter("entity_class_id"));
		String tangible_asset_yn 	= StringUtil.parseString(request.getParameter("tangible_asset_yn"));
		String logical_yn 	= StringUtil.parseString(request.getParameter("logical_yn"));
		String physi_conf_id 	= StringUtil.parseString(request.getParameter("physi_conf_id"));
		String regist_type 	= StringUtil.parseString(request.getParameter("regist_type"));
		String pop_up_yn 		= StringUtil.parseString(request.getParameter("pop_up_yn"));
		
		paramMap.put("user_id"			, user_id);
		paramMap.put("conf_id"			, conf_id);
		paramMap.put("asset_id"		, asset_id);
		paramMap.put("class_id"		, class_id);
		paramMap.put("entity_class_id", entity_class_id);
		paramMap.put("class_type"	, class_type);
		paramMap.put("entity_id"		, entity_class_id);
		paramMap.put("view_type"		, view_type);
		paramMap.put("pop_up_yn"		, pop_up_yn);
		paramMap.put("logical_yn"		, logical_yn);
		paramMap.put("tangible_asset_yn"		, tangible_asset_yn);
		paramMap.put("physi_conf_id"		, physi_conf_id);
		paramMap.put("regist_type"		, regist_type);
		
		assetAutomationService.selectClassEntitySetupInfo(paramMap);
		
		if(view_type.equals("pop_view") && pop_up_yn.equals("Y")) {
			paramMap.put("view_type", "pop_up_view");
		}
		
		return "/itg/itam/opms/opmsDetail.nvf";
	}
	
	/**
	 * 선택된 운영자 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsSelMngUserList.do")
	public @ResponseBody ResultVO selectOpmsSelMngUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsSelMngUserList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 운영자 선택 팝업 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsMngUserList.do")
	public @ResponseBody ResultVO selectOpmsMngUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			commonCodeService.getSearchList(paramMap,"searchKey","searchValue","SEARCH_USER");
			int cnt	= assetAutomationService.selectOpmsMngUserCnt(paramMap);
			List rows = assetAutomationService.selectOpmsMngUserList(paramMap);
			gridVO.setRows(rows);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(cnt);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 선택 팝업 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsUserList.do")
	public @ResponseBody ResultVO selectOpmsUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			commonCodeService.getSearchList(paramMap,"searchKey","searchValue","SEARCH_USER");
			int cnt	= assetAutomationService.selectOpmsUserCnt(paramMap);
			List rows = assetAutomationService.selectOpmsUserList(paramMap);
			gridVO.setRows(rows);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(cnt);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 사용자 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsSelUserList.do")
	public @ResponseBody ResultVO selectOpmsSelUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsSelUserList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티코드 리스트 (사용자 서비스 항목)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/searchCodeDataList.do")
	public @ResponseBody ResultVO searchCodeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = null;
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try{
			resultList = assetAutomationService.searchCodeDataList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}	
	
	
	/**
	 * 해당 모자산의 자자산 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsSelChildList.do")
	public @ResponseBody ResultVO selectOpmsSelChildList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsSelChildList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 해당 자산의 구매정보 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsSelPurInfoList.do")
	public @ResponseBody ResultVO selectOpmsSelPurInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsSelPurInfoList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 해당 자산의 감가상각정보 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsDepreciationInfoList.do")
	public @ResponseBody ResultVO selectOpmsDepreciationInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsDepreciationInfoList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 연관 장비 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsRelConfList.do")
	public @ResponseBody ResultVO selectOpmsRelConfList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsRelConfList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 분류 별 분류 트리가져오기..
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectRelConfClassTree.do")
	public @ResponseBody ResultVO selectRelConfClassTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = new TreeVO();
			List result = assetAutomationService.selectRelConfClassTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(result, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 선택 팝업
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsConfList.do")
	public @ResponseBody ResultVO selectOpmsConfList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			commonCodeService.getSearchList(paramMap,"searchKey","searchValue","SEARCH_USER");
			int cnt	= assetAutomationService.selectOpmsConfCnt(paramMap);
			List rows = assetAutomationService.selectOpmsConfList(paramMap);
			gridVO.setRows(rows);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(cnt);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 연관 서비스 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsRelServiceList.do")
	public @ResponseBody ResultVO selectOpmsRelServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsRelServiceList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스 부서 콤보박스가져오기..
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectServiceCustList.do")
	public @ResponseBody ResultVO selectServiceCustList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectServiceCustList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 서비스 선택 팝업 서비스 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsServiceList.do")
	public @ResponseBody ResultVO selectOpmsServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			//commonCodeService.getSearchList(paramMap,"searchKey","searchValue","SEARCH_USER")
			int cnt	= assetAutomationService.selectOpmsServiceCnt(paramMap);
			List rows = assetAutomationService.selectOpmsServiceList(paramMap);
			gridVO.setRows(rows);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(cnt);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 설치정보 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsInstallInfoList.do")
	public @ResponseBody ResultVO selectOpmsInstallInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsInstallInfoList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산 수정
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/amdb/updateOpmsDetail.do")
	public @ResponseBody ResultVO updateOpmsDetail(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			/*if(assetAutomationService.isDuplicateAsset(paramMap)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"호스트명"}));
			} else {*/
				UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
				assetAutomationService.updateOpmsDetail(paramMap);
			/*}*/
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산 등록
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/amdb/registOpmsDetail.do")
	public @ResponseBody ResultVO registOpmsDetail(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			/*if(assetAutomationService.isDuplicateAsset(paramMap)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"호스트명"}));
			} else {*/
				UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
				assetAutomationService.registOpmsDetail(paramMap);
			/*}*/
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 및 구성 유형별 트리를 가져온다.
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectConfClassTree.do")
	public @ResponseBody ResultVO selectConfClassTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			TreeVO treeVO = new TreeVO();
			List result = new ArrayList();
			String mngType = new String();
			result = assetAutomationService.searchConfTypeTree(paramMap);
			
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(result, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	

	/**
	 * 
	 * 가상서버(클라우드와 연계된 가상서버) 목록
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsVmList.do")
	public @ResponseBody ResultVO selectOpmsVmList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsVmList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 폼 데이터 수정 이력을 가져온다.
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsFormModList.do")
	public @ResponseBody ResultVO selectOpmsFormModList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			commonCodeService.getSearchList(paramMap,"modSearchKey","modSearchValue","MOD_SEARCH_KEY");
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsFormModList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 컴포넌트별 수정이력을 모아서 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsCompModList.do")
	public @ResponseBody ResultVO selectOpmsCompModList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			commonCodeService.getSearchListNotIn(paramMap,"modSearchKey","modSearchValue","MOD_SEARCH_KEY","CHG_COLUMN_ID,CHG_COLUMN_NM");
			List rows = assetAutomationService.selectOpmsCompModList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 
	 * TODO 속성별 수정 상세이력
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectFormAttrModList.do")
	public @ResponseBody ResultVO selectFormAttrModList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			GridVO gridVO = new GridVO();
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			commonCodeService.getSearchList(paramMap,"modSearchKey","modSearchValue","MOD_SEARCH_KEY");
			List rows = assetAutomationService.selectFormAttrModList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
			
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

	@RequestMapping(value="/itg/itam/amdb/selectCompDataColumn.do")
	public @ResponseBody ResultVO selectCompDataColumn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			JSONArray jsonArray = assetAutomationService.selectCompDataColumn(paramMap);
			
			resultVO.setResultString(jsonArray.toString());
			
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/opms/selectCompDetailModList.do")
	public @ResponseBody ResultVO selectCompDetailModList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			GridVO gridVO = new GridVO();
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			commonCodeService.getSearchListNotIn(paramMap,"modSearchKey","modSearchValue","MOD_SEARCH_KEY","CHG_COLUMN_ID,CHG_COLUMN_NM");
			List rows = assetAutomationService.selectCompDetailModList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
			
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 버튼 선택 트리 팝업
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsBtnTree.do")
	public @ResponseBody ResultVO searchAmClassCheckTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = new CheckTreeVO();
			List resultList = assetAutomationService.selectOpmsBtnTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, true);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 분류별 설정정보를 가져와서 화면을 그려주고 해당 데이터를 가져온다.
	 * 부서/사용자별 자산현황 그리드()
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/userDeptStateDetail.do")
	public String userDeptStateDetail(HttpServletRequest request,ModelMap paramMap) throws NkiaException {
		
		UserVO userVO		= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String user_id			= userVO.getUser_id();
		String conf_id 			= StringUtil.parseString(request.getParameter("conf_id"));
		String asset_id 		= StringUtil.parseString(request.getParameter("asset_id"));
		String class_id 		= StringUtil.parseString(request.getParameter("class_id"));
		String class_type 	= StringUtil.parseString(request.getParameter("class_type"));
		String view_type 		= StringUtil.parseString(request.getParameter("view_type"));
		String entity_class_id 		= StringUtil.parseString(request.getParameter("entity_class_id"));
		String tangible_asset_yn 	= StringUtil.parseString(request.getParameter("tangible_asset_yn"));
		String logical_yn 	= StringUtil.parseString(request.getParameter("logical_yn"));
		String physi_conf_id 	= StringUtil.parseString(request.getParameter("physi_conf_id"));
		String regist_type 	= StringUtil.parseString(request.getParameter("regist_type"));
		
		paramMap.put("user_id"			, user_id);
		paramMap.put("conf_id"			, conf_id);
		paramMap.put("asset_id"		, asset_id);
		paramMap.put("class_id"		, class_id);
		paramMap.put("entity_class_id", entity_class_id);
		paramMap.put("class_type"	, class_type);
		paramMap.put("entity_id"		, entity_class_id);
		paramMap.put("view_type"		, view_type);
		paramMap.put("logical_yn"		, logical_yn);
		paramMap.put("tangible_asset_yn"		, tangible_asset_yn);
		paramMap.put("physi_conf_id"		, physi_conf_id);
		paramMap.put("regist_type"		, regist_type);
		
		assetAutomationService.selectClassEntitySetupInfo(paramMap);
		
		return "/itg/itam/opms/userDeptStateDetail.nvf";
	}
	/**
	 * 모델속성을 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/searchModelTreeComp.do")
	public @ResponseBody ResultVO searchModelTreeComp(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetAutomationService.searchModelTreeComp(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**  
	 * 업무분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/searchWorkTreeComp.do")
	public @ResponseBody ResultVO searchWorkTreeComp(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetAutomationService.searchWorkTreeComp(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 업무분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/searchHISOSTreeComp.do")
	public @ResponseBody ResultVO searchHISOSTreeComp(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetAutomationService.searchHISOSTreeComp(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 설치된 소프트웨어 목록 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsInsSwList.do")
	public @ResponseBody ResultVO selectOpmsInsSwList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsInsSwList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

	//
	/**
	 * 설치 소프트웨어와 맵핑되는 소프트웨어 옵션 전체 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOptionMappingList.do")
	public @ResponseBody ResultVO selectOptionMappingList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOptionMappingList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 설치 소프트웨어와 맵핑되는 선택되어있던 소프트웨어 옵션 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectSelectedOptionList.do")
	public @ResponseBody ResultVO selectSelectedOptionList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectSelectedOptionList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	/**
	 * 유지보수 정보 폼 데이터 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectAssetMaintInfo.do")
	public @ResponseBody ResultVO selectAssetMaintInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap param = (ModelMap)assetAutomationService.selectAssetMaintInfo(paramMap);
			resultVO.setResultMap(param);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 모자산 선택 팝업
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectMotherList.do")
	public @ResponseBody ResultVO selectMotherList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			int cnt	= assetAutomationService.selectMotherListCnt(paramMap);
			List rows = assetAutomationService.selectMotherList(paramMap);
			gridVO.setRows(rows);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(cnt);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 모자산 변경 서비스
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/updateMotherInfo.do")
	public @ResponseBody ResultVO updateMotherInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			assetAutomationService.updateMotherInfo(paramMap);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;

	}
	
	/**
	 * 자산 도입년도 목록 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectAmIntroDtGroupBy.do")
	public @ResponseBody ResultVO selectAmIntroDtGroupBy(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectAmIntroDtGroupBy(paramMap);
			gridVO.setRows(rows);
			gridVO.setTotalCount(rows.size());
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;

	}
	
	/**
	 * 자품 변경 이력 등록
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/insertChildChgHis.do")
	public @ResponseBody ResultVO insertChildChgHis(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			assetAutomationService.insertChildChgHis(paramMap);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자품 변경 이력 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectChildChgHisList.do")
	public @ResponseBody ResultVO selectChildChgHisList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectChildChgHisList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자품 변경 이력 상세 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectChildChgHisDetail.do")
	public @ResponseBody ResultVO selectChildChgHisDetail(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectChildChgHisDetail(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 논리서버 이력 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsLogicHisList.do")
	public @ResponseBody ResultVO selectOpmsLogicHisList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsLogicHisList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산등록 시 클래스 명
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/opms/searchClassNm.do")
	public @ResponseBody ResultVO searchClassNm(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = assetAutomationService.searchClassNm(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 장애 이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchProcHist.do")
	public @ResponseBody ResultVO searchProcHist(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.searchProcHist(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 변경 이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchChangeProcHist.do")
	public @ResponseBody ResultVO searchChangeProcHist(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.searchChangeProcHist(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 문제 이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchProblemProcHist.do")
	public @ResponseBody ResultVO searchProblemProcHist(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.searchProblemProcHist(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 서비스 이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchServiceProcHist.do")
	public @ResponseBody ResultVO searchServiceProcHist(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.searchServiceProcHist(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * DCA 연계 PC S/W 목록
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectIfPcSwList.do")
	public @ResponseBody ResultVO selectIfPcSwList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectIfPcSwList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 논리 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsRelLogicList.do")
	public @ResponseBody ResultVO selectOpmsRelLogicList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.selectOpmsRelLogicList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 수정 이력조회 - 장비별 요청현황 조회
	 * 2016.12.22 좌은진
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchConfReqStateList.do")
	public @ResponseBody ResultVO searchConfReqStateList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = assetAutomationService.searchConfReqStateList(paramMap);		
			totalCount = assetAutomationService.searchConfReqStateListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자동화 컴포넌트 - 자산 메모리 증설량 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchBsInsMemoryView.do")
	public @ResponseBody ResultVO searchBsInsMemoryView(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.searchBsInsMemoryView(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자동화 컴포넌트 - 자산 CPU Core 증설량 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchBsInsCpuView.do")
	public @ResponseBody ResultVO searchBsInsCpuView(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.searchBsInsCpuView(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자동화 컴포넌트 - 자산 Storage 증설량 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchBsInsStorageView.do")
	public @ResponseBody ResultVO searchBsInsStorageView(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.searchBsInsStorageView(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자동화 컴포넌트 - 자산  NW 서비스중단일관리 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/searchBsSvcStopView.do")
	public @ResponseBody ResultVO searchBsSvcStopView(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.searchBsSvcStopView(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [컴포넌트]
	 * 디스크정보 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsRelDiskList.do")
	public @ResponseBody ResultVO selectOpmsRelDiskList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.selectOpmsRelDiskList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [컴포넌트]
	 * EMS수집 디스크정보 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectEmsDiskList.do")
	public @ResponseBody ResultVO selectEmsDiskList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.selectEmsDiskList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [컴포넌트]
	 * IP정보 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsRelIPList.do")
	public @ResponseBody ResultVO selectOpmsRelIPList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.selectOpmsRelIPList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [컴포넌트]
	 * EMS수집 IP정보 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectEmsIPList.do")
	public @ResponseBody ResultVO selectEmsIPList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.selectEmsIPList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [컴포넌트]
	 * Nic정보 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsRelNicList.do")
	public @ResponseBody ResultVO selectOpmsRelNicList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.selectOpmsRelNicList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * [컴포넌트]
	 * 이중화 정보 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsDualInfoList.do")
	public @ResponseBody ResultVO selectOpmsDualInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetAutomationService.selectOpmsDualInfoList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [컴포넌트]
	 * 가상서버 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsRelVMList.do")
	public @ResponseBody ResultVO selectOpmsRelVMList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			if(!StringUtils.isEmpty(paramMap.get("page"))) {
				//@@ 페이징 관련 내용 
				PagingUtil.getFristEndNum(paramMap);
				totalCount = assetAutomationService.selectOpmsRelVMCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}

			List resultList = assetAutomationService.selectOpmsRelVMList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 중복체크
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/amdb/checkDuplicateAsset.do")
	public @ResponseBody ResultVO checkDuplicateAsset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			/*if(assetAutomationService.isDuplicateAsset(paramMap)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"호스트명"}));
			}*/
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 중복체크(일괄수정)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/amdb/checkDuplicateBatchAsset.do")
	public @ResponseBody ResultVO checkDuplicateBatchAsset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			
			/*if(assetAutomationService.isDuplicateBatchAsset(paramMap)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"호스트명"}));
			}*/
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
