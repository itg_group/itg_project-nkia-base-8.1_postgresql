package com.nkia.itg.itam.cluster.web;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.cluster.service.ClusterService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ClusterController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "clusterService")
	private ClusterService clusterService;
	
	/**
	 * message 호출
	 */	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 클러스터관리 탭 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/cluster/clusterTab.do")
	public String clusterTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//메뉴접근검증
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/cluster/clusterTab.nvf";
		return forwarPage;
	}
	
	/**
	 * 클러스터관리 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/cluster/cluster.do")
	public String clusterIndex(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/cluster/cluster.nvf";
		return forwarPage;
	}
	
	/**
	 * 클러스터관리 상세 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value="/itg/itam/cluster/clusterDetail.do")
	public String clusterDetail(HttpServletRequest request, ModelMap paramMap) throws NkiaException, UnsupportedEncodingException {
		String forwarPage = "/itg/itam/cluster/clusterDetail.nvf";
		
		paramMap.put("CL_ID", new String(StringUtil.parseString(request.getParameter("cl_id")).getBytes("ISO-8859-1"),"UTF-8"));
		paramMap.put("CL_TYPE", StringUtil.parseString(request.getParameter("cl_type")));
		
		return forwarPage;
	}
	
	/**
	 * 클러스터 물리서버 목록 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value="/itg/itam/cluster/clusterDetailPop.do")
	public String clusterDetailPop(HttpServletRequest request, ModelMap paramMap) throws NkiaException, UnsupportedEncodingException {
		String forwarPage = "/itg/itam/cluster/clusterDetailPop.nvf";
		
		paramMap.put("CL_ID", new String(StringUtil.parseString(request.getParameter("cl_id")).getBytes("ISO-8859-1"),"UTF-8"));
		
		return forwarPage;
	}
	
	/**
	 * 클러스터 목록 조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/cluster/searchClusterList.do")
	public @ResponseBody ResultVO selectClusterList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("user_id", userVO.getUser_id());
	    	}
	    	
	    	//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = clusterService.searchClusterList(paramMap);		
			int totalCount = clusterService.searchClusterCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 클러스터 상세 조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/cluster/searchCluster.do")
	public @ResponseBody ResultVO selectCluster(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = clusterService.searchCluster(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 클러스터 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/cluster/insertCluster.do")
	public @ResponseBody ResultVO insertCluster(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("INS_USER_ID", userVO.getUser_id());
	    	}			
	    	clusterService.insertCluster(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 클러스터 - 연관 서버 조회
	 * @param paramMap
	 * @return resultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/cluster/searchClusterRelList.do")
	public @ResponseBody ResultVO searchClusterRelList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			
			if(!StringUtils.isEmpty(paramMap.get("page"))) {
				//@@ 페이징 관련 내용 
				PagingUtil.getFristEndNum(paramMap);
				int totalCount = clusterService.searchClusterRelCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			
			List resultList = clusterService.searchClusterRelList(paramMap);		
		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 클러스터 연관 서버 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/cluster/insertClusterRel.do")
	public @ResponseBody ResultVO insertClusterRel(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("INS_USER_ID", userVO.getUser_id());
	    	}			
	    	clusterService.insertClusterRel(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * VMM 목록 조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/cluster/searchVMMList.do")
	public @ResponseBody ResultVO selectVMMList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			if(!StringUtils.isEmpty(paramMap.get("page"))) {
				//@@ 페이징 관련 내용 
				PagingUtil.getFristEndNum(paramMap);
				totalCount = clusterService.searchVMMCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			
			List resultList = clusterService.searchVMMList(paramMap);		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 클러스터 연관 서버 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/cluster/deleteCluster.do")
	public @ResponseBody ResultVO deleteCluster(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	clusterService.deleteClusterRel(paramMap);
	    	clusterService.deleteCluster(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
