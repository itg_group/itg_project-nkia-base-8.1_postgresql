package com.nkia.itg.itam.cluster.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.cluster.dao.ClusterDAO;
import com.nkia.itg.itam.cluster.service.ClusterService;

@Service("clusterService")
public class ClusterServiceImpl implements ClusterService{
	private static final Logger logger = LoggerFactory.getLogger(ClusterServiceImpl.class);
	
	@Resource(name = "clusterDAO")
	public ClusterDAO clusterDAO;

	@Override
	public List searchClusterList(ModelMap paramMap) throws NkiaException {
		return clusterDAO.searchClusterList(paramMap);
	}

	@Override
	public int searchClusterCount(ModelMap paramMap) throws NkiaException {
		return clusterDAO.searchClusterCount(paramMap);
	}
	
	public HashMap searchCluster(ModelMap paramMap) throws NkiaException {
		return clusterDAO.searchCluster(paramMap);
	}
	
	public void insertCluster(ModelMap paramMap) throws NkiaException {
		if(StringUtils.isEmpty(paramMap.get("CL_ID"))) {
			String clId = clusterDAO.searchNewKey(paramMap);
			paramMap.put("CL_ID", clId);
			clusterDAO.insertCluster(paramMap);
		}
		
		clusterDAO.deleteClusterRel(paramMap);
		
		if(((List)paramMap.get("relDataList")).size() > 0) {
			clusterDAO.insertClusterRel(paramMap);
		}
	}	
	
	public void deleteCluster(ModelMap paramMap) throws NkiaException {
		clusterDAO.deleteCluster(paramMap);
	}
	
	@Override
	public List searchClusterRelList(ModelMap paramMap) throws NkiaException {
		return clusterDAO.searchClusterRelList(paramMap);
	}
	
	@Override
	public int searchClusterRelCount(ModelMap paramMap) throws NkiaException {
		return clusterDAO.searchClusterRelCount(paramMap);
	}
	
	public void insertClusterRel(ModelMap paramMap) throws NkiaException {
		clusterDAO.deleteClusterRel(paramMap);
		clusterDAO.insertClusterRel(paramMap);
	}	
	
	public void deleteClusterRel(ModelMap paramMap) throws NkiaException {
		clusterDAO.deleteClusterRel(paramMap);
	}

	@Override
	public List searchVMMList(ModelMap paramMap) throws NkiaException {
		return clusterDAO.searchVMMList(paramMap);
	}
	
	@Override
	public int searchVMMCount(ModelMap paramMap) throws NkiaException {
		return clusterDAO.searchVMMCount(paramMap);
	}
}
