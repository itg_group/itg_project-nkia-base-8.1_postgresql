package com.nkia.itg.itam.cluster.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("clusterDAO")
public class ClusterDAO  extends EgovComAbstractDAO {

	public List searchClusterList(ModelMap paramMap) throws NkiaException{
		return list("ClusterDAO.searchClusterList", paramMap);
	}

	public int searchClusterCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("ClusterDAO.searchClusterCount", paramMap);
	}
	
	public HashMap searchCluster(ModelMap paramMap) throws NkiaException{
		return (HashMap)selectByPk("ClusterDAO.searchCluster", paramMap);
	}
	
	public String searchNewKey(ModelMap paramMap) throws NkiaException{
		return (String)selectByPk("ClusterDAO.searchNewKey", paramMap);
	}
	
	public void insertCluster(ModelMap paramMap) throws NkiaException {
		update("ClusterDAO.mergeCluster", paramMap);
	}		
	
	public void deleteCluster(ModelMap paramMap) throws NkiaException {
		this.delete("ClusterDAO.deleteCluster", paramMap);
	}
	
	public List searchClusterRelList(ModelMap paramMap) throws NkiaException{
		return list("ClusterDAO.searchClusterRelList", paramMap);
	}
	
	public int searchClusterRelCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("ClusterDAO.searchClusterRelCount", paramMap);
	}
	
	public void insertClusterRel(ModelMap paramMap) throws NkiaException {
		this.insert("ClusterDAO.insertClusterRel", paramMap);
	}		
	
	public void deleteClusterRel(ModelMap paramMap) throws NkiaException {
		this.delete("ClusterDAO.deleteClusterRel", paramMap);
	}
	
	public List searchVMMList(ModelMap paramMap) throws NkiaException{
		return list("ClusterDAO.searchVMMList", paramMap);
	}
	
	public int searchVMMCount(ModelMap paramMap) throws NkiaException{
		return (Integer)selectByPk("ClusterDAO.searchVMMCount", paramMap);
	}
}
