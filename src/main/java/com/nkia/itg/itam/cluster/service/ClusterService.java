package com.nkia.itg.itam.cluster.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ClusterService {

	public List searchClusterList(ModelMap paramMap) throws NkiaException;
	
	public int searchClusterCount(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchCluster(ModelMap paramMap) throws NkiaException;
	
	public void insertCluster(ModelMap paramMap) throws NkiaException;
	
	public void deleteCluster(ModelMap paramMap) throws NkiaException;
	
	public List searchClusterRelList(ModelMap paramMap) throws NkiaException;
	
	public int searchClusterRelCount(ModelMap paramMap) throws NkiaException;
	
	public void insertClusterRel(ModelMap paramMap) throws NkiaException;
	
	public void deleteClusterRel(ModelMap paramMap) throws NkiaException;
	
	public List searchVMMList(ModelMap paramMap) throws NkiaException;
	
	public int searchVMMCount(ModelMap paramMap) throws NkiaException;
}
