/*
 * @(#)SupplyDecideDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("supplyDecideDAO")
public class SupplyDecideDAO extends EgovComAbstractDAO{
	
	public List searchSupplyDecideList(ModelMap paramMap) throws NkiaException{
		return list("SupplyDecideDAO.searchSupplyDecideList", paramMap);
	}

	public int searchSupplyDecideListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("SupplyDecideDAO.searchSupplyDecideListCount", paramMap);
	}

	public HashMap searchSupplyDecideAsset(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("SupplyDecideDAO.searchSupplyDecideAsset", paramMap);
	}
	
	/**
	 * @param entityInfo
	 * @return
	 */
	public String selectAssetId(HashMap entityInfo) throws NkiaException {
		// TODO Auto-generated method stub
		String id = (String) this.selectByPk("SupplyDecideDAO.selectAssetId", entityInfo);
		return id;
	}
	
	/**
	 * @param entityInfo
	 * @return
	 */
	public String selectConfId(HashMap entityInfo) throws NkiaException {
		// TODO Auto-generated method stub
		String id = (String) this.selectByPk("SupplyDecideDAO.selectConfId", entityInfo);
		return id;
	}

	public void updateCheckDT(HashMap entityInfo) throws NkiaException {
		this.update("SupplyDecideDAO.updateCheckDT", entityInfo);
	}	

	public void insertAssetMaster(HashMap entityInfo) throws NkiaException {
		this.update("SupplyDecideDAO.insertAssetMaster", entityInfo);
	}	
	
	public void insertAsset(HashMap entityInfo) throws NkiaException {
		this.update("SupplyDecideDAO.insertAsset", entityInfo);
	}
	
	public List searchSupplyDecideExcelDown(Map paramMap) throws NkiaException {
		List resultList = this.list("SupplyDecideDAO.searchSupplyDecideExcelDown", paramMap);
		return resultList;
	}
	
	public List searchSupplySetExcelDown(Map paramMap) throws NkiaException {
		List resultList = this.list("SupplyDecideDAO.searchSupplyRelAssetList", paramMap);
		return resultList;
	}
	
	public List searchSupplySetList(ModelMap paramMap) throws NkiaException{
		return list("SupplyDecideDAO.searchSupplySetList", paramMap);
	}
	
	public int searchSupplySetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("SupplyDecideDAO.searchSupplySetListCount", paramMap);
	}
	
	public List searchSupplyRelAssetList(ModelMap paramMap) throws NkiaException{
		return list("SupplyDecideDAO.searchSupplyRelAssetList", paramMap);
	}
	
	public void insertSupplySetCheck(ModelMap paramMap) throws NkiaException {
		this.insert("SupplyDecideDAO.insertSupplySetCheck", paramMap);
	}
	
	public void updateAssetSetCheck(ModelMap paramMap) throws NkiaException {
		this.update("SupplyDecideDAO.updateAssetSetCheck", paramMap);
	}
	
	public void updateSupplyState(ModelMap paramMap) throws NkiaException {
		this.update("SupplyDecideDAO.updateSupplyState", paramMap);
	}
}
