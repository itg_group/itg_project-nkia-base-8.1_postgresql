/*
 * @(#)SupplyDecideController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.supply.service.SupplyDecideService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class SupplyDecideController {
	
	@Resource(name = "supplyDecideService")
	private SupplyDecideService supplyDecideService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 공급계약확정 - 공급계약확정 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/supply/supplyDecideMng.do")
	public String supplyDecideMng(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/supply/supplyDecideMng.nvf";
		return forwarPage;
	}
	
	/**
	 * New 공급계약검수 - 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/supply/supplySetMng.do")
	public String supplySetMng(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/supply/supplySetMng.nvf";
		return forwarPage;
	}
	
	/**
	 * 공급계약확정 - 공급계약목록 리스트
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/supply/searchSupplyDecideList.do")
	public @ResponseBody ResultVO searchSupplyDecideList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = supplyDecideService.searchSupplyDecideList(paramMap);		
			totalCount = supplyDecideService.searchSupplyDecideListCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 공급계약확정 - 공급계약목록 리스트
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/supply/searchSupplyDecideAsset.do")
	public @ResponseBody ResultVO searchSupplyDecideAsset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = supplyDecideService.searchSupplyDecideAsset(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 공급계약확정 - 자산확정시 AM_ASSET, AM_INFRA/AM_SW 데이터 삽입
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/supply/insertAssetDecide.do")
	public @ResponseBody ResultVO insertAssetDecide(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			supplyDecideService.insertAssetDecide(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공급계약확정 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/supply/searchSupplyDecideExcelDown.do")
    public @ResponseBody ResultVO searchSupplyDecideExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = supplyDecideService.searchSupplyDecideExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공급계약확정 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/supply/searchSupplySetExcelDown.do")
	public @ResponseBody ResultVO searchSupplySetExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");  
			Map param = (Map) paramMap.get("param");
			
			List resultList = supplyDecideService.searchSupplySetExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * New공급계약확정 - 계약목록
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/supply/searchSupplySetList.do")
	public @ResponseBody ResultVO searchSupplySetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = supplyDecideService.searchSupplySetList(paramMap);		
			totalCount = supplyDecideService.searchSupplySetListCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * New공급계약확정 - 자산목록
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/supply/searchSupplyRelAssetList.do")
	public @ResponseBody ResultVO searchSupplyRelAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			
			List resultList = supplyDecideService.searchSupplyRelAssetList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	


	/**
	 * 공급계약확정 - 검수일 / 무상유지보수 기간 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/supply/updateAssetSetCheck.do")
	public @ResponseBody ResultVO updateAssetSetCheck(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			supplyDecideService.updateAssetSetCheck(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
}
