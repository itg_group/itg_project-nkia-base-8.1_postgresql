/*
 * @(#)supplyDecideService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SupplyDecideService {

	public List searchSupplyDecideList(ModelMap paramMap) throws NkiaException;

	public int searchSupplyDecideListCount(ModelMap paramMap) throws NkiaException;	
	
	public HashMap searchSupplyDecideAsset(ModelMap paramMap) throws NkiaException;
	
	public void insertAssetDecide(ModelMap paramMap) throws NkiaException;
	
	public List searchSupplyDecideExcelDown(Map paramMap) throws NkiaException;
	
	public List searchSupplySetExcelDown(Map paramMap) throws NkiaException;
	
	public List searchSupplySetList(ModelMap paramMap) throws NkiaException;
	
	public int searchSupplySetListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchSupplyRelAssetList(ModelMap paramMap) throws NkiaException;
	
	public void updateAssetSetCheck(ModelMap paramMap) throws NkiaException;
}
