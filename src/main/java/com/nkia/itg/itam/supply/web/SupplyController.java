/*
 * @(#)SupplyController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.supply.service.SupplyService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class SupplyController {
	
	@Resource(name = "supplyService")
	private SupplyService supplyService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	
	/**
	 * 공급계약관리 - 공급계약관리 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/supply/supplyManager.do")
	public String supplyManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/supply/supplyManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 공급계약관리 - 공급계약관리 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/supply/searchSupply.do")
	@ResponseBody 
	public ResultVO searchSupply(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = supplyService.searchSupply(paramMap);		
			totalCount = supplyService.searchSupplyCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공급계약관리 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/supply/searchSupplyExcelDown.do")
	@ResponseBody
    public ResultVO searchSupplyExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = supplyService.searchSupplyExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공급계약관리 - 공급계약내역 신규등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/supply/insertSupply.do")
	@ResponseBody
	public ResultVO insertSupply(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
			supplyService.insertSupply(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공급계약관리 - 공급계약내역 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/supply/updateSupply.do")
	@ResponseBody
	public ResultVO updateSupply(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			supplyService.updateSupply(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 공급계약관리 - 공급계약내역 제거
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/supply/deleteSupply.do")
	public @ResponseBody ResultVO deleteSupply(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			supplyService.deleteSupply(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공급계약관리 - 등록된 공급계약자산 INFRA 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/supply/searchSupplyAssetInfra.do")
	@ResponseBody
	public ResultVO searchSupplyAssetInfra(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = supplyService.searchSupplyAssetInfra(paramMap);		
			totalCount = supplyService.searchSupplyAssetInfraCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공급계약관리 - 등록된 공급계약자산 SW 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/supply/searchSupplyAssetSW.do")
	@ResponseBody
	public ResultVO searchSupplyAssetSW(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = supplyService.searchSupplyAssetSW(paramMap);		
			totalCount = supplyService.searchSupplyAssetSWCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
