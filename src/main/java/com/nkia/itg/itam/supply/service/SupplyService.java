/*
 * @(#)supplyService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SupplyService {

	public List searchSupply(ModelMap paramMap) throws NkiaException;

	public int searchSupplyCount(ModelMap paramMap) throws NkiaException;

	public List searchSupplyAssetInfra(ModelMap paramMap) throws NkiaException;
	
	public int searchSupplyAssetInfraCount(ModelMap paramMap) throws NkiaException;
		
	public List searchSupplyAssetSW(ModelMap paramMap) throws NkiaException;
	
	public int searchSupplyAssetSWCount(ModelMap paramMap) throws NkiaException;
	
	public void insertSupply(ModelMap paramMap) throws NkiaException;
	
	public void updateSupply(ModelMap paramMap) throws NkiaException;
	
	public void deleteSupply(ModelMap paramMap) throws NkiaException, IOException;
		
	public List searchSupplyExcelDown(Map paramMap) throws NkiaException;
}
