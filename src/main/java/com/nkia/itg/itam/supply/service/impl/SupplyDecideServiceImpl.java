/*
 * @(#)supplyServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.service.impl;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.supply.dao.SupplyDecideDAO;
import com.nkia.itg.itam.supply.service.SupplyDecideService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("supplyDecideService")
public class SupplyDecideServiceImpl implements SupplyDecideService{
	
	private static final Logger logger = LoggerFactory.getLogger(SupplyDecideServiceImpl.class);
	
	@Resource(name = "supplyDecideDAO")
	public SupplyDecideDAO supplyDecideDAO;
	
	
	public List searchSupplyDecideList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub		
		return supplyDecideDAO.searchSupplyDecideList(paramMap);
	}

	public int searchSupplyDecideListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return supplyDecideDAO.searchSupplyDecideListCount(paramMap);
	}	
	
	public HashMap searchSupplyDecideAsset(ModelMap paramMap) throws NkiaException {
		return supplyDecideDAO.searchSupplyDecideAsset(paramMap);
	}
	
	/**
	 * 공급계약확정 - 자산확정시 AM_ASSET, AM_INFRA/AM_SW 데이터 삽입
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	public void insertAssetDecide(ModelMap paramMap) throws NkiaException {		
				
		HashMap entityInfo = new HashMap();
		entityInfo = paramMap;
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		String login_user_id = userVO.getUser_id();
    		entityInfo.put("login_user_id", userVO.getUser_id());
    	}
    	
    	int amount = Integer.parseInt((String) entityInfo.get("amount"));
    	
    	if("SW".equals(entityInfo.get("asset_type"))){
    		amount = 1;
    	}
    	
    	GregorianCalendar calendar = new GregorianCalendar();
    	int year = calendar.get(Calendar.YEAR)-2000;
    	int month = calendar.get(Calendar.MONTH)+1;
    	entityInfo.put("year", year);
    	if(month < 10){
    		String mon = "0"+month;
        	entityInfo.put("month", mon);
    	}else{
        	entityInfo.put("month", month);
    	}
    	for(int i=0; i<amount; i++){
    		
    		String asset_id = supplyDecideDAO.selectAssetId(entityInfo); // ASSET_ID 생성
    		String conf_id  = supplyDecideDAO.selectConfId(entityInfo);  // CONF_ID 생성    	
    		
    		if(asset_id == null || asset_id == ""){
    			asset_id = "AT"+(String)entityInfo.get("class_type")+year+month+"0001";
    		}
    		
    		if(conf_id == null || conf_id == ""){
    			conf_id = "CI"+(String)entityInfo.get("class_type")+year+month+"0001";    				
    		}
    		
    		entityInfo.put("asset_id", asset_id);
    		entityInfo.put("conf_id", conf_id);
    		
    		supplyDecideDAO.insertAssetMaster(entityInfo);
    		supplyDecideDAO.insertAsset(entityInfo);
    		
    	}
		supplyDecideDAO.updateCheckDT(entityInfo);
		
	}
	
	public List searchSupplyDecideExcelDown(Map paramMap) throws NkiaException {
		List resultList = supplyDecideDAO.searchSupplyDecideExcelDown(paramMap); 
		return resultList;
	}
	
	public List searchSupplySetExcelDown(Map paramMap) throws NkiaException {
		List resultList = supplyDecideDAO.searchSupplySetExcelDown(paramMap); 
		return resultList;
	}
	
	public List searchSupplySetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub		
		return supplyDecideDAO.searchSupplySetList(paramMap);
	}
	
	public int searchSupplySetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return supplyDecideDAO.searchSupplySetListCount(paramMap);
	}	
	
	public List searchSupplyRelAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub		
		return supplyDecideDAO.searchSupplyRelAssetList(paramMap);
	}
	
	public void updateAssetSetCheck(ModelMap paramMap) throws NkiaException {
		//am_supplyt_check table에 검수일 및 무상유지보수기간 insert
		supplyDecideDAO.insertSupplySetCheck(paramMap);
		//am_asset table에 검수일 및 무상유지보수기간 
		supplyDecideDAO.updateAssetSetCheck(paramMap);
		//am_supply table에 supply_state(공급계약상태) 변경
		supplyDecideDAO.updateSupplyState(paramMap);
	}		
}
