/*
 * @(#)supplyServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.itam.supply.dao.SupplyDAO;
import com.nkia.itg.itam.supply.service.SupplyService;

@Service("supplyService")
public class SupplyServiceImpl implements SupplyService{
	
	private static final Logger logger = LoggerFactory.getLogger(SupplyServiceImpl.class);
	
	@Resource(name = "supplyDAO")
	public SupplyDAO supplyDAO;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "jsonUtil")
	public JsonUtil jsonUtil;
	
	public List searchSupply(ModelMap paramMap) throws NkiaException {
		return supplyDAO.searchSupply(paramMap);
	}

	public int searchSupplyCount(ModelMap paramMap) throws NkiaException {
		return supplyDAO.searchSupplyCount(paramMap);
	}
	
	/**
	 * 공급계약등록
	 */
	public void insertSupply(ModelMap paramMap) throws NkiaException {
		paramMap.put("supply_id", supplyDAO.getNextSuppylyId());
		supplyDAO.insertSupply(paramMap);
	}

	/**
	 * 공급계약수정
	 */
	public void updateSupply(ModelMap paramMap) throws NkiaException {
		supplyDAO.updateSupply(paramMap);
	}
	
	public void deleteSupply(ModelMap paramMap) throws NkiaException, IOException {
//		// 공급자산 인프라 삭제
//		supplyDAO.deleteSupplyAssetInfra(paramMap);
//		// 공급자산 S/W 삭제
//		supplyDAO.deleteSupplyAssetSw(paramMap);
		// 공급계약 삭제
		supplyDAO.deleteSupply(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	public List searchSupplyAssetInfra(ModelMap paramMap) throws NkiaException {
		return supplyDAO.searchSupplyAssetInfra(paramMap);
	}

	public int searchSupplyAssetInfraCount(ModelMap paramMap) throws NkiaException {
		return supplyDAO.searchSupplyAssetInfraCount(paramMap);
	}
	
	public List searchSupplyAssetSW(ModelMap paramMap) throws NkiaException {
		return supplyDAO.searchSupplyAssetSW(paramMap);
	}
	
	public int searchSupplyAssetSWCount(ModelMap paramMap) throws NkiaException {
		return supplyDAO.searchSupplyAssetSWCount(paramMap);
	}
	
	public List searchSupplyExcelDown(Map paramMap) throws NkiaException {
		return supplyDAO.searchSupplyExcelDown(paramMap);
	}
}
