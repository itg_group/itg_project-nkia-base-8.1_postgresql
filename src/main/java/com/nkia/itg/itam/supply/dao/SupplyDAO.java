/*
 * @(#)SupplyDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.supply.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("supplyDAO")
public class SupplyDAO extends EgovComAbstractDAO{
	
	public List searchSupply(ModelMap paramMap) throws NkiaException{
		return list("SupplyDAO.searchSupply", paramMap);
	}

	public int searchSupplyCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("SupplyDAO.searchSupplyCount", paramMap);
	}
	
	public void insertSupply(ModelMap paramMap) throws NkiaException {
		insert("SupplyDAO.insertSupply", paramMap);
	}
	
	public void updateSupply(ModelMap paramMap) throws NkiaException {
		update("SupplyDAO.updateSupply", paramMap);
	}	

	public List searchSupplyAssetInfra(ModelMap paramMap) throws NkiaException{
		return list("SupplyDAO.searchSupplyAssetInfra", paramMap);
	}

	public int searchSupplyAssetInfraCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("SupplyDAO.searchSupplyAssetInfraCount", paramMap);
	}
	
	public List searchSupplyAssetSW(ModelMap paramMap) throws NkiaException{
		return list("SupplyDAO.searchSupplyAssetSW", paramMap);
	}
	
	public int searchSupplyAssetSWCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("SupplyDAO.searchSupplyAssetSWCount", paramMap);
	}
	
	public void insertSupplyAssetSw(HashMap dataMap) throws NkiaException {
		insert("SupplyDAO.insertSupplyAssetSw", dataMap);
	}	
	
	public void deleteSupplyAssetSw(ModelMap paramMap) throws NkiaException {
		delete("SupplyDAO.deleteSupplyAssetSw", paramMap);
	}	
	
	public void deleteSupply(ModelMap paramMap) throws NkiaException {
		delete("SupplyDAO.deleteSupply", paramMap);
	}
	
	public void insertSupplyAssetInfra(HashMap dataMap) throws NkiaException {
		insert("SupplyDAO.insertSupplyAssetInfra", dataMap);
	}	
	
	public void deleteSupplyAssetInfra(ModelMap paramMap) throws NkiaException {
		insert("SupplyDAO.deleteSupplyAssetInfra", paramMap);
	}
	
	public List searchSupplyExcelDown(Map paramMap) throws NkiaException {
		return list("SupplyDAO.searchSupplyExcelDown", paramMap);
	}

	public String getNextSuppylyId() throws NkiaException {
		return (String)selectByPk("SupplyDAO.getNextSuppylyId", null);
	}	
}
