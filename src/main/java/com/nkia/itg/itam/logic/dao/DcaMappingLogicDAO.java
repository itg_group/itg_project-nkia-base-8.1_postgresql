package com.nkia.itg.itam.logic.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("dcaMappingLogicDAO")
public class DcaMappingLogicDAO extends EgovComAbstractDAO {
	
	/**
	 * 검색용 분류체계 콤보박스 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectSearchClassList() throws NkiaException {
		return list("DcaMappingLogicDAO.selectSearchClassList", null);
	}
	
	/**
	 * 물리서버 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectPhysiList(ModelMap paramMap) throws NkiaException {
		return list("DcaMappingLogicDAO.selectPhysiList", paramMap);
	}
	
	/**
	 * 물리서버 목록 Count
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public int selectPhysiListCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer)selectByPk("DcaMappingLogicDAO.selectPhysiListCount", paramMap);
		return count;
	}
	
	/**
	 * DCA수집 논리서버 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectDcaLogicList(ModelMap paramMap) throws NkiaException {
		return list("DcaMappingLogicDAO.selectDcaLogicList", paramMap);
	}
	
	/**
	 * DCA수집 논리서버 목록 Count
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public int selectDcaLogicListCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer)selectByPk("DcaMappingLogicDAO.selectDcaLogicListCount", paramMap);
		return count;
	}
	
	/**
	 * 논리서버 전용 구성ID 생성 
	 * (논리서버 CLASS_TYPE을 강제로 Put하여 opmsDetailDAO의 CONF_ID 생성 쿼리를 참조)
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createConfIdByDcaLogic()throws NkiaException{
		HashMap paramMap = new HashMap();
		paramMap.put("class_type", "SL");
		return (String)selectByPk("opmsDetailDAO.createConfId", paramMap);
	}
	
	/**
	 * DCA수집 논리서버를 자산테이블에 등록 - AM_INFRA
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAmInfra(HashMap paramMap) throws NkiaException {
		insert("DcaMappingLogicDAO.insertDcaLogicToAmInfra", paramMap);
	}
	
	/**
	 * DCA수집 논리서버를 자산테이블에 등록 - AM_SV_LOGIC
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAmSvLogic(HashMap paramMap) throws NkiaException {
		insert("DcaMappingLogicDAO.insertDcaLogicToAmSvLogic", paramMap);
	}
	
	/**
	 * 해당 시리얼번호를 가지고 있는 물리서버 리턴
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectPhysiServerBySerialNo(HashMap paramMap) throws NkiaException {
		return list("DcaMappingLogicDAO.selectPhysiServerBySerialNo", paramMap);
	}
	
}
