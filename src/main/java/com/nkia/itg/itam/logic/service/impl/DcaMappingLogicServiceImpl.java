package com.nkia.itg.itam.logic.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.intergration.polestar.job.service.ItgJobService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.logic.dao.DcaMappingLogicDAO;
import com.nkia.itg.itam.logic.service.DcaMappingLogicService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("dcaMappingLogicService")
public class DcaMappingLogicServiceImpl implements DcaMappingLogicService {
	
	private static final Logger logger = LoggerFactory.getLogger(LogicManagerServiceImpl.class);
	
	@Resource(name = "dcaMappingLogicDAO")
	public DcaMappingLogicDAO dcaMappingLogicDAO;
	
	@Resource(name = "itgJobService")
	public ItgJobService itgJobService;
	
	/**
	 * 검색용 분류체계 콤보박스 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectSearchClassList() throws NkiaException {
		return dcaMappingLogicDAO.selectSearchClassList();
	}
	
	/**
	 * 물리서버 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectPhysiList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return dcaMappingLogicDAO.selectPhysiList(paramMap);
	}
	
	/**
	 * 물리서버 목록 Count
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public int selectPhysiListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return dcaMappingLogicDAO.selectPhysiListCount(paramMap);
	}
	
	/**
	 * DCA수집 논리서버 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectDcaLogicList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return dcaMappingLogicDAO.selectDcaLogicList(paramMap);
	}
	
	/**
	 * DCA수집 논리서버 목록 Count
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public int selectDcaLogicListCount(ModelMap paramMap) throws NkiaException {
		return dcaMappingLogicDAO.selectDcaLogicListCount(paramMap);
	}
	
	/**
	 * DCA수집 논리서버를 자산테이블에 등록 - AM_INFRA와 AM_SV_LOGIC
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAm(HashMap paramMap) throws NkiaException {
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = userVO.getUser_id();
		
		List<HashMap> logicList = (List)paramMap.get("logicArray");
		
		if( logicList != null && logicList.size() > 0 ){
			
			for(int i=0; i<logicList.size(); i++) {
				
				HashMap<String, String> dataMap = new HashMap();
				
				dataMap = logicList.get(i);
				dataMap.put("UPD_USER_ID", userId);
				dataMap.put("CONF_ID", createConfIdByDcaLogic());
				
				// DCA 수집 논리서버 정보를 AM_INFRA에 등록한다.
				insertDcaLogicToAmInfra(dataMap);
				// DCA 수집 논리서버 정보를 AM_SV_LOGIC에 등록한다.
				insertDcaLogicToAmSvLogic(dataMap);
			}
			
			// 논리서버 정보 Insert 완료후, 논리서버 정보 동기화
			itgJobService.updateSyncDcaDataToAmServer("ONCE");
			
		}
	}
	
	/**
	 * DCA 수집 논리서버 정보를 AM_INFRA에 등록한다.
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAmInfra(HashMap paramMap) throws NkiaException {
		dcaMappingLogicDAO.insertDcaLogicToAmInfra(paramMap);
	}
	
	/**
	 * DCA 수집 논리서버 정보를 AM_SV_LOGIC에 등록한다.
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAmSvLogic(HashMap paramMap) throws NkiaException {
		dcaMappingLogicDAO.insertDcaLogicToAmSvLogic(paramMap);
	}
	
	/**
	 * 논리서버용 CONF_ID 생성
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public String createConfIdByDcaLogic() throws NkiaException {
		return dcaMappingLogicDAO.createConfIdByDcaLogic();
	}
	
	/**
	 * 해당 시리얼번호를 가지고 있는 물리서버 리턴
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectPhysiServerBySerialNo(HashMap paramMap) throws NkiaException {
		return dcaMappingLogicDAO.selectPhysiServerBySerialNo(paramMap);
	}

}
