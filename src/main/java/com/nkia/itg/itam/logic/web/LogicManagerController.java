package com.nkia.itg.itam.logic.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.logic.service.LogicManagerService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
/**
 * @version 1.0 2013. 4. 8.
 * @author <a href="mailto:minsoo@nkia.co.kr"> 
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class LogicManagerController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "logicManagerService")
	private LogicManagerService logicManagerService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 논리자산등록 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/logic/operLogicManager.do")
	public String operManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/logic/operLogicManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 물리자산선택 팝업 그리드 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/logic/physiGridListPop.do")
	public @ResponseBody ResultVO physiGridListPop(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			

			if(!initFlag) {
				totalCount = logicManagerService.physiGridListPopCount(paramMap);
				resultList = logicManagerService.physiGridListPop(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 공급계약업체 정보 출력
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/logic/searchPhysiServerInfoPop.do")
	public @ResponseBody ResultVO searchPhysiServerInfoPop(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = logicManagerService.searchPhysiServerInfoPop(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 논리자산등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/logic/insertServerLogicInfo.do")
	public @ResponseBody ResultVO insertServerLogicInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	logicManagerService.insertServerLogicInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
