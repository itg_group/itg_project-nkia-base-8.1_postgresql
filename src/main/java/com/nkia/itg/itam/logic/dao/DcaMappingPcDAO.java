package com.nkia.itg.itam.logic.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("dcaMappingPcDAO")
public class DcaMappingPcDAO extends EgovComAbstractDAO {

	/**
	 * PC 자산 목록 개수
	 * @param paramMap
	 * @return
	 */
	public int selectPcAssetListCount(ModelMap paramMap) {
		return (Integer) selectByPk("DcaMappingPcDAO.selectPcAssetListCount", paramMap);
	}

	/**
	 * Pc 자산 목록
	 * @param paramMap
	 * @return
	 */
	public List selectPcAssetList(ModelMap paramMap) {
		return list("DcaMappingPcDAO.selectPcAssetList", paramMap);
	}

	/**
	 * PC자산과  DCA수집 서버 매핑
	 * @param paramMap
	 */
	public void updatePcDcaId(HashMap paramMap) {
		update("DcaMappingPcDAO.updatePcDcaId", paramMap);
	}

	/**
	 * DCA수집 PC목록
	 * @param paramMap
	 */
	public List selectDcaPcList(ModelMap paramMap) {
		return list("DcaMappingPcDAO.selectDcaPcList", paramMap);
	}

	public List selectPcBySerialNo(HashMap dataMap) {
		return list("DcaMappingPcDAO.selectPcBySerialNo", dataMap);
	}
	
}
