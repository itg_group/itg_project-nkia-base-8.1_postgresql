package com.nkia.itg.itam.logic.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.itam.logic.dao.DcaMappingPcDAO;
import com.nkia.itg.itam.logic.service.DcaMappingPcService;

@Service("dcaMappingPcService")
public class DcaMappingPcServiceImpl implements DcaMappingPcService{

	@Resource(name="dcaMappingPcDAO")
	private DcaMappingPcDAO dcaMappingPcDAO;

	@Override
	public int selectPcAssetListCount(ModelMap paramMap) {
		return dcaMappingPcDAO.selectPcAssetListCount(paramMap);
	}

	@Override
	public List selectPcAssetList(ModelMap paramMap) {
		return dcaMappingPcDAO.selectPcAssetList(paramMap);
	}

	@Override
	public void updatePcDcaId(HashMap paramMap) {
		dcaMappingPcDAO.updatePcDcaId(paramMap);
	}

	@Override
	public List selectDcaPcList(ModelMap paramMap) {
		return dcaMappingPcDAO.selectDcaPcList(paramMap);
	}

	@Override
	public List selectPcBySerialNo(HashMap dataMap) {
		return dcaMappingPcDAO.selectPcBySerialNo(dataMap);
	}
	
	
}
