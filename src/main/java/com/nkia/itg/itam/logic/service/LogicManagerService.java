/**
 * 
 */
package com.nkia.itg.itam.logic.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface LogicManagerService {
	
	public int physiGridListPopCount(ModelMap paramMap) throws NkiaException;

	public List physiGridListPop(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchPhysiServerInfoPop(ModelMap paramMap) throws NkiaException;
	
	public void insertServerLogicInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateServerLogicInfo(ModelMap amSvLogicDataMap) throws NkiaException;

	public void updateServerLogicVmStatus(ModelMap amSvLogicDataMap) throws NkiaException;

	public void updateServerLogicVmStatusReturn(HashMap amServerLogicVmData) throws NkiaException;

	public void updateServerLogicInfoByVm(ModelMap amSvLogicDataMap) throws NkiaException;

	public void deleteAmSvLogic(String partitionConfId)throws NkiaException;

	public void insertAmSvLogicHis(HashMap amServerLogicVmData)throws NkiaException;
}
