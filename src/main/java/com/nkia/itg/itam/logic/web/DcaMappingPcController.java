package com.nkia.itg.itam.logic.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.logic.service.DcaMappingPcService;

@Controller
public class DcaMappingPcController {

	@Resource(name = "dcaMappingPcService")
	private DcaMappingPcService dcaMappingPcService;
	
	
	/**
	 * DCA - PC서버 매핑 화면 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/logic/goDcaMappingPcManager.do")
	public String goDcaMappingPcManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/logic/dcaMappingPcManager.nvf";
		return forwarPage;
	}
	

	/**
	 * PC 자산 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/logic/selectPcList.do")
	public @ResponseBody ResultVO selectPcList(@RequestBody ModelMap paramMap) throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			totalCount = dcaMappingPcService.selectPcAssetListCount(paramMap);
			resultList = dcaMappingPcService.selectPcAssetList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * PC자산과 dca수집서버 매핑
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/logic/updatePcDcaId.do")
	public @ResponseBody ResultVO updatePcDcaId(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap dataMap = new HashMap();
			dataMap = paramMap;
			dcaMappingPcService.updatePcDcaId(dataMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * DCa 수집 PC 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/logic/selectDcaPcList.do")
	public @ResponseBody ResultVO selectDcaPcList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = dcaMappingPcService.selectDcaPcList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
