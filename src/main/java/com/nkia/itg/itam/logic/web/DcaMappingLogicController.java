package com.nkia.itg.itam.logic.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.logic.service.DcaMappingLogicService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class DcaMappingLogicController {
	
	@Resource(name = "dcaMappingLogicService")
	private DcaMappingLogicService dcaMappingLogicService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * DCA - 논리서버 매핑 화면 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/logic/goDcaMappingLogicManager.do")
	public String goDcaMappingLogicManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/logic/dcaMappingLogicManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 검색용 분류체계 콤보박스 목록 Select
	 * 논리서버를 가지는 분류체계 목록 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/logic/selectSearchClassList.do")
	public @ResponseBody ResultVO selectSearchClassList(@RequestBody ModelMap paramMap) throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(dcaMappingLogicService.selectSearchClassList());
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 물리서버 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/logic/selectPhysiList.do")
	public @ResponseBody ResultVO selectPhysiList(@RequestBody ModelMap paramMap) throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			totalCount = dcaMappingLogicService.selectPhysiListCount(paramMap);
			resultList = dcaMappingLogicService.selectPhysiList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * DCA수집 논리서버 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/logic/selectDcaLogicList.do")
	public @ResponseBody ResultVO selectDcaLogicList(@RequestBody ModelMap paramMap) throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			PagingUtil.setSortColumn(paramMap);
			resultList = dcaMappingLogicService.selectDcaLogicList(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * DCA수집 논리서버를 자산테이블에 등록 - AM_INFRA와 AM_SV_LOGIC
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/logic/insertDcaLogicToAm.do")
	public @ResponseBody ResultVO insertDcaLogicToAm(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			dcaMappingLogicService.insertDcaLogicToAm(paramMap);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

}
