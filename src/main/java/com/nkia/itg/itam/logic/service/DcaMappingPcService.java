package com.nkia.itg.itam.logic.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

public interface DcaMappingPcService {

	int selectPcAssetListCount(ModelMap paramMap);
	
	List selectPcAssetList(ModelMap paramMap);

	void updatePcDcaId(HashMap paramMap);

	List selectDcaPcList(ModelMap paramMap);

	List selectPcBySerialNo(HashMap dataMap);

}
