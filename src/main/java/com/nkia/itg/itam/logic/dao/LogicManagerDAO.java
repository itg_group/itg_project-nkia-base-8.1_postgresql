/**
 * 
 */
package com.nkia.itg.itam.logic.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("logicManagerDAO")
public class LogicManagerDAO extends EgovComAbstractDAO {
	
	public List physiGridListPop(ModelMap paramMap) throws NkiaException {
		return list("logicDAO.physiGridListPop", paramMap);
	}
	
	public int physiGridListPopCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer)selectByPk("logicDAO.physiGridListPopCount", paramMap);
		return count;
	}
	
	public HashMap searchPhysiServerInfoPop(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("logicDAO.searchPhysiServerInfoPop", paramMap);
	}
	
	public void insertLogicBasicInfo(Map paramMap) throws NkiaException {
		this.insert("logicDAO.insertLogicBasicInfo", paramMap);
	}
	
	public void insertLogicSpecInfo(Map paramMap) throws NkiaException {
		this.insert("logicDAO.insertLogicSpecInfo", paramMap);
	}
	
	public String createConfId(ModelMap paramMap) throws NkiaException {
		return (String)selectByPk("logicDAO.createConfId", paramMap);
	}

	public void updateLogicOperState(Map basicInfo)throws NkiaException {
		update("logicDAO.updateLogicOperState", basicInfo);
	}

	public void updateLogicSpecInfo(Map specInfo)throws NkiaException {
		update("logicDAO.updateLogicSpecInfo", specInfo);
	}

	public void updateLogicSpecPhysiConfId(Map specInfo)throws NkiaException {
		update("logicDAO.updateLogicSpecPhysiConfId", specInfo);
	}

	public void updateInfraLogicByVm(Map basicInfo) throws NkiaException {
		update("logicDAO.updateInfraLogicByVm", basicInfo);
	}

	public void deleteAmSvLogic(String partitionConfId) throws NkiaException {
		delete("logicDAO.deleteAmSvLogic", partitionConfId);
	}

	public void deleteAmInfra(String partitionConfId) throws NkiaException {
		delete("logicDAO.deleteAmInfra", partitionConfId);
	}

	public void insertAmSvLogicHis(HashMap amServerLogicVmData)  throws NkiaException {
		delete("logicDAO.insertAmSvLogicHis", amServerLogicVmData);
	}
	
}
