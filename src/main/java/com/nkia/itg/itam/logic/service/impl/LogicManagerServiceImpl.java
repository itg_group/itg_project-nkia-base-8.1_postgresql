/**
 * 
 */
package com.nkia.itg.itam.logic.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.logic.dao.LogicManagerDAO;
import com.nkia.itg.itam.logic.service.LogicManagerService;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("logicManagerService")
public class LogicManagerServiceImpl implements LogicManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(LogicManagerServiceImpl.class);
	
	@Resource(name = "logicManagerDAO")
	public LogicManagerDAO logicManagerDAO;
	
	public List physiGridListPop(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return logicManagerDAO.physiGridListPop(paramMap);
	}
	
	public int physiGridListPopCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return logicManagerDAO.physiGridListPopCount(paramMap);
	}
	
	public HashMap searchPhysiServerInfoPop(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return logicManagerDAO.searchPhysiServerInfoPop(paramMap);
	}
	
	public void insertServerLogicInfo(ModelMap paramMap) throws NkiaException {		
		String confId = logicManagerDAO.createConfId(paramMap);
		String userId = (String)paramMap.get("upd_user_id");
		
		Map basicInfo = (Map)paramMap.get("basicInfo");
		Map specInfo = (Map)paramMap.get("specInfo");
		
		basicInfo.put("conf_id", confId);
		basicInfo.put("upd_user_id", userId);
		specInfo.put("conf_id", confId);		
		specInfo.put("upd_user_id", userId);		
		
		logicManagerDAO.insertLogicBasicInfo(basicInfo);
		logicManagerDAO.insertLogicSpecInfo(specInfo);
	}
	
	@Override
	public void updateServerLogicInfo(ModelMap amSvLogicDataMap)throws NkiaException {
		Map basicInfo = (Map)amSvLogicDataMap.get("basicInfo");
		Map specInfo = (Map)amSvLogicDataMap.get("specInfo");
		
		logicManagerDAO.updateLogicOperState(basicInfo);
		logicManagerDAO.updateLogicSpecInfo(specInfo);
	}

	@Override
	public void updateServerLogicVmStatus(ModelMap amSvLogicDataMap)throws NkiaException {
		Map basicInfo = (Map)amSvLogicDataMap.get("basicInfo");
		Map specInfo = (Map)amSvLogicDataMap.get("specInfo");
		
		logicManagerDAO.updateInfraLogicByVm(basicInfo);
		logicManagerDAO.updateLogicSpecPhysiConfId(specInfo);
	}

	@Override
	public void updateServerLogicVmStatusReturn(HashMap amServerLogicVmData)throws NkiaException {
		logicManagerDAO.updateLogicOperState(amServerLogicVmData);
	}

	@Override
	public void updateServerLogicInfoByVm(ModelMap amSvLogicDataMap)throws NkiaException {
		Map basicInfo = (Map)amSvLogicDataMap.get("basicInfo");
		Map specInfo = (Map)amSvLogicDataMap.get("specInfo");
		
		logicManagerDAO.updateInfraLogicByVm(basicInfo);
		logicManagerDAO.updateLogicSpecInfo(specInfo);
	}

	@Override
	public void deleteAmSvLogic(String partitionConfId) throws NkiaException {
		logicManagerDAO.deleteAmSvLogic(partitionConfId);
		logicManagerDAO.deleteAmInfra(partitionConfId);
	}

	@Override
	public void insertAmSvLogicHis(HashMap amServerLogicVmData)throws NkiaException {
		logicManagerDAO.insertAmSvLogicHis(amServerLogicVmData);
		
	}
}
