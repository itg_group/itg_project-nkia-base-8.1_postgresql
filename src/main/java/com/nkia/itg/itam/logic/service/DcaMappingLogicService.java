package com.nkia.itg.itam.logic.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DcaMappingLogicService {
	
	/**
	 * 검색용 분류체계 콤보박스 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectSearchClassList() throws NkiaException;
	
	/**
	 * 물리서버 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectPhysiList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 물리서버 목록 Count
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public int selectPhysiListCount(ModelMap paramMap) throws NkiaException;
	
	/**
	 * DCA수집 논리서버 목록 Select
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectDcaLogicList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * DCA수집 논리서버 목록 Count
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public int selectDcaLogicListCount(ModelMap paramMap) throws NkiaException;
	
	/**
	 * DCA수집 논리서버를 자산테이블에 등록 - AM_INFRA와 AM_SV_LOGIC
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAm(HashMap paramMap) throws NkiaException;
	
	/**
	 * DCA 수집 논리서버 정보를 AM_INFRA에 등록한다.
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAmInfra(HashMap paramMap) throws NkiaException;
	
	/**
	 * DCA 수집 논리서버 정보를 AM_SV_LOGIC에 등록한다.
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public void insertDcaLogicToAmSvLogic(HashMap paramMap) throws NkiaException;
	
	/**
	 * 논리서버용 CONF_ID 생성
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public String createConfIdByDcaLogic() throws NkiaException;
	
	/**
	 * 해당 시리얼번호를 가지고 있는 물리서버 리턴
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List selectPhysiServerBySerialNo(HashMap paramMap) throws NkiaException;

}
