/*
 * @(#)inventoryService.java              2013. 11. 13.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.inventory.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.AtchFileVO;

public interface InventoryService {
	
	public List searchAssetDataDown(Map paramMap) throws NkiaException;

	public List searchBarcodeInfoList(ModelMap paramMap) throws NkiaException;

	public int searchBarcodeInfoCount(ModelMap paramMap) throws NkiaException;
	
	public List searchPublicationAssetList(ModelMap paramMap) throws NkiaException;
	
	public int searchPublicationAssetCount(ModelMap paramMap) throws NkiaException;
	
	public List searchPopAssetList(ModelMap paramMap) throws NkiaException;
	
	public int searchPopAssetCount(ModelMap paramMap) throws NkiaException;
	
	public void insertBarcodeInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateBarcodeInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteBarcodeInfo(ModelMap paramMap) throws NkiaException;
	
	public Map uploadPdaUpdateData(AtchFileVO atchFileVO, HttpServletRequest request)throws NkiaException;
	
	public List searchPdaUpFileManagerList(ModelMap paramMap) throws NkiaException;
	
	public int searchPdaUpFileManagerCount(ModelMap paramMap) throws NkiaException;
	
	public void insertPdaFileInfo(ModelMap paramMap) throws NkiaException;
	
	public void updatePdaFileInfo(ModelMap paramMap) throws NkiaException;
	
	public void deletePdaFileInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchBaseAsset(ModelMap paramMap) throws NkiaException;
	
	public void updateAsset(ModelMap paramMap) throws NkiaException;
}
