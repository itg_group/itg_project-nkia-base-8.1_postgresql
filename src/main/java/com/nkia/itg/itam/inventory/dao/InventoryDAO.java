/*
 * @(#)InventoryDAO.java              2013. 11. 13.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.inventory.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("inventoryDAO")
public class InventoryDAO extends EgovComAbstractDAO{
	
	
	public List searchAssetDataDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("InventoryDAO.searchAssetDataDown", paramMap);
		return resultList;
	}		
	
	public List searchBarcodeInfoList(ModelMap paramMap) throws NkiaException{
		return list("InventoryDAO.searchBarcodeInfoList", paramMap);
	}

	public int searchBarcodeInfoCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("InventoryDAO.searchBarcodeInfoCount", paramMap);
	}
	
	public List searchPublicationAssetList(ModelMap paramMap) throws NkiaException{
		return list("InventoryDAO.searchPublicationAssetList", paramMap);
	}
	
	public int searchPublicationAssetCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("InventoryDAO.searchPublicationAssetCount", paramMap);
	}
	
	public List searchPopAssetList(ModelMap paramMap) throws NkiaException{
		return list("InventoryDAO.searchPopAssetList", paramMap);
	}
	
	public int searchPopAssetCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("InventoryDAO.searchPopAssetCount", paramMap);
	}
	
	public void insertBarcodeInfo(HashMap barcodeInfo) throws NkiaException {
		this.insert("InventoryDAO.insertBarcodeInfo", barcodeInfo);
	}
	
	public void insertBarcodeAsset(HashMap assetInfo) throws NkiaException {
		this.insert("InventoryDAO.insertBarcodeAsset", assetInfo);
	}

	public String selectBarcodeID(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (String)selectByPk("InventoryDAO.selectBarcodeID", paramMap);
	}	
	
	public void updateBarcodeInfo(HashMap barcodeInfo) throws NkiaException {
		this.update("InventoryDAO.updateBarcodeInfo", barcodeInfo);
	}
	
	public void deleteBarcodeInfo(HashMap barcodeInfo) throws NkiaException {
		this.delete("InventoryDAO.deleteBarcodeInfo", barcodeInfo);
	}
	
	public void deleteBarcodeAsset(HashMap barcodeInfo) throws NkiaException {
		this.delete("InventoryDAO.deleteBarcodeAsset", barcodeInfo);
	}
	
	public void deleteBarcodeAssetNo(HashMap barcodeInfo) throws NkiaException {
		this.delete("InventoryDAO.deleteBarcodeAssetNo", barcodeInfo);
	}
	
	public List searchPdaUpFileManagerList(ModelMap paramMap) throws NkiaException{
		return list("InventoryDAO.searchPdaUpFileManagerList", paramMap);
	}
	
	public int searchPdaUpFileManagerCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("InventoryDAO.searchPdaUpFileManagerCount", paramMap);
	}
	
	public void insertPdaFileInfo(HashMap paramMap) throws NkiaException {
		this.insert("InventoryDAO.insertPdaFileInfo", paramMap);
	}
	
	public void deletePdaFileInfo(HashMap paramMap) throws NkiaException {
		this.delete("InventoryDAO.deletePdaFileInfo", paramMap);
	}
	
	public void updatePdaFileInfo(HashMap paramMap) throws NkiaException {
		this.update("InventoryDAO.updatePdaFileInfo", paramMap);
	}
	
	public List searchBaseAsset(ModelMap paramMap) throws NkiaException{
		return list("InventoryDAO.searchBaseAsset", paramMap);
	}	
	
	public void updateAsset(HashMap paramMap) throws NkiaException {
		this.update("InventoryDAO.updateAsset", paramMap);
	}
	
	public int selectConfNmCnt(HashMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("InventoryDAO.selectConfNmCnt", paramMap);
	}
	
	public void insertConfNmCode(HashMap paramMap) throws NkiaException {
		this.update("InventoryDAO.insertConfNmCode", paramMap);
	}
	
	public void updateConfNmCode(HashMap paramMap) throws NkiaException {
		this.update("InventoryDAO.updateConfNmCode", paramMap);
	}
		
	public String selectConfID(HashMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (String)selectByPk("InventoryDAO.selectConfID", paramMap);
	}
	
	public String selectColNm(String col_id)throws NkiaException {
		// TODO Auto-generated method stub
		return (String)selectByPk("InventoryDAO.selectColNm", col_id);
	}
	
	public String selectPrevValue(HashMap seqParam)throws NkiaException {
		// TODO Auto-generated method stub
		return (String)selectByPk("InventoryDAO.selectPrevValue", seqParam);
	}
}

