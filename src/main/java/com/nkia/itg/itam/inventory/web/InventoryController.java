/*
 * @(#)InventoryController.java              2013. 11. 13.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.inventory.web;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.inventory.service.InventoryService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class InventoryController {
	
	private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);

	@Resource(name = "inventoryService")
	private InventoryService inventoryService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil;
	
	
	/**
	 * 
	 * TODO PDA 정보 현행화(다운로드화면)
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/inventory/pdaInfoDataDown.do")
	public String inventoryManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/inventory/pdaInfoDataDown.nvf";
		return forwarPage;
	}

	/**
	 * 
	 * TODO 바코드발행정보 관리
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/inventory/barcodePublicationInfo.do")
	public String barcodePublicationInfo(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/inventory/barcodePublicationInfo.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * TODO PDA수정 정보관리 탭
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/pdaDataUpdateTab.do")
	public String pdaDataUpdateTab(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/inventory/pdaDataUpdateTab.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * TODO PDA수정 정보관리 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/inventory/pdaDataUpdate.do")
	public String pdaDataUpdate(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/inventory/pdaDataUpdate.nvf";
		return forwarPage;
	}

	/**
	 * 
	 * TODO PDA수정 정보 상세화면
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */				
	@RequestMapping(value="/itg/itam/inventory/pdaDataUpdateAsset.do")
	public String pdaDataUpdateAsset(ModelMap paramMap, HttpServletRequest request) throws NkiaException{
		
		String FILE_ID = (request.getParameter("FILE_ID") != null)?request.getParameter("FILE_ID"): "";
		String MNG_NM = (request.getParameter("MNG_NM") != null)?request.getParameter("MNG_NM"): "";
		String SAVE_FILE_NM = (request.getParameter("SAVE_FILE_NM") != null)?request.getParameter("SAVE_FILE_NM"): "";
		String REMARK = (request.getParameter("REMARK") != null)?request.getParameter("REMARK"): "";
		
		paramMap.put("FILE_ID", FILE_ID);
		paramMap.put("MNG_NM", MNG_NM);
		paramMap.put("SAVE_FILE_NM", SAVE_FILE_NM);
		paramMap.put("REMARK", REMARK);
		
		return "/itg/itam/inventory/pdaDataUpdateAsset.nvf";
	}	
	
	/**
	 * 파일다운로드 기능
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/inventory/searchAssetDataDown.do")
	public void searchAssetDataDown(HttpServletRequest request, HttpServletResponse response) throws NkiaException {
          ResultVO resultVO = new ResultVO();
          List resultList = new ArrayList();
          ModelMap excelMap = new ModelMap();
          BufferedWriter writer = null;
          try{
        	  	String stordFilePate = BaseConstraints.getPdaFileUploadPath();
        	  	
        	  	ModelMap params = new ModelMap();
        	  	String type = request.getParameter("type");
        	  	params.put("type", type);
                resultList = inventoryService.searchAssetDataDown(params);
                 
                String saveFileNm = type+"정보.txt";
                String origFileNm = type+"정보.txt";
                
                //writer = new BufferedWriter(new FileWriter(stordFilePate+saveFileNm));
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(stordFilePate+saveFileNm)));
                for(int i=0; i < resultList.size(); i++){
                	HashMap rowData = (HashMap)resultList.get(i);
                	String getData = (String) rowData.get("DATA");
                    writer.write(getData);
                    writer.newLine();
                }
                writer.close();
                
    		    boolean isSuccess = atchFileUtil.createOutputFile(request, response, saveFileNm, origFileNm, stordFilePate, "application/vnd.ms-excel");
    		    
    			if( !isSuccess ){
    				PrintWriter out = response.getWriter();
    				response.setContentType("text/html; charset=euc-kr");
    				out.println("파일을 찾을 수 없습니다.");
    			}
                 
          } catch(Exception e) {
                 throw new NkiaException(e);
          } finally {
  		    if (writer != null) {
  				try {
  					writer.close();
  				} catch (Exception ignore) {
  				    throw new NkiaException(ignore);
  				}
  			}
  		}
	}
	
	/**
	 * 바코드발행정보 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/searchBarcodeInfoList.do")
	public @ResponseBody ResultVO searchBarcodeInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = inventoryService.searchBarcodeInfoList(paramMap);		
			totalCount = inventoryService.searchBarcodeInfoCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 바코드발행정보 자산 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/searchPublicationAssetList.do")
	public @ResponseBody ResultVO searchPublicationAssetList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = inventoryService.searchPublicationAssetList(paramMap);		
			totalCount = inventoryService.searchPublicationAssetCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 바코드발행정보 팝업 자산 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/searchPopAssetList.do")
	public @ResponseBody ResultVO searchPopAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = inventoryService.searchPopAssetList(paramMap);		
			totalCount = inventoryService.searchPopAssetCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 바코드 발행정보 등록
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/insertBarcodeInfo.do")
	public @ResponseBody ResultVO insertBarcodeInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{		
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			inventoryService.insertBarcodeInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 바코드 발행정보 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/updateBarcodeInfo.do")
	public @ResponseBody ResultVO updateBarcodeInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			inventoryService.updateBarcodeInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 바코드 발행정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/deleteBarcodeInfo.do")
	public @ResponseBody ResultVO deleteBarcodeInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			inventoryService.deleteBarcodeInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * PDA 수정 파일 업로드
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value = "/itg/itam/inventory/uploadPdaUpdateData.do")
	public void uploadPdaUpdateData(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException {
		JSONObject returnData = new JSONObject();
		
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			// 파일등록
			Map result = inventoryService.uploadPdaUpdateData(atchFileVO, request);
			
	        //sets success to true
			returnData.put("success", true);
			returnData.put("resultData", result);
	        
			//convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        //out.close();
	        
		} catch (Exception e) {
/*			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
*/			
			throw new NkiaException(e);
		} finally {
		    if (out != null) {
				try {
					out.close();
				} catch (Exception ignore) {
					throw new NkiaException(ignore);
				}
			}	
		}
	}
	
	
	/**
	 * PDA 수정파일 이력리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/searchPdaUpFileManagerList.do")
	public @ResponseBody ResultVO searchPdaUpFileManagerList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = inventoryService.searchPdaUpFileManagerList(paramMap);		
			totalCount = inventoryService.searchPdaUpFileManagerCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	
	/**
	 * PDA 수정이력정보 등록
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/insertPdaFileInfo.do")
	public @ResponseBody ResultVO insertPdaFileInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			inventoryService.insertPdaFileInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * PDA 수정이력정보 변경
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/updatePdaFileInfo.do")
	public @ResponseBody ResultVO updatePdaFileInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
			inventoryService.updatePdaFileInfo(paramMap);

			String oldFileNm = (String) paramMap.get("old_file_nm");
			String saveFileNm = (String) paramMap.get("save_file_nm");	
			
			
			if(!oldFileNm.equals(saveFileNm)){
				String pdaFileUploadPath = BaseConstraints.getPdaFileUploadPath();
				String yymm = oldFileNm.substring(0, 6);
				String deleteFilePath = pdaFileUploadPath + yymm + "/" +oldFileNm;
				File deleteFile = new File(deleteFilePath);
				if ( deleteFile.exists() ){
					deleteFile.delete();
				}
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));			
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.common.file.notfound"));							
			}
			
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * PDA 수정이력정보 제거
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/deletePdaFileInfo.do")
	public @ResponseBody ResultVO deletePdaFileInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			inventoryService.deletePdaFileInfo(paramMap);
			
			String pdaFileUploadPath = BaseConstraints.getPdaFileUploadPath();
			String saveFileName = (String) paramMap.get("save_file_nm");
			String yymm = saveFileName.substring(0, 6);
			String deleteFilePath = pdaFileUploadPath + yymm + "/" +saveFileName;
			File deleteFile = new File(deleteFilePath);
			if ( deleteFile.exists() ){
				deleteFile.delete();
			}
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 바코드발행정보 팝업 자산 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/readFile.do")
	public @ResponseBody ResultVO readFile(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		File f1 = null;
		FileInputStream fis = null;
		BufferedReader b_reader = null;
		try{
			String pdaFileUploadPath = BaseConstraints.getPdaFileUploadPath();
			String saveFileName = (String) paramMap.get("save_file_nm");
			String dataType = (String) paramMap.get("dataType");
			String yymm = saveFileName.substring(0, 6);		
			f1 = new File(pdaFileUploadPath+ yymm+"/"+saveFileName);
			fis = new FileInputStream(f1);
			b_reader = new BufferedReader(new InputStreamReader(fis,"EUC-KR"));
			
			List resultList = new ArrayList();
			List baseList = new ArrayList();
			GridVO gridVO = new GridVO();

	         
	        // 파일로부터 한줄단위로 처리하는 반복문
	        String str = null;
	        String asset_id = "''";
	        while((str=b_reader.readLine()) != null){
//	            System.out.println(str);
	            String[] spl = str.split("");
	            HashMap rowData = new HashMap();
	            for(int i = 0; i < spl.length; i++){
	            	String[] k = spl[i].split(":");
	            	String val = null;
	            	if(k.length == 1){
	            		val = "";
	            	}else{
	            		val = k[1];
	            	}
	            	rowData.put(k[0], val); 
	            	if("ASSET_ID".equals(k[0])){
	            		asset_id = asset_id + ",'"+val+"'";
	            	}
	            	
	            }
	            resultList.add(rowData);
	        }
	       // fis.close();
	        //b_reader.close();
	        
	        if("BASE".equals(dataType)){
	        	paramMap.put("asset_id", asset_id);
	        	baseList = inventoryService.searchBaseAsset(paramMap);
	        	gridVO.setRows(baseList);	        	
	        }else{
	        	gridVO.setRows(resultList);	        	
	        }

			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		} finally {
		    if (fis != null) {
				try {
					fis.close();
				} catch (Exception ignore) {
					throw new NkiaException(ignore);
				}
			}
		    if (b_reader != null) {
				try {
					b_reader.close();
				} catch (Exception ignore) {
					throw new NkiaException(ignore);
				}
		    }	
		}
		return resultVO;
	}
	

	/**
	 * 기존 자산 정보를 PDA 수정된 자산 정보로 변경
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/inventory/updateAsset.do")
	public @ResponseBody ResultVO updateAsset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}					
	    	inventoryService.updateAsset(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
}
