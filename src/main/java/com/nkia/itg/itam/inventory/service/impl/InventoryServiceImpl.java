/*
 * @(#)inventoryServiceImpl.java              2013. 11. 13.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.inventory.service.impl;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.inventory.dao.InventoryDAO;
import com.nkia.itg.itam.inventory.service.InventoryService;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;

import egovframework.com.cmm.EgovWebUtil;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("inventoryService")
public class InventoryServiceImpl implements InventoryService{
	
	private static final Logger logger = LoggerFactory.getLogger(InventoryServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "inventoryDAO")
	public InventoryDAO inventoryDAO;

	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	public List searchAssetDataDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = inventoryDAO.searchAssetDataDown(paramMap); 
		return resultList;
	}	
	
	public List searchBarcodeInfoList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchBarcodeInfoList(paramMap);
	}

	public int searchBarcodeInfoCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchBarcodeInfoCount(paramMap);
	}	
	
	public List searchPublicationAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchPublicationAssetList(paramMap);
	}
	
	public int searchPublicationAssetCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchPublicationAssetCount(paramMap);
	}	
	
	public List searchPopAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchPopAssetList(paramMap);
	}
	
	public int searchPopAssetCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchPopAssetCount(paramMap);
	}	
	
	public void insertBarcodeInfo(ModelMap paramMap) throws NkiaException {		
		int mapSize = paramMap.size();
		String login_user_id = null;
		String barcode_id = inventoryDAO.selectBarcodeID(paramMap);
		
		for(int i=0; i<mapSize; i++){
			String mapIndex = toString().valueOf(i);
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
			if(i == 0){
				HashMap barcodeInfo = new HashMap();
				barcodeInfo = (HashMap) paramMap.get(mapIndex);
		    	if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		login_user_id = userVO.getUser_id();
		    		barcodeInfo.put("login_user_id", userVO.getUser_id());
		    	}
		    	barcodeInfo.put("barcode_id", barcode_id);
		    	inventoryDAO.insertBarcodeInfo(barcodeInfo);
			}else{
				HashMap assetInfo = new HashMap();
				assetInfo = (HashMap) paramMap.get(mapIndex);
				if(assetInfo != null){
					assetInfo.put("barcode_id", barcode_id);
					inventoryDAO.insertBarcodeAsset(assetInfo);					
				}
			}
		}		
	}	
	
	public void updateBarcodeInfo(ModelMap paramMap) throws NkiaException {		
		int mapSize = paramMap.size();
		String login_user_id = null;
		String barcode_id = null;
		for(int i=0; i<mapSize; i++){
			String mapIndex = toString().valueOf(i);
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
			if(i == 0){
				HashMap barcodeInfo = new HashMap();
				barcodeInfo = (HashMap) paramMap.get(mapIndex);
				barcode_id = (String) barcodeInfo.get("barcode_id");
				if(isAuthenticated) {
					UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					login_user_id = userVO.getUser_id();
					barcodeInfo.put("login_user_id", userVO.getUser_id());
				}
				inventoryDAO.updateBarcodeInfo(barcodeInfo);
			}/*else{
				HashMap assetInfo = new HashMap();
				assetInfo = (HashMap) paramMap.get(mapIndex);
				assetInfo.put("barcode_id", barcode_id);
				if(assetInfo != null){
					if(assetInfo.get("barcode_no") != null && !"".equals(assetInfo.get("barcode_no")) ){
						inventoryDAO.deleteBarcodeAssetNo(assetInfo);
					}
					inventoryDAO.updateBarcodeAsset(assetInfo);
				}
			}*/
		}		
	}	
	
	public void deleteBarcodeInfo(ModelMap paramMap) throws NkiaException {
		inventoryDAO.deleteBarcodeAsset(paramMap);
		inventoryDAO.deleteBarcodeInfo(paramMap);
	}
	
	/**
	 * 
	 * PDA 수정자료 업로드
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	private static final String pda_file_upload_path = BaseConstraints.getPdaFileUploadPath();
	
	public Map uploadPdaUpdateData(AtchFileVO atchFileVO, HttpServletRequest request)throws NkiaException {
		MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
		InputStream stream = null;

		try {
		    File cFile = new File(EgovWebUtil.filePathBlackList(pda_file_upload_path));
		    
		    cFile.setExecutable(false, true);
		    cFile.setReadable(true);
		    cFile.setWritable(false, true);
		    
		    if (!cFile.exists()){
		    	cFile.mkdir();
		    }

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		Map result = null;
		Iterator<String> fileIter = mptRequest.getFileNames();
		while (fileIter.hasNext()) {
			String fileName = (String) fileIter.next();
			MultipartFile mFile = mptRequest.getFile( fileName );
			if (mFile.getSize() > 0) {
				// 개별 최대 파일 Check
				result = EgovFileMngUtil.uploadFile(mFile, pda_file_upload_path);
			}
		}
		
		return result;
	}
	
	public List searchPdaUpFileManagerList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchPdaUpFileManagerList(paramMap);
	}
	
	public int searchPdaUpFileManagerCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchPdaUpFileManagerCount(paramMap);
	}	
	
	public void insertPdaFileInfo(ModelMap paramMap) throws NkiaException {
		inventoryDAO.insertPdaFileInfo(paramMap);
	}
	
	public void updatePdaFileInfo(ModelMap paramMap) throws NkiaException {
		inventoryDAO.updatePdaFileInfo(paramMap);
	}
	
	public void deletePdaFileInfo(ModelMap paramMap) throws NkiaException {
		inventoryDAO.deletePdaFileInfo(paramMap);
	}
	
	public List searchBaseAsset(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return inventoryDAO.searchBaseAsset(paramMap);
	}
	
	public void updateAsset(ModelMap paramMap) throws NkiaException {
		
		int mapSize = paramMap.size();
		String login_user_id = null;
		
		for(int i=0; i<mapSize; i++){
			String mapIndex = toString().valueOf(i);
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
			if(i > 0){
				HashMap assetInfo = new HashMap();
				assetInfo = (HashMap) paramMap.get(mapIndex);
				if(assetInfo != null){
					
					// 변경 이력 남기기
					String user_id = "";
					
					if(isAuthenticated) {
						UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
						user_id = userVO.getUser_id();
					}
					String conf_id = inventoryDAO.selectConfID(assetInfo);
//					assetHistoryService.insertAssetAttrHistory(conf_id, assetInfo, "", user_id);
					String asset_id = (String) assetInfo.get("asset_id");
					
					inventoryDAO.updateAsset(assetInfo);
				}
			}
		}		
		
	}		
	
	
/*	public String selectAssetChgHisSeq(HashMap dataMap)throws NkiaException{
		opmsDetailDAO.insertChgHisSeq(dataMap);
		return opmsDetailDAO.selectAssetChgHisSeq(dataMap);
	}*/
	
}
