package com.nkia.itg.itam.batchEdit.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("batchEditDownDAO")
public class BatchEditDownDAO extends EgovComAbstractDAO {

	public List selectEditAttrList(HashMap paramMap) {
		return list("BatchEditDownDAO.selectEditAttrList", paramMap);
	}

	public List searchAssetListForExcel(Map paramMap) {
		return list("BatchEditDownDAO.searchAssetListForExcel", paramMap);
	}

}
