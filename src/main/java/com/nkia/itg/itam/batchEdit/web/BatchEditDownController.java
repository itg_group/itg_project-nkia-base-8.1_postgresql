package com.nkia.itg.itam.batchEdit.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.excel.ExcelMakerByPoi;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.batchEdit.service.BatchEditDownService;
import com.nkia.itg.itam.statistics.service.AssetStatisticsService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class BatchEditDownController {

	@Resource(name="batchEditDownService")
	private BatchEditDownService batchEditDownService;
	
	@Resource(name="excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name="excelMakerByPoi")
	private ExcelMakerByPoi excelMakerByPoi;	
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "assetStatisticsService")
	private AssetStatisticsService assetStatisticsService;
	
	/**
	 * 수정 대상 자산 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/goSelectBatchEditAsset.do")
	public String goSelectBatchEditAsset(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		// 엑셀템플릿 자동 생성(By Poi)
		String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim();
		paramMap.put("excel_library",excelLib);
		
		String forwarPage = "/itg/itam/batchEdit/selectBatchEditAsset.nvf";
		return forwarPage;
	}
	
	/**
	 * 수정 속성 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/selectEditAttrList.do")
	public @ResponseBody ResultVO selectEditAttrList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = batchEditDownService.selectEditAttrList(paramMap);

			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
			HashMap resultMap = new HashMap();
			resultMap.put("resultList", resultList);
			resultVO.setResultMap(resultMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/batchEditAssetExcelDown.do")
	public @ResponseBody ResultVO batchEditAssetExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Boolean isAuthenticated = false;
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			String classType = (String) param.get("class_type_arr");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			HashMap sheetMap = (HashMap) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			String columns = (String) sheetMap.get("includeColumns");
			
			if(null != columns){
				
				String newColumnList = "";
				
				param.put("col_id", columns);
				param.put("class_type", classType);
				
				isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
				if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		param.put("login_user_id", userVO.getUser_id());
		    	}
				
				List assetList = batchEditDownService.searchAssetListForExcel(param);
				resultArray.add(assetList);
				
				newSheetList.add(sheetMap);
				
			} 
			else {
				resultArray.add(null);
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFileForBatchEdit(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	/**
	 * 엑셀템플릿 자동 생성(By Poi)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/batchEditAssetExcelDownByPoi.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO batchEditAssetExcelDownByPoi(@RequestBody ModelMap param) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) param.get("excelParam");
			Map paramMap = (Map) param.get("param");
			
			String classType = (String) paramMap.get("class_type_arr");
			String classId = (String)paramMap.get("class_id");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			HashMap sheetMap = (HashMap) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			String columns = (String) sheetMap.get("includeColumns");
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", loginCustId);
			}
	    	
			boolean bTestUser = true; // *POIEXCEL* START
			
			if(null != columns){
				// 자산조회 파라미터 : paramMap

				// *POIEXCEL* START
				//String columns = (String) sheetMap.get("includeColumns");
				String htmltypes = (String) sheetMap.get("htmlTypes");
				String tblcols = (String) sheetMap.get("tblCols");
				String col_id = "";  /*(columns.substring(0,1).equals(",")==true?"":",") + columns */
				
				String [] strarrColumn = columns.split(",");
				String [] strarrHtmlType = htmltypes.split(",");
				String [] strarrTblcol = tblcols.split(",");
				String strTemp = "";
				
				for ( int c = 0 ; c < strarrColumn.length ; c++ ){
					if ( bTestUser == true && strarrColumn.length == strarrHtmlType.length && strarrColumn.length == strarrTblcol.length) {
						String [] strarrTemp = strarrTblcol[c].split("\\.");
						if ( String.valueOf(strarrHtmlType[c]).indexOf("POPUP") > -1 && strarrTemp.length > 0 ) {
							col_id += ", DECODE(" + strarrColumn[c] + "_NM,NULL,NULL, " + strarrColumn[c] + "_NM || ' [' ||" +strarrTemp[1]+ " || ']') AS " + strarrColumn[c];
						} else if ( String.valueOf(strarrHtmlType[c]).equals("SELECT") == true && strarrTemp.length > 0 ) {
							col_id += "," + strarrColumn[c] + "_NM AS " + strarrColumn[c];
						} /*else if ( String.valueOf(strarrHtmlType[c]).equals("DATE") == true && strarrTemp.length > 0 ) {
							col_id += ", TO_CHAR(" + strarrColumn[c] + ", 'YYYY-MM-DD') AS " + strarrColumn[c];
						} */
						else { col_id += "," + strarrColumn[c]; }
					}
					else { col_id += "," + strarrColumn[c]; }
				} // for c
				
				// *POIEXCEL* END
				
				//paramMap.put("col_id", col_id );/*searchAssetListForExcel 쿼리에 시작 컬럼이 있는 경우*/
				paramMap.put("col_id", "'BATCHEDITDOWN' AS QUERY_TYPE" + col_id );/*searchAssetListForExcel 쿼리에 시작 컬럼이 없는 경우*/
				paramMap.put("class_type", classType);
				
				
				
//				List assetList = batchEditDownService.searchAssetListForExcel(paramMap);
				List assetList = assetStatisticsService.searchExcelPoi(paramMap);	//BASE 202104 custom : 엑셀 다운로드를 POI 방식으로 변경 
				resultArray.add(assetList);
				
				newSheetList.add(sheetMap);
				

			}else {
				resultArray.add(null);
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);
			
			excelParam.put("class_id", classId);
			
			if ( bTestUser == true ){
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultArray); // *POIEXCEL*
			}
			else {
				excelMap = excelMaker.makeExcelFileForBatchEdit(excelParam, resultArray);
			}

			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}	
}
