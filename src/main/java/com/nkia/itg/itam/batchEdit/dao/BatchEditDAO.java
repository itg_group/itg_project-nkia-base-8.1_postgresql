/*
 * @(#)BatchEditDAO.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.batchEdit.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("batchEditDAO")
public class BatchEditDAO extends EgovComAbstractDAO{
	
	public List selectClassTypeClassName(Map paramMap) throws NkiaException{
		return list("batchEditDAO.selectClassTypeClassName", paramMap);
	}
	
	// 자산 List 쿼리 직접 참조
	public List searchBatchEditAssetPop(Map paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchAssetList", paramMap);
	}
	
	public int searchBatchEditAssetPopCount(Map paramMap)throws NkiaException {
		return (Integer)selectByPk("AssetStatisticsDAO.searchAssetListCount", paramMap);
	}
	
	public List searchBatchEditUpAssetList(Map paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchAssetList", paramMap);
	}
	// 자산 List 쿼리 직접 참조
	
	public List searchBatchEditCommonAttrList(Map paramMap) throws NkiaException{
		return list("batchEditDAO.searchBatchEditCommonAttrList", paramMap);
	}
	
	public List searchBatchEditClassAttrList(Map paramMap) throws NkiaException{
		return list("batchEditDAO.searchBatchEditClassAttrList", paramMap);
	}
	public List searchBatchEditAssetAttrList(Map paramMap) throws NkiaException{
		return list("batchEditDAO.searchBatchEditAssetAttrList", paramMap);
	}
	
	public void updateBatchEditCommonAttr(Map paramMap) throws NkiaException{
		update("batchEditDAO.updateBatchEditCommonAttr", paramMap);
	}
	
	public void updateBatchEditClassAttr(Map paramMap) throws NkiaException{
		update("batchEditDAO.updateBatchEditClassAttr", paramMap);
	}
	
	public List searchBatchEditComponentAttrList(Map paramMap) throws NkiaException{
		return list("batchEditDAO.searchBatchEditComponentAttrList", paramMap);
	}
	
	public HashMap selectAnythingQueryString(Map paramMap) throws NkiaException{
		return (HashMap)selectByPk("batchEditDAO.selectAnythingQueryString", paramMap);
	}
	
	public void insertAnythingQueryString(Map paramMap) throws NkiaException{
		insert("batchEditDAO.insertAnythingQueryString", paramMap);
	}
	
	public void updateAnythingQueryString(Map paramMap) throws NkiaException{
		update("batchEditDAO.updateAnythingQueryString", paramMap);
	}
	
	public void deleteAnythingQueryString(Map paramMap) throws NkiaException{
		delete("batchEditDAO.deleteAnythingQueryString", paramMap);
	}
	
	public String selectBatchEditColName(Map paramMap) throws NkiaException{
		String colName = (String)this.selectByPk("batchEditDAO.selectBatchEditColName", paramMap);
		return colName;
	}
	
	public String selectBatchEditColHtmlType(Map paramMap) throws NkiaException{
		String colName = (String)this.selectByPk("batchEditDAO.selectBatchEditColHtmlType", paramMap);
		return colName;
	}
	
	public void updateAssetCustId(HashMap paramMap) throws NkiaException{
		update("batchEditDAO.updateAssetCustId", paramMap);
	}
	
}
