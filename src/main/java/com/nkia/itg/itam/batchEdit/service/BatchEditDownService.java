package com.nkia.itg.itam.batchEdit.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

public interface BatchEditDownService {

	List selectEditAttrList(HashMap paramMap);

	List searchAssetListForExcel(Map param);
	
}
