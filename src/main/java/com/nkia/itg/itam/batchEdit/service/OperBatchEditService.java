/*
 * @(#)OperBatchEditService.java              2018. 4. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.batchEdit.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface OperBatchEditService {
	
	/**
	 * 자산과 매핑된 전체 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAssetOperList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 부서 트리 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchDeptTree(HashMap paramMap) throws NkiaException;
	
	/**
	 * 사용자 전체 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAllUserList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 사용자 전체 조회 카운트(페이징용)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchAllUserListCount(HashMap paramMap) throws NkiaException;
	
	/**
	 * 자산담당자 일괄 수정(정)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateAssetOperMain(HashMap paramMap) throws NkiaException;
	
	/**
	 * 자산담당자 일괄 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateAssetOper(HashMap paramMap) throws NkiaException;
	
}
