/*
 * @(#)OperBatchEditServiceImpl.java              2018. 4. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.batchEdit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.batchEdit.dao.OperBatchEditDAO;
import com.nkia.itg.itam.batchEdit.service.OperBatchEditService;

@Service("operBatchEditService")
public class OperBatchEditServiceImpl implements OperBatchEditService {
	
	private static final Logger logger = LoggerFactory.getLogger(OperBatchEditServiceImpl.class);
	
	@Resource(name = "operBatchEditDAO")
	public OperBatchEditDAO operBatchEditDAO;
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	/**
	 * 자산과 매핑된 전체 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchAssetOperList(HashMap paramMap) throws NkiaException{
		return operBatchEditDAO.searchAssetOperList(paramMap);
	}
	
	/**
	 * 부서 트리 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchDeptTree(HashMap paramMap) throws NkiaException {
		return operBatchEditDAO.searchDeptTree(paramMap);
	}
	
	/**
	 * 사용자 전체 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchAllUserList(HashMap paramMap) throws NkiaException{
		return operBatchEditDAO.searchAllUserList(paramMap);
	}
	
	/**
	 * 사용자 전체 조회 카운트(페이징용)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public int searchAllUserListCount(HashMap paramMap) throws NkiaException{
		return operBatchEditDAO.searchAllUserListCount(paramMap);
	}
	
	/**
	 * 자산담당자 일괄 수정(정)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void updateAssetOperMain(HashMap paramMap) throws NkiaException{
		HashMap formData = (HashMap)paramMap.get("formData");
		List gridData = (List)paramMap.get("gridData");
		
		//구성자원 관리자수정
		for(int j=0; j<gridData.size(); j++) {
			HashMap gridMap = (HashMap)gridData.get(j);
			
			HashMap userMap = new HashMap();
			userMap.put("OPER_USER_ID", gridMap.get("OPER_USER_ID"));
			userMap.put("CHANGE_OPER_USER_ID", formData.get("oper_user_id"));
			userMap.put("CUSTOMER_ID", formData.get("customer_id"));
			
			List<HashMap> assetList = operBatchEditDAO.searchAssetListByOper(gridMap);
			
			for (HashMap assetInfo : assetList) {
				HashMap dataMap = new HashMap<>();
				dataMap.put("MAIN_USER_ID", formData.get("oper_user_id"));
				
				// 이력 등록
				assetHistoryService.insertAssetAttrHistory(assetInfo.get("CONF_ID").toString()
						, dataMap
						, "담당자 일괄 변경"
						, paramMap.get("upd_user_id").toString());
				
				userMap.put("CONF_ID", assetInfo.get("CONF_ID"));
				
				// 담당자 업데이트
				operBatchEditDAO.updateAssetMainUser(userMap);
			}
		}
	}
	
	/**
	 * 자산담당자 일괄 수정(부)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void updateAssetOper(HashMap paramMap) throws NkiaException{
		
		HashMap formData = (HashMap)paramMap.get("formData");
		List gridData = (List)paramMap.get("gridData");
		HashMap tempMap = new HashMap();
		tempMap.put("dataList", gridData);
		tempMap.put("CUSTOMER_ID", formData.get("customer_id"));
		
		//구성자원 담당자 조회
		List<HashMap> confDataList = operBatchEditDAO.searchConfList(tempMap);
		
		HashMap insertMap = new HashMap();
		
		for(int i=0;i<confDataList.size();i++){
			HashMap dataMap = confDataList.get(i);
			String confId = dataMap.get("CONF_ID").toString();
			String operSeq = dataMap.get("SEQ").toString();
			List<HashMap> histDataList = new ArrayList<HashMap>();
			
			insertMap.put("conf_id",confId);
			insertMap.put("seq",operSeq);
			insertMap.put("oper_user_id",formData.get("oper_user_id"));
			insertMap.put("upd_user_id",paramMap.get("upd_user_id"));
			insertMap.put("oper_user_type",formData.get("oper_user_type"));

			tempMap.put("conf_id", confId);
			
			//기존데이터 삭제
			operBatchEditDAO.deleteDistinctOperr(tempMap);
			
			HashMap histDataMap = new HashMap<>();
			histDataMap.put("USER_ID", formData.get("oper_user_id"));
			histDataMap.put("OPER_USER_TYPE", formData.get("oper_user_type"));
			histDataList.add(histDataMap);
			
			// 담당자 컴포넌트 이력데이터를 등록
			assetHistoryService.insertOperCompHistory(confId
					, histDataList
					, "담당자 일괄 변경"
					, paramMap.get("upd_user_id").toString());
			
			operBatchEditDAO.insertModifyOper(insertMap);
		}
	}
}
