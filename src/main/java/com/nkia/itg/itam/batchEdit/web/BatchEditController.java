/*
 * @(#)BatchEditController.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.batchEdit.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.itam.batchEdit.service.BatchEditService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class BatchEditController {

	@Resource(name = "batchEditService")
	private BatchEditService batchEditService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name="assetAutomationService")
	private AssetAutomationService assetAutomationService;
	
	/**
	 * 자산속성 일괄수정 - 속성 일괄수정 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/batchEditTab.do")
	public String newAssetManagerTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/batchEdit/batchEditTab.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산속성 일괄수정 - 속성 일괄수정 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/batchEdit/batchEdit.do")
	public String goBatchEdit(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/batchEdit/batchEdit.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산속성 일괄수정 - 분류체계 정보 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/selectClassTypeClassName.do")
	public @ResponseBody ResultVO selectClassTypeClassName(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = batchEditService.selectClassTypeClassName(paramMap);	
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산속성 일괄수정 - 자산선택 팝업 자산목록 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/searchBatchEditAssetPop.do")
	public @ResponseBody ResultVO searchBatchEditAssetPop(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			String isCallStatistics = (String)paramMap.get("is_call_statistics");
			
			if("true".equals(isCallStatistics)) {
				resultList = batchEditService.searchBatchEditAssetPop(paramMap);
			} else {
				//@@ 페이징 관련 내용 
				PagingUtil.getFristEndNum(paramMap);
				resultList = batchEditService.searchBatchEditAssetPop(paramMap);		
				totalCount = batchEditService.searchBatchEditAssetPopCount(paramMap);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
				gridVO.setTotalCount(totalCount);
			}
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산속성 일괄수정 - 속성리스트 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/searchBatchEditAttrList.do")
	public @ResponseBody ResultVO searchBatchEditAttrList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			resultList = batchEditService.searchBatchEditAttrList(paramMap);	
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산속성 일괄수정 - 수정대상 자산리스트 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/searchBatchEditUpAssetList.do")
	public @ResponseBody ResultVO searchBatchEditUpAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			resultList = batchEditService.searchBatchEditUpAssetList(paramMap);	
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산속성 일괄수정 - 원장화면
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/openBatchEditDetail.do")
	public String openBatchEditDetail(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String user_id = userVO.getUser_id();
		String colTbls = StringUtil.parseString(request.getParameter("colTbls"));
		String confIds = StringUtil.parseString(request.getParameter("confIds"));
		String class_id = StringUtil.parseString(request.getParameter("class_id"));
		String view_type = StringUtil.parseString(request.getParameter("view_type"));
		String entity_id = StringUtil.parseString(request.getParameter("entity_id"));
		String entity_class_id = StringUtil.parseString(request.getParameter("entity_class_id"));
		String edit_type = StringUtil.parseString(request.getParameter("edit_type"));
		String class_type = StringUtil.parseString(request.getParameter("class_type"));
		boolean isManagerComp = StringUtil.parseBoolean(request.getParameter("isManagerComp"));
		
		paramMap.put("user_id", user_id);
		paramMap.put("colTbls", colTbls);
		paramMap.put("confIds", confIds);
		paramMap.put("class_id", class_id);
		paramMap.put("view_type", view_type);
		paramMap.put("entity_id", entity_id);
		paramMap.put("entity_class_id", entity_class_id);
		paramMap.put("edit_type", edit_type);
		paramMap.put("class_type",class_type);
		paramMap.put("isManagerComp", isManagerComp);
		
		List resultList = new ArrayList();
		resultList = batchEditService.searchBatchEditAssetAttrList(paramMap);
		paramMap.put("attrList",resultList);
		
		batchEditService.selectClassEntitySetupInfo(paramMap);
			
		return "/itg/itam/batchEdit/batchEditDetail.nvf";
	}

	
	/**
	 * 자산속성 일괄수정 - 자산수정-공통수정
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/updateBatchEditCommonAttr.do")
	public @ResponseBody ResultVO updateBatchEditCommonAttr(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			
			if(assetAutomationService.isDuplicateBatchAsset(paramMap)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"호스트명"}));
			} else {
				batchEditService.updateBatchEditCommonAttr(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산속성 일괄수정 - 자산수정-분류체계별 수정
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/batchEdit/updateBatchEditClassAttr.do")
	public @ResponseBody ResultVO updateBatchEditClassAttr(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			
			if(assetAutomationService.isDuplicateBatchAsset(paramMap)) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"호스트명"}));
			} else {
				batchEditService.updateBatchEditClassAttr(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
