package com.nkia.itg.itam.batchEdit.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.batchEdit.dao.BatchEditUploadDAO;
import com.nkia.itg.itam.batchEdit.service.BatchEditUploadService;
import com.nkia.itg.itam.regist.service.AssetExcelRegistService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("batchEditUploadService")
public class BatchEditUploadServiceImpl implements BatchEditUploadService{
	
	@Resource(name="batchEditUploadDAO")
	private BatchEditUploadDAO batchEditUploadDAO;
	
	@Resource(name="assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	@Resource(name="assetExcelRegistService")
	private AssetExcelRegistService assetExcelRegistService;

	/**
	 * 자산 일괄수정 처리
	 * @throws NkiaException 
	 */
	@Override
	public HashMap updateBatchEditAsset(HashMap paramMap) throws NkiaException {
		HashMap paramData = (HashMap) paramMap.get("paramData");
		List<HashMap> assetList = (List) paramMap.get("assetList");
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = "";
		if(userVO != null) {
			userId = (String)userVO.getUser_id();
		}
		paramMap.put("ins_user_id", userId);
		
		// 일괄수정 시퀀스 테이블 등록
		String hisSeq = batchEditUploadDAO.createBatchHisSeq();
		paramData.put("hisSeq", hisSeq);
		batchEditUploadDAO.insertAmChgHisBatch(paramData);
		
		// 컬럼 key 목록 추출
		HashMap tempData = assetList.get(0);
		tempData.remove("id");
		List<String> tableArr = new ArrayList();
		List<String> colArr = new ArrayList();
		Iterator iter1 = tempData.keySet().iterator();
		while(iter1.hasNext()) {
			String key = (String) iter1.next();	// 형태 : [ 테이블ID.컬럼ID ]
			String[] keyArr = key.split("\\.");
			String tableNm = keyArr[0];
			String colId = keyArr[1];
			
			// 수정할 속성 및 테이블 목록 정의
			if(!"EMPTY".equals(tableNm)){
				if(!"ASSET_ID".equals(colId) && !"CONF_ID".equals(colId)){
					if(!tableArr.contains(tableNm)) {
						tableArr.add(tableNm);
					}
					colArr.add(colId);
				}
			}
		}
		
		List failList = new ArrayList(); 		// 저장 실패 데이터 목록
		List updConfList = new ArrayList();		// 일괄수정 자산 테이블에 담기위한 List
		for(HashMap dataMap : assetList) {
			
			String updStatus = "SUCCESS";	// 자산 수정 상태
			String message = "";	// 오류메세지
			String assetId = "";
			String confId = "";
			
			if(dataMap.containsKey("AM_ASSET.ASSET_ID")){
				assetId = (String) dataMap.get("AM_ASSET.ASSET_ID");
			}
			if(dataMap.containsKey("AM_INFRA.CONF_ID")){
				confId = (String) dataMap.get("AM_INFRA.CONF_ID");
			}
			
			dataMap.remove("id");
			
			if(!"".equals(assetId)){
				
				try {
				
					HashMap attrHisMap = new HashMap();	// 자산 속성 이력 데이터 Map
					
					// 쿼리 생성
					for(String tableNm : tableArr) {
						
						StringBuffer setQuery = new StringBuffer();
						
						Iterator iter2 = dataMap.keySet().iterator();
						while(iter2.hasNext()) {
							
							String key = (String) iter2.next();	// 형태 : [ 테이블ID.컬럼ID ]
							String[] keyArr = key.split("\\.");
							String keyTableNm = keyArr[0];
							String colId = keyArr[1];
							
							if(tableNm.equals(keyTableNm)) {
								if(colArr.contains(colId)){
									String value = StringUtil.parseString(dataMap.get(key));
									
									// 쿼리 생성
									String conjunction = ", ";
									if(setQuery.length() < 1){
										conjunction = "";
									}
									
									// 수정 컬럼 쿼리 만듬
									setQuery.append(conjunction + colId + " = '" + value + "'");
									
									attrHisMap.put(colId, value);
								}
							}
							
						}
						
						// UPDATE 쿼리 생성
						if(setQuery != null) {
							StringBuffer updQuery = new StringBuffer("UPDATE " + tableNm + " SET ");
							updQuery.append(setQuery);
							// 조건문
							if("AM_ASSET".equals(tableNm)){
								updQuery.append(" WHERE ASSET_ID = '" + assetId + "'");
							}else{
								updQuery.append(" WHERE CONF_ID = '" + confId + "'");
							}
							
							// 자산 이력 등록
							assetHistoryService.insertAssetAttrHistory(confId, attrHisMap, "일괄수정", userId);
							
							// 자산 수정등록
							batchEditUploadDAO.executeUpdQuery(updQuery.toString());
							
						}
						
					}
					
				} catch (Exception e) {
					updStatus = "FAIL";
/*					message = e.getMessage();
					dataMap.put("message", message);
					failList.add(dataMap);*/
					throw new NkiaException(e);
				}
				finally{
					HashMap confMap = new HashMap();
					confMap.put("hisSeq", hisSeq);
					confMap.put("confId", confId);
					confMap.put("updStatus", updStatus);
					confMap.put("message", message);
//					updConfList.add(confMap);
					
					// 일괄수정 자산 테이블 등록
					batchEditUploadDAO.insertAmChgHisBatchAsset(confMap);
				}
			
			}
			
			// 수정일자 업데이트
			HashMap updParamMap = new HashMap();
			updParamMap.put("confId", confId);
			updParamMap.put("updUserId", userId);
			batchEditUploadDAO.updateInfraUpdDt(updParamMap);
			batchEditUploadDAO.updateSwUpdDt(updParamMap);
		}
		
		
		HashMap resultMap = new HashMap();
		int totalCnt = assetList.size();
		
		resultMap.put("hisSeq", hisSeq);
		resultMap.put("totalCnt", totalCnt);	// 전체 수정 대상 자산 수
		resultMap.put("failList", failList);	// 오류 자산 목록
		resultMap.put("errorCnt", failList.size());	// 오류 자산 수
		
		return resultMap;
		
	}

	/**
	 * 자산 수정 실패 목록 조회
	 */
	@Override
	public List selectBatchAssetResultList(HashMap paramMap) {
		return batchEditUploadDAO.selectBatchAssetResultList(paramMap);
	}

	/**
	 * 일괄수정자산 검증
	 * @throws IOException 
	 */
	@Override
	public HashMap validateAssetListData(String classType, List<String> classIdList, List<HashMap> assetList) throws NkiaException, IOException {
		
		//  업로드 자산 내에서 중복 validation 실행
		String infraTableNm = "AM_INFRA";
		if("SW".equals(classType)){
			infraTableNm = "AM_SW";
		}
		// 중복 체크 할 컬럼
		String[] checkColumns = {
				infraTableNm + ".CONF_ID"
		};
		HashMap resultMap = assetExcelRegistService.checkDuplicateAsset(classType, assetList, checkColumns);
		
		// 속성 형식에 대한 공통 validation
		boolean isSuccess = (boolean) resultMap.get("isSuccess");
		if(isSuccess) {
			resultMap = assetExcelRegistService.validateAssetListData(classType, classIdList, assetList);
		} 
		
		
		return resultMap;
	}
	
	/**
	 * 업로드 자산 중복 확인
	 * @param classType
	 * @param assetList
	 * @param resultMap
	 * @return
	 */
	private HashMap checkDuplicateAsset(String classType, List<HashMap> assetList, HashMap resultMap ){
		List duplicateAssetList = new ArrayList();
		boolean isSuccess = true;
		StringBuffer resultMsg = new StringBuffer("중복된 자산이 존재합니다.");
		
		int idx = 0;
		for(HashMap dataMap : assetList) {
			String confId = "";
			String confIdKey = "";
			
			if("SW".equals(classType)){
				confId = (String) dataMap.get("AM_SW.CONF_ID");
				confIdKey = "AM_SW.CONF_ID";
			} else {
				confId = (String) dataMap.get("AM_INFRA.CONF_ID");
				confIdKey = "AM_INFRA.CONF_ID";
			}
			
			if(!"".equals(confId)){
				int duplicateCnt = 0;
				for(HashMap tempMap : assetList) {
					String tempConfId = (String) tempMap.get(confIdKey);
					if(tempConfId.equals(confId)){
						duplicateCnt++;
						if(duplicateCnt > 1){
							break;
						}
					}
				}
				
				if(duplicateCnt > 1){
					
					dataMap.put("IDX", idx);
					dataMap.put("COLUMN", confIdKey);
					
					duplicateAssetList.add(dataMap);
					isSuccess = false;
					resultMsg.append("\n - 구성ID : " + confId);
					break;
				}
				idx++;
				
			}
		}
		
		resultMap.put("isSuccess", isSuccess);
		if(!isSuccess) {
			resultMap.put("resultMsg", resultMsg.toString());
			resultMap.put("resultList", duplicateAssetList);
		}
		
		return resultMap;
	}
	

}


