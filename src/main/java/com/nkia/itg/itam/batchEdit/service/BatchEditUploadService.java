package com.nkia.itg.itam.batchEdit.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface BatchEditUploadService {

	// 자산 일괄수정 처리
	HashMap updateBatchEditAsset(HashMap paramMap) throws NkiaException;

	List selectBatchAssetResultList(HashMap resultMap);

	HashMap validateAssetListData(String classType, List<String> classIdList, List<HashMap> assetList) throws NkiaException, IOException;

}
