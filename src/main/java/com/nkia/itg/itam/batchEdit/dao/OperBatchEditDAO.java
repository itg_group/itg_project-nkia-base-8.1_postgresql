/*
 * @(#)OperOperBatchEditDAO.java              2018. 4. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.batchEdit.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("operBatchEditDAO")
public class OperBatchEditDAO extends EgovComAbstractDAO {
	
	/**
	 * 자산과 매핑된 전체 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAssetOperList(HashMap paramMap) throws NkiaException{
		return list("OperBatchEditDAO.searchAssetOperList", paramMap);
	}
	
	/**
	 * 부서 트리 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchDeptTree(HashMap paramMap) throws NkiaException {
		return list("OperBatchEditDAO.searchDeptTree", paramMap);
	}
	
	/**
	 * 사용자 전체 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAllUserList(HashMap paramMap) throws NkiaException{
		return list("OperBatchEditDAO.searchAllUserList", paramMap);
	}
	
	/**
	 * 사용자 전체 조회 카운트(페이징용)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchAllUserListCount(HashMap paramMap) throws NkiaException{
		return (Integer)selectByPk("OperBatchEditDAO.searchAllUserListCount", paramMap);
	}
	/**
	 * conf id 추출
	 * @param paramMap
	 * @return
	 */
	public List searchConfList(HashMap paramMap) {
		return list("OperBatchEditDAO.searchConfList",paramMap);
	}
	/**
	 * 자산담당자 일괄 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateAssetOper(HashMap paramMap) throws NkiaException{
		update("OperBatchEditDAO.updateAssetOper", paramMap);
	}
	
	/**
	 * 자산담당자 일괄 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetOper(HashMap paramMap) throws NkiaException{
		insert("OperBatchEditDAO.insertAssetOper", paramMap);
	}
	
	/**
	 * 자산담당자 일괄 등록 (머지)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void mergeAssetOper(HashMap paramMap) throws NkiaException{
		update("OperBatchEditDAO.mergeAssetOper", paramMap);
	}
	
	/**
	 * 자산담당자 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteAssetOper(HashMap paramMap) throws NkiaException{
		delete("OperBatchEditDAO.deleteAssetOper", paramMap);
	}
	
	/**
	 * 담당자의 자산목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAllConfIdList(HashMap paramMap) throws NkiaException {
		return list("OperBatchEditDAO.searchAllConfIdList", paramMap);
	}
	/**
	 * 중복 담당자 삭제
	 * @param 
	 * @return
	 * @throws NkiaException
	 */
	public void deleteDistinctOperr(HashMap paramMap) throws NkiaException {
		delete("OperBatchEditDAO.deleteDistinctOperr", paramMap);
	}
	public void insertModifyOper(HashMap paramMap) throws NkiaException {
		delete("OperBatchEditDAO.insertModifyOper", paramMap);
	}
	
	/**
	 * 자산원장 관리자 일괄 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateAssetMainUser(HashMap paramMap) throws NkiaException{
		update("OperBatchEditDAO.updateAssetMainUser", paramMap);
	}
	
	/**
	 * 담당 자산목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAssetListByOper(HashMap paramMap) throws NkiaException {
		return list("OperBatchEditDAO.searchAssetListByOper", paramMap);
	}
}
