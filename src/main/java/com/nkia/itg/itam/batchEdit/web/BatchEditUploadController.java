package com.nkia.itg.itam.batchEdit.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.batchEdit.service.BatchEditUploadService;
import com.nkia.itg.itam.regist.service.AssetExcelRegistService;

@Controller
public class BatchEditUploadController {

	@Resource(name="batchEditUploadService")
	private BatchEditUploadService batchEditUploadService;
	
	@Resource(name="assetExcelRegistService")
	private AssetExcelRegistService assetExcelRegistService;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 수정 대상 자산 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/goBatchEditUpload.do")
	public String goBatchEditUpload(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/batchEdit/batchEditUpload.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산목록 일괄 수정처리
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/updateBatchEditAsset.do")
	public @ResponseBody ResultVO updateBatchEditAsset(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			
			HashMap resultMap = batchEditUploadService.updateBatchEditAsset(paramMap);
			int errorCnt = (Integer) resultMap.get("errorCnt");
			boolean allSuccess = true;
			if(errorCnt > 0){
				allSuccess = false;
			}
			resultMap.put("allSuccess", allSuccess);
			resultVO.setResultMap(resultMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 수정 결과 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/selectBatchAssetResultList.do")
	public @ResponseBody ResultVO selectBatchAssetResultList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List failAssetList = failAssetList = batchEditUploadService.selectBatchAssetResultList(paramMap);
			gridVO.setRows(failAssetList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 수정 결과 엑셀다운
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/selectBatchAssetResultListExcelDown.do")
	public @ResponseBody ResultVO selectBatchResultListExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = (List) param.get("resultList");
			excelMap = excelMaker.makeExcelFileForBatchEdit(excelParam, resultList);

			resultVO.setResultMap(excelMap);
	
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}

	/**
	 * 엑셀파일 파싱
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/parsingExcelTemplateData.do")
	public @ResponseBody ResultVO parsingExcelTemplateData(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		HashMap resultMap = new HashMap();
		try {
			// 업로드된 파일 정보 조회
			AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO();
			atchFileDetailVO.setAtch_file_id((String) paramMap.get("atch_file_id"));
			atchFileDetailVO.setFile_idx("SINGLE_FILE");
			atchFileDetailVO = atchFileService.selectAtchFileDetail(atchFileDetailVO);
			String extsn = atchFileDetailVO.getFile_extsn();
			
			if("xlsx".equals(extsn)){
				resultMap = assetExcelRegistService.parsingAssetExcelForXlsx(
						paramMap,
						atchFileDetailVO.getStore_file_name(),
						atchFileDetailVO.getOrignl_file_name(),
						atchFileDetailVO.getFile_store_cours(),
						atchFileDetailVO.getFile_extsn(),
						atchFileDetailVO.getFile_content_type()
						);
			}else if("xls".equals(extsn)){
				resultMap = assetExcelRegistService.parsingAssetExcelForXls(
						paramMap,
						atchFileDetailVO.getStore_file_name(),
						atchFileDetailVO.getOrignl_file_name(),
						atchFileDetailVO.getFile_store_cours(),
						atchFileDetailVO.getFile_extsn(),
						atchFileDetailVO.getFile_content_type()
						);
			}
			resultVO.setSuccess(true);
			resultVO.setResultMsg("업로드가 완료되었습니다. <br>검증을 실행하여 주십시오.</br>");
		} catch (Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg("업로드 처리 중 오류가 발생되었습니다. <br>첨부파일을 확인하여 주십시오.</br>");
		} finally {
			resultVO.setResultMap(resultMap);
			GridVO gridVO = new  GridVO();
			List resultList = (List) resultMap.get("resultList");
			if(resultList != null) {
				gridVO.setRows(resultList);
				gridVO.setTotalCount(resultList.size());
			}
			resultVO.setGridVO(gridVO);
		}
			
		return resultVO;
	}
	
	
	/**
	 * 데이터 검증
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/batchEdit/validateBatchEditData.do")
	public @ResponseBody ResultVO validateBatchEditData(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			String classType = (String) paramMap.get("class_type");
			List classIdList = (List) paramMap.get("class_id_list");
			List assetList = (List) paramMap.get("assetList");
			
			HashMap resultMap = batchEditUploadService.validateAssetListData(classType, classIdList, assetList);
			
			resultVO.setResultMap(resultMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}


