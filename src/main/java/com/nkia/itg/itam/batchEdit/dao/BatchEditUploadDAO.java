package com.nkia.itg.itam.batchEdit.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("batchEditUploadDAO")
public class BatchEditUploadDAO extends EgovComAbstractDAO {

	public String createBatchHisSeq() {
		return (String) selectByPk("BatchEditUploadDAO.createBatchHisSeq", null);
	}

	public void insertAmChgHisBatch(HashMap paramData) {
		insert("BatchEditUploadDAO.insertAmChgHisBatch", paramData);
	}

	public void insertAmChgHisBatchAsset(HashMap batchAssetParam) {
		insert("BatchEditUploadDAO.insertAmChgHisBatchAsset", batchAssetParam);
	}

	public void executeUpdQuery(String queryString) {
		update("BatchEditUploadDAO.executeUpdQuery", queryString);
	}

	public List selectBatchAssetResultList(HashMap paramMap) {
		return list("BatchEditUploadDAO.selectBatchAssetResultList", paramMap);
	}

	public void updateInfraUpdDt(HashMap paramMap) {
		update("BatchEditUploadDAO.updateInfraUpdDt", paramMap);
	}

	public void updateSwUpdDt(HashMap paramMap) {
		update("BatchEditUploadDAO.updateSwUpdDt", paramMap);
	}


	
}
