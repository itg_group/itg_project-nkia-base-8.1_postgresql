/*
 * @(#)BatchEditServiceImpl.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.batchEdit.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.automation.ui.WebixUIHandler;
import com.nkia.itg.itam.batchEdit.dao.BatchEditDAO;
import com.nkia.itg.itam.batchEdit.service.BatchEditService;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;
import com.nkia.itg.itam.opms.service.OpmsDetailService;
import com.nkia.itg.itam.opms.util.OpmsUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("batchEditService")
public class BatchEditServiceImpl implements BatchEditService {
	
	private static final Logger logger = LoggerFactory.getLogger(BatchEditServiceImpl.class);
	
	@Resource(name = "batchEditDAO")
	public BatchEditDAO batchEditDAO;
	
	@Resource(name = "opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;
	
	@Resource(name = "opmsDetailService")
	private OpmsDetailService opmsDetailService;
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	@Resource(name = "opmsUtil")
	private OpmsUtil opmsUtil;
	
	@Resource(name = "webixUIHandler")
	private WebixUIHandler webixUIHandler;
	
	public List selectClassTypeClassName(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return batchEditDAO.selectClassTypeClassName(paramMap);
	}
		
	public List searchBatchEditAssetPop(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return batchEditDAO.searchBatchEditAssetPop(paramMap);
	}

	public int searchBatchEditAssetPopCount(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return batchEditDAO.searchBatchEditAssetPopCount(paramMap);
	}
	
	public List searchBatchEditUpAssetList(Map paramMap) throws NkiaException {
		
		String editType = (String)paramMap.get("editType");
		
		// 문자열로 넘어온 구성ID 목록을 split하여 리스트로 만들어 준다.
		String confIds = (String)paramMap.get("confIdArr");
		String[] confIdStr = confIds.split("@-@");
		String confIdsParam = "";
		
		confIdsParam = "'" + confIdStr[0] + "'";
		for(int i=1; i<confIdStr.length; i++) {
			confIdsParam += ", '" + confIdStr[i] + "'";
		}
		
		paramMap.put("conf_id_list", confIdsParam);
		
		// 수정타입이 분류체계별 수정이라면, 
		// 각 속성의 테이블명이 다르므로 테이블명과 속성명을 맵핑하기 위해 각각 리스트로 만들어 준다
		if("CLASS".equals(editType)) {
			
			List colIdList = new ArrayList();
			List tblNmList = new ArrayList();
			
			String colIdListStr = (String)paramMap.get("colIdListStr");
			String tblNmListStr = (String)paramMap.get("tblNmListStr");
			
			colIdListStr = (colIdListStr.replaceAll("\\[", "")).replaceAll("\\]", "");
			tblNmListStr = (tblNmListStr.replaceAll("\\[", "")).replaceAll("\\]", "");
			
			String[] colIdStr = colIdListStr.split(", ");
			String[] tblNmStr = tblNmListStr.split(", ");
			
			for(int i=0; i<colIdStr.length; i++) {
				colIdList.add(colIdStr[i]);
				tblNmList.add(tblNmStr[i]);
			}
			
			paramMap.put("colIdList", colIdList);
			paramMap.put("tblNmList", tblNmList);
		}
		
		return batchEditDAO.searchBatchEditUpAssetList(paramMap);
	}
	
	
	public List searchBatchEditAttrList(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = new ArrayList();
		List attrList = new ArrayList();
		List compList = new ArrayList();
		
		if("COMMON".equals(paramMap.get("edit_type"))) {
			attrList = batchEditDAO.searchBatchEditCommonAttrList(paramMap);
		} else if("CLASS".equals(paramMap.get("edit_type"))) {
			attrList = batchEditDAO.searchBatchEditClassAttrList(paramMap);
			// 컴포넌트 세팅쿼리, POC버전에서는 일단 막음
			//compList = batchEditDAO.searchBatchEditComponentAttrList(paramMap);
		} else {
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			attrList = new ArrayList();
		}
		
		// 리턴할 속성 리스트에 먼저 컴포넌트 리스트를 우선 세팅한다.
		for(int i=0; i<compList.size(); i++) {
			resultList.add(compList.get(i));
		}
		
		if (attrList != null && attrList.size() > 0) {
			// 컴포넌트가 다 세팅되면 (쿼리상에서 우선순위가 젤 위로 정렬된) 일반 속성 리스트를 세팅한다.
			for(int j=0; j<attrList.size(); j++) {
				resultList.add(attrList.get(j));
			}
		}
		
		return resultList;
	}
	
	public List searchBatchEditAssetAttrList(Map paramMap) throws NkiaException {
		
		//List confIdList = new ArrayList();
		List colIdList = new ArrayList();
		List colNmList = new ArrayList();
		List colHtmlList = new ArrayList();
		List colViewIdList = new ArrayList();
		List tblNmList = new ArrayList();
		
		String colTbls = (String)paramMap.get("colTbls");
		String confIds = (String)paramMap.get("confIds");
		
		int query_count = 0;
		
		String[] strColTbls = colTbls.split("@\\^@"); // [0]col-tbl [1]col-tbl ...
		String query = "";
		for(int i=0; i<strColTbls.length; i++) {
			ModelMap colIdMap = new ModelMap();
			String[] colTbl = strColTbls[i].split("@-@"); // [0]col [1]tbl
			//if(!"MANAGER@-@AM_OPER".equals(strColTbls[i])) {
				if(query_count==0) {
					query += "AND ( (A.COL_ID='" + colTbl[0] + "' AND B.GEN_TABLE_NM='" + colTbl[1] + "')";
				} else {
					query += "\n\tOR (A.COL_ID='" + colTbl[0] + "' AND B.GEN_TABLE_NM='" + colTbl[1] + "')";
				}
				colIdList.add(colTbl[0]);
				tblNmList.add(colTbl[1]);
				colIdMap.put("col_id", colTbl[0]);
				colNmList.add(selectBatchEditColName(colIdMap));
				colHtmlList.add(selectBatchEditColHtmlType(colIdMap));
				
				query_count++;
			//}
		}
		query += ")";
		// 쿼리 = AND ( (조건1) OR (조건2) OR ... )
		
		// 화면에 보여주기 위한 값이 들어있는 컬럼명 세팅 (콤보박스나 팝업)
		for(int j=0; j<colIdList.size(); j++) {
			String j_col_id = (String)colIdList.get(j);
			String j_col_html = (String)colHtmlList.get(j);
			String j_col_view_id = "";
			
			if(j_col_html.equals("SELECT") || j_col_html.equals("POPUP")){
				j_col_view_id = ( j_col_id + "_NM" );
			}else{
				j_col_view_id = j_col_id;
			}
			colViewIdList.add(j_col_view_id);
		}
		
		paramMap.put("class_select_query", query);
		paramMap.put("colIdList", colIdList);
		paramMap.put("colViewIdList", colViewIdList);
		paramMap.put("tblNmList", tblNmList);
		paramMap.put("colNmList", colNmList);
		paramMap.put("colTblLength", strColTbls.length);
		paramMap.put("confIdList",confIds);
		
		List resultList = new ArrayList();
		resultList = batchEditDAO.searchBatchEditAssetAttrList(paramMap);
		
		return resultList;
	}
	
	public String selectBatchEditColName(Map paramMap) throws NkiaException {
		return batchEditDAO.selectBatchEditColName(paramMap);
	}
	
	public String selectBatchEditColHtmlType(Map paramMap) throws NkiaException {
		return batchEditDAO.selectBatchEditColHtmlType(paramMap);
	}
	
	@Override
	public Map selectClassEntitySetupInfo(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		paramMap.put("level", "2");
		HashMap entityMap 		= new HashMap();
		JSONArray tabArray		= new JSONArray();
		JSONArray formArray		= new JSONArray();
		JSONArray dataArray		= new JSONArray();
		String result = "";
		
		entityMap.put("COL_SIZE", "2");
		entityMap.put("ENTITY_ID", "batchEditData");
		entityMap.put("ENTITY_NM", "속성 목록");
		
		// 해당 그룹에 관련된 속성리스트
		List<HashMap> attrList = (List)paramMap.get("attrList");
		if(attrList != null && attrList.size()>0){
//			result = opmsUtil.createEditorFormComp(attrList,null,entityMap,formArray,null);
			result = webixUIHandler.createEditorFormJsonString(attrList, new ArrayList<HashMap>(), entityMap, formArray, null);
		}
		
		if(!"".equals(result)){
			//탭 array add
			tabArray.add(result);
		}
		
		boolean isManagerComp = (Boolean)paramMap.get("isManagerComp");
		  
		if(isManagerComp) {
			
			HashMap mngMap = new HashMap();
			
			mngMap.put("ENTITY_ID", "managerListGrid");
			mngMap.put("ENTITY_NM", "운영자 정보");
			mngMap.put("AUTH_TYPE", "U");
			String mngResult = opmsUtil.createManagerList(mngMap);
			
			JSONObject editorIdsObj = new JSONObject();

			editorIdsObj.put("entity_id", "managerListGrid");
			editorIdsObj.put("comp_id", "managerListGrid");
			editorIdsObj.put("up_id", null);
			editorIdsObj.put("type", "SL");
			editorIdsObj.put("entity_type", "COMPONENT");

			formArray.add(editorIdsObj); 
			
			if(!"".equals(mngResult)){
				//탭 array add
				tabArray.add(mngResult);
			}
		}
		
		//tab 정보
		paramMap.put("tabItem"		, opmsUtil.replaceAllquotes(tabArray.toString()));
		//데이터 id array
		paramMap.put("formItemIds"	, formArray.toString());
		//form 데이터 array
		paramMap.put("formData"		, dataArray.toString());
		//수정 권한 갯수
		//paramMap.put("authCnt"		, authCnt);
		
		return paramMap;
	}
	
	
	public void updateBatchEditCommonAttr(Map paramMap) throws NkiaException {
		
		// 자산ID 리스트
		List assetIdList = (List)paramMap.get("assetIdList");
		// 구성ID 리스트
		List confIdList = (List)paramMap.get("confIdList");
		// 분류체계ID 리스트
		List classIdList = (List)paramMap.get("classIdList");
				
		// 속성ID 리스트
		List colIdList = (List)paramMap.get("colIdList");
		
		String updateQuery = "";
		int queryCount = 0;
		HashMap newDataMap = new HashMap();
		
		// paramMap : (K)속성ID(컬럼명) / (V)속성값(데이터)
		for(int i=0; i<colIdList.size(); i++) {
			
			// 만약 i번째에 있는 param값(속성값)이 공백이라면
			// 업데이트문을 수행하지 않기위해 Query-String을 만들지 않고 다음 속성으로 점프한다.
			// (수정)=>> 공백입력 시 공백으로 업데이트 되야하기 때문에 일단 주석처리
			//if("".equals( ((String)paramMap.get((String)colIdList.get(i))).trim() )) {
			//	i++;
			//} else {
				// 속성값이 있다면 예)ASSET_ID='AT01SV0001' 과 같은 문장을 만들고 queryCount를 1증가시킨다.
				String colId = (String)colIdList.get(i);
				if(i == 0) {
					updateQuery = colId + "=" + "'" + paramMap.get(colId)+ "'";
				} else {
					updateQuery += ", " + colId + "=" + "'" + paramMap.get(colId)+ "'";
				}
				queryCount++;
				
				// 변경되는 속성을 이력을 위해 별도의 Map에 담아준다.
				newDataMap.put(colId, paramMap.get(colId));
			//}
		}
		
		// 만들어진 쿼리가 있다면
		if(queryCount > 0) {
			
			// 만들어진 쿼리문을 paramMap에 세팅 후
			paramMap.put("common_update_query", updateQuery);
			
			// 선택한 자산의 수만큼 반복하면서
			for(int i=0; i<assetIdList.size(); i++) {
				
				String asset_id = (String)assetIdList.get(i);
				String conf_id = (String)confIdList.get(i);
				String class_id = (String)classIdList.get(i);

				// 일괄수정 이력 등록
				assetHistoryService.insertAssetAttrHistory(conf_id, newDataMap,"일괄수정",(String)paramMap.get("upd_user_id"));
				
				// 선택된 복수의 자산 하나하나의 속성을 업데이트 한다.
				paramMap.put("asset_id", asset_id);
				batchEditDAO.updateBatchEditCommonAttr(paramMap);
				
			}
		}
	}
	
	
	public void updateBatchEditClassAttr(Map paramMap) throws NkiaException {
		
		/*
		 * 테이블 명과 속성명을 맵핑 해주는 부분 시작
		 * 
		 * EX) 넘어오는 paramMap data
		 *   = colIdList{ASSET_ID, INTRO_DT, DIV_HIERACHY, CONF_NM, CPU_CNT, USE_YN ...}
		 *     tblNmList{AM_ASSET, AM_ASSET, AM_ASSET AM_INFRA, AM_SV_PHYSI, AM_SV_PHYSI ...}
		 * 
		 * 맵핑 후 data
		 *   = Map["AM_ASSET", "ASSET_ID-INTRO_DT-DIV_HIERACHY"]
		 *        ["AM_INFRA", "CONF_NM"]
		 *        ["AM_SV_PHYSI", "CPU_CNT-USE_YN"]
		 *  @ 테이블명에 맵핑되는 속성목록은 핸들링을 편하게 하기위해 '-'로 구분되는 일련의 String으로 넣어준다
		 */
		HashMap tblMapping = new HashMap();
		
		List tblList = (List)paramMap.get("tblNmList");
		List colList = (List)paramMap.get("colIdList");
		// 테이블명 리스트로부터 중복을 제거하여 새로 담아둘 리스트
		List newSetTblList = new ArrayList();
		// 컴포넌트 목록의 테이블명만 따로 담아둘 리스트
		List compTblList = new ArrayList();
		HashMap newDataMap = new HashMap();
		
		for(int i=0; i<tblList.size(); i++) {
			
			String tblNm = (String)tblList.get(i);
			
			// 테이블명이 AM_OPER 라면 컬럼 리스트를 만들지 않고 컴포넌트 테이블 리스트에 따로 추가해둠
			// (다른 컴포넌트들이 생길 시, 여기에 OR 조건으로 추가하면 됨)
			if(tblNm.equals("AM_OPER") /* || tblNm.equals("AM_CONF_REL") || ... */) {
				compTblList.add(tblNm);
			} else {
				if(tblMapping.get(tblNm) == null) {
					tblMapping.put(tblNm, colList.get(i));
					newSetTblList.add(tblNm);
				} else {
					String cols = tblMapping.get(tblNm) + "@-@" + colList.get(i);
					tblMapping.put(tblNm, cols);
				}
			}
		}
		/*
		 * 테이블 명과 속성명을 맵핑 해주는 부분 끝
		 */
		
		
		/*
		 * 동적으로 쿼리 생성해주는 부분 시작
		 */
		// 자산ID 리스트
		List assetIdList = (List)paramMap.get("assetIdList");
		// 구성ID 리스트
		List confIdList = (List)paramMap.get("confIdList");
		// 분류체계ID 리스트
		List classIdList = (List)paramMap.get("classIdList");
		
		/***** i = 선택한(업데이트 할) 자산의 수 *****/
		for(int i=0; i<confIdList.size(); i++) {
			
			/*************** 일반 속성 이력 추가 시작 ***************/
			String asset_id = (String)assetIdList.get(i);
			String conf_id = (String)confIdList.get(i);
			String class_id = (String)classIdList.get(i);
			
			// 메뉴얼 속성(기본 속성) 쿼리 생성 및 쿼리 수행 시작
			/***** m = 선택된 속성들의 테이블명 리스트로부터 중복을 제거한 리스트
					(업데이트 할 테이블의 수) *****/
			for(int m=0; m<newSetTblList.size(); m++) {
				
				String sql = "";
				String tableNm = "";
				String setQuery = "";
				String whereQuery = "";
				
				// 테이블 명을 차례대로 가져와 해당 테이블명에 맵핑되어 있는 속성을 리스트로 만든다
				tableNm = (String)newSetTblList.get(m);
				String[] arrCol = ((String)tblMapping.get(tableNm)).split("@-@");
				
				/***** s = 선택한(업데이트 할) 속성의 수 *****/
				for(int s=0; s<arrCol.length; s++) {
					if(s == 0) {
						setQuery = arrCol[s] + "='" + paramMap.get(arrCol[s]) + "'";
					} else {
						setQuery += "\n, " + arrCol[s] + "='" + paramMap.get(arrCol[s]) + "'";
					}
					// EX) setQuery RETURN : ASSET_ID='AT01SV00001', CONF_NM='자산명' ...
					
					// 변경되는 속성을 이력을 위해 별도의 Map에 담아준다.
					newDataMap.put(arrCol[s], paramMap.get(arrCol[s]));
				}
				
				// 테이블이 AM_ASSET 이면 WHERE 조건을 ASSET_ID 로 줘야하고
				// 그 외의 테이블일 경우 CONF_ID 로 업데이트 해야하기때문에 분기로 나눔
				if(tableNm.equals("AM_ASSET")) {
					whereQuery = "ASSET_ID = '" + assetIdList.get(i) + "'";
				} else {
					whereQuery = "CONF_ID = '" + confIdList.get(i) + "'";
				}
				
				// 최종적으로 쿼리문을 만들어줘서 반복해서 업데이트 함
				sql = "UPDATE \n" + tableNm + "\n"
						+ "SET \n" + setQuery + "\n"
						+ "WHERE \n" + whereQuery;
				
				paramMap.put("class_update_query", sql);
				batchEditDAO.updateBatchEditClassAttr(paramMap);
			}
			
			// 컴포넌트 속성 쿼리 생성 및 쿼리 수행 시작
			/***** c = 선택한 컴포넌트들이 insert 될 테이블 리스트 *****/
			for(int c=0; c<compTblList.size(); c++) {
				
				String sql = "";
				int seq = 1;
				String tableNm = "";
				HashMap operMap = new HashMap();
				
				tableNm = (String)compTblList.get(c);
				
				// 운영자(담당자) 컴포넌트 일 경우
				if(tableNm.equals("AM_OPER")) {
					List<HashMap> compRowList = (List)paramMap.get(tableNm);
					/*************** 담당자 이력 정보 추가  ***************/
					
					// 만약 운영자(담당자) 컴포넌트(그리드)에 값이 없으면 해당 구성ID의 담당자 정보를 모두 삭제한다.
					// (Null값 입력시에는 해당정보가 Null로 업데이트 되므로)
					if((Integer)paramMap.get("MNG_COMP_LENGTH") == 0) {
						
						operMap.put("conf_id", confIdList.get(i));
						opmsDetailDAO.deleteOper(operMap);
						
					} else {
						
						// 만약 운영자(담당자) 컴포넌트(그리드)에 값이 있으면
						// 입력전에 일단 데이터 삭제부터 하고
						operMap.put("conf_id", confIdList.get(i));
						opmsDetailDAO.deleteOper(operMap);
						
						// 운영자 컴포넌트 그리드의 Row만큼 반복문을 수행하며 신규데이터를 AM_OPER 테이블에 넣는다
						for(int y=0; y<compRowList.size(); y++) {
							
							HashMap gridRowData = (HashMap)compRowList.get(y);
							
							operMap.put("SEQ", seq);
							operMap.put("ASSET_ID", assetIdList.get(i));
							operMap.put("CONF_ID", confIdList.get(i));
							operMap.put("CUST_ID", gridRowData.get("CUST_ID"));
							operMap.put("USER_ID", gridRowData.get("USER_ID"));
							operMap.put("UPD_USER_ID", paramMap.get("upd_user_id"));
							
							if( "Y".equals( gridRowData.get("MAIN_YN").toString() ) ){
								operMap.put("MAIN_YN", "Y");
								// AM_ASSET 테이블의 운영부서를 -> 담당자 '정/부' 중 '정'인 담당자의 부서코드로 업데이트 한다.
								// batchEditDAO.updateAssetCustId(operMap); // AM_ASSET 테이블의 ASSET_CUST_ID 컬럼을 사용할 경우 사용
							}else{
								operMap.put("MAIN_YN", "N");
							}
							
							seq++;
							
							opmsDetailDAO.insertOper(operMap);
							
						}
					}
				}
				// 운영자(담당자) 컴포넌트 수행 끝
				
				// 다른 컴포넌트 추가시 여기 if문 추가 (Ex: 연관장비)
				/*
				 if(tableNm.equals("CONF_REL")) {
				 	로직
				 	...
				 }
				 */
			}
			
			// 일괄수정 이력 등록
			assetHistoryService.insertAssetAttrHistory(conf_id, newDataMap,"일괄수정",(String)paramMap.get("upd_user_id"));
			// 컴포넌트 속성 쿼리 생성 및 쿼리 수행 끝
		}
		/*
		 * 동적으로 쿼리 생성해주는 부분 끝
		 */
	}
	
}
