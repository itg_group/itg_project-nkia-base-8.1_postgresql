package com.nkia.itg.itam.batchEdit.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.itam.batchEdit.service.BatchEditDownService;
import com.nkia.itg.itam.batchEdit.dao.BatchEditDownDAO;

@Service("batchEditDownService")
public class BatchEditDownServiceImpl implements BatchEditDownService{

	@Resource(name="batchEditDownDAO")
	private BatchEditDownDAO batchEditDownDAO;

	@Override
	public List selectEditAttrList(HashMap paramMap) {
		return batchEditDownDAO.selectEditAttrList(paramMap);
	}

	/**
	 * 일괄자산 수정 엑셀파일 조회용 자산 목록 조회
	 */
	@Override
	public List searchAssetListForExcel(Map param) {
		return batchEditDownDAO.searchAssetListForExcel(param);
	}

}
