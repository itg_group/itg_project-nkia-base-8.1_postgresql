/*
 * @(#)BatchEditService.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.batchEdit.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface BatchEditService {
	
	public List selectClassTypeClassName(Map paramMap) throws NkiaException;
	
	public List searchBatchEditAssetPop(Map paramMap) throws NkiaException;

	public int searchBatchEditAssetPopCount(Map paramMap) throws NkiaException;
	
	public List searchBatchEditUpAssetList(Map paramMap) throws NkiaException;
	
	public List searchBatchEditAttrList(Map paramMap) throws NkiaException;

	public List searchBatchEditAssetAttrList(Map paramMap) throws NkiaException;
	
	public Map selectClassEntitySetupInfo(Map paramMap) throws NkiaException;
	
	public void updateBatchEditCommonAttr(Map paramMap) throws NkiaException;
	
	public void updateBatchEditClassAttr(Map paramMap) throws NkiaException;
	
	public String selectBatchEditColName(Map paramMap) throws NkiaException;
	
}
