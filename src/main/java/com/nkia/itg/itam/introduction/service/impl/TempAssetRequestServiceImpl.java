package com.nkia.itg.itam.introduction.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.assetHis.dao.AssetHistoryDAO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.automation.dao.AssetAutomationDAO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.itam.introduction.dao.TempAssetDAO;
import com.nkia.itg.itam.introduction.dao.TempAssetRequestDAO;
import com.nkia.itg.itam.introduction.service.TempAssetRequestService;
import com.nkia.itg.itam.introduction.service.TempAssetService;
import com.nkia.itg.itam.opms.dao.OpmsDetailDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("tempAssetRequestService")
public class TempAssetRequestServiceImpl implements TempAssetRequestService{
	
	public static final String comma = ",";
	
	@Resource(name="tempAssetRequestDAO")
	private TempAssetRequestDAO tempAssetRequestDAO;
	
	@Resource(name="tempAssetDAO")
	private TempAssetDAO tempAssetDAO;
	
	@Resource(name="assetAutomationService")
	private AssetAutomationService assetAutomationService;
	
	@Resource(name="tempAssetService")
	private TempAssetService tempAssetService;

	@Resource(name = "opmsDetailDAO")
	private OpmsDetailDAO opmsDetailDAO;
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	@Resource(name = "assetHistoryDAO")
	private AssetHistoryDAO assetHistoryDAO;
	
	@Resource(name = "assetAutomationDAO")
	private AssetAutomationDAO assetAutomationDAO;
	
	/**
	 * 입고요청 처리
	 * 자산 등록
	 * @param dataMap
	 * @throws NkiaException
	 */
	@Override
	public void insertOpmsInfoData(HashMap paramMap) throws NkiaException{
		
		String user_id = (String)paramMap.get("user_id");
		String class_id = (String) paramMap.get("CLASS_ID");		
		String class_type = (String) paramMap.get("CLASS_TYPE");
		paramMap.put("class_type", class_type);
		
		// 신규 자산/구성 ID 생성
		HashMap paramData = new HashMap();
		paramData.put("class_id", class_id);
		paramData.put("class_type", class_type);
		String asset_id = (String) assetAutomationService.createAssetId(paramData);
		String conf_id = (String) assetAutomationService.createConfId(paramData);
		
		List<HashMap> tableList = tempAssetRequestDAO.selectTableList(paramMap);
		
		HashMap subParams = new HashMap();
		subParams.put("asset_id", asset_id);
		
		for(int i=0; i<tableList.size(); i++){
			HashMap tableData = tableList.get(i);
			String tableName = (String) tableData.get("TABLE_NM");
			paramMap.put("table_nm", tableName);
			List<HashMap> colList = tempAssetRequestDAO.selectColList(paramMap);			
			
			/** 속성정보 이력데이터 등록 **/
			HashMap dataMap = new HashMap();			
			for(int j=0; j<colList.size(); j++){
				HashMap col_val = colList.get(j);
				dataMap.put(col_val.get("COLUMN_ID"), col_val.get("COL_VALUE"));
			}
//			assetHistoryService.insertAssetAttrHistory(conf_id, dataMap, "정보수집서 승인", user_id);
			
			/**  여기부터 쿼리 만들기 **/
			StringBuffer querySb = new StringBuffer();
			StringBuffer colSb = new StringBuffer();
			StringBuffer insSb = new StringBuffer();
			
			String tableNm = tableName;
			String confId = conf_id;
			boolean confIdFlag = false;
			
			for(int j=0;j<colList.size();j++){
				HashMap installColMap = colList.get(j);
				String colId = StringUtil.parseString(installColMap.get("col_id"));
				if("CONF_ID".equals(colId)){
					confIdFlag = true;
					break;
				}
			}
			if("AM_ASSET".equals(tableNm)){
				colSb.append("ASSET_ID"); 
				insSb.append("'"+asset_id+"'"); 
				
				//등록자
				colSb.append(comma + "INS_USER_ID"); 
				insSb.append(comma + "'"+user_id+"'");
				//등록일
				colSb.append(comma + "INS_DT"); 
				insSb.append(comma + "SYSDATE");
			}else if("AM_ASSET_SUB".equals(tableNm)){
				colSb.append("ASSET_ID"); 
				insSb.append("'"+asset_id+"'");
			}else{
				if(!confIdFlag){
					if("SV".equals(class_type)){
						if("AM_INFRA".equals(tableNm)){
							colSb.append("CONF_ID" + comma + "ASSET_ID"+ comma + "CLASS_TYPE" + comma + "TANGIBLE_ASSET_YN" + "\n"); 
							insSb.append("'"+confId+"'" + comma +"'"+asset_id+"'"+ comma +"'"+class_type+"'"+ comma + "'Y'" +"\n"); 	
						}else{
							colSb.append("CONF_ID"); 
							insSb.append("'"+confId+"'");
						}						
					}else if("SW".equals(class_type)){
						if("AM_SW".equals(tableNm)){
							colSb.append("CONF_ID" + comma + "ASSET_ID"+ comma + "CLASS_TYPE" + comma + "TANGIBLE_ASSET_YN" + "\n"); 
							insSb.append("'"+confId+"'" + comma +"'"+asset_id+"'"+ comma +"'"+class_type+"'"+ comma + "'Y'" +"\n"); 	
						}else{
							colSb.append("CONF_ID"); 
							insSb.append("'"+confId+"'");
						}
					}else if("DC".equals(class_type)){
						if("AM_DC".equals(tableNm)){
							colSb.append("CONF_ID" + comma + "ASSET_ID"+ comma + "CLASS_TYPE" + comma + "TANGIBLE_ASSET_YN" + "\n"); 
							insSb.append("'"+confId+"'" + comma +"'"+asset_id+"'"+ comma +"'"+class_type+"'"+ comma + "'Y'" +"\n"); 	
						}else{
							colSb.append("CONF_ID"); 
							insSb.append("'"+confId+"'");
						}
					}else{
						if("AM_INFRA".equals(tableNm)){
							colSb.append("CONF_ID" + comma + "ASSET_ID"+ comma + "TANGIBLE_ASSET_YN"+ comma + "CLASS_TYPE" + "\n"); 
							insSb.append("'"+confId+"'" + comma +"'"+asset_id+"'"+ comma +"'Y'"+ comma +"'"+class_type+"'"+ "\n"); 	
						}else{
							colSb.append("CONF_ID"); 
							insSb.append("'"+confId+"'");
						}
					}	
				}
			}
			
			int roopCnt = 0;
			for(int j=0;j<colList.size();j++){
				
				HashMap installColMap = colList.get(j);
				String colId = StringUtil.parseString(installColMap.get("COLUMN_ID"));
				String colVal = StringUtil.parseString(installColMap.get("COL_VALUE"));
				
				if(colId != null && !"".equals(colId)){
					if(!confIdFlag){
						colSb.append(comma);
						insSb.append(comma); 
					}else{
						if(i>0){
							colSb.append(comma);
							insSb.append(comma); 
						}
					}
					
					colSb.append(colId + "\n");
					//명칭 > CODE 변환 ST
					if("AM_ASSET".equals(tableNm)){
						insSb.append("'"+colVal+"'\n");
					}else if("AM_INFRA".equals(tableNm)){
						if("CONF_LEVEL".equals(colId)){
							//등급(중요도)
							colVal = "FC_GET_CODE_ID('INCDNT_GRAD_CD', '"+colVal+"')";
							insSb.append(colVal+"\n");
						}else{
							insSb.append("'"+colVal+"'\n");
						}
					}else{
						insSb.append("'"+colVal+"'\n");
					}
					//END
				}
			}
			
			querySb.append("INSERT INTO " + tableNm + "( \n").append(colSb).append(") VALUES (").append(insSb + ")");
			opmsDetailDAO.excuteCudQuery(querySb.toString());
			
		}
	
		
		// 담당자 테이블 데이터 이관
		List<HashMap> operDataList = tempAssetRequestDAO.selectOperDataList(paramMap);
		if(!operDataList.isEmpty()) {
			for(int i=0; i<operDataList.size(); i++){
				HashMap serviceData = operDataList.get(i);
				serviceData.put("CONF_ID",conf_id);
				tempAssetRequestDAO.insertOperData(serviceData);
			}
			// 이력데이터 등록
//			assetHistoryService.insertOperCompHistory(conf_id, operDataList, "정보수집서 승인", user_id);
		}
		
		// 연관서비스 테이블 데이터 이관
		List<HashMap> serviceDataList = tempAssetRequestDAO.selectServiceDataList(paramMap);
		if(!serviceDataList.isEmpty()) {
			for(int i=0; i<serviceDataList.size(); i++){
				HashMap serviceData = serviceDataList.get(i);
				serviceData.put("CONF_ID",conf_id);
				tempAssetRequestDAO.insertServiceData(serviceData);
			}
			// 이력데이터 등록
//			assetHistoryService.insertRelServiceCompHistory(conf_id, serviceDataList, "정보수집서 승인", user_id);
		}
		
		// 연계 장비 데이터 이관
		List<HashMap> rel_confDataList = tempAssetRequestDAO.selectRelConfDataList(paramMap);
		if(!rel_confDataList.isEmpty()){
			List<HashMap> conf_relDataList = tempAssetRequestDAO.selectConfRelDataList(paramMap);
			
			// 이력 시퀀스 생성
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			
			for(int i=0; i<rel_confDataList.size(); i++){
				HashMap rel_confData = rel_confDataList.get(i);
				rel_confData.put("CONF_ID",conf_id);
				tempAssetRequestDAO.insertRelData(rel_confData);
				
				// 이력데이터 등록
//				rel_confData.put("SEQ", chgSeq);
//				rel_confData.put("REL_CLASS_TYPE", rel_confData.get("CONF_TYPE"));
//				rel_confData.put("CHG_MESSAGE", "정보수집서 승인");
//				rel_confData.put("INS_USER_ID", user_id);
//				assetHistoryDAO.insertRelInfraCompHistory(rel_confData);
			}
			for(int i=0; i<conf_relDataList.size(); i++){
				HashMap conf_relData = conf_relDataList.get(i);
				conf_relData.put("CONF_ID",conf_relData.get("CONF_TEMP_ID"));
				conf_relData.put("REL_CONF_ID", conf_id);
				tempAssetRequestDAO.insertRelData(conf_relData);
				
				// 이력데이터 등록
//				conf_relData.put("SEQ", chgSeq);
//				conf_relData.put("REL_CLASS_TYPE", class_type);
//				conf_relData.put("CHG_MESSAGE", "정보수집서 승인");
//				conf_relData.put("INS_USER_ID", user_id);
//				assetHistoryDAO.insertRelInfraCompHistory(conf_relData);
			}
		}
		
		// 설치정보 데이터 이관 시작
		List<HashMap> installTableList = tempAssetRequestDAO.searchOpmsInstallTableInfo(paramMap);
		for(int i=0; i<installTableList.size(); i++){
			HashMap installInfo = installTableList.get(i);
			String table_name = (String) installInfo.get("TABLE_NM");
			String info_table_name = (String) installInfo.get("INFO_TABLE_NM");
			String comp_id = (String) installInfo.get("COMP_ID");
			installInfo.put("CONF_ID", conf_id);
			installInfo.put("CONF_TEMP_ID", paramMap.get("CONF_TEMP_ID"));
			int installCnt = tempAssetRequestDAO.selectInstallInfoCnt(installInfo);
			
			if(installCnt > 0){
				List<HashMap> installData = tempAssetRequestDAO.selectInstallInfoData(installInfo);
				for(int j=0; j<installData.size(); j++){
					HashMap datas = installData.get(j);
					datas.put("CONF_ID", conf_id);
					datas.put("COMP_ID", comp_id);
					tempAssetRequestDAO.insertInstallInfoData(datas);
				}
			}
		}

		// 임시테이블 등록상태 변경 (성공)
		paramMap.put("asset_id", asset_id);	//신규 자산 id
		paramMap.put("conf_id", conf_id);	//신규 conf id
		paramMap.put("status", "SUCCESS");
		tempAssetService.updateAmTempMapping(paramMap);
		
		// 임시자산 데이터 삭제(cmdb등록시 임시자산 삭제)
//		tempAssetService.deleteAmTempData(paramMap);
//		tempAssetService.deleteAmTempMapping(paramMap);
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramMap.put("user_id", userVO.getUser_id());
		
		// 자산등록자 입력
		opmsDetailDAO.updateInsUserInfo(paramMap);
		
		// 가용성, 기밀성, 무결성 값 합산하여 자산등급 값 업데이트
		assetAutomationDAO.updateImportanceByAvCnIn(paramMap);
		
		// 유무형 자산 여부 업데이트
		String inTangibleClassId = NkiaApplicationPropertiesMap.getProperty("Globals.Cmdb.Intangible.ClassId");
		
		if(inTangibleClassId != null) {
			String[] inTangibleClassIdList = inTangibleClassId.split(",");
			paramMap.put("inTangibleClassIdList", inTangibleClassIdList);
			assetAutomationDAO.updateTangibleAsset(paramMap);
		}
	}


}
