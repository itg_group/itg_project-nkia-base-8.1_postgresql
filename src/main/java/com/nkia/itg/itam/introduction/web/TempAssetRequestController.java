package com.nkia.itg.itam.introduction.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.introduction.service.TempAssetRequestService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * 자산 입고 요청
 * 검토된 임시자산을 CMDB 등록 요청 (구성자산 등록)
 * @author ejjwa
 *
 */

@Controller
public class TempAssetRequestController {

	@Resource(name="tempAssetRequestService")
	TempAssetRequestService tempAssetRequestService;
	
	/**
	 * 자산 입고요청 화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/goTempAssetRequestData.do")
	public String goTempAssetRequestData(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/introduction/tempAssetRequestData.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산 입고요청 화면(롯데UBIT용)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/goTempAssetRequestDataNew.do")
	public String goTempAssetRequestDataNew(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/introduction/tempAssetRequestData_new.nvf";
		return forwarPage;
	}
	
	/**
	 * 입고요청 처리
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/insertOpmsInfoData.do")
	public @ResponseBody ResultVO insertOpmsInfoData(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			List<HashMap> paramList = (List) paramMap.get("paramList");
			
			for(HashMap dataMap : paramList) {
				dataMap.put("user_id", userVO.getUser_id());
				tempAssetRequestService.insertOpmsInfoData(dataMap);
			}
			
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
}
