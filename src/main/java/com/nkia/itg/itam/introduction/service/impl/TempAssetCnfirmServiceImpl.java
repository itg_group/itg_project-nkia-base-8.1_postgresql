package com.nkia.itg.itam.introduction.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.itam.introduction.dao.TempAssetCnfirmDAO;
import com.nkia.itg.itam.introduction.service.TempAssetCnfirmService;
import com.nkia.itg.itam.introduction.service.TempAssetService;


@Service("tempAssetCnfirmService")
public class TempAssetCnfirmServiceImpl  implements TempAssetCnfirmService{
	
	@Resource(name="tempAssetCnfirmDAO")
	private TempAssetCnfirmDAO tempAssetCnfirmDAO;
	
	@Resource(name="tempAssetService")
	private TempAssetService tempAssetService;

	@Override
	public void cnfirmTempAsset(HashMap paramMap) {
		List<HashMap> assetList = (List<HashMap>) paramMap.get("paramList");
		
		for(HashMap dataMap : assetList) {
			dataMap.put("status", "CONFIRM");
			tempAssetService.updateAmTempMapping(dataMap);
		}
	}

}
