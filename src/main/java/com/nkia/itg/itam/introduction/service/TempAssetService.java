package com.nkia.itg.itam.introduction.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface TempAssetService{


	public String createTempSeq()throws NkiaException;
	
	public String createConfTempId()throws NkiaException;
	
	public String createAssetTempId()throws NkiaException;
	
	List searchTempSeqList(HashMap paramMap);
	
	int searchTempSeqListCount(HashMap paramMap);

	List searchTempAssetList(HashMap paramMap);
	
	int searchTempAssetListCount(HashMap paramMap);
	
	void deleteAmTempData(HashMap dataParam);

	void updateAmTempMapping(HashMap dataParam);

	public void insertAmTempSeq(HashMap paramData);

	public void insertAmTempMapping(HashMap mappingData);

	public void insertAmTempData(HashMap tempParamMap);

	public void deleteAmTempMapping(HashMap paramMap);
	
	/**
	 * 임시자산 컴포넌트 목록 조회
	 */
	public List selectOpmsInfoServiceList(HashMap paramMap) throws NkiaException;
	
	public List selectOpmsInfoRelConfList(HashMap paramMap)throws NkiaException;

	public List selectOpmsInfoSelMngUserList(ModelMap paramMap)throws NkiaException;

	void deleteTempAsset(HashMap paramMap);

	void deleteTempSeq(ModelMap paramMap);

	/** 분류체계별 필수 최하위 테이블 임시데이터 등록 */
	public void mustInsertClassTable(String classType, String assetTempId, String confTempId, String updUserId);

	public boolean checkCnfirmMenu() throws NkiaException;

	
}
