package com.nkia.itg.itam.introduction.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.introduction.service.TempAssetCnfirmService;

/**
 * 자산 일괄 검토
 * 일괄등록된 자산을 검토처리
 * @author ejjwa
 *
 */
@Controller
public class TempAssetCnfirmController {
	
	@Resource(name="tempAssetCnfirmService")
	private TempAssetCnfirmService tempAssetCnfirmService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 자산 일괄 검토 화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/goTempAssetCnfirm.do")
	public String goCnfirmTempAssetData(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/introduction/tempAssetCnfirm.nvf";
		return forwarPage;
	}
	
	
	/**
	 * 임시 자산 검토완료 처리
	 * STATUS : REGIST -> CONFIRM
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/cnfirmTempAsset.do")
	public @ResponseBody ResultVO cnfirmTempAsset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			tempAssetCnfirmService.cnfirmTempAsset(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
}
