package com.nkia.itg.itam.introduction.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("tempAssetDAO")
public class TempAssetDAO extends EgovComAbstractDAO {
	
	public String createTempSeq()throws NkiaException{
		return (String)selectByPk("TempAssetDAO.createTempSeq", null);
	}
	
	public String createConfTempId()throws NkiaException{
		return (String)selectByPk("TempAssetDAO.createConfTempId", null);
	}
	
	public String createAssetTempId()throws NkiaException{
		return (String)selectByPk("TempAssetDAO.createAssetTempId", null);
	}

	public List searchTempSeqList(HashMap paramMap) {
		return list("TempAssetDAO.searchTempSeqList", paramMap);
	}
	
	public int searchTempSeqListCount(HashMap paramMap) {
		return (Integer) selectByPk("TempAssetDAO.searchTempSeqListCount", paramMap);
	}

	public List searchTempAssetList(HashMap paramMap) {
		return list("TempAssetDAO.searchTempAssetList", paramMap);
	}
	
	public int searchTempAssetListCount(HashMap paramMap) {
		return (Integer) selectByPk("TempAssetDAO.searchTempAssetListCount", paramMap);
	}

	public void deleteAmTempData(HashMap paramMap) {
		delete("TempAssetDAO.deleteAmTempData", paramMap);
	}
	
	public void deleteAmTempMapping(HashMap paramMap) {
		delete("TempAssetDAO.deleteAmTempMapping", paramMap);
	}

	public void insertAmTempSeq(HashMap paramData) {
		insert("TempAssetDAO.insertAmTempSeq", paramData);
	}

	public void insertAmTempMapping(HashMap paramData) {
		insert("TempAssetDAO.insertAmTempMapping", paramData);
	}
	
	public void insertAmTempData(HashMap paramMap) {
		insert("TempAssetDAO.insertAmTempData", paramMap);
	}

	public void insertAmTempDataForList(HashMap paramMap) {
		insert("TempAssetDAO.insertAmTempDataForList", paramMap);
	}

	public void updateAmTempMapping(HashMap paramMap) {
		update("TempAssetDAO.updateAmTempMapping", paramMap);
	}
	
	/**
	 * 
	 * 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInfoSelMngUserList(HashMap paramMap) throws NkiaException {
		return list("TempAssetDAO.selectOpmsInfoSelMngUserList",paramMap);
	}
//	
	/**
	 * 
	 * 연관 서비스 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInfoServiceList(HashMap paramMap) throws NkiaException {
		return list("TempAssetDAO.selectOpmsInfoServiceList",paramMap);
	}

	/**
	 * 연관 장비 정보 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInfoRelConfList(HashMap paramMap)throws NkiaException{
		return list("TempAssetDAO.selectOpmsInfoRelConfList",paramMap);
	}
	
	/**
	 * 연관 장비(DR) 정보 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInfoRelDRList(HashMap paramMap)throws NkiaException{
		return list("TempAssetDAO.selectOpmsInfoRelDRList",paramMap);
	}

	public void deleteAmTempSeq(HashMap dataMap) {
		delete("TempAssetDAO.deleteAmTempSeq", dataMap);
	}

	public void deleteAmTempDataForTempSeq(HashMap dataMap) {
		delete("TempAssetDAO.deleteAmTempDataForTempSeq", dataMap);
	}

	public void deleteAmTempMappingForTempSeq(HashMap dataMap) {
		delete("TempAssetDAO.deleteAmTempMappingForTempSeq", dataMap);
		
	}

	public boolean checkCnfirmMenu() {
		return (boolean) selectByPk("TempAssetDAO.checkCnfirmMenu", null);
	}
}
