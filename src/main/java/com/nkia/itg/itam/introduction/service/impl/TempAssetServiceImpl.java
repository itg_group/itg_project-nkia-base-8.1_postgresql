package com.nkia.itg.itam.introduction.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.NamedParamDAO;
import com.nkia.itg.itam.introduction.dao.TempAssetDAO;
import com.nkia.itg.itam.introduction.service.TempAssetService;


@Service("tempAssetService")
public class TempAssetServiceImpl implements TempAssetService{
	
	@Resource(name="tempAssetDAO")
	private TempAssetDAO tempAssetDAO;
	
	@Resource(name="namedParamDAO")
	private NamedParamDAO namedParamDAO;
	
	@Override
	public String createTempSeq()throws NkiaException{
		return tempAssetDAO.createTempSeq();
	}
	
	@Override
	public String createConfTempId()throws NkiaException{
		return  tempAssetDAO.createConfTempId();
	}
	
	@Override
	public String createAssetTempId()throws NkiaException{
		return tempAssetDAO.createAssetTempId();
	}	
	

	@Override
	public List searchTempSeqList(HashMap paramMap) {
		return tempAssetDAO.searchTempSeqList(paramMap);
	}

	@Override
	public int searchTempSeqListCount(HashMap paramMap) {
		return tempAssetDAO.searchTempSeqListCount(paramMap);
	}

	@Override
	public List searchTempAssetList(HashMap paramMap) {
		return tempAssetDAO.searchTempAssetList(paramMap);
	}

	@Override
	public int searchTempAssetListCount(HashMap paramMap) {
		return tempAssetDAO.searchTempAssetListCount(paramMap);
	}

	/**
	 * 임시자산 수정
	 * 임시자산 항목 삭제
	 */
	@Override
	public void deleteAmTempData(HashMap paramMap) {
		tempAssetDAO.deleteAmTempData(paramMap);
	}

	/**
	 * 임시자산 수정
	 * 임시자산 등록 상태 변경
	 */
	@Override
	public void updateAmTempMapping(HashMap paramMap) {
		tempAssetDAO.updateAmTempMapping(paramMap);
	}
	
	/**
	 * 임시자산 입고요청
	 * 임시자산 매핑 삭제
	 */
	@Override
	public void deleteAmTempMapping(HashMap paramMap) {
		tempAssetDAO.deleteAmTempMapping(paramMap);
		
	}
	
	/**
	 * 임시자산 시퀀스 등록
	 */
	@Override
	public void insertAmTempSeq(HashMap paramData) {
		tempAssetDAO.insertAmTempSeq(paramData);
	}

	/**
	 * 임시자산 매핑 등록
	 */
	@Override
	public void insertAmTempMapping(HashMap paramData) {
		
		StringBuffer query = new StringBuffer();
		query.append("INSERT INTO AM_TEMP_MAPPING ( TEMP_SEQ, ASSET_TEMP_ID, CONF_TEMP_ID, STATUS ) VALUES (");
		query.append("\n	:temp_seq, :asset_temp_id, :conf_temp_id, :status )");
		namedParamDAO.insert(query.toString(), paramData);
		
//		tempAssetDAO.insertAmTempMapping(paramData);
	}

	/**
	 * 임시자산 데이터 등록
	 */
	@Override
	public void insertAmTempData(HashMap paramData) {
		
		if(paramData.containsKey("paramList")) {
			tempAssetDAO.insertAmTempDataForList(paramData);
		}else{
			tempAssetDAO.insertAmTempData(paramData);
		}
	}
	

	/**
	 * 일괄 등록 임시자산 삭제
	 * @param paramMap
	 */
	@Override
	public void deleteTempAsset(HashMap paramMap) {
		List<HashMap> assetList = (List<HashMap>) paramMap.get("paramList");
		
		for(HashMap dataMap : assetList) {
			tempAssetDAO.deleteAmTempMapping(dataMap);
			tempAssetDAO.deleteAmTempData(dataMap);
		}
	}

	/**
	 * 일괄 등록 요청 삭제
	 */
	@Override
	public void deleteTempSeq(ModelMap paramMap) {
		List<HashMap> assetList = (List<HashMap>) paramMap.get("paramList");
		
		for(HashMap dataMap : assetList) {
			tempAssetDAO.deleteAmTempDataForTempSeq(dataMap);
			tempAssetDAO.deleteAmTempMappingForTempSeq(dataMap);
			tempAssetDAO.deleteAmTempSeq(dataMap);
		}
	}
	
	/**
	 * =========임시자산 컴포넌트 데이터 조회 Start===================================================
	 */
	@Override
	public List selectOpmsInfoServiceList(HashMap paramMap) throws NkiaException {
		return tempAssetDAO.selectOpmsInfoServiceList(paramMap);
	}
	
	@Override
	public List selectOpmsInfoRelConfList(HashMap paramMap) throws NkiaException {
		String class_type = (String)paramMap.get("class_type");
		List relConfList = null;
		
		if("HA".equalsIgnoreCase(class_type)){ 
			//이중화의 경우 
			//relConfList = opmsInfoDAO.selectOpmsRelHaGrpList(paramMap);
		}else if("LOGSV".equalsIgnoreCase(class_type)){
			//연관 장비에서 논리 연계정보인경우
			//relConfList = opmsInfoDAO.selectOpmsReLogConfList(paramMap);
		}else if("DR".equalsIgnoreCase(class_type)){
			relConfList = tempAssetDAO.selectOpmsInfoRelDRList(paramMap);
		}else{
			//나머지.. 등등
			relConfList = tempAssetDAO.selectOpmsInfoRelConfList(paramMap);
		}
		
		return relConfList;
	}	
	@Override
	public List selectOpmsInfoSelMngUserList(ModelMap paramMap)throws NkiaException {
		return tempAssetDAO.selectOpmsInfoSelMngUserList(paramMap);
	}

	/**
	 * =========임시자산 컴포넌트 데이터 조회 end===================
	 */
	
	/**
	 * 분류체계별 필수 테이블 컬럼 등록 (더미데이터)
	 * >> 사이트별로 맞게 테이블 정의 <<
	 * 분류체계 테이블에 데이터 누락 방지를 위한 더미데이터 등록 서비스입니다.
	 */
	@Override
	public void mustInsertClassTable(String classType, String assetTempId, String confTempId, String updUserId) {
		String tableId = "";
		
		if("SV".equals(classType)){
			tableId = "AM_SV_PHYSI";
		}else{
			tableId = "AM_"+classType;
		}
		
		HashMap tempMap = new HashMap();
		tempMap.put("asset_temp_id", assetTempId);
		tempMap.put("conf_temp_id", confTempId);
		tempMap.put("ins_table_nm", tableId);
		tempMap.put("ins_user_id", updUserId);
		
		insertAmTempData(tempMap);
	}

	@Override
	public boolean checkCnfirmMenu() throws NkiaException {
		return tempAssetDAO.checkCnfirmMenu();
	}

}
