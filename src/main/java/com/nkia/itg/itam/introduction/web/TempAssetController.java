package com.nkia.itg.itam.introduction.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.introduction.service.TempAssetService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;


@Controller
public class TempAssetController {

	@Resource(name="tempAssetService")
	TempAssetService tempAssetService;
	
	/**
	 * 일괄 등록 요청 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/searchTempSeqList.do")
	public @ResponseBody ResultVO searchTempSeqList(@RequestBody ModelMap paramMap)  throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			List resultList = new ArrayList();

			//@@ 페이징 관련 내용 
//			PagingUtil.getFristEndNum(paramMap);
//			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));

			resultList = tempAssetService.searchTempSeqList(paramMap);
			totalCount = tempAssetService.searchTempSeqListCount(paramMap);
			
			
			gridVO.setRows(resultList);
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 일괄 등록 자산 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/searchTempAssetList.do")
	public @ResponseBody ResultVO searchTempAssetList(@RequestBody ModelMap paramMap)  throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			List resultList = new ArrayList();

			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = tempAssetService.searchTempAssetList(paramMap);
			totalCount = tempAssetService.searchTempAssetListCount(paramMap);
			
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			
			gridVO.setRows(resultList);
			gridVO.setTotalCount(totalCount);
			
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsInfoSelMngUserList.do")
	public @ResponseBody ResultVO selectOpmsInfoSelMngUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = tempAssetService.selectOpmsInfoSelMngUserList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

	
	/**
	 * 정보수집서 서비스 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsInfoServiceList.do")
	public @ResponseBody ResultVO selectOpmsInfoServiceList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = tempAssetService.selectOpmsInfoServiceList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 연관 장비 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/opms/selectOpmsInfoRelConfList.do")
	public @ResponseBody ResultVO selectOpmsInfoRelConfList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = tempAssetService.selectOpmsInfoRelConfList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 임시 자산 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/deleteTempAsset.do")
	public @ResponseBody ResultVO deleteTempAsset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			tempAssetService.deleteTempAsset(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 일괄 등록 요청 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/introduction/deleteTempSeq.do")
	public @ResponseBody ResultVO deleteTempSeq(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			tempAssetService.deleteTempSeq(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
