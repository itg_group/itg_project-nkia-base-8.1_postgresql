package com.nkia.itg.itam.introduction.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("tempAssetRequestDAO")
public class TempAssetRequestDAO extends EgovComAbstractDAO {

	public List<HashMap> selectTableList(HashMap paramMap) {
		return list("TempAssetRequestDAO.selectTableList",paramMap);
	}

	public List<HashMap> selectColList(HashMap paramMap) {
		return list("TempAssetRequestDAO.selectColList",paramMap);
	}

	public List<HashMap> selectOperDataList(HashMap paramMap) {
		return list("TempAssetRequestDAO.selectOperDataList",paramMap);
	}

	public void insertOperData(HashMap paramMap) {
		insert("TempAssetRequestDAO.insertOperData", paramMap);
	}

	public List<HashMap> selectServiceDataList(HashMap paramMap) {
		return list("TempAssetRequestDAO.selectServiceDataList",paramMap);
	}

	public void insertServiceData(HashMap paramMap) {
		insert("TempAssetRequestDAO.insertServiceData", paramMap);
	}

	public List<HashMap> selectRelConfDataList(HashMap paramMap) {
		return list("TempAssetRequestDAO.selectRelConfDataList", paramMap);
	}

	public List<HashMap> selectConfRelDataList(HashMap paramMap) {
		return list("TempAssetRequestDAO.selectConfRelDataList", paramMap);
	}

	public void insertRelData(HashMap dataMap) {
		insert("TempAssetRequestDAO.insertRelData", dataMap);
		
	}

	public List<HashMap> searchOpmsInstallTableInfo(HashMap paramMap) {
		return list("TempAssetRequestDAO.searchOpmsInstallTableInfo",paramMap);
	}

	public int selectInstallInfoCnt(HashMap paramMap) {
		return (Integer)selectByPk("TempAssetRequestDAO.selectInstallInfoCnt", paramMap);
	}

	public List<HashMap> selectInstallInfoData(HashMap paramMap) {
		return list("TempAssetRequestDAO.selectInstallInfoData",paramMap);
	}

	public void insertInstallInfoData(HashMap dataMap) {
		insert("TempAssetRequestDAO.insertInstallInfoData", dataMap);
		
	}


	
}
