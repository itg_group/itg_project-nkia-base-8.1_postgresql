package com.nkia.itg.itam.location.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.location.dao.LocationDAO;
import com.nkia.itg.itam.location.service.LocationService;

@Service("locationService")
public class LocationServiceImpl implements LocationService{
	private static final Logger logger = LoggerFactory.getLogger(LocationServiceImpl.class);
	
	@Resource(name = "locationDAO")
	public LocationDAO locationDAO;

	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public List searchLocation(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return locationDAO.searchLocation(paramMap);
	}

	public HashMap selectLocation(ModelMap paramMap) throws NkiaException {
		return locationDAO.selectLocation(paramMap);
	}
	
	public void insertLocation(ModelMap paramMap) throws NkiaException {
		locationDAO.insertLocation(paramMap);
	}	
	
	public void updateLocation(ModelMap paramMap) throws NkiaException {
		if(paramMap.get("use_yn_back") != paramMap.get("use_yn")){
			locationDAO.updateDownLocation(paramMap);
		}
		locationDAO.updateLocation(paramMap);
	}	
	
	public void deleteLocation(ModelMap paramMap) throws NkiaException {
		locationDAO.deleteLocation(paramMap);
	}

	@Override
	public List searchFloorByLocation(ModelMap paramMap) {
		return locationDAO.searchFloorByLocation(paramMap);
	}
	
	@Override
	public List selectCenterList(ModelMap paramMap) {
		return locationDAO.selectCenterList(paramMap);
	}
}
