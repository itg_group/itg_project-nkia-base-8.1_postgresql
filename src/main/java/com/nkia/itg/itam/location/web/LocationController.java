/*
 * @(#)LocationController.java              2013. 5. 22.
 *
 * @eungyu@nakia.co.kr
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.location.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.location.service.LocationService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class LocationController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "locationService")
	private LocationService locationService;
	
	/**
	 * message 호출
	 */	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 위치관리 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/location/locationManager.do")
	public String locationManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/itam/location/locationManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 위치관리_트리생성
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/location/searchLocation.do")
	public @ResponseBody ResultVO searchLocation(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = locationService.searchLocation(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 위치관리(해당 위치의 층만 나오도록)_트리생성
	 * @param paramMap{parent_loc_code:(부모위치값)} 필수
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/location/searchFloorByLocation.do")
	public @ResponseBody ResultVO searchFloorByLocation(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = locationService.searchFloorByLocation(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	
	/**
	 * 위치관리 - 트리선택시 위치정보 조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/location/selectLocation.do")
	public @ResponseBody ResultVO selectLocation(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = locationService.selectLocation(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 위치관리 - 위치정보 신규등록
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/location/insertLocation.do")
	public @ResponseBody ResultVO insertLocation(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}	
	    	
			HashMap locationInfo = locationService.selectLocation(paramMap);
			
			// 등록된 코드가 없을경우 신규등록 처리
			if(locationInfo == null) {
				locationService.insertLocation(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));			
			} else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"위치코드"}));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 위치관리 - 위치정보 수정
	 * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/location/updateLocation.do")
	public @ResponseBody ResultVO updateLocation(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}			
			locationService.updateLocation(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 위치관리 - 위치정보 삭제
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/location/deleteLocation.do")
	public @ResponseBody ResultVO deleteLocation(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			locationService.deleteLocation(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 센터 위치 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/location/searchCenterList.do")
	public @ResponseBody ResultVO searchCodeList(@RequestBody ModelMap paramMap)throws NkiaException   {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = locationService.selectCenterList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
