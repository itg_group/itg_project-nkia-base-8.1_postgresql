package com.nkia.itg.itam.location.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface LocationService {

	public List searchLocation(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectLocation(ModelMap paramMap) throws NkiaException;
	
	public void insertLocation(ModelMap paramMap) throws NkiaException;
	
	public void updateLocation(ModelMap paramMap) throws NkiaException;
	
	public void deleteLocation(ModelMap paramMap) throws NkiaException;

	public List searchFloorByLocation(ModelMap paramMap);
	
	public List selectCenterList(ModelMap paramMap);
}
