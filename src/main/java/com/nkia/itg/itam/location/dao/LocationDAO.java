package com.nkia.itg.itam.location.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("locationDAO")
public class LocationDAO  extends EgovComAbstractDAO {

	public List searchLocation(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("locationDAO.searchLocation", paramMap);
		return resultList;
	}	

	public HashMap selectLocation(ModelMap paramMap) throws NkiaException{
		return (HashMap)selectByPk("locationDAO.selectLocation", paramMap);
	}	
	
	public void insertLocation(ModelMap paramMap) throws NkiaException {
		this.insert("locationDAO.insertLocation", paramMap);
	}		
	
	public void updateLocation(ModelMap paramMap) throws NkiaException {
		this.update("locationDAO.updateLocation", paramMap);
	}		
	
	public void updateDownLocation(ModelMap paramMap) throws NkiaException {
		this.update("locationDAO.updateDownLocation", paramMap);
	}		
	
	public void deleteLocation(ModelMap paramMap) throws NkiaException {
		this.delete("locationDAO.deleteLocation", paramMap);
	}	
	
	public List searchFloorByLocation(ModelMap paramMap) {
		List resultList = null;
		resultList = this.list("locationDAO.searchFloorByLocation", paramMap);
		return resultList;
	}
	
	public List selectCenterList(ModelMap paramMap) {
		List resultList = null;
		resultList = this.list("locationDAO.selectCenterList", paramMap);
		return resultList;
	}
}
