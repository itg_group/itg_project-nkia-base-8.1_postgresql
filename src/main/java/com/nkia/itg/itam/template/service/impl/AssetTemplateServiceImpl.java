/*
 * @(#)AssetTemplateServiceImpl.java              2013. 09. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.template.service.impl;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.dao.AtchFileDAO;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.itam.template.dao.AssetTemplateDAO;
import com.nkia.itg.itam.template.service.AssetTemplateService;

import egovframework.com.cmm.EgovWebUtil;
import egovframework.com.cmm.service.EgovFileMngUtil;

@Service("assetTemplateService")
public class AssetTemplateServiceImpl implements AssetTemplateService{
	
	@Resource(name = "atchFileDAO")
	public AtchFileDAO atchFileDAO;
	
	@Resource(name = "assetTemplateDAO")
	public AssetTemplateDAO assetTemplateDAO;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil; 
	
	public static final int BUFF_SIZE = 2048;
	
	private static final String process_template_file_path = BaseConstraints.getTemplateFilePath();
	
	
	public List searchTemplate(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetTemplateDAO.searchTemplate(paramMap);
	}

	public int searchTemplateCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetTemplateDAO.searchTemplateCount(paramMap);
	}
	
	/**
	 * 
	 * 프로세스 템플릿 업로드
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	public Map uploadAssetTemplate(AtchFileVO atchFileVO, HttpServletRequest request)throws NkiaException {
		MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
		InputStream stream = null;

		try {
		    File cFile = new File(EgovWebUtil.filePathBlackList(process_template_file_path));
		    
		    cFile.setExecutable(false, true);
		    cFile.setReadable(true);
		    cFile.setWritable(false, true);
		    
		    if (!cFile.exists()){
		    	cFile.mkdir();
		    }

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		Map result = null;
		Iterator<String> fileIter = mptRequest.getFileNames();
		while (fileIter.hasNext()) {
			String fileName = (String) fileIter.next();
			MultipartFile mFile = mptRequest.getFile( fileName );
			if (mFile.getSize() > 0) {
				// 개별 최대 파일 Check
				result = EgovFileMngUtil.uploadFile(mFile, process_template_file_path);
			}
		}
		
		return result;
	}
	
	public void insertTemplate(ModelMap paramMap) throws NkiaException {
		assetTemplateDAO.insertTemplate(paramMap);
	}
	
	public void updateTemplate(ModelMap paramMap) throws NkiaException {
		assetTemplateDAO.updateTemplate(paramMap);
	}
	
	public void deleteTemplate(ModelMap paramMap) throws NkiaException {
		assetTemplateDAO.deleteTemplate(paramMap);
	}
	
	public HashMap selectFileName(String id) throws NkiaException {
		HashMap saveFileName = assetTemplateDAO.selectFileName(id);
		return saveFileName;
	}

	@Override
	public int selectTemplateCnt(ModelMap paramMap) throws NkiaException {
		return assetTemplateDAO.selectTemplateCnt(paramMap);
	}

}
