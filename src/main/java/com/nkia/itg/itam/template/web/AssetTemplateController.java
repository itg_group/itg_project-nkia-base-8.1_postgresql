/*
 * @(#)AssetTemplateController.java              2013. 09. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.template.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.template.service.AssetTemplateService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AssetTemplateController {
	
	@Resource(name = "assetTemplateService")
	private AssetTemplateService assetTemplateService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil;
	
	
	@RequestMapping(value="/itg/itam/template/assetTemplate.do")
	public String assetTemplate(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/template/assetTemplate.nvf";
		return forwarPage;
	}	
	
	/**
	 * 자산 템플릿 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/template/searchTemplate.do")
	public @ResponseBody ResultVO searchTemplate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = assetTemplateService.searchTemplate(paramMap);		
			totalCount = assetTemplateService.searchTemplateCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 
	 * 자산 템플릿 업로드
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value = "/itg/itam/template/uploadAssetTemplate.do")
	public void uploadAssetTemplate(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException {
		JSONObject returnData = new JSONObject();
		
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			// 파일등록
			Map result = assetTemplateService.uploadAssetTemplate(atchFileVO, request);
			
	        //sets success to true
			returnData.put("success", true);
			returnData.put("resultData", result);
	        
			//convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        //out.close();
		} catch (Exception e) {
/*			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
*/			throw new NkiaException(e);
			
		}finally
		{
			 out.close();
		}
	}
	
	/**
	 * 자산 템플릿 신규등록
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/template/insertTemplate.do")
	public @ResponseBody ResultVO insertTemplate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
	    	assetTemplateService.insertTemplate(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자산 템플릿 수정
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/template/updateTemplate.do")
	public @ResponseBody ResultVO updateTemplate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}
			assetTemplateService.updateTemplate(paramMap);
			String oldFileNm = (String) paramMap.get("old_file_nm");
			String saveFileNm = (String) paramMap.get("save_file_nm");			
			if(!oldFileNm.equals(saveFileNm)){
				String stordFilePate = BaseConstraints.getTemplateFilePath();
				String yymm = oldFileNm.substring(0, 6)+ "/";
				String deleteFilePath = stordFilePate + yymm + oldFileNm;
				File deleteFile = new File(deleteFilePath);
				if ( deleteFile.exists() ){
					deleteFile.delete();
				}
			}
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자산 템플릿 삭제
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/template/deleteTemplate.do")
	public @ResponseBody ResultVO deleteTemplate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}
			assetTemplateService.deleteTemplate(paramMap);
			
			String stordFilePate = BaseConstraints.getTemplateFilePath();
			String saveFileName = (String) paramMap.get("save_file_nm");
			String yymm = saveFileName.substring(0, 6)+ "/";
			String deleteFilePath = stordFilePate + yymm + saveFileName;
			File deleteFile = new File(deleteFilePath);
			if ( deleteFile.exists() ){
				deleteFile.delete();
			}
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 
	 * 자산 템플릿 다운로드
	 * 
	 * @param request
	 * @param response
	 * @return 
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/itam/template/downAssetTemplate.do")
	public void downAssetTemplate(HttpServletRequest request, HttpServletResponse response) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			HashMap fileInfo = assetTemplateService.selectFileName(request.getParameter("tem_id"));
			String saveFileNm = (String) fileInfo.get("SAVE_FILE_NM");
			String origFileNm = (String) fileInfo.get("ORIGIN_FILE_NM");
			String yymm = saveFileNm.substring(0, 6) + "/";
			String stordFilePate = BaseConstraints.getTemplateFilePath() + yymm;
	
			if("".equals(origFileNm)){
				origFileNm = saveFileNm;
			}
		    boolean isSuccess = atchFileUtil.createOutputFile(request, response, saveFileNm, origFileNm, stordFilePate, "application/vnd.ms-excel");
		    
			if( !isSuccess ){
				PrintWriter out = response.getWriter();
				response.setContentType("text/html; charset=euc-kr");
				out.println("파일을 찾을 수 없습니다.");
			}
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
	
	/**
	 * 템플릿 파일 존재유무 확인
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/template/selectTemplateCnt.do")
	public @ResponseBody ResultVO selectTemplateCnt(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			int result = assetTemplateService.selectTemplateCnt(paramMap);
			resultVO.setResultInt(result);
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}

}
