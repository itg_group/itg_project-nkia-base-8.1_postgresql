package com.nkia.itg.itam.template.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("assetTemplateDAO")
public class AssetTemplateDAO extends EgovComAbstractDAO{
	
	public List searchTemplate(ModelMap paramMap) throws NkiaException{
		return list("assetTemplateDAO.searchTemplate", paramMap);
	}

	public int searchTemplateCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("assetTemplateDAO.searchTemplateCount", paramMap);
	}
	
	public void insertTemplate(ModelMap paramMap) throws NkiaException {
		this.insert("assetTemplateDAO.insertTemplate", paramMap);
	}
	
	public void updateTemplate(ModelMap paramMap) throws NkiaException {
		this.update("assetTemplateDAO.updateTemplate", paramMap);
	}
	
	public void deleteTemplate(ModelMap paramMap) throws NkiaException {
		this.delete("assetTemplateDAO.deleteTemplate", paramMap);
	}	

	public HashMap selectFileName(String id) throws NkiaException{
		return (HashMap)selectByPk("assetTemplateDAO.selectFileName", id);
	}

	public int selectTemplateCnt(HashMap paramMap) throws NkiaException{
		return (Integer)selectByPk("assetTemplateDAO.selectTemplateCnt", paramMap);
	}	
}