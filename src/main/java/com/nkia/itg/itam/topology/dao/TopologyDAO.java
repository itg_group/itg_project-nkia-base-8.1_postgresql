/*
 * @(#)TopologyDAO.java              2013. 11. 25.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.topology.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("topologyDAO")
public class TopologyDAO extends EgovComAbstractDAO {
	
	/**
	 * 
	 * CI Topology ROOT 장비 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchCiTopologyRootData(ModelMap paramMap)throws NkiaException {
		return (HashMap)selectByPk("TopologyDAO.searchCiTopologyRootData", paramMap);
	}
	
	/**
	 * CI Topology ROOT 논리 정보
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchCiTopologyRootDataByLogic(ModelMap paramMap)throws NkiaException {
		return (HashMap)selectByPk("TopologyDAO.searchCiTopologyRootDataByLogic", paramMap);
	}
	
	/**
	 * 
	 * CI Topology 연계 장비 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchCiTopologyConfList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchCiTopologyConfList", paramMap);
	}

	/**
	 * 
	 * CI Topology 연계 서비스 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchCiTopologyServiceList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchCiTopologyServiceList", paramMap);
	}
	
	/**
	 * 
	 * CI Topology 연계 DR 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchCiTopologyDRList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchCiTopologyDRList", paramMap);
	}
	
	/**
	 * 
	 * 설치 SW 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchTopologyInsSwList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchTopologyInsSwList", paramMap);
	}
	
	/**
	 * 
	 * CI Topology 연계 물리 -> 논리 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchCiTopologySvLogicList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchCiTopologySvLogicList", paramMap);
	}
	
	/**
	 * 
	 * CI Topology 연계 논리 -> 물리 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchCiTopologySvPhysiList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchCiTopologySvPhysiList", paramMap);
	}
	
	/**
	 * 
	 * Service Topology ROOT 서비스 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchServiceTopologyRootData(ModelMap paramMap)throws NkiaException {
		return (HashMap)selectByPk("TopologyDAO.searchServiceTopologyRootData", paramMap);
	}
	
	/**
	 * 
	 * Service Topology 연계 장비 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchServiceTopologyConfList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchServiceTopologyConfList", paramMap);
	}
	
	/**
	 * 
	 * 이중화 연결장비 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchTopologyDualInfraList(ModelMap paramMap)throws NkiaException {
		return list("TopologyDAO.searchTopologyDualInfraList", paramMap);
	}
	
}
