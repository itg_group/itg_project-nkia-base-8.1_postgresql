/*
 * @(#)TopologyService.java              2013. 11. 25.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.topology.service;

import java.util.HashMap;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;


public interface TopologyService {
	
	/**
	 * 
	 * CI Topology
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	HashMap searchCiTopology(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * Service Topology
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	HashMap searchServiceTopology(ModelMap paramMap)throws NkiaException;

}
