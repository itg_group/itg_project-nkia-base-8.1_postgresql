/*
 * @(#)TopologyServiceImpl.java              2013. 11. 25.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.topology.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.topology.dao.TopologyDAO;
import com.nkia.itg.itam.topology.service.TopologyService;

@Service("topologyService")
public class TopologyServiceImpl implements TopologyService{

	@Resource(name = "topologyDAO")
	public TopologyDAO topologyDAO;
	
	/**
	 * 
	 * CI Topology
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public HashMap searchCiTopology(ModelMap paramMap) throws NkiaException {
		HashMap topologyMap = new HashMap();
		List topologyNodeList = new ArrayList();
		List topologyLinkList = new ArrayList();
		
		String nodeType = (String)paramMap.get("node_type");
		String tangibleAssetYn = (String)paramMap.get("tangible_asset_yn");
		boolean rootNode = (Boolean)paramMap.get("root_node");
		if( rootNode ){
			// 구성 토폴로지 Root 장비 정보
			HashMap topologyRootData = null;
			if( nodeType.equals("SV") && tangibleAssetYn.equals("N") ){
				topologyRootData = topologyDAO.searchCiTopologyRootDataByLogic(paramMap);
			} else if( nodeType.equals("NW") && tangibleAssetYn.equals("N") ){
				topologyRootData = topologyDAO.searchCiTopologyRootDataByLogic(paramMap);
			} else {
				topologyRootData = topologyDAO.searchCiTopologyRootData(paramMap);
			}
			topologyNodeList.add(topologyRootData);
		}
		
		// 구성 토폴로지 연계장비 정보
		List topologyConfList = topologyDAO.searchCiTopologyConfList(paramMap);
		if( topologyConfList != null && topologyConfList.size() > 0 ){
			topologyNodeList.addAll(topologyConfList);
			topologyLinkList.addAll(topologyConfList);
		}
		
		// 구성 토폴로지 연계서비스 정보
		List topologyServiceList = topologyDAO.searchCiTopologyServiceList(paramMap);
		if( topologyServiceList != null && topologyServiceList.size() > 0 ){
			topologyNodeList.addAll(topologyServiceList);
			topologyLinkList.addAll(topologyServiceList);
		}
		
		// 구성 토폴로지 연계서비스 정보
		List topologyDRList = topologyDAO.searchCiTopologyDRList(paramMap);
		if( topologyDRList != null && topologyDRList.size() > 0 ){
			topologyNodeList.addAll(topologyDRList);
			topologyLinkList.addAll(topologyDRList);
		}
		
		// 구성 토폴로지 설치SW 정보
		List topologyInsSwList = topologyDAO.searchTopologyInsSwList(paramMap);
		if( topologyInsSwList != null && topologyInsSwList.size() > 0 ){
			topologyNodeList.addAll(topologyInsSwList);
			topologyLinkList.addAll(topologyInsSwList);
		}
		
		// 이중화 연결장비 정보
		List topologyDualInfraList = topologyDAO.searchTopologyDualInfraList(paramMap);
		if( topologyDualInfraList != null && topologyDualInfraList.size() > 0 ){
			topologyNodeList.addAll(topologyDualInfraList);
			topologyLinkList.addAll(topologyDualInfraList);
		}
		
		// 구성 토폴로지 서버일 경우에 물리/논리정보
		boolean isLogical = (Boolean)paramMap.get("logical");
		if( isLogical ){
			if( nodeType.equals("SV") || nodeType.equals("NW") ){
				if( tangibleAssetYn.equals("Y") ){
					// 물리에서 논리정보 가져오기
					List topologySvLogicList = topologyDAO.searchCiTopologySvLogicList(paramMap);
					if( topologySvLogicList != null && topologySvLogicList.size() > 0 ){
						topologyNodeList.addAll(topologySvLogicList);
						topologyLinkList.addAll(topologySvLogicList);
					}
				}else if( tangibleAssetYn.equals("N") ){
					// 논리에서 물리정보 가져오기 
					List topologySvPhysiList = topologyDAO.searchCiTopologySvPhysiList(paramMap);
					if( topologySvPhysiList != null && topologySvPhysiList.size() > 0 ){
						topologyNodeList.addAll(topologySvPhysiList);
						topologyLinkList.addAll(topologySvPhysiList);
					}
				}
			}
		}
		
		topologyMap.put("nodes", topologyNodeList);
		topologyMap.put("links", topologyLinkList);
		return topologyMap;
	}
	
	/**
	 * 
	 * Service Topology
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public HashMap searchServiceTopology(ModelMap paramMap) throws NkiaException {
		HashMap topologyMap = new HashMap();
		List topologyNodeList = new ArrayList();
		List topologyLinkList = new ArrayList();
		boolean rootNode = (Boolean)paramMap.get("root_node");
		if( rootNode ){
			// 서비스 토폴로지 Root 서비스 정보
			HashMap topologyRootData = topologyDAO.searchServiceTopologyRootData(paramMap);
			topologyNodeList.add(topologyRootData);
		}
		
		// 서비스 토폴로지 연계장비 정보
		List topologyConfList = topologyDAO.searchServiceTopologyConfList(paramMap);
		if( topologyConfList != null && topologyConfList.size() > 0 ){
			topologyNodeList.addAll(topologyConfList);
			topologyLinkList.addAll(topologyConfList);
		}
		
		topologyMap.put("nodes", topologyNodeList);
		topologyMap.put("links", topologyLinkList);
		return topologyMap;
	}

}
