/*
 * @(#)TolopogyController.java              2013. 11. 25.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.topology.web;

import java.util.HashMap;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.topology.service.TopologyService;

@Controller
public class TopologyController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "topologyService")
	private TopologyService topologyService;
	
	
	/**
	 * 
	 * Topology 화면 Forward
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/topology/goTopology.do")
	public String goTopology(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/topology/topology.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * CI Topology
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/topology/searchCiTopology.do")
	public @ResponseBody ResultVO searchCiTopology(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = topologyService.searchCiTopology(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * Service Topology
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/topology/searchServiceTopology.do")
	public @ResponseBody ResultVO searchServiceTopology(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = topologyService.searchServiceTopology(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
