/*
 * @(#)AssetStatisticsServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.statistics.dao.AssetStatisticsSubDAO;
import com.nkia.itg.itam.statistics.service.AssetStatisticsSubService;

@Service("assetStatisticsSubService")
public class AssetStatisticsSubServiceImpl implements AssetStatisticsSubService{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetStatisticsSubServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "assetStatisticsSubDAO")
	public AssetStatisticsSubDAO assetStatisticsSubDAO;
	
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsSubDAO.searchClassTree(paramMap);
	}	
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsSubDAO.searchAssetInfo(paramMap);
	}

	public int searchAssetInfoCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsSubDAO.searchAssetInfoCount(paramMap);
	}	

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsSubDAO.searchCodeDataList(paramMap);
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsSubDAO.searchAssetList(paramMap);
	}

	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsSubDAO.searchAssetListCount(paramMap);
	}
	
	public List searchExcel(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetStatisticsSubDAO.searchExcel(paramMap); 
		return resultList;
	}
	
	public List searchSubDeptList(ModelMap paramMap) throws NkiaException {
		return assetStatisticsSubDAO.searchSubDeptList(paramMap);
	}	
}
