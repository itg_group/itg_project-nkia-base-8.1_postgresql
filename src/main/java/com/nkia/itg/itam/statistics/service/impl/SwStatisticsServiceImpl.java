package com.nkia.itg.itam.statistics.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.statistics.dao.SwStatisticsDAO;
import com.nkia.itg.itam.statistics.service.SwStatisticsService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("swStatisticsService")
public class SwStatisticsServiceImpl implements SwStatisticsService{

	@Resource(name="swStatisticsDAO")
	private SwStatisticsDAO swStatisticsDAO;
	
	/**
	 * 연계 SW 자산 목록 조회
	 */
	@Override
	public List swAssetList(ModelMap paramMap) throws NkiaException {
		return swStatisticsDAO.swAssetList(paramMap);
	}

	/**
	 * PC 자산 목록 조회
	 */
	@Override
	public List selectPcAssetList(ModelMap paramMap) throws NkiaException {
		return swStatisticsDAO.selectPcAssetList(paramMap);
	}

	/**
	 * PC 자산 목록 개수
	 */
	@Override
	public int selectPcAssetListCount(ModelMap paramMap) throws NkiaException {
		return swStatisticsDAO.selectPcAssetListCount(paramMap);
	}

	/**
	 * 서버 자산 목록 조회
	 */
	@Override
	public List selectServerAssetList(ModelMap paramMap) throws NkiaException {
		return swStatisticsDAO.selectServerAssetList(paramMap);
	}

	/**
	 * 서버 자산 목록 개수
	 */
	@Override
	public int selectServerAssetListCount(ModelMap paramMap) throws NkiaException {
		return swStatisticsDAO.selectServerAssetListCount(paramMap);
	}

	/**
	 * 연계 SW 자산 목록 엑셀다운
	 */
	@Override
	public List swAssetListExcel(Map paramMap) throws NkiaException {
		return swStatisticsDAO.swAssetListExcel(paramMap);
	}

	/**
	 * 서버 자산 목록 엑셀 다운
	 */
	@Override
	public List selectServerAssetListExcel(Map paramMap) throws NkiaException {
		return swStatisticsDAO.selectServerAssetListExcel(paramMap);
	}

	/**
	 * PC 자산 목록 엑셀 다운
	 */
	@Override
	public List selectPcAssetListExcel(Map param) throws NkiaException {
		return swStatisticsDAO.selectPcAssetListExcel(param);
	}

	/**
	 * 라이센스 할당 된 PC자산 목록
	 */
	@Override
	public List selectAssignPcAssetList(ModelMap paramMap) throws NkiaException {
		return swStatisticsDAO.selectAssignPcAssetList(paramMap);
	}

	/**
	 * 라이센스 할당 된 PC자산 목록 개수
	 */
	@Override
	public int selectAssignPcAssetListCount(ModelMap paramMap) throws NkiaException {
		return swStatisticsDAO.selectAssignPcAssetListCount(paramMap);
	}

	/**
	 * 라이센스 회수
	 */
	@Override
	public void deleteSwAssignLicence(ModelMap paramMap) throws NkiaException {
		
		List paramList = (List) paramMap.get("paramList");
		if(paramList != null){
			for(int i = 0; i < paramList.size(); i++){
				Map paramData = (Map) paramList.get(i);
				swStatisticsDAO.deleteSwAssignLicence(paramData);
			}
		}
	}

	/**
	 * 라이선스 할당
	 */
	@Override
	public void insertSwAssignLicence(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	List hwConfIdArr = (List) paramMap.get("hw_conf_ids");
    	for(int i = 0; i < hwConfIdArr.size(); i++){
    		paramMap.put("hw_conf_id", hwConfIdArr.get(i));
    		swStatisticsDAO.insertSwAssignLicence(paramMap);
    	}
	}

}
