package com.nkia.itg.itam.statistics.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.statistics.dao.SvBlacklistDAO;
import com.nkia.itg.itam.statistics.service.SvBlacklistService;

@Service("svBlacklistService")
public class SvBlacklistServiceImpl implements SvBlacklistService{
	
	@Resource(name="svBlacklistDAO")
	private SvBlacklistDAO svBlacklistDAO;

	@Override
	public List searchSvConfBlacklist(HashMap paramMap) throws NkiaException {
		return svBlacklistDAO.searchSvConfBlacklist(paramMap);
	}

	@Override
	public List searchIfAdBlacklist(HashMap paramMap) throws NkiaException {
		return svBlacklistDAO.searchIfAdBlacklist(paramMap);
	}

	@Override
	public int searchSvConfBlacklistCount(HashMap paramMap) throws NkiaException {
		return svBlacklistDAO.searchSvConfBlacklistCount(paramMap);
	}

	@Override
	public int searchIfAdBlacklistCount(HashMap paramMap) throws NkiaException {
		return svBlacklistDAO.searchIfAdBlacklistCount(paramMap);
	}

}
