/*
 * @(#)InfraController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.itam.statistics.service.InfraService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class InfraController {
	
	@Resource(name = "infraService")
	private InfraService infraService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 분류체계별 life cycle 현황  페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
//	@RequestMapping(value="/itg/itam/statistics/classTypeLifeCycle.do")
//	public String classTypeLifeCycle(ModelMap paramMap) throws NkiaException {
//		String forwarPage = "/itg/itam/statistics/classTypeLifeCycle.nvf";
//		return forwarPage;
//	}
	
	/**
	 * 부서별 분류체계 현황 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/custTypeClass.do")
	public String custTypeClass(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/custTypeClass.nvf";
		return forwarPage;
	}
	
	/**
	 * 부서별 life cycle 현황  페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/custTypeLifeCycle.do")
	public String custTypeLifeCycle(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/custTypeLifeCycle.nvf";
		return forwarPage;
	}
	
	/**
	 * 부서별 도입현황 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/custTypeIntroduction.do")
	public String custTypeIntroduction(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/custTypeIntroduction.nvf";
		return forwarPage;
	}
	
	/**
	 * 연도별 도입현황 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/yearsTypeIntroduction.do")
	public String yearsTypeIntroduction(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/yearsTypeIntroduction.nvf";
		return forwarPage;
	}
	
	/**
	 * 기간별 도입금액 현황 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/termByIntroCost.do")
	public String termByIntroCost(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/termByIntroCost.nvf";
		return forwarPage;
	}
	
	/**
	 * 연도별 도입금액 현황 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/yearByIntroCost.do")
	public String yearByIntroCost(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/yearByIntroCost.nvf";
		return forwarPage;
	}
	
	/**
	 * 부서별 life cycle 차트
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/infra/searchCustTypeLifeCycleChart.do")
	public @ResponseBody ResultVO searchCustTypeLifeCycleChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = infraService.searchCustTypeLifeCycleChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 부서별 life cycle TreeGrid 리스트
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/infra/searchCustTypeLifeCycleList.do")
	public @ResponseBody ResultVO searchCustTypeLifeCycleList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = infraService.searchCustTypeLifeCycleList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
		
	}
	
	/**
	 * 부서별 분류체계 차트
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/infra/searchCustTypeClassChart.do")
	public @ResponseBody ResultVO searchCustTypeClassChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = infraService.searchCustTypeClassChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서별 분류체계 리스트
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/infra/searchCustTypeClassList.do")
	public @ResponseBody ResultVO searchCustTypeClassList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = infraService.searchCustTypeClassList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			new NkiaException(e);
		}
		return resultVO;	
		
	}
	
	/**
	 * 연도별 분류체계 리스트
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/infra/searchYearTypeIntroductionList.do")
	public @ResponseBody ResultVO searchYearTypeIntroductionList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = infraService.searchYearTypeIntroductionList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 기간별 도입금액 리스트
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/infra/searchTermByIntroCostList.do")
	public @ResponseBody ResultVO searchTermByIntroCostList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = infraService.searchTermByIntroCostList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
		
	}
	
	/**
	 * 연도별 도입금액 차트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/infra/searchYearByIntroCostChart.do")
	public @ResponseBody ResultVO searchYearByIntroCostChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = infraService.searchYearByIntroCostChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 연도별 도입금액 리스트
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/infra/searchYearByIntroCostList.do")
	public @ResponseBody ResultVO searchYearByIntroCostList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = infraService.searchYearByIntroCostList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
		
	}
	
	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/infra/searchPresentExcelDown.do")
    public @ResponseBody ResultVO searchPresentExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = infraService.searchPresentExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서/사용자별 자산현황 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/infra/userDeptState.do")
	public String operManagerTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/userDeptStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/infra/userDeptStateList.do")
	public String operManagerList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/userDeptState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 위치 분류별 자산현황 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/infra/locationClassificationState.do")
	public String locationClassificationStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/locationClassificationStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/infra/locationClassificationStateList.do")
	public String locationClassificationState(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/locationClassificationState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * IP별 자산현황 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/infra/ipAddressState.do")
	public String ipStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/ipAddressState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/infra/ipAddressStateList.do")
	public String ipStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/ipAddressStateList.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	/**
	 *  사내 ITAM 통계 추가
	 * 2016.09.19 좌은진
	 */
	
	/**
	 * 부서별 자산현황
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */

	@RequestMapping(value="/itg/itam/statistics/custByAssetState.do")
	public String custByAssetState(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/custByAssetState.nvf";
		return forwarPage;
	}
	
	/**
	 * 부서별 현황 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchCustByAssetStateList.do")
    public @ResponseBody ResultVO searchCustByAssetStateList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			TreeVO treeVO = null;
			List resultList = infraService.searchCustByAssetStateList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 부서별 현황 차트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchCustByAssetStateChart.do")
	public @ResponseBody ResultVO searchCustByAssetStateChart(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = infraService.searchCustByAssetStateChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 연도별 자산현황
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */

	@RequestMapping(value="/itg/itam/statistics/classTypeByYear.do")
	public String classTypeByYear(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/classTypeByYear.nvf";
		return forwarPage;
	}
	
	/**
	 * 연도별 현황 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchClassTypeByYearList.do")
    public @ResponseBody ResultVO searchClassTypeByYearList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			TreeVO treeVO = null;
			List resultList = infraService.searchClassTypeByYearList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 연도별 도입 현황 차트 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchClassTypeByYearChart.do")
	public @ResponseBody ResultVO searchClassTypeByYearChart(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = infraService.searchClassTypeByYearChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	
}
