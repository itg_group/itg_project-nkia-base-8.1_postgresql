/*
 * @(#)AssetStatisticsWithCategoryController.java              2013. 12. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.statistics.service.AssetStatisticsWithCategoryService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AssetStatisticsWithCategoryController {
	
	@Resource(name = "assetStatisticsWithCategoryService")
	private AssetStatisticsWithCategoryService assetStatisticsService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 자산현황 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchCategoryAssetList.do")
	public @ResponseBody ResultVO searchAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = assetStatisticsService.searchAssetList(paramMap);		
			totalCount = assetStatisticsService.searchAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 ROW CNT
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchCategoryAssetListCount.do")
	public @ResponseBody ResultVO searchAssetListCount(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			totalCount = assetStatisticsService.searchAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 담당자별 자산현황 화면 로딩 (운영담당자)
	 * This scene is search the asset list for a asset operator.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/operUserState.do")
	public String operUserStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/operUserStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/statistics/operUserStateList.do")
	public String operUserStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/operUserState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 자산별 자산현황 화면 로딩 (운영담당자)
	 * This scene is search the asset list for a asset operator.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/assetState.do")
	public String assetStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/assetStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/assetStateList.do")
	public String assetStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/assetState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 부서/사용자별 자산현황 화면 로딩
	 * This scene is search the asset list for the user and department.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/userDeptState.do")
	public String deptStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/userDeptStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/userDeptStateList.do")
	public String deptStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/userDeptState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 위치 분류별 자산현황 화면 로딩
	 * This scene is search the asset list for the location of the asset.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/locationClassificationState.do")
	public String locationClassificationStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/locationClassificationStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/locationClassificationStateList.do")
	public String locationClassificationState(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/locationClassificationState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * IP별 자산현황 화면 로딩
	 * This scene is search the asset list for the IP address of the asset.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/ipAddressState.do")
	public String ipStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/ipAddressState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/ipAddressStateList.do")
	public String ipStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/ipAddressStateList.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 업체별 자산현황 화면 로딩
	 * This scene is search the asset list for the vendor.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/vendorState.do")
	public String vendorStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/vendorStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/vendorStateList.do")
	public String vendorStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/vendorState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 업무(SERVICE)별 자산현황 화면 로딩 (Asset Statistics Of Service)
	 * This Scene is Asset Statistics Of Service. 
	 * Different than "HiSOS Service status".
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/serviceState.do")
	public String serviceStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/serviceStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/serviceStateList.do")
	public String serviceList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/serviceState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 서비스(SERVICE)별 자산현황 화면 로딩 (Asset Statistics Of HiSOS Service)
	 * This Scene is Asset Statistics Of "HiSOS Service".
	 * Different than "Service status".
	 * This Data is HiSOS Data Interface.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/serviceHiSOSState.do")
	public String serviceHiSOSStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/serviceHiSOSStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/statistics/serviceHiSOSStateList.do")
	public String serviceHiSOSList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/serviceHiSOSState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 유휴(SERVICE) 자산 현황 화면 로딩 (Asset Statistics Of Inactive State)
	 * This Scene is Asset Statistics Of "Inactive State".
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/inactiveState.do")
	public String inactiveStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/inactiveStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/statistics/inactiveStateList.do")
	public String inactiveStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/inactiveState.nvf";
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_nm", userVO.getUser_nm());
	    	}
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	//자산 예약 상태 입력
	@RequestMapping(value="/itg/itam/statistics/insertInactive.do")
	public @ResponseBody ResultVO insertInactive(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	assetStatisticsService.insertInactive(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//자산 예약 상태 취소(삭제)
	@RequestMapping(value="/itg/itam/statistics/deleteInactive.do")
	public @ResponseBody ResultVO deleteInactive(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	assetStatisticsService.deleteInactive(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 폐기 예정(BOOKING DISUSE) 자산 현황 화면 로딩 (Asset Statistics Of Booking disuse State)
	 * This Scene is Asset Statistics Of "Booking disuse State".
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/disuseState.do")
	public String disuseStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/disuseStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/statistics/disuseStateList.do")
	public String disuseStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/disuseState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 운영체제별 자산현황 화면 로딩 (Asset Statistics Of Operation System)
	 * This Scene is Asset Statistics Of Operation System.
	 * It is used by the operating system to view the property.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/osState.do")
	public String osStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/osStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/osStateList.do")
	public String osList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/osState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * Workstation 부서별 자산현황 화면 로딩
	 * This scene is search the asset list for Workstation of department.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/workstationState.do")
	public String workstationStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/workstationStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/workstationStateList.do")
	public String workstationStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/workstationState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * Workstation 상태별 자산현황 화면 로딩
	 * This scene is search the asset list for Workstation statement.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/workstationEstadoState.do")
	public String workstationEstadoStateTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/workstationEstadoStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/workstationEstadoStateList.do")
	public String workstationEstadoStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/workstationEstadoState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 자산별 TCO 현황 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/tcoAssetState.do")
	public String tcoAssetState(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/tcoAssetStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/tcoAssetStateList.do")
	public String tcoAssetStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/tcoAssetState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 업무별 TCO 현황 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/tcoServiceState.do")
	public String tcoServiceState(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/tcoServiceStateTab.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/statistics/tcoServiceStateList.do")
	public String tcoServiceStateList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/tcoServiceState.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/searchExcelDownWica.do")
    public @ResponseBody ResultVO searchPresentExcelDown(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = assetStatisticsService.searchExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산현황 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchAssetTcoList.do")
	public @ResponseBody ResultVO searchAssetTcoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = assetStatisticsService.searchAssetTcoList(paramMap);		
			totalCount = assetStatisticsService.searchAssetTcoListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
