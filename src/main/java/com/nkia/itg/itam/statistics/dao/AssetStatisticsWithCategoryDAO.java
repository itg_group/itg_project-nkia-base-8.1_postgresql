/*
 * @(#)AssetStatisticsWithCategoryDAO.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("assetStatisticsWithCategoryDAO")
public class AssetStatisticsWithCategoryDAO extends EgovComAbstractDAO{
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("AssetStatisticsWithCategoryDAO.searchClassTree", paramMap);
		return resultList;
	}	
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException{
		return list("AssetStatisticsWithCategoryDAO.searchAssetInfo", paramMap);
	}

	public int searchAssetInfoCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsWithCategoryDAO.searchAssetInfoCount", paramMap);
	}

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("AssetStatisticsWithCategoryDAO.searchCodeDataList", paramMap);
		return resultList;
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException{
//		List responseList = list("AssetStatisticsWithCategoryDAO.searchAssetList", paramMap);
//		List resultList = new ArrayList();
//		Map resultMap = new HashMap();
		
		List resultList = list("AssetStatisticsWithCategoryDAO.searchAssetList", paramMap);
		List resultList2 = new ArrayList();
		if(paramMap.containsKey("rnum")){
			Map resultMap = new HashMap();
			
			//List resultList = new List();
			int size = resultList.size();
			for(int i=0; i<size; i++) {
				resultMap = (HashMap)resultList.get(i);
				resultMap.put("RNUM", (String)paramMap.get("rnum")+"-"+(String)resultMap.get("RNUM"));
				resultList2.add(resultMap);
			}
			return resultList2;
		}else{
			return resultList;
		}
		
//		for(int i=0; i<responseList.size(); i++) {
//			resultMap = (HashMap)responseList.get(i);
			
//			if("S".equals(resultMap.get("DIV_HIERACHY"))) {
//				resultMap.put("ASSET_ID", "<font color='blue'>┗" + resultMap.get("ASSET_ID") + "</font>");
//			}
//			resultList.add(resultMap);
//		}
//		return resultList;
	}

	public int searchAssetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsWithCategoryDAO.searchAssetListCount", paramMap);
	}
	
	public List searchOperAssetList(ModelMap paramMap) throws NkiaException{
		List responseList = list("AssetStatisticsWithCategoryDAO.searchOperAssetList", paramMap);
		List resultList = new ArrayList();
		Map resultMap = new HashMap();
		
		for(int i=0; i<responseList.size(); i++) {
			resultMap = (HashMap)responseList.get(i);
			
//			if("S".equals(resultMap.get("DIV_HIERACHY"))) {
//				resultMap.put("ASSET_ID", "<font color='blue'>┗" + resultMap.get("ASSET_ID") + "</font>");
//			}
			resultList.add(resultMap);
		}
		return resultList;
	}

	public int searchOperAssetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsWithCategoryDAO.searchOperAssetListCount", paramMap);
	}

	public List searchExcel(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("AssetStatisticsWithCategoryDAO.searchExcel", paramMap);
		return resultList;
	}	
	
	public List searchAssetListPop(ModelMap paramMap) throws NkiaException{
		List resultList = this.list("AssetStatisticsWithCategoryDAO.searchAssetListPop", paramMap);
		
		return resultList;
	}

	public int searchAssetListPopCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsWithCategoryDAO.searchAssetListPopCount", paramMap);
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
//		String excelGubun = (String)paramMap.get("excelGubun");
		resultList = this.list("AssetStatisticsWithCategoryDAO.searchAssetList", paramMap);	
//		if("yearByIntroCost".equals(excelGubun)){
//			resultList = this.list("assetStatisticsDAO.searchYearByIntroCostExcel", paramMap);			
//		}else if("classTypeLifeCycle".equals(excelGubun)){
//			resultList = this.list("assetStatisticsDAO.searchClassTypeLifeCycleExcel", paramMap);
//		}else if("custTypeClass".equals(excelGubun)){
//			resultList = this.list("assetStatisticsDAO.searchCustTypeClassExcel", paramMap);
//		}
		
		return resultList;
	}
	
	public String selectBookingSeq(ModelMap paramMap) throws NkiaException {
		return (String)selectByPk("AssetStatisticsWithCategoryDAO.selectBookingSeq", paramMap);
	}
	
	public String selectDeleteBookingSeq(ModelMap paramMap) throws NkiaException {
		return (String)selectByPk("AssetStatisticsWithCategoryDAO.selectDeleteBookingSeq", paramMap);
	}
	
	public void insertInactive(ModelMap paramMap) throws NkiaException {
		this.insert("AssetStatisticsWithCategoryDAO.insertInactive", paramMap);
	}
	
	public void deleteInactive(ModelMap paramMap) throws NkiaException {
		this.update("AssetStatisticsWithCategoryDAO.deleteInactive", paramMap);
	}
	
	public void updateInactiveState(ModelMap paramMap) throws NkiaException {
		this.update("AssetStatisticsWithCategoryDAO.updateInactiveState", paramMap);
	}
	
	public List searchAssetTcoList(ModelMap paramMap) throws NkiaException{
//		List responseList = list("AssetStatisticsWithCategoryDAO.searchAssetList", paramMap);
//		List resultList = new ArrayList();
//		Map resultMap = new HashMap();
		
		List resultList = list("AssetStatisticsWithCategoryDAO.searchAssetTcoList", paramMap);
		List resultList2 = new ArrayList();
		if(paramMap.containsKey("rnum")){
			Map resultMap = new HashMap();
			
			//List resultList = new List();
			int size = resultList.size();
			for(int i=0; i<size; i++) {
				resultMap = (HashMap)resultList.get(i);
				resultMap.put("RNUM", (String)paramMap.get("rnum")+"-"+(String)resultMap.get("RNUM"));
				resultList2.add(resultMap);
			}
			return resultList2;
		}else{
			return resultList;
		}
		
//		for(int i=0; i<responseList.size(); i++) {
//			resultMap = (HashMap)responseList.get(i);
			
//			if("S".equals(resultMap.get("DIV_HIERACHY"))) {
//				resultMap.put("ASSET_ID", "<font color='blue'>┗" + resultMap.get("ASSET_ID") + "</font>");
//			}
//			resultList.add(resultMap);
//		}
//		return resultList;
	}

	public int searchAssetTcoListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsWithCategoryDAO.searchAssetTcoListCount", paramMap);
	}
	
}
