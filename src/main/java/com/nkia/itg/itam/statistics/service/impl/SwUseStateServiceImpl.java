/*
 * @(#)SwUseStateServiceImpl.java              2014. 2. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.statistics.dao.SwUseStateDAO;
import com.nkia.itg.itam.statistics.service.SwUseStateService;

@Service("swUseStateService")
public class SwUseStateServiceImpl implements SwUseStateService{
	
	private static final Logger logger = LoggerFactory.getLogger(SwUseStateServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "swUseStateDAO")
	public SwUseStateDAO swUseStateDAO;
	
	
	public List searchSwList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return swUseStateDAO.searchSwList(paramMap);
	}

	public int searchSwListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return swUseStateDAO.searchSwListCount(paramMap);
	}
	
	public List searchRelAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return swUseStateDAO.searchRelAssetList(paramMap);
	}
	
	public int searchRelAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return swUseStateDAO.searchRelAssetListCount(paramMap);
	}

	public List searchSwExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = swUseStateDAO.searchSwExcelDown(paramMap); 
		return resultList;
	}
	
	public List searchSwRelConfExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = swUseStateDAO.searchSwRelConfExcelDown(paramMap); 
		return resultList;
	}

}
