/*
 * @(#)AssetStatisticsController.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.itam.statistics.service.AssetStatisticsSubService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class AssetStatisticsSubController {
	
	@Resource(name = "assetStatisticsSubService")
	private AssetStatisticsSubService assetStatisticsSubService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	
	/**
	 * 탭페이지 오픈
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/statisticsSub/operManager.do")
	public String operManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/operManagerTab_sub.nvf";
		try{		
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));
		} catch (Exception e) {
			throw new NkiaException(e);
		}		
		return forwarPage;
	}

	/**
	 * 자산현황조회 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statisticsSub/assetStatisticsManager.do")
	public String classTypeLifeCycle(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/assetStatisticsManager_sub.nvf";
		try{
			paramMap.put("class_mng_type", request.getParameter("class_mng_type"));		
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 자산현황조회 - 분류체계정보
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statisticsSub/searchClassTree.do")
	public @ResponseBody ResultVO searchClassTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatisticsSubService.searchClassTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자산현황 정보조회
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statisticsSub/searchAssetInfo.do")
	public @ResponseBody ResultVO searchSupply(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = assetStatisticsSubService.searchAssetInfo(paramMap);		
			totalCount = assetStatisticsSubService.searchAssetInfoCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	
	/**
	 * 멀티코드 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/statisticsSub/searchCodeDataList.do")
	public @ResponseBody ResultVO searchCodeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetStatisticsSubService.searchCodeDataList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statisticsSub/searchAssetList.do")
	public @ResponseBody ResultVO searchAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = assetStatisticsSubService.searchAssetList(paramMap);		
			totalCount = assetStatisticsSubService.searchAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 ROW CNT
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statisticsSub/searchAssetListCount.do")
	public @ResponseBody ResultVO searchAssetListCount(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			totalCount = assetStatisticsSubService.searchAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/statisticsSub/searchExcel.do")
	public @ResponseBody ResultVO searchExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			ArrayList classType = new ArrayList();
			classType = (ArrayList)param.get("class_type_arr");
			
			param.put("class_type", "SV");
			List svList  = assetStatisticsSubService.searchExcel(param);
			param.put("class_type", "NW");
			List nwList  = assetStatisticsSubService.searchExcel(param);
			param.put("class_type", "BK");
			List bkList  = assetStatisticsSubService.searchExcel(param);
			param.put("class_type", "SC");
			List scList  = assetStatisticsSubService.searchExcel(param);
			param.put("class_type", "ST");
			List stList  = assetStatisticsSubService.searchExcel(param);
			param.put("class_type", "SW");
			List swList  = assetStatisticsSubService.searchExcel(param);
			param.put("class_type", "ET");
			List etList  = assetStatisticsSubService.searchExcel(param);
			
			for(int i = 0; i < classType.size(); i++){
				String typeName = (String)classType.get(i);
				
				if(typeName.equals("SV")){	resultArray.add(svList);	}
				else if(typeName.equals("NW")){	resultArray.add(nwList);	}
				else if(typeName.equals("BK")){	resultArray.add(bkList);	}
				else if(typeName.equals("SC")){	resultArray.add(scList);	}
				else if(typeName.equals("ST")){	resultArray.add(stList);	}
				else if(typeName.equals("SW")){	resultArray.add(swList);	}
				else if(typeName.equals("ET")){	resultArray.add(etList);	}
				
			}			

			
			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부대관리트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/statisticsSub/searchSubDeptList.do")
	public @ResponseBody ResultVO searchSubDeptList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatisticsSubService.searchSubDeptList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			new NkiaException(e);
		}
		return resultVO;
	}	
}
