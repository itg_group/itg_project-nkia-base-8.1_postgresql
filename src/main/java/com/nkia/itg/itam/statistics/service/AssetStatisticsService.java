/*
 * @(#)AssetStatisticsService.java              2013. 10. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AssetStatisticsService {
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException;
	
	public List searchConfTypeTree(ModelMap paramMap) throws NkiaException;
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException;

	public int searchAssetInfoCount(ModelMap paramMap) throws NkiaException;
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException;
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException;

	public int searchAssetListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchExcelByResource(Map paramMap) throws NkiaException;
	
	public List searchExcel(Map paramMap)throws NkiaException;
	
	public List searchAssetListPop(ModelMap paramMap) throws NkiaException;

	public int searchAssetListPopCount(ModelMap paramMap) throws NkiaException;
	
	// 메인화면용 자산운영현황
	public HashMap searchActiveAssetStateCnt(ModelMap paramMap) throws NkiaException;

	public HashMap updateAssetState(ModelMap paramMap) throws NkiaException;

	public List searchAssetStateChangeResultList(ModelMap paramMap) throws NkiaException;
	
	public String selectSysCode(String column) throws NkiaException;
	
	public List searchClassTypeAttrList(ModelMap paramMap) throws NkiaException;
	
	public List searchDeptTree(ModelMap paramMap) throws NkiaException;
	
	public List searchCustTree(ModelMap paramMap) throws NkiaException;

	public void changeAssetState(ModelMap paramMap) throws NkiaException;

	public void insertAssetDisuse(ModelMap paramMap) throws NkiaException;

	public List searchDisuseAssetList(ModelMap paramMap);
	
	public int searchDisuseAssetListCount(ModelMap paramMap);

	public List searchDisuseAssetExcelDown(Map param);

	public List searchClassTreeForMaint(ModelMap paramMap);

	public List searchMaintAssetList(ModelMap paramMap);

	public int searchMaintAssetListCount(ModelMap paramMap);

	public List searchMaintAssetListExcel(Map param) throws NkiaException;

	public String seelctAssetViewAuth(String loginCustId) throws NkiaException;

	public List searchAssetExcelHeader(HashMap paramMap) throws NkiaException;
	
	public List selectRackInfoList(ModelMap paramMap) throws NkiaException;

	public List selectRackInfoList(HashMap paramMap) throws NkiaException;
	
	public List selectRackInfoExcelList(HashMap paramMap) throws NkiaException;
	
	public List selectChangeRackInfoList(HashMap paramMap) throws NkiaException;
	
	public int selectChangeRackInfoListCount(ModelMap paramMap);
	
	public List searchExcelPoi(Map paramMap)throws Exception;
}
