/*
 * @(#)AssetStatisticsController.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.web;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.csv.CsvMaker;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.excel.ExcelMakerByPoi;
import com.nkia.itg.base.application.util.excel.ExcelRackInfo;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.statistics.service.AssetStatisticsService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AssetStatisticsController {
	
	@Resource(name = "assetStatisticsService")
	private AssetStatisticsService assetStatisticsService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name="excelMakerByPoi")
	private ExcelMakerByPoi excelMakerByPoi;	
	
	@Resource(name = "csvMaker")
	private CsvMaker csvMaker;
	
	@Resource(name="excelRackInfo")
	private ExcelRackInfo excelRackInfo;
	
	/**
	 * 자산현황조회 페이지
	 * 조회타입에 따라서 페이지가 달라진다.(AM/CM/SL)
	 * AM : 자산조회, CM : 구성조회, SL : 파티션/가상화서버 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/assetStatisticsManager.do")
	public String classTypeLifeCycle(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = null;
		Boolean isAuthenticated = false;
		try{
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
			
			//AM(자산)인지 SL(논리)인지 등을 구분.
			String classMngType = request.getParameter("class_mng_type");
			paramMap.put("class_mng_type", classMngType);
			
			//view_type : 관리 화면인지, 조회 화면인지를 구분
			String view_type = StringUtil.parseString(request.getParameter("view_type"));
			paramMap.put("view_type", view_type);
						
			////mng_type : ASSET(IT자원조회), mng_type : CONF(구성조회)
			String mng_type = StringUtil.parseString(request.getParameter("mng_type"));
			paramMap.put("mng_type", mng_type);
			
			//tree_type : 왼쪽 트리의 type(자산구성관리 화면에서만 유효한 파라미터)
			String tree_type = StringUtil.parseString(request.getParameter("tree_type"));
			paramMap.put("tree_type", tree_type);
			
			//conf_kind_cd : 구성분류
			String conf_kind_cd = StringUtil.parseString(request.getParameter("conf_kind_cd"));
			paramMap.put("conf_kind_cd", conf_kind_cd);
			forwarPage = "/itg/itam/statistics/assetStatisticsManagerBy" + classMngType + ".nvf";
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
		
	/**
	 * 자산현황조회 - 분류체계정보
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/statistics/searchClassTree.do")
	public @ResponseBody ResultVO searchClassTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			String mng_type = StringUtil.parseString(paramMap.get("mng_type"));		//자산(ASSET)인지, 구성(CONF)인지, 자산구성(ASSETCONF)인지 구분
			String tree_type = StringUtil.parseString(paramMap.get("tree_type"));
			List resultList = new ArrayList(); 			
			
			if(!"".equals(mng_type)) {
				if("ASSET".equals(mng_type)) {
					//자산 분류체계
					resultList = assetStatisticsService.searchClassTree(paramMap);
					
				} else {
					if("ASSETCONF".equals(mng_type)) {
						if("ASSET".equals(tree_type)) {
							//자산 분류체계
							resultList = assetStatisticsService.searchClassTree(paramMap);
						} else {
							//구성 분류체계
							resultList = assetStatisticsService.searchConfTypeTree(paramMap);
						}
					} else {
						//구성 분류체계
						resultList = assetStatisticsService.searchConfTypeTree(paramMap);
					}										
				}
			} else {
				//자산 분류체계 > 기존과 동일
				resultList = assetStatisticsService.searchClassTree(paramMap);
			}
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자산현황조회 - 유지보수 대상자산 분류체계정보
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/statistics/searchClassTreeForMaint.do")
	public @ResponseBody ResultVO searchClassTreeForMaint(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatisticsService.searchClassTreeForMaint(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	/**
	 * 자산현황 정보조회
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchAssetInfo.do")
	public @ResponseBody ResultVO searchSupply(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = assetStatisticsService.searchAssetInfo(paramMap);		
			totalCount = assetStatisticsService.searchAssetInfoCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	
	/**
	 * 멀티코드 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/statistics/searchCodeDataList.do")
	public @ResponseBody ResultVO searchCodeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetStatisticsService.searchCodeDataList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchAssetList.do")
	public @ResponseBody ResultVO searchAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				String assetViewAuth = assetStatisticsService.seelctAssetViewAuth(loginCustId);
				paramMap.put("loginCustId", loginCustId);
				paramMap.put("assetViewAuth", assetViewAuth);
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			// 콤보박스 검색쿼리 자동 생성을 위한 처리 ( 쿼리는 AND 부터 시작해야 함 )
			String strAutoCombo = StringUtil.parseString(paramMap.get("search_automation_combofield"));
			String strAutoComboValue = StringUtil.parseString(paramMap.get("search_automation_combofield_value"));
			if ( strAutoCombo.equals("") == false && strAutoComboValue.equals("") == false ){
				String [] arrstrAutoCombo = strAutoCombo.split(",");
				String [] arrstrAutoComboValue = strAutoComboValue.split(",");
				if ( arrstrAutoCombo.length > 0 && arrstrAutoCombo.length == arrstrAutoComboValue.length ){
					String strAutoComboQuery = "";
					for ( int a = 0 ; a < arrstrAutoCombo.length ; a++ ){
						strAutoComboQuery += " AND " + arrstrAutoCombo[a] + " = '"+arrstrAutoComboValue[a]+"' ";
					}
					paramMap.put("search_automation_query", strAutoComboQuery);
				}
			}
			// END - 콤보박스 검색쿼리 자동 생성을 위한 처리 ( 쿼리는 AND 부터 시작해야 함 )
			
			List resultList = assetStatisticsService.searchAssetList(paramMap);		
			totalCount = assetStatisticsService.searchAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 ROW CNT
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchAssetListCount.do")
	public @ResponseBody ResultVO searchAssetListCount(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			totalCount = assetStatisticsService.searchAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 리소스로 자산 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchExcelByResource.do")
	public @ResponseBody ResultVO searchExcelByResource(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = assetStatisticsService.searchExcelByResource(param);

			// 엑셀템플릿 자동 생성(By Poi)
			String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim().toUpperCase();			
			if ( excelLib.equals("POI") == true ) {
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultList);
			} else {
				excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			}		

			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/statistics/searchExcel.do")
	public @ResponseBody ResultVO searchExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			ArrayList classType = new ArrayList();
			classType = (ArrayList)param.get("class_type_arr");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						if( !"ASSET_ID".equalsIgnoreCase(column)
							&& !"CONF_ID".equalsIgnoreCase(column)
							&& !"PATH_CLASS_NM".equalsIgnoreCase(column)
						) {
							String columnNm = ","; 
							
							if(htmlType.equals("SELECT") || htmlType.contains("POPUP")){
								if("ID".equals(popupShowType)){
									columnNm += column;
								}else{
									columnNm += ( column + "_NM" );
								}
								
							}else{
								columnNm += column;
							}
							
							columnList += columnNm;
						}
					}
					
					param.put("col_id", columnList);
					
					String typeName = (String)classType.get(k);
					param.put("class_type", typeName);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					
					// 콤보박스 검색쿼리 자동 생성을 위한 처리 ( 쿼리는 AND 부터 시작해야 함 )
					String strAutoCombo = StringUtil.parseString(param.get("search_automation_combofield"));
					String strAutoComboValue = StringUtil.parseString(param.get("search_automation_combofield_value"));
					if ( strAutoCombo.equals("") == false && strAutoComboValue.equals("") == false ){
						String [] arrstrAutoCombo = strAutoCombo.split(",");
						String [] arrstrAutoComboValue = strAutoComboValue.split(",");
						if ( arrstrAutoCombo.length > 0 && arrstrAutoCombo.length == arrstrAutoComboValue.length ){
							String strAutoComboQuery = "";
							for ( int a = 0 ; a < arrstrAutoCombo.length ; a++ ){
								strAutoComboQuery += " AND " + arrstrAutoCombo[a] + " = '"+arrstrAutoComboValue[a]+"' ";
							}
							param.put("search_automation_query", strAutoComboQuery);
						}
					}
					// END - 콤보박스 검색쿼리 자동 생성을 위한 처리 ( 쿼리는 AND 부터 시작해야 함 )
					
					List tempArrayList = assetStatisticsService.searchExcel(param);
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = "ASSET_ID,CONF_ID,PATH_CLASS_NM" + columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			
			// 엑셀템플릿 자동 생성(By Poi)
			String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim().toUpperCase();			
			if ( excelLib.equals("POI") == true ) {
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultArray);
			} else {
				excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			}
			
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}	
	
	/**
	 * 
	 * 자산 CSV파일 다운로드 CSV 예제
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
//	@RequestMapping(value="/itg/itam/statistics/searchAssetListForCsv.do")
//	public @ResponseBody ResultVO searchAssetListForCsv(@RequestBody ModelMap paramMap) throws NkiaException {
//		ResultVO resultVO = new ResultVO();
//		try{
//			ModelMap csvMap = new ModelMap();
//			List resultArray = new ArrayList();
//			resultArray = assetStatisticsService.searchAssetList(paramMap);
//			csvMap = csvMaker.makeCsvFile(paramMap, resultArray);
//			resultVO.setResultMap(csvMap);
//			
//		} catch(Exception e) {
//			throw new NkiaException(e);
//		}
//		return resultVO;
//	}
	
	/**
	 * 
	 * 자산종합현황에서 엑셀다운로드
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/statistics/searchAmSummaryExcel.do")
	public @ResponseBody ResultVO searchAmSummaryExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			resultArray.add(assetStatisticsService.searchExcel(param));	
			
			// 엑셀템플릿 자동 생성(By Poi)
			String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim().toUpperCase();			
			if ( excelLib.equals("POI") == true ) {
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultArray);
			} else {
				excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			}			
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 팝업 리스트(모품 선택용)
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchAssetListPop.do")
	public @ResponseBody ResultVO searchAssetListPop(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = assetStatisticsService.searchAssetListPop(paramMap);		
			totalCount = assetStatisticsService.searchAssetListPopCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 메인화면 자산운영현황
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchActiveAssetStateCnt.do")
	public @ResponseBody ResultVO searchActiveAssetStateCnt(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = assetStatisticsService.searchActiveAssetStateCnt(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/statistics/updateAssetState.do")
	public @ResponseBody ResultVO updateAssetState(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = assetStatisticsService.updateAssetState(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/statistics/searchAssetStateChangeResultList.do")
	public @ResponseBody ResultVO searchAssetStateChangeResultList(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
					
			resultList = assetStatisticsService.searchAssetStateChangeResultList(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 엑셀다운로드 분류체계별 속성목록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchClassTypeAttrList.do")
	public @ResponseBody ResultVO searchClassTypeAttrList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetStatisticsService.searchClassTypeAttrList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서조회트리 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchDeptTree.do")
	public @ResponseBody ResultVO searchDeptTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		TreeVO treeVO = null;
		List resultList = new ArrayList();
		try{
			resultList = assetStatisticsService.searchDeptTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 계열사 조회 트리 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchCustTree.do")
	public @ResponseBody ResultVO searchCustTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		TreeVO treeVO = null;
		List resultList = new ArrayList();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			resultList = assetStatisticsService.searchCustTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산상태변경
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/statistics/changeAssetState.do")
	public @ResponseBody ResultVO insertMaintJob(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetStatisticsService.changeAssetState(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산현황 리스트 (페이징없는 그리드용)
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchAssetListNoPaging.do")
	public @ResponseBody ResultVO searchAssetListNoPaging(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			PagingUtil.setSortColumn(paramMap);
			
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
					
			resultList = assetStatisticsService.searchAssetList(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산상태변경
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/statistics/insertAssetDisuse.do")
	public @ResponseBody ResultVO insertAssetDisuse(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetStatisticsService.insertAssetDisuse(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 폐기자산 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchDisuseAssetList.do")
	public @ResponseBody ResultVO searchDisuseAssetList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			List resultList = assetStatisticsService.searchDisuseAssetList(paramMap);		
			totalCount = assetStatisticsService.searchDisuseAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 폐기자산 엑셀다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchDisuseAssetExcelDown.do")
	public @ResponseBody ResultVO searchDisuseAssetExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = assetStatisticsService.searchDisuseAssetExcelDown(param);
			// 엑셀템플릿 자동 생성(By Poi)
			String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim().toUpperCase();			
			if ( excelLib.equals("POI") == true ) {
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultList);
			} else {
				excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			}

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 유지보수 대상자산 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/searchMaintAssetList.do")
	public @ResponseBody ResultVO searchMaintAssetList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = assetStatisticsService.searchMaintAssetList(paramMap);		
			totalCount = assetStatisticsService.searchMaintAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 유지보수대상자산 엑셀 다운로드
	 */
	@RequestMapping(value="/itg/itam/statistics/searchMaintAssetListExcel.do")
	public @ResponseBody ResultVO searchMaintAssetListExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			resultArray.add(assetStatisticsService.searchMaintAssetListExcel(param));	

			// 엑셀템플릿 자동 생성(By Poi)
			String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim().toUpperCase();			
			if ( excelLib.equals("POI") == true ) {
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultArray);
			} else {
				excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			}			

			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/statistics/searchAssetExcelHeader.do")
	public @ResponseBody ResultVO searchAssetExcelHeader(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = new ArrayList();
			
			resultList = assetStatisticsService.searchAssetExcelHeader(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 랙정보 목록 화면
	 * @return
	 * @throws NkiaException
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value="/itg/itam/rack/rackInfoList.do")
	public String goRackInfo(HttpServletRequest request, ModelMap paramMap) throws NkiaException, UnsupportedEncodingException {
		String forwarPage = "/itg/itam/rack/rackInfo.nvf";
		
		paramMap.put("center_position_cd", request.getParameter("center_position_cd"));
		paramMap.put("center_position_cd_nm", new String(StringUtil.parseString(request.getParameter("center_position_cd_nm")).getBytes("ISO-8859-1"),"UTF-8"));
		paramMap.put("change_date_startDate", request.getParameter("change_date_startDate"));
		paramMap.put("change_date_endDate", request.getParameter("change_date_endDate"));
		
		return forwarPage;
	}
	
	/**
	 * 랙정보 목록 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/rack/searchRackInfoList.do")
	public @ResponseBody ResultVO searchRackInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			// 랙정보 수정대상 체크항목
			String rackInfoChgColumns = NkiaApplicationPropertiesMap.getProperty("Globals.Change.Rack.Info.Columns");
			
			if(rackInfoChgColumns != null) {
				String[] rackInfoChgColumnsArr = rackInfoChgColumns.split(",");
				paramMap.put("rack_info_chg_col_list", rackInfoChgColumnsArr);
			}
					
			resultList = assetStatisticsService.selectRackInfoList(paramMap);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 랙 실장도 다운로드
	 * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/itam/rack/searchRackInfoExcel.do")
	public @ResponseBody ResultVO rackInfoDownload(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		
		try{
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				param.put("user_id", userVO.getUser_id());
			}
			
			List resultList = assetStatisticsService.selectRackInfoExcelList(param);
			excelMap = excelRackInfo.makeExcelFileXlsx(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 랙 수정정보 목록 화면
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/rack/changeRackInfoList.do")
	public String goChangeRackInfo(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/rack/changeRackInfo.nvf";
		return forwarPage;
	}
	
	/**
	 * 랙 수정정보 목록 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/rack/searchChangeRackInfoList.do")
	public @ResponseBody ResultVO searchChangeRackInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			// 랙정보 수정대상 체크항목
			String rackInfoChgColumns = NkiaApplicationPropertiesMap.getProperty("Globals.Change.Rack.Info.Columns");
			
			if(rackInfoChgColumns != null) {
				String[] rackInfoChgColumnsArr = rackInfoChgColumns.split(",");
				paramMap.put("rack_info_chg_col_list", rackInfoChgColumnsArr);
			}
			
			List resultList = assetStatisticsService.selectChangeRackInfoList(paramMap);		
			int totalCount = assetStatisticsService.selectChangeRackInfoListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경대상 자산 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/batchEditAssetExcelDown.do")
	public @ResponseBody ResultVO batchEditAssetExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Boolean isAuthenticated = false;
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			String classType = (String) param.get("class_type_arr");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			HashMap sheetMap = (HashMap) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			String columns = (String) sheetMap.get("includeColumns");
			
			if(null != columns){
				
				String newColumnList = "";
				
				param.put("col_id", columns);
				param.put("class_type", classType);
				
				isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
				if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		param.put("user_id", userVO.getUser_id());
		    	}
				
				List assetList = assetStatisticsService.searchExcelByResource(param);
				resultArray.add(assetList);
				
				newSheetList.add(sheetMap);
				
			} 
			else {
				resultArray.add(null);
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			// 엑셀템플릿 자동 생성(By Poi)
			String excelLib = StringUtil.parseString( NkiaApplicationPropertiesMap.getProperty("Itam.Template.Excel.Lib") ).trim().toUpperCase();			
			if ( excelLib.equals("POI") == true ) {
				excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultArray);
			} else {
				excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			}		
			
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
}
