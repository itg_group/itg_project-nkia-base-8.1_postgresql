/*
 * @(#)integrationServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.statistics.dao.InfraDAO;
import com.nkia.itg.itam.statistics.dao.IntegrationDAO;
import com.nkia.itg.itam.statistics.service.IntegrationService;

@Service("integrationService")
public class IntegrationServiceImpl implements IntegrationService{
	
	private static final Logger logger = LoggerFactory.getLogger(IntegrationServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "integrationDAO")
	public IntegrationDAO integrationDAO;
	
	@Resource(name = "infraDAO")
	public InfraDAO infraDAO;
	
	public List searchCustTypeClassChart(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList =  integrationDAO.searchCustTypeClassChart(paramMap);
		return resultList;
	}
	
	public List searchCustTypeClassList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = integrationDAO.searchCustTypeClassList(paramMap);
		return resultList;
	}
	
	public List searchIntegrationList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = integrationDAO.searchIntegrationList(paramMap);
		return resultList;
	}

	public List searchExcel(Map paramMap) throws NkiaException {
		List resultList = null;
		List subList = null;
		
		resultList = integrationDAO.searchExcel(paramMap); 
		
		return resultList;
	}	

}
