package com.nkia.itg.itam.statistics.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SvBlacklistService {
	
	List searchSvConfBlacklist(HashMap paramMap) throws NkiaException;

	List searchIfAdBlacklist(HashMap paramMap) throws NkiaException;

	int searchSvConfBlacklistCount(HashMap paramMap) throws NkiaException;

	int searchIfAdBlacklistCount(HashMap paramMap) throws NkiaException;

}
