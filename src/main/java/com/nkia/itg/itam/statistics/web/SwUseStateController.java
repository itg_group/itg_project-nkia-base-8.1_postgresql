/*
 * @(#)SwUseStateController.java              2014. 2. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.statistics.service.SwUseStateService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class SwUseStateController {
	
	@Resource(name = "swUseStateService")
	private SwUseStateService swUseStateService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 소프트웨어 사용현황 화면 출력
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/swUseState.do")
	public String classTypeLifeCycle(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/swUseState.nvf";
		return forwarPage;
	}
	
	
	/**
	 * 소프트웨어 사용현황 - 소프트웨어 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchSwList.do")
	public @ResponseBody ResultVO searchSwList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			resultList = swUseStateService.searchSwList(paramMap);		
			totalCount = swUseStateService.searchSwListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 소프트웨어 사용현황 - 연계장비 목록
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/searchRelAssetList.do")
	public @ResponseBody ResultVO searchRelAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = swUseStateService.searchRelAssetList(paramMap);		
			totalCount = swUseStateService.searchRelAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * 소프트웨어 사용현황 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/searchSwExcelDown.do")
    public @ResponseBody ResultVO searchSwExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = swUseStateService.searchSwExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 소프트웨어 사용현황 - 연계자산 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/searchSwRelConfExcelDown.do")
	public @ResponseBody ResultVO searchSwRelConfExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");  
			Map param = (Map) paramMap.get("param");
			
			List resultList = swUseStateService.searchSwRelConfExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
