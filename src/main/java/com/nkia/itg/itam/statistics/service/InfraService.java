/*
 * @(#)maintService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface InfraService {

	public List searchCustTypeLifeCycleChart(ModelMap paramMap) throws NkiaException;
	
	public List searchCustTypeLifeCycleList(ModelMap paramMap) throws NkiaException;
	
	public List searchCustTypeClassChart(ModelMap paramMap) throws NkiaException;
	
	public List searchCustTypeClassList(ModelMap paramMap) throws NkiaException;
	
	public List searchYearTypeIntroductionList(ModelMap paramMap) throws NkiaException;
	
	public List searchTermByIntroCostList(ModelMap paramMap) throws NkiaException;	
	
	public List searchYearByIntroCostChart(ModelMap paramMap) throws NkiaException;
	
	public List searchYearByIntroCostList(ModelMap paramMap) throws NkiaException;
	
	public List searchPresentExcelDown(Map paramMap) throws NkiaException;

	public List searchCustByAssetStateList(ModelMap paramMap) throws NkiaException;

	public List searchCustByAssetStateChart(ModelMap paramMap)throws NkiaException;

	public List searchClassTypeByYearList(ModelMap paramMap)throws NkiaException;

	public List searchClassTypeByYearChart(ModelMap paramMap)throws NkiaException;
}
