package com.nkia.itg.itam.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AssetStatsService {

	// 엑셀다운로드(통계 공통)
	public List searchPresentExcelDown(Map paramMap) throws NkiaException;
	
	// 분류체계별 자산현황(life cyle) 차트 조회
	public List searchClassTypeLifeCycleChart(ModelMap paramMap) throws NkiaException;
	
	// 분류체계별 자산현황(life cyle) 트리그리드 목록 조회
	public List searchClassTypeLifeCycleList(ModelMap paramMap) throws NkiaException;

	// 연도별 자산도입현황 차트 조회
	public List introStatsClassTypeByYearChart(ModelMap paramMap) throws NkiaException;

	public List introStatsClassTypeByYearList(ModelMap paramMap) throws NkiaException;

	//자산위치별 현황 목록 조회
	public List searchAssetLocByClassTypeList(ModelMap paramMap) throws NkiaException;

	//자산위치별 현황 차트
	public List searchAssetLocByClassTypeChart(ModelMap paramMap) throws NkiaException;
	
	//자산위치별 분류체계 조회
	public List searchClassTypeList(ModelMap paramMap) throws NkiaException;

	// 폐기자산 현황 목록 조회
	public List disuseAssetClassTypeByYearList(ModelMap paramMap) throws NkiaException;
	
	// 폐기자산 현황 차트
	public List disuseAssetClassTypeByYearChart(ModelMap paramMap) throws NkiaException;

	// 위치별 논리서버 운영현황 목록 조회
	public List logicServerLocByOsTypeList(ModelMap paramMap) throws NkiaException;
	
	// 위치별 논리서버 운영현황 차트 조회
	public List logicServerLocByOsTypeChart(ModelMap paramMap) throws NkiaException;
	
	
}
