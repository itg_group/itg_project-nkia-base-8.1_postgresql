package com.nkia.itg.itam.statistics.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.statistics.service.SvBlacklistService;

@Controller
public class SvBlacklistController {

	@Resource(name="svBlacklistService")
	private SvBlacklistService svBlacklistService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@RequestMapping("/itg/itam/statistics/goSvBlacklistTab.do")
	public String goSvBlacklistTab(ModelMap paramMap) throws NkiaException{
		String forwarPage = "/itg/itam/statistics/svBlacklistTab.nvf";
		return forwarPage;
	}
	
	@RequestMapping("/itg/itam/statistics/goSvBlacklist.do")
	public String goSvBlacklist(ModelMap paramMap) throws NkiaException{
		String forwarPage = "/itg/itam/statistics/svBlacklist.nvf";
		return forwarPage;
	}
	
	@RequestMapping("/itg/itam/statistics/searchSvBlacklist.do")
	public @ResponseBody ResultVO searchSvBlacklist(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			
			GridVO gridVO = new GridVO();
			List resultList = new ArrayList();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			// 서버 구성 내에 불일치 목록 조회
			resultList = svBlacklistService.searchSvConfBlacklist(paramMap);
			totalCount = svBlacklistService.searchSvConfBlacklistCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
	@RequestMapping("/itg/itam/statistics/searchIfAdBlacklist.do")
	public @ResponseBody ResultVO searchIfAdBlacklist(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			
			GridVO gridVO = new GridVO();
			List resultList = new ArrayList();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			// EMS 수집정보 불일치 목록 조회
			resultList = svBlacklistService.searchIfAdBlacklist(paramMap);
			totalCount = svBlacklistService.searchIfAdBlacklistCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
	
	
	@RequestMapping(value="/itg/itam/statistics/searchSvBlacklistExcelDown.do")
	public @ResponseBody ResultVO searchSvBlacklistExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");
			
			List resultList = new ArrayList();
			// 서버 구성 내에 불일치 목록 조회
			resultList = svBlacklistService.searchSvConfBlacklist(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/statistics/searchSvConfBlacklistExcelDown.do")
	public @ResponseBody ResultVO searchSvConfBlacklistExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");
			
			List resultList = new ArrayList();
			// EMS 수집정보 불일치 목록 조회
			resultList = svBlacklistService.searchIfAdBlacklist(param);
			
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
}
