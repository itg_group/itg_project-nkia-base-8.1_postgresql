/*
 * @(#)IntegrationController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.itam.statistics.service.InfraService;
import com.nkia.itg.itam.statistics.service.IntegrationService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class IntegrationController {
	
	@Resource(name = "integrationService")
	private IntegrationService integrationService;
	
	@Resource(name = "infraService")
	private InfraService infraService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 통합자산현황
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/integration/integrationAsset.do")
	public String classTypeLifeCycle(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/integrationAsset.nvf";
		return forwarPage;
	}
	
	/**
	 * 부서별 분류체계 차트
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/integration/searchCustTypeClassChart.do")
	public @ResponseBody ResultVO searchCustTypeClassChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = integrationService.searchCustTypeClassChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서별 분류체계 리스트
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/integration/searchCustTypeClassList.do")
	public @ResponseBody ResultVO searchCustTypeClassList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = integrationService.searchCustTypeClassList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
		
	}
	
	/**
	 * 통합조회 리스트
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/integration/searchIntegrationList.do")
	public @ResponseBody ResultVO searchIntegrationList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = integrationService.searchIntegrationList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/integration/searchExcel.do")
    public @ResponseBody ResultVO searchExcel(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = integrationService.searchExcel(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

}
