package com.nkia.itg.itam.statistics.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.statistics.service.SwStatisticsService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class SwStatisticsController {
	
	@Resource(name="swStatisticsService")
	private SwStatisticsService swStatisticsService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * PC 소프트웨어 사용현황 화면 출력
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/pcSwUseState.do")
	public String pcSwUseState(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/pcSwUseState.nvf";
		return forwarPage;
	}
	
	/**
	 * 연계 S/W 현황
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/swAssetList.do")
	public @ResponseBody ResultVO swAssetList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			
			List resultList = swStatisticsService.swAssetList(paramMap);

			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 연계 S/W 현황 엑셀다운
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/swAssetListExcel.do")
	public @ResponseBody ResultVO swAssetListExcel(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = swStatisticsService.swAssetListExcel(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * PC 현황 목록 
	 * [PC SW 현황]
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/selectPcAssetList.do")
	public @ResponseBody ResultVO selectPcAssetList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;

			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = swStatisticsService.selectPcAssetListCount(paramMap);
			List resultList = swStatisticsService.selectPcAssetList(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서버 소프트웨어 사용현황 화면 출력
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/serverSwUseState.do")
	public String serverSwUseState(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/serverSwUseState.nvf";
		return forwarPage;
	}
	
	/**
	 * 서버 현황 목록
	 * [서버 SW 현황]
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/selectServerAssetList.do")
	public @ResponseBody ResultVO selectServerAssetList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;

			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = swStatisticsService.selectServerAssetListCount(paramMap);
			List resultList = swStatisticsService.selectServerAssetList(paramMap);
			gridVO.setRows(resultList);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서버 현황 엑셀 다운
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/selectServerAssetListExcel.do")
	public @ResponseBody ResultVO selectServerAssetListExcel(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = swStatisticsService.selectServerAssetListExcel(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * PC 현황 엑셀 다운
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/selectPcAssetListExcel.do")
	public @ResponseBody ResultVO selectPcAssetListExcel(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = swStatisticsService.selectPcAssetListExcel(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * PC 소프트웨어 라이센스 할당 된 PC자산 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/selectAssignPcAssetList.do")
	public @ResponseBody ResultVO selectAssignPcAssetList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;

			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = swStatisticsService.selectAssignPcAssetListCount(paramMap);
			List resultList = swStatisticsService.selectAssignPcAssetList(paramMap);
			
			gridVO.setRows(resultList);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * PC 소프트웨어 라이센스 회수
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/deleteSwAssignLicence.do")
	public @ResponseBody ResultVO deleteSwAssignLicence(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			swStatisticsService.deleteSwAssignLicence(paramMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * PC 소프트웨어 라이선스 할당
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/insertSwAssignLicence.do")
	public @ResponseBody ResultVO insertSwAssignLicence(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			swStatisticsService.insertSwAssignLicence(paramMap);		
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
