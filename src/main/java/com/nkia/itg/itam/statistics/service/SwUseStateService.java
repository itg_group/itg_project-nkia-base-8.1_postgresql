/*
 * @(#)SwUseStateService.java              2013. 2. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SwUseStateService {

	public List searchSwList(ModelMap paramMap) throws NkiaException;
	
	public int searchSwListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchRelAssetList(ModelMap paramMap) throws NkiaException;
	
	public int searchRelAssetListCount(ModelMap paramMap) throws NkiaException;

	public List searchSwExcelDown(Map paramMap) throws NkiaException;
	
	public List searchSwRelConfExcelDown(Map paramMap) throws NkiaException;
}
