package com.nkia.itg.itam.statistics.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("svBlacklistDAO")
public class SvBlacklistDAO extends EgovComAbstractDAO{


	public List searchSvConfBlacklist(HashMap paramMap) {
		return list("SvBlacklistDAO.searchSvConfBlacklist", paramMap);
	}

	public List searchIfAdBlacklist(HashMap paramMap) {
		return list("SvBlacklistDAO.searchIfAdBlacklist", paramMap);
	}

	public int searchSvConfBlacklistCount(HashMap paramMap) {
		return (int) selectByPk("SvBlacklistDAO.searchSvConfBlacklistCount", paramMap);
	}

	public int searchIfAdBlacklistCount(HashMap paramMap) {
		return (int) selectByPk("SvBlacklistDAO.searchIfAdBlacklistCount", paramMap);
	}

}
