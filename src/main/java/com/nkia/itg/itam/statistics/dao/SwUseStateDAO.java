/*
 * @(#)SwUseStateDAO.java              2014. 2. 24.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("swUseStateDAO")
public class SwUseStateDAO extends EgovComAbstractDAO{
	
	public List searchSwList(ModelMap paramMap) throws NkiaException{
		return list("swUseStateDAO.searchSwList", paramMap);
	}

	public int searchSwListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("swUseStateDAO.searchSwListCount", paramMap);
	}
	
	public List searchRelAssetList(ModelMap paramMap) throws NkiaException{
		return list("swUseStateDAO.searchRelAssetList", paramMap);
	}
	
	public int searchRelAssetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("swUseStateDAO.searchRelAssetListCount", paramMap);
	}
	
	public List searchSwExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("swUseStateDAO.searchSwList", paramMap);
		return resultList;
	}		
	
	public List searchSwRelConfExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("swUseStateDAO.searchRelAssetList", paramMap);
		return resultList;
	}		
}
