package com.nkia.itg.itam.statistics.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.statistics.dao.AssetStatsDAO;
import com.nkia.itg.itam.statistics.service.AssetStatsService;

@Service("assetStatsService")
public class AssetStatsServiceImpl implements AssetStatsService{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetStatsServiceImpl.class);

	@Resource(name="assetStatsDAO")
	private AssetStatsDAO assetStatsDAO;

	@Override
	public List searchPresentExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = assetStatsDAO.searchPresentExcelDown(paramMap); 
		return resultList;
	}

	@Override
	public List searchClassTypeLifeCycleChart(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = assetStatsDAO.searchClassTypeLifeCycleChart(paramMap);
		return resultList;
	}

	@Override
	public List searchClassTypeLifeCycleList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList =  assetStatsDAO.searchClassTypeLifeCycleList(paramMap);
		return resultList;
	}

	/**
	 * 연도별 도입현황(수량/금액) 차트 조회
	 */
	@Override
	public List introStatsClassTypeByYearChart(ModelMap paramMap) throws NkiaException {
		
		String searchStatsIntroType = "COUNT"; // 기본값 수량
		if(paramMap.containsKey("search_stats_intro")){
			searchStatsIntroType = (String) paramMap.get("search_stats_intro");
		}
		
		int baseYear = (Integer) paramMap.get("baseYear");
		paramMap.put("nowYear", StringUtil.parseString(baseYear));
		paramMap.put("first_year_ago", StringUtil.parseString(baseYear - 1));
		paramMap.put("two_year_ago", StringUtil.parseString(baseYear - 2));
		paramMap.put("three_year_ago", StringUtil.parseString(baseYear - 3));
		paramMap.put("four_year_ago", StringUtil.parseString(baseYear - 4));
		paramMap.put("five_year_ago", StringUtil.parseString(baseYear - 5));
		paramMap.put("six_year_ago", StringUtil.parseString(baseYear - 6));
		
		List resultList = null;
		if("COUNT".equals(searchStatsIntroType)){	//연도별 도입수량 현황 조회
			resultList = assetStatsDAO.searchIntroCountClassTypeByYearChart(paramMap);
		}else if("COST".equals(searchStatsIntroType)){	// 연도별 도입금액 현황 조회
			resultList = assetStatsDAO.searchIntroCostClassTypeByYearChart(paramMap);
		}else{
			resultList = assetStatsDAO.searchIntroCountClassTypeByYearChart(paramMap);
		}
		
				
		return resultList;
	}

	/**
	 * 연도별 도입현황(수량/금액) 목록 조회
	 */
	@Override
	public List introStatsClassTypeByYearList(ModelMap paramMap) throws NkiaException {
		String searchStatsIntroType = "COUNT"; // 기본값 수량
		if(paramMap.containsKey("search_stats_intro")){
			searchStatsIntroType = (String) paramMap.get("search_stats_intro");
		}
		
		int baseYear = (Integer) paramMap.get("baseYear");
		paramMap.put("nowYear", StringUtil.parseString(baseYear));
		paramMap.put("first_year_ago", StringUtil.parseString(baseYear - 1));
		paramMap.put("two_year_ago", StringUtil.parseString(baseYear - 2));
		paramMap.put("three_year_ago", StringUtil.parseString(baseYear - 3));
		paramMap.put("four_year_ago", StringUtil.parseString(baseYear - 4));
		paramMap.put("five_year_ago", StringUtil.parseString(baseYear - 5));
		paramMap.put("six_year_ago", StringUtil.parseString(baseYear - 6));
		
		List resultList = null;
		if("COUNT".equals(searchStatsIntroType)){
			resultList = assetStatsDAO.searchIntroCountClassTypeByYearList(paramMap);
		}else if("COST".equals(searchStatsIntroType)){
			resultList = assetStatsDAO.searchIntroCostClassTypeByYearList(paramMap);
		}else{
			resultList = assetStatsDAO.searchIntroCountClassTypeByYearList(paramMap);
		}
		
		return resultList;
	}

	/**
	 * 자산위치별 수량 목록 조회
	 */
	@Override
	public List searchAssetLocByClassTypeList(ModelMap paramMap) throws NkiaException {
		return assetStatsDAO.searchAssetLocByClassTypeList(paramMap);
	}
	
	/**
	 * 자산위치별 분류체계 조회
	 */
	@Override
	public List searchClassTypeList(ModelMap paramMap) throws NkiaException {
		return assetStatsDAO.searchClassTypeList(paramMap);
	}

	/**
	 * 자산위치별 현황 차트
	 */
	@Override
	public List searchAssetLocByClassTypeChart(ModelMap paramMap) throws NkiaException {
		return assetStatsDAO.searchAssetLocByClassTypeChart(paramMap);
	}

	/**
	 * 폐기자산 현황 목록 조회
	 */
	@Override
	public List disuseAssetClassTypeByYearList(ModelMap paramMap) throws NkiaException {
		int baseYear = (Integer) paramMap.get("baseYear");
		paramMap.put("nowYear", StringUtil.parseString(baseYear));
		paramMap.put("first_year_ago", StringUtil.parseString(baseYear - 1));
		paramMap.put("two_year_ago", StringUtil.parseString(baseYear - 2));
		paramMap.put("three_year_ago", StringUtil.parseString(baseYear - 3));
		paramMap.put("four_year_ago", StringUtil.parseString(baseYear - 4));
		paramMap.put("five_year_ago", StringUtil.parseString(baseYear - 5));
		paramMap.put("six_year_ago", StringUtil.parseString(baseYear - 6));
		
		List resultList = assetStatsDAO.disuseAssetClassTypeByYearList(paramMap);
		
		return resultList;
	}

	
	/**
	 * 폐기자산 현황 차트
	 */
	@Override
	public List disuseAssetClassTypeByYearChart(ModelMap paramMap) throws NkiaException {
		int baseYear = (Integer) paramMap.get("baseYear");
		paramMap.put("nowYear", StringUtil.parseString(baseYear));
		paramMap.put("first_year_ago", StringUtil.parseString(baseYear - 1));
		paramMap.put("two_year_ago", StringUtil.parseString(baseYear - 2));
		paramMap.put("three_year_ago", StringUtil.parseString(baseYear - 3));
		paramMap.put("four_year_ago", StringUtil.parseString(baseYear - 4));
		paramMap.put("five_year_ago", StringUtil.parseString(baseYear - 5));
		paramMap.put("six_year_ago", StringUtil.parseString(baseYear - 6));
		
		List resultList = assetStatsDAO.disuseAssetClassTypeByYearChart(paramMap);
		
		return resultList;
	}

	/**
	 * 위치별 논리서버 운영현황 목록
	 */
	@Override
	public List logicServerLocByOsTypeList(ModelMap paramMap) throws NkiaException {
		return assetStatsDAO.logicServerLocByOsTypeList(paramMap);
	}

	/**
	 * 위치별 논리서버 운영현황 차트
	 */
	@Override
	public List logicServerLocByOsTypeChart(ModelMap paramMap) throws NkiaException {
		return assetStatsDAO.logicServerLocByOsTypeChart(paramMap);
	}
	
	
}
