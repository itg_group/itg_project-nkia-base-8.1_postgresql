package com.nkia.itg.itam.statistics.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("assetStatsDAO")
public class AssetStatsDAO extends EgovComAbstractDAO {

	public List searchPresentExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		String excelGubun = (String)paramMap.get("excelGubun");
		if("classTypeLifeCycle".equals(excelGubun)){	
			//분류체계별 자산현황(라이프사이클)
			resultList = this.list("AssetStatsDAO.searchClassTypeLifeCycleExcel", paramMap);
		}else if("introCountClassTypeByYear".equals(excelGubun)){
			// 연도별 자산도입 수량 현황
			int baseYear = (Integer) paramMap.get("baseYear");
			paramMap.put("nowYear", StringUtil.parseString(baseYear));
			paramMap.put("first_year_ago", StringUtil.parseString(baseYear - 1));
			paramMap.put("two_year_ago", StringUtil.parseString(baseYear - 2));
			paramMap.put("three_year_ago", StringUtil.parseString(baseYear - 3));
			paramMap.put("four_year_ago", StringUtil.parseString(baseYear - 4));
			paramMap.put("five_year_ago", StringUtil.parseString(baseYear - 5));
			paramMap.put("six_year_ago", StringUtil.parseString(baseYear - 6));
			resultList = this.list("AssetStatsDAO.searchIntroCountClassTypeByYearExcel", paramMap);
		}else if("introCostClassTypeByYear".equals(excelGubun)){
			// 연도별 자산도입 금액 현황
			int baseYear = (Integer) paramMap.get("baseYear");
			paramMap.put("nowYear", StringUtil.parseString(baseYear));
			paramMap.put("first_year_ago", StringUtil.parseString(baseYear - 1));
			paramMap.put("two_year_ago", StringUtil.parseString(baseYear - 2));
			paramMap.put("three_year_ago", StringUtil.parseString(baseYear - 3));
			paramMap.put("four_year_ago", StringUtil.parseString(baseYear - 4));
			paramMap.put("five_year_ago", StringUtil.parseString(baseYear - 5));
			paramMap.put("six_year_ago", StringUtil.parseString(baseYear - 6));
			resultList = this.list("AssetStatsDAO.searchIntroCostClassTypeByYearExcel", paramMap);
		}else if("disuseAssetClassTypeByYear".equals(excelGubun)){
			// 연도별 폐기자산 현황
			int baseYear = (Integer) paramMap.get("baseYear");
			paramMap.put("nowYear", StringUtil.parseString(baseYear));
			paramMap.put("first_year_ago", StringUtil.parseString(baseYear - 1));
			paramMap.put("two_year_ago", StringUtil.parseString(baseYear - 2));
			paramMap.put("three_year_ago", StringUtil.parseString(baseYear - 3));
			paramMap.put("four_year_ago", StringUtil.parseString(baseYear - 4));
			paramMap.put("five_year_ago", StringUtil.parseString(baseYear - 5));
			paramMap.put("six_year_ago", StringUtil.parseString(baseYear - 6));
			resultList = this.list("AssetStatsDAO.disuseAssetClassTypeByYearExcel", paramMap);
		}else if("logicServerLocByOsType".equals(excelGubun)){
			// 논리서버 운영현황
			resultList = this.list("AssetStatsDAO.logicServerLocByOsTypeExcel", paramMap);
		}else if("assetLocByclassType".equals(excelGubun)){
			//자산위치별 현황
			resultList = this.list("AssetStatsDAO.searchAssetLocByClassTypeExcel", paramMap);
		}else if("disuseAssetClassTypeByYear".equals(excelGubun)){
			//폐기자산 현황
			resultList = this.list("AssetStatsDAO.disuseAssetClassTypeByYearExcel", paramMap);
		}
		return resultList;
	}
	
	public List searchClassTypeLifeCycleChart(ModelMap paramMap) throws NkiaException{
		return list("AssetStatsDAO.searchClassTypeLifeCycleChart", paramMap);
	}
	
	public List searchClassTypeLifeCycleList(ModelMap paramMap) throws NkiaException{
		return list("AssetStatsDAO.searchClassTypeLifeCycleList", paramMap);
	}

	public List searchIntroCountClassTypeByYearChart(ModelMap paramMap) throws NkiaException{
		return list("AssetStatsDAO.searchIntroCountClassTypeByYearChart", paramMap);
	}

	public List searchIntroCountClassTypeByYearList(ModelMap paramMap)throws NkiaException {
		return list("AssetStatsDAO.searchIntroCountClassTypeByYearList", paramMap);
	}

	public List searchIntroCostClassTypeByYearList(ModelMap paramMap) throws NkiaException{
		return list("AssetStatsDAO.searchIntroCostClassTypeByYearList", paramMap);
	}

	public List searchIntroCostClassTypeByYearChart(ModelMap paramMap) {
		return list("AssetStatsDAO.searchIntroCostClassTypeByYearChart", paramMap);
	}

	public List searchAssetLocByClassTypeList(ModelMap paramMap) {
		return list("AssetStatsDAO.searchAssetLocByClassTypeList", paramMap);
	}

	public List searchAssetLocByClassTypeChart(ModelMap paramMap) {
		return list("AssetStatsDAO.searchAssetLocByClassTypeChart", paramMap);
	}

	public List disuseAssetClassTypeByYearList(ModelMap paramMap) {
		return list("AssetStatsDAO.disuseAssetClassTypeByYearList", paramMap);
	}
	
	public List disuseAssetClassTypeByYearChart(ModelMap paramMap) {
		return list("AssetStatsDAO.disuseAssetClassTypeByYearChart", paramMap);
	}

	public List logicServerLocByOsTypeList(ModelMap paramMap) {
		return list("AssetStatsDAO.logicServerLocByOsTypeList", paramMap);
	}
	
	public List logicServerLocByOsTypeChart(ModelMap paramMap) {
		return list("AssetStatsDAO.logicServerLocByOsTypeChart", paramMap);
	}
	
	public List searchClassTypeList(ModelMap paramMap) {
		return list("AssetStatsDAO.searchClassTypeList", paramMap);
	}
	
}
