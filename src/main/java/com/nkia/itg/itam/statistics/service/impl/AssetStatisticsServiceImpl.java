/*
 * @(#)AssetStatisticsServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.batchEdit.service.BatchEditService;
import com.nkia.itg.itam.statistics.dao.AssetStatisticsDAO;
import com.nkia.itg.itam.statistics.service.AssetStatisticsService;

@Service("assetStatisticsService")
public class AssetStatisticsServiceImpl implements AssetStatisticsService{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetStatisticsServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "assetStatisticsDAO")
	public AssetStatisticsDAO assetStatisticsDAO;
	
	@Resource(name = "batchEditService")
	public BatchEditService batchEditService;
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
		
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchClassTree(paramMap);
	}	
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetInfo(paramMap);
	}

	public int searchAssetInfoCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetInfoCount(paramMap);
	}	

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchCodeDataList(paramMap);
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetList(paramMap);
	}

	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetListCount(paramMap);
	}
	
	public List searchExcelByResource(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetStatisticsDAO.searchExcelByResource(paramMap); 
		return resultList;
	}
	
	public List searchExcel(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetStatisticsDAO.searchExcel(paramMap); 
		return resultList;
	}	
	
	public List searchAssetListPop(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetListPop(paramMap);
	}

	public int searchAssetListPopCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetListPopCount(paramMap);
	}
	
	// 메인화면용 자산운영 현황
	public HashMap searchActiveAssetStateCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchActiveAssetStateCnt(paramMap);
	}

	@Override
	public HashMap updateAssetState(ModelMap paramMap) throws NkiaException {
		List assetList = (ArrayList)paramMap.get("assetList");
		String assetState = (String)paramMap.get("asset_state");
		HashMap param = new HashMap();
		
		for(int i = 0 ; i < assetList.size() ; i++) {
			Map assetInfo = (Map)assetList.get(i);
			String confId = (String)assetInfo.get("CONF_ID");
			String assetId = (String)assetInfo.get("ASSET_ID");
			String classId = (String)assetInfo.get("CLASS_ID");
			
			param.put("ASSET_STATE", assetState);
			
			List assetIdList = new ArrayList();
			assetIdList.add(assetId);
			param.put("assetIdList", assetIdList);
			
			List classIdList = new ArrayList();
			classIdList.add(classId);
			param.put("classIdList", classIdList);
			
			List colIdList = new ArrayList();
			colIdList.add("ASSET_STATE");
			param.put("colIdList", colIdList);
			
			List confIdList = new ArrayList();
			confIdList.add(confId);
			param.put("confIdList", confIdList);
			
			List tblNmList = new ArrayList();
			tblNmList.add("AM_ASSET");
			param.put("tblNmList", tblNmList);
			
			batchEditService.updateBatchEditClassAttr(param);
			
		}
		
		return null;
	}

	@Override
	public List searchAssetStateChangeResultList(ModelMap paramMap) throws NkiaException {
		return assetStatisticsDAO.searchAssetStateChangeResultList(paramMap);
	}
	
	public String selectSysCode(String column) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.selectSysCode(column);
	}
	
	public List searchClassTypeAttrList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchClassTypeAttrList(paramMap);
	}

	@Override
	public List searchDeptTree(ModelMap paramMap) throws NkiaException {
		return assetStatisticsDAO.searchDeptTree(paramMap);
	}
	
	@Override
	public List searchCustTree(ModelMap paramMap) throws NkiaException {
		return assetStatisticsDAO.searchCustTree(paramMap);
	}

	@Override
	public void changeAssetState(ModelMap paramMap) throws NkiaException{
		List assetInfoList = (List)paramMap.get("asset_list");
		String assetState = (String)paramMap.get("pop_asset_state");
		String userId = (String)paramMap.get("user_id");
		
		if(assetInfoList != null && assetInfoList.size() > 0) {
			
			for(int i=0; i<assetInfoList.size(); i++) {
				HashMap assetMap = new HashMap();
				HashMap dataMap = new HashMap();
				HashMap hisMap = new HashMap();
				
				assetMap = (HashMap)assetInfoList.get(i);
				
				String assetId = (String)assetMap.get("ASSET_ID");
				String confId = (String)assetMap.get("CONF_ID");
				String classId = (String)assetMap.get("CLASS_ID");
				String classType = (String)assetMap.get("CLASS_TYPE");
				
				// 변경전 이력 업데이트
				hisMap.put("ASSET_STATE", assetState);
				assetHistoryService.insertAssetAttrHistory(confId, hisMap, "", userId);
				
				// 자산상태 변경
				dataMap.put("asset_id", assetId);
				dataMap.put("asset_state", assetState);
				dataMap.put("upd_user_id", userId);
				assetStatisticsDAO.changeAssetState(dataMap);
			}
		}
	}

	@Override
	public void insertAssetDisuse(ModelMap paramMap) throws NkiaException{
		List assetList = (List)paramMap.get("assetList");
		String disuse_dt = (String) paramMap.get("disuse_dt");
		String disuse_desc = (String) paramMap.get("disuse_desc");
		String user_id = (String) paramMap.get("user_id");
		
		if(assetList != null && assetList.size() != 0){
			
			for(int i = 0; i < assetList.size(); i++){
				// 폐기정보 삭제 / 등록
				HashMap assetMap = new HashMap();
				assetMap = (HashMap) assetList.get(i);
				assetMap.put("disuse_dt", disuse_dt);
				assetMap.put("disuse_desc", disuse_desc);
				assetMap.put("upd_user_id", user_id);
				assetStatisticsDAO.deleteAssetDisuse(assetMap);
				assetStatisticsDAO.insertAssetDisuse(assetMap);
				
				// 자산상태변경
				String assetId = (String)assetMap.get("ASSET_ID");
				String confId = (String)assetMap.get("CONF_ID");
				String classId = (String)assetMap.get("CLASS_ID");
				String classType = (String)assetMap.get("CLASS_TYPE");
				String assetState = "DISUSE";
				
				HashMap paramDataMap = new HashMap();
				
				// 변경전 이력 업데이트
				paramDataMap.put("ASSET_STATE", assetState);
				assetHistoryService.insertAssetAttrHistory(confId, paramDataMap, "", user_id);
				
				// 자산상태 변경
				paramDataMap.put("asset_id", assetId);
				paramDataMap.put("asset_state", assetState);
				paramDataMap.put("upd_user_id", user_id);
				assetStatisticsDAO.changeAssetState(paramDataMap);
			}
		}
		
		
	}

	@Override
	public List searchDisuseAssetList(ModelMap paramMap) {
		return assetStatisticsDAO.searchDisuseAssetList(paramMap);
	}

	@Override
	public int searchDisuseAssetListCount(ModelMap paramMap) {
		return assetStatisticsDAO.searchDisuseAssetListCount(paramMap);
	}

	@Override
	public List searchDisuseAssetExcelDown(Map param) {
		return assetStatisticsDAO.searchDisuseAssetExcelDown(param);
	}

	@Override
	public List searchClassTreeForMaint(ModelMap paramMap) {
		return assetStatisticsDAO.searchClassTreeForMaint(paramMap);
	}

	@Override
	public List searchMaintAssetList(ModelMap paramMap) {
		return assetStatisticsDAO.searchMaintAssetList(paramMap);
	}

	@Override
	public int searchMaintAssetListCount(ModelMap paramMap) {
		return assetStatisticsDAO.searchMaintAssetListCount(paramMap);
	}

	@Override
	public List searchMaintAssetListExcel(Map param) throws NkiaException {
		return assetStatisticsDAO.searchMaintAssetListExcel(param);
	}

	@Override
	public String seelctAssetViewAuth(String loginCustId) throws NkiaException {
		String assetViewAuth = "";
		if(loginCustId.startsWith("KE")){
			assetViewAuth = assetStatisticsDAO.seelctAssetViewAuth(loginCustId);
		}else{
			assetViewAuth = "ALL";
		}
		return assetViewAuth;
	}

	@Override
	public List searchAssetExcelHeader(HashMap paramMap) throws NkiaException {
		return assetStatisticsDAO.searchAssetExcelHeader(paramMap);
	}

	@Override
	public List selectRackInfoList(ModelMap paramMap) throws NkiaException {
		return assetStatisticsDAO.selectRackInfoList(paramMap);
	}
	
	@Override
	public List selectRackInfoList(HashMap paramMap) throws NkiaException {
		return assetStatisticsDAO.selectRackInfoList(paramMap);
	}
	
	@Override
	public List selectRackInfoExcelList(HashMap paramMap) throws NkiaException {
		return assetStatisticsDAO.selectRackInfoExcelList(paramMap);
	}
	
	@Override
	public List selectChangeRackInfoList(HashMap paramMap) throws NkiaException {
		return assetStatisticsDAO.selectChangeRackInfoList(paramMap);
	}
	
	@Override
	public int selectChangeRackInfoListCount(ModelMap paramMap) {
		return assetStatisticsDAO.selectChangeRackInfoListCount(paramMap);
	}

	@Override
	public List searchExcelPoi(Map paramMap) throws Exception {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetStatisticsDAO.searchExcelPoi(paramMap); 
		return resultList;
	}

	@Override
	public List searchConfTypeTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetStatisticsDAO.searchConfTypeTree(paramMap);
		return resultList;
	}
}
