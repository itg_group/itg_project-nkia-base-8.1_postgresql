package com.nkia.itg.itam.statistics.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("swStatisticsDAO")
public class SwStatisticsDAO extends EgovComAbstractDAO {
	
	public List swAssetList(ModelMap paramMap) throws NkiaException{
		return list("swStatisticsDAO.swAssetList", paramMap);
	}

	public List selectPcAssetList(ModelMap paramMap) throws NkiaException {
		return list("swStatisticsDAO.selectPcAssetList", paramMap);
	}

	public List selectServerAssetList(ModelMap paramMap) throws NkiaException{
		return list("swStatisticsDAO.selectServerAssetList", paramMap);
	}

	public int selectPcAssetListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("swStatisticsDAO.selectPcAssetListCount", paramMap);
	}

	public int selectServerAssetListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("swStatisticsDAO.selectServerAssetListCount", paramMap);
	}

	public List swAssetListExcel(Map paramMap) throws NkiaException {
		return list("swStatisticsDAO.swAssetList", paramMap);
	}

	public List selectServerAssetListExcel(Map paramMap) throws NkiaException{
		return list("swStatisticsDAO.selectServerAssetList", paramMap);
	}

	public List selectPcAssetListExcel(Map param) throws NkiaException{
		return list("swStatisticsDAO.selectPcAssetList", param);
	}

	public List selectAssignPcAssetList(ModelMap paramMap) throws NkiaException {
		return list("swStatisticsDAO.selectAssignPcAssetList", paramMap);
	}
	
	public int selectAssignPcAssetListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("swStatisticsDAO.selectAssignPcAssetListCount", paramMap);
	}

	public void deleteSwAssignLicence(Map paramData) throws NkiaException{
		delete("swStatisticsDAO.deleteSwAssignLicence", paramData);
	}

	public void insertSwAssignLicence(ModelMap paramMap) throws NkiaException{
		insert("swStatisticsDAO.insertSwAssignLicence", paramMap);
	}
}
