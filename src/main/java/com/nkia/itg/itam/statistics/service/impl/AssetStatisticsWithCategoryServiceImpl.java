/*
 * @(#)AssetStatisticsServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.statistics.dao.AssetStatisticsWithCategoryDAO;
import com.nkia.itg.itam.statistics.service.AssetStatisticsWithCategoryService;

@Service("assetStatisticsWithCategoryService")
public class AssetStatisticsWithCategoryServiceImpl implements AssetStatisticsWithCategoryService{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetStatisticsWithCategoryServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "assetStatisticsWithCategoryDAO")
	public AssetStatisticsWithCategoryDAO assetStatisticsDAO;
	
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchClassTree(paramMap);
	}	
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetInfo(paramMap);
	}

	public int searchAssetInfoCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetInfoCount(paramMap);
	}	

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchCodeDataList(paramMap);
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetList(paramMap);
	}

	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetListCount(paramMap);
	}
	
	public List searchOperAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchOperAssetList(paramMap);
	}

	public int searchOperAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchOperAssetListCount(paramMap);
	}
	
	public List searchExcel(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetStatisticsDAO.searchExcel(paramMap); 
		return resultList;
	}	
	
	public List searchAssetListPop(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetListPop(paramMap);
	}

	public int searchAssetListPopCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetListPopCount(paramMap);
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {		
		List resultList = null;
		resultList = assetStatisticsDAO.searchExcelDown(paramMap); 
		return resultList;
	}
	
	public void insertInactive(ModelMap paramMap) throws NkiaException{
		String seq = assetStatisticsDAO.selectBookingSeq(paramMap);

		if(StringUtils.isNumeric(seq)){
			paramMap.put("seq", seq);
		}else{
			paramMap.put("seq", 1);
		}
		//유휴자산 이력
		assetStatisticsDAO.insertInactive(paramMap);
		//자산 상태 변경
		assetStatisticsDAO.updateInactiveState(paramMap);
	}
	
	public void deleteInactive(ModelMap paramMap) throws NkiaException{
		String seq = assetStatisticsDAO.selectDeleteBookingSeq(paramMap);

		if(StringUtils.isNumeric(seq)){
			paramMap.put("seq", seq);
		}else{
			paramMap.put("seq", 1);
		}
		//유휴자산 이력
		assetStatisticsDAO.deleteInactive(paramMap);
		//자산 상태 변경
		assetStatisticsDAO.updateInactiveState(paramMap);
	}
	
	public List searchAssetTcoList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetTcoList(paramMap);
	}

	public int searchAssetTcoListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetStatisticsDAO.searchAssetTcoListCount(paramMap);
	}
}
