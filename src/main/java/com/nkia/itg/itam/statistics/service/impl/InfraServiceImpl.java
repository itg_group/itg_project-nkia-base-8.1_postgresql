/*
 * @(#)infraServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.itam.statistics.dao.InfraDAO;
import com.nkia.itg.itam.statistics.service.InfraService;

@Service("infraService")
public class InfraServiceImpl implements InfraService{
	
	private static final Logger logger = LoggerFactory.getLogger(InfraServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "infraDAO")
	public InfraDAO infraDAO;
	
	public List searchClassTypeLifeCycleList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList =  infraDAO.searchClassTypeLifeCycleList(paramMap);
		return resultList;
	}
	
	public List searchClassTypeLifeCycleChart(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = infraDAO.searchClassTypeLifeCycleChart(paramMap);
		return resultList;
	}
	
	public List searchCustTypeLifeCycleList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList =  infraDAO.searchCustTypeLifeCycleList(paramMap);
		return resultList;
	}
	
	public List searchCustTypeLifeCycleChart(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = infraDAO.searchCustTypeLifeCycleChart(paramMap);
		return resultList;
	}
	
	public List searchCustTypeClassChart(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList =  infraDAO.searchCustTypeClassChart(paramMap);
		return resultList;
	}
	
	public List searchCustTypeClassList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = infraDAO.searchCustTypeClassList(paramMap);
		return resultList;
	}
	
	public List searchYearTypeIntroductionList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList =  infraDAO.searchYearTypeIntroductionList(paramMap);
		return resultList;
	}
	
	public List searchTermByIntroCostList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList =  infraDAO.searchTermByIntroCostList(paramMap);
		return resultList;
	}
	
	public List searchYearByIntroCostList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList =  infraDAO.searchYearByIntroCostList(paramMap);
		return resultList;
	}
	
	public List searchYearByIntroCostChart(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = infraDAO.searchYearByIntroCostChart(paramMap);
		return resultList;
	}
	
	public List searchPresentExcelDown(Map paramMap) throws NkiaException {		
		List resultList = null;
		resultList = infraDAO.searchPresentExcelDown(paramMap); 
		return resultList;
	}

	@Override
	public List searchCustByAssetStateList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = infraDAO.searchCustByAssetStateList(paramMap); 
		return resultList;
	}

	@Override
	public List searchCustByAssetStateChart(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = infraDAO.searchCustByAssetStateChart(paramMap); 
		return resultList;
	}

	@Override
	public List searchClassTypeByYearList(ModelMap paramMap) throws NkiaException {
		int baseYear = (Integer) paramMap.get("baseYear");
		paramMap.put("nowYear", baseYear);
		paramMap.put("first_year_ago", baseYear - 1);
		paramMap.put("two_year_ago", baseYear - 2);
		paramMap.put("three_year_ago", baseYear - 3);
		paramMap.put("four_year_ago", baseYear - 4);
		paramMap.put("five_year_ago", baseYear - 5);
		paramMap.put("six_year_ago", baseYear - 6);
		
		List resultList = new ArrayList();
		resultList = infraDAO.searchClassTypeByYearList(paramMap);
		
		return resultList;
	}

	@Override
	public List searchClassTypeByYearChart(ModelMap paramMap) throws NkiaException {
		int baseYear = (Integer) paramMap.get("baseYear");
		paramMap.put("nowYear", baseYear);
		paramMap.put("first_year_ago", baseYear - 1);
		paramMap.put("two_year_ago", baseYear - 2);
		paramMap.put("three_year_ago", baseYear - 3);
		paramMap.put("four_year_ago", baseYear - 4);
		paramMap.put("five_year_ago", baseYear - 5);
		paramMap.put("six_year_ago", baseYear - 6);
		
		List resultList = new ArrayList();
		resultList = infraDAO.searchClassTypeByYearChart(paramMap);

		return resultList;
	}
}
