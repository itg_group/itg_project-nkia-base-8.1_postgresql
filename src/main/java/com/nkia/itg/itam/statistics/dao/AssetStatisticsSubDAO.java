/*
 * @(#)AssetStatisticsSubDAO.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.SubComAbstractDAO;

@Repository("assetStatisticsSubDAO")
public class AssetStatisticsSubDAO extends SubComAbstractDAO{
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("AssetStatisticsDAO.searchClassTree", paramMap);
		return resultList;
	}	
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException{
		return list("AssetStatisticsDAO.searchAssetInfo", paramMap);
	}

	public int searchAssetInfoCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsDAO.searchAssetInfoCount", paramMap);
	}

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("AssetStatisticsDAO.searchCodeDataList", paramMap);
		return resultList;
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException{
		return list("AssetStatisticsDAO.searchAssetList", paramMap);
	}

	public int searchAssetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsDAO.searchAssetListCount", paramMap);
	}

	public List searchExcel(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("AssetStatisticsDAO.searchExcel", paramMap);
		return resultList;
	}
	
	public List searchSubDeptList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("AssetStatisticsDAO.searchSubDeptList", paramMap);
		return resultList;
	}	
}
