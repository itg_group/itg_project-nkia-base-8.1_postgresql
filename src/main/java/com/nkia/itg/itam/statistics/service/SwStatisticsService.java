package com.nkia.itg.itam.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SwStatisticsService {

	//연계 SW 자산 목록 조회
	public List swAssetList(ModelMap paramMap) throws NkiaException;

	//PC 자산 목록 조회 
	public List selectPcAssetList(ModelMap paramMap) throws NkiaException;

	//PC 자산 목록 개수
	public int selectPcAssetListCount(ModelMap paramMap)throws NkiaException;

	//서버 자산 목록 조회
	public List selectServerAssetList(ModelMap paramMap) throws NkiaException;

	//서버 자산 목록 개수
	public int selectServerAssetListCount(ModelMap paramMap) throws NkiaException;

	//연계 SW 자산 목록 엑셀다운
	public List swAssetListExcel(Map paramMap) throws NkiaException;

	//서버 자산 목록 엑셀 다운
	public List selectServerAssetListExcel(Map paramMap) throws NkiaException;

	//PC 자산 목록 엑셀 다운
	public List selectPcAssetListExcel(Map param) throws NkiaException;

	//라이센스 할당 된 PC목록
	public List selectAssignPcAssetList(ModelMap paramMap) throws NkiaException;
	//라이센스 할당 된 PC목록 개수
	public int selectAssignPcAssetListCount(ModelMap paramMap) throws NkiaException;

	//라이센스 회수
	public void deleteSwAssignLicence(ModelMap paramMap) throws NkiaException;

	//라이선스 할당
	public void insertSwAssignLicence(ModelMap paramMap) throws NkiaException;

}
