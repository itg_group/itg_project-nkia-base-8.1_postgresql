/*
 * 자산 통계    2017.04.24
 */
package com.nkia.itg.itam.statistics.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.itam.statistics.service.AssetStatsService;
import egovframework.com.cmm.EgovMessageSource;

@Controller
public class AssetStatsController {

	private static final Logger logger = LoggerFactory.getLogger(AssetStatsController.class);
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name="assetStatsService")
	private AssetStatsService assetStatsService;
	
	
	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/searchPresentExcelDown.do")
    public @ResponseBody ResultVO searchPresentExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = assetStatsService.searchPresentExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 분류체계별 life cycle 현황  페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/classTypeLifeCycle.do")
	public String classTypeLifeCycle(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/stats/classTypeLifeCycle.nvf";
		return forwarPage;
	}
	
	/**
	 * 분류체계별 life cycle 차트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/statistics/stats/searchClassTypeLifeCycleChart.do")
	public @ResponseBody ResultVO searchClassTypeLifeCycleChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetStatsService.searchClassTypeLifeCycleChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 분류체계별 life cycle TreeGrid 리스트
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/searchClassTypeLifeCycleList.do")
	public @ResponseBody ResultVO searchClassTypeLifeCycleList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatsService.searchClassTypeLifeCycleList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
	}
	
	
	/**
	 * 연도별 자산현황
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */

	@RequestMapping(value="/itg/itam/statistics/stats/introStatsClassTypeByYear.do")
	public String classTypeByYear(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/stats/introStatsClassTypeByYear.nvf";
		return forwarPage;
	}
	
	/**
	 * 연도별 도입현황 차트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/stats/introStatsClassTypeByYearChart.do")
	public @ResponseBody ResultVO introStatsClassTypeByYearChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetStatsService.introStatsClassTypeByYearChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 연도별 도입현황 TreeGrid 리스트
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/introStatsClassTypeByYearList.do")
	public @ResponseBody ResultVO introStatsClassTypeByYearList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatsService.introStatsClassTypeByYearList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
	}
	
	/**
	 * 자산위치별 현황
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/assetLocByClassType.do")
	public String assetLocByClassType(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/stats/assetLocByClassType.nvf";
		return forwarPage;
	}
	
	
	/**
	 * 자산위치별 분류체계 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/statistics/stats/searchClassTypeList.do")
	public @ResponseBody ResultVO searchClassTypeList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetStatsService.searchClassTypeList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 자산위치별 현황 TreeGrid 리스트
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/searchAssetLocByClassTypeList.do")
	public @ResponseBody ResultVO searchAssetLocByClassTypeList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatsService.searchAssetLocByClassTypeList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
	}
	
	/**
	 * 자산위치별 현황 차트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/stats/searchAssetLocByClassTypeChart.do")
	public @ResponseBody ResultVO searchAssetLocByClassTypeChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetStatsService.searchAssetLocByClassTypeChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 연도별 폐기자산 현황
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/disuseAssetClassTypeByYear.do")
	public String disuseAssetclassTypeByYear(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/stats/disuseAssetClassTypeByYear.nvf";
		return forwarPage;
	}
	
	/**
	 * 연도별 폐기자산 TreeGrid 리스트
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/disuseAssetClassTypeByYearList.do")
	public @ResponseBody ResultVO disuseAssetClassTypeByYearList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatsService.disuseAssetClassTypeByYearList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
	}
	
	/**
	 * 연도별 폐기자산 현황 차트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/stats/disuseAssetClassTypeByYearChart.do")
	public @ResponseBody ResultVO disuseAssetClassTypeByYearChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetStatsService.disuseAssetClassTypeByYearChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
	/**
	 * 위치별 논리서버 운영 현황
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/logicServerLocByOsType.do")
	public String logicServerLocByOsType(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/statistics/stats/logicServerLocByOsType.nvf";
		return forwarPage;
	}
	
	/**
	 * 위치별 논리서버 운영현황 TreeGrid 리스트
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/statistics/stats/logicServerLocByOsTypeList.do")
	public @ResponseBody ResultVO logicServerLocByOsTypeList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetStatsService.logicServerLocByOsTypeList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;	
	}
	
	/**
	 * 위치별 논리서버 운영현황 현황 차트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/statistics/stats/logicServerLocByOsTypeChart.do")
	public @ResponseBody ResultVO logicServerLocByOsTypeChart(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = assetStatsService.logicServerLocByOsTypeChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
}
