/*
 * @(#)AssetStatisticsService.java              2013. 10. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AssetStatisticsWithCategoryService {
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException;
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException;

	public int searchAssetInfoCount(ModelMap paramMap) throws NkiaException;
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException;
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException;

	public int searchAssetListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchOperAssetList(ModelMap paramMap) throws NkiaException;

	public int searchOperAssetListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchExcel(Map paramMap)throws NkiaException;
	
	public List searchAssetListPop(ModelMap paramMap) throws NkiaException;

	public int searchAssetListPopCount(ModelMap paramMap) throws NkiaException;
	
	public List searchExcelDown(Map paramMap) throws NkiaException;
	
	public void insertInactive(ModelMap paramMap) throws NkiaException;
	
	public void deleteInactive(ModelMap paramMap) throws NkiaException;
	
	public List searchAssetTcoList(ModelMap paramMap) throws NkiaException;

	public int searchAssetTcoListCount(ModelMap paramMap) throws NkiaException;

}
