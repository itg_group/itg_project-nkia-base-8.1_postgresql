/*
 * @(#)maintService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface IntegrationService {
	
	public List searchCustTypeClassChart(ModelMap paramMap) throws NkiaException;
	
	public List searchCustTypeClassList(ModelMap paramMap) throws NkiaException;
	
	public List searchIntegrationList(ModelMap paramMap) throws NkiaException;
	
	public List searchExcel(Map paramMap) throws NkiaException;
}
