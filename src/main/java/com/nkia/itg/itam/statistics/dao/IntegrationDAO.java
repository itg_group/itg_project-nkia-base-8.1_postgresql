/*
 * @(#)IntegrationDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.SubComAbstractDAO;

@Repository("integrationDAO")
public class IntegrationDAO extends SubComAbstractDAO{
	
	public List searchCustTypeClassList(ModelMap paramMap) throws NkiaException{
		return list("integrationDAO.searchCustTypeClassList", paramMap);
	}
	
	public List searchCustTypeClassChart(ModelMap paramMap) throws NkiaException{
		return list("integrationDAO.searchCustTypeClassChart", paramMap);
	}
	
	public List searchIntegrationList(ModelMap paramMap) throws NkiaException{
		return list("integrationDAO.searchIntegrationList", paramMap);
	}

	public List searchExcel(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("integrationDAO.searchIntegrationList", paramMap);
		return resultList;
	}		
}
