/*
 * @(#)InfraDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("infraDAO")
public class InfraDAO extends EgovComAbstractDAO{
	
	public List searchClassTypeLifeCycleList(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchClassTypeLifeCycleList", paramMap);
	}
	
	public List searchClassTypeLifeCycleChart(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchClassTypeLifeCycleChart", paramMap);
	}
	
	public List searchCustTypeLifeCycleList(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchCustTypeLifeCycleList", paramMap);
	}
	
	public List searchCustTypeLifeCycleChart(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchCustTypeLifeCycleChart", paramMap);
	}
	
	public List searchCustTypeClassList(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchCustTypeClassList", paramMap);
	}
	
	public List searchCustTypeClassChart(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchCustTypeClassChart", paramMap);
	}
	
	public List searchYearTypeIntroductionList(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchYearTypeIntroductionList", paramMap);
	}
	
	public List searchTermByIntroCostList(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchTermByIntroCostList", paramMap);
	}
	
	public List searchYearByIntroCostList(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchYearByIntroCostList", paramMap);
	}
	
	public List searchYearByIntroCostChart(ModelMap paramMap) throws NkiaException{
		return list("InfraDAO.searchYearByIntroCostChart", paramMap);
	}
	
	public List searchPresentExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		String excelGubun = (String)paramMap.get("excelGubun");
		if("yearByIntroCost".equals(excelGubun)){
			resultList = this.list("InfraDAO.searchYearByIntroCostExcel", paramMap);			
		}else if("classTypeLifeCycle".equals(excelGubun)){
			resultList = this.list("InfraDAO.searchClassTypeLifeCycleExcel", paramMap);
		}else if("custTypeClass".equals(excelGubun)){
			resultList = this.list("InfraDAO.searchCustTypeClassExcel", paramMap);
		}else if("custTypeLifeCycle".equals(excelGubun)){
			resultList = this.list("InfraDAO.searchCustTypeLifeCycleExcel", paramMap);
		}else if("yearsTypeIntroduction".equals(excelGubun)){
			resultList = this.list("InfraDAO.searchYearTypeIntroductionList", paramMap);
		}else if("termByIntroCost".equals(excelGubun)){
			resultList = this.list("InfraDAO.searchTermByIntroCostExcel", paramMap);
		}else if("custByAssetState".equals(excelGubun)){	// 부서별현황 추가 - 좌은진
			resultList = this.list("InfraDAO.searchCustByAssetStateExcel", paramMap);
		}
		
		return resultList;
	}

	public List searchCustByAssetStateList(ModelMap paramMap) throws NkiaException {
		return list("InfraDAO.searchCustByAssetStateList", paramMap);
	}

	public List searchCustByAssetStateChart(ModelMap paramMap) throws NkiaException {
		return list("InfraDAO.searchCustByAssetStateChart", paramMap);
	}

	public List searchClassTypeByYearList(ModelMap paramMap) throws NkiaException {
		return list("InfraDAO.searchClassTypeByYearList", paramMap);
	}

	public List searchClassTypeByYearChart(ModelMap paramMap) throws NkiaException {
		return list("InfraDAO.searchClassTypeByYearChart", paramMap);
	}
}
