/*
 * @(#)AssetStatisticsDAO.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.statistics.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("assetStatisticsDAO")
public class AssetStatisticsDAO extends EgovComAbstractDAO{
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchClassTree", paramMap);
	}	
	
	//hmsong cmdb 개선 작업 Start
	public List searchConfTypeTree(ModelMap paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchConfTypeTree", paramMap);
	}
	//hmsong cmdb 개선 작업 End
	
	public List searchAssetInfo(ModelMap paramMap) throws NkiaException{
		return list("AssetStatisticsDAO.searchAssetInfo", paramMap);
	}

	public int searchAssetInfoCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("AssetStatisticsDAO.searchAssetInfoCount", paramMap);
	}

	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchCodeDataList", paramMap);
	}
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException{
		//List responseList = list("AssetStatisticsDAO.searchAssetList", paramMap);
		List resultList = list("AssetStatisticsDAO.searchAssetList", paramMap);
		List resultList2 = new ArrayList();
		if(paramMap.containsKey("rnum")){
			Map resultMap = new HashMap();
			
			//List resultList = new List();
			int size = resultList.size();
			for(int i=0; i<size; i++) {
				resultMap = (HashMap)resultList.get(i);
				resultMap.put("RNUM", (String)paramMap.get("rnum")+"-"+(String)resultMap.get("RNUM"));
				resultList2.add(resultMap);
			}
			return resultList2;
		}else{
			return resultList;
		}
		
		//Map resultMap = new HashMap();
		/*
		for(int i=0; i<responseList.size(); i++) {
			resultMap = (HashMap)responseList.get(i);
			
			if("S".equals(resultMap.get("DIV_HIERACHY"))) {
				resultMap.put("ASSET_ID", "<font color='blue'>┗" + resultMap.get("ASSET_ID") + "</font>");
			}
			resultList.add(resultMap);
		}
		*/
		
//		Map resultMap = new HashMap();
//		int s = 0;
//		for(int i=0; i<resultList.size(); i++) {
//			resultMap = (HashMap)resultList.get(i);
//			resultMap.put("RNUM", "　" + (String)resultMap.get("RNUM") + "." + Integer.toString(s));
//			}else{
//				s = 0;
//			}
//			
//			resultList.add(resultMap);
//		}
		
	}

	public int searchAssetListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsDAO.searchAssetListCount", paramMap);
	}

	public List searchExcelByResource(Map paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchExcelByResource", paramMap);
	}
	
	public List searchExcel(Map paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchExcel", paramMap);
	}
	
	public List searchAssetListPop(ModelMap paramMap) throws NkiaException{
		return list("AssetStatisticsDAO.searchAssetListPop", paramMap);
	}

	public int searchAssetListPopCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetStatisticsDAO.searchAssetListPopCount", paramMap);
	}
	
	// 메인화면용 자산운영현황
	public HashMap searchActiveAssetStateCnt(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("AssetStatisticsDAO.searchActiveAssetStateCnt", paramMap);
	}

	public List searchAssetStateChangeResultList(ModelMap paramMap) {
		return list("AssetStatisticsDAO.searchAssetList", paramMap);
	}
	
	public String selectSysCode(String column)throws NkiaException {
		// TODO Auto-generated method stub
		return (String)selectByPk("AssetStatisticsDAO.selectSysCode", column);
	}

	public List searchClassTypeAttrList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("AssetStatisticsDAO.searchClassTypeAttrList", paramMap);
		return resultList;
	}
	
	public List searchDeptTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("AssetStatisticsDAO.searchDeptTree", paramMap);
		return resultList;
	}
	
	public List searchCustTree(ModelMap paramMap) throws NkiaException {
		return list("AssetStatisticsDAO.searchCustTree", paramMap);
	}

	public void changeAssetState(HashMap paramMap) {
		update("AssetStatisticsDAO.changeAssetState",paramMap);
	}

	public void insertAssetDisuse(HashMap assetMap) {
		insert("AssetStatisticsDAO.insertAssetDisuse", assetMap);
	}

	public void deleteAssetDisuse(HashMap assetMap) {
		delete("AssetStatisticsDAO.deleteAssetDisuse", assetMap);
	}

	public List searchDisuseAssetList(ModelMap paramMap) {
		return list("AssetStatisticsDAO.searchDisuseAssetList", paramMap);
	}
	
	public int searchDisuseAssetListCount(ModelMap paramMap) {
		return (Integer) selectByPk("AssetStatisticsDAO.searchDisuseAssetListCount", paramMap);
	}

	public List searchDisuseAssetExcelDown(Map param) {
		return list("AssetStatisticsDAO.searchDisuseAssetList", param);
	}

	public List searchClassTreeForMaint(ModelMap paramMap) {
		return list("AssetStatisticsDAO.searchClassTreeForMaint", paramMap);
	}

	public List searchMaintAssetList(ModelMap paramMap) {
		return list("AssetStatisticsDAO.searchMaintAssetList", paramMap);
	}
	
	public int searchMaintAssetListCount(ModelMap paramMap) {
		return (Integer) selectByPk("AssetStatisticsDAO.searchMaintAssetListCount", paramMap);
	}

	public List searchMaintAssetListExcel(Map param) {
		return list("AssetStatisticsDAO.searchMaintAssetList", param);
	}

	public String seelctAssetViewAuth(String custId) {
		return (String) selectByPk("AssetStatisticsDAO.seelctAssetViewAuth", custId);
	}

	public List searchAssetExcelHeader(HashMap paramMap) {
		return list("AssetStatisticsDAO.searchAssetExcelHeader", paramMap);
	}
	
	public List selectRackInfoList(ModelMap param) {
		return list("AssetStatisticsDAO.selectRackInfoList", param);
	}
	
	public List selectRackInfoList(HashMap param) {
		return list("AssetStatisticsDAO.selectRackInfoList", param);
	}
	
	public List selectRackInfoExcelList(HashMap param) {
		return list("AssetStatisticsDAO.selectRackInfoExcelList", param);
	}
	
	public List selectChangeRackInfoList(HashMap param) {
		return list("AssetStatisticsDAO.selectChangeRackInfoList", param);
	}
	
	public int selectChangeRackInfoListCount(ModelMap paramMap) {
		return (Integer) selectByPk("AssetStatisticsDAO.selectChangeRackInfoListCount", paramMap);
	}
	
	public int selectAssetDuplicateCount(HashMap paramMap)throws NkiaException {
		return (Integer)selectByPk("AssetStatisticsDAO.selectAssetDuplicateCount", paramMap);
	}
	
	//BASE 202104 custom : 엑셀다운로드를 POI로 수정
	public List searchExcelPoi(Map paramMap) throws Exception {
		return list("AssetStatisticsDAO.searchExcelPoi", paramMap);
	}
}
