/*
 * @(#)LifeCycleDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.lifeCycle.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("lifeCycleDAO")
public class LifeCycleDAO extends EgovComAbstractDAO{
	
	public List searchLifeCycleReq(ModelMap paramMap) throws NkiaException{
		return list("LifeCycleDAO.searchLifeCycleReq", paramMap);
	}
	
	public int searchLifeCycleReqCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("LifeCycleDAO.searchLifeCycleReqCount", paramMap);
	}
	
	public List searchLifeCycleAsset(ModelMap paramMap) throws NkiaException{
		return list("LifeCycleDAO.searchLifeCycleAsset", paramMap);
	}
	
	public int searchLifeCycleAssetCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("LifeCycleDAO.searchLifeCycleAssetCount", paramMap);
	}
	
	public List searchLifeCycleReqAsset(ModelMap paramMap) throws NkiaException{
		return list("LifeCycleDAO.searchLifeCycleReqAsset", paramMap);
	}
	
	public void insertLifeCycleReq(HashMap infoData) throws NkiaException {
		this.insert("LifeCycleDAO.insertLifeCycleReq", infoData);
	}
	
	public void deleteLifeCycleReq(ModelMap paramMap) throws NkiaException {
		this.delete("LifeCycleDAO.deleteLifeCycleReq", paramMap);
	}
	
	public void deleteLifeCycleReqAsset(HashMap infoData) throws NkiaException {
		this.delete("LifeCycleDAO.deleteLifeCycleReqAsset", infoData);
	}
	
	public void insertLifeCycleReqAsset(HashMap infoData) throws NkiaException {
		this.insert("LifeCycleDAO.insertLifeCycleReqAsset", infoData);
	}
	
	public void updateLifeCycleReq(HashMap infoData) throws NkiaException {
		this.update("LifeCycleDAO.updateLifeCycleReq", infoData);
	}
	
	public void updateLifeCycleApprovalReq(ModelMap paramMap) throws NkiaException {
		this.update("LifeCycleDAO.updateLifeCycleApprovalReq", paramMap);
	}
	
	public void updateAssetState(ModelMap paramMap) throws NkiaException {
		this.update("LifeCycleDAO.updateAssetState", paramMap);
	}
	
	public void updateLifeCycleApproval(ModelMap paramMap) throws NkiaException {
		this.update("LifeCycleDAO.updateLifeCycleApproval", paramMap);
	}
	
	public void updateLifeCycleReturn(ModelMap paramMap) throws NkiaException {
		this.update("LifeCycleDAO.updateLifeCycleReturn", paramMap);
	}
	
	public List searchLifeCycleExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("LifeCycleDAO.searchLifeCycleExcelDown", paramMap);
		return resultList;
	}	
}
