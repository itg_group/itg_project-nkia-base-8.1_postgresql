/*
 * @(#)lifeCycleService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.lifeCycle.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface LifeCycleService {
	
	public List searchLifeCycleReq(ModelMap paramMap) throws NkiaException;
	
	public int searchLifeCycleReqCount(ModelMap paramMap) throws NkiaException;
	
	public List searchLifeCycleAsset(ModelMap paramMap) throws NkiaException;
	
	public int searchLifeCycleAssetCount(ModelMap paramMap) throws NkiaException;
	
	public List searchLifeCycleReqAsset(ModelMap paramMap) throws NkiaException;
	
	public void insertLifeCycleReq(ModelMap paramMap) throws NkiaException;
	
	public void deleteLifeCycleReq(ModelMap paramMap) throws NkiaException, IOException;
	
	public void deleteLifeCycleReqAsset(ModelMap paramMap) throws NkiaException;

	public void insertLifeCycleReqAsset(ModelMap paramMap) throws NkiaException;
	
	public void updateLifeCycleReq(ModelMap paramMap) throws NkiaException;
	
	public void updateLifeCycleApprovalReq(ModelMap paramMap) throws NkiaException;
	
	public void updateLifeCycleApproval(ModelMap paramMap) throws NkiaException;
	
	public void updateLifeCycleReturn(ModelMap paramMap) throws NkiaException;
	
	public List searchLifeCycleExcelDown(Map paramMap) throws NkiaException;
}
