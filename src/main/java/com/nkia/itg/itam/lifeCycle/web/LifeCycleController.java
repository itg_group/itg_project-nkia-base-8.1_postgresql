/*
 * @(#)LifeCycleController.java              2013. 5. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.lifeCycle.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.lifeCycle.service.LifeCycleService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class LifeCycleController {
	
	@Resource(name = "lifeCycleService")
	private LifeCycleService lifeCycleService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	/**
	 * LifeCycle 관리 - 자산 LIFE CYCLE 변경요청 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/lifeCycle/infraLifeCycleReq.do")
	public String lifeCycleManager(ModelMap paramMap) throws NkiaException {
    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("login_user_id", userVO.getUser_id());
    	}
		String forwarPage = "/itg/itam/lifeCycle/infraLifeCycleReqMng.nvf";
		return forwarPage;
	}

	/**
	 * LifeCycle 관리 - 자산 LIFE CYCLE 변경승인 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/infraLifeCycleAppr.do")
	public String maintNoneContReq(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/lifeCycle/infraLifeCycleApprMng.nvf";
		return forwarPage;
	}

	/**
	 * LifeCycle 관리 - 등록가능한 요청자산 리스트 팝업 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/searchLifeCycleAsset.do")
	public @ResponseBody ResultVO searchLifeCycleAsset(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = lifeCycleService.searchLifeCycleAsset(paramMap);		
			totalCount = lifeCycleService.searchLifeCycleAssetCount(paramMap);

			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수 요청리스트 조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/searchLifeCycleReq.do")
	public @ResponseBody ResultVO searchLifeCycleReq(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = lifeCycleService.searchLifeCycleReq(paramMap);		
			totalCount = lifeCycleService.searchLifeCycleReqCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수 요청자산 리스트 조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/searchLifeCycleReqAsset.do")
	public @ResponseBody ResultVO searchLifeCycleReqAsset(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = lifeCycleService.searchLifeCycleReqAsset(paramMap);	
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수요청 신규등록(임시저장 버튼 선택시)
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/insertLifeCycleReq.do")
	public @ResponseBody ResultVO insertLifeCycleReq(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{		
			lifeCycleService.insertLifeCycleReq(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수요청 내용 제거
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/deleteLifeCycleReq.do")
	public @ResponseBody ResultVO deleteLifeCycleReq(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			lifeCycleService.deleteLifeCycleReq(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수 요청자산 제거
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/deleteLifeCycleReqAsset.do")
	public @ResponseBody ResultVO deleteLifeCycleReqAsset(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			lifeCycleService.deleteLifeCycleReqAsset(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수 요청자산 입력
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/insertLifeCycleReqAsset.do")
	public @ResponseBody ResultVO insertLifeCycleReqAsset(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{	
			lifeCycleService.insertLifeCycleReqAsset(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수요청 내용 수정
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/updateLifeCycleReq.do")
	public @ResponseBody ResultVO updateLifeCycleReq(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			lifeCycleService.updateLifeCycleReq(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수요청 내용 승인요청
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/updateLifeCycleApprovalReq.do")
	public @ResponseBody ResultVO updateLifeCycleApprovalReq(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			lifeCycleService.updateLifeCycleApprovalReq(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수요청 내용 승인
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/updateLifeCycleApproval.do")
	public @ResponseBody ResultVO updateLifeCycleApproval(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			lifeCycleService.updateLifeCycleApproval(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 유지보수요청 내용 반려
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/updateLifeCycleReturn.do")
	public @ResponseBody ResultVO updateLifeCycleReturn(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			lifeCycleService.updateLifeCycleReturn(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * LifeCycle 관리 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/lifeCycle/searchLifeCycleExcelDown.do")
    public @ResponseBody ResultVO searchLifeCycleExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
          ResultVO resultVO = new ResultVO();
          List resultList = new ArrayList();
          ModelMap excelMap = new ModelMap();
          try{
        	  Map excelParam = (Map) paramMap.get("excelParam");  
        	  Map param = (Map) paramMap.get("param");
			 
        	  resultList = lifeCycleService.searchLifeCycleExcelDown(param);
        	  excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			 
        	  resultVO.setResultMap(excelMap);
          } catch(Exception e) {
        	  throw new NkiaException(e);
          }
          return resultVO;
	}
}
