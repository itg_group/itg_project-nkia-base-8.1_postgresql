/*
 * @(#)lifeCycleServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.lifeCycle.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.lifeCycle.dao.LifeCycleDAO;
import com.nkia.itg.itam.lifeCycle.service.LifeCycleService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("lifeCycleService")
public class LifeCycleServiceImpl implements LifeCycleService{
	
	private static final Logger logger = LoggerFactory.getLogger(LifeCycleServiceImpl.class);
	
	@Resource(name = "lifeCycleDAO")
	public LifeCycleDAO lifeCycleDAO;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public List searchLifeCycleReq(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return lifeCycleDAO.searchLifeCycleReq(paramMap);
	}
	
	public int searchLifeCycleReqCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return lifeCycleDAO.searchLifeCycleReqCount(paramMap);
	}
	
	public List searchLifeCycleAsset(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return lifeCycleDAO.searchLifeCycleAsset(paramMap);
	}
	
	public int searchLifeCycleAssetCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return lifeCycleDAO.searchLifeCycleAssetCount(paramMap);
	}
	
	public List searchLifeCycleReqAsset(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return lifeCycleDAO.searchLifeCycleReqAsset(paramMap);
	}
	
	public void insertLifeCycleReq(ModelMap paramMap) throws NkiaException {
		int mapSize = paramMap.size();
		String login_user_id = null;
		for(int i=0; i<mapSize; i++){
			String mapIndex = toString().valueOf(i);
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
			if(i == 0){
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
		    	if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		login_user_id = userVO.getUser_id();
		    		infoData.put("login_user_id", userVO.getUser_id());
		    	}
		    	infoData.put("lifecycle_req_state", "1");
		    	infoData.put("atch_file_id", (String)paramMap.get("atch_file_id"));		    	
				lifeCycleDAO.insertLifeCycleReq(infoData);
			}else{
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
				if(infoData != null){
					infoData.put("login_user_id", login_user_id);
			    	lifeCycleDAO.insertLifeCycleReqAsset(infoData);
				}
			}
		}		
		
		
	}	
	
	public void deleteLifeCycleReq(ModelMap paramMap) throws NkiaException, IOException {
		lifeCycleDAO.deleteLifeCycleReqAsset(paramMap);
		lifeCycleDAO.deleteLifeCycleReq(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}		
	}	
	
	public void deleteLifeCycleReqAsset(ModelMap paramMap) throws NkiaException {
		lifeCycleDAO.deleteLifeCycleReqAsset(paramMap);
	}
	
	public void insertLifeCycleReqAsset(ModelMap paramMap) throws NkiaException {
		lifeCycleDAO.insertLifeCycleReqAsset(paramMap);
	}	
	
	public void updateLifeCycleReq(ModelMap paramMap) throws NkiaException {
		int mapSize = paramMap.size();
		String req_no = null;
		String login_user_id = null;
		
		for(int i=0; i<mapSize; i++){
			String mapIndex = toString().valueOf(i);
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			//신규테이블 정보 테이블관리테이블에 INSERT  >> 테이블 정보는 첫번째 맵에 담겨있음
			if(i == 0){
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
		    	if(isAuthenticated) {
		    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		    		login_user_id = userVO.getUser_id();
		    		infoData.put("login_user_id", userVO.getUser_id());
		    	}
		    	infoData.put("lifecycle_req_state", "1");
		    	infoData.put("atch_file_id", (String)paramMap.get("atch_file_id"));
		    	req_no = (String) infoData.get("req_no");
				lifeCycleDAO.updateLifeCycleReq(infoData);
				lifeCycleDAO.deleteLifeCycleReqAsset(infoData);
			}else{
				HashMap infoData = new HashMap();
				infoData = (HashMap) paramMap.get(mapIndex);
				if(infoData != null){
					infoData.put("login_user_id", login_user_id);
					infoData.put("req_no", req_no);
			    	lifeCycleDAO.insertLifeCycleReqAsset(infoData);
				}
			}
		}		
	}
	
	public void updateLifeCycleApprovalReq(ModelMap paramMap) throws NkiaException {
		lifeCycleDAO.updateLifeCycleApprovalReq(paramMap);
	}
	
	public void updateLifeCycleApproval(ModelMap paramMap) throws NkiaException {
		lifeCycleDAO.updateLifeCycleApproval(paramMap);
		lifeCycleDAO.updateAssetState(paramMap);
	}
	
	public void updateLifeCycleReturn(ModelMap paramMap) throws NkiaException {
		lifeCycleDAO.updateLifeCycleReturn(paramMap);
	}
	
	public List searchLifeCycleExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = lifeCycleDAO.searchLifeCycleExcelDown(paramMap); 
		return resultList;
	}
}
