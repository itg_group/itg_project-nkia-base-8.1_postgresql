/**
 * 
 */
package com.nkia.itg.itam.oper.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("operDetailManagerDAO")
public class OperDetailManagerDAO extends EgovComAbstractDAO {
	
	public List searchServerAssetMemoryList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchServerAssetMemoryList", paramMap);
		return resultList;
	}
	
	public List searchServerAssetIpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchServerAssetIpList", paramMap);
		return resultList;
	}
	
	public List searchServerAssetCpuList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchServerAssetCpuList", paramMap);
		return resultList;
	}
	
	public List searchAssetInnerDiskList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchAssetInnerDiskList", paramMap);
		return resultList;
	}
	
	public HashMap searchRelInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operDetailManagerDAO.searchRelInfo", paramMap);
	}
	
	public List searchSnbList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchSnbList", paramMap);
		return resultList;
	}
	
	public List searchServerPhysiLogicList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchServerPhysiLogicList", paramMap);
		return resultList;
	}
	
	public List searchStList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchStList", paramMap);
		return resultList;
	}
	
	public List searchBackUpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchBackUpList", paramMap);
		return resultList;
	}
	
	public List searchSoftWareList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchSoftWareList", paramMap);
		return resultList;
	}
	
	public List searchServerLogicList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchServerLogicList", paramMap);
		return resultList;
	}
	
	public List searchHAList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHAList", paramMap);
		return resultList;
	}
		
	public List searchPopSetHaList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchPopSetHaList", paramMap);
		return resultList;
	}
	
	public List searchAssetOperUserList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchAssetOperUserList", paramMap);
		return resultList;
	}
	
	public HashMap selectHistAssetSupplyInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operDetailManagerDAO.selectHistAssetSupplyInfo", paramMap);
	}
	
	public List searchHistServerAssetMemoryList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistServerAssetMemoryList", paramMap);
		return resultList;
	}
	
	public List searchHistServerAssetIpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistServerAssetIpList", paramMap);
		return resultList;
	}
	
	public List searchHistServerAssetCpuList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistServerAssetCpuList", paramMap);
		return resultList;
	}
	
	public List searchHistAssetInnerDiskList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistAssetInnerDiskList", paramMap);
		return resultList;
	}
	
	public List searchHistAssetOperUserList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistAssetOperUserList", paramMap);
		return resultList;
	}
	
	public List searchHistSnbList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistSnbList", paramMap);
		return resultList;
	}
	
	public List searchHistServerPhysiLogicList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistServerPhysiLogicList", paramMap);
		return resultList;
	}
	
	public List searchHistStList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistStList", paramMap);
		return resultList;
	}
	
	public List searchHistBackUpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistBackUpList", paramMap);
		return resultList;
	}
	
	public List searchHistHAList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistHAList", paramMap);
		return resultList;
	}
	
	public List searchHistSoftWareList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchHistSoftWareList", paramMap);
		return resultList;
	}
	// @@20130719 전민수 START
	// searchHistRelInfo : 구성연계 이력관리 조회
	public HashMap searchHistRelInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operDetailManagerDAO.searchHistRelInfo", paramMap);
	}
	// @@20130719 전민수 END   
	
	// @@20130724 전민수 START
	// searchAmServiceList : 자산원장 서비스연계 리스트 조회 신규추가
	public List searchAmServiceList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operDetailManagerDAO.searchAmServiceList", paramMap);
		return resultList;
	}
	// @@20130724 전민수 END  
}
