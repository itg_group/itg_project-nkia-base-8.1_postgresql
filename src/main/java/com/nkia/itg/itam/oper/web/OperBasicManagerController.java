package com.nkia.itg.itam.oper.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.oper.service.OperBasicManagerService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
/**
 * @version 1.0 2013. 4. 8.
 * @author <a href="mailto:minsoo@nkia.co.kr"> 
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class OperBasicManagerController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "operBasicManagerService")
	private OperBasicManagerService operBasicManagerService;
	
	/**
	 * message 호출
	 */
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 자산 기본정보 정보
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetBasicInfo.do")
	public @ResponseBody ResultVO searchAssetBasicInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		String amTable = new String();
		try{			
			String confType = (String) paramMap.get("asset_type");
			
			if(confType.equals("SW")){
				amTable = "AM_SW";
			}else{
				amTable = "AM_INFRA";
			}
			
			paramMap.put("amTable", amTable);
			
			HashMap resultMap = operBasicManagerService.searchAssetBasicInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 기본정보 정보
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchInfraBasicInfo.do")
	public @ResponseBody ResultVO searchInfraBasicInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{			
			String amTable = new String();
			HashMap resultMap = new HashMap();
			String confType = (String) paramMap.get("asset_type");
			
			if(confType.equals("SW")){
				//resultMap = operBasicManagerService.searchSwBasicInfo(paramMap);
			}else{
				resultMap = operBasicManagerService.searchInfraBasicInfo(paramMap);
			}

			resultVO.setResultMap(resultMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 기본정보 정보(이력)
     * @param paramMap
	 * @return
	 * @
	 */					   
	@RequestMapping(value="/itg/itam/oper/searchHistAssetInfo.do")
	public @ResponseBody ResultVO searchHistAssetInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{						
			HashMap resultMap = operBasicManagerService.searchHistAssetInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 스펙(서버) 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetServerSpecInfo.do")
	public @ResponseBody ResultVO searchAssetServerSpecInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = operBasicManagerService.searchAssetServerSpecInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 스펙(네트워크) 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetNetworkSpecInfo.do")
	public @ResponseBody ResultVO searchAssetNetworkSpecInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = operBasicManagerService.searchAssetNetworkSpecInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 스펙(보안) 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetSecuritySpecInfo.do")
	public @ResponseBody ResultVO searchAssetSecuritySpecInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = operBasicManagerService.searchAssetSecuritySpecInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 스펙(백업) 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetBackUpSpecInfo.do")
	public @ResponseBody ResultVO searchAssetBackUpSpecInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = operBasicManagerService.searchAssetBackUpSpecInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 스펙(스토리지) 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetStorageSpecInfo.do")
	public @ResponseBody ResultVO searchAssetStorageSpecInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = operBasicManagerService.searchAssetStorageSpecInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 스펙(소프트웨어) 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetSoftwareSpecInfo.do")
	public @ResponseBody ResultVO searchAssetSoftwareSpecInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = operBasicManagerService.searchAssetSoftwareSpecInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 유지보수업체 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetMaintBuyInfo.do")
	public @ResponseBody ResultVO searchAssetMaintBuyInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = operBasicManagerService.searchAssetMaintBuyInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 기본정보(기본정보, 스펙정보, 유지보수정보) 수정 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/updateAssetBasicInfo.do")
	public @ResponseBody ResultVO updateAssetBasicInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			operBasicManagerService.updateAssetBasicInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 기본정보(기본정보, 스펙정보, 유지보수정보) 수정 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/updateServerLogicInfo.do")
	public @ResponseBody ResultVO updateServerLogicInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			operBasicManagerService.updateServerLogicInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 서버자산 아이피 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetOperHist.do")
	public @ResponseBody ResultVO searchAssetOperHist(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operBasicManagerService.searchAssetOperHist(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 스펙(서버) 정보 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchLogicServerInfo.do")
	public @ResponseBody ResultVO searchLogicServerInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = operBasicManagerService.searchLogicServerInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
