/**
 * 
 */
package com.nkia.itg.itam.oper.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface OperBasicManagerService {
	
	public HashMap searchAssetBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchInfraBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchHistAssetInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchAssetMaintBuyInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchAssetServerSpecInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchAssetNetworkSpecInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchAssetSecuritySpecInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchAssetBackUpSpecInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchAssetStorageSpecInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchAssetSoftwareSpecInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateAssetBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateServerLogicInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchAssetOperHist(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchLogicServerInfo(ModelMap paramMap) throws NkiaException;
}
