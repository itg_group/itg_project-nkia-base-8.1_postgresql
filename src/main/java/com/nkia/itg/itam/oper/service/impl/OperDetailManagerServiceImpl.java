/**
 * 
 */
package com.nkia.itg.itam.oper.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.oper.dao.OperDetailManagerDAO;
import com.nkia.itg.itam.oper.service.OperDetailManagerService;


/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("operDetailManagerService")
public class OperDetailManagerServiceImpl implements OperDetailManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(OperDetailManagerServiceImpl.class);
	
	@Resource(name = "operDetailManagerDAO")
	public OperDetailManagerDAO operDetailManagerDAO;
	
	public List searchServerAssetMemoryList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchServerAssetMemoryList(paramMap);
	}
	
	public List searchServerAssetIpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchServerAssetIpList(paramMap);
	}
	
	public List searchServerAssetCpuList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchServerAssetCpuList(paramMap);
	}
	
	public List searchAssetInnerDiskList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchAssetInnerDiskList(paramMap);
	}

	public HashMap searchRelInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchRelInfo(paramMap);
	}
	
	public List searchSnbList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchSnbList(paramMap);
	}
	
	public List searchServerPhysiLogicList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchServerPhysiLogicList(paramMap);
	}
	
	public List searchStList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchStList(paramMap);
	}
	
	public List searchBackUpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchBackUpList(paramMap);
	}
	
	public List searchSoftWareList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchSoftWareList(paramMap);
	}
	
	public List searchServerLogicList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchServerLogicList(paramMap);
	}
	
	public List searchHAList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHAList(paramMap);
	}

	public List searchPopSetHaList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchPopSetHaList(paramMap);
	}
	
	public List searchAssetOperUserList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchAssetOperUserList(paramMap);
	}

	public HashMap selectHistAssetSupplyInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.selectHistAssetSupplyInfo(paramMap);
	}
	
	public List searchHistServerAssetMemoryList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistServerAssetMemoryList(paramMap);
	}
	
	public List searchHistServerAssetIpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistServerAssetIpList(paramMap);
	}
	
	public List searchHistServerAssetCpuList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistServerAssetCpuList(paramMap);
	}
	
	public List searchHistAssetInnerDiskList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistAssetInnerDiskList(paramMap);
	}
	
	public List searchHistAssetOperUserList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistAssetOperUserList(paramMap);
	}
	
	public List searchHistSnbList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistSnbList(paramMap);
	}
	
	public List searchHistServerPhysiLogicList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistServerPhysiLogicList(paramMap);
	}
	
	public List searchHistStList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistStList(paramMap);
	}
	
	public List searchHistBackUpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistBackUpList(paramMap);
	}
	
	public List searchHistHAList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistHAList(paramMap);
	}
	
	public List searchHistSoftWareList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistSoftWareList(paramMap);
	}
	// @@20130719 전민수 START
	// searchHistRelInfo : 구성연계 이력관리 조회
	public HashMap searchHistRelInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchHistRelInfo(paramMap);
	}
	// @@20130719 전민수 END  
	// @@20130724 전민수 START
	// searchAmServiceList : 자산원장 서비스연계 리스트 조회 신규추가
	public List searchAmServiceList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operDetailManagerDAO.searchAmServiceList(paramMap);
	}
	// @@20130724 전민수 END
}
