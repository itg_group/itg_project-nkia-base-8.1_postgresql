/**
 * 
 */
package com.nkia.itg.itam.oper.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("operBasicManagerDAO")
public class OperBasicManagerDAO extends EgovComAbstractDAO {
	
	public HashMap searchAssetBasicInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetBasicInfo", paramMap);
	}
	
	public HashMap searchInfraBasicInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchInfraBasicInfo", paramMap);
	}
	
	public HashMap searchHistAssetInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchHistAssetInfo", paramMap);
	}
	
	public HashMap searchAssetMaintBuyInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetMaintBuyInfo", paramMap);
	}
	
	public HashMap searchAssetServerSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetServerSpecInfo", paramMap);
	}
	
	public HashMap searchLogicServerInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchLogicServerInfo", paramMap);
	}
	
	public HashMap searchAssetNetworkSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetNetworkSpecInfo", paramMap);
	}
	
	public HashMap searchAssetSecuritySpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetSecuritySpecInfo", paramMap);
	}
	
	public HashMap searchAssetBackUpSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetBackUpSpecInfo", paramMap);
	}
	
	public HashMap searchAssetStorageSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetStorageSpecInfo", paramMap);
	}
	
	public HashMap searchAssetSoftwareSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (HashMap)selectByPk("operBasicManagerDAO.searchAssetSoftwareSpecInfo", paramMap);
	}
	
	public void updateGeneralInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateGeneralInfo", map);
	}
	
	public void updateServerLogicBasicInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateServerLogicBasicInfo", map);
	}
	
	public void updateServerLogicDetailInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateServerLogicDetailInfo", map);
	}
	
	public void updateSupplyInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateSupplyInfo", map);
	}
	
	public void updateInfraBasic(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateInfraBasic", map);
	}
	
	public void updateSpecSvInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateSpecSvInfo", map);
	}
	
	public void updateSpecNwInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateSpecNwInfo", map);
	}
	
	public void updateSpecScInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateSpecScInfo", map);
	}
	
	public void updateSpecBkInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateSpecBkInfo", map);
	}
	
	public void updateSpecStInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateSpecStInfo", map);
	}
	
	public void updateSpecSwInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateSpecSwInfo", map);
	}
	
	public void deleteCpuInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteCpuInfo", paramMap);
	}
	
	public void insertCpuInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertCpuInfo", paramMap);
	}
	
	public void deleteOperUserInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteOperUserInfo", paramMap);
	}
	
	public void insertOperUserInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertOperUserInfo", paramMap);
	}
	
	public void deleteIpInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteIpInfo", paramMap);
	}
	
	public void insertIpInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertIpInfo", paramMap);
	}
	
	public void deleteMemInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteMemInfo", paramMap);
	}
	
	public void insertMemInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertMemInfo", paramMap);
	}
	
	public void deleteInnerDiskInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteInnerDiskInfo", paramMap);
	}
	
	public void insertInnerDiskInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertInnerDiskInfo", paramMap);
	}
	
	public void deleteRelInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteRelInfo", paramMap);
	}
	
	// @@20130719 전민수 START
	// 1.deleteAllRelInfo : 자산원장 삭제로직 수정 
	public void deleteAllRelInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteAllRelInfo", paramMap);
	}
	// @@20130719 전민수 END
	
	public void insertRelInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertRelInfo", paramMap);
		this.insert("operBasicManagerDAO.insertReverseRelInfo", paramMap);
	}
	// @@20130719 전민수 START
	// 2.자산원장에서 인프라연계 논리서버에 대한 삭제 및 수정 로직이 없었으므로 신규 추가함
	public void deleteServerLogicRelInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteServerLogicRelInfo", paramMap);
	}
	
	public void deleteServerLogicInfraInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteServerLogicInfraInfo", paramMap);
	}
	// @@20130719 전민수 END
	
	public void deleteRelHaInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteRelHaInfo", paramMap);
	}
	
	public void insertRelHaInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertRelHaInfo", paramMap);
	}
	
	public void createHisSeqReverse(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.createHisSeqReverse", paramMap);
	}
	
	public void createHisSeq(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.createHisSeq", paramMap);
	}
	
	public String createSeq(Map map) throws NkiaException {
		// TODO Auto-generated method stub
		return (String) selectByPk("operBasicManagerDAO.createSeq", map);
	}
	
	public List searchAssetOperHist(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operBasicManagerDAO.searchAssetOperHist", paramMap);
		return resultList;
	}
	// @@20130719 전민수 START
	// 3.updateFileInfo : 첨부파일 기능 추가 
	public void updateFileInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateFileInfo", map);
	}
	// @@20130719 전민수 END
	
	// @@20130726 전민수 START
	// 4.서비스 연계 로직이 없으므로 신규추가 되었으며 ALLDELETE/ALLINSERT 로직임 
	public void deleteRelServiceInfo(Map paramMap) throws NkiaException {
		this.delete("operBasicManagerDAO.deleteRelServiceInfo", paramMap);
	}
	
	public void insertRelServiceInfo(Map paramMap) throws NkiaException {
		this.insert("operBasicManagerDAO.insertRelServiceInfo", paramMap);
	}
	// @@20130726 전민수 END
	
	public void updateAssetInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateAssetInfo", map);
	}
	
	public void updateMaintInfo(Map map) throws NkiaException {
		this.update("operBasicManagerDAO.updateMaintInfo", map);
	}
	
}
