/*
 * @(#)CheckTreeVO.java              2013. 3. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.oper.vo;

import java.io.Serializable;

import com.nkia.itg.base.vo.TreeVO;

public class OperTreeGridVO extends TreeVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String rnum;
	private String conf_id;
	private String asset_id;
	private String conf_nm;
	private String vendor_nm;
	private String model_nm;
	private String amdb_class;
	private String class_type;
	private String physi_conf_id;
	private String tangible_asset_yn;
	private int totalCount;
	
	
	public String getTangible_asset_yn() {
		return tangible_asset_yn;
	}
	public void setTangible_asset_yn(String tangible_asset_yn) {
		this.tangible_asset_yn = tangible_asset_yn;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public String getRnum() {
		return rnum;
	}
	public void setRnum(String rnum) {
		this.rnum = rnum;
	}
	public String getConf_id() {
		return conf_id;
	}
	public void setConf_id(String conf_id) {
		this.conf_id = conf_id;
	}
	public String getAsset_id() {
		return asset_id;
	}
	public void setAsset_id(String asset_id) {
		this.asset_id = asset_id;
	}
	public String getConf_nm() {
		return conf_nm;
	}
	public void setConf_nm(String conf_nm) {
		this.conf_nm = conf_nm;
	}
	public String getVendor_nm() {
		return vendor_nm;
	}
	public void setVendor_nm(String vendor_nm) {
		this.vendor_nm = vendor_nm;
	}
	public String getModel_nm() {
		return model_nm;
	}
	public void setModel_nm(String model_nm) {
		this.model_nm = model_nm;
	}
	public String getAmdb_class() {
		return amdb_class;
	}
	public void setAmdb_class(String amdb_class) {
		this.amdb_class = amdb_class;
	}
	public String getClass_type() {
		return class_type;
	}
	public void setClass_type(String class_type) {
		this.class_type = class_type;
	}
	public String getPhysi_conf_id() {
		return physi_conf_id;
	}
	public void setPhysi_conf_id(String physi_conf_id) {
		this.physi_conf_id = physi_conf_id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}

