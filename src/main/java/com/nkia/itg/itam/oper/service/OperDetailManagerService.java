/**
 * 
 */
package com.nkia.itg.itam.oper.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface OperDetailManagerService {
	
	public List searchServerAssetMemoryList(ModelMap paramMap) throws NkiaException;
	
	public List searchServerAssetIpList(ModelMap paramMap) throws NkiaException;
	
	public List searchServerAssetCpuList(ModelMap paramMap) throws NkiaException;
	
	public List searchAssetInnerDiskList(ModelMap paramMap) throws NkiaException;

	public HashMap searchRelInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchSnbList(ModelMap paramMap) throws NkiaException;
	
	public List searchServerPhysiLogicList(ModelMap paramMap) throws NkiaException;
	
	public List searchStList(ModelMap paramMap) throws NkiaException;
	
	public List searchBackUpList(ModelMap paramMap) throws NkiaException;
	
	public List searchSoftWareList(ModelMap paramMap) throws NkiaException;
	
	public List searchServerLogicList(ModelMap paramMap) throws NkiaException;
	
	public List searchHAList(ModelMap paramMap) throws NkiaException;
	
	public List searchPopSetHaList(ModelMap paramMap) throws NkiaException;
					
	public List searchAssetOperUserList(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectHistAssetSupplyInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchHistServerAssetMemoryList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistServerAssetIpList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistServerAssetCpuList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistAssetInnerDiskList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistAssetOperUserList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistServerPhysiLogicList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistSnbList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistStList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistBackUpList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistHAList(ModelMap paramMap) throws NkiaException;
	
	public List searchHistSoftWareList(ModelMap paramMap) throws NkiaException;
	// @@20130719 전민수 START
	// searchHistRelInfo : 구성연계 이력관리 조회
	public HashMap searchHistRelInfo(ModelMap paramMap) throws NkiaException;
	// @@20130719 전민수 END
	// @@20130724 전민수 START
	// searchAmServiceList : 자산원장 서비스연계 리스트 조회 신규추가
	public List searchAmServiceList(ModelMap paramMap) throws NkiaException;
	// @@20130724 전민수 END
	
}
