/**
 * 
 */
package com.nkia.itg.itam.oper.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.oper.dao.OperManagerDAO;
import com.nkia.itg.itam.oper.service.OperManagerService;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("operManagerService")
public class OperManagerServiceImpl implements OperManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(OperManagerServiceImpl.class);
	
	@Resource(name = "operManagerDAO")
	public OperManagerDAO operManagerDAO;
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchAssetList(paramMap);
	}
	
	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchAssetListCount(paramMap);
	}
	
	public List searchPopHaList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchPopHaList(paramMap);
	}
	
	public List searchPopHaRelAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchPopHaRelAssetList(paramMap);
	}
	
	public List searchCiAssetSoftWareList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetSoftWareList(paramMap);
	}
	
	public int searchCiAssetSoftWareListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetSoftWareListCount(paramMap);
	}
	
	public List searchCiAssetBackUpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetBackUpList(paramMap);
	}
	
	public int searchCiAssetBackUpListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetBackUpListCount(paramMap);
	}
	
	public List searchCiAssetStList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetStList(paramMap);
	}
	
	public int searchCiAssetStListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetStListCount(paramMap);
	}
	
	public List searchCiAssetSvList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetSvList(paramMap);
	}
	
	public int searchCiAssetSvListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetSvListCount(paramMap);
	}
	
	public List searchCiAssetNwList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetNwList(paramMap);
	}
	
	public int searchCiAssetNwListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetNwListCount(paramMap);
	}
	
	public List searchCiAssetScList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetScList(paramMap);
	}
	
	public int searchCiAssetScListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operManagerDAO.searchCiAssetScListCount(paramMap);
	}
	//@@20130711 전민수 START
    //신규추가
	public List searchAssetExcelDown(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = operManagerDAO.searchAssetExcelDown(paramMap); 
		return resultList;
	}
	//@@20130711 전민수 
    //END
	//@@20130726 전민수 START
	//서비스연계 추가할시 팝업내 시스템 리스트들 조회하는 쿼리
	public List searchSystemList(ModelMap paramMap) throws NkiaException {
		return operManagerDAO.searchSystemList(paramMap);
	}
	//@@20130726 전민수 END
}
