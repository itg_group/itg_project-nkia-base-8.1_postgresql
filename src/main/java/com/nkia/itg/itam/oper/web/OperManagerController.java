package com.nkia.itg.itam.oper.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.oper.service.OperManagerService;
import com.nkia.itg.itam.statistics.service.AssetStatisticsService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
/**
 * @version 1.0 2013. 4. 8.
 * @author <a href="mailto:minsoo@nkia.co.kr"> 
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class OperManagerController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "operManagerService")
	private OperManagerService operManagerService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name="assetStatisticsService")
	private AssetStatisticsService assetStatisticsService;
	
	/**
	 * 자산운영관리 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/oper/operManager.do")
	public String operManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/operManagerTab.nvf";
		paramMap.put("class_mng_type", request.getParameter("class_mng_type"));
		String center_asset_yn = StringUtil.parseString(request.getParameter("center_asset_yn"));
		paramMap.put("center_asset_yn", center_asset_yn);
		String view_type = StringUtil.parseString(request.getParameter("view_type"));
		paramMap.put("view_type", view_type);
		// 정정윤추가 - Webix 페이지
		forwarPage = "/itg/itam/automation/assetManagerMain.nvf";
		return forwarPage;
	}	
	
	
	@RequestMapping(value="/itg/itam/oper/operManagerList.do")
	public String operManagerList(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/oper/operManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산목록리스트
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetList.do")
	public @ResponseBody ResultVO searchAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				String assetViewAuth = assetStatisticsService.seelctAssetViewAuth(loginCustId);
				paramMap.put("loginCustId", loginCustId);
				paramMap.put("assetViewAuth", assetViewAuth);
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			totalCount = operManagerService.searchAssetListCount(paramMap);
			List resultList = operManagerService.searchAssetList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	

	/**
	 * 이중화 팝업 이중화 그룹 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchPopHaList.do")
	public @ResponseBody ResultVO searchPopHaList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operManagerService.searchPopHaList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 *  이중화 팝업 이중화그룹 연관 자산 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchPopHaRelAssetList.do")
	public @ResponseBody ResultVO searchPopHaRelAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operManagerService.searchPopHaRelAssetList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 구성조회팝업(서버) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchCiAssetSvList.do")
	public @ResponseBody ResultVO searchCiAssetSvList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = null;
			GridVO gridVO = new GridVO();
			int page = (Integer) paramMap.get("page");
			int limit = (Integer) paramMap.get("limit");
			int start = (Integer) paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;

			boolean initFlag = false;
			if (paramMap.get("init") != null) {
				initFlag = (Boolean) paramMap.get("init");
			}

			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);

			if (!initFlag) {
				totalCount = operManagerService.searchCiAssetSvListCount(paramMap);
				resultList = operManagerService.searchCiAssetSvList(paramMap);
			}

			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 구성조회팝업(네트워크) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchCiAssetNwList.do")
	public @ResponseBody ResultVO searchCiAssetNwList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			

			if(!initFlag) {
				totalCount = operManagerService.searchCiAssetNwListCount(paramMap);
				resultList = operManagerService.searchCiAssetNwList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 구성조회팝업(보안) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchCiAssetScList.do")
	public @ResponseBody ResultVO searchCiAssetScList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			

			if(!initFlag) {
				totalCount = operManagerService.searchCiAssetScListCount(paramMap);
				resultList = operManagerService.searchCiAssetScList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 구성조회팝업() 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchCiAssetStList.do")
	public @ResponseBody ResultVO searchCiAssetStList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			

			if(!initFlag) {
				totalCount = operManagerService.searchCiAssetStListCount(paramMap);
				resultList = operManagerService.searchCiAssetStList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 구성조회팝업() 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchCiAssetBackUpList.do")
	public @ResponseBody ResultVO searchCiAssetBackUpList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			

			if(!initFlag) {
					totalCount = operManagerService.searchCiAssetBackUpListCount(paramMap);
					resultList = operManagerService.searchCiAssetBackUpList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 구성조회팝업() 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchCiAssetSoftWareList.do")
	public @ResponseBody ResultVO searchCiAssetSoftWareList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = null;
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			

			if(!initFlag) {
					totalCount = operManagerService.searchCiAssetSoftWareListCount(paramMap);
					resultList = operManagerService.searchCiAssetSoftWareList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//@@20130711 전민수 START
    //신규추가
	/**
	 * 자산 엑셀다운로드
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetExcelDown.do")
	public @ResponseBody ResultVO searchAssetExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			param.put("class_type", "SV");
			List svList  = operManagerService.searchAssetExcelDown(param);
			param.put("class_type", "NW");
			List nwList  = operManagerService.searchAssetExcelDown(param);
			param.put("class_type", "BK");
			List bkList  = operManagerService.searchAssetExcelDown(param);
			param.put("class_type", "SC");
			List scList  = operManagerService.searchAssetExcelDown(param);
			param.put("class_type", "ST");
			List stList  = operManagerService.searchAssetExcelDown(param);
			param.put("class_type", "BA");
			List baList  = operManagerService.searchAssetExcelDown(param);
			param.put("class_type", "ETC");
			List etcList = operManagerService.searchAssetExcelDown(param);
			param.put("class_type", "SW");
			List swList  = operManagerService.searchAssetExcelDown(param);
			
			resultArray.add(svList);
			resultArray.add(nwList);
			resultArray.add(bkList);
			resultArray.add(scList);
			resultArray.add(stList);
			resultArray.add(baList);
			resultArray.add(etcList);
			resultArray.add(swList);
			
			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	//@@20130711 전민수 
    //END
	
	//@@20130726 전민수 START
	//서비스연계 추가할시 팝업내 시스템 리스트들 조회하는 쿼리
	/**
	 * 서비스연계 추가시 팝업내 시스템리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchSystemList.do")
	public @ResponseBody ResultVO searchSystemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = operManagerService.searchSystemList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			new NkiaException(e);
		}
		return resultVO;
	}
	//@@20130726 전민수 END
}
