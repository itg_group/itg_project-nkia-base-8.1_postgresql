package com.nkia.itg.itam.oper.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.oper.service.OperDetailManagerService;

import egovframework.com.cmm.EgovMessageSource;
/**
 * @version 1.0 2013. 4. 8.
 * @author <a href="mailto:minsoo@nkia.co.kr"> 
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class OperDetailManagerController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "operDetailManagerService")
	private OperDetailManagerService operDetailManagerService;
	
	/**
	 * message 호출
	 */
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 자상 원장 서버자산 메모리 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchServerAssetMemoryList.do")
	public @ResponseBody ResultVO searchServerAssetMemoryList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchServerAssetMemoryList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 서버자산 메모리 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistServerAssetMemoryList.do")
	public @ResponseBody ResultVO searchHistServerAssetMemoryList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistServerAssetMemoryList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 서버자산 아이피 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchServerAssetIpList.do")
	public @ResponseBody ResultVO searchServerAssetIpList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchServerAssetIpList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 서버자산 아이피 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistServerAssetIpList.do")
	public @ResponseBody ResultVO searchHistServerAssetIpList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistServerAssetIpList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	// @@20130724 전민수 START
	// searchAmServiceList : 자산원장 서비스연계 리스트 조회 신규추가
	/**
	 * 자상 원장 서비스연계 서비스리스트
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/oper/searchAmServiceList.do")
	public @ResponseBody ResultVO searchAmServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchAmServiceList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	// @@20130724 전민수 END
	/**
	 * 자상 원장 서버자산 CPU 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchServerAssetCpuList.do")
	public @ResponseBody ResultVO searchServerAssetCpuList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchServerAssetCpuList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 서버자산 CPU 리스트(이력) 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistServerAssetCpuList.do")
	public @ResponseBody ResultVO searchHistServerAssetCpuList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistServerAssetCpuList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 내장디스크 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetInnerDiskList.do")
	public @ResponseBody ResultVO searchAssetInnerDiskList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchAssetInnerDiskList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 내장디스크 리스트 (이력)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistAssetInnerDiskList.do")
	public @ResponseBody ResultVO searchHistAssetInnerDiskList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistAssetInnerDiskList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 자상 원장 스토리지 장비(DISK) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchRelInfo.do")
	public @ResponseBody ResultVO searchRelInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = operDetailManagerService.searchRelInfo(paramMap);		
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * 자상 원장 연계정보(서버,네트워크,보안) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchSnbList.do")
	public @ResponseBody ResultVO searchSnbList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			String snbTable = null;
			List resultList = null;
			String confType = (String) paramMap.get("conf_type");
			if(confType.equals("SV")){
				resultList = operDetailManagerService.searchServerPhysiLogicList(paramMap);		
			}else if (confType.equals("NW")) {
				snbTable = "AM_NW";
				paramMap.put("snbTable", snbTable);
				resultList = operDetailManagerService.searchSnbList(paramMap);		
			}else if (confType.equals("SC")) {
				snbTable = "AM_SC";
				paramMap.put("snbTable", snbTable);
				resultList = operDetailManagerService.searchSnbList(paramMap);		
			}
			
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 연계정보(스토리지) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchStList.do")
	public @ResponseBody ResultVO searchStList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchStList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 연계정보(백업) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchBackUpList.do")
	public @ResponseBody ResultVO searchBackUpList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchBackUpList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 연계정보(소프트웨어) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchSoftWareList.do")
	public @ResponseBody ResultVO searchSoftWareList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchSoftWareList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 연계정보(논리서버) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchServerLogicList.do")
	public @ResponseBody ResultVO searchServerLogicList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchServerLogicList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 스토리지 장비(이중화) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHAList.do")
	public @ResponseBody ResultVO searchHAList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHAList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * 자상 원장 스토리지 장비(이중화) 리스트 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchPopSetHaList.do")
	public @ResponseBody ResultVO searchPopSetHaList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchPopSetHaList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * 자상 원장 작업자 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchAssetOperUser.do")
	public @ResponseBody ResultVO searchAssetWorkUser(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchAssetOperUserList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 작업자 리스트  (이력)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistAssetOperUser.do")
	public @ResponseBody ResultVO searchHistAssetWorkUser(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistAssetOperUserList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 공급계약업체 정보 출력 (이력)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/selectHistAssetSupplyInfo.do")
	public @ResponseBody ResultVO selectHistAssetSupplyInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = operDetailManagerService.selectHistAssetSupplyInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 연계정보(서버,네트워크,보안) 리스트 (이력)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistSnbList.do")
	public @ResponseBody ResultVO searchHistSnbList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			String snbTable = null;
			List resultList = null;
			String confType = (String) paramMap.get("conf_type");
			if(confType.equals("SV")){
				resultList = operDetailManagerService.searchHistServerPhysiLogicList(paramMap);		
			}else if (confType.equals("NW")) {
				snbTable = "AM_NW";
				paramMap.put("snbTable", snbTable);
				resultList = operDetailManagerService.searchHistSnbList(paramMap);		
			}else if (confType.equals("SC")) {
				snbTable = "AM_SC";
				paramMap.put("snbTable", snbTable);
				resultList = operDetailManagerService.searchHistSnbList(paramMap);		
			}
			
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 연계정보(스토리지) 리스트 (이력)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistStList.do")
	public @ResponseBody ResultVO searchHistStList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistStList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 연계정보(백업) 리스트 (이력)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistBackUpList.do")
	public @ResponseBody ResultVO searchHistBackUpList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistBackUpList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 스토리지 장비(이중화) 리스트 (이력)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistHAList.do")
	public @ResponseBody ResultVO searchHistHAList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistHAList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 자상 원장 연계정보(소프트웨어) 리스트 (이력)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistSoftWareList.do")
	public @ResponseBody ResultVO searcHistSoftWareList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = operDetailManagerService.searchHistSoftWareList(paramMap);		
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자상 원장 인프라연계정보 갯수(이력)
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/oper/searchHistRelInfo.do")
	public @ResponseBody ResultVO searchHistRelInfo(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = operDetailManagerService.searchHistRelInfo(paramMap);		
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
}
