/**
 * 
 */
package com.nkia.itg.itam.oper.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.oper.dao.OperBasicManagerDAO;
import com.nkia.itg.itam.oper.service.OperBasicManagerService;


/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:jwjeong@nkia.co.kr"> jwjeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("operBasicManagerService")
public class OperBasicManagerServiceImpl implements OperBasicManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(OperBasicManagerServiceImpl.class);
	
	@Resource(name = "operBasicManagerDAO")
	public OperBasicManagerDAO operBasicManagerDAO;
	
	public HashMap searchAssetBasicInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetBasicInfo(paramMap);
	}
	
	public HashMap searchInfraBasicInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchInfraBasicInfo(paramMap);
	}
	
	public HashMap searchHistAssetInfo(ModelMap paramMap) throws NkiaException {
		return operBasicManagerDAO.searchHistAssetInfo(paramMap);
	}
	
	public HashMap searchAssetServerSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetServerSpecInfo(paramMap);
	}
	
	public HashMap searchAssetNetworkSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetNetworkSpecInfo(paramMap);
	}
	
	public HashMap searchAssetSecuritySpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetSecuritySpecInfo(paramMap);
	}
	
	public HashMap searchAssetBackUpSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetBackUpSpecInfo(paramMap);
	}
	
	public HashMap searchAssetStorageSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetStorageSpecInfo(paramMap);
	}
	
	public HashMap searchAssetMaintBuyInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetMaintBuyInfo(paramMap);
	}
	
	public HashMap searchAssetSoftwareSpecInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetSoftwareSpecInfo(paramMap);
	}
	
	public HashMap searchLogicServerInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchLogicServerInfo(paramMap);
	}
	
	public void updateServerLogicInfo(ModelMap paramMap) throws NkiaException {
		String updUserId = (String) paramMap.get("upd_user_id");
		
		Map logicBasicInfoMap = (Map) paramMap.get("logicBasicInfoMap");
		logicBasicInfoMap.put("upd_user_id", updUserId);
		Map logicSpecInfoMap = (Map) paramMap.get("logicSpecInfoMap");
		logicSpecInfoMap.put("upd_user_id", updUserId);
		
		String seq = operBasicManagerDAO.createSeq(logicBasicInfoMap);
		logicBasicInfoMap.put("seq", seq);
		operBasicManagerDAO.createHisSeq(logicBasicInfoMap);
		operBasicManagerDAO.createHisSeqReverse(logicBasicInfoMap);
		
		operBasicManagerDAO.updateServerLogicBasicInfo(logicBasicInfoMap);
		operBasicManagerDAO.updateServerLogicDetailInfo(logicSpecInfoMap);
	}
	
	public void updateAssetBasicInfo(ModelMap paramMap) throws NkiaException {
		
		String updUserId = (String) paramMap.get("upd_user_id");
		
		Map map = (Map) paramMap.get("infraTotalMap");
		Map specMap = (Map) paramMap.get("specMap");
		
		map.put("upd_user_id", updUserId);
		specMap.put("upd_user_id", updUserId);
		
		Map assetMap = (Map) paramMap.get("assetMap");
		Map maintMap = (Map) paramMap.get("maintMap");
		
		assetMap.put("upd_user_id", updUserId);
		maintMap.put("upd_user_id", updUserId);

		String seq = operBasicManagerDAO.createSeq(map);
		map.put("seq", seq);
		
		operBasicManagerDAO.createHisSeq(map);
		operBasicManagerDAO.createHisSeqReverse(map);
		
		String classType = (String) map.get("class_type");
		String amTable = new String();
		
		if(classType.equals("SW")){
			amTable = "AM_SW";
		}else{
			amTable = "AM_INFRA";
		}
		
		map.put("amTable", amTable);
		if(classType.equals("SW")){
			amTable = "AM_SW";
		}else{
			amTable = "AM_INFRA";
		}
		map.put("amTable", amTable);
		
		operBasicManagerDAO.updateInfraBasic(map);
		operBasicManagerDAO.updateAssetInfo(assetMap);
		operBasicManagerDAO.updateMaintInfo(maintMap);
				
		
		Map fileMap = (Map) paramMap.get("fileMap");
		operBasicManagerDAO.updateFileInfo(fileMap);
		
		if(classType.equals("SV")){
			operBasicManagerDAO.updateSpecSvInfo((Map) paramMap.get("specMap"));
		}else if(classType.equals("NW")){
			operBasicManagerDAO.updateSpecNwInfo((Map)paramMap.get("specMap"));
		}else if(classType.equals("SC")){
			operBasicManagerDAO.updateSpecScInfo((Map)paramMap.get("specMap"));
		}else if(classType.equals("BK")){
			operBasicManagerDAO.updateSpecBkInfo((Map)paramMap.get("specMap"));
		}else if(classType.equals("ST")){
			operBasicManagerDAO.updateSpecStInfo((Map)paramMap.get("specMap"));
		}else if(classType.equals("SW")){
			operBasicManagerDAO.updateSpecSwInfo((Map)paramMap.get("specMap"));
		}
		
		//공급계약 정보 수정
		Map supplyMap = (Map) paramMap.get("supplyMap");
		supplyMap.put("upd_user_id", updUserId);
		
		operBasicManagerDAO.updateSupplyInfo(supplyMap);
		
		Map operUserMap = (Map) paramMap.get("operUserMap");
		Map cpuMap = (Map) paramMap.get("cpuMap");
		Map memMap = (Map) paramMap.get("memMap");
		Map ipMap = (Map) paramMap.get("ipMap");
		Map innerDiskMap = (Map) paramMap.get("innerDiskMap");
		
		operUserMap.put("upd_user_id", updUserId);
		cpuMap.put("upd_user_id", updUserId);
		memMap.put("upd_user_id", updUserId);
		ipMap.put("upd_user_id", updUserId);
		innerDiskMap.put("upd_user_id", updUserId);
		
		List<Map> cpuStoreArray = new ArrayList<Map>(); 
		cpuStoreArray = (ArrayList) cpuMap.get("cpuStore");
		
		// CPU정보 전체 삭제 
		operBasicManagerDAO.deleteCpuInfo(cpuMap);
		
		// CPU정보 등록
		for(int i = 0; i < cpuStoreArray.size();  i++){
			cpuStoreArray.get(i).put("seq", i+1);
			operBasicManagerDAO.insertCpuInfo(cpuStoreArray.get(i));
		}
		
		List<Map> operUserArray = new ArrayList<Map>(); 
		operUserArray = (ArrayList) operUserMap.get("operUserStore");
		
		// 담당자정보 전체 삭제 
		operBasicManagerDAO.deleteOperUserInfo(operUserMap);
		
		// 담당자정보 등록
		for(int i = 0; i < operUserArray.size();  i++){
			operUserArray.get(i).put("seq", i+1);
			operBasicManagerDAO.insertOperUserInfo(operUserArray.get(i));
		}
		
		List<Map> ipStoreArray = new ArrayList<Map>(); 
		ipStoreArray = (ArrayList) ipMap.get("ipStore");

		// IP정보 전체 삭제
		operBasicManagerDAO.deleteIpInfo(ipMap);
		
		// IP정보 등록
		for(int i = 0; i < ipStoreArray.size();  i++){
			ipStoreArray.get(i).put("seq", i+1);
			operBasicManagerDAO.insertIpInfo(ipStoreArray.get(i));
		}
		
		List<Map> memStoreArray = new ArrayList<Map>(); 
		memStoreArray = (ArrayList) memMap.get("memStore");

		// MEM정보 전체 삭제 
		operBasicManagerDAO.deleteMemInfo(memMap);
		
		// MEM정보 등록
		for(int i = 0; i < memStoreArray.size();  i++){
			memStoreArray.get(i).put("seq", i+1);
			operBasicManagerDAO.insertMemInfo(memStoreArray.get(i));
		}
		
		if(classType.equals("SV")){
			List<Map> inDiskStoreArray = new ArrayList<Map>(); 
			inDiskStoreArray = (ArrayList) innerDiskMap.get("innerDiskGridStore");

			// 내장디스크정보(서버일경우) 전체 삭제 
			operBasicManagerDAO.deleteInnerDiskInfo(innerDiskMap);
			
			// 내장디스크정보(서버일경우) 등록
			for(int i = 0; i < inDiskStoreArray.size();  i++){
				inDiskStoreArray.get(i).put("seq", i+1);
				operBasicManagerDAO.insertInnerDiskInfo(inDiskStoreArray.get(i));
			}
		}
		
		//연계정보
		Map relSvAssetMap = (Map) paramMap.get("relSvAssetMap");
		Map relNwAssetMap = (Map) paramMap.get("relNwAssetMap");
		Map relScAssetMap = (Map) paramMap.get("relScAssetMap");
		Map relServiceAssetMap = (Map) paramMap.get("relServiceAssetMap");
		
		// @@20130719 전민수 START 
		// 2.자산원장에서 인프라연계 논리서버에 대한 삭제로직이 없었으므로 신규 추가함 
		Map relSvlAssetMap = (Map) paramMap.get("relSvlAssetMap");
		// @@20130719 전민수 END
		Map relStAssetMap = (Map) paramMap.get("relStAssetMap");
		Map relBkAssetMap = (Map) paramMap.get("relBkAssetMap");
		Map relSwAssetMap = (Map) paramMap.get("relSwAssetMap");
		Map relHaAssetMap = (Map) paramMap.get("relHaAssetMap");
		
		relSvAssetMap.put("upd_user_id", updUserId);
		relNwAssetMap.put("upd_user_id", updUserId);
		relScAssetMap.put("upd_user_id", updUserId);
		relServiceAssetMap.put("upd_user_id", updUserId);
		// @@20130719 전민수 START 
		// 2.자산원장에서 인프라연계 논리서버에 대한 삭제로직이 없었으므로 신규 추가함 
		relSvlAssetMap.put("upd_user_id", updUserId);
		// @@20130719 전민수 END
		relStAssetMap.put("upd_user_id", updUserId);
		relBkAssetMap.put("upd_user_id", updUserId);
		relSwAssetMap.put("upd_user_id", updUserId);
		relHaAssetMap.put("upd_user_id", updUserId);
		
		//서버연계정보
		List<Map> insertSvStore = new ArrayList<Map>(); 
		insertSvStore = (ArrayList) relSvAssetMap.get("svStore");
		
		// @@20130719 전민수 START 
		// 3. 자산원장 삭제로직 변경
		operBasicManagerDAO.deleteAllRelInfo(relSvAssetMap);
		// @@20130719 전민수 END
		operBasicManagerDAO.deleteRelInfo(relSvAssetMap);
		
		if(insertSvStore != null){
			for(int i = 0; i < insertSvStore.size();  i++){
				operBasicManagerDAO.insertRelInfo(insertSvStore.get(i));
			}
		}
		
		//네트워크연계정보
		List<Map> insertNwStore = new ArrayList<Map>(); 
		insertNwStore = (ArrayList) relNwAssetMap.get("nwStore");
		
		operBasicManagerDAO.deleteRelInfo(relNwAssetMap);
		
		if(insertNwStore != null){
			for(int i = 0; i < insertNwStore.size();  i++){
				operBasicManagerDAO.insertRelInfo(insertNwStore.get(i));
			}
		}
		//보안연계정보
		List<Map> insertScStore = new ArrayList<Map>(); 
		insertScStore = (ArrayList) relScAssetMap.get("scStore");
		
		operBasicManagerDAO.deleteRelInfo(relScAssetMap);
		
		if(insertScStore != null){
			for(int i = 0; i < insertScStore.size();  i++){
				operBasicManagerDAO.insertRelInfo(insertScStore.get(i));
			}
		}
		
		//@@20130726 전민수 START
		// 4.서비스연계 추가할시 팝업내 시스템 리스트들 조회하는 쿼리
		List<Map> insertServiceRelStore = new ArrayList<Map>(); 
		insertServiceRelStore = (ArrayList) relServiceAssetMap.get("serviceRelStore");

		operBasicManagerDAO.deleteRelServiceInfo(relServiceAssetMap);
		
		for(int i = 0; i < insertServiceRelStore.size();  i++){
			operBasicManagerDAO.insertRelServiceInfo(insertServiceRelStore.get(i));
		}
		//@@20130726 전민수 END
		// @@20130719 전민수 START 
		// 2.자산원장에서 인프라연계 논리서버에 대한 삭제 및 수정 로직이 없었으므로 신규 추가함
		List<Map> insertSvlStore = new ArrayList<Map>(); 
		insertSvlStore = (ArrayList) relSvlAssetMap.get("svlStore");
		
		if(insertSvlStore != null){
			for(int i = 0; i < insertSvlStore.size();  i++){
				Map svlStore = insertSvlStore.get(i);
				String conf_id = (String) svlStore.get("conf_id");
			}
		}
		operBasicManagerDAO.deleteServerLogicRelInfo(relSvlAssetMap);
		operBasicManagerDAO.deleteServerLogicInfraInfo(relSvlAssetMap);
		// @@20130719 전민수 END
		
		List<Map> insertStStore = new ArrayList<Map>(); 
		insertStStore = (ArrayList) relStAssetMap.get("stStore");
		
		operBasicManagerDAO.deleteRelInfo(relStAssetMap);
		
		if(insertStStore != null){
			for(int i = 0; i < insertStStore.size();  i++){
				operBasicManagerDAO.insertRelInfo(insertStStore.get(i));
			}
		}
		
		List<Map> insertBkStore = new ArrayList<Map>(); 
		insertBkStore = (ArrayList) relBkAssetMap.get("bkStore");
		
		operBasicManagerDAO.deleteRelInfo(relBkAssetMap);
		
		if(insertBkStore != null){
			for(int i = 0; i < insertBkStore.size();  i++){
				operBasicManagerDAO.insertRelInfo(insertBkStore.get(i));
			}
		}
		
		List<Map> insertSwStore = new ArrayList<Map>(); 
		insertSwStore = (ArrayList) relSwAssetMap.get("swStore");
		
		operBasicManagerDAO.deleteRelInfo(relSwAssetMap);
		
		if(insertSwStore != null){
			for(int i = 0; i < insertSwStore.size();  i++){
				operBasicManagerDAO.insertRelInfo(insertSwStore.get(i));
			}
		}
		
		List<Map> insertHaStore = new ArrayList<Map>(); 
		insertHaStore = (ArrayList) relHaAssetMap.get("haStore");

		operBasicManagerDAO.deleteRelHaInfo(relHaAssetMap);
		
		for(int i = 0; i < insertHaStore.size();  i++){
			operBasicManagerDAO.insertRelHaInfo(insertHaStore.get(i));
		}
	}
	
	public List searchAssetOperHist(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return operBasicManagerDAO.searchAssetOperHist(paramMap);
	}
}
