/**
 * 
 */
package com.nkia.itg.itam.oper.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface OperManagerService {

	public int searchAssetListCount(ModelMap paramMap) throws NkiaException;

	public List searchAssetList(ModelMap paramMap) throws NkiaException;
	
	public List searchPopHaList(ModelMap paramMap) throws NkiaException;
	
	public List searchPopHaRelAssetList(ModelMap paramMap) throws NkiaException;
	
	public int searchCiAssetSoftWareListCount(ModelMap paramMap) throws NkiaException;

	public List searchCiAssetSoftWareList(ModelMap paramMap) throws NkiaException;
	
	public int searchCiAssetBackUpListCount(ModelMap paramMap) throws NkiaException;

	public List searchCiAssetBackUpList(ModelMap paramMap) throws NkiaException;
	
	public int searchCiAssetStListCount(ModelMap paramMap) throws NkiaException;

	public List searchCiAssetStList(ModelMap paramMap) throws NkiaException;
	
	public int searchCiAssetSvListCount(ModelMap paramMap) throws NkiaException;

	public List searchCiAssetSvList(ModelMap paramMap) throws NkiaException;
	
	public int searchCiAssetScListCount(ModelMap paramMap) throws NkiaException;

	public List searchCiAssetScList(ModelMap paramMap) throws NkiaException;
	
	public int searchCiAssetNwListCount(ModelMap paramMap) throws NkiaException;

	public List searchCiAssetNwList(ModelMap paramMap) throws NkiaException;
	//@@20130711 전민수 START
    //신규추가
	public List searchAssetExcelDown(Map paramMap) throws NkiaException;
	//@@20130711 전민수 
    //END
	
	//@@20130726 전민수 START
	//서비스연계 추가할시 팝업내 시스템 리스트들 조회하는 쿼리
	public List searchSystemList(ModelMap paramMap) throws NkiaException;
	//@@20130726 전민수 END
	
}
