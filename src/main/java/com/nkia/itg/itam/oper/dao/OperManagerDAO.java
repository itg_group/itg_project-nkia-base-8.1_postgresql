/**
 * 
 */
package com.nkia.itg.itam.oper.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("operManagerDAO")
public class OperManagerDAO extends EgovComAbstractDAO {
	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchAssetList", paramMap);
		return resultList;
	}
	
	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("operManagerDAO.searchAssetListCount", paramMap);
		return count;
	}
	
	public List searchPopHaList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchPopHaList", paramMap);
		return resultList;
	}
	
	public List searchPopHaRelAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchPopHaRelAssetList", paramMap);
		return resultList;
	}
	
	public List searchCiAssetSoftWareList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchCiAssetSoftWareList", paramMap);
		return resultList;
	}
	
	public int searchCiAssetSoftWareListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("operManagerDAO.searchCiAssetSoftWareListCount", paramMap);
		return count;
	}
	
	public List searchCiAssetBackUpList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchCiAssetBackUpList", paramMap);
		return resultList;
	}
	
	public int searchCiAssetBackUpListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("operManagerDAO.searchCiAssetBackUpListCount", paramMap);
		return count;
	}
	
	public List searchCiAssetStList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchCiAssetStList", paramMap);
		return resultList;
	}
	
	public int searchCiAssetStListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("operManagerDAO.searchCiAssetStListCount", paramMap);
		return count;
	}
	
	public List searchCiAssetSvList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchCiAssetSvList", paramMap);
		return resultList;
	}
	
	public int searchCiAssetSvListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("operManagerDAO.searchCiAssetSvListCount", paramMap);
		return count;
	}
	
	public List searchCiAssetNwList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchCiAssetNwList", paramMap);
		return resultList;
	}
	
	public int searchCiAssetNwListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("operManagerDAO.searchCiAssetNwListCount", paramMap);
		return count;
	}
	
	public List searchCiAssetScList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("operManagerDAO.searchCiAssetScList", paramMap);
		return resultList;
	}
	
	public int searchCiAssetScListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("operManagerDAO.searchCiAssetScListCount", paramMap);
		return count;
	}
	//@@20130711 전민수 START
    //신규추가
	public List searchAssetExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("operManagerDAO.searchAssetExcelDown", paramMap);
		return resultList;
	}
	//@@20130711 전민수 
    //END
	
	//@@20130726 전민수 START
    //서비스연계 추가할시 팝업내 시스템 리스트들 조회하는 쿼리
	public List searchSystemList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("operManagerDAO.searchSystemList", paramMap);
		return resultList;
	}
	//@@20130726 전민수 END
}
