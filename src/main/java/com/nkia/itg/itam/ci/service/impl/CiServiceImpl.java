/*
 * @(#)lifeCycleServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.ci.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.ci.dao.CiDAO;
import com.nkia.itg.itam.ci.service.CiService;

@Service("ciService")
public class CiServiceImpl implements CiService{
	
	private static final Logger logger = LoggerFactory.getLogger(CiServiceImpl.class);
	
	@Resource(name = "ciDAO")
	public CiDAO ciDAO;
	
	public List searchCiList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return ciDAO.searchCiList(paramMap);
	}
	
	public int searchCiListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return ciDAO.searchCiListCount(paramMap);
	}
	
}
