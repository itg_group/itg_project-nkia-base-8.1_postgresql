/*
 * @(#)lifeCycleService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.ci.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface CiService {
	
	public int searchCiListCount(ModelMap paramMap) throws NkiaException;

	public List searchCiList(ModelMap paramMap) throws NkiaException;
	
}
