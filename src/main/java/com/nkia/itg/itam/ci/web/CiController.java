/*
 * @(#)CiController.java              2013. 6. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.ci.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.ci.service.CiService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class CiController {
	
	@Resource(name = "ciService")
	private CiService ciService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@RequestMapping(value="/itg/itam/ci/ciSearchManager.do")
	public String maintManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/ci/ciSearchManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 구성목록리스트
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/ci/searchCiList.do")
	public @ResponseBody ResultVO searchNewAssetList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = ciService.searchCiListCount(paramMap);
			List resultList = ciService.searchCiList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
