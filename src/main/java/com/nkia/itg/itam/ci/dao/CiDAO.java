/*
 * @(#)LifeCycleDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.ci.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("ciDAO")
public class CiDAO extends EgovComAbstractDAO{
	
	public List searchCiList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("ciManagerDAO.searchCiList", paramMap);
		return resultList;
	}
	
	public int searchCiListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("ciManagerDAO.searchCiListCount", paramMap);
		return count;
	}
}
