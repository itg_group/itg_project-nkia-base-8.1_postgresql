package com.nkia.itg.itam.inspect.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

/* *ITAM 정기점검* */

@SuppressWarnings({ "rawtypes" })
public interface InspectService {
	List inspectResultProgressChart(Map paramMap) throws Exception;
	List inspectResultProgressChartExcel(Map paramMap) throws Exception;
	
	Map selectInspectCountLimit(Map paramMap) throws Exception;
	boolean updateInspectCountLimit(Map paramMap) throws Exception;
	Map selectInspectItem(Map paramMap) throws Exception;
	List selectInspectItemList(Map paramMap) throws Exception;
	Integer selectInspectItemCount(Map paramMap) throws Exception;
	boolean saveInspectItem(Map paramMap) throws Exception;
	
	boolean insertInspectItemMapping(Map paramMap) throws Exception;
	List selectInspectItemMappingList(Map paramMap) throws Exception;
	Integer selectInspectItemMappingCount(Map paramMap) throws Exception;
	List selectInspectFullJoinedMappingItem(Map paramMap) throws Exception;
	Integer selectInspectFullJoinedMappingCount(Map paramMap) throws Exception;
	
	boolean insertInspectResultHead(Map paramMap) throws Exception;
	List selectInspectResultHeadList(Map paramMap) throws Exception;
	Integer selectInspectResultHeadCount(Map paramMap) throws Exception;
	boolean confirmInspectResultHead(Map paramMap) throws Exception;
	boolean deconfirmInspectResultHead(Map paramMap) throws Exception;
	
	boolean insertInspectResultDetail(Map paramMap) throws Exception;
	boolean insertInspectResultDetailByExcel(Map paramMap) throws Exception;
	List selectInspectResultDetailList(Map paramMap) throws Exception;
	Integer selectInspectResultDetailCount(Map paramMap) throws Exception;
	List selectAllInspectResultDetailList(Map paramMap) throws Exception;
	
	public List searchAssetList(ModelMap paramMap) throws Exception;
	public int searchAssetListCount(ModelMap paramMap) throws Exception;
	
	public String notUseInspectItem(ModelMap paramMap) throws Exception;
	
	public String deleteInspectItem(ModelMap paramMap) throws Exception;
	
	HashMap checkInsertInspectItem(Map paramMap) throws Exception;
	
	public void updateInspectItemMappingUseYn(ModelMap paramMap) throws Exception;
	
	public String deleteInspectMappingItem(ModelMap paramMap) throws Exception;
	
	HashMap selectExcelTemplateContent(HashMap paramMap) throws Exception;
	
	public List searchInspectItemComboList(HashMap paramMap) throws Exception;
	
	List selectInspectHeadDetailList(Map paramMap) throws Exception;
	Integer selectInspectHeadDetailListCount(Map paramMap) throws Exception;
	
	List searchInspectItemResultComboList(HashMap paramMap) throws Exception;
	
	List searchResultDetailBackDataList(HashMap paramMap) throws Exception;
	
	Integer searchResultDetailBackDataListCount(Map paramMap) throws Exception;
	
	public boolean deleteInspectResultHead(HashMap paramMap) throws Exception;
	
	public boolean deleteInspectResultDetail(HashMap paramMap) throws Exception;
	
	public boolean insertInspectItemMappingConf(HashMap paramMap) throws Exception;
	
	public List selectInspectItemCodeComboList(HashMap paramMap) throws Exception;
	
	public boolean updateUseYnInspectItemMappingConf(HashMap paramMap) throws Exception;
}
