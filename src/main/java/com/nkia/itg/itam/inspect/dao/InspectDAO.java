package com.nkia.itg.itam.inspect.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/* *ITAM 정기점검* */

@Repository("inspectDAO")
public class InspectDAO extends EgovComAbstractDAO{
	public Map selectInspectCountLimit(Map paramMap) throws Exception {
		return (Map)selectByPk("InspectDAO.selectInspectCountLimit", paramMap);
	}
	public int updateInspectCountLimit(Map paramMap) throws Exception {
		return this.update("InspectDAO.updateInspectCountLimit", paramMap);
	}
	
	public Map selectInspectItem(Map paramMap) throws Exception {
		return (Map)this.selectByPk("InspectDAO.selectInspectItem", paramMap);
	}
	public List selectInspectItemList(Map paramMap) throws Exception {
		return this.list("InspectDAO.selectInspectItemList", paramMap);
	}
	public Integer selectInspectItemCount(Map paramMap) throws Exception {
		return (int)this.selectByPk("InspectDAO.selectInspectItemCount", paramMap);
	}
	public String insertInspectItem(Map paramMap) throws Exception {
		return (String)this.insert("InspectDAO.insertInspectItem", paramMap);
	}
	public int updateInspectItem(Map paramMap) throws Exception {
		return this.update("InspectDAO.updateInspectItem", paramMap);
	}
	
	public String insertInspectItemMapping(Map paramMap) throws Exception {
		return (String)this.insert("InspectDAO.insertInspectItemMapping", paramMap);
	}
	public Map selectInspectItemMappingItem(Map paramMap) throws Exception {
		return (Map)this.selectByPk("InspectDAO.selectInspectItemMappingItem", paramMap);
	}
	public List selectInspectItemMappingList(Map paramMap) throws Exception {
		return this.list("InspectDAO.selectInspectItemMappingList", paramMap);
	}
	public Integer selectInspectItemMappingCount(Map paramMap) throws Exception {
		return (int)this.selectByPk("InspectDAO.selectInspectItemMappingCount", paramMap);
	}
	public List selectInspectFullJoinedMappingItem(Map paramMap) throws Exception {
		return this.list("InspectDAO.selectInspectFullJoinedMappingItem", paramMap);
	}
	public Integer selectInspectFullJoinedMappingCount(Map paramMap) throws Exception {
		return (int)this.selectByPk("InspectDAO.selectInspectFullJoinedMappingCount", paramMap);
	}
	
	public String insertInspectResultHead(Map paramMap) throws Exception {
		return (String)this.insert("InspectDAO.insertInspectResultHead", paramMap);
	}
	public Map selectInspectResultHeadItem(Map paramMap) throws Exception {
		return (Map)this.selectByPk("InspectDAO.selectInspectResultHeadItem", paramMap);
	}
	public List selectInspectResultHeadList(Map paramMap) throws Exception {
		return this.list("InspectDAO.selectInspectResultHeadList", paramMap);
	}
	public Integer selectInspectResultHeadCount(Map paramMap) throws Exception {
		return (int)this.selectByPk("InspectDAO.selectInspectResultHeadCount", paramMap);
	}
	public int confirmInspectResultHead(Map paramMap) throws Exception {
		return this.update("InspectDAO.confirmInspectResultHead", paramMap);
	}
	public int deconfirmInspectResultHead(Map paramMap) throws Exception {
		return this.update("InspectDAO.deconfirmInspectResultHead", paramMap);
	}
	
	public String insertInspectResultDetail(Map paramMap) throws Exception {
		return (String)this.insert("InspectDAO.insertInspectResultDetail", paramMap);
	}
	public Map selectInspectResultDetailItem(Map paramMap) throws Exception {
		return (Map)this.selectByPk("InspectDAO.selectInspectResultDetailItem", paramMap);
	}
	public List selectInspectResultDetailList(Map paramMap) throws Exception {
		return this.list("InspectDAO.selectInspectResultDetailList", paramMap);
	}
	public Integer selectInspectResultDetailCount(Map paramMap) throws Exception {
		return (int)this.selectByPk("InspectDAO.selectInspectResultDetailCount", paramMap);
	}
	
	public List inspectResultDetailChartConf(Map paramMap) throws Exception {
		return this.list("InspectDAO.inspectResultDetailChartConf", paramMap);
	}
	public List inspectResultDetailChart(Map paramMap) throws Exception {
		return this.list("InspectDAO.inspectResultDetailChart", paramMap);
	}
	public List searchAssetList(ModelMap paramMap) throws Exception{
		return list("InspectDAO.searchAssetList", paramMap);
	}

	public int searchAssetListCount(ModelMap paramMap)throws Exception {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("InspectDAO.searchAssetListCount", paramMap);
	}
	
	public List searchCheckNotUseInspect(HashMap paramMap) throws Exception {
		return list("InspectDAO.searchCheckNotUseInspect", paramMap);
	}
	
	public void updateNotUseInspectItem(HashMap paramMap) throws Exception {
		update("InspectDAO.updateNotUseInspectItem", paramMap); 
	}
	
	public void updateInspectItemName(HashMap paramMap) throws Exception {
		update("InspectDAO.updateInspectItemName", paramMap); 
	}
	
	public void deleteInspectItem(HashMap paramMap) throws Exception {
		update("InspectDAO.deleteInspectItem", paramMap); 
	}
	
	public void updateInspectItemMappingUseYn(HashMap paramMap) throws Exception {
		update("InspectDAO.updateInspectItemMappingUseYn", paramMap); 
	}
	
	public List searchCheckDeleteInspectMappingItem(HashMap paramMap) throws Exception {
		return list("InspectDAO.searchCheckDeleteInspectMappingItem", paramMap);
	}
	
	public void deleteInspectMappingItem(HashMap paramMap) throws Exception {
		delete("InspectDAO.deleteInspectMappingItem", paramMap);
	}
	
	public List selectExcelTargetConfList (HashMap paramMap) throws Exception {
		return list("InspectDAO.selectExcelTargetConfList", paramMap);
	}
	
	public List selectExcelTargetInspectItem (HashMap paramMap) throws Exception {
		return list("InspectDAO.selectExcelTargetInspectItem", paramMap);
	}
	
	public List searchInspectItemComboList (HashMap paramMap) throws Exception {
		return list("InspectDAO.searchInspectItemComboList", paramMap);
	}
	
	// 셀잠금(셀기준)
	public List selectExcelTargetInspectItemByMapping(HashMap paramMap) {
		return list("InspectDAO.selectExcelTargetInspectItemByMapping", paramMap);
	}
	
	public List selectInspectHeadDetailList(Map paramMap) throws Exception {
		return this.list("InspectDAO.selectInspectHeadDetailList", paramMap);
	}
	public Integer selectInspectHeadDetailListCount(Map paramMap) throws Exception {
		return (int)this.selectByPk("InspectDAO.selectInspectHeadDetailListCount", paramMap);
	}
	
	public List searchInspectItemResultComboList(HashMap paramMap){
		return list("InspectDAO.searchInspectItemResultComboList", paramMap);
	}
	
	public List searchResultDetailBackDataList(HashMap paramMap){
		return list("InspectDAO.searchResultDetailBackDataList", paramMap);
	}
	
	public Integer searchResultDetailBackDataListCount(Map paramMap) throws Exception {
		return (int)this.selectByPk("InspectDAO.searchResultDetailBackDataListCount", paramMap);
	}
	
	public void deleteInspectResultHead(HashMap paramMap) throws Exception {
		delete("InspectDAO.deleteInspectResultHead", paramMap);
	}
	
	public void deleteInspectResultDetail(HashMap paramMap) throws Exception {
		delete("InspectDAO.deleteInspectResultDetail", paramMap);
	}
	
	public void insertInspectItemMappingConf(HashMap paramMap) throws Exception {
		update("InspectDAO.insertInspectItemMappingConf", paramMap); 
	}
	
	public List selectInspectItemCodeComboList(HashMap paramMap) throws Exception {
		return list("InspectDAO.selectInspectItemCodeComboList", paramMap);
	}
}