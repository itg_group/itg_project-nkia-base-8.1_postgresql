package com.nkia.itg.itam.inspect.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.dao.ItgBaseDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.inspect.dao.InspectDAO;
import com.nkia.itg.itam.inspect.service.InspectService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/* *ITAM 정기점검* */

@SuppressWarnings({ "rawtypes", "unchecked" })
@Service("InspectService")
public class InspectServiceImpl implements InspectService {
	private static final Logger logger = LoggerFactory.getLogger(InspectServiceImpl.class);
	
	@Autowired
	private InspectDAO inspectDAO;
	
	@Resource(name = "itgBaseDAO")
	public ItgBaseDAO itgBaseDAO;
	
	@Override
	public List inspectResultProgressChart(Map paramMap) throws Exception {
		List resultList = null;
		
		try {
			resultList = inspectDAO.inspectResultDetailChartConf(paramMap);

		} catch(Exception e) {
			throw e;
		}
		
		return resultList;
	}
	
	@Override
	public List inspectResultProgressChartExcel(Map paramMap) throws Exception {
		return inspectDAO.inspectResultDetailChart(paramMap);
	}
	
	public Map selectInspectCountLimit(Map paramMap) throws Exception {
		return inspectDAO.selectInspectCountLimit(paramMap);
	}
	public boolean updateInspectCountLimit(Map paramMap) throws Exception {
		int result = inspectDAO.updateInspectCountLimit(paramMap);
		return (result > 0);
	}
	@Override
	public Map selectInspectItem(Map paramMap) throws Exception {
		return inspectDAO.selectInspectItem(paramMap);
	}
	@Override
	public List selectInspectItemList(Map paramMap) throws Exception {
		return inspectDAO.selectInspectItemList(paramMap);
	}
	@Override
	public Integer selectInspectItemCount(Map paramMap) throws Exception {
		return inspectDAO.selectInspectItemCount(paramMap);
	}
	@Override
	public boolean saveInspectItem(Map paramMap) throws Exception {
		Boolean resultFlag = false;
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	
    	try {
    		/*
    		List resultList = (List)paramMap.get("resultList");
    		
    		for(int count=0 ; count<resultList.size() ; count++) {
    			Map resultItem = (Map) resultList.get(count);
    			
    			if(inspectDAO.selectInspectItem(resultItem) != null) {
    				inspectDAO.updateInspectItem(resultItem);
    			} else {
    				inspectDAO.insertInspectItem(resultItem);
    			}
    		}*/ 		
    		
    		if("".equals(StringUtil.parseString(paramMap.get("INSPECT_ID")))){
    			inspectDAO.insertInspectItem(paramMap);
    		} else {
    			//점검항목명 수정하면 같은 이름 점검항목명 전부 다 수정되게 처리
        		inspectDAO.updateInspectItemName((HashMap) paramMap);
        		
    			inspectDAO.updateInspectItem(paramMap);
    		}  
			
    		resultFlag = true;
    	} catch(Exception e) {
    		resultFlag = false;
    	}
    	
    	return resultFlag;
	}
	
	@Override
	public boolean insertInspectItemMapping(Map paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("INS_USER_ID", userVO.getUser_id());
    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
    	}
    	
    	try {
    		List resultList = (List) paramMap.get("resultList");
    		
    		//2. 등록
    		for(int count=0 ; count<resultList.size() ; count++) {
    			Map resultItem = (Map)resultList.get(count);
    			resultItem.put("INS_USER_ID", paramMap.get("INS_USER_ID"));
    			resultItem.put("UPD_USER_ID", paramMap.get("UPD_USER_ID"));
    			
    			inspectDAO.insertInspectItemMapping(resultItem);
    		}
    	} catch(Exception e) {
    		return false;
    	}
		
		return true;
	}
	@Override
	public List selectInspectItemMappingList(Map paramMap) throws Exception {
		return inspectDAO.selectInspectItemMappingList(paramMap);
	}
	@Override
	public Integer selectInspectItemMappingCount(Map paramMap) throws Exception {
		return inspectDAO.selectInspectItemMappingCount(paramMap);
	}
	@Override
	public List selectInspectFullJoinedMappingItem(Map paramMap) throws Exception {
		return inspectDAO.selectInspectFullJoinedMappingItem(paramMap);
	};
	@Override 
	public Integer selectInspectFullJoinedMappingCount(Map paramMap) throws Exception {
		return inspectDAO.selectInspectFullJoinedMappingCount(paramMap);
	}
	
	@Override
	public boolean insertInspectResultHead(Map paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("INS_USER_ID", userVO.getUser_id());
    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
    	}
    	
    	try {
    		List headList = (List) paramMap.get("headList");
    		
    		for(int count=0 ; count<headList.size() ; count++) {
    			Map resultItem = (Map)headList.get(count);
    			resultItem.put("INS_USER_ID", paramMap.get("INS_USER_ID"));
    			resultItem.put("UPD_USER_ID", paramMap.get("UPD_USER_ID"));
    			inspectDAO.insertInspectResultHead(resultItem);
    		}
    	} catch(Exception e) {
    		throw e;
    	}
		
		return true;
	}
	@Override
	public List selectInspectResultHeadList(Map paramMap) throws Exception {
		return inspectDAO.selectInspectResultHeadList(paramMap);
	}
	@Override
	public Integer selectInspectResultHeadCount(Map paramMap) throws Exception {
		return inspectDAO.selectInspectResultHeadCount(paramMap);
	}
	@Override
	public boolean confirmInspectResultHead(Map paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		String login_user_id = "";
		String approval = StringUtil.parseString(paramMap.get("approval"));
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		login_user_id = userVO.getUser_id();
    	}
    	
    	List headList = (List) paramMap.get("headList");
    	for(int count=0 ; count<headList.size() ; count++) {
    		Map headItem = (Map) headList.get(count);
    		headItem.put("login_user_id", login_user_id);
    		if ( approval.equals("Y") == true ){
        		inspectDAO.confirmInspectResultHead(headItem);
    		}
    		else {
    			inspectDAO.deconfirmInspectResultHead(headItem);
    		}
    	}
    	
    	return true;
	} // confirmInspectResultHead
	
	
	// 20201117 임현대 : 사용안함. confirmInspectResultHead 으로 통합
	@Override
	public boolean deconfirmInspectResultHead(Map paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		String login_user_id = "";
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		login_user_id = userVO.getUser_id();
    	}
    	
    	List headList = (List) paramMap.get("headList");
    	for(int count=0 ; count<headList.size() ; count++) {
    		Map headItem = (Map) headList.get(count);
    		headItem.put("login_user_id", login_user_id);
    		inspectDAO.deconfirmInspectResultHead(headItem);
    	}
    	
    	return true;
	}
	
	@Override
	public boolean insertInspectResultDetail(Map paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("INS_USER_ID", userVO.getUser_id());
    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
    	}
    	
    	try {
    		List resultList = (List) paramMap.get("detailList");	//디테일 그리드
    		int inspectCount = 0;
    		int inspectNgCount = 0;
    		
    		for(int count=0 ; count<resultList.size() ; count++) {
    			Map resultItem = (Map)resultList.get(count);
    			resultItem.put("INS_USER_ID", paramMap.get("INS_USER_ID"));
    			resultItem.put("UPD_USER_ID", paramMap.get("UPD_USER_ID"));
    			inspectDAO.insertInspectResultDetail(resultItem);
    			
    			inspectCount++;
    			if("2".equals(String.valueOf(resultItem.get("INSPECT_VALUE")))) inspectNgCount++;
    		}
    		
    		Map headItem = (Map) paramMap.get("headItem");
    		Map upperHeadItem = new HashMap();
    		Iterator<String> headKeys = headItem.keySet().iterator();
    		while(headKeys.hasNext()) {
    			String headKey = headKeys.next();
    			upperHeadItem.put(headKey.toUpperCase(), headItem.get(headKey));
    			upperHeadItem.put("INS_USER_ID", paramMap.get("INS_USER_ID"));
    			upperHeadItem.put("UPD_USER_ID", paramMap.get("UPD_USER_ID"));
    			
    		}
//    		upperHeadItem.put("INSPECT_MEMO", paramMap.get("inspect_memo"));
    		upperHeadItem.put("INSPECT_COUNT", inspectCount);
    		upperHeadItem.put("INSPECT_NG_COUNT", inspectNgCount);
    		upperHeadItem.put("INSPECT_USER_ID", paramMap.get("INS_USER_ID"));
    		
    		inspectDAO.insertInspectResultHead(upperHeadItem);	//헤드정보
    	} catch(Exception e) {
    		throw e;
    	}
		
		return true;
	}
	@Override
	public boolean insertInspectResultDetailByExcel(Map paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("INS_USER_ID", userVO.getUser_id());
    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
    	}
    	
    	try {
    		List resultList = (List) paramMap.get("resultList");
    		String inspect_day = StringUtil.parseString(paramMap.get("inspect_day"));
    		String class_type = StringUtil.parseString(paramMap.get("class_type"));
    		List inspect_id_arr = (List) paramMap.get("inspect_id_arr");
    		
    		//inspect_item의 정보가 들어있음. 관리임계값, 단위, 점검항목 유형
    		List<HashMap> inspectItemList = inspectDAO.selectExcelTargetInspectItem((HashMap) paramMap);	
    		HashMap inspectItemMap = new HashMap();
    		
    		if(inspectItemList.size() > 0){
    			for(int i=0; i<inspectItemList.size(); i++){
    				inspectItemMap.put(StringUtil.parseString(inspectItemList.get(i).get("INSPECT_ID")), (HashMap)inspectItemList.get(i));
    			}
    		}
    		
    		//점검값 codeList
    		HashMap codeMap = new HashMap();
    		HashMap inspectValueMap = new HashMap();
			codeMap.put("code_grp_id", "INSPECT_VALUE");	//점검값
			List<HashMap> codeList = itgBaseDAO.searchCodeDataList(codeMap);
			
			if(codeList.size() > 0){
				for(int c=0; c<codeList.size(); c++){
					inspectValueMap.put(codeList.get(c).get("CODE_TEXT"), (HashMap)codeList.get(c));
				}
			}
			
    		//INSPECT_RESULT_HEAD
    		if(resultList.size() > 0){
    			//정기점검 점검결과 Head 등록. 구성ID의 수만큼
    			for(int i=0; i<resultList.size(); i++){
    				int inspect_ng_count = 0;
    				boolean headUpdateFlag = false; //아무것도 입력하지 않았으면 INSPECT_RESULT_HEAD 테이블을 인서트/업데이트 할 수 없음.
    				int inspect_count = 0;
    				
    				HashMap tmpRsltMap = (HashMap) resultList.get(i);
    				
    				HashMap map = new HashMap();
    				map.put("INSPECT_DAY", inspect_day);	//점검일자
    				map.put("CLASS_TYPE", class_type);	//구성TYPE
    				map.put("CONF_ID", StringUtil.parseString(tmpRsltMap.get("CONF_ID")));   				
    				map.put("INSPECT_MEMO", StringUtil.parseString(tmpRsltMap.get("INSPECT_MEMO")));	//점검총평
    				map.put("INSPECT_USER_ID", StringUtil.parseString(paramMap.get("INS_USER_ID")));	//점검자
    				map.put("INS_USER_ID", StringUtil.parseString(paramMap.get("INS_USER_ID")));	//등록자  				
    				
    				
    				//점검결과 상세등록
    				if(inspect_id_arr.size() > 0){
    					for(int j=0; j<inspect_id_arr.size(); j++){
    						String inspect_result = "";	//점검결과, 엑셀에서 입력한 값
    						String inspect_id = StringUtil.parseString(inspect_id_arr.get(j));
    						
    						if ( tmpRsltMap.containsKey(inspect_id) && tmpRsltMap.get(inspect_id) != null ){
    							if(!"".equals(StringUtil.parseString(tmpRsltMap.get(inspect_id)))){
    								inspect_result = StringUtil.parseString(tmpRsltMap.get(inspect_id));
    								headUpdateFlag = true;	//하나의 설비에 하나의 점검결과값이라도 입력되면 INSPECT_RESULT_HEAD 업데이트
									
									inspect_count ++; //점검결과값이 몇개나 입력됐는지 체크하기 위해 넣은 변수. 값이 있으면 1씩 올려준다.	
    							}
    						}
    						
//    						Set set = tmpRsltMap.keySet();
//    						Iterator iterator = set.iterator();
//    						while(iterator.hasNext()){
//    							String key = (String) iterator.next();
//    							String value = StringUtil.parseString(tmpRsltMap.get(key));
//    							
//    							
//    							
//    							if(key.equals(inspect_id)){
//    								if(!"".equals(StringUtil.parseString(tmpRsltMap.get(key)))){
//    									inspect_result = value;
//    									headUpdateFlag = true;	//하나의 설비에 하나의 점검결과값이라도 입력되면 INSPECT_RESULT_HEAD 업데이트
//    									
//    									inspect_count ++; //점검결과값이 몇개나 입력됐는지 체크하기 위해 넣은 변수. 값이 있으면 1씩 올려준다.
//    								}    								
//    							}
//    						}
    						
    						if(inspectItemMap.containsKey(inspect_id) && inspectItemMap.get(inspect_id) != null){
    							HashMap inspectItemTempMap = (HashMap) inspectItemMap.get(inspect_id);
    							String inspect_type = StringUtil.parseString(inspectItemTempMap.get("INSPECT_TYPE"));
    						
    							// 오류의 기준
								// 점검항목 종류(inspect_type) 가 '세부항목'일 때 : 점검결과값이 최소값(MIN_CRITICAL)보다 작거나 최대값(MAX_CRITICAL)보다 클 때 오류로 봄.
								// 점검항목 종류(inspect_type) 가 '단순항목'일 때 : 점검결과값이 '오류'면 오류로 봄.
    							
    							// INSPECT_VALUE[점검값] : 정상: 1, 오류: 2, 데이터부적합 : 3
    							if("02".equals(inspect_type)){
    								double min_critical = Double.parseDouble(String.valueOf(inspectItemTempMap.get("MIN_CRITICAL")));
									double max_critical = Double.parseDouble(String.valueOf(inspectItemTempMap.get("MAX_CRITICAL")));    							
									
    								if(!"".equals(inspect_result)){
										if(!isDataFormatCheck(inspect_result)){
											map.put("INSPECT_VALUE", 3);	//데이터 부적합
        								} else {
//        									int tmp_inspect_result = Integer.parseInt(inspect_result);	//long로 수
        									double tmp_inspect_result = Double.parseDouble(String.valueOf(inspect_result));
        									if(!"".equals(StringUtil.parseString(inspectItemTempMap.get("MIN_CRITICAL"))) && !"".equals(inspectItemTempMap.get("MAX_CRITICAL")) ){
        										if((tmp_inspect_result < min_critical) || (tmp_inspect_result > max_critical)){
            										inspect_ng_count ++;
            										map.put("INSPECT_VALUE", 2);	//오류
            									}else{
            										map.put("INSPECT_VALUE", 1);	//정상
            									}
        									}
        									         									
        								}
									}          								
    								
								} else {
									if(!"".equals(StringUtil.parseString(inspect_result))){

										if(inspectValueMap.containsKey(inspect_result) && inspectValueMap.get(inspect_result) != null ){
											HashMap valueCodeMap = (HashMap) inspectValueMap.get(inspect_result);
											
											if(inspect_result.equals(valueCodeMap.get("CODE_TEXT"))){
												map.put("INSPECT_RESULT", StringUtil.parseString(valueCodeMap.get("CODE_ID")));
												map.put("INSPECT_VALUE", StringUtil.parseString(valueCodeMap.get("CODE_ID")));
												
												if("2".equals(StringUtil.parseString(valueCodeMap.get("CODE_ID")))){
													inspect_ng_count ++;
												}
											}
										}else{
											map.put("INSPECT_RESULT", "3");
											map.put("INSPECT_VALUE", "3");
										}   										
									}
									
								} 
								
    							String strMin = StringUtil.parseString(inspectItemTempMap.get("MIN_CRITICAL"));
    							String strMax = StringUtil.parseString(inspectItemTempMap.get("MAX_CRITICAL"));
    							Double dMin = null, dMax = null;
    							if ( strMin.equals("") == false ){ dMin = Double.parseDouble(strMin);}
    							if ( strMax.equals("") == false ){ dMax = Double.parseDouble(strMax);}
    							
								map.put("INSPECT_TYPE", inspect_type);
								map.put("INSPECT_UNIT", StringUtil.parseString(inspectItemTempMap.get("INSPECT_UNIT")));
								map.put("INSPECT_BASE_YN", StringUtil.parseString(inspectItemTempMap.get("INSPECT_BASE_YN")));
								map.put("INSPECT_CRITICAL_MIN", dMin);
								map.put("INSPECT_CRITICAL_MAX", dMax);
    						
    						}
    						
    						//detailInsert
    						map.put("INSPECT_ID", inspect_id);
    						
    						//점검항목이 단순점검인 데이터는 INSPECT_VALUE 값을 정의할 때 같이 넣어줌.
    						if("02".equals(map.get("INSPECT_TYPE"))){
    							map.put("INSPECT_RESULT", inspect_result);    							
    						}
    						if(!"".equals(inspect_result)){
    							inspectDAO.insertInspectResultDetail(map);    
    							logger.info(StringUtil.parseString(tmpRsltMap.get("CONF_ID")) + ", " + inspect_id + ":" + inspect_result);    							
    						}
    					}
    				}    				
    				
    				//headInsert
    				map.put("INSPECT_NG_COUNT", inspect_ng_count);	//오류항목수, 세부항목 : 임계치 벗어나면 오류, 단순항목 : 오류라고 입력함 
    				map.put("INSPECT_COUNT", inspect_count); 
    				if(headUpdateFlag) {
    					inspectDAO.insertInspectResultHead(map);
    					logger.info("insert head" + StringUtil.parseString(tmpRsltMap.get("CONF_ID")) + ", 오류항목 : " + inspect_ng_count);
    				}
    				
    				
    			}
    		}
    	} catch(Exception e) {
    		throw e;
    	}
		
		return true;
	}
	@Override
	public List selectInspectResultDetailList(Map paramMap) throws Exception {
		return inspectDAO.selectInspectResultDetailList(paramMap);
	}
	@Override
	public Integer selectInspectResultDetailCount(Map paramMap) throws Exception {
		return inspectDAO.selectInspectResultDetailCount(paramMap);
	}	

	@Override
	public HashMap selectExcelTemplateContent(HashMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		HashMap resultMap = new HashMap();
		List targetConfList = inspectDAO.selectExcelTargetConfList(paramMap);	//점검결과 목록에 조회되는 IT설비들, 점검그룹 필수
		List returnArr = new ArrayList<HashMap>();
		//조회해온 IT의 설비에 선택한 점검그룹의 점검항목 정보를 나열한다. 
		//상세점검1 점검결과값 | 상세점검2 점검결과값 		
		List<LinkedHashMap> inspectItemList = inspectDAO.selectExcelTargetInspectItem(paramMap);	//class_type, inspectItem(선택)
		
		if(inspectItemList.size() > 0){
			
			for(int i=0; i<targetConfList.size(); i++){
				HashMap tmpConfMap = (HashMap) targetConfList.get(i);
				for(int j=0; j<inspectItemList.size(); j++){
					HashMap tmpInsMap = inspectItemList.get(j);
					tmpConfMap.put(StringUtil.parseString(tmpInsMap.get("INSPECT_ID")), "");
				}
				
				returnArr.add(tmpConfMap);
			}
		}
		resultMap.put("inspectItemList", inspectItemList);	//입력할 내용
		resultMap.put("excelList", returnArr);	//inspectID 들어가있음
		
		// 셀잠금(셀기준)
		List listInspectMap = inspectDAO.selectExcelTargetInspectItemByMapping(paramMap);
		resultMap.put("cellDisplayTypes", listInspectMap);
		
		return resultMap;
	}
	
	@Override
	public List selectAllInspectResultDetailList(Map paramMap) throws Exception {
		List headList = selectInspectResultHeadList(paramMap);
		
		for(int headCount=0 ; headCount<headList.size() ; headCount++) {
			Map headItem = (Map) headList.get(headCount);
			headItem.put("class_type", headItem.get("CLASS_TYPE"));
			headItem.put("conf_id", headItem.get("CONF_ID"));
			headItem.put("inspect_day", paramMap.get("inspect_day"));
			headItem.put("inspect_id", paramMap.get("inspect_id"));
			headItem.put("use_yn", "Y");
			headItem.put("first_num", 1);
			headItem.put("last_num", 1000000);
			
			List detailList = selectInspectResultDetailList(headItem);
			headItem.put("detailList", detailList);
		}
		
		return headList;
	}
	public List searchAssetList(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.searchAssetList(paramMap);
	}

	public int searchAssetListCount(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.searchAssetListCount(paramMap);
	}

	@Override
	public String notUseInspectItem(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		String returnString = "";
		if(paramMap.get("inspect_id_list") != null){
			//20201201 매핑 개수는 화면에서 체크하도록 처리
//			List checkNotUseList = inspectDAO.searchCheckNotUseInspect(paramMap);
//
//			if(checkNotUseList.size() > 0){
//				returnString = "IT설비와 매핑된 점검항목이 있습니다. IT설비와 매핑된 점검항목은 미사용 처리가 불가합니다.";
//			}else{
//				inspectDAO.updateNotUseInspectItem(paramMap);
//				returnString = "사용여부가 변경되었습니다.";
//			}
			
			inspectDAO.updateNotUseInspectItem(paramMap);
			returnString = "사용여부가 변경되었습니다.";
		}
		return returnString;
	}

	@Override
	public HashMap checkInsertInspectItem(Map paramMap) throws Exception {
		HashMap returnMap = new HashMap();
		
		String message = new String();
		String updateFlag = "true";
		
		if(!"".equals(StringUtil.parseString(paramMap.get("simpleCritical_limit"))) && "01".equals(StringUtil.parseString(paramMap.get("INSPECT_TYPE")))){
			int simpleCritical_limit = Integer.parseInt(StringUtil.parseString(paramMap.get("simpleCritical_limit")));
			
			HashMap tmpMap = new HashMap();
			
			if(!"".equals(StringUtil.parseString(paramMap.get("CLASS_TYPE")))){
				tmpMap.put("class_type", StringUtil.parseString(paramMap.get("CLASS_TYPE")));
			}
			tmpMap.put("inspect_type", "01");
			List<HashMap> simpleChecklist = inspectDAO.selectInspectItemList(tmpMap); //단순 점검 항목 최대개수 테스트
			
			if(simpleChecklist.size() > simpleCritical_limit){
				updateFlag = "false";
				message = "단순점검항목이 이미 최대 갯수만큼 등록되어 있습니다.\n(단순점검항목 최대개수 :" + simpleCritical_limit + ")";
			}
		}
		
		if(!"".equals(StringUtil.parseString(paramMap.get("INSPECT_ID")))){
			// 수정 건인데, 사용여부가 N으로 들어온 경우에는 Mapping 관계가 있는지 확인해봄
			if(!"Y".equals(StringUtil.parseString(paramMap.get("USE_YN")))){
				List checkNotUseList = inspectDAO.searchCheckNotUseInspect((HashMap) paramMap);
							
				if(checkNotUseList.size() > 0){
					updateFlag = "false";					
					message = "IT설비와 매핑된 점검항목이 있습니다. IT설비와 매핑된 점검항목은 미사용 처리가 불가합니다.";
				}
			}
		} 
		returnMap.put("updateFlag", updateFlag);
		returnMap.put("message", message);
		
		return returnMap;
	}

	@Override
	public void updateInspectItemMappingUseYn(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		inspectDAO.updateInspectItemMappingUseYn(paramMap);
	}

	@Override
	public String deleteInspectMappingItem(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		String resultMessage = new String();
		List conf_id_list = new ArrayList<String>();
		List seq_list = new ArrayList<String>();
		
		/*
		if(paramMap.get("conf_id_list") != null){
			conf_id_list = (List) paramMap.get("conf_id_list");
		}
		*/
		if(paramMap.get("seq_list") != null){
			seq_list = (List) paramMap.get("seq_list");
		}
		
		//if(conf_id_list.size() > 0){
		if(seq_list.size() > 0){
			//점검결과가 등록되어 있는지 체크. 점검결과가 등록되었으면 매핑정보를 삭제할 수 없다.
			List checkDelList = inspectDAO.searchCheckDeleteInspectMappingItem(paramMap);
			
			// 점검결과여부는 nvf 파일에서 처리함
			inspectDAO.deleteInspectMappingItem(paramMap);	//삭제
			resultMessage = "선택하신 IT설비의 매핑 정보가 삭제되었습니다.";
			
			/*
			if(checkDelList.size() > 0){
				resultMessage = "점검결과가 등록된 매핑 정보는 삭제할 수 없습니다.";
			}else{
				inspectDAO.deleteInspectMappingItem(paramMap);	//삭제
				resultMessage = "선택하신 IT설비의 매핑 정보가 삭제되었습니다.";
			}
			*/
		}else{
			resultMessage = "선택된 IT설비 정보가 없습니다.";
		}		
		
		return resultMessage;
	}

	@Override
	public List searchInspectItemComboList(HashMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.searchInspectItemComboList(paramMap);
	}

	private boolean isDataFormatCheck(String value) {
		boolean result = true;
		
		try {
			Double.parseDouble(value);
		} catch (NumberFormatException e) {
			result = false;
		}
		
		return result;
	}

	@Override
	public List selectInspectHeadDetailList(Map paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.selectInspectHeadDetailList(paramMap);
	}

	@Override
	public Integer selectInspectHeadDetailListCount(Map paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.selectInspectHeadDetailListCount(paramMap);
	}

	@Override
	public List searchInspectItemResultComboList(HashMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.searchInspectItemResultComboList(paramMap);
	}

	@Override
	public List searchResultDetailBackDataList(HashMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.searchResultDetailBackDataList(paramMap);
	}

	@Override
	public Integer searchResultDetailBackDataListCount(Map paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.searchResultDetailBackDataListCount(paramMap);
	}

	@Override
	public boolean deleteInspectResultHead(HashMap paramMap) throws Exception {
		Boolean returnFlag = false;
		if(paramMap.get("selRow") != null){
			List<HashMap> selRow = (List<HashMap>) paramMap.get("selRow");
			
			String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
			String class_type = StringUtil.parseString(paramMap.get("class_type"));
			
			List inspect_day_list = new ArrayList();	//점검일자 배열
			if(selRow.size() > 0){
				for(int i=0; i<selRow.size(); i++){
					HashMap tmpMap = selRow.get(i);
					inspect_day_list.add(StringUtil.parseString(tmpMap.get("INSPECT_DAY")));	//선택한 점검일자를 배열에 넣음				
				}
			}
			
			if(inspect_day_list.size() > 0){
				HashMap tmpDelMap = new HashMap();
				tmpDelMap.put("conf_id", conf_id);
				tmpDelMap.put("class_type", class_type);
				tmpDelMap.put("inspect_day_list", inspect_day_list);
				
				inspectDAO.deleteInspectResultDetail(tmpDelMap);	//정기점검 점검결과 상세 삭제
				inspectDAO.deleteInspectResultHead(tmpDelMap);		//정기점검 점검결과 HEAD 삭제
				
				returnFlag = true;
			}
		}
		
		
		return returnFlag;
	}

	@Override
	public boolean deleteInspectResultDetail(HashMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		Boolean returnFlag = true;
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		String class_type = StringUtil.parseString(paramMap.get("class_type"));
		String inspect_day = StringUtil.parseString(paramMap.get("inspect_day"));
		
		
		HashMap tmpDelMap = new HashMap();
		tmpDelMap.put("class_type", class_type);
		tmpDelMap.put("conf_id", conf_id);
		tmpDelMap.put("inspect_day", inspect_day);			
		
		inspectDAO.deleteInspectResultDetail(tmpDelMap);	//정기점검 점검결과 상세 삭제				
		
		return returnFlag;
	}

	@Override
	public String deleteInspectItem(ModelMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		String returnString = "";
		if(paramMap.get("inspect_id") != null){
			inspectDAO.deleteInspectItem(paramMap);
			returnString = "점검항목이 삭제 되었습니다.";
		}
		return returnString;
	}

	@Override
	public boolean insertInspectItemMappingConf(HashMap paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("INS_USER_ID", userVO.getUser_id());
    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
    	}
		boolean returnFlag = false;
		try {
			if(paramMap.get("resultList") != null && paramMap.get("confList") != null){
				List<HashMap> resultList = (List<HashMap>) paramMap.get("resultList");
				List confList = (List) paramMap.get("confList");
				
				if(resultList.size() > 0){
					
					paramMap.put("conf_id_list", confList);					
					inspectDAO.deleteInspectMappingItem(paramMap);
					
					for(int i=0; i<resultList.size(); i++){
						HashMap resultItem = (HashMap)resultList.get(i);
						resultItem.put("INS_USER_ID", paramMap.get("INS_USER_ID"));
						resultItem.put("UPD_USER_ID", paramMap.get("UPD_USER_ID"));

						inspectDAO.insertInspectItemMappingConf(resultItem);
					}
				
					returnFlag = true;
				}
			}
		} catch(Exception e) {
			returnFlag = false;
		}
		
		return returnFlag;
	}

	@Override
	public List selectInspectItemCodeComboList(HashMap paramMap) throws Exception {
		// TODO Auto-generated method stub
		return inspectDAO.selectInspectItemCodeComboList(paramMap);
	}

	@Override
	public boolean updateUseYnInspectItemMappingConf(HashMap paramMap) throws Exception {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("INS_USER_ID", userVO.getUser_id());
    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
    	}
		boolean returnFlag = false;
		try {
			if(paramMap.get("confList") != null){
				List confList = (List) paramMap.get("confList");
				
				if(confList.size() > 0){					
					paramMap.put("conf_id_list", confList);					
					inspectDAO.updateInspectItemMappingUseYn(paramMap);
					
					returnFlag = true;
				}
			}
		} catch(Exception e) {
			returnFlag = false;
		}
		
		return returnFlag;
	}
}
