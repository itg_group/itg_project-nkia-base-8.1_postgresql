package com.nkia.itg.itam.inspect.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.excel.ExcelMakerByPoi;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.inspect.service.InspectService;
import com.nkia.itg.itam.statistics.service.AssetStatisticsService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

/* *ITAM 정기점검* */

@SuppressWarnings({ "rawtypes", "unchecked" })
@Controller
public class InspectController {

	@Autowired
	private InspectService inspectService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "assetStatisticsService")
	private AssetStatisticsService assetStatisticsService;
	
	@Resource(name = "excelMakerByPoi")
	private ExcelMakerByPoi excelMakerByPoi;
	
	/*점검결과 관리 Start ===================================================================================================================*/
	
	/**
	 * 점검항목 관리 화면 이동
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectItem.do", method=RequestMethod.GET)
	public String inspectItem(ModelMap paramMap) throws Exception{
		return "/itg/itam/inspect/inspectItem.nvf";
	}
	
	/**
	 * 점검항목 관리 > 단순점검항목, 세부항목 최대개수 조회
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectCountLimit.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectCountLimit(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			Map limitMap = inspectService.selectInspectCountLimit(paramMap);
			
			resultVO.setResultMap(limitMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 관리 > 단순점검항목, 세부항목 최대개수 수정. 
	 * 공통코드에서 관리하므로 현재 사용하지 않는 기능.
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/updateInspectCountLimit.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateInspectCountLimit(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.updateInspectCountLimit(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 관리 > 점검항목 목록 조회
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectItemList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectItemList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = inspectService.selectInspectItemList(paramMap);
			int totalCount = inspectService.selectInspectItemCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 관리 > 점검항목 등록
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/saveInspectItem.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO saveInspectItem(@RequestBody Map paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		Boolean resultBoolean = new Boolean(false);		
		String resultString = new String();
		
		Boolean updateFlag = true;
		try {
			HashMap checkMap = inspectService.checkInsertInspectItem(paramMap);
			
			if(!"".equals(StringUtil.parseString(checkMap.get("updateFlag")))){
    			if(!"false".equals(StringUtil.parseString(checkMap.get("updateFlag")))){
    				updateFlag = true;
    			}else{
    				updateFlag = false;
    			}
    		}
    		if(!"".equals(StringUtil.parseString(checkMap.get("message")))) {
    			resultBoolean = false;
    			resultString =  StringUtil.parseString(checkMap.get("message"));
    		}
    		
			if(updateFlag){
				resultBoolean = inspectService.saveInspectItem(paramMap);				
			}
			
			resultVO.setResultString(resultString);
			resultVO.setResultBoolean(resultBoolean);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 관리 > [미사용 처리]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/notUseInspectItem.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO notUseInspectItem(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			String resultString = inspectService.notUseInspectItem(paramMap);
			
			resultVO.setResultString(resultString);
		} catch(Exception e) {
			resultVO.setResultString("미사용 처리 중 오류가 발생하였습니다. 다시 한번 시도해주십시오.");
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 관리 > [삭제]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/deleteInspectItem.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deleteInspectItem(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			String resultString = inspectService.deleteInspectItem(paramMap);
			
			resultVO.setResultString(resultString);
		} catch(Exception e) {
			resultVO.setResultString("삭제 중 오류가 발생하였습니다. 다시 한번 시도해주십시오.");
		}
		
		return resultVO;
	}
	
	/*점검항목 관리 End================================================================================================================================*/
	
	
	
	
	/*점검항목 매핑 Start================================================================================================================================*/	
	
	/**
	 * 점검항목 매핑 페이지 이동(탭)
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectMappingMain.do", method=RequestMethod.GET)
	public String inspectMappingMain(ModelMap paramMap) throws Exception {
		return "/itg/itam/inspect/inspectMappingTab.nvf";
	}
	
	/**
	 * 점검항목 매핑 페이지 > 탭 안의 내용
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectMapping.do", method=RequestMethod.GET)
	public String inspectMapping(ModelMap paramMap) throws Exception {
		return "/itg/itam/inspect/inspectMapping.nvf";
	}
	
	/**
	 * 점검항목 매핑 > 점검항목 매핑 결과 상세화면
	 * 점검항목 매핑 목록 더블 클릭 시 이동
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectMappingDetail.do", method=RequestMethod.GET)
	public String inspectMappingDetail(HttpServletRequest request, ModelMap paramMap) throws Exception {
		String class_type = StringUtil.parseString(request.getParameter("class_type"));
		paramMap.put("class_type", class_type);
		
		String inspect_id = StringUtil.parseString(request.getParameter("inspect_id"));
		paramMap.put("inspect_id", inspect_id);
		
		return "/itg/itam/inspect/inspectMappingDetail.nvf";
	}
	
	/**
	 * 점검항목 매핑(항목기준) > 점검항목 매핑 목록
	 * 점검항목 매핑(IT설비기준) > [점검항목 매핑] 클릭 > 점검항목 목록 조회
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectMappingList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectMappingList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			List resultList = inspectService.selectInspectItemList(paramMap);
			int totalCount = inspectService.selectInspectItemCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑(항목기준) > IT 설비 매핑 팝업 > IT설비 목록 조회 
	 * [IT설비 매핑] 클릭 > 팝업 > IT설비 목록 
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectItemMappingList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectItemMappingList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}

			List resultList = inspectService.selectInspectItemMappingList(paramMap);
			int totalCount = inspectService.selectInspectItemMappingCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑 > [IT설비 매핑] > 팝업화면의 IT설비 목록 
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/searchInspectAssetList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO searchInspectAssetList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}
			
			List resultList = inspectService.searchAssetList(paramMap);		
			totalCount = inspectService.searchAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑 > IT 설비 매핑 팝업 > 선택된 IT설비
	 * [IT설비 매핑] 클릭 > 팝업 > 선택된 IT설비
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectFullJoinedMappingItem.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectFullJoinedMappingItem(@RequestBody Map paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}

			List resultList = inspectService.selectInspectFullJoinedMappingItem(paramMap);
//			int totalCount = inspectService.selectInspectFullJoinedMappingCount(paramMap);
			
//			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑(항목기준) > 자산 매핑 등록 : [IT설비 매핑] 클릭 > 팝업 > [저장] 누르면 실행되는 메소드
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/insertInspectItemMapping.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertInspectItemMapping(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.insertInspectItemMapping(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑(항목기준) > 상세화면 > [매핑해제], [매핑복구]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/updateInspectItemMappingUseYn.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateInspectItemMappingUseYn(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
			
		try {
			inspectService.updateInspectItemMappingUseYn(paramMap);
			
			resultVO.setResultBoolean(true);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
		}
			
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑(항목기준) > 상세화면 > [매핑삭제]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/deleteInspectMappingItem.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deleteInspectMappingItem(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		String resultString = new String();	
		try {
			resultString = inspectService.deleteInspectMappingItem(paramMap);
			
			resultVO.setResultString(resultString);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
		}
				
		return resultVO;
	}
	
	/*점검항목 매핑 End================================================================================================================================*/
	
	
	
	
	
	
	/*점검항목 매핑(IT설비기준) Start================================================================================================================================*/
	
	/**
	 * 점검항목 매핑(IT설비기준)
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectConfMapping.do", method=RequestMethod.GET)
	public String inspectConfMapping(ModelMap paramMap) throws Exception {
		return "/itg/itam/inspect/inspectConfMapping.nvf";
	}
	
	/**
	 * 점검항목 매핑(IT설비기준) > 점검항목 매핑(IT설비기준) 목록
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectConfMappingList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectConfMappingList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}
			
			List resultList = inspectService.searchAssetList(paramMap);		
			totalCount = inspectService.searchAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑(IT설비기준) > 자산 매핑 등록 : [IT설비 매핑] 클릭 > 팝업 > [저장] 누르면 실행되는 메소드. 아무것도 선택하지 않고 [저장] 버튼 누르면 매핑 해제됨.
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/updateUseYnInspectItemMappingConf.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateUseYnInspectItemMappingConf(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.updateUseYnInspectItemMappingConf(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑(IT설비기준) > 자산 매핑 등록 : [IT설비 매핑] 클릭 > 팝업 > [저장] 누르면 실행되는 메소드
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/insertInspectItemMappingConf.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertInspectItemMappingConf(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.insertInspectItemMappingConf(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검항목 매핑(IT설비기준) > 자산 매핑 등록 : [IT설비 매핑] 클릭 > 팝업 > 점검항목 코드콤보
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectItemCodeComboList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectItemCodeComboList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try {
			List result = inspectService.selectInspectItemCodeComboList(paramMap);
			
			gridVO.setRows(result);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			resultVO.setSuccess(false);
		}
		
		return resultVO;
	}
	
	/*점검항목 매핑(IT설비기준) End================================================================================================================================*/
	
	
	
	
	/*점검결과 등록, 점검결과 승인, 점검결과 조회 Start================================================================================================================================*/
	// 점검결과 등록, 점검결과 승인, 점검결과 조회는 같은 화면 파일 사용. list_type 하나로 등록인지, 승인인지, 조회인지 구분
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > 탭
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectResultTab.do", method=RequestMethod.GET)
	public String inspectResultTab(HttpServletRequest request, ModelMap paramMap) throws Exception {
		String list_type = StringUtil.parseString(request.getParameter("list_type"));
		paramMap.put("list_type", list_type);
		return "/itg/itam/inspect/inspectResultTab.nvf";
	}	
	
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회> 탭 안의 메인 화면
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectResult.do", method=RequestMethod.GET)
	public String inspectResult(HttpServletRequest request,ModelMap paramMap) throws Exception{
		String list_type = StringUtil.parseString(request.getParameter("list_type"));
		paramMap.put("list_type", list_type);
		return "/itg/itam/inspect/inspectResult.nvf";
	}
	
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > 상세화면
	 * 점검결과 목록을 더블 클릭하면 상세화면 탭이 추가 된다.
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectResultDatail.do", method=RequestMethod.GET)
	public String inspectResultDatail(HttpServletRequest request, ModelMap paramMap) throws Exception{
		boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();

		String strAuthGrp = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Itam.Inspect.Auth.Admin")).trim();	//정기점검 관리자 권한
		String strAuthAdmin = "N";
		
		if(isAuthenticated) {
    		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		if(userVO != null){
    			paramMap.put("loginUserId", userVO.getUser_id());
        		
        		String login_user_auth_list = StringUtil.parseString(userVO.getAuth_list()).trim().toUpperCase();
        		
        		if ( strAuthGrp.equals("") == false ){
            		String [] arrstrAuthGrp =  strAuthGrp.split(",");
            		if ( arrstrAuthGrp != null ){
            			if ( arrstrAuthGrp.length > 0 ){
            				for ( int a = 0 ; a < arrstrAuthGrp.length ; a++){
            					if ( login_user_auth_list.indexOf(arrstrAuthGrp[a]) > -1 ){
            						strAuthAdmin = "Y";
            						break;
            					}
            				}
            			}
            		}
        		}
    		}
    		
    	}
		paramMap.put("strAuthAdmin", strAuthAdmin);
		
		String list_type = StringUtil.parseString(request.getParameter("list_type"));
		paramMap.put("list_type", list_type);

		String class_type = StringUtil.parseString(request.getParameter("class_type"));
		paramMap.put("class_type", class_type);
		
		String conf_id = StringUtil.parseString(request.getParameter("conf_id"));
		paramMap.put("conf_id", conf_id);
		
		String inspect_day_startDate = StringUtil.parseString(request.getParameter("inspect_day_startDate"));
		paramMap.put("inspect_day_startDate", inspect_day_startDate);
		
		String inspect_day_endDate = StringUtil.parseString(request.getParameter("inspect_day_endDate"));
		paramMap.put("inspect_day_endDate", inspect_day_endDate);

		return "/itg/itam/inspect/inspectResultDetail.nvf";
	}
	
	/*
	 * 미사용 메소드
	@RequestMapping(value="/itg/itam/inspect/insertInspectResultHead.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertInspectResultHead(@RequestBody Map paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.insertInspectResultHead(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	*/
	
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > 점검결과 목록
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectResultHeadList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultHeadList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}
			
			List resultList = inspectService.selectInspectResultHeadList(paramMap);
			int totalCount = inspectService.selectInspectResultHeadCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
	/**
	 * 점검결과 등록 > [일괄등록 템플릿 다운로드]
	 * 다운 받을 구성정보와 점검항목 정보를 조회해온다.
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/downloadExcelTemplateContent.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO downloadExcelTemplate(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {				
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}

			HashMap resultMap = inspectService.selectExcelTemplateContent(paramMap);
			
			resultVO.setResultMap(resultMap);
			resultVO.setResultBoolean(true);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검결과 등록 > [일괄등록 템플릿 다운로드]
	 * 엑셀로 파싱
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/batchEditGridExcelDown.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO batchEditGridExcelDown(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");

			HashMap sheetMap = new HashMap();
			
			sheetMap.put("sheetName", excelParam.get("fileName"));
			sheetMap.put("titleName", excelParam.get("fileName"));
			
			/*
			 * ※ 엑셀 일괄 다운로드용 그리드 프로퍼티 작성 요령  
				뒤에 + 'Ex'가 붙은 리소스의 정보를 토대로 엑셀 파일을 만들어주는 것을 원칙으로 함. 
				ex) 실제 목록의 그리드 리소스 : grid.process.service.accountrequestproc, 일괄 수정용 그리드 리소스 : grid.process.service.accountrequestprocEx 
				
				1. header : select 쿼리의 alias명 입력. 만약 엑셀에서 보여주고자 하는 컬럼의 속성이 'SELECT','RADIO','POPUP'에 속하고,
				ex) GET_CODE_NM('YN', USE_YN) as USE_YN_NM -> 이 값을 보여주고자 한다면 USE_YN_NM 그대로 입력하면 됩니다.					  					
				2. text : 컬럼명 입력, 엑셀 파일의 4라인에 해당					
				3. htmlType : 엑셀에 표시할 속성이 TEXT, DATE, NUMBER, SELECT, POPUP중 어디에 속하는지 입력. 셀 데이터를 파싱하기 위한 용도로 만든 속성. 					 
				4. tblCols : 테이블명.컬럼명 으로 구성. 엑셀 3라인에 해당. 헤더가 USER_NM이라도 이 부분에선 테이블명.USER_ID로 써줘야 함.					
				5. notNull : 필수값 처리를 위해 만든 속성. * -> 표시가 붙어서 나온다.					
				6. colType : 컬럼의 타입, 데이터 타입을 입력(ex) VARCHAR2, DATE, NUMBER...)					
				7. displayTypes : 엑셀에서 어떻게 보여줄지 처리함.(Y=숨김, U=수정, R=조회) 
				
				makeExcelFileByPoi 메소드에 더 자세한 설명있음.
			*/
			
			sheetMap.put("includeColumns", StringUtil.parseString(excelAttrs.get("header")));
			sheetMap.put("headerNames", StringUtil.parseString(excelAttrs.get("text")));
			sheetMap.put("htmlTypes", StringUtil.parseString(excelAttrs.get("htmlTypes")));
			sheetMap.put("tblCols", StringUtil.parseString(excelAttrs.get("tblCols")));
			sheetMap.put("notNull", StringUtil.parseString(excelAttrs.get("notNull")));
			sheetMap.put("colTypes", StringUtil.parseString(excelAttrs.get("colType")));
			sheetMap.put("displayTypes", StringUtil.parseString(excelAttrs.get("displayTypes")));
			sheetMap.put("headerWrap", StringUtil.parseString(excelAttrs.get("headerWrap")));
			
			// 셀잠금(셀기준)
			sheetMap.put("cellDisplayTypes", excelAttrs.get("cellDisplayTypes")); // List-Map
			sheetMap.put("freezeRow", excelAttrs.get("freezeRow")); // List-Map
			
			List downGrid = (List) excelParam.get("dataRow");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			String columns = StringUtil.parseString(sheetMap.get("includeColumns"));
			
			if(null != columns){				
				String newColumnList = "";				
				param.put("col_id", columns);				
				resultArray.add(downGrid);	// 목록에서 가져온 리스트 			
				newSheetList.add(sheetMap);
				
			} else {
				resultArray.add(null);
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	/**
	 * 점검결과 등록 > [템플릿 일괄등록]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/insertInspectResultDetailByExcel.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertInspectResultDetailByExcel(@RequestBody Map paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.insertInspectResultDetailByExcel(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > [엑셀 다운로드]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectResultHeadExcelDown.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultHeadExcelDown(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			
			ModelMap param = new ModelMap();
			HashMap mapTemp = (HashMap) paramMap.get("param");
			ArrayList resultArray = new ArrayList();
			Iterator iterTemp = null;
			boolean bExcel = false;
			
			if ( mapTemp != null ) { // EXCEL 
				iterTemp = mapTemp.keySet().iterator();
				bExcel = true;
			} else {
				iterTemp = paramMap.keySet().iterator();
			} while ( iterTemp.hasNext() ){
				String strKey = String.valueOf(iterTemp.next());
				String strValue = "";
				if ( bExcel == false ){
					param.put( strKey, paramMap.get(strKey));
				} else {
					param.put( strKey, mapTemp.get(strKey));
				}
			}			
			
			HashMap excelMap = new HashMap();
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				mapTemp.put("loginCustId", userVO.getCust_id());
				mapTemp.put("loginUserId", userVO.getUser_id());
			}
			
			HashMap excelParam = (HashMap) paramMap.get("excelParam");
			List downGrid = inspectService.selectInspectResultHeadList(mapTemp);
			excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, downGrid);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > 상세 화면 > 점검그룹별 점검결과 목록 
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectResultHeadDetailList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultHeadDetailList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}
			
			List resultList = inspectService.selectInspectHeadDetailList(paramMap);
			int totalCount = inspectService.selectInspectHeadDetailListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}

	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > 상세 화면 > [엑셀다운로드]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectResultHeadDetailExcel.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultHeadDetailExcel(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			
			ModelMap param = new ModelMap();
			HashMap mapTemp = (HashMap) paramMap.get("param");
			ArrayList resultArray = new ArrayList();
			Iterator iterTemp = null;
			boolean bExcel = false;
			
			if ( mapTemp != null ) { // EXCEL 
				iterTemp = mapTemp.keySet().iterator();
				bExcel = true;
			} else {
				iterTemp = paramMap.keySet().iterator();
			} while ( iterTemp.hasNext() ){
				String strKey = String.valueOf(iterTemp.next());
				String strValue = "";
				if ( bExcel == false ){
					param.put( strKey, paramMap.get(strKey));
				} else {
					param.put( strKey, mapTemp.get(strKey));
				}
			}			
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				mapTemp.put("loginCustId", userVO.getCust_id());
				mapTemp.put("loginUserId", userVO.getUser_id());
			}
			
			HashMap excelMap = new HashMap();
			HashMap excelParam = (HashMap) paramMap.get("excelParam");
			List downGrid = inspectService.selectInspectHeadDetailList(mapTemp);
			excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, downGrid);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > 점검결과 상세 등록
	 * 점검결과 목록 더블클릭 > 점검결과 상세 탭 추가 > 점검그룹별 점검결과 목록 더블 클릭 > 점검결과 상세정보 팝업 > [저장]
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/insertInspectResultDetail.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertInspectResultDetail(@RequestBody Map paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.insertInspectResultDetail(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검결과 등록, 점검결과 승인, 점검결과 조회 > 점검결과 상세 팝업 > 점검결과
	 * 점검결과 목록 더블클릭 > 점검결과 상세 탭 추가 > 점검그룹별 점검결과 목록 더블 클릭 > 점검결과 상세정보 팝업 > 점검결과 목록
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectResultDetailList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultDetailList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = inspectService.selectInspectResultDetailList(paramMap);
			int totalCount = inspectService.selectInspectResultDetailCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/*
	@RequestMapping(value="/itg/itam/inspect/searchInspectItemComboList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO searchInspectItemComboList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try {				
			List resultList = inspectService.searchInspectItemComboList(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setResultBoolean(true);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
		}
		
		return resultVO;
	}
	*/		
	
	/*
	@RequestMapping(value="/itg/itam/inspect/selectAllInspectResultDetailList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectAllInspectResultDetailList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = inspectService.selectAllInspectResultDetailList(paramMap);
			int totalCount = inspectService.selectInspectResultHeadCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	*/
	
	/**
	 * 점검결과 등록 > 점검결과 상세 탭 > [삭제]
	 * 정기점검 점검결과 HEAD(INSPECT_RESULT_HEAD)정보와 정기점검 점검결과 상세(INSPECT_RESULT_DETAIL)정보 삭제
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/deleteInspectResultHead.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deleteInspectResultHead(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.deleteInspectResultHead(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검결과 등록 > 점검결과 상세 팝업 > [삭제]
	 * 점검결과 목록 더블클릭 > 점검결과 상세 탭 추가 > 점검그룹별 점검결과 목록 더블 클릭 > 점검결과 상세정보 팝업 > [삭제]
	 * 정기점검 점검결과 상세(INSPECT_RESULT_DETAIL)정보 삭제
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/deleteInspectResultDetail.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deleteInspectResultDetail(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.deleteInspectResultDetail(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			resultVO.setResultBoolean(false);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검결과 승인 > [승인] 처리
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/confirmInspectResultHead.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO confirmInspectResultHead(@RequestBody Map paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.confirmInspectResultHead(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/*점검결과 등록, 점검결과 승인, 점검결과 조회 End================================================================================================================================*/
	
	
	
	
	
	
	/*점검결과 추이 조회 Start================================================================================================================================*/
	
	/**
	 * 점검결과 추이 조회 페이지 이동
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectResultProgress.do", method=RequestMethod.GET)
	public String inspectResultProgress(ModelMap paramMap) throws Exception{
		String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		paramMap.put("today", today);
		
		return "/itg/itam/inspect/inspectResultProgress.nvf";
	}
	
	/**
	 * 점검결과 추이 조회 > 점검항목 셀렉트 박스 조회
	 * [점검그룹]선택 시 호출하는 메소드
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/searchInspectItemResultComboList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO searchInspectItemResultComboList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			
			List resultList = inspectService.searchInspectItemResultComboList(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검결과 추이 조회 > 차트 조회
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/inspectResultProgressChart.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultProgressChart(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}
			
			List resultList = inspectService.inspectResultProgressChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검결과 추이 조회 > 정기점검결과 추이 목록
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/searchResultDetailBackDataList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO searchResultDetailBackDataList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				paramMap.put("loginCustId", userVO.getCust_id());
				paramMap.put("loginUserId", userVO.getUser_id());
			}
			
			List resultList = inspectService.searchResultDetailBackDataList(paramMap);
			int totalCount = inspectService.searchResultDetailBackDataListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 점검결과 추이 조회 > 엑셀 다운로드 기능
     * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/itam/inspect/selectInspectResultHeadExcel.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultHeadExcel(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			
			ModelMap param = new ModelMap();
			HashMap mapTemp = (HashMap) paramMap.get("param");
			ArrayList resultArray = new ArrayList();
			Iterator iterTemp = null;
			boolean bExcel = false;
			
			if ( mapTemp != null ) { // EXCEL 
				iterTemp = mapTemp.keySet().iterator();
				bExcel = true;
			} else {
				iterTemp = paramMap.keySet().iterator();
			} while ( iterTemp.hasNext() ){
				String strKey = String.valueOf(iterTemp.next());
				String strValue = "";
				if ( bExcel == false ){
					param.put( strKey, paramMap.get(strKey));
				} else {
					param.put( strKey, mapTemp.get(strKey));
				}
			}			
			
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				mapTemp.put("loginCustId", userVO.getCust_id());
				mapTemp.put("loginUserId", userVO.getUser_id());
			}
			
			HashMap excelMap = new HashMap();
			HashMap excelParam = (HashMap) paramMap.get("excelParam");
			List downGrid = inspectService.searchResultDetailBackDataList(mapTemp);
			excelMap = excelMakerByPoi.makeExcelFileByPoi(excelParam, downGrid);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/*
	@RequestMapping(value="/itg/itam/inspect/inspectResultProgressChartExcel.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultProgressChartExcelData(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			GridVO gridVO = new GridVO();
			List resultList = inspectService.inspectResultProgressChartExcel(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	*/
	
	/*
	@RequestMapping(value="/itg/itam/inspect/selectInspectResultProgressChartExcel.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectInspectResultProgressChartExcel(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			List tempArrayList = inspectService.inspectResultProgressChartExcel(param);
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	*/
	
	/*점검결과 추이 조회 End===================================================================================================================*/
	
	
	
	
	
	
	/*================================================================================================================================*/
	
	/*
	 * 안쓰는 url
	@RequestMapping(value="/itg/itam/inspect/inspectResultQuery.do", method=RequestMethod.POST)
	public String inspectResultQuery(ModelMap paramMap) throws Exception{
		return "/itg/itam/inspect/inspectResultQuery.nvf";
	}
	*/
	
	
	
	/*
	@RequestMapping(value="/itg/itam/inspect/inspectResultConfirm.do", method=RequestMethod.POST)
	public String inspectResultConfirm(ModelMap paramMap) throws Exception{
		return "/itg/itam/inspect/inspectResultConfirm.nvf";
	}
	*/
	
	
	
	// 20201117 임현대 : 사용안함. confirmInspectResultHead 으로 통합
	@RequestMapping(value="/itg/itam/inspect/deconfirmInspectResultHead.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deconfirmInspectResultHead(@RequestBody Map paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean result = inspectService.deconfirmInspectResultHead(paramMap);
			
			resultVO.setResultBoolean(result);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
}
