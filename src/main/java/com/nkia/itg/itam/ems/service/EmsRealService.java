package com.nkia.itg.itam.ems.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface EmsRealService {

	public List searchEmsRealList(ModelMap paramMap) throws NkiaException;
	
	public int searchEmsRealCount(ModelMap paramMap) throws NkiaException;
}
