package com.nkia.itg.itam.ems.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.ems.dao.EmsRealDAO;
import com.nkia.itg.itam.ems.service.EmsRealService;

@Service("emsRealService")
public class EmsRealServiceImpl implements EmsRealService{
	private static final Logger logger = LoggerFactory.getLogger(EmsRealServiceImpl.class);
	
	@Resource(name = "emsRealDAO")
	public EmsRealDAO emsRealDAO;

	@Override
	public List searchEmsRealList(ModelMap paramMap) throws NkiaException {
		return emsRealDAO.searchEmsRealList(paramMap);
	}

	@Override
	public int searchEmsRealCount(ModelMap paramMap) throws NkiaException {
		return emsRealDAO.searchEmsRealCount(paramMap);
	}
}
