package com.nkia.itg.itam.ems.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.intergration.polestar.datasource.IntergrationPolestarSmsDS;
import com.nkia.itg.base.application.exception.NkiaException;

@Repository("emsRealDAO")
public class EmsRealDAO extends IntergrationPolestarSmsDS {

	public List<HashMap> searchEmsRealList(ModelMap paramMap) throws NkiaException{
		return list("EmsRealDAO.searchEmsRealList", paramMap);
	}

	public int searchEmsRealCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("EmsRealDAO.searchEmsRealCount", paramMap);
	}
	
}
