/*
 * @(#)ProjectDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.project.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("projectDAO")
public class ProjcetDAO extends EgovComAbstractDAO{
	
	public List searchProject(ModelMap paramMap) throws NkiaException{
		return list("ProjectDAO.searchProject", paramMap);
	}

	public int searchProjectCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("ProjectDAO.searchProjectCount", paramMap);
	}
	
	public void insertProject(HashMap infoData) throws NkiaException {
		this.insert("ProjectDAO.insertProject", infoData);
	}
	
	public void updateProject(HashMap infoData) throws NkiaException {
		this.update("ProjectDAO.updateProject", infoData);
	}	

	public List searchProjectSub(ModelMap paramMap) throws NkiaException{
		return list("ProjectDAO.searchProjectSub", paramMap);
	}

	public int searchProjectSubCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("ProjectDAO.searchProjectSubCount", paramMap);
	}

	public void deleteProject(ModelMap paramMap) throws NkiaException {
		this.delete("ProjectDAO.deleteProject", paramMap);
	}
	
	public void insertProjectSub(HashMap infoData) throws NkiaException {
		this.insert("ProjectDAO.insertProjectSub", infoData);
	}
	
	public void deleteProjectSub(HashMap infoData) throws NkiaException {
		this.insert("ProjectDAO.deleteProjectSub", infoData);
	}
	
	public List searchSubProject(ModelMap paramMap) throws NkiaException{
		return list("ProjectDAO.searchSubProject", paramMap);
	}
	
	public int searchSubProjectCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("ProjectDAO.searchSubProjectCount", paramMap);
	}
	
	public List searchProjectExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("ProjectDAO.searchProjectExcelDown", paramMap);
		return resultList;
	}	
	
	public List searchProjectYear(ModelMap paramMap) throws NkiaException{
		return list("ProjectDAO.searchProjectYear", paramMap);
	}
	
	public List searchDrimsProject(ModelMap paramMap) throws NkiaException{
		return list("ProjectDAO.searchDrimsProject", paramMap);
	}

	public int searchDrimsProjectCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("ProjectDAO.searchDrimsProjectCount", paramMap);
	}

	public void insertDrimsProjectData(ModelMap paramMap) throws NkiaException {
		this.insert("ProjectDAO.insertDrimsProjectData", paramMap);
	}
	
	public void deleteDrimsProjectData(ModelMap paramMap) throws NkiaException {
		this.delete("ProjectDAO.deleteDrimsProjectData", paramMap);
	}
		
}
