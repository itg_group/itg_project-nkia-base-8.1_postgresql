/*
 * @(#)projectService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.project.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ProjectService {

	public List searchProject(ModelMap paramMap) throws NkiaException;

	public int searchProjectCount(ModelMap paramMap) throws NkiaException;

	public List searchProjectSub(ModelMap paramMap) throws NkiaException;
	
	public int searchProjectSubCount(ModelMap paramMap) throws NkiaException;
	
	public void insertProject(ModelMap paramMap) throws NkiaException;
	
	public void updateProject(ModelMap paramMap) throws NkiaException;
	
	public void deleteProject(ModelMap paramMap) throws NkiaException, IOException;
		
	public void insertProjectSub(ModelMap paramMap) throws NkiaException;
	
	public void deleteProjectSub(ModelMap paramMap) throws NkiaException;
	
	public List searchSubProject(ModelMap paramMap) throws NkiaException;
	
	public int searchSubProjectCount(ModelMap paramMap) throws NkiaException;	
	
	public List searchProjectExcelDown(Map paramMap) throws NkiaException;

	public List searchProjectYear(ModelMap paramMap) throws NkiaException;
	
	public List searchDrimsProject(ModelMap paramMap) throws NkiaException;

	public int searchDrimsProjectCount(ModelMap paramMap) throws NkiaException;
	
	public void insertDrimsProjectData(ModelMap paramMap) throws NkiaException;
		
}
