/*
 * @(#)projectServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.project.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.project.dao.ProjcetDAO;
import com.nkia.itg.itam.project.service.ProjectService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("projectService")
public class ProjectServiceImpl implements ProjectService{
	
	private static final Logger logger = LoggerFactory.getLogger(ProjectServiceImpl.class);
	
	@Resource(name = "projectDAO")
	public ProjcetDAO projectDAO;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;	
	
	public List searchProject(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchProject(paramMap);
	}

	public int searchProjectCount(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchProjectCount(paramMap);
	}
	
	public void insertProject(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("login_user_id", userVO.getUser_id());
    	}
    	projectDAO.insertProject(paramMap);
	}

	public void updateProject(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("login_user_id", userVO.getUser_id());
    	}
    	projectDAO.updateProject(paramMap);
	}
	
	public void deleteProject(ModelMap paramMap) throws NkiaException, IOException {
		
		projectDAO.deleteProjectSub(paramMap);
		projectDAO.deleteProject(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	public List searchProjectSub(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchProjectSub(paramMap);
	}

	public int searchProjectSubCount(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchProjectSubCount(paramMap);
	}
	
	public void insertProjectSub(ModelMap paramMap) throws NkiaException {
		projectDAO.insertProjectSub(paramMap);
	}	
	
	public void deleteProjectSub(ModelMap paramMap) throws NkiaException {
		projectDAO.deleteProjectSub(paramMap);
	}

	
	public List searchSubProject(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchSubProject(paramMap);
	}
	
	public int searchSubProjectCount(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchSubProjectCount(paramMap);
	}
	
	public List searchProjectExcelDown(Map paramMap) throws NkiaException {		
		List resultList = null;
		resultList = projectDAO.searchProjectExcelDown(paramMap); 
		return resultList;
	}

	public List searchProjectYear(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchProjectYear(paramMap);
	}
	
	
	public List searchDrimsProject(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchDrimsProject(paramMap);
	}

	public int searchDrimsProjectCount(ModelMap paramMap) throws NkiaException {
		return projectDAO.searchDrimsProjectCount(paramMap);
	}
	
	public void insertDrimsProjectData(ModelMap paramMap) throws NkiaException {

		StringBuffer project_id = new StringBuffer();
		for(int i=0; i<paramMap.size(); i++){
			String mapIndex = toString().valueOf(i);
			project_id.append("'"+paramMap.get(mapIndex)+"'");
			
			if(i != paramMap.size()-1){
				project_id.append(",");
			}
		}
		
		paramMap.put("project_id", project_id);
		
		projectDAO.deleteDrimsProjectData(paramMap);
		projectDAO.insertDrimsProjectData(paramMap);
	}		
}
