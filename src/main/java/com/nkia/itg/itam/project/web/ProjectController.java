/*
 * @(#)ProjectController.java              2013. 5. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.project.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.project.service.ProjectService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ProjectController {
	
	@Resource(name = "projectService")
	private ProjectService projectService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	

	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 사업정보관리 - 사업정보관리 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/project/projectManager.do")
	public String projectManager(ModelMap paramMap) throws NkiaException {
    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("login_user_id", userVO.getUser_id());
    	}
		String forwarPage = "/itg/itam/project/projectManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 사업정보관리 - 사업정보관리 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/project/searchProject.do")
	public @ResponseBody ResultVO searchProject(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		List<HashMap> resultList = null;
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = projectService.searchProject(paramMap);		
			totalCount = projectService.searchProjectCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	
	/**
	 * 사업정보관리 - 사업관리 신규등록
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/project/insertProject.do")
	public @ResponseBody ResultVO insertProject(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			projectService.insertProject(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사업정보관리 - 사업관리 수정
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/project/updateProject.do")
	public @ResponseBody ResultVO updateProject(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			projectService.updateProject(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사업정보관리 - 세부사업내역 등록
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/project/insertProjectSub.do")
	public @ResponseBody ResultVO insertProjectSub(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			projectService.insertProjectSub(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사업정보관리 - 세부사업내역 제거
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/project/deleteProjectSub.do")
	public @ResponseBody ResultVO deleteProjectSub(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			projectService.deleteProjectSub(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사업정보관리 - 사업정보 제거
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/project/deleteProject.do")
	public @ResponseBody ResultVO deleteProject(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			projectService.deleteProject(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사업정보관리 - 상세내역 등록시 세부사업조회 팝업
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/project/searchSubProject.do")
	public @ResponseBody ResultVO searchSubProject(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = projectService.searchSubProject(paramMap);		
			totalCount = projectService.searchSubProjectCount(paramMap);
				
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사업정보관리 - 등록된 세부사업내역 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/project/searchProjectSub.do")
	public @ResponseBody ResultVO searchProjectSub(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = projectService.searchProjectSub(paramMap);		
			totalCount = projectService.searchProjectSubCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사업정보관리 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/itam/project/searchProjectExcelDown.do")
    public @ResponseBody ResultVO searchProjectExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
          ResultVO resultVO = new ResultVO();
          List resultList = new ArrayList();
          ModelMap excelMap = new ModelMap();
          try{
                 Map excelParam = (Map) paramMap.get("excelParam");  
                 Map param = (Map) paramMap.get("param");
                 
                 resultList = projectService.searchProjectExcelDown(param);
                 excelMap = excelMaker.makeExcelFile(excelParam, resultList);
                 
                 resultVO.setResultMap(excelMap);
                 
          } catch(Exception e) {
                 throw new NkiaException(e);
          }
          return resultVO;
	}

	
	/**
	 * 멀티코드 리스트
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/project/searchProjectYear.do")
	public @ResponseBody ResultVO searchProjectYear(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		List resultList = null;
		try{
			GridVO gridVO = new GridVO();
			resultList = projectService.searchProjectYear(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
}
