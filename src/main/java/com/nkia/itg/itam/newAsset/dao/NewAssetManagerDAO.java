/*
 * @(#)NewAssetManagerDAO.java              2013. 6. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.newAsset.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("newAssetManagerDAO")
public class NewAssetManagerDAO extends EgovComAbstractDAO {
	
	public List searchNewAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("newAssetManagerDAO.searchNewAssetList", paramMap);
		return resultList;
	}
	
	public int searchNewAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		int count = (Integer) this.selectByPk("newAssetManagerDAO.searchNewAssetListCount", paramMap);
		return count;
	}
	
	public void updateAssetStateChange(Map paramMap) throws NkiaException {
		this.update("newAssetManagerDAO.updateAssetStateChange", paramMap);
	}
	
	public List searchAmClassName(Map param) throws NkiaException {
		return list("newAssetManagerDAO.searchAmClassName", param);
	}
	
	public String selectClassType(ModelMap paramMap) throws NkiaException {
		return (String) this.selectByPk("newAssetManagerDAO.selectClassType", paramMap);
	}
	
	public void insertAssetInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertAssetInfo", map);
	}
	
	public void insertSwInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertSwInfo", map);
	}
	
	public void insertMaintBuyInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertMaintBuyInfo", map);
	}
	
	public void insertInfraInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertInfraInfo", map);
	}
	
	public void insertSpecSvInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertSpecSvInfo", map);
	}
	
	public void insertSpecNwInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertSpecNwInfo", map);
	}
	
	public void insertSpecScInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertSpecScInfo", map);
	}
	
	public void insertSpecBkInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertSpecBkInfo", map);
	}
	
	public void insertSpecStInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertSpecStInfo", map);
	}
	
	public void insertSpecSwInfo(Map map) throws NkiaException {
		this.update("newAssetManagerDAO.insertSpecSwInfo", map);
	}
	
	public String createAssetId(ModelMap paramMap) throws NkiaException {
    	GregorianCalendar calendar = new GregorianCalendar();
    	int year = calendar.get(Calendar.YEAR)-2000;
    	int month = calendar.get(Calendar.MONTH)+1;
    	paramMap.put("year", year);
    	if(month < 10){
    		String mon = "0"+month;
    		paramMap.put("month", mon);
    	}else{
    		paramMap.put("month", month);
    	}
		return (String) this.selectByPk("newAssetManagerDAO.createAssetId", paramMap);
	}
	
	public String createConfId(ModelMap paramMap) throws NkiaException {
    	GregorianCalendar calendar = new GregorianCalendar();
    	int year = calendar.get(Calendar.YEAR)-2000;
    	int month = calendar.get(Calendar.MONTH)+1;
    	paramMap.put("year", year);
    	if(month < 10){
    		String mon = "0"+month;
    		paramMap.put("month", mon);
    	}else{
    		paramMap.put("month", month);
    	}
		return (String) this.selectByPk("newAssetManagerDAO.createConfId", paramMap);
	}
	
	public void deleteCpuInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteCpuInfo", paramMap);
	}
	
	public void insertCpuInfo(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.insertCpuInfo", paramMap);
	}
	
	public void deleteOperUserInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteOperUserInfo", paramMap);
	}
	
	public void insertOperUserInfo(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.insertOperUserInfo", paramMap);
	}
	
	public void deleteIpInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteIpInfo", paramMap);
	}
	
	public void insertIpInfo(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.insertIpInfo", paramMap);
	}
	
	public void deleteMemInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteMemInfo", paramMap);
	}
	
	public void insertMemInfo(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.insertMemInfo", paramMap);
	}
	
	public void deleteInnerDiskInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteInnerDiskInfo", paramMap);
	}
	
	public void insertInnerDiskInfo(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.insertInnerDiskInfo", paramMap);
	}

	public void deleteRelInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteRelInfo", paramMap);
	}
	// @@20130719 전민수 START
	// 자산원장 삭제로직 수정
	public void deleteAllRelInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteAllRelInfo", paramMap);
	}
	//@@20130719 전민수 END
	public void insertRelInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.insertRelInfo", paramMap);
		this.delete("newAssetManagerDAO.insertReverseRelInfo", paramMap);
	}
	
	public void deleteRelHaInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteRelHaInfo", paramMap);
	}
	
	public void insertRelHaInfo(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.insertRelHaInfo", paramMap);
	}
	//@@20130711 전민수  START
	// createHisSeqReverse 메소드 신규추가
	// createHisSeq 메소드 신규추가
	// createSeq 메소드 신규추가
	public void createHisSeqReverse(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.createHisSeqReverse", paramMap);
	}
	
	public void createHisSeq(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.createHisSeq", paramMap);
	}
	
	public String createSeq(Map map) throws NkiaException {
		// TODO Auto-generated method stub
		return (String) selectByPk("newAssetManagerDAO.createSeq", map);
	}
	//@@20130711 전민수 END
	
	//@@20130726 전민수  START
	// 서비스연계에 대한 로직이 없었으므로 신규추가함
	public void deleteRelServiceInfo(Map paramMap) throws NkiaException {
		this.delete("newAssetManagerDAO.deleteRelServiceInfo", paramMap);
	}
	
	public void insertRelServiceInfo(Map paramMap) throws NkiaException {
		this.insert("newAssetManagerDAO.insertRelServiceInfo", paramMap);
	}
	//@@20130726 전민수 END
	
	public void updateAssetState(ModelMap paramMap) throws NkiaException {
		this.update("newAssetManagerDAO.updateAssetState", paramMap);
	}
		
}
