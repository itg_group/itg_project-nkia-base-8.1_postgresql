/*
 * @(#)newAssetManagerService.java              2013. 6. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.newAsset.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface NewAssetManagerService {
	
	public int searchNewAssetListCount(ModelMap paramMap) throws NkiaException;

	public List searchNewAssetList(ModelMap paramMap) throws NkiaException;
	
	public void updateAssetStateChange(ModelMap paramMap) throws NkiaException;
	
	public List searchAmClassName(Map param) throws NkiaException;
	
	public String selectClassType(ModelMap paramMap) throws NkiaException;
	
	public void insertAssetBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public String createAssetId(ModelMap paramMap) throws NkiaException;
	
	public String createConfId(ModelMap paramMap) throws NkiaException;
	
	public void updateAssetState(ModelMap paramMap) throws NkiaException;
}
