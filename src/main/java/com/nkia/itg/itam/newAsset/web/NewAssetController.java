/*
 * @(#)NewAssetController.java              2013. 6. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.newAsset.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.newAsset.service.NewAssetManagerService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
/**
 * @version 1.0 2013. 4. 8.
 * @author <a href="mailto:minsoo@nkia.co.kr"> 
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class NewAssetController {
	
	/**
	 * service 호출
	 */
	@Resource(name = "newAssetManagerService")
	private NewAssetManagerService newAssetManagerService;

	/**
	 * message 호출
	 */
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 자산도입 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/newAsset/newAssetManagerTab.do")
	public String newAssetManagerTab(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/newAsset/newAssetManagerTab.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/newAsset/newAssetManagerTab_webix.do")
	public String newAssetManagerTabWebix(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		
		//hmsong cmdb 개선 작업 Start
		String confAdminYn = new String(); //confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		paramMap.put("confAdminYn", confAdminYn);		
		//hmsong cmdb 개선 작업 End
		
		String forwarPage = "/itg/itam/newAsset/newAssetManagerTab_webix.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산도입 화면 로딩
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/newAsset/newAssetManager.do")
	public String newAssetManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {		
		String forwarPage = "/itg/itam/newAsset/newAssetManager.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/newAsset/newAssetManager_webix.do")
	public String newAssetManagerWebix(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//hmsong cmdb 개선 작업 Start
		String confAdminYn = new String(); //confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		paramMap.put("confAdminYn", confAdminYn);
		//hmsong cmdb 개선 작업 End
		String forwarPage = "/itg/itam/newAsset/newAssetManager_webix.nvf";
		return forwarPage;
	}
		
	/**
	 * 자산목록리스트
     * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/itam/newAsset/searchNewAssetList.do")
	public @ResponseBody ResultVO searchNewAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = newAssetManagerService.searchNewAssetList(paramMap);		
			totalCount = newAssetManagerService.searchNewAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/newAsset/searchAmClassName.do")
	public @ResponseBody ResultVO searchAmClassName(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = newAssetManagerService.searchAmClassName(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 자산 상태 변경 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/newAsset/updateAssetStateChange.do")
	public @ResponseBody ResultVO updateAssetStateChange(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	newAssetManagerService.updateAssetStateChange(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 구성타입조회 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/newAsset/selectClassType.do")
	public @ResponseBody ResultVO selectClassType(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			String checkValue = newAssetManagerService.selectClassType(paramMap);
			resultVO.setResultString(checkValue);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	/**
	 * 자산 기본정보(기본정보, 스펙정보, 유지보수정보) 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/newAsset/insertAssetBasicInfo.do")
	public @ResponseBody ResultVO insertAssetBasicInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	newAssetManagerService.insertAssetBasicInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산ID, 구성ID 생성  
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/newAsset/createAssetConfId.do")
	public @ResponseBody ResultVO createAssetConfId(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		String amTable = new String();
		try{
			String confType = (String) paramMap.get("class_type");
			
			if(confType.equals("SW")){
				amTable = "AM_SW";
			}else{
				amTable = "AM_INFRA";
			}
			paramMap.put("amTable", amTable);
			
			String assetId = newAssetManagerService.createAssetId(paramMap);
			String confId = newAssetManagerService.createConfId(paramMap);
			
			paramMap.put("assetId", assetId);
			paramMap.put("confId", confId);
			
			resultVO.setResultMap(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	/**
	 * 자산 기본정보(기본정보, 스펙정보, 유지보수정보) 등록
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/newAsset/updateAssetState.do")
	public @ResponseBody ResultVO updateAssetState(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("upd_user_id", userVO.getUser_id());
			}
			newAssetManagerService.updateAssetState(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
