/*
 * @(#)newAssetManagerServiceImpl.java              2013. 6. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.newAsset.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.newAsset.dao.NewAssetManagerDAO;
import com.nkia.itg.itam.newAsset.service.NewAssetManagerService;

/**
 * @version 1.0 2013. 4. 9.
 * @author <a href="mailto:minsoo@nkia.co.kr"> msJeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("newAssetManagerService")
public class NewAssetManagerServiceImpl implements NewAssetManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(NewAssetManagerServiceImpl.class);

	@Resource(name = "newAssetManagerDAO")
	public NewAssetManagerDAO newAssetManagerDAO;
	
	public List searchNewAssetList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return newAssetManagerDAO.searchNewAssetList(paramMap);
	}
	
	public int searchNewAssetListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return newAssetManagerDAO.searchNewAssetListCount(paramMap);
	}
	
	public void updateAssetStateChange(ModelMap paramMap) throws NkiaException {
		newAssetManagerDAO.updateAssetStateChange(paramMap);
	}
	
	public List searchAmClassName(Map param) throws NkiaException {
		// TODO Auto-generated method stub
		return newAssetManagerDAO.searchAmClassName(param);
	}
	
	public String selectClassType(ModelMap paramMap) throws NkiaException {
		return newAssetManagerDAO.selectClassType(paramMap);
	}
	
	public void insertAssetBasicInfo(ModelMap paramMap) throws NkiaException {
		
		Map map = (Map) paramMap.get("infraTotalMap");
		// @@20130719 전민수 START
		// 첨부 파일기능 추가됐음
		Map fileMap = (Map) paramMap.get("fileMap");
		//@@20130719 전민수 END
		
		String updUserId = (String) paramMap.get("upd_user_id");
		map.put("upd_user_id", updUserId);
		
		String classType = (String) map.get("class_type");

		String amTable = new String();
		String seq = newAssetManagerDAO.createSeq(map);
		map.put("seq", seq);
		
		newAssetManagerDAO.createHisSeq(map);
		newAssetManagerDAO.createHisSeqReverse(map);
		
		// @@20130719 전민수 START
		// 첨부 파일기능 추가됐음
		String atch_file_id = (String) fileMap.get("atch_file_id");
		map.put("atch_file_id", atch_file_id);
		//@@20130719 전민수 END
		
		newAssetManagerDAO.insertAssetInfo(map);
		
		if(classType.equals("SW")){
			newAssetManagerDAO.insertSwInfo(map);
		}else{
			newAssetManagerDAO.insertInfraInfo(map);
		}
		
		newAssetManagerDAO.insertMaintBuyInfo(map);

		if(classType.equals("SV")){
			newAssetManagerDAO.insertSpecSvInfo(map);
		}else if(classType.equals("NW")){
			newAssetManagerDAO.insertSpecNwInfo(map);
		}else if(classType.equals("SC")){
			newAssetManagerDAO.insertSpecScInfo(map);
		}else if(classType.equals("BK")){
			newAssetManagerDAO.insertSpecBkInfo(map);
		}else if(classType.equals("ST")){
			newAssetManagerDAO.insertSpecStInfo(map);
		}else if(classType.equals("SW")){
			newAssetManagerDAO.insertSpecSwInfo(map);
		}		

		Map operUserMap = (Map) paramMap.get("operUserMap");
		Map cpuMap = (Map) paramMap.get("cpuMap");
		Map memMap = (Map) paramMap.get("memMap");
		Map ipMap = (Map) paramMap.get("ipMap");
		Map innerDiskMap = (Map) paramMap.get("innerDiskMap");
		
		operUserMap.put("upd_user_id", updUserId);
		cpuMap.put("upd_user_id", updUserId);
		memMap.put("upd_user_id", updUserId);
		ipMap.put("upd_user_id", updUserId);
		innerDiskMap.put("upd_user_id", updUserId);
		
		List<Map> cpuStoreArray = new ArrayList<Map>(); 
		cpuStoreArray = (ArrayList) cpuMap.get("cpuStore");
				
		// CPU정보 전체 삭제 
		newAssetManagerDAO.deleteCpuInfo(cpuMap);
		
		// CPU정보 등록
		for(int i = 0; i < cpuStoreArray.size();  i++){
			cpuStoreArray.get(i).put("seq", i+1);
			newAssetManagerDAO.insertCpuInfo(cpuStoreArray.get(i));
		}
		
		List<Map> operUserArray = new ArrayList<Map>(); 
		operUserArray = (ArrayList) operUserMap.get("operUserStore");
		
		// 담당자정보 전체 삭제 
		newAssetManagerDAO.deleteOperUserInfo(operUserMap);
		
		// 담당자정보 등록
		for(int i = 0; i < operUserArray.size();  i++){
			operUserArray.get(i).put("seq", i+1);
			newAssetManagerDAO.insertOperUserInfo(operUserArray.get(i));
		}
		
		List<Map> ipStoreArray = new ArrayList<Map>(); 
		ipStoreArray = (ArrayList) ipMap.get("ipStore");

		// IP정보 전체 삭제
		newAssetManagerDAO.deleteIpInfo(ipMap);
		
		// IP정보 등록
		for(int i = 0; i < ipStoreArray.size();  i++){
			ipStoreArray.get(i).put("seq", i+1);
			newAssetManagerDAO.insertIpInfo(ipStoreArray.get(i));
		}
		
		List<Map> memStoreArray = new ArrayList<Map>(); 
		memStoreArray = (ArrayList) memMap.get("memStore");

		// MEM정보 전체 삭제 
		newAssetManagerDAO.deleteMemInfo(memMap);
		
		// MEM정보 등록
		for(int i = 0; i < memStoreArray.size();  i++){
			memStoreArray.get(i).put("seq", i+1);
			newAssetManagerDAO.insertMemInfo(memStoreArray.get(i));
		}
		
		if(classType.equals("SV")){
			List<Map> inDiskStoreArray = new ArrayList<Map>(); 
			inDiskStoreArray = (ArrayList) innerDiskMap.get("innerDiskGridStore");

			// 내장디스크정보(서버일경우) 전체 삭제 
			newAssetManagerDAO.deleteInnerDiskInfo(innerDiskMap);
			
			// 내장디스크정보(서버일경우) 등록
			for(int i = 0; i < inDiskStoreArray.size();  i++){
				inDiskStoreArray.get(i).put("seq", i+1);
				newAssetManagerDAO.insertInnerDiskInfo(inDiskStoreArray.get(i));
			}
		}
		// @@20130726 전민수 START
		// 3.서비스연계정보 로직이 없어서 신규 추가
		Map relServiceAssetMap = (Map) paramMap.get("relServiceAssetMap");
		relServiceAssetMap.put("upd_user_id", updUserId);
		
		List<Map> serviceRelStore = new ArrayList<Map>(); 
		serviceRelStore = (ArrayList) relServiceAssetMap.get("serviceRelStore");

		newAssetManagerDAO.deleteRelServiceInfo(relServiceAssetMap);
				
		for(int i = 0; i < serviceRelStore.size();  i++){
			newAssetManagerDAO.insertRelServiceInfo(serviceRelStore.get(i));
		}
		// @@20130726 전민수 END
		
		//인프라연계정보
		Map relSvAssetMap = (Map) paramMap.get("relSvAssetMap");
		Map relNwAssetMap = (Map) paramMap.get("relNwAssetMap");
		Map relScAssetMap = (Map) paramMap.get("relScAssetMap");
		Map relStAssetMap = (Map) paramMap.get("relStAssetMap");
		Map relBkAssetMap = (Map) paramMap.get("relBkAssetMap");
		Map relSwAssetMap = (Map) paramMap.get("relSwAssetMap");
		Map relHaAssetMap = (Map) paramMap.get("relHaAssetMap");
				
		relSvAssetMap.put("upd_user_id", updUserId);
		relNwAssetMap.put("upd_user_id", updUserId);
		relScAssetMap.put("upd_user_id", updUserId);
		relStAssetMap.put("upd_user_id", updUserId);
		relBkAssetMap.put("upd_user_id", updUserId);
		relSwAssetMap.put("upd_user_id", updUserId);
		relHaAssetMap.put("upd_user_id", updUserId);
		
		//서버연계정보
		List<Map> insertSvStore = new ArrayList<Map>(); 
		insertSvStore = (ArrayList) relSvAssetMap.get("svStore");
		// @@20130719 전민수 START
		// 2.자산원장 삭제로직 수정
		newAssetManagerDAO.deleteAllRelInfo(relSvAssetMap);
		// @@20130719 전민수 END
		newAssetManagerDAO.deleteRelInfo(relSvAssetMap);
				
		if(insertSvStore != null){
			for(int i = 0; i < insertSvStore.size();  i++){
				newAssetManagerDAO.insertRelInfo(insertSvStore.get(i));
			}
		}
				
		//네트워크연계정보
		List<Map> insertNwStore = new ArrayList<Map>(); 
		insertNwStore = (ArrayList) relNwAssetMap.get("nwStore");
			
		newAssetManagerDAO.deleteRelInfo(relNwAssetMap);
				
		if(insertNwStore != null){
			for(int i = 0; i < insertNwStore.size();  i++){
				newAssetManagerDAO.insertRelInfo(insertNwStore.get(i));
			}
		}
		//보안연계정보
		List<Map> insertScStore = new ArrayList<Map>(); 
		insertScStore = (ArrayList) relScAssetMap.get("scStore");
				
		newAssetManagerDAO.deleteRelInfo(relScAssetMap);
				
		if(insertScStore != null){
			for(int i = 0; i < insertScStore.size();  i++){
				newAssetManagerDAO.insertRelInfo(insertScStore.get(i));
			}
		}
				
		List<Map> insertStStore = new ArrayList<Map>(); 
		insertStStore = (ArrayList) relStAssetMap.get("stStore");
				
		newAssetManagerDAO.deleteRelInfo(relStAssetMap);
				
		if(insertStStore != null){
			for(int i = 0; i < insertStStore.size();  i++){
				newAssetManagerDAO.insertRelInfo(insertStStore.get(i));
			}
		}
				
		List<Map> insertBkStore = new ArrayList<Map>(); 
		insertBkStore = (ArrayList) relBkAssetMap.get("bkStore");
				
		newAssetManagerDAO.deleteRelInfo(relBkAssetMap);
				
		if(insertBkStore != null){
			for(int i = 0; i < insertBkStore.size();  i++){
				newAssetManagerDAO.insertRelInfo(insertBkStore.get(i));
			}
		}
				
		List<Map> insertSwStore = new ArrayList<Map>(); 
		insertSwStore = (ArrayList) relSwAssetMap.get("swStore");
				
		newAssetManagerDAO.deleteRelInfo(relSwAssetMap);
				
		if(insertSwStore != null){
			for(int i = 0; i < insertSwStore.size();  i++){
				newAssetManagerDAO.insertRelInfo(insertSwStore.get(i));
			}
		}
				
		List<Map> insertHaStore = new ArrayList<Map>(); 
		insertHaStore = (ArrayList) relHaAssetMap.get("haStore");

		newAssetManagerDAO.deleteRelHaInfo(relHaAssetMap);
				
		for(int i = 0; i < insertHaStore.size();  i++){
			newAssetManagerDAO.insertRelHaInfo(insertHaStore.get(i));
		}
	}

	
	public String createAssetId(ModelMap paramMap) throws NkiaException {
		return newAssetManagerDAO.createAssetId(paramMap);
	}
	
	public String createConfId(ModelMap paramMap) throws NkiaException {
		return newAssetManagerDAO.createConfId(paramMap);
	}

	public void updateAssetState(ModelMap paramMap) throws NkiaException {
		newAssetManagerDAO.updateAssetState(paramMap);
	}	
}
