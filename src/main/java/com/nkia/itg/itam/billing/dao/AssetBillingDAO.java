package com.nkia.itg.itam.billing.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * 자산 과금 등록/수정
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2017. 04. 03. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("assetBillingDAO")
public class AssetBillingDAO extends EgovComAbstractDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetBillingDAO.class);
	
	// 과금대상 자산 분류체계 트리
	public List searchClassTree(HashMap paramMap) throws NkiaException {
		return list("AssetBillingDAO.searchClassTree", paramMap);
	}
	
	// 과금대상 자산 목록
	public List searchAssetBillingList(HashMap paramMap) throws NkiaException{
		List resultList = list("AssetBillingDAO.searchAssetBillingList", paramMap);
		List resultList2 = new ArrayList();
		if(paramMap.containsKey("rnum")){
			Map resultMap = new HashMap();
			
			//List resultList = new List();
			int size = resultList.size();
			for(int i=0; i<size; i++) {
				resultMap = (HashMap)resultList.get(i);
				resultMap.put("RNUM", (String)paramMap.get("rnum")+"-"+(String)resultMap.get("RNUM"));
				resultList2.add(resultMap);
			}
			return resultList2;
		}else{
			return resultList;
		}
	}
	
	// 과금대상 자산 목록 카운트
	public int searchAssetBillingListCount(HashMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetBillingDAO.searchAssetBillingListCount", paramMap);
	}
	
	// 엑셀 다운로드
	public List searchExcel(Map paramMap) throws NkiaException {
		return list("AssetBillingDAO.searchExcel", paramMap);
	}
	
	// 엑셀 다운로드 리소스
	public List searchExcelByResource(Map paramMap) throws NkiaException {
		return list("AssetBillingDAO.searchExcelByResource", paramMap);
	}
	
	// 자산 마스터정보 조회
	public List searchAssetBillingMaster(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchAssetBillingMaster", paramMap);
	}
	
	// 자산 서버 과금정보 조회
	public List searchAssetBillingSv(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchAssetBillingSv", paramMap);
	}
	
	// 자산 서버 과금정보 없으면 등록, 있으면 수정
	public void mergeIntoAssetBillingSv(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoAssetBillingSv", paramMap);
	}
	
	// 자산 메모리 증설량 조회
	public List searchBsInsMemory(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchBsInsMemory", paramMap);
	}
	
	// 자산 CPU Core 증설량 조회
	public List searchBsInsCpu(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchBsInsCpu", paramMap);
	}
	
	// 자산 스토리지 증설량 조회
	public List searchBsInsStorage(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchBsInsStorage", paramMap);
	}

	// 자산 메모리 증설량 없으면 등록, 있으면 수정
	public void mergeIntoBsInsMemory(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoBsInsMemory", paramMap);
	}

	// 자산 CPU Core 증설량 없으면 등록, 있으면 수정
	public void mergeIntoBsInsCpu(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoBsInsCpu", paramMap);
	}

	// 자산 스토리지 증설량 없으면 등록, 있으면 수정
	public void mergeIntoBsInsStorage(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoBsInsStorage", paramMap);
	}

	// 자산 메모리 증설량 삭제
	public void deleteBsInsMemory(HashMap paramMap) throws NkiaException{
		this.delete("AssetBillingDAO.deleteBsInsMemory", paramMap);
	}

	// 자산 CPU Core 증설량 삭제
	public void deleteBsInsCpu(HashMap paramMap) throws NkiaException{
		this.delete("AssetBillingDAO.deleteBsInsCpu", paramMap);
	}

	// 자산 스토리지 증설량 삭제
	public void deleteBsInsStorage(HashMap paramMap) throws NkiaException{
		this.delete("AssetBillingDAO.deleteBsInsStorage", paramMap);
	}
	
	// 사용처 조회
	public List searchBsCustRate(HashMap paramMap) throws NkiaException {
		return list("AssetBillingDAO.searchBsCustRate", paramMap);
	}
	
	// 사용처 등록용 부서 트리
	public List searchDeptTree(HashMap paramMap) throws NkiaException {
		return list("AssetBillingDAO.searchDeptTree", paramMap);
	}
	
	// 사용처 등록용 업무 리스트
	public List searchService(HashMap paramMap) throws NkiaException {
		return list("AssetBillingDAO.searchService", paramMap);
	}
	
	// 사용처 - 이전 과금일 업데이트
	// 과금일이 수정 되었을 때,
	// 이전 과금시작일이 수정된 과금시작일보다 크면
	// 이전 과금종료일을 수정된 과금시작일-1 로 업데이트 한다.
	public void updateBsCustRateOverEndDt(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.updateBsCustRateOverEndDt", paramMap);
	}
	
	// 사용처 - 수정
	public void updateBsCustRate(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.updateBsCustRate", paramMap);
	}
		
	// 사용처 - 등록
	public void insertBsCustRate(HashMap paramMap) throws NkiaException{
		this.insert("AssetBillingDAO.insertBsCustRate", paramMap);
	}
	
	// 사용처 삭제
	public int deleteBsCustRate(HashMap paramMap) throws NkiaException{
		return this.delete("AssetBillingDAO.deleteBsCustRate", paramMap);
	}
	
	// 자산 NW 과금정보 조회
	public List searchAssetBillingNw(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchAssetBillingNw", paramMap);
	}
	
	// 자산 NW 과금정보 없으면 등록, 있으면 수정
	public void mergeIntoAssetBillingNw(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoAssetBillingNw", paramMap);
	}
	
	// 자산 NW 모델 조회
	public List searchAssetBillingNwModel(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchAssetBillingNwModel", paramMap);
	}
	
	// 자산 NW 서비스중단일 조회
	public List searchBsSvcStop(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchBsSvcStop", paramMap);
	}

	// 자산  NW 서비스중단일 없으면 등록, 있으면 수정
	public void mergeIntoBsSvcStop(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoBsSvcStop", paramMap);
	}

	// 자산  NW 서비스중단일 삭제
	public void deleteBsSvcStop(HashMap paramMap) throws NkiaException{
		this.delete("AssetBillingDAO.deleteBsSvcStop", paramMap);
	}
	
	// 자산 LN 과금정보 조회
	public List searchAssetBillingLn(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchAssetBillingLn", paramMap);
	}
	
	// 자산 LN 과금정보 없으면 등록, 있으면 수정
	public void mergeIntoAssetBillingLn(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoAssetBillingLn", paramMap);
	}
	
	// 자산 Storage 과금정보 조회
	public List searchAssetBillingSt(HashMap paramMap) throws NkiaException{
		return list("AssetBillingDAO.searchAssetBillingSt", paramMap);
	}
	
	// 자산 Storage 과금정보 없으면 등록, 있으면 수정
	public void mergeIntoAssetBillingSt(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoAssetBillingSt", paramMap);
	}
	
	// 사용처(공통비) 조회
	public List searchBsCmprc(HashMap paramMap) throws NkiaException {
		return list("AssetBillingDAO.searchBsCmprc", paramMap);
	}
	
	// 자산  사용처(공통비) 없으면 등록, 있으면 수정
	public void mergeIntoBsCmprc(HashMap paramMap) throws NkiaException{
		this.update("AssetBillingDAO.mergeIntoBsCmprc", paramMap);
	}

	// 자산  사용처(공통비) 삭제
	public void deleteBsCmprc(HashMap paramMap) throws NkiaException{
		this.delete("AssetBillingDAO.deleteBsCmprc", paramMap);
	}
}
