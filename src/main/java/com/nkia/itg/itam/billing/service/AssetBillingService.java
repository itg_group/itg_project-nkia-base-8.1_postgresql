package com.nkia.itg.itam.billing.service;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;


/**
 * 자산 과금 등록/수정
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2017. 04. 03. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface AssetBillingService {
	
	/**
	 * 공통 과금 영역
	 */
	// 과금대상 자산 분류체계 트리
	public List searchClassTree(HashMap paramMap) throws NkiaException;
	
	// 과금대상 자산 목록
	public List searchAssetBillingList(HashMap paramMap) throws NkiaException;
	
	// 과금대상 자산 목록 카운트
	public int searchAssetBillingListCount(HashMap paramMap) throws NkiaException;
	
	// 엑셀 다운로드
	public List searchExcel(Map paramMap) throws NkiaException;
	
	// 엑셀 다운로드 리소스
	public List searchExcelByResource(Map paramMap) throws NkiaException;
	
	// 자산 마스터정보 조회
	public List searchAssetBillingMaster(HashMap paramMap) throws NkiaException;
	
	// 사용처 조회
	public List searchBsCustRate(HashMap paramMap) throws NkiaException;
	
	// 사용처 등록용 부서 트리
	public List searchDeptTree(HashMap paramMap) throws NkiaException;
	
	// 사용처 등록용 업무 리스트
	public List searchService(HashMap paramMap) throws NkiaException;
	
	// 사용처(공통비) 조회
	public List searchBsCmprc(HashMap paramMap) throws NkiaException;
	
	
	/**
	 * SV 과금 영역
	 */
	// 자산 메모리 증설량 조회
	public List searchBsInsMemory(HashMap paramMap) throws NkiaException;
	
	// 자산 CPU Core 증설량 조회
	public List searchBsInsCpu(HashMap paramMap) throws NkiaException;
	
	// 자산 스토리지 증설량 조회
	public List searchBsInsStorage(HashMap paramMap) throws NkiaException;
	
	// 자산 서버 과금정보 조회
	public List searchAssetBillingSv(HashMap paramMap) throws NkiaException;

	// 서버 과금정보 (센터 내) 등록
	public void changeAssetBillingSvIn(HashMap paramMap) throws NkiaException, ParseException;
	
	// 서버 과금정보 (센터 외) 등록
	public void changeAssetBillingSvOut(HashMap paramMap) throws NkiaException, ParseException;
	
	
	/**
	 * NW 과금 영역
	 */
	// 자산 NW 과금정보 조회
	public List searchAssetBillingNw(HashMap paramMap) throws NkiaException;
	
	// NW 과금정보 (센터 내) 등록
	public void changeAssetBillingNwIn(HashMap paramMap) throws NkiaException;
	
	// NW 과금정보 (센터 외) 등록
	public void changeAssetBillingNwOut(HashMap paramMap) throws NkiaException, ParseException;
	
	// 자산 NW 모델 조회
	public List searchAssetBillingNwModel(HashMap paramMap) throws NkiaException;
	
	// 서비스중단일 조회
	public List searchBsSvcStop(HashMap paramMap) throws NkiaException;
	
	
	/**
	 * LN 과금 영역
	 */
	// 자산 LN 과금정보 조회
	public List searchAssetBillingLn(HashMap paramMap) throws NkiaException;
	
	// LN 과금정보 (센터 내) 등록
	public void changeAssetBillingLnIn(HashMap paramMap) throws NkiaException;
	
	// LN 과금정보 (센터 외) 등록
	public void changeAssetBillingLnOut(HashMap paramMap) throws NkiaException, ParseException;
	
	
	/**
	 * Storage 과금 영역
	 */
	// 자산 Storage 과금정보 조회
	public List searchAssetBillingSt(HashMap paramMap) throws NkiaException;
	
	// Storage 과금정보 (센터 내) 등록
	public void changeAssetBillingStIn(HashMap paramMap) throws NkiaException;
	
	// Storage 과금정보 (센터 외) 등록
	public void changeAssetBillingStOut(HashMap paramMap) throws NkiaException;
	
}