package com.nkia.itg.itam.billing.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.billing.dao.AssetBillingDAO;
import com.nkia.itg.itam.billing.service.AssetBillingService;

/**
 * 자산 과금 등록/수정
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2017. 04. 03. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("assetBillingService")
public class AssetBillingServiceImpl implements AssetBillingService{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetBillingServiceImpl.class);
	
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	@Resource(name = "assetBillingDAO")
	private AssetBillingDAO assetBillingDAO;
	
	// 과금 청구처별 사용량 (BS_CUST_RATE) 의 컬럼정보를 나열한다.[0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] BS_CUST_RATE_COLS = {
		"CUST_ID",
		"BS_BGN_DT",
		"BS_END_DT",
		"RATE",
		"SERVICE_ID"
	};
	
	// 과금대상 자산 분류체계 트리
	public List searchClassTree(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchClassTree(paramMap);
	}
	
	// 과금대상 자산 목록
	public List searchAssetBillingList(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchAssetBillingList(paramMap);
	}
	
	// 과금대상 자산 목록 카운트
	public int searchAssetBillingListCount(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchAssetBillingListCount(paramMap);
	}
	
	// 엑셀 다운로드
	public List searchExcel(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetBillingDAO.searchExcel(paramMap); 
		return resultList;
	}
	
	// 엑셀 다운로드 리소스
	public List searchExcelByResource(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = assetBillingDAO.searchExcelByResource(paramMap); 
		return resultList;
	}
	
	// 자산 마스터정보 조회
	public List searchAssetBillingMaster(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchAssetBillingMaster(paramMap);
	}
	
	// 자산 서버 과금정보 조회
	public List searchAssetBillingSv(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchAssetBillingSv(paramMap);
	}
	
	// 자산 메모리 증설량 조회
	public List searchBsInsMemory(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchBsInsMemory(paramMap);
	}
	
	// 자산 CPU Core 증설량 조회
	public List searchBsInsCpu(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchBsInsCpu(paramMap);
	}
	
	// 자산 스토리지 증설량 조회
	public List searchBsInsStorage(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchBsInsStorage(paramMap);
	}
	
	// 사용처 조회
	public List searchBsCustRate(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchBsCustRate(paramMap);
	}
	
	// 서비스중단일 조회
	public List searchBsSvcStop(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchBsSvcStop(paramMap);
	}
	
	// 사용처 등록용 부서 트리
	public List searchDeptTree(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchDeptTree(paramMap);
	}
	
	// 사용처 등록용 업무 리스트
	public List searchService(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchService(paramMap);
	}
	
	// 사용처(공통비) 조회
	public List searchBsCmprc(HashMap paramMap) throws NkiaException {
		return assetBillingDAO.searchBsCmprc(paramMap);
	}
	
	// 서버 과금정보 (센터 내) 등록
	public void changeAssetBillingSvIn(HashMap paramMap) throws NkiaException, ParseException{
		
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		
		/**
		 * 수정 전 데이터
		 */
		List<HashMap> prevMemList = searchBsInsMemory(confIdMap);
		List<HashMap> prevCpuList = searchBsInsCpu(confIdMap);
		List<HashMap> prevStList = searchBsInsStorage(confIdMap);
		
		/**
		 * 수정대상 화면에서 넘어온 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
		List<HashMap> curMemList = (List)paramMap.get("BS_INS_MEMORY");
		List<HashMap> curCpuList = (List)paramMap.get("BS_INS_CPU");
		List<HashMap> curStList = (List)paramMap.get("BS_INS_STORAGE");
		
		/**
		 * 서버 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingSv(formData);
		
		/**
		 * 메모리 증설량 삭제 로직
		 */
		if(prevMemList!=null && prevMemList.size()>0) {
			for(int i=0; i<prevMemList.size(); i++) {
				if(curMemList!=null && curMemList.size()>0) {
					// 새로운 데이터가 있으면 메모리 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevMemList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curMemList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curMemList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsInsMemory(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존 메모리 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsInsMemory(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * CPU Core 증설량 삭제 로직
		 */
		if(prevCpuList!=null && prevCpuList.size()>0) {
			for(int i=0; i<prevCpuList.size(); i++) {
				if(curCpuList!=null && curCpuList.size()>0) {
					// 새로운 데이터가 있으면  CPU Core 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevCpuList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curCpuList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curCpuList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsInsCpu(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존  CPU Core 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsInsCpu(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * 스토리지 증설량 삭제 로직
		 */
		if(prevStList!=null && prevStList.size()>0) {
			for(int i=0; i<prevStList.size(); i++) {
				if(curStList!=null && curStList.size()>0) {
					// 새로운 데이터가 있으면  스토리지 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevStList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curStList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curStList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsInsStorage(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존  스토리지 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsInsStorage(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * 메모리 증설량 등록 로직
		 */
		if(curMemList!=null && curMemList.size()>0) {
			// 메모리 증설량의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curMemList.size(); i++) {
				HashMap row = new HashMap();
				row = curMemList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsInsMemory(row);
			}
		}
		
		/**
		 * CPU Core 증설량 등록 로직
		 */
		if(curCpuList!=null && curCpuList.size()>0) {
			// CPU Core 증설량의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curCpuList.size(); i++) {
				HashMap row = new HashMap();
				row = curCpuList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsInsCpu(row);
			}
		}
		
		/**
		 * 스토리지 증설량 등록 로직
		 */
		if(curStList!=null && curStList.size()>0) {
			// 스토리지 증설량의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curStList.size(); i++) {
				HashMap row = new HashMap();
				row = curStList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsInsStorage(row);
			}
		}
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingSv = searchAssetBillingSv(confIdMap);
		List<HashMap> newMemList = searchBsInsMemory(confIdMap);
		List<HashMap> newCpuList = searchBsInsCpu(confIdMap);
		List<HashMap> newStList = searchBsInsStorage(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsSvHistory(parentConfId, newBillingSv.get(0), "", userId);
		assetHistoryService.insertBsInsMemoryHistory(parentConfId, newMemList, "", userId);
		assetHistoryService.insertBsInsCpuHistory(parentConfId, newCpuList, "", userId);
		assetHistoryService.insertBsInsStorageHistory(parentConfId, newStList, "", userId);
		
		/**
		 * 수정대상 화면에서 사용처 데이터 리스트 (현재 값)
		 */
		List<HashMap> curCustRateSvList = (List)paramMap.get("CUST_RATE_SV");
		List<HashMap> curCustRateStList = (List)paramMap.get("CUST_RATE_ST");
		
		/**
		 * 사용처 등록 로직 실행
		 */
		changeCustRate(parentConfId, "SV", userId, curCustRateSvList);
		changeCustRate(parentConfId, "ST", userId, curCustRateStList);
	}
	
	// 서버 과금정보 (센터 외) 등록
	public void changeAssetBillingSvOut(HashMap paramMap) throws NkiaException, ParseException{
		
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		
		/**
		 * 수정 전 데이터
		 */
		List<HashMap> prevMemList = searchBsInsMemory(confIdMap);
		List<HashMap> prevCpuList = searchBsInsCpu(confIdMap);
		List<HashMap> prevStList = searchBsInsStorage(confIdMap);
		
		/**
		 * 수정대상 화면에서 넘어온 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
		List<HashMap> curMemList = (List)paramMap.get("BS_INS_MEMORY");
		List<HashMap> curCpuList = (List)paramMap.get("BS_INS_CPU");
		List<HashMap> curStList = (List)paramMap.get("BS_INS_STORAGE");
		
		/**
		 * 서버 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingSv(formData);
		
		/**
		 * 메모리 증설량 삭제 로직
		 */
		if(prevMemList!=null && prevMemList.size()>0) {
			for(int i=0; i<prevMemList.size(); i++) {
				if(curMemList!=null && curMemList.size()>0) {
					// 새로운 데이터가 있으면 메모리 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevMemList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curMemList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curMemList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsInsMemory(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존 메모리 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsInsMemory(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * CPU Core 증설량 삭제 로직
		 */
		if(prevCpuList!=null && prevCpuList.size()>0) {
			for(int i=0; i<prevCpuList.size(); i++) {
				if(curCpuList!=null && curCpuList.size()>0) {
					// 새로운 데이터가 있으면  CPU Core 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevCpuList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curCpuList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curCpuList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsInsCpu(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존  CPU Core 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsInsCpu(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * 스토리지 증설량 삭제 로직
		 */
		if(prevStList!=null && prevStList.size()>0) {
			for(int i=0; i<prevStList.size(); i++) {
				if(curStList!=null && curStList.size()>0) {
					// 새로운 데이터가 있으면  스토리지 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevStList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curStList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curStList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsInsStorage(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존  스토리지 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsInsStorage(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * 메모리 증설량 등록 로직
		 */
		if(curMemList!=null && curMemList.size()>0) {
			// 메모리 증설량의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curMemList.size(); i++) {
				HashMap row = new HashMap();
				row = curMemList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsInsMemory(row);
			}
		}
		
		/**
		 * CPU Core 증설량 등록 로직
		 */
		if(curCpuList!=null && curCpuList.size()>0) {
			// CPU Core 증설량의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curCpuList.size(); i++) {
				HashMap row = new HashMap();
				row = curCpuList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsInsCpu(row);
			}
		}
		
		/**
		 * 스토리지 증설량 등록 로직
		 */
		if(curStList!=null && curStList.size()>0) {
			// 스토리지 증설량의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curStList.size(); i++) {
				HashMap row = new HashMap();
				row = curStList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsInsStorage(row);
			}
		}
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingSv = searchAssetBillingSv(confIdMap);
		List<HashMap> newMemList = searchBsInsMemory(confIdMap);
		List<HashMap> newCpuList = searchBsInsCpu(confIdMap);
		List<HashMap> newStList = searchBsInsStorage(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsSvHistory(parentConfId, newBillingSv.get(0), "", userId);
		assetHistoryService.insertBsInsMemoryHistory(parentConfId, newMemList, "", userId);
		assetHistoryService.insertBsInsCpuHistory(parentConfId, newCpuList, "", userId);
		assetHistoryService.insertBsInsStorageHistory(parentConfId, newStList, "", userId);
		
		/**
		 * 수정대상 화면에서 사용처 데이터 리스트 (현재 값)
		 */
		List<HashMap> curCustRateSvList = (List)paramMap.get("CUST_RATE_SV");
		List<HashMap> curCustRateStList = (List)paramMap.get("CUST_RATE_ST");
		
		/**
		 * 사용처 등록 로직 실행
		 */
		changeCustRate(parentConfId, "SV", userId, curCustRateSvList);
		changeCustRate(parentConfId, "ST", userId, curCustRateStList);
	}
	
	// 자산 NW 과금정보 조회
	public List searchAssetBillingNw(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchAssetBillingNw(paramMap);
	}
	
	// NW 과금정보 (센터 내) 등록
	public void changeAssetBillingNwIn(HashMap paramMap) throws NkiaException{
		
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		confIdMap.put("conf_type", "NW");
		
		/**
		 * 수정 전 데이터
		 */
		List<HashMap> prevSvcStopList = searchBsSvcStop(confIdMap);
		List<HashMap> prevCmprcList = searchBsCmprc(confIdMap);
		
		/**
		 * 수정대상 화면에서 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
		List<HashMap> curSvcStopList = (List)paramMap.get("BS_SVC_STOP");
		List<HashMap> curCmprcList = (List)paramMap.get("CMPRC_SE_NW");
		
		/**
		 * NW 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingNw(formData);
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingNw = searchAssetBillingNw(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsNwHistory(parentConfId, newBillingNw.get(0), "", userId);
		
		/**
		 * 사용처(공통비) 삭제 로직
		 */
		if(prevCmprcList!=null && prevCmprcList.size()>0) {
			for(int i=0; i<prevCmprcList.size(); i++) {
				if(curCmprcList!=null && curCmprcList.size()>0) {
					// 새로운 데이터가 있으면 메모리 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevCmprcList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curCmprcList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curCmprcList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsCmprc(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존 메모리 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsCmprc(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * 사용처(공통비) 등록 로직
		 */
		if(curCmprcList!=null && curCmprcList.size()>0) {
			// 서비스 중단일의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curCmprcList.size(); i++) {
				HashMap row = new HashMap();
				row = curCmprcList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsCmprc(row);
			}
		}
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newCmprcList = searchBsCmprc(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsCmprcCustRateHistory(parentConfId, newCmprcList, "", userId);
		
		/**
		 * 서비스 중단일 삭제 로직
		 */
		if(prevSvcStopList!=null && prevSvcStopList.size()>0) {
			for(int i=0; i<prevSvcStopList.size(); i++) {
				if(curSvcStopList!=null && curSvcStopList.size()>0) {
					// 새로운 데이터가 있으면 메모리 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevSvcStopList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curSvcStopList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curSvcStopList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsSvcStop(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존 메모리 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsSvcStop(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * 서비스 중단일 등록 로직
		 */
		if(curSvcStopList!=null && curSvcStopList.size()>0) {
			// 서비스 중단일의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curSvcStopList.size(); i++) {
				HashMap row = new HashMap();
				row = curSvcStopList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsSvcStop(row);
			}
		}
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newSvcStopList = searchBsSvcStop(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsSvcStopHistory(parentConfId, newSvcStopList, "", userId);
		
	}
	
	// NW 과금정보 (센터 외) 등록
	public void changeAssetBillingNwOut(HashMap paramMap) throws NkiaException, ParseException{
		
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		confIdMap.put("conf_type", "NW");
		
		/**
		 * 수정 전 데이터
		 */
//		List<HashMap> prevSvcStopList = searchBsSvcStop(confIdMap);
		
		/**
		 * 수정대상 화면에서 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
//		List<HashMap> curSvcStopList = (List)paramMap.get("BS_SVC_STOP");
		
		/**
		 * NW 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingNw(formData);
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingNw = searchAssetBillingNw(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsNwHistory(parentConfId, newBillingNw.get(0), "", userId);
		
		/**
		 * 서비스 중단일 삭제 로직
		 */
//		if(prevSvcStopList!=null && prevSvcStopList.size()>0) {
//			for(int i=0; i<prevSvcStopList.size(); i++) {
//				if(curSvcStopList!=null && curSvcStopList.size()>0) {
//					// 새로운 데이터가 있으면 메모리 증설량 삭제 데이터 찾은 후 해당 row 삭제
//					HashMap prevMap = new HashMap();
//					prevMap = prevSvcStopList.get(i);
//					String prevIdx = (String)prevMap.get("BS_INDX");
//					boolean isExist = false;
//					for(int j=0; j<curSvcStopList.size(); j++) {
//						HashMap curMap = new HashMap();
//						curMap = curSvcStopList.get(j);
//						String curIdx = (String)curMap.get("BS_INDX");
//						if(prevIdx.equals(curIdx)) {
//							isExist = true;
//							break;
//						}
//					}
//					if(!isExist) {
//						HashMap deleteMap = new HashMap();
//						deleteMap.put("CONF_ID", parentConfId);
//						deleteMap.put("BS_INDX", prevIdx);
//						assetBillingDAO.deleteBsSvcStop(deleteMap);
//					}
//				} else {
//					// 새로운 데이터가 없으면 기존 메모리 증설량 모두 삭제
//					HashMap deleteMap = new HashMap();
//					deleteMap.put("CONF_ID", parentConfId);
//					assetBillingDAO.deleteBsSvcStop(deleteMap);
//					break;
//				}
//			}
//		}
		
		/**
		 * 서비스 중단일 등록 로직
		 */
//		if(curSvcStopList!=null && curSvcStopList.size()>0) {
//			// 서비스 중단일의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
//			for(int i=0; i<curSvcStopList.size(); i++) {
//				HashMap row = new HashMap();
//				row = curSvcStopList.get(i);
//				row.put("user_id", userId);
//				assetBillingDAO.mergeIntoBsSvcStop(row);
//			}
//		}
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
//		List<HashMap> newSvcStopList = searchBsSvcStop(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
//		assetHistoryService.insertBsSvcStopHistory(parentConfId, newSvcStopList, "", userId);
		
		
		/**
		 * 수정대상 화면에서 사용처 데이터 리스트 (현재 값)
		 */
		List<HashMap> curCustRateNwList = (List)paramMap.get("CUST_RATE_NW");
		
		/**
		 * 사용처 등록 로직 실행
		 */
		changeCustRate(parentConfId, "NW", userId, curCustRateNwList);
	}
	
	// 자산 NW 모델 조회
	public List searchAssetBillingNwModel(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchAssetBillingNwModel(paramMap);
	}
	
	
	// 사용처 로직
	private void changeCustRate(String CONF_ID, String CLASS_TYPE, String USER_ID, List<HashMap> curCustRateList) throws NkiaException, ParseException{
		
		HashMap paramMap = new HashMap();
		paramMap.put("conf_id", CONF_ID);
		paramMap.put("conf_type", CLASS_TYPE);
		paramMap.put("user_id", USER_ID);
		
		/**
		 * 이전 데이터 선택
		 */
		List<HashMap> prevCustRateList = searchBsCustRate(paramMap);
		
		/**
		 * 수정로직 시작
		 */
		if(prevCustRateList==null || prevCustRateList.isEmpty()) {
			/**
			 * 이전 데이터가 없으면
			 * 현재 데이터를 등록한다.
			 */
			if(curCustRateList!=null && curCustRateList.size()>0) {
				
				for(int i=0; i<curCustRateList.size(); i++) {
					HashMap row = new HashMap();
					row = curCustRateList.get(i);
					row.put("user_id", USER_ID);
					assetBillingDAO.insertBsCustRate(row);
				}
				
				// INSERT 된 데이터 다시 선택
				List<HashMap> insertCustRateList = searchBsCustRate(paramMap);
				
				// 다시 선택 된 데이터로 이력모듇 호출
				assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, insertCustRateList, "", USER_ID);
			}
			
		} else if(curCustRateList==null || curCustRateList.isEmpty()) {
			/**
			 * 이전 데이터가 있고, 현재 데이터가 없으면
			 * 이전 데이터를 삭제한다.
			 */
			// 이전 데이터의 과금시작일, 종료일을 세팅
			String prevBgnDtStr = (String)((HashMap)prevCustRateList.get(0)).get("BS_BGN_DT");
			String prevEndDtStr = (String)((HashMap)prevCustRateList.get(0)).get("BS_END_DT");
			int prevBgnDtInt = Integer.parseInt(prevBgnDtStr.replaceAll("-", ""));
			int prevEndDtInt = Integer.parseInt(prevEndDtStr.replaceAll("-", ""));
						
			HashMap childParamMap = new HashMap();
			childParamMap.put("conf_id", CONF_ID);
			childParamMap.put("conf_type", CLASS_TYPE);
			childParamMap.put("bs_bgn_dt", prevBgnDtStr);
			childParamMap.put("bs_end_dt", prevEndDtStr);
			
			// 이전 데이터 삭제
			assetBillingDAO.deleteBsCustRate(childParamMap);
			
			// 삭제 된 정보이므로 빈 배열로 이력 모듈 호출
			assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, null, "", USER_ID);
			
		} else {
			/**
			 * 이전 데이터와 현재 데이터가 모두 있을 경우
			 * 두 데이터를 비교하여 로직 수행
			 */
			// 현재 데이터 등록 여부 flag
			boolean isNewDataInsert = false;
			
			// 이전 데이터와 현재 데이터의 각 과금시작일, 종료일을 세팅
			String prevBgnDtStr = (String)((HashMap)prevCustRateList.get(0)).get("BS_BGN_DT");
			String prevEndDtStr = (String)((HashMap)prevCustRateList.get(0)).get("BS_END_DT");
			int prevBgnDtInt = Integer.parseInt(prevBgnDtStr.replaceAll("-", ""));
			int prevEndDtInt = Integer.parseInt(prevEndDtStr.replaceAll("-", ""));
			
			String curBgnDtStr = (String)((HashMap)curCustRateList.get(0)).get("BS_BGN_DT");
			String curEndDtStr = (String)((HashMap)curCustRateList.get(0)).get("BS_END_DT");
			int curBgnDtInt = Integer.parseInt(curBgnDtStr.replaceAll("-", ""));
			int curEndDtInt = Integer.parseInt(curEndDtStr.replaceAll("-", ""));
			
			if(curBgnDtInt > prevBgnDtInt) {
				/**
				 * 현재 데이터의 시작일이 이전 데이터의 시작일보다 클 경우
				 * 이전 데이터의 종료일을 UPDATE 후 (현재 시작일의 D-1로)
				 * 현재 데이터를 등록한다.
				 */
				Date curBgnDt = new SimpleDateFormat("yyyy-MM-dd").parse(curBgnDtStr);
				Calendar cal = Calendar.getInstance();
				cal.setTime(curBgnDt);
				cal.add(cal.DATE,-1);
				String newPrevEndDtStr = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
				
				HashMap childParamMap = new HashMap();
				childParamMap.put("conf_id", CONF_ID);
				childParamMap.put("conf_type", CLASS_TYPE);
				childParamMap.put("user_id", USER_ID);
				childParamMap.put("bs_end_dt", newPrevEndDtStr);
				
				// 종료일 업데이트 되기 전 데이터 선택
				List<HashMap> beforeCustRateList = searchBsCustRate(paramMap);
				
				// 종료일 업데이트 되기 전 데이터로 히스토리 insert
				assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, beforeCustRateList, "", USER_ID);
				
				// 종료일 UPDATE
				assetBillingDAO.updateBsCustRateOverEndDt(childParamMap);
				
				// 종료일 UPDATE 된 데이터 다시 선택
//				List<HashMap> updateCustRateList = searchBsCustRate(paramMap);
				
				// 다시 선택 된 데이터로 이력모듇 호출 (종료일 업데이트 되기 전 데이터로 히스토리 insert 하는 로직으로 변경)
//				assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, updateCustRateList, "", USER_ID);
				
				// 현재 데이터 등록 여부 true
				isNewDataInsert = true;
				
			} else if(curBgnDtInt < prevBgnDtInt) {
				/**
				 * 현재 데이터의 시작일이 이전 데이터의 시작일보다 작을 경우
				 * 이전 데이터를 삭제 후
				 * 현재 데이터를 등록한다.
				 */
				HashMap childParamMap = new HashMap();
				childParamMap.put("conf_id", CONF_ID);
				childParamMap.put("conf_type", CLASS_TYPE);
				childParamMap.put("bs_bgn_dt", curBgnDtInt);
				childParamMap.put("bs_end_dt", curBgnDtInt);
				
				// 이전 데이터 삭제
				assetBillingDAO.deleteBsCustRate(childParamMap);
				
				// 삭제 된 정보이므로 빈 배열로 이력 모듈 호출
//				assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, null, "", USER_ID);
				
				// 현재 데이터 등록 여부 true
				isNewDataInsert = true;
				
			} else {
				
				if( (curEndDtInt == prevEndDtInt && prevCustRateList.size() != curCustRateList.size()) || prevCustRateList.size() != curCustRateList.size() ) {
					/**
					 * 시작일과 종료일이 모두 같고, 
					 * 현재 데이터와 이전 데이터의 사이즈가 다르다면
					 * 이전 데이터 삭제
					 */
					HashMap childParamMap = new HashMap();
					childParamMap.put("conf_id", CONF_ID);
					childParamMap.put("conf_type", CLASS_TYPE);
					childParamMap.put("bs_bgn_dt", curBgnDtInt);
					childParamMap.put("bs_end_dt", curBgnDtInt);
					
					// 이전 데이터 삭제
					assetBillingDAO.deleteBsCustRate(childParamMap);
					
					// 삭제 된 정보이므로 빈 배열로 이력 모듈 호출
//					assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, null, "", USER_ID);
					
					// 현재 데이터 등록 여부 true
					isNewDataInsert = true;
				} else {
					/**
					 * 현재 데이터의 시작일이 같을 경우
					 * 변경 값이 있는지 비교하여 변경 값이 있을 경우에만
					 * 이전 데이터 삭제 후 현재 데이터를 등록한다.
					 */
					// 이전 데이터와 현재 데이터의 크기가 같으면 값을 비교하여 수정 값이 있는지 체크
					int equalDataCount = 0;
					
					// 이전 데이터와 현재 데이터의 row를 하나의 문자열로 만들어 주어 문자열을 비교한다.
					List<String> prevConcatData = new ArrayList<String>();
					List<String> curConcatData = new ArrayList<String>();
					
					for(int i=0; i<prevCustRateList.size(); i++) {
						// 이전 데이터의 HashMap에 있는 값을 한 문자열로 만들어 새 리스트를 만든다.
						HashMap dataMap = new HashMap();
						dataMap = prevCustRateList.get(i);
						String concatData = "";
						for(int j=0; j<BS_CUST_RATE_COLS.length; j++) {
							concatData += StringUtil.parseString(dataMap.get(BS_CUST_RATE_COLS[j]));
						}
						prevConcatData.add(concatData);
					}
					
					for(int i=0; i<curCustRateList.size(); i++) {
						// 현재 데이터의 HashMap에 있는 값을 한 문자열로 만들어 새 리스트를 만든다.
						HashMap dataMap = new HashMap();
						dataMap = curCustRateList.get(i);
						String concatData = "";
						for(int j=0; j<BS_CUST_RATE_COLS.length; j++) {
							concatData += StringUtil.parseString(dataMap.get(BS_CUST_RATE_COLS[j]));
						}
						curConcatData.add(concatData);
					}
					
					// 문자열로 만든 리스트끼리 비교하여 같은 값일 때, 카운트를 증가시켜준다.
					for(int i=0; i<prevConcatData.size(); i++) {
						for(int j=0; j<curConcatData.size(); j++) {
							if(prevConcatData.get(i).equals(curConcatData.get(j))) {
								equalDataCount++;
								break;
							}
						}
					}
					
					// 카운트의 값이 값 리스트의 크기와 다르다면 다른 값이 있는 것이므로,
					// 이전 데이터를 삭제한다.
					if(equalDataCount != prevCustRateList.size()) {
						
						HashMap childParamMap = new HashMap();
						childParamMap.put("conf_id", CONF_ID);
						childParamMap.put("conf_type", CLASS_TYPE);
						childParamMap.put("bs_bgn_dt", prevBgnDtStr);
						childParamMap.put("bs_end_dt", prevEndDtStr);
						
						// 이전 데이터 삭제
						assetBillingDAO.deleteBsCustRate(childParamMap);
						
						// 삭제 된 정보이므로 빈 배열로 이력 모듈 호출
//						assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, null, "", USER_ID);
						
						// 현재 데이터 등록 여부 true
						isNewDataInsert = true;
					}
				}
			}
			
			/**
			 * 현재 데이터 등록 여부가 true 이면
			 * 현재 데이터 등록 후 이력 모듈 호출
			 */
			if(isNewDataInsert) {
				// 현재 데이터 등록
				for(int i=0; i<curCustRateList.size(); i++) {
					HashMap row = new HashMap();
					row = curCustRateList.get(i);
					row.put("user_id", USER_ID);
					assetBillingDAO.insertBsCustRate(row);
				}
				
				// INSERT 된 데이터 다시 선택
				List<HashMap> insertCustRateList = searchBsCustRate(paramMap);
				
				// 다시 선택 된 데이터로 이력모듇 호출
				assetHistoryService.insertBsCustRateHistory(CONF_ID, CLASS_TYPE, insertCustRateList, "", USER_ID);
			}
		}
	}
	
	// 자산 LN 과금정보 조회
	public List searchAssetBillingLn(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchAssetBillingLn(paramMap);
	}
	
	// LN 과금정보 (센터 내) 등록
	public void changeAssetBillingLnIn(HashMap paramMap) throws NkiaException{
		
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		confIdMap.put("conf_type", "LN");
		
		/**
		 * 수정대상 화면에서 폼 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
		
		/**
		 * 이전 데이터 선택
		 */
		List<HashMap> prevCmprcList = searchBsCmprc(paramMap);
		
		/**
		 * LN 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingLn(formData);
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingLn = searchAssetBillingLn(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsLnHistory(parentConfId, newBillingLn.get(0), "", userId);
		
		/**
		 * 수정대상 화면에서 사용처 데이터 리스트 (현재 값)
		 */
		List<HashMap> curCmprcList = (List)paramMap.get("CMPRC_SE_LN");
		
		/**
		 * 사용처(공통비) 삭제 로직
		 */
		if(prevCmprcList!=null && prevCmprcList.size()>0) {
			for(int i=0; i<prevCmprcList.size(); i++) {
				if(curCmprcList!=null && curCmprcList.size()>0) {
					// 새로운 데이터가 있으면 메모리 증설량 삭제 데이터 찾은 후 해당 row 삭제
					HashMap prevMap = new HashMap();
					prevMap = prevCmprcList.get(i);
					String prevIdx = (String)prevMap.get("BS_INDX");
					boolean isExist = false;
					for(int j=0; j<curCmprcList.size(); j++) {
						HashMap curMap = new HashMap();
						curMap = curCmprcList.get(j);
						String curIdx = (String)curMap.get("BS_INDX");
						if(prevIdx.equals(curIdx)) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						HashMap deleteMap = new HashMap();
						deleteMap.put("CONF_ID", parentConfId);
						deleteMap.put("BS_INDX", prevIdx);
						assetBillingDAO.deleteBsCmprc(deleteMap);
					}
				} else {
					// 새로운 데이터가 없으면 기존 메모리 증설량 모두 삭제
					HashMap deleteMap = new HashMap();
					deleteMap.put("CONF_ID", parentConfId);
					assetBillingDAO.deleteBsCmprc(deleteMap);
					break;
				}
			}
		}
		
		/**
		 * 사용처(공통비) 등록 로직
		 */
		if(curCmprcList!=null && curCmprcList.size()>0) {
			// 서비스 중단일의 기존값이 없으면 등록, 있으면 수정 (Merge Into)
			for(int i=0; i<curCmprcList.size(); i++) {
				HashMap row = new HashMap();
				row = curCmprcList.get(i);
				row.put("user_id", userId);
				assetBillingDAO.mergeIntoBsCmprc(row);
			}
		}
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newCmprcList = searchBsCmprc(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsCmprcCustRateHistory(parentConfId, newCmprcList, "", userId);
		
	}
	
	// LN 과금정보 (센터 외) 등록
	public void changeAssetBillingLnOut(HashMap paramMap) throws NkiaException, ParseException{
		
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		confIdMap.put("conf_type", "LN");
		
		/**
		 * 수정대상 화면에서 폼 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
		
		/**
		 * LN 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingLn(formData);
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingLn = searchAssetBillingLn(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsLnHistory(parentConfId, newBillingLn.get(0), "", userId);
		
		/**
		 * 수정대상 화면에서 사용처 데이터 리스트 (현재 값)
		 */
		List<HashMap> curCustRateLnList = (List)paramMap.get("CUST_RATE_LN");
		
		/**
		 * 사용처 등록 로직 실행
		 */
		changeCustRate(parentConfId, "LN", userId, curCustRateLnList);
	}
	
	// 자산 Storage 과금정보 조회
	public List searchAssetBillingSt(HashMap paramMap) throws NkiaException{
		return assetBillingDAO.searchAssetBillingSt(paramMap);
	}
	
	// Storage 과금정보 (센터 내) 등록
	public void changeAssetBillingStIn(HashMap paramMap) throws NkiaException{
		
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		confIdMap.put("conf_type", "ST");
		
		/**
		 * 수정대상 화면에서 폼 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
		
		/**
		 * Storage 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingSt(formData);
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingSt = searchAssetBillingSt(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsStHistory(parentConfId, newBillingSt.get(0), "", userId);
	}
	
	// Storage 과금정보 (센터 내) 등록
	public void changeAssetBillingStOut(HashMap paramMap) throws NkiaException{
			
		String userId = (String)paramMap.get("user_id");
		String parentConfId = (String)paramMap.get("parent_conf_id");
		
		HashMap confIdMap = new HashMap();
		confIdMap.put("conf_id", parentConfId);
		confIdMap.put("conf_type", "ST");
		
		/**
		 * 수정대상 화면에서 폼 데이터 리스트 (현재 값)
		 */
		HashMap formData = (HashMap)paramMap.get("FORM");
		
		/**
		 * Storage 과금 정보의 기존값이 없으면 등록, 있으면 수정
		 */
		formData.put("user_id", userId);
		assetBillingDAO.mergeIntoAssetBillingSt(formData);
		
		/**
		 * 수정 후 데이터 다시 SELECT
		 */
		List<HashMap> newBillingSt = searchAssetBillingSt(confIdMap);
		
		/**
		 * 다시 SELECT 된 데이터로 이력 등록모듈 호출 (시퀀스 생성이 데이터가 있어야 함)
		 */
		assetHistoryService.insertBsStHistory(parentConfId, newBillingSt.get(0), "", userId);
	}
	
}
