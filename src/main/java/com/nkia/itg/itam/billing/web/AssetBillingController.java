package com.nkia.itg.itam.billing.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.billing.service.AssetBillingService;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * 자산 과금 등록/수정
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2017. 04. 03. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class AssetBillingController {
	
	private static final Logger logger = LoggerFactory.getLogger(AssetBillingController.class);
	
	@Resource(name = "assetBillingService")
	private AssetBillingService assetBillingService;
	
	@Resource(name = "commonCodeService")
	private CommonCodeService commonCodeService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 과금 대상 자산리스트 및 원장화면 메인 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/billing/goAssetBillingManagerMain.do")
	public String goAssetBillingManagerMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String center_asset_yn = StringUtil.parseString(request.getParameter("center_asset_yn"));
		paramMap.put("center_asset_yn", center_asset_yn);
		String forwarPage = "/itg/itam/billing/assetBillingManagerMain.nvf";
		return forwarPage;
	}
	
	/**
	 * 과금 대상 자산리스트 조회 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/billing/goAssetBillingListPage.do")
	public String goAssetBillingListPage(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String center_asset_yn = StringUtil.parseString(request.getParameter("center_asset_yn"));
		String view_type = StringUtil.parseString(request.getParameter("view_type"));
		paramMap.put("center_asset_yn", center_asset_yn);
		paramMap.put("view_type", view_type);
		String forwarPage = "/itg/itam/billing/assetBillingList.nvf";
		return forwarPage;
	}
	
	/**
	 * 과금 대상 자산 분류체계 트리
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/billing/searchClassTree.do")
	public @ResponseBody ResultVO searchClassTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetBillingService.searchClassTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 과금 대상 자산 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchAssetBillingList.do")
	public @ResponseBody ResultVO searchAssetBillingList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = assetBillingService.searchAssetBillingList(paramMap);		
			totalCount = assetBillingService.searchAssetBillingListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 과금 상세 화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/billing/goAssetBillingDetailViewer.do")
	public String goAssetBillingDetailViewer(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		
		UserVO userVO		= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String user_id			= userVO.getUser_id();
		String conf_id 			= StringUtil.parseString(request.getParameter("conf_id"));
		String asset_id 		= StringUtil.parseString(request.getParameter("asset_id"));
		String class_id 		= StringUtil.parseString(request.getParameter("class_id"));
		String class_type 	= StringUtil.parseString(request.getParameter("class_type"));
		String view_type 		= StringUtil.parseString(request.getParameter("view_type"));
		String entity_class_id 		= StringUtil.parseString(request.getParameter("entity_class_id"));
		String tangible_asset_yn 	= StringUtil.parseString(request.getParameter("tangible_asset_yn"));
		String logical_yn 	= StringUtil.parseString(request.getParameter("logical_yn"));
		String physi_conf_id 	= StringUtil.parseString(request.getParameter("physi_conf_id"));
		String regist_type 	= StringUtil.parseString(request.getParameter("regist_type"));
		String pop_up_yn 		= StringUtil.parseString(request.getParameter("pop_up_yn"));
		String center_asset_yn = StringUtil.parseString(request.getParameter("center_asset_yn"));
		
		// 페이지 명 생성을 위한 처리 (센터내외구분)
		String convertCenterAssetYn = "";
		if(!"".equals(center_asset_yn)) {
			if("Y".equals(center_asset_yn)) {
				convertCenterAssetYn = "_IN";
			} else {
				convertCenterAssetYn = "_OUT";
			}
		}
		
		// 페이지 명 생성을 위한 처리 (분류체계타입)
		String pageClassType = "";
		if(!"".equals(class_type)) {
			if("SL".equals(class_type)) {
				pageClassType = "SV";
			} else {
				pageClassType = class_type;
			}
		}
		
		paramMap.put("user_id"			, user_id);
		paramMap.put("conf_id"			, conf_id);
		paramMap.put("asset_id"		, asset_id);
		paramMap.put("class_id"		, class_id);
		paramMap.put("entity_class_id", entity_class_id);
		paramMap.put("class_type"	, class_type);
		paramMap.put("entity_id"		, entity_class_id);
		paramMap.put("view_type"		, view_type);
		paramMap.put("pop_up_yn"		, pop_up_yn);
		paramMap.put("logical_yn"		, logical_yn);
		paramMap.put("tangible_asset_yn"		, tangible_asset_yn);
		paramMap.put("center_asset_yn"		, center_asset_yn);
		paramMap.put("physi_conf_id"		, physi_conf_id);
		paramMap.put("regist_type"		, regist_type);
		
		if(view_type.equals("pop_view") && pop_up_yn.equals("Y")) {
			paramMap.put("view_type", "pop_up_view");
		}
		
		return "/itg/itam/billing/assetBsDetailViewer_" + pageClassType + convertCenterAssetYn + ".nvf";
	}
	
	/**
	 * 자산 마스터 정보 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchAssetBillingMaster.do")
	public @ResponseBody ResultVO searchAssetBillingMaster(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchAssetBillingMaster(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 서버 과금정보 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchAssetBillingSv.do")
	public @ResponseBody ResultVO searchAssetBillingSv(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchAssetBillingSv(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 메모리 증설량 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchBsInsMemory.do")
	public @ResponseBody ResultVO searchBsInsMemory(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchBsInsMemory(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * CPU Core 증설량 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchBsInsCpu.do")
	public @ResponseBody ResultVO searchBsInsCpu(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchBsInsCpu(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 스토리지 증설량 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchBsInsStorage.do")
	public @ResponseBody ResultVO searchBsInsStorage(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchBsInsStorage(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용처 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchBsCustRate.do")
	public @ResponseBody ResultVO searchBsCustRate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchBsCustRate(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용처 등록용 부서 트리
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/billing/searchDeptTree.do")
	public @ResponseBody ResultVO searchDeptTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = assetBillingService.searchDeptTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeMapToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용처 등록용 업무 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchService.do")
	public @ResponseBody ResultVO searchService(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchService(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서버 과금정보 (센터 내) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingSvIn.do")
	public @ResponseBody ResultVO changeAssetBillingSvIn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingSvIn(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서버 과금정보 (센터 외) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingSvOut.do")
	public @ResponseBody ResultVO changeAssetBillingSvOut(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingSvOut(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 NW 과금정보 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchAssetBillingNw.do")
	public @ResponseBody ResultVO searchAssetBillingNw(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchAssetBillingNw(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * NW 과금정보 (센터 내) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingNwIn.do")
	public @ResponseBody ResultVO changeAssetBillingNwIn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingNwIn(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * NW 과금정보 (센터 외) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingNwOut.do")
	public @ResponseBody ResultVO changeAssetBillingNwOut(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingNwOut(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 NW 모델 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchAssetBillingNwModel.do")
	public @ResponseBody ResultVO searchAssetBillingNwModel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchAssetBillingNwModel(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 NW 서비스중단일 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchBsSvcStop.do")
	public @ResponseBody ResultVO searchBsSvcStop(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchBsSvcStop(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 LN 과금정보 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchAssetBillingLn.do")
	public @ResponseBody ResultVO searchAssetBillingLn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchAssetBillingLn(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * LN 과금정보 (센터 내) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingLnIn.do")
	public @ResponseBody ResultVO changeAssetBillingLnIn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingLnIn(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * LN 과금정보 (센터 외) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingLnOut.do")
	public @ResponseBody ResultVO changeAssetBillingLnOut(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingLnOut(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 자산 Storage 과금정보 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchAssetBillingSt.do")
	public @ResponseBody ResultVO searchAssetBillingSt(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchAssetBillingSt(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * Storage 과금정보 (센터 내) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingStIn.do")
	public @ResponseBody ResultVO changeAssetBillingStIn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingStIn(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * Storage 과금정보 (센터 외) 등록
	 * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/billing/changeAssetBillingStOut.do")
	public @ResponseBody ResultVO changeAssetBillingStOut(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("user_id", userVO.getUser_id());
			}
			assetBillingService.changeAssetBillingStOut(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용처(공통비) 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/billing/searchBsCmprc.do")
	public @ResponseBody ResultVO searchBsCmprc(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = assetBillingService.searchBsCmprc(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 엑셀 다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/itam/billing/searchExcel.do")
	public @ResponseBody ResultVO searchExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			ArrayList classType = new ArrayList();
			classType = (ArrayList)param.get("class_type_arr");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						
						if( !"ASSET_ID".equalsIgnoreCase(column)
							&& !"CONF_ID".equalsIgnoreCase(column)
							&& !"CONF_NM".equalsIgnoreCase(column)
							&& !"PATH_CLASS_NM".equalsIgnoreCase(column)
						) {
							String columnNm = ","; 
							
							if(htmlType.equals("SELECT") || htmlType.equals("POPUP")){
								columnNm += ( column + "_NM" );
							}else{
								columnNm += column;
							}
							
							columnList += columnNm;
						}
					}
					
					param.put("col_id", columnList);
					
					String typeName = (String)classType.get(k);
					param.put("class_type", typeName);
					
					List tempArrayList = assetBillingService.searchExcel(param);
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = "ASSET_ID,CONF_ID,CONF_NM,PATH_CLASS_NM" + columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	/**
	 * 리소스로 자산 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/billing/searchExcelByResource.do")
	public @ResponseBody ResultVO searchExcelByResource(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = assetBillingService.searchExcelByResource(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
}
