/*
 * 2019.10.22 LedService.java
 * 
 * LED manager
 * PARK JUN HO
 */
package com.nkia.itg.itam.lowformat.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;

public interface LowFormatService {

	public List searchHostInfo(ModelMap paramMap) throws NkiaException ;

	public int searchHostInfoCount(ModelMap paramMap) throws NkiaException ;

}
