/*
 * 2019.10.22 Monitormanagementcontroller.java
 * 
 * 관제 관리.
 * OH MIN SEok
 */
package com.nkia.itg.itam.lowformat.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.lowformat.service.LowFormatService;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class LowFormatController {

	@Resource(name = "lowFormatService")
	private LowFormatService lowFormatService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;	

	@Resource(name = "processCommonExecutor")
	private PostWorkExecutor processCommonExecutor;
	
	/**
	 * 호스트명 조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/itam/lowformat/searchHostInfo.do")
	public @ResponseBody ResultVO searchHostInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String loginCustId = userVO.getCust_id();
				//String assetViewAuth = assetStatisticsService.seelctAssetViewAuth(loginCustId);
				paramMap.put("loginCustId", loginCustId);
			//	paramMap.put("assetViewAuth", assetViewAuth);
				paramMap.put("user_id", userVO.getUser_id());
			}
			
			List resultList = lowFormatService.searchHostInfo(paramMap);		
			totalCount = lowFormatService.searchHostInfoCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}		
}
