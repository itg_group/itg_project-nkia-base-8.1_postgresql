/*
 * 2019.10.22 LedDAO.java
 * 
 * LED manager
 * PARK JUN HO
 */
package com.nkia.itg.itam.lowformat.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("lowFormatDAO")
public class LowFormatDAO  extends EgovComAbstractDAO{

	public List searchHostInfo(ModelMap paramMap) throws NkiaException{
		//List responseList = list("AssetStatisticsDAO.searchAssetList", paramMap);
		List resultList = list("monitorManagementDAO.searchHostInfo", paramMap);
		List resultList2 = new ArrayList();
		if(paramMap.containsKey("rnum")){
			Map resultMap = new HashMap();
			
			//List resultList = new List();
			int size = resultList.size();
			for(int i=0; i<size; i++) {
				resultMap = (HashMap)resultList.get(i);
				resultMap.put("RNUM", (String)paramMap.get("rnum")+"-"+(String)resultMap.get("RNUM"));
				resultList2.add(resultMap);
			}
			return resultList2;
		}else{
			return resultList;
		}
	}

	public int searchHostInfoCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("monitorManagementDAO.searchHostInfoCount", paramMap);
	}
}
