/*
 * 2019.10.22 LedServiceImpl.java
 * 
 * LED manager
 * PARK JUN HO
 */
package com.nkia.itg.itam.lowformat.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.itam.lowformat.dao.LowFormatDAO;
import com.nkia.itg.itam.lowformat.service.LowFormatService;


@Service("lowFormatService")
public class LowFormatServiceImpl implements LowFormatService {

	private static final Logger logger = LoggerFactory.getLogger(LowFormatServiceImpl.class);

	@Resource(name = "lowFormatDAO")
	public LowFormatDAO lowFormatDAO;
	
	@Override	
	public List searchHostInfo(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return lowFormatDAO.searchHostInfo(paramMap);
	}
	@Override
	public int searchHostInfoCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return lowFormatDAO.searchHostInfoCount(paramMap);
	}	

	
}
