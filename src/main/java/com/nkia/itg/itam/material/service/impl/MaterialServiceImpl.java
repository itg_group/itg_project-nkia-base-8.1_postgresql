/*
 * @(#)maintReqServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.material.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.material.dao.MaterialDAO;
import com.nkia.itg.itam.material.service.MaterialService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("materialService")
public class MaterialServiceImpl implements MaterialService{
	
	//private static final Logger logger = LoggerFactory.getLogger(MaterialServiceImpl.class);
	
	@Resource(name = "materialDAO")
	public MaterialDAO materialDAO;

	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return materialDAO.searchClassTree(paramMap);
	}	
	
	public List searchUserList(ModelMap paramMap) throws NkiaException {
			
			List resultList = null;
			resultList = materialDAO.searchUserList(paramMap); 
			return resultList;
		}
	
	public int searchUserListCount(ModelMap paramMap) throws NkiaException {
		return materialDAO.searchUserListCount(paramMap);
	}
	
	@Override
	public void insertMaterialData(ModelMap paramMap) throws NkiaException {
	
			materialDAO.insertMaterialData(paramMap);
	
		
	}
	
	@Override
	public void updateMaterialData(ModelMap paramMap) throws NkiaException {	
			materialDAO.updateMaterialData(paramMap);

	}
	@Override
	public void deleteMaterialData(ModelMap paramMap) throws NkiaException {	
			materialDAO.deleteMaterialData(paramMap);

	}

	@Override
	public int materialPreTot(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return materialDAO.materialPreTot(paramMap);
	}

	@Override
	public int materialCurOrd(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return materialDAO.materialCurOrd(paramMap);
	}

	@Override
	public int materialCurUse(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return materialDAO.materialCurUse(paramMap);
	}

	@Override
	public int existAtchId(ModelMap paramMap) throws NkiaException {
		return materialDAO.existAtchId(paramMap);
	}

	@Override
	public void initAtchId(ModelMap paramMap) throws NkiaException {
		materialDAO.initAtchId(paramMap);
		
	}

	@Override
	public List materialDisplayList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = materialDAO.materialDisplayList(paramMap); 
		return resultList;
	}

	@Override
	public int materialDisplayListCount(ModelMap paramMap) throws NkiaException {
		return materialDAO.materialDisplayListCount(paramMap);
	}

	@Override
	public List searchUserList(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = materialDAO.searchUserList(paramMap); 
		return resultList;
	}

	@Override
	public List materialDisplayList(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = materialDAO.materialDisplayList(paramMap); 
		return resultList;
	}
	@Override
	public List materialDisplayListDist(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = materialDAO.materialDisplayListDist(paramMap); 
		return resultList;
	}
	
	@Override
	public List getStandardList(ModelMap paramMap) throws NkiaException {
		return materialDAO.getStandardList(paramMap);
	}	
	@Override
	public int getStandardListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return materialDAO.getStandardListCount(paramMap);
	}
	
	
}
