/*
 * @(#)AssetStatisticsDAO.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.material.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;
@Repository("materialDAO")
//@Repository("assetStatisticsDAO")

public class MaterialDAO  extends EgovComAbstractDAO{

	
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		return list("MaterialDAO.searchClassTree", paramMap);
		//return list("AssetStatisticsDAO.searchClassTree", paramMap);
		
	}	
		
		
	public int searchUserListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("MaterialDAO.searchUserListCount", paramMap);
	}
	
	public int materialPreTot(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("MaterialDAO.materialPreTot", paramMap);
	}
	
	public int materialCurOrd(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("MaterialDAO.materialCurOrd", paramMap);
	}
	
	public int materialCurUse(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("MaterialDAO.materialCurUse", paramMap);
	}
	
	public List searchUserList(ModelMap paramMap) throws NkiaException {
		return list("MaterialDAO.searchUserList", paramMap);
	}
	
	public void insertMaterialData(Map paramMap) throws NkiaException {
		this.update("MaterialDAO.insertMaterialData", paramMap);
	}
	
	public void updateChildCodeData(Map paramMap) throws NkiaException {
		this.update("MaterialDAO.updateChildCodeData", paramMap);
	}

	public void updateMaterialData(Map paramMap) throws NkiaException {
		this.update("MaterialDAO.updateMaterialData", paramMap);
	}
	
	public void deleteMaterialData(Map paramMap) throws NkiaException {
		this.update("MaterialDAO.deleteMaterialData", paramMap);
	}
	
	public int existAtchId(ModelMap paramMap) throws NkiaException {
	return (Integer) selectByPk("MaterialDAO.existAtchId", paramMap);
	}
	
	public void initAtchId(ModelMap paramMap) throws NkiaException {
		this.update("MaterialDAO.initAtchId", paramMap);
	}	
	
	
	public List materialDisplayList(ModelMap paramMap) throws NkiaException {
	return list("MaterialDAO.materialDisplayList", paramMap);
}

	public int materialDisplayListCount(ModelMap paramMap) throws NkiaException {
	return (Integer) selectByPk("MaterialDAO.materialDisplayListCount", paramMap);
}
	
	
	public List searchUserList(Map paramMap) throws NkiaException {
		return list("MaterialDAO.searchUserList", paramMap);
	}
	public List materialDisplayList(Map paramMap) throws NkiaException {
	return list("MaterialDAO.materialDisplayList", paramMap);
	}
		
	public List materialDisplayListDist(Map paramMap) throws NkiaException {
		return list("MaterialDAO.materialDisplayListDist", paramMap);
	}
	
	public List getStandardList(Map paramMap) throws NkiaException{
		return list("MaterialDAO.getStandardList", paramMap);
	}
	public int getStandardListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("MaterialDAO.getStandardListCount", paramMap);
	}
	
}





	
	