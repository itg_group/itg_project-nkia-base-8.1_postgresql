/*
 * @(#)MaintReqController.java              2013. 5. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.material.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.system.dataset.web.DatasetController;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.material.service.MaterialService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class MaterialController {
	
	private static final Logger logger = LoggerFactory.getLogger(DatasetController.class);
	
	@Resource(name = "materialService")
	private MaterialService materialService;

	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;	


	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 인프라 재자관리 - 자재 등록 페이지 제작.
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */		
	@RequestMapping(value="/itg/itam/material/goMaterialManagement.do")
	public String searchMaterialContReqAsset(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/material/materialManagement.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/itam/material/goMaterialDisplay.do")
	public String searchMaterialDisplay(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/itam/material/materialDisplay.nvf";
		return forwarPage;
	}
		
	
		
	@RequestMapping(value="/itg/itam/material/searchClassTree.do")
	public @ResponseBody ResultVO searchClassTree(@RequestBody ModelMap paramMap) throws NkiaException {		
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = null;
		
		try {
			if (paramMap.get("is_all_tree") != null && !paramMap.get("is_all_tree").equals("Y")) {
				loginUserId = userVO.getUser_id();
				paramMap.addAttribute("login_user_id", loginUserId);
			} else {
				paramMap.addAttribute("login_user_id", loginUserId);
			}
			List resultList = materialService.searchClassTree(paramMap);
			
			logger.debug(">>>>> "+resultList);
			
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	
	/**
	 * 사용자 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/material/searchUserList.do")
	public @ResponseBody ResultVO searchUserList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = materialService.searchUserListCount(paramMap);
			List resultList = materialService.searchUserList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 숫자 출력(이월 양, 현재수량, 수량, 등등)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/material/materialCntList.do")
	public @ResponseBody ResultVO materialCntList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();			
			int tot_pre_res_cnt = 0;
			int tot_order_cnt = 0;
			int tot_use_cnt = 0;	
			 tot_pre_res_cnt = materialService.materialPreTot(paramMap);
			 tot_order_cnt = materialService.materialCurOrd(paramMap);
			 tot_use_cnt = materialService.materialCurUse(paramMap);			
			HashMap resultMap = new HashMap();
			resultMap.put("tot_pre_res_cnt", tot_pre_res_cnt);
			resultMap.put("tot_order_cnt", tot_order_cnt);
			resultMap.put("tot_use_cnt", tot_use_cnt);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	
	
	
	
	
	
	
	@RequestMapping(value="/itg/itam/material/insertMaterialData.do")
	public @ResponseBody ResultVO insertMaterialData(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	
	    	
	    	materialService.insertMaterialData(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 공통 코드 수정 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/material/updateMaterialData.do")
	public @ResponseBody ResultVO updateMaterialData(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	int atch_cnt = materialService.existAtchId(paramMap);
	    	
	    	if(atch_cnt == 0){
	    		materialService.initAtchId(paramMap);
	    	}{	    	
	    		materialService.updateMaterialData(paramMap);
	    	}
	    	
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 공통 코드 삭제 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/material/deleteMaterialData.do")
	public @ResponseBody ResultVO deleteMaterialData(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	materialService.deleteMaterialData(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/material/materialExcelDown.do")
	public @ResponseBody ResultVO materialExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();

		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		
		Map excelParam = (Map) paramMap.get("excelParam");
		Map temp = (Map) paramMap.get("param");
		
		try{
			//page=1, start=0, limit=10 있는 경우, 
			resultList = materialService.searchUserList(temp);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);	
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
		
	}
	
	

	/**
	 * 기반자재 display 월 현황 자료
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/material/materialDisplayList.do")
	public @ResponseBody ResultVO materialDisplayList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = materialService.materialDisplayListCount(paramMap);
			List resultList = materialService.materialDisplayList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/itam/material/materialDisplayExcelDown.do")
	public @ResponseBody ResultVO materialDisplayExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();

		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		
		Map excelParam = (Map) paramMap.get("excelParam");
		Map temp = (Map) paramMap.get("param");
		
		
		try{
			//page=1, start=0, limit=10 있는 경우, 
						
			
			resultList = materialService.materialDisplayList(temp);
			List tabList = materialService.materialDisplayListDist(temp);

			excelMap = excelMaker.makeExcelFileTab(excelParam, resultList, tabList);	
			
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
		
	}
	
	@RequestMapping(value="/itg/itam/material/getStandardList.do")
	public @ResponseBody ResultVO getStandardList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("user_id", userVO.getUser_id());
	    	}
	    	
			gridVO.setRows(materialService.getStandardList(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 숫자 출력(이월 양, 현재수량, 수량, 등등)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/itam/material/getStandardListCount.do")
	public @ResponseBody ResultVO getStandardListCount(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();			
			int standard_cnt = 0;			
			standard_cnt = materialService.getStandardListCount(paramMap);						
			HashMap resultMap = new HashMap();
			resultMap.put("standard_cnt", standard_cnt);
			
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
}
