/*
 * @(#)maintReqService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.material.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface MaterialService {

	public List searchClassTree(ModelMap paramMap) throws NkiaException;
	
	public List searchUserList(ModelMap paramMap) throws NkiaException ;
	public int searchUserListCount(ModelMap paramMap) throws NkiaException ;
	
	public int materialPreTot(ModelMap paramMap) throws NkiaException ;
	public int materialCurOrd(ModelMap paramMap) throws NkiaException ;
	public int materialCurUse(ModelMap paramMap) throws NkiaException ;
	
	
	
	public void insertMaterialData(ModelMap paramMap) throws NkiaException ;
	
	public void updateMaterialData(ModelMap paramMap) throws NkiaException ;
	
	public void deleteMaterialData(ModelMap paramMap) throws NkiaException ;
	
	
	public int existAtchId(ModelMap paramMap) throws NkiaException ;		
	public void initAtchId(ModelMap paramMap) throws NkiaException ;
	
	
	public List materialDisplayList(ModelMap paramMap) throws NkiaException ;
	public int materialDisplayListCount(ModelMap paramMap) throws NkiaException ;
	
	public List searchUserList(Map paramMap) throws NkiaException ;
	public List materialDisplayList(Map paramMap) throws NkiaException ;
	public List materialDisplayListDist(Map paramMap) throws NkiaException ;
	
	
	public List getStandardList(ModelMap paramMap) throws NkiaException ;
	public int getStandardListCount(ModelMap paramMap) throws NkiaException ;
}
