package com.nkia.itg.itam.automation.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import net.sf.json.JSONArray;

/**
 * 자산원장 화면 자동화
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2016. 11. 11. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class AssetAutomationController {
	
	private static final Logger logger = LoggerFactory.getLogger(AssetAutomationController.class);
	
	@Resource(name = "assetAutomationService")
	private AssetAutomationService assetAutomationService;
	
	@Resource(name = "commonCodeService")
	private CommonCodeService commonCodeService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 자산리스트 및 원장화면 메인 페이지
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/itam/automation/goAssetManagerMain.do")
	public String operManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/itam/automation/assetManagerMain.nvf";
		String view_type = StringUtil.parseString(request.getParameter("view_type"));
		paramMap.put("view_type", view_type);
		String mng_type = StringUtil.parseString(request.getParameter("mng_type"));
		paramMap.put("mng_type", mng_type);
		String tree_type = StringUtil.parseString(request.getParameter("tree_type"));
		paramMap.put("tree_type", tree_type);
		String conf_kind_cd = StringUtil.parseString(request.getParameter("conf_kind_cd"));
		paramMap.put("conf_kind_cd", conf_kind_cd);
		return forwarPage;
	}
	
	/**
	 * 분류별 설정정보를 가져와서 화면에 배치될 개체를 JSON 형태로 세팅한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/automation/goAssetDetailViewer.do")
	public String getRegistDetailJson(HttpServletRequest request,ModelMap paramMap) throws NkiaException {
		
		UserVO userVO		= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String user_id			= userVO.getUser_id();
		String sr_id			= StringUtil.parseString(request.getParameter("sr_id"));	//요청 팝업일 때 사용되는 파라미터
		String conf_id 			= StringUtil.parseString(request.getParameter("conf_id"));
		String asset_id 		= StringUtil.parseString(request.getParameter("asset_id"));
		String class_id 		= StringUtil.parseString(request.getParameter("class_id"));
		String class_type 		= StringUtil.parseString(request.getParameter("class_type"));
		String view_type 		= StringUtil.parseString(request.getParameter("view_type"));	//조회(view)인지, 관리(regist)인지 구분
		String pop_up_yn 		= StringUtil.parseString(request.getParameter("pop_up_yn"));	//팝업여부
		String asset_state = StringUtil.parseString(request.getParameter("asset_state"));	//운영상태, 디폴트 값이 화면에 있다면 원장으로 넘겨준다.
		
		/* ※  mng_type 설명 : 자산등록인지, 구성등록인지, 자산+구성등록인지 구분하는 구분자
		 	 mng_type : ASSET(자원), mng_type : CONF(구성), mng_type : ASSETCONF(자산구성)
		 */
		String mng_type = StringUtil.parseString(request.getParameter("mng_type"));	
		String conf_type = StringUtil.parseString(request.getParameter("conf_type"));	//구성용도
		String oper_state = StringUtil.parseString(request.getParameter("oper_state"));	//구성상태, 디폴트 값이 화면에 있다면 원장으로 넘겨준다.
		
		/* ※ entity_class_id 설명 : 자산정보과 구성정보의 속성을 불러 낼 때 사용하는 파라미터
			mng_type(ASSET) : class_id가  entity_class_id에 들어옴
			mng_type(CONF) : conf_type이  entity_class_id에 들어옴
			mng_type(ASSETCONF) : class_id가  entity_class_id에 들어옴. 자산구성 속성을 한번에 등록 수정하는 경우에는 entity_conf_type_id 변수에 conf_type을 넣어준다.
		*/
		String entity_root_id 		= StringUtil.parseString(request.getParameter("entity_root_id"));	// mngType이 ASSET이거나 CONF일 때, 분류체계별 속성을 조회하기 위해 사용하는 파라미터. class_id나 conf_type이 들어있음			
		String entity_class_id 		= StringUtil.parseString(request.getParameter("entity_class_id"));	// mngType이 ASSETCONF일 때, class_id를 담고 있음		
		String entity_conf_type_id 		= StringUtil.parseString(request.getParameter("entity_conf_type_id"));	// mngType이 ASSETCONF일 때, conf_id를 담고 있음		
		
		String conf_kind_cd 		= StringUtil.parseString(request.getParameter("conf_kind_cd"));	// 구성종류
		//CMDB 과제에서 사용하지 않는 변수
//		String tangible_asset_yn 	= StringUtil.parseString(request.getParameter("tangible_asset_yn"));	//물리자산여부
//		String regist_type 	= StringUtil.parseString(request.getParameter("regist_type"));	//PHYSI
//		String logical_yn 	= StringUtil.parseString(request.getParameter("logical_yn"));
//		String center_asset_yn = StringUtil.parseString(request.getParameter("center_asset_yn"));
//		String physi_conf_id 	= StringUtil.parseString(request.getParameter("physi_conf_id"));	//물리구성ID, 미사용
		
		paramMap.put("user_id"				, user_id);
		paramMap.put("sr_id"				, sr_id);
		paramMap.put("conf_id"				, conf_id);
		paramMap.put("asset_id"				, asset_id);
		paramMap.put("class_id"				, class_id);
		paramMap.put("class_type"			, class_type);
		paramMap.put("entity_id"			, entity_root_id);
		paramMap.put("view_type"			, view_type);
		paramMap.put("pop_up_yn"			, pop_up_yn);
		paramMap.put("asset_state"			, asset_state);

		paramMap.put("mng_type"				, mng_type);	//mng_type : ASSET(자산조회), mng_type : CONF(구성조회),  mng_type : ASSETCONF(자산구성조회)
		paramMap.put("conf_type"			, conf_type);
		paramMap.put("oper_state"			, oper_state);
		
		paramMap.put("entity_root_id"		, entity_root_id);	//mng_type이  ASSET 또는 CONF일 때 class_id나 conf_type을 넣어줌.
		paramMap.put("entity_class_id"		, entity_class_id);	//mng_type이 ASSETCONF일 때, class_id
		paramMap.put("entity_conf_type_id"	, entity_conf_type_id);	//mng_type이 ASSETCONF일 때, class_id
		paramMap.put("conf_kind_cd"			, conf_kind_cd);	//구성분류

		//CMDB 과제에서 사용하지 않는 변수
//		paramMap.put("tangible_asset_yn"	, tangible_asset_yn);
//		paramMap.put("regist_type"		, regist_type);
//		paramMap.put("logical_yn"		, logical_yn);
//		paramMap.put("center_asset_yn"		, center_asset_yn);
//		paramMap.put("physi_conf_id"		, physi_conf_id);
		
		//구성용도 명칭 호출
		HashMap confInfo = assetAutomationService.searchConfTypeNm(paramMap);
		if(confInfo != null) {
			if((confInfo.get("CONF_TYPE_NM") != null) && (!"".equals(StringUtil.parseString(confInfo.get("CONF_TYPE_NM"))))) paramMap.put("conf_type_nm", confInfo.get("CONF_TYPE_NM"));			
		}
		//자산분류체계 명칭 호출
		HashMap classInfo = assetAutomationService.searchClassNm(paramMap);
		if(classInfo != null) {
			if((classInfo.get("CLASS_NM") != null) && (!"".equals(StringUtil.parseString(classInfo.get("CLASS_NM"))))) paramMap.put("class_id_nm", classInfo.get("CLASS_NM"));		
		}
		assetAutomationService.selectClassEntitySetupInfo(paramMap);	//분류체계별 권한 및 속성을 조회
		
		if(view_type.equals("pop_view") && pop_up_yn.equals("Y")) {
			paramMap.put("view_type", "pop_up_view");
		}
		
		return "/itg/itam/automation/assetDetailViewer.nvf";
	}
	
	/**
	 * 컴포넌트 중 DB로 컬럼이 관리되는 그리드의 컬럼정보를 가지고 온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/automation/selectCompDataColumn.do")
	public @ResponseBody ResultVO selectCompDataColumn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			JSONArray jsonArray = assetAutomationService.selectCompDataColumn(paramMap);
			resultVO.setResultString(jsonArray.toString());
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 컴포넌트 이력 리스트를 가지고 온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/automation/selectCompDetailModList.do")
	public @ResponseBody ResultVO selectCompDetailModList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			commonCodeService.getSearchListNotIn(paramMap,"modSearchKey","modSearchValue","MOD_SEARCH_KEY","CHG_COLUMN_ID,CHG_COLUMN_NM");
			List rows = assetAutomationService.selectCompDetailModList(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 프로세스 전체요청이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/automation/searchProcMasterHist.do")
	public @ResponseBody ResultVO searchProcMasterHist(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List rows = assetAutomationService.searchProcMasterHist(paramMap);
			gridVO.setRows(rows);
			resultVO.setGridVO(gridVO);
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산 정보 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/automation/selectAssetInfo.do")
	public @ResponseBody ResultVO selectAssetInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = (HashMap) assetAutomationService.selectAssetInfo(paramMap);
			resultVO.setResultMap(resultMap);
			
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * EMS 정보 업데이트
	 * 
     * @param paramMap
	 * @return
	 */		
	@RequestMapping(value="/itg/itam/automation/updateEmsInfo.do")
	public @ResponseBody ResultVO updateEmsInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
			
			assetAutomationService.updateEmsInfo(paramMap);
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산정보에 등록된 EMS ID 건수 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/automation/selectEmsIdCount.do")
	public @ResponseBody ResultVO selectEmsIdCount(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			int resultCnt = assetAutomationService.selectEmsIdCount(paramMap);
			
			if(resultCnt == 0) {
				resultVO.setSuccess(true);
			} else {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014", new String[]{"EMS ID"}));
				resultVO.setSuccess(false);
			}
		}catch (Exception e){
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
