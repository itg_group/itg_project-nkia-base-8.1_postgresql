package com.nkia.itg.itam.automation.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.automation.core.AssetAutomationComponents;
import com.nkia.itg.itam.automation.dao.AssetAutomationDAO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.itam.automation.ui.WebixUIHandler;
import com.nkia.itg.itam.introduction.service.TempAssetService;
import com.nkia.itg.itam.statistics.dao.AssetStatisticsDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 자산원장 화면 자동화
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2016. 11. 11. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Service("assetAutomationService")
public class AssetAutomationServiceImpl implements AssetAutomationService{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetAutomationServiceImpl.class);
	
	@Resource(name = "webixUIHandler")
	private WebixUIHandler webixUIHandler;
	
	@Resource(name = "assetAutomationComponents")
	private AssetAutomationComponents assetAutomationComponents;
	
	@Resource(name = "assetAutomationDAO")
	private AssetAutomationDAO assetAutomationDAO;
	
	@Resource(name = "assetStatisticsDAO")
	public AssetStatisticsDAO assetStatisticsDAO;
	
	@Resource(name = "tempAssetService")
	private TempAssetService tempAssetService;
	
	@Resource(name = "jsonUtil")
	private JsonUtil jsonUtil;  
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	@Override
	public void selectClassEntitySetupInfo(HashMap paramMap) throws NkiaException {
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String userId = (String)userVO.getUser_id();
		// hmsong cmdb 개선 작업 : 화면에서 가져온다(메뉴 ID에 세팅. 자산관리인지, 구성관리인지, 자산구성관리인지를 구분하는 구분자)
		String mngType = StringUtil.parseString(paramMap.get("mng_type"));	
		// 1 레벨은 분류체계이므로 자산원장 구성요소들을 가지고 오기 위해 2레벨 부터 SELECT 하도록 parameter 세팅
		paramMap.put("level", "2");
		
		
		List<HashMap> entityList = new ArrayList<HashMap>();
		
		Integer authCnt = 0;
				
		if("ASSETCONF".equals(mngType)) {
			/*
			 * mngType이 ASSETCONF인 경우(자산+구성 등록) 자산 속성과 구성 속성을 UNION ALL하여 속성을 조회한다.
			 * 또한, class_id로 매핑된 분류체계별 속성과 conf_type으로 매핑된 속성 모두를 조회해야 하므로 아래와 같이 파라미터를 전달한다.
			 */
			entityList = assetAutomationDAO.selectClassConfUnionEntityInfo(paramMap);// 현재 분류체계의 자산원장 구성요소들을 모두 가져온다	
			authCnt = assetAutomationDAO.selectClassConfUnionUpdateAuthCnt(paramMap);// 수정권한의 수를 가져온다
		} else {
			entityList = assetAutomationDAO.selectClassEntityInfo(paramMap);// 현재 분류체계의 자산원장 구성요소들을 모두 가져온다
			authCnt = assetAutomationDAO.selectUpdateAuthCnt(paramMap);// 수정권한의 수를 가져온다
		}	
		
		/*
		// EMS 연계 일 경우 EMS_ID get - CMDB 개선과 관련없음
		String ems_id = assetAutomationDAO.selectEmsIds(paramMap);
		// DCA 연계 일 경우 DCA_ID get - CMDB 개선과 관련없음
		String dca_id = assetAutomationDAO.selectDcaIds(paramMap);
		*/
		
		paramMap.put("confId", paramMap.get("conf_id"));
		paramMap.put("assetId", paramMap.get("asset_id"));		
		
		// 자산 등록정보 조회
		HashMap assetInfo = assetAutomationDAO.selectAssetInfo(paramMap);
		
		JSONArray tabs = new JSONArray();
		JSONObject tabBars = new JSONObject();
		
		JSONArray formArray = new JSONArray();
		JSONArray dataArray = new JSONArray();
		
		JSONArray tabBarItems = new JSONArray();
		JSONArray tabInnerItems = null;
		
		if(entityList != null && entityList.size()>0){
			
			for(int i=0;i<entityList.size();i++){				
				
				HashMap entityMap = entityList.get(i);
				
				String entity_type = (String) entityMap.get("ENTITY_TYPE");
				String entity_id = (String) entityMap.get("ENTITY_ID");

				webixUIHandler.paramSync(paramMap,entityMap);
				
				entityMap.put("entity_id", entity_id);
				/*
				entityMap.put("ems_id", ems_id);  //- CMDB 개선과 관련없음
				entityMap.put("dca_id", dca_id);  //- CMDB 개선과 관련없음
				*/
				//ENTITY_ROOT_ID : 선택한 자산 OR 구성의 CLASS_ID 또는 CONF_TYPE
				entityMap.put("entity_root_id", (String) entityMap.get("ENTITY_ROOT_ID"));
				//entity_mng_type 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
				entityMap.put("entity_mng_type", (String) entityMap.get("ENTITY_MNG_TYPE"));
				
				if(assetInfo != null) {
					entityMap.put("ins_user_id_nm", StringUtil.parseString(assetInfo.get("INS_USER_ID_NM")));
					entityMap.put("ins_dt", StringUtil.parseString(assetInfo.get("INS_DT")));
				}
				
				// 탭의 유형이 '그룹' 일 경우 (일반 탭)
				if(entity_type.equalsIgnoreCase("GROUP")){
					
					// 1개 탭 페이지 내부에 구성할 아이템들을 담아 줄 JSON배열 객체 생성
					tabInnerItems = new JSONArray();
					
					// 탭 하위에 구성되어 있는 원장 구성요소들을 모두 가져온다
					List<HashMap> childEntityList = assetAutomationDAO.selectClassEntityInfo(entityMap);
					
					for(int j=0;j<childEntityList.size();j++){
						
						HashMap childEntityMap = childEntityList.get(j);
						
						String childEntityType = (String) childEntityMap.get("ENTITY_TYPE");
						String childEntityId = (String) childEntityMap.get("ENTITY_ID");
						
						// request 에서 넘어온 정보를 다시 세팅해준다.
						webixUIHandler.paramSync(paramMap,childEntityMap);
						
						childEntityMap.put("entity_id", childEntityId);
						/*
						 * CMDB 개선과 관련없음
						childEntityMap.put("ems_id", ems_id);
						childEntityMap.put("dca_id", dca_id);
						*/
						//ENTITY_ROOT_ID : 선택한 자산 OR 구성의 CLASS_ID 또는 CONF_TYPE
						childEntityMap.put("entity_class_id", (String) entityMap.get("ENTITY_ROOT_ID"));
						//entity_mng_type 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
						childEntityMap.put("entity_mng_type", (String) entityMap.get("ENTITY_MNG_TYPE"));
						
						// 구성요소가 에디터 폼 일 경우
						if(childEntityType.equalsIgnoreCase("MANUAL")){
							
							// 해당 에디터 폼에 구성되어 있는 필드들을 가져온다
							List<HashMap> attrList = assetAutomationDAO.selectManualAttrList(childEntityMap);
							
							// 각 필드별 권한설정과 전체수정권한(ALL)인 필드들을 가져온다
							attrList = selectOpmsMnualAllList(attrList,childEntityMap);
							
							// 에디터 폼 패널의 버튼  리스트를 가져온다
							String panelBtns = webixUIHandler.createPanelBtnJsonString(assetAutomationDAO.selectEntityBtnInfo(childEntityMap), paramMap);	
					
							if(attrList != null && attrList.size()>0){
								
								// 히든필드 목록을 가져온다
								List<HashMap> hiddenList = selectOpmsHiddenList(attrList,childEntityMap);
								
								// 에디터 폼을 생성할 수 있는 Function을 만들어 준다
								String editorForm = webixUIHandler.createEditorFormJsonString(attrList,hiddenList,childEntityMap,formArray,panelBtns);
								
								// 1개 탭 내부의 구성요소로 추가한다
								tabInnerItems.add(editorForm);
								
								// 에디터 폼 내의 필드들의 데이터를 가져온다
								selectMnualCompData(attrList,hiddenList,childEntityMap,dataArray);
							}
						} 
						
						// 구성요소가 컴포넌트 일 경우 (그리드)
						else if(childEntityType.equalsIgnoreCase("COMPONENT")) {
							
//							// 권한을 가져온다
							List<HashMap> authList = assetAutomationDAO.selectCompAuthList(childEntityMap);
							
//							// 권한을 세팅한다 (내부적으로 childEntityMap에 권한을 putting)
							selectOpmsCompAuth(authList,childEntityMap);
							
							// 연관장비와,, 설치정보의 경우.. 상세 리스트를 가져와야한다.
							List<HashMap> attrList = assetAutomationComponents.selectOpmsCompSetupList(childEntityMap);
//							
							// 컴포넌트 중에 예외 처리 다른 핸들링이 필요한경우 .....
							exceptionCompHandler(attrList,childEntityMap,dataArray);
							
//							// 그리드 컴포넌트를 생성할 수 있는 Function을 만들어 준다
							String grid = webixUIHandler.createGridComponentJsonString(attrList, childEntityMap, formArray, userId);
							
							// 1개 탭 내부의 구성요소로 추가한다
							tabInnerItems.add(grid);
						}
					}
					
					// 1개의 그룹(탭)의 구성이 끝나면 아이템들을 concat() 함수를 통해 하나로 묶어준다.
					String concatItem = webixUIHandler.concatArrayItems(tabInnerItems);
					
					// 1개의 탭 페이지를 생성한다
					String tabPage = webixUIHandler.createTabPageJsonString(entityMap, concatItem);
					tabs.add(tabPage);
					
					// 탭이 추가 완료 되었다면 탭의 헤더를 의미하는 탭바도 역시 하나 추가 한다.
					String tabBarItem = webixUIHandler.createTabBarItemJsonString(entityMap);
					tabBarItems.add(tabBarItem);
				
				} 
				
				// 탭의 유형이 '컴포넌트' 일 경우 (수정이력, 요청이력 등)
				else if(entity_type.equalsIgnoreCase("COMPONENT")) {
					
					// 컴포넌트 권한 리스트
					List<HashMap> authList = assetAutomationDAO.selectCompAuthList(entityMap);
					
					// 권한 설정
					selectOpmsCompAuth(authList,entityMap);
					
					// 연관장비와,, 설치정보의 경우.. 상세 리스트를 가져와야한다.
					List<HashMap> attrList = assetAutomationComponents.selectOpmsCompSetupList(entityMap);
					
					// 컴포넌트 중에 예외 처리 다른 핸들링이 필요한경우 ..... 
					exceptionCompHandler(attrList,entityMap,dataArray);
					
					// 탭 컴포넌트 생성
					String tab = webixUIHandler.createTabComponentJsonString(attrList,entityMap,formArray);
					
					// 1개의 탭 페이지를 생성한다
					String tabPage = webixUIHandler.createTabPageJsonString(entityMap, tab);
					tabs.add(tabPage);
					
					// 탭이 추가 완료 되었다면 탭의 헤더를 의미하는 탭바도 역시 하나 추가 한다.
					String tabBarItem = webixUIHandler.createTabBarItemJsonString(entityMap);
					tabBarItems.add(tabBarItem);
				}
			}
			
			// 최종적으로 탭바를 구성한다. (탭 아이템들은 Array 이므로 이미 구성완료 되었기에, Map형태인 Tab bar 최종 구성)
			tabBars.put("options", tabBarItems);
		}
		
		//tab 정보
		paramMap.put("tabBars" , webixUIHandler.replaceAllquotes(tabBars.toString()));
		paramMap.put("tabItems" , webixUIHandler.replaceAllquotes(tabs.toString()));
		
		//데이터 id array
		paramMap.put("formItemIds"	, formArray.toString());
		//form 데이터 array
		paramMap.put("formData"		, dataArray.toString());
		//수정 권한 갯수
		paramMap.put("authCnt"		, authCnt);
	}
	
	@Override
	public void exceptionCompHandler(List<HashMap> attrList,HashMap entityMap,JSONArray dataArray)throws NkiaException{
		
		String entityId = StringUtil.parseString(entityMap.get("ENTITY_ID"));
		String compType = StringUtil.parseString(entityMap.get("COMP_TYPE"));
		JSONObject dataObj = new JSONObject();
		
		if("FILE".equalsIgnoreCase(compType)){//파일
			//파일 인경우 파일 id를 가져온다.
			String file_id = assetAutomationDAO.selectatchFileId(entityMap);
			entityMap.put("atch_file_id", file_id);
		}else if("INSTALL".equalsIgnoreCase(compType)){
			//설치 정보인경우 해당 관련 속성을 가져온다..
			for(int k=0;k<attrList.size();k++){
				HashMap attrDataMap = attrList.get(k);
				String code_id = StringUtil.parseString(attrDataMap.get("CODE_ID"));
				//설정된 설치 정보 리스트
				List installColList = assetAutomationDAO.selectOpmsInstallColumnList(code_id);
				entityMap.put(code_id, installColList);
			}
		}else if("CONFNMCODE".equalsIgnoreCase(compType)){
			//장비 명명정보는 manual form 형식이 아니어서 데이터를 동적으로 가져오지 못한다.. 
			HashMap confNmCode = assetAutomationDAO.selectOpmsConfNmCodeInfo(entityMap);
			//장비 명명 정보를 가져온다.
			dataObj.put(entityId, confNmCode);
			dataArray.add(dataObj);
		}else if("MAINT".equalsIgnoreCase(compType)){
			HashMap maintData = assetAutomationDAO.selectOpmsMaintFormInfo(entityMap);
			//장비 명명 정보를 가져온다.
			dataObj.put(entityId, maintData);
			dataArray.add(dataObj);
		}
	}
	
	@Override
	public List selectOpmsHiddenList(List<HashMap> attrList,HashMap entityMap)throws NkiaException{
		//auth_type : u: 수정 ,r: 읽기 , n: 히든
		entityMap.put("auth_type", "N");
		entityMap.put("attrList"	, attrList);
		return assetAutomationDAO.selectMnualAttrListByType(entityMap);
	}
	
	@Override
	public List selectOpmsMnualAllList(List<HashMap> attrList,HashMap entityMap)throws NkiaException{
		if(attrList != null){
			String authType = "R";
			boolean isAllAuth = false;
			
			//전체권한 [ALL]이 있는지 체크
			for(int i=0;i<attrList.size();i++){
				HashMap attrMap  = (HashMap)attrList.get(i);
				String colId = (String)attrMap.get("COL_ID");
				String colAuthType = (String)attrMap.get("AUTH_TYPE");
				
				// 전체 읽기, 수정일 경우
				if("ALL".equalsIgnoreCase(colId)) {
					if("U".equals(colAuthType)) {
						authType = colAuthType;
					}
					
					isAllAuth = true;
				} else {
					if("U".equals(colAuthType)){
						authType = colAuthType;
					}
				}
			}
			
			// manual 인 경우 전체 읽기 나 전체 수정으로 권한을 받으면 속성 리스트를 다시 가져온다.
			if(isAllAuth) {
				entityMap.put("auth_type", authType);
				attrList = new ArrayList<HashMap>();
				attrList = assetAutomationDAO.selectMnualAttrListByType(entityMap);
			}
			
			entityMap.put("AUTH_TYPE", authType);
		}
		
		return attrList;
	}
	
	@Override
	public HashMap selectOpmsCompAuth(List<HashMap> attrList,HashMap entityMap)throws NkiaException{
		String authType = "R";
		String compareAuthType = "";
		//컴포넌트 권한 핸들러
		for(int i=0;i<attrList.size();i++){
			HashMap attrMap  = (HashMap)attrList.get(i);
			compareAuthType = (String) attrMap.get("AUTH_TYPE");
			
			if(compareAuthType.equals("U")){
				authType = compareAuthType;
				break;
			}
		}
		entityMap.put("AUTH_TYPE", authType);
		return entityMap;
	}
	
	@Override
	public List selectOpmsSelMngUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//담당자 리스트
		return assetAutomationDAO.selectOpmsSelMngUserList(paramMap);
	}

	@Override
	public int selectOpmsMngUserCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsMngUserCnt(paramMap);
	}
	
	@Override
	public List selectOpmsMngUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsMngUserList(paramMap);
	}
	
	@Override
	public List selectOpmsSwLicenceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//소프트웨어 라이센스 리스트
		return assetAutomationDAO.selectOpmsSwLicenceList(paramMap);
	}
	
	@Override
	public List selectOpmsSelUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//사용자 리스트
		return assetAutomationDAO.selectOpmsSelUserList(paramMap);
	}
	
	@Override
	public int selectOpmsUserCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsUserCnt(paramMap);
	}
	
	@Override
	public List selectOpmsUserList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsUserList(paramMap);
	}
	@Override
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchCodeDataList(paramMap);
	}
	@Override
	public List selectOpmsSelChildList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//자자산리스트
		return assetAutomationDAO.selectOpmsSelChildList(paramMap);
	}
	@Override
	public List selectOpmsSelPurInfoList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//구매정보리스트
		return assetAutomationDAO.selectOpmsSelPurInfoList(paramMap);
	}
	@Override
	public List selectOpmsDepreciationInfoList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//구매정보리스트
		return assetAutomationDAO.selectOpmsDepreciationInfoList(paramMap);
	}
	
	@Override
	public List selectOpmsRelConfList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		String class_type = (String)paramMap.get("class_type");
		List relConfList = null;
		
		if("HA".equalsIgnoreCase(class_type)){ 
			//이중화의 경우 
			relConfList = assetAutomationDAO.selectOpmsRelHaGrpList(paramMap);
		}else if("LOGSV".equalsIgnoreCase(class_type)){
			//연관 장비에서 논리 연계정보인경우
			relConfList = assetAutomationDAO.selectOpmsReLogConfList(paramMap);
		}else if("DR".equalsIgnoreCase(class_type)){
			//연관 장비에서 논리 연계정보인경우
			relConfList = assetAutomationDAO.selectOpmsRelDRList(paramMap);
		}else{
			//나머지.. 등등
			relConfList = assetAutomationDAO.selectOpmsRelConfList(paramMap);
		}
		
		return relConfList;
	}
	
	@Override
	public List selectRelConfClassTree(HashMap paramMap)throws NkiaException{
		List returnList = new ArrayList();
		String mng_type = StringUtil.parseString(paramMap.get("mng_type"));
		
		if("ASSET".equals(mng_type)) {
			returnList = assetAutomationDAO.selectRelConfClassTree(paramMap);
		} else {
			returnList = assetAutomationDAO.selectRelConfTypeTree(paramMap);
		}
		
		return returnList;
	}

	@Override
	public int selectOpmsConfCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsConfCnt(paramMap);
	}

	@Override
	public List selectOpmsConfList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsConfList(paramMap);
	}
	
	@Override
	public List selectOpmsRelServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsRelServiceList(paramMap);
	}

	@Override
	public int selectOpmsServiceCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsServiceCnt(paramMap);
	}

	@Override
	public List selectOpmsServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsServiceList(paramMap);
	}
	
	@Override
	public List selectServiceCustList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectServiceCustList(paramMap);
	}
	

	@Override
	public List selectOpmsInstallInfoList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//insType : 설치 분류 정보 
		String insType = (String)paramMap.get("ins_type");
		List insInfoList = null;
		
		HashMap intstallTblInfo = assetAutomationDAO.selectOpmsInstallInfo(insType);
		//String emsShowYn = StringUtil.parseString(intstallTblInfo.get("EMS_SHOW_YN"));
		//설치 정보마다 셋팅된 컬럼 정보를 가져온다.
		List<HashMap> intstallColList = assetAutomationDAO.selectOpmsInstallColumnList(insType);
		
		webixUIHandler.paramSync(paramMap, intstallTblInfo);
		//select 쿼리를 만든다.
		String query = webixUIHandler.createInstallQuery(intstallTblInfo,intstallColList, false);
		//정보리스트를 가져온다.
		insInfoList = assetAutomationDAO.excuteQueryList(query);
		
		return insInfoList;
	}
	
	@Override
	public HashMap selectMnualCompData(List attrList,List hiddenList,HashMap entityMap,JSONArray dataArray)throws NkiaException{
		
		JSONObject dataObj = new JSONObject();
		String entity_id = (String)entityMap.get("ENTITY_ID");
		
		//그룹에 해당되는 테이블 리스트
		List<HashMap> tblList 	=	assetAutomationDAO.selectTblList(entityMap);
		//join정보 
		List<HashMap> joinList	=  assetAutomationDAO.selectJoinList(entityMap);
		
		
		entityMap.put("tblList", tblList);
		entityMap.put("sel_type", "SKEYID");
		//conf_id,asset_id 리스트를 가져온다.
		List<HashMap> assetConfIdList	=  assetAutomationDAO.selectTypeColumn(entityMap);
		
		//select 쿼리를 만든다..
		String query = null;
		if(entityMap.get("view_type").equals("infmodifiy")){
			query = webixUIHandler.createOpmsInfoSelectQuery(attrList,hiddenList,tblList,joinList,assetConfIdList,entityMap);
		}else{
			query = webixUIHandler.createSelectQuery(attrList,hiddenList,tblList,joinList,assetConfIdList,entityMap);
		}
		
		HashMap resultMap  = assetAutomationDAO.excuteQuery(query);
		
		if(resultMap != null) {
			// 날짜 유형의 시간 데이터 처리   
			for (HashMap attrMap : (List<HashMap>)attrList) {
				if("DATE".equals(attrMap.get("COL_TYPE"))) {
					if(attrMap.get("DATE_TYPE").toString().contains("HH24")) {
						resultMap.put(attrMap.get("COL_ID") + "_hour", resultMap.get(attrMap.get("COL_ID") + "_HOUR"));
					}
					
					if(attrMap.get("DATE_TYPE").toString().contains("MI")) {
						resultMap.put(attrMap.get("COL_ID") + "_min", resultMap.get(attrMap.get("COL_ID") + "_MIN"));
					}
				}
			}
		}
		
		if(dataArray != null){
			dataObj.put(entity_id, resultMap);
			dataArray.add(dataObj);
		}
		
		return resultMap;
	}
	
	@Override
	public void updateMnualCompData(List attrList,List hiddenList,HashMap entityMap,HashMap currentData)throws NkiaException{
		
		JSONObject dataObj = new JSONObject();
		String entity_id = (String)entityMap.get("ENTITY_ID");
		
		// 숨김 속성중 AGENT_ID가 있을경우 수정 처리 대상
		for (int i=0; i<hiddenList.size(); i++) {
			HashMap hiddenMap = (HashMap)hiddenList.get(i);
			
			//hmsong cmdb 개선 작업 : 용도 불분명
//			if ("AGENT_ID".equals(hiddenMap.get("COL_ID"))) {
//				hiddenMap.put("AUTH_TYPE", "U");
//				hiddenMap.put("AM_VALUE", StringUtil.parseString(currentData.get("AGENT_ID")));
//				attrList.add(hiddenMap);
//			}
		}
		
		//해당 컴포넌트에 맵핑된 테이블 리스트를 가져온다.
		List<HashMap> tblList 	=	assetAutomationDAO.selectTblList(entityMap);
		
		entityMap.put("tblList", tblList);
		entityMap.put("attrList",attrList);
		
		for(int i=0;i<tblList.size();i++){
			
			HashMap tblMap = tblList.get(i);
			entityMap.put("gen_table_nm", tblMap.get("GEN_TABLE_NM"));
			
			//update key정보를 가져온다.
			entityMap.put("sel_type", "UKEYID");
			List<HashMap> assetConfIdList	=  assetAutomationDAO.selectTypeColumn(entityMap);
			
			//ins_user_id,ins_dt 가 존재하는지 존재하면 리스트를 가져온다.
			entityMap.put("sel_type", "INSID");
			List<HashMap> insUserDtList		=  assetAutomationDAO.selectTypeColumn(entityMap);
			
			//upd_user_id,upd_dt 가 존재하는지 존재하면 리스트를 가져온다.
			entityMap.put("sel_type", "UPDID");
			List<HashMap> updUserDtList		=  assetAutomationDAO.selectTypeColumn(entityMap);
			
			//실제 테이블에 존재하는 컬럼 리스트를 가져온다.
			entityMap.put("sel_type", "COLID");
			List<HashMap> tblColIdList	=  assetAutomationDAO.selectTypeColumn(entityMap);
			
			String query = "";
			//해당 id로 카운트를 가져온다.
			query = webixUIHandler.createCountQuery(assetConfIdList,entityMap);
			
			int cnt = 0;
			
			if(!"".equalsIgnoreCase(query)){
				//카운트 실행쿼리
				cnt = assetAutomationDAO.excuteQueryInt(query);
			}
			
			if(!"".equalsIgnoreCase(query)){
				if(cnt == 0){
					//데이터가 없으면 insert 쿼리를 만든다
					query = webixUIHandler.createInsertQuery(attrList,assetConfIdList,insUserDtList,updUserDtList,tblColIdList,entityMap,currentData);
				}else{
					//데이터가 있으면 update 쿼리를 만든다.
					query = webixUIHandler.createUpdateQuery(attrList,assetConfIdList,updUserDtList,tblColIdList,entityMap,currentData);
				}
				//쿼리 실행
				if(!"".equals(query)){
					assetAutomationDAO.excuteCudQuery(query);
				}
			}
		}
	}
	
	@Override
	public void updateOpmsDetail(HashMap paramMap)throws NkiaException{
		
		String conf_id	 	= StringUtil.parseString(paramMap.get("conf_id"));
		String asset_id 	= StringUtil.parseString(paramMap.get("asset_id"));
		String user_id 		= StringUtil.parseString(paramMap.get("user_id"));
		String class_id 	= StringUtil.parseString(paramMap.get("class_id"));
		String class_type 	= StringUtil.parseString(paramMap.get("class_type"));
		String sr_id 	= StringUtil.parseString(paramMap.get("sr_id"));
		String chgMessage = StringUtils.isEmpty(sr_id) ? "" : sr_id;	//요청 팝업에서 수정한 경우에 이력 메시지를 sr_id로 남긴다
		String file_id	 	= StringUtil.parseString(paramMap.get("atch_file_id"));
		String entity_root_id 	= StringUtil.parseString(paramMap.get("entity_root_id"));	//mng_type이  ASSET 또는 CONF일 때 class_id나 conf_type을 넣어줌.
		String entity_class_id 	= StringUtil.parseString(paramMap.get("entity_class_id"));	//mng_type이 ASSETCONF일 때, class_id
		String entity_conf_type_id 	= StringUtil.parseString(paramMap.get("entity_conf_type_id"));	//mng_type이 ASSETCONF일 때, conf_type	
		String conf_type	 	= StringUtil.parseString(paramMap.get("conf_type"));	//구성용도
//		String asset_id_hi	= StringUtil.parseString(paramMap.get("asset_id_hi"));		//hmsong cmdb 개선 작업 : 용도 불분명
		
		//mng_type : ASSET(자산조회), mng_type : CONF(구성조회),  mng_type : ASSETCONF(자산구성조회)
		String mng_type 	= StringUtil.parseString(paramMap.get("mng_type"));	
		
		List formItemIds	= (List)paramMap.get("formItemIds");
		
		if(formItemIds != null){
			
			// AM_ENTITY_TBL_MAPPING 테이블에 매핑된 테이블들이 asset 속성인지, conf 속성인지 구분. CONF_ID를 가지고 있으면 구성으로 보고, CONF_ID가 없으면 자산속성으로 봄.
			List<HashMap> mapTableKindList = assetAutomationDAO.searchTableAssetConfDivInfo(paramMap);
			HashMap mapTableKind = new HashMap();
			
			if(mapTableKindList.size() > 0) {
				for(int i=0; i<mapTableKindList.size(); i++) {
					HashMap tmpMap = (HashMap) mapTableKindList.get(i);
					mapTableKind.put(StringUtil.parseString(tmpMap.get("GEN_TABLE_NM")), StringUtil.parseString(tmpMap.get("PK_INFO")));
				}
			}		
			
			int hisAuthCnt = 0;
			
			for(int i=0;i<formItemIds.size();i++){
				
				HashMap dataMap = (HashMap)formItemIds.get(i);
				
				String entity_id	= StringUtil.parseString(dataMap.get("entity_id"));
				String comp_id		= StringUtil.parseString(dataMap.get("comp_id"));
				String type 			= StringUtil.parseString(dataMap.get("type"));
				String entity_type= StringUtil.parseString(dataMap.get("entity_type"));
				//entity_mng_type(자산구성속성구분) 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
				String entity_mng_type = StringUtil.parseString(dataMap.get("entity_mng_type"));
				
				dataMap.put("asset_id", asset_id);
				dataMap.put("conf_id", conf_id);
				dataMap.put("class_id", class_id);
				dataMap.put("user_id", user_id);
				dataMap.put("ENTITY_ID", entity_id);
				dataMap.put("conf_type", conf_type);
				dataMap.put("mng_type", entity_mng_type);	
//				dataMap.put("asset_id_hi", asset_id_hi);	//hmsong 미사용
//				dataMap.put("chg_his_seq", chgSeq);			//hmsong 미사용
				
				// 화면에서 가져온 속성(예시.기본정보, 운영정보 등)이 'ASSET'일 때는 entity_root_id에 class_id를 넣어주고, 'CONF' 일 때는 conf_type을 파라미터로 전달한다.
				if("ASSET".equals(entity_mng_type)) {
					dataMap.put("entity_root_id", class_id);
				} else {
					dataMap.put("entity_root_id", conf_type);
				}	

				//권한이 있는지를 판단한다.
				int authCnt	 = assetAutomationDAO.selectUpdateAuthCnt(dataMap);
				
				// entity_type - 폼인 경우 MANUAL이 들어가고, 컴포넌트인 경우엔 컴포넌트의 COMP_TYPE(담당자인지, 수정이력인지 등)이 들어감
				// hmsong cmdb 개선 작업 - 주석처리 이유 : 현재 BASE와 관련성 없음. 등록이나 저장시에 영향도 없음. 
				/*
				if("REL_INFRA".equals(entity_type) || "INSTALL".equals(entity_type)){
					hisAuthCnt = authCnt;
				}else if(entity_type.endsWith("_HIST")){
					authCnt = hisAuthCnt;
				}
				*/

				/*
				   ※ type 설명
				  WebixUiHandler에서 type 정해준 다음 화면으로 전달.
				  form이면 F, 컴포넌트 리스트(탭으로 분리되지 않은 컴포넌트)이면 SL(단,설치정보와 구성연계장비정보는 TL),탭 컴포넌트면 TAB
				*/
				if("F".equalsIgnoreCase(type) || "CF".equalsIgnoreCase(type)){
					//현제 update 데이터
					HashMap currentData 		= (HashMap)paramMap.get(comp_id);
					//해당 속성 리스트를 가져온다.
					List<HashMap> attrList 		= assetAutomationDAO.selectManualAttrList(dataMap);
					//권한 및 all인경우 해당 속성 리스트를 가져온다.
					attrList 					= selectOpmsMnualAllList(attrList,dataMap);
					//숨김 속성리스트를 가져온다.
					List<HashMap> hiddenList 	= selectOpmsHiddenList(attrList,dataMap);
					//히스토리 테이블에서 해당 데이터와 지금 현재 데이터를 비교한다.  -  기존이력 주석처리 20170329
//					compareDetailData(attrList,dataMap,currentData);
					if(authCnt>0){

						// 이력데이터를 등록한다.	- 신규이력 20170329 좌은진
						// 변경되는 항목 중 원장에 있는 속성 목록만 추출하여 이력호출한다.
						List<HashMap> newDataList = new ArrayList();
						for(HashMap tempMap : attrList){
							String columnId = (String) tempMap.get("COL_ID");
							if(currentData.containsKey(columnId)){
								HashMap attrMap = new HashMap();
								attrMap.put("COLUMN_ID", columnId);
								attrMap.put("CHG_VALUE", currentData.get(columnId));
								attrMap.put("GEN_TABLE_NM", (String) tempMap.get("GEN_TABLE_NM"));
								newDataList.add(attrMap);
							}
						}
						// as is 자산속성 이력등록 호출
						//assetHistoryService.insertAssetAttrHistory(conf_id, newDataList, chgMessage, user_id);//hmsong CMDB 구현을 위해 주석						
						// to be 새로운 CMDB용 자산속성 이력등록 호출
						assetHistoryService.insertAssetConfAttrHistory(conf_id, conf_type, asset_id, class_type, entity_mng_type, newDataList, chgMessage, user_id, mapTableKind);

						//권한이 있으면 update or insert
						updateMnualCompData(attrList,hiddenList,dataMap,currentData);
						
					}
				} else {
					// 컴포넌트 데이터 업데이트 함수 호출
					assetAutomationComponents.updateComponent(paramMap, dataMap, authCnt);
				}
			}
			// 첨부파일에 대한 처리
			if(!"".equalsIgnoreCase(file_id)){
				assetAutomationDAO.updateAtchFileId(paramMap);
			}
		}

		if(!"".equals(mng_type)) {
			if(!"ASSET".equals(mng_type) ) {
				assetAutomationDAO.updateImportanceByAvCnIn(paramMap); // hmsong 가용성, 기밀성, 무결성, 자산등급은 am_conf의 속성이기 때문에 아래와 같이 처리. 
			}
		}
		
	}
	

	@Override
	public String selectAssetChgHisSeq(HashMap dataMap)throws NkiaException{
		assetAutomationDAO.insertChgHisSeq(dataMap);
		return assetAutomationDAO.selectAssetChgHisSeq(dataMap);
	}
	
	/**
	 * 기존 이력 비교로직
	 * entity_type == > manual 히스토리 값과 비교.. update전에 미리 데이터를 넣어놓습니다.. 
	 * @param attrList
	 * @param dataMap
	 * @param currentData
	 * @throws NkiaException
	 */
	@Override
	public void compareDetailData(List<HashMap> attrList,HashMap dataMap,HashMap currentData)throws NkiaException{
		
		String chg_his_seq 	= StringUtil.parseString(dataMap.get("chg_his_seq"));
		String conf_id 			= StringUtil.parseString(dataMap.get("conf_id"));
		String asset_id 		= StringUtil.parseString(dataMap.get("asset_id"));
		String user_id 			= StringUtil.parseString(dataMap.get("user_id"));
		
		for(int i=0;i<attrList.size();i++){
			
			HashMap colDataMap = attrList.get(i);
			
			String mkCompNm	= StringUtil.parseString(colDataMap.get("MK_COMP_NM"));
			String colId			= StringUtil.parseString(colDataMap.get("COL_ID"));
			String genTableNm= StringUtil.parseString(colDataMap.get("GEN_TABLE_NM"));
			String curValue	= StringUtil.parseString(currentData.get(mkCompNm));
			String defaultVal 	= StringUtil.parseString(colDataMap.get("DEFAULT_VALUE"));
			
			if("".equals(defaultVal)){
				HashMap hisDataMap = new HashMap();
				
				hisDataMap.put("conf_id"			, conf_id);
				hisDataMap.put("asset_id"			, asset_id);
				hisDataMap.put("chg_his_seq"	, chg_his_seq);
				hisDataMap.put("seq"				, "1");
				hisDataMap.put("chg_table"		, genTableNm);
				hisDataMap.put("chg_column_id", colId);
				hisDataMap.put("curValue", curValue);
				
				assetAutomationDAO.updateAmAssetChgHisModYn(hisDataMap);
			}
		}
	}
	
	/**
	 * 설치 정보 가져온다.
	 * @param dataMap
	 * @throws NkiaException
	 */
	@Override
	public void deleteInstallInfo(HashMap dataMap)throws NkiaException{
		String query = webixUIHandler.createInstallDelQuery(dataMap);
		assetAutomationDAO.excuteCudQuery(query);
	}
	
	/**
	 * 설치 정보 등록
	 * @param dataMap
	 * @param installColList
	 * @throws NkiaException
	 */
	@Override
	public void insertInstallInfo(HashMap dataMap,List<HashMap> installColList)throws NkiaException{
		String query = webixUIHandler.createInstallInsQuery(dataMap,installColList);
		assetAutomationDAO.excuteCudQuery(query);
	}
	
	/**
	 * 그룹에 관련된 테이블의 데이터를 히스토리 테이블에 모두 넣어준다.. 
	 * @param dataMap
	 * @throws NkiaException
	 */
	@Override
	public void insertPrevDataHis(HashMap dataMap)throws NkiaException{
		
		String chg_his_seq 	= StringUtil.parseString(dataMap.get("chg_his_seq"));
		String conf_id 			= StringUtil.parseString(dataMap.get("conf_id"));
		String asset_id 		= StringUtil.parseString(dataMap.get("asset_id"));
		String user_id 			= StringUtil.parseString(dataMap.get("user_id"));
		
		//각그룹에 해당되는 테이블 리스트 목록을 가져온다.
		List<HashMap> entityTblList = assetAutomationDAO.selectEntityManualTable(dataMap);
		
		if(entityTblList != null){
			for(int i=0;i<entityTblList.size();i++){
				
				List<HashMap> tblList = new ArrayList();
				
				HashMap tblDataMap = entityTblList.get(i);
				
				String gen_table_nm = StringUtil.parseString(tblDataMap.get("GEN_TABLE_NM"));
				dataMap.put("GEN_TABLE_NM", gen_table_nm);
				
				List<HashMap> colList = assetAutomationDAO.selectTabAllColumnsList(dataMap);
				
				tblList.add(dataMap);
				
				dataMap.put("tblList", tblList);
				dataMap.put("sel_type", "SKEYID");
				
				List<HashMap> assetConfIdList	=  assetAutomationDAO.selectTypeColumn(dataMap);
				
				String query = webixUIHandler.createSelectQuery(colList,null,tblList,null,assetConfIdList,dataMap);
				
				HashMap resultMap  = assetAutomationDAO.excuteQuery(query);
				
				for(int j=0;j<colList.size();j++){
					HashMap colDataMap = colList.get(j);
					
					String mkCompNm	= StringUtil.parseString(colDataMap.get("MK_COMP_NM"));
					String colId			= StringUtil.parseString(colDataMap.get("COL_ID"));
					String colNm		= StringUtil.parseString(colDataMap.get("COL_NAME"));
					String hystoryYn 	= StringUtil.parseString(colDataMap.get("HISTORY_YN"));
					String genTableNm= StringUtil.parseString(colDataMap.get("GEN_TABLE_NM"));
					String htmlType 	= StringUtil.parseString(colDataMap.get("HTML_TYPE"));
					String defaultVal 	= StringUtil.parseString(colDataMap.get("DEFAULT_VALUE"));
					
					if(resultMap != null){
						String value		= StringUtil.parseString(resultMap.get(mkCompNm));
						
						if("".equals(defaultVal)){
							HashMap hisDataMap = new HashMap();
							
							hisDataMap.put("conf_id"			, conf_id);
							hisDataMap.put("asset_id"			, asset_id);
							hisDataMap.put("user_id"			, user_id);
							hisDataMap.put("chg_his_seq"	, chg_his_seq);
							hisDataMap.put("chg_cud"			, "U");
							hisDataMap.put("code_yn"			, "N");
							hisDataMap.put("chg_yn"			, "N");
							hisDataMap.put("seq"				, "1");
							hisDataMap.put("chg_table"		, genTableNm);
							hisDataMap.put("chg_column_nm", colNm);
							hisDataMap.put("chg_column_id", colId);
							hisDataMap.put("chg_prev_value", value);
							
							if("DATE".equalsIgnoreCase(htmlType)){
								value = StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.replaceAll(value, "-", "")," ",""),":","");
							}else if("COMBO".equalsIgnoreCase("HTML_TYPE") 
									|| "CHECK".equalsIgnoreCase("HTML_TYPE") 
									|| "RADIO".equalsIgnoreCase("HTML_TYPE")){
								hisDataMap.put("code_yn"	, "Y");
							}
							
							int cnt = assetAutomationDAO.selectAmAssetChgHisCnt(hisDataMap);
							
							if(cnt == 0){
								assetAutomationDAO.insertAmAssetChgHis(hisDataMap);
							}
						}
					}
				}
			}
		}
		
		List<HashMap> componentList = assetAutomationDAO.selectEntityCompList(dataMap);
		
		if(componentList != null){
			// 컴포넌트 이력 등록 메서드 호출
//			assetAutomationComponents.registComponentHistory(dataMap, componentList);
		}
	}
	
	@Override
	public void registOpmsDetail(HashMap paramMap)throws NkiaException{
		String user_id 		= StringUtil.parseString(paramMap.get("user_id"));
		String class_id 	= StringUtil.parseString(paramMap.get("class_id"));
		String file_id	 	= StringUtil.parseString(paramMap.get("atch_file_id"));
		String regist_type	 	= StringUtil.parseString(paramMap.get("regist_type"));	//수정인지, 등록인지
		
//		String tangible_asset_yn 	= StringUtil.parseString(paramMap.get("tangible_asset_yn")); //hmsong cmdb 개선 작업 : 용도 불분명
//		String physi_asset_id	 	= StringUtil.parseString(paramMap.get("physi_asset_id")); //hmsong cmdb 개선 작업 : 용도 불분명
//		String physi_conf_id	 	= StringUtil.parseString(paramMap.get("physi_conf_id")); //hmsong cmdb 개선 작업 : 용도 불분명
		
		String mng_type 		= StringUtil.parseString(paramMap.get("mng_type")); // hmsong cmdb 개선 작업 : 화면에서 가져온 화면 구분자. 화면이 자산정보 등록(ASSET)인지, 구성정보 등록(CONF)인지, 자산+구성정보 등록(ASSETCONF)인지 구분.
		String conf_type 		= StringUtil.parseString(paramMap.get("conf_type")); // 구성용도
		
		String entity_root_id 	= StringUtil.parseString(paramMap.get("entity_root_id"));	//mng_type이  ASSET 또는 CONF일 때 class_id나 conf_type을 넣어줌.
		String entity_class_id 	= StringUtil.parseString(paramMap.get("entity_class_id"));	//mng_type이 ASSETCONF일 때, class_id
		String entity_conf_type_id 	= StringUtil.parseString(paramMap.get("entity_conf_type_id")); //mng_type이 ASSETCONF일 때, conf_type	
		
		if(!"".equals(mng_type)) {
			if("ASSET".equals(mng_type) || "ASSETCONF".equals(mng_type)) {
				selectClassInfo(paramMap); //클래스 정보
				createAssetId(paramMap); //자산ID 생성
			} 
			createConfId(paramMap); //구성ID 생성
		}
		
		String class_type = StringUtil.parseString(paramMap.get("class_type"));
		String asset_id = StringUtil.parseString(paramMap.get("asset_id"));
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		List formItemIds	= (List)paramMap.get("formItemIds");
//		String asset_id_hi = StringUtil.parseString(paramMap.get("asset_id_hi"));	//hmsong cmdb 개선 작업 : 용도 불분명
		
		if(formItemIds != null){
			
			int hisAuthCnt = 0;
			
			// AM_ENTITY_TBL_MAPPING 테이블에 매핑된 테이블들이 asset 속성인지, conf 속성인지 구분. CONF_ID를 가지고 있으면 구성으로 보고, CONF_ID가 없으면 자산속성으로 봄.
			List<HashMap> mapTableKindList = assetAutomationDAO.searchTableAssetConfDivInfo(paramMap);
			HashMap mapTableKind = new HashMap(); // FORMAT : { AM_ASSET : ASSET , AM_CONF : CONF , AM_SV : CONF }
			
			if(mapTableKindList.size() > 0) {
				for(int i=0; i<mapTableKindList.size(); i++) {
					HashMap tmpMap = (HashMap) mapTableKindList.get(i);
					mapTableKind.put(StringUtil.parseString(tmpMap.get("GEN_TABLE_NM")), StringUtil.parseString(tmpMap.get("PK_INFO")));
				}
			}		
		
			for(int i=0;i<formItemIds.size();i++){
				
				HashMap dataMap = (HashMap)formItemIds.get(i);
				
				String entity_id	= StringUtil.parseString(dataMap.get("entity_id"));
				String comp_id		= StringUtil.parseString(dataMap.get("comp_id"));
				String type 		= StringUtil.parseString(dataMap.get("type"));
				String entity_type= StringUtil.parseString(dataMap.get("entity_type"));
				//entity_mng_type 설명 : 불러오는 속성의 정보(예시.기본정보, 운영정보 등)가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
				String entity_mng_type= StringUtil.parseString(dataMap.get("entity_mng_type")); 		
				
				if("ASSET".equals(entity_mng_type)) {
					//업데이트할 속성이 '자산'에 속하는 속성이면 entity_root_id에 class_id를 넣어준다.
					entity_root_id = class_id;	
				} else {
					//업데이트할 속성이 '구성'에 속하는 속성이면 entity_root_id에 conf_type를 넣어준다.
					entity_root_id = conf_type; 
				}
				
				dataMap.put("asset_id", asset_id);
				dataMap.put("conf_id", conf_id);
				dataMap.put("class_id", class_id);
				dataMap.put("conf_type", conf_type);	//hmsong 추가, 구성용도
				
				dataMap.put("entity_root_id", entity_root_id);	
				dataMap.put("user_id", user_id);
				dataMap.put("ENTITY_ID", entity_id);
				dataMap.put("entity_mng_type", entity_mng_type);
//				dataMap.put("asset_id_hi", asset_id_hi);				//hmsong cmdb 개선 작업 : 용도 불분명
//				dataMap.put("physi_conf_id", physi_conf_id);			//hmsong cmdb 개선 작업 : 용도 불분명
//				dataMap.put("tangible_asset_yn", tangible_asset_yn);	//hmsong cmdb 개선 작업 : 용도 불분명
				
				
				/* ※  mng_type 설명 : 자산등록인지, 구성등록인지, 자산+구성등록인지 구분하는 구분자
			 	 mng_type : ASSET(자원), mng_type : CONF(구성), mng_type : ASSETCONF(자산구성)
				*/
				dataMap.put("mng_type", mng_type);		//hmsong 추가 
				
				int authCnt	 = assetAutomationDAO.selectUpdateAuthCnt(dataMap);	//가져온 개체의 권한정보 조회
				
				//entity_type - 폼인 경우 MANUAL이 들어가고, 컴포넌트인 경우엔 컴포넌트의 COMP_TYPE(담당자인지, 수정이력인지 등)이 들어감
				// hmsong cmdb 개선 작업 - 주석처리 이유 : 현재 BASE와 관련성 없음. 등록이나 저장시에 영향도 없음.
				/*
				if("REL_INFRA".equals(entity_type) || "INSTALL".equals(entity_type)){
					hisAuthCnt = authCnt;
				}else if(entity_type.endsWith("_HIST")){
					authCnt = hisAuthCnt;
				}
				*/
				
				/*
				   ※ type 설명
				  WebixUiHandler에서 type 정해준 다음 화면으로 전달.
				  form이면 F, 컴포넌트 리스트(탭으로 분리되지 않은 컴포넌트)이면 SL(단,설치정보와 구성연계장비정보는 TL),탭 컴포넌트면 TAB
				*/
				if("F".equalsIgnoreCase(type) || "CF".equalsIgnoreCase(type)){
					
					HashMap currentData 		= (HashMap)paramMap.get(comp_id);
					
					List<HashMap> attrList 		= assetAutomationDAO.selectManualAttrList(dataMap);
					attrList 					= selectOpmsMnualAllList(attrList,dataMap);
					List<HashMap> hiddenList 	= selectOpmsHiddenList(attrList,dataMap);
					
					if(authCnt>0){
											
						// 최초 등록값에 대하여 이력데이터를 등록한다.	- 신규이력 20170329 좌은진
						// 변경되는 항목 중 원장에 있는 속성 목록만 추출하여 이력호출한다.
						List<HashMap> newDataList = new ArrayList();
						for(HashMap tempMap : attrList){
							String columnId = (String) tempMap.get("COL_ID");
							if(currentData.containsKey(columnId)){
								HashMap attrMap = new HashMap();
								attrMap.put("COLUMN_ID", columnId);
								attrMap.put("CHG_VALUE", currentData.get(columnId));
								attrMap.put("GEN_TABLE_NM", (String) tempMap.get("GEN_TABLE_NM"));
								newDataList.add(attrMap);
							}
						}
						// as is 자산속성 이력등록 호출
						//assetHistoryService.insertAssetAttrHistory(conf_id, newDataList, chgMessage, user_id);//hmsong CMDB 구현을 위해 주석						
						// to be 새로운 CMDB용 자산속성 이력등록 호출
						assetHistoryService.insertAssetConfAttrHistory(conf_id, conf_type,  asset_id, class_type, entity_mng_type, attrList, "최초등록", user_id, mapTableKind);
						
						// 자산정보 등록
					}
					updateMnualCompData(attrList,hiddenList,dataMap,currentData);
					
				}else{ 
					// 컴포넌트 데이터 등록 함수 호출
					assetAutomationComponents.registComponent(paramMap, dataMap, authCnt);
				}
			}
			exceptionInsertHandler(paramMap);
		}
		
		if(!"".equals(mng_type)) {
			if(!"ASSET".equals(mng_type) ) {
				assetAutomationDAO.updateImportanceByAvCnIn(paramMap); // hmsong 가용성, 기밀성, 무결성, 자산등급은 am_conf의 속성이기 때문에 아래와 같이 처리. 
			}
		}	
		
	}
	
	@Override
	public void exceptionInsertHandler(HashMap paramMap)throws NkiaException{
		assetAutomationDAO.updateAtchFileId(paramMap);
		
		String mngType = StringUtil.parseString(paramMap.get("mng_type"));
		
		// hmsong cmdb 개선 작업 - 자산+구성 정보 등록일 때는 AM_ASSET정보랑 AM_CONF정보 둘 다 저장.
		if("ASSETCONF".equals(mngType)) {
			HashMap map = new HashMap();
			map.put("mng_type", "ASSET");
			map.put("class_type", StringUtil.parseString(paramMap.get("class_type")));
			map.put("class_id", StringUtil.parseString(paramMap.get("class_id")));
			map.put("asset_id", StringUtil.parseString(paramMap.get("asset_id")));
			assetAutomationDAO.updateBaseDataInfo(map);
			
			map = new HashMap();
			map.put("mng_type", "CONF");
			map.put("conf_type", StringUtil.parseString(paramMap.get("conf_type")));
			map.put("conf_id", StringUtil.parseString(paramMap.get("conf_id")));
			assetAutomationDAO.updateBaseDataInfo(map);
		} else {
			assetAutomationDAO.updateBaseDataInfo(paramMap);
		}
	
//		assetAutomationDAO.updateBaseLogSvInfo(paramMap);
	}
	
	@Override
	public String createConfId(HashMap paramMap)throws NkiaException{
		/*
		String classType = StringUtil.parseString(paramMap.get("class_type"));
		String tangibleAssetYn = StringUtil.parseString(paramMap.get("tangible_asset_yn"));
		
		// 구성ID Suffix 생성 시, 논리서버는 SL로 변경
		if("SV".equals(classType) && "N".equals(tangibleAssetYn)) {
			paramMap.put("in_class_type", "SL");
		} else {
			paramMap.put("in_class_type", classType);
		}
		*/
		
		//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		String confAdminYn = new String();
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}		
										
		String conf_type = StringUtil.parseString(paramMap.get("conf_type"));
		String classType = StringUtil.parseString(paramMap.get("class_type"));
		String in_class_type = new String();
		
		//confAdminYn == "Y" : 구성용도속성 관리여부가 '예'일 때는 conf_id의 앞에 구성용도가 들어가고, 그렇지 않을 때는 class_type이 들어간다. 
		if("Y".equals(confAdminYn)) {
			in_class_type = conf_type;	 
			
			if("".equals(conf_type) || "undefined".equals(conf_type) || "null".equals(conf_type)) {
				in_class_type = classType;	
			}
		} else {
			in_class_type = classType;			
		}
		paramMap.put("in_class_type", in_class_type);
		String conf_id = assetAutomationDAO.createConfId(paramMap);
		paramMap.put("conf_id", conf_id);
		
		return conf_id;
	}
	
	@Override
	public String createAssetId(HashMap paramMap)throws NkiaException{
		String asset_id = assetAutomationDAO.createAssetId(paramMap);
		paramMap.put("asset_id", asset_id);
		
		return asset_id;
	}
	
	@Override
	public void selectClassInfo(HashMap paramMap)throws NkiaException{
		HashMap classInfoMap = assetAutomationDAO.selectClassInfo(paramMap);
		String classType = StringUtil.parseString(classInfoMap.get("CLASS_TYPE"));
		paramMap.put("class_type", classType);
	}

	/**
	 * 
	 * 가상서버(클라우드와 연계된 가상서버) 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsVmList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOpmsVmList(paramMap);
	}

	/**
	 * 폼 관련 속성 이력정보를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsFormModList(HashMap paramMap) throws NkiaException {
		String comp_type = StringUtil.parseString(paramMap.get("comp_type")); 
		List<HashMap> attrList = new ArrayList<HashMap>();
		List<HashMap> tblList = new ArrayList<HashMap>();
		
		//장비 명명 정보
		if("CONFNMCODE".equalsIgnoreCase(comp_type)){
			//컴포넌트 폼으로 만들어진 겁니다.. 메가센터에만 있습니다.
			attrList	= webixUIHandler.getConfNmCodeResourceList();
			HashMap tblMap = new HashMap();
			tblMap.put("GEN_TABLE_NM", "AM_CONF_NM_CODE");
			tblList.add(tblMap);
		}else{//매뉴얼폼..
			attrList	=assetAutomationDAO.selectManualAttrList(paramMap);
			//All 인경우 다시 속성 리스트를 가져온다.권한 설정도 동시에 이루어짐..
			attrList	=selectOpmsMnualAllList(attrList,paramMap);
			//테이블 리스트를 가져온다.
			tblList		=assetAutomationDAO.selectTblList(paramMap);
		}
		
		paramMap.put("attrList", attrList);
		paramMap.put("tblList", tblList);
		
		return assetAutomationDAO.selectOpmsFormModList(paramMap);
		
		
	}

	
	/**
	 * 속성별 상세 이력 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectFormAttrModList(HashMap paramMap)throws NkiaException{
		// TODO Auto-generated method stub
		//모든키를 소문자로 변환해서 추가한다.
		webixUIHandler.replaceAddRowerKey(paramMap);
		
		String comp_type = StringUtil.parseString(paramMap.get("COMP_TYPE")); 
		List<HashMap> attrList = new ArrayList<HashMap>();
		List<HashMap> tblList = new ArrayList<HashMap>();
		//장비 명명 정보
		if("CONFNMCODE".equalsIgnoreCase(comp_type)){
			//컴포넌트 폼으로 만들어진 겁니다.. 메가센터에만 있습니다.
			attrList	= webixUIHandler.getConfNmCodeResourceList();
			HashMap tblMap = new HashMap();
			tblMap.put("GEN_TABLE_NM", "AM_CONF_NM_CODE");
			tblList.add(tblMap);
		}else{//매뉴얼폼..
			attrList	=assetAutomationDAO.selectManualAttrList(paramMap);
			//All 인경우 다시 속성 리스트를 가져온다.권한 설정도 동시에 이루어짐..
			attrList	=selectOpmsMnualAllList(attrList,paramMap);
			//테이블 리스트를 가져온다.
			tblList		=assetAutomationDAO.selectTblList(paramMap);
		}
		paramMap.put("attrList", attrList);
		paramMap.put("tblList", tblList);
		
		return assetAutomationDAO.selectFormAttrModList(paramMap);
	}

	@Override
	public JSONArray selectCompDataColumn(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		String comp_type = StringUtil.parseString(paramMap.get("COMP_TYPE"));
		String tab_type = StringUtil.parseString(paramMap.get("TAB_TYPE"));
		
		JSONArray defaultColArr = webixUIHandler.getGridResource("grid.itam.compModCompHisList");
		JSONArray detailColArr = new JSONArray();
		JSONArray returnColArr = new JSONArray();
		
		String gridResource = (String)paramMap.get("gridResource");
		
		if("SERVER_SELECT".equals(gridResource)) {
			if("INSTALL".equalsIgnoreCase(comp_type)){
				// 컴포넌트 유형이 설치정보 일 시 DB에서 컬럼정보를 select 하여 화면으로 넘겨준다.
				List installColList = assetAutomationDAO.selectOpmsInstallColumnList(tab_type);
				detailColArr = webixUIHandler.getGridResourceByList(installColList,"COL_ID","COL_NM");
			}
		} else {
			detailColArr = webixUIHandler.getGridResource(gridResource);
		}
		
		returnColArr = webixUIHandler.getMergeGridResource(defaultColArr,detailColArr,"[REPLACE_COL_DAT]");
		
		return returnColArr;
	}

	@Override
	public List selectCompDetailModList(HashMap paramMap) throws NkiaException {
		List<HashMap> resultList = new ArrayList<HashMap>();
		resultList = assetAutomationComponents.selectCompDetailModList(paramMap);
		
		return resultList;
	}

	
	
	
	
	@Override
	public List selectOpmsBtnTree(HashMap paramMap)throws NkiaException{
		return assetAutomationDAO.selectOpmsBtnTree(paramMap);
	}
	/**
	 * 모델속성을 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchModelTreeComp(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.searchModelTreeComp(paramMap);
	}
	/**
	 * 업무분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchWorkTreeComp(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.searchWorkTreeComp(paramMap);
		
	}
	
	/**
	 * HISOS를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchHISOSTreeComp(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.searchHISOSTreeComp(paramMap);
		
	}
	
	/**
	 * 설치된 소프트웨어 목록 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsInsSwList(HashMap paramMap) throws NkiaException {
		
		// 설치된 SW리스트
		List resultList = new ArrayList();
		
		// 선택한 1개의 SW에 맵핑된 옵션 테이블
		List optionList = new ArrayList();
		
		// 값을 새로 세팅해서 리턴할 리스트 
		List returnList = new ArrayList();
		
		// 먼저 설치된 SW 정보를 받아오고
		resultList = assetAutomationDAO.selectOpmsInsSwList(paramMap);
		
		// 1개의 SW마다 로직 실행
		for(int i=0; i<resultList.size(); i++) {
			
			HashMap returnMap = new HashMap();
			List optionIdList = new ArrayList();
			returnMap = (HashMap)resultList.get(i);
			
			// SW가 가지고 있는 옵션 테이블 조회
			optionList = selectSelectedOptionList(returnMap);
			returnMap.put("OPTION_ID_LIST", "");
			
			// 옵션테이블에 루프 돌려서
			for(int j=0; j<optionList.size(); j++) {
				
				HashMap optionMap = new HashMap();
				optionMap = (HashMap)optionList.get(j);
				
				// 옵션ID값만 가져와서 맵에 세팅
				optionIdList.add((String)optionMap.get("OPTION_ID"));
				returnMap.put("OPTION_ID_LIST", optionIdList);
			}
			
			// 리턴할 리스트에 맵 담아줌
			returnList.add(returnMap);
		}
		
		return returnList;
	}
	
	/**
	 * 설치 소프트웨어와 맵핑되는 소프트웨어 옵션 전체 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOptionMappingList(HashMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOptionMappingList(paramMap);
		
	}
	
	/**
	 * 설치 소프트웨어와 맵핑되는 선택되어있던 소프트웨어 옵션 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSelectedOptionList(HashMap paramMap) throws NkiaException {
		return  assetAutomationDAO.selectSelectedOptionList(paramMap);
		
	}

	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteInsSwOption(HashMap paramMap)throws NkiaException{
		assetAutomationDAO.deleteInsSwOption(paramMap);
	}
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertInsSwOption(HashMap paramMap)throws NkiaException{
		assetAutomationDAO.insertInsSwOption(paramMap);
	}
	
	/**
	 * 유지보수 정보 폼 데이터를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAssetMaintInfo(HashMap paramMap) throws NkiaException{
		return assetAutomationDAO.selectAssetMaintInfo(paramMap);
	}
	
	/**
	 * 모자산 변경 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public int selectMotherListCnt(HashMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectMotherListCnt(paramMap);
	}
	/**
	 * 모자산 변경 팝업 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectMotherList(HashMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectMotherList(paramMap);
	}
	/**
	 * 모자산 변경 정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateMotherInfo(HashMap paramMap) throws NkiaException {
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String User_Id = (String)userVO.getUser_id();
		
		int paramsize = paramMap.size();
		HashMap param = null;
		boolean isSelSeq = false;
		int seq = 0;
		
		for( int i=0; i < paramsize; i++ ){
			
			param = (HashMap) paramMap.get(String.valueOf(i));
			
			// 이력테이블 insert용 시퀀스 세팅
			if(!isSelSeq) {
				String a_asset_id_hi = (String) param.get("ASSET_ID_HI");
				param.put("a_asset_id_hi", a_asset_id_hi);
				
				seq = assetAutomationDAO.selectChildChgHisSeq(param);
				isSelSeq = true;
			}
			
			HashMap assetMap = new HashMap();
			assetMap.put("asset_id", (String) param.get("ASSET_ID"));
			
			param.put("seq"				, seq);
			param.put("asset_id"		, (String) param.get("ASSET_ID"));
			param.put("a_asset_id_hi"	, (String) param.get("ASSET_ID_HI"));
			param.put("b_asset_id_hi"	, (String) param.get("ORI_ASSET_ID"));
			param.put("conf_nm"			, (String) param.get("CONF_NM"));
			param.put("user_id"			, User_Id);
			param.put("chg_cnt"			, assetAutomationDAO.selectChildChgHisCnt(assetMap));
			
			// 이력테이블에 insert
			insertChildChgHis(param);
			// 모자산 정보 변경 update
			assetAutomationDAO.updateMotherInfo(param);
		}
	}

	@Override
	public List selectAmIntroDtGroupBy(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectAmIntroDtGroupBy(paramMap);
	} 
	
	/**
	 * 자품 변경 이력 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertChildChgHis(HashMap dataMap) throws NkiaException{
		
		//int seq = assetAutomationDAO.selectChildChgHisSeq(dataMap);
		
		//UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		//String userId = (String)userVO.getUser_id();
		
		//dataMap.put("seq", seq);
		//dataMap.put("user_id", userId);
		
		assetAutomationDAO.insertChildChgHis(dataMap);
	}
	
	/**
	 * 자품 변경 이력 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisList(HashMap paramMap) throws NkiaException{
		List resultList = assetAutomationDAO.selectChildChgHisList(paramMap);
		List newList = new ArrayList();
		
		for(int i=0; i<resultList.size(); i++) {
			HashMap dataMap = new HashMap();
			dataMap = (HashMap)resultList.get(i);
			String asset_id = (String)dataMap.get("ASSET_ID_VIEW");
			String asset_id_view = asset_id.substring(0,14) + "-" + asset_id.substring(14);
			dataMap.put("ASSET_ID_VIEW", asset_id_view);
			newList.add(dataMap);
		}
		
		return newList;
	}
	
	/**
	 * 자품 변경 이력 상세 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisDetail(HashMap paramMap) throws NkiaException{
		return (List)assetAutomationDAO.selectChildChgHisDetail(paramMap);
	}
	
	@Override
	public List selectOpmsLogicHisList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		//논리서버 이력 리스트
		return assetAutomationDAO.selectOpmsLogicHisList(paramMap);
	}
	
	/**
	 * 자산등록시 클래스 명
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchClassNm(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchClassNm(paramMap);
	}
	
	/**
	 * 프로세스 전체요청이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchProcMasterHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List reqList = new ArrayList();
		String entity_mng_type = new String();
		if(!"".equals(StringUtil.parseString(paramMap.get("entity_mng_type")))) {
			entity_mng_type = StringUtil.parseString(paramMap.get("entity_mng_type"));
		}
		
		// hmsong cmdb 개선 작업 - 요청 이력을 자산정보와 구성정보로 분리
		if("ASSET".equals(entity_mng_type)) {
			reqList = assetAutomationDAO.searchAssetProcMasterHist(paramMap);
		} else {
			reqList = assetAutomationDAO.searchConfProcMasterHist(paramMap);
		}
		return reqList;
	}
	
	/**
	 * 프로세스 장애이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchProcHist(paramMap);
	}

	/**
	 * 프로세스 변경이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchChangeProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchChangeProcHist(paramMap);
	}
	
	/**
	 * 프로세스 문제이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchProblemProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchProblemProcHist(paramMap);
	}

	/**
	 * 프로세스 서비스이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchServiceProcHist(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchServiceProcHist(paramMap);
	}

	/**
	 *  DCA 연계 PC S/W 목록
	 */
	@Override
	public List selectIfPcSwList(ModelMap paramMap) {
		return assetAutomationDAO.selectIfPcSwList(paramMap);
	}
	
	/**
	 * 하위 논리 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsRelLogicList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.selectOpmsRelLogicList(paramMap);
	}
	
	/**
	 * selectOpmsCompModList
	 * 컴포넌트 들의 수정이력을 가공한다.
	 */
	@Override
	public List selectOpmsCompModList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List<HashMap> compList = (List<HashMap>)paramMap.get("compItems");
		List<HashMap> modHistoryList = new ArrayList<HashMap>();
		if(compList != null){
			for(int i=0;compList.size()>i;i++){
				HashMap compMap = compList.get(i);
				webixUIHandler.paramSync(paramMap, compMap);
				assetAutomationComponents.selectOpmsCompTypeModList(modHistoryList,compMap);
			}
		}
		return modHistoryList;
	}
	
	/**
	 * 이력조회 - 장비별 요청현황조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@Override
	public List searchConfReqStateList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchConfReqStateList(paramMap);
	}
	
	@Override
	public int searchConfReqStateListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchConfReqStateListCount(paramMap);
	}
	
	/**
	 * 자산 메모리 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchBsInsMemoryView(HashMap paramMap)throws NkiaException{
		return assetAutomationDAO.searchBsInsMemoryView(paramMap);
	}
	
	/**
	 * 자산 CPU Core 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchBsInsCpuView(HashMap paramMap)throws NkiaException{
		return assetAutomationDAO.searchBsInsCpuView(paramMap);
	}
	
	/**
	 * 자산 Storage 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchBsInsStorageView(HashMap paramMap)throws NkiaException{
		return assetAutomationDAO.searchBsInsStorageView(paramMap);
	}
	
	/**
	 * 자산  NW 서비스중단일관리 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchBsSvcStopView(HashMap paramMap)throws NkiaException{
		return assetAutomationDAO.searchBsSvcStopView(paramMap);
	}
	
	/**
	 * 자산정보 조회
	 */
	@Override
	public HashMap selectAssetInfo(HashMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectAssetInfo(paramMap);
	}

	/**
	 * 디스크 목록 조회
	 */
	@Override
	public List selectOpmsRelDiskList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOpmsRelDiskList(paramMap);
	}
	
	/**
	 * IP 목록 조회
	 */
	@Override
	public List selectOpmsRelIPList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOpmsRelIPList(paramMap);
	}

	/**
	 * NIC 목록 조회
	 */	
	@Override
	public List selectOpmsRelNicList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOpmsRelNicList(paramMap);
	}
	
	/**
	 * 이중화 정보 목록 조회
	 */	
	@Override
	public List selectOpmsDualInfoList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOpmsDualInfoList(paramMap);
	}

	@Override
	public List selectOpmsRelVMList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOpmsRelVMList(paramMap);
	}
	
	@Override
	public int selectOpmsRelVMCount(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectOpmsRelVMCount(paramMap);
	}

	@Override
	public List selectEmsDiskList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectEmsDiskList(paramMap);
	}
	
	@Override
	public List selectEmsIPList(ModelMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectEmsIPList(paramMap);
	}

	@Override
	public void updateEmsInfo(HashMap paramMap) throws NkiaException {
		String confId = paramMap.get("conf_id").toString();
		String userId = paramMap.get("user_id").toString();
		String fieldId = paramMap.get("field_id").toString();
		String emsId = paramMap.get("ems_id").toString();
		String resultMsg = "";

		List<HashMap> newDataList = new ArrayList();

		HashMap attrMap = new HashMap();
		attrMap.put("COLUMN_ID", fieldId);
		attrMap.put("CHG_VALUE", emsId);
		newDataList.add(attrMap);
		
		// 이력 정보 저장
		assetHistoryService.insertAssetAttrHistory(confId, newDataList, "센터 EMS ID 저장버튼 클릭", userId);

		// 수정
		assetAutomationDAO.updateEmsInfo(paramMap);
	}

	@Override
	public int selectEmsIdCount(HashMap paramMap) throws NkiaException {
		return assetAutomationDAO.selectEmsIdCount(paramMap);
	}

	@Override
	public boolean isDuplicateAsset(HashMap paramMap) throws NkiaException {
		String confId = StringUtil.parseString(paramMap.get("conf_id"));
		String classType 	= StringUtil.parseString(paramMap.get("class_type"));
		List formItemIds = (List)paramMap.get("formItemIds");
		boolean isDuplicate = false;
		
		HashMap checkParamMap = new HashMap<>();
		checkParamMap.put("conf_id", confId);
		checkParamMap.put("class_type", classType);
		
		// 자산 중복 체크 컬럼
		String duplicateCheckColumns = NkiaApplicationPropertiesMap.getProperty("Globals.Asset.Duplicate.Check.Columns");
		String[] duplicateCheckColArr = {};
		
		if(!StringUtils.isEmpty(duplicateCheckColumns)) {
			duplicateCheckColArr = duplicateCheckColumns.split(",");
		}
		
		if(formItemIds != null){
			for(int i=0;i<formItemIds.size();i++){
				HashMap dataMap = (HashMap)formItemIds.get(i);
				String comp_id = StringUtil.parseString(dataMap.get("comp_id"));
				String type = StringUtil.parseString(dataMap.get("type"));
				
				if("F".equalsIgnoreCase(type) || "CF".equalsIgnoreCase(type)){
					//현제 update 데이터
					HashMap currentData = (HashMap)paramMap.get(comp_id);
					
					for (int j=0; j<duplicateCheckColArr.length; j++) {
						String[] paramColsArr = duplicateCheckColArr[j].split("\\.");
						
						if(currentData.containsKey(paramColsArr[1])) {
							String paramVal = currentData.get(paramColsArr[1].toUpperCase()).toString();
							checkParamMap.put(paramColsArr[1].toLowerCase(), paramVal);
						}
					}
				}
			}
		}
		
		int duplicateCnt = assetStatisticsDAO.selectAssetDuplicateCount(checkParamMap);
		
		if(duplicateCnt > 0) {
			isDuplicate = true;
		}
		
		return isDuplicate;
	}

	@Override
	public boolean isDuplicateBatchAsset(HashMap paramMap) throws NkiaException {
		// 구성ID 리스트
		List confIdList = (List)paramMap.get("confIdList");
		// 속성ID 리스트
		List colIdList = (List)paramMap.get("colIdList");
		String classType = StringUtil.parseString(paramMap.get("classType"));
		String userId = StringUtil.parseString(paramMap.get("upd_user_id"));
		String confIds = "";
		boolean isDuplicate = false;
		
		// 자산 중복 체크 컬럼
		String duplicateCheckColumns = NkiaApplicationPropertiesMap.getProperty("Globals.Asset.Duplicate.Check.Columns");
		String[] duplicateCheckColArr = {};
		
		if(!StringUtils.isEmpty(duplicateCheckColumns)) {
			duplicateCheckColArr = duplicateCheckColumns.split(",");
		}
		
		for (String confId : (List<String>)confIdList) {
			if(!StringUtils.isEmpty(confIds)) confIds += ",";
			confIds += "'" + confId + "'";
		}
		
		ModelMap conditionsMap = new ModelMap();
		conditionsMap.put("conf_id_list", confIds);
		conditionsMap.put("class_type", classType);
		conditionsMap.put("user_id", userId);
		
		// 업데이트 대상 자산 조회
		List<HashMap> updateAssetList = assetStatisticsDAO.searchAssetList(conditionsMap);
		
		if(updateAssetList.size() == 0) {
			isDuplicate = true;
		} else {
			for (HashMap assetInfo : updateAssetList) {
				String checkConfId = assetInfo.get("CONF_ID").toString();
				String checkClassType = assetInfo.get("CLASS_TYPE").toString();
				
				HashMap checkParamMap = new HashMap();
				checkParamMap.put("conf_id", checkConfId);
				checkParamMap.put("class_type", checkClassType);
				
				for (int j=0; j<duplicateCheckColArr.length; j++) {
					String[] paramColsArr = duplicateCheckColArr[j].split("\\.");
					boolean isExistCol = false;
					
					// 중복체크 대상 항목이 있으면 조회조건에 추가
					for (String colId : (List<String>)colIdList) {
						if(paramMap.containsKey((paramColsArr[1]))) {
							String paramVal = paramMap.get(paramColsArr[1].toUpperCase()).toString();
							checkParamMap.put(paramColsArr[1].toLowerCase(), paramVal);
							isExistCol = true;
						}
					}
					
					// 중복체크 대상 항목이 없으면 수정대상 자산 항목을 조회조건에 추가
					if(!isExistCol) {
						checkParamMap.put(paramColsArr[1].toLowerCase(), assetInfo.get(paramColsArr[1].toUpperCase()));
					}
				}
				
				int duplicateCnt = assetStatisticsDAO.selectAssetDuplicateCount(checkParamMap);
				
				if(duplicateCnt > 0) {
					isDuplicate = true;
					break;
				}
			}
		}
			
		return isDuplicate;
	}

	//hmsong cmdb 개선 작업 Start
	@Override
	public HashMap searchConfTypeNm(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchConfTypeNm(paramMap);
	}

	@Override
	public List searchConfTypeTree(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return assetAutomationDAO.searchConfTypeTree(paramMap);
	}
	//hmsong cmdb 개선 작업 End
}
