package com.nkia.itg.itam.automation.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.drools.core.util.StringUtils;
import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.automation.core.AssetAutomationComponents;

import egovframework.com.cmm.EgovMessageSource;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 자산원장 화면 자동화
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2016. 11. 11. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Component("webixUIHandler")
public class WebixUIHandler {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "assetAutomationComponents")
	private AssetAutomationComponents assetAutomationComponents;
	
	/**
	 * 필드 수정은 일어나야 하지만, 입력 불가로 상태로 생성해야 하는 필드의 컬럼 ID를 아래 배열에 추가해주세요.
	 * (자산 자동화에서 필드를 조회 속성으로 설정해두면, 자산 수정시 해당 필드값이 수정되지 않기 때문)
	 */
	// 아래 속성(컬럼명)들은 출력유형이 '수정'이지만, 입력불가 형태로 출력됩니다.
	private static final String[] EXCEPT_READ_ONLY_COLUMN_IDS = {
		"SW_GROUP_OPTION_ID",
		"FLO_DIT_CD"
	};
	
	// concat을 수행할 자바스크립트 함수
	private static final String ARRAY_CONCAT			= "new Array().concat";

	// BRACKET : 괄호 , BRACE : 중괄호
	private static final String LEFT_BRACKET			= "(";
	private static final String RIGHT_BRACKET			= ")";
	private static final String LEFT_BRACE				= "{";
	private static final String RIGHT_BRACE				= "}";
	private static final String LEFT_SQUARE_BRACKET		= "[";
	private static final String RIGHT_SQUARE_BRACKET	= "]";
	
	// 기능성 특수 문자들
	private static final String SEMI_COLON				= ";";
	private static final String COLON					= ":";
	private static final String SINGLE_QUOTES			= "'";
	private static final String DOUBLE_QUOTES			= "\"";
	private static final String COMMA					= ",";
	private static final String DOT						= ".";
	
	// 날짜 형식
	private static final String DATE_TYPE_DAY			= "YYYY-MM-DD";
	private static final String DATE_TYPE_TIME			= "YYYY-MM-DD HH24:MI";
	private static final String DATE_TYPE_HOUR			= "HH24";
	private static final String DATE_TYPE_MIN			= "MI";
	
	// 팝업 관련
	private static final String POPUP_SUFFIX			= "_POP";
	private static final String POPUP_NAME_FIELD_SUFFIX	= "_POP_NM";
	private static final String BUTTON_SUFFIX			= "_BTN";
	private static final String CLEAR_BUTTON_SUFFIX		= "_CLR_BTN";
	
	// Field 생성 Function
	private static final String EDITOR_FORM				= "createFormComp";
	private static final String TEXT					= "createTextFieldComp";
	private static final String TEXTAREA				= "createTextAreaFieldComp";
	private static final String COMBO					= "createCodeComboBoxComp";
	private static final String DATE					= "createDateFieldComp";
	private static final String CHECKBOX				= "createMultiSelectFieldComp";
	private static final String RADIO					= "createCodeRadioComp";
	private static final String UNION_FIELD				= "createUnionFieldComp";
	private static final String SEARCH_FIELD			= "createSearchFieldComp";
	private static final String BUTTON					= "createBtnComp";
	
	// 컴포넌트 생성 Function
	private static final String GRID_COMPONENT			= "createItamAutomationGridComponent";
	private static final String TAB_COMPONENT			= "createItamAutomationTabComponent";
	
	// 팝업 window 오픈 Function
	private static final String POPUP_OPEN				= "createPopupForAssetDetail";
	// 팝업 필드 초기화 버튼 이벤트 Function
	private static final String POPUP_CLEAR				= "clickPopupClearBtn";
	// 패널 버튼 생성 Function
	private static final String PANEL_BUTTON			= "createItamAutomationButton";
	// 저장 버튼 생성 Function
	private static final String SAVE_BUTTON				= "clickPopupSaveBtn";
	
	
	/**
	 * 여러 개의 아이템들을 하나의 rows로 묶어준다.
	 * 
	 * @param itemArray
	 * @return String
	 * @throws NkiaException
	 */
	public String concatArrayItems(JSONArray itemArray) throws NkiaException {
		
		String result = "";
		
		for(int i=0; i<itemArray.size(); i++) {
			if(i > 0) {
				result += ("," + (String)itemArray.get(i));
			} else {
				result = (String)itemArray.get(i);
			}
		}
		
		return ARRAY_CONCAT + LEFT_BRACKET + result + RIGHT_BRACKET;
	}
	
	/**
	 * Tabbar Item
	 * 
	 * @param entityMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createTabBarItemJsonString(HashMap entityMap) throws NkiaException {
		
		JSONObject tabBarItemObject = new JSONObject();
		
		String id = (String) entityMap.get("ENTITY_ID");
		String value = (String) entityMap.get("ENTITY_NM");

		tabBarItemObject.put("id", attachQuotes(id));
		tabBarItemObject.put("value", attachQuotes(value));

		return tabBarItemObject.toString();
	}
	
	/**
	 * Tab Page
	 * 
	 * @param entityMap
	 * @param tabInnerItem
	 * @return String
	 * @throws NkiaException
	 */
	public String createTabPageJsonString(HashMap entityMap, String tabInnerItem) throws NkiaException {
		
		JSONObject tabPageObject = new JSONObject();
		
		String id = (String) entityMap.get("ENTITY_ID");

		tabPageObject.put("id", attachQuotes(id));
		tabPageObject.put("rows", tabInnerItem);

		return tabPageObject.toString();
	}
	

	/**
	 * Editor Form
	 * 
	 * @param dataList
	 * @param hiddenList
	 * @param entityMap
	 * @param formArray
	 * @param btnArray
	 * @return String
	 * @throws NkiaException
	 */
	public String createEditorFormJsonString(List<HashMap> dataList, List<HashMap> hiddenList, HashMap entityMap, JSONArray formArray, String panelBtns) 
			throws NkiaException {

		JSONObject editorFormObject = new JSONObject();
		
		JSONObject formHeaderObject = new JSONObject();
		JSONObject formFieldsObject = new JSONObject();
		JSONObject hiddenFieldsObject = new JSONObject();
		
		JSONObject itemObject = null;
		JSONArray itemArray = new JSONArray();
		
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String classId = (String) entityMap.get("class_id");
		String classType = (String) entityMap.get("class_type");
		String entityId = (String) entityMap.get("ENTITY_ID");
		String upId = (String) entityMap.get("UP_ENTITY_ID");
		String entityNm = (String) entityMap.get("ENTITY_NM");
		int colSize = StringUtil.parseInteger(entityMap.get("COL_SIZE"));
		String ems_id = StringUtil.parseString(entityMap.get("ems_id"));
		String dca_id = StringUtil.parseString(entityMap.get("dca_id"));
		
		String confType = (String) entityMap.get("conf_type");
		String entity_mng_type = (String) entityMap.get("entity_mng_type");
		
		// 자산 폼 필드의 화면 표시 열 수 (기본값 2열)
		if(colSize < 1) {
			colSize = 2;
		}
		
		// 아이템 객체들(필드) 세팅
		for (int i = 0; i < dataList.size(); i++) {

			HashMap dataMap = dataList.get(i);
			dataMap.put("EMS_ID", ems_id);
			dataMap.put("DCA_ID", dca_id);
			dataMap.put("class_type", classType);
			dataMap.put("CLASS_ID", classId);
			dataMap.put("CONF_ID", confId);
			dataMap.put("CONF_TYPE", confType);
			
			String htmlType = (String) dataMap.get("HTML_TYPE");
			int colspan = dataMap.get("COLSPAN") != null ? ((BigDecimal)dataMap.get("COLSPAN")).intValue() : 1;
			
			String itemJsonString = "";

			//HTML 유형별로 만들어주어야 함
			if ("TEXT".equalsIgnoreCase(htmlType)) { 
				// 텍스트 필드
				itemJsonString = createTextFieldJsonString(dataMap);
			} else if ("TEXTAREA".equalsIgnoreCase(htmlType)) { 
				// 텍스트 박스
				itemJsonString = createTextAreaFieldJsonString(dataMap);
			} else if ("DATE".equalsIgnoreCase(htmlType)) { 
				// 날짜
				itemJsonString = createDateFieldJsonString(dataMap);
			} else if ("SELECT".equalsIgnoreCase(htmlType)) { 
				// 콤보
				itemJsonString = createComboFieldJsonString(dataMap);
			} else if ("RADIO".equalsIgnoreCase(htmlType)) { 
				// 라디오
				itemJsonString = createRadioFieldJsonString(dataMap);
			} else if ("CHECK".equalsIgnoreCase(htmlType)) { 
				// 체크
				itemJsonString = createCheckFieldJsonString(dataMap);
			} else if ("POPUP".equalsIgnoreCase(htmlType)) { 
				// 팝업
				itemJsonString = createPopUpFieldJsonString(hiddenList, dataMap, entityMap);
			} else if ("POPUPASSET".equalsIgnoreCase(htmlType)) { 
				// 팝업 + 연계자산
				itemJsonString = createPopUpAssetFieldJsonString(hiddenList, dataMap, entityMap);
			} else if ("POPUPCLUSTER".equalsIgnoreCase(htmlType)) { 
				// 팝업 + 클러스터
				itemJsonString = createPopUpClusterFieldJsonString(hiddenList, dataMap, entityMap);
			} else if ("POPUPEMS".equalsIgnoreCase(htmlType)) { 
				// 팝업 + EMS
				itemJsonString = createPopUpEmsFieldJsonString(hiddenList, dataMap, entityMap);
			} else if ("DUMMY".equalsIgnoreCase(htmlType)) { 
				// 더미필드
				dataMap.put("dummyFieldIndex", i);
				itemJsonString = createTextDummyFieldJsonString(dataMap);
			} else if ("POPUPCONF".equalsIgnoreCase(htmlType)) { 
				// 팝업 + 연계구성
				itemJsonString = createPopUpConfFieldJsonString(hiddenList, dataMap, entityMap);
			}
			
			itemObject = new JSONObject();
			itemObject.put("item", itemJsonString);
			itemObject.put("colspan", colspan);
			
			itemArray.add(itemObject);
		}
		
		// 히든 객체 세팅
		if (hiddenList != null) {
			for (int i = 0; i < hiddenList.size(); i++) {
				HashMap hiddenMap = hiddenList.get(i);
				hiddenFieldsObject.put((String)hiddenMap.get("MK_COMP_NM"), "''");
				// 예를 들면 아래와 같이 세팅 함 (빈 값이므로 싱글따옴표 필수)
				// {
				//    asset_id : "''" ,
				//    conf_nm : "''" ,
				//    ...
				// }
			}
		}
		
		// 폼 필드 내부 요소 구성
		formFieldsObject.put("colSize", colSize);
		formFieldsObject.put("items", itemArray);
		
		// 폼 헤더 내부 요소 구성
		formHeaderObject.put("title", attachQuotes(entityNm));
		formHeaderObject.put("buttons", panelBtns);
		
		// 최종적으로 에디터 폼 구성
		editorFormObject.put("id", attachQuotes(entityId));
		editorFormObject.put("header", formHeaderObject);
		editorFormObject.put("fields", formFieldsObject);
		editorFormObject.put("hiddens", hiddenFieldsObject);
		
		// Entity에 접근하기 위해 에디터 폼에 부가 속성 구성
		editorFormObject.put("entity_id", attachQuotes(entityId));
		editorFormObject.put("comp_id", attachQuotes(entityId));
		editorFormObject.put("up_id", attachQuotes(upId));
		editorFormObject.put("type", attachQuotes("F"));
		editorFormObject.put("entity_type", attachQuotes("MANUAL"));
		editorFormObject.put("entity_mng_type", attachQuotes(entity_mng_type));
		
		// 폼 아이템(formItemIds) 세팅 (화면에서 데이터 바인딩을 하기 위한 배열)
		JSONObject formItemsObject = new JSONObject();
		formItemsObject.put("entity_id", entityId);
		formItemsObject.put("comp_id", entityId);
		formItemsObject.put("up_id", upId);
		formItemsObject.put("type", "F");
		formItemsObject.put("entity_type", "MANUAL");
		formItemsObject.put("entity_mng_type", entity_mng_type);
		
		formArray.add(formItemsObject);
		
		return EDITOR_FORM + LEFT_BRACKET + editorFormObject.toString() + RIGHT_BRACKET;
	}
	
	
	private String createTextDummyFieldJsonString(HashMap dataMap) throws NkiaException {
		
		JSONObject textFieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String unit = (String) dataMap.get("UNIT");
		String dummyFieldIndex = String.valueOf(dataMap.get("dummyFieldIndex"));
		
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		
		if(readOnly) {
			// 읽기전용 add
			textFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if( dataMap.containsKey("AM_VALUE") ){
			textFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		
		textFieldProp.put("label", attachQuotes(label));
		textFieldProp.put("name", attachQuotes(name  + dummyFieldIndex));
		textFieldProp.put("id", attachQuotes(name));
		
		textFieldProp.put("isProcAfterHide", true);
		return TEXT + LEFT_BRACKET + textFieldProp.toString() + RIGHT_BRACKET;
	}
	/**
	 * Grid
	 * 
	 * @param attrList
	 * @param entityMap
	 * @param formArray
	 * @param userId
	 * @return String
	 * @throws NkiaException
	 */
	public String createGridComponentJsonString(List<HashMap> attrList, HashMap entityMap, JSONArray formArray, String userId) throws NkiaException {
		
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String classType = (String) entityMap.get("class_type");
		String entityId = (String) entityMap.get("ENTITY_ID");
		String entityNm = (String) entityMap.get("ENTITY_NM");
		String upEntityId = (String) entityMap.get("UP_ENTITY_ID");
		String componentType = (String) entityMap.get("COMP_TYPE");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String view_type = (String) entityMap.get("view_type");
		String atchFileId = (String) entityMap.get("atch_file_id");
		String emsId = (String) entityMap.get("ems_id");
		String dcaId = (String) entityMap.get("dca_id");
//		String businessTypeCd = (String) entityMap.get("business_type_cd"); //hmsong cmdb 개선 작업 : 용도 불분명
		String entityMngType = (String) entityMap.get("entity_mng_type"); //entity_mng_type 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
		String gridType = "SL";
		
		JSONObject gridProp = new JSONObject();
		
		gridProp.put("id", attachQuotes(entityId));
		gridProp.put("title", attachQuotes(entityNm));
		gridProp.put("asset_id", attachQuotes(assetId));
		gridProp.put("conf_id", attachQuotes(confId));
		gridProp.put("class_type", attachQuotes(classType));
		gridProp.put("authType", attachQuotes(authType));
		gridProp.put("view_type",attachQuotes(view_type));
		gridProp.put("component_type",attachQuotes(componentType));
		gridProp.put("conn_user_id",attachQuotes(userId));
		gridProp.put("entity_id", attachQuotes(entityId));
		gridProp.put("entity_nm", attachQuotes(entityNm));
		gridProp.put("ems_id", attachQuotes(emsId));
		gridProp.put("dca_id", attachQuotes(dcaId));
		gridProp.put("entity_mng_type", attachQuotes(entityMngType));
//		gridProp.put("business_type_cd", attachQuotes(businessTypeCd)); //hmsong 사용하지 않는 속성 파라미터
		
		if(atchFileId != null && !atchFileId.equals("")) {
			gridProp.put("atch_file_id",attachQuotes(atchFileId));
		}
		
		JSONArray itemArray = JSONArray.fromObject(attrList);
		gridProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		
		// 탭 그리드의 경우 TL 타입을 주어야 한다.
		if ("REL_INFRA".equalsIgnoreCase(componentType)) {
			gridType = "TL";
		} else if ("INSTALL".equalsIgnoreCase(componentType)) {
			gridType = "TL";
			// 설치 정보는 그리드헤더가 DB로 관리 되므로, DB를 조회하여 get한 값으로 컬럼정보를 생성한다.
			JSONObject tabGridColumn = getInstallGridColumnResource(attrList, entityMap);
			gridProp.put("columnInfo",tabGridColumn);
		}
		
		// 폼 아이템(formItemIds) 세팅 (화면에서 데이터 바인딩을 하기 위한 배열)
		JSONObject formItemsObject = new JSONObject();
		
		// 탭 리스트(TL)의 경우 탭의 수만큼 반복하여 탭마다 존재하는 그리드의 정보를 각각 formItemsObject 에 담아준다.
		if("TL".equals(gridType)) {
			if (attrList != null && attrList.size() > 0) {
				for (int i = 0; i < attrList.size(); i++) {
					HashMap dataMap = new HashMap();
					dataMap = attrList.get(i);
					formItemsObject.put("entity_id", entityId);
					formItemsObject.put("comp_id", dataMap.get("CODE_ID"));
					formItemsObject.put("up_id", upEntityId);
					formItemsObject.put("type", gridType);
					formItemsObject.put("entity_type", componentType);
					formItemsObject.put("entity_mng_type", entityMngType);
					formArray.add(formItemsObject);
				}
			}
		} else {
			// 탭 리스트가 아닐 경우, 반복문 없이 현재 컴포넌트의 정보를 그대로 formItemsObject 에 담아준다.
			formItemsObject.put("entity_id", entityId);
			formItemsObject.put("comp_id", entityId);
			formItemsObject.put("up_id", upEntityId);
			formItemsObject.put("type", gridType);
			formItemsObject.put("entity_type", componentType);
			formItemsObject.put("entity_mng_type", entityMngType);
			if(atchFileId != null && !atchFileId.equals("")) {
				formItemsObject.put("atch_file_id",atchFileId);
			}
			formArray.add(formItemsObject);
		}
		
		
		
		return GRID_COMPONENT + LEFT_BRACKET + gridProp.toString() + RIGHT_BRACKET;
	}
	
	
	/**
	 * Tab Component
	 * 
	 * @param attrList
	 * @param entityMap
	 * @param formArray
	 * @param userId
	 * @return String
	 * @throws NkiaException
	 */
	public String createTabComponentJsonString(List<HashMap> attrList, HashMap entityMap, JSONArray formArray) throws NkiaException {
		
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String classType = (String) entityMap.get("class_type");
		String entityId = (String) entityMap.get("ENTITY_ID");
		String entityNm = (String) entityMap.get("ENTITY_NM");
		String upEntityId = (String) entityMap.get("UP_ENTITY_ID");
		String componentType = (String) entityMap.get("COMP_TYPE");
		String authType = (String) entityMap.get("AUTH_TYPE");
		String view_type = (String) entityMap.get("view_type");
		String ins_user_id_nm = (String) entityMap.get("ins_user_id_nm");
		String ins_dt = (String) entityMap.get("ins_dt");
//		String tangibleAssetYn = (String) entityMap.get("tangible_asset_yn");	//hmsong cmdb 개선 작업 : 사용하지 않는 속성 파라미터
		String compType = "TAB";
		
		//entity_mng_type 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
		String entityMngType = (String) entityMap.get("entity_mng_type");
		
		JSONObject tabProp = new JSONObject();
		
		tabProp.put("id", attachQuotes(entityId));
		tabProp.put("title", attachQuotes(entityNm));
		tabProp.put("asset_id", attachQuotes(assetId));
		tabProp.put("conf_id", attachQuotes(confId));
		tabProp.put("class_type", attachQuotes(classType));
		tabProp.put("authType", attachQuotes(authType));
		tabProp.put("view_type",attachQuotes(view_type));
		tabProp.put("component_type",attachQuotes(componentType));
		tabProp.put("ins_user_id_nm",attachQuotes(ins_user_id_nm));
		tabProp.put("ins_dt",attachQuotes(ins_dt));
		tabProp.put("entity_mng_type",attachQuotes(entityMngType));
//		tabProp.put("tangible_asset_yn", attachQuotes(tangibleAssetYn));	//hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
		
		// 현대커스텀
//		String centerAssetYn = (String) entityMap.get("center_asset_yn");	//hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//		tabProp.put("center_asset_yn", attachQuotes(centerAssetYn));		//hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
		
		JSONArray itemArray = JSONArray.fromObject(attrList);
		tabProp.put("itemArray", replaceValueQuotes(itemArray.toString()));
		
		// 폼 아이템(formItemIds) 세팅 (화면에서 데이터 바인딩을 하기 위한 배열)
		JSONObject formItemsObject = new JSONObject();
		formItemsObject.put("entity_id", entityId);
		formItemsObject.put("comp_id", entityId);
		formItemsObject.put("up_id", upEntityId);
		formItemsObject.put("type", compType);
		formItemsObject.put("entity_type", componentType);
		formItemsObject.put("entity_mng_type", entityMngType);
		formArray.add(formItemsObject);
		
		return TAB_COMPONENT + LEFT_BRACKET + tabProp.toString() + RIGHT_BRACKET;
	}
	
	
	/**
	 * 설치 정보는 그리드헤더가 DB로 관리 되므로, DB를 조회하여 get한 값으로 컬럼정보를 생성한다.
	 * 
	 * @param attrList, entityMap
	 * @return JSONObject
	 * @throws NkiaException
	 */
	private JSONObject getInstallGridColumnResource(List attrList, HashMap entityMap) throws NkiaException {
		
		JSONObject columnInfoObject = new JSONObject();
		
		for(int i=0;i<attrList.size();i++){
			
			HashMap attrMap = (HashMap)attrList.get(i);
			String codeId 	= StringUtil.parseString(attrMap.get("CODE_ID"));
			List<HashMap> installColList = (List<HashMap>)entityMap.get(codeId);
			
			StringBuffer header = new StringBuffer();
			StringBuffer text = new StringBuffer();
			StringBuffer width = new StringBuffer();
			StringBuffer xtype = new StringBuffer();
			StringBuffer align = new StringBuffer();
			StringBuffer flex = new StringBuffer();
			StringBuffer sortable = new StringBuffer();
			
			for(int j=0;j<installColList.size();j++){
				
				HashMap colDataMap = installColList.get(j);
				
				if(j > 0) {
					header.append(",");
					text.append(",");
					width.append(",");
					xtype.append(",");
					align.append(",");
					flex.append(",");
					sortable.append(",");
				}
				
				header.append(colDataMap.get("COL_ID"));
				text.append(colDataMap.get("COL_NM"));
				width.append(colDataMap.get("WIDTH"));
				xtype.append(colDataMap.get("XTYPE"));
				align.append(colDataMap.get("ALIGN"));
				flex.append(colDataMap.get("FLEX"));
				sortable.append(colDataMap.get("SORTABLE"));
			}
			
			JSONObject resourceObject = new JSONObject();
			resourceObject.put("gridHeader", SINGLE_QUOTES + header.toString() + SINGLE_QUOTES);
			resourceObject.put("gridText", SINGLE_QUOTES + text.toString() + SINGLE_QUOTES);
			resourceObject.put("gridWidth", SINGLE_QUOTES + width.toString() + SINGLE_QUOTES);
			resourceObject.put("gridXtype", SINGLE_QUOTES + xtype.toString() + SINGLE_QUOTES);
			resourceObject.put("gridAlign", SINGLE_QUOTES + align.toString() + SINGLE_QUOTES);
			resourceObject.put("gridFlex", SINGLE_QUOTES + flex.toString() + SINGLE_QUOTES);
			resourceObject.put("gridSortable", SINGLE_QUOTES + sortable.toString() + SINGLE_QUOTES);
			
			columnInfoObject.put(codeId, resourceObject);
		}
		
		return columnInfoObject;
	}

	/**
	 * 텍스트필드
	 * 
	 * @param dataMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createTextFieldJsonString(HashMap dataMap) throws NkiaException {

		JSONObject textFieldProp = new JSONObject();

		String classType = (String) dataMap.get("class_type");
		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		String colType = (String) dataMap.get("COL_TYPE");
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(name.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}
		
		if(!StringUtil.isNull(vtype)) {
			// 속성 제약조건
			if(vtype.equals("ip")){
				textFieldProp.put("inputType", attachQuotes(vtype));
			}else if(vtype.equals("number")){
				textFieldProp.put("inputType", attachQuotes("price"));
			}
		}
		if(maxLength>0) {
			// 최대 입력가능 문자 add
			textFieldProp.put("maxsize", maxLength);
		}
		if(notNull) {
			// 필수입력 add
			textFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			textFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// textFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			textFieldProp.put("emsYn", true);
			textFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			textFieldProp.put("dcaYn", true);
			textFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			textFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			textFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			textFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			textFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			textFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		
		textFieldProp.put("label", attachQuotes(label));
		textFieldProp.put("name", attachQuotes(name));
		textFieldProp.put("id", attachQuotes(name));
		if(colType!=null && "NUMBER".equals(colType)) {
			String inputType = "price";
			if(!StringUtil.isNull(vtype) && "numberDot".equals(vtype)) {
				inputType = "double";
				
				// UBIT는 소수점 2자리 사용
				textFieldProp.put("decimalSize", attachQuotes("2"));
			}
			textFieldProp.put("inputType", attachQuotes(inputType));
		}
		
		return TEXT + LEFT_BRACKET + textFieldProp.toString() + RIGHT_BRACKET;
	}

	/**
	 * 텍스트 박스
	 * 
	 * @param dataMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createTextAreaFieldJsonString(HashMap dataMap) throws NkiaException {

		JSONObject textAreafieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(name.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}

		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(maxLength>0) {
			// 최대 입력가능 문자 add
			textAreafieldProp.put("maxsize", maxLength);
		}
		if(notNull) {
			// 필수입력 add
			textAreafieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			textAreafieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// textAreafieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			textAreafieldProp.put("emsYn", true);
			textAreafieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			textAreafieldProp.put("dcaYn", true);
			textAreafieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			textAreafieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			textAreafieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			textAreafieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			textAreafieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}

		textAreafieldProp.put("label", attachQuotes(label));
		textAreafieldProp.put("name", attachQuotes(name));
		textAreafieldProp.put("id", attachQuotes(name));
		textAreafieldProp.put("resizable", true);
		
		return TEXTAREA + LEFT_BRACKET + textAreafieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 
	 * TODO 날짜 필드
	 * 
	 * @param dataMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createDateFieldJsonString(HashMap dataMap) throws NkiaException {

		JSONObject dateProp = new JSONObject();
		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String dateType = (String) dataMap.get("DATE_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		String dateFormat = "";
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(name.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}
		
		// 자동화에서 관리하는 날짜형식 별로 컴포넌트에서 사용할 포맷으로 변환
		if(DATE_TYPE_DAY.equalsIgnoreCase(dateType)) { //YYYY-MM-DD
			dateFormat = "D";
		} else if(DATE_TYPE_TIME.equalsIgnoreCase(dateType)) { //YYYY-MM-DD HH24:MI
			dateFormat = "DHM";
		} else if(DATE_TYPE_HOUR.equalsIgnoreCase(dateType)) { //HH24
			dateFormat = "D";
		} else if(DATE_TYPE_HOUR.equalsIgnoreCase(dateType)) { //MI
			dateFormat = "D";
		} else {
			dateFormat = "D";
		}
		
		if(notNull) {
			// 필수입력 add
			dateProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			dateProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// dateProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}

		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			dateProp.put("emsYn", true);
			dateProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			dateProp.put("dcaYn", true);
			dateProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			dateProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			dateProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			dateProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			dateProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}

		dateProp.put("label", attachQuotes(label));
		dateProp.put("name", attachQuotes(name));
		dateProp.put("id", attachQuotes(name));
		dateProp.put("dateType", attachQuotes(dateFormat));
		
		return DATE + LEFT_BRACKET + dateProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 콤보박스
	 * 
	 * @param dataMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createComboFieldJsonString(HashMap dataMap) throws NkiaException {

		JSONObject comboFieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		String code_grp_id = (String) dataMap.get("SYS_CODE");
		String url = (String) dataMap.get("REQ_URL");
		String classId = (String) dataMap.get("CLASS_ID");
		String confId = (String) dataMap.get("CONF_ID");
		
		if(!StringUtils.isEmpty(url)) {
			if(url.contains("?")) {
				url += "&class_id=";
			} else {
				url += "?class_id=";
			}
			
			url += classId;
			
			if(url.contains("?")) {
				url += "&conf_id=";
			} else {
				url += "?conf_id=";
			}
			
			url += confId;
		}
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(name.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}

		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(notNull) {
			// 필수입력 add
			comboFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			comboFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// comboFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			comboFieldProp.put("emsYn", true);
			comboFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			comboFieldProp.put("dcaYn", true);
			comboFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			comboFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			comboFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			comboFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			comboFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			comboFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}

		comboFieldProp.put("label", attachQuotes(label));
		comboFieldProp.put("name", attachQuotes(name));
		comboFieldProp.put("id", attachQuotes(name));
		comboFieldProp.put("code_grp_id", attachQuotes(code_grp_id));
		comboFieldProp.put("url", attachQuotes(url));
		comboFieldProp.put("attachChoice", true);
		
		return COMBO + LEFT_BRACKET + comboFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 체크박스
	 * 
	 * @param dataMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createCheckFieldJsonString(HashMap dataMap) throws NkiaException {

		JSONObject checkFieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		String code_grp_id = (String) dataMap.get("SYS_CODE");
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(name.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}

		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(notNull) {
			// 필수입력 add
			checkFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			checkFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// checkFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			checkFieldProp.put("emsYn", true);
			checkFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			checkFieldProp.put("dcaYn", true);
			checkFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			checkFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			checkFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			checkFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			checkFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			checkFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}

		checkFieldProp.put("label", attachQuotes(label));
		checkFieldProp.put("name", attachQuotes(name));
		checkFieldProp.put("id", attachQuotes(name));
		checkFieldProp.put("code_grp_id", attachQuotes(code_grp_id));
		
		return CHECKBOX + LEFT_BRACKET + checkFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 라디오
	 * 
	 * @param dataMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createRadioFieldJsonString(HashMap dataMap) throws NkiaException {

		JSONObject radioFieldProp = new JSONObject();

		String label = (String) dataMap.get("COL_NAME");
		String name = (String) dataMap.get("MK_COMP_NM");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		String code_grp_id = (String) dataMap.get("SYS_CODE");
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(name.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}

		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(notNull) {
			// 필수입력 add
			radioFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			radioFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// radioFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			radioFieldProp.put("emsYn", true);
			radioFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			radioFieldProp.put("dcaYn", true);
			radioFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			radioFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			radioFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			radioFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			radioFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			radioFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}
		
		radioFieldProp.put("label", attachQuotes(label));
		radioFieldProp.put("name", attachQuotes(name));
		radioFieldProp.put("id", attachQuotes(name));
		radioFieldProp.put("code_grp_id", attachQuotes(code_grp_id));
		
		return RADIO + LEFT_BRACKET + radioFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 팝업 필드
	 * 
	 * @param hiddenList
	 * @param dataMap
	 * @param entityMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createPopUpFieldJsonString(List<HashMap> hiddenList, HashMap dataMap, HashMap entityMap) 
			throws NkiaException {
		
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String classId = (String) entityMap.get("class_id");
		String classType = (String) entityMap.get("class_type");
		String viewType = (String) entityMap.get("view_type");
		String formId = (String) entityMap.get("ENTITY_ID");
		String confType = (String) dataMap.get("CONF_TYPE");
		
		String label = (String) dataMap.get("COL_NAME");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		
		String popupCode = (String) dataMap.get("POPUP_CODE");
		
		String mng_type = "";
		//html유형 팝업 - 구성
		if("CONF".equals(popupCode)) {
			mng_type = popupCode;

		}
		//html유형 팝업 - 자산
		if("ASSET".equals(popupCode)) {
			mng_type = popupCode;
		}
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		String popupShowType = (String) dataMap.get("POPUP_SHOW_TYPE");
		
		// 팝업 창 객체 ID, 실제 필드(히든)의 ID, 보여지는 필드의 ID, 초기화 버튼의 ID값 세팅
		String hiddenFieldId = (String) dataMap.get("MK_COMP_NM");
		String popUpWindowId = ( hiddenFieldId + POPUP_SUFFIX );
		String popUpNameFieldId = ( hiddenFieldId + POPUP_NAME_FIELD_SUFFIX );
		String clearButtonId = ( hiddenFieldId + CLEAR_BUTTON_SUFFIX );
		
		// 클릭 이벤트 Function에 전달할 인자값 세팅
		JSONObject functionProp = new JSONObject();
		functionProp.put("id", attachQuotes(popUpWindowId));
		functionProp.put("asset_id", attachQuotes(assetId));
		functionProp.put("conf_id", attachQuotes(confId));
		functionProp.put("class_id", attachQuotes(classId));
		functionProp.put("class_type", attachQuotes(classType));
		functionProp.put("view_type", attachQuotes(viewType));
		functionProp.put("auth_type", readOnly);
		functionProp.put("target_form_id", attachQuotes(formId));
		functionProp.put("popup_object_id", attachQuotes(popUpWindowId));
		functionProp.put("field_id", attachQuotes(hiddenFieldId));
		functionProp.put("name_field_id", attachQuotes(popUpNameFieldId));
		functionProp.put("name_field_suffix", attachQuotes(POPUP_NAME_FIELD_SUFFIX));
		functionProp.put("clear_btn_id", attachQuotes(clearButtonId));
		functionProp.put("popup_type", attachQuotes(popupCode));
		functionProp.put("popup_show_type", attachQuotes(popupShowType));
		
		functionProp.put("mng_type", attachQuotes(mng_type));
		functionProp.put("conf_type", attachQuotes(confType));
		functionProp.put("callFunc", POPUP_OPEN);
		
		String popUpFunction = null;
		
		/*
		if(popupCode.equals("ASSET") ){
			// 윈도우 펑션 호출
			popUpFunction = ( 
					"function() {" 
					 + "windowPopupOpen" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
					 + "}"
					);

		}else{
			// webix ui 펑션 호출
			popUpFunction = ( 
				"function() {" 
				+ "nkia.ui.utils.window" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
				+ "}"
			);
		}
		*/
		// hmsong cmdb 개선 작업 - 팝업 윈도우로만 호출하게 수정. windowPopupOpen -> 이 function이 BASE에 없음.
		popUpFunction = ( 
				"function() {" 
				+ "nkia.ui.utils.window" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
				+ "}"
		);
		
		// 초기화 버튼 클릭 이벤트 Function 생성
		String clearBtnFunction = ( 
			"function() {" 
			+ POPUP_CLEAR + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET 
			+ "}"
		);
		
		/**
		 * 1. 실제 코드값을 가지고 있는 필드는 Hidden 객체로 생성 되어야 하므로 폼에서 생성될 수 있도록 Hidden Array에 추가해준다.
		**/
		hiddenList.add(dataMap);
		
		/**
		 * 2. 보여지는 필드(NAME 필드)를 만들어준다.
		**/
		JSONObject popupNameFieldProp = new JSONObject();
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(hiddenFieldId.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}
		
		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(maxLength>0) {
			// 최대 입력가능 문자 add
		}
		if(notNull) {
			// 필수입력 add
			popupNameFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			popupNameFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// textFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			popupNameFieldProp.put("emsYn", true);
			popupNameFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			popupNameFieldProp.put("dcaYn", true);
			popupNameFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			popupNameFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			popupNameFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			popupNameFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			popupNameFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			popupNameFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}
		
		popupNameFieldProp.put("label", attachQuotes(label));
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("click", popUpFunction);
		
		String popupNameField = ( SEARCH_FIELD + LEFT_BRACKET + popupNameFieldProp.toString() + RIGHT_BRACKET );
		
		/**
		 * 3. 초기화 버튼을 만들어준다.
		**/
		JSONObject clearBtnProp = new JSONObject();
		
		clearBtnProp.put("label", "'초기화'");
		clearBtnProp.put("click", clearBtnFunction);
		
		if(readOnly) {
			// 읽기전용 add
			clearBtnProp.put("readonly", true);
		}
		
		String clearBtn = ( BUTTON + LEFT_BRACKET + clearBtnProp.toString() + RIGHT_BRACKET );
		
		
		/**
		 * 4. 만들어진 필드들로 Union 필드를 구성한다.
		**/
		JSONObject popupFieldProp = new JSONObject();
		JSONArray popupItems = new JSONArray();
		
		popupItems.add(popupNameField);
		
		if(!"ASSET".equals(popupCode)){
			// 자산원장 팝업이 아닌 경우에만 초기화 버튼 생성
			popupItems.add(clearBtn);
		}
		
		popupFieldProp.put("items", popupItems.toString());
		
		return UNION_FIELD + LEFT_BRACKET + popupFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 팝업+상위자산 필드
	 * 
	 * @param hiddenList
	 * @param dataMap
	 * @param entityMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createPopUpAssetFieldJsonString(List<HashMap> hiddenList, HashMap dataMap, HashMap entityMap) 
			throws NkiaException {
		
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String classId = (String) entityMap.get("class_id");
		String classType = (String) entityMap.get("class_type");
		String mngType = "ASSET";	// hmsong cmdb 개선 작업 - 이 메소드는 '자산'만 호출하는 팝업 생성 메소드입니다.
		String viewType = (String) entityMap.get("view_type");
		String formId = (String) entityMap.get("ENTITY_ID");
		
		String label = (String) dataMap.get("COL_NAME");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		
		String popupCode = (String) dataMap.get("POPUP_CODE");
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		String popupShowType = (String) dataMap.get("POPUP_SHOW_TYPE");
		
		// 팝업 창 객체 ID, 실제 필드(히든)의 ID, 보여지는 필드의 ID, 초기화 버튼의 ID값 세팅
		String hiddenFieldId = (String) dataMap.get("MK_COMP_NM");
		String popUpWindowId = ( hiddenFieldId + POPUP_SUFFIX );
		String popUpNameFieldId = ( hiddenFieldId + POPUP_NAME_FIELD_SUFFIX );
		String clearButtonId = ( hiddenFieldId + CLEAR_BUTTON_SUFFIX );
		
		// 클릭 이벤트 Function에 전달할 인자값 세팅
		JSONObject functionProp = new JSONObject();
		functionProp.put("id", attachQuotes(popUpWindowId));
		functionProp.put("asset_id", attachQuotes(assetId));
		functionProp.put("conf_id", attachQuotes(confId));
		functionProp.put("class_id", attachQuotes(classId));
		functionProp.put("class_type", attachQuotes(classType));
		functionProp.put("view_type", attachQuotes(viewType));
		functionProp.put("mng_type", attachQuotes(mngType));
		functionProp.put("auth_type", readOnly);
		functionProp.put("target_form_id", attachQuotes(formId));
		functionProp.put("popup_object_id", attachQuotes(popUpWindowId));
		functionProp.put("field_id", attachQuotes(hiddenFieldId));
		functionProp.put("name_field_id", attachQuotes(popUpNameFieldId));
		functionProp.put("name_field_suffix", attachQuotes(POPUP_NAME_FIELD_SUFFIX));
		functionProp.put("clear_btn_id", attachQuotes(clearButtonId));
		functionProp.put("popup_type", attachQuotes(popupCode));
		functionProp.put("popup_show_type", attachQuotes(popupShowType));
		functionProp.put("callFunc", POPUP_OPEN);
		
		String popUpFunction = null;
		String popUpFunctionAsset = null;
		
		
		// webix ui 펑션 호출
		popUpFunction = ( 
			"function() {" 
			+ "nkia.ui.utils.window" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
			+ "}"
		);
		
		// 초기화 버튼 클릭 이벤트 Function 생성
		String clearBtnFunction = ( 
			"function() {" 
			+ POPUP_CLEAR + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET 
			+ "}"
		);
		functionProp.put("id", attachQuotes(popUpWindowId+"Asset"));
		functionProp.put("popup_type", attachQuotes("ASSET"));
		// 윈도우 펑션 호출 + 상위자산조회 
		popUpFunctionAsset = ( 
				"function() {" 
				 + "windowPopupOpen" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
				 + "}"
				);
		/**
		 * 1. 실제 코드값을 가지고 있는 필드는 Hidden 객체로 생성 되어야 하므로 폼에서 생성될 수 있도록 Hidden Array에 추가해준다.
		**/
		hiddenList.add(dataMap);
		
		/**
		 * 2. 보여지는 필드(NAME 필드)를 만들어준다.
		**/
		JSONObject popupNameFieldProp = new JSONObject();
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(hiddenFieldId.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}
		
		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(maxLength>0) {
			// 최대 입력가능 문자 add
		}
		if(notNull) {
			// 필수입력 add
			popupNameFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			popupNameFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// textFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			popupNameFieldProp.put("emsYn", true);
			popupNameFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			popupNameFieldProp.put("dcaYn", true);
			popupNameFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			popupNameFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			popupNameFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			popupNameFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			popupNameFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			popupNameFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}
		
		popupNameFieldProp.put("label", attachQuotes(label));
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("click", popUpFunction);
		
		String popupNameField = ( SEARCH_FIELD + LEFT_BRACKET + popupNameFieldProp.toString() + RIGHT_BRACKET );
		
		/**
		 * 3. 초기화 버튼을 만들어준다.
		**/
		JSONObject clearBtnProp = new JSONObject();
		
		clearBtnProp.put("label", "'초기화'");
		clearBtnProp.put("click", clearBtnFunction);
		
		if(readOnly) {
			// 읽기전용 add
			clearBtnProp.put("readonly", true);
		}
		
		String clearBtn = ( BUTTON + LEFT_BRACKET + clearBtnProp.toString() + RIGHT_BRACKET );
		/**
		 * 3-1. 자산정보 버튼을 만들어준다.
		**/
		
		JSONObject popupNameFieldPropAsset = new JSONObject();
		popupNameFieldPropAsset = popupNameFieldProp;
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId+"Asset"));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId+"Asset"));
		popupNameFieldPropAsset.put("click", popUpFunctionAsset);
		popupNameFieldPropAsset.put("label", "'정보'");
		String assetBtn = ( BUTTON + LEFT_BRACKET + popupNameFieldPropAsset.toString() + RIGHT_BRACKET );
		
		/**
		 * 4. 만들어진 필드들로 Union 필드를 구성한다.
		**/
		JSONObject popupFieldProp = new JSONObject();
		JSONArray popupItems = new JSONArray();
		
		popupItems.add(popupNameField);
		
		popupItems.add(clearBtn);
		
		popupItems.add(assetBtn);
		
		popupFieldProp.put("items", popupItems.toString());
		
		return UNION_FIELD + LEFT_BRACKET + popupFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 팝업+상위자산 필드
	 * 
	 * @param hiddenList
	 * @param dataMap
	 * @param entityMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createPopUpConfFieldJsonString(List<HashMap> hiddenList, HashMap dataMap, HashMap entityMap) 
			throws NkiaException {
		
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String confType = (String) entityMap.get("conf_type");
		String mngType = "CONF"; //hmsong cmdb 개선 작업 - 이 메소드는 '구성'만 호출하는 팝업 생성 메소드 입니다.
		String viewType = (String) entityMap.get("view_type");
		String formId = (String) entityMap.get("ENTITY_ID");
		
		String label = (String) dataMap.get("COL_NAME");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		
		String popupCode = (String) dataMap.get("POPUP_CODE");
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		String popupShowType = (String) dataMap.get("POPUP_SHOW_TYPE");
		
		// 팝업 창 객체 ID, 실제 필드(히든)의 ID, 보여지는 필드의 ID, 초기화 버튼의 ID값 세팅
		String hiddenFieldId = (String) dataMap.get("MK_COMP_NM");
		String popUpWindowId = ( hiddenFieldId + POPUP_SUFFIX );
		String popUpNameFieldId = ( hiddenFieldId + POPUP_NAME_FIELD_SUFFIX );
		String clearButtonId = ( hiddenFieldId + CLEAR_BUTTON_SUFFIX );
		
		// 클릭 이벤트 Function에 전달할 인자값 세팅
		JSONObject functionProp = new JSONObject();
		functionProp.put("id", attachQuotes(popUpWindowId));
		functionProp.put("asset_id", attachQuotes(assetId));
		functionProp.put("conf_id", attachQuotes(confId));
		functionProp.put("conf_type", attachQuotes(confType));
		functionProp.put("mng_type", attachQuotes(mngType));
		functionProp.put("view_type", attachQuotes(viewType));
		functionProp.put("auth_type", readOnly);
		functionProp.put("target_form_id", attachQuotes(formId));
		functionProp.put("popup_object_id", attachQuotes(popUpWindowId));
		functionProp.put("field_id", attachQuotes(hiddenFieldId));
		functionProp.put("name_field_id", attachQuotes(popUpNameFieldId));
		functionProp.put("name_field_suffix", attachQuotes(POPUP_NAME_FIELD_SUFFIX));
		functionProp.put("clear_btn_id", attachQuotes(clearButtonId));
		functionProp.put("popup_type", attachQuotes(popupCode));
		functionProp.put("popup_show_type", attachQuotes(popupShowType));
		functionProp.put("callFunc", POPUP_OPEN);
		
		String popUpFunction = null;
		String popUpFunctionConf = null;
		
		
		// webix ui 펑션 호출
		popUpFunction = ( 
			"function() {" 
			+ "nkia.ui.utils.window" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
			+ "}"
		);
		
		// 초기화 버튼 클릭 이벤트 Function 생성
		String clearBtnFunction = ( 
			"function() {" 
			+ POPUP_CLEAR + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET 
			+ "}"
		);
		functionProp.put("id", attachQuotes(popUpWindowId+"Conf"));
		functionProp.put("popup_type", attachQuotes("CONF"));
		// 윈도우 펑션 호출 + 상위자산조회 
		popUpFunctionConf = ( 
				"function() {" 
				 + "windowPopupOpen" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
				 + "}"
				);
		/**
		 * 1. 실제 코드값을 가지고 있는 필드는 Hidden 객체로 생성 되어야 하므로 폼에서 생성될 수 있도록 Hidden Array에 추가해준다.
		**/
		hiddenList.add(dataMap);
		
		/**
		 * 2. 보여지는 필드(NAME 필드)를 만들어준다.
		**/
		JSONObject popupNameFieldProp = new JSONObject();
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(hiddenFieldId.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}
		
		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(maxLength>0) {
			// 최대 입력가능 문자 add
		}
		if(notNull) {
			// 필수입력 add
			popupNameFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			popupNameFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// textFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			popupNameFieldProp.put("emsYn", true);
			popupNameFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			popupNameFieldProp.put("dcaYn", true);
			popupNameFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			popupNameFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			popupNameFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			popupNameFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			popupNameFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			popupNameFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}
		
		popupNameFieldProp.put("label", attachQuotes(label));
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("click", popUpFunction);
		
		String popupNameField = ( SEARCH_FIELD + LEFT_BRACKET + popupNameFieldProp.toString() + RIGHT_BRACKET );
		
		/**
		 * 3. 초기화 버튼을 만들어준다.
		**/
		JSONObject clearBtnProp = new JSONObject();
		
		clearBtnProp.put("label", "'초기화'");
		clearBtnProp.put("click", clearBtnFunction);
		
		if(readOnly) {
			// 읽기전용 add
			clearBtnProp.put("readonly", true);
		}
		
		String clearBtn = ( BUTTON + LEFT_BRACKET + clearBtnProp.toString() + RIGHT_BRACKET );
		/**
		 * 3-1. 자산정보 버튼을 만들어준다.
		**/
		
		JSONObject popupNameFieldPropConf = new JSONObject();
		popupNameFieldPropConf = popupNameFieldProp;
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId+"Conf"));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId+"Conf"));
		popupNameFieldPropConf.put("click", popUpFunctionConf);
		popupNameFieldPropConf.put("label", "'정보'");
		String assetBtn = ( BUTTON + LEFT_BRACKET + popupNameFieldPropConf.toString() + RIGHT_BRACKET );
		
		/**
		 * 4. 만들어진 필드들로 Union 필드를 구성한다.
		**/
		JSONObject popupFieldProp = new JSONObject();
		JSONArray popupItems = new JSONArray();
		
		popupItems.add(popupNameField);
		
		popupItems.add(clearBtn);
		
		popupItems.add(assetBtn);
		
		popupFieldProp.put("items", popupItems.toString());
		
		return UNION_FIELD + LEFT_BRACKET + popupFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 팝업+클러스터 필드
	 * 
	 * @param hiddenList
	 * @param dataMap
	 * @param entityMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createPopUpClusterFieldJsonString(List<HashMap> hiddenList, HashMap dataMap, HashMap entityMap) 
			throws NkiaException {
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String confKindCd = "CLUSTER";	// hmsong cmdb 개선 작업 - 이 메소드는 클러스터만 호출하는 팝업 생성 메소드입니다.
		String mngType = "CONF";	// hmsong cmdb 개선 작업 - 클러스터는 기본적으로 구성
//		String classId = (String) entityMap.get("class_id");
//		String classType = (String) entityMap.get("class_type");
		String viewType = (String) entityMap.get("view_type");
		String formId = (String) entityMap.get("ENTITY_ID");
		
		String label = (String) dataMap.get("COL_NAME");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		
		String popupCode = (String) dataMap.get("POPUP_CODE");
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		String popupShowType = (String) dataMap.get("POPUP_SHOW_TYPE");
		
		// 팝업 창 객체 ID, 실제 필드(히든)의 ID, 보여지는 필드의 ID, 초기화 버튼의 ID값 세팅
		String hiddenFieldId = (String) dataMap.get("MK_COMP_NM");
		String popUpWindowId = ( hiddenFieldId + POPUP_SUFFIX );
		String popUpNameFieldId = ( hiddenFieldId + POPUP_NAME_FIELD_SUFFIX );
		String clearButtonId = ( hiddenFieldId + CLEAR_BUTTON_SUFFIX );
		
		// 클릭 이벤트 Function에 전달할 인자값 세팅
		JSONObject functionProp = new JSONObject();
		functionProp.put("id", attachQuotes(popUpWindowId));
		functionProp.put("asset_id", attachQuotes(assetId));
		functionProp.put("conf_id", attachQuotes(confId));
		functionProp.put("conf_kind_cd", attachQuotes(confKindCd));
		functionProp.put("mng_type", attachQuotes(mngType));
		functionProp.put("view_type", attachQuotes(viewType));
		functionProp.put("auth_type", readOnly);
		functionProp.put("target_form_id", attachQuotes(formId));
		functionProp.put("popup_object_id", attachQuotes(popUpWindowId));
		functionProp.put("field_id", attachQuotes(hiddenFieldId));
		functionProp.put("name_field_id", attachQuotes(popUpNameFieldId));
		functionProp.put("name_field_suffix", attachQuotes(POPUP_NAME_FIELD_SUFFIX));
		functionProp.put("clear_btn_id", attachQuotes(clearButtonId));
		functionProp.put("popup_type", attachQuotes(popupCode));
		functionProp.put("popup_show_type", attachQuotes(popupShowType));
		functionProp.put("callFunc", POPUP_OPEN);
		
		String popUpFunction = null;
		String popUpFunctionCluster = null;
		
		
		// webix ui 펑션 호출
		popUpFunction = ( 
			"function() {" 
			+ "nkia.ui.utils.window" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
			+ "}"
		);
		
		// 초기화 버튼 클릭 이벤트 Function 생성
		String clearBtnFunction = ( 
			"function() {" 
			+ POPUP_CLEAR + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET 
			+ "}"
		);
		functionProp.put("id", attachQuotes(popUpWindowId+"Cluster"));
		functionProp.put("popup_type", attachQuotes("CLUSTER"));
		// 윈도우 펑션 호출 + 클러스터에 묶여있는 자산조회  
		popUpFunctionCluster = ( 
				"function() {" 
				 + "windowPopupOpen" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
				 + "}"
				);
		/**
		 * 1. 실제 코드값을 가지고 있는 필드는 Hidden 객체로 생성 되어야 하므로 폼에서 생성될 수 있도록 Hidden Array에 추가해준다.
		**/
		hiddenList.add(dataMap);
		
		/**
		 * 2. 보여지는 필드(NAME 필드)를 만들어준다.
		**/
		JSONObject popupNameFieldProp = new JSONObject();
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(hiddenFieldId.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}
		
		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(maxLength>0) {
			// 최대 입력가능 문자 add
		}
		if(notNull) {
			// 필수입력 add
			popupNameFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			popupNameFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// textFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			popupNameFieldProp.put("emsYn", true);
			popupNameFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			popupNameFieldProp.put("dcaYn", true);
			popupNameFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			popupNameFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			popupNameFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			popupNameFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			popupNameFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			popupNameFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}
		
		popupNameFieldProp.put("label", attachQuotes(label));
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("click", popUpFunction);
		
		String popupNameField = ( SEARCH_FIELD + LEFT_BRACKET + popupNameFieldProp.toString() + RIGHT_BRACKET );
		
		/**
		 * 3. 초기화 버튼을 만들어준다.
		**/
		JSONObject clearBtnProp = new JSONObject();
		
		clearBtnProp.put("label", "'초기화'");
		clearBtnProp.put("click", clearBtnFunction);
		
		if(readOnly) {
			// 읽기전용 add
			clearBtnProp.put("readonly", true);
		}
		
		String clearBtn = ( BUTTON + LEFT_BRACKET + clearBtnProp.toString() + RIGHT_BRACKET );
		/**
		 * 3-1. 클러스터정보 버튼을 만들어준다.
		**/
		
		JSONObject popupNameFieldPropCluster = new JSONObject();
		popupNameFieldPropCluster = popupNameFieldProp;
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId+"Cluster"));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId+"Cluster"));
		popupNameFieldPropCluster.put("click", popUpFunctionCluster);
		popupNameFieldPropCluster.put("label", "'정보'");
		String assetBtn = ( BUTTON + LEFT_BRACKET + popupNameFieldPropCluster.toString() + RIGHT_BRACKET );
		
		/**
		 * 4. 만들어진 필드들로 Union 필드를 구성한다.
		**/
		JSONObject popupFieldProp = new JSONObject();
		JSONArray popupItems = new JSONArray();
		
		popupItems.add(popupNameField);
		
		popupItems.add(clearBtn);
		
		popupItems.add(assetBtn);
		
		popupFieldProp.put("items", popupItems.toString());
		
		return UNION_FIELD + LEFT_BRACKET + popupFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * 팝업+EMS 필드
	 * 
	 * @param hiddenList
	 * @param dataMap
	 * @param entityMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createPopUpEmsFieldJsonString(List<HashMap> hiddenList, HashMap dataMap, HashMap entityMap) 
			throws NkiaException {
		
		String assetId = (String) entityMap.get("asset_id");
		String confId = (String) entityMap.get("conf_id");
		String classId = (String) entityMap.get("class_id");
		String classType = (String) entityMap.get("class_type");
		String viewType = (String) entityMap.get("view_type");
		String formId = (String) entityMap.get("ENTITY_ID");
		String customerIdNm = (String) entityMap.get("customer_id_nm");
		String entityClassId = (String) entityMap.get("entity_class_id");
		String entityId = (String) entityMap.get("entity_id");
		
		String label = (String) dataMap.get("COL_NAME");
		String vtype = (String) dataMap.get("CONST_TYPE");
		String ems_id = (String) dataMap.get("EMS_ID");
		String dca_id = (String) dataMap.get("DCA_ID");
		String unit = (String) dataMap.get("UNIT");
		
		String popupCode = (String) dataMap.get("POPUP_CODE");
		
		int maxLength = StringUtil.parseInteger(dataMap.get("COL_LEN"));
		
		boolean notNull = StringUtil.parseBoolean(dataMap.get("NOT_NULL"), "Y");
		boolean readOnly = StringUtil.parseBoolean(dataMap.get("AUTH_TYPE"), "R");
		boolean emsYn = StringUtil.parseBoolean(dataMap.get("EMS_YN"), "Y");
		boolean tblEmsYn = StringUtil.parseBoolean(dataMap.get("TBL_EMS_YN"), "Y");
		boolean dcaYn = StringUtil.parseBoolean(dataMap.get("DCA_YN"), "Y");
		boolean tblDcaYn = StringUtil.parseBoolean(dataMap.get("TBL_DCA_YN"), "Y");
		String popupShowType = (String) dataMap.get("POPUP_SHOW_TYPE");
		
		// 팝업 창 객체 ID, 실제 필드(히든)의 ID, 보여지는 필드의 ID, 초기화 버튼의 ID값 세팅
		String hiddenFieldId = (String) dataMap.get("MK_COMP_NM");
		String popUpWindowId = ( hiddenFieldId + POPUP_SUFFIX );
		String popUpNameFieldId = ( hiddenFieldId + POPUP_NAME_FIELD_SUFFIX );
		String clearButtonId = ( hiddenFieldId + CLEAR_BUTTON_SUFFIX );
		
		// 클릭 이벤트 Function에 전달할 인자값 세팅
		JSONObject functionProp = new JSONObject();
		functionProp.put("id", attachQuotes(popUpWindowId));
		functionProp.put("asset_id", attachQuotes(assetId));
		functionProp.put("conf_id", attachQuotes(confId));
		functionProp.put("class_id", attachQuotes(classId));
		functionProp.put("class_type", attachQuotes(classType));
		functionProp.put("customer_id_nm", attachQuotes(customerIdNm));
		functionProp.put("entity_class_id", attachQuotes(entityClassId));
		functionProp.put("entity_id", attachQuotes(entityId));
		functionProp.put("view_type", attachQuotes(viewType));
		functionProp.put("auth_type", readOnly);
		functionProp.put("target_form_id", attachQuotes(formId));
		functionProp.put("popup_object_id", attachQuotes(popUpWindowId));
		functionProp.put("field_id", attachQuotes(hiddenFieldId));
		functionProp.put("name_field_id", attachQuotes(popUpNameFieldId));
		functionProp.put("name_field_suffix", attachQuotes(POPUP_NAME_FIELD_SUFFIX));
		functionProp.put("clear_btn_id", attachQuotes(clearButtonId));
		functionProp.put("popup_type", attachQuotes(popupCode));
		functionProp.put("popup_show_type", attachQuotes(popupShowType));
		functionProp.put("callFunc", POPUP_OPEN);
		
		String popUpFunction = null;
		
		// webix ui 펑션 호출
		popUpFunction = ( 
			"function() {" 
			+ "nkia.ui.utils.window" + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET + SEMI_COLON
			+ "}"
		);
		
		// 초기화 버튼 클릭 이벤트 Function 생성
		String clearBtnFunction = ( 
			"function() {" 
			+ POPUP_CLEAR + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET 
			+ "}"
		);
		// 저장 버튼 클릭 이벤트 Function 생성
		String saveBtnFunction = ( 
			"function() {" 
			+ SAVE_BUTTON + LEFT_BRACKET + functionProp.toString() + RIGHT_BRACKET 
			+ "}"
		);
		
		/**
		 * 1. 실제 코드값을 가지고 있는 필드는 Hidden 객체로 생성 되어야 하므로 폼에서 생성될 수 있도록 Hidden Array에 추가해준다.
		**/
		hiddenList.add(dataMap);
		
		/**
		 * 2. 보여지는 필드(NAME 필드)를 만들어준다.
		**/
		JSONObject popupNameFieldProp = new JSONObject();
		
		// 실제 수정되어야 하는 필드지만 readonly 상태만 가져야 하는 컬럼에 대한 처리
		for(String columnIds : EXCEPT_READ_ONLY_COLUMN_IDS) {
			if(hiddenFieldId.equals(columnIds)) {
				readOnly = true;
				break;
			}
		}
		
		if("regist".equals(viewType) || "info".equals(viewType) || "infmodify".equals(viewType)){
			readOnly = true;
		}
		
		if(!StringUtil.isNull(vtype)) {
			// 벨리데이션 add
		}
		if(maxLength>0) {
			// 최대 입력가능 문자 add
		}
		if(notNull) {
			// 필수입력 add
			popupNameFieldProp.put("required", true);
		}
		if(readOnly) {
			// 읽기전용 add
			popupNameFieldProp.put("readonly", true);
		}
		if(!StringUtil.isNull(unit)) {
			// 단위를 표시해주는 text add
			// textFieldProp.put("bottomLabel", attachQuotes(unit));
			// 단위는 라벨에 붙임
			label = label + ( " <font color=#0077ee>(단위 : "+unit+")</font>" );
		}
		
		if (emsYn && tblEmsYn && !"".equalsIgnoreCase(ems_id)) {
			popupNameFieldProp.put("emsYn", true);
			popupNameFieldProp.put("emsValue", attachQuotes((String)dataMap.get("EMS_VALUE")));
		}
		if (dcaYn && tblDcaYn && !"".equalsIgnoreCase(dca_id)) {
			popupNameFieldProp.put("dcaYn", true);
			popupNameFieldProp.put("dcaValue", attachQuotes((String)dataMap.get("DCA_VALUE")));
		}
		if( dataMap.containsKey("AM_VALUE") ){
			popupNameFieldProp.put("amValue", attachQuotes((String)dataMap.get("AM_VALUE")));
		}
		if( dataMap.containsKey("COL_EXPRESSION") ){
			String colExpression = (String)dataMap.get("COL_EXPRESSION");
			popupNameFieldProp.put("colExpression", attachQuotes(colExpression));
		}
		if( dataMap.containsKey("HELPER_TEXT") ){
			popupNameFieldProp.put("helper", attachQuotes((String)dataMap.get("HELPER_TEXT")));
		}
		if( dataMap.containsKey("HELPER_WIDTH") ){
			popupNameFieldProp.put("helperWidth", StringUtil.parseInteger(dataMap.get("HELPER_WIDTH")));
		}
		if( dataMap.containsKey("HELPER_HEIGHT") ){
			popupNameFieldProp.put("helperHeight", StringUtil.parseInteger(dataMap.get("HELPER_HEIGHT")));
		}

		popupNameFieldProp.put("label", attachQuotes(label));
		popupNameFieldProp.put("name", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("id", attachQuotes(popUpNameFieldId));
		popupNameFieldProp.put("click", popUpFunction);
		
		String popupNameField = ( SEARCH_FIELD + LEFT_BRACKET + popupNameFieldProp.toString() + RIGHT_BRACKET );
		
		/**
		 * 3. 초기화 버튼을 만들어준다.
		**/
		JSONObject clearBtnProp = new JSONObject();
		
		clearBtnProp.put("label", "'초기화'");
		clearBtnProp.put("click", clearBtnFunction);
		
		if(readOnly) {
			// 읽기전용 add
			clearBtnProp.put("readonly", true);
		}
		
		String clearBtn = ( BUTTON + LEFT_BRACKET + clearBtnProp.toString() + RIGHT_BRACKET );
		
		/**
		 * 3-1. 저장 버튼을 만들어준다.
		**/
		JSONObject saveBtnProp = new JSONObject();
		
		saveBtnProp.put("label", "'저장'");
		saveBtnProp.put("click", saveBtnFunction);
		saveBtnProp.put("type", "'form'");
		
		if(readOnly) {
			// 읽기전용 add
			saveBtnProp.put("readonly", true);
		}
		
		String saveBtn = ( BUTTON + LEFT_BRACKET + saveBtnProp.toString() + RIGHT_BRACKET );
		
		/**
		 * 4. 만들어진 필드들로 Union 필드를 구성한다.
		**/
		JSONObject popupFieldProp = new JSONObject();
		JSONArray popupItems = new JSONArray();
		
		popupItems.add(popupNameField);
		
		popupItems.add(clearBtn);
		
		popupItems.add(saveBtn);
		
		popupFieldProp.put("items", popupItems.toString());
		
		return UNION_FIELD + LEFT_BRACKET + popupFieldProp.toString() + RIGHT_BRACKET;
	}
	
	/**
	 * Json Object 변환시 파람 값에 더블 쿼츠 붙는거 바꾼다. TODO
	 * 
	 * @param baseString
	 * @return String
	 * @throws NkiaException
	 */
	public String replaceAllquotes(String baseString) throws NkiaException {
		return StringUtil.replaceAll(StringUtil.replaceAll(baseString, "\\\\", ""), "\"", "");
	}

	/**
	 * List를 Json Array로 바로 변환시에 파람 값에 더블 쿼츠 붙는거 바꾼다. TODO
	 * 
	 * @param baseString
	 * @return String
	 * @throws NkiaException
	 */
	public String replaceValueQuotes(String baseString) throws NkiaException {
		return StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.replaceAll(baseString, ":\"", ":'\""), "\",", "\"',"), "\"}", "'\"}");
	}

	/**
	 * 
	 * TODO 쿼츠 붙이기..
	 * 
	 * @param baseString
	 * @return String
	 * @throws NkiaException
	 */
	public String attachQuotes(String baseString) throws NkiaException {
		if (baseString != null) {
			if (baseString.indexOf("'") > 0) {
				baseString = DOUBLE_QUOTES + baseString + DOUBLE_QUOTES;
			} else if (baseString.indexOf("\"") > 0) {
				baseString = SINGLE_QUOTES + baseString + SINGLE_QUOTES;
			} else {
				baseString = SINGLE_QUOTES + baseString + SINGLE_QUOTES;
			}
		}
		return baseString;
	}

	/**
	 * 
	 * TODO 두개의 맵을 서로 싱크
	 * 
	 * @param baseMap
	 * @param targetMap
	 * @return targetMap
	 * @throws NkiaException
	 */
	public HashMap paramSync(HashMap baseMap, HashMap targetMap)throws NkiaException {
		Iterator iter = baseMap.keySet().iterator();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			targetMap.put(key, baseMap.get(key));
		}
		return targetMap;
	}

	/**
	 * 
	 * TODO SELECT 쿼리 생성
	 * 
	 * @param attrList
	 * @param hiddenList
	 * @param tblList
	 * @param joinList
	 * @param assetConfIdList
	 * @param entityMap
	 * @return String
	 * @throws NkiaException
	 */
	public String createSelectQuery(List<HashMap> attrList, List<HashMap> hiddenList, List<HashMap> tblList, List<HashMap> joinList,
			List<HashMap> assetConfIdList, HashMap entityMap) throws NkiaException {

		StringBuffer queryText = new StringBuffer();
		String conf_id = (String) entityMap.get("conf_id");
		String asset_id = (String) entityMap.get("asset_id");
		String class_id = (String) entityMap.get("class_id");
		String class_type = (String) entityMap.get("class_type");
		
		if(tblList != null && tblList.size()>0){
			queryText.append("SELECT\n");
			
			if(attrList != null){
				if (attrList.size() > 0) {
					for (int i = 0; i < attrList.size(); i++) {
		
						HashMap dataMap = attrList.get(i);
						boolean isAppend = true;
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						String popupFn = StringUtil.parseString(dataMap.get("POPUP_NM_FN"));
						String popupShowType = StringUtil.parseString(dataMap.get("POPUP_SHOW_TYPE"));
						
						// 실제 DB 생성 컬럼 데이터 타입
						String dbDataType = StringUtil.parseString(dataMap.get("COL_TYPE"));
		
						StringBuilder tempText = new StringBuilder();
						if (i > 0) {
							tempText.append(COMMA);
						}
						
						if (htmlType.equalsIgnoreCase("TEXT") || htmlType.equalsIgnoreCase("INFO")) {
							// 2017-04-10 정정윤 추가(현대커스텀) : 모델과 제조사는 Function을 통해 호출
//							String selectTableNm = "AM_INFRA";
//							if("AM_SW".equals(table)) {
//								selectTableNm = table;
//							}
//							if("MODEL".equals(colId)) {
//								tempText.append("FC_BS_NW_MODEL('"+conf_id+"', AM_ASSET.MODEL, '"+class_type+"') AS MODEL");
//							} else if("VENDOR_ID".equals(colId)) {
//								tempText.append("FC_BS_NW_VENDOR_ID('"+conf_id+"', AM_ASSET.VENDOR_ID, '"+class_type+"') AS VENDOR_ID");
//							} else {
								tempText.append(table + DOT + colId + " AS " + name);
								//queryText.append(table + DOT + colId + " AS " + name);
//							}
						} else if (htmlType.equalsIgnoreCase("TEXTAREA")) { // 텍스트 박스
							tempText.append(table + DOT + colId + " AS " + name);
							//queryText.append(table + DOT + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
							String dateType = (String) dataMap.get("DATE_TYPE");
							if(dbDataType!=null && ( "VARCHAR".equals(dbDataType) || "VARCHAR2".equals(dbDataType) )) {
								// VARCHAR로 관리되는 날짜컬럼의 경우에 대한 분기 추가
								tempText.append("TO_CHAR(TO_DATE(" + table + DOT + colId + "),'" + DATE_TYPE_DAY + "')" + " AS " + name);
								tempText.append(COMMA + "TO_CHAR(TO_DATE(" + table + DOT + colId + "),'" + DATE_TYPE_DAY + "')" + " AS " + name + "_DAY");
							} else {
								if (dateType.equals(DATE_TYPE_TIME)) {
									tempText.append("TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_TIME + "')" + " AS " + name);
									tempText.append(COMMA + "TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_DAY + "')" + " AS " + name + "_DAY");
									tempText.append(COMMA + "TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_HOUR + "')" + " AS " + name + "_HOUR");
									tempText.append(COMMA + "TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_MIN + "')" + " AS " + name + "_MIN");
								} else {
									tempText.append("TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_DAY + "')" + " AS " + name);
									tempText.append(COMMA + "TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_DAY + "')" + " AS " + name + "_DAY");
								}
							}
						} else if (htmlType.equalsIgnoreCase("SELECT")) {// 콤보
							tempText.append(table + DOT + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("RADIO")) {// 라디오
							tempText.append(table + DOT + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("CHECK")) {// 체크
							tempText.append(table + DOT + colId + " AS " + name);
						} else if (htmlType.equalsIgnoreCase("POPUP")) {// 팝업
							tempText.append(table + DOT + colId + " AS " + name);
							if (!"".equalsIgnoreCase(popupFn)) {
								tempText.append("\n");
								tempText.append(COMMA);
								String popupColIds = table + DOT + colId;
								
								// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
								popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
								
								popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
								
								// 팝업 출력유형에 따라 적용
								if("ID".equals(popupShowType)){
									tempText.append(table + DOT + colId + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}else{
									tempText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);									
								}
								
							}
						} else if (htmlType.equalsIgnoreCase("POPUPASSET") || htmlType.equalsIgnoreCase("POPUPCONF")) {// 팝업상위자산, 팝업상위구성
							tempText.append(table + DOT + colId + " AS " + name);
							if (!"".equalsIgnoreCase(popupFn)) {
								tempText.append("\n");
								tempText.append(COMMA);
								String popupColIds = table + DOT + colId;
								
								// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
								popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
								
								popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
								
								// 팝업 출력유형에 따라 적용
								if("ID".equals(popupShowType)){
									tempText.append(table + DOT + colId + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}else{
									tempText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);									
								}
								
							}
						} else if (htmlType.equalsIgnoreCase("POPUPCLUSTER")) {// 팝업클러스터
							tempText.append(table + DOT + colId + " AS " + name);
							if (!"".equalsIgnoreCase(popupFn)) {
								tempText.append("\n");
								tempText.append(COMMA);
								String popupColIds = table + DOT + colId;
								
								// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
								popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
								
								popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
								
								// 팝업 출력유형에 따라 적용
								if("ID".equals(popupShowType)){
									tempText.append(table + DOT + colId + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}else{
									tempText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);									
								}
								
							}
						} else if (htmlType.equalsIgnoreCase("POPUPEMS")) {// 팝업EMS
							tempText.append(table + DOT + colId + " AS " + name);
							if (!"".equalsIgnoreCase(popupFn)) {
								tempText.append("\n");
								tempText.append(COMMA);
								String popupColIds = table + DOT + colId;
								
								// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
								popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
								
								popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
								
								// 팝업 출력유형에 따라 적용
								if("ID".equals(popupShowType)){
									tempText.append(table + DOT + colId + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}else{
									tempText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);									
								}
								
							}
						} else {
							isAppend = false;
						}
						if(isAppend) {
							queryText.append(tempText.toString());
							queryText.append("\n");
						}
					}
				}
			}
			
			if(hiddenList != null){
				if (hiddenList.size() > 0) {
					for (int i = 0; i < hiddenList.size(); i++) {
		
						HashMap dataMap = hiddenList.get(i);
		
						String htmlType = (String) dataMap.get("HTML_TYPE");
						String colId = (String) dataMap.get("COL_ID");
						String name = (String) dataMap.get("MK_COMP_NM");
						String table = (String) dataMap.get("GEN_TABLE_NM");
						String dbDataType = (String) dataMap.get("COL_TYPE");
		
						if(!htmlType.equalsIgnoreCase("DUMMY")) {
							if (attrList != null) {
								if (attrList.size() > 0) {
									queryText.append(COMMA);
								} else {
									if (i > 0) {
										queryText.append(COMMA);
									}
								}
							}
						}
						
						if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
							String dateType = (String) dataMap.get("DATE_TYPE");
							if(dbDataType!=null && ( "VARCHAR".equals(dbDataType) || "VARCHAR2".equals(dbDataType) )) {
								// VARCHAR로 관리되는 날짜컬럼의 경우에 대한 분기 추가
								queryText.append("TO_CHAR(TO_DATE(" + table + DOT + colId + "),'" + DATE_TYPE_DAY + "')" + " AS " + name);
							} else {
								if (dateType.equals(DATE_TYPE_TIME)) {
									queryText.append("TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_TIME + "')" + " AS " + name + "\n");
								} else {
									queryText.append("TO_CHAR(" + table + DOT + colId + ",'" + DATE_TYPE_DAY + "')" + " AS " + name + "\n");
								}
							}
						} else if(htmlType.equalsIgnoreCase("DUMMY")) {
							queryText.append("");
						} else {
							queryText.append(table + DOT + colId + " AS " + name + "\n");
						}
					}
				}
			}
	
			queryText.append("FROM\n");
			
			if (tblList.size() > 0) {
				for (int i = 0; i < tblList.size(); i++) {
					HashMap dataMap = tblList.get(i);
					String table = (String) dataMap.get("GEN_TABLE_NM");
					if (i > 0) {
						queryText.append(COMMA);
					}
					queryText.append(table + "\n");
				}
			}
	
			queryText.append("WHERE\n");
	
			if(joinList != null){
				if (joinList.size() > 0) {
					for (int i = 0; i < joinList.size(); i++) {
		
						HashMap dataMap = joinList.get(i);
						String left = (String) dataMap.get("LEFT_COL_ID");
						String right = (String) dataMap.get("RIGHT_COL_ID");
		
						if (i > 0) {
							queryText.append("AND ");
						}
						queryText.append(left + " = " + right + "\n");
					}
					queryText.append("AND ");
				}
			}
	
			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				String gen_table_nm = (String) dataMap.get("GEN_TABLE_NM");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					
					queryText.append(gen_table_nm + DOT + col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
			
			//hmsong 구성 ID가 존재하는 경우 자산ID 조건절은 무시
			int attatchAsset = 0;
			if(attatchConf == 0) {
				//자산아이디가 있을경우..
				for (int i = 0; i < assetConfIdList.size(); i++) {
					HashMap dataMap = assetConfIdList.get(i);
					String col_id = (String) dataMap.get("COL_ID");
					if ("ASSET_ID".equalsIgnoreCase(col_id)) {
						if (attatchConf > 0) {
							queryText.append("AND ");
						}
						
						queryText.append(col_id + "='" + asset_id + "'" + "\n");
						attatchAsset = 1;
						break;
					}
				}
			}
	
			// 검색 조건이 없어서 에러날경우 대비
			if (attatchConf == 0 && attatchAsset == 0) {
				queryText.append(" ROWNUM=1\n");
			}
		}
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO 카운트 쿼리 생성
	 * 
	 * @param assetConfIdList
	 * @param entityMap
	 * @return
	 */
	public String createCountQuery(List<HashMap> assetConfIdList,HashMap entityMap){
		
		StringBuffer queryText = new StringBuffer();
		String conf_id = StringUtil.parseString(entityMap.get("conf_id"));
		String asset_id = StringUtil.parseString(entityMap.get("asset_id"));
		String tblNm = StringUtil.parseString(entityMap.get("gen_table_nm"));
		
		if(assetConfIdList != null && assetConfIdList.size()>0){
			
			queryText.append("SELECT COUNT(1) FROM " + tblNm + "\n");
			// 조건절시작
			queryText.append("WHERE\n");			

			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					
					queryText.append(col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
			
			//hmsong 구성 ID가 존재하는 경우 자산ID 조건절은 무시
			if(attatchConf == 0) {
				int attatchAsset = 0;
				//자산아이디가 있을경우..
				for (int i = 0; i < assetConfIdList.size(); i++) {
					HashMap dataMap = assetConfIdList.get(i);
					String col_id = (String) dataMap.get("COL_ID");
					if ("ASSET_ID".equalsIgnoreCase(col_id)) {
						if (attatchConf > 0) {
							queryText.append("AND ");
						}
						
						queryText.append(col_id + "='" + asset_id + "'" + "\n");
						attatchAsset = 1;
						break;
					}
				}
			}
		}
		return queryText.toString();
	}
	

	/**
	 * 
	 * TODO 원장 자동화 수정 쿼리 생성
	 * 
	 * @param attrList
	 * @param assetConfIdList
	 * @param updUserDtList
	 * @param tblColIdList
	 * @param entityMap
	 * @param currentData
	 * @return
	 * @throws NkiaException
	 */
	public String createUpdateQuery(List<HashMap> attrList
								  ,List<HashMap> assetConfIdList
								  ,List<HashMap> updUserDtList
								  ,List<HashMap> tblColIdList
								  ,HashMap entityMap
								  ,HashMap currentData) throws NkiaException{

		StringBuffer queryText = new StringBuffer();
		String conf_id = StringUtil.parseString(entityMap.get("conf_id"));
		String asset_id = StringUtil.parseString(entityMap.get("asset_id"));
		String user_id = StringUtil.parseString(entityMap.get("user_id"));
		//hmsong cmdb 개선 작업 : 용도 불분명
//		String asset_id_hi = StringUtil.parseString(entityMap.get("asset_id_hi"));
//		String div_hierachy;
//		if(asset_id_hi == null || "".equals(asset_id_hi)) {
//			div_hierachy = "M";
//		} else {
//			div_hierachy = "S";
//		}
		
		String tblNm = StringUtil.parseString(entityMap.get("gen_table_nm"));
		int cnt = 0;
		
		// 수정되는 컬럼이 있는 경우에만 query 생성	-- 20180118 좌은진
		if(tblColIdList != null && tblColIdList.size() > 0){

			queryText.append("UPDATE " + tblNm + " SET\n");
			
			for(int i=0;i<tblColIdList.size();i++){
				
				HashMap tblColDataMap = tblColIdList.get(i);
				String tblUpdateAbleCol = StringUtil.parseString(tblColDataMap.get("COL_ID"));
				
				if (attrList.size() > 0) {
					
					for (int j = 0; j < attrList.size(); j++) {
						
						HashMap dataMap = attrList.get(j);
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						//String popupFn = StringUtil.parseString(dataMap.get("POPUP_NM_FN"));
						String authType = StringUtil.parseString(dataMap.get("AUTH_TYPE"));
						String defaultVal = StringUtil.parseString(dataMap.get("DEFAULT_VAL"));
						String dateType = StringUtil.parseString(dataMap.get("DATE_TYPE"));
							
//						if (!"ASSET_ID".equalsIgnoreCase(colId)
//						 && !"CONF_ID".equalsIgnoreCase(colId)) {
						// hmsong cmdb 개선 작업 - 물리구성의 경우 ASSET_ID를 수정할 수 있음
						if (!"CONF_ID".equalsIgnoreCase(colId)) {	
							if(tblUpdateAbleCol.equalsIgnoreCase(colId)){
								
								//디폴트값이 없을때..
								if ("".equalsIgnoreCase(defaultVal)) {
									if (cnt > 0) {
										queryText.append(COMMA);
									}
									//날짜인경우..
									if("DATE".equalsIgnoreCase(htmlType)){
										queryText.append( colId + " = TO_DATE('"+StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.parseString(currentData.get(name)), "-", "")," ","")+"','"+dateType+"')");
									}else{
										if(null == currentData.get(name)){
											queryText.append( colId + " = '" + "" + "'");
										}else{
											queryText.append( colId + " = '"+StringUtil.replaceSingleQuotation(StringUtil.parseString(currentData.get(name)))+"'");
										}
									}
									queryText.append("\n");
								} else {//디폴트값이 있지만 upd_user_id와 upd_dt 는 따로 세팅 외의 컬럼을 넣기 위해..
									int compareCnt = 0;
									for(int k=0;k<updUserDtList.size();k++){
										HashMap updDataMap = updUserDtList.get(k);
										String updColId = StringUtil.parseString(updDataMap.get("COL_ID"));
										if(updColId.equalsIgnoreCase(colId)){
											compareCnt++;
										}
									}
									if(compareCnt == 0){//upd_user_id,upd_dt 가 아닌경우.. 
										if (cnt > 0) {
											queryText.append(COMMA);
										}
										if ("USER_ID".equalsIgnoreCase(defaultVal)) {
											queryText.append(colId + " =  '" + user_id+ "'");
										} else if ("SYSDATE".equalsIgnoreCase(defaultVal)) {
											queryText.append(colId + " =  SYSDATE");
										} else {
											queryText.append(colId + " =  ''");
										}
										queryText.append("\n");
									}
								}
								cnt++;
							}
						}
					}
				}
			}
			
			//upd_user_id 와 upd_dt 를 세팅한다.
			if(updUserDtList != null){
				for(int i=0;i<updUserDtList.size();i++){
					HashMap updColDataMap = updUserDtList.get(i);
					String updColId = StringUtil.parseString(updColDataMap.get("COL_ID"));
					if(attrList != null && tblColIdList.size() > 0){
						queryText.append(COMMA);
					}else{
						if(i>0){
							queryText.append(COMMA);
						}
					}
					if("UPD_USER_ID".equals(updColId)){
						queryText.append(updColId+" = '"+user_id+"'\n");
					}else if("UPD_DT".equals(updColId)){
						queryText.append(updColId+" = SYSDATE \n");
					}
					cnt++;
				}
			}
			
			// 조건절시작
			queryText.append("WHERE\n");
			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					
					queryText.append(col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
			
			//hmsong 구성 ID가 존재하는 경우 자산ID 조건절은 무시
			if(attatchConf == 0) {
				int attatchAsset = 0;
				//자산아이디가 있을경우..
				for (int i = 0; i < assetConfIdList.size(); i++) {
					HashMap dataMap = assetConfIdList.get(i);
					String col_id = (String) dataMap.get("COL_ID");
					if ("ASSET_ID".equalsIgnoreCase(col_id)) {
						if (attatchConf > 0) {
							queryText.append("AND ");
						}
						
						queryText.append(col_id + "='" + asset_id + "'" + "\n");
						attatchAsset = 1;
						break;
					}
				}
			}
		}
		
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO 원장 자동화 등록 쿼리 생성
	 * 
	 * @param attrList
	 * @param assetConfIdList
	 * @param updUserDtList
	 * @param tblColIdList
	 * @param entityMap
	 * @param currentData
	 * @return
	 * @throws NkiaException
	 */
	public String createInsertQuery(List<HashMap> attrList, List<HashMap> assetConfIdList, List<HashMap> insUserDtList, List<HashMap> updUserDtList, List<HashMap> tblColIdList,
			HashMap entityMap, HashMap currentData) throws NkiaException {

		StringBuffer queryText = new StringBuffer();
		StringBuffer colQueryText = new StringBuffer();
		StringBuffer valQueryText = new StringBuffer();
		
		String conf_id = StringUtil.parseString(entityMap.get("conf_id"));
		String asset_id = StringUtil.parseString(entityMap.get("asset_id"));
		String user_id = StringUtil.parseString(entityMap.get("user_id"));
		String tblNm = StringUtil.parseString(entityMap.get("gen_table_nm"));
//		String asset_id_hi = StringUtil.parseString(entityMap.get("asset_id_hi"));
//		String div_hierachy;
//		if(asset_id_hi == null || "".equals(asset_id_hi)) {
//			div_hierachy = "M";
//		} else {
//			div_hierachy = "S";
//		}
		
		int cnt = 0;
		boolean assetInsertFlag = false;
		if (tblColIdList != null) {
			
			for (int i = 0; i < tblColIdList.size(); i++) {

				HashMap tblColDataMap = tblColIdList.get(i);
				String tblUpdateAbleCol = StringUtil.parseString(tblColDataMap.get("COL_ID"));

				if (attrList.size() > 0) {
					for (int j = 0; j < attrList.size(); j++) {
							
						HashMap dataMap = attrList.get(j);
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						String authType = StringUtil.parseString(dataMap.get("AUTH_TYPE"));
						String defaultVal = StringUtil.parseString(dataMap.get("DEFAULT_VAL"));
						String dateType = StringUtil.parseString(dataMap.get("DATE_TYPE"));
						
						if (tblUpdateAbleCol.equalsIgnoreCase(colId) && !"ASSET_ID".equals(colId) && !"CONF_ID".equals(colId)) {
							// 디폴트값이 없을때..
							if ("".equalsIgnoreCase(defaultVal)) {
								if (cnt > 0) {
									colQueryText.append(COMMA);
									valQueryText.append(COMMA);
								}
								// 날짜인경우..
								if ("DATE".equalsIgnoreCase(htmlType)) {
									colQueryText.append(colId);
									valQueryText.append("TO_DATE('"+ StringUtil.replaceAll(StringUtil.replaceAll(StringUtil.parseString(currentData.get(name)), "-", ""), " ", "")+ "','" + dateType + "')");
								} else {
									colQueryText.append(colId);
									valQueryText.append("'" + StringUtil.replaceSingleQuotation(StringUtil.parseString(currentData.get(name))) + "'");
								}
								colQueryText.append("\n");
								valQueryText.append("\n");
							} else {// 디폴트값이 있지만 upd_user_id와 upd_dt 는 따로 세팅
									// 외의 컬럼을 넣기 위해..
								int compareCnt = 0;
								for (int k = 0; k < updUserDtList.size(); k++) {
									HashMap updDataMap = updUserDtList.get(k);
									String updColId = StringUtil.parseString(updDataMap.get("COL_ID"));
									if (updColId.equalsIgnoreCase(colId)) {
										compareCnt++;
									}
								}
								if (compareCnt == 0) {// upd_user_id,upd_dt 가 아닌경우..
									if (cnt > 0) {
										colQueryText.append(COMMA);
										valQueryText.append(COMMA);
									}
									if ("USER_ID".equalsIgnoreCase(defaultVal)) {
										colQueryText.append(colId);
										valQueryText.append("'" + user_id + "'");
									} else if ("SYSDATE".equalsIgnoreCase(defaultVal)) {
										colQueryText.append(colId);
										valQueryText.append("SYSDATE");
									} else {
										colQueryText.append(colId);
										valQueryText.append("''");
									}
									colQueryText.append("\n");
									valQueryText.append("\n");
								}
							}
							cnt++;
						}
					}
				}
			}
		}
		
		if (insUserDtList != null) { // ins_user_id 와 ins_dt 를 세팅한다.
			for (int i = 0; i < insUserDtList.size(); i++) {
				
				HashMap insColDataMap = insUserDtList.get(i);
				String insColId = StringUtil.parseString(insColDataMap.get("COL_ID"));
				
				if (cnt > 0) {
					colQueryText.append(COMMA);
					valQueryText.append(COMMA);
				} else {
					if (i > 0) {
						colQueryText.append(COMMA);
						valQueryText.append(COMMA);
					}
				}
				if ("INS_USER_ID".equals(insColId)) {
					colQueryText.append(insColId);
					valQueryText.append("'" + user_id + "'");
				} else if ("INS_DT".equals(insColId)) {
					colQueryText.append(insColId);
					valQueryText.append("SYSDATE");
				}
				colQueryText.append("\n");
				valQueryText.append("\n");
				cnt++;
			}
		}
		
		if (updUserDtList != null) { // upd_user_id 와 upd_dt 를 세팅한다.
			for (int i = 0; i < updUserDtList.size(); i++) {
				
				HashMap updColDataMap = updUserDtList.get(i);
				String updColId = StringUtil.parseString(updColDataMap.get("COL_ID"));
				
				if (cnt > 0) {
					colQueryText.append(COMMA);
					valQueryText.append(COMMA);
				} else {
					if (i > 0) {
						colQueryText.append(COMMA);
						valQueryText.append(COMMA);
					}
				}
				if ("UPD_USER_ID".equals(updColId)) {
					colQueryText.append(updColId);
					valQueryText.append("'" + user_id + "'");
				} else if ("UPD_DT".equals(updColId)) {
					colQueryText.append(updColId);
					valQueryText.append("SYSDATE");
				}
				colQueryText.append("\n");
				valQueryText.append("\n");
				cnt++;
			}
		}

		// 자산아이디가 있을경우..
		////////////////////////////////		
		for (int i = 0; i < assetConfIdList.size(); i++) {
			HashMap dataMap = assetConfIdList.get(i);
			String updColId = (String) dataMap.get("COL_ID");
			
			if ("ASSET_ID".equalsIgnoreCase(updColId)) {
				if (cnt> 0) {
					colQueryText.append(COMMA);
					valQueryText.append(COMMA);
				}
				colQueryText.append(updColId+ "\n");
				valQueryText.append("'" + asset_id + "'" + "\n");
				cnt++;
				break;
			}
		}

		int attatchConf = 0;
		// 구성 id가 있을경우
		for (int i = 0; i < assetConfIdList.size(); i++) {
			HashMap dataMap = assetConfIdList.get(i);
			String updColId = (String) dataMap.get("COL_ID");
			if ("CONF_ID".equalsIgnoreCase(updColId)) {
				if (cnt> 0){
					colQueryText.append(COMMA);
					valQueryText.append(COMMA);
				}
				colQueryText.append(updColId+ "\n");
				valQueryText.append("'" + conf_id + "'" + "\n");
				cnt++;
				break;
			}
		}
		////////////////////////////////
		
		if(colQueryText != null){
			queryText.append("INSERT INTO " + tblNm +" \n")
							.append("(\n")
							.append(colQueryText.toString()+" \n")
							.append(") VALUES (\n")
							.append(valQueryText.toString())
							.append(")");
			
		}
		
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO prefix 정보
	 * 
	 * @param resource_prefix
	 * @return
	 * @throws NkiaException
	 */
	public JSONArray getGridResource(String resource_prefix) throws NkiaException{
		
		JSONArray jsonArray = new JSONArray();
		
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".text")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".header")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".width")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".align")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".flex")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".sortable")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".xtype")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".tpl")+"'");
		jsonArray.add("'"+messageSource.getMessage(resource_prefix + ".hidden")+"'");
		
		return jsonArray;
	}
	
	/**
	 * 
	 * TODO 리스트 형태를 그리드 헤더 정보에 맞게 변환
	 * 
	 * @param collList
	 * @param col_id
	 * @param col_nm
	 * @return
	 * @throws NkiaException
	 */
	public JSONArray getGridResourceByList(List<HashMap> collList,String col_id,String col_nm) throws NkiaException{
		
		JSONArray jsonArray = new JSONArray();
		
		String text = "";
		String header = "";
		String width = "";
		String align = "";
		String flex = "";
		String sortable = "";
		String xtype = "";
		String tpl = "";
		String hidden = "";
		String attachCOMMA = "";
		
		for(int i=0;i<collList.size();i++){
			
			HashMap colDataMap = collList.get(i);
			if(i > 0 ){
				attachCOMMA = COMMA;
			}
			
			text 	+= attachCOMMA + StringUtil.parseString(colDataMap.get(col_nm));
			header 	+= attachCOMMA + StringUtil.parseString(colDataMap.get(col_id));
			width 	+= attachCOMMA + StringUtil.parseString(colDataMap.get("WIDTH"));
			align 	+= attachCOMMA + StringUtil.parseString(colDataMap.get("ALIGN"));
			flex 	+= attachCOMMA + StringUtil.parseString(colDataMap.get("FLEX"));
			sortable+= attachCOMMA + StringUtil.parseString(colDataMap.get("SORTABLE"));
			xtype 	+= attachCOMMA + StringUtil.parseString(colDataMap.get("XTYPE"));
			tpl 	+= attachCOMMA + StringUtil.parseString(colDataMap.get("TPL"));
			hidden 	+= attachCOMMA + StringUtil.parseString(colDataMap.get("HIDDEN"));
		}
		
		jsonArray.add("'"+text+"'");
		jsonArray.add("'"+header+"'");
		jsonArray.add("'"+width+"'");
		jsonArray.add("'"+align+"'");
		jsonArray.add("'"+flex+"'");
		jsonArray.add("'"+sortable+"'");
		jsonArray.add("'"+xtype+"'");
		jsonArray.add("'"+tpl+"'");
		jsonArray.add("'"+hidden+"'");
		
		return jsonArray;
	}
	
	public JSONArray getInstallCompareData(List<HashMap> collList) throws NkiaException{
		
		JSONArray jsonArray = new JSONArray();
		
		for(int i=0;i<collList.size();i++){
			
			HashMap colMap = collList.get(i);
			String compareCol = StringUtil.parseString(colMap.get("COMPARE_COL"));
			
			if("Y".equals(compareCol)){
				String col_id = StringUtil.parseString(colMap.get("COL_ID"));
				String ems_col_id = StringUtil.parseString(colMap.get("EMS_COL_ID"));
				String dca_col_id = StringUtil.parseString(colMap.get("DCA_COL_ID"));
				
				JSONObject compareData = new JSONObject();
				
				compareData.put("COL_ID", col_id);
				compareData.put("EMS_COL_ID", ems_col_id);
				compareData.put("DCA_COL_ID", dca_col_id);
				
				jsonArray.add(compareData);
			}
		}
		
		return jsonArray;
	}
	
	
	/**
	 * 
	 * TODO 헤더와 텍스트만 리턴
	 * 
	 * @param resource_prefix
	 * @return
	 * @throws NkiaException
	 */
	public HashMap getGridHeaderInfo(String resource_prefix)throws NkiaException{
		
		HashMap dataMap =new HashMap();
		
		String text = messageSource.getMessage(resource_prefix + ".text");
		String header = messageSource.getMessage(resource_prefix + ".header");
		
		String[] textArr = text.split(",");
		String[] headerArr = header.split(",");
		
		for(int i=0;i<textArr.length;i++){
			dataMap.put(textArr[i], headerArr[i]);
		}
		
		return dataMap;
	}
	
	/**
	 * 
	 * TODO 설치 정보 쿼리
	 * 
	 * @param installInfo
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallQuery(HashMap installInfo,List<HashMap> installColList, boolean isAdCompareData)throws NkiaException{
		
		StringBuffer installQuery = new StringBuffer();
		StringBuffer adCompareQuery = new StringBuffer();
		  
		String confId   = StringUtil.parseString(installInfo.get("conf_id"));
		String emsId   = StringUtil.parseString(installInfo.get("ems_id"));
		String dcaId   = StringUtil.parseString(installInfo.get("dca_id"));
		String tableNm = StringUtil.parseString(installInfo.get("TABLE_NM"));
		String viewType = StringUtil.parseString(installInfo.get("view_type"));	//@@ 고은규 수정
		if("info".equals(viewType) || "infmodify".equals(viewType)){				//@@ 고은규 수정
			tableNm = StringUtil.parseString(installInfo.get("INFO_TABLE_NM"));		//@@ 고은규 수정
		}
		String emsShowYn = StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
		String emsYn = StringUtil.parseString(installInfo.get("EMS_YN"));
		String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
		String dcaShowYn = StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
		String dcaYn = StringUtil.parseString(installInfo.get("DCA_YN"));
		String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
		String orderColumn = "";
		
		installQuery.append("SELECT\n");
		
		if( isAdCompareData ){
			adCompareQuery.append("SELECT\n");
		}
		
		for(int i=0;i<installColList.size();i++){
			
			HashMap installColMap = installColList.get(i);
			
			String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
			String dcaColId = StringUtil.parseString(installColMap.get("DCA_COL_ID"));
			String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
			
			if(i>0){
				installQuery.append(COMMA);
				if( isAdCompareData ){
					adCompareQuery.append(COMMA);
				}
			}
			
			if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
				installQuery.append(emsColId + "\n");
			}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
				installQuery.append(dcaColId + "\n");
			}else{
				installQuery.append(colId + "\n");
			}	
			
			if( isAdCompareData ){
				adCompareQuery.append(colId + "\n");
			}
			
			if("Y".equalsIgnoreCase(orderCol)){
				if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
					orderColumn = emsColId;
				}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
					orderColumn = dcaColId;
				}else{
					orderColumn = colId;
				}
			}
		}
		
		installQuery.append("FROM\n");
		if( isAdCompareData ){
			adCompareQuery.append("FROM\n");
		}
		
		if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
			installQuery.append(emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' \n");
		}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
			installQuery.append(dcaTblNm + "\n")
			.append("WHERE \n")
			.append("DEVICE_ID = '" + dcaId + "' \n");
			if( isAdCompareData ){
				adCompareQuery.append(tableNm + "\n")
				.append("WHERE \n")
				.append("CONF_ID = '" + confId + "'\n");
				
				installQuery.append("MINUS "+ adCompareQuery.toString() );
			}
		}else{
			installQuery.append(tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "'\n");
		}
		
		if( !"".equalsIgnoreCase(orderColumn) ){
			installQuery.append("ORDER BY " + orderColumn + " \n");
		}
		
		return installQuery.toString();
	}
	
	/**
	 * 
	 * TODO 설치정보 삭제 쿼리
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallDelQuery(HashMap dataMap)throws NkiaException{
		StringBuffer sb = new StringBuffer();
		String tableNm = StringUtil.parseString(dataMap.get("TABLE_NM"));
		String confId = StringUtil.parseString(dataMap.get("CONF_ID"));
		sb.append("DELETE FROM " + tableNm + " WHERE CONF_ID = '" +confId+ "'");
		return sb.toString();
	}
	
	/**
	 * 
	 * TODO 설치정보 등록 쿼리 
	 * 
	 * @param dataMap
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallInsQuery(HashMap dataMap,List<HashMap> installColList)throws NkiaException{
		
		StringBuffer querySb = new StringBuffer();
		StringBuffer colSb = new StringBuffer();
		StringBuffer insSb = new StringBuffer();
		
		String tableNm = StringUtil.parseString(dataMap.get("TABLE_NM"));
		String confId = StringUtil.parseString(dataMap.get("CONF_ID"));
		boolean confIdFlag = false;
		
		for(int i=0;i<installColList.size();i++){
			HashMap installColMap = installColList.get(i);
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			if("CONF_ID".equals(colId)){
				confIdFlag = true;
				break;
			}
		}
		
		if(!confIdFlag){
			colSb.append("CONF_ID"); 
			insSb.append("'"+confId+"'"); 
		}
		
		int roopCnt = 0;
		for(int i=0;i<installColList.size();i++){
			
			HashMap installColMap = installColList.get(i);
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			
			if(!confIdFlag){
				colSb.append(COMMA);
				insSb.append(COMMA); 
			}else{
				if(i>0){
					colSb.append(COMMA);
					insSb.append(COMMA); 
				}
			}
			colSb.append(colId + "\n");
			insSb.append("'"+StringUtil.parseString(dataMap.get(colId))+"'\n"); 
		}
		
		querySb.append("INSERT INTO " + tableNm + "( \n").append(colSb).append(") VALUES (").append(insSb + ")");
		return querySb.toString(); 	
	}
	
	/**
	 * map 의 키셋을 rowercase 로 바꿔준다.
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap replaceRowerKey(HashMap dataMap)throws NkiaException{
		HashMap returnMap = new HashMap();
		if(dataMap != null){
			Iterator iter = dataMap.keySet().iterator();
			
			while (iter.hasNext()) {
				String key = (String) iter.next();
				String value = StringUtil.parseString(dataMap.get(key));
				returnMap.put(key.toLowerCase(), value);
			}
		}
		return returnMap;
	}
	
	
	/**
	 * map 의 키셋을 rowercase 로 바꿔준다.
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap replaceAddRowerKey(HashMap dataMap)throws NkiaException{
		Iterator iter = dataMap.keySet().iterator();
		HashMap returnMap = new HashMap();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			returnMap.put(key.toLowerCase(), dataMap.get(key));
		}
		Iterator iter1 = returnMap.keySet().iterator();
		while (iter1.hasNext()) {
			String key = (String) iter1.next();
			dataMap.put(key, returnMap.get(key));
		}
		return dataMap;
	}
	
	
	/**
	 * 
	 * TODO
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public List getConfNmCodeResourceList()throws NkiaException{
		
		List headerList = new ArrayList();
		try{
		String header = messageSource.getMessage("grid.itam.confnmcode.header");
		String text = messageSource.getMessage("grid.itam.confnmcode.text");
		String code = messageSource.getMessage("grid.itam.confnmcode.code");
		
		String[] headerArr = header.split(",");
		String[] textArr = text.split(",");
		String[] codeArr = code.split(",");
		
		for(int i=0 ;i<headerArr.length;i++){
			
			HashMap dataMap = new HashMap();
			dataMap.put("COL_ID", headerArr[i]);
			dataMap.put("COL_NAME", textArr[i]);
			
			String sysCode = codeArr[i];
			
			if(!"NA".equalsIgnoreCase(sysCode)){
				dataMap.put("SYS_CODE", codeArr[i]);
				dataMap.put("HTML_TYPE", "SELECT");
			}else{
				dataMap.put("SYS_CODE", null);
				dataMap.put("HTML_TYPE", "TEXT");
			}
			dataMap.put("POPUP_CODE", null);
			dataMap.put("DATE_TYPE", null);
			
			headerList.add(dataMap);
		}
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return headerList;
	}
	
	/**
	 * 
	 * 베이스 리스트의 데이터를 타겟 리스트에 넣어준다.
	 * TODO
	 * 
	 * @param baseList
	 * @param targetList
	 * @return
	 * @throws NkiaException
	 */
	public List addTargetList(List<HashMap> baseList,List<HashMap> targetList)throws NkiaException{
		if(baseList != null){
			for(int i=0;i<baseList.size();i++){
				HashMap baseMap = baseList.get(i);
				targetList.add(baseMap);
			}
		}
		return targetList;
	}
	
	public JSONArray getMergeGridResource(JSONArray baseColArr,JSONArray inColArr,String prefix)throws NkiaException{
		
		JSONArray returnArray = new JSONArray();
		
		for(int i=0;i<baseColArr.size();i++){
			String baseCol = (String)baseColArr.get(i);
			String inCol = (String)inColArr.get(i);
			baseCol =  StringUtil.replaceAll(baseCol.replace(prefix,inCol),"'","");
			returnArray.add(baseCol);
		}
		
		return returnArray;
	}
	
	/**
	 * 
	 * TODO 설치 정보 이력 생성 쿼리
	 * 
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createInstallHisQuery(List<HashMap> installColList)throws NkiaException{
		StringBuffer sb = new StringBuffer();
		if(installColList != null){
			for(int i=0;i<installColList.size();i++){
				HashMap installColMap = installColList.get(i);
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				sb.append(COMMA + "MAX(DECODE(CHG_COLUMN_ID,'"+colId+"',CHG_PREV_VALUE,null)) AS " + colId + "\n");
			}
		}
		return sb.toString();
	}

	/**
	 * 
	 * TODO 정보수집서 조회 쿼리 생성
	 * 
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createOpmsInfoSelectQuery(List<HashMap> attrList, List<HashMap> hiddenList, List<HashMap> tblList, List<HashMap> joinList,
			List<HashMap> assetConfIdList, HashMap entityMap)throws NkiaException {

		StringBuffer queryText = new StringBuffer();
		String conf_id = (String) entityMap.get("conf_id");
		String asset_id = (String) entityMap.get("asset_id");
		String class_id = (String) entityMap.get("class_id");
		String class_type = (String) entityMap.get("class_type");
		
		if(tblList != null && tblList.size()>0){
			queryText.append("SELECT\n");
			queryText.append("ASSET_TEMP_ID\n");
			queryText.append(COMMA + "CONF_TEMP_ID\n");
			if(attrList != null){
				if (attrList.size() > 0) {
					for (int i = 0; i < attrList.size(); i++) {
		
						HashMap dataMap = attrList.get(i);
						
						String htmlType = StringUtil.parseString(dataMap.get("HTML_TYPE"));
						String colId = StringUtil.parseString(dataMap.get("COL_ID"));
						String name = StringUtil.parseString(dataMap.get("MK_COMP_NM"));
						String table = StringUtil.parseString(dataMap.get("GEN_TABLE_NM"));
						String popupFn = StringUtil.parseString(dataMap.get("POPUP_NM_FN"));
		
						if(!htmlType.equals("DUMMY")){
							queryText.append(COMMA);
	
							if (htmlType.equalsIgnoreCase("TEXT")) {
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
							} else if (htmlType.equalsIgnoreCase("TEXTAREA")) { // 텍스트 박스
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
							} else if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
								String dateType = (String) dataMap.get("DATE_TYPE");
								if (dateType.equals(DATE_TYPE_TIME)) {
									queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+DATE_TYPE_DAY+"'),'"+DATE_TYPE_TIME+"'),null),null)) AS " + name);
									queryText.append(COMMA + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+DATE_TYPE_DAY+"'),'"+DATE_TYPE_DAY+"'),null),null)) AS " + name + "_DAY");
									queryText.append(COMMA + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+DATE_TYPE_DAY+"'),'"+DATE_TYPE_HOUR+"'),null),null)) AS " + name + "_HOUR");
									queryText.append(COMMA + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+DATE_TYPE_DAY+"'),'"+DATE_TYPE_MIN+"'),null),null)) AS " + name + "_MIN");
								} else {
									queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+DATE_TYPE_DAY+"'),'"+DATE_TYPE_DAY+"'),null),null)) AS " + name);
									queryText.append(COMMA + "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+ DATE_TYPE_DAY+"'),'"+ DATE_TYPE_DAY+"'),null),null)) AS " + name+ "_DAY");
								}
							} else if (htmlType.equalsIgnoreCase("SELECT")) {// 콤보
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
							} else if (htmlType.equalsIgnoreCase("RADIO")) {// 라디오
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
							} else if (htmlType.equalsIgnoreCase("CHECK")) {// 체크
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
							} else if (htmlType.equalsIgnoreCase("POPUP")) {// 팝업
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
								if (!"".equalsIgnoreCase(popupFn)) {
									queryText.append("\n");
									queryText.append(COMMA);
									String popupColIds = "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null))";
									
									// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
									popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
									
									popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
									queryText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}
	
							} else if (htmlType.equalsIgnoreCase("POPUPASSET")) {// 팝업
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
								if (!"".equalsIgnoreCase(popupFn)) {
									queryText.append("\n");
									queryText.append(COMMA);
									String popupColIds = "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null))";
									
									// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
									popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
									
									popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
									queryText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}
	
							} else if (htmlType.equalsIgnoreCase("POPUPCLUSTER")) {// 팝업
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
								if (!"".equalsIgnoreCase(popupFn)) {
									queryText.append("\n");
									queryText.append(COMMA);
									String popupColIds = "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null))";
									
									// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
									popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
									
									popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
									queryText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}
	
							} else if (htmlType.equalsIgnoreCase("POPUPEMS")) {// 팝업
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null)) AS " + name);
								if (!"".equalsIgnoreCase(popupFn)) {
									queryText.append("\n");
									queryText.append(COMMA);
									String popupColIds = "MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',COL_VALUE,null),null))";
									
									// 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
									popupColIds = this.getAddOtherDataParams(popupColIds, colId, asset_id, conf_id, class_id, class_type);
									
									popupFn = StringUtil.replaceAll(popupFn.replace("{0}", popupColIds), "\\^", "'");
									queryText.append(popupFn + " AS " + name + POPUP_NAME_FIELD_SUFFIX);
								}
	
							}
							queryText.append("\n");
						}
					}
				}
			}
			/*
			if(hiddenList != null){
				if (hiddenList.size() > 0) {
					for (int i = 0; i < hiddenList.size(); i++) {
		
						HashMap dataMap = hiddenList.get(i);
		
						String htmlType = (String) dataMap.get("HTML_TYPE");
						String colId = (String) dataMap.get("COL_ID");
						String name = (String) dataMap.get("MK_COMP_NM");
						String table = (String) dataMap.get("GEN_TABLE_NM");
		
						if (attrList.size() > 0) {
							queryText.append(COMMA);
						} else {
							if (i > 0) {
								queryText.append(COMMA);
							}
						}
						if (htmlType.equalsIgnoreCase("DATE")) {// 날짜
							String dateType = (String) dataMap.get("DATE_TYPE");
							if (dateType.equals(DATE_TYPE_TIME)) {
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+ DATE_TYPE_DAY+"'),'"+ DATE_TYPE_TIME+"'),null),null)) AS " + name);
							} else {
								queryText.append("MAX(DECODE(TABLE_NM,'"+ table +"',DECODE(COLUMN_ID,'"+colId+"',TO_CHAR(TO_DATE(COL_VALUE,'"+ DATE_TYPE_DAY+"'),'"+ DATE_TYPE_DAY+"'),null),null)) AS " + name);
							}
						} else {
							queryText.append(table + DOT + colId + " AS " + name + "\n");
						}
					}
				}
			}
			*/
			queryText.append("\n FROM (\n");
			queryText.append("SELECT ASSET_TEMP_ID\n");
			queryText.append(COMMA + "CONF_TEMP_ID\n");
			queryText.append(COMMA + "COLUMN_ID\n");
			queryText.append(COMMA + "TABLE_NM\n");
			queryText.append(COMMA + "COL_VALUE\n");
			queryText.append("FROM AM_TEMP_DATA\n");
			queryText.append("WHERE CONF_TEMP_ID = '"+conf_id+"'\n");
			queryText.append("  AND ASSET_TEMP_ID = '"+asset_id+"'\n");
			queryText.append(" )\n");
			queryText.append("GROUP BY ASSET_TEMP_ID, CONF_TEMP_ID");
		}
		return queryText.toString();
	}	
	
	/**
	 * 
	 * TODO 설치 속성 업데이트 쿼리를 생성한다.
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public String createAutoInstallUpdateQuery(HashMap paramMap,List<HashMap> assetConfIdList, String adTarget)throws NkiaException{
		
		StringBuffer queryText = new StringBuffer();
		
		String asset_id = StringUtil.parseString(paramMap.get("asset_id"));
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		String table_nm = StringUtil.parseString(paramMap.get("gen_table_nm"));
		String am_col_id = StringUtil.parseString(paramMap.get("COL_ID"));
		String emsValue = StringUtil.parseString(paramMap.get("EMS_VALUE"));
		String dcaValue = StringUtil.parseString(paramMap.get("DCA_VALUE"));
		
		
		if(!"".equals(table_nm) && !"".equals(am_col_id)){
			
			queryText.append("UPDATE " + table_nm + " SET \n");
			if( adTarget.equals("EMS") ){
				queryText.append(am_col_id + " = '"+emsValue+"' \n");
			}else if( adTarget.equals("DCA") ){
				queryText.append(am_col_id + " = '"+dcaValue+"' \n");
			}
			
			// 조건절시작
			queryText.append("WHERE\n");
			
			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					
					queryText.append(col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
			
			//hmsong 구성 ID가 존재하는 경우 자산ID 조건절은 무시
			if(attatchConf == 0) {
				int attatchAsset = 0;
				//자산아이디가 있을경우..
				for (int i = 0; i < assetConfIdList.size(); i++) {
					HashMap dataMap = assetConfIdList.get(i);
					String col_id = (String) dataMap.get("COL_ID");
					if ("ASSET_ID".equalsIgnoreCase(col_id)) {
						if (attatchConf > 0) {
							queryText.append("AND ");
						}
						
						queryText.append(col_id + "='" + asset_id + "'" + "\n");
						attatchAsset = 1;
						break;
					}
				}
			}
		}
		return queryText.toString();
	}
	
	/**
	 * 
	 * TODO 패널버튼 아이템들을 생성한다.
	 * @return 
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public String createPanelBtnJsonString(List<HashMap> dataList, HashMap paramMap) throws NkiaException {
		
		JSONObject buttons = new JSONObject();
		JSONArray buttonItems = new JSONArray();
	
		for(int i=0; i<dataList.size(); i++ ){
			
			HashMap dataMap = dataList.get(i);
			String btn_id = (String) dataMap.get("BTN_ID");
			String asset_id = (String) paramMap.get("asset_id");
			String conf_id = (String) paramMap.get("conf_id");
			String class_type = (String) paramMap.get("class_type");
			String class_id = (String) paramMap.get("class_id");
			String conf_type = (String) paramMap.get("conf_type");
			String entity_mng_type = (String) paramMap.get("entity_mng_type");
//			String tangible_asset_yn = (String) paramMap.get("tangible_asset_yn"); ////hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			String location_cd_nm = (String) paramMap.get("LOCATION_CD_NM"); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			String center_position_cd = (String) paramMap.get("CENTER_POSITION_CD"); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			String center_position_cd_nm = (String) paramMap.get("CENTER_POSITION_CD_NM"); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			String customer_id_nm = (String) paramMap.get("CUSTOMER_ID_NM"); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
			
			JSONObject btnProp = new JSONObject();
			
			btnProp.put("button_type", attachQuotes(btn_id) );
			btnProp.put("id", attachQuotes(btn_id + BUTTON_SUFFIX) );
			btnProp.put("asset_id", attachQuotes(asset_id));
			btnProp.put("conf_id", attachQuotes(conf_id));
			btnProp.put("class_type", attachQuotes(class_type));
			btnProp.put("class_id", attachQuotes(class_id));
			btnProp.put("conf_type", attachQuotes(conf_type));
			btnProp.put("entity_mng_type", attachQuotes(entity_mng_type));
//			btnProp.put("tangible_asset_yn", attachQuotes(tangible_asset_yn)); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			btnProp.put("location_cd_nm", attachQuotes(location_cd_nm)); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			btnProp.put("center_position_cd", attachQuotes(center_position_cd)); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			btnProp.put("center_position_cd_nm", attachQuotes(center_position_cd_nm)); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
//			btnProp.put("customer_id_nm", attachQuotes(customer_id_nm)); //hmsong cmdb 개선 작업 - 사용하지 않는 속성 파라미터
			
			buttonItems.add(PANEL_BUTTON + LEFT_BRACKET + btnProp.toString() + RIGHT_BRACKET);
		}
		
		buttons.put("items", buttonItems.toString());
		
		return buttons.toString();
	}
	
	/**
	 * 
	 * TODO 설치 정보 쿼리
	 * 
	 * @param installInfo
	 * @param installColList
	 * @return
	 * @throws NkiaException
	 */
	public String createAutoInstallQuery(HashMap installInfo,List<HashMap> installColList)throws NkiaException{
		
		StringBuffer sb = new StringBuffer();
		
		String confId   = StringUtil.parseString(installInfo.get("conf_id"));
		String emsId   = StringUtil.parseString(installInfo.get("ems_id"));
		String tableNm = StringUtil.parseString(installInfo.get("TABLE_NM"));
		String viewType = StringUtil.parseString(installInfo.get("view_type"));	//@@ 고은규 수정
		if("info".equals(viewType) || "infmodify".equals(viewType)){				//@@ 고은규 수정
			tableNm = StringUtil.parseString(installInfo.get("INFO_TABLE_NM"));		//@@ 고은규 수정
		}
		String emsShowYn = StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
		String emsYn = StringUtil.parseString(installInfo.get("EMS_YN"));
		String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
		String orderColumn = "";
		
		sb.append("SELECT\n");
		
		for(int i=0;i<installColList.size();i++){
			
			HashMap installColMap = installColList.get(i);
			
			String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
			String colId = StringUtil.parseString(installColMap.get("COL_ID"));
			String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
			String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
			//	String sysCode = StringUtil.parseString(installColMap.get("SYS_CODE"));
			
			if(i>0){
				sb.append(COMMA);
			}
			
			if("Y".equalsIgnoreCase(emsShowYn)){
				sb.append(emsColId + "\n");
			}else{
				sb.append(colId + "\n");
			}	
			
			if("Y".equalsIgnoreCase(orderCol)){
				if("Y".equalsIgnoreCase(emsShowYn)){
					orderColumn = emsColId;
				}else{
					orderColumn = colId;
				}
			}
		}
		if(!"Y".equalsIgnoreCase(emsShowYn)){
			sb.append(COMMA)
			.append("GUBUN\n");
		}
		sb.append("FROM\n");
		
		if("Y".equalsIgnoreCase(emsShowYn)){
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(COMMA);
				}
				sb.append(emsColId + "\n");
				
			}
			sb.append("FROM " + emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' \n");
			sb.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(COMMA);
				}
				sb.append(colId + " AS "+ emsColId + "\n");
				
			}
			
			sb.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "') \n");
			
		}else{
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(COMMA);
				}
				sb.append(colId + "\n");
				
			}
			sb.append(COMMA)
			.append("'D' AS GUBUN \n")
			.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "' \n")
			.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(COMMA);
				}
				sb.append(emsColId + " AS "+ colId + "\n");
				
			}
			sb.append(COMMA)
			.append("'D' AS GUBUN \n")
			.append("FROM " + emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' \n")
			.append(" UNION \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(COMMA);
				}
				sb.append(colId + "\n");
				
			}
			sb.append(COMMA)
			.append("'S' AS GUBUN \n")
			.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "' \n")
			.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("(SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(COMMA);
				}
				sb.append(colId + "\n");
				
			}
			sb.append(COMMA)
			.append("'S' AS GUBUN \n")
			.append("FROM " + tableNm + "\n")
			.append("WHERE \n")
			.append("CONF_ID = '" + confId + "' \n")
			.append(" MINUS \n");
			
			for(int i=0;i<installColList.size();i++){
				if(i == 0){
					sb.append("SELECT ");					
				}
				HashMap installColMap = installColList.get(i);
				
				String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
				String colId = StringUtil.parseString(installColMap.get("COL_ID"));
				String emsColId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
				String orderCol = StringUtil.parseString(installColMap.get("ORDER_COL"));
				
				if(i>0){
					sb.append(COMMA);
				}
				sb.append(emsColId + " AS "+ colId + "\n");
				
			}
			sb.append(COMMA)
			.append("'S' AS GUBUN \n")
			.append("FROM " + emsTblNm + "\n")
			.append("WHERE \n")
			.append("NODE_ID = '" + emsId + "' )\n )\n )\n");			
			
		}
		
		if("Y".equalsIgnoreCase(orderColumn) && !"".equalsIgnoreCase(orderColumn)){
		//if(!"".equals(orderColumn)){
			/*if("INDX".equals(orderColumn)){
				sb.append("ORDER BY TO_NUMBER("+orderColumn+") \n");				
			}else{
				sb.append("ORDER BY " + orderColumn +" \n");								
			}*/
			sb.append("ORDER BY " + orderColumn +" \n");
		}
		
		return sb.toString();
	}
	
	/**
	 * 
	 * TODO 설치 속성 업데이트 쿼리를 생성한다.
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public String createAutoInstallUpdateQuery(HashMap paramMap,List<HashMap> assetConfIdList)throws NkiaException{
		
		StringBuffer queryText = new StringBuffer();
		
		String asset_id = StringUtil.parseString(paramMap.get("asset_id"));
		String conf_id = StringUtil.parseString(paramMap.get("conf_id"));
		String table_nm = StringUtil.parseString(paramMap.get("gen_table_nm"));
		String am_col_id = StringUtil.parseString(paramMap.get("COL_ID"));
		String emsValue = StringUtil.parseString(paramMap.get("EMS_VALUE"));
		
		
		if(!"".equals(table_nm) && !"".equals(am_col_id)){
			
			queryText.append("UPDATE " + table_nm + " SET \n");
			queryText.append(am_col_id + " = '"+emsValue+"' \n");
			// 조건절시작
			queryText.append("WHERE\n");
			
			int attatchConf = 0;
			//구성 id가 있을경우
			for (int i = 0; i < assetConfIdList.size(); i++) {
				HashMap dataMap = assetConfIdList.get(i);
				String col_id = (String) dataMap.get("COL_ID");
				if ("CONF_ID".equalsIgnoreCase(col_id)) {
					
					queryText.append(col_id + "='" + conf_id + "'\n");
					attatchConf = 1;
					break;
				}
			}
			
			//hmsong 구성 ID가 존재하는 경우 자산ID 조건절은 무시
			if(attatchConf == 0) {
				int attatchAsset = 0;
				//자산아이디가 있을경우..
				for (int i = 0; i < assetConfIdList.size(); i++) {
					HashMap dataMap = assetConfIdList.get(i);
					String col_id = (String) dataMap.get("COL_ID");
					if ("ASSET_ID".equalsIgnoreCase(col_id)) {
						if (attatchConf > 0) {
							queryText.append("AND ");
						}
						
						queryText.append(col_id + "='" + asset_id + "'" + "\n");
						attatchAsset = 1;
						break;
					}
				}
			}
		}
		return queryText.toString();
	}
	
	
	/**
	 * 
	 * TODO 팝업용 DB Function에서 인자값이 2개이상 들어가는 컬럼에 대해 추가 인자값들을 세팅해주는 함수
	 * 
	 * @param popupColIds
	 * @param colId
	 * @param asset_id
	 * @param conf_id
	 * @param class_id
	 * @param class_type
	 * @return String
	 * @throws NkiaException
	 */
	private String getAddOtherDataParams(
			String popupColIds,
			String colId,
			String asset_id,
			String conf_id,
			String class_id,
			String class_type
		) throws NkiaException {
		
		String result = popupColIds;
		
		if(colId != null && !colId.equals("")) {
			
			if("SW_GROUP_OPTION_ID".equals(colId)) {
				// 소프트웨어 그룹 옵션의 경우 인자값이 2개가 들어가야 하므로 추가 해준다 -20140326 정정윤
				result = result + ", '" + asset_id + "'"; 
			} else if("DIV_WORK".equals(colId)) {
				// 업무 분류의 경우 인자값이 2개 들어가야하므로 추가해준다. 20161128 좌은진
				result = "'DIV_WORK'," + result; 
			}
			
		}
		
		return result;
	}
	
	
}
