package com.nkia.itg.itam.automation.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.automation.dao.AssetAutomationDAO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.itam.automation.ui.WebixUIHandler;

/**
 * Component Handler Class
 * 사업 수행 시, 추가 되는 컴포넌트에 대해 이 곳에서 추가 할 것
 * 
 * - 컴포넌트를 추가해야하는 곳을 찾아가려면 골뱅이 2개(@@)로 검색하여 찾아가면 됩니다. (Ctrl + F : @@)
 * 
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2017. 01. 02. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Component("assetAutomationComponents")
public class AssetAutomationComponents {
	
	@Resource(name = "webixUIHandler")
	private WebixUIHandler webixUIHandler;
	
	@Resource(name = "assetAutomationService")
	private AssetAutomationService assetAutomationService;
	
	@Resource(name = "assetAutomationDAO")
	private AssetAutomationDAO assetAutomationDAO;
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	/**
	 * registComponent
	 * 그리드 컴포넌트의 값을 등록 한다.
	 */
	// @@ 컴포넌트 추가대상 메서드
	public void registComponent(HashMap paramMap, HashMap dataMap, int authCnt) throws NkiaException {
		
		String asset_id = (String)paramMap.get("asset_id");
		String conf_id = (String)paramMap.get("conf_id");
		String class_id = (String)paramMap.get("class_id");
		String class_type = (String)paramMap.get("class_type");
		String user_id = (String)paramMap.get("user_id");
		String sr_id 	= (String)paramMap.get("sr_id");
		String chgMessage = StringUtils.isEmpty(sr_id) ? "" : sr_id;
		
		String chgSeq = (String)dataMap.get("chg_his_seq");
		String comp_id = (String)dataMap.get("comp_id");
		String type = (String)dataMap.get("type");
		String entity_id = (String)dataMap.get("entity_id");
		String entity_type = (String)dataMap.get("entity_type");
		String entity_mng_type = (String)dataMap.get("entity_mng_type");
		
		String physi_conf_id = (String)dataMap.get("physi_conf_id");
		String conf_type = (String)paramMap.get("conf_type");	//hmsong cmdb 개선 작업 : 구성용도
		// 싱글 리스트
		if("SL".equalsIgnoreCase(type)){ 
			
			// 담당자
			if("MANAGER".equalsIgnoreCase(entity_type)){
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					assetAutomationDAO.deleteOper(dataMap);
					if(currentDataList != null){
						
						// 운영자 컴포넌트 이력데이터를 등록
						assetHistoryService.insertOperCompHistory(conf_id, currentDataList, chgMessage, user_id);
						
						for(int j=0;j<currentDataList.size();j++){
							HashMap curMngMap = currentDataList.get(j);
							curMngMap.put("SEQ", j+1);
							curMngMap.put("UPD_USER_ID", user_id);
							curMngMap.put("CONF_ID", conf_id);
							// main_yn(정/부 여부) 사용안하므로 주석처리 - 20170329 좌은진
//							if(curMngMap.get("MAIN_YN").toString() == "true"){
//								curMngMap.put("MAIN_YN", "Y");
//							}else{
//								curMngMap.put("MAIN_YN", "N");
//							}
							assetAutomationDAO.insertOper(curMngMap);
						}
					}
				}
			}else if("REL_SERVICE".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					
					// 연관서비스 컴포넌트 이력데이터를 등록
					assetHistoryService.insertRelServiceCompHistory(conf_id, currentDataList, chgMessage, user_id);
					
					assetAutomationDAO.deleteService(dataMap);
					if(currentDataList != null){
						for(int j=0;j<currentDataList.size();j++){
							HashMap curMngMap = currentDataList.get(j);
							curMngMap.put("UPD_USER_ID", user_id);
							curMngMap.put("CONF_ID", conf_id);
							assetAutomationDAO.insertService(curMngMap);
						}
					}
				}
			}else if("INSTALL_SW".equalsIgnoreCase(entity_type)){
				// 설치 소프트웨어
				List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
				// 현재 데이터의 OPTION_ID_LIST 따로 빼 놓는다
				if(authCnt>0){
					//기존 데이터 삭제
					assetAutomationDAO.deleteInsSwOption(dataMap); // 해당 conf_id의 옵션 전체 삭제
					assetAutomationDAO.deleteInsSw(dataMap); //TODO 기존 데이터 삭제 쿼리 작성
					if(currentData != null){
						for(int j=0;j<currentData.size();j++){
							//등록
							HashMap curMngMap = currentData.get(j);
							curMngMap.put("UPD_USER_ID", user_id);
							curMngMap.put("CONF_ID", conf_id);
							assetAutomationDAO.insertInsSw(curMngMap); // 설치 소프트웨어 목록을 insert 해준다.
							List curOptionIdList = null;
							// OPTION_ID_LIST Key 가 존재한다면
							if( curMngMap.get("OPTION_ID_LIST") != null ){
								// OPTION_ID_LIST의 데이터가 리스트형 일 때
								if( curMngMap.get("OPTION_ID_LIST") instanceof ArrayList){
									// 해당 옵션ID 들로 리스트를 만들어 주고
									curOptionIdList = (ArrayList)curMngMap.get("OPTION_ID_LIST");
									// 만들어진 리스트에 값이 들어 있다면
									if( curOptionIdList != null && curOptionIdList.size() > 0 ){
										// 해당 리스트의 길이(설치 소프트웨어가 가지고 있는 옵션의 수)만큼 다시 insert 해준다.
										for(int z=0; z<curOptionIdList.size(); z++) {
											curMngMap.put("OPTION_ID", curOptionIdList.get(z));
											assetAutomationDAO.insertInsSwOption(curMngMap);
										}
									}
								}
							} 
						}
					}
				}
			}else if("REL_DISK".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					
					updateCompRelDisk(conf_id, currentDataList, user_id, chgMessage);
					
				}
			}else if("REL_IP".equalsIgnoreCase(entity_type)){
				// 연관 IP
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					updateCompRelIP(conf_id, currentDataList, user_id, chgMessage);
				}
			}else if("REL_NIC".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					
					updateCompRelNic(conf_id, currentDataList, user_id, chgMessage);
					
				}
			}else if("DUAL_INFO".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					
					updateCompDualInfo(conf_id, currentDataList, user_id, chgMessage);
					
				}
			}
		}else if("TL".equalsIgnoreCase(type)){
			//탭 리스트
			if("REL_INFRA".equalsIgnoreCase(entity_type)){
				List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
				if(authCnt>0){
					dataMap.put("root_type", comp_id);
					
					//화면에서 가져온 연관 자산의 속성이 자산분류체계에 매핑 되어 있을 때는 class_type을...				
					if("ASSET".equals(entity_mng_type)) {
						dataMap.put("main_root_type", class_type);
					} 
					//화면에서 가져온 연관 자산의 속성이 구성용도에 매핑 되어 있을 때는 conf_type을...
					else {
						dataMap.put("main_root_type", conf_type);
					}
					
					assetAutomationDAO.deleteRelConf(dataMap);
					if(currentDataList != null){
						for(int j=0;j<currentDataList.size();j++){
							HashMap curRelConfMap = currentDataList.get(j);
							curRelConfMap.put("ROOT_TYPE", comp_id);
							curRelConfMap.put("MAIN_CONF_ID", conf_id);
							curRelConfMap.put("MAIN_ROOT_TYPE", dataMap.get("main_root_type"));
							assetAutomationDAO.insertRelConf(curRelConfMap);
						}
					}
				}
				/*
				if(comp_id.equals("LOGSV")){
					if(!physi_conf_id.isEmpty()){
						dataMap.put("physi_conf_id", physi_conf_id);
						assetAutomationDAO.insertLogSvRelConf(dataMap);
					}
				}else{
					List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
					if(authCnt>0){
						
						//파라메터 대소문자 관련해서 문제가 있습니다...
						dataMap.put("main_class_type", class_type);
						
						if("DR".equalsIgnoreCase(comp_id)){
							assetAutomationDAO.deleteRelDR(dataMap);
							if(currentDataList != null){
								for(int j=0;j<currentDataList.size();j++){
									HashMap curRelConfMap = currentDataList.get(j);
									curRelConfMap.put("MAIN_CONF_ID", conf_id);
									curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
									assetAutomationDAO.insertRelDR(curRelConfMap);
								}
							}
						} else {
							assetAutomationDAO.deleteRelConf(dataMap);
							if(currentDataList != null){
								for(int j=0;j<currentDataList.size();j++){
									HashMap curRelConfMap = currentDataList.get(j);
									curRelConfMap.put("MAIN_CONF_ID", conf_id);
									curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
									assetAutomationDAO.insertRelConf(curRelConfMap);
								}
							}
						}
					}
				}
				*/
			}else if("REL_INFRA_HIST".equalsIgnoreCase(entity_type)){
				// 연관장비 이력 등록
				if(authCnt>0){
					HashMap tabData = (HashMap) dataMap.get("data");
					
					// 연관장비 컴포넌트 이력데이터를 등록
					assetHistoryService.insertRelInfraCompHistory(conf_id, class_type, tabData, "", user_id);
				}			
			}else if("INSTALL".equalsIgnoreCase(entity_type)){
				// 설치 정보
				dataMap.put("ins_type", comp_id);
				HashMap installInfo = assetAutomationDAO.selectOpmsInstallInfo(comp_id);
				List<HashMap> installColList = assetAutomationDAO.selectOpmsInstallColumnList(comp_id);
				List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
				
				String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
				String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
				String dcaShowYn =  StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
				String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
				String tableNm	= StringUtil.parseString(installInfo.get("TABLE_NM"));						
				
				if(authCnt>0 && emsShowYn.equalsIgnoreCase("N") && dcaShowYn.equalsIgnoreCase("N")){
					//파라메터 대소문자 관련해서 문제가 있습니다...
					installInfo.put("CONF_ID", conf_id);
					assetAutomationService.deleteInstallInfo(installInfo);
					
					if(currentDataList != null){
						for(int j=0;j<currentDataList.size();j++){
							HashMap curInstallMap = currentDataList.get(j);
							curInstallMap.put("CONF_ID", conf_id);
							curInstallMap.put("TABLE_NM", tableNm);
							assetAutomationService.insertInstallInfo(curInstallMap,installColList);
						}
					}
				}
			}else if("INSTALL_HIST".equals(entity_type)){
				// 설치정보 컴포넌트 이력데이터를 등록
				if(authCnt>0){
					HashMap tabData = (HashMap) dataMap.get("data");
					assetHistoryService.insertInstallTabListCompHistory(conf_id, tabData, "", user_id);
				}
			}
		}
	}
	
	private void updateCompRelDisk(String confId, List<HashMap> dataList, String updUserId, String chgMessage) throws NkiaException {
		// 컴포넌트 이력데이터를 등록
		assetHistoryService.insertRelDiskCompHistory(confId, dataList, chgMessage, updUserId);
	
		// 기존 데이터 삭제
		assetAutomationDAO.deleteAmconfRelDisk(confId);
		// 데이터 수정
		if(dataList != null && !dataList.isEmpty()) {
			for(HashMap currentData : dataList) {
				currentData.put("CONF_ID", confId);
				currentData.put("UPD_USER_ID", updUserId);
				assetAutomationDAO.insertAmconfRelDisk(currentData);
			}
		}
	}

	private void updateCompRelNic(String confId, List<HashMap> dataList, String updUserId, String chgMessage) throws NkiaException {
		// 컴포넌트 이력데이터를 등록
		assetHistoryService.insertRelNicCompHistory(confId, dataList, chgMessage, updUserId);
	
		// 기존 데이터 삭제
		assetAutomationDAO.deleteAmconfRelNic(confId);
		// 데이터 수정
		if(dataList != null && !dataList.isEmpty()) {
			for(HashMap currentData : dataList) {
				currentData.put("CONF_ID", confId);
				currentData.put("UPD_USER_ID", updUserId);
				assetAutomationDAO.insertAmconfRelNic(currentData);
			}
		}
	}	

	private void updateCompDualInfo(String confId, List<HashMap> dataList, String updUserId, String chgMessage) throws NkiaException {
		// 컴포넌트 이력데이터를 등록
		assetHistoryService.insertDualInfoCompHistory(confId, dataList, chgMessage, updUserId);
	
		// 기존 데이터 삭제
		assetAutomationDAO.deleteAmconfDualInfo(confId);
		// 데이터 수정
		if(dataList != null && !dataList.isEmpty()) {
			for(HashMap currentData : dataList) {
				currentData.put("CONF_ID", confId);
				currentData.put("UPD_USER_ID", updUserId);
				assetAutomationDAO.insertAmconfDualInfo(currentData);
			}
		}
	}
	
	/**
	 * updateComponent
	 * 그리드 컴포넌트의 값을 업데이트 한다.
	 */
	// @@ 컴포넌트 추가대상 메서드
	public void updateComponent(HashMap paramMap, HashMap dataMap, int authCnt) throws NkiaException {
		
		String asset_id = (String)paramMap.get("asset_id");
		String conf_id = (String)paramMap.get("conf_id");
		String class_id = (String)paramMap.get("class_id");
		String class_type = (String)paramMap.get("class_type");
		String user_id = (String)paramMap.get("user_id");
		String sr_id 	= (String)paramMap.get("sr_id");
		String chgMessage = StringUtils.isEmpty(sr_id) ? "" : sr_id;
		String conf_type = (String)paramMap.get("conf_type");	
		String mng_type = (String)paramMap.get("mng_type");
		
		String comp_id = (String)dataMap.get("comp_id");
		String type = (String)dataMap.get("type");
		String entity_id = (String)dataMap.get("entity_id");
		String entity_type = (String)dataMap.get("entity_type");
		String entity_mng_type = (String)dataMap.get("entity_mng_type");
		
		// 화면에서 가져온 속성이 자산분류체계에 매핑되어 있을 때는 entity_root_id에 class_id를 넣어주고, 구성용도에 매핑되어 있을 때는'CONF' 일 때는 conf_type을 파라미터로 전달한다.
		String entity_root_id = (String)dataMap.get("entity_root_id");
		
		 // 싱글 리스트
		if("SL".equalsIgnoreCase(type)){
			//담당자
			if("MANAGER".equalsIgnoreCase(entity_type)){
				
				//수정되는 데이터
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				
				//수정자가 권한이 있는경우
				if(authCnt>0){
					//기존 데이터 삭제
					assetAutomationDAO.deleteOper(dataMap);
					if(currentDataList != null){
						
						// 운영자 컴포넌트 이력데이터를 등록
						assetHistoryService.insertOperCompHistory(conf_id, currentDataList, chgMessage, user_id);

						for(int j=0;j<currentDataList.size();j++){
							//등록
							HashMap curMngMap = currentDataList.get(j);
							curMngMap.put("SEQ", j+1);
							curMngMap.put("UPD_USER_ID", user_id);
							curMngMap.put("CONF_ID", conf_id);
							curMngMap.put("ASSET_ID", asset_id);
							assetAutomationDAO.insertOper(curMngMap);
							// main_yn(정/부 여부) 사용안하므로 주석처리 - 20170329 좌은진
//							if(j==currentDataList.size()){
//								assetAutomationDAO.updateCust(curMngMap);
//							}
						}
					}
				}
			}else if("REL_SERVICE".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				
				// 연관서비스 컴포넌트 이력데이터를 등록
				assetHistoryService.insertRelServiceCompHistory(conf_id, currentDataList, chgMessage, user_id);

				if(authCnt>0){
					assetAutomationDAO.deleteService(dataMap);
					if(currentDataList != null){
						for(int j=0;j<currentDataList.size();j++){
							HashMap curMngMap = currentDataList.get(j);
							curMngMap.put("UPD_USER_ID", user_id);
							curMngMap.put("CONF_ID", conf_id);
							assetAutomationDAO.insertService(curMngMap);
						}
					}
				}
			}
			else if("INSTALL_SW".equalsIgnoreCase(entity_type)){
				// 설치 소프트웨어
				List<HashMap> currentData	= (List<HashMap>)paramMap.get(entity_id);
				
				// 현재 데이터의 OPTION_ID_LIST 따로 빼 놓는다
				if(authCnt>0){
					
					//기존 데이터 삭제
					assetAutomationDAO.deleteInsSwOption(dataMap); // 해당 conf_id의 옵션 전체 삭제
					assetAutomationDAO.deleteInsSw(dataMap); //TODO 기존 데이터 삭제 쿼리 작성
					
					if(currentData != null){
						
						for(int j=0;j<currentData.size();j++){
							
							//등록
							HashMap curMngMap = currentData.get(j);
							curMngMap.put("UPD_USER_ID", user_id);
							curMngMap.put("CONF_ID", conf_id);
							
							assetAutomationDAO.insertInsSw(curMngMap); // 설치 소프트웨어 목록을 insert 해준다.
							
							List curOptionIdList = null;
							
							// OPTION_ID_LIST Key 가 존재한다면
							if( curMngMap.get("OPTION_ID_LIST") != null ){
								
								// OPTION_ID_LIST의 데이터가 리스트형 일 때
								if( curMngMap.get("OPTION_ID_LIST") instanceof ArrayList){
									
									// 해당 옵션ID 들로 리스트를 만들어 주고
									curOptionIdList = (ArrayList)curMngMap.get("OPTION_ID_LIST");
									
									// 만들어진 리스트에 값이 들어 있다면
									if( curOptionIdList != null && curOptionIdList.size() > 0 ){
										
										// 해당 리스트의 길이(설치 소프트웨어가 가지고 있는 옵션의 수)만큼 다시 insert 해준다.
										for(int z=0; z<curOptionIdList.size(); z++) {
											
											curMngMap.put("OPTION_ID", curOptionIdList.get(z));
											assetAutomationDAO.insertInsSwOption(curMngMap);
										}
									}
								}
							} 
						}
					}
				}
			}else if("REL_DISK".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					
					updateCompRelDisk(conf_id, currentDataList, user_id, chgMessage);
					
				}
			}else if("REL_IP".equalsIgnoreCase(entity_type)){
				// 연관 IP
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					updateCompRelIP(conf_id, currentDataList, user_id, chgMessage);
				}
			}else if("REL_NIC".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					
					updateCompRelNic(conf_id, currentDataList, user_id, chgMessage);
					
				}
			}else if("DUAL_INFO".equalsIgnoreCase(entity_type)){
				// 연관 서비스
				List<HashMap> currentDataList	= (List<HashMap>)paramMap.get(entity_id);
				if(authCnt>0){
					
					updateCompDualInfo(conf_id, currentDataList, user_id, chgMessage);
					
				}
			}					
		}
		
		// 탭 리스트
		else if("TL".equalsIgnoreCase(type)){
			//연관 자산
			if("REL_INFRA".equalsIgnoreCase(entity_type)){
				dataMap.put("root_type", comp_id);
//				dataMap.put("class_type", comp_id);
				List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
				
				if(authCnt>0){
					
					//화면에서 가져온 연관 자산의 속성이 자산분류체계에 매핑 되어 있을 때는 class_type을...				
					if("ASSET".equals(entity_mng_type)) {
						dataMap.put("main_root_type", class_type);
					} 
					//화면에서 가져온 연관 자산의 속성이 구성용도에 매핑 되어 있을 때는 conf_type을...
					else {
						dataMap.put("main_root_type", conf_type);
					}
					
					if(currentDataList != null){
						assetAutomationDAO.deleteRelConf(dataMap);
						for(int j=0;j<currentDataList.size();j++){
							HashMap curRelConfMap = currentDataList.get(j);
							curRelConfMap.put("ROOT_TYPE", comp_id);
							curRelConfMap.put("MAIN_CONF_ID", conf_id);
							curRelConfMap.put("MAIN_ROOT_TYPE", dataMap.get("main_root_type"));
							curRelConfMap.put("entity_mng_type", entity_mng_type);
							assetAutomationDAO.insertRelConf(curRelConfMap);
						}
						//현제는 구현사항이 없음... > 이제생김.
						/*
						if("LOGSV".equalsIgnoreCase(comp_id)){
							for(int j=0;j<currentDataList.size();j++){
								HashMap curRelConfMap = currentDataList.get(j);
//								curRelConfMap.put("CONF_ID", conf_id);
								curRelConfMap.put("conf_id", curRelConfMap.get("CONF_ID"));
								curRelConfMap.put("use_rate", curRelConfMap.get("USE_RATE"));
								assetAutomationDAO.updateRelConfUseRate(curRelConfMap);
							}
						} else if("HA".equalsIgnoreCase(comp_id)){
					
						} else if("VM".equalsIgnoreCase(comp_id)){
					
						} else if("RS".equalsIgnoreCase(comp_id)) {
							// 연관서비스 컴포넌트 이력데이터를 등록
							assetHistoryService.insertRelServiceCompHistory(conf_id, currentDataList, chgMessage, user_id);
							
							assetAutomationDAO.deleteService(dataMap);
							if(currentDataList != null){
								for(int j=0;j<currentDataList.size();j++){
									HashMap curMngMap = currentDataList.get(j);
									curMngMap.put("UPD_USER_ID", user_id);
									curMngMap.put("CONF_ID", conf_id);
									assetAutomationDAO.insertService(curMngMap);
								}
							}
						} else if("DR".equalsIgnoreCase(comp_id)) {
							assetAutomationDAO.deleteRelDR(dataMap);
							for(int j=0;j<currentDataList.size();j++){
								HashMap curRelConfMap = currentDataList.get(j);
								curRelConfMap.put("CLASS_TYPE", comp_id);
								curRelConfMap.put("MAIN_CONF_ID", conf_id);
								curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
								assetAutomationDAO.insertRelDR(curRelConfMap);
							}
						} else {
							assetAutomationDAO.deleteRelConf(dataMap);
							for(int j=0;j<currentDataList.size();j++){
								HashMap curRelConfMap = currentDataList.get(j);
								curRelConfMap.put("CLASS_TYPE", comp_id);
								curRelConfMap.put("MAIN_CONF_ID", conf_id);
								curRelConfMap.put("MAIN_CLASS_TYPE", class_type);
								curRelConfMap.put("MAIN_CONF_TYPE", conf_type);
								assetAutomationDAO.insertRelConf(curRelConfMap);
							}
						}*/
					} else {
						// 연관 서비스 일 경우
//						if("RS".equalsIgnoreCase(comp_id)) {
//							assetAutomationDAO.deleteService(dataMap);
//						} else if("DR".equalsIgnoreCase(comp_id)) {
//							assetAutomationDAO.deleteRelDR(dataMap);
//						} else {
//							assetAutomationDAO.deleteRelConf(dataMap);
//						}
						assetAutomationDAO.deleteRelConf(dataMap);
					}
				}
			}else if("REL_INFRA_HIST".equalsIgnoreCase(entity_type)){
				// 연관장비 이력 등록
				if(authCnt>0){
					HashMap tabData = (HashMap) dataMap.get("data");
					
					// 연관장비 컴포넌트 이력데이터를 등록
					assetHistoryService.insertRelInfraCompHistory(conf_id, entity_root_id, tabData, chgMessage, user_id);
				}								
			}else if("INSTALL".equalsIgnoreCase(entity_type)){
				// 설치 정보
				dataMap.put("ins_type", comp_id);
				HashMap installInfo = assetAutomationDAO.selectOpmsInstallInfo(comp_id);
				List<HashMap> installColList = assetAutomationDAO.selectOpmsInstallColumnList(comp_id);
				
				List<HashMap> currentDataList = (List<HashMap>)paramMap.get(comp_id);
				
				String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
				String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
				String dcaShowYn =  StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
				String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
				String tableNm	= StringUtil.parseString(installInfo.get("TABLE_NM"));
				
				if(authCnt>0 && emsShowYn.equalsIgnoreCase("N") && dcaShowYn.equalsIgnoreCase("N")){
					//파라메터 대소문자 관련해서 문제가 있습니다...
					if( !tableNm.equalsIgnoreCase("IF_DCA_SV_INSTALLED_SW_V") ){
						installInfo.put("CONF_ID", conf_id);
						assetAutomationService.deleteInstallInfo(installInfo);
						if(currentDataList != null){
							for( int j=0; j<currentDataList.size(); j++ ){
								HashMap curInstallMap = currentDataList.get(j);
								curInstallMap.put("CONF_ID", conf_id);
								curInstallMap.put("TABLE_NM", tableNm);
								assetAutomationService.insertInstallInfo(curInstallMap,installColList);
							}
						}
					}
				}
			}else if("INSTALL_HIST".equals(entity_type)){
				// 설치정보 컴포넌트 이력데이터를 등록
				if(authCnt>0){
					HashMap tabData = (HashMap) dataMap.get("data");
					assetHistoryService.insertInstallTabListCompHistory(conf_id, tabData, chgMessage, user_id);
				}
			}
		}
	}
	
	/**
	 * selectOpmsCompTypeModList
	 * 각 컴포넌트 별 이력을 가져온다.
	 */
	// @@ 컴포넌트 추가대상 메서드
	public List selectOpmsCompTypeModList(List<HashMap> modHistoryList,HashMap compMap)throws NkiaException{
		
		String entity_id = StringUtil.parseString(compMap.get("ENTITY_ID"));
		String comp_type = StringUtil.parseString(compMap.get("COMP_TYPE"));
		
		compMap.put("entity_id", entity_id);
		
		List<HashMap> resultList = new ArrayList<HashMap>();
		
		if ("MANAGER".equalsIgnoreCase(comp_type)) {
			//담당자 수정이력을 가져옫나.
			resultList = assetAutomationDAO.selectOperModHistoryList(compMap);
			webixUIHandler.addTargetList(resultList,modHistoryList);
		} else if ("REL_SERVICE".equalsIgnoreCase(comp_type)) {
			//연관 서비스 수정리스트를 가져온다.
			resultList = assetAutomationDAO.selectServiceModHistoryList(compMap);
			webixUIHandler.addTargetList(resultList,modHistoryList);
		} else if ("REL_INFRA".equalsIgnoreCase(comp_type)) {
			//설정된 리스트를 가져온다.
			List<HashMap> attrList = selectOpmsCompSetupList(compMap);
			
			for(int i=0;i<attrList.size();i++){
				//해당 분류별 수정이력을 가져온다.
				HashMap attrMap = attrList.get(i);
				String class_type = StringUtil.parseString(attrMap.get("CODE_ID"));
				String class_type_nm = StringUtil.parseString(attrMap.get("CODE_NM"));
				
				compMap.put("CLASS_TYPE", class_type);
				compMap.put("CLASS_TYPE_NM", class_type_nm);
				
				if("LOGSV".equalsIgnoreCase(class_type)){
					//논리는 따로 관리합니다.
				}else if("HA".equalsIgnoreCase(class_type)){
					//이중화는 사용하지 않음
				}else{
					//서버, 네트워크, 보안, 백업, 스토리지 등.. am_conf_rel 에 들어가는 이력 리스트
					resultList = assetAutomationDAO.selectConfModHistoryList(compMap);
				}
				webixUIHandler.addTargetList(resultList,modHistoryList);
			}
		} else if ("INSTALL".equalsIgnoreCase(comp_type)) {
			//설치정보 설정 정보를 가져온다.
			List<HashMap> attrList = selectOpmsCompSetupList(compMap);
			
			for(int i=0;i<attrList.size();i++){
				
				HashMap attrMap = attrList.get(i);
				//설치 분류 유형
				String ins_type = StringUtil.parseString(attrMap.get("CODE_ID"));
				//설치 분류 명
				String ins_type_nm = StringUtil.parseString(attrMap.get("CODE_NM"));
				//테이블 명 .. ems 연계 정보로 보여줄때는.. ems 테이블로 보여준다..
				String table_nm = StringUtil.parseString(attrMap.get("TABLE_NM"));
				
				compMap.put("COMP_ID", ins_type);
				compMap.put("COMP_NM", ins_type_nm);
				compMap.put("TABLE_NM", table_nm);
				
				//수정이력 리스트를 가져온다.
				resultList = assetAutomationDAO.selectInstallModHistoryList(compMap);
				
				webixUIHandler.addTargetList(resultList,modHistoryList);
			}
		}
		
		return modHistoryList;
	}
	
	/**
	 * selectCompDetailModList
	 * 그리드 컴포넌트의 이력 상세 정보를 불러온다.
	 */
	// @@ 컴포넌트 추가대상 메서드
	public List<HashMap> selectCompDetailModList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		
		webixUIHandler.replaceAddRowerKey(paramMap);
		
		String comp_type = StringUtil.parseString(paramMap.get("COMP_TYPE"));
		String tab_type = StringUtil.parseString(paramMap.get("TAB_TYPE"));
		
		List<HashMap> resultList = new ArrayList<HashMap>();
		
		if("MANAGER".equalsIgnoreCase(comp_type)){
			//담당자 수정이력
			resultList = assetAutomationDAO.selectOperModDetailList(paramMap);
		}else if("REL_SERVICE".equalsIgnoreCase(comp_type)){
			//연관 서비스 수정이력
			resultList = assetAutomationDAO.selectRelServiceModDetailList(paramMap);
		}else if("REL_INFRA".equalsIgnoreCase(comp_type)){
			//연관 구성장비 수정이력
			resultList = assetAutomationDAO.selectRelConfHisDetailList(paramMap);
		}else if("INSTALL".equalsIgnoreCase(comp_type)){
			//해당 컬럼 리스트를 가져온다.
			List installColList = assetAutomationDAO.selectOpmsInstallColumnList(tab_type);
			//설치 정보 이력을 가져오기 위한 쿼리를 만든다.
			String installColQuery = webixUIHandler.createInstallHisQuery(installColList);
			paramMap.put("installColQuery", installColQuery);
			
			//수정이력 리스트를 가져온다.
			resultList = assetAutomationDAO.selectInstallHisDetailList(paramMap);
		}
		
		return resultList;
	}
	
	/**
	 * registComponentHistory
	 * 그리드 컴포넌트들의 이력을 등록한다.
	 */
	// @@ 컴포넌트 추가대상 메서드
	/*public void registComponentHistory(HashMap dataMap, List<HashMap> componentList) throws NkiaException {
		
		String chg_his_seq 	= StringUtil.parseString(dataMap.get("chg_his_seq"));
		String conf_id = StringUtil.parseString(dataMap.get("conf_id"));
		String asset_id = StringUtil.parseString(dataMap.get("asset_id"));
		String user_id = StringUtil.parseString(dataMap.get("user_id"));
		
		for(int i=0;i<componentList.size();i++){
			
			HashMap compInfo = componentList.get(i);
			String compCode = StringUtil.parseString(compInfo.get("COMP_CODE"));
			
			webixUIHandler.replaceAddRowerKey(compInfo);
			
			if ("MANAGER".equalsIgnoreCase(compCode)) {
				assetAutomationDAO.insertCopyOperHis(dataMap);
			} else if ("REL_SERVICE".equalsIgnoreCase(compCode)) {
				assetAutomationDAO.insertCopyServiceHis(dataMap);
			} else if ("REL_INFRA".equalsIgnoreCase(compCode)) {
				//assetAutomationDAO.insertCopyHaHist(dataMap);
				//assetAutomationDAO.insertCopyLogSvHist(dataMap);
				assetAutomationDAO.insertCopyRelConfHis(dataMap);
				
				// 20150810 정정윤 추가 - 연관장비 이력 등록 시, 상대방 구성에도 수정이력 쌓이도록
				List targetConfList = assetAutomationDAO.selectCopyRelConfTargetList(dataMap);
				
				for(int j=0; j<targetConfList.size(); j++) {
					HashMap targetMap = new HashMap();
					HashMap subDataMap = new HashMap();
					targetMap = (HashMap)targetConfList.get(j);
					
					subDataMap.put("asset_id", targetMap.get("ASSET_ID"));
					subDataMap.put("conf_id", targetMap.get("CONF_ID"));
					subDataMap.put("chg_his_seq", dataMap.get("chg_his_seq"));
					subDataMap.put("user_id", dataMap.get("user_id"));
					subDataMap.put("cur_rel_conf_id", dataMap.get("conf_id"));
					subDataMap.put("target_chg_his_seq", dataMap.get("chg_his_seq"));
					
					assetAutomationDAO.insertCopyRelConfHis(subDataMap);
					assetAutomationDAO.insertChgHisSeq(subDataMap);
				}
				
			} else if ("INSTALL".equalsIgnoreCase(compCode)) {
				
				List<HashMap> installList = assetAutomationDAO.selectOpmsCompInstallList(compInfo);
				
				for(int j=0;j<installList.size();j++){
					HashMap installComp = installList.get(j);
					String comp_id = StringUtil.parseString(installComp.get("CODE_ID"));
					
					HashMap installInfo = assetAutomationDAO.selectOpmsInstallInfo(comp_id);
					
					webixUIHandler.paramSync(dataMap, installInfo);
					
					installInfo.put("ins_type", comp_id);
					List<HashMap> installColList = assetAutomationDAO.selectOpmsInstallColumnList(comp_id);
					List<HashMap> prevDataList = assetAutomationService.selectOpmsInstallInfoList(installInfo);
					
					String emsShowYn =  StringUtil.parseString(installInfo.get("EMS_SHOW_YN"));
					String emsTblNm = StringUtil.parseString(installInfo.get("EMS_TABLE_NM"));
					String dcaShowYn =  StringUtil.parseString(installInfo.get("DCA_SHOW_YN"));
					String dcaTblNm = StringUtil.parseString(installInfo.get("DCA_TABLE_NM"));
					String tableNm	= StringUtil.parseString(installInfo.get("TABLE_NM"));
					
					if("Y".equalsIgnoreCase(emsShowYn) && "N".equalsIgnoreCase(dcaShowYn)){
						//ems로 다이렉트로 보여줄경우..
						for(int k=0;k<prevDataList.size();k++){
							
							HashMap preDataMap = (HashMap)prevDataList.get(k);
							HashMap hisDataMap = new HashMap();
							
							hisDataMap.put("conf_id"			, conf_id);
							hisDataMap.put("asset_id"			, asset_id);
							hisDataMap.put("user_id"			, user_id);
							hisDataMap.put("chg_his_seq"		, chg_his_seq);
							hisDataMap.put("chg_cud"			, "U");
							hisDataMap.put("seq"				, j+1);
							
							for(int l=0;l<installColList.size();l++){
								HashMap installColMap = installColList.get(l);
								
								String colId = StringUtil.parseString(installColMap.get("EMS_COL_ID"));
								String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
								
								hisDataMap.put("code_yn"		, "N");
								hisDataMap.put("chg_yn"			, "N");
								hisDataMap.put("chg_table"		, emsTblNm);
								hisDataMap.put("chg_column_nm"	, colNm);
								hisDataMap.put("chg_column_id"	, colId);
								hisDataMap.put("chg_prev_value"	, preDataMap.get(colId));
							}
							assetAutomationDAO.insertAmAssetChgHis(hisDataMap);
						}
					}else if("Y".equalsIgnoreCase(dcaShowYn) && "N".equalsIgnoreCase(emsShowYn)){
						//DCA로 다이렉트로 보여줄경우..
						for(int k=0;k<prevDataList.size();k++){
							
							HashMap preDataMap = (HashMap)prevDataList.get(k);
							HashMap hisDataMap = new HashMap();
							
							hisDataMap.put("conf_id"			, conf_id);
							hisDataMap.put("asset_id"			, asset_id);
							hisDataMap.put("user_id"			, user_id);
							hisDataMap.put("chg_his_seq"		, chg_his_seq);
							hisDataMap.put("chg_cud"			, "U");
							hisDataMap.put("seq"				, j+1);
							
							for(int l=0;l<installColList.size();l++){
								HashMap installColMap = installColList.get(l);
								
								String colId = StringUtil.parseString(installColMap.get("DCA_COL_ID"));
								String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
								
								hisDataMap.put("code_yn"		, "N");
								hisDataMap.put("chg_yn"			, "N");
								hisDataMap.put("chg_table"		, emsTblNm);
								hisDataMap.put("chg_column_nm"	, colNm);
								hisDataMap.put("chg_column_id"	, colId);
								hisDataMap.put("chg_prev_value"	, preDataMap.get(colId));
							}
							assetAutomationDAO.insertAmAssetChgHis(hisDataMap);
						}
					}else{
						for(int k=0;k<prevDataList.size();k++){
							
							HashMap preDataMap = (HashMap)prevDataList.get(k);
							HashMap hisDataMap = new HashMap();
							
							hisDataMap.put("conf_id"			, conf_id);
							hisDataMap.put("asset_id"			, asset_id);
							hisDataMap.put("user_id"			, user_id);
							hisDataMap.put("chg_his_seq"		, chg_his_seq);
							hisDataMap.put("chg_cud"			, "U");
							hisDataMap.put("seq"				, k+1);
						
							for(int l=0;l<installColList.size();l++){
								
								HashMap installColMap = installColList.get(l);
								
								String colId = StringUtil.parseString(installColMap.get("COL_ID"));
								String colNm = StringUtil.parseString(installColMap.get("COL_NM"));
								String xtype = StringUtil.parseString(installColMap.get("XTYPE"));
								String curVal = "";
								String preVal = "";
								
								if("combo".equalsIgnoreCase(xtype)){
									hisDataMap.put("code_yn"			, "Y");
								}else{
									hisDataMap.put("code_yn"			, "N");
								}
								
								hisDataMap.put("chg_yn"			, "N");
								hisDataMap.put("chg_table"		, tableNm);
								hisDataMap.put("chg_column_nm"	, colNm);
								hisDataMap.put("chg_column_id"	, colId);
								hisDataMap.put("chg_prev_value"	, preDataMap.get(colId));
								
								assetAutomationDAO.insertAmAssetChgHis(hisDataMap);
							}
						}
					}
					dataMap.put("PRE_INS_"+comp_id, installInfo);
				}
			}
		}
	}*/
	
	/**
	 * selectOpmsCompSetupList
	 * 탭 리스트에 대해 해당 자산원장에서 사용 중인 그리드 목록을 불러온다.
	 */
	public List selectOpmsCompSetupList(HashMap entityMap)throws NkiaException{
		String compType = (String)entityMap.get("COMP_TYPE");
		List attrList = new ArrayList<HashMap>();
		
		//연관 장비 정보에서 맵핍된 분류 유형을 가져온다.
		String confAdminYn = new String();
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		entityMap.put("confAdminYn", confAdminYn); 
		if("REL_INFRA".equalsIgnoreCase(compType)){
			
			
			if("Y".equals(confAdminYn)) {
				// 구성용도를 관계정보에 매핑하게 됨. 구성용도는 멀티코드에서 관리합니다.
				entityMap.put("map_code_id", "CONF_TYPE"); 
			} else {
				// 기존 ITSM 방법. 자산분류체계를 관계정보에 매핑하게 됨. 관계정보에 매핑할 자산분류체계는 AM_REL_CODE 코드에서 정의된 것만 매핑 가능합니다.
				entityMap.put("map_code_id", "AM_REL_CODE"); 
			}
			attrList = assetAutomationDAO.selectOpmsCompSetupList(entityMap);
		}else if("INSTALL".equalsIgnoreCase(compType)){
			//설치 정보에서 맵핑된 분류를 가져온다.
			attrList = assetAutomationDAO.selectOpmsCompInstallList(entityMap);
		}else if("UPDATE".equalsIgnoreCase(compType)){
			//수정이력 인경우 설치 정보, 연계 장비의 탭 리스트의 요소를 가져 온다.
			attrList = assetAutomationDAO.selectModViewCompList(entityMap);
		}
		return attrList;
	}
	
	private void updateCompRelIP(String confId, List<HashMap> dataList, String updUserId, String chgMessage) throws NkiaException {
		// 컴포넌트 이력데이터를 등록
		assetHistoryService.insertRelIPCompHistory(confId, dataList, chgMessage, updUserId);
	
		// 기존 데이터 삭제
		assetAutomationDAO.deleteAmRelIP(confId);
		// 데이터 수정
		if(dataList != null && !dataList.isEmpty()) {
			for(HashMap currentData : dataList) {
				currentData.put("CONF_ID", confId);
				currentData.put("UPD_USER_ID", updUserId);
				assetAutomationDAO.insertAmRelIP(currentData);
			}
		}
	}

}
