package com.nkia.itg.itam.automation.dao;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * 자산원장 화면 자동화
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2016. 11. 11. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("assetAutomationDAO")
public class AssetAutomationDAO extends EgovComAbstractDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(AssetAutomationDAO.class);
	
	/**
	 * 분류별 속성 정보 (사용자 권한과 분류해당 item들을 가져온다.)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectClassEntityInfo(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectClassEntityInfo",paramMap);
	}
	
	
	/**
	 * hmsong cmdb 개선 작업
	 * 분류별 속성 정보 (선택한 자원의 자산, 구성정보를 모두 가져옴)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectClassConfUnionEntityInfo(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectClassConfUnionEntityInfo",paramMap);
	}
	
	/**
	 * 논리서버 연관 장비 정보 등록
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertLogSvRelConf(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertLogSvRelConf",paramMap);
	}
	/**
	 * 
	 * TODO 
	 * 사용자 지정에 해당되는 속성(컬럼정보) 들을 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectManualAttrList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectManualAttrList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 사용자의 해당 item들에 대한 권한을 체크한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectCompAuthList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectCompAuthList",paramMap);
	}
	
	/**
	 * 
	 * hmsong cmdb 개선 작업
	 * 사용자의 해당 자산, 구성 item들에 대한 권한을 체크한다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public Integer selectClassConfUnionUpdateAuthCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectClassConfUnionUpdateAuthCnt",paramMap);	
	}
	
	/**
	 * 
	 * TODO
	 * 사용자 지정 중에서 권한 설정을 전체 읽기나 전체 수정으로 한경우
	 * 해당 item의 속성정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectMnualAttrListByType(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectMnualAttrListByType",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 컴포넌트중 연계정보 그룹 정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsCompSetupList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsCompSetupList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 컴포넌트중 유지보수 정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectMaintFormDataList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectMaintFormDataList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 설치정보 리스트를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsCompInstallList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsCompInstallList",paramMap);
	}
	
	/**
	 * 선택된 담당자 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelMngUserList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsSelMngUserList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 담당자 선택 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsMngUserCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsMngUserCnt",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsMngUserList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsMngUserList",paramMap);
	}
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		return list("AssetAutomationDAO.searchCodeDataList", paramMap);
	}
	
	/**
	 * 자자산 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelChildList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsSelChildList",paramMap);
	}
	
	/**
	 * 구매정보 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelPurInfoList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsSelPurInfoList",paramMap);
	}
	/**
	 * 구매정보 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsDepreciationInfoList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsDepreciationInfoList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 선택된 사용자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelUserList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsSelUserList",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 사용자 선택 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsUserCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsUserCnt",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 사용자 선택 팝업 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsUserList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsUserList",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 사용자 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteUser(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.deleteUser",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 사용자 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertUser(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertUser",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 소프트웨어 라이센스 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSwLicenceList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsSwLicenceList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 소프트웨어 라이센스 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateSwLicence(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.updateSwLicence",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 이중화 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelHaGrpList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsRelHaGrpList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 논리 연계장비 정보
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsReLogConfList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsReLogConfList",paramMap);
	}
	
	/**
	 * 연관 장비 정보 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelConfList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsRelConfList",paramMap);
	}
	
	/**
	 * 연관 장비(DR) 정보 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelDRList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsRelDRList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 자산 분류별 트리가 나온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelConfClassTree(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectRelConfClassTree",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 구성 용도별 트리가 나온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelConfTypeTree(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchConfTypeTree",paramMap);
	}
	
	/**
	 * 연계 장비 정보 팝업 리스트 카운트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsConfCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsConfCnt",paramMap);
	}

	/**
	 * 
	 * TODO
	 * 연계장비 정보 팝업 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsConfList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("AssetAutomationDAO.selectOpmsConfList",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 연관 서비스 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("AssetAutomationDAO.selectOpmsRelServiceList",paramMap);
	}
	
	/**
	 * 연관 서비스 선택 팝업 리스트 카운트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsServiceCnt(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsServiceCnt",paramMap);
	}

	/**
	 * 연관 서비스 선택 팝업 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsServiceList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("AssetAutomationDAO.selectOpmsServiceList",paramMap);
	}
	
	/**
	 * 연관 서비스 리스트 팝업 부서 검색조건 콤보 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectServiceCustList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("AssetAutomationDAO.selectServiceCustList",paramMap);
	}
	
	/**
	 * 테이블 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTblList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("AssetAutomationDAO.selectTblList",paramMap);
	}
	
	/**
	 * 조인 정보
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectJoinList(HashMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("AssetAutomationDAO.selectJoinList",paramMap);
	}
	
	/**
	 * hashMap 리턴타입 쿼리 실행
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public HashMap excuteQuery(String query)throws NkiaException{
		return (HashMap)selectByPk("AssetAutomationDAO.excuteQuery",query);
	}
	
	/**
	 * 수정 권한 갯수
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectUpdateAuthCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectUpdateAuthCnt",paramMap);	
	}

	/**
	 * 가져오고싶은 타입 별로 컬럼을 가져온다.
	 * SKEYID	: 여러테이블 중에서 검색조건 ..  conf_id,asset_id를 가져온다.
	 * UKEYID	: 해당 테이블 중에서 검색조건 ..  conf_id,asset_id를 가져온다.
	 * UPDID	: 디폴트로 들어가는 컬럼 upd_user_id,upd_dt 존재 여부 판단 컬럼을 가져온다.
	 * COL_ID 	: 실재 스크마 정보에서 존재하는 컬럼들만을 가져온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTypeColumn(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectTypeColumn",paramMap);
	}

	/**
	 * 새로 생성된 수정테이블 seq 정보를 입력한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertChgHisSeq(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertChgHisSeq",paramMap);
	}
	
	/**
	 * 이력정보 테이블의 id를 생성한다. 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectAssetChgHisSeq(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.selectAssetChgHisSeq",paramMap);
	}
	
	/**
	 * 속성 이력정보를 입력한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertAmAssetChgHis(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertAmAssetChgHis",paramMap);
	}
	
	/**
	 * 속성이력정보에 같은 속성 카운트를 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectAmAssetChgHisCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectAmAssetChgHisCnt",paramMap);
	}
	
	/**
	 * 파일 id를 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectatchFileId(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.selectatchFileId",paramMap);
	}
	
	/**
	 * 파일 id를 수정한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateAtchFileId(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateAtchFileId",paramMap);
	}
	
	/**
	 * 담당자 정보 이력테이블에 입력한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertOperHist(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertOperHist",paramMap);
	}
	
	/**
	 * 담당자 정보를 등록한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertOper(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertOper",paramMap);
	}
	/**
	 * 담당자 정보를 등록한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateCust(HashMap dataMap)throws NkiaException{
		update("AssetAutomationDAO.updateCust", dataMap);
	}
	
	
	/**
	 * 담당자를 삭제한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteOper(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.deleteOper",paramMap);
	}
	
	/**
	 * 연관 서비스 이력정보를 입략힌다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertServiceHist(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.insertServiceHist",paramMap);
	}
	
	/**
	 * 연관 서비스 입략힌다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertService(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertService",paramMap);
	}
	
	/**
	 * 연관서비스 정보 삭제
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteService(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.deleteService",paramMap);
	}
	
	/**
	 * 이중화 이력정보를 등록한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertHaHist(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.insertHaHist",paramMap);
	}
	
	/**
	 * 논리 수정 이력정보를 넣는다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertLogSvHist(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.insertLogSvHist",paramMap);
	}
	
	/**
	 * 연관장비 정보 이력을 넣는다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertRelConfHist(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.insertRelConfHist",paramMap);
	}
	
	/**
	 * 연관 장비 정보 삭제
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteRelConf(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.deleteRelConf",paramMap);
	}
	
	/**
	 * 연관 장비 정보 등록
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertRelConf(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertRelConf",paramMap);
	}
	
	/**
	 * 연관 장비(DR) 정보 삭제
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteRelDR(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.deleteRelDR",paramMap);
	}
	
	/**
	 * 연관 장비(DR) 정보 등록
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertRelDR(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertRelDR",paramMap);
	}
	
	/**
	 * 연관 장비 사용률 등록
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateRelConfUseRate(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.updateLogSvUseRate",paramMap);
	}
	
	/**
	 * 설치 정보 테이블 정보 를 가져온다.
	 * TODO
	 * 
	 * @param ins_type
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectOpmsInstallInfo(String ins_type)throws NkiaException{
		return (HashMap)selectByPk("AssetAutomationDAO.selectOpmsInstallInfo", ins_type);
	}
	
	/**
	 * 설치 정보 테이블 관련 속성을 가져온다.
	 * TODO
	 * 
	 * @param ins_type
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInstallColumnList(String ins_type)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsInstallColumnList", ins_type);
	}
	
	/**
	 * 설치 정보 테이블 관련 속성을 가져온다.
	 * TODO
	 * 
	 * @param ins_type
	 * @return
	 * @throws NkiaException 
	 */
	public List selectOpmsInstallColumnListByAdTarget(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsInstallColumnListByAdTarget", paramMap);
	}
	
	/**
	 * 결과값이 리스트 형태인 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public List excuteQueryList(String query)throws NkiaException{
		return list("AssetAutomationDAO.excuteQueryList", query);
	}
	
	/**
	 * 등록,삭제,수정 등의 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @throws NkiaException
	 */
	public void excuteCudQuery(String query)throws NkiaException{
		delete("AssetAutomationDAO.excuteCudQuery", query);
	}
	
	/**
	 * 결과 값이 숫자유형의 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public int excuteQueryInt(String query)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.excuteQueryInt", query);
	}
	
	/**
	 * 장비 명명 정보를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectOpmsConfNmCodeInfo(HashMap dataMap)throws NkiaException{
		return (HashMap) selectByPk("AssetAutomationDAO.selectOpmsConfNmCodeInfo", dataMap);
	}
	/**
	 * 유지보수 정보를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectOpmsMaintFormInfo(HashMap dataMap)throws NkiaException{
		return (HashMap) selectByPk("AssetAutomationDAO.selectOpmsMaintFormInfo", dataMap);
	}
	
	
	
	/**
	 * 결과 값이 String 쿼리를 실행한다.
	 * TODO
	 * 
	 * @param query
	 * @return
	 * @throws NkiaException
	 */
	public String excuteQueryString(String query)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.excuteQueryString", query);
	}
	
	/**
	 * 장비 명명정보 카운트를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsConfNmCodeCnt(HashMap dataMap)throws NkiaException{
		return (Integer) selectByPk("AssetAutomationDAO.selectOpmsConfNmCodeCnt", dataMap);
	}
	
	/**
	 * 장비 명명 정보를 등록한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertOpmsConfNmCode(HashMap dataMap)throws NkiaException{
		insert("AssetAutomationDAO.insertOpmsConfNmCode", dataMap);
	}
	
	/**
	 * 장비 명명정보를 수엊ㅇ한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateOpmsConfNmCode(HashMap dataMap)throws NkiaException{
		update("AssetAutomationDAO.updateOpmsConfNmCode", dataMap);
	}
	
	/**
	 * 해당 관리그룹에 속하는 테이블 모두를 가져온다..
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityManualTable(HashMap dataMap)throws NkiaException{
		return list("AssetAutomationDAO.selectEntityManualTable",dataMap);
	}
	
	/**
	 * 해당 테이블에 속하는 모든 테이블을 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectTabAllColumnsList(HashMap dataMap)throws NkiaException{
		return list("AssetAutomationDAO.selectTabAllColumnsList",dataMap);
	}
	
	/**
	 * 속성이력의 경우 미리등록된 정보에 수정한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateAmAssetChgHisModYn(HashMap dataMap)throws NkiaException{
		update("AssetAutomationDAO.updateAmAssetChgHisModYn",dataMap);
	}
	
	/**
	 * ems_id를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectEmsIds(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.selectEmsIds", dataMap);
	}
	
	/**
	 * dca_id를 가져온다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectDcaIds(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.selectDcaIds", dataMap);
	}
	
	/**
	 * 분류체계 정보를 가졍노다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectClassInfo(HashMap dataMap)throws NkiaException{
		return (HashMap)selectByPk("AssetAutomationDAO.selectClassInfo", dataMap);
	}
	
	/**
	 * 자산 id를 생성 한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createAssetId(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.createAssetId", dataMap);
	}
	
	/**
	 * 자산 id를 생성 한다. (도입일이 있을 경우)
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createAssetIdExistIntroDt(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.createAssetIdExistIntroDt", dataMap);
	}
	
	
	/**
	 * 구성id를 생성한다.
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String createConfId(HashMap dataMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.createConfId", dataMap);
	}
	
	/**
	 * 기본적인 필수값들을 update한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBaseDataInfo(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateBaseDataInfo",paramMap);
	}
	
	/**
	 * 논리 구성에서 기본적인 필수값을 update한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBaseLogSvInfo(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateBaseLogSvInfo",paramMap);
	}
	
	/**
	 * hmsong cmdb 개선 작업
	 * (구성분류)타입별 분류리스트를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchConfTypeTree(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchConfTypeTree",paramMap);
	}
	
	/**
	 * 수정이력관리 그룹을 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectModViewCompList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectModViewCompList",paramMap);
	}
	
	/**
	 * 
	 * 가상서버(클라우드와 연계된 가상서버) 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsVmList(ModelMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsVmList",paramMap);
	}
	
	/**
	 * 
	 * 속성 수정 이력정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsFormModList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsFormModList",paramMap);
	}
	
	/**
	 * 
	 * 담당자 수정 이력 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOperModHistoryList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOperModHistoryList",paramMap);
	}
	
	/**
	 * 
	 * 연관서비스 수정 이력 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectServiceModHistoryList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectServiceModHistoryList",paramMap);
	}
	
	/**
	 * 
	 * 연관장비 수정이력
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectConfModHistoryList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectConfModHistoryList",paramMap);
	}
	
	/**
	 * 
	 * 설치정보 수정이력
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectInstallModHistoryList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectInstallModHistoryList",paramMap);
	}
	
	/**
	 * 속성별 상세 이력 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectFormAttrModList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectFormAttrModList",paramMap);
	}
	
	/**
	 * 
	 * TODO 담당자 상세 수정 이력정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOperModDetailList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOperModDetailList",paramMap);
	}
	
	/**
	 * 
	 * TODO 서비스 수정이력 상세
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelServiceModDetailList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectRelServiceModDetailList",paramMap);
	}
	
	/**
	 * 
	 * TODO 연관 구성 장비 수정이력 상세 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelConfHisDetailList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectRelConfHisDetailList",paramMap);
	}
	
	/**
	 * 
	 * TODO 설치정보 수정이력 상세 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectInstallHisDetailList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectInstallHisDetailList",paramMap);
	}
	

	/**
	 * 정보수집서 데이터 insert 유무 확인
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectDataInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectDataInsertYn",paramMap);	
	}
	
	public int selectOpmsInfoServiceInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsInfoServiceInsertYn",paramMap);	
	}
	
	public int selectOpmsInfoRelConfInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsInfoRelConfInsertYn",paramMap);	
	}
	
	public int selectOpmsInfoInstallInsertYn(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsInfoInstallInsertYn",paramMap);	
	}
	
	/**
	 * 정보수집서 연관 서비스 등록.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertOpmsInfoService(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertOpmsInfoService",paramMap);
	}
	
	/**
	 * 정보수집서 연관장비 정보.
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertOpmsInfoRelConf(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.insertOpmsInfoRelConf",paramMap);
	}	
	
	/**
	 * 
	 * TODO 컴포넌트 리스트를 모두 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityCompList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectEntityCompList",paramMap);
	}
	
	/**
	 * 
	 * TODO 히스토리테이블에 그대로 넣는다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyOperHis(HashMap dataMap) throws NkiaException{
		insert("AssetAutomationDAO.insertCopyOperHis",dataMap);
	}
	
	/**
	 * 
	 * TODO 서비스 정보를 히스토리테이블에 그대로 넣는다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyServiceHis(HashMap dataMap) throws NkiaException{
		insert("AssetAutomationDAO.insertCopyServiceHis",dataMap);
	}
	
	/**
	 * 
	 * TODO 연관구성 정보 대상 구성정보 리스트 -20150810 정정윤 추가
	 * 
	 * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List selectCopyRelConfTargetList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectCopyRelConfTargetList",paramMap);
	}
	
	/**
	 * 
	 * TODO 연관구성 정보를 히스토리테이블에 그대로 넣는다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyRelConfHis(HashMap dataMap) throws NkiaException{
		insert("AssetAutomationDAO.insertCopyRelConfHis",dataMap);
	}
	
	public void updateOperHist(HashMap dataMap) throws NkiaException{
		update("AssetAutomationDAO.updateOperHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateRelServiceHist(HashMap dataMap) throws NkiaException{
		update("AssetAutomationDAO.updateRelServiceHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateRelConfHist(HashMap dataMap) throws NkiaException{
		update("AssetAutomationDAO.updateRelConfHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateHaHist(HashMap dataMap) throws NkiaException{
		update("AssetAutomationDAO.updateHaHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 수정여부를 수정한다.
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateLogSvHist(HashMap dataMap) throws NkiaException{
		update("AssetAutomationDAO.updateLogSvHist",dataMap);
	}
	
	
	/**
	 * 
	 * TODO 이중화 연관
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyHaHist(HashMap dataMap) throws NkiaException{
		insert("AssetAutomationDAO.insertCopyHaHist",dataMap);
	}
	
	/**
	 * 
	 * TODO 논리 연관
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertCopyLogSvHist(HashMap dataMap) throws NkiaException{
		insert("AssetAutomationDAO.insertCopyLogSvHist",dataMap);
	}
	
	/**
	 * 
	 * TODO update 여부만 변경
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateInsAmAssetChgHisModYn(HashMap dataMap) throws NkiaException{
		update("AssetAutomationDAO.updateInsAmAssetChgHisModYn",dataMap);
	}
	
	/**
	 * 
	 * TODO
	 * 분류체계별 속성관리에서 버튼 컴퍼넌트 목록을 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsBtnTree(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsBtnTree",paramMap);
	}
	
	/**
	 * 분류체계별 속성관리에서 엔티티에 대한 버튼목록을 가져온다
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEntityBtnInfo(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectEntityBtnInfo",paramMap);
	}
	/**
	 * 모델속성을 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchModelTreeComp(ModelMap paramMap) throws NkiaException {
		return  list("AssetAutomationDAO.searchModelTreeComp", paramMap);
		
	}	
	/**
	 * 업무분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchWorkTreeComp(ModelMap paramMap) throws NkiaException {
		return  list("AssetAutomationDAO.searchWorkTreeComp", paramMap);
		
	}

	/**
	 * HISOS를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchHISOSTreeComp(ModelMap paramMap) throws NkiaException {
		return  list("AssetAutomationDAO.searchHISOSTreeComp", paramMap);
		
	}

	public int selectAssetHisByAttribute(HashMap paramMap)throws NkiaException {
		return (Integer)selectByPk("AssetAutomationDAO.selectAssetHisByAttribute", paramMap);
	}	
	
	/**
	 * 설치된 소프트웨어 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInsSwList(HashMap paramMap) throws NkiaException {
		return  list("AssetAutomationDAO.selectOpmsInsSwList", paramMap);
		
	}
	
	/**
	 * 설치 소프트웨어와 맵핑되는 소프트웨어 옵션 전체 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOptionMappingList(HashMap paramMap) throws NkiaException {
		return  list("AssetAutomationDAO.selectOptionMappingList", paramMap);
		
	}
	
	/**
	 * 설치 소프트웨어와 맵핑되는 선택되어있던 소프트웨어 옵션 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSelectedOptionList(HashMap paramMap) throws NkiaException {
		return  list("AssetAutomationDAO.selectSelectedOptionList", paramMap);
		
	}
	
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 목록 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteInsSw(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.deleteInsSw",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 목록 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertInsSw(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertInsSw",paramMap);
	}
	
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteInsSwOption(HashMap paramMap)throws NkiaException{
		delete("AssetAutomationDAO.deleteInsSwOption",paramMap);
	}
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertInsSwOption(HashMap paramMap)throws NkiaException{
		insert("AssetAutomationDAO.insertInsSwOption",paramMap);
	}
	/**
	 * 유지보수 폼 정보를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAssetMaintInfo(HashMap paramMap)throws NkiaException{
		return (HashMap)selectByPk("AssetAutomationDAO.selectAssetMaintInfo", paramMap);
	}
	/**
	 * 
	 * TODO 유지보수 기본 정보 입력
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertAssetMaintBaseInfo(HashMap dataMap) throws NkiaException{
		insert("AssetAutomationDAO.insertAssetMaintBaseInfo",dataMap);
	}
	
	public void updateAssetMaintBaseInfo(HashMap dataMap) throws NkiaException{
		update("AssetAutomationDAO.updateAssetMaintBaseInfo",dataMap);
	}
	
	/**
	 * 유지보수 폼 정보를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectMaintYN(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("AssetAutomationDAO.selectMaintYN", paramMap);
	}
	
	public int selectMaintCnt(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectMaintCnt", paramMap);
	}
	
	public int selectMotherListCnt(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("AssetAutomationDAO.selectMotherListCnt",paramMap);
	}

	public List selectMotherList(HashMap paramMap) throws NkiaException {
		return list("AssetAutomationDAO.selectMotherList",paramMap);
	}
	/**
	 * 선택한 자자산의 모자산을 업데이트 한다. 
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateMotherInfo(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateMotherInfo",paramMap);
	}

	public List selectAmIntroDtGroupBy(ModelMap paramMap) throws NkiaException {
		return list("AssetAutomationDAO.selectAmIntroDtGroupBy",paramMap);
	}
	
	/**
	 * 연계 논리구성 정보 (단일 데이터.. 자바에서 반복문으로 돌면서 꺼내와야함)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectRelLocSv(HashMap paramMap)throws NkiaException{
		return (HashMap)selectByPk("AssetAutomationDAO.selectRelLocSv", paramMap);
	}
	
	/**
	 * 자품 변경 이력 시퀀스 따옴
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectChildChgHisSeq(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("AssetAutomationDAO.selectChildChgHisSeq",paramMap);
	}
	
	/**
	 * 자품 변경 이력 수정횟수 따옴
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectChildChgHisCnt(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("AssetAutomationDAO.selectChildChgHisCnt",paramMap);
	}
	
	/**
	 * 자품 변경 이력 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertChildChgHis(HashMap dataMap) throws NkiaException{
		insert("AssetAutomationDAO.insertChildChgHis",dataMap);
	}
	
	/**
	 * 자품 변경 이력 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisList(HashMap paramMap) throws NkiaException{
		return list("AssetAutomationDAO.selectChildChgHisList",paramMap);
	}
	
	/**
	 * 자품 변경 이력 상세 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisDetail(HashMap paramMap) throws NkiaException{
		return list("AssetAutomationDAO.selectChildChgHisDetail",paramMap);
	}
	
	/**
	 * 논리서버 이력 리스트
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsLogicHisList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsLogicHisList",paramMap);
	}
	
	/**
	 * 자산 등록시 클래스 명
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchClassNm(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("AssetAutomationDAO.searchClassNm", paramMap);
	}
	
	/**
	 * hmsong cmdb 개선 작업
	 * 자산 등록시 구성용도 명
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchConfTypeNm(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("AssetAutomationDAO.searchConfTypeNm", paramMap);
	}

	/**
	 * 자산등록자 및 자산등록일 입력
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateInsUserInfo(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateInsUserInfo",paramMap);
	}

	public int selectOpmsInfoOperInsertYn(HashMap curMngMap) {
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsInfoOperInsertYn", curMngMap);	
	}

	public void insertOpmsInfoOper(HashMap curMngMap) {
		insert("AssetAutomationDAO.insertOpmsInfoOper", curMngMap);
	}	
	
	/**
	 * 전체요청 이력(as-is)
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProcMasterHist(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchProcMasterHist",paramMap);
	}
	
	/**
	 * hmsong cmdb 개선 작업
	 * 전체요청 이력(자산과 연관)
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAssetProcMasterHist(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchAssetProcMasterHist",paramMap);
	}
	
	/**
	 * hmsong cmdb 개선 작업
	 * 전체요청 이력(구성과 연관)
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchConfProcMasterHist(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchConfProcMasterHist",paramMap);
	}
		
	/**
	 * 장애 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProcHist(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchProcHist",paramMap);
	}
	
	/**
	 * 변경 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchChangeProcHist(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchChangeProcHist",paramMap);
	}
	
	/**
	 * 문제 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProblemProcHist(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchProblemProcHist",paramMap);
	}
	
	/**
	 * 서비스 이력
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchServiceProcHist(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchServiceProcHist",paramMap);
	}

	public List selectIfPcSwList(ModelMap paramMap) {
		return list("AssetAutomationDAO.selectIfPcSwList", paramMap);
	}
	
	/**
	 * 하위 논리 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelLogicList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsRelLogicList",paramMap);
	}
	
	/**
	 * 가용성, 기밀성, 무결성 값 합산하여 중요도 값 업데이트
	 * TODO
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateImportanceByAvCnIn(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateImportanceByAvCnIn", paramMap);
	}
	
	/**
	 * 
	 * TODO 이력조회 - 장비별 요청현황 조회
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */		
	public List searchConfReqStateList(ModelMap paramMap) throws NkiaException{
		return list("AssetAutomationDAO.searchConfReqStateList", paramMap);
	}
	
	public int searchConfReqStateListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("AssetAutomationDAO.searchConfReqStateListCount", paramMap);
	}
	
	/**
	 * 자산 메모리 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsInsMemoryView(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchBsInsMemoryView",paramMap);
	}
	
	/**
	 * 자산 CPU Core 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsInsCpuView(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchBsInsCpuView", paramMap);
	}
	
	/**
	 * 자산 Storage 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsInsStorageView(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchBsInsStorageView", paramMap);
	}
	
	/**
	 * 자산  NW 서비스중단일관리 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsSvcStopView(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.searchBsSvcStopView", paramMap);
	}
	
	/**
	 * 자산정보 조회
	 * @param paramMap
	 * @return
	 */
	public HashMap selectAssetInfo(HashMap paramMap) {
		return (HashMap) selectByPk("AssetAutomationDAO.selectAssetInfo", paramMap);
	}

	/**
	 * 디스크 목록 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsRelDiskList(ModelMap paramMap) {
		return list("AssetAutomationDAO.selectOpmsRelDiskList", paramMap);
	}

	/**
	 * 디스크목록 삭제
	 * @param confId
	 * @throws NkiaException
	 */
	public void deleteAmconfRelDisk(String confId) throws NkiaException{
		delete("AssetAutomationDAO.deleteAmconfRelDisk", confId);
	}

	/**
	 * 디스크목록 저장
	 * @param currentData
	 */
	public void insertAmconfRelDisk(HashMap paramMap) {
		insert("AssetAutomationDAO.insertAmconfRelDisk", paramMap);
	}
	
	/**
	 * EMS수집 디스크 목록 조회
	 * @param paramMap
	 * @return
	 */
	public List selectEmsDiskList(ModelMap paramMap) {
		return list("AssetAutomationDAO.selectEmsDiskList", paramMap);
	}
	
	/**
	 * EMS수집 IP 목록 조회
	 * @param paramMap
	 * @return
	 */
	public List selectEmsIPList(ModelMap paramMap) {
		return list("AssetAutomationDAO.selectEmsIPList", paramMap);
	}

	/**
	 * NIC 목록 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsRelNicList(ModelMap paramMap) {
		return list("AssetAutomationDAO.selectOpmsRelNicList", paramMap);
	}
	
	/**
	 * NIC 목록 삭제
	 * @param confId
	 * @throws NkiaException
	 */
	public void deleteAmconfRelNic(String confId) throws NkiaException{
		delete("AssetAutomationDAO.deleteAmconfRelNic", confId);
	}
	
	/**
	 * NIC 목록 저장
	 * @param currentData
	 */
	public void insertAmconfRelNic(HashMap paramMap) {
		insert("AssetAutomationDAO.insertAmconfRelNic", paramMap);
	}

	/**
	 * 이중화 정보 목록 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsDualInfoList(ModelMap paramMap) {
		return list("AssetAutomationDAO.selectOpmsDualInfoList", paramMap);
	}
	
	/**
	 * 이중화 정보 목록 삭제
	 * @param confId
	 * @throws NkiaException
	 */
	public void deleteAmconfDualInfo(String confId) throws NkiaException{
		delete("AssetAutomationDAO.deleteAmconfDualInfo", confId);
	}
	
	/**
	 * 이중화 정보 목록 저장
	 * @param currentData
	 */
	public void insertAmconfDualInfo(HashMap paramMap) {
		insert("AssetAutomationDAO.insertAmconfDualInfo", paramMap);
	}
	
	/**
	 * 가상서버 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelVMList(HashMap paramMap)throws NkiaException{
		return list("AssetAutomationDAO.selectOpmsRelVMList",paramMap);
	}
	
	/**
	 * 
	 * 가상서버 목록 건수
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsRelVMCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectOpmsRelVMCount",paramMap);
	}
	
	/**
	 * IP목록 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsRelIPList(ModelMap paramMap) {
		return list("AssetAutomationDAO.selectOpmsRelIPList", paramMap);
	}

	/**
	 * IP목록 삭제
	 * @param confId
	 * @throws NkiaException
	 */
	public void deleteAmRelIP(String confId) throws NkiaException{
		delete("AssetAutomationDAO.deleteAmRelIP", confId);
	}

	/**
	 * IP목록 저장
	 * @param paramMap
	 */
	public void insertAmRelIP(HashMap paramMap) {
		insert("AssetAutomationDAO.insertAmRelIP", paramMap);
	}
	
	/**
	 * EMS 정보를 수정한다.
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateEmsInfo(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateEmsInfo",paramMap);
	}
	
	/**
	 * 자산정보에 등록된 EMS ID 건수 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectEmsIdCount(HashMap paramMap)throws NkiaException{
		return (Integer)selectByPk("AssetAutomationDAO.selectEmsIdCount",paramMap);
	}
	
	/**
	 * 유무형 자산 정보를 수정한다.
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateTangibleAsset(HashMap paramMap)throws NkiaException{
		update("AssetAutomationDAO.updateTangibleAsset", paramMap);
	}
	
	/**
	 * AM_ENTITY_TBL_MAPPING 테이블에 매핑된 테이블들이 asset 속성인지, conf 속성인지 구분. CONF_ID를 가지고 있으면 구성으로 보고, CONF_ID가 없으면 자산속성으로 봄.
	 * @param paramMap
	 * @return
	 */
	public List<HashMap> searchTableAssetConfDivInfo(HashMap paramMap) {
		return list("AssetAutomationDAO.searchTableAssetConfDivInfo", paramMap);
	}
}
