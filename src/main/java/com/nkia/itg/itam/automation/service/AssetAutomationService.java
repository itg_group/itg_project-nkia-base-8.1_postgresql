
package com.nkia.itg.itam.automation.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import net.sf.json.JSONArray;

/**
 * 자산원장 화면 자동화
 * @version 1.0
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 정정윤 JeongYun,Jeong
 * @since 2016. 11. 11. JDK 1.7
 * <pre>
 * TODO
 * </pre>
 *
 */
public interface AssetAutomationService {
	
	/**
	 * 그룹 하위 데이터 정보를 가져온다.
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void selectClassEntitySetupInfo(HashMap paramMap)throws NkiaException;
	
	/**
	 * 속성에서 hidden을 설정된 속성들을 가져온다.
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsHiddenList(List<HashMap> attrList,HashMap entityMap)throws NkiaException;
	
	/**
	 * 그룹과 권한에 설정된 속성정보를 가져온다.
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsMnualAllList(List<HashMap> attrList,HashMap entityMap)throws NkiaException;
	
	/**
	 * 권한 정보를 가져온다.
	 * @param attrList
	 * @param entityMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectOpmsCompAuth(List<HashMap> attrList,HashMap entityMap)throws NkiaException;
	
	/**
	 * 선택된 사용자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSwLicenceList(HashMap paramMap)throws NkiaException;
	
	/**
	 * 선택된 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelMngUserList(HashMap paramMap)throws NkiaException;
	
	/**
	 * 담당자 팝업 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsMngUserList(HashMap paramMap)throws NkiaException;

	/**
	 * 담당자 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsMngUserCnt(HashMap paramMap) throws NkiaException;
	
	
	
	/**
	 * 자자산 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelChildList(HashMap paramMap)throws NkiaException;
	/**
	 * 구매정보 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelPurInfoList(HashMap paramMap)throws NkiaException;	
	
	/**
	 * 구매정보 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsDepreciationInfoList(HashMap paramMap)throws NkiaException;
	

	/**
	 * 사용자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsSelUserList(HashMap paramMap)throws NkiaException;
	/**
	 * 사용자 팝업 담당자 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsUserList(HashMap paramMap)throws NkiaException;

	/**
	 * 사용자 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsUserCnt(HashMap paramMap) throws NkiaException;
	/**
	 * 서비스 항목 멀티코드 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException;
	

	
	/**
	 * 연관 장비 정보를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelConfList(HashMap paramMap)throws NkiaException;

	/**
	 * 구성 선택 팝업 분류별 트리를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectRelConfClassTree(HashMap paramMap) throws NkiaException;
	
	/**
	 * 구성 선택 팝업 분류별 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsConfCnt(HashMap paramMap) throws NkiaException;
	
	/**
	 * 구성 선택 팝업 분류별 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsConfList(HashMap paramMap)throws NkiaException;

	/**
	 * 연관 서비스 리스트 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelServiceList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 연관 서비스 선택 팝업 리스트 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsServiceCnt(HashMap paramMap) throws NkiaException;
	
	/**
	 * 연관 서비스 선택 팝업 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsServiceList(HashMap paramMap)throws NkiaException;

	/**
	 * 연관 서비스 선택 팝업 부서 콤보 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectServiceCustList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 설치 정보 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInstallInfoList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 자산 정보 업데이트
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateOpmsDetail(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산 정보 등록
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void registOpmsDetail(HashMap paramMap)throws NkiaException;
	
	/**
	 * 등록시 예외 데이터 처리
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void exceptionInsertHandler(HashMap paramMap)throws NkiaException;
	
	/**
	 * 구성id 생성
	 * @param paramMap
	 * @throws NkiaException
	 */
	public String createConfId(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산 id 생성
	 * @param paramMap
	 * @throws NkiaException
	 */
	public String createAssetId(HashMap paramMap)throws NkiaException;
	
	/**
	 * 분류id 로 분류에 대한 정보를 가져온다.
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void selectClassInfo(HashMap paramMap)throws NkiaException;
	
	/**
	 * hmsong cmdb 개선 작업
	 * (구성분류)설정에 따른 분류트리를 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchConfTypeTree(HashMap paramMap)throws NkiaException;
	
	/**
	 * form 데이터의 등록 및 수정
	 * @param attrList
	 * @param hiddenList
	 * @param entityMap
	 * @param currentData
	 * @throws NkiaException
	 */
	public void updateMnualCompData(List attrList,List hiddenList,HashMap entityMap,HashMap currentData)throws NkiaException;
	
	/**
	 * 컴포넌트 예외 핸들러 
	 * @param attrList
	 * @param entityMap
	 * @param dataArray
	 * @throws NkiaException
	 */
	public void exceptionCompHandler(List<HashMap> attrList,HashMap entityMap,JSONArray dataArray)throws NkiaException;
	
	/**
	 * form 별로 select 쿼리를 만들고 해당 데이터를 가져온다.
	 * @param attrList
	 * @param hiddenList
	 * @param entityMap
	 * @param dataArray
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectMnualCompData(List attrList,List hiddenList,HashMap entityMap,JSONArray dataArray)throws NkiaException;
	
	/**
	 * 변경 히스토리 이력 id를 생성한다.
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectAssetChgHisSeq(HashMap dataMap)throws NkiaException;
	
	/**
	 * form 데이터 변경여부를 이력 테이블에 update 한다.
	 * @param attrList
	 * @param dataMap
	 * @param currentData
	 * @throws NkiaException
	 */
	public void compareDetailData(List<HashMap> attrList,HashMap dataMap,HashMap currentData)throws NkiaException;
	
	
	/**
	 * 설치정보를 삭제하는 쿼리를 만들고 삭제한다.
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void deleteInstallInfo(HashMap dataMap)throws NkiaException;
	
	/**
	 * 설치정보를 등록하는 쿼리를 만들고 등록한다.
	 * @param dataMap
	 * @param installColList
	 * @throws NkiaException
	 */
	public void insertInstallInfo(HashMap dataMap,List<HashMap> installColList)throws NkiaException;
	
	/**
	 * 이력 테이블에 그룹과 관련된 모든 테이블의 과거 정보를 등록한다.
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void insertPrevDataHis(HashMap dataMap)throws NkiaException;
	
	
	/**
	 * 폼 관련 속성 이력정보를 가져온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsFormModList(HashMap paramMap)throws NkiaException; 
	

	/**
	 * 
	 * 가상서버(클라우드와 연계된 가상서버) 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsVmList(ModelMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * TODO 속성별 상세 이력 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectFormAttrModList(HashMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * TODO 컴포넌트별 상세 이력 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectCompDetailModList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * TODO 각 컴포넌트별 컬럼 정보를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public JSONArray selectCompDataColumn(HashMap paramMap)throws NkiaException;
	
	
	/**
	 * 분류체계별 속성관리에서 버튼 컴퍼넌트 목록을 가져온다.
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsBtnTree(HashMap paramMap) throws NkiaException;
	
	/**
	 * 모델속성을 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public  List searchModelTreeComp(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 업무분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public  List searchWorkTreeComp(ModelMap paramMap) throws NkiaException;
	
	/**
	 * HISOS분류를 트리로 불러오는 팝업 서비스
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public  List searchHISOSTreeComp(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 설치된 소프트웨어 목록 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsInsSwList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 설치 소프트웨어와 맵핑되는 소프트웨어 옵션 전체 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOptionMappingList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 설치 소프트웨어와 맵핑되는 선택되어있던 소프트웨어 옵션 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSelectedOptionList(HashMap paramMap) throws NkiaException;

	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 기존 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteInsSwOption(HashMap paramMap)throws NkiaException;
	
	/**
	 * 
	 * TODO
	 * 설치된 소프트웨어 옵션 목록 변경정보 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertInsSwOption(HashMap paramMap)throws NkiaException;

	/**
	 * 유지보수 정보 폼 데이터를 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAssetMaintInfo(HashMap paramMap) throws NkiaException;
	
	/**
	 * 모자산 선택팝업 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectMotherListCnt(HashMap paramMap) throws NkiaException;
	/**
	 * 모자산 선택팝업 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectMotherList(HashMap paramMap) throws NkiaException ;
	/**
	 * 모자산 정보 변경 업데이트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	
	public void updateMotherInfo(HashMap paramMap) throws NkiaException ;
	
	/**
	 * 
	 * 자산 도입년도 목록 
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public List selectAmIntroDtGroupBy(ModelMap paramMap)throws NkiaException ;
	
	
	/**
	 * 자품 변경 이력 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertChildChgHis(HashMap dataMap) throws NkiaException;
	
	/**
	 * 자품 변경 이력 리스트 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 자품 변경 이력 상세 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectChildChgHisDetail(HashMap paramMap) throws NkiaException;
	
	/**
	 * 논리서버 이력 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsLogicHisList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 클래스명
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchClassNm(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 구성용도명
	 * hmsong cmdb 개선 작업
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap searchConfTypeNm(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 프로세스 전체요청이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProcMasterHist(HashMap paramMap) throws NkiaException;
	
	/**
	 * 프로세스 장애이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProcHist(HashMap paramMap) throws NkiaException;

	/**
	 * 프로세스 변경이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchChangeProcHist(HashMap paramMap) throws NkiaException;
	
	/**
	 * 프로세스 문제이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchProblemProcHist(HashMap paramMap) throws NkiaException;

	/**
	 * 프로세스 서비스이력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchServiceProcHist(HashMap paramMap) throws NkiaException;

	/**
	 * DCA 연계 PC S/W 목록
	 * @param paramMap
	 * @return
	 */
	public List selectIfPcSwList(ModelMap paramMap);
	
	/**
	 * 하위 논리 리스트
	 * @param paramMap
	 * @throws NkiaException
	 */
	public List selectOpmsRelLogicList(HashMap paramMap) throws NkiaException;
	
	/**
	 * selectOpmsCompModList
	 * 컴포넌트 들의 수정이력을 가공한다.
	 */
	public List selectOpmsCompModList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 이력조회 - 장비별 요청현황조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	public List searchConfReqStateList(ModelMap paramMap) throws NkiaException;
	public int searchConfReqStateListCount(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 자산 메모리 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsInsMemoryView(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산 CPU Core 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsInsCpuView(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산 Storage 증설량 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsInsStorageView(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산  NW 서비스중단일관리 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchBsSvcStopView(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산 정보 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAssetInfo(HashMap paramMap) throws NkiaException;

	/**
	 * 디스크 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelDiskList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * NIC 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelNicList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 이중화 정보 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsDualInfoList(ModelMap paramMap) throws NkiaException;	
	
	/**
	 * 가상서버 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelVMList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 가상서버 목록 건수 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectOpmsRelVMCount(ModelMap paramMap) throws NkiaException;
	
	/**
	 * IP 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsRelIPList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * EMS수집 IP 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEmsIPList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * EMS수집 DISK 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectEmsDiskList(ModelMap paramMap)throws NkiaException;
	
	/**
	 * EMS 정보 업데이트
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateEmsInfo(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산정보에 등록된 EMS ID 건수 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectEmsIdCount(HashMap paramMap) throws NkiaException;
	
	/**
	 * 자산 중복 체크
	 * @param paramMap
	 * @throws NkiaException
	 */
	public boolean isDuplicateAsset(HashMap paramMap)throws NkiaException;
	
	/**
	 * 자산 중복 체크(일괄수정)
	 * @param paramMap
	 * @throws NkiaException
	 */
	public boolean isDuplicateBatchAsset(HashMap paramMap)throws NkiaException;
}