/*
 * 2019.10.22 LedServiceImpl.java
 * 
 * LED manager
 * PARK JUN HO
 */
package com.nkia.itg.itam.securityReview.impl;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.automation.dao.AssetAutomationDAO;
import com.nkia.itg.itam.securityReview.dao.SecurityReviewDAO;
import com.nkia.itg.itam.securityReview.service.SecurityReviewService;

@Service("securityReviewService")
public class SecurityReviewServiceImpl implements SecurityReviewService {

	private static final Logger logger = LoggerFactory.getLogger(SecurityReviewServiceImpl.class);

	@Resource(name = "securityReviewService")
	public SecurityReviewService securityReviewService;

	@Resource(name = "securityReviewDAO")
	private SecurityReviewDAO securityReviewDAO;

	@Override
	public List getProcessRelAsset(Map paramMap) throws NkiaException {
		return securityReviewDAO.getProcessRelAsset(paramMap);
	}

	public void insertProcessRelAsset(Map paramMap) throws NkiaException {
		securityReviewDAO.insertProcessRelAsset(paramMap);
	}
	
	@Override
	public int securityReviewListCount(ModelMap paramMap) throws NkiaException {
		return securityReviewDAO.securityReviewListCount(paramMap);
	}
	
	@Override
	public List securityReviewList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = securityReviewDAO.securityReviewList(paramMap); 
		return resultList;
	}
	
	@Override
	public List securityReviewList(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = securityReviewDAO.securityReviewList(paramMap); 
		return resultList;
	}
	
	@Override
	public void updateSecurityReview(ModelMap paramMap) throws NkiaException {	
		securityReviewDAO.updateSecurityReview(paramMap);

}

	@Override
	public int existAtchId(ModelMap paramMap) throws NkiaException {
		return securityReviewDAO.existAtchId(paramMap);
	}

	@Override
	public void initAtchId(ModelMap paramMap) throws NkiaException {
		securityReviewDAO.initAtchId(paramMap);
		
	}

	@Override
	public int getServerPopListCount(ModelMap paramMap) throws NkiaException {
		return securityReviewDAO.getServerPopListCount(paramMap);
	}

	@Override
	public List getServerPopList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = securityReviewDAO.getServerPopList(paramMap); 
		return resultList;
	}

	@Override
	public void insertDirectServer(Map paramMap) throws NkiaException {
		securityReviewDAO.insertDirectServer(paramMap);
		
	}

	@Override
	public int getChangeSecuListCount(ModelMap paramMap) throws NkiaException {
		return securityReviewDAO.getChangeSecuListCount(paramMap);
	}

	@Override
	public List getChangeSecuList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = securityReviewDAO.getChangeSecuList(paramMap); 
		return resultList;
	}

	@Override
	public int getChangeRelConfCount(ModelMap paramMap) throws NkiaException {
		return securityReviewDAO.getChangeRelConfCount(paramMap);
	}

	@Override
	public List getChangeRelConf(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = securityReviewDAO.getChangeRelConf(paramMap); 
		return resultList;
	}

	
	
	
}
