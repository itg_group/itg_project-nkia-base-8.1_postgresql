/*
 * 2019.10.22 LedService.java
 * 
 * LED manager
 * PARK JUN HO
 */
package com.nkia.itg.itam.securityReview.service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SecurityReviewService {

	public List getProcessRelAsset(Map paramMap) throws NkiaException ;		
	public void insertProcessRelAsset(Map paramMap) throws NkiaException ;
	
	public int securityReviewListCount(ModelMap paramMap) throws NkiaException ;
	public List securityReviewList(ModelMap paramMap) throws NkiaException ;
	public List securityReviewList(Map paramMap) throws NkiaException;
	
	
	public void updateSecurityReview(ModelMap paramMap) throws NkiaException;
	public int existAtchId(ModelMap paramMap) throws NkiaException ;
	void initAtchId(ModelMap paramMap) throws NkiaException;
	
	
	public int getServerPopListCount(ModelMap paramMap) throws NkiaException ;
	public List getServerPopList(ModelMap paramMap) throws NkiaException ;
	
	public void insertDirectServer(Map paramMap) throws NkiaException ;
	
	public int getChangeSecuListCount(ModelMap paramMap) throws NkiaException ;
	public List getChangeSecuList(ModelMap paramMap) throws NkiaException ;	
	public int getChangeRelConfCount(ModelMap paramMap) throws NkiaException ;
	public List getChangeRelConf(ModelMap paramMap) throws NkiaException ;	
	

}
