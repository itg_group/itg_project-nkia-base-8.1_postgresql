/*
 * 2019.10.22 LedController.java
 * 
 * LED manager
 * PARK JUN HO
 */
package com.nkia.itg.itam.securityReview.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.itam.automation.service.AssetAutomationService;
import com.nkia.itg.itam.newAsset.service.NewAssetManagerService;
import com.nkia.itg.itam.securityReview.service.SecurityReviewService;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;
import com.nkia.itg.itam.automation.dao.AssetAutomationDAO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class SecurityReviewController {

	@Resource(name = "securityReviewService")
	private SecurityReviewService securityReviewService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	

	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;
	
	@Resource(name = "assetAutomationDAO")
	private AssetAutomationDAO assetAutomationDAO;
	
	
	/**
	 *  폐기자산처리 목록 페이지 로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/securityReview/securityReview.do")
	public String securityReview(ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/itam/securityReview/securityReview.nvf";
		return forwarPage;
	}
	
	
	@RequestMapping(value="/itg/itam/securityReview/securityReviewList.do")
	public @ResponseBody ResultVO securityReviewList(@RequestBody ModelMap paramMap)throws NkiaException {
		
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		
		paramMap.put("login_user_id", login_user_id);

		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();

		try{
			
			//page=1, start=0, limit=10 있는 경우, 
			if (paramMap.get("page") != null) {
				int totalCount = 0;
				PagingUtil.getFristEndNum(paramMap);
				
				totalCount = securityReviewService.securityReviewListCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			}
			
			resultList = securityReviewService.securityReviewList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	
	@RequestMapping(value="/itg/itam/securityReview/updateSecurityReview.do")
	public @ResponseBody ResultVO updateSecurityReview(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		
	//	paramMap.put("disuse_state", "PROCESSING");
		try{						
			
	    	UserVO userVO = new UserVO();
	    	userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    	String login_user_id = userVO.getUser_id();
	    		
	    	paramMap.put("login_user_id", login_user_id);
	    	
	    	if("FINISH_ACTION".equals(paramMap.get("securev_status"))){
	    		paramMap.put("finishflag","1");	    		
	    	}
	    	
	    	
	    	int atch_cnt = securityReviewService.existAtchId(paramMap);
	    	
	    	if(atch_cnt == 0){
	    		securityReviewService.initAtchId(paramMap);
	    	}{	    	
	    		securityReviewService.updateSecurityReview(paramMap);	
	    	}
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		//"CD0000001105".equals(paramMap.get("REQ_DOC_ID"))
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/securityReview/initAtchId.do")
	public @ResponseBody ResultVO initAtchId(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		
	//	paramMap.put("disuse_state", "PROCESSING");
		try{									
	    	UserVO userVO = new UserVO();
	    	userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    	String login_user_id = userVO.getUser_id();	    		
	    	paramMap.put("login_user_id", login_user_id);	    	
	    		    	
	    	int atch_cnt = securityReviewService.existAtchId(paramMap);
	    	
	    	if(atch_cnt == 0){
	    		securityReviewService.initAtchId(paramMap);
	    	}
 			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		//"CD0000001105".equals(paramMap.get("REQ_DOC_ID"))
		return resultVO;
	}	
	
	@RequestMapping(value="/itg/itam/securityReview/securityReviewExcelDown.do")
	public @ResponseBody ResultVO securityReviewExcelDown(@RequestBody ModelMap paramMap) throws NkiaException{
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();

		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		ModelMap excelMap = new ModelMap();
		
		Map excelParam = (Map) paramMap.get("excelParam");
		Map temp = (Map) paramMap.get("param");
		
		try{

			
			resultList = securityReviewService.securityReviewList(temp);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);	
			
			resultVO.setResultMap(excelMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
		
	}
	
	
	@RequestMapping(value="/itg/itam/securityReview/getServerPopList.do")
	public @ResponseBody ResultVO getServerPopList(@RequestBody ModelMap paramMap)throws NkiaException {
		
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		
		paramMap.put("login_user_id", login_user_id);

		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();

		try{
			
			//page=1, start=0, limit=10 있는 경우, 
			if (paramMap.get("page") != null) {
				int totalCount = 0;
				PagingUtil.getFristEndNum(paramMap);
				
				totalCount = securityReviewService.getServerPopListCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			}
			
			resultList = securityReviewService.getServerPopList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/itam/securityReview/insertDirectServer.do")
	public @ResponseBody ResultVO insertDirectServer(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{									
	    	UserVO userVO = new UserVO();
	    	userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    	String login_user_id = userVO.getUser_id();	    		
	    	paramMap.put("login_user_id", login_user_id);
	    	
	    	
	    	int atch_cnt = securityReviewService.existAtchId(paramMap);
	    	
	    	if(atch_cnt == 0){
	    		securityReviewService.initAtchId(paramMap);
	    	}{	    	
	    		securityReviewService.insertDirectServer(paramMap);	
	    	}
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		//"CD0000001105".equals(paramMap.get("REQ_DOC_ID"))
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/securityReview/getChangeSecuList.do")
	public @ResponseBody ResultVO getChangeSecuList(@RequestBody ModelMap paramMap)throws NkiaException {
		
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		
		paramMap.put("login_user_id", login_user_id);

		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();

		try{
			
			//page=1, start=0, limit=10 있는 경우, 
			if (paramMap.get("page") != null) {
				int totalCount = 0;
				PagingUtil.getFristEndNum(paramMap);
				
				totalCount = securityReviewService.getChangeSecuListCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			}
			
			resultList = securityReviewService.getChangeSecuList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/securityReview/getChangeRelConf.do")
	public @ResponseBody ResultVO getChangeRelConf(@RequestBody ModelMap paramMap)throws NkiaException {
		
		String login_user_id = "";
		UserVO userVO = new UserVO();
		userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		login_user_id = userVO.getUser_id();
		
		paramMap.put("login_user_id", login_user_id);

		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();

		try{
			
			//page=1, start=0, limit=10 있는 경우, 
			if (paramMap.get("page") != null) {
				int totalCount = 0;
				PagingUtil.getFristEndNum(paramMap);
				
				totalCount = securityReviewService.getChangeRelConfCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition(StringUtil.parseInteger(paramMap.get(BaseEnum.PAGER_START_PARAM.getString())));
			}
			
			resultList = securityReviewService.getChangeRelConf(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
