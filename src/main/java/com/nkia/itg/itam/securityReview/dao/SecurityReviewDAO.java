/*
 * 2019.10.22 LedDAO.java
 * 
 * LED manager
 * PARK JUN HO
 */
package com.nkia.itg.itam.securityReview.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("securityReviewDAO")
public class SecurityReviewDAO  extends EgovComAbstractDAO{
	/////접수 시 POSTWORK .. 시작.
	public List getProcessRelAsset(Map paramMap) throws NkiaException {
		return list("SecurityReviewDAO.getProcessRelAsset", paramMap);
	}

	public void insertProcessRelAsset(Map paramMap) throws NkiaException {
		this.insert("SecurityReviewDAO.insertProcessRelAsset", paramMap);
	}	
	/////접수 시 POSTWORK .. 끝.
	
	public List selectEnginReqList(Map paramMap) throws NkiaException {
		return list("SecurityReviewDAO.selectEnginReqList", paramMap);
	}
	
	
	
	///////////관리페이지 그리드 조회 시작
	public int securityReviewListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.securityReviewListCount", paramMap);
	}

	public List securityReviewList(ModelMap paramMap) throws NkiaException {
		return list("SecurityReviewDAO.securityReviewList", paramMap);
	}
	
	public List securityReviewList(Map paramMap) throws NkiaException {
		return list("SecurityReviewDAO.securityReviewList", paramMap);
	}
	
	
	public void updateSecurityReview(ModelMap paramMap) throws NkiaException {
	this.update("SecurityReviewDAO.updateSecurityReview", paramMap);
	}

	public int existAtchId(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.existAtchId", paramMap);
	}
	
	public void initAtchId(ModelMap paramMap) throws NkiaException {
		this.update("SecurityReviewDAO.initAtchId", paramMap);
	}
	
	
	
	public int getServerPopListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.getServerPopListCount", paramMap);
	}

	public List getServerPopList(ModelMap paramMap) throws NkiaException {
		return list("SecurityReviewDAO.getServerPopList", paramMap);
	}
	
	public void insertDirectServer(Map paramMap) throws NkiaException {
		this.insert("SecurityReviewDAO.insertDirectServer", paramMap);
	}	
	
	
	public int getChangeSecuListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.getChangeSecuListCount", paramMap);
	}
	public List getChangeSecuList(ModelMap paramMap) throws NkiaException {
		return list("SecurityReviewDAO.getChangeSecuList", paramMap);
	}	
	
	
	public int getChangeRelConfCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.getChangeRelConfCount", paramMap);
	}
	public List getChangeRelConf(ModelMap paramMap) throws NkiaException {
		return list("SecurityReviewDAO.getChangeRelConf", paramMap);
	}	
	
	
	
	
	
	
	/*
	///////////관리페이지 그리드 조회 시작
	public int serviceCarryOutServiceCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.serviceCarryOutServiceCount", paramMap);
	}

	public List serviceCarryOutService(ModelMap paramMap) throws NkiaException {
		return list("SecurityReviewDAO.serviceCarryOutService", paramMap);
	}
	///////////관리페이지 그리드 조회 끝
	
	public void updateDiskFormatType(Map paramMap) throws NkiaException {
		this.update("SecurityReviewDAO.updateDiskFormatType", paramMap);
	}

	public void updateStateOutDate(Map paramMap) throws NkiaException {
		this.update("SecurityReviewDAO.updateStateOutDate", paramMap);
	}
	
	public void updateProcDisuseAssetGrid(Map paramMap) throws NkiaException {
		this.update("SecurityReviewDAO.updateProcDisuseAssetGrid", paramMap);
	}	
	
	public int selectCarryOutDiskCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.selectCarryOutDiskCount", paramMap);
	}

	public List selectCarryOutDisk(ModelMap paramMap) throws NkiaException {
		return list("SecurityReviewDAO.selectCarryOutDisk", paramMap);
	}
	public void insertCarryOutDisk(Map paramMap) throws NkiaException {
		this.insert("SecurityReviewDAO.insertCarryOutDisk", paramMap);
	}	
	public int selectExistSrId(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("SecurityReviewDAO.selectExistSrId", paramMap);		
	}		
	
	public List serviceCarryOutServiceListDis(Map paramMap) throws NkiaException {
		return list("SecurityReviewDAO.serviceCarryOutServiceListDis", paramMap);
	}	
	
	public List serviceCarryOutServiceList(Map paramMap) throws NkiaException {
		return list("SecurityReviewDAO.serviceCarryOutServiceList", paramMap);
	}	
	*/
}