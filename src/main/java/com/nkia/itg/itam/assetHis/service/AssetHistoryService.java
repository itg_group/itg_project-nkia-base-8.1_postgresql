/*
 * @(#)AssetHistoryService.java              2016. 03. 28.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.assetHis.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AssetHistoryService {
	
	
	/** 이력 등록 */
	public List selectNewLogicAttrList() throws NkiaException;
	
	/** 자산속성 이력 등록 (List형태)
	 * 속성 파라메터 : List
	 * @param confId	: 구성ID(CONF_ID)
	 * @param newDataList	: 신규 데이터 List
	 * @param chgMsg	: 변경 메세지
	 * @param updUSerId	: 수정자 ID
	 * newAttrList <{"COLUMN_ID": value, "CHG_VALUE" : value}>:
	 * KEY 가 되는 컬럼ID는 대문자로 담아준다.
	 */
	public void insertAssetAttrHistory(String conf_id, List<HashMap> attrList,String chgMsg, String user_id) throws NkiaException;
	
	
	/**
	 * ※ 해당 메소드는 conf_type(구성구분)개념 이후로 생긴 메소드입니다. 자산과 구성이 별개인 프로젝트에서 사용되는 메소드입니다.
	 * 자산, 구성속성 이력 저장 실행
	 * 현재 데이터와 이전 데이터를 비교하여 변경된 속성과 값만 추출하여 이력 등록한다.
	 * @param confId(구성ID),confType(구성용도),assetId(자산ID),classType(자산분류),entity_mng_type(자산구성속성구분),newAllDataList(분류체계별 속성에서 매핑한 목록),chgMsg(저장메시지),updUserId(저장한userId),mapTableKind(테이블이 자산속성인지, 구성속성인지 map형식으로 저장)
	 * @param newAllDataList
	 * @param chgMsg
	 * @param updUSerId
	 * newAttrList <{"COLUMN_ID": value, "CHG_VALUE" : value}>:
	 * KEY 가 되는 컬럼ID는 대문자로 담아준다.
	 */
	public void insertAssetConfAttrHistory(String confId, String confYype, String assetId, String classType, String entity_mng_type, List<HashMap> attrList,
			String chgMsg, String user_id, HashMap mapTableKind) throws NkiaException;
	
	/** 자산속성 이력 등록 (Map형태)*/
	public void insertAssetAttrHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException;

	/** 자산원장 운영자 이력 등록 */
	public void insertOperCompHistory(String conf_id, List<HashMap> currentData,String chgMsg, String user_id) throws NkiaException;

	/** 자산원장 연계서비스 이력 등록 */
	public void insertRelServiceCompHistory(String conf_id,List<HashMap> currentDataList, String chgMsg, String user_id) throws NkiaException;

	/** 자산원장 연계장비 이력 등록 */
	public void insertRelInfraCompHistory(String confId, String classType, HashMap tabDataMap, String chgMsg, String updUserId) throws NkiaException;
	
	/** 자산원장 설치정보 탭리스트 이력 등록 */
	public void insertInstallTabListCompHistory(String conf_id, HashMap tabDataMap, String chgMsg, String updUserId) throws NkiaException;

	/** DCA수집 설치 디스크 인터페이스 이력 등록 */
	public void insertDcaInsDiskHistory(String string) throws NkiaException;

	/** DCA수집 설치 네트워크 인터페이스 이력 등록 */
	public void insertDcaInsNifHistory(String string) throws NkiaException;	
	
	/** 과금 메모리 증설 이력 등록 */
	public void insertBsInsMemoryHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException;
	
	/** 과금 Core 증설 이력 등록 */
	public void insertBsInsCpuHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException;
	
	/** 과금 스토리지 증설 이력 등록 */
	public void insertBsInsStorageHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException;
	
	/** 서비스중단 이력 등록 */
	public void insertBsSvcStopHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException;
	
	/** 공통비청구처 관리 이력 등록 */
	public void insertBsCmprcCustRateHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException;
	
	/** 청구처별 사용량 이력 등록 */
	public void insertBsCustRateHistory(String confId, String confType, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException;

	/** 과금 서버 속성 이력 등록 */
	public void insertBsSvHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException;

	/** 과금 스토리지 속성 이력 등록 */
	public void insertBsStHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException;
	
	/** 과금 전용회선 속성 이력 등록 */
	public void insertBsLnHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException;
	
	/** 과금 네트워크  속성 이력 등록 */
	public void insertBsNwHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException;

	
	/*************** 이력조회 시작******************************/
	
	public List selectOpmsHisOperList(ModelMap paramMap) throws NkiaException;

	public List selectOpmsHisRelServiceList(ModelMap paramMap) throws NkiaException;

	public List selectOpmsHisRelInfraList(ModelMap paramMap) throws NkiaException;
	
	public List selectOpmsHisRelDRList(ModelMap paramMap) throws NkiaException;

	public List selectOpmsHisBsSvList(ModelMap paramMap);

	public List selectOpmsHisBsStList(ModelMap paramMap);

	public List selectOpmsHisBsLnList(ModelMap paramMap);
	
	public List selectOpmsHisBsNwList(ModelMap paramMap);

	public List selectOpmsHisBsInsMemoryList(ModelMap paramMap);
	
	public List selectOpmsHisBsInsCpuList(ModelMap paramMap);
	
	public List selectOpmsHisBsInsStorageList(ModelMap paramMap);
	
	public List selectOpmsHisBsSvcStopList(ModelMap paramMap);
	
	public List selectOpmsBsCustRateList(ModelMap paramMap);
	
	public List selectOpmsHisBsCustRateList(ModelMap paramMap);
	
	public List selectOpmsHisAttrList(HashMap paramMap) throws NkiaException;
	
	public List selectOpmsHisAttrDetailList(HashMap paramMap) throws NkiaException;

	public List selectOpmsHisInsDiskDetailList(HashMap paramMap) throws NkiaException;
	
	public List selectOpmsHisInsNifDetailList(HashMap paramMap) throws NkiaException;

	public List selectOpmsHisBsCmprcCustRateList(ModelMap paramMap) throws NkiaException;

	public void insertRelDiskCompHistory(String confId, List<HashMap> dataList, String chgMsg, String updUserId) throws NkiaException;

	public List selectOpmsHisRelDiskList(HashMap paramMap) throws NkiaException;
	
	public void insertRelNicCompHistory(String confId, List<HashMap> dataList, String chgMsg, String updUserId) throws NkiaException;
	
	public List selectOpmsHisRelNicList(HashMap paramMap) throws NkiaException;
	
	public void insertDualInfoCompHistory(String confId, List<HashMap> dataList, String chgMsg, String updUserId) throws NkiaException;
	
	public List selectOpmsHisDualInfoList(HashMap paramMap) throws NkiaException;
	
	public void insertRelIPCompHistory(String confId, List<HashMap> dataList, String chgMsg, String updUserId) throws NkiaException;

	public List selectOpmsHisRelIPList(HashMap paramMap) throws NkiaException;
}
