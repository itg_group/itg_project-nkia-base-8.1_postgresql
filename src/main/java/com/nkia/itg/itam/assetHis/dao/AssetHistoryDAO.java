/*
 * @(#)AssetHistoryDAO.java              2016. 03. 28.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.assetHis.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("assetHistoryDAO")
public class AssetHistoryDAO extends EgovComAbstractDAO{
	
	/**
	 * 이력정보 테이블의 ID를 생성한다. 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectAssetChgHisSeq(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("AssetHistoryDAO.selectAssetChgHisSeq",paramMap);
	}
	
	/**
	 * 자산 이력 시퀀스 테이블에 신규 시퀀스를 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisSeq(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisSeq", paramMap);
	}
	
	/**
	 * Select 쿼리 실행
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List excuteSelectQuery(String query)throws NkiaException{
		return list("AssetHistoryDAO.excuteSelectQuery", query);
	}
	
	/**
	 * Update 쿼리 실행
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void excuteUpdateQuery(String query)throws NkiaException{
		delete("AssetHistoryDAO.excuteUpdateQuery", query);
	}
	
	/**
	 * 해당 분류체계의 속성리스트를 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectClassAttributeList(Map paramMap) throws NkiaException {
		return list("AssetHistoryDAO.selectClassAttributeList", paramMap);
	}
	
	/**
	 * 자산 이력테이블에 데이터를 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHis(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHis", paramMap);
	}
	
	/**
	 * 자산 이력테이블에 변경여부를 Y 로 업데이트 / 현재값을 업데이트 한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateCurrentHisData(Map paramMap) throws NkiaException {
		this.update("AssetHistoryDAO.updateCurrentHisData", paramMap);
	}
	
	/**
	 * 변경여부가 Y로 업데이트 된 값과, 절대 변경되지 않는 컬럼값(ASSET_ID)를 제외하고 모든 속성 이력을 삭제한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteAssetAttrHistoryExtChgY(Map paramMap) throws NkiaException {
		this.delete("AssetHistoryDAO.deleteAssetAttrHistoryExtChgY", paramMap);
	}
	
	/**
	 * 논리서버의 신규 수집된 스펙(속성)을 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectNewLogicAttrList() throws NkiaException {
		return list("AssetHistoryDAO.selectNewLogicAttrList", null);
	}
	
	/**
	 * 설치정보 전용 이력정보 테이블의 ID를 생성한다. 
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectAssetChgHisInsSeq(HashMap paramMap)throws NkiaException{
		return (String)selectByPk("AssetHistoryDAO.selectAssetChgHisInsSeq",paramMap);
	}
	
	/**
	 * 설치정보 전용 이력 시퀀스 테이블에 신규 시퀀스를 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisInsSeq(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisInsSeq", paramMap);
	}
	
	
	/**
	 * 자산원장의 설치 디스크 수정이력 변경 횟수를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsHisInsDiskCountList(HashMap paramMap) throws NkiaException {
		return list("AssetHistoryDAO.selectOpmsHisInsDiskCountList", paramMap);
	}
	
	/**
	 * 수집된 설치디스크 정보와 현재 설치디스크정보를 차집합하여 신규추가된 디스크정보를 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectGatherAddInsDiskList() throws NkiaException {
		return list("AssetHistoryDAO.selectGatherAddInsDiskList", null);
	}
	
	/**
	 * 수집된 설치디스크 정보와 현재 설치디스크정보 중 값이 서로 일치하지 않는 디스크정보를 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectNotEqualInsDiskList() throws NkiaException {
		return list("AssetHistoryDAO.selectNotEqualInsDiskList", null);
	}
	
	/**
	 * 수집된 설치디스크 정보와 현재 설치디스크정보를 차집합하여 삭제된 디스크정보를 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectGatherRemoveInsDiskList() throws NkiaException {
		return list("AssetHistoryDAO.selectGatherRemoveInsDiskList", null);
	}
	
	/**
	 * 수집결과 추가된 디스크 정보를 이력테이블에 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisAddInsDisk(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisAddInsDisk", paramMap);
	}
	
	/**
	 * 수집결과 삭제된 디스크 정보를 이력테이블에 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisRemoveInsDisk(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisRemoveInsDisk", paramMap);
	}
	
	/**
	 * 수집결과 값이 변경된 디스크 정보를 이력테이블에 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisUpdateInsDisk(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisUpdateInsDisk", paramMap);
	}
	
	/**
	 * 수집된 설치NIF 정보와 현재 설치NIF정보를 차집합하여 신규추가된 NIF정보를 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectGatherAddInsNifList() throws NkiaException {
		return list("AssetHistoryDAO.selectGatherAddInsNifList", null);
	}
	
	/**
	 * 수집된 설치NIF 정보와 현재 설치NIF정보 중 값이 서로 일치하지 않는NIF정보를 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectNotEqualInsNifList() throws NkiaException {
		return list("AssetHistoryDAO.selectNotEqualInsNifList", null);
	}
	
	/**
	 * 수집된 설치NIF 정보와 현재 설치NIF정보를 차집합하여 삭제된 NIF정보를 불러온다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectGatherRemoveInsNifList() throws NkiaException {
		return list("AssetHistoryDAO.selectGatherRemoveInsNifList", null);
	}
	
	/**
	 * 수집결과 추가된 NIF정보를 이력테이블에 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisAddInsNif(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisAddInsNif", paramMap);
	}
	
	/**
	 * 수집결과 삭제된 NIF정보를 이력테이블에 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisRemoveInsNif(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisRemoveInsNif", paramMap);
	}
	
	/**
	 * 수집결과 값이 변경된 NIF정보를 이력테이블에 insert한다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertAssetChgHisUpdateInsNif(Map paramMap) throws NkiaException {
		this.insert("AssetHistoryDAO.insertAssetChgHisUpdateInsNif", paramMap);
	}

	/**
	 * 자산원장의 네트워크인터페이스 수정이력 변경 횟수를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsHisInsNifCountList(HashMap paramMap) throws NkiaException {
		return list("AssetHistoryDAO.selectOpmsHisInsNifCountList", paramMap);
	}
	
	
	/**
	 * 수정이력 테이블 정보 삭제
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteAmAssetChgHis(HashMap paramMap)throws NkiaException{
		delete("AssetHistoryDAO.deleteAmAssetChgHis", paramMap);
	}
	
	/**
	 * 수정이력 시퀀스 테이블 정보 삭제
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteAmAssetChgHisSeq(HashMap paramMap)throws NkiaException{
		delete("AssetHistoryDAO.deleteAmAssetChgHisSeq", paramMap);
	}
	
	/**  =========================================================================================================*/
	
	/**
	 * 자산 원장의 현재 속성값 조회
	 * @param paramMap
	 * @return
	 */
	public List selectPreValueList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectPreValueList", paramMap);
	}

	/**
	 * 자산 속성데이터를 이력테이블에 등록
	 * @param paramMap
	 */
	public void insertAssetAttrHistory(HashMap paramMap) {
		insert("AssetHistoryDAO.insertAssetAttrHistory", paramMap);
	}

	public List<String> selectAssetAttrList(String confId) {
		return list("AssetHistoryDAO.selectAssetAttrList", confId);
	}

	/**
	 * 현재 운영자데이터 목록 조회
	 * @param confId
	 * @return
	 */
	public List selectCurrentOperDataList(String confId) {
		return list("AssetHistoryDAO.selectCurrentOperDataList", confId);
	}

	/**
	 * 운영자컴포넌트 히스토리 시퀀스 생성
	 * @return
	 */
	public String selectCompHisSeq() {
		return (String) selectByPk("AssetHistoryDAO.selectCompHisSeq", null);
	}

	/**
	 * 운영자 컴포넌트 이력데이터 등록
	 * @param chgHisDataMap
	 */
	public void insertOperCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertOperCompHistory", chgHisDataMap);
	}

	/**
	 * 현재 연계서비스 목록 조회
	 * @param confId
	 * @return
	 */
	public List selectCurrentRelServiceDataList(String confId) {
		return list("AssetHistoryDAO.selectCurrentRelServiceDataList", confId);
	}

	/**
	 * 연계서비스 컴포넌트 이력데이터 등록
	 * @param chgHisDataMap
	 */
	public void insertRelServiceCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertRelServiceCompHistory", chgHisDataMap);
	}

	/**
	 * 연계장비 컴포넌트 이력데이터 등록
	 * @param confId
	 * @return
	 */
	public List selectCurrentRelInfraDataList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentRelInfraDataList", paramMap);
	}
	
	/**
	 * 연계장비(DR) 컴포넌트 이력데이터 등록
	 * @param confId
	 * @return
	 */
	public List selectCurrentRelDRDataList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentRelDRDataList", paramMap);
	}

	/**
	 * 연계장비 컴포넌트 이력데이터 등록
	 * @param chgHisDataMap
	 */
	public void insertRelInfraCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertRelInfraCompHistory", chgHisDataMap);
	}

	public void insertAddRelInfraSyncHistory(HashMap addDataMap) {
		insert("AssetHistoryDAO.insertAddRelInfraSyncHistory", addDataMap);
	}

	public void insertRemoveRelInfraSyncHistory(HashMap removeDataMap) {
		insert("AssetHistoryDAO.insertRemoveRelInfraSyncHistory", removeDataMap);
	}

	public List<HashMap> selectRelInfraList(HashMap removeDataMap) {
		return list("AssetHistoryDAO.selectRelInfraList", removeDataMap);
	}

	public void insertRelInfraNullDataHistory(HashMap removeDataMap) {
		insert("AssetHistoryDAO.insertRelInfraNullDataHistory", removeDataMap);
	}
	
	/**
	 * 연계장비(DR) 컴포넌트 이력데이터 등록
	 * @param chgHisDataMap
	 */
	public void insertRelDRCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertRelDRCompHistory", chgHisDataMap);
	}

	public void insertAddRelDRSyncHistory(HashMap addDataMap) {
		insert("AssetHistoryDAO.insertAddRelDRSyncHistory", addDataMap);
	}

	public void insertRemoveRelDRSyncHistory(HashMap removeDataMap) {
		insert("AssetHistoryDAO.insertRemoveRelDRSyncHistory", removeDataMap);
	}

	public List<HashMap> selectRelDRList(HashMap removeDataMap) {
		return list("AssetHistoryDAO.selectRelDRList", removeDataMap);
	}

	public void insertRelDRNullDataHistory(HashMap removeDataMap) {
		insert("AssetHistoryDAO.insertRelDRNullDataHistory", removeDataMap);
	}

	public List<HashMap> selectCurrentInsDiskDataList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentInsDiskDataList", paramMap);
	}

	public void insertInsDiskCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertInsDiskCompHistory", chgHisDataMap);
	}

	public List<HashMap> selectCurrentInsNifDataList(HashMap paramMap) {
		 	return list("AssetHistoryDAO.selectCurrentInsNifDataList", paramMap);
	}

	public void insertInsNifCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertInsNifCompHistory", chgHisDataMap);
	}

	public List<HashMap> selectUpdateDiskConfList() {
		return list("AssetHistoryDAO.selectUpdateDiskConfList", null);
	}

	public List selectInsDiskList(String confId) {
		return list("AssetHistoryDAO.selectInsDiskList", confId);
	}

	public List<HashMap> selectUpdateNifConfList() {
		return list("AssetHistoryDAO.selectUpdateNifConfList", null);
	}

	public List selectInsNifList(String confId) {
		return list("AssetHistoryDAO.selectInsNifList" ,confId);
	}

	public List<HashMap> selectCurrentBsInsMemoryList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentBsInsMemoryList", paramMap);
	}

	public void insertBsInsMemoryHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertBsInsMemoryHistory", chgHisDataMap);
	}

	public String selectBsHistIndx() {
		return (String) selectByPk("AssetHistoryDAO.selectBsHistIndx", null);
	}

	public void insertBsSvHistory(HashMap newDataMap) {
		insert("AssetHistoryDAO.insertBsSvHistory", newDataMap);
	}

	public List<HashMap> selectCurrentBsCustRate(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentBsCustRate", paramMap);
	}

	public void insertBsCustRateHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertBsCustRateHistory", chgHisDataMap);
	}

	public List<HashMap> selectCurrentBsInsCpuList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentBsInsCpuList", paramMap);
	}

	public void insertBsInsCoreHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertBsInsCoreHistory", chgHisDataMap);
	}

	public List<HashMap> selectCurrentBsInsStorageList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentBsInsStorageList", paramMap);
	}

	public void insertBsInsStorageHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertBsInsStorageHistory", chgHisDataMap);
	}

	public List<HashMap> selectCurrentBsSvcStopList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentBsSvcStopList", paramMap);
	}

	public void insertBsSvcStopHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertBsSvcStopHistory", chgHisDataMap);
	}

	public void insertBsStHistory(HashMap newDataMap) {
		insert("AssetHistoryDAO.insertBsStHistory", newDataMap);
	}

	public HashMap selectCurrentBsStData(String confId) {
		return (HashMap) selectByPk("AssetHistoryDAO.selectCurrentBsStData", confId);
	}

	public HashMap selectCurrentBsLnData(String confId) {
		return (HashMap) selectByPk("AssetHistoryDAO.selectCurrentBsLnData", confId);
	}

	public void insertBsLnHistory(HashMap newDataMap) {
		insert("AssetHistoryDAO.insertBsLnHistory", newDataMap);
	}

	public HashMap selectCurrentBsNwData(String confId) {
		return (HashMap) selectByPk("AssetHistoryDAO.selectCurrentBsNwData", confId);
	}

	public void insertBsNwHistory(HashMap newDataMap) {
		insert("AssetHistoryDAO.insertNwLnHistory", newDataMap);
	}

	public HashMap selectCurrentBsSvData(String confId) {
		return (HashMap) selectByPk("AssetHistoryDAO.selectCurrentBsSvData", confId);
	}

	
	/**
	 * 자산원장의 수정이력 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsHisAttrList(Map paramMap) throws NkiaException {
		return list("AssetHistoryDAO.selectOpmsHisAttrList", paramMap);
	}
	
	/**
	 * 자산원장의 수정이력 상세정보 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsHisAttrDetailList(Map paramMap) throws NkiaException {
		return list("AssetHistoryDAO.selectOpmsHisAttrDetailList", paramMap);
	}
	
	/**
	 * 자산원장의 설치 디스크 수정이력 상세 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsHisInsDiskDetailList(HashMap paramMap) throws NkiaException {
		return list("AssetHistoryDAO.selectOpmsHisInsDiskDetailList", paramMap);
	}
	
	
	/**
	 * 자산원장의 네트워크인터페이스 수정이력 상세 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectOpmsHisInsNifDetailList(HashMap paramMap) throws NkiaException {
		return list("AssetHistoryDAO.selectOpmsHisInsNifDetailList", paramMap);
	}
	
	/**
	 * 자산원장의 운영자 수정이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisOperList(ModelMap paramMap) throws NkiaException{
		return list("AssetHistoryDAO.selectOpmsHisOperList", paramMap);
	}

	/**
	 * 자산원장의 연계서비스 수정이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisRelServiceList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisRelServiceList", paramMap);
	}

	/**
	 * 자산원장의 연계장비 수정이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisRelInfraList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisRelInfraList", paramMap);
	}
	
	/**
	 * 자산원장의 연계장비(DR) 수정이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisRelDRList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisRelDRList", paramMap);
	}

	/**
	 * 서버과금속성 이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisBsSvList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsSvList", paramMap);
	}

	/**
	 * 스토리지과금속성 이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisBsStList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsStList", paramMap);
	}

	/**
	 * 전용회선과금속성 이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisBsLnList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsLnList", paramMap);
	}

	/**
	 * 네트워크과금속성 이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisBsNwList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsNwList", paramMap);
	}

	/**
	 * 메모리 증설 이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisBsInsMemoryList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsInsMemoryList", paramMap);
	}

	/**
	 * 스토리지 증설 이력 리스트 조회
	 * @param paramMap
	 * @return
	 */
	public List selectOpmsHisBsInsCpuList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsInsCpuList", paramMap);
	}

	/**
	 * 스토리지 증설 이력 리스트 조회
	 */
	public List selectOpmsHisBsInsStorageList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsInsStorageList", paramMap);
	}
	
	/**
	 * 서비스중단 이력 리스트 조회
	 */
	public List selectOpmsHisBsSvcStopList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsSvcStopList", paramMap);
	}
	
	/**
	 * 청구처별 사용량 리스트 조회
	 */
	public List selectOpmsBsCustRateList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsBsCustRateList", paramMap);
	}
	
	/**
	 * 청구처별 사용량 이력 리스트 조회
	 */
	public List selectOpmsHisBsCustRateList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsCustRateList", paramMap);
	}

	public List<String> selectEntityAttrList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectEntityAttrList", paramMap);
	}

	/**
	 * 공통비 청구처 현재데이터 조회
	 * @param paramMap
	 * @return
	 */
	public List<HashMap> selectBsCmprcCustRateList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectBsCmprcCustRateList", paramMap);
	}

	public void insertBsCmprcCustRateHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertBsCmprcCustRateHistory", chgHisDataMap);
	}

	public List selectOpmsHisBsCmprcCustRateList(ModelMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisBsCmprcCustRateList", paramMap);
	}

	public List<HashMap> selectCurrentRelDiskList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentRelDiskList", paramMap);
	}

	public void insertRelDiskCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertRelDiskCompHistory", chgHisDataMap);
	}

	public HashMap selectPreValueMap(HashMap paramMap) {
		return (HashMap) selectByPk("AssetHistoryDAO.selectPreValueMap", paramMap);
	}

	public List selectOpmsHisRelDiskList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisRelDiskList", paramMap);
	}

	public List<HashMap> selectCurrentRelNicList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentRelNicList", paramMap);
	}
	
	public void insertRelNicCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertRelNicCompHistory", chgHisDataMap);
	}	
	
	public List selectOpmsHisRelNicList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisRelNicList", paramMap);
	}

	public List<HashMap> selectCurrentDualInfoList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentDualInfoList", paramMap);
	}
	
	public void insertDualInfoCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertDualInfoCompHistory", chgHisDataMap);
	}	
	
	public List selectOpmsHisDualInfoList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisDualInfoList", paramMap);
	}
	
	public List<HashMap> selectCurrentRelIPList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectCurrentRelIPList", paramMap);
	}
	
	public List selectOpmsHisRelIPList(HashMap paramMap) {
		return list("AssetHistoryDAO.selectOpmsHisRelIPList", paramMap);
	}
	
	public void insertRelIPCompHistory(HashMap chgHisDataMap) {
		insert("AssetHistoryDAO.insertRelIPCompHistory", chgHisDataMap);
	}
}
