/*
 * @(#)AssetHistoryServiceImpl.java              2017. 03. 28.
 *
 * Copyright 2017 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.assetHis.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.itam.assetHis.dao.AssetHistoryDAO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;

import egovframework.com.cmm.EgovMessageSource;

@Service("assetHistoryService")
public class AssetHistoryServiceImpl implements AssetHistoryService {
	
	private static final Logger logger = LoggerFactory.getLogger(AssetHistoryServiceImpl.class);
	
	/*******   각 컬럼의 데이터변경여부 비교를 위해 비교되는 속성의 컬럼을 정의한다.   */
	
	// INS_DISK 의 컬럼정보를 나열한다. [0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] AM_INS_DISK_COLS = {
		"DISK_NM",
		"DISK_VENDOR",
		"DISK_CAPA"
	};
	
	//INS_NIF 의 컬럼정보를 나열한다. [0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] AM_INS_NIF_COLS = {
		"NIF_NM",
		"MAC",
		"IP",
		"NETMASK",
		"BANDWIDTH",
		"MTU",
		"MANAGED",
		"IPMP_GROUP_NM",
		"VIP_YN",
		"DUPLEX"
	};
	
	// 과금메모리증설(BS_INS_MEMORY) 의 컬럼정보를 나열한다.[0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] BS_INS_MEMORY_COLS = {
		"BS_INDX",
		"COST_CLS",
		"BS_BGN_DT",
		"BS_END_DT",
		"UNIT_INSTALL_DT",
		"MEM_SIZE"
	};
	
	// 과금메모리증설(BS_INS_CPU) 의 컬럼정보를 나열한다.[0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] BS_INS_CPU_COLS = {
		"BS_INDX",
		"COST_CLS",
		"BS_BGN_DT",
		"BS_END_DT",
		"UNIT_INSTALL_DT",
		"CORE_CNT"
	};
	
	// 과금메모리증설(BS_INS_STORAGE) 의 컬럼정보를 나열한다.[0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] BS_INS_STORAGE_COLS = {
		"BS_INDX",
		"REL_CONF_ID",
		"COST_CLS",
		"BS_BGN_DT",
		"BS_END_DT",
		"UNIT_INSTALL_DT",
		"DISK_SIZE",
		"RAID_SE"
	};
	
	// 서비스중단일 (BS_SVC_STOP) 의 컬럼정보를 나열한다.[0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] BS_SVC_STOP_COLS = {
		"BS_INDX",
		"STOP_BGN_DT",
		"STOP_END_DT"
	};
	
	// 공통비 청구처 (BS_CMPRC_CUST_RATE) 의 컬럼정보를 나열한다.[0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] BS_CMPRC_CUST_RATE_COLS = {
		"BS_INDX",
		"BS_BGN_DT",
		"BS_END_DT",
		"CMPRC_SE"
	};
	
	// 과금 청구처별 사용량 (BS_UST_RATE) 의 컬럼정보를 나열한다.[0] 번째는 키가 되는 컬럼을 지정한다.
	private static final String[] BS_CUST_RATE_COLS = {
		"BS_INDX",
		"CUST_ID",
		"BS_BGN_DT",
		"BS_END_DT",
		"RATE",
		"SERVICE_ID"
	};
	
	// 스토리지과금속성(BS_ST) 컬럼정보 나열한다.
	private static final String[] BS_ST_COLS = {
		"BS_BGN_DT",
		"BS_END_DT",
		"BILL_YN",
		"ST_COST_CLS",
		"RAID_SE",
		"DISK_SE",
		"ST_LV",
		"ST_SE"
	};
	
	//전용회선과금 속성(BS_LN)
	private static final String[] BS_LN_COLS = {
		"BS_BGN_DT",
		"BS_END_DT",
		"BILL_YN",
		"LN_PRC",
		"MA_PRC",
		"RET_YN"
	};
	
	//네트워크과금 속성(BS_NW)
	private static final String[] BS_NW_COLS = {
		"BS_BGN_DT",
		"BS_END_DT",
		"BILL_YN",
		"NW_COST_CLS",
		"BS_MODEL_NM",
		"RET_YN"
	};
	
	//서버과금 속성(BS_NW)
	private static final String[] BS_SV_COLS = {
		"BS_BGN_DT",
		"BS_END_DT",
		"SV_COST_CLS",
		"BILL_YN",
		"SV_SE",
		"SV_LV",
		"SV_USE"
	};
	
	private static final String[] REL_DISK = {
		"ST_TYPE",
		"INTER_DISK_SIZE",
		"INTER_DISK_CNT",
		"RAID_TYPE",
		"ST_ASSIGN_CNT"
	};
	
	private static final String[] REL_IP = {
		"USAGE",
		"IP"
	};
		
	private static final String[] REL_NIC = {
		"MAIN_YN",
		"IP",
		"NIC_SPEED",
		"DUAL_YN",
		"NIC_USE",
		"ST_CONF_ID"
	};

	private static final String[] DUAL_INFO = {
		"DUAL_CONF_TYPE",
		"DUAL_CONF_ID",
		"DUAL_TYPE",
		"DUAL_METHOD"
	};	
	
	@Resource(name = "assetHistoryDAO")
	public AssetHistoryDAO assetHistoryDAO;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	
	/** ==================================================================================================================== */
	
/**
	 * 논리서버의 신규 수집된 스펙(속성)을 불러온다.
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@Override
	public List selectNewLogicAttrList() throws NkiaException {
		return assetHistoryDAO.selectNewLogicAttrList();
	}
	
	/**
	 * 자산속성 이력 저장
	 * 속성 파라메터 : List
	 * @param confId
	 * @param newDataList
	 * @param chgMsg
	 * @param updUSerId
	 * newAttrList <{"COLUMN_ID": value, "CHG_VALUE" : value}>:
	 * KEY 가 되는 컬럼ID는 대문자로 담아준다.
	 */
	public void insertAssetAttrHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException{
		
		insertAssetAttrHistoryProc(confId, newDataList, chgMsg, updUserId);
	}

	/**
	 * 자산속성 이력 저장
	 * 속성 파라메터 : Map
	 * @param confId
	 * @param newDataMap
	 * @param chgMsg
	 * @param updUSerId
	 */
	@Override
	public void insertAssetAttrHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException{
		
		List newDataList = new ArrayList();
		
		Iterator iter = (Iterator) newDataMap.keySet().iterator();
		while(iter.hasNext()){
			String key = (String) iter.next();
			HashMap tempMap = new HashMap();
			tempMap.put("COLUMN_ID", key);
			tempMap.put("CHG_VALUE", newDataMap.get(key));
			newDataList.add(tempMap);
		}
		
		insertAssetAttrHistoryProc(confId, newDataList, chgMsg, updUserId);
		
	}
	/**
	 * 자산속성 이력 저장 실행
	 * 현재 데이터와 이전 데이터를 비교하여 변경된 속성과 값만 추출하여 이력 등록한다.
	 * @param confId
	 * @param newAllDataList
	 * @param chgMsg
	 * @param updUSerId
	 * newAttrList <{"COLUMN_ID": value, "CHG_VALUE" : value}>:
	 * KEY 가 되는 컬럼ID는 대문자로 담아준다.
	 */
	private void insertAssetAttrHistoryProc(String confId, List<HashMap> newAllDataList, String chgMsg, String updUserId) throws NkiaException{
		if (newAllDataList != null) {
			//최종 변경속성 목록을 담아줄 List 생성
			List<HashMap> newDataList = new ArrayList();	// 넘어온 데이터 중 원장의 수정가능속성 데이터 목록 
			List<HashMap> chgDataList = new ArrayList();	// 수정이 일어난 데이터 목록

			// 원장에 있는 컬럼 중 수정 가능 속성 목록 조회
			HashMap confMap = new HashMap();
			confMap.put("confId", confId);
			List<String> entityAttrList = assetHistoryDAO.selectEntityAttrList(confMap);
			
			String classType = confId.substring(0, 2);
			
			if(entityAttrList != null && !entityAttrList.isEmpty()){
				
				// 넘어온 데이터 중 원장의 수정 가능 속성만 추려서 별로 List(newDataList) 에 담아준다.
				for(HashMap attrMap : newAllDataList){
					String key = (String) attrMap.get("COLUMN_ID");
					if(entityAttrList.contains(key)){
						newDataList.add(attrMap);
					}
				}
				
				if(newDataList != null && !newDataList.isEmpty()) {
					
					// 현재 데이터를 이력테이블에서 조회한다.
					HashMap paramMap = new HashMap();
					paramMap.put("CONF_ID", confId);
					paramMap.put("chgAttrList", newDataList);
					paramMap.put("classType", classType);
					
					HashMap preDataMap = assetHistoryDAO.selectPreValueMap(paramMap);				// 이전값을 view테이블에서 현재 데이터로 조회 - 20181124 좌은진
					
					// 이력테이블에 이력데이터가 없을경우 넘어온 모든 속성값을 이력테이블에 저장하기 위해 최종 변경속성List에 담아준다.
					if(preDataMap == null || preDataMap.isEmpty()){
						chgDataList = newDataList;
					}else{
						/** 변경여부 비교 */
						//이전 데이터와 비교하여 변경된 데이터만 chgDataList 에 담아준다. 
						for(HashMap attrMap : newDataList){
							String key = (String) attrMap.get("COLUMN_ID");
							String newValue = StringUtil.parseString(attrMap.get("CHG_VALUE"));
							String preValue =  "";
							if(preDataMap.containsKey(key)){
								preValue = StringUtil.parseString(preDataMap.get(key));
							}
							if(!newValue.equals(preValue)){
								attrMap.put("PRE_VALUE", preValue);
								chgDataList.add(attrMap);
							}
						}
					}
					// 변경속성값을 이력테이블에 등록
					if(chgDataList != null && !chgDataList.isEmpty()){
						for(HashMap chgHisDataMap : chgDataList){
							chgHisDataMap.put("CONF_ID", confId);
							chgHisDataMap.put("CHG_MESSAGE", chgMsg);
							chgHisDataMap.put("INS_USER_ID", updUserId);
							assetHistoryDAO.insertAssetAttrHistory(chgHisDataMap);
						}
					}
				}
			}
		}
	}
	
	
	/**
	 * 운영자컴포넌트 수정이력 저장
	 * @param confId
	 * @param newDataList<HashMap>
	 * @param chgMsg
	 * @param updUSerId
	 * newDataList<HashMap> > HashMap 필수값 :  {USER_ID: value, OPER_USER_TYPE: value}
	 * 
	 */
	public void insertOperCompHistory(String confId, List<HashMap> newDataList,String chgMsg, String updUserId) throws NkiaException {

		// 변경 데이터 목록
		List resultNewDataList = new ArrayList();
		// 데이터 변경여부
		boolean isOperChg = false;
		
		// 변경 데이터 목록이 null 일 경우 이력에 '-' 을 기본데이터로 이력을 등록한다. (삭제나 데이터 없음을 의미하기 위해)
		if(newDataList == null || newDataList.isEmpty()){
			HashMap nullDataMap = new HashMap();
			nullDataMap.put("USER_ID", "-");
			nullDataMap.put("OPER_USER_TYPE", "-");
			resultNewDataList.add(nullDataMap);
		}else{
			resultNewDataList = newDataList;
		}
		
		// 현재 데이터 목록을 조회한다.
		List preDataList = assetHistoryDAO.selectCurrentOperDataList(confId);
		
		if(preDataList == null || preDataList.isEmpty()){
			// 현재 데이터가 없으면 변경된것으로 간주한다.
			isOperChg = true;
		}else{
			// 데이터 변경여부 비교
			if(preDataList.size() == resultNewDataList.size()){
				for(int i = 0; i < resultNewDataList.size(); i++){
					HashMap newDataMap = (HashMap) resultNewDataList.get(i);
					String newOperUserId = null;
					if(newDataMap.containsKey("USER_ID")){
						newOperUserId = (String) newDataMap.get("USER_ID");
					}else if(newDataMap.containsKey("OPER_USER_ID")){
						newOperUserId = (String) newDataMap.get("OPER_USER_ID");
					}
					
					String newOperUserType = (String) newDataMap.get("OPER_USER_TYPE");
					int notChgCnt = 0;
					
					for(int k = 0; k < preDataList.size(); k++) {
						HashMap preDataMap = (HashMap) preDataList.get(k);
						String preOperUserId = (String) preDataMap.get("OPER_USER_ID");
						String preOperUserType = (String) preDataMap.get("OPER_USER_TYPE");
						
						if(newOperUserId.equalsIgnoreCase(preOperUserId) && newOperUserType.equalsIgnoreCase(preOperUserType)){
							notChgCnt++;
							break;
						}
					}
					if(notChgCnt == 0){
						isOperChg = true;
						break;
					}
				}
			}else{
				// 이전 목록 사이즈와 변경 목록 사이즈가 다르면 변경된 것으로 간주한다.
				isOperChg = true;
			}
		}
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isOperChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			for(int i = 0; i < resultNewDataList.size(); i++){
				HashMap chgHisDataMap = (HashMap) resultNewDataList.get(i);
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("SEQ", chgSeq);
				chgHisDataMap.put("CHG_MESSAGE", chgMsg);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertOperCompHistory(chgHisDataMap);
			}
		}
		
	}

	/**
	 * 연계서비스 컴포넌트 수정이력 저장
	 * @param confId
	 * @param newDataList<HashMap>
	 * @param chgMsg
	 * @param updUSerId
	 * newDataList<HashMap> > HashMap 필수값 :  {SERVICE_ID: value}
	 * 
	 */
	@Override
	public void insertRelServiceCompHistory(String confId,List<HashMap> newDataList, String chgMsg, String updUserId)throws NkiaException {
		// 변경 데이터 목록
		List resultNewDataList = new ArrayList();
		// 데이터 변경여부
		boolean isDataChg = false;
		
		// 변경 데이터 목록이 null 일 경우 이력에 '-' 을 기본데이터로 이력을 등록한다. (삭제나 데이터 없음을 의미하기 위해)
		if(newDataList == null || newDataList.isEmpty()){
			HashMap nullDataMap = new HashMap();
			nullDataMap.put("SERVICE_ID", "-");
			resultNewDataList.add(nullDataMap);
		}else{
			resultNewDataList = newDataList;
		}
		
		// 현재 데이터 목록을 조회한다.
		List preDataList = assetHistoryDAO.selectCurrentRelServiceDataList(confId);
		
		if(preDataList == null || preDataList.isEmpty()){
			// 현재 데이터가 없으면 변경된것으로 간주한다.
			isDataChg = true;
		}else{
			// 데이터 변경여부 비교
			if(preDataList.size() == resultNewDataList.size()){
				for(int i = 0; i < resultNewDataList.size(); i++){
					HashMap newDataMap = (HashMap) resultNewDataList.get(i);
					String newServiceId = (String) newDataMap.get("SERVICE_ID");
					int notChgCnt = 0;
					
					for(int k = 0; k < preDataList.size(); k++) {
						HashMap preDataMap = (HashMap) preDataList.get(k);
						String preServiceId = (String) preDataMap.get("SERVICE_ID");
						
						if(newServiceId.equalsIgnoreCase(preServiceId)){
							notChgCnt++;
							break;
						}
					}
					if(notChgCnt == 0){
						isDataChg = true;
						break;
					}
				}
			}else{
				// 이전 목록 사이즈와 변경 목록 사이즈가 다르면 변경된 것으로 간주한다.
				isDataChg = true;
			}
		}
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			for(int i = 0; i < resultNewDataList.size(); i++){
				HashMap chgHisDataMap = (HashMap) resultNewDataList.get(i);
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("SEQ", chgSeq);
				chgHisDataMap.put("CHG_MESSAGE", chgMsg);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertRelServiceCompHistory(chgHisDataMap);
			}
		}
	}

	
	/**
	 * 연계장비 컴포넌트 수정이력 저장(원장 등록 및 수정)
	 * - 일괄 등록의 이력은 별도
	 * @param confId
	 * @param mainRootType (자산분류체계에 매핑된 컴포넌트면 CLASS_TYPE이, 구성용도에 매핑된 컴포넌트면 CONF_TYPE이 mainRootType에 들어옴.)
	 * @param tabDataMap<HashMap>
	 * @param chgMsg
	 * @param updUSerId
	 * tabDataMap : {SV : [{CONF_ID: value, CLASS_TYPE: SV},{CONF_ID: value, CLASS_TYPE: SV}],
	 * 						NW : [{CONF_ID: value, CLASS_TYPE: NW},{CONF_ID: value, CLASS_TYPE: NW}]
	 * 						}  
	 * 
	 * {REL_CONF_ID: value, REL_CLASS_TYPE: value}
	 * 
	 */
	@Override
	public void insertRelInfraCompHistory(String confId, String mainRootType, HashMap tabDataMap, String chgMsg, String updUserId)throws NkiaException {
		
		// 변경 데이터 목록
		List<HashMap> resultNewDataList = new ArrayList();	// 각 탭의 모든 데이터 목록
		List<HashMap> addDataList = new ArrayList();			// 추가된 데이터 목록
		List<HashMap> removeDataList = new ArrayList();		// 삭제된 데이터 목록

		// 데이터 변경여부
		boolean isDataChg = false;
		
		Iterator iter = tabDataMap.keySet().iterator();
		List<String> tabIdList = new ArrayList();
		String entity_mng_type = (String) tabDataMap.get("entity_mng_type");
		
		while (iter.hasNext()) {
			String relRootType = (String) iter.next(); //관계정보 탭리스트의 ID를 가져옴.(예시)SV,NW,ST...)

			//hmsong cmdb 개선 작업 : 연관 서비스는 이력 별도 관리, entity_mng_type는 추후 처리할 예정
			if("RS".equals(relRootType) || "entity_mng_type".equals(relRootType)) {
				continue;
			}
			
			List<HashMap> tabDataList = (List) tabDataMap.get(relRootType);
			//각 탭의 현재 데이터 목록을 조회한다.
			HashMap paramMap = new HashMap();
			paramMap.put("CONF_ID", confId);
			paramMap.put("REL_ROOT_TYPE", relRootType);
			paramMap.put("entity_mng_type", entity_mng_type);
			List<HashMap> preDataList = new ArrayList<HashMap>();
			
			if("DR".equals(relRootType)) {
				preDataList = assetHistoryDAO.selectCurrentRelDRDataList(paramMap);
			} else {
				preDataList = assetHistoryDAO.selectCurrentRelInfraDataList(paramMap);
			}

			if((tabDataList == null || tabDataList.isEmpty()) && (preDataList == null || preDataList.isEmpty()) ){
				// 기존과 신규 둘 다 null 일 경우 이력등록 pass
			}else{
				
				/**  데이터 변경여부 비교 시작 - 
				 * 각 탭 통틀어 하나라도 변경이 된 경우 모든 탭의 리스트를 이력등록한다.
				 * 기존 데이터가 있었다가 삭제된 경우 각 탭(분류체계) 별로 null 데이터 (-) 를 등록한다.
				 */
				if(tabDataList == null || tabDataList.isEmpty()){
					HashMap preDataMap = (HashMap) preDataList.get(0);
					String preRelConfId = (String) preDataMap.get("REL_CONF_ID");
					// 탭데이터가 삭제된 경우 : 이전 데이터의 값이 '-' 이 아니면 null 데이터('-') 를 이력으로 등록
					if(!"-".equals(preRelConfId)){
						HashMap nullDataMap = new HashMap();
						nullDataMap.put("REL_CONF_ID", "-");
						nullDataMap.put("REL_ROOT_TYPE", relRootType);	
						nullDataMap.put("REL_TYPE_CD", relRootType);
						resultNewDataList.add(nullDataMap);
						isDataChg = true;
						
						// 이전 데이터가 모두 삭제된 경우이므로 삭제된 데이터 List에 담아준다.
						removeDataList.addAll(preDataList);
					}
					
				}else{
					
					List<HashMap> cPreDataList = new ArrayList();
					List<HashMap> cTabDataList = new ArrayList();
					
					/** 각 List 의 Map 형식을 동일하게 만들어준다. */
					for(HashMap tabDataDetailMap : tabDataList){
						HashMap tempMap = new HashMap();
						tempMap.put("REL_CONF_ID", tabDataDetailMap.get("CONF_ID"));
						
						//hmsong cmdb 개선 작업 : 업데이트 할 관계정보 탭 컴포넌트가 자산분류체계에 매핑되에 있으면 REL_ROOT_TYPE에 CLASS_TYPE을 넣어주고, 그렇지 않을 때는 CONF_TYPE을 넣어준다.
						if("ASSET".equals(entity_mng_type)) {
							tempMap.put("REL_ROOT_TYPE", tabDataDetailMap.get("CLASS_TYPE"));							
						} else {
							tempMap.put("REL_ROOT_TYPE", tabDataDetailMap.get("CONF_TYPE"));		
						}
						tempMap.put("REL_TYPE_CD", relRootType);
						cTabDataList.add(tempMap);
					}
					
					for(HashMap preDataDetailMap : preDataList){
						HashMap tempMap = new HashMap();
						tempMap.put("REL_CONF_ID", preDataDetailMap.get("REL_CONF_ID"));
						
						if("ASSET".equals(entity_mng_type)) {
							tempMap.put("REL_ROOT_TYPE", preDataDetailMap.get("CLASS_TYPE"));							
						} else {
							tempMap.put("REL_ROOT_TYPE", preDataDetailMap.get("CONF_TYPE"));		
						}
						
						tempMap.put("REL_TYPE_CD", relRootType);
						cPreDataList.add(tempMap);
					}
					
					/** 삭제/추가 여부를 비교하여 각각의 List에 담아준다. */ 
					// 삭제된 경우
					for(HashMap preDataMap : cPreDataList){
						if( (!"-".equals(preDataMap.get("REL_CONF_ID"))) && (!cTabDataList.contains(preDataMap)) ){
							removeDataList.add(preDataMap);
							isDataChg = true;
						}
					}
					
					// 추가된 경우
					for(HashMap newDataMap : cTabDataList){
						if(!cPreDataList.contains(newDataMap)){
							addDataList.add(newDataMap);
							isDataChg = true;
						}
					}
					// 각 탭의 데이터리스트를 하나의 리스트에 담는다.
					resultNewDataList.addAll(cTabDataList);
				}
				/**  데이터 변경여부 비교 끝 */
				
			}
		
		}
		
		if(isDataChg){
			// 탭의 모든 데이터목록을 이력데이터에 등록한다.
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("SEQ", chgSeq);
				chgHisDataMap.put("CHG_MESSAGE", chgMsg);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				
				if("DR".equals(chgHisDataMap.get("REL_TYPE_CD"))) {
					assetHistoryDAO.insertRelDRCompHistory(chgHisDataMap);
				} else {
					assetHistoryDAO.insertRelInfraCompHistory(chgHisDataMap);
				}
			}
			
			/** 연계자산에 대한 이력데이터 등록 */
			// 추가된 연계장비의 이력 등록
			for(HashMap addDataMap : addDataList){
				addDataMap.put("CONF_ID", confId);
				addDataMap.put("REL_ROOT_TYPE", mainRootType);
				addDataMap.put("SEQ", chgSeq);
				addDataMap.put("CHG_MESSAGE", chgMsg);
				addDataMap.put("INS_USER_ID", updUserId);
				addDataMap.put("entity_mng_type", entity_mng_type);
				if("DR".equals(addDataMap.get("REL_TYPE_CD"))) {
					assetHistoryDAO.insertAddRelDRSyncHistory(addDataMap);
				} else {
					assetHistoryDAO.insertAddRelInfraSyncHistory(addDataMap);
				}
			}
			
			// 삭제된 연계장비의 이력 등록
			for(HashMap removeDataMap : removeDataList){
				removeDataMap.put("CONF_ID", confId);
				removeDataMap.put("REL_ROOT_TYPE", mainRootType);
				removeDataMap.put("SEQ", chgSeq);
				removeDataMap.put("CHG_MESSAGE", chgMsg);
				removeDataMap.put("INS_USER_ID", updUserId);
				
				// 삭제되는 연계장비를 제외한 현재 연계장비 이력을 다시 INSERT 해준다.
				if("DR".equals(removeDataMap.get("REL_TYPE_CD"))) {
					assetHistoryDAO.insertRemoveRelDRSyncHistory(removeDataMap);
					
					// 연계장비의 현재 연관장비 목록 조회(삭제되는 연관장비 제외한 목록)
					List<HashMap> relInfraList = assetHistoryDAO.selectRelDRList(removeDataMap);
					if(relInfraList.size() == 0){
						// 삭제된 연계장비 분류체계의 데이터가 없으면 NULL 데이터 (') 를 INSERT 해준다.
						assetHistoryDAO.insertRelDRNullDataHistory(removeDataMap);
					}
				} else {
					assetHistoryDAO.insertRemoveRelInfraSyncHistory(removeDataMap);
					
					// 연계장비의 현재 연관장비 목록 조회(삭제되는 연관장비 제외한 목록)
					List<HashMap> relInfraList = assetHistoryDAO.selectRelInfraList(removeDataMap);
					if(relInfraList.size() == 0){
						// 삭제된 연계장비 분류체계의 데이터가 없으면 NULL 데이터 (') 를 INSERT 해준다.
						assetHistoryDAO.insertRelInfraNullDataHistory(removeDataMap);
					}
				}
			}
		}
		
	}
	
	/**
	 * 설치정보 컴포넌트 이력 등록
	 * 2017.04.03 현재 : 설치DISK, 설치NIF
	 */
	@Override
	public void insertInstallTabListCompHistory(String conf_id, HashMap tabDataMap, String chgMsg, String user_id) throws NkiaException {
		
		// 탭의 key값을 List 로 반환한다.
		Iterator iter = tabDataMap.keySet().iterator();
		List<String> tabIdList = new ArrayList();
		while (iter.hasNext()) {
			String tabId = (String) iter.next();
			tabIdList.add(tabId);
		}
		
		//  각 탭의 분기별로 이력을 별도 등록한다.
		for(String tabId : tabIdList){
			List tabDataList = (List) tabDataMap.get(tabId);
			if("DISK".equals(tabId)){
				insDiskCompHistory(conf_id, tabDataList, chgMsg, user_id);
			}else if("NIF".equals(tabId)){
				insNifCompHistory(conf_id, tabDataList, chgMsg, user_id);
			}
		}
		
	}

	/**
	 * 설치 DISK 이력데이터 등록
	 * @param conf_id
	 * @param tabDataList
	 * @param string
	 * @param user_id
	 */
	private void insDiskCompHistory(String confId, List<HashMap> tabDataList, String chgMsg, String updUserId) {
		
		List<HashMap> resultNewDataList = new ArrayList();
				
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentInsDiskDataList(paramMap);
		
		// 데이터 변경여부 비교
		boolean isDataChg = compareInsComData(preDataList, tabDataList, "DISK", resultNewDataList);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("SEQ", chgSeq);
				chgHisDataMap.put("CHG_MESSAGE", chgMsg);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertInsDiskCompHistory(chgHisDataMap);
			}
		}
	}
	
	/**
	 * 설치 NIF 이력데이터 등록
	 * @param conf_id
	 * @param tabDataList
	 * @param string
	 * @param user_id
	 */
	private void insNifCompHistory(String confId, List<HashMap> tabDataList, String chgMsg, String updUserId) {

		List<HashMap> resultNewDataList = new ArrayList();
				
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentInsNifDataList(paramMap);
		
		//데이터 변경여부 비교
		boolean isDataChg = compareInsComData(preDataList, tabDataList, "NIF", resultNewDataList);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("SEQ", chgSeq);
				chgHisDataMap.put("CHG_MESSAGE", chgMsg);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertInsNifCompHistory(chgHisDataMap);
			}
		}
		
	}

	/**
	 * 설치정보 컴포넌트에 대한 데이터 변경여부 비교
	 * @param preDataList
	 * @param tabDataList
	 * @param insCompType
	 * @param resultNewDataList
	 * @return
	 */
	public boolean compareInsComData(List<HashMap> preDataList, List<HashMap> tabDataList, String insCompType, List resultNewDataList){
		boolean isDataChg = false;
		String[] insCols = null;		
		if("DISK".equals(insCompType)){
			insCols = AM_INS_DISK_COLS;
		}else if ("NIF".equals(insCompType)){
			insCols = AM_INS_NIF_COLS;
		}
		
		if((tabDataList == null || tabDataList.isEmpty()) && (preDataList == null || preDataList.isEmpty())){
				return isDataChg;
		}else{
			
			if(tabDataList == null || tabDataList.isEmpty()){
				HashMap preDataMap = (HashMap) preDataList.get(0);
				String nifNm = (String) preDataMap.get(insCols[0]);
				
				// 이전 데이터의 값이 '-' 이 아니면 null 데이터('-') 를 이력으로 등록
				if(!"-".equals(nifNm) ){
					HashMap nullDataMap = new HashMap();
					nullDataMap.put(insCols[0], "-");
					resultNewDataList.add(nullDataMap);
					isDataChg = true;
				}
				
			}else if( preDataList == null || preDataList.isEmpty()){
				// 이전데이터가 null 이면 현재 목록 모두 이력에 등록.
				isDataChg = true;
			}else if(preDataList.size() != tabDataList.size()){
				// 이전 데이터와 현재 데이터의 크리가 다르면 변경된것으로 간주한다.
				isDataChg = true;
			}else{
				for(HashMap tabDataMap : tabDataList){
					String newKeyValue = (String) tabDataMap.get(insCols[0]);	// 각 row 데이터의 키가되는 컬럼의 값
					boolean isExist = false; 	// 변경되는 row가 기존에 있던 row 인지 구분
					
					for(int i = 0; i < preDataList.size(); i++){
						HashMap preDataMap = preDataList.get(i);
						// 키가 되는 row 가 같은게 있으면 세부 컬럼의 값을 비교한다.
						if(newKeyValue.equals(preDataMap.get(insCols[0]))){
							isExist = true;
							// 각 컬럼의 데이터 비교
							for(int k = 1; k < insCols.length; k++){
								String colId = insCols[k];
								if(tabDataMap.containsKey(colId) && preDataMap.containsKey(colId)){
									String newValue = StringUtil.parseString(tabDataMap.get(colId));
									String preValue = StringUtil.parseString(preDataMap.get(colId));
									if(!newValue.equals(preValue)){
										isDataChg = true;
										break;
									}
								}
							}
						}
					}
					if(!isExist){
						isDataChg = true;	 // 넘어온 값이 이전 데이터에 없는 값이면, 변경된 데이터임.
						break;
					}
				}
				
			}
		}
		if(isDataChg && tabDataList != null){
			resultNewDataList.addAll(tabDataList);
		}
		return isDataChg;
	}

	/**
	 * DCA 수집 설치DISK 이력 등록
	 */
	@Override
	public void insertDcaInsDiskHistory(String updUserId) throws NkiaException {
		
		// DCA 동기화되는 DISK가 있는 구성id 목록
		List<HashMap> updateDiskConfList = assetHistoryDAO.selectUpdateDiskConfList();
		
		// 각 구성별 이력모듈 호출
		if(updateDiskConfList != null){
			for(HashMap dataMap  : updateDiskConfList){
				String confId = (String) dataMap.get("CONF_ID");
				List newDataList = assetHistoryDAO.selectInsDiskList(confId);	// 변경된 disk 정보 목록 조회
				insDiskCompHistory(confId, newDataList, "IF 자동 업데이트", updUserId);
			}
		}
		
	}

	/**
	 * DCA 수집 설치NIF 이력 등록
	 */
	@Override
	public void insertDcaInsNifHistory(String updUserId) throws NkiaException {
		// DCA 동기화되는 nif가 있는 구성id 목록
		List<HashMap> updateNifConfList = assetHistoryDAO.selectUpdateNifConfList();
		
		// 각 구성별 이력모듈 호출
		if(updateNifConfList != null){
			for(HashMap dataMap  : updateNifConfList){
				String confId = (String) dataMap.get("CONF_ID");
				List newDataList = assetHistoryDAO.selectInsNifList(confId);	// 변경된 disk 정보 목록 조회
				insNifCompHistory(confId, newDataList, "IF 자동 업데이트", updUserId);
			}
		}
	}
	
	/**
	 * [과금컴포넌트 이력등록]
	 * 과금 CPU 증설 이력 등록
	 * @param confId
	 * @param newDataList
	 * @param chgMsg
	 * @param updUserId
	 * @throws NkiaException
	 */
	public void insertBsInsCpuHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId)throws NkiaException{
		
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentBsInsCpuList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, BS_INS_CPU_COLS, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectBsHistIndx();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("BS_HIST_INDX", chgSeq);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertBsInsCoreHistory(chgHisDataMap);
			}
		}
		
	}
	
	/**
	 * [과금컴포넌트 이력등록]
	 * 과금 메모리 증설 이력 등록
	 * @param confId
	 * @param newDataList
	 * @param chgMsg
	 * @param updUserId
	 * @throws NkiaException
	 */
	public void insertBsInsMemoryHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId)throws NkiaException{
		
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentBsInsMemoryList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, BS_INS_MEMORY_COLS, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectBsHistIndx();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("BS_HIST_INDX", chgSeq);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertBsInsMemoryHistory(chgHisDataMap);
			}
		}
		
	}
	
	/**
	 * [과금컴포넌트 이력등록]
	 * 과금 스토리지 증설 이력 등록
	 * @param confId
	 * @param newDataList
	 * @param chgMsg
	 * @param updUserId
	 * @throws NkiaException
	 */
	public void insertBsInsStorageHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId)throws NkiaException{
		
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentBsInsStorageList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, BS_INS_STORAGE_COLS, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectBsHistIndx();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("BS_HIST_INDX", chgSeq);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertBsInsStorageHistory(chgHisDataMap);
			}
		}
		
	}
	
	/**
	 * [과금컴포넌트 이력등록]
	 * 과금 서비스중단일 이력 등록
	 * @param confId
	 * @param newDataList
	 * @param chgMsg
	 * @param updUserId
	 * @throws NkiaException
	 */
	public void insertBsSvcStopHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId)throws NkiaException{
		
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentBsSvcStopList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, BS_SVC_STOP_COLS, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectBsHistIndx();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("BS_HIST_INDX", chgSeq);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertBsSvcStopHistory(chgHisDataMap);
			}
		}
		
	}
	
	/**
	 * [과금컴포넌트 이력등록]
	 * 과금 공통비 청구처 이력 등록
	 * @param confId
	 * @param newDataList
	 * @param chgMsg
	 * @param updUserId
	 * @throws NkiaException
	 */
	public void insertBsCmprcCustRateHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId)throws NkiaException{
		
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectBsCmprcCustRateList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, BS_CMPRC_CUST_RATE_COLS, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectBsHistIndx();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("BS_HIST_INDX", chgSeq);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertBsCmprcCustRateHistory(chgHisDataMap);
			}
		}
		
	}
	
	/**
	 * 청구처별 사용량 이력 등록 실행
	 * @param confId
	 * @param confType
	 * @param newDataList
	 * @param chgMsg
	 * @param updUserId
	 * @throws NkiaException
	 */
	@Override
	public void insertBsCustRateHistory(String confId, String confType, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException {
		
		List<HashMap> resultNewDataList = new ArrayList();
		
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		paramMap.put("CONF_TYPE", confType);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentBsCustRate(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, BS_CUST_RATE_COLS, resultNewDataList, 2);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectBsHistIndx();
			for(HashMap chgHisDataMap : resultNewDataList){
				chgHisDataMap.put("CONF_ID", confId);
				chgHisDataMap.put("BS_HIST_INDX", chgSeq);
				chgHisDataMap.put("CONF_TYPE", confType);
				chgHisDataMap.put("INS_USER_ID", updUserId);
				assetHistoryDAO.insertBsCustRateHistory(chgHisDataMap);
			}
		}
	}
	
	/**
	 *  [컴포넌트 이력]
	 * 컴포넌트 데이터 변경여부 비교
	 * @param preDataList
	 * @param newDataList
	 * @param compareCols
	 * @param resultNewDataList
	 * @return
	 */
	private boolean compareBsCompData(List<HashMap> preDataList, List<HashMap> newDataList, String[] compareCols, List resultNewDataList, int keyColCnt){
		boolean isDataChg = false;
		
		if((newDataList == null || newDataList.isEmpty()) && (preDataList == null || preDataList.isEmpty())){
			//
		}else{
			
			if(newDataList == null || newDataList.isEmpty()){
				HashMap preDataMap = (HashMap) preDataList.get(0);
				String bsIndx = String.valueOf(preDataMap.get(compareCols[0]));
				
				// 이전 데이터의 값이 '-' 이 아니면 null 데이터('-') 를 이력으로 등록
				if(!"-".equals(bsIndx) ){
					HashMap nullDataMap = new HashMap();
					// 키가 되는 컬럼의 개수만큼(필수값인 컬럼) '-' 을 넣어준다.
					for(int i=0; i < keyColCnt; i++){
						nullDataMap.put(compareCols[i], "-");
					}
					resultNewDataList.add(nullDataMap);
					isDataChg = true;
				}
				
			}else if( preDataList == null || preDataList.isEmpty()){
				// 이전데이터가 null 이면 현재 목록 모두 이력에 등록.
				isDataChg = true;
			}else if(preDataList.size() != newDataList.size()){
				// 이전 데이터와 현재 데이터의 크리가 다르면 변경된것으로 간주한다.
				isDataChg = true;
			}else{	// 각 목록이 null 이 아니고 사이즈가 같을 경우
				// 각 컬럼의 데이터를 비교해준다.
				for(HashMap newDataMap : newDataList){
					int rowEqualCnt = 0;
					for(int i = 0; i < preDataList.size(); i++){
						HashMap preDataMap = preDataList.get(i);
						// 각 컬럼의 데이터 비교
						int colEqualCnt = 0;
						for(int k = 0; k < compareCols.length; k++){
							String colId = compareCols[k];
							if(newDataMap.containsKey(colId) && preDataMap.containsKey(colId)){
								String newValue = StringUtil.parseString(newDataMap.get(colId));
								String preValue = StringUtil.parseString(preDataMap.get(colId));
								if(newValue.equals(preValue)){
									colEqualCnt++;
								}
							}
						}
						if(colEqualCnt == compareCols.length){
							rowEqualCnt++;
							preDataList.remove(i);
							break;
						}
					}
					if(rowEqualCnt == 0) {
						isDataChg = true;
						break;
					}
				}
			}
		}
		// 변경이 일어나고, 변경되는 데이터가 null이 아닌경우 변경되는 데이터 목록을 최종 변경리스트(resultNewDataList) 에 담아준다.
		if(isDataChg && newDataList != null){
			resultNewDataList.addAll(newDataList);
		}
		return isDataChg;
	}

	/**
	 * [과금 속성]
	 * 과금속성 변경여부 비교
	 * @param newDataMap	현재 데이터 맵
	 * @param preDataMap	이전 데이터 맵
	 * @param compareCols	과금 속성 배열
	 * @param isDataChg		변경여부
	 * @return
	 */
	private boolean isBsAttrChg(HashMap newDataMap, HashMap preDataMap, String[] compareCols, boolean isDataChg){
		for(String colId : compareCols){
			if(newDataMap.containsKey(colId) && preDataMap.containsKey(colId)){
				String newValue = StringUtil.parseString(newDataMap.get(colId));
				String preValue = StringUtil.parseString(preDataMap.get(colId));
				if(!newValue.equals(preValue)){
					isDataChg = true;
					break;
				}
			}
		}
		return isDataChg;
	}
	
	/**
	 * [과금속성 이력등록]
	 * 서버과금 속성 이력 등록
	 */
	public void insertBsSvHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException {
		
		boolean isDataChg = false;
		
		// 현재 데이터 조회
		HashMap preDataMap = assetHistoryDAO.selectCurrentBsSvData(confId);
		
		// 각 속성의 데이터 비교
		if(preDataMap == null){
			isDataChg = true;	// 이전데이터가 없으면(최초등록) 이력등록한다.
		}else{
			// 과금속성 변경여부 
			isDataChg = isBsAttrChg(newDataMap,preDataMap, BS_SV_COLS, isDataChg);
		}
		
		// 속성 1개라도 변경되었을경우 이력등록한다.
		if(isDataChg){
			newDataMap.put("CONF_ID",confId);
			newDataMap.put("INS_USER_ID",updUserId);
			assetHistoryDAO.insertBsSvHistory(newDataMap);
		}
		
	}

	/**
	 * [과금속성 이력등록]
	 * 스토리지과금 속성 이력 등록
	 */
	public void insertBsStHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException {
		
		boolean isDataChg = false;
		
		// 현재 데이터 조회
		HashMap preDataMap = assetHistoryDAO.selectCurrentBsStData(confId);
		
		// 각 속성의 데이터 비교
		if(preDataMap == null){
			isDataChg = true;	// 이전데이터가 없으면(최초등록) 이력등록한다.
		}else{
			// 과금속성 변경여부 
			isDataChg = isBsAttrChg(newDataMap,preDataMap, BS_ST_COLS, isDataChg);
		}
		
		// 속성 1개라도 변경되었을경우 이력등록한다.
		if(isDataChg){
			newDataMap.put("CONF_ID",confId);
			newDataMap.put("INS_USER_ID",updUserId);
			assetHistoryDAO.insertBsStHistory(newDataMap);
		}
		
	}
	
	/**
	 * [과금속성 이력등록]
	 * 전용회선과금 속성 이력 등록
	 */
	public void insertBsLnHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException {
		
		boolean isDataChg = false;
		
		// 현재 데이터 조회
		HashMap preDataMap = assetHistoryDAO.selectCurrentBsLnData(confId);
		
		// 각 속성의 데이터 비교
		if(preDataMap == null){
			isDataChg = true;	// 이전데이터가 없으면(최초등록) 이력등록한다.
		}else{
			// 과금속성 변경여부 
			isDataChg = isBsAttrChg(newDataMap,preDataMap, BS_LN_COLS, isDataChg);
		}
		
		// 속성 1개라도 변경되었을경우 이력등록한다.
		if(isDataChg){
			newDataMap.put("CONF_ID",confId);
			newDataMap.put("INS_USER_ID",updUserId);
			assetHistoryDAO.insertBsLnHistory(newDataMap);
		}
		
	}
	
	/**
	 * [과금속성 이력등록]
	 * 네트워크과금 속성 이력 등록
	 */
	public void insertBsNwHistory(String confId, HashMap newDataMap, String chgMsg, String updUserId) throws NkiaException {
		
		boolean isDataChg = false;
		
		// 현재 데이터 조회
		HashMap preDataMap = assetHistoryDAO.selectCurrentBsNwData(confId);
		
		// 각 속성의 데이터 비교
		if(preDataMap == null){
			isDataChg = true;	// 이전데이터가 없으면(최초등록) 이력등록한다.
		}else{
			// 과금속성 변경여부 
			isDataChg = isBsAttrChg(newDataMap,preDataMap, BS_NW_COLS, isDataChg);
		}
		
		// 속성 1개라도 변경되었을경우 이력등록한다.
		if(isDataChg){
			newDataMap.put("CONF_ID",confId);
			newDataMap.put("INS_USER_ID",updUserId);
			assetHistoryDAO.insertBsNwHistory(newDataMap);
		}
		
	}
	
	/***  이력 조회 ****/
	/**
	 * 자산원장의 수정이력 리스트를 불러온다
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsHisAttrList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisAttrList(paramMap);
	}
	
	/**
	 * 자산원장의 수정이력 상세정보 리스트를 불러온다
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsHisAttrDetailList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisAttrDetailList(paramMap);
	}
	
	/**
	 * 자산원장의 설치 디스크 수정이력 상세 리스트를 불러온다
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsHisInsDiskDetailList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisInsDiskDetailList(paramMap);
	}
	
	/**
	 * 자산원장의 네트워크인터페이스 수정이력 상세 리스트를 불러온다
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@Override
	public List selectOpmsHisInsNifDetailList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisInsNifDetailList(paramMap);
	}

	/**
	 * 자산원장의 운영자 수정이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisOperList(ModelMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisOperList(paramMap);
	}

	/**
	 * 자산원장의 연계서비스 수정이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisRelServiceList(ModelMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisRelServiceList(paramMap);
	}

	/**
	 * 자산원장의 연계장비 수정이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisRelInfraList(ModelMap paramMap) throws NkiaException {
		List returnList = new ArrayList();
		
		//hmsong cmdb 개선 작업 Start
		//confAdminYn - 구성용도 속성 관리여부(설명 : 구성용도의 속성을 관리하는지, 따로 관리하지 않는지를 결정하는 프로퍼티)
		String confAdminYn = new String();
		if(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn") != null) {
			if(!"".equals(StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn")))) {
				confAdminYn = StringUtil.parseString(NkiaApplicationPropertiesMap.getProperty("Globals.Itg.Cmdb.ConfAdminYn"));
			}
		}			
		paramMap.put("confAdminYn", confAdminYn);
		return assetHistoryDAO.selectOpmsHisRelInfraList(paramMap);
	}
	
	/**
	 * 자산원장의 연계장비(DR) 수정이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisRelDRList(ModelMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisRelDRList(paramMap);
	}

	/**
	 * 서버과금 속성 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsSvList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsSvList(paramMap);
	}

	/**
	 * 스토리지과금 속성 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsStList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsStList(paramMap);
	}
	
	/**
	 * 전용회선과금 속성 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsLnList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsLnList(paramMap);
	}
	
	/**
	 * 네트워크과금속성 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsNwList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsNwList(paramMap);
	}

	/**
	 * 메모리증설 컴포넌트 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsInsMemoryList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsInsMemoryList(paramMap);
	}

	/**
	 * 과금 core 증설 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsInsCpuList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsInsCpuList(paramMap);
	}

	/**
	 * 과금 스토리지 증설 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsInsStorageList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsInsStorageList(paramMap);
	}

	/**
	 * 서비스중단일 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsSvcStopList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsSvcStopList(paramMap);
	}
	
	/**
	 * 청구처별 사용량 리스트 조회
	 */
	@Override
	public List selectOpmsBsCustRateList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsBsCustRateList(paramMap);
	}

	/**
	 * 청구처별 사용량 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsCustRateList(ModelMap paramMap) {
		return assetHistoryDAO.selectOpmsHisBsCustRateList(paramMap);
	}

	/**
	 * 공통비청구처 이력 리스트 조회
	 */
	@Override
	public List selectOpmsHisBsCmprcCustRateList(ModelMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisBsCmprcCustRateList(paramMap);
	}

	/**
	 * 디스크정보 이력 등록
	 */
	@Override
	public void insertRelDiskCompHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException {
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentRelDiskList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, REL_DISK, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			HashMap chgHisDataMap = new HashMap();
			chgHisDataMap.put("CONF_ID", confId);
			chgHisDataMap.put("SEQ", chgSeq);
			chgHisDataMap.put("CHG_MESSAGE", chgMsg);
			chgHisDataMap.put("INS_USER_ID", updUserId);
			chgHisDataMap.put("hisList", resultNewDataList);
			assetHistoryDAO.insertRelDiskCompHistory(chgHisDataMap);
		}
	}

	/**
	 * 디스크정보
	 * 수정이력 목록 조회
	 */
	@Override
	public List selectOpmsHisRelDiskList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisRelDiskList(paramMap);
	}
	
	/**
	 * IP정보 이력 등록
	 */
	@Override
	public void insertRelIPCompHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException {
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentRelIPList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, REL_IP, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			HashMap chgHisDataMap = new HashMap();
			chgHisDataMap.put("CONF_ID", confId);
			chgHisDataMap.put("SEQ", chgSeq);
			chgHisDataMap.put("CHG_MESSAGE", chgMsg);
			chgHisDataMap.put("INS_USER_ID", updUserId);
			chgHisDataMap.put("hisList", resultNewDataList);
			assetHistoryDAO.insertRelIPCompHistory(chgHisDataMap);
		}
	}

	/**
	 * IP정보
	 * 수정이력 목록 조회
	 */
	@Override
	public List selectOpmsHisRelIPList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisRelIPList(paramMap);
	}

	/**
	 * Nic정보 이력 등록
	 */
	@Override
	public void insertRelNicCompHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException {
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentRelNicList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, REL_NIC, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			HashMap chgHisDataMap = new HashMap();
			chgHisDataMap.put("CONF_ID", confId);
			chgHisDataMap.put("SEQ", chgSeq);
			chgHisDataMap.put("CHG_MESSAGE", chgMsg);
			chgHisDataMap.put("INS_USER_ID", updUserId);
			chgHisDataMap.put("hisList", resultNewDataList);
			assetHistoryDAO.insertRelNicCompHistory(chgHisDataMap);
		}
	}	
	
	/**
	 * Nic정보
	 * 수정이력 목록 조회
	 */
	@Override
	public List selectOpmsHisRelNicList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisRelNicList(paramMap);
	}
	
	/**
	 * 이중화 정보 이력 등록
	 */
	@Override
	public void insertDualInfoCompHistory(String confId, List<HashMap> newDataList, String chgMsg, String updUserId) throws NkiaException {
		List<HashMap> resultNewDataList = new ArrayList();
		
		// 현재 데이터 목록을 조회한다.
		HashMap paramMap = new HashMap();
		paramMap.put("CONF_ID", confId);
		List<HashMap> preDataList = assetHistoryDAO.selectCurrentDualInfoList(paramMap);
		
		// 데이터 변경여부를 비교한다.
		boolean isDataChg = compareBsCompData(preDataList, newDataList, DUAL_INFO, resultNewDataList, 1);
		
		// 변경여부가 true 이면 이력테이블 등록
		if(isDataChg){
			String chgSeq = assetHistoryDAO.selectCompHisSeq();
			HashMap chgHisDataMap = new HashMap();
			chgHisDataMap.put("CONF_ID", confId);
			chgHisDataMap.put("SEQ", chgSeq);
			chgHisDataMap.put("CHG_MESSAGE", chgMsg);
			chgHisDataMap.put("INS_USER_ID", updUserId);
			chgHisDataMap.put("hisList", resultNewDataList);
			assetHistoryDAO.insertDualInfoCompHistory(chgHisDataMap);
		}
	}	
	
	/**
	 * Nic정보
	 * 수정이력 목록 조회
	 */
	@Override
	public List selectOpmsHisDualInfoList(HashMap paramMap) throws NkiaException {
		return assetHistoryDAO.selectOpmsHisDualInfoList(paramMap);
	}

	//hmsong cmdb 개선 작업 Start
	/**
	 * ※ 해당 메소드는 conf_type(구성구분)개념 이후로 생긴 메소드입니다. 자산과 구성이 별개인 프로젝트에서 사용되는 메소드입니다.
	 * 자산, 구성속성 이력 저장 실행
	 * 현재 데이터와 이전 데이터를 비교하여 변경된 속성과 값만 추출하여 이력 등록한다.
	 * @param confId(구성ID),confType(구성용도),assetId(자산ID),classType(자산분류),entity_mng_type(자산구성속성구분),newAllDataList(분류체계별 속성에서 매핑한 목록),chgMsg(저장메시지),updUserId(저장한userId),mapTableKind(테이블이 자산속성인지, 구성속성인지 map형식으로 저장)
	 * @param newAllDataList
	 * @param chgMsg
	 * @param updUSerId
	 * newAttrList <{"COLUMN_ID": value, "CHG_VALUE" : value}>:
	 * KEY 가 되는 컬럼ID는 대문자로 담아준다.
	 */
	@Override
	public void insertAssetConfAttrHistory(String confId, String confType, String assetId, String classType, String entity_mng_type, List<HashMap> newAllDataList,
			String chgMsg, String user_id, HashMap mapTableKind) throws NkiaException {
		
		if (newAllDataList != null) {
			//최종 변경속성 목록을 담아줄 List 생성
			List<HashMap> newDataList = new ArrayList();	// 넘어온 데이터 중 원장의 수정가능속성 데이터 목록 
			List<HashMap> chgDataList = new ArrayList();	// 수정이 일어난 데이터 목록

			// 원장에 있는 컬럼 중 수정 가능 속성 목록 조회
			HashMap entityParamMap = new HashMap();
			List<String> entityAttrList = new ArrayList();
			
			//entity_mng_type 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
			if(!"".equals(entity_mng_type)) {
				entityParamMap.put("mng_type", entity_mng_type);
				
				if("ASSET".equals(entity_mng_type)) {
					entityParamMap.put("assetId", assetId);
					entityAttrList = assetHistoryDAO.selectEntityAttrList(entityParamMap);	//MAP형식으로 수정. 테이블 정보 함꼐 로딩
				} else {
					entityParamMap.put("confId", confId);
					entityAttrList = assetHistoryDAO.selectEntityAttrList(entityParamMap); //MAP형식으로 수정. 테이블 정보 함꼐 로딩
				}
				
			}
			
			if(entityAttrList != null && !entityAttrList.isEmpty()){
				
				// 넘어온 데이터 중 원장의 수정 가능 속성만 추려서 별로 List(newDataList) 에 담아준다.
				for(HashMap attrMap : newAllDataList){
					String key = (String) attrMap.get("COLUMN_ID");
					if(entityAttrList.contains(key)){
						newDataList.add(attrMap);
					}
				}
				
				if(newDataList != null && !newDataList.isEmpty()) {
					
					// 현재 데이터를 이력테이블에서 조회한다.
					HashMap paramMap = new HashMap();
					paramMap.put("CONF_ID", confId);
					paramMap.put("ASSET_ID", assetId);
					paramMap.put("chgAttrList", newDataList);
					paramMap.put("classType", classType);
					paramMap.put("confType", confType);
					paramMap.put("mng_type", entity_mng_type);
					
					HashMap preDataMap = assetHistoryDAO.selectPreValueMap(paramMap);		// 이전값을 view테이블에서 현재 데이터로 조회 - 20181124 좌은진
					
					// 이력테이블에 이력데이터가 없을경우 넘어온 모든 속성값을 이력테이블에 저장하기 위해 최종 변경속성List에 담아준다.
					if(preDataMap == null || preDataMap.isEmpty()){
						chgDataList = newDataList;
					}else{
						/** 변경여부 비교 */
						//이전 데이터와 비교하여 변경된 데이터만 chgDataList 에 담아준다. 
						for(HashMap attrMap : newDataList){
							String key = (String) attrMap.get("COLUMN_ID");
							String newValue = StringUtil.parseString(attrMap.get("CHG_VALUE"));
							String preValue =  "";
							if(preDataMap.containsKey(key)){
								preValue = StringUtil.parseString(preDataMap.get(key));
							}
							if(!newValue.equals(preValue)){
								// 20200813 임현대 : 날짜형 데이터의 경우 별도 처리가 필요함(화면 : YYYY-MM-DD , DB : YY/MM/DD)
								if ( newValue.matches("\\d{4}-\\d{2}-\\d{2}") == true ){
									String strC = newValue.replaceAll("-", "");
									String strB = preValue.replaceAll("/", "");
									if ( strB != null){if(strB.length() == 6){strB = "20" + strB;}}
									if ( strC.equals(strB) == false ){
										attrMap.put("PRE_VALUE", preValue);
										chgDataList.add(attrMap);
									}
								}else {
									attrMap.put("PRE_VALUE", preValue);
									chgDataList.add(attrMap);
								}
							}
						}
					}
					// 변경속성값을 이력테이블에 등록
					if(chgDataList != null && !chgDataList.isEmpty()){
						for(HashMap chgHisDataMap : chgDataList){
							chgHisDataMap.put("ASSET_ID", assetId);
							chgHisDataMap.put("CONF_ID", confId);
							chgHisDataMap.put("CHG_MESSAGE", chgMsg);
							chgHisDataMap.put("INS_USER_ID", user_id);
							//entityAttrList 에서 호출한 정보 중 gentablenm으로 mapTableKind에서 찾아서, 자산인지 구성인지 구분
							
							String tableName = StringUtil.parseString(chgHisDataMap.get("GEN_TABLE_NM"));

							chgHisDataMap.put("TABLE_MNG_TYPE", mapTableKind.get(tableName)); //자산이력 구성이력은 테이블 기준이어야 함(자산이력, 구성이력, 자산구성이력) 테이블에 conf_id가 있는지 없는지로 구분하는 게 가장 확실
							
							//자산구성속성구분(ENTITY_MNG_TYPE)이 ASSET이면 ASSET_ID를 키로 잡아 저장
							//자산구성속성구분(ENTITY_MNG_TYPE)이 CONF나 ASSETCONF이면 CONF_ID를 키로 잡아 저장
							assetHistoryDAO.insertAssetAttrHistory(chgHisDataMap);
						}
					}
				}
			}
		}
	}
	//hmsong cmdb 개선 작업 End
}
