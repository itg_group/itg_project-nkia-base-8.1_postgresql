/*
 * @(#)AssetHistoryController.java              2016. 03. 28.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.itam.assetHis.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.assetHis.service.AssetHistoryService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class AssetHistoryController {

	@Resource(name = "assetHistoryService")
	private AssetHistoryService assetHistoryService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 자산원장의 수정이력 리스트를 불러온다
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisAttrList.do")
	public @ResponseBody ResultVO selectOpmsHisAttrList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisAttrList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산원장의 수정이력 상세정보 리스트를 불러온다
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisAttrDetailList.do")
	public @ResponseBody ResultVO selectOpmsHisAttrDetailList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisAttrDetailList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	

	/**
	 * 자산원장의 설치 디스크 수정이력 상세 리스트를 불러온다
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisInsDiskDetailList.do")
	public @ResponseBody ResultVO selectOpmsHisInsDiskDetailList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisInsDiskDetailList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 자산원장의 네트워크인터페이스 수정이력 상세 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisInsNifDetailList.do")
	public @ResponseBody ResultVO selectOpmsHisInsNifDetailList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisInsNifDetailList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산원장의 운영자 수정이력 상세 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisOperList.do")
	public @ResponseBody ResultVO selectOpmsHisOperList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisOperList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산원장의 연계서비스 수정이력 상세 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisRelServiceList.do")
	public @ResponseBody ResultVO selectOpmsHisRelServiceList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisRelServiceList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 자산원장의 연계장비 수정이력 상세 리스트를 불러온다
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisRelInfraList.do")
	public @ResponseBody ResultVO selectOpmsHisRelInfraList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			if("DR".equals(paramMap.get("CLASS_TYPE"))) {
				resultList = assetHistoryService.selectOpmsHisRelDRList(paramMap);
			} else {
				resultList = assetHistoryService.selectOpmsHisRelInfraList(paramMap);
			}
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 서버과금 속성 이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsSvList.do")
	public @ResponseBody ResultVO selectOpmsHisBsSvList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsSvList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 스토리지과금 속성 이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsStList.do")
	public @ResponseBody ResultVO selectOpmsHisBsStList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsStList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 전용회선과금 속성 이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsLnList.do")
	public @ResponseBody ResultVO selectOpmsHisBsLnList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsLnList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 네트워크과금 속성 이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsNwList.do")
	public @ResponseBody ResultVO selectOpmsHisBsNwList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsNwList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 과금 메모리증설 컴포넌트이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsInsMemoryList.do")
	public @ResponseBody ResultVO selectOpmsHisBsInsMemoryList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsInsMemoryList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 과금 core증설 컴포넌트이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsInsCpuList.do")
	public @ResponseBody ResultVO selectOpmsHisBsInsCpuList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsInsCpuList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 과금 스토리지 증설 컴포넌트이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsInsStorageList.do")
	public @ResponseBody ResultVO selectOpmsHisBsInsStorageList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsInsStorageList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 서비스중단일 컴포넌트이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsSvcStopList.do")
	public @ResponseBody ResultVO selectOpmsHisBsSvcStopList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsSvcStopList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 청구처별 사용량 컴포넌트이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsBsCustRateList.do")
	public @ResponseBody ResultVO selectOpmsBsCustRateList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsBsCustRateList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 청구처별 사용량 컴포넌트이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsCustRateList.do")
	public @ResponseBody ResultVO selectOpmsHisBsCustRateList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsCustRateList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	
	/**
	 * 공통비청구처 컴포넌트이력 리스트 조회
	 * TODO
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisBsCmprcCustRateList.do")
	public @ResponseBody ResultVO selectOpmsHisBsCmprcCustRateList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisBsCmprcCustRateList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [디스크컴포넌트]
	 * 수정이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisRelDiskList.do")
	public @ResponseBody ResultVO selectOpmsHisRelDiskList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisRelDiskList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * [IP컴포넌트]
	 * 수정이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisRelIPList.do")
	public @ResponseBody ResultVO selectOpmsHisRelIPList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisRelIPList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * [NIC컴포넌트]
	 * 수정이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisRelNicList.do")
	public @ResponseBody ResultVO selectOpmsHisRelNicList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisRelNicList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * [이중화 정보 컴포넌트]
	 * 수정이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/selectOpmsHisDualInfoList.do")
	public @ResponseBody ResultVO selectOpmsHisDualInfoList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = assetHistoryService.selectOpmsHisDualInfoList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}		
	
	/**
	 * 자산 테스트화면
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/itam/assetHistory/testPage.do")
	public String testPage(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/asset/testPageEjjwa.nvf";
		return forwarPage;
	}
	
	// 테스트 실행
	@RequestMapping(value="/itg/itam/assetHistory/testProc.do")
	public @ResponseBody ResultVO testProc(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			
			String confId = "SW17054086";
			List list = new ArrayList();
			String chgMsg = "이력 등록 테스트";
			String updUserId = "omsadmin";
			
//			assetHistoryService.insertBsCmprcCustRateHistory(confId, resultList, chgMsg, updUserId);
			
			assetHistoryService.insertBsCustRateHistory(confId, "SV", resultList, chgMsg, updUserId);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
