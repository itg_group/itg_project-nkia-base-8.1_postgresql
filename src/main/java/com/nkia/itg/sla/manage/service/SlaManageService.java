package com.nkia.itg.sla.manage.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

@SuppressWarnings({ "rawtypes" })
public interface SlaManageService {
	List searchContractAllTree(Map param) throws NkiaException;
	List searchContractAllTreeByCustomer(Map param) throws NkiaException;
	boolean insertCustomer(Map paramMap) throws NkiaException; 
	int deleteCustomer(Map paramMap) throws NkiaException;
	Map selectCustomer(Map paramMap) throws NkiaException;
	boolean insertContract(Map paramMap) throws NkiaException; 
	boolean updateContract(Map paramMap) throws NkiaException; 
	Map selectContract(Map paramMap) throws NkiaException; 
	Map selectLastContract(Map paramMap) throws NkiaException;
	
	int selectContractListCount(Map paramMap) throws NkiaException;
	int vaildContractDatePeriod(Map paramMap) throws NkiaException;
	List selectContractList(Map paramMap) throws NkiaException;
	List selectPopupIndicatorList(Map paramMap) throws NkiaException;
	int selectPopupIndicatorListCount(Map paramMap) throws NkiaException;
	List selectContractIndicatorList(Map paramMap) throws NkiaException;
	int selectContractIndicatorListCount(Map paramMap) throws NkiaException;
	List selectContractIndicatorTotalList(Map paramMap) throws NkiaException;
	int selectContractIndicatorTotalListCount(Map paramMap) throws NkiaException;
	List selectContractCalList(Map paramMap) throws NkiaException;
	int selectContractCalListCount(Map paramMap) throws NkiaException;
	List selectIndicatorCalViewList(Map paramMap) throws NkiaException;
	int selectIndicatorCalViewCount(Map paramMap) throws NkiaException;
	// 20200227_이원재_001_이원화
	List selectIndicatorCalViewByCustomerList(Map paramMap) throws NkiaException;
	int selectIndicatorCalViewByCustomerCount(Map paramMap) throws NkiaException;
	
	// 20200204_이원재_001_신규작성
	boolean insertCustomerPoint(Map paramMap) throws NkiaException;
	List selectCustomerPointList(Map paramMap) throws NkiaException;
	int selectCustomerPointCount(Map paramMap) throws NkiaException;
	
	// 20200210_이원재_001_신규작성
	List searchContractIndicatorExcel(Map paramMap) throws NkiaException;
	
	// 20200317_이원재_001_신규작성
	Map selectContractIndicator(Map paramMap) throws NkiaException;
	// 20200330_이원재_001_신규작성
	List selectEmsCustomerNmList(Map paramMap) throws NkiaException;
}
