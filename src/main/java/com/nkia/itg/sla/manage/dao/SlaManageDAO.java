package com.nkia.itg.sla.manage.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings({ "rawtypes" })
@Repository("SlaManageDAO")
public class SlaManageDAO extends EgovComAbstractDAO{
	public List searchContractAllTree(Map paramMap) throws NkiaException {
		return list("SlaManage.searchContractAllTree", paramMap);
	}
	// 20200227_이원재_001_이원화
	public List searchContractAllTreeByCustomer(Map paramMap) throws NkiaException {
		return list("SlaManage.searchContractAllTreeByCustomer", paramMap);
	}
	// 20200227_이원재_001_이원화 END
	public String insertCustomer(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManage.insertCustomer", paramMap);
	}
	// 20200408_이원재_007_신규추가
	public int deleteCustomer(Map paramMap) throws NkiaException {
		return delete("SlaManage.deleteCustomer", paramMap);
	}
	// 20200408_이원재_007_신규추가 END
	// 20200204_이원재_001_신규추가
	public int updateCustomerLimit(Map paramMap) throws NkiaException {
		return this.update("SlaManage.updateCustomerLimit", paramMap);
	}
	// 20200204_이원재_001_신규추가
	public Map selectCustomer(Map paramMap) throws NkiaException {
		return (Map) this.selectByPk("SlaManage.selectCustomer", paramMap);
	}
	// 20200204_이원재_001_신규추가 END
	public String insertContract(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManage.insertContract", paramMap);
	}
	public int updateContract(Map paramMap) throws NkiaException {
		return this.update("SlaManage.updateContract", paramMap);
	}
	public Map selectContract(Map paramMap) throws NkiaException {
		return (Map) this.selectByPk("SlaManage.selectContract", paramMap);
	}
	public List selectContractList(Map paramMap) throws NkiaException {
		return list("SlaManage.selectContractList", paramMap);
	}
	public Map selectLastContract(Map paramMap) throws NkiaException {
		return (Map) this.selectByPk("SlaManage.selectLastContract", paramMap);
	}
	public String copyContractIndicator(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManage.copyContractIndicator", paramMap);
	}
	public int selectContractListCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManage.selectContractListCount", paramMap);
	}
	public int vaildContractDatePeriod(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManage.vaildContractDatePeriod", paramMap);
	}
	public String insertContractIndicator(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManage.insertContractIndicator", paramMap);
	}
	public Map selectContractIndicator(Map paramMap) throws NkiaException {
		return (Map)this.selectByPk("SlaManage.selectContractIndicator", paramMap);
	}
	public int deleteContractIndicator(Map paramMap) throws NkiaException {
		return delete("SlaManage.deleteContractIndicator", paramMap);
	}
	// 20200120_이원재_001_신규추가
	public String insertCustomerGroup(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManage.insertCustomerGroup", paramMap);
	}
	// 20200121_이원재_002_신규추가
	public String insertContractFile(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManage.insertContractFile", paramMap);
	}
	// 20200131_이원재_003_신규추가
	public List selectContractFile(Map paramMap) throws NkiaException {
		return this.list("SlaManage.selectContractFile", paramMap);
	}
	// 20200131_이원재_003_신규추가
	public int deleteContractFile(Map paramMap) throws NkiaException {
		return this.delete("SlaManage.deleteContractFile", paramMap);
	}
	// 20200204_이원재_001_신규추가
	public String insertCustomerPoint(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManage.insertCustomerPoint", paramMap);
	}
	public int deleteCustomerPoint(Map paramMap) throws NkiaException {
		return delete("SlaManage.deleteCustomerPoint", paramMap);
	}
	public List selectCustomerPointList(Map paramMap) throws NkiaException {
		return this.list("SlaManage.selectCustomerPointList", paramMap);
	}
	public int selectCustomerPointCount(Map paramMap) throws NkiaException {
		return (Integer) selectByPk("SlaManage.selectCustomerPointCount", paramMap);
	}
	// 20200204_이원재_001_신규추가 END
	// 20200330_이원재_004_신규추가
	public List selectEmsCustomerNmList(Map paramMap) throws NkiaException {
		return this.list("SlaManage.selectEmsCustomerNmList", paramMap);
	}
	// 20200330_이원재_004_신규추가 END
	
	public List selectPopupIndicatorList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectPopupIndicatorList", paramMap);
	}
	public int selectPopupIndicatorListCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManageIndicator.selectPopupIndicatorListCount", paramMap);
	}
	public List selectContractIndicatorList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectContractIndicatorList", paramMap);
	}
	public int selectContractIndicatorListCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManageIndicator.selectContractIndicatorListCount", paramMap);
	}
	public List searchContractIndicatorExcel(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.searchContractIndicatorExcel", paramMap);
	}
	public List selectContractIndicatorTotalList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectContractIndicatorTotalList", paramMap);
	}
	public int selectContractIndicatorTotalListCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManageIndicator.selectContractIndicatorTotalListCount", paramMap);
	}
	public List searchIndicatorAllTree(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.searchIndicatorAllTree", paramMap);
	}
	public String insertIndicatorGroup(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManageIndicator.insertIndicatorGroup", paramMap);
	}
	public String insertIndicatorDetail(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManageIndicator.insertIndicatorDetail", paramMap);
	}
	public Map selectIndicatorGroup(Map paramMap) throws NkiaException {
		return (Map) this.selectByPk("SlaManageIndicator.selectIndicatorGroup", paramMap);
	}
	public Map selectIndicatorDetail(Map paramMap) throws NkiaException {
		return (Map) this.selectByPk("SlaManageIndicator.selectIndicatorDetail", paramMap);
	}
	public List selectIndicatorDetailList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectIndicatorDetailList", paramMap);
	}
	public int selectIndicatorDetailCount(Map paramMap) throws NkiaException {
		return (Integer) selectByPk("SlaManageIndicator.selectIndicatorDetailCount", paramMap);
	}
	public List updateContractIndicatorCalDetail(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.updateContractIndicatorCalDetail", paramMap);
	}
	public int updateIndicatorGroup(Map paramMap) throws NkiaException {
		return this.update("SlaManageIndicator.updateIndicatorGroup", paramMap);
	}
	public int updateCalIndicatorClose(Map paramMap) throws NkiaException {
		return this.update("SlaManageIndicator.updateCalIndicatorClose", paramMap);
	}
	public int updateIndicatorDetail(Map paramMap) throws NkiaException {
		return this.update("SlaManageIndicator.updateIndicatorDetail", paramMap);
	}
	public int updateIndicatorCalculator(Map paramMap) throws NkiaException {
		return this.update("SlaManageIndicator.updateIndicatorCalculator", paramMap);
	}
	public int updateContractIndicator(Map paramMap) throws NkiaException {
		return this.update("SlaManageIndicator.updateContractIndicator", paramMap);
	}
	public Map selectContractIndicatorCal(Map paramMap) throws NkiaException {
		return (Map) this.selectByPk("SlaManageIndicator.selectContractIndicatorCal", paramMap);
	}
	public String insertContractIndicatorCal(Map paramMap) throws NkiaException {
		return (String) this.insert("SlaManageIndicator.insertContractIndicatorCal", paramMap);
	}
	public int updateContractIndicatorCal(Map paramMap) throws NkiaException {
		return this.update("SlaManageIndicator.updateContractIndicatorCal", paramMap);
	}
	public int updateContractIndicatorSum(Map paramMap) throws NkiaException {
		return this.update("SlaManageIndicator.updateContractIndicatorSum", paramMap);
	}
	public List selectContractCalList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectContractCalList", paramMap);
	}
	public int selectContractCalListCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManageIndicator.selectContractCalListCount", paramMap);
	}
	public List selectIndicatorCalViewList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectIndicatorCalViewList", paramMap);
	}
	public int selectIndicatorCalViewCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManageIndicator.selectIndicatorCalViewCount", paramMap);
	}
	// 20200227_이원재_001_이원화
	public List selectIndicatorCalViewByCustomerList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectIndicatorCalViewByCustomerList", paramMap);
	}
	public int selectIndicatorCalViewByCustomerCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("SlaManageIndicator.selectIndicatorCalViewByCustomerCount", paramMap);
	}
	// 20200227_이원재_001_이원화 END
	public List indicatorGroupCombo(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.indicatorGroupCombo", paramMap);
	}
	public List indicatorCombo(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.indicatorCombo", paramMap);
	}
	public List selectIndicatorCriticViewList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.selectIndicatorCriticViewList", paramMap);
	}
	public Integer selectIndicatorCriticViewCount(Map paramMap) throws NkiaException {
		return (Integer) selectByPk("SlaManageIndicator.selectIndicatorCriticViewCount", paramMap);
	}
	public List indicatorCalChart(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.indicatorCalChart", paramMap);
	}
	public List indicatorCalChartPerMonth(Map paramMap) throws NkiaException {
		return list("SlaManageIndicator.indicatorCalChartPerMonth", paramMap);
	}
	public Map indicatorCalHeaderPerMonth(Map paramMap) throws NkiaException {
		return (Map) selectByPk("SlaManageIndicator.indicatorCalHeaderPerMonth", paramMap);
	}
	public void updateCalIndicator(Map paramMap) throws NkiaException {
		selectByPk("SlaManageIndicator.updateCalIndicator", paramMap);
	}
	public Map procIndicatorLevelGet(Map paramMap) throws NkiaException {
		return (Map)selectByPk("SlaManageIndicator.procIndicatorLevelGet", paramMap);
	}
	public Map procSpSlaIndicatorValue(Map paramMap) throws NkiaException {
		return (Map)selectByPk("SlaManageIndicator.procSpSlaIndicatorValue", paramMap);
	}
	public Map procCheckValueCal(Map paramMap) throws NkiaException {
		return (Map)selectByPk("SlaManageIndicator.procCheckValueCal", paramMap);
	}
	public void procExecSubtoal(Map paramMap) throws NkiaException {
		selectByPk("SlaManageIndicator.procExecSubtoal", paramMap);
	}
	
	// 20200221_이원재_003_일배치 스케줄러 전용 프로시저 실행 등록
	public void procSpSlaExecDaily() throws NkiaException {
		selectByPk("SlaManageIndicator.procSpSlaExecDaily", null);
	}
	// 20200221_이원재_003_일배치 스케줄러 전용 프로시저 실행 등록 END
	
	public void updateIndicatorFreezing(Map paramMap) throws NkiaException {
		update("SlaManageIndicatorFreezing.updateIndicatorFreezing", paramMap);
	}
	public Map selectIndicatorFreezing(Map paramMap) throws NkiaException {
		return (Map)selectByPk("SlaManageIndicatorFreezing.selectIndicatorFreezing", paramMap);
	}
	
	// 20200205_이원재_001_신규추가
	public List selectFreezing(Map paramMap) throws NkiaException {
		return list("SlaManageIndicatorFreezing.executeQuery", paramMap);
	}
	public Map selectFreezingViewHeader(Map paramMap) throws NkiaException {
		return (Map)selectByPk("SlaManageIndicatorFreezing.selectFreezingViewHeader", paramMap);
	}
	public List selectFreezingView(Map paramMap) throws NkiaException {
		return list("SlaManageIndicatorFreezing.selectFreezingView", paramMap);
	}
	public List selectFreezingCustomerMap() throws NkiaException {
		return list("SlaManage.selectFreezingCustomerMap", null);
	}
	// 20200205_이원재_001_신규추가 END
	
	// 20200217_이원재_003_신규추가
	public List searchExecLogList(Map paramMap) throws NkiaException {
		return this.list("SlaManageIndicatorFreezing.searchExecLogList", paramMap);
	}
	public int searchExecLogCount(Map paramMap) throws NkiaException {
		return (Integer) this.selectByPk("SlaManageIndicatorFreezing.searchExecLogCount", paramMap);
	}
	// 20200217_이원재_003_신규추가 END
	
	
	
	public Map selectFreezingQuery(Map paramMap) throws NkiaException {
		return (Map) selectByPk("SlaManageIndicatorFreezing.selectFreezingQuery", paramMap);
	}
	public void updateFreezingQuery(Map paramMap) throws NkiaException {
		update("SlaManageIndicatorFreezing.updateFreezingQuery", paramMap);
	}
	public List selectViewColumnsList(Map paramMap) throws NkiaException {
		return list("SlaManageIndicatorFreezing.selectViewColumnsList", paramMap);
	}
	public int selectViewColumnsCount(Map paramMap) throws NkiaException {
		return (Integer) selectByPk("SlaManageIndicatorFreezing.selectViewColumnsCount", paramMap);
	}
	public void updateViewColumnsComment(Map paramMap) throws NkiaException {
		update("SlaManageIndicatorFreezing.updateViewColumnsComment", paramMap);
	}
	public void createOrReplaceView(Map paramMap) throws NkiaException {
		update("SlaManageIndicatorFreezing.createOrReplaceView", paramMap);
	}
	public void procViewComment(Map paramMap) throws NkiaException {
		selectByPk("SlaManageIndicatorFreezing.procViewComment", paramMap);
	}
}
