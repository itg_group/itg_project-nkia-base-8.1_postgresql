package com.nkia.itg.sla.manage.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.sla.manage.service.SlaManageService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class SlaIndicatorDetailController {
	@Autowired
	private SlaManageService slaManageService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * SLA 정보 관리
	 * 계약 대상별 관리 지표 수준 설정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicatorDetail.do")
	public String indicatorDetail(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", true);
		return "/itg/sla/manage/indicatorDetail.nvf";
	}
	/**
	 * SLA 정보 관리
	 * 계약 대상별 관리 지표 수준 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicatorDetailView.do")
	public String indicatorDetailView(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", false);
		return "/itg/sla/manage/indicatorDetail.nvf";
	}
	/**
	 * SLA 정보 관리
	 * 관리지표 등록/산출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicatorCal.do")
	public String indicatorCal(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", true);
		return "/itg/sla/manage/indicatorCal.nvf";
	}
	/**
	 * SLA 정보 관리
	 * 근거자료 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicatorRawDataMain.do")
	public String indicatorRawDataMain(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", false);
		return "/itg/sla/manage/indicatorRawDataMain.nvf";
	}
	@RequestMapping(value="/itg/sla/manage/indicatorRawData.do")
	public String indicatorRawData(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", false);
		return "/itg/sla/manage/indicatorRawData.nvf";
	}
	
	@RequestMapping(value="/itg/sla/manage/indicatorRawViewMain.do")
	public String indicatorRawViewMain(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", false);
		return "/itg/sla/manage/indicatorRawViewMain.nvf";
	}
	@RequestMapping(value="/itg/sla/manage/indicatorRawView.do")
	public String indicatorRawView(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", false);
		return "/itg/sla/manage/indicatorRawView.nvf";
	}
	
	/**
	 * 20200206_이원재_001_신규추가
	 * SLA 정보 관리
	 * 임계치 조회
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicatorCriticView.do")
	public String indicatorCriticView(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		return "/itg/sla/manage/indicatorCriticView.nvf";
	}
	
	/**
	 * 20200217_이원재_002_신규추가
	 * 지표산출 > 작업결과 조회
	 * @param request
	 * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/sla/manage/execLog.do")
	public String execLog(HttpServletRequest request, ModelMap paramMap) {
		return "/itg/sla/manage/execLog.nvf";
	}
	
	@RequestMapping(value="/itg/sla/manage/emsEventInterface.do")
	public String emsEventInterface(HttpServletRequest request, ModelMap paramMap) {
		return "/itg/sla/manage/emsEventInterface.nvf";
	}
}
