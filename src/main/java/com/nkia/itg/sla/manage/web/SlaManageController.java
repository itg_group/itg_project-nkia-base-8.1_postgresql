package com.nkia.itg.sla.manage.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.sla.manage.service.SlaManageIndicatorService;
import com.nkia.itg.sla.manage.service.SlaManageService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * @author 송종환
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
@Controller
public class SlaManageController {

	@Autowired
	private SlaManageService slaManageService;
	
	@Autowired
	private SlaManageIndicatorService slaManageIndicatorService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * SLA 정보 관리
	 * 계약대상 설정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/contract.do")
	public String postMetric(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String edit = request.getParameter("edit") == null ? "true" : request.getParameter("edit");
		
		if(edit.equals("") || !(edit.equals("false") || edit.equals("true"))) edit = "true";
		
		paramMap.put("edit", edit);
		return "/itg/sla/manage/contract.nvf";
	}
	
	/* 20200120_이원재_001_컨트롤러 새로운 페이지 매핑 추가 */
	@RequestMapping(value="/itg/sla/manage/contractNew.do")
	public String contractNew(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		return "/itg/sla/manage/contractNew.nvf";
	}
	
	/**
	 * SLA 정보 관리
	 * 계약대상 TREE
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/searchContractAllTree.do")
	public @ResponseBody ResultVO searchContractAllTree(@RequestBody  ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = slaManageService.searchContractAllTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/searchContractAllTreeByCustomer.do")
	public @ResponseBody ResultVO searchContractAllTreeByCustomer(@RequestBody  ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = slaManageService.searchContractAllTreeByCustomer(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * SLA 정보 관리
	 * 업체정보 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/insertCustomer.do")
	public @ResponseBody ResultVO insertCustomer(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			boolean isPass = slaManageService.insertCustomer(paramMap);
			resultVO.setSuccess(true);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014"));
			}
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/deleteCustomer.do")
	public @ResponseBody ResultVO deleteCustomer(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			boolean isPass = slaManageService.deleteCustomer(paramMap) > 0;
			resultVO.setSuccess(isPass);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectCustomer.do")
	public @ResponseBody ResultVO selectCustomer(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = new ArrayList();
			Map limitMap = null;
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			limitMap = slaManageService.selectCustomer(paramMap);
			totalCount = slaManageService.selectCustomerPointCount(paramMap);
			resultList = slaManageService.selectCustomerPointList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
			resultVO.setResultMap(limitMap);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * SLA 정보 관리
	 * 계약정보 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/selectContract.do")
	public @ResponseBody ResultVO selectContract(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) slaManageService.selectContract(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * SLA 정보 관리
	 * 계약정보 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/insertContract.do")
	public @ResponseBody ResultVO insertContract(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			int rs = slaManageService.vaildContractDatePeriod(paramMap);
			
			if(rs == 0) {
				boolean isPass = slaManageService.insertContract(paramMap);
				resultVO.setSuccess(isPass);
				if(isPass) {
					// 등록 성공
					resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
				}
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultInt(rs);
				resultVO.setResultMsg("기존에 등록된 계약기간이 존재 합니다.");
			}
			
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * SLA 정보 관리
	 * 계약정보 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/updateContract.do")
	public @ResponseBody ResultVO updateContract(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			int rs = slaManageService.vaildContractDatePeriod(paramMap);
			
			if(rs == 0) {
				boolean isPass = slaManageService.updateContract(paramMap);
				resultVO.setSuccess(isPass);
				if(isPass) {
					// 등록 성공
					resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
				}
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultInt(rs);
				resultVO.setResultMsg("기존에 등록된 계약기간이 존재 합니다.");
			}
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * SLA 정보 관리
	 * 계약정보 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/selectContractList.do")
	public @ResponseBody ResultVO selectContractList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = slaManageService.selectContractListCount(paramMap);
				resultList = slaManageService.selectContractList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectLastContract.do")
	public @ResponseBody ResultVO selectLastContract(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			Map contractMap = slaManageService.selectLastContract(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMap(contractMap != null? contractMap: null);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectPopupIndicatorList.do")
	public @ResponseBody ResultVO selectPopupIndicatorList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = slaManageService.selectPopupIndicatorListCount(paramMap);
				resultList = slaManageService.selectPopupIndicatorList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectContractIndicatorList.do")
	public @ResponseBody ResultVO selectContractIndicatorList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			// PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = slaManageService.selectContractIndicatorListCount(paramMap);
				resultList = slaManageService.selectContractIndicatorList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			// gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	@RequestMapping(value="/itg/sla/manage/selectContractIndicatorTotalList.do")
	public @ResponseBody ResultVO selectContractIndicatorTotalList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = slaManageService.selectContractIndicatorTotalListCount(paramMap);
				resultList = slaManageService.selectContractIndicatorTotalList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/insertContractIndicator.do")
	public @ResponseBody ResultVO insertContractIndicator(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		
		List dataList = (List)paramMap.get("indicatorRemoveList");
		for (int i=0; i<dataList.size(); i++) {
			Map map = (Map)dataList.get(i);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/searchContractIndicatorExcel.do")
	public @ResponseBody ResultVO searchContractIndicatorExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					List tempArrayList = slaManageService.searchContractIndicatorExcel(param);
					
					for(int count=0 ; count<3 ; count++) {
						Map emptyLineItem = new HashMap();
						Iterator keyItr = ((Map) tempArrayList.get(0)).keySet().iterator();
						while(keyItr.hasNext()) {
							String key = (String) keyItr.next();
							emptyLineItem.put(key, " ");
						}
						tempArrayList.add(emptyLineItem);
					}
					
					param.put("first_num", 1);
					param.put("last_num", 1);
					List tempTotalList = slaManageService.selectContractIndicatorTotalList(param);
					if(tempTotalList != null && tempTotalList.size() > 0) {
						Map tempTotalItem = (Map) tempTotalList.get(0);
						Map totalLineMap = new HashMap();
						totalLineMap.put(columnsAttrs[0], "당월산출점수");
						totalLineMap.put(columnsAttrs[1], "목표수준");
						totalLineMap.put(columnsAttrs[2], "최소수준");
						totalLineMap.put(columnsAttrs[3], "평가등급");
						for(int count=4 ; count<columnsAttrs.length ; count++) {
							totalLineMap.put(columnsAttrs[count], "");
						}
						tempArrayList.add(totalLineMap);
						
						totalLineMap = new HashMap();
						totalLineMap.put(columnsAttrs[0], tempTotalItem.get("CALCULATOR_VALUE"));
						totalLineMap.put(columnsAttrs[1], tempTotalItem.get("TARGET_LEVEL"));
						totalLineMap.put(columnsAttrs[2], tempTotalItem.get("MINIMUM_LEVEL"));
						totalLineMap.put(columnsAttrs[3], tempTotalItem.get("CALCULATOR_LEVEL"));
						for(int count=4 ; count<columnsAttrs.length ; count++) {
							totalLineMap.put(columnsAttrs[count], "");
						}
						tempArrayList.add(totalLineMap);
					}
					
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectContractCalList.do")
	public @ResponseBody ResultVO selectContractCalList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = slaManageService.selectContractCalListCount(paramMap);
				resultList = slaManageService.selectContractCalList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	// 20200204_이원재_001_신규추가
	@RequestMapping(value="/itg/sla/manage/insertCustomerPoint.do")
	public @ResponseBody ResultVO insertCustomerPoint(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			boolean isPass = slaManageService.insertCustomerPoint(paramMap);
			resultVO.setSuccess(true);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014"));
			}
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	// 20200204_이원재_001_신규추가 END
	
	
	
	// 20200206_이원재_001_신규추가
	@RequestMapping(value="/itg/sla/manage/customerCal.do")
	public String customerCal(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		return "/itg/sla/manage/customerCal.nvf";
	}
	
	/**
	 * SLA 정보 관리
	 * 관리지표 등록/산출 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicatorCalView.do")
	public String indicatorCalView(ModelMap paramMap)throws NkiaException{
		paramMap.put("edit", false);
		return "/itg/sla/manage/indicatorCalView.nvf";
	}
	// 20200206_이원재_001_신규추가 END
	
	// 20200207_이원재_001_신규추가
	@RequestMapping(value="/itg/sla/manage/selectIndncatorCalView.do")
	public @ResponseBody ResultVO indicatorCalViewSearch(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			resultList = slaManageService.selectIndicatorCalViewList(paramMap);
			totalCount = slaManageService.selectIndicatorCalViewCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	// 20200207_이원재_001_신규추가 END
	// 20200227_이원재_001_이원화
	@RequestMapping(value="/itg/sla/manage/selectIndncatorCalViewByCustomer.do")
	public @ResponseBody ResultVO indicatorCalViewSearchByCustomer(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			resultList = slaManageService.selectIndicatorCalViewByCustomerList(paramMap);
			totalCount = slaManageService.selectIndicatorCalViewByCustomerCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	// 20200227_이원재_001_이원화 END
	
	// 20200317_이원재_001_신규작성
	@RequestMapping(value="/itg/sla/manage/selectContractIndicator.do")
	public @ResponseBody ResultVO selectContractIndicator(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			Map resultMap = slaManageService.selectContractIndicator(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMap(resultMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	// 20200317_이원재_001_신규작성 END
}
