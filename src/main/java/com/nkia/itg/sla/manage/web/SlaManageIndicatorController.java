package com.nkia.itg.sla.manage.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.sla.manage.service.SlaManageIndicatorService;
import com.nkia.itg.sla.manage.service.SlaManageService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * @author 송종환
 *
 */
@Controller
public class SlaManageIndicatorController {

	@Autowired
	private SlaManageService slaManageService;
	
	@Autowired
	private SlaManageIndicatorService slaManageIndicatorService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * SLA 정보 관리
	 * 관리지표 설정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicator.do")
	public String postMetric(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String edit = request.getParameter("edit") == null ? "true" : request.getParameter("edit");
		
		if(edit.equals("") || !(edit.equals("false") || edit.equals("true"))) edit = "true";
		
		paramMap.put("edit", edit);
		return "/itg/sla/manage/indicator.nvf";
	}
	
	/**
	 * SLA 정보 관리
	 * 관리지표 TREE
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/searchIndicatorAllTree.do")
	public @ResponseBody ResultVO searchIndicatorAllTree(@RequestBody  ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = slaManageIndicatorService.searchIndicatorAllTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/insertIndicatorGroup.do")
	public @ResponseBody ResultVO insertIndicatorGroup(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			boolean isPass = slaManageIndicatorService.insertIndicatorGroup(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	@RequestMapping(value="/itg/sla/manage/insertIndicatorDetail.do")
	public @ResponseBody ResultVO insertIndicatorDetail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			boolean isPass = slaManageIndicatorService.insertIndicatorDetail(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectIndicatorDetailList.do")
	public @ResponseBody ResultVO selectIndicatorDetailList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			gridVO.setRows(slaManageIndicatorService.selectIndicatorDetailList(paramMap));
			gridVO.setTotalCount(slaManageIndicatorService.selectIndicatorDetailCount(paramMap));
			
			resultVO.setGridVO(gridVO);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectIndicatorGroup.do")
	public @ResponseBody ResultVO selectIndicatorGroup(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) slaManageIndicatorService.selectIndicatorGroup(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectIndicatorDetail.do")
	public @ResponseBody ResultVO selectIndicatorDetail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) slaManageIndicatorService.selectIndicatorDetail(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateIndicatorGroup.do")
	public @ResponseBody ResultVO updateIndicatorGroup(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			boolean isPass = slaManageIndicatorService.updateIndicatorGroup(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	@RequestMapping(value="/itg/sla/manage/updateIndicatorDetail.do")
	public @ResponseBody ResultVO updateIndicatorDetail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			boolean isPass = slaManageIndicatorService.updateIndicatorDetail(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateIndicatorCalculator.do")
	public @ResponseBody ResultVO updateIndicatorCalculator(@RequestBody ModelMap paramMap)throws NkiaException {
		System.err.println("updateIndicatorCalculator paramMap: " + paramMap.toString());
		ResultVO resultVO = new ResultVO();
		
		try{
			
			boolean isPass = slaManageIndicatorService.updateIndicatorCalculator(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateContractIndicator.do")
	public @ResponseBody ResultVO updateContractIndicator(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			boolean isPass = slaManageIndicatorService.updateContractIndicator(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
				
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	@RequestMapping(value="/itg/sla/manage/updateContractIndicatorCal.do")
	public @ResponseBody ResultVO updateContractIndicatorCal(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			boolean isPass = slaManageIndicatorService.updateContractIndicatorCal(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateCalIndicatorClose.do")
	public @ResponseBody ResultVO updateCalIndicatorClose(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			boolean isPass = slaManageIndicatorService.updateCalIndicatorClose(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateCalIndicator.do")
	public @ResponseBody ResultVO updateCalIndicator(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			slaManageIndicatorService.updateCalIndicator(paramMap);
			boolean isPass = true;
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateContractIndicatorCalDetail.do")
	public @ResponseBody ResultVO updateContractIndicatorCalDetail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			//PagingUtil.getFristEndNum(paramMap);
			paramMap.put("first_num", 1);
			paramMap.put("last_num", 100);
			
			if(!initFlag) {
				resultList = slaManageIndicatorService.updateContractIndicatorCalDetail(paramMap);
				totalCount = resultList.size();
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * SLA 정보 관리
	 * 산출결과 추이
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/indicatoCalChartr.do")
	public String indicatoCalChartr(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		return "/itg/sla/manage/indicatorCalChart.nvf";
	}
	
	@RequestMapping(value="/itg/sla/manage/indicatorGroupCombo.do")
	public @ResponseBody ResultVO indicatorGroupCombo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(slaManageIndicatorService.indicatorGroupCombo(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/indicatorCombo.do")
	public @ResponseBody ResultVO indicatorCombo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(slaManageIndicatorService.indicatorCombo(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/indicatorCalChart.do")
	public @ResponseBody ResultVO indicatorCalChart(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = slaManageIndicatorService.indicatorCalChart(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/indicatorCalChartPerMonth.do")
	public @ResponseBody ResultVO indicatorCalChartPerMonth(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = slaManageIndicatorService.indicatorCalChartPerMonth(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/indicatorCalHeaderPerMonth.do")
	public @ResponseBody ResultVO indicatorCalHeaderPerMonth(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			Map map = slaManageIndicatorService.indicatorCalHeaderPerMonth(paramMap);
			
			resultVO.setResultMap(map);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/procIndicatorLevelGet.do")
	public @ResponseBody ResultVO procIndicatorLevelGet(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			Map result = slaManageIndicatorService.procIndicatorLevelGet(paramMap);
			resultVO.setResultMap(result);
			boolean isPass = true;
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/procSlaIndicatorValue.do")
	public @ResponseBody ResultVO procSlaIndicatorValue(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			Map result = slaManageIndicatorService.procSpSlaIndicatorValue(paramMap);
			
			resultVO.setResultMap(paramMap);
			boolean isPass = true;
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/procCheckValueCal.do")
	public @ResponseBody ResultVO procCheckValueCal(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			Map result = slaManageIndicatorService.procCheckValueCal(paramMap);
			resultVO.setResultMap(result);
			boolean isPass = true;
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectIndicatorCriticViewList.do")
	public @ResponseBody ResultVO selectIndicatorCriticViewList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			resultList = slaManageIndicatorService.selectIndicatorCriticViewList(paramMap);

			totalCount = slaManageIndicatorService.selectIndicatorCriticViewCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/indicatorCalChartPerCustomer.do")
	public String indicatorCalChartPerCustomer(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		return "/itg/sla/manage/indicatorCalChartPerCustomer.nvf";
	}
	
	// 20200924_이원재_001_신규작성
	
	@RequestMapping(value="/itg/sla/manage/rawview.do")
	public String rawview(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		return "/itg/sla/manage/rawview.nvf";
	}
	
	// 20200924_이원재_001_신규작성_END
}
