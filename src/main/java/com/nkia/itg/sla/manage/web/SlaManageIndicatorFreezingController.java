package com.nkia.itg.sla.manage.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.intergration.polestar.job.service.ItgSmsJobService;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.sla.manage.service.SlaManageIndicatorFreezingService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class SlaManageIndicatorFreezingController {
	@Autowired
	private SlaManageIndicatorFreezingService slaManageIndicatorFreezingService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name="egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="itgSmsJobService")
	private ItgSmsJobService itgSmsJobService;
	
	@RequestMapping(value="/itg/sla/manage/updateIndicatorFreezing.do")
	public @ResponseBody ResultVO updateIndicatorFreezing(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		Boolean isAuthenticated = null;
		
		try {
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
			
			slaManageIndicatorFreezingService.updateIndicatorFreezing(paramMap);
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectIndicatorFreezing.do")
	public @ResponseBody ResultVO selectIndicatorFreezing(@RequestBody ModelMap paramMap) throws NkiaException{
		
		ResultVO resultVO = new ResultVO();
		Boolean isAuthenticated = null;
		
		try {
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				Map resultMap = slaManageIndicatorFreezingService.selectIndicatorFreezing(paramMap);
				resultVO.setResultMap(resultMap);
				resultVO.setSuccess(true);
			}
		} catch(Exception e) {
			resultVO.setSuccess(false);
			
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
	
	/**
	 * 20200205_이원재_001_신규추가 (서비스수준관리>지표산출>근거자료 조회)
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sla/manage/searchIndicatorRawData.do")
	public @ResponseBody ResultVO searchIndicatorRawData(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			List rawDataList = slaManageIndicatorFreezingService.selectFreezing(paramMap);
			
			gridVO.setRows(rawDataList);
			gridVO.setTotalCount(rawDataList.size());
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectFreezingViewHeader.do")
	public @ResponseBody ResultVO selectFreezingViewHeader(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			Map gridHeaderMap = slaManageIndicatorFreezingService.selectFreezingViewHeader(paramMap);
			
			resultVO.setResultMap(gridHeaderMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/searchIndicatorRawView.do")
	public @ResponseBody ResultVO searchIndicatorRawView(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			List rawDataList = slaManageIndicatorFreezingService.searchFreezingView(paramMap);
			
			gridVO.setRows(rawDataList);
			gridVO.setTotalCount(rawDataList.size());
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/searchIndicatorRawDataExcel.do")
	public @ResponseBody ResultVO searchIndicatorRawDataExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			List tempArrayList = slaManageIndicatorFreezingService.selectFreezing(param);
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/searchIndicatorRawViewExcel.do")
	public @ResponseBody ResultVO searchIndicatorRawViewExcel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			ModelMap excelMap = new ModelMap();
			ArrayList resultArray = new ArrayList();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			Map excelAttrs = (Map) excelParam.get("excelAttrs");
			List<HashMap> sheetList = (List<HashMap>) excelAttrs.get("sheet");
			List<HashMap> newSheetList = new ArrayList<HashMap>();
			
			List tempArrayList = slaManageIndicatorFreezingService.searchFreezingView(param);
			
			for (int k = 0; k < sheetList.size(); k++) {
				HashMap sheetMap = sheetList.get(k);
				String columns = (String) sheetMap.get("includeColumns");
				String htmlTypes = (String) sheetMap.get("htmlTypes");
				String popupShowTypes = (String) sheetMap.get("popupShowTypes");
				
				if(null != htmlTypes){
					
					String[] columnsAttrs = columns.split(",");
					String[] htmlTypeAttrs = htmlTypes.split(",");
					String[] popupShowTypeAttrs = popupShowTypes.split(",");
					
					String columnList = "";
					String newColumnList = "";
					int size = columnsAttrs.length;
					
					for(int j=0; j <size; j++){
						String column = (String) columnsAttrs[j];
						String htmlType = (String) htmlTypeAttrs[j];
						String popupShowType = (String) popupShowTypeAttrs[j];
						
						String columnNm = "".equals(columnList)? "": ","; 
						columnNm += column;
						columnList += columnNm;
					}
					
					param.put("col_id", columnList);
					
					UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
					if(userVO != null){
						param.put("loginCustId", userVO.getCust_id());
						param.put("user_id", userVO.getUser_id());
					}
					resultArray.add(tempArrayList);
					
					// 위의 if문에서 검사하는 컬럼명들은 따로 앞에 붙여줘야 한다.
					newColumnList = columnList;
					
					sheetMap.remove("includeColumns");
					sheetMap.put("includeColumns", newColumnList);
					
					newSheetList.add(sheetMap);
					
				} 
				else {
					resultArray.add(null);
				}
			}
			
			excelAttrs.remove("sheet");
			excelParam.remove("excelParam");
			
			excelAttrs.put("sheet", newSheetList);
			excelParam.put("excelParam", excelAttrs);

			excelMap = excelMaker.makeExcelFile(excelParam, resultArray);
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		} 
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/searchExecLogList.do")
	public @ResponseBody ResultVO searchExecLogList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			PagingUtil.getFristEndNum(paramMap);
			
			List execLogList = slaManageIndicatorFreezingService.searchExecLogList(paramMap);
			int execLogCount = slaManageIndicatorFreezingService.searchExecLogCount(paramMap);
			
			gridVO.setTotalCount(execLogCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(execLogList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	// 20200326_이원재_001_신규작성
	@RequestMapping(value="/itg/sla/manage/selectEmsMessageList.do")
	public @ResponseBody ResultVO selectEmsMessageList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			PagingUtil.getFristEndNum(paramMap);
			
			List emsMessageList = itgSmsJobService.selectEmsMessageList(paramMap);
			int emsMessageCount = itgSmsJobService.selectEmsMessageCount(paramMap);
			
			gridVO.setTotalCount(emsMessageCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(emsMessageList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	// 20200326_이원재_001_신규작성 END
	
	
	
	@RequestMapping(value="/itg/sla/manage/selectFreezingQuery.do")
	public @ResponseBody ResultVO selectFreezingQuery(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			Map freeingQueryMap = slaManageIndicatorFreezingService.selectFreezingQuery(paramMap);
			
			resultVO.setResultMap(freeingQueryMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateFreezingQuery.do")
	public @ResponseBody ResultVO updateFreezingQuery(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			slaManageIndicatorFreezingService.updateFreezingQuery(paramMap);
			
			resultVO.setResultBoolean(true);
			resultVO.setResultMsg("수정되었습니다.");
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/selectViewColumns.do")
	public @ResponseBody ResultVO selectViewColumns(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			PagingUtil.getFristEndNum(paramMap);
			
			List viewColumnsList = slaManageIndicatorFreezingService.selectViewColumnsList(paramMap);
			int viewColumnsCount = slaManageIndicatorFreezingService.selectViewColumnsCount(paramMap);
			
			gridVO.setTotalCount(viewColumnsCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(viewColumnsList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/updateViewColumnsComment.do")
	public @ResponseBody ResultVO updateViewColumnsComment(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			List paramList = (List) paramMap.get("columns");
			slaManageIndicatorFreezingService.updateViewColumnsComment(paramList);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sla/manage/createOrReplaceView.do")
	public @ResponseBody ResultVO createOrReplaceView(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			slaManageIndicatorFreezingService.createOrReplaceView(paramMap);
			
			resultVO.setResultBoolean(true);
			resultVO.setResultMsg("적용되었습니다.");
		} catch(Exception e) {
			if(e.getMessage().indexOf("ORA-") > -1) {
				resultVO.setResultBoolean(false);
				resultVO.setResultMsg(e.getMessage().substring(e.getMessage().indexOf("ORA-")));
			} else {
				throw new NkiaException(e);
			}
		}
		
		return resultVO;
	}
}