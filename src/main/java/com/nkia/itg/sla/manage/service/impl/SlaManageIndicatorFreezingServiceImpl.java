package com.nkia.itg.sla.manage.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.sla.manage.dao.SlaManageDAO;
import com.nkia.itg.sla.manage.service.SlaManageIndicatorFreezingService;
import com.nkia.itg.system.code.dao.CommonCodeDAO;

@SuppressWarnings({ "rawtypes" })
@Service("SlaManageIndicatorFreezingService")
public class SlaManageIndicatorFreezingServiceImpl implements SlaManageIndicatorFreezingService {
	@Autowired private SlaManageDAO slaManageDAO;
	@Autowired private CommonCodeDAO commonCodeDao;
	
	@Override
	public void updateIndicatorFreezing(Map paramMap) throws NkiaException {
		slaManageDAO.updateIndicatorFreezing(paramMap);
	}
	
	@Override
	public Map selectIndicatorFreezing(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorFreezing(paramMap);
	}
	
	@Override
	public List selectFreezing(Map paramMap) throws NkiaException {
		List result = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		Map indicator = slaManageDAO.selectIndicatorDetail(paramMap);
		
		String query = (String) indicator.get("RAW_DATA_QUERY");
		query = query.replace(":P_CUSTOMER_ID", "'" + (String) paramMap.get("customer_id") + "'")
					 .replace(":P_APPLY_YM", "'" + (String) paramMap.get("apply_ym") + "'")
					 .replaceAll(";", "");
		
		paramMap.put("query", query);
		result = slaManageDAO.selectFreezing(paramMap);
		
		for(int count=0 ; count<result.size() ; count++) {
			Map item = (Map) result.get(count);
			item.put("RNUM", 			(count + 1) + "");
			try { item.put("REG_DT", 			sdf.format((Date) item.get("REG_DT")));			} catch(Exception e) {}
			try { item.put("NOTICE_DT", 		sdf.format((Date) item.get("NOTICE_DT")));		} catch(Exception e) {}
			try { item.put("OCCRRNC_DT", 		sdf.format((Date) item.get("OCCRRNC_DT")));		} catch(Exception e) {}
			try { item.put("END_DUE_DT", 		sdf.format((Date) item.get("END_DUE_DT")));		} catch(Exception e) {}
			try { item.put("REC_DT", 			sdf.format((Date) item.get("REC_DT")));			} catch(Exception e) {}
			try { item.put("PLAN_REG_DT", 		sdf.format((Date) item.get("PLAN_REG_DT")));	} catch(Exception e) {}
			try { item.put("PLAN_STR_DT", 		sdf.format((Date) item.get("PLAN_STR_DT")));	} catch(Exception e) {}
			try { item.put("PLAN_END_DT", 		sdf.format((Date) item.get("PLAN_END_DT")));	} catch(Exception e) {}
			try { item.put("WORK_STR_DT", 		sdf.format((Date) item.get("WORK_STR_DT")));	} catch(Exception e) {}
			try { item.put("WORK_END_DT", 		sdf.format((Date) item.get("WORK_END_DT")));	} catch(Exception e) {}
			try { item.put("WORK_REG_DT", 		sdf.format((Date) item.get("WORK_REG_DT")));	} catch(Exception e) {}
			try { item.put("INS_DATE", 			sdf.format((Date) item.get("INS_DATE")));		} catch(Exception e) {}
		}
		
		return result;
	}
	
	@Override
	public Map selectFreezingViewHeader(Map paramMap) throws NkiaException {
		return slaManageDAO.selectFreezingViewHeader(paramMap);
	}
	
	@Override
	public List searchFreezingView(Map paramMap) throws NkiaException {
		ModelMap codeParamMap = new ModelMap();
		codeParamMap.put("code_grp_id", "SLA_FREEZING_TYPE");
		codeParamMap.put("code_id", paramMap.get("base_data_type"));
		Map codeMap = commonCodeDao.searchCodeDetail(codeParamMap);
		
		// 공통코드(SLA_FREEZING_TYPE)의 CODE_DESC에 등록된 VIEW 명 조회
		paramMap.put("viewname", codeMap.get("CODE_DESC"));
		if(codeMap.get("CODE_DESC") == null || String.valueOf(codeMap.get("CODE_DESC")) == "") {
			paramMap.put("viewname", "dual");
			paramMap.remove("customer_id");
			paramMap.remove("apply_ym");
		}
		
		/*paramMap.put("viewname", codeMap.get("CODE_NM_EN"));
		if(codeMap.get("CODE_NM_EN") == null || String.valueOf(codeMap.get("CODE_NM_EN")) == "") {
			paramMap.put("viewname", "dual");
			paramMap.remove("customer_id");
			paramMap.remove("apply_ym");
		}
		*/
		return slaManageDAO.selectFreezingView(paramMap);
	};
	
	@Override
	public List searchExecLogList(Map paramMap) throws NkiaException {
		return slaManageDAO.searchExecLogList(paramMap);
	}
	
	@Override
	public int searchExecLogCount(Map paramMap) throws NkiaException {
		return slaManageDAO.searchExecLogCount(paramMap);
	}
	
	
	
	@Override
	public Map selectFreezingQuery(Map paramMap) throws NkiaException {
		return slaManageDAO.selectFreezingQuery(paramMap);
	}
	
	@Override
	public void updateFreezingQuery(Map paramMap) throws NkiaException {
		slaManageDAO.updateFreezingQuery(paramMap);
	}
	
	@Override
	public List selectViewColumnsList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectViewColumnsList(paramMap);
	}
	
	@Override
	public int selectViewColumnsCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectViewColumnsCount(paramMap);
	}
	
	@Override
	public void updateViewColumnsComment(List paramList) throws NkiaException {
		for(Object paramItem: paramList) {
			Map paramMap = (Map) paramItem;
			
			String query = String.format(
					"COMMENT ON COLUMN %s.%s IS '%s'", 
					String.valueOf(paramMap.get("TABLE_NAME")), 
					String.valueOf(paramMap.get("COLUMN_NAME")), 
					String.valueOf(paramMap.get("COLUMN_COMMENTS"))
			);
			paramMap.put("query", query);
			
			slaManageDAO.updateViewColumnsComment(paramMap);
		}
	}
	
	@Override
	public void createOrReplaceView(Map paramMap) throws NkiaException {
		String baseDataType = String.valueOf(paramMap.get("base_data_type"));
		
		paramMap.put("freezing_type_cd", baseDataType);
		paramMap = selectFreezingQuery(paramMap);
		
		Map queryMap = new HashMap();
		String query = String.valueOf(paramMap.get("view_query"));
		
		if(query.indexOf(";") > -1) {
			query = query.substring(0, query.indexOf(";"));
		}
		
		queryMap.put("query", query);
		queryMap.put("p_freezing_type", baseDataType);
		queryMap.put("p_work_type", "IN");
		
		slaManageDAO.procViewComment(queryMap);
		slaManageDAO.createOrReplaceView(queryMap);
		
		queryMap.put("p_work_type", "OUT");
		slaManageDAO.procViewComment(queryMap);
	}
}