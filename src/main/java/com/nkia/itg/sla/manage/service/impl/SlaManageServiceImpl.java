package com.nkia.itg.sla.manage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.sla.manage.dao.SlaManageDAO;
import com.nkia.itg.sla.manage.service.SlaManageService;
import com.nkia.itg.system.customer.dao.CustomerDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Service("SlaManageService")
public class SlaManageServiceImpl implements SlaManageService {

	@Autowired
	private SlaManageDAO slaManageDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Override
	public List searchContractAllTree(Map param) throws NkiaException {
		return slaManageDAO.searchContractAllTree(param);
	}
	
	@Override
	public List searchContractAllTreeByCustomer(Map param) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if(isAuthenticated) {
			UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
			param.put("user_id", userVO.getUser_id());
		}
		
		
		return slaManageDAO.searchContractAllTreeByCustomer(param);
	}

	@Override
	public boolean insertCustomer(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
    	ModelMap customerParam = new ModelMap();
    	customerParam.put("customer_id", paramMap.get("customer_id"));
    	Map customer = customerDAO.searchCustomerBasicView(customerParam);
    	
    	ModelMap customerGroupParam = new ModelMap();
    	customerGroupParam.put("customer_id", customer.get("UP_CUSTOMER_ID"));
    	Map customerGroup = customerDAO.searchCustomerBasicView(customerGroupParam);

    	customerGroup.put("customer_id", customerGroup.get("CUSTOMER_ID"));
    	customerGroup.put("customer_nm", customerGroup.get("CUSTOMER_NM"));
    	customerGroup.put("ins_user_id", paramMap.get("ins_user_id"));
    	slaManageDAO.insertCustomerGroup(customerGroup);
    	
    	paramMap.put("customer_id", paramMap.get("customer_id"));
    	paramMap.put("customer_type_cd", customerGroup.get("CUSTOMER_ID"));
    	paramMap.put("customer_name", ((String) paramMap.get("customer_name")).substring(((String) paramMap.get("customer_name")).lastIndexOf(">") + 1));
    	String id = slaManageDAO.insertCustomer(paramMap);
    	
    	if(id == null || id.equals("")) {
    		return false;
    	}else {
    		return true;
    	}
	}
	
	@Override
	public int deleteCustomer(Map paramMap) throws NkiaException {
		return slaManageDAO.deleteCustomer(paramMap);
	}
	
	@Override
	public Map selectCustomer(Map paramMap) throws NkiaException {
		return slaManageDAO.selectCustomer(paramMap);
	}

	@Override
	public Map selectContract(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContract(paramMap);
	}

	@Override
	public boolean insertContract(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
    	String CUSTOMER_ID = (String)paramMap.get("customer_id");
    	
    	String id = slaManageDAO.insertContract(paramMap);
    	
    	// 20200121_이원재_001_계약등록 페이지에서의 관리지표 등록 로직 주석화
    	/*List dataList = (List)paramMap.get("indicatorList");
		for (int i=0; i<dataList.size(); i++) {
			Map map = (Map)dataList.get(i);
			
			if(map.get("INDICATOR_GROUP_ID") != null && !map.get("INDICATOR_GROUP_ID").equals("")) {
				map.put("CUSTOMER_ID", CUSTOMER_ID);
				map.put("CONTRACT_ID", id);
				map.put("INS_USER_ID", paramMap.get("ins_user_id"));
				
				slaManageDAO.insertContractIndicator(map);
			}
		}*/
		// 20200121_이원재_001_계약등록 페이지에서의 관리지표 등록 로직 주석화 END
    	
    	// 20200212_이원재_002_계약등록 시 이전계약참조가 있을 경우 계약 지표 복사
    	if(paramMap.get("origin_contract_id") != null && !"".equals((String) paramMap.get("origin_contract_id"))) {
    		slaManageDAO.copyContractIndicator(paramMap);
    		
    		Map originMap = new HashMap();
    		originMap.put("contract_id", paramMap.get("origin_contract_id"));
    		originMap = slaManageDAO.selectContract(originMap);
    		
    		paramMap.put("target_level", 		originMap.get("TARGET_LEVEL"));
    		paramMap.put("minimum_level", 		originMap.get("MINIMUM_LEVEL"));
    		paramMap.put("weight", 				originMap.get("WEIGHT"));
    		paramMap.put("critical_score_a", 	originMap.get("CRITICAL_SCORE_A"));
    		paramMap.put("critical_score_b", 	originMap.get("CRITICAL_SCORE_B"));
    		paramMap.put("critical_score_c", 	originMap.get("CRITICAL_SCORE_C"));
    		paramMap.put("level_point_type", 	originMap.get("LEVEL_POINT_TYPE"));
    		paramMap.put("a_level_point", 		originMap.get("A_LEVEL_POINT"));
    		paramMap.put("b_level_point", 		originMap.get("B_LEVEL_POINT"));
    		paramMap.put("c_level_point", 		originMap.get("C_LEVEL_POINT"));
    		paramMap.put("d_level_point", 		originMap.get("D_LEVEL_POINT"));
    		
    		slaManageDAO.updateContract(paramMap);
    	}
    	// 20200212_이원재_002_계약등록 시 이전계약참조가 있을 경우 계약 지표 복사 END
    	
    	// 20200121_이원재_002_계약등록 시 첨부파일 연계로직 신규추가
    	if(paramMap.get("atch_file_id") != null && !("".equals(paramMap.get("atch_file_id")))) {
    		slaManageDAO.insertContractFile(paramMap);
    	}
    	// 20200121_이원재_002_계약등록 시 첨부파일 연계로직 신규추가 END
    	
    	if(id == null || id.equals("")) {
    		return false;
    	}else {
    		return true;
    	}
	}

	@Override
	public int selectContractListCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractListCount(paramMap);
	}

	@Override
	public List selectContractList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractList(paramMap);
	}
	
	@Override
	public Map selectLastContract(Map paramMap) throws NkiaException {
		return slaManageDAO.selectLastContract(paramMap);
	}

	@Override
	public boolean updateContract(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	
    	int id = slaManageDAO.updateContract(paramMap);
    	
    	slaManageDAO.deleteContractFile(paramMap);
    	if(paramMap.get("atch_file_id") != null) {
    		slaManageDAO.insertContractFile(paramMap);
    	}
    	
    	/*
		 * 20200131_이원재_001_지표등록기능 삭제 (계약등록 화면에서 지표관리기능을 취급하지 않음)
    	//지표삭제
    	String CUSTOMER_ID = (String)paramMap.get("customer_id");
    	String CONTRACT_ID = (String)paramMap.get("contract_id");
    	
    	List dataList = (List)paramMap.get("indicatorRemoveList");
		for (int i=0; i<dataList.size(); i++) {
			Map map = (Map)dataList.get(i);
			if(map.get("GRID_TYPE") != null && map.get("GRID_TYPE").equals("DELETE")) {
				map.put("CUSTOMER_ID", CUSTOMER_ID);
				map.put("CONTRACT_ID", CONTRACT_ID);
				map.put("INS_USER_ID", paramMap.get("ins_user_id"));
				
				slaManageDAO.deleteContractIndicator(map);
			}
		}
		
		//지표등록
		dataList = (List)paramMap.get("indicatorList");
		for (int i=0; i<dataList.size(); i++) {
			Map map = (Map)dataList.get(i);
			if(map.get("GRID_TYPE") != null && map.get("GRID_TYPE").equals("INSERT")) {
				map.put("CUSTOMER_ID", CUSTOMER_ID);
				map.put("CONTRACT_ID", CONTRACT_ID);
				map.put("INS_USER_ID", paramMap.get("ins_user_id"));
				
				slaManageDAO.insertContractIndicator(map);
			}
		}
		 * 20200131_이원재_001_지표등록기능 삭제 (계약등록 화면에서 지표관리기능을 취급하지 않음) END
    	 */
    	
    	if(id == 0) {
    		return false;
    	}else {
    		return true;
    	}
	}

	@Override
	public int vaildContractDatePeriod(Map paramMap) throws NkiaException {
		return slaManageDAO.vaildContractDatePeriod(paramMap);
	}

	@Override
	public List selectPopupIndicatorList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectPopupIndicatorList(paramMap);
	}

	@Override
	public int selectPopupIndicatorListCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectPopupIndicatorListCount(paramMap);
	}

	@Override
	public List selectContractIndicatorList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractIndicatorList(paramMap);
	}

	@Override
	public int selectContractIndicatorListCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractIndicatorListCount(paramMap);
	}
	
	@Override
	public List searchContractIndicatorExcel(Map paramMap) throws NkiaException {
		return slaManageDAO.searchContractIndicatorExcel(paramMap);
	}

	@Override
	public List selectContractCalList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractCalList(paramMap);
	}

	@Override
	public int selectContractCalListCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractCalListCount(paramMap);
	}

	@Override
	public List selectContractIndicatorTotalList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractIndicatorTotalList(paramMap);
	}

	@Override
	public int selectContractIndicatorTotalListCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractIndicatorTotalListCount(paramMap);
	}
	
	// 20200204_이원재_001_신규등록
	@Override
	public boolean insertCustomerPoint(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
		
    	List removeList = (List) paramMap.get("removeList");
		for(int count=0 ; count<removeList.size() ; count++) {
			Map removeItem = (Map) removeList.get(count);
			removeItem.put("customer_id", 	removeItem.get("CUSTOMER_ID"));
			removeItem.put("point_type", 	removeItem.get("POINT_TYPE"));
			
			slaManageDAO.deleteCustomerPoint(removeItem);
		}
		
		List pointList = (List) paramMap.get("pointMap");
		for(int count=0 ; count<pointList.size() ; count++) {
			Map pointMap = (Map) pointList.get(count);
			pointMap.put("customer_id", pointMap.get("CUSTOMER_ID"));
			pointMap.put("point_type", 	pointMap.get("POINT_TYPE"));
			pointMap.put("point_value", pointMap.get("POINT_VALUE"));
			pointMap.put("ins_user_id", pointMap.get("ins_user_id"));
			pointMap.put("upd_user_id", pointMap.get("upd_user_id"));
			
			slaManageDAO.insertCustomerPoint(pointMap);
		}
		
		return true;
	}
	
	@Override
	public List selectCustomerPointList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectCustomerPointList(paramMap);
	}
	
	@Override
	public int selectCustomerPointCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectCustomerPointCount(paramMap);
	}
	// 20200204_이원재_001_신규등록 END
	
	// 20200207_이원재_001_이원화
	@Override
	public List selectIndicatorCalViewList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorCalViewList(paramMap);
	}
	
	@Override
	public int selectIndicatorCalViewCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorCalViewCount(paramMap);
	}
	// 20200207_이원재_001_신규등록 END
	// 20200227_이원재_001_신규등록
	@Override
	public List selectIndicatorCalViewByCustomerList(Map paramMap) throws NkiaException {
		boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if(isAuthenticated) {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
		}
		
		return slaManageDAO.selectIndicatorCalViewByCustomerList(paramMap);
	}
	
	@Override
	public int selectIndicatorCalViewByCustomerCount(Map paramMap) throws NkiaException {
		boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if(isAuthenticated) {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("user_id", userVO.getUser_id());
		}
		
		return slaManageDAO.selectIndicatorCalViewByCustomerCount(paramMap);
	}
	// 20200227_이원재_001_이원화 END
	
	// 20200317_이원재_001_신규작성
	@Override
	public Map selectContractIndicator(Map paramMap) throws NkiaException {
		return slaManageDAO.selectContractIndicator(paramMap);
	}
	// 20200317_이원재_001_신규작성 END
	// 20200330_이원재_004_신규작성
	@Override
	public List selectEmsCustomerNmList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectEmsCustomerNmList(paramMap);
	}
	// 20200330_이원재_004_신규작성 END
}
