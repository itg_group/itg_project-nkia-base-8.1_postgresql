package com.nkia.itg.sla.manage.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

@SuppressWarnings({ "rawtypes" })
public interface SlaManageIndicatorService {
	List searchIndicatorAllTree(Map param) throws NkiaException;
	boolean insertIndicatorGroup(Map paramMap) throws NkiaException;
	boolean insertIndicatorDetail(Map paramMap) throws NkiaException;
	Map selectIndicatorGroup(Map paramMap) throws NkiaException;
	Map selectIndicatorDetail(Map paramMap) throws NkiaException;
	List selectIndicatorDetailList(Map paramMap) throws NkiaException;
	int selectIndicatorDetailCount(Map paramMap) throws NkiaException;
	List updateContractIndicatorCalDetail(Map paramMap) throws NkiaException;
	boolean updateIndicatorGroup(Map paramMap) throws NkiaException;
	boolean updateIndicatorDetail(Map paramMap) throws NkiaException;
	boolean updateIndicatorCalculator(Map paramMap) throws NkiaException;
	boolean updateContractIndicator(Map paramMap) throws NkiaException;
	boolean updateContractIndicatorCal(Map paramMap) throws NkiaException;
	boolean updateCalIndicatorClose(Map paramMap) throws NkiaException;
	void updateCalIndicator(Map paramMap) throws NkiaException;
	Map procIndicatorLevelGet(Map paramMap) throws NkiaException;
	Map procSpSlaIndicatorValue(Map paramMap) throws NkiaException;
	void procSpSlaExecDaily() throws NkiaException;
	Map procCheckValueCal(Map paramMap) throws NkiaException;
	List indicatorGroupCombo(Map paramMap) throws NkiaException;
	List indicatorCombo(Map paramMap) throws NkiaException;
	List indicatorCalChart(Map paramMap) throws NkiaException;
	List indicatorCalChartPerMonth(Map paramMap) throws NkiaException;
	Map indicatorCalHeaderPerMonth(Map paramMap) throws NkiaException;
	List selectIndicatorCriticViewList(Map paramMap) throws NkiaException;
	int selectIndicatorCriticViewCount(Map paramMap) throws NkiaException;
}
