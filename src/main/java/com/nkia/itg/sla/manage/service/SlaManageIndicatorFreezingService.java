package com.nkia.itg.sla.manage.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

@SuppressWarnings({ "rawtypes" })
public interface SlaManageIndicatorFreezingService {
	void updateIndicatorFreezing(Map paramMap) throws NkiaException;
	Map selectIndicatorFreezing(Map paramMap) throws NkiaException;
	
	List selectFreezing(Map paramMap) throws NkiaException;
	Map selectFreezingViewHeader(Map paramMap) throws NkiaException;
	List searchFreezingView(Map paramMap) throws NkiaException;
	
	List searchExecLogList(Map paramMap) throws NkiaException;
	int searchExecLogCount(Map paramMap) throws NkiaException;
	
	Map selectFreezingQuery(Map paramMap) throws NkiaException;
	void updateFreezingQuery(Map paramMap) throws NkiaException;
	List selectViewColumnsList(Map paramMap) throws NkiaException;
	int selectViewColumnsCount(Map paramMap) throws NkiaException;
	void updateViewColumnsComment(List paramList) throws NkiaException;
	void createOrReplaceView(Map paramMap) throws NkiaException;
}