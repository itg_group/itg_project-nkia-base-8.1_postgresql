package com.nkia.itg.sla.manage.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.sla.manage.dao.SlaManageDAO;
import com.nkia.itg.sla.manage.service.SlaManageIndicatorService;
import com.nkia.itg.system.customer.dao.CustomerDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Service("SlaManageIndicatorService")
public class SlaManageIndicatorServiceImpl implements SlaManageIndicatorService {

	@Autowired
	private SlaManageDAO slaManageDAO;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Override
	public List searchIndicatorAllTree(Map param) throws NkiaException {
		return slaManageDAO.searchIndicatorAllTree(param);
	}


	@Override
	public boolean insertIndicatorGroup(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}

    	String id = slaManageDAO.insertIndicatorGroup(paramMap);
    	
    	if(id == null || id.equals("")) {
    		return false;
    	}else {
    		return true;
    	}
	}


	@Override
	public Map selectIndicatorGroup(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorGroup(paramMap);
	}

	@Override
	public Map selectIndicatorDetail(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorDetail(paramMap);
	}

	@Override
	public List selectIndicatorDetailList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorDetailList(paramMap);
	}
	
	@Override
	public int selectIndicatorDetailCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorDetailCount(paramMap);
	}

	@Override
	public boolean updateIndicatorGroup(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	
    	int id = slaManageDAO.updateIndicatorGroup(paramMap);
    	
    	if(id == 0) {
    		return false;
    	}else {
    		return true;
    	}
	}


	@Override
	public boolean insertIndicatorDetail(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}

    	String id = slaManageDAO.insertIndicatorDetail(paramMap);
    	
    	if(id == null || id.equals("")) {
    		return false;
    	}else {
    		return true;
    	}
	}


	@Override
	public boolean updateIndicatorDetail(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	
    	int id = slaManageDAO.updateIndicatorDetail(paramMap);
    	
    	if(id == 0) {
    		return false;
    	}else {
    		return true;
    	}
	}
	
	
	@Override
	public boolean updateIndicatorCalculator(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	
    	int id = slaManageDAO.updateIndicatorCalculator(paramMap);
    	
    	if(id == 0) {
    		return false;
    	}else {
    		return true;
    	}
	}


	@Override
	public boolean updateContractIndicator(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
    	String CUSTOMER_ID = (String)paramMap.get("customer_id");
    	String CONTRACT_ID = (String)paramMap.get("contract_id");
    	
    	List deleteList = (List)paramMap.get("deleteList");
    	for(int i=0; i<deleteList.size(); i++) {
    		Map map = (Map)deleteList.get(i);
    		
    		slaManageDAO.deleteContractIndicator(map);
    	}
    	
    	List dataList = (List)paramMap.get("indicatorList");
		for (int i=0; i<dataList.size(); i++) {
			Map map = (Map)dataList.get(i);
			
			map.put("CUSTOMER_ID", 				CUSTOMER_ID);
			map.put("CONTRACT_ID", 				CONTRACT_ID);
			map.put("INDICATOR_GROUP_ID", 		map.get("INDICATOR_GROUP_ID"));
			map.put("INDICATOR_ID", 			map.get("INDICATOR_ID"));
			map.put("CALCULATE_TYPE_CD", 		map.get("CALCULATE_TYPE_CD"));
			map.put("INDICATOR_DETAIL_YN", 		map.get(""));
			map.put("INDICATOR_DETAIL_CODE", 	map.get(""));
			map.put("SLA_INDICATOR_TYPE_CD", 	map.get("SLA_INDICATOR_TYPE_CD"));
			map.put("SLA_UNIT", 				map.get("SLA_UNIT"));
			map.put("USE_YN", 					map.get("USE_YN"));
			map.put("TARGET_LEVEL", 			map.get("TARGET_LEVEL"));
			map.put("MINIMUM_LEVEL", 			map.get("MINIMUM_LEVEL"));
			map.put("WEIGHT", 					map.get("WEIGHT"));
			map.put("CRITICAL_SCORE_A", 		map.get("CRITICAL_SCORE_A"));
			map.put("CRITICAL_SCORE_B", 		map.get("CRITICAL_SCORE_B"));
			map.put("CRITICAL_SCORE_C", 		map.get("CRITICAL_SCORE_C"));
			map.put("LEVEL_POINT_TYPE", 		map.get("LEVEL_POINT_TYPE"));
			map.put("END_LEVEL",				map.get(""));
			map.put("S_LEVEL_SCORE", 			map.get("S_LEVEL_SCORE"));
			map.put("A_LEVEL_SCORE", 			map.get("A_LEVEL_SCORE"));
			map.put("B_LEVEL_SCORE", 			map.get("B_LEVEL_SCORE"));
			map.put("C_LEVEL_SCORE", 			map.get("C_LEVEL_SCORE"));
			map.put("D_LEVEL_SCORE",			map.get("D_LEVEL_SCORE"));
			map.put("E_LEVEL_SCORE", 			map.get("E_LEVEL_SCORE"));
			map.put("F_LEVEL_SCORE", 			map.get("F_LEVEL_SCORE"));
			map.put("A_LEVEL_POINT", 			map.get("A_LEVEL_POINT"));
			map.put("B_LEVEL_POINT", 			map.get("B_LEVEL_POINT"));
			map.put("C_LEVEL_POINT", 			map.get("C_LEVEL_POINT"));
			map.put("D_LEVEL_POINT",			map.get("D_LEVEL_POINT"));
			map.put("INS_USER_ID", 				paramMap.get("ins_user_id"));
			map.put("UPD_USER_ID", 				paramMap.get("ins_user_id"));
			
			slaManageDAO.updateContractIndicator(map);
		}
		
		dataList = (List)paramMap.get("indicatorSumList");
		for (int i=0; i<dataList.size(); i++) {
			Map map = (Map)dataList.get(i);
			
			map.put("CUSTOMER_ID", 				CUSTOMER_ID);
			map.put("CONTRACT_ID", 				CONTRACT_ID);
			map.put("INDICATOR_GROUP_ID", 		"TOTAL");
			map.put("INDICATOR_ID", 			"TOTAL");
			map.put("TARGET_LEVEL", 			map.get("TARGET_LEVEL"));
			map.put("MINIMUM_LEVEL", 			map.get("MINIMUM_LEVEL"));
			map.put("WEIGHT", 					map.get("WEIGHT"));
			map.put("CRITICAL_SCORE_A", 		map.get("CRITICAL_SCORE_A"));
			map.put("CRITICAL_SCORE_B", 		map.get("CRITICAL_SCORE_B"));
			map.put("CRITICAL_SCORE_C", 		map.get("CRITICAL_SCORE_C"));
			map.put("LEVEL_POINT_TYPE", 		map.get("LEVEL_POINT_TYPE"));
			map.put("END_LEVEL",				"");
			map.put("S_LEVEL_SCORE", 			map.get("S_LEVEL_SCORE"));
			map.put("A_LEVEL_SCORE", 			map.get("A_LEVEL_SCORE"));
			map.put("B_LEVEL_SCORE", 			map.get("B_LEVEL_SCORE"));
			map.put("C_LEVEL_SCORE", 			map.get("C_LEVEL_SCORE"));
			map.put("D_LEVEL_SCORE",			map.get("D_LEVEL_SCORE"));
			map.put("E_LEVEL_SCORE", 			map.get("E_LEVEL_SCORE"));
			map.put("F_LEVEL_SCORE", 			map.get("F_LEVEL_SCORE"));
			map.put("A_LEVEL_POINT", 			map.get("A_LEVEL_POINT"));
			map.put("B_LEVEL_POINT", 			map.get("B_LEVEL_POINT"));
			map.put("C_LEVEL_POINT", 			map.get("C_LEVEL_POINT"));
			map.put("D_LEVEL_POINT",			map.get("D_LEVEL_POINT"));
			map.put("LEVEL_POINT_TYPE", 		map.get("LEVEL_POINT_TYPE"));
			map.put("UPD_USER_ID", 				paramMap.get("ins_user_id"));
			
			slaManageDAO.updateContractIndicator(map);
		}
    	
		return true;
	}


	@Override
	public boolean updateContractIndicatorCal(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
    	String SLA_ID = (String)paramMap.get("sla_id");
    	String APPLY_YM = (String)paramMap.get("apply_ym");
    	
    	List dataList = (List)paramMap.get("indicatorList");
		for (int i=0; i<dataList.size(); i++) {
			Map map = (Map)dataList.get(i);
			
			map.put("SLA_ID", SLA_ID);
			map.put("APPLY_YM", APPLY_YM);
			map.put("UPD_USER_ID", paramMap.get("ins_user_id"));
			
			slaManageDAO.updateContractIndicatorCal(map);
			// slaManageDAO.procExecSubtoal(map);
		}
		
		String CUSTOMER_ID = (String)paramMap.get("customer_id");
		String CONTRACT_ID = (String)paramMap.get("contract_id");
		
		dataList = (List)paramMap.get("indicatorSumList"); 
		for (int i=0; i<dataList.size(); i++) { 
			Map map = (Map)dataList.get(i);
			
			map.put("CUSTOMER_ID", CUSTOMER_ID); 
			map.put("CONTRACT_ID", CONTRACT_ID);
			map.put("APPLY_YM", APPLY_YM);
			map.put("SLA_ID", SLA_ID);
			map.put("INDICATOR_GROUP_ID", "TOTAL");
			map.put("INDICATOR_ID", "TOTAL");
			map.put("UPD_USER_ID", paramMap.get("ins_user_id"));
			
			slaManageDAO.updateContractIndicatorCal(map);
		}
    	
		return true;
	}


	@Override
	public boolean updateCalIndicatorClose(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	
    	int id = slaManageDAO.updateCalIndicatorClose(paramMap);
    	
    	if(id == 0) {
    		return false;
    	}else {
    		return true;
    	}
	}


	@Override
	public List updateContractIndicatorCalDetail(Map paramMap) throws NkiaException {
		return slaManageDAO.updateContractIndicatorCalDetail(paramMap);
	}


	@Override
	public List indicatorGroupCombo(Map paramMap) throws NkiaException {
		return slaManageDAO.indicatorGroupCombo(paramMap);
	}


	@Override
	public List indicatorCombo(Map paramMap) throws NkiaException {
		return slaManageDAO.indicatorCombo(paramMap);
	}
	
	
	@Override
	public List selectIndicatorCriticViewList(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorCriticViewList(paramMap);
	}
	
	@Override
	public int selectIndicatorCriticViewCount(Map paramMap) throws NkiaException {
		return slaManageDAO.selectIndicatorCriticViewCount(paramMap);
	}


	@Override
	public List indicatorCalChart(Map paramMap) throws NkiaException {
		return slaManageDAO.indicatorCalChart(paramMap);
	}

	@Override
	public List indicatorCalChartPerMonth(Map paramMap) throws NkiaException {
		List resultList = new ArrayList();
		Map resultMap = new HashMap();
		
		List indicatorCalChart = slaManageDAO.indicatorCalChartPerMonth(paramMap);
		
		resultMap.put("CLASS_NM", "환산점수");
		resultMap.put("TYPE", "환산점수");
		for(int count=0 ; count<indicatorCalChart.size() ; count++) {
			Map indicatorCalMap = (Map) indicatorCalChart.get(count);
			resultMap.put(indicatorCalMap.get("INDICATOR_ID"), indicatorCalMap.get("CONVERSION_VALUE"));
		}
		resultList.add(resultMap);
		
		if(paramMap != null && "grid".equals(paramMap.get("type"))) {
			resultMap = new HashMap();
			resultMap.put("CLASS_NM", "측정값");
			resultMap.put("TYPE", "측정값");
			for(int count=0 ; count<indicatorCalChart.size() ; count++) {
				Map indicatorCalMap = (Map) indicatorCalChart.get(count);
				resultMap.put(indicatorCalMap.get("INDICATOR_ID"), indicatorCalMap.get("ROUND_VALUE"));
			}
			resultList.add(resultMap);
		}
		
		return resultList;
	}
	
	@Override
	public Map indicatorCalHeaderPerMonth(Map paramMap) throws NkiaException {
		return slaManageDAO.indicatorCalHeaderPerMonth(paramMap);
	}

	@Override
	public void updateCalIndicator(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("user_id", userVO.getUser_id());
    	}
    	
		try {
			slaManageDAO.updateCalIndicator(paramMap);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
	}


	@Override
	public Map procIndicatorLevelGet(Map paramMap) throws NkiaException {
		return slaManageDAO.procIndicatorLevelGet(paramMap);
	}
	
	@Override
	public Map procSpSlaIndicatorValue(Map paramMap) throws NkiaException {
		// 20200409_이원재_002_값 입력 시 소숫점에 의한 프로시저 오계산현상 회피
		paramMap.put("SLA_VALUE", Double.parseDouble((String) paramMap.get("SLA_VALUE")));
		
		return slaManageDAO.procSpSlaIndicatorValue(paramMap);
	}
	
	// 20200221_이원재_003_일배치 스케줄러 전용 프로시저 실행 등록
	@Override
	public void procSpSlaExecDaily() throws NkiaException {
		slaManageDAO.procSpSlaExecDaily();
	}
	// 20200221_이원재_003_일배치 스케줄러 전용 프로시저 실행 등록 END


	@Override
	public Map procCheckValueCal(Map paramMap) throws NkiaException {
		return slaManageDAO.procCheckValueCal(paramMap);
	}
}
