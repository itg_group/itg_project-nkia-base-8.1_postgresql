package com.nkia.itg.sample.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;


public interface FuncHttpProxyService {


	/**
	 * 신규번호체번
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public String selectNewSeq(Map<String, Object> paramMap) throws NkiaException;
	
	/**
	 * 로그 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertLog(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 로그 변경
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateLog(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 단건 조회
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public Map<String, Object> searchData1(Map<String, Object> paramMap) throws NkiaException; 

	/**
	 * 목록 조회
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public List<Map<String, Object>> searchData1List(Map<String, Object> paramMap) throws NkiaException; 
	

	/**
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public Map<String, Object> searchData3(Map<String, Object> paramMap) throws NkiaException; 

}
