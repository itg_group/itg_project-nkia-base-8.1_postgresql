/*
 * @(#)ExtjsPrototypeServiceImpl.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.sample.dao.ExtjsPrototypeDAO;
import com.nkia.itg.sample.service.ExtjsPrototypeService;

@Service("extjsPrototypeService")
public class ExtjsPrototypeServiceImpl implements ExtjsPrototypeService{

	@Resource(name = "extjsPrototypeDAO")
	public ExtjsPrototypeDAO extjsPrototypeDAO;
	
	public List selectPrototypeDataList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.selectPrototypeDataList(paramMap);
	}

	public List selectPrototypeTreeData(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.selectPrototypeTreeData(paramMap);
	}
	
	public List searchTestGirdData(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.searchTestGirdData(paramMap);
	}
	
	public String searchTestGridDataCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.searchTestGridDataCount(paramMap);
	}

	public int selectPrototypeDataListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.selectPrototypeDataListCount(paramMap);
	}
	
	public List goUserList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.goUserList(paramMap);
	}
	
	public int goUserListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.goUserListCount(paramMap);
	}
	
	public List goCiList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.goCiList(paramMap);
	}
	
	public int goCiListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.goCiListCount(paramMap);
	}
	
	public List goStaticCiAssetTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.goStaticCiAssetTree(paramMap);
	}
	
	public List goStaticCiAssetCheckTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.goStaticCiAssetCheckTree(paramMap);
	}
	
	public List setAmAssetGrid(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.setAmAssetGrid(paramMap);
	}
	
	public int setAmAssetGridCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.setAmAssetGridCount(paramMap);
	}
	
	public List getAmAssetGrid(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.getAmAssetGrid(paramMap);
	}
	
	public int getAmAssetGridCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return extjsPrototypeDAO.getAmAssetGridCount(paramMap);
	}
	
	public List goSelectCiServiceInfo(ModelMap paramMap) throws NkiaException {
		return extjsPrototypeDAO.goSelectCiServiceInfo(paramMap);
	}
	
	public List selectAllClassNm(ModelMap paramMap) throws NkiaException {
		return extjsPrototypeDAO.selectAllClassNm(paramMap);
	}
}
