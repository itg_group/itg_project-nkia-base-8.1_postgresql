package com.nkia.itg.sample.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.sample.dao.FuncHttpProxyDAO;
import com.nkia.itg.sample.service.FuncHttpProxyService;

import egovframework.com.cmm.EgovMessageSource;

@Service("funcHttpProxyService")
public class FuncHttpProxyServiceImpl implements FuncHttpProxyService {

	private static final Logger logger = LoggerFactory.getLogger(FuncHttpProxyServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "funcHttpProxyDAO")
	private FuncHttpProxyDAO funcHttpProxyDAO;
	
	/**
	 * 로그 번호 체번
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public String selectNewSeq(Map<String, Object> paramMap) throws NkiaException {
		return funcHttpProxyDAO.selectNewSeq(paramMap);
	}
	
	/**
	 * 로그 등록
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertLog(Map<String, Object> paramMap) throws NkiaException {
		funcHttpProxyDAO.insertLog(paramMap);
	}

	@Override
	public void updateLog(Map<String, Object> paramMap) throws NkiaException {
		funcHttpProxyDAO.updateLog(paramMap);
	}
	
	/**
	 * 단건 조회
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public Map<String, Object> searchData1(Map<String, Object> paramMap) throws NkiaException {
		logger.debug(""+paramMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("USER_ID", "USER" + DateUtil.getDateFormat());
		resultMap.put("USER_NM", "홍길동");
		resultMap.put("USER_TYPE", "1");
		return resultMap;
	}
	
	/**
	 * 목록 조회
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public List<Map<String, Object>> searchData1List(Map<String, Object> paramMap) throws NkiaException {
		logger.debug(""+paramMap);
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = null;
		map = new HashMap<String, Object>();
		map.put("MENU_ID", "MENU01");
		map.put("MENU_NM", "시스템관리" + DateUtil.getDateFormat());
		map.put("MENU_TYPE", "1");
		resultList.add(map);
		map = new HashMap<String, Object>();
		map.put("MENU_ID", "MENU02");
		map.put("MENU_NM", "사용자관리");
		map.put("MENU_TYPE", "2");
		resultList.add(map);
		return resultList;
	}
	
	/**
	 * 단건 조회
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public Map<String, Object> searchData3(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> responseData = new HashMap<String, Object>();
		
		String request_no = String.valueOf(paramMap.get("request_no"));
		String request_vendor_id = String.valueOf(paramMap.get("request_vendor_id"));
		String request_user_id = String.valueOf(paramMap.get("request_user_id"));
		String request_work_type = String.valueOf(paramMap.get("request_work_type"));

		if("CHANGE_END".equals(request_work_type)){
			// 업무서비스 호출 (아래는 데이타 예제)
			responseData.put("result_code", "OK");
			responseData.put("result_msg", messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		}else if("CHANGE01".equals(request_work_type)){ // 
			// 업무서비스 호출 (아래는 데이타 예제)
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("USER_ID", "USER" + DateUtil.getDateFormat());
			map.put("USER_NM", "홍길동");
			map.put("USER_TYPE", "1");
			responseData.put("out1_info", "사용자 상세");
			responseData.put("out1_map", map);
			
			responseData.put("result_code", "OK");
			responseData.put("result_msg", messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		}else if("CHANGE02".equals(request_work_type)){
			// 업무서비스 호출 (아래는 데이타 예제)
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = null;
			map = new HashMap<String, Object>();
			map.put("MENU_ID", "MENU01");
			map.put("MENU_NM", "시스템관리" + DateUtil.getDateFormat());
			map.put("MENU_TYPE", "1");
			resultList.add(map);
			map = new HashMap<String, Object>();
			map.put("MENU_ID", "MENU02");
			map.put("MENU_NM", "사용자관리");
			map.put("MENU_TYPE", "2");
			resultList.add(map);
			responseData.put("out1_info", "메뉴 목록");
			responseData.put("out1_list", resultList);
			
			responseData.put("result_code", "OK");
			responseData.put("result_msg", messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		}else if("CHANGE03".equals(request_work_type)){
			// 업무서비스 호출 (아래는 데이타 예제)
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = null;
			map = new HashMap<String, Object>();
			map.put("MENU_ID", "MENU01");
			map.put("MENU_NM", "시스템관리" + DateUtil.getDateFormat());
			map.put("MENU_TYPE", "1");
			resultList.add(map);
			map = new HashMap<String, Object>();
			map.put("MENU_ID", "MENU02");
			map.put("MENU_NM", "사용자관리");
			map.put("MENU_TYPE", "2");
			resultList.add(map);
			responseData.put("out1_info", "메뉴 목록");
			responseData.put("out1_list", resultList);
			
			map = new HashMap<String, Object>();
			map.put("USER_ID", "USER01");
			map.put("USER_NM", "홍길동");
			map.put("USER_TYPE", "1");
			resultList.add(map);
			map = new HashMap<String, Object>();
			map.put("USER_ID", "USER02");
			map.put("USER_NM", "홍길서");
			map.put("USER_TYPE", "1");
			resultList.add(map);
			responseData.put("out2_info", "사용자 목록");
			responseData.put("out2_list", resultList);
			
			map.put("CLASS_ID", "CLASS01");
			map.put("CLASS_NM", "분류체계");
			responseData.put("out3_info", "분류체계 상세");
			responseData.put("out3_map", map);
			
			responseData.put("result_code", "OK");
			responseData.put("result_msg", messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		}else{
			throw new NkiaException("요청번호 " + request_no + " 의 요청작업유형을 확인해주세요 22 [request_work_type:" + request_work_type + "]");
		}
		return responseData;
	}

	
}
