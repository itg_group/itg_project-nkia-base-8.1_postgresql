package com.nkia.itg.sample.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.sample.dao.DashboardSampleDAO;
import com.nkia.itg.sample.service.DashboardSampleService;

@Service("dashboardSampleService")
public class DashboardSampleServiceImpl implements DashboardSampleService {
	
	@Resource(name="dashboardSampleDAO")
	private DashboardSampleDAO dashboardSampleDAO;
	
	/**
	 * 요청통계/ 서비스요청처리현황
	 */
	public List selectReqTypeProcessList(Map paramMap) throws NkiaException {
		return dashboardSampleDAO.selectReqTypeProcessList(paramMap);
	}
	
}
