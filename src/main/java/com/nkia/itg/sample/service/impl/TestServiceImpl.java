/*
 * @(#)TestServiceImpl.java              2013. 5. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.sample.dao.TestDAO;
import com.nkia.itg.sample.service.TestService;

@Service("testService")
public class TestServiceImpl implements TestService {

	@Resource(name = "testDAO")
	public TestDAO testDAO;
	
	@Override
	public int searchSummaryListCount(ModelMap paramMap) throws NkiaException {
		return testDAO.searchSummaryListCount(paramMap);
	}

	@Override
	public List searchSummaryList(ModelMap paramMap) throws NkiaException {
		return testDAO.searchSummaryList(paramMap);
	}

	@Override
	public List searchSummaryData(ModelMap paramMap) throws NkiaException {
		return testDAO.searchSummaryData(paramMap);
	}

}
