/*
 * @(#)TestService.java              2013. 5. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface TestService {

	public int searchSummaryListCount(ModelMap paramMap) throws NkiaException;

	public List searchSummaryList(ModelMap paramMap) throws NkiaException;

	public List searchSummaryData(ModelMap paramMap) throws NkiaException;
	
}
