/*
 * @(#)ExtjsPrototypeService.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ExtjsPrototypeService {

	List selectPrototypeDataList(ModelMap paramMap)throws NkiaException;

	List selectPrototypeTreeData(ModelMap paramMap)throws NkiaException;
	
	String searchTestGridDataCount(ModelMap paramMap)throws NkiaException;
	
	List searchTestGirdData(ModelMap paramMap)throws NkiaException;

	int selectPrototypeDataListCount(ModelMap paramMap)throws NkiaException;
	
	List goUserList(ModelMap paramMap)throws NkiaException;
	
	public int goUserListCount(ModelMap paramMap) throws NkiaException;
	
	List goCiList(ModelMap paramMap)throws NkiaException;
	
	public int goCiListCount(ModelMap paramMap) throws NkiaException;
	
	public List goStaticCiAssetTree(ModelMap paramMap) throws NkiaException;
	
	public List goStaticCiAssetCheckTree(ModelMap paramMap) throws NkiaException;
	
	List setAmAssetGrid(ModelMap paramMap)throws NkiaException;
	
	int setAmAssetGridCount(ModelMap paramMap)throws NkiaException;
	
	List getAmAssetGrid(ModelMap paramMap)throws NkiaException;
	
	int getAmAssetGridCount(ModelMap paramMap)throws NkiaException;
	
	public List goSelectCiServiceInfo(ModelMap paramMap) throws NkiaException;
	
	public List selectAllClassNm(ModelMap paramMap) throws NkiaException;

}
