package com.nkia.itg.sample.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.TreeVO;

public interface DashboardSampleService {

	public List selectReqTypeProcessList(Map paramMap) throws NkiaException;
	
}
