package com.nkia.itg.sample.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.sample.dao.WebixSampleDAO;
import com.nkia.itg.sample.service.WebixSampleService;
import com.nkia.itg.system.board.dao.BoardCategoryTreeDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("webixSampleService")
public class WebixSampleServiceImpl implements WebixSampleService{
	
	@Resource(name="webixSampleDAO")
	private WebixSampleDAO webixSampleDAO;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
		
	public List searchCellMergeUserList(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchCellMergeUserList(paramMap);
	}
	
	public List searchRowMergeDataList(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchRowMergeDataList(paramMap);
	}
	
	public List searchTreeGridDataList(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchTreeGridDataList(paramMap);
	}
	
	public List searchLifeCycleSample(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchLifeCycleSample(paramMap);
	}
	
	public List searchClassTypeSample(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchClassTypeSample(paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchBoardList(paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchBoardListCount(paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException, IOException {
		webixSampleDAO.deleteBoardInfo(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException {
   		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
   		paramMap.put("ins_user_id", userVO.getUser_id());
		webixSampleDAO.updateBoardInfo(paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(ModelMap paramMap) throws NkiaException {
   		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
   		paramMap.put("ins_user_id", userVO.getUser_id());

   		String BoardId = selectBoardId(paramMap);
		paramMap.put("BoardId", BoardId);
		
		webixSampleDAO.insertBoardInfo(paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.selectBoardId(paramMap);
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(ModelMap paramMap) throws NkiaException {
		Map boardInfo = webixSampleDAO.selectBoardInfo(paramMap);
		Map addData = webixSampleDAO.searchBothInfoNo(paramMap);
		
		boardInfo.put("addData", addData);
		
		return boardInfo;
	}
	
	public List searchRackInfoList(ModelMap paramMap) throws NkiaException {
		return webixSampleDAO.searchRackInfoList(paramMap);
	}
}
