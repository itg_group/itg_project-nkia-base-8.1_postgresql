package com.nkia.itg.sample.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("webixSampleDAO")
public class WebixSampleDAO extends EgovComAbstractDAO {

	public List searchCellMergeUserList(ModelMap paramMap) {
		return list("WebixSampleDAO.searchCellMergeUserList", paramMap);
	}
	
	public List searchRowMergeDataList(ModelMap paramMap) {
		return list("WebixSampleDAO.searchRowMergeDataList", paramMap);
	}
	
	public List searchTreeGridDataList(ModelMap paramMap) {
		return list("WebixSampleDAO.searchTreeGridDataList", paramMap);
	}
	
	public List searchLifeCycleSample(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = list("WebixSampleDAO.searchLifeCycleSample", paramMap);
		return resultList;
	}

	public List searchClassTypeSample(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = list("WebixSampleDAO.searchClassTypeSample", paramMap);
		return resultList;
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(ModelMap paramMap)throws NkiaException {
		return list("WebixSampleDAO.searchBoardList", paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("WebixSampleDAO.searchBoardListCount", paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException {
		this.delete("WebixSampleDAO.deleteBoardInfo", paramMap);
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException {
		this.update("WebixSampleDAO.updateBoardInfo", paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(ModelMap paramMap) throws NkiaException {
		this.insert("WebixSampleDAO.insertBoardInfo", paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(ModelMap paramMap) throws NkiaException {
		String BoardID = (String) this.selectByPk("WebixSampleDAO.selectBoardId", paramMap);
		return BoardID;
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public HashMap selectBoardInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap) this.selectByPk("WebixSampleDAO.selectBoardInfo", paramMap);
	}
	
	/**
	 * 이전글 / 다음글 내역 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public HashMap searchBothInfoNo(ModelMap paramMap)throws NkiaException {
		return (HashMap) this.selectByPk("WebixSampleDAO.searchBothInfoNo", paramMap);
	}
	
	public List searchRackInfoList(ModelMap paramMap)throws NkiaException {
		return list("WebixSampleDAO.searchRackInfoList", paramMap);
	}
}
