package com.nkia.itg.sample.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("dashboardSampleDAO")
public class DashboardSampleDAO extends EgovComAbstractDAO {

	/**
	 * 요청통계/ 서비스요청처리현황
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectReqTypeProcessList(Map paramMap) throws NkiaException {
		List resultList = list("DashboardSampleDAO.selectReqTypeProcessList", paramMap);
		return resultList;
	}
	
}
