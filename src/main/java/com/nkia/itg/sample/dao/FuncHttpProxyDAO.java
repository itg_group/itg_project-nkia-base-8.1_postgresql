package com.nkia.itg.sample.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("funcHttpProxyDAO")
public class FuncHttpProxyDAO extends EgovComAbstractDAO {


	/**
	 * 신규번호생성
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public String selectNewSeq(Map<String, Object> paramMap) throws NkiaException {
		return (String) selectByPk("KairWsSupHistDAO.selectNewSeq", paramMap);
	}
	
	/**
	 * 로그 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertLog(Map<String, Object> paramMap) throws NkiaException {
		this.insert("KairWsSupHistDAO.insertKairWsSupHist", paramMap);
	}
	
	/**
	 * 로그 변경
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateLog(Map<String, Object> paramMap) throws NkiaException {
		this.insert("KairWsSupHistDAO.updateKairWsSupHist", paramMap);
	}
}
