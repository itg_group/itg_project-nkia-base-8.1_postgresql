/*
 * @(#)ExtjsPrototypeDAO.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * 
 * @version 1.0 2013. 1. 15.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Repository("extjsPrototypeDAO")
public class ExtjsPrototypeDAO extends EgovComAbstractDAO{

	public List selectPrototypeDataList(ModelMap paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ExtjsPrototypeDAO.selectPrototypeDataList", paramMap);
		return resultList;
	}

	public List selectPrototypeTreeData(ModelMap paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ExtjsPrototypeDAO.selectPrototypeTreeData", paramMap);
		return resultList;
	}
	
	public String searchTestGridDataCount(ModelMap paramMap)throws NkiaException {
		String count ;
		count = (String) selectByPk("ExtjsPrototypeDAO.searchTestGridDataCount", paramMap);
		return count;
	}
	
	public List searchTestGirdData(ModelMap paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ExtjsPrototypeDAO.searchTestGirdData", paramMap);
		return resultList;
	}

	public int selectPrototypeDataListCount(ModelMap paramMap)throws NkiaException {
		int resultCount = 0;
		resultCount = (Integer)selectByPk("ExtjsPrototypeDAO.selectPrototypeDataListCount", paramMap);
		return resultCount;
	}
	
	public List goUserList(ModelMap paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ExtjsPrototypeDAO.goUserList", paramMap);
		return resultList;
	}
	
	public int goUserListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("ExtjsPrototypeDAO.goUserListCount", paramMap);
	}
	
	public List goCiList(ModelMap paramMap)throws NkiaException {
		List resultList = null;
		//resultList = list("ExtjsPrototypeDAO.goCiList", paramMap);
		resultList = list("ExtjsPrototypeDAO.goUserList", paramMap);
		return resultList;
	}
	
	public int goCiListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		//return (Integer)selectByPk("ExtjsPrototypeDAO.goCiListCount", paramMap);
		return (Integer)selectByPk("ExtjsPrototypeDAO.goUserListCount", paramMap);
	}
	
	public List goStaticCiAssetTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("ExtjsPrototypeDAO.goStaticCiAssetTree", paramMap);
		return resultList;
	}
	
	public List goStaticCiAssetCheckTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		List resultList = null;
		resultList = this.list("ExtjsPrototypeDAO.goStaticCiAssetCheckTree", paramMap);
		return resultList;
	}
	
	public List setAmAssetGrid(ModelMap paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ExtjsPrototypeDAO.setAmAssetGrid", paramMap);
		return resultList;
	}
	
	public int setAmAssetGridCount(ModelMap paramMap)throws NkiaException {
		int resultCount = 0;
		resultCount = (Integer)selectByPk("ExtjsPrototypeDAO.setAmAssetGridCount", paramMap);
		return resultCount;
	}
	
	public List getAmAssetGrid(ModelMap paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ExtjsPrototypeDAO.getAmAssetGrid", paramMap);
		return resultList;
	}
	
	public int getAmAssetGridCount(ModelMap paramMap)throws NkiaException {
		int resultCount = 0;
		resultCount = (Integer)selectByPk("ExtjsPrototypeDAO.getAmAssetGridCount", paramMap);
		return resultCount;
	}
	
	public List goSelectCiServiceInfo(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("ExtjsPrototypeDAO.goSelectCiServiceInfo", paramMap);
		return resultList;
	}
	
	public List selectAllClassNm(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("ExtjsPrototypeDAO.selectAllClassNm", paramMap);
		return resultList;
	}
}
