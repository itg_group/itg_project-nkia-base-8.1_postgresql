/*
 * @(#)TestDAO.java              2013. 5. 14.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.OtherComAbstractDAO;

@Repository("testDAO")
public class TestDAO extends OtherComAbstractDAO {

	
	public int searchSummaryListCount(ModelMap paramMap) {
		Integer result = null;
		result = (Integer)selectByPk("TestDAO.searchSummaryListCount", paramMap);
		return result;
	}

	public List searchSummaryList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = list("TestDAO.searchSummaryList", paramMap);
		return resultList;
	}

	public List searchSummaryData(ModelMap paramMap) {
		List resultList = null;
		resultList = list("TestDAO.searchSummaryData", paramMap);
		return resultList;
	}

}
