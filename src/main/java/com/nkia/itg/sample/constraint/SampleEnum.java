package com.nkia.itg.sample.constraint;

public enum SampleEnum {

	SG_SERVER_IP("192.168.20.132"),
	SG_SERVER_PORT("5080"),
	SG_SERVER_PATH_GET_DATA_SR("/webservice/sample/selectSrData"),
	SG_SERVER_PATH_SEND_DATA_SR("/webservice/sample/sendSGApprData"),
	SG_SERVER_PATH_GET_ATCHFILE_SR("/webservice/sample/downloadSGAtchFile");
	
	private String message;
	
	SampleEnum(String message){
		this.message = message;
    }
	
	public String getString() {
		return this.message;
	}
	
}
