package com.nkia.itg.sample.vo;

import java.util.Map;

public class FuncHttpProxyVO {
	
	private boolean isSuccess = true;	// 성공실패/여부
	
	private String requestNo; // 요청번호
	private String requestWorkType; // 요청작업구분
	public Map<String, Object> requestData; // 요청 데이타
	
	private String responseNo; // 응답번호
	public Map<String, Object> responseData; // 응답데이타
	
	private String resultCode; // 결과코드
	private String resultMsg; // 결과메시지
	private String resultDesc; // 결과상세정보
	
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	
	public String getRequestNo() {
		return requestNo;
	}
	public void setRequestNo(String requestNo) {
		this.requestNo = requestNo;
	}
	public String getRequestWorkType() {
		return requestWorkType;
	}
	public void setRequestWorkType(String requestWorkType) {
		this.requestWorkType = requestWorkType;
	}
	public String getResponseNo() {
		return responseNo;
	}
	public void setResponseNo(String responseNo) {
		this.responseNo = responseNo;
	}
	public Map<String, Object> getRequestData() {
		return requestData;
	}
	public void setRequestData(Map<String, Object> requestData) {
		this.requestData = requestData;
	}
	public Map<String, Object> getResponseData() {
		return responseData;
	}
	public void setResponseData(Map<String, Object> responseData) {
		this.responseData = responseData;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String getResultDesc() {
		return resultDesc;
	}
	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}
	@Override
	public String toString() {
		return "FuncHttpProxyVO [isSuccess=" + isSuccess + ", requestNo=" + requestNo + ", requestWorkType=" + requestWorkType + ", requestData=" + requestData + ", responseNo=" + responseNo + ", responseData=" + responseData + ", resultCode="
				+ resultCode + ", resultMsg=" + resultMsg + ", resultDesc=" + resultDesc + "]";
	}
	
	
	
	
	
}
