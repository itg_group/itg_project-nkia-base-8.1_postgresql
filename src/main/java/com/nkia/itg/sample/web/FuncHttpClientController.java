package com.nkia.itg.sample.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.sample.vo.FuncHttpProxyVO;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class FuncHttpClientController {

	private static final Logger logger = LoggerFactory.getLogger(FuncHttpClientController.class);
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * Webix - 기본 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/func/httpclient/goHttpClient.do")
	public String goHttpClient(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/func/httpClient.nvf";
		return forwarPage;
	}
	
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/func/httpclient/sendData1.do")
	public @ResponseBody ResultVO sendData1(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	        String proxyUrl = "" + paramMap.get("proxy_url");
	        
			Map<String, Object> json = new HashMap<String, Object>();
			if(paramMap != null) {
				Iterator keySet = paramMap.keySet().iterator();
				while(keySet.hasNext()) {
					String paramName = (String)keySet.next();
					String paramValue = String.valueOf(paramMap.get(paramName));
					json.put(paramName, paramValue);
					
				}
			}
//			json.put("iName1", "idata" + DateUtil.getDateFormat()); // 샘플데이타
//			json.put("iName2", "idata2"); // 샘플데이타
//			json.put("iName3", "idata3"); // 샘플데이타
//			json.put("iName4", "idata4"); // 샘플데이타
			String body = JsonMapper.toJson(json);
			
			URL postUrl = new URL(proxyUrl);
			HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setConnectTimeout(3000);
			
			OutputStream os = conn.getOutputStream();
			os.write(body.getBytes());
			os.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			String responseJson = "";
			while ((output = br.readLine()) != null) {
				responseJson += output;
			}
			conn.disconnect();
			logger.debug("■C1■ responseJson : " + responseJson);
			resultVO= JsonMapper.fromJson(responseJson, ResultVO.class);
			//Gson gson = new Gson();
			//resultVO = gson.fromJson(responseJson, ResultVO.class);
			
//			resultVO.setSuccess(true);
//			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		} catch(Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.00007")); // 담당자에게 문의 바랍니다.
			resultVO.setResultString(e.toString());
		}
		
		logger.debug("■C1■ ■■■■■■■■■■■■■■■■■■■■■■■");
		logger.debug("■C1■ resultVO.toString() : " + resultVO.toString());
		logger.debug("■C1■ resultVO.getResultMsg() : " + resultVO.getResultMsg());
		logger.debug("■C1■ resultVO.getResultString() : " + resultVO.getResultString());
		logger.debug("■C1■ resultVO.getResultMap() : " + (null != resultVO.getResultMap()? resultVO.getResultMap(): ""));
		logger.debug("■C1■ resultVO.getGridVO().toString() : " + (null != resultVO.getGridVO()? resultVO.getGridVO().toString(): ""));
		logger.debug("■C1■ resultVO.getTreeVO().toString() : " + (null != resultVO.getTreeVO()? resultVO.getTreeVO().toString(): ""));
		logger.debug("■C1■ resultVO.isSuccess() : " + resultVO.isSuccess());
		logger.debug("■C1■ ■■■■■■■■■■■■■■■■■■■■■■■");
        
		return resultVO;
	}
	
	@RequestMapping(value="/itg/sample/func/httpclient/sendData1Test.do")
	public @ResponseBody ResultVO sendData1Test(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	        //호출할 주소 정보
	        String serviceUrl = "" + paramMap.get("proxy_url");
	        
			Map<String, Object> inJsonMap = new HashMap<String, Object>();
			
			paramMap.putAll(inJsonMap);
			if(paramMap != null) {
				Iterator keySet = paramMap.keySet().iterator();
				while(keySet.hasNext()) {
					String paramName = (String)keySet.next();
					String paramValue = String.valueOf(paramMap.get(paramName));
					inJsonMap.put(paramName, paramValue);
					
				}
			}
			inJsonMap.put("iName1", "idata" + DateUtil.getDateFormat());
			inJsonMap.put("iName2", "idata2");
			inJsonMap.put("iName3", "idata3");
			inJsonMap.put("iName4", "idata4");
			String body = JsonMapper.toJson(inJsonMap);
			logger.debug(body);
			
			URL postUrl = new URL(serviceUrl);
			HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
			conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
			conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setConnectTimeout(3000);
			
			logger.debug("■C2■ serviceUrl : " + serviceUrl);
			logger.debug("■C2■ conn.getConnectTimeout() : " + conn.getConnectTimeout());
			logger.debug("■C2■ conn.getRequestMethod() : " + conn.getRequestMethod());
			logger.debug("■C2■ conn.getRequestProperty(Content-Type) : " + conn.getRequestProperty("Content-Type"));
			logger.debug("■C2■ conn.getDoOutput() : " + conn.getDoOutput());
			logger.debug("■C2■ conn.getDoInput() : " + conn.getDoInput());
			
			OutputStream os = conn.getOutputStream();
			os.write(body.getBytes());
			os.flush();
			logger.debug("■C2■ Location: " + conn.getHeaderField("Location"));

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			logger.debug("■C2■ Output from Server .... ");
			String responseJson = "";
			while ((output = br.readLine()) != null) {
				logger.debug(output);
				responseJson += output;
			}
			conn.disconnect();
			logger.debug("■C2■ responseStr : " + responseJson);
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("cName1", "cData" + DateUtil.getDateFormat());
			resultMap.put("cName2", "cData2");
			resultMap.put("cName3", "cData3");
			resultMap.put("cName4", "cData4");
				
//			<dependency>
//			  <groupId>com.google.code.gson</groupId>
//			  <artifactId>gson</artifactId>
//			  <version>2.2.4</version>
//			</dependency>
			logger.debug("■C9■ responseJson : " + responseJson);
			resultVO= JsonMapper.fromJson(responseJson, ResultVO.class);
			//Gson gson = new Gson();
			//resultVO = gson.fromJson(responseJson, ResultVO.class);
			
//			resultVO.setSuccess(true);
//			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		} catch(Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.00007")); // 담당자에게 문의 바랍니다.
			resultVO.setResultString(e.toString());
		}
		
		logger.debug("■C9■ ■■■■■■■■■■■■■■■■■■■■■■■");
		logger.debug("■C9■ resultVO.toString() : " + resultVO.toString());
		logger.debug("■C9■ resultVO.getResultMsg() : " + resultVO.getResultMsg());
		logger.debug("■C9■ resultVO.getResultString() : " + resultVO.getResultString());
		logger.debug("■C9■ resultVO.getResultMap() : " + (null != resultVO.getResultMap()? resultVO.getResultMap(): ""));
		logger.debug("■C9■ resultVO.getGridVO().toString() : " + (null != resultVO.getGridVO()? resultVO.getGridVO().toString(): ""));
		logger.debug("■C9■ resultVO.getTreeVO().toString() : " + (null != resultVO.getTreeVO()? resultVO.getTreeVO().toString(): ""));
		logger.debug("■C9■ resultVO.isSuccess() : " + resultVO.isSuccess());
		logger.debug("■C9■ ■■■■■■■■■■■■■■■■■■■■■■■");
        
		return resultVO;
	}
	
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/sample/func/httpclient/sendData3.do")
	public @ResponseBody FuncHttpProxyVO sendData2(@RequestBody ModelMap paramMap)throws NkiaException, IOException {
		
		FuncHttpProxyVO resultVO = new FuncHttpProxyVO();
		Map<String, Object> inJsonMap = new HashMap<String, Object>();
		// 요청 기본정보
		inJsonMap.put("request_no", System.currentTimeMillis());
		inJsonMap.put("request_vendor_id", "KAIR");
		inJsonMap.put("request_user_id", "US001");
		String request_work_type = "CHANGE03"; // 작업구분 : CHANGE01: 변경상세조회, CHANGE02: 변경목록조회, CHANGE03: 변경 여러목록조회
		inJsonMap.put("request_work_type", request_work_type);
		
		// 요청 파라메터 정보 
		if("CHANGE01".equals(request_work_type)){
			// 출처 : 인터페이스 운영자 메뉴얼_v1.0_20150612(ISM 연계).pptx > DOR 보고서 Web Service 테이블 매핑 > 작업현황 및 작업계획(NOSCHANGE)
			inJsonMap.put("DESCIRPTION", "9901"); // 작업명
			inJsonMap.put("WONUM", "9901"); // 변경번호
			inJsonMap.put("LOCATION", "9902"); // 위치
			inJsonMap.put("CIWITH", "9903"); // 대상장비
			inJsonMap.put("WOLO1", "9904"); // 작업목적
			inJsonMap.put("RISK", "9905"); // 작업영향
			inJsonMap.put("CLOSURECODE", "9906"); // 작업결과
			inJsonMap.put("SCHEDSTART", "9907"); // 계획시작시간
			inJsonMap.put("SCHEDFINISH", "9908"); // 계획완료시간
			inJsonMap.put("ACTSTART", "9909"); // 작업시작시간
			inJsonMap.put("ACTFINISH", "9910"); // 작업완료시간
			
		}else if("CHANGE02".equals(request_work_type)){
			// 출처 : 인터페이스 운영자 메뉴얼_v1.0_20150612(ISM 연계).pptx > DOR 보고서 Web Service 테이블 매핑 > RQ(작업요청)
			inJsonMap.put("TICKETID", "9901"); // 작업 요청
			inJsonMap.put("CREATEDBY", "9902"); // 레코드 생성자
			inJsonMap.put("CLASSSTRUCTUREID", "9903"); // 분류
			inJsonMap.put("ASSIGNGROUP", "9904"); // 작업 그룹
			inJsonMap.put("ASSIGN", "9905"); // 작업자
			inJsonMap.put("PMCOMRESOLUTION", "9906"); // 진행 상태
			inJsonMap.put("REPORTEDBY", "9907"); // 요청자
			inJsonMap.put("AFFECTEDPERSON", "9908"); // 열람자
			inJsonMap.put("DESCRIPTION", "9909"); // 작업요청명
			inJsonMap.put("DESCRIPTION_LONGDESCRIPTION", "9910"); // 세부사항
			inJsonMap.put("TARGETFINISHDATE", "9911"); // 완료요청일
			inJsonMap.put("COMMODITY", "9912"); // 시스템
			inJsonMap.put("COMMODITYGROUP", "9913"); // 소팀
			
		}else if("CHANGE03".equals(request_work_type)){
			inJsonMap.put("sr_id", "SR99999999");
			inJsonMap.put("sr_type_cd", "9999");
			inJsonMap.put("class_id", "CLASS9999");
			
		}
		
        String proxyUrl = "" + paramMap.get("proxy_url");
        
		if(paramMap != null) {
			Iterator keySet = paramMap.keySet().iterator();
			while(keySet.hasNext()) {
				String paramName = (String)keySet.next();
				String paramValue = String.valueOf(paramMap.get(paramName));
				inJsonMap.put(paramName, paramValue);
				
			}
		}
//		json.put("iName1", "idata" + DateUtil.getDateFormat()); // 샘플데이타
//		json.put("iName2", "idata2"); // 샘플데이타
//		json.put("iName3", "idata3"); // 샘플데이타
//		json.put("iName4", "idata4"); // 샘플데이타
		String body = JsonMapper.toJson(inJsonMap);
		
		URL postUrl = new URL(proxyUrl);
		HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
		conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
		conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setConnectTimeout(3000);
		
//		System.out.println("■C■ proxyUrl : " + proxyUrl);
//		System.out.println("■C■ conn.getConnectTimeout() : " + conn.getConnectTimeout());
//		System.out.println("■C■ conn.getRequestMethod() : " + conn.getRequestMethod());
//		System.out.println("■C■ conn.getRequestProperty(Content-Type) : " + conn.getRequestProperty("Content-Type"));
//		System.out.println("■C■ conn.getDoOutput() : " + conn.getDoOutput());
//		System.out.println("■C■ conn.getDoInput() : " + conn.getDoInput());
//		System.out.println("■C■ conn.getExpiration() : " + conn.getExpiration());
//		System.out.println("■C■ conn.getContentType() : " + conn.getContentType());

		OutputStream os = conn.getOutputStream();
		os.write(body.getBytes());
		os.flush();
//		System.out.println("■C■ Location: " + conn.getHeaderField("Location"));

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		String output;
//		System.out.println("■C■ Output from Server .... ");
		String responseJson = "";
		while ((output = br.readLine()) != null) {
			//System.out.println(output);
			responseJson += output;
		}
		conn.disconnect();
//		System.out.println("■C■ responseStr : " + responseJson);
		
//		System.out.println("■C■ ChangeVO.toString() 1 : " + resultVO.toString());
		try {
			resultVO= JsonMapper.fromJson(responseJson, FuncHttpProxyVO.class);
			//Gson gson = new Gson();
			//resultVO = gson.fromJson(responseJson, FuncHttpProxyVO.class);
		} catch (Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.00007")); // 담당자에게 문의 바랍니다.
			resultVO.setResultDesc(e.toString());
		}
		logger.debug("■C■ 결과 ■■■■■■■■■■■■■■■■■■■■■■■");

		Map<String, Object> requestData = resultVO.getRequestData();
		request_work_type = resultVO.getRequestWorkType();
		Map<String, Object> responseData = resultVO.getResponseData();
		
		if("OK".equals(resultVO.getResultCode())){
			// 작업구분 : CHANGE01: 변경상세조회, CHANGE02: 변경목록조회
			if("CHANGE01".equals(request_work_type)){
				logger.debug("■C■ 작업구분 ["+request_work_type+"] CHANGE01: 변경상세조회 ");
				logger.debug("■C■ out1_title : " + responseData.get("out1_title"));
				Map<String, Object> map = (Map)responseData.get("out1_map");
				logger.debug("■C■ out1_map > map : " + map);
	
			}else if("CHANGE02".equals(request_work_type)){
				logger.debug("■C■ 작업구분 ["+request_work_type+"] CHANGE02: 변경목록조회 ");
				logger.debug("■C■ out1_title : " + responseData.get("OUT1_TITLE"));
				List<Map<String, Object>> out1_list = (List)responseData.get("out1_list");
				for(int i=0; i<out1_list.size(); i++){
					Map<String, Object> map = out1_list.get(i);
					logger.debug("■C■ ["+i+"/"+out1_list.size()+"] out1_list > map : " + map);
				}
				
			}else if("CHANGE03".equals(request_work_type)){
				logger.debug("■C■ 작업구분 ["+request_work_type+"] CHANGE02: 변경목록조회 ");
				logger.debug("■C■ out1_title : " + responseData.get("out1_title"));
				List<Map<String, Object>> out1_list = (List)responseData.get("out1_list");
				for(int i=0; i<out1_list.size(); i++){
					Map<String, Object> map = out1_list.get(i);
					logger.debug("■C■ ["+i+"/"+out1_list.size()+"] out1_list > map : " + map);
				}
				logger.debug("■C■ out2_title : " + responseData.get("out2_title"));
				List<Map<String, Object>> out2_list = (List)responseData.get("out2_list");
				for(int i=0; i<out2_list.size(); i++){
					Map<String, Object> map = out2_list.get(i);
					logger.debug("■C■ ["+i+"/"+out2_list.size()+"] out2_list > map : " + map);
				}
				logger.debug("■C■ out3_title : " + responseData.get("out3_title"));
				Map<String, Object> map = (Map)responseData.get("out3_map");
				logger.debug("■C■ out3_map > map : " + map);
			}else{
				logger.debug("■C■ ["+request_work_type+"] request_work_type 미정의된 구분값 ");
			}
		}
		
		logger.debug("■C■ resultVO.getRequestNo() : " + resultVO.getRequestNo());
		logger.debug("■C■ resultVO.getRequestData() : " + (null != resultVO.getRequestData()? resultVO.getRequestData(): ""));
		logger.debug("■C■ resultVO.getResponseNo() : " + resultVO.getResponseNo());
		logger.debug("■C■ resultVO.getResponseData() : " + (null != resultVO.getResponseData()? resultVO.getResponseData(): ""));
		logger.debug("■C■ resultVO.getResultCode() : " + resultVO.getResultCode());
		logger.debug("■C■ resultVO.getResultMsg() : " + resultVO.getResultMsg());
		logger.debug("■C■ resultVO.getResultDesc() : " + resultVO.getResultDesc());
		logger.debug("■C■ resultVO.toString() : " + resultVO.toString());
		logger.debug("■C■ ■■■■■■■■■■■■■■■■■■■■■■■");
		return resultVO;
	}

	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/sample/func/httpclient/sendData4.do")
	public @ResponseBody Map<String, Object> sendData4(@RequestBody ModelMap paramMap)throws NkiaException, IOException {
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> inJsonMap = new HashMap<String, Object>();
		// 요청 기본정보
		inJsonMap.put("request_no", System.currentTimeMillis());
		inJsonMap.put("request_vendor_id", "KAIR");
		inJsonMap.put("request_user_id", "US001");
		String request_work_type = "CHANGE03"; // 작업구분 : CHANGE01: 변경상세조회, CHANGE02: 변경목록조회, CHANGE03: 변경 여러목록조회
		inJsonMap.put("request_work_type", request_work_type);
		
		// 요청 파라메터 정보 
		if("CHANGE01".equals(request_work_type)){
			// 출처 : 인터페이스 운영자 메뉴얼_v1.0_20150612(ISM 연계).pptx > DOR 보고서 Web Service 테이블 매핑 > 작업현황 및 작업계획(NOSCHANGE)
			inJsonMap.put("DESCIRPTION", "9901"); // 작업명
			inJsonMap.put("WONUM", "9901"); // 변경번호
			inJsonMap.put("LOCATION", "9902"); // 위치
			inJsonMap.put("CIWITH", "9903"); // 대상장비
			inJsonMap.put("WOLO1", "9904"); // 작업목적
			inJsonMap.put("RISK", "9905"); // 작업영향
			inJsonMap.put("CLOSURECODE", "9906"); // 작업결과
			inJsonMap.put("SCHEDSTART", "9907"); // 계획시작시간
			inJsonMap.put("SCHEDFINISH", "9908"); // 계획완료시간
			inJsonMap.put("ACTSTART", "9909"); // 작업시작시간
			inJsonMap.put("ACTFINISH", "9910"); // 작업완료시간
			
		}else if("CHANGE02".equals(request_work_type)){
			// 출처 : 인터페이스 운영자 메뉴얼_v1.0_20150612(ISM 연계).pptx > DOR 보고서 Web Service 테이블 매핑 > RQ(작업요청)
			inJsonMap.put("TICKETID", "9901"); // 작업 요청
			inJsonMap.put("CREATEDBY", "9902"); // 레코드 생성자
			inJsonMap.put("CLASSSTRUCTUREID", "9903"); // 분류
			inJsonMap.put("ASSIGNGROUP", "9904"); // 작업 그룹
			inJsonMap.put("ASSIGN", "9905"); // 작업자
			inJsonMap.put("PMCOMRESOLUTION", "9906"); // 진행 상태
			inJsonMap.put("REPORTEDBY", "9907"); // 요청자
			inJsonMap.put("AFFECTEDPERSON", "9908"); // 열람자
			inJsonMap.put("DESCRIPTION", "9909"); // 작업요청명
			inJsonMap.put("DESCRIPTION_LONGDESCRIPTION", "9910"); // 세부사항
			inJsonMap.put("TARGETFINISHDATE", "9911"); // 완료요청일
			inJsonMap.put("COMMODITY", "9912"); // 시스템
			inJsonMap.put("COMMODITYGROUP", "9913"); // 소팀
			
		}else if("CHANGE03".equals(request_work_type)){
			inJsonMap.put("sr_id", "SR99999999");
			inJsonMap.put("sr_type_cd", "9999");
			inJsonMap.put("class_id", "CLASS9999");
			
		}
		
        String proxyUrl = "" + paramMap.get("proxy_url");
        
		if(paramMap != null) {
			Iterator keySet = paramMap.keySet().iterator();
			while(keySet.hasNext()) {
				String paramName = (String)keySet.next();
				String paramValue = String.valueOf(paramMap.get(paramName));
				inJsonMap.put(paramName, paramValue);
				
			}
		}
//		json.put("iName1", "idata" + DateUtil.getDateFormat()); // 샘플데이타
//		json.put("iName2", "idata2"); // 샘플데이타
//		json.put("iName3", "idata3"); // 샘플데이타
//		json.put("iName4", "idata4"); // 샘플데이타
		String body = JsonMapper.toJson(inJsonMap);
		
		URL postUrl = new URL(proxyUrl);
		HttpURLConnection conn = (HttpURLConnection) postUrl.openConnection();
		conn.setDoOutput(true); // xml내용을 전달하기 위해서 출력 스트림을 사용
		conn.setInstanceFollowRedirects(false); //Redirect처리 하지 않음
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setConnectTimeout(3000);
		
//		System.out.println("■C■ proxyUrl : " + proxyUrl);
//		System.out.println("■C■ conn.getConnectTimeout() : " + conn.getConnectTimeout());
//		System.out.println("■C■ conn.getRequestMethod() : " + conn.getRequestMethod());
//		System.out.println("■C■ conn.getRequestProperty(Content-Type) : " + conn.getRequestProperty("Content-Type"));
//		System.out.println("■C■ conn.getDoOutput() : " + conn.getDoOutput());
//		System.out.println("■C■ conn.getDoInput() : " + conn.getDoInput());
//		System.out.println("■C■ conn.getExpiration() : " + conn.getExpiration());
//		System.out.println("■C■ conn.getContentType() : " + conn.getContentType());

		OutputStream os = conn.getOutputStream();
		os.write(body.getBytes());
		os.flush();
//		System.out.println("■C■ Location: " + conn.getHeaderField("Location"));

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		String output;
//		System.out.println("■C■ Output from Server .... ");
		String responseJson = "";
		while ((output = br.readLine()) != null) {
			//System.out.println(output);
			responseJson += output;
		}
		conn.disconnect();
//		System.out.println("■C■ responseStr : " + responseJson);
		
//		System.out.println("■C■ resultMap.toString() 1 : " + resultMap.toString());
		try {
			resultMap= JsonMapper.fromJson(responseJson, Map.class);
			//Gson gson = new Gson();
			//resultMap = gson.fromJson(responseJson, Map.class);
		} catch (Exception e) {
			resultMap.put("success", false);
			resultMap.put("result_msg", messageSource.getMessage("msg.common.00007")); // 담당자에게 문의 바랍니다.
			resultMap.put("result_string", e.toString());
		}
		logger.debug("■C■ resultMap 1 : " + resultMap);
		logger.debug("■C■ 결과 ■■■■■■■■■■■■■■■■■■■■■■■");

		Map<String, Object> requestData = (Map)resultMap.get("request_data");
		request_work_type = (String)resultMap.get("request_work_type");
		Map<String, Object> responseData = (Map)resultMap.get("response_data");
		
		if("OK".equals((String)resultMap.get("result_code"))){
			// 작업구분 : CHANGE01: 변경상세조회, CHANGE02: 변경목록조회
			if("CHANGE01".equals(request_work_type)){
				logger.debug("■C■ 작업구분 ["+request_work_type+"] CHANGE01: 변경상세조회 ");
				logger.debug("■C■ out1_title : " + responseData.get("out1_title"));
				Map<String, Object> map = (Map)responseData.get("out1_map");
				logger.debug("■C■ out1_map > map : " + map);
	
			}else if("CHANGE02".equals(request_work_type)){
				logger.debug("■C■ 작업구분 ["+request_work_type+"] CHANGE02: 변경목록조회 ");
				logger.debug("■C■ out1_title : " + responseData.get("OUT1_TITLE"));
				List<Map<String, Object>> out1_list = (List)responseData.get("out1_list");
				for(int i=0; i<out1_list.size(); i++){
					Map<String, Object> map = out1_list.get(i);
					logger.debug("■C■ ["+i+"/"+out1_list.size()+"] out1_list > map : " + map);
				}
				
			}else if("CHANGE03".equals(request_work_type)){
				logger.debug("■C■ 작업구분 ["+request_work_type+"] CHANGE02: 변경목록조회 ");
				logger.debug("■C■ out1_title : " + responseData.get("out1_title"));
				List<Map<String, Object>> out1_list = (List)responseData.get("out1_list");
				for(int i=0; i<out1_list.size(); i++){
					Map<String, Object> map = out1_list.get(i);
					logger.debug("■C■ ["+i+"/"+out1_list.size()+"] out1_list > map : " + map);
				}
				logger.debug("■C■ out2_title : " + responseData.get("out2_title"));
				List<Map<String, Object>> out2_list = (List)responseData.get("out2_list");
				for(int i=0; i<out2_list.size(); i++){
					Map<String, Object> map = out2_list.get(i);
					logger.debug("■C■ ["+i+"/"+out2_list.size()+"] out2_list > map : " + map);
				}
				logger.debug("■C■ out3_title : " + responseData.get("out3_title"));
				Map<String, Object> map = (Map)responseData.get("out3_map");
				logger.debug("■C■ out3_map > map : " + map);
			}else{
				logger.debug("■C■ ["+request_work_type+"] request_work_type 미정의된 구분값 ");
			}
		}

		if(resultMap != null) {
			Iterator keySet = resultMap.keySet().iterator();
			while(keySet.hasNext()) {
				String paramName = (String)keySet.next();
				logger.debug("■C■ resultMap["+paramName+"] : " + resultMap.get(paramName));
			}
		}
//		logger.debug("■C■ resultMap.request_no() : " + resultMap.get("request_no"));
//		logger.debug("■C■ resultMap.request_data() : " + resultMap.get("request_data"));
//		logger.debug("■C■ resultMap.response_no() : " + resultMap.get("response_no"));
//		logger.debug("■C■ resultMap.response_data() : " + resultMap.get("response_data"));
//		logger.debug("■C■ resultMap.result_code() : " + resultMap.get("result_code"));
//		logger.debug("■C■ resultMap.result_msg() : " + resultMap.get("result_msg"));
//		logger.debug("■C■ resultMap.result_msg() : " + resultMap.get("result_desc"));
//		logger.debug("■C■ resultMap.toString() : " + resultMap.toString());
		logger.debug("■C■ ■■■■■■■■■■■■■■■■■■■■■■■");
		return resultMap;
	}
}
