package com.nkia.itg.sample.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelRackInfo;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.sample.service.WebixSampleService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class WebixSampleController {
	
	@Resource(name = "webixSampleService")
	private WebixSampleService webixSampleService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="excelRackInfo")
	private ExcelRackInfo excelRackInfo;
	
	/**
	 * Webix - 기본 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goBasicGrid.do")
	public String goBasicGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridBasic.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 체크박스 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goCheckboxGrid.do")
	public String goCheckboxGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridCheckbox.nvf";
		return forwarPage;
	}

	/**
	 * Webix - 의존관계 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goDependencyGrid.do")
	public String goDependencyGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridDependency.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 입력가능 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goEditableGrid.do")
	public String goEditableGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridEditable.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 탭 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goTabGrid.do")
	public String goTabGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridTab.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 조건에 의한 [사용불가] 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goCondtionDisabledGrid.do")
	public String goCondtionDisabledGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridCondtionDisabled.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 조건에 의한 [읽음상태] 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goCondtionReadGrid.do")
	public String goCondtionReadGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridCondtionRead.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 조건에 의한 [아이콘 표시] 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goCondtionIconGrid.do")
	public String goCondtionIconGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridCondtionIcon.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 액션컬럼 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goActionColumnGrid.do")
	public String goActionColumnGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridActionColumn.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - Cell Merge 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goCellMergeGrid.do")
	public String goCellMergeGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridMergeCell.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - Cell Merge 그리드 데이터 조회
	 * @param paramMap
	 * @return resultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/searchCellMergeUserList.do")
	public @ResponseBody ResultVO searchCellMergeUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = webixSampleService.searchCellMergeUserList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * Webix - Row Merge 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goRowMergeGrid.do")
	public String goRowMergeGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridMergeRow.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - Row Merge 그리드 데이터 조회
	 * @param paramMap
	 * @return resultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/searchRowMergeDataList.do")
	public @ResponseBody ResultVO searchRowMergeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = webixSampleService.searchRowMergeDataList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * Webix - Header Merge 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goHeaderMergeGrid.do")
	public String goHeaderMergeGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridMergeHeader.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 그리드 커스텀 리소스 만들기 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goCustomResourceGrid.do")
	public String goCustomResourceGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridCustomResource.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - Tree 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goTreeGrid.do")
	public String goTreeGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridTree.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - Tree 그리드 데이터 조회
	 * @param paramMap
	 * @return resultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/searchTreeGridDataList.do")
	public @ResponseBody ResultVO searchTreeGridDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = webixSampleService.searchTreeGridDataList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * Webix - Checkbox Tree 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goTreeCheckboxGrid.do")
	public String goTreeCheckboxGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridTreeCheckbox.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - Checkgrid Tree 그리드 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goTreeCheckGrid.do")
	public String goTreeCheckGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/gridTreeCheckgrid.nvf";
		return forwarPage;
	}

	/**
	 * Webix - 기본 트리 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goBasicTree.do")
	public String goBasicTree(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/treeBasic.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 체크박스 트리 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goCheckboxTree.do")
	public String goCheckboxTree(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/treeCheckbox.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 검색 폼 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goSearchForm.do")
	public String goSearchForm(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/formSearch.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 입력 폼 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goEditForm.do")
	public String goEditForm(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/formEdit.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 입력 폼 팝업 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goEditPopupForm.do")
	public String goEditPopupForm(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/formEditPopup.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 입력 폼 첨부파일 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goEditFileForm.do")
	public String goEditFileForm(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/formEditFile.nvf";
		return forwarPage;
	}
	
	/**
	 * Webix - 하이차트 샘플 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goHighChart.do")
	public String goHighChart(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/highChart.nvf";
		return forwarPage;
	}
	
	/**
	 * 자산 상태별 차트 샘플 데이터
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/searchLifeCycleSample.do")
	public @ResponseBody ResultVO searchLifeCycleSample(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = webixSampleService.searchLifeCycleSample(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 분류체계별 차트 샘플 데이터
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/searchClassTypeSample.do")
	public @ResponseBody ResultVO searchClassTypeSample(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = webixSampleService.searchClassTypeSample(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * Webix - 필드 readonly 테스트 페이지
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goReadonlyTest.do")
	public String goReadonlyTest(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/readonlyTest.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/goSampleDownBoard.do")
	public String goFaqBoard(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/webix/formEditFileForStream.nvf";
		return forwarPage;
	}
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 글목록 리스트
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/searchDownBoardList.do")
	public @ResponseBody ResultVO searchBoardList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
//			boolean initFlag = false;
//			if(paramMap.get("init") != null) {
//				initFlag = (Boolean)paramMap.get("init");		
//			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			//if(!initFlag) {
				totalCount = webixSampleService.searchBoardListCount(paramMap);
				resultList = webixSampleService.searchBoardList(paramMap);		
			//}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 글 삭제
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/deleteDownBoardInfo.do")
	public @ResponseBody ResultVO deleteBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			webixSampleService.deleteBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 글 수정
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/updateDownBoardInfo.do")
	public @ResponseBody ResultVO updateBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			webixSampleService.updateBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 글 등록
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/insertDownBoardInfo.do")
	public @ResponseBody ResultVO insertBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			webixSampleService.insertBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/webix/selectDownBoardInfo.do")
	public @ResponseBody ResultVO selectBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) webixSampleService.selectBoardInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/rackExcelDown.do")
	public @ResponseBody ResultVO batchEditAssetExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		List resultList = new ArrayList();
		ModelMap excelMap = new ModelMap();
		try{
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			resultList = webixSampleService.searchRackInfoList(paramMap);
			excelMap = excelRackInfo.makeExcelFileXlsx(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
