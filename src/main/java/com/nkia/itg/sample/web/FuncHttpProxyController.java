package com.nkia.itg.sample.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.sample.service.FuncHttpProxyService;
import com.nkia.itg.sample.vo.FuncHttpProxyVO;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class FuncHttpProxyController {
	
	private static final Logger logger = LoggerFactory.getLogger(FuncHttpProxyController.class);
	
	@Resource(name = "funcHttpProxyService")
	private FuncHttpProxyService funcHttpProxyService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/sample/func/httpproxy/searchData1.do")
	public @ResponseBody ResultVO searchData(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Map<String, Object> resultMap = funcHttpProxyService.searchData1(paramMap);
			resultVO.setResultMap(resultMap);;
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		} catch (Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.00007")); // 담당자에게 문의 바랍니다.
		}
		logger.debug("■S1■ ■■■■■■■■■■■■■■■■■■■■■■■");
		logger.debug("■S1■ resultVO.toString() : " + resultVO.toString());
		logger.debug("■S1■ resultVO.getResultMsg() : " + resultVO.getResultMsg());
		logger.debug("■S1■ resultVO.getResultString() : " + resultVO.getResultString());
		logger.debug("■S1■ resultVO.getResultMap() : " + (null != resultVO.getResultMap()? resultVO.getResultMap(): ""));
		logger.debug("■S1■ resultVO.getGridVO().toString() : " + (null != resultVO.getGridVO()? resultVO.getGridVO().toString(): ""));
		logger.debug("■S1■ resultVO.getTreeVO().toString() : " + (null != resultVO.getTreeVO()? resultVO.getTreeVO().toString(): ""));
		logger.debug("■S1■ resultVO.isSuccess() : " + resultVO.isSuccess());
		logger.debug("■S1■ ■■■■■■■■■■■■■■■■■■■■■■■");
		
		return resultVO;
	}

	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/sample/func/httpproxy/searchData1List.do")
	public @ResponseBody ResultVO searchDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = funcHttpProxyService.searchData1List(paramMap);		
			totalCount = 99;
			
			gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		} catch (Exception e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.00007")); // 담당자에게 문의 바랍니다.
		}
		logger.debug("■S2■ ■■■■■■■■■■■■■■■■■■■■■■■");
		logger.debug("■S2■ resultVO.toString() : " + resultVO.toString());
		logger.debug("■S2■ resultVO.getResultMsg() : " + resultVO.getResultMsg());
		logger.debug("■S2■ resultVO.getResultString() : " + resultVO.getResultString());
		logger.debug("■S2■ resultVO.getResultMap() : " + (null != resultVO.getResultMap()? resultVO.getResultMap(): ""));
		logger.debug("■S2■ resultVO.getGridVO().toString() : " + (null != resultVO.getGridVO()? resultVO.getGridVO().toString(): ""));
		logger.debug("■S2■ resultVO.getTreeVO().toString() : " + (null != resultVO.getTreeVO()? resultVO.getTreeVO().toString(): ""));
		logger.debug("■S2■ resultVO.isSuccess() : " + resultVO.isSuccess());
		logger.debug("■S2■ ■■■■■■■■■■■■■■■■■■■■■■■");
		
		return resultVO;
	}
	
	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/sample/func/httpproxy/searchData3.do")
	public @ResponseBody FuncHttpProxyVO searchData3(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		FuncHttpProxyVO resultVO = new FuncHttpProxyVO();
		resultVO.setResultCode("OK");
		resultVO.setRequestData(new HashMap<String, Object>());
		resultVO.setResponseData(new HashMap<String, Object>());
		Map<String, Object> logMap = new HashMap<String, Object>();
		long startTime = System.currentTimeMillis();
		
		String request_no = StringUtil.replaceNull(String.valueOf(paramMap.get("request_no")), "");
		String request_vendor_id = StringUtil.replaceNull(String.valueOf(paramMap.get("request_vendor_id")), "");
		String request_user_id = StringUtil.replaceNull(String.valueOf(paramMap.get("request_user_id")), "");
		String request_work_type = StringUtil.replaceNull(String.valueOf(paramMap.get("request_work_type")), "");
		
		// 요청 로그 기록
		try {
			String response_no = funcHttpProxyService.selectNewSeq(paramMap);
			resultVO.setRequestNo(request_no);
			resultVO.setRequestWorkType(request_work_type);
			resultVO.setResponseNo(response_no);
			resultVO.setRequestData(paramMap);
			
			String service_method = FuncHttpProxyController.class.getName() + ".searchData3()";
			logMap.put("service_no", response_no);
			logMap.put("service_type", "HOST");
			logMap.put("service_method", service_method);
			logMap.put("request_no", request_no);
			logMap.put("request_vendor_id", request_vendor_id);
			logMap.put("request_user_id", request_user_id);
			logMap.put("request_work_type", request_work_type);
			logMap.put("response_no", response_no);
			logMap.put("request_data", paramMap.toString());
			logMap.put("login_user_id", "IF_USER");
			funcHttpProxyService.insertLog(logMap);
		} catch (Exception e) {
			resultVO.setResultCode("FAIL11");
			resultVO.setResultMsg("요청로그 등록중 오류가 발생했습니다.");
			resultVO.setResultDesc(e.toString());
		}
		
		// 필수값 체크
		String validMsg = "";
		
		if("".equals(request_no)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_no";
		}
		if("".equals(request_vendor_id)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_vendor_id";
		}
		if("".equals(request_user_id)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_user_id";
		}
		if("".equals(request_work_type)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_work_type";
		}
		if(0 < validMsg.length()){
			resultVO.setResultCode("FAIL12");
			resultVO.setResultMsg("필수정보가 없습니다  ["+validMsg+"]");
		}

		// 요청 업무진행
		try {
			if("OK".equals(resultVO.getResultCode())){
				logger.debug("■S1■ paramMap : " + paramMap);
				Map<String, Object> responseData = funcHttpProxyService.searchData3(paramMap);
				logger.debug("■S1■ responseData : " + responseData);
				resultVO.setResponseData(responseData);
				
				String result_code = StringUtil.replaceNull(String.valueOf(responseData.get("result_code")), "");
				String result_msg = StringUtil.replaceNull(String.valueOf(responseData.get("result_msg")), "");
				if(!"".equals(result_code)){
					responseData.remove("result_code");
					resultVO.setResultCode(result_code);
				}
				if(!"".equals(result_msg)){
					responseData.remove("result_msg");
					resultVO.setResultMsg(result_msg);
				}
			}
		} catch (NkiaException e1) {
			resultVO.setResultCode("NKIA_COM_FAIL_21");
			resultVO.setResultMsg(e1.getMessage());
			//resultVO.setResultDesc(ExceptionUtils.getThrowableList(e1).toString());
			resultVO.setResultDesc(e1.toString());
		} catch (Exception e) {
			resultVO.setResultCode("NKIA_COM_FAIL_22");
			resultVO.setResultMsg(e.getMessage());
			//resultVO.setResultDesc(ExceptionUtils.getThrowableList(e).toString());
			resultVO.setResultDesc(e.toString());
		}
		
		// 요청결과 로그 기록
		try {
			logMap.put("response_data", resultVO.getResponseData().toString());
			logMap.put("elapsed_time", System.currentTimeMillis() - startTime);
			logMap.put("result_code", resultVO.getResultCode());
			logMap.put("result_msg", resultVO.getResultMsg());
			logMap.put("result_desc", resultVO.getResultDesc());
			//paramMap.put("elased_time", System.currentTimeMillis() - startTime); // 결과시간
			funcHttpProxyService.updateLog(logMap);
		} catch (Exception e) {
			resultVO.setResultCode("FAIL13");
			resultVO.setResultMsg("요청로그 등록중 오류가 발생했습니다.");
			resultVO.setResultDesc(e.toString());
		}

		logger.debug("■S1■ 반환결과 ■■■■■■■■■■■■■■■■■■■■■■■");
		logger.debug("■S1■ resultVO.getRequestNo() : " + resultVO.getRequestNo());
		logger.debug("■S1■ resultVO.getRequestData() : " + (null != resultVO.getRequestData()? resultVO.getRequestData(): ""));
		logger.debug("■S1■ resultVO.getResponseNo() : " + resultVO.getResponseNo());
		logger.debug("■S1■ resultVO.getResponseData() : " + (null != resultVO.getResponseData()? resultVO.getResponseData(): ""));
		logger.debug("■S1■ resultVO.getResultCode() : " + resultVO.getResultCode());
		logger.debug("■S1■ resultVO.getResultMsg() : " + resultVO.getResultMsg());
		logger.debug("■S1■ resultVO.getResultDesc() : " + resultVO.getResultDesc());
		logger.debug("■S1■ resultVO.toString() : " + resultVO.toString());
		logger.debug("■S1■ ■■■■■■■■■■■■■■■■■■■■■■■");
		
		return resultVO;
	}

	/**
	 * 샘플 자료실 - DB저장 첨부파일 컴포넌트
	 * 선택항목 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/sample/func/httpproxy/searchData4.do")
	public @ResponseBody Map<String, Object> searchData3b(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("result_code", "OK");
		resultMap.put("success", true);
//		resultMap.setRequestData(new HashMap<String, Object>());
//		resultMap.setResponseData(new HashMap<String, Object>());
		Map<String, Object> logMap = new HashMap<String, Object>();
		long startTime = System.currentTimeMillis();
		
		String request_no = StringUtil.replaceNull(String.valueOf(paramMap.get("request_no")), "");
		String request_vendor_id = StringUtil.replaceNull(String.valueOf(paramMap.get("request_vendor_id")), "");
		String request_user_id = StringUtil.replaceNull(String.valueOf(paramMap.get("request_user_id")), "");
		String request_work_type = StringUtil.replaceNull(String.valueOf(paramMap.get("request_work_type")), "");
		
		// 요청 로그 기록
		try {
			String response_no = funcHttpProxyService.selectNewSeq(paramMap);
			resultMap.put("request_no", request_no);
			resultMap.put("request_work_type", request_work_type);
			resultMap.put("response_no", response_no);
			resultMap.put("request_data", paramMap);
			
			String service_method = FuncHttpProxyController.class.getName() + ".searchData3()";
			logMap.put("service_no", response_no);
			logMap.put("service_type", "HOST");
			logMap.put("service_method", service_method);
			logMap.put("request_no", request_no);
			logMap.put("request_vendor_id", request_vendor_id);
			logMap.put("request_user_id", request_user_id);
			logMap.put("request_work_type", request_work_type);
			logMap.put("response_no", response_no);
			String request_data = paramMap.toString();
			if(1500 < request_data.length()){
				request_data = request_data.substring(0, 1500);
			}
			logMap.put("request_data", request_data);
			logMap.put("login_user_id", "IF_USER");
			funcHttpProxyService.insertLog(logMap);
		} catch (Exception e) {
			resultMap.put("result_code", "FAIL11");
			resultMap.put("result_msg", "요청로그 등록중 오류가 발생했습니다.");
			resultMap.put("result_desc", e.toString());
		}
		
		// 필수값 체크
		String validMsg = "";
		
		if("".equals(request_no)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_no";
		}
		if("".equals(request_vendor_id)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_vendor_id";
		}
		if("".equals(request_user_id)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_user_id";
		}
		if("".equals(request_work_type)){
			validMsg += (!"".equals(validMsg)? "^": "") + "request_work_type";
		}
		if(0 < validMsg.length()){
			resultMap.put("result_code", "FAIL12");
			resultMap.put("result_msg", "필수정보가 없습니다  ["+validMsg+"]");
		}

		// 요청 업무진행
		try {
			if("OK".equals(resultMap.get("result_code"))){
				logger.debug("■S1■ paramMap : " + paramMap);
				Map<String, Object> responseData = funcHttpProxyService.searchData3(paramMap);
				logger.debug("■S1■ responseData : " + responseData);
				resultMap.put("response_data", responseData);
				
				String result_code = StringUtil.replaceNull(String.valueOf(responseData.get("result_code")), "");
				String result_msg = StringUtil.replaceNull(String.valueOf(responseData.get("result_msg")), "");
				if(!"".equals(result_code)){
					responseData.remove("result_code");
					resultMap.put("result_code", result_code);
				}
				if(!"".equals(result_msg)){
					responseData.remove("result_msg");
					resultMap.put("result_msg", result_msg);
				}
			}
		} catch (NkiaException e1) {
			resultMap.put("result_code", "NKIA_COM_FAIL_21");
			resultMap.put("result_msg", e1.getMessage());
			//resultVO.put("result_desc", ExceptionUtils.getThrowableList(e1).toString());
			resultMap.put("result_desc", e1.toString());
		} catch (Exception e) {
			resultMap.put("result_code", "NKIA_COM_FAIL_22");
			resultMap.put("result_msg", e.getMessage());
			//resultVO.put("result_desc", ExceptionUtils.getThrowableList(e).toString());
			resultMap.put("result_desc", e.toString());
		}
		
		// 요청결과 로그 기록
		try {
			logMap.put("response_data", resultMap.get("response_data").toString());
			logMap.put("elapsed_time", System.currentTimeMillis() - startTime);
			logMap.put("result_code", resultMap.get("result_code"));
			logMap.put("result_msg", resultMap.get("result_msg"));
			logMap.put("result_desc", resultMap.get("result_desc"));
			//paramMap.put("elased_time", System.currentTimeMillis() - startTime); // 결과시간
			funcHttpProxyService.updateLog(logMap);
		} catch (Exception e) {
			resultMap.put("result_code", "FAIL13");
			resultMap.put("result_msg", "요청로그 등록중 오류가 발생했습니다.");
			resultMap.put("result_desc", e.toString());
		}

		logger.debug("■S1■ 반환결과 ■■■■■■■■■■■■■■■■■■■■■■■");
//		logger.debug("■S1■ resultVO.getRequestNo() : " + resultMap.getRequestNo());
//		logger.debug("■S1■ resultVO.getRequestData() : " + (null != resultMap.getRequestData()? resultMap.getRequestData(): ""));
//		logger.debug("■S1■ resultVO.getResponseNo() : " + resultMap.getResponseNo());
//		logger.debug("■S1■ resultVO.get("response_data") : " + (null != resultMap.get("response_data")? resultMap.get("response_data"): ""));
//		logger.debug("■S1■ resultVO.get("result_code") : " + resultMap.get("result_code"));
//		logger.debug("■S1■ resultVO.get("result_msg") : " + resultMap.get("result_msg"));
//		logger.debug("■S1■ resultVO.get("result_desc") : " + resultMap.get("result_desc"));
		logger.debug("■S1■ resultVO.toString() : " + resultMap.toString());
		logger.debug("■S1■ ■■■■■■■■■■■■■■■■■■■■■■■");
		
		return resultMap;
	}
	
}
