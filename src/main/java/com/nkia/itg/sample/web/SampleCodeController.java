/*
 * @(#)SampleCodeController.java              2013. 1. 28.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.sample.service.ExtjsPrototypeService;

import egovframework.com.cmm.EgovMessageSource;

/**
 * @version 1.0 2013. 1. 28.
 * @author <a href="mailto:kdw0929@nkia.co.kr"> DoWon Kim
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class SampleCodeController {
	
	@Resource(name = "extjsPrototypeService")
	private ExtjsPrototypeService extjsPrototypeService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 
	 * Extjs Prototype - PROCESS Grid 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/processGrid.do")
	public String processGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/processGrid.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * Extjs Prototype - PROCESS Component 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/processComponent.do")
	public String processComponent(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/processComponent.nvf";
		return forwarPage;
	}
	
	
	/**
	 * 
	 * Extjs Prototype - 기본 Grid 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/itgSampleGrid.do")
	public String goGridSample(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgSampleGrid.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * Extjs Prototype - 기본 Grid 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/itgGrid_SyncScroll.do")
	public String itgGrid_SyncScroll(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgGrid_SyncScroll.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * Extjs Prototype - 기본 Grid 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/itgSampleGrid2.do")
	public String goGridSample2(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgSampleGrid2.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/ui_template/itgSampleGridDetail.do")
	public String goGridDetail(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgSampleGridDetail.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/ui_template/itgSampleGridEditOnly.do")
	public String goGridEditOnly(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgSampleGirdEditOnly.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/ui_template/itgSampleTree.do")
	public String goTreeSample(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgSampleTree.nvf";
		return forwarPage;
	}
		
	/**
	 * 
	 * Extjs Prototype - 기본 Grid 
     * 
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
		@RequestMapping(value="/itg/sample/ui_template/itgSampleBoard.do")
		public String itgSampleBoard(ModelMap paramMap) throws NkiaException {
			String forwarPage = "/itg/system/board/faq.nvf";
			return forwarPage;
		}
	
	/**
	 * 
	 * Extjs Prototype - 기본 Popup
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/itgSamplePopUp.do")
	public String itgSamplePopUp(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgSamplePopUp.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * Extjs Prototype - 사용자 팝업 샘플 	
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException	
	 */
	@RequestMapping(value="/itg/sample/ui_template/goUserList.do")
	public @ResponseBody GridVO goUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start);
			paramMap.put("last_num", end);
			
			if(!initFlag) {
				totalCount = extjsPrototypeService.goUserListCount(paramMap);
				resultList = extjsPrototypeService.goUserList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
						
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	/**
	 * 
	 * Extjs Prototype - 구성자원 팝업 샘플 	
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException	
	 */
	@RequestMapping(value="/itg/sample/ui_template/goCiList.do")
	public @ResponseBody GridVO goCiList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start);
			paramMap.put("last_num", end);
			
			if(!initFlag) {
				totalCount = extjsPrototypeService.goCiListCount(paramMap);
				resultList = extjsPrototypeService.goCiList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
						
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	/**
	 * 
	 * Extjs Prototype - 사용자 팝업 샘플 	
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException	
	 */
	@RequestMapping(value="/itg/sample/ui_template/goBoardList.do")
	public @ResponseBody GridVO goBoardList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			paramMap.put("first_num", start);
			paramMap.put("last_num", end);
			
			if(!initFlag) {
				totalCount = extjsPrototypeService.goUserListCount(paramMap);
				resultList = extjsPrototypeService.goUserList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
						
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	@RequestMapping(value="/itg/sample/ui_template/goCustomPagingGrid.do")
	public String goCustomPagingGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/customPagingGrid.nvf";
		return forwarPage;
	}

	@RequestMapping(value="/itg/sample/ui_template/goTwoDepthTree.do")
	public String goTwoDepthTree(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/twoDepthTree.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/ui_template/gridTabLayout.do")
	public String gridTabLayout(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/gridTabLayout.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/ui_template/goLayoutTemplate2.do")
	public String goLayoutTemplate2(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/layoutTemplate2.nvf";
		return forwarPage;
	}	

}
