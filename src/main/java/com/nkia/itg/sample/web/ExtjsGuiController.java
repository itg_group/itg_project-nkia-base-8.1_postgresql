/*
 * @(#)ExtjsGuiController.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * 
 * @version 1.0 2013. 1. 15.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * Extjs Gui Prototype Sample
 * </pre>
 *
 */
@Controller
public class ExtjsGuiController {
	
	@RequestMapping(value="/itg/sample/extjs/gui/goGuiDragDropHighlight.do")
	public String goGuiDragDropHighlight(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/gui/drag_drop_highlight.nvf";
		return forwarPage;
	}
	  
	@RequestMapping(value="/itg/sample/extjs/gui/goGuiDragDropCreateCmp.do")
	public String goGuiDragDropCreateCmp(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/gui/drag_drop_create_cmp.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/extjs/gui/goGuiDragDropCmpProp.do")
	public String goGuiDragDropCmpProp(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/gui/drag_drop_cmp_prop.nvf"; 
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/extjs/gui/goGuiDragDropCmpDiagram.do")
	public String goGuiDragDropCmpDiagram(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/gui/drag_drop_cmp_diagram.nvf"; 
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/extjs/gui/goGuiComponent.do")
	public String goGuiComponent(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/gui/gui_component.nvf"; 
		return forwarPage; 
	}
	
	@RequestMapping(value="/itg/sample/extjs/gui/goGuiDesigner.do")
	public String goGuiDesigner(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/gui/변경요청.nvf";
		return forwarPage;
	}
}
