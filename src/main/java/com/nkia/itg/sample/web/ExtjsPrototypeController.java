/*
 * @(#)ExtjsPrototypeController.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.sample.service.ExtjsPrototypeService;

import egovframework.com.cmm.EgovMessageSource;

/**
 * 
 * @version 1.0 2013. 1. 15.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * Extjs Prototype
 * </pre>
 *
 */
@Controller
public class ExtjsPrototypeController {
	
	@Resource(name = "extjsPrototypeService")
	private ExtjsPrototypeService extjsPrototypeService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 
	 * Extjs Prototype - 기본 Grid 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/extjs/prototype/goBaseGrid.do")
	public String goGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/prototype/base_grid.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * Extjs Prototype - 검색조건 Grid 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/extjs/prototype/goSearchGrid.do")
	public String goSearchGrid(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/prototype/search_grid.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/extjs/prototype/selectPrototypeDataList.do")
	public @ResponseBody GridVO selectPrototypeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			
			paramMap.put("first_num", start);
			paramMap.put("last_num", end);
			
			if(!initFlag) {
				totalCount = extjsPrototypeService.selectPrototypeDataListCount(paramMap);
				resultList = extjsPrototypeService.selectPrototypeDataList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	/**
	 * 
	 * Extjs Prototype - 트리 기본형 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/extjs/prototype/goBaseTree.do")
	public String goBaseTree(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/extjs/prototype/base_tree.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/sample/extjs/prototype/selectPrototypeTreeData.do")
	public @ResponseBody TreeVO selectPrototypeTreeData(@RequestBody ModelMap paramMap) throws NkiaException {
		TreeVO treeVO = null;
		List resultList = new ArrayList();
		List dataList = new ArrayList();
		try{
			
			resultList = extjsPrototypeService.selectPrototypeTreeData(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expand_level");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, false, expand_level);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return treeVO;
	}
	
	@RequestMapping(value="/itg/sample/ui_template/searchChartData.do")	
	public String searchChartData(ModelMap model) throws NkiaException {
		
		List memoryList = new ArrayList();
		for(int i = 0 ; i < 24 ; i++) {
			memoryList.add(String.valueOf(i));			
		}
				
		String title = "Memory 성능 그래프";
		String[] dataSetName = {"Memory 사용률추이"};
		String[] lineColor = {"8BBA00"};
		HashMap dataMap = new HashMap();
		dataMap.put("memoryList", memoryList);
		
		return "/itg/sample/ui_template/chartData.nvf";
	}
	
	/**
	 * 
	 * Extjs Prototype - 기본 FileUpload Panel형
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/fileuploadPanel.do")
	public String fileuploadPanel(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/fileupload_panel.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * Extjs Prototype - 기본 FileUpload Button형
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/fileuploadButton.do")
	public String fileuploadButton(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/fileupload_button.nvf";
		return forwarPage;
	}
	
	/**
	 * 구성자원조회팝업 자산분류체계  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/goStaticCiAssetTree.do")
	public @ResponseBody TreeVO goStaticCiAssetTree(@RequestBody ModelMap paramMap) throws NkiaException {
		TreeVO treeVO = null;
		List resultList = new ArrayList();
		List dataList = new ArrayList();
		try{
			resultList = extjsPrototypeService.goStaticCiAssetTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return treeVO;
	}
	
	/**
	 * 구성자원조회팝업 자산분류체계  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/goStaticCiAssetCheckTree.do")
	public @ResponseBody CheckTreeVO goStaticCiAssetCheckTree(@RequestBody ModelMap paramMap) throws NkiaException {
		CheckTreeVO treeVO = null;
		List resultList = new ArrayList();
		List dataList = new ArrayList();
		try{
			resultList = extjsPrototypeService.goStaticCiAssetCheckTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return treeVO;
	}
	/**
	 * 구성자원조회팝업 자산리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/setAmAssetGrid.do")
	public @ResponseBody GridVO setAmAssetGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			
			paramMap.put("first_num", start);
			paramMap.put("last_num", end);
			
			if(!initFlag) {
				totalCount = extjsPrototypeService.setAmAssetGridCount(paramMap);
				resultList = extjsPrototypeService.setAmAssetGrid(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	/**
	 * 구성자원조회팝업 자산리스트  
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/getAmAssetGrid.do")
	public @ResponseBody GridVO getAmAssetGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			
			paramMap.put("first_num", start);
			paramMap.put("last_num", end);
			
			if(!initFlag) {
				totalCount = extjsPrototypeService.getAmAssetGridCount(paramMap);
				resultList = extjsPrototypeService.getAmAssetGrid(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setRows(resultList);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	/**
	 * 사용자 기본정보 저장
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/extjs/prototype/goSelectCiServiceInfo.do")
	public @ResponseBody GridVO goSelectCiServiceInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		GridVO gridVO = new GridVO();
		List resultList = new ArrayList();
		try{
			resultList = extjsPrototypeService.goSelectCiServiceInfo(paramMap);
			gridVO.setRows(resultList);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	/**
	 * 사용자 기본정보 저장
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/extjs/prototype/selectAllClassNm.do")
	public @ResponseBody GridVO selectAllClassNm(@RequestBody ModelMap paramMap) throws NkiaException {
		GridVO gridVO = new GridVO();
		List resultList = new ArrayList();
		try{
			resultList = extjsPrototypeService.selectAllClassNm(paramMap);
			gridVO.setRows(resultList);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	/**
	 * 
	 * Gui 디자이너 Preview
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/ui_template/goDesignPreview.do")
	public String goDesignPreview(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/designPreview.nvf";
		return forwarPage;
	}
}
