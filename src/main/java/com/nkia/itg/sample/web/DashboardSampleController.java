package com.nkia.itg.sample.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.sample.service.DashboardSampleService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class DashboardSampleController {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="dashboardSampleService")
	private DashboardSampleService dashboardSampleService;
	
	/**
	 * 요청통계 메인
	 * 
	 * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/sample/dashboard/summary/goServiceRequestMain.do")
	public String goServiceRequestMain(ModelMap paramMap) throws NkiaException {
		
		return "/itg/sample/dashboard/serviceRequestMain.nvf";
	}
	
	/**
	 * 요청통계 > 주간 탭
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/dashboard/goServiceRequestWeekly.do")
	public String goServiceRequestWeekly(
			HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		
		
		return "/itg/sample/dashboard/serviceRequestWeekly.nvf";
	}
	
	/**
	 * 요청통계 > 요청처리 목록
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/support/dashboard/selectReqTypeProcessList.do")
	public @ResponseBody ResultVO selectReqTypeProcessList(
			HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			resultList = dashboardSampleService.selectReqTypeProcessList(paramMap);
			
			gridVO.setTotalCount(resultList.size());
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
}
