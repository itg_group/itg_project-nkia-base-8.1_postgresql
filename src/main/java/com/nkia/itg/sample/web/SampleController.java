/*
 * @(#)SampleCodeController.java              2013. 1. 28.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.nbpm.provide.common.service.NbpmSampleProviderService;
import com.nkia.itg.sample.constraint.SampleEnum;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import egovframework.com.cmm.EgovMessageSource;



@Controller
public class SampleController {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="nbpmSampleProviderService")
	private NbpmSampleProviderService nbpmSampleProviderService;
	
	/**
	 * 5분 대기 하면서 30초에 1번씩 10번을 실행해서 데이터가 등록되었는지 체크
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/sample/searchGetSampleSrApprData.do")
	public @ResponseBody ResultVO searchGetSampleSrApprData(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		//5분대기 - 300,000
		//30초마다 5 
		final int SLEEP_INTERVAL = 30000; //30초
		final int MAX_LOOP_COUNT = 10;
		
		int loopCount = 0;
		
		try {
			while(true) {
				String result = nbpmSampleProviderService.selectSampleSGApprData((String) paramMap.get("sr_id"));
				if (result != null) {
					resultVO.setResultString("SUCCESS");
					return resultVO;
				} else{
					Thread.sleep(SLEEP_INTERVAL);
					loopCount++;
				}
				
				if(loopCount >= MAX_LOOP_COUNT) {
					resultVO.setResultString("FAIL");
					return resultVO;
				}
			}
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
	
	/**
	 * SG 전자결재 팝업 예시
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/sample/goSamplePopup.do")
	public String goSamplePopup(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/interface/sample_if_popup.nvf";
		
		String server_ip = SampleEnum.SG_SERVER_IP.getString();
		String server_port = SampleEnum.SG_SERVER_PORT.getString();
		String server_path = SampleEnum.SG_SERVER_PATH_GET_DATA_SR.getString();
		
		String server_url 		= "http://" + server_ip + ":" + server_port + server_path;
		ClientConfig config 	= new DefaultClientConfig();
		Client client			= Client.create(config);
		
		WebResource webResource = client.resource(server_url);
		try {
			String sr_id = request.getParameter("sr_id");
			paramMap.addAttribute("sr_id", sr_id);
			
			JSONObject requestJson = new JSONObject();
			requestJson.put("sr_id", paramMap.get("sr_id"));
			ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, requestJson.toString());
			String srData = response.getEntity(String.class);
			
			ObjectMapper mapper = new ObjectMapper();
	        Map<String, Object> srDataMap = new HashMap<String, Object>();
	        srDataMap = mapper.readValue(srData, new TypeReference<Map<String, Object>>() {});
	        if ("SUCCESS".equals(srDataMap.get("WS_STATUS"))) {
				JSONArray columnInfo = JSONArray.fromObject(JSONSerializer.toJSON(srDataMap.get("RESULT_DATA")));
				for(int i=0,ii=columnInfo.size();i<ii;i++){
					JSONObject sortMap =  columnInfo.getJSONObject(i);
					paramMap.put("title", sortMap.get("TITLE"));
					paramMap.put("content", sortMap.get("CONTENT"));
					paramMap.put("req_user_id", sortMap.get("REQ_USER_ID"));
				}
				
				JSONArray fileInfo = JSONArray.fromObject(JSONSerializer.toJSON(srDataMap.get("RESULT_ATCH_FILE")));
				paramMap.put("atch_file_list", fileInfo);
	        }
	        
	        String down_url = "http://" + server_ip + ":" + server_port;
	        paramMap.put("down_url", down_url);
	        paramMap.addAttribute("success", "Y");
		} catch(Exception e) {
			paramMap.addAttribute("success", "N");
			return forwarPage;
			//throw new NkiaException(e);
		}
		
		return forwarPage;
	}
	
	/**
	 * SG 전자결재 정보(전자결재번호, 상태) 및 요청번호 데이터를 프로세스 등록 하는 webservice로 전달 
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/nbpm/sample/updateSampleSrApprData.do")
	public void updateSampleSrApprData(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String server_ip = SampleEnum.SG_SERVER_IP.getString();
		String server_port = SampleEnum.SG_SERVER_PORT.getString();
		String server_path = SampleEnum.SG_SERVER_PATH_SEND_DATA_SR.getString();
		
		String server_url 		= "http://" + server_ip + ":" + server_port + server_path;
		ClientConfig config 	= new DefaultClientConfig();
		Client client			= Client.create(config);
		
		WebResource webResource = client.resource(server_url);
		
		try {
			JSONObject requestJson = new JSONObject();
			requestJson.put("sr_id", (String) request.getParameter("sr_id"));
			requestJson.put("sg_appr_no", (String) request.getParameter("sg_appr_no"));
			requestJson.put("sg_appr_status", (String) request.getParameter("sg_appr_status"));
			webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, requestJson.toString());
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		
	}
	
}
