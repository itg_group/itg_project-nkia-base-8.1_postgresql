/*
 * @(#)WorkflowController.java              2013. 2. 1.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.sample.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.sample.service.TestService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * @version 1.0 2013. 2. 1.
 * @author <a href="mailto:kdw0929@nkia.co.kr"> DoWon Kim
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class TestController {
	
	@Resource(name = "testService")
	private TestService testService;
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/test/basicGrid.do")
	public @ResponseBody GridVO searchProcessList(@RequestParam(value="rowCount", required=true) Integer rowCount) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
//			int totalCount = 1000;  
			for(int i=0; i<rowCount; i++)
			{
//				HashMap map = new HashMap();
//				StringBuffer sb = new StringBuffer("company");
//				map.put("company", sb.append(i));
//				
//				double d = Math.round(((Math.random() * 100) + 1) * 100)/100.0 ;
//				map.put("price", d);
//				
//				double d1 = Math.round(((Math.random() * 0.1)) * 100)/100.0 ;
//				map.put("change", d1);
//				
//				double d2 = Math.round(((Math.random())) * 100)/100.0 ;
//				map.put("pctChange", d2);
//				
//				resultList.add(map);
			}
			
			gridVO.setTotalCount(rowCount);
			gridVO.setRows(resultList);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	@RequestMapping(value="/test/infiniteGrid.do", method=RequestMethod.POST)
	public @ResponseBody GridVO infiniteList(/*@RequestBody ModelMap paramMap,*/
												@RequestParam(value="rowCount", required=true) Integer rowCount, 
												HttpServletRequest request, 
												HttpServletResponse response) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		try{
//			int page = (Integer)paramMap.get("page");
//			int limit = (Integer)paramMap.get("limit");
//			int start = (Integer)paramMap.get("start");
//			int end = limit * page;
//			int totalCount = 0;
//			
//			boolean initFlag = false;
//			if(paramMap.get("init") != null) {
//				initFlag = (Boolean)paramMap.get("init");		
//			}
//			
//			
//			paramMap.put("first_num", start + 1);
//			paramMap.put("last_num", end);
//			
//			if(!initFlag) {
//				totalCount = workflowService.searchProcessListCount(paramMap);
//				resultList = workflowService.searchProcessList(paramMap);		
//			}
//			
//			gridVO.setTotalCount(totalCount);
//			gridVO.setRows(resultList);
//			int totalCount = rowCount;
//			int start = Integer.parseInt(request.getParameter("start"));
//			int limit = Integer.parseInt(request.getParameter("limit"));
//			int page = Integer.parseInt(request.getParameter("page"));
//			int end = limit * page;
//			for(int i=start; i<end; i++)
//			{
//				HashMap map = new HashMap();
//				StringBuffer sb = new StringBuffer("company");
//				map.put("company", sb.append(i));
//				
//				double d = Math.round(((Math.random() * 100) + 1) * 100)/100.0 ;
//				map.put("price", d);
//				
//				double d1 = Math.round(((Math.random() * 0.1)) * 100)/100.0 ;
//				map.put("change", d1);
//				
//				double d2 = Math.round(((Math.random())) * 100)/100.0 ;
//				map.put("pctChange", d2);
//				
//				resultList.add(map);
//			}
			
			gridVO.setTotalCount(rowCount);
			gridVO.setRows(resultList);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return gridVO;
	}
	
	@RequestMapping(value="/test/loadTree.do")
	public @ResponseBody TreeVO loadTreeList(/*@RequestBody ModelMap node*/
											/*@RequestParam(value="node", required=true) String node*/
											HttpServletRequest request, 
											HttpServletResponse response) throws NkiaException {
		List resultList = new ArrayList();
		TreeVO root = new TreeVO();
		root.setId("root");
		root.setText("Cygnus");
		root.setExpanded(true);
		
		try{
			int totalCount = 1500;
			for(int i=0; i<totalCount; i++)
			{
				TreeVO treeVO = new TreeVO();
				StringBuffer sb = new StringBuffer("company");
				treeVO.setText(sb.append(i).toString());
				treeVO.setLeaf("false");
				
				resultList.add(treeVO);
			}
			
			root.setChildren(resultList);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return root;
	}
	
	@RequestMapping(value="/test/tree.do")
	public @ResponseBody TreeVO treeList() throws NkiaException {
		List resultList = new ArrayList();
		TreeVO root = new TreeVO();
		root.setId("root");
		root.setText("Cygnus");
		root.setExpanded(true);

		try{
			int totalCount = 10;
			for(int i=0; i<totalCount; i++)
			{
				TreeVO treeVO = new TreeVO();
				StringBuffer sb = new StringBuffer("company");
				treeVO.setText(sb.append(i).toString());
				treeVO.setLeaf("false");
				treeVO.setChildren(getTreeChildren());
//				treeVO.setExpanded(true);
				
				resultList.add(treeVO);
			}
			
			root.setChildren(resultList);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return root;
	}
	
	public List<TreeVO> getTreeChildren() {
		List resultList = new ArrayList();
		
		int totalCount = 1500;
		for(int i=0; i<totalCount; i++)
		{
			TreeVO treeVO = new TreeVO();
			StringBuffer sb = new StringBuffer("company");
			treeVO.setText(sb.append(i).toString());
			treeVO.setLeaf("true");
			
			resultList.add(treeVO);
		}
		
		return resultList;
	}
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/sample/searchSummaryList.do")
	public @ResponseBody ResultVO searchSummaryList(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		GridVO gridVO = new GridVO();
		List summaryList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int page = (Integer)paramMap.get("page");
			int limit = (Integer)paramMap.get("limit");
			int start = (Integer)paramMap.get("start");
			int end = limit * page;
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			
			paramMap.put("first_num", start + 1);
			paramMap.put("last_num", end);
			
			paramMap.put("user_id", userId);
			
			if(!initFlag) {
				resultList = testService.searchSummaryList(paramMap);
			}
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/nbpm/goSummaryListPage.do")
	public String goSummaryListPage(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/sample/ui_template/itgSampleSummaryGrid.nvf";
		return forwarPage;
	}	
	
}
