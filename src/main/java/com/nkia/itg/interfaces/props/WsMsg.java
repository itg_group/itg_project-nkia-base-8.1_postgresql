package com.nkia.itg.interfaces.props;

import java.io.File;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.util.MsgLoader;

public class WsMsg {
	
	private static final Logger logger = LoggerFactory.getLogger(WsMsg.class);
	
	private static MsgLoader props = null;
	
	private final static String PROPS_FILE_NAME = "webservices.properties";
	private final static String CLASS_NAME = "WsMsg.class";
	private final static String CLASS_PATH = "WEB-INF/classes";
	private final static String PROP_FILE_PATH = "egovframework/egovProps";
	
	
    public static void init() {
    	
    	if(props == null) {
    		URL resourceUrl = WsMsg.class.getResource(CLASS_NAME);
    		if (resourceUrl != null) {
    			String path = resourceUrl.getFile();
        		String props_path = path.substring(0, 
        				path.indexOf(CLASS_PATH) + CLASS_PATH.length()) 
        				+ File.separator + PROP_FILE_PATH + File.separator;
        		props = new MsgLoader(props_path + PROPS_FILE_NAME);
    		}
    	}
    }
    
    public static String get(String proName) {
    	String proValue = "";    	
    	
    	try {    	
    		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
    		if(props.aHashMap.get(proName) != null) {
    			if(!"".equals((String)props.aHashMap.get(proName))) {
    				proValue = ((String)props.aHashMap.get(proName)).trim();	   		    			    				
    			}
    		}
		
		} catch (Exception e) {
			logger.info("exception :" + e.getMessage());
		}	
		
		return proValue;
    }
    
    public static Map getMessageMap() {
    	return props.aHashMap;
    }

    /**
     * {0}은 {1}초 이상으로 설정해 주십시오. 형식으로 된 메시지를 치환하여 리턴
     * @param type
     * @param proName
     * @param arguments
     * @return
     */
    public String format(String proName, Object ... arguments){
    	
    	String msg = get(proName);
    	String result = MessageFormat.format(msg,arguments);
    	
    	return result;
    	
    }
}
