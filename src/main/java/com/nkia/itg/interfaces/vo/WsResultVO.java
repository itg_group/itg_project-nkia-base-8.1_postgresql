package com.nkia.itg.interfaces.vo;

public class WsResultVO {
	private String resultMessage;
	private boolean result = false;
	
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
}
