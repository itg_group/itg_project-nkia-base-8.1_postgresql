package com.nkia.itg.interfaces.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsEventBean;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("wsEventDAO")
public class WsEventDAO extends EgovComAbstractDAO {

	public void insertSmsEventLog(WsEventBean wsEventBean) throws NkiaException {
		insert("wsEventDAO.insertSmsEventLog", wsEventBean);
	}

	public Map selectEmsSvIdByCiInfo(String svId) throws NkiaException {
		return (Map)selectByPk("wsEventDAO.selectEmsSvIdByCiInfo", svId);
	}

}
