package com.nkia.itg.interfaces.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("wsCommDAO")
public class WsCommDAO extends EgovComAbstractDAO {

	public void insertIfWebserviceHis(Map hisParamMap) {
		insert("WsCommDAO.insertIfWebserviceHis", hisParamMap);
	}

}
