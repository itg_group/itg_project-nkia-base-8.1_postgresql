package com.nkia.itg.interfaces.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("wsSampleDAO")
public class WsSampleDAO extends EgovComAbstractDAO {

	public List selectSampleSGAppr(Map paramMap) throws NkiaException {
		return list("WsSampleDAO.selectSampleSGAppr", paramMap);
	}
}
