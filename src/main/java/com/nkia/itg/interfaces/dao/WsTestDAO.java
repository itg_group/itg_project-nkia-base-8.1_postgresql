package com.nkia.itg.interfaces.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("wsTestDAO")
public class WsTestDAO extends EgovComAbstractDAO {

	public List selectCodeList(Map paramMap) {
		return list("WsTestDAO.selectCodeList", paramMap);
	}
	
}
