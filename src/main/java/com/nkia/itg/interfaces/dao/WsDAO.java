package com.nkia.itg.interfaces.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("wsDAO")
public class WsDAO extends EgovComAbstractDAO {
	
	/**
	 * 
	 * 클라우드 가상서버 요청정보 조회
	 * 
	 * @param srId
	 * @return
	 * @throws NkiaException
	 */
	public List selectProcCloudSub(String srId) throws NkiaException {
		ModelMap modelMap = new ModelMap();
		modelMap.put("sr_id", srId);
		return list("ProcDetail.selectPROC_CLOUD_SUB", modelMap);
	}

	/**
	 * 
	 * 클라우드 가상서버 Workflow ID 업데이트
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateProcCloudSubByWorkflow(HashMap dataMap) throws NkiaException {
		update("ProcDetail.updatePROC_CLOUD_SUB_WORKFLOW_ID", dataMap);
	}
	
	/**
	 * 
	 * 클라우드 가상서버 Workflow ID 업데이트
	 * 
	 * @param dataMap
	 * @throws NkiaException
	 */
	public void updateProcCloudSubByVmId(HashMap dataMap) throws NkiaException {
		update("ProcDetail.updatePROC_CLOUD_SUB_VM_ID", dataMap);
	}
}
