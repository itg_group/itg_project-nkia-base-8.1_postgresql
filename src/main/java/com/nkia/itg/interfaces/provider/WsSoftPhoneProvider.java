package com.nkia.itg.interfaces.provider;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsSoftPhoneBean;
import com.nkia.itg.interfaces.manager.WsSoftPhoneManager;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;

@Path("/softPhone")
public class WsSoftPhoneProvider {

	private static final Logger logger = LoggerFactory.getLogger(WsSoftPhoneProvider.class);
	
	@POST
	@Consumes("multipart/mixed")
	@Path("/createSrId")
	public Response createSrId(MultiPart multiPart)throws NkiaException{
		logger.debug("[RECEIVE] Client Connect");
		
		List<BodyPart> 	multiPartList 	= (List<BodyPart>)multiPart.getBodyParts();
		try {
			if(multiPartList != null){
				Object cls = null;
				BodyPart bodyPart = (BodyPart)multiPartList.get(0);
				if(bodyPart.getMediaType().equals(MediaType.APPLICATION_XML_TYPE)){
					cls = (Object) bodyPart.getEntityAs(WsSoftPhoneBean.class);
					multiPartList.remove(0);
				}
			}			
				
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return Response.status(201).entity("SUCCESS").build();
	}
	
	@POST
	@Consumes("multipart/mixed")
	@Path("/requestPop")
	public Response pushRequestPop(MultiPart multiPart)throws NkiaException{
		logger.debug("[RECEIVE] Client Connect");
		String result = "";
		List<BodyPart> 	multiPartList 	= (List<BodyPart>)multiPart.getBodyParts();
		WsSoftPhoneManager manager = new WsSoftPhoneManager();
		WsSoftPhoneBean paramObj = new WsSoftPhoneBean();
		try {
			if(multiPartList != null){
				BodyPart bodyPart = (BodyPart)multiPartList.get(0);
				if(bodyPart.getMediaType().equals(MediaType.APPLICATION_XML_TYPE)){
					paramObj = (WsSoftPhoneBean) bodyPart.getEntityAs(WsSoftPhoneBean.class);
					multiPartList.remove(0);
				}
				String srId = manager.createSrId();
				paramObj.setSr_id(srId);
				manager.pushRequestPop(paramObj);
				result = srId;
				paramObj.setSuccess(true);
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return Response.status(Status.OK).entity(paramObj).build();
	}
	
	@POST
	@Consumes("multipart/mixed")
	@Path("/insertCallback")
	public Response insertCallback(MultiPart multiPart)throws NkiaException{
		logger.debug("[RECEIVE] Client Connect");
		
		List<BodyPart> 	multiPartList 	= (List<BodyPart>)multiPart.getBodyParts();
		WsSoftPhoneManager manager = new WsSoftPhoneManager();
		try {
			if(multiPartList != null){
				Object cls = null;
				BodyPart bodyPart = (BodyPart)multiPartList.get(0);
				if(bodyPart.getMediaType().equals(MediaType.APPLICATION_XML_TYPE)){
					cls = (Object) bodyPart.getEntityAs(WsSoftPhoneBean.class);
					multiPartList.remove(0);
				}
				
				manager.insertCallback((WsSoftPhoneBean)cls);				
			}	
				
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return Response.status(201).entity("SUCCESS").build();
	}

}
