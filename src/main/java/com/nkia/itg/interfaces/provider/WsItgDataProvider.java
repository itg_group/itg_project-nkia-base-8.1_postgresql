/*
 * @(#)WsXeusProvider.java              2013. 10. 29.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.provider;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.service.WsService;

@Path("/data")
public class WsItgDataProvider {
	
	/**
	 * 
	 * ITG 분류체계 정보
	 * 
	 * @param classType
	 * @return
	 * @throws NkiaException
	 */
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/getAssetClass")
	public Response provideItgAssetClass(@QueryParam("class_type")final String classType)throws NkiaException {
		String message = "";
		try {
			WsService wsService = (WsService)NkiaApplicationContext.getCtx().getBean("wsService");
			message = wsService.provideItgAssetClass(classType);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return Response.status(Response.Status.ACCEPTED)
				.entity(message).type(MediaType.APPLICATION_XML)
				.build();
		
	}
	
	/**
	 * 
	 * ITG 코드 정보
	 * 
	 * @param classType
	 * @return
	 * @throws NkiaException
	 */
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/getCode")
	public Response provideItgCode(@QueryParam("code_grp_id")final String codeGrpId)throws NkiaException {
		String message = "";
		try {
			WsService wsService = (WsService)NkiaApplicationContext.getCtx().getBean("wsService");
			message = wsService.provideItgCode(codeGrpId);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return Response.status(Response.Status.ACCEPTED)
				.entity(message).type(MediaType.APPLICATION_XML)
				.build();
		
	}
}
