package com.nkia.itg.interfaces.provider;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsEventBean;
import com.nkia.itg.interfaces.manager.WsEventManager;
import com.nkia.itg.interfaces.service.WsService;
import com.nkia.itg.interfaces.util.WsUtil;
import com.nkia.itg.interfaces.vo.WsResultVO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;

@Path("/event")
public class WsEventProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(WsEventProvider.class);
//	private final String INCIDENT_PROCESS_TYPE = "INCDNT";
	private final String INCIDENT_PROCESS_TYPE = "INCIDENT";
	
	WsContext context = new WsContext();
	
	@POST
	//@Consumes(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/regist")
	public Response insertEvent(String jsonString)throws NkiaException{
		logger.debug("[RECEIVE] Client Connect");
		
		String wsMethod = "insertEvent";
		String wsName = "insertEvent";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			
			WsEventManager weManager = new WsEventManager();
			WsUtil wsUtil = new WsUtil();
			WsEventBean wsEventBean = new WsEventBean();

			@Override
			public String executeLogic(String jsonString) throws NkiaException, ParseException, JsonParseException, JsonMappingException, IOException {
				
				if(jsonString != null && !"".equals(jsonString)) {
				
					ObjectMapper mapper = new ObjectMapper();
					
					wsEventBean = mapper.readValue(jsonString, WsEventBean.class);
					WsService wsService = (WsService)NkiaApplicationContext.getCtx().getBean("wsService");
					
					wsService.insertSmsEventLog(wsEventBean);
	
					String srId = "";
					srId = weManager.selectSrId();
					Map ciInfo = weManager.selectCiInfo(wsEventBean.getSv_id());
					
					NbpmProcessVO processVO = new NbpmProcessVO();
					Map param = new HashMap();
					
					//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
					if(srId != null) {
						if(!"".equals(srId)) {
							processVO.setSr_id(srId);
							param.put("sr_id", srId);
						}
					}
					
					
					processVO.setReq_user_id(wsEventBean.getUser_id());
					param.put("user_id", wsEventBean.getUser_id());
					param.put("ins_user_id", wsEventBean.getUser_id());
					
					//서비스데스크가 없는 경우 지정된 장애그룹에 티켓 할당
					String sms_type = NkiaApplicationPropertiesMap.getProperty("Globals.Servicedesk.Use.YN");
					if("N".equals(sms_type)) 
					{
						LinkedHashMap grpOperInfo = new LinkedHashMap();	
						grpOperInfo.put("select_type", "SINGLE");
						grpOperInfo.put("oper_type", "ROLE_OPETR_01");
						
						//케이스1 시작.EMS장애통보대상그룹ID 
						grpOperInfo.put("oper_user_id", NkiaApplicationPropertiesMap.getProperty("Globals.Incident.Grpid"));
						grpOperInfo.put("oper_user_nm", "그룹A");
						grpOperInfo.put("grp_slct_yn", "Y");
						//케이스1 끝
						
						//케이스2 시작.요청자=접수자 또는 직접 user정보를 파라미터로 받는 경우
						//grpOperInfo.put("oper_user_id", wsEventBean.getUser_id());
						//grpOperInfo.put("grp_slct_yn", "N");
						//케이스2 끝
						
						param.put("grpOperInfo", grpOperInfo);
					}
					
					// 엔진
					weManager.setProcessParameter(processVO, param, ciInfo, wsEventBean, INCIDENT_PROCESS_TYPE);
					weManager.insertEvent(processVO, param);
				}
				
				logger.debug("[RECEIVE]	Insert Success " + wsEventBean.getEvt_id());
				
				String resultMsg = "SUCCESS";
				
				return resultMsg;
			}

			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, jsonString);
		
		if(result.isResult()){
			return Response.status(201).entity(result.getResultMessage()).build();
		}else{
			return Response.status(400).entity("FAIL : " + result.getResultMessage()).build();
		}
		
	}
	
	// 이전버전 (multipart 받아서 XML 처리)
	@POST
	@Consumes("multipart/mixed")
	@Path("/registOld")
	public Response insertEvent(MultiPart multiPart)throws NkiaException{
		logger.debug("[RECEIVE] Client Connect");
		
		String wsMethod = "insertEvent";
		String wsName = "insertEvent";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {

			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException, ParseException {
				WsEventManager weManager = new  WsEventManager();
				WsUtil wsUtil = new WsUtil();
				List<BodyPart> 	multiPartList 	= (List<BodyPart>)multiPart.getBodyParts();
				WsEventBean wsEventBean = new WsEventBean();
			
				if(multiPartList != null){
					wsEventBean = (WsEventBean)wsUtil.getWsBean(multiPartList, WsEventBean.class);
					WsService wsService = (WsService)NkiaApplicationContext.getCtx().getBean("wsService");
					wsService.insertSmsEventLog(wsEventBean);

					String srId = weManager.selectSrId();
					Map ciInfo = weManager.selectCiInfo(wsEventBean.getSv_id());
					
					NbpmProcessVO processVO = new NbpmProcessVO();
					Map param = new HashMap();
					processVO.setSr_id(srId);
					param.put("sr_id", srId);
					
					//@@ 20140408 고한조 - EMS에서 장애 이관시 사용자 ID를 추가로 전송해 주도록 되었는데
					// 그로 인해 Bean과 processVO에 사용자 ID 부분 추가함
					processVO.setReq_user_id(wsEventBean.getUser_id());
					param.put("user_id", wsEventBean.getUser_id());
					//@@ 20140827 freechang - 등록자 셋팅
					param.put("ins_user_id", wsEventBean.getUser_id());
					
					weManager.setProcessParameter(processVO, param, ciInfo, wsEventBean, INCIDENT_PROCESS_TYPE);
					
					weManager.insertEvent(processVO, param);
				}

				logger.debug("[RECEIVE]	Insert Success " + wsEventBean.getEvt_id());
				
				String resultMsg = "SUCCESS";
				
				return resultMsg;
			}
			
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, multiPart);
		
		if(result.isResult()){
			return Response.status(201).entity(result.getResultMessage()).build();
		}else{
			return Response.status(400).entity("FAIL : " + result.getResultMessage()).build();
		}
		
	}
}
