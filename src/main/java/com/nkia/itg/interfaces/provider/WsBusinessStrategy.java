package com.nkia.itg.interfaces.provider;

import java.io.IOException;
import java.text.ParseException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.nkia.itg.base.application.exception.NkiaException;
import com.sun.jersey.multipart.MultiPart;

public interface WsBusinessStrategy {

	public String executeLogic(String jsonString) throws NkiaException, ParseException, JsonParseException, JsonMappingException, IOException ;

	public String executeLogic(MultiPart multiPart) throws NkiaException, IOException, Exception;
	
}
