package com.nkia.itg.interfaces.provider;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.ZipUtil;
import com.nkia.itg.base.application.util.file.FileUtils;
import com.nkia.itg.interfaces.beans.WsBpmnXmlDataBean;
import com.nkia.itg.interfaces.beans.WsProcessBean;
import com.nkia.itg.interfaces.beans.WsStatisBean;
import com.nkia.itg.interfaces.beans.WsWorkFlowDeployBean;
import com.nkia.itg.interfaces.manager.WsProcessManager;
import com.nkia.itg.interfaces.provider.WsTestProvider.CALL_STATUS;
import com.nkia.itg.interfaces.vo.WsResultVO;
import com.nkia.itg.nbpm.common.NbpmRuntimeContainer;
import com.nkia.itg.nbpm.process.service.DeployExecutor;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.MultiPart;

@Path("/process")
public class WsProcessProvider {

	private static final Logger logger = LoggerFactory.getLogger(WsProcessProvider.class);

	@Resource(name = "zipUtil")
	public ZipUtil zipUtil;
	
	@Resource(name = "fileUtils")
	public FileUtils fileUtils;
	
	private WsContext context = new WsContext();
	
	public enum CALL_STATUS {
		SUCCESS, FAIL;
	}

	// private static final String processDesignDeployPath =
	// BaseConstraints.Process_Design_Deploy_Path;
	private static String workspacePath = NkiaApplicationPropertiesMap.getProperty("Globals.Workspace.Path");
	private static final String webSourcePath = BaseConstraints.WEB_SOURCE_PATH;
	private static final String processPath = BaseConstraints.NBPM_PROCESS_PATH;
	
	private static final String classesPath = BaseConstraints.CLASSES_PATH;
	private static final String resourcePath = BaseConstraints.RESOURCES_SOURCE_PATH;
	private static final String messageResourcePath = BaseConstraints.MESSAGE_RESOURCE_PATH;
	
	private static final boolean developmentMode = NkiaApplicationPropertiesMap.getPropertyToBoolean("Globals.Development.Mode");
	private static final String previewPath = "preview";
	private static final String procDetailSqlXmlPath = BaseConstraints.NBPM_SQL_FILE_PATH;
	private static final String sourceProcDetailSqlXmlPath = workspacePath + BaseConstraints.NBPM_SQL_SOURCE_FILE_PATH;
	
	public final static String propertiesFileName = "process-itg_ko_KR.properties";

	@POST
	@Consumes("application/json")
	@Path("/byjson")  
	public String getProcessJson(WsProcessBean wsProcessBean) {
		return "Hello, REST!";
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/account/userNum={id}&userName={name}")
	public String printParamValues(@PathParam("id") final int userNum,
			@PathParam("name") final String userName) {
		return "User number : " + userNum + " name : " + userName;
	}

	@POST
	@Consumes("multipart/mixed")
	@Path("/deploy")
	public Response processDesignDeploy(MultiPart multiPart) throws NkiaException {
		// First part contains a WsProcessBean
		
		String wsMethod = "processDesignDeploy";
		String wsName = "processDesignDeploy";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			@Override
			public String executeLogic(MultiPart multiPart) throws Exception {
				
				String sourceDeployPath = this
						.getClass()
						.getResource("")
						.getPath()
						.substring(
								0,
								NbpmRuntimeContainer.class.getResource("").getPath()
										.lastIndexOf("classes"))
						+ "classes" + File.separator;
				String webFileDeployPath = NkiaApplicationContext.getProjectPath();
				WsProcessBean wsProcessBean = (WsProcessBean) multiPart.getBodyParts()
						.get(0).getEntityAs(WsProcessBean.class);
				String projectId = wsProcessBean.getProject_id();
				String packageId = wsProcessBean.getPackage_id();
				String archiveDeployFileName = wsProcessBean.getArchive_file_name();
				
				// Project Folder 생성
				String processFileRootPath = webFileDeployPath + File.separator + "deploy";
				File processFileRoot = new File(processFileRootPath);

				processFileRoot.setExecutable(false,true);
				processFileRoot.setReadable(true);
				processFileRoot.setWritable(false,true);

				if (!processFileRoot.exists() && !processFileRoot.isDirectory()) {
					processFileRoot.mkdirs();
				}

				// Project Folder 생성
				File projectFolder = new File(processFileRootPath + File.separator
						+ projectId);

				projectFolder.setExecutable(false,true);
				projectFolder.setReadable(true);
				projectFolder.setWritable(false,true);

				if (!projectFolder.exists() && !projectFolder.isDirectory()) {
					projectFolder.mkdirs();
				}

				// Package Folder 생성
				File packageFolder = new File(processFileRootPath + File.separator
						+ projectId + File.separator + packageId);

				packageFolder.setExecutable(false,true);
				packageFolder.setReadable(true);
				packageFolder.setWritable(false,true);

				if (!packageFolder.exists() && !packageFolder.isDirectory()) {
					packageFolder.mkdirs();
				}

				// Deploy File
				String deployFile = processFileRootPath + File.separator + projectId
						+ File.separator + packageId + File.separator
						+ archiveDeployFileName;
				
				// Second part contains a archive desing file
				BodyPartEntity filePart = (BodyPartEntity) multiPart.getBodyParts()
						.get(1).getEntity();
				boolean isProcessed = false;
				String message = null;
				InputStream source = filePart.getInputStream();
				File outputFile = new File(deployFile);

				OutputStream outStream = new FileOutputStream(outputFile);
				// 읽어들일 버퍼크기를 메모리에 생성
				byte[] buf = new byte[1024];
				int len = 0;
				// 끝까지 읽어들이면서 File 객체에 내용들을 쓴다
				while ((len = source.read(buf)) > 0) {
					outStream.write(buf, 0, len);
				}
				// Stream 객체를 모두 닫는다.
				outStream.close();
				source.close();

				// Auto Decompress
				if (wsProcessBean.getDecompress_yn().equals("Y")) {
					// 운영 Process Path
					File operProcessPath = new File(webFileDeployPath + processPath + projectId + "/" + wsProcessBean.getProcess_name() + "-" + wsProcessBean.getProcess_version());

					operProcessPath.setExecutable(false,true);
					operProcessPath.setReadable(true);
					operProcessPath.setWritable(false,true);

					if (!operProcessPath.exists() && !operProcessPath.isDirectory()) {
						operProcessPath.mkdirs();
					}
					zipUtil.unzip(outputFile, operProcessPath);
					
					// 소스 폴더 이동
					String[] moveFolderList = {"source", "output", "resource", "schema"};
					if(wsProcessBean.isInclude_source()) {
						for (int i = 0; i < moveFolderList.length; i++) {
							String moveFolder = moveFolderList[i];
							FileUtils.deleteFolder(webFileDeployPath + processPath + projectId + File.separator + moveFolder);
							FileUtils.copyFolder(new File(operProcessPath + File.separator + moveFolder), new File(webFileDeployPath + processPath + projectId + File.separator + moveFolder), null);
						}
					}
					
					// 프로세스 프로퍼티파일 이동
					File operProcessPropertiesFile = new File(operProcessPath + File.separator + propertiesFileName);
					if(operProcessPropertiesFile.isFile()){
						FileUtils.moveFile(operProcessPropertiesFile, sourceDeployPath + messageResourcePath + propertiesFileName);
					}
					
					// 개발모드 일 경우에는 Source Level에도 압축을 풀어준다.
					if (developmentMode) {
						File devProcessPath = new File(workspacePath + webSourcePath + processPath + projectId + "/" + wsProcessBean.getProcess_name() + "-" + wsProcessBean.getProcess_version());

						devProcessPath.setExecutable(false,true);
						devProcessPath.setReadable(true);
						devProcessPath.setWritable(false,true);

						if (!devProcessPath.exists() && !devProcessPath.isDirectory()) {
							devProcessPath.mkdirs();
						}
						zipUtil.unzip(outputFile, devProcessPath);
						
						// 소스 폴더 이동
						if(wsProcessBean.isInclude_source()) {
							for (int i = 0; i < moveFolderList.length; i++) {
								String moveFolder = moveFolderList[i];
								FileUtils.deleteFolder(workspacePath + webSourcePath + processPath + projectId + File.separator + moveFolder);
								FileUtils.copyFolder(new File(devProcessPath + File.separator + moveFolder), new File(workspacePath + webSourcePath + processPath + projectId + File.separator + moveFolder), null);
								FileUtils.deleteFolder(devProcessPath + File.separator + moveFolder);
							}
						}
						
						// 프로세스 프로퍼티파일 이동
						File devProcessPropertiesFile = new File(workspacePath + webSourcePath + processPath + projectId + "/" + wsProcessBean.getProcess_name() + "-" + wsProcessBean.getProcess_version() + File.separator + propertiesFileName);
						if(devProcessPropertiesFile.isFile()){
							FileUtils.moveFile(devProcessPropertiesFile, workspacePath + resourcePath + messageResourcePath + propertiesFileName);
						}
					}
					
					// 소스 폴더 삭제
					for(int i = 0; i < moveFolderList.length; i++){
						String moveFolder = moveFolderList[i];
						FileUtils.deleteFolder(operProcessPath + File.separator + moveFolder);
					}
				}

				String resultMsg = "Attachements processed successfully.";
				
				return resultMsg;
			}

			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				return null;
			}
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, multiPart);
		
		if (result.isResult()) {
			return Response.status(Response.Status.ACCEPTED).entity(result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Failed to process attachments. Reason : " + result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
	}

	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/compareSiteCode")
	public Response siteCodeCompare(String str) {
		// First part contains a WsProcessBean

		String wsMethod = "siteCodeCompare";
		String wsName = "siteCodeCompare";

		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				return null;
			}

			@Override
			public String executeLogic(String str) throws NkiaException {

				String packageId = str;
				String resultMsg = "";
				
				String nbpbSiteCode = NkiaApplicationPropertiesMap.getProperty("Nbpm.itsm.site");

				JSONObject resultJsonObj = new JSONObject();

				if (packageId.equals(nbpbSiteCode)) {
					resultJsonObj.put("resultMsg", "The project id is same");
					resultJsonObj.put("sameSite", "Y");

				} else {
					resultJsonObj.put("resultMsg", "The project id is different.");
					resultJsonObj.put("sameSite", "N");

				}

				return resultJsonObj.toString();// resultMsg;
			}
		};

		WsResultVO result = context.executeWebservice(businessLogic, wsMethod,
				wsName, str);

		if (result.isResult()) {
			return Response.status(Response.Status.ACCEPTED)
					.entity(result.getResultMessage())
					.type(MediaType.TEXT_PLAIN).build();
		}
		return Response
				.status(Response.Status.BAD_REQUEST)
				.entity("Failed to process attachments. Reason : "
						+ result.getResultMessage()).type(MediaType.TEXT_PLAIN)
				.build();
	}
	
	@POST
	@Consumes("multipart/mixed")
	@Path("/preview")
	public Response processDesignPreview(MultiPart multiPart) throws NkiaException {
		// First part contains a WsProcessBean
		
		String wsMethod = "processDesignPreview";
		String wsName = "processDesignPreview";
		
		Map param = new HashMap();
		param.put("parameter", multiPart);
		param.put("parameterType", "MULTIPART");
		
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException, IOException {
				
				String processDesignDeployPath = NkiaApplicationContext.getProjectPath();
				WsProcessBean wsProcessBean = (WsProcessBean) multiPart.getBodyParts().get(0).getEntityAs(WsProcessBean.class);
				String processName = wsProcessBean.getProcess_name();
				String previewFileName = wsProcessBean.getPreview_file_name();
				String previewDate = wsProcessBean.getPreview_date();

				// Project Folder 생성
				String processFileRootPath = processDesignDeployPath;
				
				String previewProcessPath = processFileRootPath + processPath + previewPath;
				String previewDatePath = previewProcessPath + File.separator + previewDate;
				File previewProcessFolder = new File(previewDatePath);

				previewProcessFolder.setExecutable(false,true);
				previewProcessFolder.setReadable(true);
				previewProcessFolder.setWritable(false,true);

				if (!previewProcessFolder.exists() && !previewProcessFolder.isDirectory()) {
					previewProcessFolder.mkdirs();
				}

				// Deploy File
				String previewArchiveFile = previewDatePath + File.separator + previewDate + ".zip";
				
				// Second part contains a archive desing file
				BodyPartEntity filePart = (BodyPartEntity) multiPart.getBodyParts().get(1).getEntity();
				
				InputStream source = filePart.getInputStream();
				File outputFile = new File(previewArchiveFile);

				OutputStream outStream = new FileOutputStream(outputFile);
				// 읽어들일 버퍼크기를 메모리에 생성
				byte[] buf = new byte[1024];
				int len = 0;
				// 끝까지 읽어들이면서 File 객체에 내용들을 쓴다
				while ((len = source.read(buf)) > 0) {
					outStream.write(buf, 0, len);
				}
				// Stream 객체를 모두 닫는다.
				outStream.close();
				source.close();
				
//					if( developmentMode ){
//		  				File devProcessPath = new File(sourceWebPath + nbpmPath + File.separator + "preview" + File.separator + wsProcessBean.getPreview_file_name());
//		  				OutputStream outStream = new FileOutputStream(outputFile);
//		  			}
				
				// Preview 폴더 삭제
				removePreviewFolder(previewProcessPath, previewDate);
				
				// 운영 Process Path
				zipUtil.unzip(outputFile, previewProcessFolder);
				
				// 프로세스 프로퍼티파일 이동
				File previewProcessPropertiesFile = new File(previewProcessFolder + File.separator + propertiesFileName);
				if(previewProcessPropertiesFile.isFile()){
					FileUtils.moveFile(previewProcessPropertiesFile, processDesignDeployPath + classesPath + messageResourcePath + propertiesFileName);
				}

				// 개발모드 일 경우에는 Source Level에도 압축을 풀어준다.
				if (developmentMode) {
					String devPreviewFolder = workspacePath+ webSourcePath + processPath + previewPath;
					String devPreviewPath = devPreviewFolder + File.separator + previewDate;
					File devPreviewSaveFolder = new File(devPreviewPath);

					devPreviewSaveFolder.setExecutable(false,true);
					devPreviewSaveFolder.setReadable(true);
					devPreviewSaveFolder.setWritable(false,true);

					if (!devPreviewSaveFolder.exists() && !devPreviewSaveFolder.isDirectory()) {
						devPreviewSaveFolder.mkdirs();
					}
					
					// Preview 폴더 삭제
					removePreviewFolder(devPreviewFolder, previewDate);
					
					zipUtil.unzip(outputFile, devPreviewSaveFolder);
					
					// 프로세스 프로퍼티파일 이동
					File devProcessPropertiesFile = new File(workspacePath + webSourcePath + processPath + wsProcessBean.getProcess_name() + "-" + wsProcessBean.getProcess_version() + File.separator + propertiesFileName);
					if(devProcessPropertiesFile.isFile()){
						FileUtils.moveFile(devProcessPropertiesFile, workspacePath + resourcePath + messageResourcePath + propertiesFileName);
					}
				}
				
				String resultMsg = "Attachements processed successfully.";
			
				return resultMsg;
			}

			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				return null;
			}
			
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, multiPart);
			
		if (result.isResult()) {
			return Response.status(Response.Status.ACCEPTED).entity(result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Failed to process attachments. Reason : " + result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
	}
	
	/**
	 * 
	 * 현재일 이전의 Preview 폴더를 삭제
	 * 
	 * @param previewPath
	 * @param date
	 */
	private void removePreviewFolder(String previewPath, String date)throws NkiaException{
		File previewFolder = new File(previewPath);
		if (previewFolder.isDirectory() && previewFolder.listFiles().length > 0) {
			File[] sourceFiles = previewFolder.listFiles();
			int sourceFileCount = sourceFiles.length - 1;
			for (int i = sourceFileCount; i >= 0; i--) {
				File sourceFile = sourceFiles[i];
				String folderName = sourceFile.getName();
				if( folderName.indexOf(date.substring(0,8)) == -1 ){
					fileUtils.deleteFolder(sourceFile.getAbsolutePath());
				}
			}
		}
	}

	@POST
	@Consumes("multipart/mixed")
	@Path("/processWorkFlow")
	public Response processWorkFlowDeploy(MultiPart multiPart) throws NkiaException {
		
		String wsMethod = "processWorkFlowDeploy";
		String wsName = "processWorkFlowDeploy";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException, IOException {

				DeployExecutor deployExecutor = (DeployExecutor) NkiaApplicationContext
						.getCtx().getBean("deployCommonExecutor");
				WsWorkFlowDeployBean wsWorkFlowDeployBean = (WsWorkFlowDeployBean) multiPart
						.getBodyParts().get(0)
						.getEntityAs(WsWorkFlowDeployBean.class);
				deployExecutor.deployProcess(wsWorkFlowDeployBean);
			
				String resultMsg = "Deploy successfully.";
				
				return resultMsg;
			}

			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, multiPart);
		
		if (result.isResult()) {
			return Response.status(Response.Status.ACCEPTED).entity(result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Failed to process deploy. Reason : " + result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/sqlMap/getProcDetail")
	public Response getProcessDetailSqlMap() throws NkiaException {
		
		String wsMethod = "getProcessDetailSqlMap";
		String wsName = "getProcessDetailSqlMap";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				return null;
			}

			@Override
			public String executeLogic(String jsonString) throws NkiaException, IOException {
				
				String xml = "";
				String processDesignDeployPath = NkiaApplicationContext.getProjectPath();
					String procSqlFilePath = this
							.getClass()
							.getResource("")
							.getPath()
							.substring(
									0,
									this.getClass().getResource("").getPath()
											.lastIndexOf("classes"))
							+ "classes" + File.separator + procDetailSqlXmlPath; 
					xml = fileUtils.getFileContents(procSqlFilePath);
				
				return xml;
			}
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, "");
		
		return Response.status(201).entity(result.getResultMessage()).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Path("/sqlMap/saveProcDetail")
	public Response saveProcessDetailSqlMap(String xml) {
		
		String wsMethod = "saveProcessDetailSqlMap";
		String wsName = "saveProcessDetailSqlMap";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				return null;
			}

			@Override
			public String executeLogic(String xml) throws NkiaException, IOException {
				String processDesignDeployPath = NkiaApplicationContext.getProjectPath();
				String procSqlFilePath = this
						.getClass()
						.getResource("")
						.getPath()
						.substring(
								0,
								this.getClass().getResource("").getPath()
										.lastIndexOf("classes"))
						+ "classes" + File.separator + procDetailSqlXmlPath; 
				// 개발모드인 경우에는 소스레벨에도 적용해준다.
				if (developmentMode) {
					String sourcePath = sourceProcDetailSqlXmlPath;
					fileUtils.writeToFile(new File(sourcePath), xml);
				}
				
				fileUtils.writeToFile(new File(procSqlFilePath), xml);
				
				String resultMsg = "File is create successfully.";
				
				return resultMsg;
			}
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, xml);
		
		if (result.isResult()) {
			return Response.status(Response.Status.ACCEPTED) .entity(result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Failed to file create. Reason : " + result.getResultMessage()).type(MediaType.TEXT_PLAIN).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/registerStatis")
	public Response registerStatis(WsStatisBean statisBean) {
		
		boolean isProcessed = true;
		String message = "";
		try {
			WsProcessManager manager = new  WsProcessManager();
			isProcessed = manager.registerStatis(statisBean);
			
		} catch (Exception e) {
			isProcessed = false;
			new NkiaException(e);
		}
		if (isProcessed) {
			return Response.status(Response.Status.ACCEPTED).entity("Deploy successfully.").type(MediaType.APPLICATION_JSON).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Failed to process deploy. Reason : " + message).type(MediaType.APPLICATION_JSON).build();
	}
	
	@POST
	@Consumes("multipart/mixed")
	@Path("/selectBpmnXmlData")
	public Response selectBpmnXmlData(MultiPart multiPart) throws NkiaException {
		
		String wsMethod = "selectBpmnXmlData";
		String wsName = "selectBpmnXmlData";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {

			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException, SQLException {
				
				WsBpmnXmlDataBean bpmnXmlDataBean = (WsBpmnXmlDataBean) multiPart.getBodyParts().get(0).getEntityAs(WsBpmnXmlDataBean.class);
				WsProcessManager manager = new  WsProcessManager();
				String message = manager.selectBpmnXmlData(bpmnXmlDataBean);
					
				return message;
			}
			
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, multiPart);
		
		if (result.isResult()) {
			return Response.status(Response.Status.ACCEPTED).entity(result.getResultMessage()).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity("Failed to process attachments. Reason : " + result.getResultMessage()).type(MediaType.WILDCARD_TYPE).build();
	}
}
