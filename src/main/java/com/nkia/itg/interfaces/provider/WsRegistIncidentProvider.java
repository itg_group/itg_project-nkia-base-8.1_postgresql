package com.nkia.itg.interfaces.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.json.JSONObject;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsRegistIncidentBean;
import com.nkia.itg.interfaces.manager.WsRegistManager;
import com.nkia.itg.interfaces.vo.WsResultVO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.sun.jersey.multipart.MultiPart;

@Path("/regist")
public class WsRegistIncidentProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(WsRegistIncidentProvider.class);
	private final String INCIDENT_PROCESS_TYPE = "INCIDENT"; //요청유형
	private final String INCIDENT_QUERY_KEY = "countPROC_INCIDENT,insertPROC_INCIDENT,updatePROC_INCIDENT"; //요청유형에 따른 서브테이블 쿼리키
	private final String SYSTEM_USER = "SYSTEM"; //기본사용자
	private final String ROLE_OPETR_01 = "ROLE_OPETR_01"; //담당자(첫번째 권한을 가진 경우)
	
	PostWorkExecutor processCommonExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
	WsContext context = new WsContext();
	
	private enum WS_STATUS {
		SUCCESS, FAIL;
	}
	
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/incident")
	public Response insertEvent(String jsonString)throws NkiaException{
		
		final String wsMethod = "registIncident"; //이력저장을 위한 웹서비스메소드명
		final String wsName   = "[장애요청]등록";    //이력저장을 위한 웹서비스명
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			
			WsRegistManager wsRegistIncidentManager = new WsRegistManager(); //요청등록을 처리
			WsRegistIncidentBean wsRegistIncidentBean = new WsRegistIncidentBean();          //...
				
			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				JSONObject resultJsonObj = new JSONObject();
				
				try{
					
					if(jsonString != null && !"".equals(jsonString)){
						//WsService wsService = (WsService)NkiaApplicationContext.getCtx().getBean("wsService");
						
						ObjectMapper mapper = new ObjectMapper();
						wsRegistIncidentBean = mapper.readValue(jsonString, WsRegistIncidentBean.class);
						
						Map paramMap = new HashMap();
						paramMap.put("req_type", INCIDENT_PROCESS_TYPE); //요청유형
						paramMap.put("queryKey", INCIDENT_QUERY_KEY);    //서브테이블 쿼리키
						paramMap.put("system_user", SYSTEM_USER);        //기본 담당자 세팅값이 없는 경우를 대비한 값
						
						//[프로퍼티 관리]
						//프로퍼티로 관리는 장애그룹에 담당자 기본 세팅
						String incidentGrpId = NkiaApplicationPropertiesMap.getProperty("Globals.Incident.Grpid");
						if("".equals(incidentGrpId)){
							incidentGrpId = SYSTEM_USER; //기본 담당자 세팅값이 없는 경우를 대비한 값
						}
						
						LinkedHashMap grpOperInfo = new LinkedHashMap();
						grpOperInfo.put("oper_type", ROLE_OPETR_01);
						grpOperInfo.put("oper_user_id", incidentGrpId);
						grpOperInfo.put("grp_slct_yn", "Y");
						paramMap.put("grpOperInfo", grpOperInfo);
						
						String srId = "";
						srId = processCommonExecutor.createSrId();  //새로 생성되는 요청번호
						
						//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
						if(srId != null) {
							if(!"".equals(srId)) {
								paramMap.put("sr_id", srId); //새로 생성되는 요청번호
							}
						}						
						
						//요청자
						if("".equals(wsRegistIncidentBean.getReq_user_id())){
							paramMap.put("req_user_id", "SYSTEM");
						}else{
							paramMap.put("req_user_id", wsRegistIncidentBean.getReq_user_id()); 
						}
						//제목
						paramMap.put("title", wsRegistIncidentBean.getTitle());
						//내용
						paramMap.put("content", wsRegistIncidentBean.getContent());
						//요청일시
						paramMap.put("req_dt", wsRegistIncidentBean.getReq_dt());
						//장애등급(S1 : 1등급, S2 : 2등급, S3 : 3등급, S4 : 4등급)
						paramMap.put("incdnt_grad_cd", wsRegistIncidentBean.getIncdnt_grad_cd());
						//장애발생일시
						paramMap.put("incdnt_occrrnc_dt", wsRegistIncidentBean.getIncdnt_occrrnc_dt());
						//장애인지일시
						paramMap.put("incdnt_recog_dt", wsRegistIncidentBean.getIncdnt_recog_dt());
						//호스트명 -> 시스템메세지
						paramMap.put("sys_mssage_cn", wsRegistIncidentBean.getHost_nm());
						
						//요청등록
						wsRegistIncidentManager.regist(paramMap);
						
						resultJsonObj.put("SR_ID", srId);
						resultJsonObj.put("WS_STATUS", WS_STATUS.SUCCESS);
					}
		
				} catch(Exception e){
					resultJsonObj.put("SR_ID", "");
					resultJsonObj.put("WS_STATUS", WS_STATUS.FAIL);
				}
				
				return resultJsonObj.toString();
			}

			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, jsonString);
		
		return Response.status(result.isResult() ? Response.Status.ACCEPTED : Response.Status.BAD_REQUEST).entity(result.getResultMessage()).build();
		
	}

}
