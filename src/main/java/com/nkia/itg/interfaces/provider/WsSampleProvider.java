package com.nkia.itg.interfaces.provider;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.URLUTF8Encoder;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.interfaces.manager.WsSampleManager;
import com.nkia.itg.interfaces.provider.WsTestProvider.CALL_STATUS;
import com.nkia.itg.interfaces.service.WsCommService;
import com.nkia.itg.interfaces.service.WsSampleService;
import com.nkia.itg.interfaces.service.WsTestService;
import com.nkia.itg.interfaces.vo.WsResultVO;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.sun.jersey.multipart.MultiPart;

@Path("/sample")
public class WsSampleProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(WsSampleProvider.class);

	WsContext context = new WsContext();
	
	WsCommService wsCommService = (WsCommService)NkiaApplicationContext.getCtx().getBean("wsCommService");
	
	/**
	 * 요청번호를 조건으로 데이터 조회
	 * @param jsonString
	 * @return
	 * @throws NkiaException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/selectSrData")
	public Response selectSGEvent(String jsonString) throws NkiaException{
		logger.debug("[RECEIVE] Client Connect");
		
		String wsMethod = "selectSGEvent"; //이력저장을 위한 웹서비스메소드명
		String wsName = "WsSampleProvider"; //이력저장을 위한 웹서비스명
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			
			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				WsSampleService wsSampleService = (WsSampleService)NkiaApplicationContext.getCtx().getBean("wsSampleService"); 
				
				//jsonString 을 Map으로 변환
				Map paramData = JsonMapper.fromJson(jsonString, Map.class);
				
				//대문자키를 소문자로 변환
				Map lowerCaseParamMap = WebUtil.lowerCaseMapKey(paramData);
				
				//요청 조회
				List srData = wsSampleService.selectSampleSGAppr(lowerCaseParamMap);
				
				//요청 파일
				ModelMap fileMap = new ModelMap();
				fileMap.put("sr_id", paramData.get("sr_id"));
				List srFileData = wsSampleService.selectSampleSGApprFileData(fileMap);
				
//				JSONArray resultJsonList = new JSONArray();
//				resultJsonList.addAll(srData);
				
				JSONObject resultJsonObj = new JSONObject();
				resultJsonObj.put("WS_STATUS", CALL_STATUS.SUCCESS);
				resultJsonObj.put("RESULT_COUNT", srData.size());
				resultJsonObj.put("RESULT_DATA", srData);
				resultJsonObj.put("RESULT_ATCH_FILE", srFileData);
				
				return resultJsonObj.toString();
			}

			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}

		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, jsonString);
		
		return Response.status(result.isResult() ? Response.Status.ACCEPTED : Response.Status.BAD_REQUEST).entity(result.getResultMessage()).build();
	}
	
	/**
	 * 임시등록된 데이터를 절차에 등록해주는 API
	 * @param jsonString
	 * @return
	 * @throws NkiaException
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/sendSGApprData")
	public Response sendSGApprData(String jsonString) throws NkiaException {
		logger.debug("[RECEIVE] Client Connect");
		
		String wsMethod = "sendSGApprData";
		String wsName = "WsSampleProvider";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			
			WsSampleManager SGSampleManager = new WsSampleManager();

			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				//jsonString 을 Map으로 변환
				Map paramData = JsonMapper.fromJson(jsonString, Map.class);
				
				//대문자키를 소문자로 변환
				Map lowerCaseParamMap = WebUtil.lowerCaseMapKey(paramData);
				
				String sr_id = (String) lowerCaseParamMap.get("sr_id");
				String sg_appr_no = (String) lowerCaseParamMap.get("sg_appr_no");
				String sg_appr_status = (String) lowerCaseParamMap.get("sg_appr_status");
				
				//임시등록된 요청 데이터 조회
				Map param = WebUtil.lowerCaseMapKey(SGSampleManager.selectTempSrData(sr_id));
				param.put("sg_appr_no", sg_appr_no);
				param.put("sg_appr_status", sg_appr_status);
				
				NbpmProcessVO processVO = new NbpmProcessVO();
				processVO.setSr_id(sr_id);
				
			// 엔진 ===========================================================
				// param 에 있는 데이터와 task 관련 셋팅
				SGSampleManager.setProcessParameter(processVO, param);
				// 엔진실행
				SGSampleManager.updateSGProcessTask(processVO, param);
			// 엔진 ===========================================================
				
				JSONObject resultJsonObj = new JSONObject();
				resultJsonObj.put("WS_STATUS", CALL_STATUS.SUCCESS);
				
				return resultJsonObj.toString();
			}
			
			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, jsonString);
		
		return Response.status(result.isResult() ? Response.Status.ACCEPTED : Response.Status.BAD_REQUEST).entity(result.getResultMessage()).build();
	}
	
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces("application/octet-stream")
	@Path("/downloadSGAtchFile/atch_file_id={atch_file_id}&file_idx={file_idx}")
	public Response downloadSGAtchFile(@PathParam("atch_file_id") String atch_file_id, @PathParam("file_idx") String file_idx) throws NkiaException {
		logger.debug("[RECEIVE] Client Connect");
		
		final String wsMethod = "downloadSGAtchFile"; //이력저장을 위한 웹서비스메소드명
		final String wsName = "WsSampleProvider"; //이력저장을 위한 웹서비스명
		
		//첨부파일 상세 정보 조회
		AtchFileDetailVO achFileBean = new AtchFileDetailVO();
		achFileBean.setAtch_file_id(atch_file_id);
		achFileBean.setFile_idx(file_idx);
		AtchFileService atchFileService = (AtchFileService)NkiaApplicationContext.getCtx().getBean("atchFileService");
		Map fileData = atchFileService.selectAtchFileDetailForService(achFileBean);
		
		final String fileStoreCours = (String) fileData.get("FILE_STORE_COURS");
		final String storeFileName = (String) fileData.get("STORE_FILE_NAME");
		String orignlFileName = (String) fileData.get("ORIGNL_FILE_NAME");
		
		StreamingOutput output = new StreamingOutput() {
	         
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				Map<String, Object> hisParamMap = new HashMap<String, Object>();
				hisParamMap.put("wsMethod", wsMethod);
				hisParamMap.put("wsName", wsName);
				hisParamMap.put("wsMsg", "");
				hisParamMap.put("wsTy", "provider");
				hisParamMap.put("wsResult", 0);
				
				String result = null;
				
				String downloadPath = fileStoreCours + storeFileName;
	    		File file = new File(downloadPath);
	    		
	    		BufferedInputStream bufferedReader = new BufferedInputStream(new FileInputStream(file));
	    		BufferedOutputStream bufferedWriter = new BufferedOutputStream(output);

	    		try {
	    			byte b[] = new byte[4096];
	    			int read = 0;
	    			while ((read = bufferedReader.read(b)) != -1){
	    				bufferedWriter.write(b,0,read);
	    			}
				} catch (Exception e) {
					JSONObject resultJsonObj = new JSONObject();
					resultJsonObj.put("WS_STATUS", "FAIL");
					resultJsonObj.put("RESULT_DATA", e.toString());
					result = resultJsonObj.toString();
					hisParamMap.put("wsResult", 1);
					hisParamMap.put("wsErrorMsg", result);
					
					logger.info("exception :" + e.getMessage());
					
					new NkiaException(e);
				} finally {
					
					//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
					if((hisParamMap != null) && (hisParamMap.isEmpty() == false)) {
						wsCommService.insertIfWebserviceHis(hisParamMap);						
					}
					
	    			try {
	    				if(bufferedReader != null) bufferedReader.close();
	    				if(bufferedWriter != null) bufferedWriter.close();
	    			} catch(Exception ex) {
	    				new NkiaException(ex);
	    			} finally{
	    				if(bufferedReader != null) bufferedReader.close();
	    				if(bufferedWriter != null) bufferedWriter.close();
	    			} 
	    		}
			}
		};
		
		return Response.status(200)
				.entity(output)
		        .header("Content-Disposition", "attachment;filename="+URLUTF8Encoder.encode(orignlFileName)+";")
		        .build();
	}
}
