package com.nkia.itg.interfaces.provider;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.interfaces.service.WsTestService;
import com.nkia.itg.interfaces.vo.WsResultVO;
import com.sun.jersey.multipart.MultiPart;

@Path("/nkia/itam")
public class WsTestProvider {
	
	private WsContext context = new WsContext();
	
	public enum CALL_STATUS {
		SUCCESS, FAIL;
	}
	
	/**
	 * 공통코드 목록 조회
	 * post 버전
	 * @return
	 * @throws NkiaException 
	 */
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Path("/selectCodeList_post")
	public Response selectCodeList_post(String jsonString) throws NkiaException{

		String wsMethod = "selectCodeList_post";
		String wsName = "코드 정보 조회";
		
		WsBusinessStrategy businessLogic = new WsBusinessStrategy() {
			
			//비즈니스 로직 구현
			@Override
			public String executeLogic(String jsonString) throws NkiaException {
				WsTestService wsTestService = (WsTestService)NkiaApplicationContext.getCtx().getBean("wsTestService");
				
				//jsonString 을 Map으로 변환
				Map paramData = JsonMapper.fromJson(jsonString, Map.class);
				//대문자키를 소문자로 변환
				Map lowerCaseParamMap = WebUtil.lowerCaseMapKey(paramData);
				
				//공통코드 조회
				List codeList = wsTestService.selectCodeList(lowerCaseParamMap);
				
				JSONArray resultJsonList = new JSONArray();
				resultJsonList.addAll(codeList);
				
				JSONObject resultJsonObj = new JSONObject();
				resultJsonObj.put("WS_STATUS", CALL_STATUS.SUCCESS);
				resultJsonObj.put("RESULT_COUNT", codeList.size());
				resultJsonObj.put("RESULT_DATA", resultJsonList);

				return resultJsonObj.toString();
			}

			@Override
			public String executeLogic(MultiPart multiPart) throws NkiaException {
				// TODO Auto-generated method stub
				return null;
			}

		};
		
		WsResultVO result = context.executeWebservice(businessLogic, wsMethod, wsName, jsonString);
		
		return Response.status(result.isResult() ? Response.Status.ACCEPTED : Response.Status.BAD_REQUEST).entity(result.getResultMessage()).build();
	}
	
}
