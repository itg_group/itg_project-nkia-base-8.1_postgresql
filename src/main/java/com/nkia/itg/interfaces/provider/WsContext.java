package com.nkia.itg.interfaces.provider;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.JSONObject;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.service.WsCommService;
import com.nkia.itg.interfaces.vo.WsResultVO;
import com.sun.jersey.multipart.MultiPart;

public class WsContext {
	
	private static final Logger logger = LoggerFactory.getLogger(WsContext.class);
	
	WsCommService wsCommService = (WsCommService)NkiaApplicationContext.getCtx().getBean("wsCommService");
	
	public WsResultVO executeWebservice(WsBusinessStrategy businessLogic, String wsMethod, String wsName, String paramString) {

		Map<String, Object> hisParamMap = new HashMap<String, Object>();
		hisParamMap.put("wsMethod", wsMethod);
		hisParamMap.put("wsName", wsName);
		hisParamMap.put("wsMsg", paramString);
		hisParamMap.put("wsTy", "provider");
		hisParamMap.put("wsResult", 0);
		
		WsResultVO resultVO = new WsResultVO();
		String result = null;
		try {

			result = businessLogic.executeLogic(paramString);
			resultVO.setResult(true);
			
		} catch (Exception e) {

			JSONObject resultJsonObj = new JSONObject();
			resultJsonObj.put("WS_STATUS", "FAIL");
			resultJsonObj.put("RESULT_DATA", e.toString());
			
			result = resultJsonObj.toString();
			
			hisParamMap.put("wsResult", 1);
			hisParamMap.put("wsErrorMsg", result);
			
			logger.info("exception :" + e.getMessage());
		} finally {
//			wsCommService.insertIfWebserviceHis(hisParamMap);
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if((hisParamMap != null) && (hisParamMap.isEmpty() == false)) {
				wsCommService.insertIfWebserviceHis(hisParamMap);
			}
		}
		
		resultVO.setResultMessage(result);
		return resultVO;
	}
	
	public WsResultVO executeWebservice(WsBusinessStrategy businessLogic, String wsMethod, String wsName, MultiPart multiPart) throws NkiaException {

		Map<String, Object> hisParamMap = new HashMap<String, Object>();
		hisParamMap.put("wsMethod", wsMethod);
		hisParamMap.put("wsName", wsName);
		hisParamMap.put("wsMsg", "");
		hisParamMap.put("wsTy", "provider");
		hisParamMap.put("wsResult", 0);
		
		WsResultVO resultVO = new WsResultVO();
		String result = null;
		try {

			result = businessLogic.executeLogic(multiPart);
			resultVO.setResult(true);
			
		} catch (Exception e) {

			result = e.getMessage();
			
			hisParamMap.put("wsResult", 1);
			hisParamMap.put("wsErrorMsg", result);
			throw new NkiaException(e);
		} finally {
//			wsCommService.insertIfWebserviceHis(hisParamMap);				
			//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
			if(hisParamMap != null) {
				wsCommService.insertIfWebserviceHis(hisParamMap);				
			}
		}
		
		resultVO.setResultMessage(result);
		return resultVO;
	}
	
}
