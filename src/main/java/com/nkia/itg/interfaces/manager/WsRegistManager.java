package com.nkia.itg.interfaces.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.process.service.ProcessAuthService;



public class WsRegistManager {
	
	PostWorkExecutor processCommonExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
	ProcessAuthService processAuthService = (ProcessAuthService) NkiaApplicationContext.getCtx().getBean("processAuthService");
	
	private final String ROLE_OPETR_01 = "ROLE_OPETR_01"; //담당자(첫번째 권한을 가진 경우)
		
	public void regist(Map paramMap){
		
		try {
			
			String srId = (String)paramMap.get("sr_id");   	   //새로 생성되는 요청번호
			String reqType = (String)paramMap.get("req_type"); //요청유형
			String procAuthId = reqType+'_'+ROLE_OPETR_01;     //프로세스 권한 ID (ex> INCIDENT_ROLE_OPETR_01)
			
			paramMap.put("req_user_id",paramMap.get("req_user_id")); //요청자
			//paramMap.put("ins_user_id", wsRegistIncidentBean.getReq_user_id()); 
			//paramMap.put("upd_user_id", wsRegistIncidentBean.getReq_user_id()); 
			
			NbpmProcessVO processVO = new NbpmProcessVO();
			processVO = processCommonExecutor.selectFinalVersionProcessInfo(reqType);
			processVO.setSr_id(srId);
			processVO.setTask_name(NbpmConstraints.TASK_NAME_START);
			processVO.setReq_user_id((String)paramMap.get("req_user_id"));    //요청자
			processVO.setWorker_user_id((String)paramMap.get("req_user_id")); //시스템상으로 들어가는 요청자(reg_user_id)
			
			Map searchGrpMap = new HashMap();
			Map processAuthMap = new HashMap();
			searchGrpMap.put("proc_auth_id", procAuthId);
			processAuthMap = processAuthService.searchProcessAuthInfo(searchGrpMap);
			
			List nbpmOperList = new ArrayList();
			if(paramMap.get("grpOperInfo")!= null){
				nbpmOperList.add(paramMap.get("grpOperInfo"));	
			
			}else{
				//세팅한 담당자가 없는 경우
				LinkedHashMap grpOperInfo = new LinkedHashMap();
				grpOperInfo.put("oper_type", ROLE_OPETR_01);
				grpOperInfo.put("oper_user_id", paramMap.get("system_user"));
				grpOperInfo.put("grp_slct_yn", "N");
				paramMap.put("grpOperInfo", grpOperInfo);
				nbpmOperList.add(paramMap.get("grpOperInfo"));	
			}
			
			processVO.setOperList(nbpmOperList);
			
			//화면에서 넘어온 담당자 정보 중 없는 BPM User 테이블에 데이터가 있는지 체크하여 등록한다.
			processCommonExecutor.registerUser(processVO, paramMap);
			//요청을 처리한다.
			Map result = processCommonExecutor.executeTask(processVO, paramMap);
			//새로 생성된 타스크들의 알림을 실행한다.
			processCommonExecutor.notifyTask(processVO, result);		
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
