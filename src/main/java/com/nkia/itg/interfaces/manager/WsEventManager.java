package com.nkia.itg.interfaces.manager;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.interfaces.beans.WsEventBean;
import com.nkia.itg.interfaces.dao.WsEventDAO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;


public class WsEventManager {
	
	private final String REQ_PATH_SMS = "EMS";
	//케이스2. 요청자=접수자 인 경우 주석처리
	private final String REQ_USER_ID = "omsadmin";
	
	public void insertEvent(NbpmProcessVO processVO, Map param) throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		//화면에서 넘어온 담당자 정보 중 없는 BPM User 테이블에 데이터가 있는지 체크하여 등록한다.
		processExecutor.registerUser(processVO, param);
		//요청 처리를 한다.
		Map result = processExecutor.executeTask(processVO, param);
		//새로 생성된 타스크들의 알림을 실행한다.
		processExecutor.notifyTask(processVO, result);

	}

	public Map selectCiInfo(String svId) throws NkiaException {
		WsEventDAO wsEventDAO = (WsEventDAO) NkiaApplicationContext.getCtx().getBean("wsEventDAO");
		Map ciInfo = wsEventDAO.selectEmsSvIdByCiInfo(svId);
		Map result = new HashMap();
		
		if(ciInfo != null && !ciInfo.isEmpty()) {
			Iterator keySet = ciInfo.keySet().iterator();
			while(keySet.hasNext()) {
				String key = (String)keySet.next();
				result.put(key.toLowerCase(), ciInfo.get(key));
			}
		}
		return result;
	}

	public String selectSrId() throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		return processExecutor.createSrId();
	}

	public void setProcessParameter(NbpmProcessVO processVO, Map param, Map ciInfo, WsEventBean wsEventBean, String processType) throws NkiaException, ParseException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		NbpmProcessVO processInfo = processExecutor.selectFinalVersionProcessInfo(processType);
		String procVersion = processInfo.getVersion();
		processVO.setProcessId(processInfo.getProcessId());
		processVO.setProcess_name(processInfo.getProcess_name());
		processVO.setProcess_type(processInfo.getProcess_type());
		
		//케이스1. EMS장애통보대상그룹ID
		processVO.setWorker_user_id(REQ_USER_ID);
		//케이스2. 요청자=접수자
		//processVO.setWorker_user_id(processVO.getReq_user_id());
		
		processVO.setTask_name("start");
		processVO.setVersion(procVersion);
		processVO.setWorkType(NbpmConstraints.WORKTYPE_NORMAL_PROC);
		processVO.setReq_user_id(processVO.getReq_user_id());

		
		List nbpmOperList = new ArrayList();
		LinkedHashMap requestorInfo = new LinkedHashMap();
		//케이스1. EMS장애통보대상그룹ID
		requestorInfo.put("oper_user_id", REQ_USER_ID);
		//케이스2. 요청자=접수자
		//requestorInfo.put("oper_user_id", processVO.getReq_user_id());
		
		requestorInfo.put("sr_id", processVO.getSr_id());
		requestorInfo.put("oper_type", NbpmConstraints.REQUESTOR_ROLE);
		nbpmOperList.add(requestorInfo);	

		//서비스데스크가 없는 경우 지정된 장애그룹에 티켓 할당
		if(param.get("grpOperInfo")!=null)
		{
			nbpmOperList.add(param.get("grpOperInfo"));	
		}
		
		processVO.setOperList(nbpmOperList);
		
		
		String custId = (String)ciInfo.get("cust_id");
		if(ciInfo != null){
			List itemList = new ArrayList();
			itemList.add(ciInfo);
			processVO.setItemList(itemList);
		}
		
		String evtDate = null;
		if( wsEventBean.getEvt_date() != null ){
			if( wsEventBean.getEvt_date().length() == 14 ){
				evtDate = DateUtil.changeFormt(wsEventBean.getEvt_date(), "yyyyMMddHHmmss", "yyyy-MM-ddHHmm");
			}else{
				evtDate = DateUtil.changeFormt(wsEventBean.getEvt_date(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-ddHHmm");
			}
		}
		
		String curDate = DateUtil.getDateFormat("yyyy-MM-ddHHmm");
		param.put("queryKey", "insertPROC_INCIDENT");
		param.put("title", getTitle(ciInfo, wsEventBean));
		param.put("req_type", processType);
		param.put("req_zone", custId);
		param.put("req_path", REQ_PATH_SMS);
		param.put("content", getContent(wsEventBean));
		//케이스1. EMS장애통보대상그룹ID
		param.put("req_user_id", REQ_USER_ID);
		//케이스2. 요청자=접수자
		//param.put("req_user_id", processVO.getReq_user_id());
		
		param.put("req_dt", curDate);
		param.put("proc_ver", procVersion);
		param.put("inc_accident_dt", evtDate);
		
		param.put("incdnt_cntrl_se_cd", "001");
		param.put("incdnt_phnomen_cd", "008");
		
	}

	//이벤트 장애요청의 요청내용을 세팅한다.
	private String getContent(WsEventBean wsEventBean) {
		return wsEventBean.getEvt_msg();
	}

	//이벤트 장애요청의 요청제목을 세팅한다.
	private String getTitle(Map ciInfo, WsEventBean wsEventBean) {
		String evt_level = wsEventBean.getSeverity();
		//이벤트 등급( 0 : 정상, 1 : info, 2 : minor, 3 : major, 4 : critical, 5 : down) 
		String evt_level_nm = "NONE";
		if("0".equals(evt_level)){
			evt_level_nm = "정상";
		}else if("1".equals(evt_level)){
			evt_level_nm = "info";
		}else if("2".equals(evt_level)){
			evt_level_nm = "minor";
		}else if("3".equals(evt_level)){
			evt_level_nm = "major";
		}else if("4".equals(evt_level)){
			evt_level_nm = "critical";
		}else if("5".equals(evt_level)){
			evt_level_nm = "down";
		}
		
		String conf_nm = (String)ciInfo.get("conf_nm");
		if(conf_nm == null || "null".equals(conf_nm)) {
			conf_nm = "";
		}
		
		String evt_msg = wsEventBean.getEvt_msg();
		//String evt_nm = wsEventBean.getEvt_nm();
		String title = "["+evt_level_nm+"] "+ conf_nm + " 장애 이벤트 발생";
		
		return title;
	}	
	
}
