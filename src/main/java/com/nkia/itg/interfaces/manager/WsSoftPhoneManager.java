package com.nkia.itg.interfaces.manager;

import java.util.HashMap;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsSoftPhoneBean;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.noti.NbpmPushManager;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.workflow.callback.service.CallbackService;

public class WsSoftPhoneManager {
	public void pushRequestPop(NbpmProcessVO processVO, Map param) throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		processExecutor.executeTask(processVO, param);
	}

	public void pushRequestPop(WsSoftPhoneBean softPhoneBean) {
		NbpmPushManager pushManager = (NbpmPushManager) NkiaApplicationContext.getCtx().getBean("nbpmPushManager");
		Map param = new HashMap();
		param.put("srId", softPhoneBean.getSr_id());
		param.put("hpNo", softPhoneBean.getHp_no());
		param.put("userId", softPhoneBean.getUser_id());
		param.put("remoteIp", softPhoneBean.getRemoteIp());
		param.put("push_type", NbpmConstraints.PUSH_TYPE_SERVICEDESK);
		if(pushManager != null) {
			pushManager.pushTargetUser(param);
		}
	}

	public void insertCallback(WsSoftPhoneBean cls) throws NkiaException {
		CallbackService callBackService = (CallbackService) NkiaApplicationContext.getCtx().getBean("callbackService");
		HashMap insertMap = new HashMap();
		insertMap.put("sr_id", cls.getSr_id());
		insertMap.put("hp_no", cls.getHp_no());
		insertMap.put("sdesk_user_id", cls.getUser_id());
		callBackService.insertCallback(insertMap);
	}

	public String createSrId() throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		return processExecutor.createSrId();
	}
}
