package com.nkia.itg.interfaces.manager;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsBpmnXmlDataBean;
import com.nkia.itg.interfaces.beans.WsStatisBean;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;

public class WsProcessManager {
	
	private static final Logger logger = LoggerFactory.getLogger(WsProcessManager.class);
	
	public boolean registerStatis(WsStatisBean statisBean) throws NkiaException {
		String srId = statisBean.getSr_id();
		
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		String nbpmTaskId = processExecutor.selectLastTaskId(srId);
		NbpmProcessVO paramVO = new NbpmProcessVO();
		paramVO.setSr_id(srId);
		if(nbpmTaskId != null) {
			paramVO.setTask_id(Long.parseLong(nbpmTaskId));
		} else {
			paramVO.setTask_id(0);
		}
		
		NbpmProcessVO processVO = processExecutor.selectTaskPageInfo(paramVO);
		processVO.setWorker_user_id(statisBean.getUser_id());
		processVO.setWorker_user_id(statisBean.getUser_id());
		Map paramMap = new HashMap();
		paramMap.put("statis_result", statisBean.getStatis_result());
		paramMap.put("statis_content", statisBean.getStatis_content());
		paramMap.put("sr_id", srId);
		
		Map result = processExecutor.executeTask(processVO, paramMap);
		
		return true;
	}

	public String selectBpmnXmlData(WsBpmnXmlDataBean bpmnXmlDataBean) throws NkiaException, SQLException {
		NbpmClient nbpmClient = (NbpmClient) NkiaApplicationContext.getCtx().getBean("nbpmCommonClient");
		String processId = bpmnXmlDataBean.getProcessId();
		String xmlData = nbpmClient.selectBpmnXmlData(processId);
		return xmlData;
	}
}
