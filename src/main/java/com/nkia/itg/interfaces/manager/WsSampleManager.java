package com.nkia.itg.interfaces.manager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.interfaces.beans.WsEventBean;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;


public class WsSampleManager {
	
	public Map selectTempSrData(String sr_id) throws NkiaException {
		NbpmClient nbpmClient = (NbpmClient) NkiaApplicationContext.getCtx().getBean("nbpmCommonClient");
		Map srDataMap = nbpmClient.selectTempSrData(sr_id);
		return srDataMap;
	}
	
	public void updateSGProcessTask(NbpmProcessVO processVO, Map param) throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		//화면에서 넘어온 담당자 정보 중 없는 BPM User 테이블에 데이터가 있는지 체크하여 등록한다.
		processExecutor.registerUser(processVO, param);
		//요청 처리를 한다.
		Map result = processExecutor.executeTask(processVO, param);
		//새로 생성된 타스크들의 알림을 실행한다.
		//processExecutor.notifyTask(processVO, result);
	}
	
	public void setProcessParameter(NbpmProcessVO processVO, Map param) throws NkiaException {
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		String processType = (String) param.get("req_type");
		NbpmProcessVO processInfo = processExecutor.selectFinalVersionProcessInfo(processType);
		String procVersion = processInfo.getVersion();
		String reqUserId = (String) param.get("req_user_id");
		processVO.setProcessId(processInfo.getProcessId());
		processVO.setProcess_name(processInfo.getProcess_name());
		processVO.setProcess_type(processInfo.getProcess_type());
		processVO.setWorker_user_id(reqUserId);
		processVO.setTask_name("start");
		processVO.setVersion(procVersion);
		processVO.setWorkType(NbpmConstraints.WORKTYPE_NORMAL_PROC);
		processVO.setReq_user_id(reqUserId);
		//담당자정보(임시저장된 담당자 정보를 조회한 데이터이기 때문에 요청자도 포함되어 있음)
		List nbpmOperList = (List) param.get("oper_list");
		processVO.setOperList(nbpmOperList);
	}
}
