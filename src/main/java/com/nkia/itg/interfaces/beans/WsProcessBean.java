package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class WsProcessBean {
	
	private String project_id;
	private String package_id;
	private String archive_file_name;
	private String process_version;
	private String decompress_yn;
	private boolean include_source;
	
	private String preview_file_name;
	private String preview_date;
	
	/*
	 * 연계정보 START
	 */
	private String workflowID;			//연계ID(제우스 요청번호)
	private String req_type;			//요청유형
	private String req_date;			//요청일
	private String user_id;				//요청자 ID
	private String user_name;			//요청자명
	private String user_corp;			//부처
	private String user_dept;			//부서명
	private String user_mobilephone;	//핸드폰
	private String user_email;			//이메일
	private String vm_zone;				//Zone
	private String vm_cluster;			//자원풀
	private String vm_job;				//업무
	private String vm_name;				//가상서버명
	private String os_type;				//운영체제
	private String hypervisor_type;		//Hypervisor종류 CODE
	private String software;			//소프트웨어
	private String service_date;		//서비스기간
	private String vcore_size;			//vCore
	private String memory_size;			//메모리
	private String storage_size;		//스토리지
	
	private String ip_address;
	private String host_name;
	private String storage_number;
	private String storage_createdate;
	private String req_storage_size;
	private String app_storage_size;
	private String cur_vcore_size;
	private String cur_memory_size;
	private String chg_vcore_size;
	private String chg_memory_size;

	private String append_file;			//첨부파일
	
	private String worker_id;			//1차 승인자 ID
	private String worker_nm;			//1차 승인자 이름
	
	
	private String approval_opnion;		//1차 승인의견
	private String approval_opnion2;	//2차 승인의견
	private String reject_opnion;		//1차 승인의견
	private String reject_opnion2;		//2차 승인의견
	/*
	 * 연계정보 END
	 */
	
	/*
	 * process data START
	 */	
	private String process_name;
	private String process_start;
	private String temporary_save;
	private String role1_name;
	private String role1_auth;
	private String role1;
	private String virtualreq_type;
	private String selection;
	
	private String tran_name;
	private String request_type;
	private String message;
	
	/*
	 * process data END
	 */	
	
	public String getReq_type() {
		return req_type;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getWorkflowID() {
		return workflowID;
	}
	
	public void setWorkflowID(String workflowID) {
		this.workflowID = workflowID;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_corp() {
		return user_corp;
	}
	public void setUser_corp(String user_corp) {
		this.user_corp = user_corp;
	}
	public String getUser_dept() {
		return user_dept;
	}
	public void setUser_dept(String user_dept) {
		this.user_dept = user_dept;
	}
	public String getUser_mobilephone() {
		return user_mobilephone;
	}
	public void setUser_mobilephone(String user_mobilephone) {
		this.user_mobilephone = user_mobilephone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getVm_zone() {
		return vm_zone;
	}
	public void setVm_zone(String vm_zone) {
		this.vm_zone = vm_zone;
	}
	public String getVm_cluster() {
		return vm_cluster;
	}
	public void setVm_cluster(String vm_cluster) {
		this.vm_cluster = vm_cluster;
	}
	public String getVm_job() {
		return vm_job;
	}
	public void setVm_job(String vm_job) {
		this.vm_job = vm_job;
	}
	public String getVm_name() {
		return vm_name;
	}
	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}
	public String getOs_type() {
		return os_type;
	}
	public void setOs_type(String os_type) {
		this.os_type = os_type;
	}
	public String getHypervisor_type() {
		return hypervisor_type;
	}
	public void setHypervisor_type(String hypervisor_type) {
		this.hypervisor_type = hypervisor_type;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getService_date() {
		return service_date;
	}
	public void setService_date(String service_date) {
		this.service_date = service_date;
	}
	public String getVcore_size() {
		return vcore_size;
	}
	public void setVcore_size(String vcore_size) {
		this.vcore_size = vcore_size;
	}
	public String getMemory_size() {
		return memory_size;
	}
	public void setMemory_size(String memory_size) {
		this.memory_size = memory_size;
	}
	public String getStorage_size() {
		return storage_size;
	}
	public void setStorage_size(String storage_size) {
		this.storage_size = storage_size;
	}
	public String getAppend_file() {
		return append_file;
	}
	public void setAppend_file(String append_file) {
		this.append_file = append_file;
	}
	public String getProcess_name() {
		return process_name;
	}
	public void setProcess_name(String process_name) {
		this.process_name = process_name;
	}
	public String getProcess_start() {
		return process_start;
	}
	public void setProcess_start(String process_start) {
		this.process_start = process_start;
	}
	public String getTemporary_save() {
		return temporary_save;
	}
	public void setTemporary_save(String temporary_save) {
		this.temporary_save = temporary_save;
	}
	public String getWorker_id() {
		return worker_id;
	}
	public void setWorker_id(String worker_id) {
		this.worker_id = worker_id;
	}
	public String getRole1_name() {
		return role1_name;
	}
	public void setRole1_name(String role1_name) {
		this.role1_name = role1_name;
	}
	public String getRole1_auth() {
		return role1_auth;
	}
	public void setRole1_auth(String role1_auth) {
		this.role1_auth = role1_auth;
	}
	public String getRole1() {
		return role1;
	}
	public void setRole1(String role1) {
		this.role1 = role1;
	}
	public String getVirtualreq_type() {
		return virtualreq_type;
	}
	public void setVirtualreq_type(String virtualreq_type) {
		this.virtualreq_type = virtualreq_type;
	}
	public String getSelection() {
		return selection;
	}
	public void setSelection(String selection) {
		this.selection = selection;
	}
	public String getWorker_nm() {
		return worker_nm;
	}
	public void setWorker_nm(String worker_nm) {
		this.worker_nm = worker_nm;
	}
	public String getTran_name() {
		return tran_name;
	}
	public void setTran_name(String tran_name) {
		this.tran_name = tran_name;
	}
	public String getRequest_type() {
		return request_type;
	}
	public void setRequest_type(String request_type) {
		this.request_type = request_type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getApproval_opnion() {
		return approval_opnion;
	}
	public void setApproval_opnion(String approval_opnion) {
		this.approval_opnion = approval_opnion;
	}
	public String getApproval_opnion2() {
		return approval_opnion2;
	}
	public void setApproval_opnion2(String approval_opnion2) {
		this.approval_opnion2 = approval_opnion2;
	}
	public String getReject_opnion() {
		return reject_opnion;
	}
	public void setReject_opnion(String reject_opnion) {
		this.reject_opnion = reject_opnion;
	}
	public String getReject_opnion2() {
		return reject_opnion2;
	}
	public void setReject_opnion2(String reject_opnion2) {
		this.reject_opnion2 = reject_opnion2;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getHost_name() {
		return host_name;
	}
	public void setHost_name(String host_name) {
		this.host_name = host_name;
	}
	public String getStorage_number() {
		return storage_number;
	}
	public void setStorage_number(String storage_number) {
		this.storage_number = storage_number;
	}
	public String getStorage_createdate() {
		return storage_createdate;
	}
	public void setStorage_createdate(String storage_createdate) {
		this.storage_createdate = storage_createdate;
	}
	public String getReq_storage_size() {
		return req_storage_size;
	}
	public void setReq_storage_size(String req_storage_size) {
		this.req_storage_size = req_storage_size;
	}
	public String getApp_storage_size() {
		return app_storage_size;
	}
	public void setApp_storage_size(String app_storage_size) {
		this.app_storage_size = app_storage_size;
	}
	public String getCur_vcore_size() {
		return cur_vcore_size;
	}
	public void setCur_vcore_size(String cur_vcore_size) {
		this.cur_vcore_size = cur_vcore_size;
	}
	public String getCur_memory_size() {
		return cur_memory_size;
	}
	public void setCur_memory_size(String cur_memory_size) {
		this.cur_memory_size = cur_memory_size;
	}
	public String getChg_vcore_size() {
		return chg_vcore_size;
	}
	public void setChg_vcore_size(String chg_vcore_size) {
		this.chg_vcore_size = chg_vcore_size;
	}
	public String getChg_memory_size() {
		return chg_memory_size;
	}
	public void setChg_memory_size(String chg_memory_size) {
		this.chg_memory_size = chg_memory_size;
	}
	@Override
	public String toString() {
		return "WsProcessBean [workflowID=" + workflowID + ", req_type="
				+ req_type + ", req_date=" + req_date + ", user_id=" + user_id
				+ ", user_name=" + user_name + ", user_corp=" + user_corp
				+ ", user_dept=" + user_dept + ", user_mobilephone="
				+ user_mobilephone + ", user_email=" + user_email
				+ ", vm_zone=" + vm_zone + ", vm_cluster=" + vm_cluster
				+ ", vm_job=" + vm_job + ", vm_name=" + vm_name + ", os_type="
				+ os_type + ", hypervisor_type=" + hypervisor_type
				+ ", software=" + software + ", service_date=" + service_date
				+ ", vcore_size=" + vcore_size + ", memory_size=" + memory_size
				+ ", storage_size=" + storage_size + ", ip_address="
				+ ip_address + ", host_name=" + host_name + ", storage_number="
				+ storage_number + ", storage_createdate=" + storage_createdate
				+ ", req_storage_size=" + req_storage_size
				+ ", app_storage_size=" + app_storage_size
				+ ", cur_vcore_size=" + cur_vcore_size + ", cur_memory_size="
				+ cur_memory_size + ", chg_vcore_size=" + chg_vcore_size
				+ ", chg_memory_size=" + chg_memory_size + ", append_file="
				+ append_file + ", worker_id=" + worker_id + ", worker_nm="
				+ worker_nm + ", approval_opnion=" + approval_opnion
				+ ", approval_opnion2=" + approval_opnion2 + ", reject_opnion="
				+ reject_opnion + ", reject_opnion2=" + reject_opnion2
				+ ", process_name=" + process_name + ", process_start="
				+ process_start + ", temporary_save=" + temporary_save
				+ ", role1_name=" + role1_name + ", role1_auth=" + role1_auth
				+ ", role1=" + role1 + ", virtualreq_type=" + virtualreq_type
				+ ", selection=" + selection + ", tran_name=" + tran_name
				+ ", request_type=" + request_type + ", message=" + message
				+ "]";
	}
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getPackage_id() {
		return package_id;
	}
	public void setPackage_id(String package_id) {
		this.package_id = package_id;
	}
	public String getArchive_file_name() {
		return archive_file_name;
	}
	public void setArchive_file_name(String archive_file_name) {
		this.archive_file_name = archive_file_name;
	}
	public String getDecompress_yn() {
		return decompress_yn;
	}
	public void setDecompress_yn(String decompress_yn) {
		this.decompress_yn = decompress_yn;
	}
	public boolean isInclude_source() {
		return include_source;
	}
	public void setInclude_source(boolean include_source) {
		this.include_source = include_source;
	}
	public String getProcess_version() {
		return process_version;
	}
	public void setProcess_version(String process_version) {
		this.process_version = process_version;
	}
	public String getPreview_file_name() {
		return preview_file_name;
	}
	public void setPreview_file_name(String preview_file_name) {
		this.preview_file_name = preview_file_name;
	}
	public String getPreview_date() {
		return preview_date;
	}
	public void setPreview_date(String preview_date) {
		this.preview_date = preview_date;
	}
	
	

}
