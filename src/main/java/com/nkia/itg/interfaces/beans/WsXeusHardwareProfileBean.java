package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("hardware_profile")
public class WsXeusHardwareProfileBean {
	
	@XStreamAsAttribute
	private String id;

	@XStreamAsAttribute
	private String name;
	
	@XStreamImplicit(itemFieldName = "property")
	private List<WsXeusPropertyBean> property;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WsXeusPropertyBean> getProperty() {
		return property;
	}

	public void setProperty(List<WsXeusPropertyBean> property) {
		this.property = property;
	}

	
}
