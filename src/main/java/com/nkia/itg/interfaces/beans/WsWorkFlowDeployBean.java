/*
 * @(#)WsProcessWorkFlowBean.java              2013. 6. 13.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WsWorkFlowDeployBean {
	private String fname;
	private String fext;
	private String data;
	private String processType;
	private String processName;
	
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getFext() {
		return fext;
	}
	public void setFext(String fext) {
		this.fext = fext;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
}
