package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("asset_class")
public class WsItgDataAssetClassBean {
	
	private String class_id;
	private String class_nm;
	private String up_class_id;
	
	public String getClass_id() {
		return class_id;
	}
	public void setClass_id(String class_id) {
		this.class_id = class_id;
	}
	public String getClass_nm() {
		return class_nm;
	}
	public void setClass_nm(String class_nm) {
		this.class_nm = class_nm;
	}
	public String getUp_class_id() {
		return up_class_id;
	}
	public void setUp_class_id(String up_class_id) {
		this.up_class_id = up_class_id;
	}

}
