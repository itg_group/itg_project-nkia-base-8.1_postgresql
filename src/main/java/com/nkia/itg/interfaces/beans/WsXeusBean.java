package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "message",
    "size",
    "result",
    "type"
})
@XmlRootElement(name = "workflowAction")
public class WsXeusBean {
	
	/*
	 * 연계정보 START
	 */
	@XmlAttribute
    protected String workflowID;	// 제우스(연계)ID
	
	@XmlElement(required = true)
    protected String message;		// 메시지
    protected long size;			// Size
    protected boolean result;		// 결과
    protected String type;
    
    
    
    /*
	 * 연계정보 END
	 */
    
	public String getWorkflowID() {
		return workflowID;
	}
	public void setWorkflowID(String workflowID) {
		this.workflowID = workflowID;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    
}
