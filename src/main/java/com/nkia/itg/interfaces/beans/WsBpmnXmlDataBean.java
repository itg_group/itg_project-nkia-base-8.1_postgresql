package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="bpmnXmlData")
public class WsBpmnXmlDataBean {
	private String processId;
	private String bpmnXmlData;
	
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	public String getBpmnXmlData() {
		return bpmnXmlData;
	}
	public void setBpmnXmlData(String bpmnXmlData) {
		this.bpmnXmlData = bpmnXmlData;
	}
	
	
	
}
