/*
 * @(#)WsDcaNetworkConfigsBean.java              2014. 1. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("networkConfigs")
public class WsDcaNetworkConfigsBean {
	
	@XStreamImplicit(itemFieldName="networkConfig")
    public List<WsDcaNetworkConfigBean> networkConfig;

	public List<WsDcaNetworkConfigBean> getNetworkConfig() {
		return networkConfig;
	}

	public void setNetworkConfig(List<WsDcaNetworkConfigBean> networkConfig) {
		this.networkConfig = networkConfig;
	}
}
