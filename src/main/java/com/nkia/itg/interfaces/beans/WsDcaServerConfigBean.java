/*
 * @(#)WsDcaServerConfigBean.java              2014. 1. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("serverConfig")
public class WsDcaServerConfigBean {
	
	private String ip;			// 서버 IP
	private String port;		// Agent Port
	private String foreignKey;	// 자산ID
	private String targetManagerIp;	// Target Manager IP
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getForeignKey() {
		return foreignKey;
	}
	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}
	public String getTargetManagerIp() {
		return targetManagerIp;
	}
	public void setTargetManagerIp(String targetManagerIp) {
		this.targetManagerIp = targetManagerIp;
	}
}
