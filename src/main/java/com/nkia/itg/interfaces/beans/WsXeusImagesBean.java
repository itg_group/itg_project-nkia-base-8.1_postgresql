package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("images")
public class WsXeusImagesBean {
	
	@XStreamImplicit(itemFieldName = "image")
	public List<WsXeusImageBean> image;

	public List<WsXeusImageBean> getImage() {
		return image;
	}

	public void setImage(List<WsXeusImageBean> image) {
		this.image = image;
	}

}
