/*
 * @(#)WsDcaServerConfigs.java              2014. 1. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("serverConfigs")
public class WsDcaServerConfigsBean {
	
	@XStreamImplicit(itemFieldName="serverConfig")
    private List<WsDcaServerConfigBean> serverConfig;

	public List<WsDcaServerConfigBean> getServerConfig() {
		return serverConfig;
	}

	public void setServerConfig(List<WsDcaServerConfigBean> serverConfig) {
		this.serverConfig = serverConfig;
	}

}
