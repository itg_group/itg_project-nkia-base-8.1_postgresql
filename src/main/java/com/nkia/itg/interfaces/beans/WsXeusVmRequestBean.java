package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("vm_request")
public class WsXeusVmRequestBean {
	
	private String userID;
    private String networkCodeID;
    private String purposeCodeID;
    private String osCodeID;
    private String testYN;
    private String applID;
    private String imageID;
    private String hardwareProfileID;
    private String vmName;
    private String hostName;
    private String leaseStartDate;
    private String leateExpiredDate;
    
    @XStreamAsAttribute
    private String href;
    
    @XStreamAsAttribute
    private String id;	// 요청ID(SR_ID)

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getNetworkCodeID() {
		return networkCodeID;
	}

	public void setNetworkCodeID(String networkCodeID) {
		this.networkCodeID = networkCodeID;
	}

	public String getPurposeCodeID() {
		return purposeCodeID;
	}

	public void setPurposeCodeID(String purposeCodeID) {
		this.purposeCodeID = purposeCodeID;
	}

	public String getOsCodeID() {
		return osCodeID;
	}

	public void setOsCodeID(String osCodeID) {
		this.osCodeID = osCodeID;
	}

	public String getTestYN() {
		return testYN;
	}

	public void setTestYN(String testYN) {
		this.testYN = testYN;
	}

	public String getApplID() {
		return applID;
	}

	public void setApplID(String applID) {
		this.applID = applID;
	}

	public String getImageID() {
		return imageID;
	}

	public void setImageID(String imageID) {
		this.imageID = imageID;
	}

	public String getHardwareProfileID() {
		return hardwareProfileID;
	}

	public void setHardwareProfileID(String hardwareProfileID) {
		this.hardwareProfileID = hardwareProfileID;
	}

	public String getVmName() {
		return vmName;
	}

	public void setVmName(String vmName) {
		this.vmName = vmName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getLeaseStartDate() {
		return leaseStartDate;
	}

	public void setLeaseStartDate(String leaseStartDate) {
		this.leaseStartDate = leaseStartDate;
	}

	public String getLeateExpiredDate() {
		return leateExpiredDate;
	}

	public void setLeateExpiredDate(String leateExpiredDate) {
		this.leateExpiredDate = leateExpiredDate;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
