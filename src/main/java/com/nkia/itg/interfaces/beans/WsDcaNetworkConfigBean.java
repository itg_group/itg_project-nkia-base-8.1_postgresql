/*
 * @(#)WsDcaNetworkConfigBean.java              2014. 1. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("networkConfig")
public class WsDcaNetworkConfigBean {
	
	private String ip;			// 서버 IP
	private String port;		// SNMP Port
	private String foreignKey;	// 자산ID
	private String community;	// Community
	private String version;		// Version
	private String targetManagerIp; 	// Target Manager IP
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getForeignKey() {
		return foreignKey;
	}
	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTargetManagerIp() {
		return targetManagerIp;
	}
	public void setTargetManagerIp(String targetManagerIp) {
		this.targetManagerIp = targetManagerIp;
	}
}
