package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("asset_classes")
public class WsItgDataAssetClassesBean {
	
	@XStreamImplicit(itemFieldName = "asset_class")
	public List<WsItgDataAssetClassBean> asset_class;

	public List<WsItgDataAssetClassBean> getAsset_class() {
		return asset_class;
	}

	public void setAsset_class(List<WsItgDataAssetClassBean> asset_class) {
		this.asset_class = asset_class;
	}
}
