package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("vm")
public class WsXeusVmBean {
	
	@XStreamAsAttribute
	private String id;
	
	@XStreamAsAttribute
	private String uuid;

	private String agent_key, host_name, ip_address, os_type, cpu_core_cnt, memory_size, disk_size, pm_asset_id;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getAgent_key() {
		return agent_key;
	}

	public void setAgent_key(String agent_key) {
		this.agent_key = agent_key;
	}

	public String getHost_name() {
		return host_name;
	}

	public void setHost_name(String host_name) {
		this.host_name = host_name;
	}

	public String getIp_address() {
		return ip_address;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public String getOs_type() {
		return os_type;
	}

	public void setOs_type(String os_type) {
		this.os_type = os_type;
	}

	public String getCpu_core_cnt() {
		return cpu_core_cnt;
	}

	public void setCpu_core_cnt(String cpu_core_cnt) {
		this.cpu_core_cnt = cpu_core_cnt;
	}

	public String getMemory_size() {
		return memory_size;
	}

	public void setMemory_size(String memory_size) {
		this.memory_size = memory_size;
	}

	public String getDisk_size() {
		return disk_size;
	}

	public void setDisk_size(String disk_size) {
		this.disk_size = disk_size;
	}

	public String getPm_asset_id() {
		return pm_asset_id;
	}

	public void setPm_asset_id(String pm_asset_id) {
		this.pm_asset_id = pm_asset_id;
	}
	
    
    
}
