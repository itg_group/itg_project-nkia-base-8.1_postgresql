/*
 * @(#)WsDcaResultObjects.java              2014. 1. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("resultObjects")
public class WsDcaResultObjectsBean {
	
	@XStreamImplicit(itemFieldName="resultObject")
    public List<WsDcaResultObjectBean> resultObject;

	public List<WsDcaResultObjectBean> getResultObject() {
		return resultObject;
	}

	public void setResultObject(List<WsDcaResultObjectBean> resultObject) {
		this.resultObject = resultObject;
	}
}
