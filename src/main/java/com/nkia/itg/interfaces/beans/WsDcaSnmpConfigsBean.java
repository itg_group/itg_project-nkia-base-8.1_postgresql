/*
 * @(#)WsDcaSnmpConfigs.java              2014. 5. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("snmpConfigs")
public class WsDcaSnmpConfigsBean {
	
	@XStreamImplicit(itemFieldName="snmpConfig")
    public List<WsDcaSnmpConfigBean> snmpConfig;

	public List<WsDcaSnmpConfigBean> getSnmpConfig() {
		return snmpConfig;
	}

	public void setSnmpConfig(List<WsDcaSnmpConfigBean> snmpConfig) {
		this.snmpConfig = snmpConfig;
	}

}	
