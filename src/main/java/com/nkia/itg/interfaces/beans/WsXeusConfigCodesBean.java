package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("config_codes")
public class WsXeusConfigCodesBean {
	
	@XStreamImplicit(itemFieldName = "config_code")
	public List<WsXeusConfigCodeBean> config_code;

	public List<WsXeusConfigCodeBean> getConfig_code() {
		return config_code;
	}

	public void setConfig_code(List<WsXeusConfigCodeBean> config_code) {
		this.config_code = config_code;
	}
    
}
