package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("vm_mod")
public class WsXeusVmModBean {
	
	private String userID;
    private String type;
    private String vmID;
    private String hardwareProfileID;
    private String storageSize;
    
    @XStreamAsAttribute
    private String id;	// 요청ID(SR_ID)

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVmID() {
		return vmID;
	}

	public void setVmID(String vmID) {
		this.vmID = vmID;
	}

	public String getHardwareProfileID() {
		return hardwareProfileID;
	}

	public void setHardwareProfileID(String hardwareProfileID) {
		this.hardwareProfileID = hardwareProfileID;
	}

	public String getStorageSize() {
		return storageSize;
	}

	public void setStorageSize(String storageSize) {
		this.storageSize = storageSize;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
