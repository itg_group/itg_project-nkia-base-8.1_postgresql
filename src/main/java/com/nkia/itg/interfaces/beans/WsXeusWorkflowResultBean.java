package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("workflow_result")
public class WsXeusWorkflowResultBean {
	
	@XStreamAsAttribute
	private String id;

	private String vm_id;
	private String sr_id;
	private String state;
	private String type;
	private String error_message;
	private String asset_id;
	private String agent_key;
	
	@XStreamImplicit(itemFieldName = "vm")
	public List<WsXeusVmBean> vm;
	
	public List<WsXeusVmBean> getVm() {
		return vm;
	}
	public void setVm(List<WsXeusVmBean> vm) {
		this.vm = vm;
	}
	public String getAgent_key() {
		return agent_key;
	}
	public void setAgent_key(String agent_key) {
		this.agent_key = agent_key;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVm_id() {
		return vm_id;
	}
	public void setVm_id(String vm_id) {
		this.vm_id = vm_id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
	public String getAsset_id() {
		return asset_id;
	}
	public void setAsset_id(String asset_id) {
		this.asset_id = asset_id;
	}
	public String getSr_id() {
		return sr_id;
	}
	public void setSr_id(String sr_id) {
		this.sr_id = sr_id;
	}
	
}
