/*
 * @(#)WsDcaResultObjectBean.java              2014. 1. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("resultObject")
public class WsDcaResultObjectBean {
	
	private String id;			// DCA ID
	private String foreignKey;	// 자산ID
	private String status;		// 성공/실패
	private String msg;			// 실패 메시지
	private String targetManagerIp; // Target manager ip
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getForeignKey() {
		return foreignKey;
	}
	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getTargetManagerIp() {
		return targetManagerIp;
	}
	public void setTargetManagerIp(String targetManagerIp) {
		this.targetManagerIp = targetManagerIp;
	}
}
