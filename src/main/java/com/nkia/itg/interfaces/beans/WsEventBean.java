package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class WsEventBean {
	
	private String evt_source;
	private String evt_id;			//이벤트ID
	private String host_nm;			//호스트네임(노드명)
	private String host_ip;
	private String evt_date;		//이벤트 발생일시(YYYY-MM-DD HH24:MI) 
	private String severity;		//이벤트 등급(0 : 정상, 1 : info, 2 : minor, 3 : major, 4 : critical, 5 : down) 
	private String evt_nm;
	private String evt_msg;			//이벤트내용
	private String cust_id;
	private String dept_id;
	private String ci_nm;
	private String ci_id;
	private String work_state;
	private String sv_id;
	private String alias;
	private String user_id;
	
	
	public String getUser_id() {
		return user_id;
	}
	
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getEvt_source() {
		return evt_source;
	}
	
	public void setEvt_source(String evtSource) {
		evt_source = evtSource;
	}
	
	public String getEvt_id() {
		return evt_id;
	}
	
	public void setEvt_id(String evtId) {
		evt_id = evtId;
	}
	
	public String getHost_nm() {
		return host_nm;
	}
	
	public void setHost_nm(String hostNm) {
		host_nm = hostNm;
	}
	
	public String getHost_ip() {
		return host_ip;
	}
	
	public void setHost_ip(String hostIp) {
		host_ip = hostIp;
	}
	
	public String getEvt_date() {
		return evt_date;
	}
	
	public void setEvt_date(String evtDate) {
		evt_date = evtDate;
	}
	
	public String getSeverity() {
		return severity;
	}
	
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	
	public String getEvt_nm() {
		return evt_nm;
	}
	
	public void setEvt_nm(String evtNm) {
		evt_nm = evtNm;
	}
	
	public String getEvt_msg() {
		return evt_msg;
	}
	
	public void setEvt_msg(String evtMsg) {
		evt_msg = evtMsg;
	}
	
	public String getCust_id() {
		return cust_id;
	}
	
	public void setCust_id(String custId) {
		cust_id = custId;
	}

	public String getDept_id() {
		return dept_id;
	}
	
	public void setDept_id(String deptId) {
		dept_id = deptId;
	}
	
	public String getCi_nm() {
		return ci_nm;
	}
	
	public void setCi_nm(String ciNm) {
		ci_nm = ciNm;
	}
	
	public String getCi_id() {
		return ci_id;
	}
	
	public void setCi_id(String ciId) {
		ci_id = ciId;
	}
	
	public String getWork_state() {
		return work_state;
	}
	
	public void setWork_state(String workState) {
		work_state = workState;
	}
	
	public String getSv_id() {
		return sv_id;
	}
	
	public void setSv_id(String svId) {
		sv_id = svId;
	}
	
	public String getAlias() {
		return alias;
	}
	
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
}
