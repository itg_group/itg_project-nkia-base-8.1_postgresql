/*
 * @(#)WsDcaSnmpConfig.java              2014. 5. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("snmpConfig")
public class WsDcaSnmpConfigBean {
	
	private String ip;			// SNMP IP
	private String port;		// SNMP Port
	private String community;	// SNMP Community
	private String version;		// SNMP Version
	private String foreignKey;	// SNMP foreignKey
	private String targetManagerIp;	// Target Manager IP
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTargetManagerIp() {
		return targetManagerIp;
	}
	public void setTargetManagerIp(String targetManagerIp) {
		this.targetManagerIp = targetManagerIp;
	}
	public String getForeignKey() {
		return foreignKey;
	}
	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}
	
	
}
