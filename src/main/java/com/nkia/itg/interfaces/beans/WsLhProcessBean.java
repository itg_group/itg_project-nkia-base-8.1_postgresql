package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="processBean")
public class WsLhProcessBean {
	
	private String title;
	private String content;
	private String req_user_id;
	private String hope_dt;
	private String req_dt;
	private String inc_evt_message;
	private String inc_start_dt;
	private String inc_known_dt;
	private String service_id;
	private String atch_file_nm;

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReq_user_id() {
		return req_user_id;
	}

	public void setReq_user_id(String req_user_id) {
		this.req_user_id = req_user_id;
	}

	public String getHope_dt() {
		return hope_dt;
	}

	public void setHope_dt(String hope_dt) {
		this.hope_dt = hope_dt;
	}

	public String getReq_dt() {
		return req_dt;
	}

	public void setReq_dt(String req_dt) {
		this.req_dt = req_dt;
	}

	public String getInc_evt_message() {
		return inc_evt_message;
	}

	public void setInc_evt_message(String inc_evt_message) {
		this.inc_evt_message = inc_evt_message;
	}

	public String getInc_start_dt() {
		return inc_start_dt;
	}

	public void setInc_start_dt(String inc_start_dt) {
		this.inc_start_dt = inc_start_dt;
	}

	public String getInc_known_dt() {
		return inc_known_dt;
	}

	public void setInc_known_dt(String inc_known_dt) {
		this.inc_known_dt = inc_known_dt;
	}

	public String getService_id() {
		return service_id;
	}

	public void setService_id(String service_id) {
		this.service_id = service_id;
	}

	public String getAtch_file_nm() {
		return atch_file_nm;
	}

	public void setAtch_file_nm(String atch_file_nm) {
		this.atch_file_nm = atch_file_nm;
	}


}
