package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("codes")
public class WsItgDataCodesBean {
	
	@XStreamImplicit(itemFieldName = "code")
	public List<WsItgDataCodeBean> code;

	public List<WsItgDataCodeBean> getCode() {
		return code;
	}

	public void setCode(List<WsItgDataCodeBean> code) {
		this.code = code;
	}
}
