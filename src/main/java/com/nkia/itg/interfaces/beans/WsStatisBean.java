package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WsStatisBean {

	private String sr_id;
	private String user_id;
	private String statis_result;
	private String statis_content;
	
	public String getSr_id() {
		return sr_id;
	}
	public void setSr_id(String sr_id) {
		this.sr_id = sr_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getStatis_result() {
		return statis_result;
	}
	public void setStatis_result(String statis_result) {
		this.statis_result = statis_result;
	}
	public String getStatis_content() {
		return statis_content;
	}
	public void setStatis_content(String statis_content) {
		this.statis_content = statis_content;
	}
}
