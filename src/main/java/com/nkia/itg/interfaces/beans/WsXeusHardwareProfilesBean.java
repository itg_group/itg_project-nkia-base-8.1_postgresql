package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("hardware_profiles")
public class WsXeusHardwareProfilesBean {
	
	@XStreamImplicit(itemFieldName = "hardware_profile")
	public List<WsXeusHardwareProfileBean> hardware_profile;

	public List<WsXeusHardwareProfileBean> getHardware_profile() {
		return hardware_profile;
	}

	public void setHardware_profile(List<WsXeusHardwareProfileBean> hardware_profile) {
		this.hardware_profile = hardware_profile;
	}
}
