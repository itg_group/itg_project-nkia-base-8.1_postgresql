package com.nkia.itg.interfaces.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class WsRegistIncidentBean {
	
	private String req_user_id;	      // 요청자
	private String title;             // 제목
	private String content;           // 장애내용
	private String req_dt;		      // 요청일(YYYY-MM-DDHH24MI)
	private String incdnt_grad_cd;    // 장애등급(S1 : 1등급, S2 : 2등급, S3 : 3등급, S4 : 4등급)
	private String incdnt_occrrnc_dt; // 장애발생일시(YYYY-MM-DDHH24MI)
	private String incdnt_recog_dt;   // 장애인지일시(YYYY-MM-DDHH24MI)
	private String host_nm;           // host명
	
	
	public String getReq_user_id() {
		return req_user_id;
	}
	public void setReq_user_id(String req_user_id) {
		this.req_user_id = req_user_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getReq_dt() {
		return req_dt;
	}
	public void setReq_dt(String req_dt) {
		this.req_dt = req_dt;
	}
	public String getIncdnt_grad_cd() {
		return incdnt_grad_cd;
	}
	public void setIncdnt_grad_cd(String incdnt_grad_cd) {
		this.incdnt_grad_cd = incdnt_grad_cd;
	}
	public String getIncdnt_occrrnc_dt() {
		return incdnt_occrrnc_dt;
	}
	public void setIncdnt_occrrnc_dt(String incdnt_occrrnc_dt) {
		this.incdnt_occrrnc_dt = incdnt_occrrnc_dt;
	}
	public String getIncdnt_recog_dt() {
		return incdnt_recog_dt;
	}
	public void setIncdnt_recog_dt(String incdnt_recog_dt) {
		this.incdnt_recog_dt = incdnt_recog_dt;
	}
	public String getHost_nm() {
		return host_nm;
	}
	public void setHost_nm(String host_nm) {
		this.host_nm = host_nm;
	}
	
	
}
