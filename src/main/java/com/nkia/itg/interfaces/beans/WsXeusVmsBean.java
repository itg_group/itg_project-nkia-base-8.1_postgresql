package com.nkia.itg.interfaces.beans;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("vms")
public class WsXeusVmsBean {
	
	@XStreamImplicit(itemFieldName = "vm")
	public List<WsXeusVmBean> vm;

	public List<WsXeusVmBean> getVm() {
		return vm;
	}

	public void setVm(List<WsXeusVmBean> vm) {
		this.vm = vm;
	}

}
