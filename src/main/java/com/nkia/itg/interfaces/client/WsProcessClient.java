package com.nkia.itg.interfaces.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsProcessBean;

public class WsProcessClient {
	
	public static void requestMethodPostByJson(HttpURLConnection conn)throws NkiaException, IOException{
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json; charset=euc-kr");
//		conn.setRequestProperty("Content-Language", "UTF-8");
//		String input = "{\"title\": 제우스 IP할당 요청" +
//				       ",\"content\":\"2012년 제우스운영서버에 IP할당을 요청합니다.\"" +
//				       ",\"req_user\":\"시스템담당자\"}";
		
		String input = "{\"title\": 1" +
	       ",\"content\":\"2\"" +
	       ",\"req_user\":\"3\"}";
		
		
 
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();
 
		if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ conn.getResponseCode());
		}
 
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
 
		String output;
		
		while ((output = br.readLine()) != null) {
			//System.out.println(output);
		}
 
		conn.disconnect();

	}
	
	public static void requestMethodPostByXml(HttpURLConnection conn)throws NkiaException, IOException, JAXBException{
		try {
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/xml;");
			
			JAXBContext jaxbContext = JAXBContext.newInstance(WsProcessBean.class);
			
			WsProcessBean ws = setTestData();
			/*
			ws.setTitle(URLEncoder.encode("테스트","EUC-KR"));
			ws.setContent("Test112323");
			ws.setReq_user("admin");
			*/
			
			StringWriter wirte = new StringWriter();
			
			jaxbContext.createMarshaller().marshal(ws, wirte);
			
			String input = wirte.toString();
			
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();
 
			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
			}
 
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
 
			String output;
			while ((output = br.readLine()) != null) {
				//System.out.println(output);
			}
 
			conn.disconnect();
		} catch (RuntimeException e) {
			throw new NkiaException(e);
		}

	}
	
	private static WsProcessBean setTestData() throws NkiaException{
		WsProcessBean wsProcessBean = new WsProcessBean();
		//
		// wsProcessBean.setWorkflowID("VM000000001");
		// wsProcessBean.setUser_id("aaaaa");
		// wsProcessBean.setUser_name(URLEncoder.encode("운영자","EUC-KR"));
		// wsProcessBean.setVm_zone("Vm_zone");
		// wsProcessBean.setVm_job("Vm_job");
		// wsProcessBean.setVm_zone("Vm_zone");
		// wsProcessBean.setReq_date("201207051200");
		// wsProcessBean.setSoftware("Software");
		// wsProcessBean.setVm_name("Vm_name");
		// wsProcessBean.setOs_type("Os_type");
		// wsProcessBean.setStorage_size("Storage_size");
		// wsProcessBean.setVm_cluster("Vm_cluster");
		// wsProcessBean.setHypervisor_type("Hypervisor_type");
		// wsProcessBean.setVirtualreq_type("Virtualreq_type");
		// wsProcessBean.setVcore_size("Vcore_size");
		// wsProcessBean.setService_date("Service_date");
		// wsProcessBean.setReq_type("vmc");
		// wsProcessBean.setWorker_id("omsadmin");
		// wsProcessBean.setWorker_nm(URLEncoder.encode("운영자","EUC-KR"));
		
		return wsProcessBean;
	}

	public static void test() {
		
	}
	
	public static void requestMethodGet(HttpURLConnection conn)throws NkiaException, IOException{
		conn.setRequestMethod("GET");
		// conn.setRequestProperty("Accept", "application/text");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn
				.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			//System.out.println(output);
		}

		conn.disconnect();

	}
}
