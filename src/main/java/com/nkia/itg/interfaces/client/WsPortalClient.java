package com.nkia.itg.interfaces.client;

import javax.ws.rs.core.MediaType;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsPortalBean;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;

public class WsPortalClient {

	private static MultiPart multiPart;
	private final static String multi_part = "multipart/mixed";
	private static String resultMsg;
	private String taskName = "";

	public WsPortalClient() {
		multiPart = new MultiPart();
	}	
	
	public WsPortalClient(String taskName) {
		multiPart = new MultiPart();
		this.taskName = taskName;
	}

	public WebResource getResource() throws NkiaException {
		WebResource webResource = null;
		final String server_ip = NkiaApplicationPropertiesMap.getProperty("ws.client.portal.ip");
		final String server_port = NkiaApplicationPropertiesMap.getProperty("ws.client.portal.port");
		StringBuffer stb = null;
		if(NbpmConstraints.TASK_NAME_START.equals(taskName)) {
			stb = new StringBuffer(NkiaApplicationPropertiesMap.getProperty("ws.client.portal.request.path"));
		} else {
			stb = new StringBuffer(NkiaApplicationPropertiesMap.getProperty("ws.client.portal.process.path"));
		}
		final String server_path = stb.toString();
		
		try {
			String server_url = "http://" + server_ip + ":" + server_port
					+ server_path;
			//server_url += server_path_evt;
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);
			webResource = client.resource(server_url);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return webResource;
	}

	public void setResponse(WebResource resource, String mediaType)
			throws NkiaException {
		try {
			ClientResponse response = resource.type(mediaType).post(
					ClientResponse.class, multiPart);
			resultMsg = response.getEntity(String.class);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	}

	public void sendData(WsPortalBean wsPortalBean) throws NkiaException {
		try {
			WebResource resource = getResource();
			setBeanPart(wsPortalBean);
			setResponse(resource, multi_part);
		} catch (Exception e) {
			resultMsg = e.toString();
			throw new NkiaException(e);
		}
	}

	public void setBeanPart(Object obj) throws NkiaException {
		try {
			multiPart.bodyPart(new BodyPart(obj, MediaType.APPLICATION_XML_TYPE));
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	}

	public String getResultMsg() {
		return resultMsg;
	}

/*	public static void main(String[] args) throws NkiaException {

		WsPortalClient wcpe = new WsPortalClient();

		WsPortalBean wsPortalBean = new WsPortalBean();

		wsPortalBean.setSr_id("");
		wsPortalBean.setWorker_id("");

		wcpe.sendData(wsPortalBean);
		wcpe.getResultMsg();

		System.out.println(wcpe.getResultMsg());
	}*/
}
