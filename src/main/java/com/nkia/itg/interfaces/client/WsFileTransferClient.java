/*
 * @(#)WsTestClient.java              2013. 5. 2.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;

import javax.ws.rs.core.MediaType;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsProcessBean;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;

public class WsFileTransferClient {

	public static void requestMethodPostByXml(HttpURLConnection conn)
			throws NkiaException, IOException {
		try {
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/xml;");

			// JAXBContext jaxbContext =
			// JAXBContext.newInstance(WsProcessBean.class);

			// WsProcessBean ws = setTestData();
			/*
			 * ws.setTitle(URLEncoder.encode("테스트","EUC-KR"));
			 * ws.setContent("Test112323"); ws.setReq_user("admin");
			 */

			StringWriter wirte = new StringWriter();

			// jaxbContext.createMarshaller().marshal(ws, wirte);
			String input = wirte.toString();

			// OutputStream os = conn.getOutputStream();
			// os.write(input.getBytes());
			// os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			conn.disconnect();
		} catch (RuntimeException e) {
			throw new NkiaException(e);
		}

	}
}
