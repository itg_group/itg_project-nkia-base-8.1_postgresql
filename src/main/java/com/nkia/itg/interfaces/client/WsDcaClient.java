package com.nkia.itg.interfaces.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.nkia.intergration.polestar.enums.IntergrationPolestarEnums;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.props.WsMsg;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

@Component("wsDcaClient")
public class WsDcaClient {
	
	private String dcaServerIp;
	private String dcaServerPort;
	private String dcaWebserviceItamUrl;			// DCA에서 ITAM으로 제공되는 Webservice URL
	private String dcaItamUri = "/itam";			// DCA ITAM URI
	private String dcaServerRegistUri = "/servers_add";		// DCA 서버등록 suffix URI
	private String dcaServerRemoveUri = "/servers_delete";		// DCA 서버삭제 suffix URI
	private String dcaNetworkRegistUri = "/networks_add";		// DCA 네트워크등록 suffix URI
	private String dcaNetworkRemoveUri = "/networks_delete";	// DCA 네트워크삭제 suffix URI
	private String dcaRequestSysName = "/request_sysName";		// DCA SNMP ConnectionTest
	private String dcaCheckDevice = "/check_device";			// DCA Device 상태체크
	
	private static final Logger logger = LoggerFactory.getLogger(WsDcaClient.class);
	
	public WsDcaClient()throws NkiaException{
		dcaServerIp = IntergrationPolestarEnums.DCA_WEB_SERVER_IP.toString();
		dcaServerPort = IntergrationPolestarEnums.DCA_WEB_SERVER_PORT.toString();
		dcaWebserviceItamUrl = "http://" + dcaServerIp + ":" + dcaServerPort + dcaItamUri;
	}
	
	/**
	 * 
	 * DCA 서버등록
	 * 
	 * @param serverXml
	 * @return
	 * @throws NkiaException
	 */
	public String sendDcaServerRegist(String dcaServerXml)throws NkiaException{
		String resultMsg = null;
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		
		WebResource webResource = client.resource(dcaWebserviceItamUrl + dcaServerRegistUri);
		
		// POST the request
	    ClientResponse response = webResource.type("application/xml").post(ClientResponse.class, dcaServerXml);
	    String resultXml = response.getEntity(String.class);
	    resultMsg = resultXml;
	    if( resultMsg.indexOf("Error") != -1 ){
	    	throw new NkiaException("Webservice server is not connected.");
	    }
	    return resultMsg;
	}
	
	/**
	 * 
	 * DCA 서버삭제
	 * 
	 * @param serverXml
	 * @return
	 * @throws NkiaException
	 */
	public String sendDcaServerRemove(String dcaServerXml)throws NkiaException{
		String resultMsg = null;
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		
		WebResource webResource = client.resource(dcaWebserviceItamUrl + dcaServerRemoveUri);
		
		// POST the request
	    ClientResponse response = webResource.type("application/xml").post(ClientResponse.class, dcaServerXml);
	    String resultXml = response.getEntity(String.class);
	    resultMsg = resultXml;
	    if( resultMsg.indexOf("Error") != -1 ){
	    	throw new NkiaException("Webservice server is not connected.");
	    }
	    return resultMsg;
	}
	
	/**
	 * 
	 * DCA 네트워크등록
	 * 
	 * @param serverXml
	 * @return
	 * @throws NkiaException
	 */
	public String sendDcaNetworkRegist(String dcaNetworkXml)throws NkiaException{
		String resultMsg = null;
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		
		WebResource webResource = client.resource(dcaWebserviceItamUrl + dcaNetworkRegistUri);
		
		// POST the request
	    ClientResponse response = webResource.type("application/xml").post(ClientResponse.class, dcaNetworkXml);
	    String resultXml = response.getEntity(String.class);
	    resultMsg = resultXml;
	    if( resultMsg.indexOf("Error") != -1 ){
	    	throw new NkiaException("Webservice server is not connected.");
	    }
	    return resultMsg;
	}
	
	/**
	 * 
	 * DCA 네트워크삭제
	 * 
	 * @param serverXml
	 * @return
	 * @throws NkiaException
	 */
	public String sendDcaNetworkRemove(String dcaNetworkXml)throws NkiaException{
		String resultMsg = null;
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		
		WebResource webResource = client.resource(dcaWebserviceItamUrl + dcaNetworkRemoveUri);
		
		// POST the request
	    ClientResponse response = webResource.type("application/xml").post(ClientResponse.class, dcaNetworkXml);
	    String resultXml = response.getEntity(String.class);
	    resultMsg = resultXml;
	    if( resultMsg.indexOf("Error") != -1 ){
	    	throw new NkiaException("Webservice server is not connected.");
	    }
	    return resultMsg;
	}
	
	/**
	 * 
	 * DCA SNMP Connection Test
	 * 
	 * @param serverXml
	 * @return
	 * @throws NkiaException
	 */
	public String sendDcaSnmpRequest(String dcaSnmpXml)throws NkiaException{
		String resultMsg = null;
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		
		WebResource webResource = client.resource(dcaWebserviceItamUrl + dcaRequestSysName);
		
		// POST the request
	    ClientResponse response = webResource.type("application/xml").post(ClientResponse.class, dcaSnmpXml);
	    String resultXml = response.getEntity(String.class);
	    resultMsg = resultXml;
	    if( resultMsg.indexOf("Error") != -1 ){
	    	throw new NkiaException("Webservice server is not connected.");
	    }
	    return resultMsg;
	}
	
	public String sendDcaCheckDevice(String deviceId)throws NkiaException{
		String resultMsg = null;
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		
		WebResource webResource = client.resource(dcaWebserviceItamUrl + dcaCheckDevice);
		
		// POST the request
	    ClientResponse response = webResource.type("application/xml").post(ClientResponse.class, deviceId);
	    String resultXml = response.getEntity(String.class);
	    resultMsg = resultXml;
	    if( resultMsg.indexOf("Error") != -1 ){
	    	throw new NkiaException("Webservice server is not connected.");
	    }
	    return resultMsg;
	}
	
}
