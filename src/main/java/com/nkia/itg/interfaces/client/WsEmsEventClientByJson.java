package com.nkia.itg.interfaces.client;

import java.util.HashMap;

import javax.ws.rs.core.MediaType;

import net.sf.json.JSONObject;

import com.nkia.itg.base.application.exception.NkiaException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * EMS 장애연계 테스트용 클라이언트
 */
public class WsEmsEventClientByJson {
	
	public static void main(String[] args) throws NkiaException {
		
		final String server_ip = "192.168.20.6";
		final String server_port = "8012";
		final String server_path = "/webservice/event/regist";
		
		String server_url 		= "http://" + server_ip + ":" + server_port + server_path;
		ClientConfig config 	= new DefaultClientConfig();
		Client client			= Client.create(config);
		WebResource webResource = client.resource(server_url);
		
		// Test data setting ***************************************************************
		
		JSONObject requestJson = new JSONObject();
		
		requestJson.put("user_id", "omsadmin");
		requestJson.put("evt_id", "917");
		requestJson.put("host_ip", "192.168.10.156");
		requestJson.put("severity", "1");
		requestJson.put("host_nm", "cho-PC");
		requestJson.put("evt_msg", "----------Heap 사용률 [41.9 % (> 2 %)]");
		requestJson.put("evt_date", "20170412112738");
		requestJson.put("sv_id", "test_sv_id");
		
		// Test data setting END ***********************************************************
		
//		ClientResponse response = webResource.type(MediaType.TEXT_PLAIN).post(ClientResponse.class, requestJson.toString());
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, requestJson.toString());
		String resultMsg = response.getEntity(String.class);
		
		String wsStatus = "SUCCESS";
		HashMap resultMap = new HashMap(); 
		if(resultMsg.indexOf("FAIL") != -1) {
			wsStatus = "FAIL";
		}
		resultMap.put("result", resultMsg);
		resultMap.put("wsStatus", wsStatus);
		
		
		
	}
}

