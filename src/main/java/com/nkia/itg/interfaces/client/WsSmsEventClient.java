package com.nkia.itg.interfaces.client;

import javax.ws.rs.core.MediaType;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsEventBean;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;

public class WsSmsEventClient {
	
	private static MultiPart multiPart;
	private final static String multi_part		= "multipart/mixed";
	private static String resultMsg;
	
	public WsSmsEventClient(){
		multiPart = new MultiPart();
	}
	
	public WebResource getResource()throws NkiaException{
		WebResource webResource = null;
		try{
			final String server_ip = "192.168.10.98";
			final String server_port = "8082";
			final String server_path = "/webservice/event/regist";
			
			String server_url = "http://"+server_ip+":"+server_port+server_path;
			//server_url += server_path_evt;
			ClientConfig config 	= new DefaultClientConfig();
			Client client			= Client.create(config);
			webResource = client.resource(server_url);
		}catch (Exception e) {
			throw new NkiaException(e);
		}
		return webResource;
	}
	
	public void setResponse(WebResource resource,String mediaType)throws NkiaException{
		try{
			ClientResponse response = resource.type(mediaType).post(ClientResponse.class, multiPart);
			resultMsg = response.getEntity(String.class);
		}catch(Exception e){
			throw new NkiaException(e);
		}
	}

	public void sendData(WsEventBean WsEventBean)throws NkiaException{
		try{
			WebResource resource = getResource();
			setBeanPart(WsEventBean);
			setResponse(resource,multi_part);
		}catch (Exception e) {
			resultMsg = e.toString(); 
			throw new NkiaException(e);
		}
	}
	
	public void setBeanPart(Object obj)throws NkiaException{
		try{
			multiPart.bodyPart(new BodyPart(obj, MediaType.APPLICATION_XML_TYPE));
		}catch (Exception e) {
			throw new NkiaException(e);
		}
	}
	
	public String getResultMsg() {
		return resultMsg;
	}

/*	public static void main(String[] args) throws NkiaException {
		
		WsSmsEventClient wcpe = new WsSmsEventClient();
		
		wcpe = new WsSmsEventClient();
		WsEventBean wsEventBean = new WsEventBean();
		
		//wsEventBean.setSv_id("MA_192.168.232.21_20160329110215");
		
		wsEventBean.setSv_id("test_sv_id");
		
		wsEventBean.setEvt_id("12312344435331");
		wsEventBean.setEvt_msg("JVM 인스턴스 초기화시 라이센스 초과");
		wsEventBean.setEvt_nm("JVM 인스턴스");
		wsEventBean.setEvt_date("20160330102010");
		wsEventBean.setSeverity("1");
		
		wcpe.sendData(wsEventBean);
		wcpe.getResultMsg();
		
		//System.out.println(wcpe.getResultMsg());
	}*/
}

