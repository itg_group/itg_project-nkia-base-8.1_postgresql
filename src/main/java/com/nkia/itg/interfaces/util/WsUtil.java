package com.nkia.itg.interfaces.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.nkia.intergration.polestar.enums.IntergrationPolestarEnums;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.interfaces.beans.WsDcaNetworkConfigBean;
import com.nkia.itg.interfaces.beans.WsDcaNetworkConfigsBean;
import com.nkia.itg.interfaces.beans.WsDcaResultObjectBean;
import com.nkia.itg.interfaces.beans.WsDcaResultObjectsBean;
import com.nkia.itg.interfaces.beans.WsDcaServerConfigBean;
import com.nkia.itg.interfaces.beans.WsDcaServerConfigsBean;
import com.nkia.itg.interfaces.beans.WsDcaSnmpConfigBean;
import com.nkia.itg.interfaces.beans.WsDcaSnmpConfigsBean;
import com.nkia.itg.interfaces.beans.WsItgDataAssetClassBean;
import com.nkia.itg.interfaces.beans.WsItgDataAssetClassesBean;
import com.nkia.itg.interfaces.beans.WsItgDataCodeBean;
import com.nkia.itg.interfaces.beans.WsItgDataCodesBean;
import com.nkia.itg.interfaces.beans.WsXeusConfigCodeBean;
import com.nkia.itg.interfaces.beans.WsXeusConfigCodesBean;
import com.nkia.itg.interfaces.beans.WsXeusHardwareProfileBean;
import com.nkia.itg.interfaces.beans.WsXeusHardwareProfilesBean;
import com.nkia.itg.interfaces.beans.WsXeusImageBean;
import com.nkia.itg.interfaces.beans.WsXeusImagesBean;
import com.nkia.itg.interfaces.beans.WsXeusPropertyBean;
import com.nkia.itg.interfaces.beans.WsXeusVmBean;
import com.nkia.itg.interfaces.beans.WsXeusVmModBean;
import com.nkia.itg.interfaces.beans.WsXeusVmRequestBean;
import com.nkia.itg.interfaces.beans.WsXeusVmsBean;
import com.nkia.itg.interfaces.beans.WsXeusWorkflowResultBean;
import com.sun.jersey.multipart.BodyPart;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

@Component("wsUtil")
public class WsUtil {
	
	private final String xmlTag = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	
	/**
	 * 
	 * @param multiPartList
	 * @param obj
	 * @return
	 * @throws NkiaException
	 */
	public Object getWsBean(List multiPartList,Class beanCls)throws NkiaException{
		Object cls = null;
		try{
			BodyPart bodyPart = (BodyPart)multiPartList.get(0);
			if(bodyPart.getMediaType().equals(MediaType.APPLICATION_XML_TYPE)){
				cls = (Object) bodyPart.getEntityAs(beanCls);
				multiPartList.remove(0);
			}
		}catch(Exception e){
			throw new NkiaException(e);
		}
		return cls;
	}
	
	/**
	 * 
	 * Xeus XStream 초기화
	 * 
	 * @return
	 * @throws NkiaException
	 */
	private XStream initXeusXStream(boolean isReplacer)throws NkiaException{
		XStream xstream = null;
		// underscore 2개가 발생하여 replacer를 넣어줌.
		if( isReplacer ){
			xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("D", "_")));
		}else{
			xstream = new XStream(new DomDriver());
		}
		
		return xstream;
	}
	
	/**
	 * 
	 * 제우스(클라우드) 코드정보 변환
	 * 
	 * @param xml
	 * @return
	 * @throws NkiaException
	 */
	public List parserXeusCodeList(String xml)throws NkiaException{
		List xeusCodeList = new ArrayList();
		XStream xstream = initXeusXStream(false);
		xstream.processAnnotations(WsXeusConfigCodesBean.class);
		xml = xml.replaceAll(xmlTag, "");

		WsXeusConfigCodesBean wsXeusConfigCodesBean = (WsXeusConfigCodesBean)xstream.fromXML(xml);
		List wsXeusConfigCodeList = wsXeusConfigCodesBean.getConfig_code();
		for( int i = 0; i < wsXeusConfigCodeList.size(); i++ ){
			WsXeusConfigCodeBean wsXeusConfigCodeBean = (WsXeusConfigCodeBean)wsXeusConfigCodeList.get(i);
			HashMap codeMap = new HashMap();
			codeMap.put("CODE_ID", wsXeusConfigCodeBean.getId());
			codeMap.put("CODE_TEXT", wsXeusConfigCodeBean.getName());
			xeusCodeList.add(codeMap);
		}
	    return xeusCodeList;
	}
	
	/**
	 * 
	 * 제우스(클라우드) 이미지정보 변환
	 * 
	 * @param xml
	 * @return
	 * @throws NkiaException
	 */
	public List parserXeusImageList(String xml)throws NkiaException {
		List xeusImageList = new ArrayList();
		XStream xstream = initXeusXStream(false);
		xstream.processAnnotations(WsXeusImagesBean.class);
		xml = xml.replaceAll(xmlTag, "");

		WsXeusImagesBean wsXeusImagesBean = (WsXeusImagesBean)xstream.fromXML(xml);
	    List wsXeusImageList = wsXeusImagesBean.getImage();
	    if( wsXeusImageList != null && wsXeusImageList.size() > 0 ){
	    	for( int i = 0; i < wsXeusImageList.size(); i++ ){
		    	WsXeusImageBean wsXeusImageBean = (WsXeusImageBean)wsXeusImageList.get(i);
		    	HashMap listMap = new HashMap();
		    	listMap.put("RNUM", i+1);
		    	listMap.put("ID", wsXeusImageBean.getId());
		    	String descriptions = wsXeusImageBean.getDescription();
		    	String[] description = descriptions.split("_");
		    	listMap.put("OS_TYPE", replaceNullToSpace(description[0]));
		    	listMap.put("OS_UNAME", replaceNullToSpace(description[1]));
		    	listMap.put("OS_VERSION", replaceNullToSpace(description[2]));
		    	listMap.put("PURPOSE", castMultiToComma(replaceNullToSpace(description[3])));
		    	listMap.put("PLAFORM", replaceNullToSpace(description[4]));
		    	listMap.put("OS_BITS", replaceNullToSpace(description[5]));
		    	listMap.put("KERNEL", replaceNullToSpace(description[6]));
		    	listMap.put("LANGUAGE", replaceNullToSpace(description[7]));
		    	listMap.put("SOFTWARE", castMultiToComma(replaceNullToSpace(description[8])));
		    	listMap.put("CLUSTER", replaceNullToSpace(description[9]));
		    	listMap.put("STORAGE", replaceNullToSpace(description[10]));
		    	xeusImageList.add(listMap);
		    }
	    }
	    return xeusImageList;
	}
	
	/**
	 * 
	 * 제우스(클라우드) 하드웨어 프로파일 변환
	 * 
	 * @param xml
	 * @return
	 * @throws NkiaException
	 */
	public List parserXeusHardwareProfileList(String xml)throws NkiaException{
		List xeusHardwareProfileList = new ArrayList();
		XStream xstream = initXeusXStream(false);
		xstream.processAnnotations(WsXeusHardwareProfilesBean.class);
		
		xml = xml.replaceAll(xmlTag, "");

		WsXeusHardwareProfilesBean wsXeusHardwareProfilesBean = (WsXeusHardwareProfilesBean)xstream.fromXML(xml);
	    List wsHardwareProfileList = wsXeusHardwareProfilesBean.getHardware_profile();
	    if( wsHardwareProfileList != null && wsHardwareProfileList.size() > 0 ){
	    	for( int i = 0; i < wsHardwareProfileList.size(); i++ ){
		    	WsXeusHardwareProfileBean wsXeusHardwareProfileBean = (WsXeusHardwareProfileBean)wsHardwareProfileList.get(i);
		    	HashMap listMap = new HashMap();
		    	listMap.put("ID", wsXeusHardwareProfileBean.getId());
		    	listMap.put("NAME", wsXeusHardwareProfileBean.getName());
		    	List propertyList = wsXeusHardwareProfileBean.getProperty();
		    	for( int j = 0; j < propertyList.size(); j++ ){
		    		WsXeusPropertyBean wsXeusPropertyBean = (WsXeusPropertyBean)propertyList.get(j);
		    		String name = wsXeusPropertyBean.getName();
		    		listMap.put(name.toUpperCase(), wsXeusPropertyBean.getValue());
		    	}
		    	xeusHardwareProfileList.add(listMap);
		    }
	    }
	    return xeusHardwareProfileList;
	}
	
	/**
	 * 
	 * 제우스(클라우드) 가상서버 요청 XML로 변환
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String parserXeusVmRequestXml(HashMap dataMap)throws NkiaException {
		XStream xstream = initXeusXStream(true);
		xstream.processAnnotations(WsXeusVmRequestBean.class);
		
		WsXeusVmRequestBean wsXeusVmRequestBean = new WsXeusVmRequestBean();
		wsXeusVmRequestBean.setId((String)dataMap.get("SR_ID"));				// 요청ID
		wsXeusVmRequestBean.setVmName((String)dataMap.get("CLO_VM_NM"));		// 가상서버명
		wsXeusVmRequestBean.setHostName((String)dataMap.get("CLO_HOST_NM"));	// 호스트명
		wsXeusVmRequestBean.setUserID((String)dataMap.get("REQ_USER_ID"));		// 요청자
		wsXeusVmRequestBean.setImageID((String)dataMap.get("CLO_IMAGE"));		// 이미지
		wsXeusVmRequestBean.setHardwareProfileID((String)dataMap.get("CLO_HARDWARE_PROFILE"));	// 하드웨어 프로파일
		wsXeusVmRequestBean.setNetworkCodeID((String)dataMap.get("CLO_NETWORK_CODE"));	// 망코드
		wsXeusVmRequestBean.setPurposeCodeID((String)dataMap.get("CLO_PURPOSE_CODE"));	// 용도코드
		wsXeusVmRequestBean.setOsCodeID((String)dataMap.get("CLO_OS_CODE"));			// OS코드
		wsXeusVmRequestBean.setApplID((String)dataMap.get("CLO_SERVICE_ID"));				// 업무명
		
		String testYn = (String)dataMap.get("CLO_TEST_YN");						// 테스트유무
		if( testYn.equals("0") ){
			testYn = "N";
		}else if( testYn.equals("1") ){
			testYn = "Y";
		}
		wsXeusVmRequestBean.setTestYN(testYn);
		wsXeusVmRequestBean.setLeaseStartDate((String)dataMap.get("CLO_PERIOD_ST_DT_DAY"));		// 임대시작일
		wsXeusVmRequestBean.setLeateExpiredDate((String)dataMap.get("CLO_PERIOD_END_DT_DAY"));	// 임대만료일
		wsXeusVmRequestBean.setHref("");
		
		String vmRequestXml = xstream.toXML(wsXeusVmRequestBean);
		
		return xmlTag + vmRequestXml;
	}
	
	/**
	 * 
	 * 제우스(클라우드) 가상서버 스펙변경/스토리지/삭제요청 XML로 변환
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 */
	public String parserXeusVmModXml(HashMap dataMap)throws NkiaException {
		XStream xstream = initXeusXStream(true);
		xstream.processAnnotations(WsXeusVmModBean.class);
		
		String type = (String)dataMap.get("TYPE");
		WsXeusVmModBean wsXeusVmModBean = new WsXeusVmModBean();
		wsXeusVmModBean.setId((String)dataMap.get("SR_ID"));			// 요청ID
		wsXeusVmModBean.setType(type);			// 요청타입 
		wsXeusVmModBean.setVmID((String)dataMap.get("CLO_VM_ID"));		// VM_ID
		wsXeusVmModBean.setUserID((String)dataMap.get("REQ_USER_ID"));		// 요청자
		
		if( type.equalsIgnoreCase("VMU_SPEC") ){	// 스펙변경시
			wsXeusVmModBean.setHardwareProfileID((String)dataMap.get("CLO_HARDWARE_PROFILE"));	// 하드웨어 프로파일
		}
		if( type.equalsIgnoreCase("VMU_ADD_STORAGE") ){	// 스토리지 변경시
			wsXeusVmModBean.setStorageSize((String)dataMap.get("CLO_ADD_STORAGE"));		// 스토리지 Size
		}
		
		String vmRequestXml = xstream.toXML(wsXeusVmModBean);
		
		return xmlTag + vmRequestXml;
	}
	
	/**
	 * 
	 * 가상서버신청 결과 Parser
	 * 
	 * @param xml
	 * @return
	 * @throws NkiaException
	 */
	public HashMap parserXeusWorkflowResult(String xml)throws NkiaException{
		HashMap parserDataMap = new HashMap();
		XStream xstream = initXeusXStream(true);
		xstream.processAnnotations(WsXeusWorkflowResultBean.class);
		
		WsXeusWorkflowResultBean wXeusWorkflowResultBean = (WsXeusWorkflowResultBean)xstream.fromXML(xml);
		parserDataMap.put("workflow_id", wXeusWorkflowResultBean.getId());
		parserDataMap.put("vm_id", wXeusWorkflowResultBean.getVm_id());
		parserDataMap.put("sr_id", wXeusWorkflowResultBean.getSr_id());
		parserDataMap.put("asset_id", wXeusWorkflowResultBean.getAsset_id());
		parserDataMap.put("ems_id", wXeusWorkflowResultBean.getAgent_key());
		parserDataMap.put("state", wXeusWorkflowResultBean.getState());
		parserDataMap.put("type", wXeusWorkflowResultBean.getType());
		parserDataMap.put("error_message", wXeusWorkflowResultBean.getError_message());
		parserDataMap.put("vm", wXeusWorkflowResultBean.getVm());
		
		return parserDataMap;
	}
	
	/**
	 * 
	 * Xeus 가상화 목록 
	 * 
	 * @param xml
	 * @param xeusOsCodeList
	 * @param reqistVmList
	 * @return
	 * @throws NkiaException
	 */
	public List parserXeusVmList(String xml, List xeusOsCodeList, List reqistVmList)throws NkiaException {
		List xeusVmList = new ArrayList();
		XStream xstream = initXeusXStream(false);
		xstream.processAnnotations(WsXeusVmsBean.class);
		xml = xml.replaceAll(xmlTag, "");

		WsXeusVmsBean wsXeusVmsBean = (WsXeusVmsBean)xstream.fromXML(xml);
		List wsXeusVmList = wsXeusVmsBean.getVm();
		for( int i = 0; i < wsXeusVmList.size(); i++ ){
			boolean isExist = false;
			WsXeusVmBean wsXeusVmBean = (WsXeusVmBean)wsXeusVmList.get(i);
			String vmId = wsXeusVmBean.getId();
			if( reqistVmList != null && reqistVmList.size() > 0 ){
				for( int j = 0; j < reqistVmList.size(); j++ ){
					HashMap registVmData = (HashMap)reqistVmList.get(j);
					if( vmId.equals((String)registVmData.get("VM_ID")) ){
						isExist = true;
						break;
					}
				}
			}
			
			String osType = wsXeusVmBean.getOs_type();
			if( !isExist && osType != null && osType.length() > 0 ){
				HashMap vmMap = new HashMap();
				
				vmMap.put("VM_ID", vmId);
				for( int k = 0; k < xeusOsCodeList.size(); k++ ){
					HashMap osCodeData = (HashMap)xeusOsCodeList.get(k);
					if( osType.equalsIgnoreCase((String)osCodeData.get("CODE_TEXT")) ){
						vmMap.put("OS_CODE", (String)osCodeData.get("CODE_ID"));
					}
				}
				
				vmMap.put("UUID", wsXeusVmBean.getUuid());
				vmMap.put("HOST_NAME", wsXeusVmBean.getHost_name());
				vmMap.put("IP_ADDRESS", wsXeusVmBean.getIp_address());
				vmMap.put("CPU_CORE_CNT", wsXeusVmBean.getCpu_core_cnt());
				vmMap.put("MEMORY_SIZE", wsXeusVmBean.getMemory_size());
				vmMap.put("DISK_SIZE", wsXeusVmBean.getDisk_size());
				vmMap.put("PM_ASSET_ID", wsXeusVmBean.getPm_asset_id());
				xeusVmList.add(vmMap);
			}
		}
	    return xeusVmList;
	}
	
	/**
	 * 
	 * ITG 자산분류체계 목록
	 * 
	 * @param amdbClassList
	 * @return
	 * @throws NkiaException
	 */
	public String parserItgAmdbClassToXml(List amdbClassList)throws NkiaException {
		XStream xstream = initXeusXStream(true); 
		xstream.processAnnotations(WsItgDataAssetClassesBean.class);
		
		WsItgDataAssetClassesBean wsItgDataAssetClassesBean = new WsItgDataAssetClassesBean();
		wsItgDataAssetClassesBean.setAsset_class(new ArrayList());
		
		for( int i = 0; i < amdbClassList.size(); i++ ){
			TreeVO amdbClassData = (TreeVO)amdbClassList.get(i);
			WsItgDataAssetClassBean wsItgDataAssetClassBean = new WsItgDataAssetClassBean();
			wsItgDataAssetClassBean.setClass_id((String)amdbClassData.getId());
			wsItgDataAssetClassBean.setClass_nm((String)amdbClassData.getText());
			wsItgDataAssetClassBean.setUp_class_id((String)amdbClassData.getUp_node_id());
			wsItgDataAssetClassesBean.getAsset_class().add(wsItgDataAssetClassBean);
		}
		
		String itgAmdbClassXml = xstream.toXML(wsItgDataAssetClassesBean);
		 
		return xmlTag + "\n" + itgAmdbClassXml;
	}
	
	/**
	 * 
	 * ITG 코드 목록
	 * 
	 * @param codeList
	 * @return
	 * @throws NkiaException
	 */
	public String parserItgCodeToXml(List codeList)throws NkiaException {
		XStream xstream = initXeusXStream(true); 
		xstream.processAnnotations(WsItgDataCodesBean.class);
		
		WsItgDataCodesBean wsItgDataCodesBean = new WsItgDataCodesBean();
		wsItgDataCodesBean.setCode(new ArrayList());
		
		for( int i = 0; i < codeList.size(); i++ ){
			HashMap codeData = (HashMap)codeList.get(i);
			WsItgDataCodeBean wsItgDataCodeBean = new WsItgDataCodeBean();
			wsItgDataCodeBean.setCode_id((String)codeData.get("CODE_ID"));
			wsItgDataCodeBean.setCode_nm((String)codeData.get("CODE_TEXT"));
			wsItgDataCodesBean.getCode().add(wsItgDataCodeBean);
		}
		
		String itgCodeXml = xstream.toXML(wsItgDataCodesBean);
		 
		return xmlTag + "\n" + itgCodeXml;
	}
	
	/**
	 * 
	 * DCA 서버 요청(등록/삭제) -> XML
	 * 
	 * @param assetList
	 * @param dcaAgentPort
	 * @return
	 * @throws NkiaException
	 */
	public String parserRequestDcaServerXml(List assetList, String dcaAgentPort)throws NkiaException {
		XStream xstream = initXeusXStream(true); 
		xstream.processAnnotations(WsDcaServerConfigsBean.class);
		
		WsDcaServerConfigsBean wsDcaServerConfigsBean = new WsDcaServerConfigsBean();
		wsDcaServerConfigsBean.setServerConfig(new ArrayList());
		for( int i = 0; i < assetList.size(); i++ ){
			HashMap assetData = (HashMap)assetList.get(i);
			WsDcaServerConfigBean wsDcaServerConfigBean = new WsDcaServerConfigBean();
			wsDcaServerConfigBean.setIp((String)assetData.get("ip"));
			wsDcaServerConfigBean.setPort(dcaAgentPort);
			wsDcaServerConfigBean.setTargetManagerIp((String)assetData.get("target_manager_ip"));
			wsDcaServerConfigBean.setForeignKey((String)assetData.get("conf_id"));
			wsDcaServerConfigsBean.getServerConfig().add(wsDcaServerConfigBean);
		}
		String dcaServerXml = xstream.toXML(wsDcaServerConfigsBean);
		return xmlTag + "\n" + dcaServerXml;
	}
	
	/**
	 * 
	 * DCA 네트워크요청(등록/삭제) -> XML
	 * 
	 * @param assetList
	 * @return
	 * @throws NkiaException
	 */
	public String parserRequestDcaNetworkXml(List assetList)throws NkiaException {
		XStream xstream = initXeusXStream(true); 
		xstream.processAnnotations(WsDcaNetworkConfigsBean.class);
		
		WsDcaNetworkConfigsBean wsDcaNeworkConfigsBean = new WsDcaNetworkConfigsBean();
		wsDcaNeworkConfigsBean.setNetworkConfig(new ArrayList());
		for( int i = 0; i < assetList.size(); i++ ){
			HashMap assetData = (HashMap)assetList.get(i);
			WsDcaNetworkConfigBean wsDcaNetworkConfigBean = new WsDcaNetworkConfigBean();
			wsDcaNetworkConfigBean.setIp((String)assetData.get("ip"));
			wsDcaNetworkConfigBean.setPort((String)assetData.get("port"));
			wsDcaNetworkConfigBean.setTargetManagerIp((String)assetData.get("target_manager_ip"));
			wsDcaNetworkConfigBean.setForeignKey((String)assetData.get("conf_id"));
			wsDcaNetworkConfigBean.setCommunity((String)assetData.get("community"));
			wsDcaNetworkConfigBean.setVersion((String)assetData.get("version"));
			wsDcaNeworkConfigsBean.getNetworkConfig().add(wsDcaNetworkConfigBean);
		}
		String dcaNetworkXml = xstream.toXML(wsDcaNeworkConfigsBean);
		return xmlTag + "\n" + dcaNetworkXml;
	}
	
	/**
	 * 
	 * DCA 요청결과를 List 변환
	 * 
	 * @param dcaResultXml
	 * @param assetList 
	 * @return
	 * @throws NkiaException
	 */
	public List parserResultDcaXml(String dcaResultXml, List assetList)throws NkiaException {
		List parserList = new ArrayList();
		XStream xstream = initXeusXStream(true); 
		xstream.processAnnotations(WsDcaResultObjectsBean.class);
		
		WsDcaResultObjectsBean wsDcaResultObjectsBean = (WsDcaResultObjectsBean)xstream.fromXML(dcaResultXml);
		List resultData = wsDcaResultObjectsBean.getResultObject();
		if( resultData != null && resultData.size() > 0 ){
			for( int i = 0; i < resultData.size(); i++ ){
				HashMap parserDataMap = new HashMap();
				WsDcaResultObjectBean wsDcaResultObjectBean = (WsDcaResultObjectBean)resultData.get(i);
				parserDataMap.put("dca_id", (String)wsDcaResultObjectBean.getId());
				String confId = (String)wsDcaResultObjectBean.getForeignKey();
				String targetManagerIp = (String)wsDcaResultObjectBean.getTargetManagerIp();
				
				parserDataMap.put("conf_id", confId);
				for( int j = 0; j < assetList.size(); j++ ){
					HashMap assetData = (HashMap)assetList.get(j);
					if( confId.equalsIgnoreCase((String)assetData.get("conf_id")) ){
						parserDataMap.put("conf_nm", (String)assetData.get("conf_nm"));
						parserDataMap.put("ip", (String)assetData.get("ip"));
						break;
					}
				}
				
				if( targetManagerIp.equalsIgnoreCase(IntergrationPolestarEnums.DCA_FIRST_MANAGER_IP.toString()) ){
					parserDataMap.put("region", IntergrationPolestarEnums.DCA_FIRST_REGION_CODE.toString());
				}else if( targetManagerIp.equalsIgnoreCase(IntergrationPolestarEnums.DCA_SECOND_MANAGER_IP.toString()) ){
					parserDataMap.put("region", IntergrationPolestarEnums.DCA_SECOND_REGION_CODE.toString());
				}
				
				parserDataMap.put("status", (String)wsDcaResultObjectBean.getStatus());
				parserDataMap.put("msg", (String)wsDcaResultObjectBean.getMsg());
				parserList.add(parserDataMap);
			}
		}
		return parserList;
	}
	
	/**
	 * DCA SNMP
	 * @param assetList
	 * @return
	 * @throws NkiaException
	 */
	public String parserRequestDcaSnmpXml(List assetList) throws NkiaException {
		XStream xstream = initXeusXStream(true); 
		xstream.processAnnotations(WsDcaSnmpConfigsBean.class);
		
		WsDcaSnmpConfigsBean wsDcaSnmpConfigsBean = new WsDcaSnmpConfigsBean();
		wsDcaSnmpConfigsBean.setSnmpConfig(new ArrayList());
		for( int i = 0; i < assetList.size(); i++ ){
			HashMap assetData = (HashMap)assetList.get(i);
			WsDcaSnmpConfigBean wsDcaSnmpConfigBean = new WsDcaSnmpConfigBean();
			
			String ip = (String)assetData.get("ip");
			if( ip.split("/").length > 1 ){
				ip = ip.split("/")[0];
			}
			if( ip.split(",").length > 1 ){
				ip = ip.split(",")[0];
			}
			
			wsDcaSnmpConfigBean.setIp(ip);
			wsDcaSnmpConfigBean.setPort("161");
			wsDcaSnmpConfigBean.setCommunity("public");
			wsDcaSnmpConfigBean.setVersion("1");
			wsDcaSnmpConfigBean.setForeignKey((String)assetData.get("conf_id"));
			wsDcaSnmpConfigBean.setTargetManagerIp((String)assetData.get("target_manager_ip"));
			wsDcaSnmpConfigsBean.getSnmpConfig().add(wsDcaSnmpConfigBean);
		}
		String dcaDcaSnmpXml = xstream.toXML(wsDcaSnmpConfigsBean);
		
		return xmlTag + "\n" + dcaDcaSnmpXml;
	}
	
	/**
	 * 
	 * 공백데이터 구분에 따른 replace
	 * 
	 * @param data
	 * @return
	 * @throws NkiaException
	 */
	public String replaceNullToSpace(String data)throws NkiaException{
		return data.replaceAll("%", "");
	}
	
	/**
	 * 
	 * 다중데이터 구분에 따른 replace
	 * 
	 * @param data
	 * @return
	 * @throws NkiaException
	 */
	public String castMultiToComma(String data)throws NkiaException{
		return data.replaceAll("\\|", ",").replaceAll("~", " ");
	}
	
}

