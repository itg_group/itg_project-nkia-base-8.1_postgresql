package com.nkia.itg.interfaces.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.AtchFileDAO;
import com.nkia.itg.interfaces.dao.WsSampleDAO;
import com.nkia.itg.interfaces.service.WsSampleService;

@Service("wsSampleService")
public class WsSampleServiceImpl implements WsSampleService {

	@Resource(name="wsSampleDAO")
	private WsSampleDAO wsSampleDAO;
	
	@Resource(name = "atchFileDAO")
	public AtchFileDAO atchFileDAO;
	
	/**
	 * 조건 : 요청번호
	 * 요청 데이터 조회
	 */
	public List selectSampleSGAppr(Map paramMap) throws NkiaException {
		return wsSampleDAO.selectSampleSGAppr(paramMap);
	}
	
	/**
	 * 조건 : 요청번호
	 * 요청의 첨부파일 조회
	 */
	public List selectSampleSGApprFileData(ModelMap paramMap) throws NkiaException {
		return atchFileDAO.selectAtchFileListForProc(paramMap);
	}

}
