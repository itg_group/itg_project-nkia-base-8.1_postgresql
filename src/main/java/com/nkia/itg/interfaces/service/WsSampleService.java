package com.nkia.itg.interfaces.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface WsSampleService {

	/**
	 * 요청 데이터 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSampleSGAppr(Map paramMap) throws NkiaException;
	
	
	/**
	 * 요청의 첨부파일 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSampleSGApprFileData(ModelMap paramMap) throws NkiaException;
	
}
