/*
 * @(#)WsServiceImpl.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.ItgBaseService;
import com.nkia.itg.interfaces.beans.WsEventBean;
import com.nkia.itg.interfaces.beans.WsXeusVmBean;
import com.nkia.itg.interfaces.dao.WsDAO;
import com.nkia.itg.interfaces.dao.WsEventDAO;
import com.nkia.itg.interfaces.service.WsService;
import com.nkia.itg.interfaces.util.WsUtil;
import com.nkia.itg.itam.amdb.service.AmdbClassService;
import com.nkia.itg.itam.opms.service.OpmsVmService;

@Service("wsService")
public class WsServiceImpl implements WsService {
	
	@Resource(name = "wsUtil")
	private WsUtil wsUtil;
	
	@Resource(name = "wsDAO")
	private WsDAO wsDAO;
	
	@Resource(name = "opmsVmService")
	private OpmsVmService opmsVmService;
	
	@Resource(name = "wsEventDAO")
	private WsEventDAO wsEventDAO;
	
	@Resource(name = "amdbClassService") 
	private AmdbClassService amdbClassService;
	
	@Resource(name = "itgBaseService")
	private ItgBaseService itgBaseService;
	
	
	/**
	 * 
	 * ITG 자산분류 목록
	 * 
	 * @param xml
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public String provideItgAssetClass(String classType) throws NkiaException {
		ModelMap paramMap = new ModelMap();
		if( !classType.equalsIgnoreCase("ALL") ){
			paramMap.put("classType", classType);
		}
		paramMap.put("use_yn", "Y");
		List amdbClassList = amdbClassService.searchAmClassAllTree(paramMap);
		String amdbClassXml = wsUtil.parserItgAmdbClassToXml(amdbClassList);
		return amdbClassXml;
	}
	
	/**
	 * 
	 * ITG 코드목록
	 * 
	 * @param grpId
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public String provideItgCode(String codeGrpId) throws NkiaException {
		ModelMap paramMap = new ModelMap();
		paramMap.put("code_grp_id", codeGrpId);
		paramMap.put("multi_code", "N");
		List codeList = itgBaseService.searchCodeDataList(paramMap);
		String codeXml = wsUtil.parserItgCodeToXml(codeList);
		return codeXml;
	}
	
	@Override
	public void insertSmsEventLog(WsEventBean wsEventBean) throws NkiaException {
		wsEventDAO.insertSmsEventLog(wsEventBean);
	}

}
