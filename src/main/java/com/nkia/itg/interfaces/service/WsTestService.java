package com.nkia.itg.interfaces.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface WsTestService {

	public List selectCodeList(Map paramMap) throws NkiaException;
	
}
