package com.nkia.itg.interfaces.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.dao.WsTestDAO;
import com.nkia.itg.interfaces.service.WsTestService;

@Service("wsTestService")
public class WsTestServiceImpl implements WsTestService {

	@Resource(name="wsTestDAO")
	private WsTestDAO wsTestDAO;
	@Override
	public List selectCodeList(Map paramMap) throws NkiaException {
		return wsTestDAO.selectCodeList(paramMap);
	}

}
