/*
 * @(#)WsService.java              2013. 10. 22.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.interfaces.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.interfaces.beans.WsEventBean;

public interface WsService {


	/**
	 * 
	 * ITG 자산분류 목록
	 * 
	 * @param xml
	 * @return
	 * @throws NkiaException
	 */
	public String provideItgAssetClass(String classType)throws NkiaException;
	
	/**
	 * 
	 * ITG 코드목록
	 * 
	 * @param grpId
	 * @return
	 * @throws NkiaException
	 */
	public String provideItgCode(String codeGrpId)throws NkiaException;
	
	/**
	 * Sms Event Log 등록
	 * @param wsEventBean
	 * @throws NkiaException
	 */
	public void insertSmsEventLog(WsEventBean wsEventBean) throws NkiaException;
}
