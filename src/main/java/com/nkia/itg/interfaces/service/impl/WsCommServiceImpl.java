package com.nkia.itg.interfaces.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.interfaces.dao.WsCommDAO;
import com.nkia.itg.interfaces.service.WsCommService;

@Service("wsCommService")
public class WsCommServiceImpl implements WsCommService{

	@Resource(name = "wsCommDAO")
	private WsCommDAO wsCommDAO;

	/**
	 * 인터페이스 인터페이스 이력 저장
	 */
	@Override
	public void insertIfWebserviceHis(Map hisParamMap) {
		wsCommDAO.insertIfWebserviceHis(hisParamMap);
	}

}
