package com.nkia.itg.system.user.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

public interface UserService {

	public List searchDeptForTree(ModelMap paramMap) throws NkiaException;
	
	public int searchUserListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchUserList(ModelMap paramMap) throws NkiaException;
	
	public List searchAllUserGrp(ModelMap paramMap) throws NkiaException;
	
	public List searchUserGrp(ModelMap paramMap) throws NkiaException;
	
	public List searchUserGrpChargerGrp(ModelMap paramMap) throws NkiaException;
	
	public List searchCustomer(ModelMap paramMap) throws NkiaException;
	
	public List searchHaveUserGrp(ModelMap paramMap) throws NkiaException;
	
	public List selectleafFalseId(ModelMap paramMap) throws NkiaException;
		
	public void insertUserGrp(Object object) throws NkiaException;
	
	public void insertCustomer(Object object) throws NkiaException;
	
	public HashMap<String, String> insertUserInfo(ModelMap paramMap) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException;

	public HashMap<String, String> updateUserInfo(ModelMap paramMap) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, IOException;
	
	public String selectUserIdComp(ModelMap paramMap) throws NkiaException;

	public UserVO selectUserInfo(String userId) throws NkiaException;
	
	public void deleteUserInfo(ModelMap paramMap) throws NkiaException;

	public HashMap selectUserInfoDetail(ModelMap paramMap) throws NkiaException;
	
	public List searchUserExcelDown(Map paramMap) throws NkiaException;
	
	public HashMap selectUserReqZoneInfo(ModelMap paramMap) throws NkiaException;

	/** 사용자 현황 조회 (대한항공 추가 건) **/
	public List searchUserViewList(ModelMap paramMap) throws NkiaException;
	
	public int searchUserViewListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchUserViewExcelDown(Map paramMap) throws NkiaException;
	
	public void updateTwofactor(Map paramMap) throws NkiaException;
	
	/**
	 * 
	 * 사용자 패스워드 초기화 및 계정잠금 해제
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchAlgorithmException 
	 */
	public void updateUserPwIntAndReleseLock(ModelMap paramMap)throws NkiaException, NoSuchAlgorithmException, UnsupportedEncodingException, IOException;
	
	/**
	 * 
	 * 사용자 패스워드 체크 Validation
	 * @param userId
	 * @param pw
	 * @param pwCheck
	 * @param encryptPw
	 * @return
	 * @throws NkiaException
	 */
	public HashMap<String, String> getUserPwValidation(String userId, String pw, String pwCheck, String encryptPw) throws NkiaException;
	
	public void InitializationUserPwBatch() throws NkiaException, IOException, NoSuchAlgorithmException, SQLException;
	
	/**
	 * 메일 발송을 위한 사용자 정보 목록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserListForMail(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 메일 발송을 위한 사용자 정보 목록[리포트 수신]
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserListForReport(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 메일 발송을 위한 사용자 정보 목록[리포트 참조]
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserListForReportCC(ModelMap paramMap) throws NkiaException;
	
	public int searchUserHistoryListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchUserHistoryList(ModelMap paramMap) throws NkiaException;
	
	public int searchUserGrpHistoryListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchUserGrpHistoryList(ModelMap paramMap) throws NkiaException;
	
	public int searchCustomerHistoryListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchCustomerHistoryList(ModelMap paramMap) throws NkiaException;
	
	public void insertSelectUserHistory(ModelMap paramMap) throws NkiaException;
	
}
