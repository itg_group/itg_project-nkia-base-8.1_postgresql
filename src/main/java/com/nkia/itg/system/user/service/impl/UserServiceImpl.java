package com.nkia.itg.system.user.service.impl;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.security.CipherRSA;
import com.nkia.itg.base.application.util.security.CipherSHA;
import com.nkia.itg.base.application.util.security.CryptionChange;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.system.service.impl.SystemServiceImpl;
import com.nkia.itg.system.user.dao.UserDAO;
import com.nkia.itg.system.user.service.UserService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("userService")
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(SystemServiceImpl.class);
	
	@Resource(name = "userDAO")
	private UserDAO userDAO;
	
	@Resource(name = "cryptionChange")
	private CryptionChange cryptionChange;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	public List searchDeptForTree(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return userDAO.searchDeptForTree(paramMap);
	}
	
	public int searchUserListCount(ModelMap paramMap) throws NkiaException {
		return userDAO.searchUserListCount(paramMap);
	}
	
	public List searchUserList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = userDAO.searchUserList(paramMap); 
		return resultList;
	}

	public List searchAllUserGrp(ModelMap paramMap) throws NkiaException {
		return userDAO.searchAllUserGrp(paramMap);
	}
	
	public List searchUserGrp(ModelMap paramMap) throws NkiaException {
		return userDAO.searchUserGrp(paramMap);
	}
	
	public List searchUserGrpChargerGrp(ModelMap paramMap) throws NkiaException {
		return userDAO.searchUserGrpChargerGrp(paramMap);
	}

	public List searchCustomer(ModelMap paramMap) throws NkiaException {
		return userDAO.searchCustomer(paramMap);
	}	
	
	public List searchHaveUserGrp(ModelMap paramMap) throws NkiaException {
		return userDAO.searchHaveUserGrp(paramMap);
	}
	
	public List selectleafFalseId(ModelMap paramMap) throws NkiaException {
		return userDAO.selectleafFalseId(paramMap);
	}
	
	public void insertUserGrp(Object object) throws NkiaException {
		userDAO.insertUserGrp(object);
	}
	public void insertCustomer(Object object) throws NkiaException {
		userDAO.insertCustomer(object);
	}

	public HashMap<String, String> insertUserInfo(ModelMap paramMap) throws NkiaException, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {	

		HashMap<String, String> resultMap = new HashMap<String, String>();
		UserVO user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		//내부직원 외부직원여부  
		
		
		HashMap insertMap = new HashMap();
		insertMap.putAll((Map) paramMap.get("insertNameValue"));
		
		String passWord = (String) insertMap.get("regist_pw");
		String passWordCheck = (String) insertMap.get("regist_pw_check");
		
		// 클라이언트 RSA복호화
		byte[] saltBytes = CipherSHA.getSalt();
		String encryptPw ="";
		if(!"".equals(passWord)){
			PrivateKey privateKey = CipherRSA.getPrivateKey();
			passWord = CipherRSA.getDecrypt(privateKey, passWord);
			passWordCheck = CipherRSA.getDecrypt(privateKey, passWordCheck);
			
			// Salt 취득
			saltBytes = CipherSHA.getSalt();
			
			// 패스워드 검증(자리수, 반복, 패스워드일치 등을 검증)
			// jjy
			encryptPw = CipherSHA.getEncrypt(passWord, saltBytes);
			resultMap = getUserPwValidation((String)paramMap.get("user_id"), passWord, passWordCheck, encryptPw);
			
			String isValid = resultMap.get("IS_VALID");
				
			if(isValid.equals("false")) {
				return resultMap;
			}
            resultMap = getUserPwValidation((String)paramMap.get("user_id"), passWord, passWordCheck, encryptPw);
		}else{
			
			//passWord = NkiaApplicationPropertiesMap.getProperty("Globals.Security.BasicPassword");  //초기비번을 프로퍼티로 관리하는방법
			passWord = (String)insertMap.get("user_id");  //초기비번을 아이디와 동일하게 관리하는방법
			encryptPw = CipherSHA.getEncrypt(passWord, saltBytes);
			resultMap.put("RESULT_MSG", resultMsg);
		}
		// 비밀번호 암호화 
		insertMap.put("pw", encryptPw);
		insertMap.put("scrty_salt", StringUtil.byteToBase64(saltBytes));
		
		// 로그인한 세션유저정보 받아오기
		insertMap.put("ins_user_id", user.getUser_id());
		
		// 사용자 등록
		userDAO.insertUserInfo(insertMap);
		
		// 패스워드 이력관리에 등록을 해줍니다.
		
		userDAO.insertPwHistory(insertMap);
		
		
		// 사용자 권한 등록
		ArrayList array_node = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingInfo")).get("user_grp_id");
		((HashMap<String, Object>) paramMap.get("mappingInfo")).put("upd_user_id", paramMap.get("upd_user_id"));
		for( int i = 0; i < array_node.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("mappingInfo")).put("user_grp_id", array_node.get(i));
			userDAO.insertUserGrp(paramMap.get("mappingInfo"));	
			userDAO.insertUserGrpHistory(paramMap.get("mappingInfo"));
		}
		
		// 고객사 등록
		ArrayList array_customer = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("customerInfo")).get("customer_id");
		((HashMap<String, Object>) paramMap.get("customerInfo")).put("upd_user_id", paramMap.get("upd_user_id"));
		for( int i = 0; i < array_customer.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("customerInfo")).put("customer_id", array_customer.get(i));
			userDAO.insertCustomer(paramMap.get("customerInfo"));
			userDAO.insertCustomerHistory(paramMap.get("customerInfo"));
		}
		
		return resultMap;
	}
	
	public HashMap<String, String> updateUserInfo(ModelMap paramMap) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		
		HashMap<String, String> resultMap = new HashMap<String, String>();
		String resultMsg = messageSource.getMessage("msg.common.result.00004");

		//업데이트를 하기위한 맵
		HashMap updateMap = new HashMap();
		updateMap.put("upd_user_id", paramMap.get("upd_user_id"));
		updateMap.putAll((Map) paramMap.get("insertNameValue"));

		
		// 패스워드 변경여부
		String isPasswordChange = (String) updateMap.get("is_password_change");
		
		// 패스워드 변경일 경우
		if("Y".equals(isPasswordChange)){
			// 이전 사용자 정보
			HashMap detailMap = userDAO.selectUserInfoPassword(paramMap);
			
			String changePassword = (String) updateMap.get("change_pw");
			String changePasswordCheck = (String) updateMap.get("change_pw_check");
			
			// 클라이언트 RSA복호화
			PrivateKey privateKey = CipherRSA.getPrivateKey();
			changePassword = CipherRSA.getDecrypt(privateKey, changePassword);
			changePasswordCheck = CipherRSA.getDecrypt(privateKey, changePasswordCheck);
			
			// 패스워드 검증(자리수, 반복, 패스워드일치, 이전 패스워드 사용여부 등을 검증)
			// jjy
			String encryptPw = CipherSHA.getEncrypt(changePassword, StringUtil.base64ToByte((String)detailMap.get("SCRTY_SALT")));
			resultMap = getUserPwValidation((String)paramMap.get("user_id"), changePassword, changePasswordCheck, encryptPw);
			
			String isValid = resultMap.get("IS_VALID");
			
			if(isValid.equals("false")) {
				return resultMap;
			} 
			
			updateMap.put("pw", encryptPw);
		} else {
			resultMap.put("IS_VALID", "true");
			resultMap.put("RESULT_MSG", resultMsg);
		}
		updateMap.put("sid",updateMap.get("user_id"));
		// 패스워드 업데이트
		userDAO.updateUserInfo(updateMap);
		
		// 패스워드 변경시 : 패스워드 이력등록
		if("Y".equals(isPasswordChange)){
			userDAO.insertPwHistory(updateMap);
		}
		
		// 권한 삭제
		((HashMap<String, Object>) paramMap.get("mappingInfo")).put("upd_user_id", paramMap.get("upd_user_id"));
		userDAO.deleteUserGrp(paramMap.get("mappingInfo"));
		userDAO.deleteUserGrpHistory(paramMap.get("mappingInfo"));
		// 권한 등록
		ArrayList array_node = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingInfo")).get("user_grp_id");
		
		for( int i = 0; i < array_node.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("mappingInfo")).put("user_grp_id", array_node.get(i));
			userDAO.insertUserGrp(paramMap.get("mappingInfo"));
			userDAO.insertUserGrpHistory(paramMap.get("mappingInfo"));
		}
		
		// 고객사 삭제
		((HashMap<String, Object>) paramMap.get("customerInfo")).put("upd_user_id", paramMap.get("upd_user_id"));
		userDAO.deleteCustomer(paramMap.get("customerInfo"));
		userDAO.deleteCustomerHistory(paramMap.get("customerInfo"));
		
		// 고객사 등록
		ArrayList array_customer = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("customerInfo")).get("customer_id");
		
		for( int i = 0; i < array_customer.size(); i++ ){
			((HashMap<String, Object>) paramMap.get("customerInfo")).put("customer_id", array_customer.get(i));
			userDAO.insertCustomer(paramMap.get("customerInfo"));	// 1센터
			userDAO.insertCustomerHistory(paramMap.get("customerInfo"));	
		}
		return resultMap;
	}
	
	public String selectUserIdComp(ModelMap paramMap) throws NkiaException {

		String c1UserCnt = userDAO.selectUserIdComp(paramMap);
		return c1UserCnt;
	}

	public UserVO selectUserInfo(String userId) throws NkiaException {
		
		return userDAO.selectUserInfo(userId);
	}
	
	public void deleteUserInfo(ModelMap paramMap) throws NkiaException {
		// 삭제 로직
		userDAO.deleteUserGrp(paramMap.get("deleteMap"));
		userDAO.deleteUserGrpHistory(paramMap.get("deleteMap"));
		userDAO.deleteCustomer(paramMap.get("deleteMap"));
		userDAO.deleteCustomerHistory(paramMap.get("deleteMap"));
		userDAO.deleteUserInfo(paramMap.get("deleteMap"));
		userDAO.deleteAmOperInfo(paramMap.get("deleteMap"));
		userDAO.deleteServiceOperInfo(paramMap.get("deleteMap"));
	}

	public HashMap selectUserInfoDetail(ModelMap paramMap) throws NkiaException {
		HashMap resultMap = new HashMap();
		
		resultMap = userDAO.selectUserInfoDetail(paramMap); 
		String passWord = (String)resultMap.get("PW"); 
		String value = cryptionChange.decryptPassWord(passWord);
			resultMap.put("PW", value);
			resultMap.put("PWCHECK", value);
		
		return resultMap;
	}
	
	public List searchUserExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = userDAO.searchUserExcelDown(paramMap); 
		return resultList;
	}
	
	
	public HashMap selectUserReqZoneInfo(ModelMap paramMap) throws NkiaException {
		HashMap resultMap = new HashMap();
		resultMap = userDAO.selectUserReqZoneInfo(paramMap); 
		return resultMap;
	}

	/**
	 * 
	 * 사용자 패스워드 초기화 및 계정잠금 해제
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchAlgorithmException 
	 */
	@Override
	public void updateUserPwIntAndReleseLock(ModelMap paramMap)throws NkiaException, NoSuchAlgorithmException, UnsupportedEncodingException, IOException {
		// 이전 사용자 정보
		HashMap detailMap = userDAO.selectUserInfoPassword(paramMap);
		paramMap.put("pw", CipherSHA.getEncrypt(NkiaApplicationPropertiesMap.getProperty("Globals.Security.BasicPassword"), StringUtil.base64ToByte((String)detailMap.get("SCRTY_SALT"))));
		userDAO.updateUserPwIntAndReleseLock(paramMap);
	}

	/**
	 * 
	 * 비밀번호 벨리데이션 체크 후, IS_VALID와 RESULT_MSG가 담긴 Map 리턴
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 * @author 정정윤 jyjeong@nkia.co.kr 
	 */
	public HashMap<String, String> getUserPwValidation(String userId, String pw, String pwCheck, String encryptPw) throws NkiaException {
		
		HashMap paramMap = new HashMap();
		paramMap.put("user_id", userId);
		
		HashMap<String, String> resultMap = new HashMap<String, String>();
		
		boolean isPwCheck = true;
		String isValid = "true";
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		
		// 비밀번호와 비밀번호 확인이 다를 시
		if( !pw.equals(pwCheck) ) {
			isValid = "false";
			resultMsg = messageSource.getMessage("msg.system.user.pw.validation.00001");
		// 비밀번호와 비밀번호 확인이 같으면 검증 시작
		} else {
			// 연속(반복)문자 체크
			if("true".equals(NkiaApplicationPropertiesMap.getProperty("Globals.Pw.Check.Repeat"))) {
				for(int i=0; i<(pw.length()-1); i++) {
					// 비밀번호를 Char 단위로 다음 인덱스와 비교하여 같은 문자열이 나올 시 반복된 문자열로 간주한다.
					if(pw.substring(i, i+1).equals(pw.substring(i+1, i+2))) {
						isPwCheck = false;
						isValid = "false";
						resultMsg = messageSource.getMessage("msg.system.user.pw.validation.00003");
						break;
					}
				}
			}
			
			if("true".equals(NkiaApplicationPropertiesMap.getProperty("Globals.Pw.Check.Continuous.Number"))) {
				for(int i=0; i<(pw.length()-1); i++) {
					// 연속된 숫자를 비교한다.
					try {
						// 숫자만! 비교해야하므로 숫자형 변환할때 NumberFormat 예외발생 시 해당 인덱스는 패스하고 반복문 continue
						int CurrPwToInt = Integer.parseInt(pw.substring(i, i+1));
						int nextPwToInt = Integer.parseInt(pw.substring(i+1, i+2));
						// 두 숫자의 차이를 구하면 -(마이너스)가 나올 수도 있으므로 절대값으로 치환
						int resultInt = Math.abs(CurrPwToInt-nextPwToInt);
						// 두 숫자의 차이가 1일때(연속되는 숫자) 해당 비밀번호 변경 불가
						if(resultInt == 1) {
							isPwCheck = false;
							isValid = "false";
							resultMsg = messageSource.getMessage("msg.system.user.pw.validation.00004");
							break;
						}
					} catch(NumberFormatException e) {
						// NumberFormat 예외발생 시 해당 인덱스는 문자열이 아니므로 패스 후, 반복문 continue
						continue;
					}
				}
				
			}
			
			// 반복된 문자열이 없다면 or 연속된 숫자가 없다면
			if("true".equals(NkiaApplicationPropertiesMap.getProperty("Globals.Pw.Check.Reuse"))) {
				if(isPwCheck) {
					// 이전 변경이력을 조회하여 한번 바꾼 비밀번호로 바꿀 수 없도록 한다.
					List resultList = userDAO.selectUserPwHistory(paramMap); 
					
					if(resultList != null && resultList.size() > 0) {
						for(int i=0; i<resultList.size(); i++) {
							HashMap dataMap = new HashMap();
							dataMap = (HashMap)resultList.get(i);
							if(encryptPw.equals(dataMap.get("OLD_PW"))) {
								isPwCheck = false;
								isValid = "false";
								resultMsg = messageSource.getMessage("msg.system.user.pw.validation.00002");
								break;
							}
						}
					}
				}
			}
		}
		
		resultMap.put("IS_VALID", isValid);
		resultMap.put("RESULT_MSG", resultMsg);
		
		return resultMap;
	}
	
	public void InitializationUserPwBatch() throws NkiaException, IOException, NoSuchAlgorithmException, SQLException {
		
		List<HashMap> userList = userDAO.selectInitPwTargetList();
		
		for (HashMap<String, String> userMap : userList) {
			String passWord = (String) userMap.get("USER_ID");
			String saltString = StringUtil.parseString(userMap.get("SCRTY_SALT"));
			byte[] saltBytes = StringUtil.base64ToByte(saltString);

			if("".equals(saltString)) {
				// Salt 취득
				saltBytes = CipherSHA.getSalt();
				userMap.put("scrty_salt", StringUtil.byteToBase64(saltBytes));
			}
			
			// 암호화된 pw
			String encryptPw = CipherSHA.getEncrypt(passWord, saltBytes);
		
			// pw정보 변경
			userMap.put("pw", encryptPw);
			userMap.put("user_id", userMap.get("USER_ID"));
			userMap.put("upd_user_id", "SYSTEM");
			
			try {
				userDAO.getSqlMapClient().startTransaction();
				userDAO.getSqlMapClient().getCurrentConnection().setAutoCommit(false);
				userDAO.getSqlMapClient().startBatch();
				
				userDAO.updateUserPwInfo(userMap);
				
				userDAO.getSqlMapClient().executeBatch();
				userDAO.getSqlMapClient().commitTransaction();
				userDAO.getSqlMapClient().getCurrentConnection().commit();
			} catch (Exception e) {
				new NkiaException(e);
			} finally {
				userDAO.getSqlMapClient().endTransaction();
			}
		}
	}
	
	
	/**
	 * 메일 발송을 위한 사용자 정보 목록
	 */
	public List searchUserListForMail(ModelMap paramMap) throws NkiaException {
		List resultList = userDAO.searchUserListForMail(paramMap); 
		return resultList;
	}
	
	/**
	 * 메일 발송을 위한 사용자 정보 목록(수신자)
	 */
	public List searchUserListForReport(ModelMap paramMap) throws NkiaException {
		List resultList = userDAO.searchUserListForReport (paramMap); 
		return resultList;
	}
	/**
	 * 메일 발송을 위한 사용자 정보 목록(참조자)
	 */
	public List searchUserListForReportCC(ModelMap paramMap) throws NkiaException {
		List resultList = userDAO.searchUserListForReportCC (paramMap); 
		return resultList;
	}
	
	/**
	 * 사용자 현황 조회 (대한항공 추가 건)
	 */
	@Override
	public List searchUserViewList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = userDAO.searchUserViewList(paramMap); 
		return resultList;
	}

	@Override
	public int searchUserViewListCount(ModelMap paramMap) throws NkiaException {
		return userDAO.searchUserViewListCount(paramMap);
	}

	@Override
	public List searchUserViewExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = userDAO.searchUserViewExcelDown(paramMap); 
		return resultList;
	}
	
	@Override
	public void updateTwofactor(Map paramMap) throws NkiaException {
		userDAO.updateTwofactor(paramMap);
	}
	
	@Override
	public int searchUserHistoryListCount(ModelMap paramMap) throws NkiaException {
		return userDAO.searchUserHistoryListCount(paramMap);
	}
	@Override
	public List searchUserHistoryList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = userDAO.searchUserHistoryList(paramMap); 
		return resultList;
	}
	@Override
	public int searchUserGrpHistoryListCount(ModelMap paramMap) throws NkiaException {
		return userDAO.searchUserGrpHistoryListCount(paramMap);
	}
	@Override
	public List searchUserGrpHistoryList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = userDAO.searchUserGrpHistoryList(paramMap); 
		return resultList;
	}
	@Override
	public int searchCustomerHistoryListCount(ModelMap paramMap) throws NkiaException {
		return userDAO.searchCustomerHistoryListCount(paramMap);
	}
	@Override
	public List searchCustomerHistoryList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = userDAO.searchCustomerHistoryList(paramMap); 
		return resultList;
	}
	@Override
	public void insertSelectUserHistory(ModelMap paramMap) throws NkiaException {
		userDAO.insertSelectUserHistory(paramMap);
	}
}
