package com.nkia.itg.system.user.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.user.service.UserService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;


/**
 * @version 1.0 2013. 2. 26.
 * @author <a href="mailto:minsoo@nkia.co.kr"> Minsoo Jeon
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class UserController {
	
	@Resource(name = "userService")
	private UserService userService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	private int KEY_SIZE = 1024;
	
	/**
	 * 사용자관리 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/goUserManager.do")
	public String goUserManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("adminCheck",NkiaApplicationPropertiesMap.getProperty("Globals.ID.check"));
		paramMap.put("dataMap", dataMap);
		String forwarPage = "/itg/system/user/userManager.nvf";
		return forwarPage;
	}	
	
	
	/**
	 * 유저매니저테스트 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/goUserManagerTest.do")
	public String goUserManagertest(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		HashMap<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("adminCheck",NkiaApplicationPropertiesMap.getProperty("Globals.ID.check"));
		paramMap.put("dataMap", dataMap);
		String forwarPage = "/itg/system/user/userManagerTest.nvf";
		return forwarPage;
	}	
	
	/**
	 * 좌측 부서트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchDeptForTree.do")
	public @ResponseBody ResultVO searchDeptForTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = userService.searchDeptForTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 사용자 그리드 리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchUserList.do")
	public @ResponseBody ResultVO searchUserList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = userService.searchUserListCount(paramMap);
			List resultList = userService.searchUserList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 기본정보 수정
     * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @
	 */
	@RequestMapping(value="/itg/system/user/updateUserInfo.do")
	public @ResponseBody ResultVO updateUserInfo(@RequestBody ModelMap paramMap)throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    		paramMap.put("updId", userVO.getUser_id());
	    	}	
	    	HashMap<String, String> resultMap = userService.updateUserInfo(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));

		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 모든 사용자 그룹(drag&drop) 트리 호출 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchAllUserGrp.do")
	public @ResponseBody ResultVO searchAllUserGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = userService.searchAllUserGrp(paramMap);
			String up_node_id ="";
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, false, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 사용자 그룹(drag&drop)에서 사용될 gabage 트리 호출 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchUserGrp.do")
	public @ResponseBody ResultVO  searchUserGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			List resultList = userService.searchUserGrp(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	/**
	 * 담당자 그룹관리엣허 사용할 사용자 그룹호출 
     * @param paramMap
	 * @return
	 * @
	 */ 
	@RequestMapping(value="/itg/system/user/searchUserGrpChargerGrp.do")
	public @ResponseBody ResultVO  searchUserGrpChagerGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			List resultList = userService.searchUserGrpChargerGrp(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	/**
	 * 사용자정보에서 사용될 고객사정보 트리 호출 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchCustomer.do")
	public @ResponseBody ResultVO  searchCustomer(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			List resultList = userService.searchCustomer(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	/**
	 * 특정사용자가 보유한 사용자 그룹(drag&drop) 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchHaveUserGrp.do")
	public @ResponseBody ResultVO  searchHaveUserGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = userService.searchHaveUserGrp(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 그룹(drag&drop) 하위분류트리 존재 여부 확인 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/selectleafFalseId.do")
	public @ResponseBody ResultVO selectleafFalseId(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = userService.selectleafFalseId(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 기본정보 저장
     * @param paramMap
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @
	 */
	@RequestMapping(value="/itg/system/user/insertUserInfo.do")
	public @ResponseBody ResultVO  insertUserInfo(@RequestBody ModelMap paramMap)throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			HashMap<String, String> resultMap = userService.insertUserInfo(paramMap);
	    	resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(resultMap.get("RESULT_MSG"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 ID 존재 유무 체크 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/selectUserIdComp.do")
	public @ResponseBody ResultVO selectUserIdComp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			String resultMap = userService.selectUserIdComp(paramMap);
			resultVO.setResultString(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/deleteUserInfo.do")
	public @ResponseBody ResultVO deleteUserInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			userService.deleteUserInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자정보 수정시 비밀번호 복호화를 위하여 단건 조회 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/selectUserInfoDetail.do")
	public @ResponseBody ResultVO selectUserInfoDetail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		
		try {
			resultMap = userService.selectUserInfoDetail(paramMap);
			/*조회이력 삽입  - 롯데UBIT*/  
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	userService.insertSelectUserHistory(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 사용자리스트 엑셀다운로드
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchUserExcelDown.do")
	public @ResponseBody ResultVO searchUserExcelDown(@RequestBody ModelMap paramMap)throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			List resultList = userService.searchUserExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자의 요청부대 정보 조회 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/user/selectUserReqZoneInfo.do")
	public @ResponseBody ResultVO selectUserReqZoneInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			HashMap resultMap = userService.selectUserReqZoneInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 
	 * 사용자 패스워드 초기화 및 계정잠금 해제
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/updateUserPwIntAndReleseLock.do")
	public @ResponseBody ResultVO updateUserPwIntAndReleseLock(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			userService.updateUserPwIntAndReleseLock(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 현황 호출 대한항공 추가 건
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/goUserViewManager.do")
	public String selectUserViewManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/user/userManagerView.nvf";
		return forwarPage;
	}	
	
	/**
	 * 사용자 현황 대한항공 추가 건
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/searchUserViewList.do")
	public @ResponseBody ResultVO searchUserViewList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = userService.searchUserViewListCount(paramMap);
			List resultList = userService.searchUserViewList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 현황 엑셀다운로드 대한항공 추가 건
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 * @
	 */
	@RequestMapping(value="/itg/system/user/searchUserViewExcelDown.do")
	public @ResponseBody ResultVO searchUserViewExcelDown(@RequestBody ModelMap paramMap)throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			
			List resultList = userService.searchUserViewExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자정보변경이력 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/goUserHistoryManager.do")
	public String goUserHistoryManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/user/userHistoryManager.nvf";
		return forwarPage;
	}	
	/**
	 * 사용자그룹변경이력 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/goUserGrpHistoryManager.do")
	public String goUserGrpHistoryManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/user/userGrpHistoryManager.nvf";
		return forwarPage;
	}	
	/**
	 * 고객사정보변경이력 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/user/goCustomerHistoryManager.do")
	public String goCustomerHistoryManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/user/customerHistoryManager.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/system/user/searchUserHistoryList.do")
	public @ResponseBody ResultVO searchUserHistoryList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = userService.searchUserHistoryListCount(paramMap);
			List resultList = userService.searchUserHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	@RequestMapping(value="/itg/system/user/searchUserGrpHistoryList.do")
	public @ResponseBody ResultVO searchUserGrpHistoryList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = userService.searchUserGrpHistoryListCount(paramMap);
			List resultList = userService.searchUserGrpHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	@RequestMapping(value="/itg/system/user/searchCustomerHistoryList.do")
	public @ResponseBody ResultVO searchCustomerHistoryList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = userService.searchCustomerHistoryListCount(paramMap);
			List resultList = userService.searchCustomerHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
