package com.nkia.itg.system.user.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("userDAO")
public class UserDAO extends EgovComAbstractDAO {

	public List searchDeptForTree(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchDeptForTree", paramMap);
	}

	public int searchUserListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("UserDAO.searchUserListCount", paramMap);
	}

	public List searchUserList(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserList", paramMap);
	}

	public void updateUserInfo(Object object) throws NkiaException {
		update("UserDAO.updateUserInfo", object);
	}

	public void updateUserPwInfo(Object object) throws NkiaException {
		update("UserDAO.updateUserPwInfo", object);
	}

	public List searchAllUserGrp(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchAllUserGrp", paramMap);
	}

	public List searchUserGrp(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserGrp", paramMap);
	}
	
	public List searchUserGrpChargerGrp(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserGrpChargerGrp", paramMap);
	}
	
	public List searchCustomer(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchCustomer", paramMap);
	}

	public List searchHaveUserGrp(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchHaveUserGrp", paramMap);
	}

	public List selectleafFalseId(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.selectleafFalseId", paramMap);
	}

	public void insertUserGrp(Object object) throws NkiaException {
		this.insert("UserDAO.insertUserGrp", object);
	}
	
	public void insertUserGrpHistory(Object object) throws NkiaException {
		this.insert("UserDAO.insertUserGrpHistory", object);
	}

	public void insertCustomer(Object object) throws NkiaException {
		this.insert("UserDAO.insertCustomer", object);
	}
	
	public void insertCustomerHistory(Object object) throws NkiaException {
		this.insert("UserDAO.insertCustomerHistory", object);
	}
	
	public void insertUserInfo(Object object) throws NkiaException {
		this.insert("UserDAO.insertUserInfo", object);
	}

	public String selectUserIdComp(ModelMap paramMap) throws NkiaException {
		return (String) selectByPk("UserDAO.selectUserIdComp", paramMap);
	}

	public String selectUserName(String currentUserId) throws NkiaException {
		return (String) this
				.selectByPk("UserDAO.selectUserName", currentUserId);
	}

	public UserVO selectUserInfo(String userId) throws NkiaException {
		return (UserVO) selectByPk("UserDAO.selectUserInfo", userId);
	}

	public void deleteUserInfo(Object object) throws NkiaException {
		delete("UserDAO.deleteUserInfo", object);
	}

	public void deleteUserGrp(Object object) throws NkiaException {
		delete("UserDAO.deleteUserGrp", object);
	}
	
	public void deleteUserGrpHistory(Object object) throws NkiaException {
		this.insert("UserDAO.deleteUserGrpHistory", object);
	}
	
	public void deleteCustomer(Object object) throws NkiaException {
		delete("UserDAO.deleteCustomer", object);
	}
	public void deleteCustomerHistory(Object object) throws NkiaException {
		this.insert("UserDAO.deleteCustomerHistory", object);
	}
	public void deleteAmOperInfo(Object object) throws NkiaException {
		delete("UserDAO.deleteAmOperInfo", object);
	}

	public void deleteServiceOperInfo(Object object) throws NkiaException {
		delete("UserDAO.deleteServiceOperInfo", object);
	}

	public HashMap selectUserInfoDetail(ModelMap paramMap) throws NkiaException {
		return (HashMap) selectByPk("UserDAO.selectUserInfoDetail", paramMap);
	}

	public List searchUserExcelDown(Map paramMap) throws NkiaException {
		return list("UserDAO.searchUserExcelDown", paramMap);
	}

	// 패스워드 이력관리에 입력합니다.
	public void insertPwHistory(HashMap insertMap) throws NkiaException {
		insert("UserDAO.insertPwHistory", insertMap);
	}

	public HashMap selectUserReqZoneInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap) selectByPk("UserDAO.selectUserReqZoneInfo", paramMap);
	}

	/**
	 * 
	 * 사용자ID와 패스워드 정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectUserInfoPassword(ModelMap paramMap) throws NkiaException {
		return (HashMap) selectByPk("UserDAO.selectUserInfoPassword", paramMap);
	}

	/**
	 * 
	 * 사용자 패스워드 초기화 및 계정잠금 해제
	 * 
	 * @param paramMap
	 */
	public void updateUserPwIntAndReleseLock(ModelMap paramMap)
			throws NkiaException {
		this.update("UserDAO.updateUserPwIntAndReleseLock", paramMap);
	}

	public List selectUserPwHistory(HashMap paramMap) throws NkiaException {
		return list("UserDAO.selectUserPwHistory", paramMap);
	}

	public List selectInitPwTargetList() throws NkiaException{
		return list("UserDAO.selectInitPwTargetList", null);
	}

	/**
	 * 메일 발송을 위한 사용자 정보 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserListForMail(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserListForMail", paramMap);
	}

	/**
	 * 메일 발송을 위한 사용자 정보 목록[리포트수신]
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserListForReport(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserListForReport", paramMap);
	}

	/**
	 * 메일 발송을 위한 사용자 정보 목록[리포트참조]
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserListForReportCC(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserListForReportCC", paramMap);
	}

	/**
	 * 사용자 현황 조회 (대한항공 추가 건)
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchUserViewList(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserViewList", paramMap);
	}

	public int searchUserViewListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("UserDAO.searchUserViewListCount", paramMap);
	}

	public List searchUserViewExcelDown(Map paramMap) throws NkiaException {
		return list("UserDAO.searchUserViewExcelDown", paramMap);
	}

	//
	public void updateTwofactor(Map paramMap) throws NkiaException {
		update("UserDAO.updateTwofactor", paramMap);
	}
	public int searchUserHistoryListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("UserDAO.searchUserHistoryListCount", paramMap);
	}
	public List searchUserHistoryList(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserHistoryList", paramMap);
	}
	public int searchUserGrpHistoryListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("UserDAO.searchUserGrpHistoryListCount", paramMap);
	}
	public List searchUserGrpHistoryList(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchUserGrpHistoryList", paramMap);
	}
	public int searchCustomerHistoryListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("UserDAO.searchCustomerHistoryListCount", paramMap);
	}
	public List searchCustomerHistoryList(ModelMap paramMap) throws NkiaException {
		return list("UserDAO.searchCustomerHistoryList", paramMap);
	}
	public void insertSelectUserHistory(Map paramMap) throws NkiaException {
		update("UserDAO.insertSelectUserHistory", paramMap);
	}
}
