package com.nkia.itg.system.batchExcel.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("batchExcelDAO")
public class BatchExcelDAO  extends EgovComAbstractDAO {

	public String selectColDataType(HashMap paramMap) {
		return (String) selectByPk("BatchExcelDAO.selectColDataType", paramMap);
	}

	public List selectTblColInfoList(List<HashMap> gridHeaderList) {
		return list("BatchExcelDAO.selectTblColInfoList", gridHeaderList);
	}

	public HashMap selectTblColInfo(HashMap paramMap) {
		return (HashMap) selectByPk("BatchExcelDAO.selectTblColInfo", paramMap);
	}

	public void executeQuery(String queryString) {
		insert("BatchExcelDAO.executeQuery", queryString);
	}

	public List selectPkColList(List<HashMap> paramMap) {
		return list("BatchExcelDAO.selectPkColList", paramMap);
	}

	public List<HashMap> selectNotNullColList(List<HashMap> gridHeaderList) {
		return list("BatchExcelDAO.selectNotNullColList", gridHeaderList);
	}

	public List selectPkNotExistTable(List<HashMap> gridHeaderList) {
		return list("BatchExcelDAO.selectPkNotExistTable", gridHeaderList);
	}

}
