package com.nkia.itg.system.batchExcel.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface BatchExcelService {

	HashMap parsingExcelForXlsx(HashMap paramMap, String store_file_name,
			String orignl_file_name, String file_store_cours,
			String file_extsn, String file_content_type) throws NkiaException, IOException;

	HashMap parsingExcelForXls(HashMap paramMap, String storeFileName,
			String orignlFileName, String fileStoreCours, String fileExtsn,
			String fileContentType) throws NkiaException, IOException;

	HashMap validateBatchExcelData(HashMap paramMap) throws NkiaException, UnsupportedEncodingException;

	HashMap insertBatchExcelData(HashMap paramMap) throws NkiaException;

	HashMap updateBatchExcelData(ModelMap paramMap) throws NkiaException;

}
