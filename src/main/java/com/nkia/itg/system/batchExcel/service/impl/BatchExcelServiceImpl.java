package com.nkia.itg.system.batchExcel.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.batchExcel.customize.CustomizeQueryExecute;
import com.nkia.itg.system.batchExcel.dao.BatchExcelDAO;
import com.nkia.itg.system.batchExcel.service.BatchExcelService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("batchExcelService")
public class BatchExcelServiceImpl implements BatchExcelService{

	@Resource(name="batchExcelDAO")
	private BatchExcelDAO batchExcelDAO;

	/**
	 * 엑셀파일의 sheet를 읽는다.(Xlsx 용)
	 * @param storeFileName
	 * @param fileStoreCours
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	private XSSFSheet getExcelFileSeetForXlsx(String storeFileName, String fileStoreCours, int sheetNum) throws NkiaException, IOException{
		
		FileInputStream fis = null;
		XSSFSheet sheet = null;
		
		try {
			String storeFilePath = fileStoreCours + storeFileName;
			File file = new File(storeFilePath);
			fis = new FileInputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(sheetNum);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e){
					throw new NkiaException(e);
				}
			}
		}
		
		return sheet;
	}
	
	/**
	 * [엑셀 데이터 파싱 (Xlsx 용)]
	 * @throws IOException 
	 */
	@Override
	public HashMap parsingExcelForXlsx(HashMap paramMap,
			String storeFileName,
			String orignlFileName, 
			String fileStoreCours,
			String fileExtsn, 
			String fileContentType) throws NkiaException, IOException{
		
		HashMap resultMap = new HashMap();
		
		// 엑셀파일의 0번째 sheet 를 가져온다
		XSSFSheet sheet = getExcelFileSeetForXlsx(storeFileName, fileStoreCours, 0);
		
		// 엑셀헤더목록, 그리드리소스, 마지막cell 리턴
		HashMap headerInfoMap = getHeaderInfoMapForXlsx(paramMap, sheet);
		
		if(headerInfoMap.size() > 0) {
			
			List<HashMap> excelHeaderList = (List) headerInfoMap.get("excelHeaderList");	// 헤더 정보
			HashMap gridResourceMap = (HashMap) headerInfoMap.get("gridResourceMap");	// grid resource 정보
			int lastCellNum = (Integer) headerInfoMap.get("lastCellNum");					// 마지막 cell 번호
			
			// 데이터 정보
			List<HashMap> dataList = getExcelDataMapForXlsx(paramMap, sheet, excelHeaderList, lastCellNum);
			
			resultMap.put("resultList", dataList);
			resultMap.put("gridResourceMap", gridResourceMap);
		}
			
		return resultMap;
		
	}
	
	/**
	 * header 정보를 담아준다(Xlsx 용)
	 * excelHeaderList : 엑셀 헤더 정보
	 * gridResourceMap : 그리드 resource 정보 
	 * @param paramMap
	 * @param sheet
	 * @return
	 */
	private HashMap<String, Object> getHeaderInfoMapForXlsx(HashMap paramMap, XSSFSheet sheet) throws NkiaException{
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		// 헤더 정보를 만든다.
		int HEADER_ID_START_ROW_NUMBER = 3;		// 엑셀 헤더ID 시작 행
		int HEADER_NM_START_ROW_NUMBER = 4;		// 엑셀 헤더명 시작 행 
		XSSFRow headerIdRow = sheet.getRow(HEADER_ID_START_ROW_NUMBER - 1);
		XSSFRow headerNmRow = sheet.getRow(HEADER_NM_START_ROW_NUMBER - 1);
		
		if(headerIdRow != null) {
			
			int lastCellNum = headerIdRow.getLastCellNum();
		
			
			/**  엑셀의 헤더Cell을 List 객체에 따로 담아준다. */
			List excelHeaderList = new ArrayList();
			for(int i = 0; i < lastCellNum; i++) {
			
				XSSFCell headerIdCell = headerIdRow.getCell(i);
				XSSFCell headerNmCell = headerNmRow.getCell(i);
				
				String headerId = headerIdCell.getStringCellValue().trim();	// 형태 : [ 테이블명.컬럼ID ]
				String headerNm = headerNmCell.getStringCellValue().trim();
				String[] tableCols = headerId.split("\\.");
				String tableNm = tableCols[0];
				String colId = tableCols[1];
				
				HashMap excelHeaderMap = new HashMap();
				
				excelHeaderMap.put("tableCols", headerId);
				excelHeaderMap.put("table", tableNm);
				excelHeaderMap.put("columnId", colId);
				excelHeaderMap.put("columnNm", headerNm);
				String dataType = selectColDataType(excelHeaderMap);
				excelHeaderMap.put("dataType", dataType);
				
				excelHeaderList.add(excelHeaderMap);
			}
			
			HashMap gridResourceMap = getGridResourceMapCommon(excelHeaderList);
			resultMap.put("excelHeaderList", excelHeaderList);
			resultMap.put("gridResourceMap", gridResourceMap);
			resultMap.put("lastCellNum", lastCellNum);
			
		}
		return resultMap;
	}
	
	/**
	 * 엑셀 데이터를 파싱하여 List 로 return(Xlsx 용)
	 * @param paramMap
	 * @param sheet
	 * @param excelHeaderList
	 * @param lastCellNum
	 * @return
	 */
	private List getExcelDataMapForXlsx(HashMap paramMap, XSSFSheet sheet, List<HashMap> excelHeaderList, int lastCellNum) throws NkiaException{
		
		int HEADER_DATA_START_ROW_NUMBER = 5;	// 엑셀 자산 데이터 시작 행
		int lastRownum = (Integer) paramMap.get("last_row_num"); // 엑셀 마지막 행번호
		
		DataFormatter formatter = new DataFormatter();
		List<HashMap> dataList = new ArrayList();
		
		// row 반복문
		for(int i = (HEADER_DATA_START_ROW_NUMBER -1) ; i < lastRownum; i++) {
		
			HashMap dataMap = new HashMap();
			XSSFRow dataRow = sheet.getRow(i);
			if(dataRow != null) {
				
				// cell 반복문
				for(int c = 0; c < lastCellNum; c++) {
					
					// 엑셀 헤더
					HashMap headerMap = excelHeaderList.get(c);
					XSSFCell dataCell = dataRow.getCell(c);
					// 값
					String dataCellValue = StringUtil.parseString(formatter.formatCellValue(dataCell)).trim();
					
					if(c == 0 ) {
						// 행번호 컬럼 값 추가(엑셀에 없으므로 따로 추가해준다)
						dataMap.put("EMPTY.ROW_NUM", (i+1));
					}
					
					// dataMap
					getExcelDataMapCommon(dataMap, headerMap, dataCellValue);
				}
			}
			
			
			if(dataMap != null && dataMap.size() > 0) {
				dataList.add(dataMap);
			}
			
		}
		return dataList;
	}
	
	
	

	/**
	 * [엑셀 데이터 파싱 (Xls 용)]
	 * @throws IOException 
	 */
	@Override
	public HashMap parsingExcelForXls(HashMap paramMap,
			String storeFileName,
			String orignlFileName, 
			String fileStoreCours,
			String fileExtsn, 
			String fileContentType) throws NkiaException, IOException{
		
		HashMap resultMap = new HashMap();
		
		// 엑셀파일의 0번째 sheet 를 가져온다
		HSSFSheet sheet = getExcelFileSeetForXls(storeFileName, fileStoreCours, 0);
		
		// 엑셀헤더목록, 그리드리소스, 마지막cell 리턴
		HashMap headerInfoMap = getHeaderInfoMapForXls(paramMap, sheet);
		
		if(headerInfoMap.size() > 0) {
			
			List<HashMap> excelHeaderList = (List) headerInfoMap.get("excelHeaderList");	// 헤더 정보
			HashMap gridResourceMap = (HashMap) headerInfoMap.get("gridResourceMap");	// grid resource 정보
			int lastCellNum = (Integer) headerInfoMap.get("lastCellNum");					// 마지막 cell 번호
			
			// 데이터 정보
			List<HashMap> dataList = getExcelDataMapForXls(paramMap, sheet, excelHeaderList, lastCellNum);
			
			resultMap.put("resultList", dataList);
			resultMap.put("gridResourceMap", gridResourceMap);
		}
			
		return resultMap;
	}
	
	private HSSFSheet getExcelFileSeetForXls(String storeFileName, String fileStoreCours, int sheetNum) throws NkiaException, IOException {
		FileInputStream fis = null;
		HSSFSheet sheet = null;
		
		try {
			String storeFilePath = fileStoreCours + storeFileName;
			File file = new File(storeFilePath);
			fis = new FileInputStream(file);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			sheet = workbook.getSheetAt(sheetNum);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e){
					throw new NkiaException(e);
				}
			}
		}
		
		return sheet;
	}
	
	/**
	 * header 정보를 담아준다(Xls 용)
	 * excelHeaderList : 엑셀 헤더 정보
	 * gridResourceMap : 그리드 resource 정보 
	 * @param paramMap
	 * @param sheet
	 * @return
	 */
	private HashMap<String, Object> getHeaderInfoMapForXls(HashMap paramMap, HSSFSheet sheet) throws NkiaException{
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		// 헤더 정보를 만든다.
		int HEADER_ID_START_ROW_NUMBER = 3;		// 엑셀 헤더ID 시작 행
		int HEADER_NM_START_ROW_NUMBER = 4;		// 엑셀 헤더명 시작 행 
		HSSFRow headerIdRow = sheet.getRow(HEADER_ID_START_ROW_NUMBER - 1);
		HSSFRow headerNmRow = sheet.getRow(HEADER_NM_START_ROW_NUMBER - 1);
		
		if(headerIdRow != null) {
			
			int lastCellNum = headerIdRow.getLastCellNum();
		
			
			/**  엑셀의 헤더Cell을 List 객체에 따로 담아준다. */
			List excelHeaderList = new ArrayList();
			for(int i = 0; i < lastCellNum; i++) {
			
				HSSFCell headerIdCell = headerIdRow.getCell(i);
				HSSFCell headerNmCell = headerNmRow.getCell(i);
				
				String headerId = headerIdCell.getStringCellValue().trim();	// 형태 : [ 테이블명.컬럼ID ]
				String headerNm = headerNmCell.getStringCellValue().trim();
				String[] tableCols = headerId.split("\\.");
				String tableNm = tableCols[0];
				String colId = tableCols[1];
				
				HashMap excelHeaderMap = new HashMap();
				
				excelHeaderMap.put("tableCols", headerId);
				excelHeaderMap.put("table", tableNm);
				excelHeaderMap.put("columnId", colId);
				excelHeaderMap.put("columnNm", headerNm);
				String dataType = selectColDataType(excelHeaderMap);
				excelHeaderMap.put("dataType", dataType);
				
				excelHeaderList.add(excelHeaderMap);
			}
			
			HashMap gridResourceMap = getGridResourceMapCommon(excelHeaderList);
			resultMap.put("excelHeaderList", excelHeaderList);
			resultMap.put("gridResourceMap", gridResourceMap);
			resultMap.put("lastCellNum", lastCellNum);
			
		}
		return resultMap;
	}
	
	/**
	 * 엑셀 데이터를 파싱하여 List 로 return(Xls 용)
	 * @param paramMap
	 * @param sheet
	 * @param excelHeaderList
	 * @param lastCellNum
	 * @return
	 */
	private List getExcelDataMapForXls(HashMap paramMap, HSSFSheet sheet, List<HashMap> excelHeaderList, int lastCellNum) throws NkiaException{
		
		int HEADER_DATA_START_ROW_NUMBER = 5;	// 엑셀 자산 데이터 시작 행
		int lastRownum = (Integer) paramMap.get("last_row_num"); // 엑셀 마지막 행번호
		
		DataFormatter formatter = new DataFormatter();
		List<HashMap> dataList = new ArrayList();
		
		// row 반복문
		for(int i = (HEADER_DATA_START_ROW_NUMBER -1) ; i < lastRownum; i++) {
		
			HashMap dataMap = new HashMap();
			HSSFRow dataRow = sheet.getRow(i);
			if(dataRow != null) {
				
				// cell 반복문
				for(int c = 0; c < lastCellNum; c++) {
					
					// 엑셀 헤더
					HashMap headerMap = excelHeaderList.get(c);
					HSSFCell dataCell = dataRow.getCell(c);
					// 값
					String dataCellValue = StringUtil.parseString(formatter.formatCellValue(dataCell)).trim();
					
					if(c == 0 ) {
						// 행번호 컬럼 값 추가(엑셀에 없으므로 따로 추가해준다)
						dataMap.put("EMPTY.ROW_NUM", (i+1));
					}
					
					// dataMap
					getExcelDataMapCommon(dataMap, headerMap, dataCellValue);
				}
			}
			
			if(dataMap != null && dataMap.size() > 0) {
				dataList.add(dataMap);
			}
		}
		return dataList;
	}
	
	
	
	/**
	 * Grid 헤더 정보 추출
	 * @param headerList
	 * @param classType
	 * @return
	 */
	private HashMap getGridResourceMapCommon(List<HashMap> headerList) throws NkiaException{
		HashMap gridResourceMap = new HashMap();
		
		// 화면 그리드 reousrce 정의
		// 첫 번째 CELL은 행번호로 고정
		StringBuffer gridHeader = new StringBuffer("EMPTY.ROW_NUM");
		StringBuffer gridText = new StringBuffer("행번호");	
		StringBuffer gridWidth = new StringBuffer("70");
		StringBuffer gridFlex = new StringBuffer("1");
		StringBuffer gridSortable = new StringBuffer("na");
		StringBuffer gridAlign = new StringBuffer("center");
		StringBuffer gridXtype = new StringBuffer("na");
		
	
		for(HashMap headerMap : headerList) {
			String tableCols = (String) headerMap.get("tableCols");
			String columnNm = (String) headerMap.get("columnNm");

			/**
			 *  화면 grid 의 resource 정의
			 */
			gridHeader.append("," + tableCols);
			gridText.append("," + columnNm);
			gridWidth.append("," + (columnNm.length() * 35));
			gridFlex.append("," + "na");
			gridSortable.append("," + "na");
			gridAlign.append("," + "left");
			gridXtype.append("," + "na");
			
		}
		
		gridResourceMap.put("gridHeader", gridHeader.toString());
		gridResourceMap.put("gridText", gridText.toString());
		gridResourceMap.put("gridWidth", gridWidth.toString());
		gridResourceMap.put("gridFlex", gridFlex.toString());
		gridResourceMap.put("gridSortable", gridSortable.toString());
		gridResourceMap.put("gridAlign", gridAlign.toString());
		gridResourceMap.put("gridXtype", gridXtype.toString());
		
		return gridResourceMap;
	}
	
	/**
	 * 컬럼 데이터타입 조회
	 * @param paramMap
	 * @return
	 */
	private String selectColDataType(HashMap paramMap) throws NkiaException{
		return batchExcelDAO.selectColDataType(paramMap);
	}
	
	/**
	 * 데이터를 파싱 공통 로직
	 * @param dataMap
	 * @param headerMap
	 * @param commCodeList
	 * @param popupCodeList
	 * @param dataCellValue
	 */
	private void getExcelDataMapCommon(
				HashMap dataMap, 
				HashMap headerMap,
				String dataCellValue) throws NkiaException{
		
		String dataType = (String) headerMap.get("dataType");
		
		// 숫자형의 경우 값에서 콤마 문자를 제거
		if("NUMBER".equals(dataType)){
			dataCellValue = dataCellValue.replace(",", "");
		}else if("DATE".equals(dataType)){
			dataCellValue = getChgDateFormat(dataCellValue, "yyyy-MM-dd");
		}
		
		dataMap.put((String)headerMap.get("tableCols"), dataCellValue);
	}
	
	
	/**
	 * 날짜 형식 변경
	 * @param dateValue
	 * @param toFormat
	 * @return
	 */
	private String getChgDateFormat(String dateValue, String toFormat) throws NkiaException{
		String resultValue = "";
		String fromFormat = "";
		
		// 기본 데이트포멧
		if(toFormat == null) {
			toFormat = "yyyy-MM-dd";
		}
		
		// 기존 데이트포멧 찾기
		SimpleDateFormat dateFormatParser1 = new SimpleDateFormat("MM/dd/yy");
		try{
			dateFormatParser1.parse(dateValue);
			fromFormat = "MM/dd/yy";
		}catch(Exception e) {
			/*try {
				SimpleDateFormat dateFormatParser2 = new SimpleDateFormat("yyyy-MM-dd");
				dateFormatParser2.parse(dateValue);
				fromFormat = "yyyy-MM-dd";
			} catch (Exception e2) {
				try {
					SimpleDateFormat dateFormatParser3 = new SimpleDateFormat("yyyyMMdd");
					dateFormatParser3.parse(dateValue);
					fromFormat = "yyyyMMdd";
				} catch (Exception e3) {
					
				}
				
			}*/
			throw new NkiaException(e);
		}
		
		// 데이트 포맷 변경
		try {
			SimpleDateFormat fFormat = new SimpleDateFormat(fromFormat);
			SimpleDateFormat tFormat = new SimpleDateFormat(toFormat);
			Date chageDate = fFormat.parse(dateValue);
			
			resultValue = tFormat.format(chageDate);
		
		} catch (ParseException e) {
			resultValue = dateValue;
		}
		return resultValue;
	}

	/**
	 *  데이터 Validation
	 * - 데이터형식 (날짜, 숫자)
	 * - 문자열 길이
	 * - 필수값(notNull)
	 * @throws NkiaException 
	 * @throws UnsupportedEncodingException 
	 */
	@Override
	public HashMap validateBatchExcelData(HashMap paramMap) throws NkiaException, UnsupportedEncodingException {
		
		List<HashMap> dataList = (List) paramMap.get("dataList");
		List<HashMap> gridHeaderList = (List) paramMap.get("gridHeaderList");
		
		boolean isSuccess = true;
		StringBuffer resultMsg = new StringBuffer();	// 결과메세지
		List validationReusltList = new ArrayList();	// validation 실패 항목 리스트
		
		String insertType = (String) paramMap.get("insertType");
		if("UPDATE".equals(insertType)) {
			// [ 테이블의 Primary Key 생성 여부 ]
			List<String> pkNotExistTable = batchExcelDAO.selectPkNotExistTable(gridHeaderList);
			if(!pkNotExistTable.isEmpty()) {
				for(String tableNm : pkNotExistTable) {
					isSuccess = false;
					resultMsg.append( tableNm + " 테이블의 Primary Key 가 존재하지 않습니다. \n일괄수정 기능은 Primary Key가 있는 테이블 데이터만 수정 가능합니다.\n");
				}
			}else{
				// [엑셀 항목에 Primary Key 존재 여부]
				List<HashMap<String, String>> pkList = batchExcelDAO.selectPkColList(gridHeaderList);
				for(HashMap<String, String> pkMap : pkList) {
					String tableNm = pkMap.get("TABLE_NAME");
					String columnNm = pkMap.get("COLUMN_NAME");
					boolean isPkExist = false;
					for(HashMap<String, String> gridHeaderMap : gridHeaderList) {
						if( tableNm.equals(gridHeaderMap.get("table")) && columnNm.equals(gridHeaderMap.get("columnId")) ){
							isPkExist = true;
							break;
						}
					}
					if(!isPkExist){
						isSuccess = false;
						resultMsg.append( tableNm + " 테이블의 Primary Key (" + columnNm +") 가 정의되지 않았습니다.\n");
					}
				}
				
			}
			
			
		}else if("INSERT".equals(insertType)){
			// [ 등록 대상 테이블의 not null 컬럼 조회 ]
			List<HashMap> notNullColList = batchExcelDAO.selectNotNullColList(gridHeaderList);
			
			// [필수값 컬럼 존재 여부]
			boolean isKeyExist = false;
			for(HashMap<String, String> notNullColMap : notNullColList) {
				String tableName = notNullColMap.get("TABLE_NAME");
				String columnName = notNullColMap.get("COLUMN_NAME");
				
				for(HashMap gridHeaderMap : gridHeaderList) {
					String headerTable = (String) gridHeaderMap.get("table");
					String headerColumnId = (String) gridHeaderMap.get("columnId");
					if(headerTable.equals(tableName) && headerColumnId.equals(columnName)) {
						isKeyExist = true;
						break;
					}
				}
				
				if(!isKeyExist) {
					isSuccess = false;
					resultMsg.append( tableName + " 테이블의 필수컬럼(" + columnName +")이 정의되지 않았습니다. \n엑셀 양식에서 필수컬럼을 추가해주십시오.");
				}
			}
		}
		
		if(isSuccess) {
			// validation을 위한 테이블/컬럼 목록 생성
			for(HashMap gridHeaderMap : gridHeaderList) {
				String headerTable = (String) gridHeaderMap.get("table");
				String headerColumnId = (String) gridHeaderMap.get("columnId");
				
				//컬럼정보목록 조회(테이블,컬럼,데이터타입,문자열길이,null허용, 컬럼 존재여부)
				HashMap colInfoMap = batchExcelDAO.selectTblColInfo(gridHeaderMap);
				if(colInfoMap != null) {
					gridHeaderMap.putAll(colInfoMap);
					gridHeaderMap.put("isColumnExist", true);	// 테이블컬럼존재여부
				}else{
					gridHeaderMap.put("isColumnExist", false);
				}
				
			}

			int gridRow = 0;	// 그리드 행
			for(HashMap dataMap : dataList){
				int validFailCnt = 0;
				
				dataMap.remove("id");		// grid 속성 id 제거
				int rowNum = (Integer) dataMap.get("EMPTY.ROW_NUM");	// 엑셀 행번호
				
				Iterator iter = (Iterator) dataMap.keySet().iterator();
				while(iter.hasNext()){
					HashMap validFalseDataMap = new HashMap();
					String tableCol = (String) iter.next();	// 형태 : [ 테이블ID.컬럼ID ]
					String columnValue = StringUtil.parseString(dataMap.get(tableCol));
					
					if(!"EMPTY.ROW_NUM".equals(tableCol)){		// 행번호는 VALIDATION 제외
						
						for(HashMap colInfo : gridHeaderList){
							String colKey = (String) colInfo.get("tableCol");
							String colNm = (String) colInfo.get("columnNm");
							if(tableCol.equals(colKey)){
								
								// ***** [컬럼 존재여부 체크] *******
								boolean isExistColumn = (Boolean) colInfo.get("isColumnExist");
								if(!isExistColumn){
									isSuccess = false;
									resultMsg.append(rowNum + " 번행의 " + colNm + " 테이블 컬럼정보가 존재하지않습니다. \n");
									validFalseDataMap.put("IDX", gridRow);
									validFalseDataMap.put("COLUMN", tableCol);
									validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
									validFailCnt++;
									
								}else{
									
									if("".equals(columnValue)) {
										// ***** [NULL 체크] *******
										String nullAble = (String) colInfo.get("NULLABLE");
										if("N".equals(nullAble)){
											isSuccess = false;
											resultMsg.append(rowNum + " 번행의 " + colNm + " 은(는) 필수입력입니다. \n");
											validFalseDataMap.put("IDX", gridRow);
											validFalseDataMap.put("COLUMN", tableCol);
											validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
											validFailCnt++;
										}		// end Null체크
									}else {
										// ***** [데이터 타입 체크] *******
										String dataType = (String) colInfo.get("DATA_TYPE");
										if("NUMBER".equals(dataType)){
											if(!isDataFormatCheck(dataType, columnValue)){
												isSuccess = false;
												resultMsg.append(rowNum + " 번행의 " + colNm + " 은(는) 숫자만 입력가능합니다. \n");
												validFalseDataMap.put("IDX", gridRow);
												validFalseDataMap.put("COLUMN", tableCol);
												validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
												validFailCnt++;
											}
										}else if("DATE".equals(dataType)){
											if(!isDataFormatCheck(dataType, columnValue)){
												isSuccess = false;
												resultMsg.append(rowNum + " 번행의 " + colNm + " 은(는)  날짜 형식이 잘못되었습니다. \n");
												validFalseDataMap.put("IDX", gridRow);
												validFalseDataMap.put("COLUMN", tableCol);
												validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
												validFailCnt++;
											}
										}else{
											
											// ***** [문자열 길이체크] *******
											String dataLengthStr = (String) colInfo.get("DATA_LENGTH");
//											int dataLength = (int) colInfo.get("DATA_LENGTH");
											if(dataLengthStr != null  && !"".equals(dataLengthStr)){
												int dataLength = Integer.parseInt(dataLengthStr);   
												
												int dataValueLength =  columnValue.getBytes(BaseConstraints.APP_DATABASE_CHARSET).length;
												if(dataValueLength >  dataLength) {
													isSuccess = false;
													resultMsg.append(rowNum + " 번행의 " + colNm + " 이 작성 가능한 길이를 초과하였습니다. [ " + dataValueLength + "/" + dataLength + " ] bytes \n");
													validFalseDataMap.put("IDX", gridRow);
													validFalseDataMap.put("COLUMN", tableCol);
													validationReusltList.add(validFalseDataMap);	// validation 실패항목을 List에 담아준다.
													validFailCnt++;
												}
											}
											
										}
									}
									
								}
								
								break;
							}
							
						}
					}
					
				}
				
				gridRow++;
				if(validFailCnt > 0) {
					resultMsg.append("\n");
				}
			}
		}
		
		HashMap resultMap = new HashMap();
		resultMap.put("isSuccess", isSuccess);
		if(isSuccess){
			resultMsg.append("검증 성공하였습니다.");
		}
		resultMap.put("resultMsg", resultMsg.toString());
		resultMap.put("resultList", validationReusltList);
		
		return resultMap;
	}
	
	/**
	 * 데이터 타입 체크
	 * @param dataType [DATE, NUMBER]
	 * @param value
	 * @return
	 * @throws NkiaException 
	 */
	private boolean isDataFormatCheck(String dataType, String value) throws NkiaException {
		boolean result = true;
		
		if("DATE".equals(dataType)) {
			SimpleDateFormat dateFormatParser1 = new SimpleDateFormat("MM/dd/yy");
			try{
				dateFormatParser1.setLenient(false);
				dateFormatParser1.parse(value);
			}catch(Exception e) {
				/*try {
					SimpleDateFormat dateFormatParser2 = new SimpleDateFormat("yyyy-MM-dd");
					dateFormatParser2.setLenient(false); 
					dateFormatParser2.parse(value);
				} catch (Exception e2) {
					try {
						SimpleDateFormat dateFormatParser3 = new SimpleDateFormat("yyyyMMdd");
						dateFormatParser3.setLenient(false); 
						dateFormatParser3.parse(value);
					} catch (Exception e3) {
						result = false;
					}
					
				}*/
				throw new NkiaException(e);
			}
		} else if("NUMBER".equals(dataType)) {
			try {
				Double.parseDouble(value);
			} catch (NumberFormatException e) {
				result = false;
			}
		}
		
		return result;
	}

	/**
	 * 일괄 데이터 등록
	 */
	@Override
	public HashMap insertBatchExcelData(HashMap paramMap) throws NkiaException {

		HashMap resultMap = new HashMap();
		
		// 등록 대상 목록
		List<HashMap> dataList = (List) paramMap.get("dataList");
		// 헤더 목록
		List<HashMap> headerList = (List) paramMap.get("headerList");
		
		// 데이터 INSERT 및 저장 실패 데이터 리턴
		CustomizeQueryExecute cQueryExecute = new CustomizeQueryExecute();
		String menuType = (String) paramMap.get("menuType");
		List insertFailList = new ArrayList();
		
		// menuType 에 따라 별도의 등록쿼리 작성 및 실행
		if("BASIC".equals(menuType)) {
			insertFailList = cQueryExecute.executeBasicBatchInsertSql(headerList, dataList);
		}else{ 
			insertFailList = cQueryExecute.executeBasicBatchInsertSql(headerList, dataList);
		}
		
		boolean isAllSuccess = true;
		if(!insertFailList.isEmpty()){
			isAllSuccess = false;	// 모든 데이터 저장 성공여부
		}
		resultMap.put("insertFailList", insertFailList);	// 등록실패목록
		resultMap.put("isAllSuccess", isAllSuccess);		// 모든 데이터 저장 성공여부
		resultMap.put("errorCnt", insertFailList.size());	// 오류 데이터 건수
		resultMap.put("totalCnt", dataList.size());			// 전체 데이터 건수
		
		return resultMap;
	}
	
	/**
	 * 일괄 데이터 수정 실행
	 */
	@Override
	public HashMap updateBatchExcelData(ModelMap paramMap) throws NkiaException {

		HashMap resultMap = new HashMap();
		boolean isAllSuccess = true;
		
		// 등록 대상 목록
		List<HashMap> dataList = (List) paramMap.get("dataList");
		// 헤더 목록
		List<HashMap> headerList = (List) paramMap.get("headerList");
		// 저장 실패 데이터 목록
		List<HashMap> insertFailList = new ArrayList();
		
		UserVO userVO = new UserVO();
		try {
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String userId = "";
		if(userVO != null) {
			userId = (String)userVO.getUser_id();
		}
		paramMap.put("ins_user_id", userId);
		
		// 등록 대상 테이블 추출
		List<String> tableArr = new ArrayList();
		 
		for(HashMap headerMap : headerList) {
			String tableNm = (String) headerMap.get("table");
			if(!"EMPTY".equals(tableNm)){		// 테이블명이 EMPTY 인 건 제외(행번호임)
				if(!tableArr.contains(tableNm)){
					tableArr.add(tableNm);
				}
			}
		}
		
		// 등록 대상 테이블의 KEY 컬럼 조회 (UPDATE 문의 조회조건에 사용)
		List<HashMap> pkColList = batchExcelDAO.selectPkColList(headerList);
		
		// 등록 대상 테이블 별 쿼리 생성
		for(HashMap dataMap : dataList) {
			
			try {
				
				for(String trgetTable : tableArr) {
					
					List<String> pkCols = new ArrayList();
					// 타겟테이블의 key컬럼 목록 생성
					for(HashMap<String, String> pkColsMap : pkColList){
						String tableName = pkColsMap.get("TABLE_NAME");
						if(tableName.equals(trgetTable)){
							pkCols.add(pkColsMap.get("COLUMN_NAME"));
						}
					}
					
					
					 // 등록 타겟 테이블의 수정컬럼 추출해서 수정query 만듬
					String conjunction = "";
					StringBuffer updColQuery = new StringBuffer();		// 컬럼 수정 쿼리
					StringBuffer conditionQuery = new StringBuffer();	// 수정 쿼리 조건문
	
					for(HashMap<String, String> headerMap : headerList) {
						
						String tableCol = headerMap.get("tableCol");
						String table = headerMap.get("table");
						if(trgetTable.equals(table)){
							String columnId = headerMap.get("columnId");	// 컬럼Id
							String columnNm = headerMap.get("columnNm");	// 컬럼명
							String columnValue = StringUtil.parseString(dataMap.get(tableCol));	// 데이터 값
							
							if(updColQuery.length() > 1){
								conjunction = ",";
							}
							
							updColQuery.append(conjunction + columnId + " = '" + columnValue + "' \n");
							
							// PK인 경우 조건쿼리 생성
							if(pkCols.contains(columnId)){
								if("".equals(columnValue)){
									// 키값이 조회조건이 되어야하므로 공백이면 수정 쿼리 실행 못하도록 함
									conditionQuery.setLength(0);	//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
									dataMap.put("message", trgetTable + " 테이블의 Primary Key (" + columnNm + ") 의 값이 없습니다.");
									insertFailList.add(dataMap);
									break;
								}else{
									if(conditionQuery.length() < 1 ) {
										conditionQuery.append("WHERE " + columnId + " = '" + columnValue + "' ");
									}else{
										conditionQuery.append(" AND " + columnId + " = '" + columnValue + "' ");
									}
									
								}
							}
	
						}
					}
					
					// 키 컬럼이 있고, 수정 속성이 있는 경우 쿼리 실행
					if(conditionQuery != null && conditionQuery.length() > 1 ){
						
						if(updColQuery.length() > 1) {
							StringBuffer queryString = new StringBuffer("UPDATE " + trgetTable + " SET ");
							queryString.append(updColQuery.toString());
							// key 조건 추가
							queryString.append(conditionQuery);

							// update 쿼리 실행
							batchExcelDAO.executeQuery(queryString.toString());
						}
					}else{
						isAllSuccess = false;
						dataMap.put("message", trgetTable + " 테이블의 Primary Key 가 존재하지 않습니다.");
						insertFailList.add(dataMap);
					}
					
				}
	
			
			} catch (Exception e) {
				/*dataMap.put("message", e.getMessage());
				insertFailList.add(dataMap);*/
				throw new NkiaException(e);
			}
		}
		
		if(!insertFailList.isEmpty()){
			isAllSuccess = false;	// 모든 데이터 저장 성공여부
		}
		resultMap.put("insertFailList", insertFailList);	// 등록실패목록
		resultMap.put("isAllSuccess", isAllSuccess);		// 모든 데이터 저장 성공여부
		resultMap.put("errorCnt", insertFailList.size());	// 오류 데이터 건수
		resultMap.put("totalCnt", dataList.size());			// 전체 데이터 건수
		
		return resultMap;
	}
	
}
