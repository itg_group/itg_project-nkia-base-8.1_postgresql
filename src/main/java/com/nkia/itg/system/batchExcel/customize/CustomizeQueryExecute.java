package com.nkia.itg.system.batchExcel.customize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.batchExcel.dao.BatchExcelDAO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

public class CustomizeQueryExecute {

	/**
	 * [BASIC]
	 * 데이터 등록 쿼리를 만들고 실행한다.
	 * @return insertFailList : 저장 실패 데이터 목록
	 * @throws NkiaException 
	 */
	public List executeBasicBatchInsertSql(List<HashMap> headerList, List<HashMap> dataList) throws NkiaException {
		List insertFailList = new ArrayList();
		UserVO userVO = new UserVO();
		try {
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String userId = "";
		if(userVO != null) {
			userId = (String)userVO.getUser_id();
		}
		
		// 등록 대상 테이블 추출(distinct)
		List<String> tableArr = new ArrayList();
		 
		for(HashMap headerMap : headerList) {
			String tableNm = (String) headerMap.get("table");
			if(!"EMPTY".equals(tableNm)){		// 테이블명이 EMPTY 인 건 제외(행번호임)
				if(!tableArr.contains(tableNm)){
					tableArr.add(tableNm);
				}
			}
		}
			
		/**
		 * 등록 대상 테이블 별 쿼리 생성
		 * 쿼리 예시
		 * -------------------------------------
		 * INSERT ALL
		 * INTO TABLE1 (COLUMN1, COLUMN2) VALUES (VALUE1 , VALUE2)
		 * INTO TABLE2 (COLUMN1, COLUMN2) VALUES (VALUE1 , VALUE2)
		 * SELECT * FROM DUAL
		 * -------------------------------------
		 */
		for(HashMap dataMap : dataList) {
			
			try {
				StringBuffer queryString = new StringBuffer("INSERT ALL \n");
				for(String trgetTable : tableArr) {
					
					// 등록 타겟 테이블의 컬럼 추출
					String conjunction = "";
					StringBuffer colIds = new StringBuffer();		// 컬럼 ID
					StringBuffer colValues = new StringBuffer();	// 컬럼 값
	
					for(HashMap<String, String> headerMap : headerList) {
						
						if(colIds.length() > 1){
							conjunction = ", ";
						}
						
						String tableCol = headerMap.get("tableCol");
						String table = headerMap.get("table");
						
						if(trgetTable.equals(table)){
							String columnId = headerMap.get("columnId");	// 컬럼Id
							String columnValue = StringUtil.parseString(dataMap.get(tableCol));	// 데이터 값
							
							// 등록자 정보는 하단에서 추가하므로 제외.
							if(!columnId.equals("INS_USER_ID") && !columnId.equals("INS_DT")){		
								colIds.append(conjunction + columnId);
								colValues.append(conjunction + "'" + columnValue + "'");
							}
						}
						
					}
					
					if(colIds.length() > 1){

						// 등록자 정보를 고정으로 추가한다.(INS_USER_ID, INS_DT 가 생성되어있어야함)
						colIds.append(",INS_USER_ID, INS_DT");
						colValues.append(",'" + userId + "', SYSDATE");
						
						// INSERT QUERY 연결
						String tableQuery = "INTO " + trgetTable + "(" + colIds.toString() + ") VALUES (" + colValues.toString() + ") \n";
						queryString.append(tableQuery);
						
					}
				}
				queryString.append("SELECT * FROM DUAL");

				// 쿼리 실행
				BatchExcelDAO batchExcelDAO = (BatchExcelDAO) NkiaApplicationContext.getCtx().getBean("batchExcelDAO");
				batchExcelDAO.executeQuery(queryString.toString());
			
			} catch (Exception e) {
				dataMap.put("message", e.getMessage());
				insertFailList.add(dataMap);
				new NkiaException(e);
			}
		}
		
		return insertFailList;
	}
	
}
