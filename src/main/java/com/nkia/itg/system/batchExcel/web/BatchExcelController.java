package com.nkia.itg.system.batchExcel.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.itam.regist.service.AssetExcelRegistService;
import com.nkia.itg.system.batchExcel.service.BatchExcelService;

@Controller
public class BatchExcelController {

	@Resource(name="batchExcelService")
	private BatchExcelService batchExcelService;
	
	@Resource(name="assetExcelRegistService")
	private AssetExcelRegistService assetExcelRegistService;
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil; 
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 일괄 업로드 화면 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/batchExcel/goBatchExcelRegist.do")
	public String goBatchExcelRegist(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String menuType = request.getParameter("menuType");
		// menuType 이 없을 경우, 기본으로 BASIC 사용
		if(menuType == null){
			menuType = "BASIC";
		}
		paramMap.put("menuType", menuType);
		String forwarPage = "/itg/system/batchExcel/batchExcelRegist_" + menuType + ".nvf";
		return forwarPage;
	}
	
	/**
	 * 엑셀 데이터를 파싱한다.
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws NkiaExceptionp
	 */
	@RequestMapping(value="/itg/itam/batchExcel/parsingExcelTemplateData.do")
	public @ResponseBody ResultVO parsingExcelTemplateData(@RequestBody ModelMap paramMap) throws NkiaException, IOException{
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		try {
			// 업로드된 파일 정보 조회
			AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO();
			atchFileDetailVO.setAtch_file_id((String) paramMap.get("atch_file_id"));
			atchFileDetailVO.setFile_idx("SINGLE_FILE");
			atchFileDetailVO = atchFileService.selectAtchFileDetail(atchFileDetailVO);
			String extsn = atchFileDetailVO.getFile_extsn();
			
			if("xlsx".equals(extsn)){
				resultMap = batchExcelService.parsingExcelForXlsx(
						paramMap,
						atchFileDetailVO.getStore_file_name(),
						atchFileDetailVO.getOrignl_file_name(),
						atchFileDetailVO.getFile_store_cours(),
						atchFileDetailVO.getFile_extsn(),
						atchFileDetailVO.getFile_content_type()
						);
			}else if("xls".equals(extsn)){
				resultMap = batchExcelService.parsingExcelForXls(
						paramMap,
						atchFileDetailVO.getStore_file_name(),
						atchFileDetailVO.getOrignl_file_name(),
						atchFileDetailVO.getFile_store_cours(),
						atchFileDetailVO.getFile_extsn(),
						atchFileDetailVO.getFile_content_type()
						);
			}
			resultVO.setSuccess(true);
			resultVO.setResultMsg("업로드가 완료되었습니다. <br>검증을 실행하여 주십시오.</br>");
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg("업로드 처리 중 오류가 발생되었습니다. <br>첨부파일을 확인하여 주십시오.</br>");
			throw new NkiaException(e);
		} finally {
			resultVO.setResultMap(resultMap);
			GridVO gridVO = new  GridVO();
			List resultList = (List) resultMap.get("resultList");
			if(resultList != null) {
				gridVO.setRows(resultList);
				gridVO.setTotalCount(resultList.size());
			}
			resultVO.setGridVO(gridVO);
		}
		
		return resultVO;
	}
	
	/**
	 * 데이터 Validation
	 * - 데이터형식 (날짜, 숫자)
	 * - 문자열 길이
	 * - 필수값
	 * @param paramMap
	 * @return
	 * @throws NkiaException 
	 * @throws NkiaException
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value="/itg/itam/batchExcel/validateBatchExcelData.do")
	public @ResponseBody ResultVO validateBatchExcelData(@RequestBody ModelMap paramMap) throws NkiaException, UnsupportedEncodingException {
		ResultVO resultVO = new ResultVO();
		try {
			
			HashMap resultMap = batchExcelService.validateBatchExcelData(paramMap);
			
			resultVO.setResultMap(resultMap);
			resultVO.setSuccess(true);
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 일괄 데이터 저장처리
	 * @param paramMap
	 * @return
	 * @throws NkiaException 
	 */
	@RequestMapping(value="/itg/itam/batchExcel/insertBatchExcelData.do")
	public @ResponseBody ResultVO insertBatchExcelData(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		try {
			
			resultMap = batchExcelService.insertBatchExcelData(paramMap);
			
			resultVO.setSuccess(true);
		
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultMap.put("errMsg", e.getMessage());
			throw new NkiaException(e);			
		} finally {
			resultVO.setResultMap(resultMap);
		}
		return resultVO;
	}
	
	/**
	 * 일괄 데이터 수정처리
	 * @param paramMap
	 * @return
	 * @throws NkiaException 
	 */
	@RequestMapping(value="/itg/itam/batchExcel/updateBatchExcelData.do")
	public @ResponseBody ResultVO updateBatchExcelData(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		try {
			
			resultMap = batchExcelService.updateBatchExcelData(paramMap);
			
			resultVO.setSuccess(true);
		
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultMap.put("errMsg", e.getMessage());
			throw new NkiaException(e);
		} finally {
			resultVO.setResultMap(resultMap);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/itam/batchExcel/basicExcelTemplateDown.do")
	public @ResponseBody ResultVO basicExcelTemplateDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try {
			
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");

			List resultList = new ArrayList();
			excelMap = excelMaker.makeExcelFileForBatchExcel(excelParam, resultList);

			resultVO.setResultMap(excelMap);
	
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}

	
	
	/**
	 * 등록 오류 데이터 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/itam/batchExcel/selectBatchResultListExcelDown.do")
	public @ResponseBody ResultVO selectBatchResultListExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try {
			
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = (List) param.get("resultList");
			excelMap = excelMaker.makeExcelFileForBatchExcel(excelParam, resultList);

			resultVO.setResultMap(excelMap);
	
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	
	/**
	 * 업로드 된 엑셀템플릿 파일을 파싱
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/batchExcel/getListForGrid.do")
	public @ResponseBody ResultVO getListForGrid(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new  GridVO();
		List resultList = (List) paramMap.get("paramList");
		gridVO.setRows(resultList);
		gridVO.setTotalCount(resultList.size());
		resultVO.setGridVO(gridVO);
		return resultVO;
	}
	
	/**
	 * 서버의 Template 파일을 다운로드한다
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/batchEdit/downloadTemplateFile.do")
	public void downloadTemplateFile(HttpServletRequest request, HttpServletResponse response, ModelMap paramMap) throws NkiaException, IOException {
		try {
			
			String fileNm = request.getParameter("file_name");
			String filePath = NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Template.Path");
			
			boolean isSuccess = atchFileUtil.createOutputFile(request, response, fileNm, fileNm, filePath, null);
			
			if( !isSuccess ){
				PrintWriter out = response.getWriter();
				response.setContentType("text/html; charset=utf-8");
				out.println("FileNotFound");
			}
		} catch (NkiaException e) {
			/*PrintWriter out = response.getWriter();
			response.setContentType("text/html; charset=utf-8");
			out.println("FileNotFound");*/
			throw new NkiaException(e);
		}
	}
	
	
}
