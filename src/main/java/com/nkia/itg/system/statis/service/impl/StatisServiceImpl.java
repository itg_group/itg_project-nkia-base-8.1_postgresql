package com.nkia.itg.system.statis.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.system.statis.dao.StatisDAO;
import com.nkia.itg.system.statis.service.StatisService;

@Service("statisService")
public class StatisServiceImpl implements StatisService {

	@Resource(name = "StatisDAO")
	public StatisDAO statisDAO;
	
	/**
	 * 고객만족도 평가 조회
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public Map selectStatisInfo(ModelMap paramMap) throws NkiaException {
		paramMap = (ModelMap) WebUtil.lowerCaseMapKey(paramMap);
		Map resultInfo = statisDAO.selectStatisInfo(paramMap);
		
		return resultInfo;
	}

	/**
	 * 고객만족도 평가 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertStatisInfo(ModelMap paramMap) throws NkiaException {
		paramMap = (ModelMap) WebUtil.lowerCaseMapKey(paramMap);
		statisDAO.insertStatisInfo(paramMap);
	}
	
	/**
	 * 고객만족도 평가 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void upDateStatisInfo(ModelMap paramMap) throws NkiaException {
		paramMap = (ModelMap) WebUtil.lowerCaseMapKey(paramMap);
		statisDAO.upDateStatisInfo(paramMap);
	}
	
	/**
	 * 고객만족도 평가 점수 합산 반영
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void upDateStatisResult(ModelMap paramMap) throws NkiaException {
		paramMap = (ModelMap) WebUtil.lowerCaseMapKey(paramMap);
		statisDAO.upDateStatisResult(paramMap);
	}
}
