package com.nkia.itg.system.statis.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("StatisDAO")
public class StatisDAO extends EgovComAbstractDAO {
	
	
	/**
	 * 고객만족도 평가 조회
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public Map selectStatisInfo(ModelMap paramMap) throws NkiaException {
		return (Map)selectByPk("StatisDAO.selectStatisInfo", paramMap);
	}
	
	
	
	/**
	 * 고객만족도 평가 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertStatisInfo(ModelMap paramMap) throws NkiaException {
		this.update("StatisDAO.insertStatisInfo", paramMap);
	}
	
		
	/**
	 * 고객만족도 평가 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void upDateStatisInfo(ModelMap paramMap) throws NkiaException {
		this.update("StatisDAO.upDateStatisInfo", paramMap);
	}
	
	/**
	 * 고객만족도 평가 점수 합산 반영
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void upDateStatisResult(ModelMap paramMap) throws NkiaException {
		this.update("StatisDAO.upDateStatisResult", paramMap);
	}

}
