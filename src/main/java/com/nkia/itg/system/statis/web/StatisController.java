package com.nkia.itg.system.statis.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.exception.NkiaException;

import org.springframework.stereotype.Controller;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.statis.service.StatisService;


@Controller
public class StatisController {

	@Resource(name = "statisService")
	private StatisService statisService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 
	 * 고객만족도평가 리스트팝업 호출
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/statis/goStatisPopup.do")
	public String goStatisListPopup(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		Map resultMap = new HashMap();
		String forwarPage = "/itg/system/statis/popup/statisPopup.nvf";
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();		
		String ins_user_id = userVO.getUser_id();
		paramMap.put("sr_id", request.getParameter("sr_id"));
		paramMap.put("ins_user_id", ins_user_id);
		return forwarPage;
	}
	
	/**
	 * 고객만족도평가 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/statis/selectStatisInfo.do")
	public @ResponseBody ResultVO selectStatisInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		Map resultMap = new HashMap();
		ResultVO resultVO = new ResultVO();
		try{
			resultMap = statisService.selectStatisInfo(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 
	 * 고객만족도평가 등록
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/statis/insertStatisListInfo.do")
	public @ResponseBody ResultVO insertStatisListInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			// PROC_MASTER STATIS_RESULT 컬럼 업데이트 처리
			statisService.upDateStatisResult(paramMap);
			statisService.insertStatisInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 
	 * 고객만족도평가 수정
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/statis/upDateStatisListInfo.do")
	public @ResponseBody ResultVO upDateStatisListInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			// PROC_MASTER STATIS_RESULT 컬럼 업데이트 처리
			statisService.upDateStatisResult(paramMap);
			statisService.upDateStatisInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 고객만족도평가 점수합산
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/statis/aVeragePointSum.do")
	public @ResponseBody ResultVO aVeragePointSum(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		String point = "";
		try{
			String code1 = (String)paramMap.get("STATIS_CODE_01");
			String code2 = (String)paramMap.get("STATIS_CODE_02");
			String code3 = (String)paramMap.get("STATIS_CODE_03");
			String code4 = (String)paramMap.get("STATIS_CODE_04");
			
			int pointSum = 0;
			pointSum += getPoint(code1);
			pointSum += getPoint(code2);
			pointSum += getPoint(code3);
			pointSum += getPoint(code4);
			
			int incident_mod = pointSum / paramMap.size();
			int strIncident_mod = 0;
			
			if(incident_mod > 90){
				strIncident_mod = 1;
			}else if(incident_mod <= 90 && incident_mod > 80){
				strIncident_mod = 2;
			}else if(incident_mod <= 80 && incident_mod > 70){
				strIncident_mod = 3;
			}else if(incident_mod <= 70 && incident_mod > 60){
				strIncident_mod = 4;
			}else if(incident_mod <= 60){
				strIncident_mod = 5;
			}
			
			resultVO.setResultInt(strIncident_mod);
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	private int getPoint(String statistic_reulst)throws NkiaException {
		int pointValue = 0;
		
		if("1".equals(statistic_reulst)){
			pointValue = 100;
		}else if("2".equals(statistic_reulst)){
			pointValue = 90;
		}else if("3".equals(statistic_reulst)){
			pointValue = 80;
		}else if("4".equals(statistic_reulst)){
			pointValue = 70;
		}else if("5".equals(statistic_reulst)){
			pointValue = 60;
		}else{
			pointValue = 0;
		}
		return pointValue;
	}
}
