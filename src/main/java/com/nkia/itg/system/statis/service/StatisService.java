package com.nkia.itg.system.statis.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface StatisService {
		
	/**
	 * 고객만족도 평가 조회
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */        
	public Map selectStatisInfo(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 고객만족도 평가 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertStatisInfo(ModelMap paramMap) throws NkiaException; 
	
	/**
	 * 고객만족도 평가 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void upDateStatisInfo(ModelMap paramMap) throws NkiaException; 
	
	/**
	 * 고객만족도 평가 점수 합산 반영
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void upDateStatisResult(ModelMap paramMap) throws NkiaException; 

}
