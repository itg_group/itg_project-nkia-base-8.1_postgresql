package com.nkia.itg.system.excelHistory.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings({ "rawtypes" })
@Repository("excelHistoryDAO")
public class ExcelHistoryDAO extends EgovComAbstractDAO {
	
	/**
	 * 
	 * 엑셀 출력 내역 리스트 조회
	 * 
	 * @param param
	 * @return List
	 * @throws Exception
	 */
	public List searchExcelHistoryList(Map paramMap) {
		return list("ExcelFileDAO.searchExcelHistoryList", paramMap);
	}
	
	/**
	 * 
	 * 엑셀 출력 내역 리스트 TotalCount 조회
	 * 
	 * @param param
	 * @return int
	 * @throws Exception
	 */
	public Integer searchExcelHistoryListCount(Map paramMap) {
		return (Integer) selectByPk("ExcelFileDAO.searchExcelHistoryListCount", paramMap);
	}
}
