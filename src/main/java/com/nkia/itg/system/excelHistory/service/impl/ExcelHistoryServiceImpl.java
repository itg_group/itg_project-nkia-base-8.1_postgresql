package com.nkia.itg.system.excelHistory.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.system.excelHistory.dao.ExcelHistoryDAO;
import com.nkia.itg.system.excelHistory.service.ExcelHistoryService;

@SuppressWarnings({ "rawtypes" })
@Service("ExcelHistoryService")
public class ExcelHistoryServiceImpl implements ExcelHistoryService {
	
	@Resource(name = "excelHistoryDAO")
	private ExcelHistoryDAO excelHistoryDAO;

	/**
	 * 
	 * 엑셀 출력 내역 리스트 TotalCount 조회
	 * 
	 * @param param
	 * @return int
	 * @throws Exception
	 */
	public int searchExcelHistoryListCount(ModelMap paramMap) {
		return excelHistoryDAO.searchExcelHistoryListCount(paramMap);
	}

	/**
	 * 
	 * 엑셀 출력 내역 리스트 조회
	 * 
	 * @param param
	 * @return List
	 * @throws Exception
	 */
	public List searchExcelHistoryList(ModelMap paramMap) {
		return excelHistoryDAO.searchExcelHistoryList(paramMap);
	}
}
