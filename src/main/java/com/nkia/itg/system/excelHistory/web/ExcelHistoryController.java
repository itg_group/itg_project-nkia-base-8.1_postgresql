package com.nkia.itg.system.excelHistory.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.excelHistory.service.ExcelHistoryService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({ "rawtypes" })
@Controller
public class ExcelHistoryController {
	
	@Resource(name="ExcelHistoryService")
	ExcelHistoryService excelHistoryService;
	
	/**
	 * excelHistory(엑셀 출력 내역) 페이지 이동
	 * 
	 * @param paramMap
	 * @return String
	**/
	@RequestMapping(value="/itg/system/excelHistory/excelHistory.do")
	public String excelHistory(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/excelHistory/excelHistory.nvf";
		
		return forwarPage;
	}
	
	/**
	 * 엑셀 출력 내역 리스트 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException 
	 */
	@RequestMapping(value="/itg/system/excelHistory/searchExcelHistoryList.do")
	public @ResponseBody ResultVO searchExcelHistoryList(@RequestBody ModelMap paramMap)throws NkiaException{
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			
			totalCount = excelHistoryService.searchExcelHistoryListCount(paramMap);
			List resultList = excelHistoryService.searchExcelHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
}
