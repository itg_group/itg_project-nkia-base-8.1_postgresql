/*
 * @(#)LoginManagerDAO.java              2018. 03. 29.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginManager.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("loginManagerDAO")
public class LoginManagerDAO extends EgovComAbstractDAO{
	
	public List searchLoginManagerList(HashMap paramMap) throws NkiaException {
		return list("LoginManagerDAO.searchLoginManagerList", paramMap);
	}	
	
	public int searchLoginManagerListCount(HashMap paramMap)throws NkiaException {
		return (Integer)selectByPk("LoginManagerDAO.searchLoginManagerListCount", paramMap);
	}
	
	public void updateLoginManager(HashMap paramMap) {
		update("LoginManagerDAO.updateLoginManager",paramMap);
	}

	public void insertLoginManager(HashMap assetMap) {
		insert("LoginManagerDAO.insertLoginManager", assetMap);
	}
	
	public List searchUserPwHistory(HashMap paramMap) throws NkiaException {
		return list("LoginManagerDAO.searchUserPwHistory", paramMap);
	}
	
	public List searchUserPwAndSalt(HashMap paramMap) throws NkiaException {
		return list("LoginManagerDAO.searchUserPwAndSalt", paramMap);
	}
	
	public List searchLoginSessionList(HashMap paramMap) throws NkiaException {
		return list("LoginManagerDAO.searchLoginSessionList", paramMap);
	}	
	
	public int searchLoginSessionListCount(HashMap paramMap)throws NkiaException {
		return (Integer)selectByPk("LoginManagerDAO.searchLoginSessionListCount", paramMap);
	}
	
	public void updateStatusLoginSession(HashMap paramMap) {
		update("LoginManagerDAO.updateStatusLoginSession",paramMap);
	}

	public void insertLoginSession(HashMap assetMap) {
		insert("LoginManagerDAO.insertLoginSession", assetMap);
	}
	
	public void deleteLoginSession(HashMap assetMap) {
		delete("LoginManagerDAO.deleteLoginSession", assetMap);
	}
	
}
