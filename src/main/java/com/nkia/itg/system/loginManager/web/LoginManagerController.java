/*
 * @(#)LoginManagerController.java              2018. 03. 29.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginManager.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.loginManager.service.LoginManagerService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class LoginManagerController {

	private static final Logger logger = LoggerFactory.getLogger(LoginManagerController.class);
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name="loginManagerService")
	private LoginManagerService loginManagerService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 로그인 관리 화면
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/system/loginManager/goLoginManager.do")
	public String classTypeLifeCycle(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/loginManager/loginManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 로그인 관리 입력된 정보 불러오기
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/loginManager/searchLoginManagerList.do")
	public @ResponseBody ResultVO searchLoginManagerList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = false;
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
	    	}
			GridVO gridVO = new GridVO();
			List resultList = loginManagerService.searchLoginManagerList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 로그인 관리 데이터 입력(수정)
	 * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/system/loginManager/registLoginManager.do")
	public @ResponseBody ResultVO registLoginManager(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			Boolean isAuthenticated = false;
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("UPD_USER_ID", userVO.getUser_id());
	    	}
			loginManagerService.registLoginManager(paramMap);
			resultVO.setResultMsg("SUCCESS");
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 현재 사용자의 최종 비밀번호 변경일 정보 선택
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/loginManager/searchUserPwHistory.do")
	public @ResponseBody ResultVO searchUserPwHistory(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = false;
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    	}
			GridVO gridVO = new GridVO();
			List resultList = loginManagerService.searchUserPwHistory(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 현재 사용자의 최종 비밀번호 변경일 정보 및 아이디와 비밀번호가 같은지 여부 체크
	 * @param paramMap
	 * @return
	 * @throws NkiaException 
	 * @throws NoSuchAlgorithmException 
	 * @throws IOException 
	 * @
	 */		
	@RequestMapping(value="/itg/system/loginManager/searchUserPwChangePopupYnInfo.do")
	public @ResponseBody ResultVO searchUserPwChangePopupYnInfo(@RequestBody ModelMap paramMap) throws NkiaException, NoSuchAlgorithmException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = false;
			isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    	}
			GridVO gridVO = new GridVO();
			List resultList = loginManagerService.searchUserPwChangePopupYnInfo(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
