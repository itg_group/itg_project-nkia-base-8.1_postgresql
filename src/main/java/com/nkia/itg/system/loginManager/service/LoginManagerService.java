/*
 * @(#)LoginManagerService.java              2018. 03. 29.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginManager.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface LoginManagerService {
	
	public List searchLoginManagerList(HashMap paramMap) throws NkiaException;

	public int searchLoginManagerListCount(HashMap paramMap) throws NkiaException;
	
	public void registLoginManager(HashMap paramMap) throws NkiaException;
	
	public void updateLoginManager(HashMap paramMap) throws NkiaException;

	public void insertLoginManager(HashMap paramMap) throws NkiaException;
	
	public List searchUserPwHistory(HashMap paramMap) throws NkiaException;
	
	public List searchLoginSessionList(HashMap paramMap) throws NkiaException;
	
	public int searchLoginSessionListCount(HashMap paramMap) throws NkiaException;

	public void updateStatusLoginSession(HashMap paramMap) throws NkiaException;

	public void insertLoginSession(HashMap paramMap) throws NkiaException;
	
	public void deleteLoginSession(HashMap paramMap) throws NkiaException;
	
	public List searchUserPwChangePopupYnInfo(HashMap paramMap) throws NkiaException, NoSuchAlgorithmException, UnsupportedEncodingException, IOException;
	
}
