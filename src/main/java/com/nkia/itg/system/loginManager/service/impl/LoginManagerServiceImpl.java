/*
 * @(#)LoginManagerServiceImpl.java              2018. 03. 29.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginManager.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.security.CipherSHA;
import com.nkia.itg.system.loginManager.dao.LoginManagerDAO;
import com.nkia.itg.system.loginManager.service.LoginManagerService;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;

@Service("loginManagerService")
public class LoginManagerServiceImpl implements LoginManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(LoginManagerServiceImpl.class);
	
	@Resource(name = "loginManagerDAO")
	public LoginManagerDAO loginManagerDAO;
	
	@Override
	public List searchLoginManagerList(HashMap paramMap) throws NkiaException {
		return loginManagerDAO.searchLoginManagerList(paramMap);
	}
	
	@Override
	public void registLoginManager(HashMap paramMap) throws NkiaException{
		int cnt = searchLoginManagerListCount(paramMap);
		if(cnt < 1) {
			insertLoginManager(paramMap);
		} else {
			updateLoginManager(paramMap);
		}
	}
	
	@Override
	public int searchLoginManagerListCount(HashMap paramMap) throws NkiaException {
		return loginManagerDAO.searchLoginManagerListCount(paramMap);
	}

	@Override
	public void updateLoginManager(HashMap paramMap) throws NkiaException{
		loginManagerDAO.updateLoginManager(paramMap);
	}

	@Override
	public void insertLoginManager(HashMap paramMap) throws NkiaException{
		loginManagerDAO.insertLoginManager(paramMap);
	}
	
	@Override
	public List searchUserPwHistory(HashMap paramMap) throws NkiaException {
		return loginManagerDAO.searchUserPwHistory(paramMap);
	}
	
	@Override
	public List searchLoginSessionList(HashMap paramMap) throws NkiaException {
		return loginManagerDAO.searchLoginSessionList(paramMap);
	}
	
	@Override
	public int searchLoginSessionListCount(HashMap paramMap) throws NkiaException {
		return loginManagerDAO.searchLoginSessionListCount(paramMap);
	}

	@Override
	public void updateStatusLoginSession(HashMap paramMap) throws NkiaException{
		loginManagerDAO.updateStatusLoginSession(paramMap);
	}

	@Override
	public void insertLoginSession(HashMap paramMap) throws NkiaException{
		loginManagerDAO.insertLoginSession(paramMap);
	}
	
	@Override
	public void deleteLoginSession(HashMap paramMap) throws NkiaException{
		loginManagerDAO.deleteLoginSession(paramMap);
	}
	
	@Override
	public List searchUserPwChangePopupYnInfo(HashMap paramMap) throws NoSuchAlgorithmException, NkiaException, IOException {
		
		String userId = StringUtil.parseString(paramMap.get("user_id"));
		List resultList = new ArrayList();
		
		List histList = loginManagerDAO.searchUserPwHistory(paramMap);
		List userPwInfo = loginManagerDAO.searchUserPwAndSalt(paramMap);
		
		HashMap dataMap = new HashMap();
		
		if(histList == null || histList.size() < 1) {
			
			if(userPwInfo != null && userPwInfo.size() > 0) {
				
				HashMap userPwInfoData = new HashMap();
				userPwInfoData = (HashMap)userPwInfo.get(0);
				
				String pw = StringUtil.parseString(userPwInfoData.get("PW"));
				String salt = StringUtil.parseString(userPwInfoData.get("SCRTY_SALT"));
				String basePw = NkiaApplicationPropertiesMap.getProperty("Globals.Security.BasicPassword");
				String encryptBasePw = CipherSHA.getEncrypt(basePw, StringUtil.base64ToByte(salt));
				String encryptUserId = CipherSHA.getEncrypt(userId, StringUtil.base64ToByte(salt));
				
				if("".equals(pw)) {
					// PW가 없는 사용자는 변경 팝업 띄우지 않음 (변경 할 수가 없음)
					dataMap.put("IS_PASS_USER", "Y");
				} else if(pw.equals(encryptUserId)) {
					// ID와 PW가 같을 경우 변경 팝업 띄움
					dataMap.put("IS_SAME_ID_PW", "Y");
				} else if(pw.equals(encryptBasePw)) {
					// ID와 프로퍼티기본패스워드가같을경우
					dataMap.put("IS_SAME_ID_PW", "Y");
				}else {
					dataMap.put("IS_SAME_ID_PW", "N");
				}
			} else {
				dataMap.put("IS_SAME_ID_PW", "N");
			}
			
		} else {
			
			dataMap = (HashMap)histList.get(0);
			
			if(userPwInfo != null && userPwInfo.size() > 0) {
				
				HashMap userPwInfoData = new HashMap();
				userPwInfoData = (HashMap)userPwInfo.get(0);
				
				String pw = StringUtil.parseString(userPwInfoData.get("PW"));
				String salt = StringUtil.parseString(userPwInfoData.get("SCRTY_SALT"));
				String isCipUser = StringUtil.parseString(userPwInfoData.get("IS_CIP_USER"));
				String basePw = NkiaApplicationPropertiesMap.getProperty("Globals.Security.BasicPassword");
				String encryptBasePw = CipherSHA.getEncrypt(basePw, StringUtil.base64ToByte(salt));
				String encryptUserId = CipherSHA.getEncrypt(userId, StringUtil.base64ToByte(salt));
				
				if("N".equals(isCipUser)) {
					// 공사 담당자는 변경 팝업 띄우지 않음
					dataMap.put("IS_PASS_USER", "Y");
				} else {
					if("".equals(pw)) {
						//  PW가 없는 사용자는 변경 팝업 띄우지 않음 (변경 할 수가 없음)
						dataMap.put("IS_PASS_USER", "Y");
					} else if(pw.equals(encryptUserId)) {
						// ID와 PW가 같을 경우 변경 팝업 띄움
						dataMap.put("IS_SAME_ID_PW", "Y");
					}else if(pw.equals(encryptBasePw)) {
						// ID와 프로퍼티기본패스워드가같을경우
						dataMap.put("IS_SAME_ID_PW", "Y");
					} else {
						dataMap.put("IS_SAME_ID_PW", "N");
					}
				}
			} else {
				dataMap.put("IS_SAME_ID_PW", "N");
			}
		}
		
		resultList.add(dataMap);
		
		return resultList;
	}
}
