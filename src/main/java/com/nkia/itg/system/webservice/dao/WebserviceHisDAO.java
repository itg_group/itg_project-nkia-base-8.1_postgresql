package com.nkia.itg.system.webservice.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("webserviceHisDAO")
public class WebserviceHisDAO extends EgovComAbstractDAO {

	/**
	 * 웹서비스 이력 조회
	 * @param paramMap
	 * @return
	 */
	public List selectWebserviceHisList(ModelMap paramMap) {
		return list("WebserviceHisDAO.selectWebserviceHisList", paramMap);
	}

	public int selectWebserviceHisListCount(ModelMap paramMap) {
		return (Integer) selectByPk("WebserviceHisDAO.selectWebserviceHisListCount", paramMap);
	}

	public List selectWebserviceHisList(Map param) {
		return list("WebserviceHisDAO.selectWebserviceHisList", param);
	}

}
