package com.nkia.itg.system.webservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface WebserviceService {
	/**
	 * 웹서비스 목록 개수
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	int searchWsListCount(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	List searchWsList(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스의 Parameter 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	List searchWsParamList(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 상세 정보 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	Map selectWsInfo(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 정보 등록
	 * @param paramMap
	 * @throws NkiaException
	 */
	void insertWsInfo(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 정보 수정
	 * @param paramMap
	 * @throws NkiaException
	 */
	void updateWsInfo(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 정보 삭제
	 * @param paramMap
	 * @throws NkiaException
	 */
	void deleteWsInfo(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 결과 등록
	 * @param paramMap
	 * @throws NkiaException
	 */
	void insertWsResult(HashMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 결과 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	List searchWsResultList(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 결과 목록 개수
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	int searchWsResultCount(ModelMap paramMap) throws NkiaException;

	/**
	 * 웹서비스 결과 상세 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	Map selectWsResultInfo(ModelMap paramMap) throws NkiaException;

}
