package com.nkia.itg.system.webservice.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.webservice.dao.WebserviceDAO;
import com.nkia.itg.system.webservice.service.WebserviceService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("webserviceService")
public class WebserviceServiceImpl implements WebserviceService{

	@Resource(name="webserviceDAO")
	private WebserviceDAO webserviceDAO;
	
	/**
	 * 웹서비스 목록 개수
	 */
	@Override
	public int searchWsListCount(ModelMap paramMap) throws NkiaException {
		return webserviceDAO.searchWsListCount(paramMap);
	}

	/**
	 * 웹서비스 목록 조회
	 */
	@Override
	public List searchWsList(ModelMap paramMap) throws NkiaException {
		return webserviceDAO.searchWsList(paramMap);
	}

	/**
	 * 웹서비스의 Parameter 목록 조회
	 */
	@Override
	public List searchWsParamList(ModelMap paramMap) throws NkiaException {
		return webserviceDAO.searchWsParamList(paramMap);
	}

	/**
	 * 웹서비스 상세 정보 조회
	 */
	@Override
	public Map selectWsInfo(ModelMap paramMap) throws NkiaException {
		return webserviceDAO.selectWsInfo(paramMap);
	}

	/**
	 * 웹서비스 정보 등록
	 */
	@Override
	public void insertWsInfo(ModelMap paramMap) throws NkiaException {
		//웹서비스 정보 등록
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
		String webserviceId = webserviceDAO.selectWsId();
		paramMap.put("webserviceId", webserviceId);
		webserviceDAO.insertWsInfo(paramMap);
	}
	
	/**
	 * 웹서비스 정보 수정
	 */
	@Override
	public void updateWsInfo(ModelMap paramMap) throws NkiaException {
		//웹서비스 정보 수정
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	webserviceDAO.updateWsInfo(paramMap);
    	
	}

	/**
	 * 웹서비스 정보 삭제
	 */
	@Override
	public void deleteWsInfo(ModelMap paramMap) throws NkiaException {
		webserviceDAO.deleteWsInfo(paramMap);	
	}

	/**
	 * 웹서비스 결과 등록
	 */
	@Override
	public void insertWsResult(HashMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("reg_user_id", userVO.getUser_id());
    	}
    	String resultId = webserviceDAO.selectResultId();
    	paramMap.put("resultId", resultId);
		webserviceDAO.insertWsResult(paramMap);
	}
	
	/**
	 * 웹서비스 결과 목록 조회
	 */
	@Override
	public List searchWsResultList(ModelMap paramMap) throws NkiaException {
		return webserviceDAO.searchWsResultList(paramMap);
	}

	/**
	 * 웹서비스 결과 목록 개수
	 */
	@Override
	public int searchWsResultCount(ModelMap paramMap) throws NkiaException {
		return webserviceDAO.searchWsResultCount(paramMap);
	}

	/**
	 * 웹서비스 결과 정보 조회
	 */
	@Override
	public Map selectWsResultInfo(ModelMap paramMap) throws NkiaException {
		return webserviceDAO.selectWsResultInfo(paramMap);
	}
	
}
