package com.nkia.itg.system.webservice.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

public interface WebserviceHisService {

	// 웹서비스 이력 조회
	List selectWebserviceHisList(ModelMap paramMap);
	
	int selectWebserviceHisListCount(ModelMap paramMap);

	List searchWebserviceHisExcelDown(Map param);

}
