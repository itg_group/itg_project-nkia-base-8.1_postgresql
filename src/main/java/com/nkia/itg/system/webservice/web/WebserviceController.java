package com.nkia.itg.system.webservice.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.webservice.service.WebserviceService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class WebserviceController {
	
	@Resource(name="webserviceService")
	private WebserviceService webserviceService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 웹서비스 관리 메인화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/webserviceMain.do")
	public String webserviceMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/webservice/webserviceMain.nvf";
		return forwarPage;
	}
	/*
	*//**
	 * 웹서비스 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/searchWsList.do")
	public @ResponseBody ResultVO searchWsList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = null;
			GridVO gridVO = new GridVO();
			
			PagingUtil.getFristEndNum(paramMap);
	
			int totalCount = 0;
			totalCount = webserviceService.searchWsListCount(paramMap);
			resultList = webserviceService.searchWsList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 웹서비스 정보 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/selectWsInfo.do")
	public @ResponseBody ResultVO selectWsInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Map resultMap = new HashMap();
			
			resultMap = webserviceService.selectWsInfo(paramMap);
			
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 웹서비스 정보 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/insertWsInfo.do")
	public @ResponseBody ResultVO insertWsInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			webserviceService.insertWsInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 웹서비스 정보 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/updateWsInfo.do")
	public @ResponseBody ResultVO updateWsInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			webserviceService.updateWsInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 웹서비스 정보 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/deleteWsInfo.do")
	public @ResponseBody ResultVO deleteWsInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			webserviceService.deleteWsInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 웹서비스 결과 이력 저장
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/insertWsResult.do")
	public @ResponseBody ResultVO insertWsResult(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			webserviceService.insertWsResult(paramMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 웹서비스 결과 이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/searchWsResultList.do")
	public @ResponseBody ResultVO searchWsResultList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try {
			List resultList = null;
			
			PagingUtil.getFristEndNum(paramMap);
			
			int totalCount = 0;
			totalCount = webserviceService.searchWsResultCount(paramMap);
			resultList = webserviceService.searchWsResultList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 웹서비스 실행 결과 상세
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/selectWsResultInfo.do")
	public @ResponseBody ResultVO selectWsResultInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Map resultMap = new HashMap();
			
			resultMap = webserviceService.selectWsResultInfo(paramMap);
			
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
