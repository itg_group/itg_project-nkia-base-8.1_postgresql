package com.nkia.itg.system.webservice.web;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.webservice.service.WebserviceHisService;

@Controller
public class WebserviceHisController {

	@Resource(name="webserviceHisService")
	private WebserviceHisService webserviceHisService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 웹서비스 이력 조회 메인화면
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/webserviceHisMain.do")
	public String webserviceMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/webservice/webserviceHisMain.nvf";
		return forwarPage;
	}
	
	/**
	 * 웹서비스 이력 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/webservice/selectWebserviceHisList.do")
	public @ResponseBody ResultVO selectWebserviceHisList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		List resultList = null;
		
		PagingUtil.getFristEndNum(paramMap);
		
		int totalCount = 0;
		totalCount = webserviceHisService.selectWebserviceHisListCount(paramMap);
		resultList = webserviceHisService.selectWebserviceHisList(paramMap);
		
		gridVO.setTotalCount(totalCount);
		gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
		gridVO.setRows(resultList);
		
		resultVO.setGridVO(gridVO);
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/webservice/searchWebserviceHisExcelDown.do")
	public @ResponseBody ResultVO searchWebserviceHisExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = webserviceHisService.searchWebserviceHisExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
