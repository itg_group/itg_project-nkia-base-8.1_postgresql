package com.nkia.itg.system.webservice.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("webserviceDAO")
public class WebserviceDAO  extends EgovComAbstractDAO{

	/**
	 * 웹서비스 목록 개수
	 * @param paramMap
	 * @return
	 */
	public int searchWsListCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("WebserviceDAO.searchWsListCount", paramMap);
	}

	/**
	 * 웹서비스 목록
	 * @param paramMap
	 * @return
	 */
	public List searchWsList(ModelMap paramMap) throws NkiaException {
		return list("WebserviceDAO.searchWsList", paramMap);
	}

	/**
	 * 웹서비스의 파라메터 목록
	 * @param paramMap
	 * @return
	 */
	public List searchWsParamList(ModelMap paramMap) throws NkiaException {
		return list("WebserviceDAO.searchWsParamList", paramMap);
	}

	/**
	 * 웹서비스의 정보 조회
	 * @param webservice_id
	 * @return
	 */
	public Map selectWsInfo(ModelMap paramMap) throws NkiaException {
		return (Map) selectByPk("WebserviceDAO.selectWsInfo", paramMap);
	}

	/**
	 * 웹서비스의 정보 등록
	 * @param paramMap
	 */
	public void insertWsInfo(ModelMap paramMap) throws NkiaException {
		insert("WebserviceDAO.insertWsInfo", paramMap);
	}

	/**
	 * 신규 웹서비스 아이디 조회
	 * @return
	 * @throws NkiaException
	 */
	public String selectWsId() throws NkiaException {
		return (String) selectByPk("WebserviceDAO.selectWsId", "");
	}

	/**
	 * 웹서비스 정보 수정
	 * @param paramMap
	 */
	public void updateWsInfo(ModelMap paramMap) throws NkiaException {
		update("WebserviceDAO.updateWsInfo", paramMap);
	}

	/**
	 * 웹서비스 정보 삭제
	 * @param webservice_id
	 */
	public void deleteWsInfo(ModelMap paramMap) throws NkiaException {
		delete("WebserviceDAO.deleteWsInfo", paramMap);
	}

	/**
	 * 웹서비스 결과 이력 저장
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertWsResult(HashMap paramMap) throws NkiaException{
		insert("WebserviceDAO.insertWsResult", paramMap);
		
	}

	/**
	 * 웹서비스 결과 아이디 조회
	 */
	public String selectResultId() {
		return (String) selectByPk("WebserviceDAO.selectResultId", "");
	}

	/**
	 * 웹서비스 결과 목록 조회
	 * @param paramMap
	 * @return
	 */
	public List searchWsResultList(ModelMap paramMap) {
		return list("WebserviceDAO.searchWsResultList", paramMap);
	}

	/**
	 * 웹서비스 결과 목록 개수
	 * @param paramMap
	 * @return
	 */
	public int searchWsResultCount(ModelMap paramMap) {
		return (Integer) selectByPk("WebserviceDAO.searchWsResultCount", paramMap);
	}

	/**
	 * 웹서비스 결과 상세 조회
	 * @param result_id
	 * @return
	 */
	public Map selectWsResultInfo(ModelMap paramMap) {
		return (Map) selectByPk("WebserviceDAO.selectWsResultInfo", paramMap);
	}
}
