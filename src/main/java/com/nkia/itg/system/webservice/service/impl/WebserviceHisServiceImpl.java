package com.nkia.itg.system.webservice.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.system.webservice.dao.WebserviceHisDAO;
import com.nkia.itg.system.webservice.service.WebserviceHisService;

@Service("webserviceHisService")
public class WebserviceHisServiceImpl implements WebserviceHisService{

	@Resource(name="webserviceHisDAO")
	private WebserviceHisDAO webserviceHisDAO;

	/**
	 * 웹서비스 이력 조회
	 */
	@Override
	public List selectWebserviceHisList(ModelMap paramMap) {
		return webserviceHisDAO.selectWebserviceHisList(paramMap);
	}

	/**
	 * 웹서비스 이력 목록 개수
	 */
	@Override
	public int selectWebserviceHisListCount(ModelMap paramMap) {
		return webserviceHisDAO.selectWebserviceHisListCount(paramMap);
	}

	@Override
	public List searchWebserviceHisExcelDown(Map param) {
		return webserviceHisDAO.selectWebserviceHisList(param);
	}

}
