package com.nkia.itg.system.webservice.client;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.json.JsonMapper;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.interfaces.beans.WsProcessBean;
import com.nkia.itg.system.webservice.service.WebserviceService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;

@Controller
public class WebserviceClient {
	
	@Resource(name="webserviceService")
	private WebserviceService webserviceService;
	
	public enum TRNSMIS_TYPE {
		POST, GET;
	}
	
	public enum PARAMETER_TYPE {
		JSON, XML, MULTIPART;
	}
	
	/**
	 * 웹서비스 호출하는 공통 client
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/itg/system/webservice/callWebserviceClient.do")
	public @ResponseBody ResultVO callWebserviceClient(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		
		try { 
			//클라이언트 구성 및 생성
			ClientConfig config = new DefaultClientConfig();
			Client client = Client.create(config);
			
			// url 세팅
			String serverIp = NkiaApplicationPropertiesMap.getProperty("App.Server.Ip");
			String serverPort = NkiaApplicationPropertiesMap.getProperty("App.Server.Port");
			String webserviceUrl = (String) paramMap.get("url");
			String callWebserviceUrl = "http://" + serverIp + ":" + serverPort + webserviceUrl;
			
			String resultMsg = null;
			int responseStatus = 0;
			String parameter = (String) paramMap.get("parameter");
			String trnsmisType = (String) paramMap.get("trnsmis_type");
			String parameterType = (String) paramMap.get("parameter_type");
			
			// 전송방식에 따른 provider 호출
			if(TRNSMIS_TYPE.GET.toString().equals(trnsmisType)){
				Map paramData = JsonMapper.fromJson(parameter, Map.class);
				
				Iterator iter_key = paramData.keySet().iterator();
				StringBuilder paramurl = new StringBuilder();
				paramurl.append("?");
				while(iter_key.hasNext()){
					String key = (String) iter_key.next();
					paramurl.append(key);
					paramurl.append("=");
					paramurl.append(paramData.get(key));
					paramurl.append("&");
				}
				
				WebResource webResource = client.resource(callWebserviceUrl + paramurl);
				ClientResponse response = webResource.type(MediaType.TEXT_PLAIN).get(ClientResponse.class);
				resultMsg = response.getEntity(String.class);
				responseStatus = response.getStatus();

			}else if(TRNSMIS_TYPE.POST.toString().equals(trnsmisType)){
				WebResource webResource = client.resource(callWebserviceUrl);
				
				ClientResponse response = null;
				
				// provider 의 데이터형에 따른 분기
				if(PARAMETER_TYPE.JSON.toString().equals(parameterType)){
					response = webResource.type(MediaType.TEXT_PLAIN).post(ClientResponse.class, parameter);
				
				}else if(PARAMETER_TYPE.XML.toString().equals(parameterType)){
					response = webResource.type(MediaType.APPLICATION_XML).post(ClientResponse.class, parameter);
				
				}else if(PARAMETER_TYPE.MULTIPART.toString().equals(parameterType)){
					File file = new File("D:\\20160810_.txt");
					MultiPart multiPart = new MultiPart();
					WsProcessBean wsProcessBean = new WsProcessBean();
					wsProcessBean.setProject_id("deploy");
					wsProcessBean.setPackage_id("test");
					wsProcessBean.setArchive_file_name("test");
					multiPart.bodyPart(new BodyPart(wsProcessBean, MediaType.APPLICATION_XML_TYPE));
					multiPart.bodyPart(new BodyPart(file, MediaType.MULTIPART_FORM_DATA_TYPE));
					response = webResource.type("multipart/mixed").post(ClientResponse.class, multiPart);
				}
		
				resultMsg = response.getEntity(String.class);
				responseStatus = response.getStatus();
			}

			String callStatus = null;
			// 실패시 jsonObject에 FAIL 을 넣어주는 방법.
			/*if(resultMsg.indexOf("FAIL") != -1) {
				callStatus = "FAIL";
			}else{
				callStatus = "SUCCESS";
			}*/
			
			// 실패시 return 되는 값에 Response.Status.BAD_REQUEST 넣어주는 방법
			if(responseStatus == 400){
				callStatus = "FAIL";
			}else{
				callStatus = "SUCCESS";
			}
			
			HashMap resultMap = new HashMap(); 
			resultMap.put("result", resultMsg);
			resultMap.put("callStatus", callStatus);
			resultMap.put("webserviceId", paramMap.get("webservice_id"));
			
			// 웹서비스 호출 이력 저장
			webserviceService.insertWsResult(resultMap);
			
			resultVO.setResultMap(resultMap);
			resultVO.setSuccess(true);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
