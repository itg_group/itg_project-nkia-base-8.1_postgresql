/*
 * @(#)manualBoardServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.manualboard.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.system.manualboard.dao.ManualBoardDAO;
import com.nkia.itg.system.manualboard.service.ManualBoardService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("manualBoardService")
public class ManualBoardServiceImpl implements ManualBoardService{
	
	private static final Logger logger = LoggerFactory.getLogger(ManualBoardServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "manualBoardDAO")
	public ManualBoardDAO manualBoardDAO;
	
	public List searchManualBoard(ModelMap paramMap) throws NkiaException {
		return manualBoardDAO.searchManualBoard(paramMap);
	}
	
	public int searchManualBoardCount(ModelMap paramMap) throws NkiaException {
		return manualBoardDAO.searchManualBoardCount(paramMap);
	}
	
	public List searchManualBoardExcelDown(Map paramMap) throws NkiaException {
		return manualBoardDAO.searchManualBoardExcelDown(paramMap);
	}
	
	public void insertManualBoard(ModelMap paramMap) throws NkiaException {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 수동지표 계약등록
		manualBoardDAO.insertManualBoard(paramMap);
	}

	public void updateManualBoard(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		
		// 수동지표 계약수정
		manualBoardDAO.updateManualBoard(paramMap);
	}
	
	public void deleteManualBoard(ModelMap paramMap) throws NkiaException, IOException {
		manualBoardDAO.deleteManualBoard(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	
}