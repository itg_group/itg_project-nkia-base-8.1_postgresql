/*
 * @(#)ManualBoardController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.manualboard.web;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.system.manualboard.service.ManualBoardService;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class ManualBoardController {
	
	@Resource(name = "manualBoardService")
	private ManualBoardService manualBoardService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	// 수동지표계약관리 페이지 로드
	@RequestMapping(value="/itg/slms/manualboard/manualBoard.do")
	public String manualBoard(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/slms/manualboard/manualBoard.nvf";
		paramMap.put("ui_type", StringUtil.replaceNull(request.getParameter("ui_type"), ""));
		return forwarPage;
	}
	
	/**
	 * 수동지표계약관리 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/slms/manualboard/searchManualBoard.do")
	public @ResponseBody ResultVO searchManualBoard(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = manualBoardService.searchManualBoard(paramMap);		
			totalCount = manualBoardService.searchManualBoardCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 수동지표계약관리 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/slms/manualboard/searchManualBoardExcelDown.do")
	public @ResponseBody ResultVO searchManualBoardExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = manualBoardService.searchManualBoardExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 수동지표계약관리 - 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/slms/manualboard/insertManualBoard.do")
	public @ResponseBody ResultVO insertManualBoard(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			manualBoardService.insertManualBoard(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 수동지표계약관리 - 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/slms/manualboard/updateManualBoard.do")
	public @ResponseBody ResultVO updateManualBoard(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			manualBoardService.updateManualBoard(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 수동지표계약관리 -  삭제
     * @param paramMap
	 * @return
	 * @throws IOException 
	 * @
	 */	
	@RequestMapping(value="/itg/slms/manualboard/deleteManualBoard.do")
	public @ResponseBody ResultVO deleteManualBoard(@RequestBody ModelMap paramMap)throws NkiaException, IOException  {
		ResultVO resultVO = new ResultVO();
		try{
			manualBoardService.deleteManualBoard(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
