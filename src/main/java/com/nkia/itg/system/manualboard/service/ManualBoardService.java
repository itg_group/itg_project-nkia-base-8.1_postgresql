/*
 * @(#)manualBoardService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.manualboard.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ManualBoardService {

	public List searchManualBoard(ModelMap paramMap) throws NkiaException;

	public int searchManualBoardCount(ModelMap paramMap) throws NkiaException;

	public List searchManualBoardExcelDown(Map paramMap) throws NkiaException;

	public void insertManualBoard(ModelMap paramMap) throws NkiaException;
	
	public void updateManualBoard(ModelMap paramMap) throws NkiaException;
	
	public void deleteManualBoard(ModelMap paramMap) throws NkiaException, IOException;
	
}
