/*
 * @(#)ManualBoardDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.manualboard.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("manualBoardDAO")
public class ManualBoardDAO extends EgovComAbstractDAO{
	
	public List searchManualBoard(ModelMap paramMap) throws NkiaException{
		return list("ManualBoardDAO.searchManualBoard", paramMap);
	}

	public int searchManualBoardCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("ManualBoardDAO.searchManualBoardCount", paramMap);
	}
	
	public List searchManualBoardExcelDown(ModelMap paramMap) throws NkiaException{
		return list("ManualBoardDAO.searchManualBoardExcelDown", paramMap);
	}
	
	public void insertManualBoard(HashMap manualBoardInfo) throws NkiaException {
		this.insert("ManualBoardDAO.insertManualBoard", manualBoardInfo);
	}
	
	public void updateManualBoard(HashMap manualBoardInfo) throws NkiaException {
		this.update("ManualBoardDAO.updateManualBoard", manualBoardInfo);
	}
	
	public void deleteManualBoard(ModelMap paramMap) throws NkiaException {
		this.delete("ManualBoardDAO.deleteManualBoard", paramMap);
	}
	
	public List searchManualBoardExcelDown(Map paramMap) throws NkiaException {
		return list("ManualBoardDAO.searchManualBoardExcelDown", paramMap);
	}
	
}

