/*
 * @(#)vendorService.java              2013. 3. .
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.vendor.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface VendorService {

	public List searchVendor(ModelMap paramMap) throws NkiaException;

	public int searchVendorCount(ModelMap paramMap) throws NkiaException;
	
	public void insertVendor(ModelMap paramMap) throws NkiaException;
		
	public void updateVendor(ModelMap paramMap) throws NkiaException;
	
	public void updateVendorType(ModelMap paramMap) throws NkiaException;
	
	public void deleteVendor(ModelMap paramMap) throws NkiaException;
	
	public void deleteVendorType(ModelMap paramMap) throws NkiaException;

	public int selectDeleteValidation(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectVendorEmail(ModelMap paramMap) throws NkiaException;
	
	public List searchExcelDown(Map paramMap) throws NkiaException;
}
