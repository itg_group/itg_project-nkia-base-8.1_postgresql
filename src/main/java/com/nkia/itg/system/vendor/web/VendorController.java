/*
 * @(#)VendorController.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.vendor.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.vendor.service.VendorService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class VendorController {
	
	@Resource(name = "vendorService")
	private VendorService vendorService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/system/vendor/vendorManager.do")
	public String vendorManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/vendor/vendorManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 업체관리 리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/system/vendor/searchVendor.do")
	public @ResponseBody ResultVO searchVendor(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = vendorService.searchVendor(paramMap);		
			totalCount = vendorService.searchVendorCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 업체관리 신규등록
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/system/vendor/insertVendor.do")
	public @ResponseBody ResultVO insertVendor(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
	    	
			vendorService.insertVendor(paramMap);
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 업체관리 수정
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/vendor/updateVendor.do")
	public @ResponseBody ResultVO updateVendor(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}			
			vendorService.updateVendor(paramMap);
			vendorService.updateVendorType(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 업체관리 삭제
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/vendor/deleteVendor.do")
	public @ResponseBody ResultVO deleteVendor(@RequestBody ModelMap paramMap) throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			vendorService.deleteVendorType(paramMap);
			vendorService.deleteVendor(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 업체관리 삭제 Validation
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/vendor/deleteValidation.do")
	public @ResponseBody ResultVO deleteValidation(@RequestBody ModelMap paramMap) throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			int count = 0;
			HashMap resultMap = paramMap;
			count = vendorService.selectDeleteValidation(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultInt(count);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 선택업체 조회
     * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/system/vendor/selectVendorEmail.do")
	public @ResponseBody ResultVO selectVendorEmail(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{     
			HashMap resultMap = vendorService.selectVendorEmail(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	
	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 * @
	 */			
	@RequestMapping(value="/itg/system/vendor/searchExcelDown.do")
    public @ResponseBody ResultVO searchExcelDown(@RequestBody ModelMap paramMap)throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = vendorService.searchExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
}
