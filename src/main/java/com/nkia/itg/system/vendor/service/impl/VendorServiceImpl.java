/*
 * @(#)vendorServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.vendor.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.system.service.impl.SystemServiceImpl;
import com.nkia.itg.system.vendor.dao.VendorDAO;
import com.nkia.itg.system.vendor.service.VendorService;

@Service("vendorService")
public class VendorServiceImpl implements VendorService{
	
	private static final Logger logger = LoggerFactory.getLogger(VendorServiceImpl.class);
	
	@Resource(name = "vendorDAO")
	public VendorDAO vendorDAO;
	
	public List searchVendor(ModelMap paramMap) throws NkiaException {
		
		return vendorDAO.searchVendor(paramMap);
	}

	public int searchVendorCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return vendorDAO.searchVendorCount(paramMap);
	}
	
	public void insertVendor(ModelMap paramMap) throws NkiaException {
		
		String vendorId = vendorDAO.selectVendorId(paramMap);
		paramMap.put("venderId", vendorId);
		vendorDAO.insertVendor(paramMap);
		vendorDAO.insertVendorType(paramMap);
	}
	
	public void insertVendorType(ModelMap paramMap) throws NkiaException {
		vendorDAO.insertVendorType(paramMap);
	}
	
	public void updateVendor(ModelMap paramMap) throws NkiaException {
		vendorDAO.updateVendor(paramMap);
	}
	
	public void updateVendorType(ModelMap paramMap) throws NkiaException {
		vendorDAO.updateVendorType(paramMap);
	}
	
	public void deleteVendor(ModelMap paramMap) throws NkiaException {
		vendorDAO.deleteVendor(paramMap);
	}
	
	public void deleteVendorType(ModelMap paramMap) throws NkiaException {
		vendorDAO.deleteVendorType(paramMap);
	}

	public int selectDeleteValidation(ModelMap paramMap) throws NkiaException {
		return vendorDAO.selectDeleteValidation(paramMap);
	}
	
	public HashMap selectVendorEmail(ModelMap paramMap) throws NkiaException {
		return vendorDAO.selectVendorEmail(paramMap);
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {				
		List resultList = null;
		resultList = vendorDAO.searchExcelDown(paramMap); 
		return resultList;
	}	
}
