/*
 * @(#)vendorDAO.java              2013. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.vendor.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("vendorDAO")
public class VendorDAO extends EgovComAbstractDAO{
	
	public List searchVendor(ModelMap paramMap) throws NkiaException{
		return list("VendorDAO.searchVendor", paramMap);
	}

	public int searchVendorCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("VendorDAO.searchVendorCount", paramMap);
	}
	
	public void insertVendor(ModelMap paramMap) throws NkiaException {
		this.insert("VendorDAO.insertVendor", paramMap);
	}	
	
	public void insertVendorType(ModelMap paramMap) throws NkiaException {
		this.insert("VendorDAO.insertVendorType", paramMap);
	}	
	
	public void updateVendor(ModelMap paramMap) throws NkiaException {
		this.update("VendorDAO.updateVendor", paramMap);
	}	
	
	public void updateVendorType(ModelMap paramMap) throws NkiaException {
		this.update("VendorDAO.updateVendorType", paramMap);
	}	
	
	public void deleteVendor(ModelMap paramMap) throws NkiaException {
		this.delete("VendorDAO.deleteVendor", paramMap);
	}	
	
	public void deleteVendorType(ModelMap paramMap) throws NkiaException {
		this.delete("VendorDAO.deleteVendorType", paramMap);
	}	
	
	public int selectDeleteValidation(ModelMap paramMap) throws NkiaException{
		return (Integer)selectByPk("VendorDAO.selectDeleteValidation", paramMap);
	}
	
	public HashMap selectVendorEmail(ModelMap paramMap) throws NkiaException{
		return (HashMap)selectByPk("VendorDAO.selectVendorEmail", paramMap);
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("VendorDAO.searchVendor", paramMap);
		return resultList;
	}	
	
	public String selectVendorId(ModelMap paramMap) throws NkiaException{
		return (String)selectByPk("VendorDAO.selectVendorId", paramMap);
	}
}
