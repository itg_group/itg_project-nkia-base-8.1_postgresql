/*
 * @(#)LoginHistoryService.java              2018. 06. 15.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginHistory.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface LoginHistoryService {
	
	public List searchLoginHistoryList(HashMap paramMap) throws NkiaException;

	public int searchLoginHistoryListCount(HashMap paramMap) throws NkiaException;
	
}
