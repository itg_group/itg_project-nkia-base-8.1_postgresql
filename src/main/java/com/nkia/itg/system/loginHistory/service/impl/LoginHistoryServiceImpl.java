/*
 * @(#)LoginHistoryServiceImpl.java              2018. 06. 15.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginHistory.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.security.CipherSHA;
import com.nkia.itg.system.loginHistory.dao.LoginHistoryDAO;
import com.nkia.itg.system.loginHistory.service.LoginHistoryService;

@Service("loginHistoryService")
public class LoginHistoryServiceImpl implements LoginHistoryService{
	
	private static final Logger logger = LoggerFactory.getLogger(LoginHistoryServiceImpl.class);
	
	@Resource(name = "loginHistoryDAO")
	public LoginHistoryDAO loginHistoryDAO;
	
	@Override
	public List searchLoginHistoryList(HashMap paramMap) throws NkiaException {
		return loginHistoryDAO.searchLoginHistoryList(paramMap);
	}
	
	@Override
	public int searchLoginHistoryListCount(HashMap paramMap) throws NkiaException {
		return loginHistoryDAO.searchLoginHistoryListCount(paramMap);
	}

}
