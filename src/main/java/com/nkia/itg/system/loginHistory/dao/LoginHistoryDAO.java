/*
 * @(#)LoginHistoryDAO.java              2018. 06. 15.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginHistory.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("loginHistoryDAO")
public class LoginHistoryDAO extends EgovComAbstractDAO{
	
	public List searchLoginHistoryList(HashMap paramMap) throws NkiaException {
		return list("LoginHistoryDAO.searchLoginHistoryList", paramMap);
	}	
	
	public int searchLoginHistoryListCount(HashMap paramMap)throws NkiaException {
		return (Integer)selectByPk("LoginHistoryDAO.searchLoginHistoryListCount", paramMap);
	}
	
}
