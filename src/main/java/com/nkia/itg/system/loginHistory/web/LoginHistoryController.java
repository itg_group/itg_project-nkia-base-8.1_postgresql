/*
 * @(#)LoginHistoryController.java              2018. 06. 15.
 * jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.loginHistory.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.loginHistory.service.LoginHistoryService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class LoginHistoryController {

	private static final Logger logger = LoggerFactory.getLogger(LoginHistoryController.class);
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name="loginHistoryService")
	private LoginHistoryService loginHistoryService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	
	/**
	 * 로그인 이력조회 화면
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */			
	@RequestMapping(value="/itg/system/loginHistory/goLoginHistory.do")
	public String goLoginHistory(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/loginHistory/loginHistory.nvf";
		return forwarPage;
	}
	
	/**
	 * 로그인 이력 목록
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/loginHistory/searchLoginHistoryList.do")
	public @ResponseBody ResultVO searchLoginHistoryList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = loginHistoryService.searchLoginHistoryListCount(paramMap);
			resultList = loginHistoryService.searchLoginHistoryList(paramMap);		
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 로그인 이력 목록 (페이징 없음)
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/loginHistory/searchLoginHistoryListNoPaging.do")
	public @ResponseBody ResultVO searchLoginHistoryListNoPaging(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			resultList = loginHistoryService.searchLoginHistoryList(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 로그인 이력 목록 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 * @
	 */		
	@RequestMapping(value="/itg/system/loginHistory/excelDownLoginHistoryList.do")
	public @ResponseBody ResultVO excelDownLoginHistoryList(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try {
			HashMap excelMap = new HashMap();
			HashMap excelParam = (HashMap) paramMap.get("excelParam");
			HashMap param = (HashMap) paramMap.get("param");
			List result = loginHistoryService.searchLoginHistoryList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, result);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
