/*
 * @(#)NamedParamTestController.java              2018. 1. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.datamonitor.web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.NamedParamService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.datamonitor.service.DataMonitorService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * 
 * @version 1.0 2018. 1. 8.
 * @author <a href="mailto:sisong@nkia.co.kr"> SungIl Song
 * @since JDK 1.7
 * <pre>
 * 	데이타모니터 Controller
 * </pre>
 *
 */
@Controller
public class DataMonitorController {
	
	private static final Logger logger = LoggerFactory.getLogger(DataMonitorController.class);
	
	@Resource(name = "dataMonitorService")
	private DataMonitorService dataMonitorService;

	@Resource(name = "namedParamService")
	private NamedParamService namedParamService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 데이타모니터링관리 화면 오픈
	 * @param paramMap
	 */
	@RequestMapping(value="/itg/system/datamonitor/goDataMonitorManager.do")
	public String goDataMonitorManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/datamonitor/dataMonitorManager.nvf";
		
		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		//로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);
		
		//로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);
		
		return forwarPage;
	}

	/**
	 * 데이타모니터링목록 화면 오픈
	 * @param paramMap
	 */
	@RequestMapping(value="/itg/system/datamonitor/goDataMonitorList.do")
	public String goDataMonitorList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/datamonitor/dataMonitorList.nvf";
		
		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		//로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);
		
		//로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);
		
		return forwarPage;
	}

	/**
	 * 데이타모니터링 목록출력
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/datamonitor/searchDataMonitorList.do")
	public @ResponseBody ResultVO searchDataMonitorList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = dataMonitorService.selectDataMonitorList(paramMap);		
			totalCount = dataMonitorService.selectDataMonitorCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 데이타모니터링 엑셀다운로드
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/datamonitor/searchDataMonitorExcelDown.do")
	public @ResponseBody ResultVO searchDataMonitorExcelDown(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = dataMonitorService.selectDataMonitorExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 데이타모니터링 등록
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/datamonitor/insertDataMonitor.do")
	public @ResponseBody ResultVO insertDataMonitor(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			Map<String, Object> resultMap = dataMonitorService.insertDataMonitor(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 데이타모니터링 수정 
	 * @param paramMap
	 */	
	@RequestMapping(value="/itg/system/datamonitor/updateDataMonitor.do")
	public @ResponseBody ResultVO updateDataMonitor(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			dataMonitorService.updateDataMonitor(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 데이타모니터링 삭제
	 * @param paramMap
	 */	
	@RequestMapping(value="/itg/system/datamonitor/deleteDataMonitorList.do")
	public @ResponseBody ResultVO deleteDataMonitorList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List<Map<String, Object>> input1List = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");	
			dataMonitorService.deleteDataMonitorList(input1List);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 데이타모니터링 점검 데이타 조회
	 * @param paramMap
	 */	
	@RequestMapping(value="/itg/system/datamonitor/searchDataMonitorCheck.do")
	public @ResponseBody ResultVO searchDataMonitor(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			Map<String, Object> resultMap =  dataMonitorService.selectDataMonitorCheck(paramMap);
			Map<String, Object> dataMonitorMap =  dataMonitorService.selectDataMonitor(paramMap);
			
			resultMap.put("DATAMON_NM", dataMonitorMap.get("DATAMON_NM"));
			resultMap.put("PARAM_ID_LIST", dataMonitorMap.get("PARAM_ID_LIST"));
			resultMap.put("PARAM_NM_LIST", dataMonitorMap.get("PARAM_NM_LIST"));
			resultMap.put("PARAM_VALUE_LIST", dataMonitorMap.get("PARAM_VALUE_LIST"));
			resultMap.put("OUT_ID_LIST", dataMonitorMap.get("OUT_ID_LIST"));
			resultMap.put("OUT_NM_LIST", dataMonitorMap.get("OUT_NM_LIST"));
			
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 데이타모니터링결과 목록출력
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/datamonitor/searchDataMonitorCheckList.do")
	public @ResponseBody ResultVO searchDataMonitorCheckList(@RequestBody ModelMap paramMap) throws NkiaException {
			ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = dataMonitorService.selectDataMonitorCheckList(paramMap);		
			totalCount = dataMonitorService.selectDataMonitorCheckCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 속성 목록삭제
	 * @param paramMap
	 */	
	@RequestMapping(value="/itg/system/datamonitor/deleteDataMonitorCheckList.do")
	public @ResponseBody ResultVO deleteTableColumnList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List<Map<String, Object>> input1List = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");	
			dataMonitorService.deleteDataMonitorCheckList(input1List);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 데이타모니터링결과상세 목록출력
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/datamonitor/searchDataMonitorCheckDataList.do")
	public @ResponseBody ResultVO searchDataMonitorCheckDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		Map<String, Object> param = null;
		Map<String, Object> excelParam = null;
		Map<String, Object> mPs = null;
		try{
			if(null != paramMap.get("param")){
				param = (Map<String, Object>)paramMap.get("param");
				excelParam = (Map<String, Object>)paramMap.get("excelParam");
			}else{
				PagingUtil.getFristEndNum(paramMap);
				param = paramMap;
			}
			if(null == param.get("mode") || "".equals(param.get("mode"))){
				throw new NkiaException("처리모드정보는 필수입력입니다.");
			}else if(null == param.get("check_no") || "".equals(param.get("check_no"))){
				throw new NkiaException("점검번호는 필수입력입니다.");
			}else if(null == param.get("out_id_list") || "".equals(param.get("out_id_list"))){
				throw new NkiaException("조회컬럼정보는 필수입력입니다.");
			}
			
			String mode = StringUtil.replaceNull(String.valueOf(param.get("mode")), ""); // 수행모드 {selectList, exceldown}
			String check_no = StringUtil.replaceNull(String.valueOf(param.get("check_no")), ""); // 현황 아이디
			String out_id_list = StringUtil.replaceNull(String.valueOf(param.get("out_id_list")), ""); // 조회컬럼정보
			String [] out_id_arry = out_id_list.split(",");
			
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			String query = "";
			StringBuffer querySb = null;
			
			/*******************
			 * 기본 SQL 생성
			 ******************/
			mPs = new HashMap<String, Object>();
			querySb = new StringBuffer();
			querySb.append("                SELECT ROW_NO") ;
			for(int i=0; i<out_id_arry.length; i++){
				querySb.append(", C" + (i+1) + " AS " +  out_id_arry[i]) ;
			}
			querySb.append("\n                   FROM (") ;
			querySb.append("\n                        SELECT CHECK_NO, ROW_NO, C1, C2, C3, C4, C5 ");
			querySb.append("\n                             , C6, C7, C8, C9, C10 ");
			querySb.append("\n                             , INS_USER_ID, INS_DT ");
			querySb.append("\n                          FROM SYS_DATAMON_CHECK_DATA A ");
			querySb.append("\n                         WHERE A.CHECK_NO = :check_no ");
			mPs.put("check_no", check_no);
			querySb.append("\n                     ) XX1 ") ;
			querySb.append("\n                 WHERE 1=1 ") ;
			
			if("selectList".equals(mode)){
				
				/*******************
				 * selectList-1) Count 조회한다. 
				 ******************/
				String query2 = "";
				query2 += "\n           SELECT /* searchDatamonitorCheckDataList.do - selectCount */ ";
				query2 += "\n                  COUNT(1) AS CNT ";
				query2 += "\n             FROM ( ";  
				query2 += "\n" + querySb.toString();
				query2 += "\n                ) XX2 ";
				query = query2;
				Map<String, Object> countMap = namedParamService.selectMap(query, mPs);
				totalCount = Integer.parseInt(String.valueOf(countMap.get("CNT")));
				
				/*******************
				 * selectList-2) ROWNUM 구하고 정렬한다
				 ******************/
				String query3 = "";
				query3 += "           SELECT ROW_NO AS RNUM ";
				query3 += "\n                , XX2.* ";
				query3 += "\n            FROM ( ";  
				query3 += "\n" + querySb.toString();
				query3 += "\n                ) XX2 ";
				query3 += "\n         ORDER BY RNUM";
				query = query3;
				
				/*******************
				 * selectList-3) 페이징 적용한다.
				 ******************/
				if(!"".equals(StringUtil.replaceNull(String.valueOf(param.get("start")), ""))){
					String query4 = "";
					query4 += "\n  SELECT /* searchDatamonitorCheckDataList.do - selectList */ ";
					query4 += "\n         XX3.*  ";
					query4 += "\n    FROM (   ";
					query4 += "\n" + query3;
					query4 += "\n       ) XX3 ";
					int page = (Integer)param.get("page");
					int limit = (Integer)param.get("limit");
					int start = (Integer)param.get("start");
					int end = limit * page;
					query4 += "\n  WHERE XX3.RNUM BETWEEN :first_num AND :last_num ";
					mPs.put("first_num", start + 1);
					mPs.put("last_num", end);
					query = query4;
				}
				List<Map<String, Object>> resultList = namedParamService.selectList(query, mPs);

				/*******************
				 * selectList-4) 결과조회
				 ******************/
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)param.get(BaseEnum.PAGER_START_PARAM.getString()));
				gridVO.setRows(resultList);
				resultVO.setGridVO(gridVO);  
				
			}else if("exceldown".equals(mode)){

				/*******************
				 * exceldown-1) 엑셀다운로드 한다.
				 ******************/
				query = querySb.toString();
				List<Map<String, Object>> resultList = namedParamService.selectList(query, mPs);
				ModelMap excelMap = excelMaker.makeExcelFile(excelParam, resultList);
				resultVO.setResultMap(excelMap); 
			}else{
				throw new NkiaException("넘겨받은 mode 유형을 처리할 수 없습니다. [" + mode + "]");
			}
		} catch (Exception e) {
			if (e.getCause() instanceof SQLException) {
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.00007"));
				throw new NkiaException(e);
			} else {
				throw new NkiaException(e);
			}
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 데이타모니터링 검사실행한다.
	 * 
	 * @param reqMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping("/itg/system/datamonitor/runDataMonitor.do")
	public @ResponseBody ResultVO runDataMonitor(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			String datamon_id = StringUtil.parseString(paramMap.get("datamon_id"));
			dataMonitorService.runDataMonitor(datamon_id, paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
}
