/*
 * @(#)DataMonitorServiceImpl.java              2018. 1. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.datamonitor.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.dao.NamedParamDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.datamonitor.dao.DataMonitorDAO;
import com.nkia.itg.system.datamonitor.service.DataMonitorService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("dataMonitorService")
public class DataMonitorServiceImpl implements DataMonitorService {

	private static final Logger logger = LoggerFactory.getLogger(DataMonitorServiceImpl.class);
	
	@Resource(name="namedParamDAO")
	private NamedParamDAO namedParamDAO;

	@Resource(name = "dataMonitorDAO")
	public DataMonitorDAO dataMonitorDAO;
	
	/**
	 * 데이타모니터 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDataMonitor(Map<String, Object> paramMap) throws NkiaException {
		return dataMonitorDAO.selectDataMonitor(paramMap);
	}
	
	/**
	 * 데이타모니터 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorList(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		String login_cust_id = "";
		String login_user_auth = "@";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			login_cust_id = userVO.getCust_id();
			List sysAuthList = userVO.getSysAuthList();
			for (Object item : sysAuthList) {
				HashMap<String, Object> items = (HashMap<String, Object>)item;
				login_user_auth += (items.get("AUTH_ID").toString().toUpperCase() + "@");
			}
		}
		paramMap.put("login_user_id", login_user_id);
		paramMap.put("login_cust_id", login_cust_id);
		//paramMap.put("login_user_auth", login_user_auth);
		paramMap.put("login_admin_yn", (-1 < login_user_auth.indexOf("@SYSTEM_USER@")? "Y": ""));
		return dataMonitorDAO.selectDataMonitorList(paramMap);
	}
	

	/**
	 * 데이타모니터 건수조회
	 * @param paramMap
	 */
	public int selectDataMonitorCount(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		String login_cust_id = "";
		String login_user_auth = "@";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			login_cust_id = userVO.getCust_id();
			List sysAuthList = userVO.getSysAuthList();
			for (Object item : sysAuthList) {
				HashMap<String, Object> items = (HashMap<String, Object>)item;
				login_user_auth += (items.get("AUTH_ID").toString().toUpperCase() + "@");
			}
		}
		paramMap.put("login_user_id", login_user_id);
		paramMap.put("login_cust_id", login_cust_id);
		//paramMap.put("login_user_auth", login_user_auth);
		paramMap.put("login_admin_yn", (-1 < login_user_auth.indexOf("@SYSTEM_USER@")? "Y": ""));
		return dataMonitorDAO.selectDataMonitorCount(paramMap);
	}

	/**
	 * 데이타모니터 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorExcelDown(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		String login_cust_id = "";
		String login_user_auth = "@";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			login_cust_id = userVO.getCust_id();
			List sysAuthList = userVO.getSysAuthList();
			for (Object item : sysAuthList) {
				HashMap<String, Object> items = (HashMap<String, Object>)item;
				login_user_auth += (items.get("AUTH_ID").toString().toUpperCase() + "@");
			}
		}
		paramMap.put("login_user_id", login_user_id);
		paramMap.put("login_cust_id", login_cust_id);
		//paramMap.put("login_user_auth", login_user_auth);
		paramMap.put("login_admin_yn", (-1 < login_user_auth.indexOf("@SYSTEM_USER@")? "Y": ""));
		return dataMonitorDAO.selectDataMonitorExcelDown(paramMap);
	}

	/**
	 * 통합현황 등록
	 * @param paramMap
	 */
	public Map<String, Object> insertDataMonitor(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		String datamon_id = dataMonitorDAO.selectDataMonitorNewId(paramMap); // SELECT PK FROM SYS_DATASET
		paramMap.put("datamon_id", datamon_id);
		paramMap.put("login_user_id", login_user_id);
		dataMonitorDAO.insertDataMonitor(paramMap); // INSERT SYS_DATASET 
		
		resultMap.put("datamon_id", datamon_id);
		return resultMap;
	}
	
	/**
	 * 통합현황 수정
	 * @param paramMap
	 */
	public void updateDataMonitor(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		paramMap.put("login_user_id", login_user_id);
		dataMonitorDAO.updateDataMonitor(paramMap); // UPDATE SYS_DATASET 
	}
	
	/**
	 * 통합현황 삭제
	 * @param paramMap
	 */
	public void deleteDataMonitorList(List<Map<String, Object>> list) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		for(Map<String, Object> map: list){
			map.put("login_user_id", login_user_id);
			dataMonitorDAO.deleteDataMonitor(map); // DELETE SYS_DATAMON_CHECK 
		}
	}
	
	/**
	 * 데이타모니터링결과 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDataMonitorCheck(Map<String, Object> paramMap) throws NkiaException {
		return dataMonitorDAO.selectDataMonitorCheck(paramMap);
	}

	/**
	 * 데이타모니터 건수조회
	 * @param paramMap
	 */
	public int selectDataMonitorCheckCount(Map<String, Object> paramMap) throws NkiaException {
		return dataMonitorDAO.selectDataMonitorCheckCount(paramMap);
	}

	/**
	 * 데이타모니터링결과 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorCheckList(Map<String, Object> paramMap) throws NkiaException {
		return dataMonitorDAO.selectDataMonitorCheckList(paramMap);
	}

	/**
	 * 데이타모니터 건수조회
	 * @param paramMap
	 */
	public int selectDataMonitorCheckDataCount(Map<String, Object> paramMap) throws NkiaException {
		return dataMonitorDAO.selectDataMonitorCheckDataCount(paramMap);
	}

	/**
	 * 데이타모니터링결과 목록삭제
	 * @param paramMap
	 */
	public int deleteDataMonitorCheckList(List<Map<String, Object>> list) throws NkiaException {
		int delCnt = 0;
		for(Map<String, Object> map: list){
			delCnt += dataMonitorDAO.deleteDataMonitorCheck(map); // DELETE SYS_DATAMON_CHECK 
			dataMonitorDAO.deleteDataMonitorCheckDataList(map); // DELETE SYS_DATAMON_CHECK_DATA
		}
		return delCnt;
	}
	
	/**
	 * 데이타모니터링결과 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorCheckDataList(Map<String, Object> paramMap) throws NkiaException {
		return dataMonitorDAO.selectDataMonitorCheckDataList(paramMap);
	}

	/**
	 * 
	 * 데이타체크 실행한다. 
	 * 
	 * @param reqMap
	 * @return
	 * @throws NkiaException
	 */
	public Map<String, Object> runDataMonitor(String datamon_id, Map<String, Object> params) throws NkiaException {
		
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		long startTime = System.currentTimeMillis(); 
		String check_no = (new SimpleDateFormat("yyyyMMddhhmmssSSS")).format(new Date(startTime)).substring(0, 16);
		if(!"".equals(StringUtil.parseString(params.get("USER_ID")))){
			login_user_id = StringUtil.parseString(params.get("USER_ID"));
		}
		
		String sRESULT_CD = "";
		String sRESULT_MSG = "";
		HashMap<String, Object> responseMap = new HashMap<String, Object>();
		Map<String, Object> psMap = null;
		int iResultCnt = 0;
		int result = 0;
		String query = "";
		
		// step1) 모니터 기준정보 상세조회 SELECT SYS_DATAMON
		StringBuffer querySb = null;
		psMap = new HashMap<String, Object>();
		psMap.put("datamon_id", datamon_id);
		Map<String, Object> datamonMap = dataMonitorDAO.selectDataMonitor(psMap);
		
		if(null == datamonMap){
			throw new NkiaException("기준정보 데이타모니터링정보가 존재하지 않습니다.");
		}
		int iCHECK_START_CNT = StringUtil.parseInteger(datamonMap.get("CHECK_START_CNT"));
		int iCHECK_END_CNT = StringUtil.parseInteger(datamonMap.get("CHECK_END_CNT"));
		
		// step2) 결과 생성 INSERT SYS_DATAMON_CHECK
		psMap = new HashMap<String, Object>();
		psMap.put("check_no", check_no);
		psMap.put("datamon_id", datamon_id);
		psMap.put("run_time", System.currentTimeMillis() - startTime);
		psMap.put("login_user_id", login_user_id);
		dataMonitorDAO.insertDataMonitorCheck(psMap);
		
		try{
			// step3) 결과 목록 저장 INSERT SYS_DATAMON_CHECK_DATA
			querySb = new StringBuffer();
			psMap = new HashMap<String, Object>();
			querySb.append("\n INSERT INTO SYS_DATAMON_CHECK_DATA( ") ;
			querySb.append("\n       CHECK_NO ") ;
			querySb.append("\n     , ROW_NO ") ;
			//String [] arrPARAM_ID_LIST = StringUtil.parseString(datamonMap.get("PARAM_ID_LIST")).split(",");
			//String [] arrPARAM_NM_LIST = StringUtil.parseString(datamonMap.get("PARAM_NM_LIST")).split(",");
			//String [] arrPARAM_VALUE_LIST = StringUtil.parseString(datamonMap.get("PARAM_VALUE_LIST")).split(",");
			String [] arrOUT_ID_LIST = StringUtil.parseString(datamonMap.get("OUT_ID_LIST")).split(",");
			for(int iCol=0; iCol<arrOUT_ID_LIST.length; iCol++){
				querySb.append("\n     , C" + (iCol + 1)) ;
			}
			querySb.append("\n     , INS_USER_ID ") ;
			querySb.append("\n     , INS_DT ) ") ;
			querySb.append("\n       (SELECT A.CHECK_NO AS CHECK_NO ") ;
			querySb.append("\n             , ROWNUM AS ROW_NO ") ;
			for(int iCol=0; iCol<arrOUT_ID_LIST.length; iCol++){
				querySb.append("\n             , B." + arrOUT_ID_LIST[iCol] + " AS C" + (iCol + 1)) ;
			}
			querySb.append("\n             , A.INS_USER_ID ") ;
			querySb.append("\n             , A.INS_DT ") ;
			querySb.append("\n         FROM ( ") ;
			querySb.append("\n               SELECT :check_no AS CHECK_NO ") ;
			querySb.append("\n                    , :login_user_id AS INS_USER_ID ") ;
			querySb.append("\n                    , SYSDATE AS INS_DT ") ;
			querySb.append("\n                 FROM DUAL ") ;
			querySb.append("\n              ) A ") ; // 기본 파라메터정보
			querySb.append("\n            , ( ") ;
			querySb.append("\n                " + StringUtil.parseString(datamonMap.get("BAS_SQL")) + " ") ;
			querySb.append("\n              ) B ") ; // 실행 쿼리정보
			querySb.append("\n        ) ") ;
			query = querySb.toString();
			psMap.put("check_no", check_no);
			psMap.put("login_user_id", login_user_id);
			
			//for(int iCol=0; iCol<arrPARAM_ID_LIST.length; iCol++){
			//	String value = "";
			//	if(params.containsKey(arrPARAM_ID_LIST[iCol])){
			//		value = StringUtil.parseString(params.get(arrPARAM_ID_LIST[iCol]));
			//	}else if(iCol < arrPARAM_VALUE_LIST.length){
			//		value = arrPARAM_VALUE_LIST[iCol];
			//		if("YYYYMMDD".equals(value)){ // YYYYMMDD 면 현재일자를 넣어준다.
			//			value = (new SimpleDateFormat("yyyyMMdd")).format(new Date(System.currentTimeMillis()));
			//		}
			//	}
			//	psMap.put(arrPARAM_ID_LIST[iCol], value);
			//}
			iResultCnt = namedParamDAO.insert(query, psMap);
			sRESULT_CD = "SUCCESS";
		}catch(Exception e){
			sRESULT_CD = "ERROR";
			throw new NkiaException(e);
		}
		
		// step4) 결과값 저장 UPDATE SYS_DATAMON_CHECK
		psMap = new HashMap<String, Object>();
		String check_status = ("SUCCESS".equals(sRESULT_CD)? (iCHECK_START_CNT <= iResultCnt && iResultCnt <= iCHECK_END_CNT? "FAIL": "OK"): "ERROR");
		long run_time = System.currentTimeMillis() - startTime;
		psMap.put("run_time", run_time);
		psMap.put("check_status", check_status);
		psMap.put("result_cnt", iResultCnt);
		psMap.put("result_cd", sRESULT_CD);
		psMap.put("result_msg", sRESULT_MSG);
		psMap.put("check_no", check_no); // PK
		dataMonitorDAO.updateDataMonitorCheck(psMap);
		
		// step5) 기준정보에 최종값 저장 UPDATE SYS_DATAMON
		psMap = new HashMap<String, Object>();
		psMap.put("login_user_id", login_user_id);
		psMap.put("last_check_no", check_no);
		psMap.put("last_check_status", check_status);
		psMap.put("last_run_time", run_time);
		psMap.put("last_result_cnt", iResultCnt);
		psMap.put("last_result_cd", sRESULT_CD);
		psMap.put("last_result_msg", sRESULT_MSG);
		psMap.put("datamon_id", datamon_id); // PK
		dataMonitorDAO.updateLastCheckInfo(psMap);
		
		// 결과반환
		responseMap.put("RESULT_CD", sRESULT_CD);
		responseMap.put("RESULT_MSG", sRESULT_MSG);
		return responseMap;
	}

	
}
