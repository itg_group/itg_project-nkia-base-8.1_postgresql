/*
 * @(#)DataMonitorServiceImpl.java              2018. 1. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.datamonitor.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DataMonitorService {

	/**
	 * 데이타모니터 단건조회
	 * @throws NkiaException 
	 */
	public Map<String, Object> selectDataMonitor(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 데이타모니터 목록조회
	 * @throws NkiaException 
	 */
	public List<Map<String, Object>> selectDataMonitorList(Map<String, Object> paramMap) throws NkiaException;
	public List<Map<String, Object>> selectDataMonitorExcelDown(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 데이타모니터 건수조회
	 * @throws NkiaException 
	 */
	public int selectDataMonitorCount(Map<String, Object> paramMap) throws NkiaException;

	public Map<String, Object> insertDataMonitor(Map<String, Object> paramMap) throws NkiaException;
	
	public void updateDataMonitor(Map<String, Object> paramMap) throws NkiaException;

	public void deleteDataMonitorList(List<Map<String, Object>> list) throws NkiaException;
	
	/**
	 * 데이타모니터결과 단건조회
	 * @throws NkiaException 
	 */
	public Map<String, Object> selectDataMonitorCheck(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 데이타모니터결과 목록조회
	 * @throws NkiaException 
	 */
	public List<Map<String, Object>> selectDataMonitorCheckList(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 데이타모니터결과 건수조회
	 * @throws NkiaException 
	 */
	public int selectDataMonitorCheckCount(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 데이타모니터결과 목록삭제
	 * @throws NkiaException 
	 */
	public int deleteDataMonitorCheckList(List<Map<String, Object>> list) throws NkiaException;
	
	/**
	 * 데이타모니터결과상세 목록조회
	 * @throws NkiaException 
	 */
	public List<Map<String, Object>> selectDataMonitorCheckDataList(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 데이타모니터결과상세 건수조회
	 * @throws NkiaException 
	 */
	public int selectDataMonitorCheckDataCount(Map<String, Object> paramMap) throws NkiaException;

	/**
	 * 데이타모니터 실행.
	 * @throws NkiaException 
	 * @throws NkiaException 
	 */
	public Map<String, Object> runDataMonitor(String monId, Map<String, Object> params) throws NkiaException ;
}
