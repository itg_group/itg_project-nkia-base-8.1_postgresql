/*
 * @(#)DataMonitorDAO.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.datamonitor.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("dataMonitorDAO")
public class DataMonitorDAO extends EgovComAbstractDAO{

	/**
	 * 데이타모니터링 조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDataMonitor(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("DataMonitorDAO.selectDataMonitor", paramMap);
	}
	
	/**
	 * 데이타모니터링 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorList(Map<String, Object> paramMap) throws NkiaException{
		return list("DataMonitorDAO.selectDataMonitorList", paramMap);
	}

	/**
	 * 데이타모니터링 건수조회
	 * @param paramMap
	 */
	public int selectDataMonitorCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("DataMonitorDAO.selectDataMonitorCount", paramMap);
	}
	
	/**
	 * 데이타모니터링 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorExcelDown(Map<String, Object> paramMap) throws NkiaException{
		return list("DataMonitorDAO.selectDataMonitorExcelDown", paramMap);
	}
	
	/**
	 * 통합현황 신규 Key값 조회
	 * @param paramMap
	 */
	public String selectDataMonitorNewId(Map<String, Object> paramMap) throws NkiaException {
		return (String) selectByPk("DataMonitorDAO.selectDataMonitorNewId", paramMap);
	}

	/**
	 * 통합현황 등록
	 * @param paramMap
	 */
	public void insertDataMonitor(Map<String, Object> dataMonitorInfo) throws NkiaException {
		this.insert("DataMonitorDAO.insertDataMonitor", dataMonitorInfo);
	}

	/**
	 * 통합현황 수정
	 * @param paramMap
	 */
	public void updateDataMonitor(Map<String, Object> dataMonitorInfo) throws NkiaException {
		this.update("DataMonitorDAO.updateDataMonitor", dataMonitorInfo);
	}

	/**
	 * 통합현황 수정
	 * @param paramMap
	 */
	public void updateLastCheckInfo(Map<String, Object> dataMonitorInfo) throws NkiaException {
		this.update("DataMonitorDAO.updateLastCheckInfo", dataMonitorInfo);
	}

	/**
	 * 통합현황 삭제
	 * @param paramMap
	 */
	public void deleteDataMonitor(Map<String, Object> paramMap) throws NkiaException {
		this.update("DataMonitorDAO.deleteDataMonitor", paramMap);
	}
	
	/**
	 * 데이타모니터링결과 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDataMonitorCheck(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("DataMonitorDAO.selectDataMonitorCheck", paramMap);
	}

	/**
	 * 통합현황 등록
	 * @param paramMap
	 */
	public void insertDataMonitorCheck(Map<String, Object> dataMonitorInfo) throws NkiaException {
		this.insert("DataMonitorDAO.insertDataMonitorCheck", dataMonitorInfo);
	}

	/**
	 * 통합현황 등록
	 * @param paramMap
	 */
	public void updateDataMonitorCheck(Map<String, Object> dataMonitorInfo) throws NkiaException {
		this.update("DataMonitorDAO.updateDataMonitorCheck", dataMonitorInfo);
	}
	
	/**
	 * 데이타모니터링결과 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorCheckList(Map<String, Object> paramMap) throws NkiaException{
		return list("DataMonitorDAO.selectDataMonitorCheckList", paramMap);
	}

	/**
	 * 데이타모니터링결과 건수조회
	 * @param paramMap
	 */
	public int selectDataMonitorCheckCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("DataMonitorDAO.selectDataMonitorCheckCount", paramMap);
	}

	/**
	 * 통합현황 삭제
	 * @param paramMap
	 */
	public int deleteDataMonitorCheck(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("DataMonitorDAO.deleteDataMonitorCheck", paramMap);
	}
	
	/**
	 * 데이타모니터링결과상세 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDataMonitorCheckDataList(Map<String, Object> paramMap) throws NkiaException{
		return list("DataMonitorDAO.selectDataMonitorCheckDataList", paramMap);
	}

	/**
	 * 데이타모니터링결과상세 건수조회
	 * @param paramMap
	 */
	public int selectDataMonitorCheckDataCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("DataMonitorDAO.selectDataMonitorCheckDataCount", paramMap);
	}

	/**
	 * 통합현황 삭제
	 * @param paramMap
	 */
	public int deleteDataMonitorCheckDataList(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("DataMonitorDAO.deleteDataMonitorCheckDataList", paramMap);
	}
	
}

