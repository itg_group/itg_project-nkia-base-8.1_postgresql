package com.nkia.itg.system.integratedboard.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/* *통합게시판* */

public interface BbsBaseInitService {

	public void setInitData(HttpServletRequest request, Map<String, Object> paramMap) throws Exception;
}
