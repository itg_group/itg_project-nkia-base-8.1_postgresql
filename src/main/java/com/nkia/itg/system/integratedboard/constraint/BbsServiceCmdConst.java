package com.nkia.itg.system.integratedboard.constraint;

import java.io.Serializable;

/* *통합게시판* */

public class BbsServiceCmdConst implements Serializable {

	private static final long serialVersionUID = 7647050674233837645L;

	// 화면오픈
	public static final String BBS_BASE_GO_BBS_MNG = "BBS_BASE_GO_BBS_MNG";
	public static final String BBS_BASE_GO_BBS_LIST = "BBS_BASE_GO_BBS_LIST";
	public static final String BBS_BASE_GO_BBS_DETAIL= "BBS_BASE_GO_BBS_DETAIL";
	public static final String BBS_BASE_GO_BBS_DEBUG_USER_INFO = "BBS_BASE_GO_BBS_DEBUG_USER_INFO";
	
	// bbs
	public static final String BBS_BASE_SELECT = "BBS_BASE_SELECT";
	public static final String BBS_BASE_SELECT_COUNT = "BBS_BASE_SELECT_COUNT";
	public static final String BBS_BASE_SELECT_LIST = "BBS_BASE_SELECT_LIST";
	public static final String BBS_BASE_SELECT_REPLY_LIST = "BBS_BASE_SELECT_REPLY_LIST";
	public static final String BBS_BASE_SELECT_EXCELDOWNLOAD = "BBS_BASE_SELECT_EXCELDOWNLOAD";
	public static final String BBS_BASE_INSERT = "BBS_BASE_INSERT";
	public static final String BBS_BASE_INSERT_REPLY = "BBS_BASE_INSERT_REPLY";
	public static final String BBS_BASE_UPDATE = "BBS_BASE_UPDATE";
	public static final String BBS_BASE_UPDATE_CONFIRM = "BBS_BASE_UPDATE_CONFIRM";
	public static final String BBS_BASE_UPDATE_READ_COUNT = "BBS_BASE_UPDATE_READ_COUNT";
	public static final String BBS_BASE_UPDATE_DELETE = "BBS_BASE_UPDATE_DELETE";
	public static final String BBS_BASE_UPDATE_DEL_CANCEL = "BBS_BASE_UPDATE_DEL_CANCEL";
	public static final String BBS_BASE_DELETE = "BBS_BASE_DELETE";
	public static final String BBS_BASE_SAVE = "BBS_BASE_SAVE";
	public static final String BBS_BASE_SAVE_LIST = "BBS_BASE_SAVE_LIST";
	
	// comnt
	public static final String BBS_COMNT_SELECT = "BBS_COMNT_SELECT";
	public static final String BBS_COMNT_SELECT_COUNT = "BBS_COMNT_SELECT_COUNT";
	public static final String BBS_COMNT_SELECT_LIST = "BBS_COMNT_SELECT_LIST";
	public static final String BBS_COMNT_SELECT_EXCELDOWNLOAD = "BBS_COMNT_SELECT_EXCELDOWNLOAD";
	public static final String BBS_COMNT_INSERT = "BBS_COMNT_INSERT";
	public static final String BBS_COMNT_UPDATE = "BBS_COMNT_UPDATE";
	public static final String BBS_COMNT_DELETE = "BBS_COMNT_DELETE";

}