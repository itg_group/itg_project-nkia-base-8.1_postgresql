package com.nkia.itg.system.integratedboard.util;

import com.nkia.itg.base.application.util.web.MDCUtil;

/* *통합게시판* */

/**
 * 공통 String Util
 * org.apache.commons.lang.StringUtils 상속후 필요 메소드 추가
 * 자세한 기타 자세한 스펙은 org.apache.commons.lang.StringUtils 참조
 * (url : http://jakarta.apache.org/commons/lang/api-release/org/apache/commons/lang/StringUtils.html)
 */
public class BbsExceptionHelper {

	/**
	 * Exception stack 정보 반환
	 */
	public String getStackTrace(Exception exception) {
		String result = "";
		StackTraceElement[] stackTraceElements = exception.getStackTrace();
		String strStackText = "";
		String strStackPrev = "";
		int nStackCnt = 0;
		result += "\n ■ [stackTraceElements] -------------------------------------------------------------- ";
		if(null != exception.getMessage()) {
			result += "\n ---- exception.getMessage() : " + exception.getMessage();
		}
		result += "\n ";
		for (StackTraceElement stackTraceElement : stackTraceElements) {
			strStackText = stackTraceElement.toString();
			if ("".equals(strStackText)) {
				continue;
			} else if (strStackPrev.equals(strStackText)) {
				continue;
			} else if (!strStackText.startsWith("com.nkia")) {
				continue;
			} else if (-1 < strStackText.indexOf("$$")) {
				continue;
			} else if (0 < strStackText.indexOf(".BaseWebMvcLogAspect.")) {
				continue;
			} else if (0 < strStackText.indexOf("Filter.")) {
				continue;
			}
			result += "\n ---- " + "E-simple [" + ++nStackCnt + "] " + strStackText;
			strStackPrev = strStackText;
		}
		if(null != MDCUtil.get(MDCUtil.REQUEST_URI_MDC)) {
			result += "\n ---- " + "T-simple : MDCUtil.REQUEST_URI_MDC : " + MDCUtil.get(MDCUtil.REQUEST_URI_MDC);
		}
		//result += "\n ------- ▶ " + e.getMessage();
		result += "\n ";
		strStackText = "";
		strStackPrev = "";
		nStackCnt = 0;
		for (StackTraceElement stackTraceElement : stackTraceElements) {
			strStackText = stackTraceElement.toString();
			if ("".equals(strStackText)) {
				continue;
			} else if (strStackPrev.equals(strStackText)) {
				continue;
			} else if (!strStackText.startsWith("com.nkia")) {
				continue;
			} else if (-1 < strStackText.indexOf("$$")) {
				continue;
			}
			result += "\n ---- " + "E-detail [" + ++nStackCnt + "] " + strStackText;
			strStackPrev = strStackText;
		}
		result += "\n ■ [Exception_Class] " + exception.getClass().getName().toString() + " [Exception_Code] " + exception.hashCode() + " [sPrefix] " + "com.nkia";
		result += "\n ------------------------------------------------------------------------ ";
		
		return result;
	}

	public String getStackTrace(Throwable exception) {
		String result = "";
		StackTraceElement[] stackTraceElements = exception.getStackTrace();
		String strStackText = "";
		String strStackPrev = "";
		int nStackCnt = 0;
		result += "\n ■ [stackTraceElements] ------------------------------------------------------------- ";
		if(null != exception.getMessage()) {
			result += "\n ---- exception.getMessage() : " + exception.getMessage();
		}
		result += "\n ";
		for (StackTraceElement stackTraceElement : stackTraceElements) {
			strStackText = stackTraceElement.toString();
			if ("".equals(strStackText)) {
				continue;
			} else if (strStackPrev.equals(strStackText)) {
				continue;
			} else if (!strStackText.startsWith("com.nkia")) {
				continue;
			} else if (-1 < strStackText.indexOf("$$")) {
				continue;
			} else if (0 < strStackText.indexOf(".BaseWebMvcLogAspect.")) {
				continue;
			} else if (0 < strStackText.indexOf("Filter.")) {
				continue;
			}
			result += "\n ---- " + "T-simple [" + ++nStackCnt + "] " + strStackText;
			strStackPrev = strStackText;
		}
		//result += "\n ------- ▶ " + e.getMessage();
		if(null != MDCUtil.get(MDCUtil.REQUEST_URI_MDC)) {
			result += "\n ---- " + "T-simple : MDCUtil.REQUEST_URI_MDC : " + MDCUtil.get(MDCUtil.REQUEST_URI_MDC);
		}
		result += "\n ";
		strStackText = "";
		strStackPrev = "";
		nStackCnt = 0;
		for (StackTraceElement stackTraceElement : stackTraceElements) {
			strStackText = stackTraceElement.toString();
			if ("".equals(strStackText)) {
				continue;
			} else if (strStackPrev.equals(strStackText)) {
				continue;
			} else if (!strStackText.startsWith("com.nkia")) {
				continue;
			} else if (-1 < strStackText.indexOf("$$")) {
				continue;
			}
			result += "\n ---- " + "T-detail [" + ++nStackCnt + "] " + strStackText;
			strStackPrev = strStackText;
		}
		result += "\n ■ [Exception_Class] " + exception.getClass().getName().toString() + " [Exception_Code] " + exception.hashCode() + " [sPrefix] " + "com.nkia";
		result += "\n ------------------------------------------------------------------------------------------------- ";
		
		return result;
	}
	
	public String getStackCurrentInfo(Throwable exception) {
		StackTraceElement currentStack = exception.getStackTrace()[0];
		String currentStackInfo = currentStack.getClassName() + "." + currentStack.getMethodName() + "("+currentStack.getFileName() + ":"+currentStack.getLineNumber() + ")";
		return currentStackInfo;
	}
	
	


}