package com.nkia.itg.system.integratedboard.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.integratedboard.constraint.BbsNameConst;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/* *통합게시판* */

@Repository("bbsComntDAO")
public class BbsComntDAO extends EgovComAbstractDAO {
	
	/**
	 * 게시글 코멘트 상세
	 * 
     * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectBbsComnt(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		return (Map<String, Object>) this.selectByPk("BbsComntDAO.selectBbsComnt", paramMap);
	}

	/**
	 * 게시글 코멘트 개수
	 * 
     * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	public int selectBbsComntCount(Map<String, Object> paramMap)throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		return (Integer)this.selectByPk("BbsComntDAO.selectBbsComntCount", paramMap);
	}
	
	/**
	 * 게시글 코멘트 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectBbsComntList(Map<String, Object> paramMap)throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		return this.list("BbsComntDAO.selectBbsComntList", paramMap);
	}

	/**
	 * 게시글 코멘트 신규 ID
	 * 
     * @param paramMap
	 * @return String
	 * @throws Exception
	 */
	public String selectBbsComntNewId(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		String BoardID = (String) this.selectByPk("BbsComntDAO.selectBbsComntNewId", paramMap);
		return BoardID;
	}

	/**
	 * 게시글 코멘트 등록
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void insertBbsComnt(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		this.insert("BbsComntDAO.insertBbsComnt", paramMap);
	}

	/**
	 * 게시글 코멘트 수정
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsComnt(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsComntDAO.updateBbsComnt", paramMap);
	}
	
	/**
	 * 게시글 코멘트 삭제
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbsComnt(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		this.delete("BbsComntDAO.deleteBbsComnt", paramMap);
	}

	/**
	 * 게시글 자식 코멘트 삭제
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbsChildComnt(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}else if(paramMap.get(BbsNameConst.BBS_DETAIL) == null){
			throw new NkiaException("글정보를 확인해주시기 바랍니다.");
		}
		
		this.delete("BbsComntDAO.deleteBbsChildComnt", paramMap);
	}
	
}
