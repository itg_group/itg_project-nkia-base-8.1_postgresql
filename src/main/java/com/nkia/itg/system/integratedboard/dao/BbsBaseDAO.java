package com.nkia.itg.system.integratedboard.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.integratedboard.constraint.BbsNameConst;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/* *통합게시판* */

@Repository("bbsBaseDAO")
public class BbsBaseDAO extends EgovComAbstractDAO {

	/**
	 * 게시글 글상세
	 * 
     * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectBbs(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap = (Map<String, Object>) this.selectByPk("BbsBaseDAO.selectBbs", paramMap);
		
		String out_contnt_yn = EgovStringUtil.isNullToString(paramMap.get("out_contnt_yn"));
		if("".equals(out_contnt_yn) || "Y".equals(out_contnt_yn)){
			if(null != resultMap){
				Map<String, Object> mPs = null;
				mPs = new HashMap<String, Object>();
				mPs.put(BbsNameConst.BBM_DETAIL, paramMap.get(BbsNameConst.BBM_DETAIL));
				mPs.put("login_user_id", paramMap.get("login_user_id"));
				mPs.put("bbm_id", resultMap.get("BBM_ID"));
				mPs.put("bbs_no", resultMap.get("BBS_NO"));
				Map<String, Object> contntMap = (Map<String, Object>) this.selectByPk("BbsBaseDAO.selectBbsContnt", mPs);
				resultMap.put("CONTNT", contntMap.get("CONTNT"));
			}
		}
		return resultMap;
	}
	
	/**
	 * 게시글 글목록 개수
	 * 
     * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	public int selectBbsCount(Map<String, Object> paramMap)throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		return (Integer)this.selectByPk("BbsBaseDAO.selectBbsCount", paramMap);
	}
	
	/**
	 * 게시글 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectBbsList(Map<String, Object> paramMap)throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		List<Map<String, Object>> resultList =  (List<Map<String, Object>>)this.list("BbsBaseDAO.selectBbsList", paramMap);
		
		String out_contnt_yn = EgovStringUtil.isNullToString(paramMap.get("out_contnt_yn"));
		if("Y".equals(out_contnt_yn)){
			Map<String, Object> mPs = null;
			for(int i=0; i<resultList.size(); i++){
				Map<String, Object> detailMap = resultList.get(i);
				mPs = new HashMap<String, Object>();
				mPs.put(BbsNameConst.BBM_DETAIL, paramMap.get(BbsNameConst.BBM_DETAIL));
				mPs.put("login_user_id", paramMap.get("login_user_id"));
				mPs.put("bbm_id", detailMap.get("BBM_ID"));
				mPs.put("bbs_no", detailMap.get("BBS_NO"));
				Map<String, Object> contntMap = (Map<String, Object>) this.selectByPk("BbsBaseDAO.selectBbsContnt", mPs);
				detailMap.put("CONTNT", contntMap.get("CONTNT"));
			}
		}
		return resultList;
	}
	
	/**
	 * 게시판 글 신규 ID
	 * 
     * @param paramMap
	 * @return String
	 * @throws Exception
	 */
	public String selectBbsNewId(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		String BoardID = (String) this.selectByPk("BbsBaseDAO.selectBbsNewId", paramMap);
		return BoardID;
	}

	/**
	 * 게시글 글 등록
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void insertBbs(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.insert("BbsBaseDAO.insertBbs", paramMap);
	}

	/**
	 * 게시글 글 수정
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbs(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbs", paramMap);
	}

	/**
	 * 게시글 승인/승인취소
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsOpen(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbsOpen", paramMap);
	}

	/**
	 * 게시글 읽음 수 증가
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsReadCountAdd(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbsReadCountAdd", paramMap);
	}

	/**
	 * 게시글 답글 순번 누적 수정
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsReplySeqAdd(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbsReplySeqAdd", paramMap);
	}

	/**
	 * 게시글 글 삭제(USE_YN = 'Y')
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsDelete(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbsDelete", paramMap);
	}

	/**
	 * 게시글 글 삭제취소(USE_YN = 'N')
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsDelCancel(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbsDelCancel", paramMap);
	}

	/**
	 * 게시글 글 삭제
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbs(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.delete("BbsBaseDAO.deleteBbs", paramMap);
	}

	/**
	 * 게시글 자식 답글 삭제(USE_YN = 'Y')
	 * 
     * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	public int selectBbsChildReplyCount(Map<String, Object> paramMap)throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		return (Integer)this.selectByPk("BbsBaseDAO.selectBbsChildReplyCount", paramMap);
	}
	
	/**
	 * 게시글 자식 답글 삭제(USE_YN = 'Y')
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsChildReplyDelete(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbsChildReplyDelete", paramMap);
	}

	/**
	 * 게시글 자식 답글 삭제
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbsChildReply(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.delete("BbsBaseDAO.deleteBbsChildReply", paramMap);
	}

	/**
	 * 사용자 글조회정보 상세
	 * 
     * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectBbsUserRead(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		return (Map<String, Object>) this.selectByPk("BbsBaseDAO.selectBbsUserRead", paramMap);
	}
	
	/**
	 * 사용자 글조회정보 등록
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void insertBbsUserRead(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.insert("BbsBaseDAO.insertBbsUserRead", paramMap);
	}

	/**
	 * 사용자 글조회정보 수정
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsUserRead(Map<String, Object> paramMap) throws Exception {
		if(paramMap.get(BbsNameConst.BBM_DETAIL) == null){
			throw new NkiaException("게시판정보를 확인해주시기 바랍니다.");
		}
		
		this.update("BbsBaseDAO.updateBbsUserRead", paramMap);
	}

	public void updateSubBbsCategory(ModelMap paramMap) throws Exception {
		this.update("BbsBaseDAO.updateSubBbsCategory", paramMap);
	}

}
