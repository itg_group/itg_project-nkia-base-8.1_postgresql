package com.nkia.itg.system.integratedboard.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.integratedboard.constraint.BbsNameConst;
import com.nkia.itg.system.integratedboard.dao.BbsBaseInitDAO;
import com.nkia.itg.system.integratedboard.service.BbsBaseInitService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/* *통합게시판* */

@Service("bbsBaseInitService")
public class BbsBaseInitServiceImpl implements BbsBaseInitService {

	@Resource(name = "bbsBaseInitDAO")
	public BbsBaseInitDAO bbsBaseInitDAO;
	
	/**
	 * 기본데이타 설정
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void setInitData(HttpServletRequest request, Map<String, Object> paramMap) throws Exception {

		/*******************************************
		 * 기본파레메터
		 *******************************************/
		
		String bbm_id = EgovStringUtil.isNullToString(paramMap.get("bbm_id"));
		String bbs_no = EgovStringUtil.isNullToString(paramMap.get("bbs_no"));
		
		if ("".equals(bbm_id.trim())) {
			throw new NkiaException("게시판아이디는 필수입력입니다.");
		}
		
		/*******************************************
		 * 파라메터에 세션정보추가
		 *******************************************/
		
		String login_user_id = getLoginUserInfo("login_user_id");
		String login_cust_id = getLoginUserInfo("login_cust_id");
		String login_user_auth = getLoginUserInfo("login_user_auth");
		String login_admin_yn = ""; // getLoginUserInfo("login_admin_yn");
		String preview_user_id = EgovStringUtil.isNullToString(paramMap.get("preview_user_id"));
		String login_user_id_origin = getLoginUserInfo("login_user_id");

		/*******************************************
		 * 사용객체정보
		 *******************************************/
		
		Map<String, Object> mPs = null; // query paramapter
		Map<String, Object> userDetail = null;
		Map<String, Object> userAuth = new HashMap<String, Object>();
		Map<String, Object> userProcAuth = new HashMap<String, Object>();
		Map<String, Object> bbmDetail = null;
		Map<String, Object> bbmUserAuth = new HashMap<String, Object>();
		Map<String, Object> bbsDetail = null;
		String user_auths = "";
		String user_proc_auths = "";
		String bbm_opts = "";
		String bbm_user_auths = "";

		/*******************************************
		 * 기본 데이타 조회
		 *******************************************/
		
		// @@@@@@@@@@@ [1/6] userDetail
		mPs = new HashMap<String, Object>();
		if(2 < preview_user_id.length()){
			mPs.put("login_user_id", preview_user_id);
		}else{
			mPs.put("login_user_id", login_user_id);
		}
		userDetail = bbsBaseInitDAO.selectInitUser(mPs); // (1)
		if(null == userDetail){
			userDetail = new HashMap<String, Object>();
		}
		login_user_id = EgovStringUtil.isNullToString(userDetail.get("USER_ID"));
		login_cust_id = EgovStringUtil.isNullToString(userDetail.get("CUST_ID"));
		
		// @@@@@@@@@@@ [2/6] userAuth, user_auths :: 사용안하는 정보
		userAuth = new HashMap<String, Object>();
		//mPs = new HashMap<String, Object>();
		//mPs.put("login_user_id", login_user_id);
		//List<Map<String, Object>> userAuthList = bbsBaseInitDAO.selectInitUserAuthList(mPs); // (2)
		//setListToMap(userAuthList, userAuth, "KEY", "VALUE");
		//user_auths = getListToStr(userAuthList, "KEY", "VALUE", "|");
		//login_user_auth = getListToStr(userAuthList, "KEY", "VALUE", "@");
		
		// @@@@@@@@@@@ [3/6] userProcAuth, user_proc_auths :: 사용안하는 정보
		userProcAuth = new HashMap<String, Object>();
		//mPs = new HashMap<String, Object>();
		//mPs.put("login_user_id", login_user_id);
		//List<Map<String, Object>> userProcAuthList = bbsBaseInitDAO.selectInitUserProcAuthList(mPs); // (3)
		//setListToMap(userProcAuthList, userProcAuth, "KEY", "VALUE");
		//user_proc_auths = getListToStr(userProcAuthList, "KEY", "VALUE", "|");
		
		// @@@@@@@@@@@ [4/6] bbmDetail, bbm_opts
		mPs = new HashMap<String, Object>();
		mPs.put("bbm_id", bbm_id);
		bbmDetail = bbsBaseInitDAO.selectInitBbm(mPs); // (4)
		bbm_opts = getMapToStr(bbmDetail, "_YN", "|");
		
		// @@@@@@@@@@@ [5/6] bbmUserAuth, bbm_user_auths
		bbmUserAuth = new HashMap<String, Object>();
		mPs = new HashMap<String, Object>();
		mPs.put("bbm_id", bbm_id);
		mPs.put("login_user_id", login_user_id);
		List<Map<String, Object>> bbmUserAuthList = bbsBaseInitDAO.selectInitBbmUserAuthList(mPs); // (5)
		setListToMap(bbmUserAuthList, bbmUserAuth, "KEY", "VALUE");
		bbm_user_auths = getListToStr(bbmUserAuthList, "KEY", "VALUE", "|");
		
		// @@@@@@@@@@@ [6/6] bbmDetail
		if(!"".equals(bbs_no)){
			mPs = new HashMap<String, Object>();
			mPs.put("bbm_id", bbm_id);
			mPs.put("bbs_no", bbs_no);
			mPs.put("login_user_id", login_user_id);
			mPs.put(BbsNameConst.BBM_DETAIL, bbmDetail);
			bbsDetail = bbsBaseInitDAO.selectInitBbs(mPs); // (6)
		}
		
		/*******************************************
		*기본 데이타 set
		*******************************************/
		
		paramMap.put("login_user_id", login_user_id);
		paramMap.put("login_cust_id", login_cust_id);
		paramMap.put("login_user_auth", login_user_auth);
		paramMap.put("login_admin_yn", login_admin_yn);
		if(!login_user_id.equals(login_user_id_origin)){
			paramMap.put("login_user_id_origin", login_user_id_origin);
		}
		
		paramMap.put(BbsNameConst.USER_DETAIL, userDetail);
		
		userAuth.put("user_auths", user_auths);
		paramMap.put(BbsNameConst.USER_AUTH, userAuth);
		
		userProcAuth.put("user_proc_auths", user_proc_auths);
		paramMap.put(BbsNameConst.USER_PROC_AUTH, userProcAuth);
		
		bbmDetail.put("bbm_opts", bbm_opts);
		paramMap.put(BbsNameConst.BBM_DETAIL, bbmDetail);
		
		bbmUserAuth.put("bbm_user_auths", bbm_user_auths);
		paramMap.put(BbsNameConst.BBM_USER_AUTH, bbmUserAuth);
		
		paramMap.put(BbsNameConst.BBS_DETAIL, bbsDetail);
		
		paramMap.put("reqeust_uri", request.getRequestURI());
	}
	
	private String getLoginUserInfo(String type) throws Exception {
		String result = "";
		String login_user_id = "";
		String login_cust_id = "";
		String login_user_auth = "";
		String login_admin_yn = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if (isAuthenticated) {
			UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			login_cust_id = userVO.getCust_id();
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			login_user_auth = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					Map<String, Object> mData = userAuth.get(i);
					login_user_auth += String.valueOf(mData.get("AUTH_ID")) + "@";
				}
			}
			//if (bbmService.checkAuth("Qna.Auth.List")) {
			//	login_admin_yn = "Y";
			//}
		}
		if("login_user_id".equals(type)){
			result = login_user_id;
		}else if("login_cust_id".equals(type)){
			result = login_cust_id;
		}else if("login_user_auth".equals(type)){
			result = login_user_auth;
		//}else if("login_admin_yn".equals(type)){
		//	result = login_admin_yn;
		}
		return result;
	}

	private void setListToMap(List<Map<String, Object>> list, Map<String, Object> map, String key, String value) throws Exception {
		if(null != map){
			for(int i=0; i<list.size(); i++){
				Map<String, Object> mData = list.get(i);
				String _key = String.valueOf(mData.get(key));
				String _value = String.valueOf(mData.get(value));
				map.put(_key, _value);
			}
		}
	}
	
	private String getListToStr(List<Map<String, Object>> list, String key, String value, String pipe) throws Exception {
		String resultStr = pipe;
		for(int i=0; i<list.size(); i++){
			Map<String, Object> mData = list.get(i);
			String _key = String.valueOf(mData.get(key));
			String _value = String.valueOf(mData.get(value));
			resultStr += _key + ":" + _value + pipe;
		}
		return resultStr;
	}
	
	private String getMapToStr(Map<String, Object> map, String lastIndex, String pipe) throws Exception {
		String resultStr = pipe;
		if(null != map){
			for(String _key : map.keySet()){
				String _value = String.valueOf(map.get(_key));
				if(1 < _key.lastIndexOf(lastIndex)){
					resultStr += _key + ":" + _value + pipe;
				}
			}
		}
		return resultStr;
	}
	
}
