package com.nkia.itg.system.integratedboard.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.system.integratedboard.dao.BbsComntDAO;
import com.nkia.itg.system.integratedboard.service.BbsComntService;

/* *통합게시판* */

@Service("bbsComntService")
public class BbsComntServiceImpl implements BbsComntService {

	@Resource(name = "bbsComntDAO")
	public BbsComntDAO bbsComntDAO;

	/**
	 * 코멘트 상세 조회
	 * 
	 * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectBbsComnt(Map<String, Object> paramMap) throws Exception {
		return bbsComntDAO.selectBbsComnt(paramMap);
	}

	/**
	 * 코멘트 개수
	 * 
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public int selectBbsComntCount(Map<String, Object> paramMap) throws Exception {
		return bbsComntDAO.selectBbsComntCount(paramMap);
	}

	/**
	 * 코멘트 리스트
	 * 
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectBbsComntList(Map<String, Object> paramMap) throws Exception {
		return bbsComntDAO.selectBbsComntList(paramMap);
	}

	/**
	 * 코멘트 등록
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void insertBbsComnt(Map<String, Object> paramMap) throws Exception {
		String comnt_no = bbsComntDAO.selectBbsComntNewId(paramMap);
		paramMap.put("comnt_no", comnt_no);
		bbsComntDAO.insertBbsComnt(paramMap);
	}
	
	/**
	 * 코멘트 수정
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsComnt(Map<String, Object> paramMap) throws Exception {
		bbsComntDAO.updateBbsComnt(paramMap);
	}

	/**
	 * 코멘트 삭제
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbsComnt(Map<String, Object> paramMap) throws Exception {
		bbsComntDAO.deleteBbsComnt(paramMap);
	}

}
