package com.nkia.itg.system.integratedboard.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.integratedboard.constraint.BbsServiceCmdConst;
import com.nkia.itg.system.integratedboard.service.BbsBaseCheckService;
import com.nkia.itg.system.integratedboard.service.BbsBaseInitService;
import com.nkia.itg.system.integratedboard.service.BbsBaseService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/* *통합게시판* */

@Controller
public class BbsBaseController {

	private static final Logger logger = LoggerFactory.getLogger(BbsBaseController.class);

	@Resource(name = "bbsBaseService")
	private BbsBaseService bbsBaseService;

	@Resource(name = "bbsBaseInitService")
	private BbsBaseInitService bbsBaseInitService;
	
	@Resource(name = "bbsBaseCheckService")
	private BbsBaseCheckService bbsBaseCheckService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 
	 * bbs 관리형 화면
	 * 
	 * @param paramMap
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/itg/system/integratedboard/goBbsMng.do", method=RequestMethod.GET)
	public String goBbsMng(HttpServletRequest request, @RequestParam Map<String, Object> reqMap, ModelMap paramMap) throws Exception {
		String forwarPage = "/itg/system/integratedboard/bbs_BASE_mng.nvf";
		try {
			if (!urlAccessCheckService.urlAuthCheck(request)) {
				return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
			}
			
			for(String key: reqMap.keySet() ){
				String value = (String)reqMap.get(key); 
				paramMap.put(key, value);
			}
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_GO_BBS_MNG, paramMap);
			
		} catch (NkiaException e) {
			e.printStackTrace();
			paramMap.put("RESULT_CD", "FAIL_BBS_0091");
			paramMap.put("RESULT_MSG", e.getMessage());
			forwarPage = BaseEnum.ERROR_PAGE_400.getString();
//			String login_user_id_origin = EgovStringUtil.isNullToString(paramMap.get("login_user_id_origin"));
//			if(-1 < login_user_id_origin.indexOf("testbbs_admin")){
//				BbsExceptionHelper bbsExceptionHelper = new BbsExceptionHelper();
//				paramMap.put("RESULT_DESC", bbsExceptionHelper.getStackTrace(e));
//				forwarPage = "/itg/system/integratedboard/bbsPageErrorDev.nvf";
//			}
		} catch (Exception e) {
			e.printStackTrace();
			paramMap.put("RESULT_CD", "FAIL_BBS_0099");
			paramMap.put("RESULT_MSG", "글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
			forwarPage = BaseEnum.ERROR_PAGE_500.getString();
//			String login_user_id_origin = EgovStringUtil.isNullToString(paramMap.get("login_user_id_origin"));
//			if(-1 < login_user_id_origin.indexOf("testbbs_admin")){
//				BbsExceptionHelper bbsExceptionHelper = new BbsExceptionHelper();
//				paramMap.put("RESULT_DESC", bbsExceptionHelper.getStackTrace(e));
//				forwarPage = "/itg/system/integratedboard/bbsPageErrorDev.nvf";
//			}
		}
		return forwarPage;
	}

	/**
	 * 
	 * debug사용자정보 확인용
	 * 
	 * @param paramMap
	 * @return String
	 * @throws Exception
	 */
//	@RequestMapping(value = "/itg/system/integratedboard/goBbsDebugPage.do")
//	public String goBbsDebugPage(HttpServletRequest request, @RequestParam Map<String, Object> reqMap, ModelMap paramMap) throws Exception {
//		String forwarPage = "/itg/system/integratedboard/bbsDebugPage.nvf";
//		String origin_params = "";
//		try {
//			for(String key: reqMap.keySet() ){
//				String value = (String)reqMap.get(key);
//				paramMap.put(key, value);
//			}
//			origin_params = "" + paramMap;
//			String req_options = EgovStringUtil.isNullToString(paramMap.get("req_options"));
//			String serviceCmd = EgovStringUtil.isNullToString(paramMap.get("serviceCmd"));
//			if("".equals(serviceCmd)){
//				serviceCmd = BbsServiceCmdConst.BBS_BASE_GO_BBS_DEBUG_USER_INFO;
//			}
//			paramMap.put("serviceCmd", serviceCmd);
//			
//			logger.info(">>>>> paramMap : " + paramMap);
//			bbsBaseInitService.setInitData(request, paramMap);
//			bbsBaseCheckService.checkBbs(serviceCmd, paramMap);
//			
//			String login_user_id = EgovStringUtil.isNullToString(paramMap.get("login_user_id"));
//			throw new NkiaException(login_user_id + " 사용자의 checkBbs("+serviceCmd+") 가 정상적으로 통과했습니다 => 강제 NkiaException 발생시킵니다 => setInitData 정보 확인용입니다 ");
//			
//		} catch (NkiaException e) {
//			e.printStackTrace();
//			paramMap.put("RESULT_CD", "FAIL_BBS_0091");
//			paramMap.put("RESULT_MSG", e.getMessage());
//			BbsExceptionHelper bbsExceptionHelper = new BbsExceptionHelper();
//			paramMap.put("RESULT_DESC", bbsExceptionHelper.getStackTrace(e) + "\n origin_params : " + origin_params);
//		} catch (Exception e) {
//			e.printStackTrace();
//			paramMap.put("RESULT_CD", "FAIL_BBS_0099");
//			paramMap.put("RESULT_MSG", "글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
//			BbsExceptionHelper bbsExceptionHelper = new BbsExceptionHelper();
//			paramMap.put("RESULT_DESC", bbsExceptionHelper.getStackTrace(e) + "\n origin_params : " + origin_params);
//		}
//		return forwarPage;
//	}
	
	/**
	 * 글 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbs.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbs(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_SELECT, paramMap);
			
			String req_options = EgovStringUtil.isNullToString(paramMap.get("req_options"));
			
			if(!(-1 < req_options.indexOf("|READ_COUNT_ADD_YN:N|"))){
				bbsBaseService.updateBbsReadCountAdd(paramMap); // 조회수 증가
			}
			
			Map<String, Object> resultMap = bbsBaseService.selectBbs(paramMap);
			resultVO.setResultMap(resultMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg((resultMap != null? "글 조회되었습니다": "글 조회된정보가 없습니다"));
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 게시글 글목록 리스트
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbsList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbsList(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_SELECT_LIST, paramMap);
			
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);
			
			int totalCount = bbsBaseService.selectBbsCount(paramMap);
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>(); 
			if(0 < totalCount){
				resultList = bbsBaseService.selectBbsList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);

			resultVO.setGridVO(gridVO);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg((0 < resultList.size()? "글목록 조회되었습니다": "글목록 조회된정보가 없습니다"));
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글목록 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 답변글목록 리스트
	 * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value = "/itg/system/integratedboard/selectBbsReplyList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbsReplyList(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_SELECT_REPLY_LIST, paramMap);
			
			GridVO gridVO = new GridVO();
			//PagingUtil.getFristEndNum(paramMap);
			
			//int totalCount = functionPointService.selectBbsCount(paramMap);
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>(); 
			//if(0 < totalCount){
				resultList = bbsBaseService.selectBbsList(paramMap);
			//}
			
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("답글목록 조회되었습니다"); // 조회되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("답글목록 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 글목록 엑셀다운로드
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbsExcelDown.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbsExcelDown(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map<String, Object> excelParam = (Map<String, Object>) paramMap.get("excelParam");
			Map<String, Object> param = (Map<String, Object>) paramMap.get("param");
			
			bbsBaseInitService.setInitData(request, param);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_SELECT_EXCELDOWNLOAD, param);
			
			String excelParamString = excelParam.toString();
			if(-1 < excelParamString.indexOf("CONTNT")){
				param.put("out_contnt_yn", "Y");
			}
			//int totalCount = bbsBaseService.selectBbsCount(param);
			//if(50000 < totalCount){
			//	throw new NkiaException("엑셀다운로드는 5만건 초과할수 없습니다 다시요청해주시기 바랍니다 [조회된건수:"+totalCount+"]");
			//}
			List resultList = bbsBaseService.selectBbsList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("엑셀다운로드 되었습니다"); // 조회되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 글 등록
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/insertBbs.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertBbs(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_INSERT, paramMap);
			
			bbsBaseService.insertBbs(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.result.00001")); // 저장되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00001")); // 저장시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 답글 등록
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/insertBbsReply.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertBbsReply(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_INSERT_REPLY, paramMap);
			
			bbsBaseService.insertBbsReply(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("답글 " + messageSource.getMessage("msg.common.result.00001")); // 저장되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("답글 " + messageSource.getMessage("msg.common.error.00001")); // 저장시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 글 수정
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/updateBbs.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateBbs(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_UPDATE, paramMap);
			
			bbsBaseService.updateBbs(paramMap);
			
			// 답글이 있는 경우 답글의 카테고리를 일괄 수정
			bbsBaseService.updateSubBbsCategory(paramMap);
			
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.result.00002")); // 수정되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00002")); // 수정시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 글 읽음 수 증가
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/updateBbsReadCount.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateBbsReadCount(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_UPDATE_READ_COUNT, paramMap);
			
			bbsBaseService.updateBbsReadCountAdd(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.result.00002")); // 수정되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00002")); // 수정시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 글 삭제
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/deleteBbs.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deleteBbs(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_DELETE, paramMap);
			
			bbsBaseService.deleteBbs(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.result.00003")); // 삭제되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00003")); // 삭제시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 글 삭제 (update)
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/updateBbsDelete.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateBbsDelete(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_DELETE, paramMap);
			
			bbsBaseService.updateBbsDelete(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.result.00003")); // 삭제되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00003")); // 삭제시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 글 삭제 취소
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/updateBbsDelCancel.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateBbsDelCancel(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_UPDATE_DEL_CANCEL, paramMap);
			
			bbsBaseService.updateBbsDelCancel(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("글 " + "삭제취소 되었습니다");
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00003")); // 삭제시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 게시글 save
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/integratedboard/saveBbs.do", method=RequestMethod.POST)
	public @ResponseBody
	ResultVO saveBbs(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_SAVE, paramMap);
			
			bbsBaseService.saveBbs(paramMap);
			
			String msg = "글저장 ";
			String saveCmd = EgovStringUtil.isNullToString(paramMap.get("saveCmd"));
			switch (saveCmd) {
			case "baseDetailConfirmAppr":
				msg = "승인이 ";
				break;
			case "baseDetailConfirmCancel":
				msg = "승인취소가 ";
				break;
			}
			resultVO.setSuccess(true);
			resultVO.setResultMsg(msg + messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00001")); // 저장시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 게시글 saveList
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/integratedboard/saveBbsList.do", method=RequestMethod.POST)
	public @ResponseBody
	ResultVO saveBbsList(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_BASE_SAVE_LIST, paramMap);
			
			bbsBaseService.saveBbsList(paramMap);

			String msg = "글 목록저장 ";
			String saveCmd = EgovStringUtil.isNullToString(paramMap.get("input1_saveCmd"));
			switch (saveCmd) {
			case "baseListConfirmAppr":
				msg = "일괄승인이 ";
				break;
			case "baseListConfirmCancel":
				msg = "일괄승인취소가 ";
				break;
			}
			resultVO.setSuccess(true);
			resultVO.setResultMsg(msg + messageSource.getMessage("msg.common.result.00004")); // 정상 처리 되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글목록 " + messageSource.getMessage("msg.common.error.00001")); // 저장시 오류가 발생하였습니다.
		}
		return resultVO;
	}

}
