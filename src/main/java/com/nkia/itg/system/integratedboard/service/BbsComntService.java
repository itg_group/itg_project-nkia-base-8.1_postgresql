package com.nkia.itg.system.integratedboard.service;

import java.util.List;
import java.util.Map;

/* *통합게시판* */

public interface BbsComntService {

	// 게시글 코멘트
	public Map<String, Object> selectBbsComnt(Map<String, Object> paramMap) throws Exception;
	public int selectBbsComntCount(Map<String, Object> paramMap) throws Exception;
	public List<Map<String, Object>> selectBbsComntList(Map<String, Object> paramMap) throws Exception;
	public void insertBbsComnt(Map<String, Object> paramMap) throws Exception; 
	public void updateBbsComnt(Map<String, Object> paramMap) throws Exception;  
	public void deleteBbsComnt(Map<String, Object> paramMap) throws Exception;
}
