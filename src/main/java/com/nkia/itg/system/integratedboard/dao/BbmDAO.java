package com.nkia.itg.system.integratedboard.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/* *통합게시판* */

@Repository("bbmDAO")
public class BbmDAO extends EgovComAbstractDAO {

	/**
	 * 게시판 글상세
	 * 
     * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectBbm(Map<String, Object> paramMap) throws Exception {
		return (Map<String, Object>) this.selectByPk("BbmDAO.selectBbm", paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws Exception
	 */
	public int selectBbmCount(Map<String, Object> paramMap)throws Exception {
		return (Integer)this.selectByPk("BbmDAO.selectBbmCount", paramMap);
	}

	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectBbmList(Map<String, Object> paramMap)throws Exception {
		return this.list("BbmDAO.selectBbmList", paramMap);
	}
	
	/**
	 * 게시판 글 신규 ID
	 * 
     * @param paramMap
	 * @return String
	 * @throws Exception
	 */
	public String selectBbmNewId(Map<String, Object> paramMap) throws Exception {
		String BoardID = (String) this.selectByPk("BbmDAO.selectBbmNewId", paramMap);
		return BoardID;
	}
	
	/**
	 * 게시판 글 등록
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void insertBbm(Map<String, Object> paramMap) throws Exception {
		this.insert("BbmDAO.insertBbm", paramMap);
	}
	
	/**
	 * 게시판 글 수정
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbm(Map<String, Object> paramMap) throws Exception {
		this.update("BbmDAO.updateBbm", paramMap);
	}

	/**
	 * 게시판 글 삭제
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbm(Map<String, Object> paramMap) throws Exception {
		this.delete("BbmDAO.deleteBbm", paramMap);
	}

	/**
	 * 게시글 테이블 생성
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void createTableBbs(Map<String, Object> paramMap)throws Exception{
		if (null == paramMap || null == paramMap.get("bbmDetail")) {
			throw new NkiaException("게시판정보가 존재하지 않습니다.");
		}else if ("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("DB_MAIN_USER")), ""))) {
			throw new NkiaException("DB_MAIN_USER 는 필수정보입니다.");
		}
		this.update("BbmDAO.createTableBbs", paramMap);
	}

	/**
	 * 게시글 인덱스 생성
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	/*public void createIndexBbs01(Map<String, Object> paramMap)throws Exception{
		if (null == paramMap || null == paramMap.get("bbmDetail")) {
			throw new NkiaException("게시판정보가 존재하지 않습니다.");
		}else if ("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("DB_MAIN_USER")), ""))) {
			throw new NkiaException("DB_MAIN_USER 는 필수정보입니다.");
		}
		this.update("BbmDAO.createIndexBbs01", paramMap);
	}*/
	
	/**
	 * 게시글 시쿼스 생성
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void createSequenceBbs(Map<String, Object> paramMap)throws Exception{
		if (null == paramMap || null == paramMap.get("bbmDetail")) {
			throw new NkiaException("게시판정보가 존재하지 않습니다.");
		}else if ("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("DB_MAIN_USER")), ""))) {
			throw new NkiaException("DB_MAIN_USER 는 필수정보입니다.");
		}
		this.update("BbmDAO.createSequenceBbs", paramMap);
	}
	
	/**
	 * 게시글 코멘트 테이블 생성
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void createTableBbsComnt(Map<String, Object> paramMap)throws Exception{
		if (null == paramMap || null == paramMap.get("bbmDetail")) {
			throw new NkiaException("게시판정보가 존재하지 않습니다.");
		}else if ("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("DB_MAIN_USER")), ""))) {
			throw new NkiaException("DB_MAIN_USER 는 필수정보입니다.");
		}
		this.update("BbmDAO.createTableBbsComnt", paramMap);
	}
	
}
