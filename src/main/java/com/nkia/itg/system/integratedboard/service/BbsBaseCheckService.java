package com.nkia.itg.system.integratedboard.service;

import java.util.Map;

/* *통합게시판* */

public interface BbsBaseCheckService {

	public void checkBbs(String funcName, Map<String, Object> paramMap) throws Exception;
	
}
