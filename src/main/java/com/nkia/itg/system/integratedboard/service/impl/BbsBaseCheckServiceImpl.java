package com.nkia.itg.system.integratedboard.service.impl;

import java.lang.reflect.Field;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.integratedboard.constraint.BbsNameConst;
import com.nkia.itg.system.integratedboard.constraint.BbsServiceCmdConst;
import com.nkia.itg.system.integratedboard.dao.BbsComntDAO;
import com.nkia.itg.system.integratedboard.service.BbsBaseCheckService;

import egovframework.com.utl.fcc.service.EgovStringUtil;

/* *통합게시판* */

@Service("bbsBaseCheckService")
public class BbsBaseCheckServiceImpl implements BbsBaseCheckService {
	
	private static final Logger logger = LoggerFactory.getLogger(BbsBaseCheckServiceImpl.class);

	@Resource(name = "bbsComntDAO")
	public BbsComntDAO bbsComntDAO;
	
	/**
	 * 게시글 권한 체크
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void checkBbs(String serviceCmd, Map<String, Object> paramMap) throws Exception {
		
		/*******************************************
		 * 파라메터셋팅 1
		 *******************************************/
		
		// 기본파라메터
		String bbm_id = EgovStringUtil.isNullToString(paramMap.get("bbm_id"));
		String bbs_no = EgovStringUtil.isNullToString(paramMap.get("bbs_no"));
		String comnt_no = EgovStringUtil.isNullToString(paramMap.get("comnt_no"));
		String up_bbs_no = EgovStringUtil.isNullToString(paramMap.get("up_bbs_no"));
		String title = EgovStringUtil.isNullToString(paramMap.get("title"));
		String req_options = EgovStringUtil.isNullToString(paramMap.get("req_options"));
		
		// 세션 정보
		String login_user_id = EgovStringUtil.isNullToString(paramMap.get("login_user_id"));
		String login_cust_id = EgovStringUtil.isNullToString(paramMap.get("login_cust_id"));
		String login_user_auth = EgovStringUtil.isNullToString(paramMap.get("login_user_auth"));
		String login_admin_yn = EgovStringUtil.isNullToString(paramMap.get("login_admin_yn"));
		String login_user_id_origin = EgovStringUtil.isNullToString(paramMap.get("login_user_id_origin"));
		String preview_user_id = EgovStringUtil.isNullToString(paramMap.get("preview_user_id"));
		
		// 조회 객체정보
		Map<String, Object> userDetail = (Map<String, Object>)paramMap.get(BbsNameConst.USER_DETAIL);
		Map<String, Object> userAuth = (Map<String, Object>)paramMap.get(BbsNameConst.USER_AUTH); // 사용안하는 정보
		Map<String, Object> userProcAuth = (Map<String, Object>)paramMap.get(BbsNameConst.USER_PROC_AUTH); // 사용안하는 정보
		Map<String, Object> bbmDetail = (Map<String, Object>)paramMap.get(BbsNameConst.BBM_DETAIL);
		Map<String, Object> bbmUserAuth = (Map<String, Object>)paramMap.get(BbsNameConst.BBM_USER_AUTH);
		Map<String, Object> bbsDetail = (Map<String, Object>)paramMap.get(BbsNameConst.BBS_DETAIL);

		String user_use_yn = "";
		String user_auths = ""; // 사용안하는 정보
		String user_proc_auths = ""; // 사용안하는 정보
		
		// BBM 정보
		String bbm_confirm_auth_yn = "";
		String bbm_use_yn = "";
		String bbm_opts = "";
		
		String bbm_user_mng_user_grp_yn = "";
		String bbm_user_reg_user_grp_yn = "";
		String bbm_user_confirm_user_grp_yn = "";
		String bbm_user_auths = "";

		String bbs_ins_user_id = "";
		//String bbs_ins_cust_id = "";
		String bbs_use_yn = "";
		String bbs_row_ins_user_yn = "";
		
		/*******************************************
		 * 파라메터셋팅 2
		 *******************************************/
		
		// @@@@@@@@@@@ [1/6] userDetail
		if(null != userDetail){
			user_use_yn = EgovStringUtil.isNullToString(userDetail.get("USE_YN"));
		}
		
		// @@@@@@@@@@@ [2/6] userAuth
		if(null != userAuth){
			user_auths = EgovStringUtil.isNullToString(userAuth.get("user_auths"));
		}
		
		// @@@@@@@@@@@ [3/6] userProcAuth
		if(null != userProcAuth){
			user_proc_auths = EgovStringUtil.isNullToString(userProcAuth.get("user_proc_auths"));
		}
		
		// @@@@@@@@@@@ [4/6] bbmDetail
		if(null != bbmDetail){
			bbm_confirm_auth_yn = EgovStringUtil.isNullToString(bbmDetail.get("CONFIRM_AUTH_YN"));
			bbm_use_yn = EgovStringUtil.isNullToString(bbmDetail.get("USE_YN"));
			bbm_opts = EgovStringUtil.isNullToString(bbmDetail.get("bbm_opts"));
		}
		
		// @@@@@@@@@@@ [5/6] bbmUserAuth
		if(null != bbmUserAuth){
			bbm_user_mng_user_grp_yn = EgovStringUtil.isNullToString(bbmUserAuth.get("USER_MNG_USER_GRP_YN"));
			bbm_user_reg_user_grp_yn = EgovStringUtil.isNullToString(bbmUserAuth.get("USER_REG_USER_GRP_YN"));
			bbm_user_confirm_user_grp_yn = EgovStringUtil.isNullToString(bbmUserAuth.get("USER_CONFIRM_USER_GRP_YN"));
			bbm_user_auths = EgovStringUtil.isNullToString(bbmUserAuth.get("bbm_user_auths"));
		}
		
		// @@@@@@@@@@@ [6/6] bbsDetail
		if(null != bbsDetail){
			bbs_ins_user_id = EgovStringUtil.isNullToString(bbsDetail.get("INS_USER_ID"));
			//bbs_ins_cust_id = EgovStringUtil.isNullToString(bbsDetail.get("INS_CUST_ID"));
			bbs_use_yn = EgovStringUtil.isNullToString(bbsDetail.get("USE_YN"));
			bbs_row_ins_user_yn = EgovStringUtil.isNullToString(bbsDetail.get(BbsNameConst.ROW_INS_USER_YN));
		}
		
		/*******************************************
		 * BBS_ACCESS_INFO 로그남기기
		 *******************************************/
		
		if(!"".equals(serviceCmd)){
			String infoMsg = "{ reqeust_uri:'"+EgovStringUtil.isNullToString(paramMap.get("reqeust_uri"))+"'";
			infoMsg += "\n, serviceCmd:'"+serviceCmd+"'";
			infoMsg += "\n, bbm_id:'"+bbm_id+"'";
			if(!"".equals(bbs_no)) infoMsg += "\n, bbs_no:'"+bbs_no+"'";
			if(!"".equals(comnt_no)) infoMsg += "\n, comnt_no:'"+comnt_no+"'";
			if(!"".equals(req_options)) infoMsg += "\n, req_options:'"+req_options+"'";
			infoMsg += "\n, login_user_id:'"+login_user_id+"'";
			infoMsg += "\n, user_use_yn:'"+user_use_yn+"'";
			if(!"".equals(login_user_id_origin)) infoMsg += "\n, login_user_id_origin:'"+login_user_id_origin+"'";
			if(!"".equals(preview_user_id)) infoMsg += "\n, preview_user_id:'"+preview_user_id+"'";
			// 옵션정보관련
			if(2 < user_auths.length()) infoMsg += "\n, user_auths:'"+user_auths+"'";
			if(2 < user_proc_auths.length()) infoMsg += "\n, user_proc_auths:'"+user_proc_auths+"'";
			if(2 < bbm_opts.length()) infoMsg += "\n, bbm_opts:'"+bbm_opts+"'";
			if(2 < bbm_user_auths.length()) infoMsg += "\n, bbm_user_auths:'"+bbm_user_auths+"'";
			infoMsg += "\n}";
			logger.info(">>>>> \n $$$$$$$$$$$ [BBS_ACCESS_INFO] \n"+infoMsg);
		}
		
		/*******************************************
		 * serviceCmd 체크
		 *******************************************/
		
		BbsServiceCmdConst bbsServiceCmdConst = new BbsServiceCmdConst();
		Class cls = bbsServiceCmdConst.getClass();
		Field[] field = cls.getFields();
		boolean isServiceCmd = false;
		for(int i=0; i<field.length; i++){
			if(field[i].getName().equals(serviceCmd)){
				isServiceCmd = true;
				break;
			}
		}
		if(!isServiceCmd){
			String reqeust_uri = EgovStringUtil.isNullToString(paramMap.get("reqeust_uri"));
			throw new NkiaException("'" + reqeust_uri + "' 의 '"+serviceCmd+"' 서비스요청은 지원하지 않습니다 ");
		}
		
		/*******************************************
		 * 체크1 : 기본 데이터 체크
		 *******************************************/
		
		// 사용자 use_yn 체크
		if (!"Y".equals(user_use_yn)) {
			throw new NkiaException("시스템사용가능한 사용자가 아닙니다");
		}
		
		// bbm_id 파라메터 체크
		if ("".equals(bbm_id.trim())) {
			throw new NkiaException("게시판아이디는 필수입력입니다.");
		}
		
		// bbs_no 파라메터 체크
		switch (serviceCmd) {
		case BbsServiceCmdConst.BBS_BASE_GO_BBS_DETAIL:
		case BbsServiceCmdConst.BBS_BASE_SELECT:
		case BbsServiceCmdConst.BBS_BASE_UPDATE:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_READ_COUNT:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_CONFIRM:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_DELETE:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_DEL_CANCEL:
		case BbsServiceCmdConst.BBS_BASE_DELETE:
		case BbsServiceCmdConst.BBS_BASE_SAVE:
		case BbsServiceCmdConst.BBS_COMNT_INSERT:
		case BbsServiceCmdConst.BBS_COMNT_UPDATE:
		case BbsServiceCmdConst.BBS_COMNT_DELETE:
			if ("".equals(bbs_no.trim())) {
				throw new NkiaException("글 번호는 필수입력입니다.");
			}
			break;
			
		default:
			;
		}
		
		// title 파라메터 체크
		switch (serviceCmd) {
		case BbsServiceCmdConst.BBS_BASE_INSERT:
		case BbsServiceCmdConst.BBS_BASE_INSERT_REPLY:
		case BbsServiceCmdConst.BBS_BASE_UPDATE:
		case BbsServiceCmdConst.BBS_COMNT_INSERT:
		case BbsServiceCmdConst.BBS_COMNT_UPDATE:
			if ("".equals(title.trim())) {
				throw new NkiaException("제목을 입력하지 않았습니다.");
			}
			break;
			
		default:
			;
		}
		
		// up_bbs_no 파라메터 체크
		switch (serviceCmd) {
		case BbsServiceCmdConst.BBS_BASE_INSERT_REPLY:
			if ("".equals(up_bbs_no.trim())) {
				throw new NkiaException("상위 글번호 정보가 유효하지 않습니다.");
			}
			break;
			
		default:
			;
		}
		
		// login_user_id 체크
		if ("".equals(EgovStringUtil.isNullToString(paramMap.get("login_user_id")))) {
			throw new NkiaException("로그인사용자정보가 존재하지 않습니다.");
		}
		
		if (null == bbmDetail) {
			throw new NkiaException("게시판정보가 존재하지 않습니다.");
		}
		
		// bbsDetail 체크
		switch (serviceCmd) {
		case BbsServiceCmdConst.BBS_BASE_GO_BBS_DETAIL:
		case BbsServiceCmdConst.BBS_BASE_SELECT:
		case BbsServiceCmdConst.BBS_BASE_UPDATE:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_READ_COUNT:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_CONFIRM:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_DELETE:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_DEL_CANCEL:
		case BbsServiceCmdConst.BBS_BASE_DELETE:
		case BbsServiceCmdConst.BBS_BASE_SAVE:
		case BbsServiceCmdConst.BBS_COMNT_INSERT:
		case BbsServiceCmdConst.BBS_COMNT_UPDATE:
		case BbsServiceCmdConst.BBS_COMNT_DELETE:
			if (null == bbsDetail) {
				throw new NkiaException("글정보가 존재하지 않습니다.");
			}
			break;
			
		default:
			;
		}
		
		/*******************************************
		 * 체크2 : 필수체크 & 권한체크 
		 *******************************************/
		
		if (!"Y".equals(bbm_use_yn)) {
			throw new NkiaException("게시판이 사용중이지 않습니다");
		}
		
		switch (serviceCmd) {
		case BbsServiceCmdConst.BBS_BASE_INSERT:
		case BbsServiceCmdConst.BBS_BASE_INSERT_REPLY:
			// @@@ 필수값 체크 @@@
			
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_MNG_USER_GRP_YN:Y|")) {
				; // 관리그룹 사용자인 경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_REG_USER_GRP_YN:Y|")) {
				; // 등록 그룹사용자인 경우 가능
			} else {
				throw new NkiaException("글 등록 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_BASE_UPDATE:
		case BbsServiceCmdConst.BBS_BASE_DELETE:
		case BbsServiceCmdConst.BBS_BASE_UPDATE_DELETE:
			// @@@ 필수값 체크 @@@
			if(!"Y".equals(bbs_use_yn)){
				throw new NkiaException("글 정보가 유효하지 않습니다.");
			}
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_MNG_USER_GRP_YN:Y|")) {
				; // 관리그룹 사용자인 경우 가능
			} else if ("Y".equals(bbs_row_ins_user_yn)) {
				; // 본인등록인경우 가능
			} else {
				throw new NkiaException("글 수정 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_BASE_UPDATE_DEL_CANCEL:
			// @@@ 필수값 체크 @@@
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_MNG_USER_GRP_YN:Y|")) {
				; // 관리그룹 사용자인 경우 가능
			} else if ("Y".equals(bbs_row_ins_user_yn)) {
				; // 본인등록인경우 가능
			} else {
				throw new NkiaException("글 삭제취소 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_BASE_UPDATE_CONFIRM:
			// @@@ 필수값 체크 @@@
			if(!"Y".equals(bbs_use_yn)){
				throw new NkiaException("글 정보가 유효하지 않습니다.");
			}
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_CONFIRM_USER_GRP_YN:Y|")) {
				; // 승인 그룹사용자인 경우 가능
			} else {
				throw new NkiaException("글 승인 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_BASE_SAVE:
			// @@@ 필수값 체크 @@@
			String saveCmd = EgovStringUtil.isNullToString(paramMap.get("saveCmd"));
			if("".equals(saveCmd)){
				throw new NkiaException("상세정보 저장구분은 필수입니다.");
			}
			if (null != bbsDetail) {
				if(!"Y".equals(bbs_use_yn)){
					throw new NkiaException("글 정보가 유효하지 않습니다.");
				}
			}
			
			if("baseDetailConfirmAppr".equals(saveCmd) || "baseDetailConfirmCancel".equals(saveCmd)){
				if (!(-1 < bbm_user_auths.indexOf("|USER_CONFIRM_USER_GRP_YN:Y|"))) {
					throw new NkiaException("글 승인 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
				}
			}else{
				throw new NkiaException("저장 구분을 확인해주시기 바랍니다.");
			}
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_MNG_USER_GRP_YN:Y|")) {
				; // 관리그룹 사용자인 경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_REG_USER_GRP_YN:Y|")) {
				; // 등록 그룹사용자인 경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_CONFIRM_USER_GRP_YN:Y|")) {
				; // 승인 그룹사용자인 경우 가능
			} else {
				throw new NkiaException("글 저장 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_BASE_SAVE_LIST:
			// @@@ 필수값 체크 @@@
			String input1_saveCmd = EgovStringUtil.isNullToString(paramMap.get("input1_saveCmd"));
			if("".equals(input1_saveCmd)){
				throw new NkiaException("일괄저장 구분은 필수입니다.");
			}
			if("baseListConfirmAppr".equals(input1_saveCmd) || "baseListConfirmCancel".equals(input1_saveCmd)){
				if (!(-1 < bbm_user_auths.indexOf("|USER_CONFIRM_USER_GRP_YN:Y|"))) {
					throw new NkiaException("글 일괄승인 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
				}
			}else{
				throw new NkiaException("일괄저장 구분을 확인해주시기 바랍니다.");
			}
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_MNG_USER_GRP_YN:Y|")) {
				; // 관리그룹 사용자인 경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_REG_USER_GRP_YN:Y|")) {
				; // 등록 그룹사용자인 경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_CONFIRM_USER_GRP_YN:Y|")) {
				; // 승인 그룹사용자인 경우 가능
			} else {
				throw new NkiaException("글 일괄저장 가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_COMNT_INSERT:
			// @@@ 필수값 체크 @@@
			if(!"Y".equals(bbs_use_yn)){
				throw new NkiaException("글 정보가 유효하지 않습니다.");
			}
			
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_MNG_USER_GRP_YN:Y|")) {
				; // 관리그룹 사용자인 경우 가능
			} else if (-1 < bbm_user_auths.indexOf("|USER_REG_USER_GRP_YN:Y|")) {
				; // 등록 그룹사용자인 경우 가능
			} else {
				throw new NkiaException("댓글 등록가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_COMNT_UPDATE:
		case BbsServiceCmdConst.BBS_COMNT_DELETE:
			// @@@ 필수값 체크 @@@
			if(!"Y".equals(bbs_use_yn)){
				throw new NkiaException("글 정보가 유효하지 않습니다.");
			}
			Map<String, Object>  bbsComntDetail = bbsComntDAO.selectBbsComnt(paramMap);
			if (null == bbsComntDetail) {
				throw new NkiaException("댓글 정보가 존재하지 않습니다.");
			}
			String comnt_ins_user_id = EgovStringUtil.isNullToString(bbsComntDetail.get("INS_USER_ID"));
			String comnt_row_ins_user_yn = EgovStringUtil.isNullToString(bbsComntDetail.get(BbsNameConst.ROW_INS_USER_YN));
			// @@@ 권한 체크 @@@
			if ("Y".equals(login_admin_yn)) {
				; // 어드민인경우 가능
			} else if ("Y".equals(bbm_user_mng_user_grp_yn)) {
				; // 관리그룹 사용자인 경우 가능
			} else if (1 < login_user_id.length() && comnt_ins_user_id.equals(login_user_id)) {
				; // 본인등록인경우 가능
			} else if ("Y".equals(comnt_row_ins_user_yn)) {
				; // 본인등록인경우 가능
			} else {
				throw new NkiaException("댓글 수정가능한 사용자가 아닙니다 \n["+login_user_id+"-"+bbm_id+"-"+bbs_no+"-"+comnt_no+"]");
			}
			break;
			
		case BbsServiceCmdConst.BBS_BASE_GO_BBS_MNG:
		case BbsServiceCmdConst.BBS_BASE_GO_BBS_LIST:
			if(!"Y".equals(bbm_use_yn)){
				throw new NkiaException("사용이 중지된 게시판입니다");
			}
			break;
			
		default:
			;
		}
	}


}
