package com.nkia.itg.system.integratedboard.service;

import java.util.List;
import java.util.Map;

/* *통합게시판* */

public interface BbmService {

	// 게시판
	public Map<String, Object> selectBbm(Map<String, Object> paramMap) throws Exception;
	public int selectBbmCount(Map<String, Object> paramMap) throws Exception;
	public List<Map<String, Object>> selectBbmList(Map<String, Object> paramMap) throws Exception;
	public void insertBbm(Map<String, Object> paramMap) throws Exception;
	public void updateBbm(Map<String, Object> paramMap) throws Exception;
	public void deleteBbm(Map<String, Object> paramMap) throws Exception;
	
	// 세션정보
	public void setLoginInfo(Map<String, Object> paramMap) throws Exception;
	public boolean checkAuth(String propName) throws Exception;
}
