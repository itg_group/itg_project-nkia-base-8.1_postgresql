package com.nkia.itg.system.integratedboard.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.integratedboard.constraint.BbsNameConst;
import com.nkia.itg.system.integratedboard.dao.BbmDAO;
import com.nkia.itg.system.integratedboard.service.BbmService;

import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.utl.fcc.service.EgovStringUtil;

/* *통합게시판* */

@Service("bbmService")
public class BbmServiceImpl implements BbmService {

	private static final Logger logger = LoggerFactory.getLogger(BbmServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "bbmDAO")
	public BbmDAO bbmDAO;
	
	/**
	 * 로그인정보 설정
	 * 
     * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public void setLoginInfo(Map<String, Object> paramMap) throws Exception {
		String login_user_id = "";
		String login_cust_id = "";
		String login_user_auth = "";
		String login_admin_yn = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		login_user_id = userVO.getUser_id();
    		login_cust_id = userVO.getCust_id();
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			login_user_auth = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					login_user_auth += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
			}
    		//login_admin_yn = (-1 < login_user_auth.indexOf("@SYSTEM_USER@")? "Y": "");
			if(checkAuth("Qna.Auth.List")){
				login_admin_yn = "Y";
			}
    	}
    	
		paramMap.put("login_user_id", login_user_id);
		paramMap.put("login_cust_id", login_cust_id);
		paramMap.put("login_user_auth", login_user_auth);
		paramMap.put("login_admin_yn", login_admin_yn);
		
	}

	public boolean checkAuth(String propName) throws Exception {
		boolean resultFlag = false;
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		// 로그인 사용자의 권한에 따라 수정, 삭제 가능
		List userGrpAuth = userVO.getGrpAuthList();
		String noticeAuthList = EgovStringUtil.isNullToString(NkiaApplicationPropertiesMap.getProperty(propName));
		String[] noticeAuthArr = noticeAuthList.split(",");
		
		for(int i=0; i < noticeAuthArr.length; i++){
			String authStr = noticeAuthArr[i];
			if(authStr != null && !authStr.isEmpty()){
				authStr = authStr.toUpperCase();
				
				for(int j=0; j<userGrpAuth.size(); j++){
					Map userGrpAuthMap =(HashMap)userGrpAuth.get(j);
					String userAuth = (String) userGrpAuthMap.get("USER_GRP_ID");
					if(userAuth != null && !userAuth.isEmpty()){
						userAuth = userAuth.toUpperCase();
						if(userAuth != null && authStr.equals(userAuth)){
							resultFlag = true;
							return resultFlag;
						}
					}
				}
			}
		}
		
		return resultFlag;
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectBbm(Map<String, Object> paramMap) throws Exception {
		setLoginInfo(paramMap); // 로그인정보 설정
		Map<String, Object> boardInfo = bbmDAO.selectBbm(paramMap);
		return boardInfo;
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public int selectBbmCount(Map<String, Object> paramMap) throws Exception {
		setLoginInfo(paramMap); // 로그인정보 설정
		return bbmDAO.selectBbmCount(paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectBbmList(Map<String, Object> paramMap) throws Exception {
		setLoginInfo(paramMap); // 로그인정보 설정
		return bbmDAO.selectBbmList(paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void insertBbm(Map<String, Object> paramMap) throws Exception {
		
		Map<String, Object> mPs = new HashMap<String, Object>();
		String bbm_uid = StringUtil.replaceNull(String.valueOf(paramMap.get("bbm_uid")), "");
		if(!"".equals(bbm_uid)){
			mPs.put("bbm_uid", bbm_uid);
			Map<String, Object> bbmUidDetail = selectBbm(mPs);
			if (null != bbmUidDetail) {
				throw new NkiaException("게시판 고유ID가 이미 존재합니다.");
			}
		}
		setLoginInfo(paramMap); // 로그인정보 설정
		String bbm_id = bbmDAO.selectBbmNewId(paramMap);
		paramMap.put("bbm_id", bbm_id);
		bbmDAO.insertBbm(paramMap);
		
		createTableBbs(bbm_id);
	}
	
	/**
	 * BBS 테이블 생성 {BBS테이블, BBS시쿼스, BBS코멘트테이블}
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	private void createTableBbs(String bbm_id) throws Exception {
		Map<String, Object> mPs = new HashMap<String, Object>();
		mPs.put("bbm_id", bbm_id);
		Map<String, Object> bbmDetail = selectBbm(mPs);
		if (null == bbmDetail) {
			throw new NkiaException("게시판정보가 존재하지 않습니다.");
		}
    	if("Y".equals(bbmDetail.get("TBL_CRE_YN"))){
    		mPs.put(BbsNameConst.BBM_DETAIL, bbmDetail);
    		mPs.put("DB_MAIN_USER", EgovProperties.getProperty("db.main.user").toUpperCase());
    		
    		bbmDAO.createTableBbs(mPs); // BBS 테이블 생성
    		if("Y".equals(bbmDetail.get("REPLY_REG_YN"))){
    			//bbmDAO.createIndexBbs01(mPs); // BBS 인덱스 생성 (답변게시판용??)
    		}
    		bbmDAO.createSequenceBbs(mPs); // BBS 시퀀스 생성
    		if("Y".equals(bbmDetail.get("COMNT_REG_YN"))){
    			bbmDAO.createTableBbsComnt(mPs); // BBS COMNT 테이블 생성 (보류함)
    		}
    	}
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void updateBbm(Map<String, Object> paramMap) throws Exception {
		setLoginInfo(paramMap); // 로그인정보 설정
		bbmDAO.updateBbm(paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbm(Map<String, Object> paramMap) throws Exception {
		
		Map<String, Object> bbmDetail = bbmDAO.selectBbm(paramMap); // 상세정보 조회
		if (null == bbmDetail) {
			throw new NkiaException("게시판정보가 존재하지 않습니다.");
		}
		
		setLoginInfo(paramMap); // 로그인정보 설정
		bbmDAO.deleteBbm(paramMap);
		
		if( bbmDetail.containsKey("ATCH_FILE_ID") && bbmDetail.get("ATCH_FILE_ID") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)bbmDetail.get("ATCH_FILE_ID"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
}
