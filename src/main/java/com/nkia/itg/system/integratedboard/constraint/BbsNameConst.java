package com.nkia.itg.system.integratedboard.constraint;

import java.io.Serializable;

/* *통합게시판* */
public class BbsNameConst implements Serializable {

	private static final long serialVersionUID = 7647050674233837645L;

	// 항목명
	public static final String USER_DETAIL = "userDetail";
	public static final String USER_AUTH = "userAuth";
	public static final String USER_PROC_AUTH = "userProcAuth";
	public static final String BBM_DETAIL = "bbmDetail";
	public static final String BBM_USER_AUTH = "bbmUserAuth";
	public static final String BBS_DETAIL = "bbsDetail";
	public static final String ROW_INS_USER_YN = "ROW_INS_USER_YN";
	
}