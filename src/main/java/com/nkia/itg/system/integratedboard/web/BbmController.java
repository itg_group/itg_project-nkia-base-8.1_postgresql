package com.nkia.itg.system.integratedboard.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.integratedboard.service.BbmService;

import egovframework.com.cmm.EgovMessageSource;

/* *통합게시판* */

@Controller
public class BbmController {

	@Resource(name = "bbmService")
	private BbmService bbmService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 
	 * 게시판 관리
	 * 
	 * @param paramMap
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping(value = "/itg/system/integratedboard/goBbmMng.do", method=RequestMethod.GET)
	public String goBbm(HttpServletRequest request, @RequestParam Map<String, Object> reqMap, ModelMap paramMap) throws Exception {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		paramMap.put("reg_user_grp", NkiaApplicationPropertiesMap.getProperty("bbs.reg.grp.id"));
		paramMap.put("confirm_user_grp", NkiaApplicationPropertiesMap.getProperty("bbs.confirm.grp.id"));
		paramMap.put("mng_user_grp", NkiaApplicationPropertiesMap.getProperty("bbs.mng.grp.id"));
		paramMap.put("bbs_category", NkiaApplicationPropertiesMap.getProperty("bbs.category.id"));
		
		String forwarPage = "/itg/system/integratedboard/bbmMng.nvf";
		bbmService.setLoginInfo(paramMap); // 로그인정보 설정
		return forwarPage;
	}
	
	/**
	 * 게시판관리 - 목록
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbm.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbm(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			Map<String, Object> resultMap = bbmService.selectBbm(paramMap);
			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 게시판 글목록 리스트
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbmList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbmList(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);

			List<Map<String, Object>> resultList = bbmService.selectBbmList(paramMap);
			totalCount = bbmService.selectBbmCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);

			resultVO.setGridVO(gridVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 게시판 글목록 엑셀다운로드
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbmExcelDown.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbmExcelDown(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map<String, Object> excelParam = (Map<String, Object>) paramMap.get("excelParam");
			Map<String, Object> param = (Map<String, Object>) paramMap.get("param");
			List<Map<String, Object>> resultList = bbmService.selectBbmList(paramMap);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 글 등록
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/insertBbm.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertBbm(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbmService.insertBbm(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001")); // 저장되었습니다.
		} catch (NkiaException e) {
			//e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			//e.printStackTrace();
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 글 수정
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/updateBbm.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateBbm(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbmService.updateBbm(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002")); // 수정되었습니다.
		} catch (NkiaException e) {
			//e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			//e.printStackTrace();
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 글 삭제
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/deleteBbm.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deleteBbm(@RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			paramMap.put("use_yn", "N");
			bbmService.updateBbm(paramMap);
			//bbmService.deleteBbm(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003")); // 삭제되었습니다.
		} catch (NkiaException e) {
			//e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			//e.printStackTrace();
			throw new NkiaException(e);
		}
		return resultVO;
	}

}
