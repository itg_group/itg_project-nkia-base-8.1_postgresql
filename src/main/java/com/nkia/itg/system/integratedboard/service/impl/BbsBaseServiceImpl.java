package com.nkia.itg.system.integratedboard.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.integratedboard.constraint.BbsNameConst;
import com.nkia.itg.system.integratedboard.dao.BbsBaseDAO;
import com.nkia.itg.system.integratedboard.service.BbsBaseService;

import egovframework.com.utl.fcc.service.EgovStringUtil;

/* *통합게시판* */

@Service("bbsBaseService")
public class BbsBaseServiceImpl implements BbsBaseService {

	@Resource(name = "bbsBaseDAO")
	public BbsBaseDAO bbsBaseDAO;
	
	/**
	 * 게시글 상세 조회
	 * 
	 * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectBbs(Map<String, Object> paramMap) throws Exception {
		return bbsBaseDAO.selectBbs(paramMap);
	}

	/**
	 * 게시글 글목록 개수
	 * 
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public int selectBbsCount(Map<String, Object> paramMap) throws Exception {
		return bbsBaseDAO.selectBbsCount(paramMap);
	}

	/**
	 * 글목록 리스트
	 * 
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectBbsList(Map<String, Object> paramMap) throws Exception {
		return bbsBaseDAO.selectBbsList(paramMap);
	}
	
	/**
	 * 글 등록
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void insertBbs(Map<String, Object> paramMap) throws Exception {
		String bbs_no = bbsBaseDAO.selectBbsNewId(paramMap);
		paramMap.put("bbs_no", bbs_no);
		bbsBaseDAO.insertBbs(paramMap);
	}
	
	/**
	 * 답글 등록
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void insertBbsReply(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> mPs = new HashMap<String, Object>();
		mPs.put("up_bbs_no", paramMap.get("up_bbs_no"));
		bbsBaseDAO.updateBbsReplySeqAdd(paramMap); // 상위 BBS 아래위치 목록 시쿼스 + 1 시킨다.

		String bbs_no = bbsBaseDAO.selectBbsNewId(paramMap);
		paramMap.put("bbs_no", bbs_no);
		bbsBaseDAO.insertBbs(paramMap);
	}
	
	/**
	 * 글 수정
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void updateBbs(Map<String, Object> paramMap) throws Exception {
		bbsBaseDAO.updateBbs(paramMap);
	}

	/**
	 * 글 읽음 수 증가
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsReadCountAdd(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> bbsDetail = (Map<String, Object>)paramMap.get(BbsNameConst.BBS_DETAIL);
		if(null == bbsDetail){
			throw new NkiaException("게시글 정보를 조회하지 못했습니다");
		}
		String login_user_id = EgovStringUtil.isNullToString(paramMap.get("login_user_id"));
		if (!login_user_id.equals(bbsDetail.get("INS_USER_ID"))) {
			bbsBaseDAO.updateBbsReadCountAdd(paramMap);
		}
	}

	/**
	 * 글 삭제
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void deleteBbs(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> bbmDetail = (Map<String, Object>)paramMap.get(BbsNameConst.BBM_DETAIL);
		if(null == bbmDetail){
			throw new NkiaException("게시판 정보를 조회하지 못했습니다");
		}
		if ("Y".equals(bbmDetail.get("REPLY_REG_YN"))) { // 답글 삭제
			String req_options = EgovStringUtil.isNullToString(paramMap.get("req_options"));
			if(!(-1 < req_options.indexOf("|REPLY_CHILD_REMOVE_NOCHECK_YN:Y|"))){
				int childCnt = bbsBaseDAO.selectBbsChildReplyCount(paramMap);
				if(0 < childCnt){
					throw new NkiaException("삭제안된 답글정보가 존재하여 삭제를 할 수 없습니다  [childCnt : "+(childCnt)+"]");
				}
			}
			bbsBaseDAO.deleteBbsChildReply(paramMap);
		}
		bbsBaseDAO.deleteBbs(paramMap); // 게시글 삭제(Update) (USE_YN='Y)
	}

	/**
	 * 글 삭제
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsDelete(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> bbmDetail = (Map<String, Object>)paramMap.get(BbsNameConst.BBM_DETAIL);
		if(null == bbmDetail){
			throw new NkiaException("게시판 정보를 조회하지 못했습니다");
		}
		if ("Y".equals(bbmDetail.get("REPLY_REG_YN"))) { // 답글 삭제
			String req_options = EgovStringUtil.isNullToString(paramMap.get("req_options"));
			if(!(-1 < req_options.indexOf("|REPLY_CHILD_REMOVE_NOCHECK_YN:Y|"))){
				int childCnt = bbsBaseDAO.selectBbsChildReplyCount(paramMap);
				if(0 < childCnt){
					throw new NkiaException("삭제안된 답글정보가 존재하여 삭제를 할 수 없습니다  [childCnt : "+(childCnt)+"]");
				}
			}
			bbsBaseDAO.updateBbsChildReplyDelete(paramMap);
		}
		bbsBaseDAO.updateBbsDelete(paramMap); // 게시글 삭제(Update) (USE_YN='Y)
	}
	
	/**
	 * 글 삭제취소
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void updateBbsDelCancel(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> bbmDetail = (Map<String, Object>)paramMap.get(BbsNameConst.BBM_DETAIL);
		if(null == bbmDetail){
			throw new NkiaException("게시판 정보를 조회하지 못했습니다");
		}
		Map<String, Object> bbsDetail = (Map<String, Object>)paramMap.get(BbsNameConst.BBS_DETAIL);
		if(null == bbsDetail){
			throw new NkiaException("글 정보를 조회하지 못했습니다");
		}
		if ("Y".equals(bbmDetail.get("REPLY_REG_YN"))) { // 답글유형인경우
			if (!(bbsDetail.get("TOP_BBS_NO").equals(bbsDetail.get("BBS_NO")))) { // 메인글이 아닌경우
				if (!("Y".equals(bbsDetail.get("TOP_BBS_USE_YN")))) {
					throw new NkiaException("기준글이 삭제되어 삭제취소할 수 없습니다");
				}else if (!("Y".equals(bbsDetail.get("UP_BBS_USE_YN")))) {
					throw new NkiaException("상위글이 삭제되어 삭제취소할 수 없습니다");
				}
			}
		}
		bbsBaseDAO.updateBbsDelCancel(paramMap); // 게시글 삭제(Update) (USE_YN='Y)
	}

	/**
	 * 게시글 저장
	 * 
	 * @param paramMap
	 * @throws Exception
	 */
	public void saveBbs(Map<String, Object> paramMap) throws Exception {
		
		/****************
		 * 저장구분값 체크
		 ***************/
		String saveCmd = EgovStringUtil.isNullToString(paramMap.get("saveCmd"));
		switch (saveCmd) {
		case "baseDetailConfirmAppr":
		case "baseDetailConfirmCancel":
			break;
		default:
			throw new NkiaException("저장 요청구분정보가 잘못되었습니다.");
		}
		
		/****************
		 * 로직처리
		 ***************/
		if("baseDetailConfirmAppr".equals(saveCmd) || "baseDetailConfirmCancel".equals(saveCmd)){
			bbsBaseDAO.updateBbsOpen(paramMap);
		}
	}

	/**
	 * 게시글 목록저장
	 * @param paramMap
	 */
	public void saveBbsList(Map paramMap) throws Exception {
		
		/****************
		 * 저장구분값 체크
		 ***************/
		String saveCmd = EgovStringUtil.isNullToString(paramMap.get("input1_saveCmd"));
		switch (saveCmd) {
		case "baseListConfirmAppr":
		case "baseListConfirmCancel":
			break;
		default:
			throw new NkiaException("저장 요청구분정보가 잘못되었습니다.");
		}

		/****************
		 * 로직처리
		 ***************/
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("input1_list");
		if("baseListConfirmAppr".equals(saveCmd) || "baseListConfirmCancel".equals(saveCmd)){
			for(int i=0; i<list.size(); i++){
				Map<String, Object> map = list.get(i);
				map.put(BbsNameConst.BBM_DETAIL, paramMap.get(BbsNameConst.BBM_DETAIL));
				map.put("login_user_id", EgovStringUtil.isNullToString(paramMap.get("login_user_id")));
				bbsBaseDAO.updateBbsOpen(map);
			}
		}
	}

	@Override
	public void updateSubBbsCategory(ModelMap paramMap) throws Exception {
		bbsBaseDAO.updateSubBbsCategory(paramMap);
	}
	
}
