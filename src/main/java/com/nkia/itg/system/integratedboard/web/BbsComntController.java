package com.nkia.itg.system.integratedboard.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.integratedboard.constraint.BbsServiceCmdConst;
import com.nkia.itg.system.integratedboard.service.BbsBaseCheckService;
import com.nkia.itg.system.integratedboard.service.BbsBaseInitService;
import com.nkia.itg.system.integratedboard.service.BbsComntService;

import egovframework.com.cmm.EgovMessageSource;

/* *통합게시판* */

@Controller
public class BbsComntController {

	@Resource(name = "bbsComntService")
	private BbsComntService bbsComntService;

	@Resource(name = "bbsBaseInitService")
	private BbsBaseInitService bbsBaseInitService;
	
	@Resource(name = "bbsBaseCheckService")
	private BbsBaseCheckService bbsBaseCheckService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;

	/**
	 * 
	 * 코멘트 목록화면
	 * 
	 * @param paramMap
	 * @return String
	 * @throws Exception
	 */
	//@RequestMapping(value = "/itg/system/integratedboard/goBbsComntList.do")
	//public String goBbsMng(HttpServletRequest request, @RequestParam Map<String, Object> reqMap, ModelMap paramMap) throws Exception {
	//	String forwarPage = "/itg/system/integratedboard/bbsComntList.nvf";
	//	if (!urlAccessCheckService.urlAuthCheck(request)) {
	//		return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
	//	}
	//	return forwarPage;
	//}
	
	/**
	 * 코멘트 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbsComnt.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbsComnt(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_COMNT_SELECT, paramMap);
			
			Map<String, Object> resultMap = bbsComntService.selectBbsComnt(paramMap);
			resultVO.setResultMap(resultMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg((resultMap != null? "댓글 조회되었습니다": "댓글 조회된정보가 없습니다"));
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 코멘트 리스트
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbsComntList.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbsComntList(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_COMNT_SELECT_LIST, paramMap);
			
			GridVO gridVO = new GridVO();
			PagingUtil.getFristEndNum(paramMap);

			int totalCount = bbsComntService.selectBbsComntCount(paramMap);
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>(); 
			if(0 < totalCount){
				resultList = bbsComntService.selectBbsComntList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg((0 < resultList.size()? " 댓글목록 조회되었습니다": "댓글목록 조회된정보가 없습니다"));
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 코멘트목록 엑셀다운로드
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/selectBbsComntExcelDown.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO selectBbsExcelDown(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map<String, Object> excelParam = (Map<String, Object>) paramMap.get("excelParam");
			Map<String, Object> param = (Map<String, Object>) paramMap.get("param");
			
			bbsBaseInitService.setInitData(request, param);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_COMNT_SELECT_EXCELDOWNLOAD, param);
			
			//int totalCount = bbsComntService.selectBbsComntCount(param);
			//if(50000 < totalCount){
			//	throw new NkiaException("엑셀다운로드는 5만건 초과할수 없습니다 다시요청해주시기 바랍니다 [조회된건수:"+totalCount+"]");
			//}
			List resultList = bbsComntService.selectBbsComntList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("엑셀다운로드 되었습니다"); // 조회되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("글 " + messageSource.getMessage("msg.common.error.00000")); // 조회시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 코멘트 등록
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/insertBbsComnt.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO insertBbsComnt(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_COMNT_INSERT, paramMap);
			
			bbsComntService.insertBbsComnt(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.result.00001")); // 저장되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.error.00001")); // 저장시 오류가 발생하였습니다.
		}
		return resultVO;
	}

	/**
	 * 코맨트 수정
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/updateBbsComnt.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO updateBbsComnt(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_COMNT_UPDATE, paramMap);
			
			bbsComntService.updateBbsComnt(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.result.00002")); // 수정되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.error.00002")); // 수정시 오류가 발생하였습니다.
		}
		return resultVO;
	}
	
	/**
	 * 코멘트 삭제
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/integratedboard/deleteBbsComnt.do", method=RequestMethod.POST)
	public @ResponseBody ResultVO deleteBbsComnt(HttpServletRequest request, @RequestBody ModelMap paramMap) throws Exception {
		ResultVO resultVO = new ResultVO();
		try {
			bbsBaseInitService.setInitData(request, paramMap);
			bbsBaseCheckService.checkBbs(BbsServiceCmdConst.BBS_COMNT_DELETE, paramMap);
			
			bbsComntService.deleteBbsComnt(paramMap);
			
			resultVO.setSuccess(true);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.result.00003")); // 삭제되었습니다.
		} catch (NkiaException e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			resultVO.setSuccess(false);
			resultVO.setResultMsg("댓글 " + messageSource.getMessage("msg.common.error.00003")); // 삭제시 오류가 발생하였습니다.
		}
		return resultVO;
	}
}
