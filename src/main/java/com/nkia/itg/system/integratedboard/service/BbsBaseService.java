package com.nkia.itg.system.integratedboard.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

/* *통합게시판* */

public interface BbsBaseService {

	// 게시글  기본
	public Map<String, Object> selectBbs(Map<String, Object> paramMap) throws Exception;
	public int selectBbsCount(Map<String, Object> paramMap) throws Exception;
	public List<Map<String, Object>> selectBbsList(Map<String, Object> paramMap) throws Exception;
	public void insertBbs(Map<String, Object> paramMap) throws Exception; 
	public void insertBbsReply(Map<String, Object> paramMap) throws Exception; 
	public void updateBbs(Map<String, Object> paramMap) throws Exception; 
	public void updateBbsReadCountAdd(Map<String, Object> paramMap) throws Exception; 
	public void deleteBbs(Map<String, Object> paramMap) throws Exception;
	public void updateBbsDelete(Map<String, Object> paramMap) throws Exception;
	public void updateBbsDelCancel(Map<String, Object> paramMap) throws Exception;
	public void saveBbs(Map<String, Object> paramMap) throws Exception;
	public void saveBbsList(Map<String, Object> paramMap) throws Exception;
	public void updateSubBbsCategory(ModelMap paramMap) throws Exception;
}
