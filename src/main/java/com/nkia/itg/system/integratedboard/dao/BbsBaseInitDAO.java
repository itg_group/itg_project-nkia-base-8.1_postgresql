package com.nkia.itg.system.integratedboard.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/* *통합게시판* */

@Repository("bbsBaseInitDAO")
public class BbsBaseInitDAO extends EgovComAbstractDAO {

	/**
	 * 사용자 정보 조회
	 * 
	 * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectInitUser(Map<String, Object> paramMap) throws Exception {
		return (Map<String, Object>) this.selectByPk("BbsBaseInitDAO.selectInitUser", paramMap);
	}

	/**
	 * 사용자 권한정보
	 * 
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectInitUserAuthList(Map<String, Object> paramMap) throws Exception {
		List<Map<String, Object>> resultList =  (List<Map<String, Object>>)this.list("BbsBaseInitDAO.selectInitUserAuthList", paramMap);
		return resultList;
	}
	
	/**
	 * 사용자 프로세스 권한정보
	 * 
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectInitUserProcAuthList(Map<String, Object> paramMap) throws Exception {
		List<Map<String, Object>> resultList =  (List<Map<String, Object>>)this.list("BbsBaseInitDAO.selectInitUserProcAuthList", paramMap);
		return resultList;
	}
	
	/**
	 * 게시판 정보 조회
	 * 
	 * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectInitBbm(Map<String, Object> paramMap) throws Exception {
		return (Map<String, Object>) this.selectByPk("BbsBaseInitDAO.selectInitBbm", paramMap);
	}
	
	/**
	 * 게시판 권한정보 목록조회
	 * 
	 * @param paramMap
	 * @return List
	 * @throws Exception
	 */
	public List<Map<String, Object>> selectInitBbmUserAuthList(Map<String, Object> paramMap) throws Exception {
		List<Map<String, Object>> resultList =  (List<Map<String, Object>>)this.list("BbsBaseInitDAO.selectInitBbmUserAuthList", paramMap);
		return resultList;
	}
	
	/**
	 * 게시글 정보 조회
	 * 
	 * @param paramMap
	 * @return Map
	 * @throws Exception
	 */
	public Map<String, Object> selectInitBbs(Map<String, Object> paramMap) throws Exception {
		Map<String, Object> resultMap = (Map<String, Object>) this.selectByPk("BbsBaseInitDAO.selectInitBbs", paramMap);
		return resultMap;
	}

}
