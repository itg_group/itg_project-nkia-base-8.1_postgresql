package com.nkia.itg.system.excuteSql.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.excuteSql.service.ExcuteSqlService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class ExcuteSqlController {
	
	@Resource(name = "excuteSqlService")
	private ExcuteSqlService excuteSqlService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
		
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 사용자 정의 쿼리 페이지 이동
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/excuteSql/goExcuteSql.do")
	public String goViewManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/excuteSql/excuteSqlManager.nvf";
		return forwarPage;
	}
	
	/**
	 * SQL 실행
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/excuteSql/excuteCustomSql.do")
	public @ResponseBody ResultVO excuteCustomSql(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			int resultCount = excuteSqlService.excuteCustomSql(paramMap);
			resultVO.setResultInt(resultCount);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/excuteSql/selectCustomSql.do")
	public @ResponseBody ResultVO selectCustomSql(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			List resultList = excuteSqlService.selectCustomSql(paramMap);
			totalCount = excuteSqlService.selectCustomSqlCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 리스트 조회 엑셀다운로드
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/system/excuteSql/excuteCustomSqlExcelDown.do")
	public @ResponseBody ResultVO excuteCustomSqlExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException{
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			ModelMap excelMap = new ModelMap();
			
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			paramMap.putAll(param);
			
			resultList.add(excuteSqlService.selectCustomSql(paramMap));
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

}
