package com.nkia.itg.system.excuteSql.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.excuteSql.dao.ExcuteSqlDAO;
import com.nkia.itg.system.excuteSql.service.ExcuteSqlService;

@Service("excuteSqlService")
public class ExcuteSqlServiceImpl implements ExcuteSqlService {
	
	private static final Logger logger = LoggerFactory.getLogger(ExcuteSqlServiceImpl.class);
	
	@Resource(name = "excuteSqlDAO")
	public ExcuteSqlDAO excuteSqlDAO;
	
	public List selectCustomSql(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		List newResultList = null;
		resultList = excuteSqlDAO.selectCustomSql(paramMap); 
		
		if(resultList != null && resultList.size() > 0) {
			
			newResultList = new ArrayList();
			
			// 제일 첫 열에는 헤더명 세팅
			LinkedHashMap rowByKey = new LinkedHashMap();
			HashMap newRowByKey = new HashMap();
			
			rowByKey = (LinkedHashMap) resultList.get(0);
			
			Iterator<String> keySetByKey = rowByKey.keySet().iterator();
			int keyCnt = 1;
			while(keySetByKey.hasNext()) {
				String mapKey = keySetByKey.next();
				newRowByKey.put("DATA_"+keyCnt, "<font color='blue'><b>[ "+mapKey+" ]</b></font>");
				keyCnt++;
			}
			
			newResultList.add(newRowByKey);
			
			// 실제 데이터 세팅
			for(int i=0; i<resultList.size(); i++) {
				
				LinkedHashMap row = new LinkedHashMap();
				HashMap newRow = new HashMap();
				
				row = (LinkedHashMap)resultList.get(i);
				
				Iterator<String> keySet = row.keySet().iterator();
				int innerKeyCnt = 1;
				while(keySet.hasNext()) {
					String mapKey = keySet.next();
					newRow.put("DATA_"+innerKeyCnt, row.get(mapKey));
					innerKeyCnt++;
				}
				
				newResultList.add(newRow);
			}
		}
		return newResultList;
	}
	
	public int selectCustomSqlCount(ModelMap paramMap) throws NkiaException {
		return excuteSqlDAO.selectCustomSqlCount(paramMap);
	}
	
	public int excuteCustomSql(ModelMap paramMap) throws NkiaException {
		
		HashMap newParamMap = null;
		
		String sql = (String)paramMap.get("excute_sql");
		String upperSql = sql.toUpperCase();
		String[] sqlArr = sql.split(";");
		
		int resultSqlCount = 0;
		
		// 생성 명령어의 경우 쿼리내에 세미콜론(;)이 있을 수 있으므로
		if(upperSql.indexOf("CREATE ") > -1 || upperSql.indexOf("REPLACE ") > -1) {
			// 단일 건 쿼리실행
			newParamMap = new HashMap();
			newParamMap.put("excute_sql", sql);
			
			excuteSqlDAO.excuteCustomSql(newParamMap);
			resultSqlCount = 1;
		} else {
			if(sqlArr.length > 1) {
				// 다중 건 쿼리실행
				for(int i=0; i<sqlArr.length; i++) {
					String newSql = sqlArr[i];
					
					newParamMap = new HashMap();
					newParamMap.put("excute_sql", newSql);
					
					excuteSqlDAO.excuteCustomSql(newParamMap);
				}
				resultSqlCount = sqlArr.length;
			} else {
				// 단일 건 쿼리실행
				newParamMap = new HashMap();
				newParamMap.put("excute_sql", sql);
				
				resultSqlCount = excuteSqlDAO.excuteCustomSql(newParamMap);
			}
		}
		
		return resultSqlCount;
	}
	
}
