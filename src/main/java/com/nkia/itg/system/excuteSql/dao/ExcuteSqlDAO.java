package com.nkia.itg.system.excuteSql.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("excuteSqlDAO")
public class ExcuteSqlDAO extends EgovComAbstractDAO {
	
	public List selectCustomSql(ModelMap paramMap) throws NkiaException {
		return list("ExcuteSqlDAO.selectCustomSql", paramMap);
	}
	
	public int selectCustomSqlCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer) this.selectByPk("ExcuteSqlDAO.selectCustomSqlCount", paramMap);
		return count;
	}
	
	public int excuteCustomSql(HashMap paramMap)throws NkiaException{
		return update("ExcuteSqlDAO.excuteCustomSql", paramMap);
	}

}
