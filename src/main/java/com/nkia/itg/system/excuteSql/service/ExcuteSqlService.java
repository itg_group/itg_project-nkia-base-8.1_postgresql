package com.nkia.itg.system.excuteSql.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ExcuteSqlService {
	
	public List selectCustomSql(ModelMap paramMap) throws NkiaException;
	
	public int selectCustomSqlCount(ModelMap paramMap) throws NkiaException;
	
	public int excuteCustomSql(ModelMap paramMap) throws NkiaException;
	
}
