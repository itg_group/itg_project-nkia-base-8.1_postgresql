package com.nkia.itg.system.amViewCreator.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.system.amViewCreator.dao.AmViewCreatorDAO;
import com.nkia.itg.system.amViewCreator.service.AmViewCreatorService;

@Service("amViewCreatorService")
public class AmViewCreatorServiceImpl implements AmViewCreatorService {
	
	@Resource(name = "AmViewCreatorDAO")
	public AmViewCreatorDAO amViewCreatorDAO;
	
	/**
	 * 자산 뷰 스크립트 생성
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List selectAmViewColumns(HashMap paramMap) throws NkiaException {
		
		List resultList = new ArrayList();
		LinkedHashMap resultMap = new LinkedHashMap();
		
		String excuteSql = "";
		String checkTableName = "";
		String checkTableAlias = "";
		
		final String INNER_SQL = 
			"SELECT"
				+ " DECODE("
				+ "		B.SYS_CODE,"
				+ "		NULL,"
				+ "		DECODE("
				+ "			B.HTML_TYPE,"
				+ "			'POPUP',"
				+ "			'@ALIAS@.'||A.COL_ID||',@LINE_CHANGE@'||(SELECT REPLACE(D.POPUP_NM_FN, '{0}', '@ALIAS@.'||A.COL_ID) FROM AM_POPUP_MNG D WHERE D.POPUP_CODE = B.POPUP_CODE)||' AS '||A.COL_ID||'_NM,',"
				+ "			'DATE',"
				+ "			'TO_CHAR(@ALIAS@.'||A.COL_ID||',''YYYY-MM-DD'') AS '||A.COL_ID||',',"
				+ "			'@ALIAS@.'||A.COL_ID||','"
				+ "		),"
				+ "		'@ALIAS@.'||A.COL_ID||',@LINE_CHANGE@'||'FC_GET_CODE_NM ('''||B.SYS_CODE||''', @ALIAS@.'||A.COL_ID||') AS '||A.COL_ID||'_NM,'"
				+ "	) AS SCRIPTS,"
				+ "	A.COL_ID,"
				+ "	B.COL_NAME,"
				+ "	B.HTML_TYPE,"
				+ "	B.SYS_CODE,"
				+ "	B.POPUP_CODE"
			+ " FROM"
				+ " AM_TBL_ATTR_MAPPING A"
				+ ", AM_ATTRIBUTE B"
			+ " WHERE"
				+ " A.GEN_TABLE_NM = '@TABLE_NAME@'"
				+ " AND A.COL_ID = B.COL_ID"
			+ " ORDER BY"
				+ " A.SORT"
		;
		
		// 최대 입력 테이블 수가 6개 이므로 6번 loop
		for(int i=1; i<=6; i++) {
			checkTableName = StringUtil.parseString(paramMap.get("table_name_" + i));
			checkTableAlias = StringUtil.parseString(paramMap.get("table_alias_" + i));
			
			if(checkTableName != null && !checkTableName.equals("") && checkTableAlias != null && !checkTableAlias.equals("")) {
				
				List tableColumnList = new ArrayList();
				
				String tableName = checkTableName;
				String tableAlias = checkTableAlias;
				
				excuteSql = INNER_SQL.replaceAll("@TABLE_NAME@", tableName).replaceAll("@ALIAS@", tableAlias);
				
				HashMap params = new HashMap();
				params.put("sql", excuteSql);
				
				tableColumnList = amViewCreatorDAO.selectAmViewColumns(params);
				
				resultMap.put(tableName, tableColumnList);
				
			} else {
				break;
			}
		}
		
		resultList.add(resultMap);
		
		return resultList;
	}
	
	
}
