package com.nkia.itg.system.amViewCreator.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AmViewCreatorService {
	
	/**
	 * 자산 뷰 스크립트 생성
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public List selectAmViewColumns(HashMap paramMap) throws NkiaException;
	
}
