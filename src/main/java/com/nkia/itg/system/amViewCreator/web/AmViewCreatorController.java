package com.nkia.itg.system.amViewCreator.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.amViewCreator.service.AmViewCreatorService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class AmViewCreatorController {

	@Resource(name = "amViewCreatorService")
	private AmViewCreatorService amViewCreatorService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 자산 뷰 스크립트 생성 페이지 호출
	 * 
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/amViewCreator/goAmViewCreator.do")
	public String searchNoticeBoardPopup(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		return "/itg/system/amViewCreator/amViewCreator.nvf";
	}
	
	/**
	 * 자산 뷰 스크립트 생성
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/amViewCreator/selectAmViewColumns.do")
	public @ResponseBody ResultVO selectAmViewColumns(@RequestBody ModelMap paramMap) throws NkiaException {
		
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = amViewCreatorService.selectAmViewColumns(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
