package com.nkia.itg.system.amViewCreator.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("AmViewCreatorDAO")
public class AmViewCreatorDAO extends EgovComAbstractDAO {
	
	/**
	 * 자산 뷰 스크립트 생성
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List selectAmViewColumns(HashMap paramMap) throws NkiaException {
		return list("AmViewCreatorDAO.selectAmViewColumns", paramMap);
	}
	
}
