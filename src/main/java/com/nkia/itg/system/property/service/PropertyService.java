package com.nkia.itg.system.property.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface PropertyService {

	public List searchPropertyList(Map paramMap) throws NkiaException;
	
	public int insertPropertyCount(Map paramMap) throws NkiaException;
	
	public int insertPropertyUserCount(Map paramMap) throws NkiaException;

	public void insertProperty(Map paramMap) throws NkiaException;
	
	public void updateProperty(Map paramMap) throws NkiaException;
	
	public void deleteProperty(Map paramMap) throws NkiaException;

	public List searchPropertyUserList() throws NkiaException;

	public void insertPropertyUser(Map paramMap) throws NkiaException;

	public void updatePropertyUser(Map paramMap) throws NkiaException;

	public void deletePropertyUser(Map paramMap) throws NkiaException;

	// 트리 내 전체(Root) 추가
	public List searchPropertyUserTreeList(Map paramMap) throws NkiaException;
	
	// 프라퍼티 상세 정보 조회
	public Map selectPropertyInfo(ModelMap paramMap) throws NkiaException;
	
	// 프라퍼티 사용자 팝업의 상세 정보 조회
	public Map selectPropertyUserInfo(ModelMap paramMap) throws NkiaException;

}
