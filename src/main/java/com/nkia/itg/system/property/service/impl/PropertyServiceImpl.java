package com.nkia.itg.system.property.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.system.property.dao.PropertyDAO;
import com.nkia.itg.system.property.service.PropertyService;

@Service("propertyService")
public class PropertyServiceImpl implements PropertyService {

	private static final Logger logger = LoggerFactory.getLogger(PropertyServiceImpl.class);
	
	@Resource(name = "propertyDAO")
	public PropertyDAO propertyDAO;
	
	public List searchPropertyList(Map paramMap) throws NkiaException {
		return propertyDAO.searchPropertyList(paramMap);
	}
	
	public int insertPropertyUserCount(Map paramMap) throws NkiaException {
		return propertyDAO.insertPropertyUserCount(paramMap);
	}
	
	public int insertPropertyCount(Map paramMap) throws NkiaException {
		return propertyDAO.insertPropertyCount(paramMap);
	}

	@Override
	public void insertProperty(Map paramMap) throws NkiaException {
		propertyDAO.insertProperty(paramMap);
		List addPropertyUserList = (List)propertyDAO.searchPropertyUserList();		
		for(int i = 0 ; i < addPropertyUserList.size() ; i++) {
			Map userMap = (Map)addPropertyUserList.get(i);
			String userId = (String)userMap.get("USER_ID");
			paramMap.put("user_id", userId);
			propertyDAO.insertPropertyUserValue(paramMap);
		}
		
		NkiaApplicationPropertiesMap.loadProperty();
	}

	@Override
	public void updateProperty(Map paramMap) throws NkiaException {
		propertyDAO.updateProperty(paramMap);
		propertyDAO.updatePropertyUserValue(paramMap);
		NkiaApplicationPropertiesMap.loadProperty();
	}

	@Override
	public void deleteProperty(Map paramMap) throws NkiaException {
		propertyDAO.deletePropertyUserValue(paramMap);
		propertyDAO.deleteProperty(paramMap);
		NkiaApplicationPropertiesMap.loadProperty();
	}

	@Override
	public List searchPropertyUserList() throws NkiaException {
		return propertyDAO.searchPropertyUserList();
	}

	@Override
	public void insertPropertyUser(Map paramMap) throws NkiaException {
		propertyDAO.insertPropertyUser(paramMap);
		
		Map serachMap = new HashMap();
		serachMap.put("search_user_id", paramMap.get("ref_user_id"));
		List propertyList = propertyDAO.searchPropertyList(serachMap);
		String userId = (String)paramMap.get("user_id");
		for(int i = 0 ; i < propertyList.size() ; i++) {
			Map property = (Map)propertyList.get(i);
			Map insertMap = WebUtil.lowerCaseMapKey(property);
			insertMap.put("user_id", userId);
			propertyDAO.insertPropertyUserValue(insertMap);
		}
	}

	@Override
	public void updatePropertyUser(Map paramMap) throws NkiaException {
		propertyDAO.updatePropertyUser(paramMap);
	}

	@Override
	public void deletePropertyUser(Map paramMap) throws NkiaException {
		propertyDAO.deletePropertyForUserDelete(paramMap);
		propertyDAO.deletePropertyUser(paramMap);
		NkiaApplicationPropertiesMap.loadProperty();
	}

	// 트리 내 전체(Root) 추가
	@Override
	public List searchPropertyUserTreeList(Map paramMap) throws NkiaException {
		return propertyDAO.searchPropertyUserTreeList(paramMap);
	}
	
	// 프라퍼티 상세 정보 조회
	public Map selectPropertyInfo(ModelMap paramMap) throws NkiaException {
		Map propertyInfo = propertyDAO.selectPropertyInfo(paramMap);
		return propertyInfo;
	}
	
	// 프라퍼티 사용자 팝업의 상세 정보 조회
	public Map selectPropertyUserInfo(ModelMap paramMap) throws NkiaException {
		Map popPropertyInfo = propertyDAO.selectPropertyUserInfo(paramMap);
		return popPropertyInfo;
	}
}
