package com.nkia.itg.system.property.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.property.service.PropertyService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class PropertyController {

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "propertyService")
	private PropertyService propertyService;
	
	@RequestMapping(value="/itg/system/property/goPropertyManager.do")
	public String goPropertyManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/property/propertyManager.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/system/property/searchPropertyList.do")
	public @ResponseBody ResultVO searchPropertyList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.setSortColumn(paramMap);
			
			List resultList = propertyService.searchPropertyList(paramMap);		
			
			String temp = (String)paramMap.get("search_user_id");
			
			gridVO.setTotalCount(resultList.size());
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.system.error"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	// 상세 폼 기능으로 인한 추가
	@RequestMapping(value="/itg/system/property/selectPropertyInfo.do")
	public @ResponseBody ResultVO selectPropertyInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) propertyService.selectPropertyInfo(paramMap) );
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.system.error"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/property/insertProperty.do")
	public @ResponseBody ResultVO insertProperty(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		int cnt = 0;
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	cnt = propertyService.insertPropertyCount(paramMap);
	    	if(cnt == 0){
	    		propertyService.insertProperty(paramMap);
	    		resultVO.setSuccess(true);
	    		resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
	    	}else{
	    		resultVO.setSuccess(true);
	    		resultVO.setResultMsg("false");
	    	}
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/property/updateProperty.do")
	public @ResponseBody ResultVO updateProperty(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	
	    	propertyService.updateProperty(paramMap);
	    	resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/property/deleteProperty.do")
	public @ResponseBody ResultVO deleteProperty(@RequestBody ModelMap paramMap) throws NkiaException   {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	
	    	propertyService.deleteProperty(paramMap);
	    	resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	// 메인트리 전체 상위폴더 추가
	@RequestMapping(value="/itg/system/property/searchPropertyUserTreeList.do")
	public @ResponseBody ResultVO searchPropertyUserTreeList(@RequestBody ModelMap paramMap) throws NkiaException   {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = propertyService.searchPropertyUserTreeList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.system.error"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/property/searchPropertyUserList.do")
	public @ResponseBody ResultVO searchPropertyUserList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			//PagingUtil.getFristEndNum(paramMap);
			PagingUtil.setSortColumn(paramMap);
			List resultList = propertyService.searchPropertyUserList();		
			
			//gridVO.setTotalCount(resultList.size());
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.system.error"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	// 팝업 상세 폼
	@RequestMapping(value="/itg/system/property/selectPropertyUserInfo.do")
	public @ResponseBody ResultVO selectPropertyUserInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) propertyService.selectPropertyUserInfo(paramMap) );
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(messageSource.getMessage("msg.system.error"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/property/insertPropertyUser.do")
	public @ResponseBody ResultVO insertPropertyUser(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		int cnt = 0;
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	cnt = propertyService.insertPropertyUserCount(paramMap);
	    	if(cnt == 0){
		    	propertyService.insertPropertyUser(paramMap);
		    	resultVO.setSuccess(true);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
	    	}else{
	    		resultVO.setSuccess(true);
				resultVO.setResultMsg("false");
	    	}
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/property/updatePropertyUser.do")
	public @ResponseBody ResultVO updatePropertyUser(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	
	    	propertyService.updatePropertyUser(paramMap);
	    	resultVO.setSuccess(true);
	    	resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/property/deletePropertyUser.do")
	public @ResponseBody ResultVO deletePropertyUser(@RequestBody ModelMap paramMap) throws NkiaException   {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
	    	
	    	propertyService.deletePropertyUser(paramMap);
	    	resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

}
