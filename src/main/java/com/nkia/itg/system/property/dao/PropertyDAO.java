package com.nkia.itg.system.property.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("propertyDAO")
public class PropertyDAO extends EgovComAbstractDAO {

	public List searchPropertyList(Map paramMap) throws NkiaException {
		return list("PropertyDAO.searchPropertyList", paramMap);
	}
	
	public int insertPropertyCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("PropertyDAO.insertPropertyCount", paramMap);
	}
	
	public int insertPropertyUserCount(Map paramMap) throws NkiaException {
		return (Integer)selectByPk("PropertyDAO.insertPropertyUserCount", paramMap);
	}
	
	public void insertPropertyUserValue(Map paramMap) throws NkiaException {
		insert("PropertyDAO.insertPropertyUserValue", paramMap);
	}
	
	public void updatePropertyUserValue(Map paramMap) throws NkiaException {
		update("PropertyDAO.updatePropertyUserValue", paramMap);
	}	
	
	public void insertProperty(Map paramMap) throws NkiaException {
		insert("PropertyDAO.insertProperty", paramMap);
	}
	
	public void updateProperty(Map paramMap) throws NkiaException {
		update("PropertyDAO.updateProperty", paramMap);
	}
	
	public void deleteProperty(Map paramMap) throws NkiaException {
		delete("PropertyDAO.deleteProperty", paramMap);
	}
	
	public void deletePropertyUserValue(Map paramMap) throws NkiaException {
		delete("PropertyDAO.deletePropertyUserValue", paramMap);
	}

	public List searchPropertyListForApplicationLoad(String propertyUserId) {
		return list("PropertyDAO.searchPropertyListForApplicationLoad", propertyUserId);
	}

	public List searchPropertyUserList() throws NkiaException {
		return list("PropertyDAO.searchPropertyUserList", null);
	}

	public void insertPropertyUser(Map paramMap) throws NkiaException {
		delete("PropertyDAO.insertPropertyUser", paramMap);
	}

	public void updatePropertyUser(Map paramMap) throws NkiaException {
		delete("PropertyDAO.updatePropertyUser", paramMap);
	}

	public void deletePropertyUser(Map paramMap) throws NkiaException {
		delete("PropertyDAO.deletePropertyUser", paramMap);
	}
	
	public void deletePropertyForUserDelete(Map paramMap) throws NkiaException {
		delete("PropertyDAO.deletePropertyForUserDelete", paramMap);
	}
	
	// 트리 내 전체(Root) 추가
	public List searchPropertyUserTreeList(Map paramMap) throws NkiaException {
		return list("PropertyDAO.searchPropertyUserTreeList", paramMap);
	}
	
	// 프라퍼티 상세 정보 조회
	public Map selectPropertyInfo(ModelMap paramMap) throws NkiaException {
		return (Map) this.selectByPk("PropertyDAO.selectPropertyInfo", paramMap);
	}
	
	// 프라퍼티 사용자 팝업의 상세 정보 조회
	public Map selectPropertyUserInfo(ModelMap paramMap) throws NkiaException {
		return (Map) this.selectByPk("PropertyDAO.selectPropertyUserInfo", paramMap);
	}
}
