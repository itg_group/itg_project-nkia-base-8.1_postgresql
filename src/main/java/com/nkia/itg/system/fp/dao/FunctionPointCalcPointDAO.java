/*
 * @(#)FunctionPointCalcPointDAO.java              2018. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("functionPointCalcPointDAO")
public class FunctionPointCalcPointDAO extends EgovComAbstractDAO {

	public int updateF_CALC_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateF_CALC_ILF", functionPointInfo);
	}
	public int updateF_CALC_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateF_CALC_EIF", functionPointInfo);
	}
	public int updateF_CALC_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateF_CALC_EI", functionPointInfo);
	}
	public int updateF_CALC_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateF_CALC_EO", functionPointInfo);
	}
	public int updateF_CALC_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateF_CALC_EQ", functionPointInfo);
	}
	public int updateF_CALC_TOT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateF_CALC_TOT", functionPointInfo);
	}
	

	/** * 가능점수_가중치_ILF변경 * @param paramMap * @return int @ */
	public int updateP_WEIGH_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_WEIGH_ILF", functionPointInfo);
	}

	/** * 가능점수_가중치_EIF변경 * @param paramMap * @return int @ */
	public int updateP_WEIGH_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_WEIGH_EIF", functionPointInfo);
	}

	/** * 가능점수_가중치_EI변경 * @param paramMap * @return int @ */
	public int updateP_WEIGH_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_WEIGH_EI", functionPointInfo);
	}

	/** * 가능점수_가중치_EO변경 * @param paramMap * @return int @ */
	public int updateP_WEIGH_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_WEIGH_EO", functionPointInfo);
	}

	/** * 가능점수_가중치_EQ변경 * @param paramMap * @return int @ */
	public int updateP_WEIGH_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_WEIGH_EQ", functionPointInfo);
	}

	/** * 기능점수_변경율산정_설계변경율_가중치변경 * @param paramMap * @return int @ */
	public int updateP_CHGRATE_DESIGN_WEIGHT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_CHGRATE_DESIGN_WEIGHT", functionPointInfo);
	}

	/** * 기능점수_변경율산정_설계변경율_비중변경 * @param paramMap * @return int @ */
	public int updateP_CHGRATE_DESIGN_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_CHGRATE_DESIGN_IMP", functionPointInfo);
	}

	/** * 기능점수_변경율산정_코드변경율_가중치변경 * @param paramMap * @return int @ */
	public int updateP_CHGRATE_CODE_WEIGHT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_CHGRATE_CODE_WEIGHT", functionPointInfo);
	}

	/** * 기능점수_변경율산정_코드변경율_비중변경 * @param paramMap * @return int @ */
	public int updateP_CHGRATE_CODE_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_CHGRATE_CODE_IMP", functionPointInfo);
	}

	/** * 기능점수_변경율산정_통합및시험변경율_가중치변경 * @param paramMap * @return int @ */
	public int updateP_CHGRATE_TEST_WEIGHT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_CHGRATE_TEST_WEIGHT", functionPointInfo);
	}

	/** * 기능점수_변경율산정_통합및시험변경율_비중변경 * @param paramMap * @return int @ */
	public int updateP_CHGRATE_TEST_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_CHGRATE_TEST_IMP", functionPointInfo);
	}

	/** * 기능점수_변경율산정_총변경율변경 * @param paramMap * @return int @ */
	public int updateP_CHGRATE_TOTCHGRATE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_CHGRATE_TOTCHGRATE", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용소프트웨어평가노력수준_수준(Max)변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_EFFORT_LEVMAX(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_EFFORT_LEVMAX", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용소프트웨어평가노력수준_값변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_EFFORT_VALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_EFFORT_VALUE", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용난이도수준판단_프로그램의구조화정보_수준변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_RELEV_PRGM_LEV(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_RELEV_PRGM_LEV", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용난이도수준판단_어플리케이션관점에서의명확성_수준변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_RELEV_APP_LEV(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_RELEV_APP_LEV", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용난이도수준판단_프로그램소스코드의서술정도_수준변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_RELEV_SOURCE_LEV(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_RELEV_SOURCE_LEV", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용난이도수준판단_값변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_RELEV_VALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_RELEV_VALUE", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용소프트웨어친숙도수준_수준(Max)변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_FAMIL_LEVMAX(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_FAMIL_LEVMAX", functionPointInfo);
	}

	/** * 기능점수_재사용평가항목집계_재사용소프트웨어친숙도수준_값변경 * @param paramMap * @return int @ */
	public int updateP_REEVAL_FAMIL_VALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_REEVAL_FAMIL_VALUE", functionPointInfo);
	}

	/** * 기능점수_재사용보정계수계산_재사용보정계수_50이하_보정값변경 * @param paramMap * @return int @ */
	public int updateP_RECALC_DN50_CRCTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_RECALC_DN50_CRCTVALUE", functionPointInfo);
	}

	/** * 기능점수_재사용보정계수계산_재사용보정계수_50초과_보정값변경 * @param paramMap * @return int @ */
	public int updateP_RECALC_UP50_CRCTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_RECALC_UP50_CRCTVALUE", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능수_ILF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_FUNCCNT_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_FUNCCNT_ILF", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능수_EIF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_FUNCCNT_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_FUNCCNT_EIF", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능수_EI변경 * @param paramMap * @return int @ */
	public int updateP_SUM_FUNCCNT_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_FUNCCNT_EI", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능수_EO변경 * @param paramMap * @return int @ */
	public int updateP_SUM_FUNCCNT_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_FUNCCNT_EO", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능수_EQ변경 * @param paramMap * @return int @ */
	public int updateP_SUM_FUNCCNT_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_FUNCCNT_EQ", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능수_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_FUNCCNT_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_FUNCCNT_SUM", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능점수_ILF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_F_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_F_ILF", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능점수_EIF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_F_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_F_EIF", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능점수_EI변경 * @param paramMap * @return int @ */
	public int updateP_SUM_F_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_F_EI", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능점수_EO변경 * @param paramMap * @return int @ */
	public int updateP_SUM_F_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_F_EO", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능점수_EQ변경 * @param paramMap * @return int @ */
	public int updateP_SUM_F_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_F_EQ", functionPointInfo);
	}

	/** * 기능점수_FP집계_기능점수_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_F_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_F_SUM", functionPointInfo);
	}

	/** * 기능점수_FP집계_신규FP_ILF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_NEWFP_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_NEWFP_ILF", functionPointInfo);
	}

	/** * 기능점수_FP집계_신규FP_EIF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_NEWFP_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_NEWFP_EIF", functionPointInfo);
	}

	/** * 기능점수_FP집계_신규FP_EI변경 * @param paramMap * @return int @ */
	public int updateP_SUM_NEWFP_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_NEWFP_EI", functionPointInfo);
	}

	/** * 기능점수_FP집계_신규FP_EO변경 * @param paramMap * @return int @ */
	public int updateP_SUM_NEWFP_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_NEWFP_EO", functionPointInfo);
	}

	/** * 기능점수_FP집계_신규FP_EQ변경 * @param paramMap * @return int @ */
	public int updateP_SUM_NEWFP_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_NEWFP_EQ", functionPointInfo);
	}

	/** * 기능점수_FP집계_신규FP_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_NEWFP_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_NEWFP_SUM", functionPointInfo);
	}

	/** * 기능점수_FP집계_재사용조정전FP_ILF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_REFP_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_REFP_ILF", functionPointInfo);
	}

	/** * 기능점수_FP집계_재사용조정전FP_EIF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_REFP_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_REFP_EIF", functionPointInfo);
	}

	/** * 기능점수_FP집계_재사용조정전FP_EI변경 * @param paramMap * @return int @ */
	public int updateP_SUM_REFP_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_REFP_EI", functionPointInfo);
	}

	/** * 기능점수_FP집계_재사용조정전FP_EO변경 * @param paramMap * @return int @ */
	public int updateP_SUM_REFP_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_REFP_EO", functionPointInfo);
	}

	/** * 기능점수_FP집계_재사용조정전FP_EQ변경 * @param paramMap * @return int @ */
	public int updateP_SUM_REFP_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_REFP_EQ", functionPointInfo);
	}

	/** * 기능점수_FP집계_재사용조정전FP_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_REFP_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_REFP_SUM", functionPointInfo);
	}

	/** * 기능점수_FP집계_재사용조정후FP_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_REAFP_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_REAFP_SUM", functionPointInfo);
	}

	/** * 기능점수_FP집계_비중_ILF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_IMP_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_IMP_ILF", functionPointInfo);
	}

	/** * 기능점수_FP집계_비중_EIF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_IMP_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_IMP_EIF", functionPointInfo);
	}

	/** * 기능점수_FP집계_비중_EI변경 * @param paramMap * @return int @ */
	public int updateP_SUM_IMP_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_IMP_EI", functionPointInfo);
	}

	/** * 기능점수_FP집계_비중_EO변경 * @param paramMap * @return int @ */
	public int updateP_SUM_IMP_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_IMP_EO", functionPointInfo);
	}

	/** * 기능점수_FP집계_비중_EQ변경 * @param paramMap * @return int @ */
	public int updateP_SUM_IMP_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_IMP_EQ", functionPointInfo);
	}

	/** * 기능점수_FP집계_비중_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_IMP_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_IMP_SUM", functionPointInfo);
	}

	/** * 기능점수_FP집계_ISBSG기준_ILF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_ISBSG_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_ISBSG_ILF", functionPointInfo);
	}

	/** * 기능점수_FP집계_ISBSG기준_EIF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_ISBSG_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_ISBSG_EIF", functionPointInfo);
	}

	/** * 기능점수_FP집계_ISBSG기준_EI변경 * @param paramMap * @return int @ */
	public int updateP_SUM_ISBSG_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_ISBSG_EI", functionPointInfo);
	}

	/** * 기능점수_FP집계_ISBSG기준_EO변경 * @param paramMap * @return int @ */
	public int updateP_SUM_ISBSG_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_ISBSG_EO", functionPointInfo);
	}

	/** * 기능점수_FP집계_ISBSG기준_EQ변경 * @param paramMap * @return int @ */
	public int updateP_SUM_ISBSG_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_ISBSG_EQ", functionPointInfo);
	}

	/** * 기능점수_FP집계_ISBSG기준_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_ISBSG_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_ISBSG_SUM", functionPointInfo);
	}

	/** * 기능점수_FP집계_편차_ILF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_DEVI_ILF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_DEVI_ILF", functionPointInfo);
	}

	/** * 기능점수_FP집계_편차_EIF변경 * @param paramMap * @return int @ */
	public int updateP_SUM_DEVI_EIF(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_DEVI_EIF", functionPointInfo);
	}

	/** * 기능점수_FP집계_편차_EI변경 * @param paramMap * @return int @ */
	public int updateP_SUM_DEVI_EI(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_DEVI_EI", functionPointInfo);
	}

	/** * 기능점수_FP집계_편차_EO변경 * @param paramMap * @return int @ */
	public int updateP_SUM_DEVI_EO(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_DEVI_EO", functionPointInfo);
	}

	/** * 기능점수_FP집계_편차_EQ변경 * @param paramMap * @return int @ */
	public int updateP_SUM_DEVI_EQ(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_DEVI_EQ", functionPointInfo);
	}

	/** * 기능점수_FP집계_편차_계변경 * @param paramMap * @return int @ */
	public int updateP_SUM_DEVI_SUM(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcPointDAO.updateP_SUM_DEVI_SUM", functionPointInfo);
	}
}
