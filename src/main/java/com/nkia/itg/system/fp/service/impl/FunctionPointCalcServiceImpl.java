/*
 * @(#)FunctionPointServiceImpl.java              2018. 3. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.system.fp.dao.FunctionPointCalcCostDAO;
import com.nkia.itg.system.fp.dao.FunctionPointCalcPointDAO;
import com.nkia.itg.system.fp.dao.FunctionPointDAO;
import com.nkia.itg.system.fp.service.FunctionPointCalcService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("functionPointCalcService")
public class FunctionPointCalcServiceImpl implements FunctionPointCalcService{

	private static final Logger logger = LoggerFactory.getLogger(FunctionPointCalcServiceImpl.class);
	
	@Resource(name = "functionPointDAO")
	public FunctionPointDAO functionPointDAO;
	
	@Resource(name = "functionPointCalcPointDAO")
	public FunctionPointCalcPointDAO functionPointCalcPointDAO;
	
	@Resource(name = "functionPointCalcCostDAO")
	public FunctionPointCalcCostDAO functionPointCalcCostDAO;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	/**
	 * 계산실행 [계산단위 모듈] 
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map<String, Object> calcRun(Map<String, Object> paramMap) throws NkiaException {		

		Map<String, Object> resultMap = new HashMap<String, Object>();
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("run_mode")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 실행모드"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("model_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모델아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("calc_ym")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 계산년월"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("module_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모듈아이디"); // 필수 입력 항목을 확인 바랍니다.
		}

		// 세션정보
		Map<String, Object> mModule = new HashMap<String, Object>();
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		int iCnt = 0;
		String run_mode = StringUtil.replaceNull(String.valueOf(paramMap.get("run_mode")), ""); // 실행모드
		String calc_ym = StringUtil.replaceNull(String.valueOf(paramMap.get("calc_ym")), ""); // 계산년월
		String model_id = StringUtil.replaceNull(String.valueOf(paramMap.get("model_id")), ""); // 모델
		String module_id = StringUtil.replaceNull(String.valueOf(paramMap.get("module_id")), ""); // 모듈
		String calc_status = "";
		String rslt_point_id = calc_ym + "_" + model_id + "_" + module_id; // 결과점수 아이디
		String rslt_cost_id = calc_ym + "_" + model_id + "_" + module_id; // 비용계산 아이디
		
		logger.debug(rslt_cost_id +" (0) rslt_point_id : " + rslt_point_id + ", rslt_cost_id : " + rslt_cost_id);

		if("CANCEL".equals(run_mode)){
			
			logger.debug(rslt_cost_id +" (1) 기존 포인트,비용 계산결과 삭제 및 초기화  {deleteRsltPoint, deleteRsltCost, insertRsltCost, insertRsltPoint}  ");
			mPs = new HashMap<String, Object>();
			mPs.put("login_user_id", login_user_id);
			mPs.put("rslt_cost_id", rslt_cost_id); // PK
			iCnt = functionPointCalcCostDAO.updateReqFuncCalcReset(mPs); // 변경기능 기존결과값 지움 {RSLT_COST_ID = null, RSLT_POINT_ID = null}
			logger.debug(">>  functionPointCalcCostDAO.updateReqFuncCalcReset :: iCnt : " + iCnt);
			
			logger.debug(rslt_cost_id +" (2 처리결과상태 취소 업데이트   ");
			mPs = new HashMap<String, Object>();
			calc_status = "CANCEL";
			mPs.put("calc_status", calc_status); // 계산상태 : 실패/완료
			mPs.put("result_cd", "SUCCESS");
			mPs.put("result_msg", "취소작업이 완료되었습니다.");
			mPs.put("result_desc", "-");
			mPs.put("rslt_cost_id", rslt_cost_id); // PK
			mPs.put("login_user_id", login_user_id);
			iCnt = functionPointDAO.updateRsltCost(mPs); // 처리결과상태 업데이트 (계산상태 : 실패 또는 완료)
			logger.debug(">>  functionPointDAO.updateRsltCost :: iCnt : " + iCnt);
			
			resultMap.put("RESULT_CD", "SUCCESS");
			resultMap.put("RESULT_MSG", "취소작업이 완료되었습니다.");
			resultMap.put("RESULT_DESC", "");
			resultMap.put("RSLT_COST_ID", rslt_cost_id);
			resultMap.put("INS_DT", "");
			resultMap.put("CALC_DESC", "");
			resultMap.put("CALC_STATUS", calc_status);
			return resultMap;
		}

        int year = Integer.parseInt(calc_ym.substring(0, 4));
        int month = Integer.parseInt(calc_ym.substring(4, 6));
        Calendar cal = Calendar.getInstance();
        cal.set(year, (month -1), 1);
        
		String calc_ym_endDay = calc_ym + (cal.getActualMaximum(Calendar.DAY_OF_MONTH) < 11? "0": "") + (cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		mPs = new HashMap<String, Object>();
		mPs.put("conf_id", module_id);
		Map<String, Object> confRelTopBottomAppSwMap = functionPointDAO.searchConfRelTopBottomAppSw(mPs); // 모듈 분류정보
		
		mPs = new HashMap<String, Object>();
		mPs.put("login_user_id", login_user_id);
		mPs.put("rslt_point_id", rslt_point_id);
		mPs.put("rslt_cost_id", rslt_cost_id);
		mPs.put("calc_ym", calc_ym);
		mPs.put("calc_date", calc_ym + "01");
		mPs.put("model_id", model_id);
		mPs.put("module_id", module_id);
		mPs.put("ci_class_id", confRelTopBottomAppSwMap.get("CONF1_ID")); // 구성분류아이디
		mPs.put("z_sla_contract", confRelTopBottomAppSwMap.get("Z_SLA_CONTRACT")); // 용역계약
		mPs.put("z_maint_vendor", confRelTopBottomAppSwMap.get("Z_MAINT_VENDOR")); // 용역업체 
		mPs.put("class1_id", confRelTopBottomAppSwMap.get("CONF1_ID")); // 
		mPs.put("class2_id", confRelTopBottomAppSwMap.get("CONF2_ID")); // 
		mPs.put("class3_id", confRelTopBottomAppSwMap.get("CONF3_ID")); // 
		mPs.put("class4_id", confRelTopBottomAppSwMap.get("CONF4_ID")); // 
		mPs.put("class5_id", confRelTopBottomAppSwMap.get("CONF5_ID")); // 
		mPs.put("oper_cust_id", confRelTopBottomAppSwMap.get("OPER_CUST_ID")); // 공사관리조직 
		
		logger.debug(rslt_cost_id +" (1) 기존 포인트,비용 계산결과 삭제 및 초기화  {deleteRsltPoint, deleteRsltCost, insertRsltCost, insertRsltPoint}  ");
		functionPointDAO.deleteRsltPoint(mPs); // 기존 결과정보삭제 FP_RSLT_POINT
		functionPointDAO.deleteRsltCost(mPs); // 기존 결과정보삭제 FP_RSLT_COST
		iCnt = functionPointCalcCostDAO.updateReqFuncCalcReset(mPs); // 변경기능 기존결과값 지움 {RSLT_COST_ID = null, RSLT_POINT_ID = null}
		logger.debug(">>  functionPointCalcCostDAO.updateReqFuncCalcReset :: iCnt : " + iCnt);
		
		logger.debug(rslt_cost_id +" (2) 결과정보 생성 : FP_REQ_FUNC :: updateReqFuncCalcInfo {OPER_CUST_ID, F_APP, CLASS_ID, CLASS1_ID~}  ");
		calc_status = "PRGRS"; // 진행
		mPs.put("calc_status", calc_status); // 계산상태 : 진행
		functionPointDAO.insertRsltCost(mPs); // 결과정보생성 FP_RSLT_POINT
		functionPointDAO.insertRsltPoint(mPs); // 결과정보생성 FP_RSLT_COST
		
		mPs = new HashMap<String, Object>();
		mPs.put("login_user_id", login_user_id);
		mPs.put("rslt_point_id", rslt_point_id);
		mPs.put("rslt_cost_id", rslt_cost_id);
		mPs.put("model_id", model_id);
		mPs.put("module_id", module_id);
		mPs.put("calc_ym", calc_ym);
		mPs.put("calc_ym_startDay", calc_ym + "01");
		mPs.put("calc_ym_endDay", calc_ym_endDay);
		iCnt = functionPointCalcCostDAO.updateReqFuncCalcSet(mPs);   // 변경기능 수행정보 계산ID반영 {RSLT_COST_ID = null, RSLT_POINT_ID = null}
		logger.debug(">>  functionPointCalcCostDAO.updateReqFuncCalcSet :: iCnt : " + iCnt);
		
		List reqFuncCalcList = functionPointCalcCostDAO.selectReqFuncCalcList(mPs); // 대상 변경기능 정보 조회
		logger.debug(rslt_cost_id +" (2) 대상변경기능 검증 : reqFuncCalcList.size()  " + reqFuncCalcList.size());
		for(int i=0; i<reqFuncCalcList.size(); i++){
			Map map = (Map)reqFuncCalcList.get(i);
			if(!rslt_cost_id.equals(map.get("RSLT_COST_ID"))){
				throw new NkiaException("이미 다른모듈에서 처리된 변경기능정보가 존재하여 계산이 불가합니다. [현재계산번호 : " + rslt_cost_id + " - 기존계산번호 : " + String.valueOf(map.get("RSLT_COST_ID")) + " - 대상구성번호 : " + String.valueOf(map.get("CONF_ID")) + "]"); // 필수 입력 항목을 확인 바랍니다.
			}
		}
		
		logger.debug(rslt_cost_id +" (4) 기능점수 부가정보 설정 FP_RSLT_POINT :: updateRsltPointCalcInfo {OPER_CUST_ID}  ");
		mPs = new HashMap<String, Object>();
		mPs.put("login_user_id", login_user_id);
		mPs.put("rslt_cost_id", rslt_cost_id);
		functionPointCalcCostDAO.updateRsltPointCalcInfo(mPs); // 기능점수 부가정보 설정 FP_RSLT_POINT {OPER_CUST_ID, Z_SLA_CONTRACT, Z_MAINT_VENDOR, CI_CLASS_ID} 
		
		logger.debug(rslt_cost_id +" (5) 비용계산 부가정보 설정 FP_RSLT_COST :: updateRsltCostCalcInfo {Z_SLA_CONTRACT, Z_MAINT_VENDOR, CI_CLASS_ID}  ");
		mPs = new HashMap<String, Object>();
		mPs.put("login_user_id", login_user_id);
		mPs.put("rslt_cost_id", rslt_cost_id);
		functionPointCalcCostDAO.updateRsltCostCalcInfo(mPs); // 비용계산 부가정보 설정 FP_RSLT_COST {OPER_CUST_ID, Z_SLA_CONTRACT, Z_MAINT_VENDOR, CI_CLASS_ID} 
		
		//logger.debug(rslt_cost_id +" (6) AM_FP_BASIC :: updateAmFpBasicCalcInfo  기능점수 update {FP_REQ_FUNC}  ");
		//mPs = new HashMap<String, Object>();
		//mPs.put("login_user_id", login_user_id);
		//mPs.put("rslt_cost_id", rslt_cost_id);
		//functionPointCalcCostDAO.updateAmFpBasicCalcInfo(mPs); // 임시 추가
		
		logger.debug(rslt_cost_id +" (7) 기능점수 계산  :: calcPoint {rslt_point_id, rslt_cost_id} ");
		mPs = new HashMap<String, Object>();
		mPs.put("model_id", model_id);
		mPs.put("calc_ym", calc_ym);
		mPs.put("module_id", module_id);
		mPs.put("rslt_point_id", rslt_point_id);
		mPs.put("rslt_cost_id", rslt_cost_id);
		resultMap = calcPoint(mPs); // 포인트 계산 실행 
		
		if("SUCCESS".equals(resultMap.get("RESULT_CD"))){
			logger.debug(rslt_cost_id +" (8) 비용 계산    :: calcPoint {rslt_cost_id} ");
			mPs = new HashMap<String, Object>();
			mPs.put("model_id", model_id);
			mPs.put("calc_ym", calc_ym);
			mPs.put("module_id", module_id);
			mPs.put("rslt_point_id", rslt_point_id);
			mPs.put("rslt_cost_id", rslt_cost_id);
			resultMap = calcCost(mPs); // 비용 계산 실행 
		}

		logger.debug(rslt_cost_id +" (9) 수행결과 저장 ");
		mPs = new HashMap<String, Object>();
		calc_status = ("SUCCESS".equals(resultMap.get("RESULT_CD"))? "OK": "FAIL"); // 계산상태 : 실패/완료
		mPs.put("calc_status", calc_status);
		mPs.put("result_cd", resultMap.get("RESULT_CD"));
		mPs.put("result_msg", resultMap.get("RESULT_MSG"));
		mPs.put("result_desc", resultMap.get("RESULT_DESC"));
		mPs.put("rslt_cost_id", rslt_cost_id); // PK
		mPs.put("login_user_id", login_user_id);
		iCnt = functionPointDAO.updateRsltCost(mPs); // 처리결과상태 업데이트 (계산상태 : 실패 또는 완료)
		logger.debug(">>  functionPointDAO.updateRsltCost :: iCnt : " + iCnt);

		mPs = new HashMap<String, Object>();
		mPs.put("rslt_point_id", rslt_point_id); // PK
		Map<String, Object> rsltPointMap = functionPointDAO.selectRsltPoint(mPs); // 포인트 결과정보 조회 
		
		mPs = new HashMap<String, Object>();
		mPs.put("rslt_cost_id", rslt_cost_id); // PK
		Map<String, Object> rsltCostMap = functionPointDAO.selectRsltCost(mPs); // 비용계산 결과정보 조회 

		logger.debug(rslt_cost_id +" (99) RETURN ");
		resultMap.put("RESULT_CD", rsltCostMap.get("RESULT_CD"));
		resultMap.put("RESULT_MSG", rsltCostMap.get("RESULT_MSG"));
		resultMap.put("RESULT_DESC", rsltCostMap.get("RESULT_DESC"));
		resultMap.put("RSLT_COST_ID", rsltCostMap.get("RSLT_COST_ID"));
		resultMap.put("INS_DT", rsltCostMap.get("INS_DT"));
		resultMap.put("CALC_STATUS", calc_status);
		resultMap.put("FP_FUNC_CNT", reqFuncCalcList.size());
		resultMap.put("FP_FUNC_POINT", rsltPointMap.get("P_SUM_FUNCCNT_SUM"));
		resultMap.put("FP_CALC_POINT", (Double.parseDouble(String.valueOf(rsltCostMap.get("C_DEV_NEW_TOTFP"))) + Double.parseDouble(String.valueOf(rsltCostMap.get("C_DEV_RE_TOTFP")))));
		resultMap.put("FP_COST", rsltCostMap.get("C_SW_TOTDEVCOST_AMT"));
		String mm = "";
//		try{
//			if("SUCCESS".equals(resultMap.get("RESULT_CD"))){
				mm += "\n [FP 집계] ";
				mm += "\n F00_변경기능개수 : " + reqFuncCalcList.size();
				mm += "\n P28_기능점수 계 : " + rsltPointMap.get("P_SUM_FUNCCNT_SUM");
				mm += " {ILF: " + rsltPointMap.get("P_SUM_FUNCCNT_ILF") + ", EIF: " + rsltPointMap.get("P_SUM_FUNCCNT_EIF") + ", EI: " + rsltPointMap.get("P_SUM_FUNCCNT_EI");
				mm += ", EO: " + rsltPointMap.get("P_SUM_FUNCCNT_EO") + ", EQ: " + rsltPointMap.get("P_SUM_FUNCCNT_EQ") + "}";
				mm += "\n P34_(가중치적용)기능점수 계 : " + rsltPointMap.get("P_SUM_F_SUM");
				mm += " {ILF: " + rsltPointMap.get("P_SUM_F_ILF") + ", EIF: " + rsltPointMap.get("P_SUM_F_EIF") + ", EI: " + rsltPointMap.get("P_SUM_F_EI");
				mm += ", EO: " + rsltPointMap.get("P_SUM_F_EO") + ", EQ: " + rsltPointMap.get("P_SUM_F_EQ") + "}";
				mm += "\n P40_신규FP계 : " + rsltPointMap.get("P_SUM_NEWFP_SUM");
				mm += " {ILF: " + rsltPointMap.get("P_SUM_F_ILF") + ", EIF: " + rsltPointMap.get("P_SUM_F_EIF") + ", EI: " + rsltPointMap.get("P_SUM_F_EI");
				mm += ", EO: " + rsltPointMap.get("P_SUM_F_EO") + ", EQ: " + rsltPointMap.get("P_SUM_F_EQ") + "}";
				mm += "\n P46_재사용조정전FP계 : " + rsltPointMap.get("P_SUM_REFP_SUM");
				mm += " {ILF: " + rsltPointMap.get("P_SUM_REFP_ILF") + ", EIF: " + rsltPointMap.get("P_SUM_REFP_EIF") + ", EI: " + rsltPointMap.get("P_SUM_REFP_EI");
				mm += ", EO: " + rsltPointMap.get("P_SUM_REFP_EO") + ", EQ: " + rsltPointMap.get("P_SUM_REFP_EQ") + "}";
				mm += "\n P47_재사용조정후FP계 : " + rsltPointMap.get("P_SUM_REAFP_SUM");
				//mm += "\n P53_비중계 : " + rsltPointMap.get("P_SUM_IMP_SUM");
				//mm += " {" + rsltPointMap.get("P_SUM_IMP_ILF") + ", " + rsltPointMap.get("P_SUM_IMP_EIF") + ", " + rsltPointMap.get("P_SUM_IMP_EI");
				//mm += ", " + rsltPointMap.get("P_SUM_IMP_EO") + ", " + rsltPointMap.get("P_SUM_IMP_EQ") + "}";
				//mm += "\n P58_ISBSG기준계 : " + rsltPointMap.get("P_SUM_ISBSG_SUM");
				//mm += " {ILF: " + rsltPointMap.get("P_SUM_ISBSG_ILF") + ", EIF: " + rsltPointMap.get("P_SUM_ISBSG_EIF") + ", EI: " + rsltPointMap.get("P_SUM_ISBSG_EI");
				//mm += ", EO: " + rsltPointMap.get("P_SUM_ISBSG_EO") + ", EQ: " + rsltPointMap.get("P_SUM_ISBSG_EQ") + "}";
				//mm += "\n P65_편차계 : " + rsltPointMap.get("P_SUM_DEVI_SUM");
				//mm += " {" + rsltPointMap.get("P_SUM_DEVI_ILF") + ", " + rsltPointMap.get("P_SUM_DEVI_EIF") + ", " + rsltPointMap.get("P_SUM_DEVI_EI");
				//mm += ", " + rsltPointMap.get("P_SUM_DEVI_EO") + ", " + rsltPointMap.get("P_SUM_DEVI_EQ") + "}";
				//mm += "\n ------------------------------ ";
				//mm += "\n C17_어플리케이션 보정계수값 : " + rsltCostMap.get("C_CF_APP_CORFACTVALUE");
				//mm += "\n C37_언어-보정계수값 {신규: " + rsltCostMap.get("C_CF_LANG1_CORFACTVALUE") + ", 재개발: " + rsltCostMap.get("C_CF_LANG2_CORFACTVALUE") + "}";
				//mm += "\n C39_규모-보정계수값 {신규: " + rsltCostMap.get("C_CF_SCALE_NEW_CORFACTVALUE") + ", 재개발: " + rsltCostMap.get("C_CF_SCALE_RE_CORFACTVALUE") + "}";
				mm += "\n [개발원가 산정] ";
				mm += "\n C51_신규-총기능점수 : " + rsltCostMap.get("C_DEV_NEW_TOTFP") + " FP";
				//mm += "\n C52_신규-언어1,2 : {" + rsltCostMap.get("C_DEV_NEW_LANG1") + ", " + rsltCostMap.get("C_DEV_NEW_LANG2") + "}";
				mm += "\n C61_신규-총개발원가 : " + rsltCostMap.get("C_DEV_NEW_TOT_DCOST");
				mm += " {분석: " + rsltCostMap.get("C_DEV_NEW_ANALY_DCOST") + ", 설계: " + rsltCostMap.get("C_DEV_NEW_DESIGN_DCOST") + ", 구현: " + rsltCostMap.get("C_DEV_NEW_DEVELOP_DCOST") + ", 시험: " + rsltCostMap.get("C_DEV_NEW_TEST_DCOST") + "}";
				mm += "\n C67_재개발-총기능점수 : " + rsltCostMap.get("C_DEV_RE_TOTFP") + " FP";
				//mm += "\n C68_재개발-언어1,2 : {" + rsltCostMap.get("C_DEV_RE_LANG1") + ", " + rsltCostMap.get("C_DEV_RE_LANG2") + "}";
				mm += "\n C77_재개발-총개발원가 : " + rsltCostMap.get("C_DEV_RE_TOT_DCOST");
				mm += " {분석: " + rsltCostMap.get("C_DEV_RE_ANALY_DCOST") + ", 설계: " + rsltCostMap.get("C_DEV_RE_DESIGN_DCOST") + ", 구현: " + rsltCostMap.get("C_DEV_RE_DEVELOP_DCOST") + ", 시험: " + rsltCostMap.get("C_DEV_RE_TEST_DCOST") + "}";
				mm += "\n [소프트웨어 개발비] ";
				mm += "\n C80_개발원가 : " + rsltCostMap.get("C_SW_DCOST_AMT") + " (" + (Double.parseDouble(String.valueOf(rsltCostMap.get("C_DEV_NEW_TOTFP"))) + Double.parseDouble(String.valueOf(rsltCostMap.get("C_DEV_RE_TOTFP")))) + " FP)";
				mm += "\n C81_직접경비 : " + rsltCostMap.get("C_SW_DIRCOST_AMT");
				if("".equals(StringUtil.replaceNull(String.valueOf(rsltCostMap.get("C_SW_DIRCOST2_AMT")), ""))){
					mm += "\n C82_직접경비2 : " + rsltCostMap.get("C_SW_DIRCOST2_AMT");
				}
				if("".equals(StringUtil.replaceNull(String.valueOf(rsltCostMap.get("C_SW_DIRCOST3_AMT")), ""))){
					mm += "\n C83_직접경비3 : " + rsltCostMap.get("C_SW_DIRCOST3_AMT");
				}
				if("".equals(StringUtil.replaceNull(String.valueOf(rsltCostMap.get("C_SW_DIRCOST4_AMT")), ""))){
					mm += "\n C84_직접경비4 : " + rsltCostMap.get("C_SW_DIRCOST4_AMT");
				}
				if("".equals(StringUtil.replaceNull(String.valueOf(rsltCostMap.get("C_SW_DIRCOST5_AMT")), ""))){
					mm += "\n C85_직접경비5 : " + rsltCostMap.get("C_SW_DIRCOST2_AMT");
				}
				mm += "\n C86_이윤 : " + rsltCostMap.get("C_SW_PROFIT_AMT") + " : (개발원가 * " + rsltCostMap.get("C_SW_PROFIT_RATE") + ")";
				mm += "\n C87_부가세 : " + rsltCostMap.get("C_SW_SURTAX_AMT") + " : (개발원가 + 직접경비 + 이윤 의 " + rsltCostMap.get("C_SW_SURTAX_RATE") + "% 적용 )";
				mm += "\n C88_총개발비용 : " + rsltCostMap.get("C_SW_TOTDEVCOST_AMT");
//			}
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		resultMap.put("CALC_DESC", mm);
		return resultMap;
	}
	
	/**
	 * 포인트 계산 [계산단위 모듈] 
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map<String, Object> calcPoint(Map<String, Object> paramMap) throws NkiaException {		
		
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("model_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모들아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("rslt_point_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 산출점수아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("calc_ym")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 산출년월"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("module_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모듈아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("rslt_cost_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 산출비용아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> mPs = null;

		// 세션정보
		Map<String, Object> mModule = new HashMap<String, Object>();
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		String model_id = String.valueOf(paramMap.get("model_id"));
		String calc_ym = String.valueOf(paramMap.get("calc_ym"));
		String module_id = String.valueOf(paramMap.get("module_id"));
		String rslt_point_id = String.valueOf(paramMap.get("rslt_point_id"));
		String rslt_cost_id = String.valueOf(paramMap.get("rslt_cost_id"));
		
		mPs = new HashMap<String, Object>();
		mPs.put("rslt_point_id", rslt_point_id);
		//Map<String, Object> modelMap = functionPointDAO.selectReq(mPs);
		
		int iCnt = 0;
		mPs = new HashMap<String, Object>();
		mPs.put("model_id", model_id);
		mPs.put("module_id", module_id);
		mPs.put("calc_ym", calc_ym);
		mPs.put("rslt_point_id", rslt_point_id);
		mPs.put("rslt_cost_id", rslt_cost_id);
		
		String calcPoint = "";
		try{

			calcPoint = "F01 : F_CALC_ILF : 가능점수_ILF CCCC";
			iCnt = functionPointCalcPointDAO.updateF_CALC_ILF(mPs);
			calcPoint = "F02 : F_CALC_EIF : 가능점수_EIF CCCC";
			iCnt = functionPointCalcPointDAO.updateF_CALC_EIF(mPs);
			calcPoint = "F03 : F_CALC_EI : 가능점수_EI CCCC";
			iCnt = functionPointCalcPointDAO.updateF_CALC_EI(mPs);
			calcPoint = "F04 : F_CALC_EO : 가능점수_EO CCCC";
			iCnt = functionPointCalcPointDAO.updateF_CALC_EO(mPs);
			calcPoint = "F05 : F_CALC_EQ : 가능점수_EQ CCCC";
			iCnt = functionPointCalcPointDAO.updateF_CALC_EQ(mPs);
			calcPoint = "F06 : F_CALC_TOT : 가능점수_계 CCCC";
			iCnt = functionPointCalcPointDAO.updateF_CALC_TOT(mPs);
			
			
			calcPoint = "P01 : P_WEIGH_ILF : 가능점수_가중치_ILF";
			iCnt = functionPointCalcPointDAO.updateP_WEIGH_ILF(mPs); // 가능점수_가중치_ILF
			calcPoint = "P02 : P_WEIGH_EIF : 가능점수_가중치_EIF";
			iCnt = functionPointCalcPointDAO.updateP_WEIGH_EIF(mPs); // 가능점수_가중치_EIF
			calcPoint = "P03 : P_WEIGH_EI : 가능점수_가중치_EI";
			iCnt = functionPointCalcPointDAO.updateP_WEIGH_EI(mPs); // 가능점수_가중치_EI
			calcPoint = "P04 : P_WEIGH_EO : 가능점수_가중치_EO";
			iCnt = functionPointCalcPointDAO.updateP_WEIGH_EO(mPs); // 가능점수_가중치_EO
			calcPoint = "P05 : P_WEIGH_EQ : 가능점수_가중치_EQ";
			iCnt = functionPointCalcPointDAO.updateP_WEIGH_EQ(mPs); // 가능점수_가중치_EQ
			calcPoint = "P06 : P_CHGRATE_DESIGN_WEIGHT : 기능점수_변경율산정_설계변경율_가중치";
			iCnt = functionPointCalcPointDAO.updateP_CHGRATE_DESIGN_WEIGHT(mPs); // 기능점수_변경율산정_설계변경율_가중치
			calcPoint = "P07 : P_CHGRATE_DESIGN_IMP : 기능점수_변경율산정_설계변경율_비중  MMMMMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_CHGRATE_DESIGN_IMP(mPs); // 기능점수_변경율산정_설계변경율_비중
			calcPoint = "P08 : P_CHGRATE_CODE_WEIGHT : 기능점수_변경율산정_코드변경율_가중치";
			iCnt = functionPointCalcPointDAO.updateP_CHGRATE_CODE_WEIGHT(mPs); // 기능점수_변경율산정_코드변경율_가중치
			calcPoint = "P09 : P_CHGRATE_CODE_IMP : 기능점수_변경율산정_코드변경율_비중  MMMMMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_CHGRATE_CODE_IMP(mPs); // 기능점수_변경율산정_코드변경율_비중
			calcPoint = "P10 : P_CHGRATE_TEST_WEIGHT : 기능점수_변경율산정_통합및시험변경율_가중치";
			iCnt = functionPointCalcPointDAO.updateP_CHGRATE_TEST_WEIGHT(mPs); // 기능점수_변경율산정_통합및시험변경율_가중치
			calcPoint = "P11 : P_CHGRATE_TEST_IMP : 기능점수_변경율산정_통합및시험변경율_비중  MMMMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_CHGRATE_TEST_IMP(mPs); // 기능점수_변경율산정_통합및시험변경율_비중
			calcPoint = "P12 : P_CHGRATE_TOTCHGRATE : 기능점수_변경율산정_총변경율  CCCCCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_CHGRATE_TOTCHGRATE(mPs); // 기능점수_변경율산정_총변경율
			calcPoint = "P13 : P_REEVAL_EFFORT_LEVMAX : 기능점수_재사용평가항목집계_재사용소프트웨어평가노력수준_수준(Max)";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_EFFORT_LEVMAX(mPs); // 기능점수_재사용평가항목집계_재사용소프트웨어평가노력수준_수준(Max)
			calcPoint = "P14 : P_REEVAL_EFFORT_VALUE : 기능점수_재사용평가항목집계_재사용소프트웨어평가노력수준_값  MMMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_EFFORT_VALUE(mPs); // 기능점수_재사용평가항목집계_재사용소프트웨어평가노력수준_값
			calcPoint = "P15 : P_REEVAL_RELEV_PRGM_LEV : 기능점수_재사용평가항목집계_재사용난이도수준판단_프로그램의구조화정보_수준  MMMMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_RELEV_PRGM_LEV(mPs); // 기능점수_재사용평가항목집계_재사용난이도수준판단_프로그램의구조화정보_수준
			calcPoint = "P16 : P_REEVAL_RELEV_APP_LEV : 기능점수_재사용평가항목집계_재사용난이도수준판단_어플리케이션관점에서의명확성_수준  MMMMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_RELEV_APP_LEV(mPs); // 기능점수_재사용평가항목집계_재사용난이도수준판단_어플리케이션관점에서의명확성_수준
			calcPoint = "P17 : P_REEVAL_RELEV_SOURCE_LEV : 기능점수_재사용평가항목집계_재사용난이도수준판단_프로그램소스코드의서술정도_수준  MMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_RELEV_SOURCE_LEV(mPs); // 기능점수_재사용평가항목집계_재사용난이도수준판단_프로그램소스코드의서술정도_수준
			calcPoint = "P18 : P_REEVAL_RELEV_VALUE : 기능점수_재사용평가항목집계_재사용난이도수준판단_값  CCCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_RELEV_VALUE(mPs); // 기능점수_재사용평가항목집계_재사용난이도수준판단_값
			calcPoint = "P19 : P_REEVAL_FAMIL_LEVMAX : 기능점수_재사용평가항목집계_재사용소프트웨어친숙도수준_수준(Max)";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_FAMIL_LEVMAX(mPs); // 기능점수_재사용평가항목집계_재사용소프트웨어친숙도수준_수준(Max)
			calcPoint = "P20 : P_REEVAL_FAMIL_VALUE : 기능점수_재사용평가항목집계_재사용소프트웨어친숙도수준_값   MMMMMMMMM";
			iCnt = functionPointCalcPointDAO.updateP_REEVAL_FAMIL_VALUE(mPs); // 기능점수_재사용평가항목집계_재사용소프트웨어친숙도수준_값
			calcPoint = "P21 : P_RECALC_DN50_CRCTVALUE : 기능점수_재사용보정계수계산_재사용보정계수_50이하_보정값  CCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_RECALC_DN50_CRCTVALUE(mPs); // 기능점수_재사용보정계수계산_재사용보정계수_50이하_보정값
			calcPoint = "P22 : P_RECALC_UP50_CRCTVALUE : 기능점수_재사용보정계수계산_재사용보정계수_50초과_보정값  CCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_RECALC_UP50_CRCTVALUE(mPs); // 기능점수_재사용보정계수계산_재사용보정계수_50초과_보정값
			calcPoint = "P23 : P_SUM_FUNCCNT_ILF : 기능점수_FP집계_기능수_ILF  CCCCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_FUNCCNT_ILF(mPs); // 기능점수_FP집계_기능수_ILF
			calcPoint = "P24 : P_SUM_FUNCCNT_EIF : 기능점수_FP집계_기능수_EIF  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_FUNCCNT_EIF(mPs); // 기능점수_FP집계_기능수_EIF
			calcPoint = "P25 : P_SUM_FUNCCNT_EI : 기능점수_FP집계_기능수_EI  CCCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_FUNCCNT_EI(mPs); // 기능점수_FP집계_기능수_EI
			calcPoint = "P26 : P_SUM_FUNCCNT_EO : 기능점수_FP집계_기능수_EO  CCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_FUNCCNT_EO(mPs); // 기능점수_FP집계_기능수_EO
			calcPoint = "P27 : P_SUM_FUNCCNT_EQ : 기능점수_FP집계_기능수_EQ   CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_FUNCCNT_EQ(mPs); // 기능점수_FP집계_기능수_EQ
			calcPoint = "P28 : P_SUM_FUNCCNT_SUM : 기능점수_FP집계_기능수_계  CCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_FUNCCNT_SUM(mPs); // 기능점수_FP집계_기능수_계
			calcPoint = "P29 : P_SUM_F_ILF : 기능점수_FP집계_기능점수_ILF  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_F_ILF(mPs); // 기능점수_FP집계_기능점수_ILF
			calcPoint = "P30 : P_SUM_F_EIF : 기능점수_FP집계_기능점수_EIF  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_F_EIF(mPs); // 기능점수_FP집계_기능점수_EIF
			calcPoint = "P31 : P_SUM_F_EI : 기능점수_FP집계_기능점수_EI   CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_F_EI(mPs); // 기능점수_FP집계_기능점수_EI
			calcPoint = "P32 : P_SUM_F_EO : 기능점수_FP집계_기능점수_EO  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_F_EO(mPs); // 기능점수_FP집계_기능점수_EO
			calcPoint = "P33 : P_SUM_F_EQ : 기능점수_FP집계_기능점수_EQ   CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_F_EQ(mPs); // 기능점수_FP집계_기능점수_EQ
			calcPoint = "P34 : P_SUM_F_SUM : 기능점수_FP집계_기능점수_계   CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_F_SUM(mPs); // 기능점수_FP집계_기능점수_계
			calcPoint = "P35 : P_SUM_NEWFP_ILF : 기능점수_FP집계_신규FP_ILF  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_NEWFP_ILF(mPs); // 기능점수_FP집계_신규FP_ILF
			calcPoint = "P36 : P_SUM_NEWFP_EIF : 기능점수_FP집계_신규FP_EIF  CCCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_NEWFP_EIF(mPs); // 기능점수_FP집계_신규FP_EIF
			calcPoint = "P37 : P_SUM_NEWFP_EI : 기능점수_FP집계_신규FP_EI   CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_NEWFP_EI(mPs); // 기능점수_FP집계_신규FP_EI
			calcPoint = "P38 : P_SUM_NEWFP_EO : 기능점수_FP집계_신규FP_EO  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_NEWFP_EO(mPs); // 기능점수_FP집계_신규FP_EO
			calcPoint = "P39 : P_SUM_NEWFP_EQ : 기능점수_FP집계_신규FP_EQ  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_NEWFP_EQ(mPs); // 기능점수_FP집계_신규FP_EQ
			calcPoint = "P40 : P_SUM_NEWFP_SUM : 기능점수_FP집계_신규FP_계   CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_NEWFP_SUM(mPs); // 기능점수_FP집계_신규FP_계
			calcPoint = "P41 : P_SUM_REFP_ILF : 기능점수_FP집계_재사용조정전FP_ILF  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_REFP_ILF(mPs); // 기능점수_FP집계_재사용조정전FP_ILF
			calcPoint = "P42 : P_SUM_REFP_EIF : 기능점수_FP집계_재사용조정전FP_EIF  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_REFP_EIF(mPs); // 기능점수_FP집계_재사용조정전FP_EIF
			calcPoint = "P43 : P_SUM_REFP_EI : 기능점수_FP집계_재사용조정전FP_EI  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_REFP_EI(mPs); // 기능점수_FP집계_재사용조정전FP_EI
			calcPoint = "P44 : P_SUM_REFP_EO : 기능점수_FP집계_재사용조정전FP_EO  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_REFP_EO(mPs); // 기능점수_FP집계_재사용조정전FP_EO
			calcPoint = "P45 : P_SUM_REFP_EQ : 기능점수_FP집계_재사용조정전FP_EQ   CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_REFP_EQ(mPs); // 기능점수_FP집계_재사용조정전FP_EQ
			calcPoint = "P46 : P_SUM_REFP_SUM : 기능점수_FP집계_재사용조정전FP_계  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_REFP_SUM(mPs); // 기능점수_FP집계_재사용조정전FP_계
			calcPoint = "P47 : P_SUM_REAFP_SUM : 기능점수_FP집계_재사용조정후FP_계  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_REAFP_SUM(mPs); // 기능점수_FP집계_재사용조정후FP_계
			calcPoint = "P48 : P_SUM_IMP_ILF : 기능점수_FP집계_비중_ILF  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_IMP_ILF(mPs); // 기능점수_FP집계_비중_ILF
			calcPoint = "P49 : P_SUM_IMP_EIF : 기능점수_FP집계_비중_EIF   CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_IMP_EIF(mPs); // 기능점수_FP집계_비중_EIF
			calcPoint = "P50 : P_SUM_IMP_EI : 기능점수_FP집계_비중_EI  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_IMP_EI(mPs); // 기능점수_FP집계_비중_EI
			calcPoint = "P51 : P_SUM_IMP_EO : 기능점수_FP집계_비중_EO   CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_IMP_EO(mPs); // 기능점수_FP집계_비중_EO
			calcPoint = "P52 : P_SUM_IMP_EQ : 기능점수_FP집계_비중_EQ   CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_IMP_EQ(mPs); // 기능점수_FP집계_비중_EQ
			calcPoint = "P53 : P_SUM_IMP_SUM : 기능점수_FP집계_비중_계   CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_IMP_SUM(mPs); // 기능점수_FP집계_비중_계
			calcPoint = "P54 : P_SUM_ISBSG_ILF : 기능점수_FP집계_ISBSG기준_ILF";
			iCnt = functionPointCalcPointDAO.updateP_SUM_ISBSG_ILF(mPs); // 기능점수_FP집계_ISBSG기준_ILF
			calcPoint = "P55 : P_SUM_ISBSG_EIF : 기능점수_FP집계_ISBSG기준_EIF";
			iCnt = functionPointCalcPointDAO.updateP_SUM_ISBSG_EIF(mPs); // 기능점수_FP집계_ISBSG기준_EIF
			calcPoint = "P56 : P_SUM_ISBSG_EI : 기능점수_FP집계_ISBSG기준_EI";
			iCnt = functionPointCalcPointDAO.updateP_SUM_ISBSG_EI(mPs); // 기능점수_FP집계_ISBSG기준_EI
			calcPoint = "P57 : P_SUM_ISBSG_EO : 기능점수_FP집계_ISBSG기준_EO";
			iCnt = functionPointCalcPointDAO.updateP_SUM_ISBSG_EO(mPs); // 기능점수_FP집계_ISBSG기준_EO
			calcPoint = "P58 : P_SUM_ISBSG_EQ : 기능점수_FP집계_ISBSG기준_EQ";
			iCnt = functionPointCalcPointDAO.updateP_SUM_ISBSG_EQ(mPs); // 기능점수_FP집계_ISBSG기준_EQ
			calcPoint = "P59 : P_SUM_ISBSG_SUM : 기능점수_FP집계_ISBSG기준_계  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_ISBSG_SUM(mPs); // 기능점수_FP집계_ISBSG기준_계
			calcPoint = "P60 : P_SUM_DEVI_ILF : 기능점수_FP집계_편차_ILF  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_DEVI_ILF(mPs); // 기능점수_FP집계_편차_ILF
			calcPoint = "P61 : P_SUM_DEVI_EIF : 기능점수_FP집계_편차_EIF  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_DEVI_EIF(mPs); // 기능점수_FP집계_편차_EIF
			calcPoint = "P62 : P_SUM_DEVI_EI : 기능점수_FP집계_편차_EI  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_DEVI_EI(mPs); // 기능점수_FP집계_편차_EI
			calcPoint = "P63 : P_SUM_DEVI_EO : 기능점수_FP집계_편차_EO  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_DEVI_EO(mPs); // 기능점수_FP집계_편차_EO
			calcPoint = "P64 : P_SUM_DEVI_EQ : 기능점수_FP집계_편차_EQ  CCCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_DEVI_EQ(mPs); // 기능점수_FP집계_편차_EQ
			calcPoint = "P65 : P_SUM_DEVI_SUM : 기능점수_FP집계_편차_계  CCCCCC";
			iCnt = functionPointCalcPointDAO.updateP_SUM_DEVI_SUM(mPs); // 기능점수_FP집계_편차_계
			calcPoint = "";
			
			resultMap.put("RESULT_CD", "SUCCESS");
			resultMap.put("RESULT_MSG", "-");
			resultMap.put("RESULT_DESC", "-");
		}catch(Exception e){
			resultMap.put("RESULT_CD", "FAIL");
			resultMap.put("RESULT_MSG", "점수계산중 오류가 발생했습니다.  \n참고위치["+calcPoint+"] ");
			//resultMap.put("RESULT_DESC", "</br> - rslt_cost_id : " + rslt_cost_id + "</br> - calcPoint : " + calcPoint + "</br></br>, " + e.getMessage());
			resultMap.put("RESULT_DESC", "</br> - rslt_cost_id : " + rslt_cost_id + "</br> - calcPoint : " + calcPoint + "</br></br>, " + "error");
		}
		return resultMap;
	}
	
	/**
	 * 비용 계산 [계산단위 모듈] 
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map<String, Object> calcCost(Map<String, Object> paramMap) throws NkiaException {		

		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("model_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모델아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("rslt_point_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 산출점수아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("calc_ym")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 계산년월"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("module_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모듈아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("rslt_cost_id")), ""))){
			throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 산출비용아이디"); // 필수 입력 항목을 확인 바랍니다.
		}
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> mPs = null;

		// 세션정보
		Map<String, Object> mModule = new HashMap<String, Object>();
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		String model_id = String.valueOf(paramMap.get("model_id"));
		String calc_ym = String.valueOf(paramMap.get("calc_ym"));
		String module_id = String.valueOf(paramMap.get("module_id"));
		String rslt_point_id = String.valueOf(paramMap.get("rslt_point_id"));
		String rslt_cost_id = String.valueOf(paramMap.get("rslt_cost_id"));
		
		int iCnt = 0;
		mPs = new HashMap<String, Object>();
		mPs.put("model_id", model_id);
		mPs.put("calc_ym", calc_ym);
		mPs.put("module_id", module_id);
		mPs.put("rslt_point_id", rslt_point_id);
		mPs.put("rslt_cost_id", rslt_cost_id);

		String calcPoint = "";
		try{
			
			calcPoint = "C51 : C_DEV_NEW_TOTFP : 재개발비산정_개발원가산정_신규개발_총기능점수  CCCCCCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_TOTFP(mPs); // 재개발비산정_개발원가산정_신규개발_총기능점수
			calcPoint = "C67 : C_DEV_RE_TOTFP : 재개발비산정_개발원가산정_재개발_총기능점수  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_TOTFP(mPs); // 재개발비산정_개발원가산정_재개발_총기능점수
			
			calcPoint = "C01 : C_CF_APP_BUSINESS_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_업무처리용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_BUSINESS_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_업무처리용_보정계수
			calcPoint = "C02 : C_CF_APP_SCIENCE_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_과학기술용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_SCIENCE_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_과학기술용_보정계수
			calcPoint = "C03 : C_CF_APP_MULTIM_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_멀티미디어용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_MULTIM_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_멀티미디어용_보정계수
			calcPoint = "C04 : C_CF_APP_INTELLI_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_지능정보용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_INTELLI_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_지능정보용_보정계수
			calcPoint = "C05 : C_CF_APP_SYSTEM_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_시스템용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_SYSTEM_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_시스템용_보정계수
			calcPoint = "C06 : C_CF_APP_COMCT_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_통신제어용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_COMCT_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_통신제어용_보정계수
			calcPoint = "C07 : C_CF_APP_PROCCTR_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_공정제어용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_PROCCTR_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_공정제어용_보정계수
			calcPoint = "C08 : C_CF_APP_COMNDCTR_CORFACT : 재개발비산정_보정계수산정_어플리케이션유형_지휘통제용_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_COMNDCTR_CORFACT(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_지휘통제용_보정계수
			calcPoint = "C09 : C_CF_APP_BUSINESS_IMP : 재개발비산정_보정계수산정_어플리케이션유형_업무처리용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_BUSINESS_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_업무처리용_비중
			calcPoint = "C10 : C_CF_APP_SCIENCE_IMP : 재개발비산정_보정계수산정_어플리케이션유형_과학기술용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_SCIENCE_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_과학기술용_비중
			calcPoint = "C11 : C_CF_APP_MULTIM_IMP : 재개발비산정_보정계수산정_어플리케이션유형_멀티미디어용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_MULTIM_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_멀티미디어용_비중
			calcPoint = "C12 : C_CF_APP_INTELLI_IMP : 재개발비산정_보정계수산정_어플리케이션유형_지능정보용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_INTELLI_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_지능정보용_비중
			calcPoint = "C13 : C_CF_APP_SYSTEM_IMP : 재개발비산정_보정계수산정_어플리케이션유형_시스템용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_SYSTEM_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_시스템용_비중
			calcPoint = "C14 : C_CF_APP_COMCT_IMP : 재개발비산정_보정계수산정_어플리케이션유형_통신제어용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_COMCT_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_통신제어용_비중
			calcPoint = "C15 : C_CF_APP_PROCCTR_IMP : 재개발비산정_보정계수산정_어플리케이션유형_공정제어용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_PROCCTR_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_공정제어용_비중
			calcPoint = "C16 : C_CF_APP_COMNDCTR_IMP : 재개발비산정_보정계수산정_어플리케이션유형_지휘통제용_비중";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_COMNDCTR_IMP(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_지휘통제용_비중
			calcPoint = "C17 : C_CF_APP_CORFACTVALUE : 재개발비산정_보정계수산정_어플리케이션유형_보정계수값  CCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_APP_CORFACTVALUE(mPs); // 재개발비산정_보정계수산정_어플리케이션유형_보정계수값
			calcPoint = "C18 : C_CF_DEVSTEP_ANALY : 재개발비산정_보정계수산정_개발단계_분석";
			iCnt = functionPointCalcCostDAO.updateC_CF_DEVSTEP_ANALY(mPs); // 재개발비산정_보정계수산정_개발단계_분석
			calcPoint = "C19 : C_CF_DEVSTEP_DESIGN : 재개발비산정_보정계수산정_개발단계_설계";
			iCnt = functionPointCalcCostDAO.updateC_CF_DEVSTEP_DESIGN(mPs); // 재개발비산정_보정계수산정_개발단계_설계
			calcPoint = "C20 : C_CF_DEVSTEP_DEVELOP : 재개발비산정_보정계수산정_개발단계_구현";
			iCnt = functionPointCalcCostDAO.updateC_CF_DEVSTEP_DEVELOP(mPs); // 재개발비산정_보정계수산정_개발단계_구현
			calcPoint = "C21 : C_CF_DEVSTEP_TEST : 재개발비산정_보정계수산정_개발단계_시험";
			iCnt = functionPointCalcCostDAO.updateC_CF_DEVSTEP_TEST(mPs); // 재개발비산정_보정계수산정_개발단계_시험
			calcPoint = "C22 : C_CF_LANG_LEV1_CORFACT : 재개발비산정_보정계수산정_언어_level1_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV1_CORFACT(mPs); // 재개발비산정_보정계수산정_언어_level1_보정계수
			calcPoint = "C23 : C_CF_LANG_LEV2_CORFACT : 재개발비산정_보정계수산정_언어_level2_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV2_CORFACT(mPs); // 재개발비산정_보정계수산정_언어_level2_보정계수
			calcPoint = "C24 : C_CF_LANG_LEV3_CORFACT : 재개발비산정_보정계수산정_언어_level3_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV3_CORFACT(mPs); // 재개발비산정_보정계수산정_언어_level3_보정계수
			calcPoint = "C25 : C_CF_LANG_LEV4_CORFACT : 재개발비산정_보정계수산정_언어_level4_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV4_CORFACT(mPs); // 재개발비산정_보정계수산정_언어_level4_보정계수
			calcPoint = "C26 : C_CF_LANG_LEV5_CORFACT : 재개발비산정_보정계수산정_언어_level5_보정계수";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV5_CORFACT(mPs); // 재개발비산정_보정계수산정_언어_level5_보정계수		
			calcPoint = "C27 : C_CF_LANG_LEV1_IMPNEW : 재개발비산정_보정계수산정_언어_level1_비중(신규)  CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV1_IMPNEW(mPs); // 재개발비산정_보정계수산정_언어_level1_비중(신규)	
			calcPoint = "C28 : C_CF_LANG_LEV2_IMPNEW : 재개발비산정_보정계수산정_언어_level2_비중(신규)  CCCCCCC";	
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV2_IMPNEW(mPs); // 재개발비산정_보정계수산정_언어_level2_비중(신규)
			calcPoint = "C29 : C_CF_LANG_LEV3_IMPNEW : 재개발비산정_보정계수산정_언어_level3_비중(신규)  CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV3_IMPNEW(mPs); // 재개발비산정_보정계수산정_언어_level3_비중(신규)
			calcPoint = "C30 : C_CF_LANG_LEV4_IMPNEW : 재개발비산정_보정계수산정_언어_level4_비중(신규)  CCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV4_IMPNEW(mPs); // 재개발비산정_보정계수산정_언어_level4_비중(신규)
			calcPoint = "C31 : C_CF_LANG_LEV5_IMPNEW : 재개발비산정_보정계수산정_언어_level5_비중(신규)  CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV5_IMPNEW(mPs); // 재개발비산정_보정계수산정_언어_level5_비중(신규)
			calcPoint = "C32 : C_CF_LANG_LEV1_IMPRE : 재개발비산정_보정계수산정_언어_level1_비중(재개발)  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV1_IMPRE(mPs); // 재개발비산정_보정계수산정_언어_level1_비중(재개발)
			calcPoint = "C33 : C_CF_LANG_LEV2_IMPRE : 재개발비산정_보정계수산정_언어_level2_비중(재개발)  CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV2_IMPRE(mPs); // 재개발비산정_보정계수산정_언어_level2_비중(재개발)
			calcPoint = "C34 : C_CF_LANG_LEV3_IMPRE : 재개발비산정_보정계수산정_언어_level3_비중(재개발)  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV3_IMPRE(mPs); // 재개발비산정_보정계수산정_언어_level3_비중(재개발)
			calcPoint = "C35 : C_CF_LANG_LEV4_IMPRE : 재개발비산정_보정계수산정_언어_level4_비중(재개발)  CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV4_IMPRE(mPs); // 재개발비산정_보정계수산정_언어_level4_비중(재개발)
			calcPoint = "C36 : C_CF_LANG_LEV5_IMPRE : 재개발비산정_보정계수산정_언어_level5_비중(재개발)  CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG_LEV5_IMPRE(mPs); // 재개발비산정_보정계수산정_언어_level5_비중(재개발)
			calcPoint = "C37 : C_CF_LANG1_CORFACTVALUE : 재개발비산정_보정계수산정_언어_1_보정계수값   CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG1_CORFACTVALUE(mPs); // 재개발비산정_보정계수산정_언어_1_보정계수값
			calcPoint = "C38 : C_CF_LANG2_CORFACTVALUE : 재개발비산정_보정계수산정_언어_2_보정계수값   CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_LANG2_CORFACTVALUE(mPs); // 재개발비산정_보정계수산정_언어_2_보정계수값
			calcPoint = "C39 : C_CF_SCALE_NEW_CORFACTVALUE : 재개발비산정_보정계수산정_규모_신규_보정계수값  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_SCALE_NEW_CORFACTVALUE(mPs); // 재개발비산정_보정계수산정_규모_신규_보정계수값
			calcPoint = "C40 : C_CF_SCALE_RE_CORFACTVALUE : 재개발비산정_보정계수산정_규모_재개발_보정계수값   CCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_SCALE_RE_CORFACTVALUE(mPs); // 재개발비산정_보정계수산정_규모_재개발_보정계수값
			calcPoint = "C41 : C_CF_QLT_VARIMPACT : 재개발비산정_보정계수산정_품질및특성_분산처리_영향도   MMMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_CF_QLT_VARIMPACT(mPs); // 재개발비산정_보정계수산정_품질및특성_분산처리_영향도
			calcPoint = "C42 : C_CF_QLT_PERF_IMPACT : 재개발비산정_보정계수산정_품질및특성_성능_영향도   MMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_CF_QLT_PERF_IMPACT(mPs); // 재개발비산정_보정계수산정_품질및특성_성능_영향도
			calcPoint = "C43 : C_CF_QLT_TRUST_IMPACT : 재개발비산정_보정계수산정_품질및특성_신뢰성_영향도   MMMMMMMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_CF_QLT_TRUST_IMPACT(mPs); // 재개발비산정_보정계수산정_품질및특성_신뢰성_영향도
			calcPoint = "C44 : C_CF_QLT_MULTIS_IMPACT : 재개발비산정_보정계수산정_품질및특성_다중사이트_영향도   MMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_CF_QLT_MULTIS_IMPACT(mPs); // 재개발비산정_보정계수산정_품질및특성_다중사이트_영향도
			calcPoint = "C45 : C_CF_QLT_CORFACTVALUE : 재개발비산정_보정계수산정_품질및특성_보정계수값  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_CF_QLT_CORFACTVALUE(mPs); // 재개발비산정_보정계수산정_품질및특성_보정계수값
			calcPoint = "C46 : C_DEV_NEW_ANALY_UPRICE : 재개발비산정_개발원가산정_신규개발_분석_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_ANALY_UPRICE(mPs); // 재개발비산정_개발원가산정_신규개발_분석_기능점수단가
			calcPoint = "C47 : C_DEV_NEW_DESIGN_UPRICE : 재개발비산정_개발원가산정_신규개발_설계_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_DESIGN_UPRICE(mPs); // 재개발비산정_개발원가산정_신규개발_설계_기능점수단가
			calcPoint = "C48 : C_DEV_NEW_DEVELOP_UPRICE : 재개발비산정_개발원가산정_신규개발_구현_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_DEVELOP_UPRICE(mPs); // 재개발비산정_개발원가산정_신규개발_구현_기능점수단가
			calcPoint = "C49 : C_DEV_NEW_TEST_UPRICE : 재개발비산정_개발원가산정_신규개발_시험_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_TEST_UPRICE(mPs); // 재개발비산정_개발원가산정_신규개발_시험_기능점수단가
			calcPoint = "C50 : C_DEV_NEW_TOT_UPRICE : 재개발비산정_개발원가산정_신규개발_총계_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_TOT_UPRICE(mPs); // 재개발비산정_개발원가산정_신규개발_총계_기능점수단가
			calcPoint = "C52 : C_DEV_NEW_LANG1 : 재개발비산정_개발원가산정_신규개발_언어1  CCCCCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_LANG1(mPs); // 재개발비산정_개발원가산정_신규개발_언어1
			calcPoint = "C53 : C_DEV_NEW_LANG2 : 재개발비산정_개발원가산정_신규개발_언어2  CCCCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_LANG2(mPs); // 재개발비산정_개발원가산정_신규개발_언어2
			calcPoint = "C54 : C_DEV_NEW_APP : 재개발비산정_개발원가산정_신규개발_어플리케이션유형   CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_APP(mPs); // 재개발비산정_개발원가산정_신규개발_어플리케이션유형
			calcPoint = "C55 : C_DEV_NEW_SCALE : 재개발비산정_개발원가산정_신규개발_규모  CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_SCALE(mPs); // 재개발비산정_개발원가산정_신규개발_규모
			calcPoint = "C56 : C_DEV_NEW_QLT : 재개발비산정_개발원가산정_신규개발_품질및특성   CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_QLT(mPs); // 재개발비산정_개발원가산정_신규개발_품질및특성
			calcPoint = "C57 : C_DEV_NEW_ANALY_DCOST : 재개발비산정_개발원가산정_신규개발_분석_개발원가   CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_ANALY_DCOST(mPs); // 재개발비산정_개발원가산정_신규개발_분석_개발원가
			calcPoint = "C58 : C_DEV_NEW_DESIGN_DCOST : 재개발비산정_개발원가산정_신규개발_설계_개발원가  CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_DESIGN_DCOST(mPs); // 재개발비산정_개발원가산정_신규개발_설계_개발원가
			calcPoint = "C59 : C_DEV_NEW_DEVELOP_DCOST : 재개발비산정_개발원가산정_신규개발_구현_개발원가  CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_DEVELOP_DCOST(mPs); // 재개발비산정_개발원가산정_신규개발_구현_개발원가
			calcPoint = "C60 : C_DEV_NEW_TEST_DCOST : 재개발비산정_개발원가산정_신규개발_시험_개발원가  CCCCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_TEST_DCOST(mPs); // 재개발비산정_개발원가산정_신규개발_시험_개발원가
			calcPoint = "C61 : C_DEV_NEW_TOT_DCOST : 재개발비산정_개발원가산정_신규개발_총계_개발원가  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_NEW_TOT_DCOST(mPs); // 재개발비산정_개발원가산정_신규개발_총계_개발원가
			calcPoint = "C62 : C_DEV_RE_ANALY_UPRICE : 재개발비산정_개발원가산정_재개발_분석_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_ANALY_UPRICE(mPs); // 재개발비산정_개발원가산정_재개발_분석_기능점수단가
			calcPoint = "C63 : C_DEV_RE_DESIGN_UPRICE : 재개발비산정_개발원가산정_재개발_설계_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_DESIGN_UPRICE(mPs); // 재개발비산정_개발원가산정_재개발_설계_기능점수단가
			calcPoint = "C64 : C_DEV_RE_DEVELOP_UPRICE : 재개발비산정_개발원가산정_재개발_구현_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_DEVELOP_UPRICE(mPs); // 재개발비산정_개발원가산정_재개발_구현_기능점수단가
			calcPoint = "C65 : C_DEV_RE_TEST_UPRICE : 재개발비산정_개발원가산정_재개발_시험_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_TEST_UPRICE(mPs); // 재개발비산정_개발원가산정_재개발_시험_기능점수단가
			calcPoint = "C66 : C_DEV_RE_TOT_UPRICE : 재개발비산정_개발원가산정_재개발_총계_기능점수단가";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_TOT_UPRICE(mPs); // 재개발비산정_개발원가산정_재개발_총계_기능점수단가
			calcPoint = "C68 : C_DEV_RE_LANG1 : 재개발비산정_개발원가산정_재개발_언어1  CCCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_LANG1(mPs); // 재개발비산정_개발원가산정_재개발_언어1
			calcPoint = "C69 : C_DEV_RE_LANG2 : 재개발비산정_개발원가산정_재개발_언어2  CCCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_LANG2(mPs); // 재개발비산정_개발원가산정_재개발_언어2
			calcPoint = "C70 : C_DEV_RE_APP : 재개발비산정_개발원가산정_재개발_어플리케이션유형  CCCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_APP(mPs); // 재개발비산정_개발원가산정_재개발_어플리케이션유형
			calcPoint = "C71 : C_DEV_RE_SCALE : 재개발비산정_개발원가산정_재개발_규모   CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_SCALE(mPs); // 재개발비산정_개발원가산정_재개발_규모
			calcPoint = "C72 : C_DEV_RE_QLT : 재개발비산정_개발원가산정_재개발_품질및특성  CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_QLT(mPs); // 재개발비산정_개발원가산정_재개발_품질및특성
			calcPoint = "C73 : C_DEV_RE_ANALY_DCOST : 재개발비산정_개발원가산정_재개발_분석_개발원가  CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_ANALY_DCOST(mPs); // 재개발비산정_개발원가산정_재개발_분석_개발원가
			calcPoint = "C74 : C_DEV_RE_DESIGN_DCOST : 재개발비산정_개발원가산정_재개발_설계_개발원가  CCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_DESIGN_DCOST(mPs); // 재개발비산정_개발원가산정_재개발_설계_개발원가
			calcPoint = "C75 : C_DEV_RE_DEVELOP_DCOST : 재개발비산정_개발원가산정_재개발_구현_개발원가  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_DEVELOP_DCOST(mPs); // 재개발비산정_개발원가산정_재개발_구현_개발원가
			calcPoint = "C76 : C_DEV_RE_TEST_DCOST : 재개발비산정_개발원가산정_재개발_시험_개발원가  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_TEST_DCOST(mPs); // 재개발비산정_개발원가산정_재개발_시험_개발원가
			calcPoint = "C77 : C_DEV_RE_TOT_DCOST : 재개발비산정_개발원가산정_재개발_총계_개발원가  CCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_DEV_RE_TOT_DCOST(mPs); // 재개발비산정_개발원가산정_재개발_총계_개발원가
			calcPoint = "C78 : C_SW_PROFIT_RATE : 재개발비산정_소프트웨어개발비_이윤_율";
			iCnt = functionPointCalcCostDAO.updateC_SW_PROFIT_RATE(mPs); // 재개발비산정_소프트웨어개발비_이윤_율
			calcPoint = "C79 : C_SW_SURTAX_RATE : 재개발비산정_소프트웨어개발비_부가세_율";
			iCnt = functionPointCalcCostDAO.updateC_SW_SURTAX_RATE(mPs); // 재개발비산정_소프트웨어개발비_부가세_율
			calcPoint = "C80 : C_SW_DCOST_AMT : 재개발비산정_소프트웨어개발비_개발원가_금액   CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_SW_DCOST_AMT(mPs); // 재개발비산정_소프트웨어개발비_개발원가_금액
			calcPoint = "C81 : C_SW_DIRCOST_AMT : 재개발비산정_소프트웨어개발비_직접경비_금액   MMMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_SW_DIRCOST_AMT(mPs); // 재개발비산정_소프트웨어개발비_직접경비_금액
			calcPoint = "C82 : C_SW_DIRCOST2_AMT : 재개발비산정_소프트웨어개발비_직접경비2_금액   MMMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_SW_DIRCOST2_AMT(mPs); // 재개발비산정_소프트웨어개발비_직접경비2_금액
			calcPoint = "C83 : C_SW_DIRCOST3_AMT : 재개발비산정_소프트웨어개발비_직접경비3_금액   MMMMMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_SW_DIRCOST3_AMT(mPs); // 재개발비산정_소프트웨어개발비_직접경비3_금액
			calcPoint = "C84 : C_SW_DIRCOST4_AMT : 재개발비산정_소프트웨어개발비_직접경비4_금액   MMMMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_SW_DIRCOST4_AMT(mPs); // 재개발비산정_소프트웨어개발비_직접경비4_금액
			calcPoint = "C85 : C_SW_DIRCOST5_AMT : 재개발비산정_소프트웨어개발비_직접경비5_금액   MMMMMMMMM";
			iCnt = functionPointCalcCostDAO.updateC_SW_DIRCOST5_AMT(mPs); // 재개발비산정_소프트웨어개발비_직접경비5_금액
			calcPoint = "C86 : C_SW_PROFIT_AMT : 재개발비산정_소프트웨어개발비_이윤_금액  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_SW_PROFIT_AMT(mPs); // 재개발비산정_소프트웨어개발비_이윤_금액
			calcPoint = "C87 : C_SW_SURTAX_AMT : 재개발비산정_소프트웨어개발비_부가세_금액   CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_SW_SURTAX_AMT(mPs); // 재개발비산정_소프트웨어개발비_부가세_금액
			calcPoint = "C88 : C_SW_TOTDEVCOST_AMT : 재개발비산정_소프트웨어개발비_총개발비용_금액  CCCCCCCC";
			iCnt = functionPointCalcCostDAO.updateC_SW_TOTDEVCOST_AMT(mPs); // 재개발비산정_소프트웨어개발비_총개발비용_금액
			calcPoint = "";
			
			resultMap.put("RESULT_CD", "SUCCESS");
			resultMap.put("RESULT_MSG", "-");
			resultMap.put("RESULT_DESC", "-");
		}catch(Exception e){
			resultMap.put("RESULT_CD", "FAIL");
			resultMap.put("RESULT_MSG", "비용계산중 오류가 발생했습니다.");
			//resultMap.put("RESULT_DESC", "</br> - rslt_cost_id : " + rslt_cost_id + "</br> - calcPoint : " + calcPoint + "</br></br>, " + e.getMessage());
			resultMap.put("RESULT_DESC", "</br> - rslt_cost_id : " + rslt_cost_id + "</br> - calcPoint : " + calcPoint + "</br></br>, " + "error");
		}
		return resultMap;
	}
	

	/**
	 * 계산결과 로그 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertRsltCostHist(Map<String, Object> paramMap) throws NkiaException {		

		// 세션정보
		Map<String, Object> mModule = new HashMap<String, Object>();
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		mPs = new HashMap<String, Object>();
		mPs.put("rslt_cost_id", paramMap.get("RSLT_COST_ID")); 
		mPs.put("login_user_id", login_user_id); 
		mPs.put("calc_status", paramMap.get("CALC_STATUS"));
		mPs.put("fp_func_cnt", paramMap.get("FP_FUNC_CNT"));
		mPs.put("fp_func_point", paramMap.get("FP_FUNC_POINT"));
		mPs.put("fp_calc_point", paramMap.get("FP_CALC_POINT"));
		mPs.put("fp_cost", paramMap.get("FP_COST"));
		mPs.put("calc_desc", paramMap.get("CALC_DESC")); 
		mPs.put("result_cd", paramMap.get("RESULT_CD")); 
		mPs.put("result_msg", paramMap.get("RESULT_MSG")); 
		mPs.put("result_desc", paramMap.get("RESULT_DESC")); 
		
		functionPointDAO.insertRsltCostHist(mPs); // 결과정보생성 FP_RSLT_COST
	}
	
}