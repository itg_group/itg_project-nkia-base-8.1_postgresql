/*
 * @(#)FunctionPointServiceImpl.java              2018. 3. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.system.fp.dao.FunctionPointDAO;
import com.nkia.itg.system.fp.service.FunctionPointService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("functionPointService")
public class FunctionPointServiceImpl implements FunctionPointService{
	
	private static final Logger logger = LoggerFactory.getLogger(FunctionPointServiceImpl.class);
	
	@Resource(name = "functionPointDAO")
	public FunctionPointDAO functionPointDAO;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 모델 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectModel(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModel(paramMap);
	}

	/**
	 * 모델 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectModelCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModelCount(paramMap);
	}

	/**
	 * 모델 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModelList(paramMap);
	}

	/**
	 * 모델 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectModelList(paramMap);
	}

	/**
	 * 모델 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertModel(ModelMap paramMap) throws NkiaException {		

		//if("".equals(StringUtil.replaceNull(String.valueOf(paramMap.get("clone_model_id")), ""))){
		//	throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 복제모델아이디"); // 필수 입력 항목을 확인 바랍니다.
		//}
		
		String login_user_id = "";
		Map<String, Object> mPs = null;
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		String clone_model_id = StringUtil.replaceNull(String.valueOf(paramMap.get("clone_model_id")), "");
		String new_model_id = functionPointDAO.selectModelNewId(paramMap);
		if("".equals(clone_model_id)){
			paramMap.put("model_id", new_model_id);
			paramMap.put("login_user_id", login_user_id);
			functionPointDAO.insertModel(paramMap); // 모델 등록
		}else{
			mPs = new HashMap<String, Object>();
			mPs.put("model_id", paramMap.get("clone_model_id"));
			Map<String, Object> cloneModel = functionPointDAO.selectModel(mPs);
			List<Map<String, Object>> cloneModelList = functionPointDAO.selectModelItemList(mPs);
			
			Map<String, Object> model = WebUtil.lowerCaseMapKey(cloneModel);
			model.put("model_id", new_model_id);
			model.put("model_title", paramMap.get("model_title"));
			model.put("use_yn", paramMap.get("use_yn"));
			model.put("login_user_id", login_user_id);
			functionPointDAO.insertModel(model); // 모델 등록
			
			for(int i=0; i<cloneModelList.size(); i++){
				Map map = (Map) cloneModelList.get(i);
				mPs = WebUtil.lowerCaseMapKey(map);
				mPs.put("model_id", new_model_id);
				functionPointDAO.insertModelItem(mPs); // 모델 항목 등록
				if(model.containsKey(map.get("ITEM_ID"))){
					model.put(String.valueOf(map.get("ITEM_ID")).toLowerCase(), map.get("ITEM_VALUE"));
				}
			}
			model.put("login_user_id", login_user_id);
			functionPointDAO.updateModel(model); // 모델 항목값 변경
		}
	}
	
	/**
	 * 모델 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public void updateModel(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.updateModel(paramMap);
	}

	/**
	 * 모델 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public void deleteModel(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.deleteModel(paramMap);
	}

	/**
	 * 변경요청 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectReq(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReq(paramMap);
	}

	/**
	 * 변경요청 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectReqCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReqCount(paramMap);
	}

	/**
	 * 변경요청 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReqList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReqList(paramMap);
	}

	/**
	 * 변경요청 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReqExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectReqList(paramMap);
	}

	/**
	 * 변경요청 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertReq(ModelMap paramMap) throws NkiaException {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.insertReq(paramMap);
	}

	/**
	 * 변경요청 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public void updateReq(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.updateReq(paramMap);
	}

	/**
	 * 변경요청 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public void deleteReq(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.deleteReq(paramMap);
	}
	
	/**
	 * 기능 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectReqFunc(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReqFunc(paramMap);
	}

	/**
	 * 기능 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectReqFuncCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReqFuncCount(paramMap);
	}

	/**
	 * 기능 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReqFuncList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReqFuncList(paramMap);
	}

	/**
	 * 기능 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReqFuncExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectReqFuncList(paramMap);
	}

	/**
	 * 기능 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map insertReqFuncList(ModelMap paramMap) throws NkiaException {
		
		Map resultMap = new HashMap<String, Object>();
		// 세션정보
		Map<String, Object> mModule = new HashMap<String, Object>();
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		if(!paramMap.containsKey("sr_id")){
			throw new NkiaException("필수항목 정보가 없습니다.");
		}
		if(!paramMap.containsKey("INPUT1_LIST")){
			throw new NkiaException("필수항목 정보가 없습니다.");
		}
		
		String mode = (String)paramMap.get("INPUT1_MODE"); // 모드 {preview, save}
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST"); // 저장 목록
		
		//if(list.size() < 1){
		//	throw new NkiaException("저장대상 정보가 없습니다.");
		//}
		
		mPs = new HashMap<String, Object>();
		mPs.put("sr_id", paramMap.get("sr_id"));
		List<Map<String, Object>> reqFuncList = functionPointDAO.selectReqFuncList(mPs);
		
		// 이미계산여부 체크
		if(0 < reqFuncList.size()){
			List<String> calcIdList = new ArrayList<String>();
			for(int i=0; i<reqFuncList.size(); i++){
				String strCALC_STATUS = StringUtil.replaceNull(String.valueOf(reqFuncList.get(i).get("CALC_STATUS")), "");
				String strRSLT_COST_ID = StringUtil.replaceNull(String.valueOf(reqFuncList.get(i).get("RSLT_COST_ID")), "");
				String strCALC_INS_DT = StringUtil.replaceNull(String.valueOf(reqFuncList.get(i).get("CALC_INS_DT")), "");
				if("OK".equals(strCALC_STATUS) && !calcIdList.contains(strRSLT_COST_ID)){
					calcIdList.add(strRSLT_COST_ID);
				}
			}
			if(0 < calcIdList.size()){
				throw new NkiaException("이미 비용산출하여 변경기능을 수정할 수 없습니다. => 비용산출id정보 " + calcIdList);
			}
		}
		
		mPs = new HashMap<String, Object>();
		mPs.put("sr_id", paramMap.get("sr_id"));
		functionPointDAO.deleteReqFuncReqAll(mPs);
		
		mPs = new HashMap<String, Object>();
		Map<String, Object> modelBaseYMap = functionPointDAO.selectModelBaseY(mPs); // 
		if(null == modelBaseYMap){
			throw new NkiaException("기본모델이 존재하지 않습니다..");
		}
		
		double function_point1 = 0.0; // 신규 기능점수
		double function_point2 = 0.0; // 재개발 기능점수
		// 처리
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			map.put("login_user_id", login_user_id);

			map.put("model_id", modelBaseYMap.get("MODEL_ID")); // 모델정보
			
			mPs = new HashMap<String, Object>();
			mPs.put("conf_id", map.get("conf_id"));
			Map<String, Object> confRelTopBottomAppSwMap = functionPointDAO.searchConfRelTopBottomAppSw(mPs); // 분류정보
			if(null == confRelTopBottomAppSwMap){
				throw new NkiaException("변경기능 구성상세정보가 존재하지 않습니다.");
			}
			logger.debug(">>> confRelTopBottomAppSwMap 1 : " + confRelTopBottomAppSwMap);
			
			String func_id = functionPointDAO.selectReqFuncNewId(map); // 기능 신규아이디 체번 

			// sr_id
			map.put("func_id", func_id);
			// conf_id
			// func_type_cd
			// f_title
			// f_desc
			map.put("f_app", confRelTopBottomAppSwMap.get("FP_APP_TYPE"));
			// f_lang
			map.put("f_ilf", StringUtil.replaceNull(String.valueOf(map.get("f_ilf")), "0"));
			map.put("f_eif", StringUtil.replaceNull(String.valueOf(map.get("f_eif")), "0"));
			map.put("f_ei_c", StringUtil.replaceNull(String.valueOf(map.get("f_ei_c")), "0"));
			map.put("f_ei_u", StringUtil.replaceNull(String.valueOf(map.get("f_ei_u")), "0"));
			map.put("f_ei_d", StringUtil.replaceNull(String.valueOf(map.get("f_ei_d")), "0"));
			map.put("f_eo", StringUtil.replaceNull(String.valueOf(map.get("f_eo")), "0"));
			map.put("f_eq", StringUtil.replaceNull(String.valueOf(map.get("f_eq")), "0"));
			map.put("class_id", confRelTopBottomAppSwMap.get("UP_CONF_ID"));
			map.put("class1_id", confRelTopBottomAppSwMap.get("CONF1_ID"));
			map.put("class2_id", confRelTopBottomAppSwMap.get("CONF2_ID"));
			map.put("class3_id", confRelTopBottomAppSwMap.get("CONF3_ID"));
			map.put("class4_id", confRelTopBottomAppSwMap.get("CONF4_ID"));
			map.put("class5_id", confRelTopBottomAppSwMap.get("CONF5_ID"));
			map.put("use_yn", "Y");
			map.put("rslt_point_id", "");
			map.put("rslt_cost_id", "");
			// model_id
			map.put("ci_class_id", confRelTopBottomAppSwMap.get("CONF1_ID")); // 구성분류아이디
			map.put("module_id", confRelTopBottomAppSwMap.get("CONF2_ID")); // 모듈아이디
			map.put("oper_cust_id", confRelTopBottomAppSwMap.get("OPER_CUST_ID")); // 공사관리조직
			map.put("z_sla_contract", confRelTopBottomAppSwMap.get("Z_SLA_CONTRACT")); // 용역계약
			map.put("z_maint_vendor", confRelTopBottomAppSwMap.get("Z_MAINT_VENDOR")); // 용역업체
			
			Map<String, Object> reqFunc = functionPointDAO.insertReqFunc(map); // 기능저장 
			logger.debug(">>> map 2 : " + map);
			
			Map<String, Object> reqFuncCalcInfoMap = functionPointDAO.selectReqFuncCalcInfo(map); // 가중치적용된 계산정보

			logger.debug(">>> map 2 : " + map);

			map.put("f_calc_ilf", reqFuncCalcInfoMap.get("F_CALC_ILF"));
			map.put("f_calc_eif", reqFuncCalcInfoMap.get("F_CALC_EIF"));
			map.put("f_calc_ei", reqFuncCalcInfoMap.get("F_CALC_EI"));
			map.put("f_calc_eo", reqFuncCalcInfoMap.get("F_CALC_EO"));
			map.put("f_calc_eq", reqFuncCalcInfoMap.get("F_CALC_EQ"));
			map.put("f_calc_tot", reqFuncCalcInfoMap.get("F_CALC_TOT"));
			logger.debug(">>> map 3 : " + map);
			functionPointDAO.updateReqFunc(map); // 가중치 계산정보 저장
			
			if("1".equals(map.get("func_type_cd"))){
				function_point1 += Double.parseDouble(reqFuncCalcInfoMap.get("F_CALC_TOT").toString());
			}else if("2".equals(map.get("func_type_cd"))){
				function_point2 += Double.parseDouble(reqFuncCalcInfoMap.get("F_CALC_TOT").toString());
			}
		}
		resultMap.put("function_point1", function_point1);
		resultMap.put("function_point2", function_point2);
		resultMap.put("function_point", (function_point1 + function_point2));
		return resultMap;
	}
	
	/**
	 * 기능 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public void updateReqFunc(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.updateReqFunc(paramMap);
	}

	/**
	 * 모델항목 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectModelItem(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModelItem(paramMap);
	}

	/**
	 * 모델항목 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelItemList(ModelMap paramMap) throws NkiaException {
		Map modelDetail = functionPointDAO.selectModel(paramMap);
		List modelList = functionPointDAO.selectModelItemList(paramMap);
		for(int i=0; i<modelList.size(); i++){
			Map map = (Map) modelList.get(i);
			if(modelDetail.containsKey(map.get("ITEM_ID"))){
				map.put("ITEM_VALUE", modelDetail.get(map.get("ITEM_ID")));
			}
		}
		return modelList;
	}

	/**
	 * 모델항목 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelItemExcelDown(Map paramMap) throws NkiaException {
		Map modelDetail = functionPointDAO.selectModel(paramMap);
		List modelList = functionPointDAO.selectModelItemList(paramMap);
		for(int i=0; i<modelList.size(); i++){
			Map map = (Map) modelList.get(i);
			if(modelDetail.containsKey(map.get("ITEM_ID"))){
				map.put("ITEM_VALUE", modelDetail.get(map.get("ITEM_ID")));
			}
		}
		return modelList;
	}

	/**
	 * 모델항목 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertModelItem(ModelMap paramMap) throws NkiaException {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.insertModelItem(paramMap);
	}

	/**
	 * 모델항목 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public void updateModelItem(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.updateModelItem(paramMap);
	}

	/**
	 * 모델항목 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public void deleteModelItem(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.deleteModelItem(paramMap);
	}
	
	/**
	 * 모델항목  목록저장
	 * @param paramMap
	 */
	public void saveModelItemList(Map paramMap) throws NkiaException {
		
		Map<String, Object> mModule = new HashMap<String, Object>();
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		String mode = (String)paramMap.get("INPUT1_MODE");
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");
		
		for(Map<String, Object> map: list){
			if("".equals(StringUtil.replaceNull(String.valueOf(map.get("model_id")), ""))){
				throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모델아이디"); // 필수 입력 항목을 확인 바랍니다.
			}
			if("".equals(StringUtil.replaceNull(String.valueOf(map.get("item_id")), ""))){
				throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 항목아이디"); // 필수 입력 항목을 확인 바랍니다.
			}
		}
		
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			map.put("login_user_id", login_user_id);
			
			if(null != functionPointDAO.selectModelItem(map)){
				functionPointDAO.updateModelItem(map);
			}else{
				functionPointDAO.insertModelItem(map);
			}
			mModule.put(String.valueOf(map.get("item_id")).toLowerCase(), map.get("item_value")); // 항목값 변경
		}
		
		if(0 < list.size()){
			mModule.put("model_id", list.get(0).get("model_id"));
			functionPointDAO.updateModel(mModule); // 모델기준정보 변경 
		}
	}
	
	/**
	 * 모델모듈 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectModuleItem(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModuleItem(paramMap);
	}

	/**
	 * 모델모듈 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectModuleItemCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModuleItemCount(paramMap);
	}

	/**
	 * 모델모듈 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModuleItemList(ModelMap paramMap) throws NkiaException {
		Map modelDetail = functionPointDAO.selectModel(paramMap);
		List modelItemList = functionPointDAO.selectModelItemList(paramMap);
		Map moduleItem = functionPointDAO.selectModuleItem(paramMap);
		for(int i=0; i<modelItemList.size(); i++){
			Map map = (Map) modelItemList.get(i);
			if(modelDetail.containsKey(map.get("ITEM_ID"))){
				map.put("ITEM_VALUE", modelDetail.get(map.get("ITEM_ID")));
			}
		}
		return modelItemList;
	}

	/**
	 * 모델모듈 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorModuleItemList(ModelMap paramMap) throws NkiaException {
//		Map modelDetail = functionPointDAO.selectModel(paramMap);
		List modelItemList = functionPointDAO.selectVendorModuleItemList(paramMap);
//		Map moduleItem = functionPointDAO.selectModuleItem(paramMap);
//		for(int i=0; i<modelItemList.size(); i++){
//			Map map = (Map) modelItemList.get(i);
//			if(modelDetail.containsKey(map.get("ITEM_ID"))){
//				map.put("INIT_VALUE", modelDetail.get(map.get("ITEM_ID")));
//			}
//		}
		return modelItemList;
	}

	/**
	 * 모델모듈 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorModuleItemExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectVendorModuleItemList(paramMap);
	}
	
	/**
	 * 모델모듈 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorModuleList(ModelMap paramMap) throws NkiaException {
		//Map modelDetail = functionPointDAO.selectModel(paramMap);
		List modelItemList = functionPointDAO.selectVendorModuleList(paramMap);
		return modelItemList;
	}

	/**
	 * 모델모듈 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorModuleExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectVendorModuleList(paramMap);
	}

	/**
	 * 모델모듈 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorList(ModelMap paramMap) throws NkiaException {
		//Map modelDetail = functionPointDAO.selectModel(paramMap);
		List modelItemList = functionPointDAO.selectVendorList(paramMap);
		return modelItemList;
	}

	/**
	 * 모델모듈 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectVendorList(paramMap);
	}
	
	/**
	 * 모델모듈 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModuleItemExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectModuleItemList(paramMap);
	}

	/**
	 * 모델모듈 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertModuleItem(ModelMap paramMap) throws NkiaException {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.insertModuleItem(paramMap);
	}

	/**
	 * 모델모듈 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public void updateModuleItem(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.updateModuleItem(paramMap);
	}

	/**
	 * 모델모듈 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public void deleteModuleItem(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.deleteModuleItem(paramMap);
	}
	
	/**
	 * 모델모듈  목록저장
	 * @param paramMap
	 */
	public void saveModuleItemList(Map paramMap) throws NkiaException {
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		String mode = (String)paramMap.get("INPUT1_mode");
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");
		for(Map<String, Object> map: list){
			if("".equals(StringUtil.replaceNull(String.valueOf(map.get("calc_ym")), ""))){
				throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 산출년월"); // 필수 입력 항목을 확인 바랍니다.
			}
			if("".equals(StringUtil.replaceNull(String.valueOf(map.get("model_id")), ""))){
				throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모델아이디"); // 필수 입력 항목을 확인 바랍니다.
			}
			if("".equals(StringUtil.replaceNull(String.valueOf(map.get("module_id")), ""))){
				throw new NkiaException(messageSource.getMessage("msg.common.00011") + " 모듈아이디"); // 필수 입력 항목을 확인 바랍니다.
			}
		}
		
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			map.put("login_user_id", login_user_id);
			map.put(String.valueOf(map.get("item_id")).toLowerCase(), map.get("item_value"));
			
			if(null != functionPointDAO.selectModuleItem(map)){
				functionPointDAO.updateModuleItem(map);
			}else{
				functionPointDAO.insertModuleItem(map);
			}
		}
	}

	
	/**
	 * 월별산출결과 변경기능 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCompleteSrFuncList(ModelMap paramMap) throws NkiaException {
		List resultList = functionPointDAO.selectCompleteSrFuncList(paramMap);
		return resultList;
	}
	
	/**
	 * 월별산출결과 변경기능 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCompleteSrFuncExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectCompleteSrFuncList(paramMap);
	}
	public int selectCompleteSrFuncCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectCompleteSrFuncCount(paramMap);
	}
	
	/**
	 * 월별산출결과 변경기능 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultModuleFuncList(ModelMap paramMap) throws NkiaException {
		List resultList = functionPointDAO.selectResultModuleFuncList(paramMap);
		return resultList;
	}
	public int selectResultModuleFuncCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectResultModuleFuncCount(paramMap);
	}
	
	/**
	 * 월별산출결과 변경기능 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultModuleFuncExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectResultModuleFuncList(paramMap);
	}

	/**
	 * 월별산출결과 설정항목 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultModuleItemList(ModelMap paramMap) throws NkiaException {
		List resultList = functionPointDAO.selectResultModuleItemList(paramMap);
		return resultList;
	}
	
	/**
	 * 월별산출결과 설정항목 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultModuleItemExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectResultModuleItemList(paramMap);
	}

	/**
	 * 월별산출결과 비용 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultCostList(ModelMap paramMap) throws NkiaException {
		List resultList = functionPointDAO.selectResultCostList(paramMap);
		return resultList;
	}
	
	/**
	 * 월별산출결과 비용 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultCostExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectResultCostList(paramMap);
	}
	
	/**
	 * 기능점수산출 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectRsltPoint(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltPoint(paramMap);
	}

	/**
	 * 기능점수산출 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectRsltPointCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltPointCount(paramMap);
	}

	/**
	 * 기능점수산출 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltPointList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltPointList(paramMap);
	}

	/**
	 * 기능점수산출 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltPointExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectRsltPointList(paramMap);
	}

	/**
	 * 기능점수산출 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertRsltPoint(ModelMap paramMap) throws NkiaException {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.insertRsltPoint(paramMap);
	}

	/**
	 * 기능점수산출 수정
     * @param paramMap
	 * @return
	 * @
	 */	
	public void updateRsltPoint(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.updateRsltPoint(paramMap);
	}

	/**
	 * 기능점수산출 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public void deleteRsltPoint(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.deleteRsltPoint(paramMap);
	}

	/**
	 * 비용산출 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectRsltCost(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltCost(paramMap);
	}

	/**
	 * 비용산출 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectRsltCostCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltCostCount(paramMap);
	}

	/**
	 * 비용산출 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltCostList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltCostList(paramMap);
	}

	/**
	 * 비용산출 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltCostExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectRsltCostList(paramMap);
	}

	/**
	 * 비용산출 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertRsltCost(ModelMap paramMap) throws NkiaException {		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.insertRsltCost(paramMap);
	}

	/**
	 * 비용산출 수정
     * @param paramMap
	 * @return
	 * @
	 */	
	public void updateRsltCost(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.updateRsltCost(paramMap);
	}

	/**
	 * 비용산출 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public void deleteRsltCost(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("login_user_id", userVO.getUser_id());
		}
		functionPointDAO.deleteRsltCost(paramMap);
	}

	/**
	 * 비용산출이력 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectResultCostHistCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectResultCostHistCount(paramMap);
	}

	/**
	 * 비용산출이력 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultCostHistList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectResultCostHistList(paramMap);
	}

	/**
	 * 비용산출이력 엑셀다운로드
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectResultCostHistExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectResultCostHistList(paramMap);
	}

	/**
	 * 어플리케이션 변경실적
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportFunctionPointList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReportFunctionPointList(paramMap);
	}
	
	/**
	 * 어플리케이션 기능점수 조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportFunctionPointExcelDown(Map paramMap) throws NkiaException {
		return functionPointDAO.selectReportFunctionPointList(paramMap);
	}
	
	/**
	 * 어플리케이션 기능점수 개수
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectReportFunctionPointCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReportFunctionPointCount(paramMap);
	}

	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportPointList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReportPointList(paramMap);
	}

	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportPointExcelDown(Map param) throws NkiaException {
		return functionPointDAO.selectReportPointList(param);
	}

	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportCostList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectReportCostList(paramMap);
	}

	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportCostExcelDown(Map param) throws NkiaException {
		return functionPointDAO.selectReportCostList(param);
	}
	
	/**
	 * 구성 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectCiSelPopCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectCiSelPopCount(paramMap);
	}

	/**
	 * 구성 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCiSelPopList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectCiSelPopList(paramMap);
	}

	/**
	 * 구성유형 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectCiTypeSelPopCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectCiTypeSelPopCount(paramMap);
	}

	/**
	 * 구성유형 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCiTypeSelPopList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectCiTypeSelPopList(paramMap);
	}

	/**
	 * 모델 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectModelSelPopCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModelSelPopCount(paramMap);
	}

	/**
	 * 모델 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelSelPopList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModelSelPopList(paramMap);
	}

	/**
	 * 모듈 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModuleSelPopList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectModuleSelPopList(paramMap);
	}

	/**
	 * 용역업체 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectVendorSelPopCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectVendorSelPopCount(paramMap);
	}

	/**
	 * 용역업체 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorSelPopList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectVendorSelPopList(paramMap);
	}

	/**
	 * 계산결과 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectRsltCostSelPopCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltCostSelPopCount(paramMap);
	}

	/**
	 * 계산결과 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltCostSelPopList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectRsltCostSelPopList(paramMap);
	}
	
	/**
	 * 코드정보 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCodeDataList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.selectCodeDataList(paramMap);
	}
	
	/**
	 * 분류트리조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.searchClassTree(paramMap);
	}
	
	/**
	 * 자산목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List searchAssetList(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.searchAssetList(paramMap);
	}
	
	/**
	 * 자산목록 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int searchAssetListCount(ModelMap paramMap) throws NkiaException {
		return functionPointDAO.searchAssetListCount(paramMap);
	}
	
}