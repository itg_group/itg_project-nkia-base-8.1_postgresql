/*
 * @(#)FunctionPointService.java              2018. 3. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface FunctionPointService {

	/*
	* FP_MODEL : 모델
	*/
	public Map selectModel(ModelMap paramMap) throws NkiaException;

	public int selectModelCount(ModelMap paramMap) throws NkiaException;
	
	public List selectModelList(ModelMap paramMap) throws NkiaException;

	public List selectModelExcelDown(Map paramMap) throws NkiaException;

	public void insertModel(ModelMap paramMap) throws NkiaException;
	
	public void updateModel(ModelMap paramMap) throws NkiaException;
	
	public void deleteModel(ModelMap paramMap) throws NkiaException;

	/*
	* FP_REQ 
	*/
	public Map selectReq(ModelMap paramMap) throws NkiaException;

	public int selectReqCount(ModelMap paramMap) throws NkiaException;
	
	public List selectReqList(ModelMap paramMap) throws NkiaException;

	public List selectReqExcelDown(Map paramMap) throws NkiaException;

	public void insertReq(ModelMap paramMap) throws NkiaException;
	
	public void updateReq(ModelMap paramMap) throws NkiaException;
	
	public void deleteReq(ModelMap paramMap) throws NkiaException;

	/*
	* FP_REQ_FUNC : 기능
	*/
	public Map selectReqFunc(ModelMap paramMap) throws NkiaException;

	public int selectReqFuncCount(ModelMap paramMap) throws NkiaException;
	
	public List selectReqFuncList(ModelMap paramMap) throws NkiaException;

	public List selectReqFuncExcelDown(Map paramMap) throws NkiaException;

	public Map insertReqFuncList(ModelMap paramMap) throws NkiaException;
	
	public void updateReqFunc(ModelMap paramMap) throws NkiaException;
	
	/*
	* FP_MODEL_ITEM : 모델항목
	*/
	public Map selectModelItem(ModelMap paramMap) throws NkiaException;

	public List selectModelItemList(ModelMap paramMap) throws NkiaException;
	public List selectModelItemExcelDown(Map paramMap) throws NkiaException;
	
	public void insertModelItem(ModelMap paramMap) throws NkiaException;
	
	public void updateModelItem(ModelMap paramMap) throws NkiaException;
	
	public void deleteModelItem(ModelMap paramMap) throws NkiaException;
	
	public void saveModelItemList(Map paramMap) throws NkiaException;
	
	/*
	* FP_MODEL_MODULE : 모델모듈
	*/
	public Map selectModuleItem(ModelMap paramMap) throws NkiaException;

	public int selectModuleItemCount(ModelMap paramMap) throws NkiaException;
	
	public List selectModuleItemList(ModelMap paramMap) throws NkiaException;

	public List selectModuleItemExcelDown(Map paramMap) throws NkiaException;

	public void insertModuleItem(ModelMap paramMap) throws NkiaException;
	
	public void updateModuleItem(ModelMap paramMap) throws NkiaException;
	
	public void deleteModuleItem(ModelMap paramMap) throws NkiaException;

	public void saveModuleItemList(Map paramMap) throws NkiaException;
	
	/*
	* FP_RSLT_POINT : 결과 기능값
	*/
	public Map selectRsltPoint(ModelMap paramMap) throws NkiaException;

	public int selectRsltPointCount(ModelMap paramMap) throws NkiaException;
	
	public List selectRsltPointList(ModelMap paramMap) throws NkiaException;

	public List selectRsltPointExcelDown(Map paramMap) throws NkiaException;

	public void insertRsltPoint(ModelMap paramMap) throws NkiaException;
	
	public void updateRsltPoint(ModelMap paramMap) throws NkiaException;
	
	public void deleteRsltPoint(ModelMap paramMap) throws NkiaException;
	
	/*
	* FP_RSLT_COST : 결과 비용
	*/
	public Map selectRsltCost(ModelMap paramMap) throws NkiaException;
	
	public int selectRsltCostCount(ModelMap paramMap) throws NkiaException;
	public List selectRsltCostList(ModelMap paramMap) throws NkiaException;
	public List selectRsltCostExcelDown(Map paramMap) throws NkiaException;
	public void insertRsltCost(ModelMap paramMap) throws NkiaException;
	public void updateRsltCost(ModelMap paramMap) throws NkiaException;
	public void deleteRsltCost(ModelMap paramMap) throws NkiaException;
	
	public List selectVendorModuleItemList(ModelMap paramMap) throws NkiaException;
	public List selectVendorModuleItemExcelDown(Map paramMap) throws NkiaException;
	public List selectVendorModuleList(ModelMap paramMap) throws NkiaException;
	public List selectVendorModuleExcelDown(Map paramMap) throws NkiaException;
	public List selectVendorList(ModelMap paramMap) throws NkiaException;
	public List selectVendorExcelDown(Map paramMap) throws NkiaException;
	
	public List selectCompleteSrFuncList(ModelMap paramMap) throws NkiaException;
	public List selectCompleteSrFuncExcelDown(Map paramMap) throws NkiaException;
	public int selectCompleteSrFuncCount(ModelMap paramMap) throws NkiaException;
	public List selectResultModuleFuncList(ModelMap paramMap) throws NkiaException;
	public int selectResultModuleFuncCount(ModelMap paramMap) throws NkiaException;
	public List selectResultModuleFuncExcelDown(Map paramMap) throws NkiaException;
	public List selectResultModuleItemList(ModelMap paramMap) throws NkiaException;
	public List selectResultModuleItemExcelDown(Map paramMap) throws NkiaException;
	public List selectResultCostList(ModelMap paramMap) throws NkiaException;
	public List selectResultCostExcelDown(Map paramMap) throws NkiaException;
	public int selectResultCostHistCount(ModelMap paramMap) throws NkiaException;
	public List selectResultCostHistList(ModelMap paramMap) throws NkiaException;
	public List selectResultCostHistExcelDown(Map paramMap) throws NkiaException;
	
	/*
	* 보고서 : 어플리케이션 변경실적
	*/
	public int selectReportFunctionPointCount(ModelMap paramMap) throws NkiaException;
	public List selectReportFunctionPointList(ModelMap paramMap) throws NkiaException;
	public List selectReportFunctionPointExcelDown(Map paramMap) throws NkiaException;

	/*
	* 보고서 : 기간 별 SW개발비 보고서
	*/
	public List selectReportPointList(ModelMap paramMap) throws NkiaException;
	public List selectReportPointExcelDown(Map param) throws NkiaException;
	
	/*
	* 보고서 : 기간 별 SW개발비 보고서
	*/
	public List selectReportCostList(ModelMap paramMap) throws NkiaException;
	public List selectReportCostExcelDown(Map param) throws NkiaException;
	

	/*
	* 선택팝업
	*/
	public int selectCiSelPopCount(ModelMap paramMap) throws NkiaException;
	
	public List selectCiSelPopList(ModelMap paramMap) throws NkiaException;

	public int selectCiTypeSelPopCount(ModelMap paramMap) throws NkiaException;
	
	public List selectCiTypeSelPopList(ModelMap paramMap) throws NkiaException;

	public int selectModelSelPopCount(ModelMap paramMap) throws NkiaException;
	
	public List selectModelSelPopList(ModelMap paramMap) throws NkiaException;

	public List selectModuleSelPopList(ModelMap paramMap) throws NkiaException;

	public int selectVendorSelPopCount(ModelMap paramMap) throws NkiaException;
	
	public List selectVendorSelPopList(ModelMap paramMap) throws NkiaException;

	public int selectRsltCostSelPopCount(ModelMap paramMap) throws NkiaException;
	
	public List selectRsltCostSelPopList(ModelMap paramMap) throws NkiaException;
	
	/*
	* 코드정보
	*/
	public List selectCodeDataList(ModelMap paramMap) throws NkiaException;

	public List searchClassTree(ModelMap paramMap) throws NkiaException;
	public List searchAssetList(ModelMap paramMap)  throws NkiaException;
	public int searchAssetListCount(ModelMap paramMap) throws NkiaException;
}
