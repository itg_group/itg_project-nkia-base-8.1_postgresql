/*
 * @(#)FunctionPointCalcCostDAO.java              2018. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("functionPointCalcCostDAO")
public class FunctionPointCalcCostDAO extends EgovComAbstractDAO{

	/** * 재개발비산정_보정계수산정_어플리케이션유형_업무처리용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_BUSINESS_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_BUSINESS_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_과학기술용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_SCIENCE_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_SCIENCE_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_멀티미디어용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_MULTIM_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_MULTIM_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_지능정보용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_INTELLI_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_INTELLI_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_시스템용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_SYSTEM_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_SYSTEM_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_통신제어용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_COMCT_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_COMCT_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_공정제어용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_PROCCTR_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_PROCCTR_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_지휘통제용_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_COMNDCTR_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_COMNDCTR_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_업무처리용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_BUSINESS_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_BUSINESS_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_과학기술용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_SCIENCE_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_SCIENCE_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_멀티미디어용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_MULTIM_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_MULTIM_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_지능정보용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_INTELLI_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_INTELLI_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_시스템용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_SYSTEM_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_SYSTEM_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_통신제어용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_COMCT_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_COMCT_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_공정제어용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_PROCCTR_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_PROCCTR_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_지휘통제용_비중변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_COMNDCTR_IMP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_COMNDCTR_IMP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_어플리케이션유형_보정계수값변경 * @param paramMap * @return int @ */
	public int updateC_CF_APP_CORFACTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_APP_CORFACTVALUE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_개발단계_분석변경 * @param paramMap * @return int @ */
	public int updateC_CF_DEVSTEP_ANALY(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_DEVSTEP_ANALY", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_개발단계_설계변경 * @param paramMap * @return int @ */
	public int updateC_CF_DEVSTEP_DESIGN(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_DEVSTEP_DESIGN", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_개발단계_구현변경 * @param paramMap * @return int @ */
	public int updateC_CF_DEVSTEP_DEVELOP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_DEVSTEP_DEVELOP", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_개발단계_시험변경 * @param paramMap * @return int @ */
	public int updateC_CF_DEVSTEP_TEST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_DEVSTEP_TEST", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level1_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV1_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV1_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level2_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV2_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV2_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level3_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV3_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV3_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level4_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV4_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV4_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level5_보정계수변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV5_CORFACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV5_CORFACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level1_비중(신규)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV1_IMPNEW(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV1_IMPNEW", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level2_비중(신규)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV2_IMPNEW(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV2_IMPNEW", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level3_비중(신규)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV3_IMPNEW(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV3_IMPNEW", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level4_비중(신규)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV4_IMPNEW(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV4_IMPNEW", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level5_비중(신규)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV5_IMPNEW(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV5_IMPNEW", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level1_비중(재개발)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV1_IMPRE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV1_IMPRE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level2_비중(재개발)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV2_IMPRE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV2_IMPRE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level3_비중(재개발)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV3_IMPRE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV3_IMPRE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level4_비중(재개발)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV4_IMPRE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV4_IMPRE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_level5_비중(재개발)변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG_LEV5_IMPRE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG_LEV5_IMPRE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_1_보정계수값변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG1_CORFACTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG1_CORFACTVALUE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_언어_2_보정계수값변경 * @param paramMap * @return int @ */
	public int updateC_CF_LANG2_CORFACTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_LANG2_CORFACTVALUE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_규모_신규_보정계수값변경 * @param paramMap * @return int @ */
	public int updateC_CF_SCALE_NEW_CORFACTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_SCALE_NEW_CORFACTVALUE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_규모_재개발_보정계수값변경 * @param paramMap * @return int @ */
	public int updateC_CF_SCALE_RE_CORFACTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_SCALE_RE_CORFACTVALUE", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_품질및특성_분산처리_영향도변경 * @param paramMap * @return int @ */
	public int updateC_CF_QLT_VARIMPACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_QLT_VARIMPACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_품질및특성_성능_영향도변경 * @param paramMap * @return int @ */
	public int updateC_CF_QLT_PERF_IMPACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_QLT_PERF_IMPACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_품질및특성_신뢰성_영향도변경 * @param paramMap * @return int @ */
	public int updateC_CF_QLT_TRUST_IMPACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_QLT_TRUST_IMPACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_품질및특성_다중사이트_영향도변경 * @param paramMap * @return int @ */
	public int updateC_CF_QLT_MULTIS_IMPACT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_QLT_MULTIS_IMPACT", functionPointInfo);
	}

	/** * 재개발비산정_보정계수산정_품질및특성_보정계수값변경 * @param paramMap * @return int @ */
	public int updateC_CF_QLT_CORFACTVALUE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_CF_QLT_CORFACTVALUE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_분석_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_ANALY_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_ANALY_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_설계_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_DESIGN_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_DESIGN_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_구현_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_DEVELOP_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_DEVELOP_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_시험_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_TEST_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_TEST_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_총계_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_TOT_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_TOT_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_총기능점수변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_TOTFP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_TOTFP", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_언어1변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_LANG1(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_LANG1", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_언어2변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_LANG2(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_LANG2", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_어플리케이션유형변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_APP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_APP", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_규모변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_SCALE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_SCALE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_품질및특성변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_QLT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_QLT", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_분석_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_ANALY_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_ANALY_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_설계_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_DESIGN_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_DESIGN_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_구현_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_DEVELOP_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_DEVELOP_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_시험_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_TEST_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_TEST_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_신규개발_총계_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_NEW_TOT_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_NEW_TOT_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_분석_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_ANALY_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_ANALY_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_설계_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_DESIGN_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_DESIGN_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_구현_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_DEVELOP_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_DEVELOP_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_시험_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_TEST_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_TEST_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_총계_기능점수단가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_TOT_UPRICE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_TOT_UPRICE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_총기능점수변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_TOTFP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_TOTFP", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_언어1변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_LANG1(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_LANG1", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_언어2변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_LANG2(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_LANG2", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_어플리케이션유형변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_APP(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_APP", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_규모변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_SCALE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_SCALE", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_품질및특성변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_QLT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_QLT", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_분석_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_ANALY_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_ANALY_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_설계_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_DESIGN_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_DESIGN_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_구현_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_DEVELOP_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_DEVELOP_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_시험_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_TEST_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_TEST_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_개발원가산정_재개발_총계_개발원가변경 * @param paramMap * @return int @ */
	public int updateC_DEV_RE_TOT_DCOST(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_DEV_RE_TOT_DCOST", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_이윤_율변경 * @param paramMap * @return int @ */
	public int updateC_SW_PROFIT_RATE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_PROFIT_RATE", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_부가세_율변경 * @param paramMap * @return int @ */
	public int updateC_SW_SURTAX_RATE(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_SURTAX_RATE", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_개발원가_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_DCOST_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_DCOST_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_직접경비_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_DIRCOST_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_DIRCOST_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_직접경비2_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_DIRCOST2_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_DIRCOST2_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_직접경비3_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_DIRCOST3_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_DIRCOST3_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_직접경비4_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_DIRCOST4_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_DIRCOST4_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_직접경비5_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_DIRCOST5_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_DIRCOST5_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_이윤_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_PROFIT_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_PROFIT_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_부가세_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_SURTAX_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_SURTAX_AMT", functionPointInfo);
	}

	/** * 재개발비산정_소프트웨어개발비_총개발비용_금액변경 * @param paramMap * @return int @ */
	public int updateC_SW_TOTDEVCOST_AMT(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateC_SW_TOTDEVCOST_AMT", functionPointInfo);
	}

	
	

	/** * 비용계산 관려정보 저장 */
	public int updateReqFuncCalcReset(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateReqFuncCalcReset", functionPointInfo);
	}
	/** * 비용계산 관려정보 저장 */
	public int updateReqFuncCalcSet(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateReqFuncCalcSet", functionPointInfo);
	}
	/** * 비용계산 관려정보 저장 */
	public List selectReqFuncCalcList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointCalcCostDAO.selectReqFuncCalcList", paramMap);
	}
	/** * 비용계산 관려정보 저장 */
	public int selectReqFuncCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointCalcCostDAO.selectReqFuncCount", paramMap);
	}
	/** * 비용계산 관려정보 저장 */
	public int updateRsltPointCalcInfo(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateRsltPointCalcInfo", functionPointInfo);
	}
	/** * 비용계산 관려정보 저장 */
	public int updateRsltCostCalcInfo(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateRsltCostCalcInfo", functionPointInfo);
	}
	/** * 비용계산 관려정보 저장 */
	public int updateAmFpBasicCalcInfo(Map<String, Object> functionPointInfo) throws NkiaException {
		return this.update("FunctionPointCalcCostDAO.updateAmFpBasicCalcInfo", functionPointInfo);
	}
	
}

