/*
 * @(#)FunctionPointController.java              2018. 3. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.system.fp.service.FunctionPointCalcService;
import com.nkia.itg.system.fp.service.FunctionPointService;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class FunctionPointController {

	private static final Logger logger = LoggerFactory.getLogger(FunctionPointController.class);
	
	@Resource(name = "functionPointService")
	private FunctionPointService functionPointService;

	@Resource(name = "functionPointCalcService")
	private FunctionPointCalcService functionPointCalcService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 모델정보관리
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goModelManager.do")
	public String moduleFpGrpSet(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/fp/modelManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 모델항목설정
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goModelItemConfig.do")
	public String goModelItemConfig(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/fp/modelItemConfig.nvf";
		return forwarPage;
	}
	
	/**
	 * 모델항목설정 - 기능산출
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goRequestManager.do")
	public String goRequestManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/fp/requestManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 모델항목설정 - 비용계산
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goModelItemConfigCost.do")
	public String goModelItemConfigCost(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/modelItemConfigCost.nvf";
		return forwarPage;
	}

	/**
	 * 월 산출정보설정
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goMonthCalcSet.do")
	public String goMonthCalcSet(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/fp/monthCalcSet.nvf";
		return forwarPage;
	}

	/**
	 * 월 산출실행
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goMonthCalcRun.do")
	public String goMonthCalcRun(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/fp/monthCalcRun.nvf";
		return forwarPage;
	}

	/**
	 * 월 산출실행
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goMonthVendorCalcRun.do")
	public String goMonthVendorCalcRun(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/system/fp/monthVendorCalcRun.nvf";
		return forwarPage;
	}

	/**
	 * 월 산출결과
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goSearchCompleteSrFunc.do")
	public String goSearchCompleteSrFunc(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/searchCompleteSrFunc.nvf";
		return forwarPage;
	}
	
	/**
	 * 계산결과탭 기본Tab 화면
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goResultBaseTab.do")
	public String goResultBaseTab(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/resultBaseTab.nvf";
		return forwarPage;
	}
	
	/**
	 * 계산결과 모듈기능정보 화면
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goResultModuleFunc.do")
	public String goResultModuleFunc(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/resultModuleFunc.nvf";
		return forwarPage;
	}
	
	/**
	 * 게산결과 비용정보화면
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goResultCost.do")
	public String goResultCost(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/resultCost.nvf";
		return forwarPage;
	}
	
	/**
	 * 계산결과 비용이력 화면
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goResultCostHist.do")
	public String goResultCostHist(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/resultCostHist.nvf";
		return forwarPage;
	}
	
	/**
	 * 계산결과 모듈항목 목록화면
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goResultModuleItemList.do")
	public String goResultModuleItemList(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/resultModuleItemList.nvf";
		paramMap.put("rslt_cost_id", StringUtil.replaceNull(request.getParameter("rslt_cost_id"), ""));
		return forwarPage;
	}
	
	/**
	 * 계산결과 모듈항목 보고서 화면
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goResultModuleItemReport.do")
	public String goResultModuleItemReport(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/fp/resultModuleItemReport.nvf";
		paramMap.put("rslt_cost_id", StringUtil.replaceNull(request.getParameter("rslt_cost_id"), ""));
		return forwarPage;
	}
	
	/**
	 * 어플리케이션 변경실적 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goReportPoint.do")
	public String goReportPoint(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/fp/reportPoint.nvf";
		return forwarPage;
	}

	/**
	 * 어플리케이션 개발비용 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goReportCost.do")
	public String goReportCost(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
				
		String forwarPage = "/itg/system/fp/reportCost.nvf";
		return forwarPage;
	}
	
	/**
	 * 어플리케이션 기능점수 조회
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/goSearchFunctionPoint.do")
	public String goSearchFunctionPoint(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/fp/searchFunctionPoint.nvf";
		return forwarPage;
	}
	
	/**
	 * 모형 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectModelList.do")
	public @ResponseBody ResultVO selectModelList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectModelList(paramMap);		
			totalCount = functionPointService.selectModelCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 모형 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectModelExcelDown.do")
	public @ResponseBody ResultVO selectModelExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectModelExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 모형 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/insertModel.do")
	public @ResponseBody ResultVO insertModel(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			functionPointService.insertModel(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 모형 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/updateModel.do")
	public @ResponseBody ResultVO updateModel(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			functionPointService.updateModel(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 모형 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/deleteModel.do")
	public @ResponseBody ResultVO deleteModel(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			functionPointService.deleteModel(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 변경요청 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectReqList.do")
	public @ResponseBody ResultVO selectReqList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectReqList(paramMap);		
			totalCount = functionPointService.selectReqCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectReqExcelDown.do")
	public @ResponseBody ResultVO selectReqExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectReqExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/insertReq.do")
	public @ResponseBody ResultVO insertReq(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			functionPointService.insertReq(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 변경요청 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/updateReq.do")
	public @ResponseBody ResultVO updateReq(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			functionPointService.updateReq(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/deleteReq.do")
	public @ResponseBody ResultVO deleteReq(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			functionPointService.deleteReq(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * CI기능 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectReqFuncList.do")
	public @ResponseBody ResultVO selectReqFuncList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			//int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectReqFuncList(paramMap);		
			//totalCount = functionPointService.selectReqFuncCount(paramMap);
			
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * CI기능 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectReqFuncExcelDown.do")
	public @ResponseBody ResultVO selectReqFuncExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectReqFuncExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * CI기능 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/insertReqFuncList.do")
	public @ResponseBody ResultVO insertReqFuncList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			Map resultMap = functionPointService.insertReqFuncList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * CI기능 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/updateReqFunc.do")
	public @ResponseBody ResultVO updateReqFunc(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			functionPointService.updateReqFunc(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 모형항목 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectModelItemList.do")
	public @ResponseBody ResultVO selectModelItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectModelItemList(paramMap);		
			//totalCount = functionPointService.selectModelItemCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectModelItemExcelDown.do")
	public @ResponseBody ResultVO selectModelItemExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectModelItemExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 모형항목 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/insertModelItem.do")
	public @ResponseBody ResultVO insertModelItem(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			functionPointService.insertModelItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 모형항목 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/updateModelItem.do")
	public @ResponseBody ResultVO updateModelItem(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			functionPointService.updateModelItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 모형항목 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/deleteModelItem.do")
	public @ResponseBody ResultVO deleteModelItem(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			functionPointService.deleteModelItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 모형항목 목록저장
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value="/itg/workflow/fp/saveModelItemList.do")
	public @ResponseBody
	ResultVO saveModelItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			functionPointService.saveModelItemList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 모형모듈 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectModuleItemList.do")
	public @ResponseBody ResultVO selectModuleItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectModuleItemList(paramMap);		
			//totalCount = functionPointService.selectModuleItemCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 모형모듈 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectModuleItemExcelDown.do")
	public @ResponseBody ResultVO selectModuleItemExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectModuleItemExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 모형모듈 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/insertModuleItem.do")
	public @ResponseBody ResultVO insertModuleItem(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			functionPointService.insertModuleItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 모형모듈 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/updateModuleItem.do")
	public @ResponseBody ResultVO updateModuleItem(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			functionPointService.updateModuleItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 모형모듈 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/deleteModuleItem.do")
	public @ResponseBody ResultVO deleteModuleItem(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			functionPointService.deleteModuleItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 모형모듈 목록저장
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value="/itg/workflow/fp/saveModuleItemList.do")
	public @ResponseBody
	ResultVO saveModuleItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			functionPointService.saveModuleItemList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 기능점수산출 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectRsltPointList.do")
	public @ResponseBody ResultVO selectRsltPointList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectRsltPointList(paramMap);		
			totalCount = functionPointService.selectRsltPointCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 기능점수산출 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectRsltPointExcelDown.do")
	public @ResponseBody ResultVO selectRsltPointExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectRsltPointExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 기능점수산출 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/insertRsltPoint.do")
	public @ResponseBody ResultVO insertRsltPoint(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			functionPointService.insertRsltPoint(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 기능점수산출 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/updateRsltPoint.do")
	public @ResponseBody ResultVO updateRsltPoint(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			functionPointService.updateRsltPoint(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 기능점수산출 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/deleteRsltPoint.do")
	public @ResponseBody ResultVO deleteRsltPoint(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			functionPointService.deleteRsltPoint(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 비용계산결과 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectRsltCostList.do")
	public @ResponseBody ResultVO selectRsltCostList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectRsltCostList(paramMap);		
			totalCount = functionPointService.selectRsltCostCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 비용계산결과 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectRsltCostExcelDown.do")
	public @ResponseBody ResultVO selectRsltCostExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectRsltCostExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 비용계산결과 등록
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/insertRsltCost.do")
	public @ResponseBody ResultVO insertRsltCost(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			functionPointService.insertRsltCost(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 비용계산결과 수정 
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/updateRsltCost.do")
	public @ResponseBody ResultVO updateRsltCost(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			functionPointService.updateRsltCost(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 비용계산결과 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/deleteRsltCost.do")
	public @ResponseBody ResultVO deleteRsltCost(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			functionPointService.deleteRsltCost(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 비용계산결과이력 리스트 출력
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectResultCostHistList.do")
	public @ResponseBody ResultVO selectResultCostHistList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectResultCostHistList(paramMap);		
			totalCount = functionPointService.selectResultCostHistCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 비용계산결과이력 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectResultCostHistExcelDown.do")
	public @ResponseBody ResultVO selectResultCostHistExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectResultCostHistExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 어플리케이션 변경실적
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectVendorModuleItemList.do")
	public @ResponseBody ResultVO selectVendorModuleItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectVendorModuleItemList(paramMap);		
			//totalCount = functionPointService.selectVendorModuleItemCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectVendorModuleItemExcelDown.do")
	public @ResponseBody ResultVO selectVendorModuleItemExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectVendorModuleItemExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 어플리케이션 변경실적
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectVendorModuleList.do")
	public @ResponseBody ResultVO selectVendorModuleList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectVendorModuleList(paramMap);		
			//totalCount = functionPointService.selectVendorModuleCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectVendorModuleExcelDown.do")
	public @ResponseBody ResultVO selectVendorModuleExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectVendorModuleExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 어플리케이션 변경실적
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectVendorList.do")
	public @ResponseBody ResultVO selectVendorList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectVendorList(paramMap);		
			//totalCount = functionPointService.selectVendorCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectVendorExcelDown.do")
	public @ResponseBody ResultVO selectVendorExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectVendorExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectCompleteSrFuncList.do")
	public @ResponseBody ResultVO selectCompleteSrFuncList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectCompleteSrFuncList(paramMap);		
			totalCount = functionPointService.selectCompleteSrFuncCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectCompleteSrFuncExcelDown.do")
	public @ResponseBody ResultVO selectCompleteSrFuncExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectCompleteSrFuncExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectResultModuleFuncList.do")
	public @ResponseBody ResultVO selectResultModuleFuncList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectResultModuleFuncList(paramMap);		
			totalCount = functionPointService.selectResultModuleFuncCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectResultModuleFuncExcelDown.do")
	public @ResponseBody ResultVO selectResultModuleFuncExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectResultModuleFuncExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 모듈별항목 결과1 정보
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectResultModuleItemList.do")
	public @ResponseBody ResultVO selectResultModuleItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectResultModuleItemList(paramMap);		
			//totalCount = functionPointService.selectResultModuleItemCount(paramMap);
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			for(int i=0; i<resultList.size(); i++){
				Map<String, Object> map = (Map<String, Object>)resultList.get(i);
				resultMap.put(String.valueOf(map.get("ALIAS_NM")).toLowerCase(), map.get("ITEM_VALUE"));
			}
			
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 모듈별항목 결과2 정보
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectResultModuleItemReportList.do")
	public @ResponseBody ResultVO selectResultModuleItemReportList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Map<String, Object> resultMap = new HashMap<String, Object>();
			List resultModuleItemList = functionPointService.selectResultModuleItemList(paramMap);
			for(int i=0; i<resultModuleItemList.size(); i++){
				Map<String, Object> map = (Map<String, Object>)resultModuleItemList.get(i);
				resultMap.put(String.valueOf(map.get("ALIAS_NM")).toLowerCase(), map.get("ITEM_VALUE"));
			}
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectResultModuleItemExcelDown.do")
	public @ResponseBody ResultVO selectResultModuleItemExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectResultModuleItemExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectResultCostList.do")
	public @ResponseBody ResultVO selectResultCostList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectResultCostList(paramMap);		
			//totalCount = functionPointService.selectResultCostCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectResultCostExcelDown.do")
	public @ResponseBody ResultVO selectResultCostExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectResultCostExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 어플리케이션 변경실적
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectReportFunctionPointList.do")
	public @ResponseBody ResultVO selectReportFunctionPointList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectReportFunctionPointList(paramMap);		
			totalCount = functionPointService.selectReportFunctionPointCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectReportFunctionPointExcelDown.do")
	public @ResponseBody ResultVO selectReportFunctionPointExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectReportFunctionPointExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectReportPointList.do")
	public @ResponseBody ResultVO selectReportPointList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectReportPointList(paramMap);		
			//totalCount = functionPointService.selectReportPointCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectReportPointExcelDown.do")
	public @ResponseBody ResultVO selectReportPointExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectReportPointExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 기간 별 SW개발비 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/workflow/fp/selectReportCostList.do")
	public @ResponseBody ResultVO selectReportCostList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectReportCostList(paramMap);		
			//totalCount = functionPointService.selectReportCostCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 변경요청 엑셀 출력
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value="/itg/workflow/fp/selectReportCostExcelDown.do")
	public @ResponseBody ResultVO selectReportCostExcelDown(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");
			List resultList = functionPointService.selectReportCostExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 구성 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectCiSelPopList.do")
	public @ResponseBody ResultVO selectCiSelPopList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectCiSelPopList(paramMap);		
			totalCount = functionPointService.selectCiSelPopCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 구성유형 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectCiTypeSelPopList.do")
	public @ResponseBody ResultVO selectCiTypeSelPopList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectCiTypeSelPopList(paramMap);		
			totalCount = functionPointService.selectCiTypeSelPopCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 모형 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectModelSelPopList.do")
	public @ResponseBody ResultVO selectModelSelPopList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectModelSelPopList(paramMap);		
			totalCount = functionPointService.selectModelSelPopCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 모듈 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectModuleSelPopList.do")
	public @ResponseBody ResultVO selectModuleSelPopList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectModuleSelPopList(paramMap);		
			//totalCount = functionPointService.selectModuleSelPopCount(paramMap);
		
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 용역업체 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectVendorSelPopList.do")
	public @ResponseBody ResultVO selectVendorSelPopList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectVendorSelPopList(paramMap);		
			totalCount = functionPointService.selectVendorSelPopCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 계산결과 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/selectRsltCostSelPopList.do")
	public @ResponseBody ResultVO selectRsltCostSelPopList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.selectRsltCostSelPopList(paramMap);		
			totalCount = functionPointService.selectRsltCostSelPopCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 기능점수 계산
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/calcPoint.do")
	public @ResponseBody ResultVO calcPoint(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			Map resultMap = functionPointCalcService.calcPoint(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 비용 계산
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/calcRun.do")
	public @ResponseBody ResultVO calcCost(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Map resultMap = functionPointCalcService.calcRun(paramMap);
			functionPointCalcService.insertRsltCostHist(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 코드정보조회
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/workflow/fp/selectCodeDataList.do")
	public @ResponseBody
	ResultVO searchCodeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			gridVO.setRows(functionPointService.selectCodeDataList(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	

	/**
	 * 자산현황조회 - 분류체계정보
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/workflow/fp/searchClassTree.do")
	public @ResponseBody ResultVO searchClassTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = functionPointService.searchClassTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	

	/**
	 * 자산현황 ROW CNT
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/searchAssetListCount.do")
	public @ResponseBody ResultVO searchAssetListCount(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			totalCount = functionPointService.searchAssetListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 자산현황 리스트
     * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/workflow/fp/searchAssetList.do")
	public @ResponseBody ResultVO searchAssetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			
			List resultList = functionPointService.searchAssetList(paramMap);		
			totalCount = functionPointService.searchAssetListCount(paramMap);
		
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
