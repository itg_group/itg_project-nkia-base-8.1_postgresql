/*
 * @(#)FunctionPointService.java              2018. 3. 20.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.service;

import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface FunctionPointCalcService {
	
	public Map<String, Object> calcRun(Map<String, Object> paramMap) throws NkiaException;
	
	public Map<String, Object> calcPoint(Map<String, Object> paramMap) throws NkiaException;
	
	public Map<String, Object> calcCost(Map<String, Object> paramMap) throws NkiaException;
	
	public void insertRsltCostHist(Map<String, Object> paramMap) throws NkiaException;

}
