/*
 * @(#)FunctionPointDAO.java              2018. 3. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.fp.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("functionPointDAO")
public class FunctionPointDAO extends EgovComAbstractDAO{
	
	/**
	 * 모델 단건조회 UseYn='Y' 정보
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectModelBaseY(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectModelBaseY", paramMap);
	}
	
	/**
	 * 모델 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectModel(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectModel", paramMap);
	}

	/**
	 * 모델 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectModelCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectModelCount", paramMap);
	}

	/**
	 * 모델 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectModelList", paramMap);
	}

	/**
	 * 모델등록 신규아이디
     * @param paramMap
	 * @return
	 * @
	 */	
	public String selectModelNewId(Map<String, Object> param) throws NkiaException {
		return (String)selectByPk("FunctionPointDAO.selectModelNewId", param);
	}
	
	/**
	 * 모델등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertModel(Map<String, Object> param) throws NkiaException {
		if("Y".equals(param.get("base_yn"))){
			this.update("FunctionPointDAO.updateModelAllBaseN", param);
		}
		this.insert("FunctionPointDAO.insertModel", param);
	}

	/**
	 * 모델변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateModel(Map<String, Object> param) throws NkiaException {
		if("Y".equals(param.get("base_yn"))){
			this.update("FunctionPointDAO.updateModelAllBaseN", param);
		}
		return this.update("FunctionPointDAO.updateModel", param);
	}

	/**
	 * 모델삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteModel(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteModel", paramMap);
	}

	/**
	 * 기능 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectReq(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectReq", paramMap);
	}

	/**
	 * 기능 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectReqCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectReqCount", paramMap);
	}

	/**
	 * 기능 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReqList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectReqList", paramMap);
	}

	/**
	 * 기능등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertReq(Map<String, Object> param) throws NkiaException {
		this.insert("FunctionPointDAO.insertReq", param);
	}

	/**
	 * 변경요청
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateReq(Map<String, Object> param) throws NkiaException {
		return this.update("FunctionPointDAO.updateReq", param);
	}

	/**
	 * 기능삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteReq(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteReq", paramMap);
	}

	/**
	 * 기능 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectReqFunc(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectReqFunc", paramMap);
	}

	/**
	 * 기능 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectReqFuncCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectReqFuncCount", paramMap);
	}

	/**
	 * 기능 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReqFuncList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectReqFuncList", paramMap);
	}

	/**
	 * 기능 신규아이디
     * @param paramMap
	 * @return
	 * @
	 */	
	public String selectReqFuncNewId(Map<String, Object> param) throws NkiaException {
		return (String)selectByPk("FunctionPointDAO.selectReqFuncNewId", param);
	}
	
	/**
	 * 기능등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map insertReqFunc(Map<String, Object> param) throws NkiaException {
		return (Map)this.insert("FunctionPointDAO.insertReqFunc", param);
	}

	/**
	 * 기능변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateReqFunc(Map<String, Object> param) throws NkiaException {
		return this.update("FunctionPointDAO.updateReqFunc", param);
	}
	
	/**
	 * 자산기능점수기본 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map insertAmFpBasic(Map<String, Object> param) throws NkiaException {
		return (Map)this.insert("FunctionPointDAO.insertAmFpBasic", param);
	}

	/**
	 * 자산기능점수기본 점수반영
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateAmFpBasic(Map<String, Object> param) throws NkiaException {
		return this.update("FunctionPointDAO.updateAmFpBasic", param);
	}

	/**
	 * 기능삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteReqFuncReqAll(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteReqFuncReqAll", paramMap);
	}

	/**
	 * 모델항목  단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectModelItem(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectModelItem", paramMap);
	}

	/**
	 * 모델항목  목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelItemList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectModelItemList", paramMap);
	}

	/**
	 * 모델항목 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertModelItem(Map<String, Object> param) throws NkiaException {
		this.insert("FunctionPointDAO.insertModelItem", param);
	}

	/**
	 * 모델항목 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateModelItem(Map<String, Object> param) throws NkiaException {
		return this.update("FunctionPointDAO.updateModelItem", param);
	}

	/**
	 * 모델항목 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteModelItem(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteModelItem", paramMap);
	}

	/**
	 * 모델항목 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteModelItemAll(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteModelItemAll", paramMap);
	}

	/**
	 * 모델모듈  단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectModuleItem(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectModuleItem", paramMap);
	}

	/**
	 * 모델모듈  개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectModuleItemCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectModuleItemCount", paramMap);
	}

	/**
	 * 모델모듈  목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModuleItemList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectModuleItemList", paramMap);
	}

	/**
	 * 모델모듈 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertModuleItem(Map<String, Object> param) throws NkiaException {
		this.insert("FunctionPointDAO.insertModuleItem", param);
	}

	/**
	 * 모델모듈 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateModuleItem(Map<String, Object> param) throws NkiaException {
		return this.update("FunctionPointDAO.updateModuleItem", param);
	}

	/**
	 * 모델모듈 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteModuleItem(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteModuleItem", paramMap);
	}

	/**
	 * 모델모듈 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteModuleItemAll(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteModuleItemAll", paramMap);
	}

	/**
	 * 기능점수산출 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectRsltPoint(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectRsltPoint", paramMap);
	}

	/**
	 * 기능점수산출 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectRsltPointCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectRsltPointCount", paramMap);
	}

	/**
	 * 기능점수산출 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltPointList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectRsltPointList", paramMap);
	}

	/**
	 * 기능점수산출 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertRsltPoint(Map<String, Object> param) throws NkiaException {
		this.insert("FunctionPointDAO.insertRsltPoint", param);
	}

	/**
	 * 기능점수산출 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateRsltPoint(Map<String, Object> param) throws NkiaException {
		return this.update("FunctionPointDAO.updateRsltPoint", param);
	}

	/**
	 * 기능점수산출 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteRsltPoint(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteRsltPoint", paramMap);
	}

	/**
	 * 비용산출 단건조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectRsltCost(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectRsltCost", paramMap);
	}

	/**
	 * 비용산출 건수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectRsltCostCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectRsltCostCount", paramMap);
	}

	/**
	 * 비용산출 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltCostList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectRsltCostList", paramMap);
	}

	/**
	 * 비용산출 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertRsltCost(Map<String, Object> param) throws NkiaException {
		this.insert("FunctionPointDAO.insertRsltCost", param);
	}

	/**
	 * 비용산출 계산로그 등록
     * @param paramMap
	 * @return
	 * @
	 */	
	public void insertRsltCostHist(Map<String, Object> param) throws NkiaException {
		this.insert("FunctionPointDAO.insertRsltCostHist", param);
	}

	/**
	 * 비용산출 변경
     * @param paramMap
	 * @return
	 * @
	 */	
	public int updateRsltCost(Map<String, Object> param) throws NkiaException {
		return this.update("FunctionPointDAO.updateRsltCost", param);
	}

	/**
	 * 비용산출 삭제
     * @param paramMap
	 * @return
	 * @
	 */	
	public int deleteRsltCost(Map<String, Object> paramMap) throws NkiaException {
		return this.delete("FunctionPointDAO.deleteRsltCost", paramMap);
	}


	/**
	 * 월별산출결과 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCompleteSrFuncList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectCompleteSrFuncList", paramMap);
	}
	public int selectCompleteSrFuncCount(Map<String, Object> paramMap) throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectCompleteSrFuncCount", paramMap);
	}
	public List selectResultModuleFuncList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectResultModuleFuncList", paramMap);
	}
	public int selectResultModuleFuncCount(Map<String, Object> paramMap) throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectResultModuleFuncCount", paramMap);
	}
	public List selectResultCostList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectResultCostList", paramMap);
	}
	public int selectResultCostHistCount(Map<String, Object> paramMap) throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectResultCostHistCount", paramMap);
	}
	public List selectResultCostHistList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectResultCostHistList", paramMap);
	}
	public List selectResultModuleItemList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectResultModuleItemList", paramMap);
	}
	
	/**
	 * 어플리케이션 변경실적 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportFunctionPointList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectReportFunctionPointList", paramMap);
	}
	public int selectReportFunctionPointCount(Map<String, Object> paramMap) throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectReportFunctionPointCount", paramMap);
	}
	
	/**
	 * 기간 별 SW개발비 보고서 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportPointList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectReportPointList", paramMap);
	}

	/**
	 * 기간 별 SW개발비 보고서 보고서
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectReportCostList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectReportCostList", paramMap);
	}
	

	/**
	 * 구성 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectCiSelPopCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectCiSelPopCount", paramMap);
	}

	/**
	 * 구성 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCiSelPopList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectCiSelPopList", paramMap);
	}

	/**
	 * 구성유형 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectCiTypeSelPopCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectCiTypeSelPopCount", paramMap);
	}

	/**
	 * 구성유형 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCiTypeSelPopList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectCiTypeSelPopList", paramMap);
	}

	/**
	 * 모델 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectModelSelPopCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectModelSelPopCount", paramMap);
	}

	/**
	 * 모델 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModelSelPopList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectModelSelPopList", paramMap);
	}

	/**
	 * 모듈 선택팝업 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectModuleSelPopList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectModuleSelPopList", paramMap);
	}

	/**
	 * 용역업체 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectVendorSelPopCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectVendorSelPopCount", paramMap);
	}

	/**
	 * 용역업체 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorSelPopList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectVendorSelPopList", paramMap);
	}

	/**
	 * 계산결과 선택팝업 개수조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public int selectRsltCostSelPopCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.selectRsltCostSelPopCount", paramMap);
	}

	/**
	 * 계산결과 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectRsltCostSelPopList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectRsltCostSelPopList", paramMap);
	}

	/**
	 * 구성 상세정보
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectConfInfo(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectConfInfo", paramMap);
	}

	/**
	 * 구성 상세정보
     * @param paramMap
	 * @return
	 * @
	 */	
	public Map selectReqFuncCalcInfo(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.selectReqFuncCalcInfo", paramMap);
	}
	public Map searchConfRelTopBottomAppSw(Map<String, Object> paramMap) throws NkiaException {
		return (Map)selectByPk("FunctionPointDAO.searchConfRelTopBottomAppSw", paramMap);
	}
	
	/**
	 * 코드정보 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectCodeDataList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectCodeDataList", paramMap);
	}

	/**
	 * 업체모듈항목 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorModuleItemList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectVendorModuleItemList", paramMap);
	}
	/**
	 * 업체모듈 목록조회
     * @param paramMap
	 * @return
	 * @
	 */	
	public List selectVendorModuleList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectVendorModuleList", paramMap);
	}
	
	public List selectVendorList(Map<String, Object> paramMap) throws NkiaException{
		return list("FunctionPointDAO.selectVendorList", paramMap);
	}
	
	public List searchClassTree(ModelMap paramMap) throws NkiaException {
		return list("FunctionPointDAO.searchClassTree", paramMap);
	}	

	public List searchAssetList(ModelMap paramMap) throws NkiaException{
		//List responseList = list("AssetStatisticsDAO.searchAssetList", paramMap);
		List resultList = list("FunctionPointDAO.searchAssetList", paramMap);
		List resultList2 = new ArrayList();
		if(paramMap.containsKey("rnum")){
			Map resultMap = new HashMap();
			
			//List resultList = new List();
			int size = resultList.size();
			for(int i=0; i<size; i++) {
				resultMap = (HashMap)resultList.get(i);
				resultMap.put("RNUM", (String)paramMap.get("rnum")+"-"+(String)resultMap.get("RNUM"));
				resultList2.add(resultMap);
			}
			return resultList2;
		}else{
			return resultList;
		}
	}

	public int searchAssetListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("FunctionPointDAO.searchAssetListCount", paramMap);
	}
}

