/*
 * @(#)systemService.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.system.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SystemService {
	
	public int searchSystemAuthListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchSystemAuthList(ModelMap paramMap) throws NkiaException;
	
	public void insertSystemAuth(ModelMap paramMap) throws NkiaException;
	
	public void deleteSystemAuth(ModelMap paramMap) throws NkiaException;
	
	public void deleteMappingUserGrp(ModelMap paramMap) throws NkiaException;
		
	public void updateSystemAuth(ModelMap paramMap) throws NkiaException;
	
	public String checkAuthIdComp(ModelMap paramMap) throws NkiaException;
	
	public String checkAuthNmIdComp(ModelMap paramMap) throws NkiaException;
	
	public List searchCheckedMenuMappingTree(ModelMap paramMap) throws NkiaException;
		
	public void deleteMappingMenu(ModelMap paramMap) throws NkiaException;

	public List searchUserSystemAuthList(Map param) throws NkiaException;
	
	public HashMap selectSystemAuthInfo(ModelMap paramMap) throws NkiaException;

	/* #################################################################################################################################### */
	/* ## *컬럼 도메인 관리* */	
	public HashMap manageDBColumnNameDomainProcess(ModelMap paramMap) throws NkiaException;
	/* ## *컬럼 도메인 관리* */	
	/* #################################################################################################################################### */

}
