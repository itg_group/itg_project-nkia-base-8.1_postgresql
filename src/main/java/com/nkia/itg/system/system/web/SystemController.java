/*
 * @(#)systemControll.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.system.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.system.service.SystemService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class SystemController {
	
	@Resource(name = "systemService")
	private SystemService systemService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/system/systemAuth/goSystemManager.do")
	public String userManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/system/systemManager.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/system/systemAuth/goTest.do")
	public String test(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/system/test.nvf";
		return forwarPage;
	}
	
	/**
	 * 시스템 권한 리스트 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/systemAuth/searchSystemAuthList.do")
	public @ResponseBody ResultVO  searchSystemAuthList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			//int totalCount = 0;
			//PagingUtil.getFristEndNum(paramMap);
			
			//totalCount = systemService.searchSystemAuthListCount(paramMap);
			List resultList = systemService.searchSystemAuthList(paramMap);		
			
			//gridVO.setTotalCount(totalCount);
			//gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 시스템 권한등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/systemAuth/insertSystemAuth.do")
	public @ResponseBody ResultVO insertSystemAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			systemService.insertSystemAuth(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 시스템 권한 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/systemAuth/deleteSystemAuth.do")
	public @ResponseBody ResultVO deleteSystemAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			systemService.deleteMappingMenu(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 시스템 권한 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/systemAuth/updateSystemAuth.do")
	public @ResponseBody ResultVO updateSystemAuth(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	

			systemService.updateSystemAuth(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));

			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 시스템 권한 아이디 중복 체크 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/systemAuth/checkAuthIdComp.do")
	public @ResponseBody ResultVO checkAuthIdComp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			String resultMap = systemService.checkAuthIdComp(paramMap);
			resultVO.setResultString(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 시스템 권한 이름 중복 체크 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/systemAuth/checkAuthNmIdComp.do")
	public @ResponseBody ResultVO checkAuthNmIdComp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			String resultMap = systemService.checkAuthNmIdComp(paramMap);
			resultVO.setResultString(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 시스템 권한에 맵핑된 메뉴 트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/systemAuth/searchCheckedMenuMappingTree.do")
	public @ResponseBody ResultVO searchCheckedMenuMappingTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			List resultList = systemService.searchCheckedMenuMappingTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expand_level, false);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 시스템권한 상세조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/system/selectSystemAuthInfo.do")
	public @ResponseBody ResultVO selectSystemAuthInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			HashMap resultMap = systemService.selectSystemAuthInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	
	/* #################################################################################################################################### */
	/* ## *컬럼 도메인 관리* */
	
	@RequestMapping(value="/itg/system/system/column/manageDBColumnNameDomain.do")
	public String manageDBColumnNameDomain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/system/column/manageDBColumnNameDomain.nvf";
		return forwarPage;
	}	
	
	@RequestMapping(value="/itg/system/system/column/manageDBColumnNameDomainProcess.do")
	public @ResponseBody ResultVO manageDBColumnNameDomainProcess(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			HashMap resultMap = systemService.manageDBColumnNameDomainProcess(paramMap);
			resultVO.setResultMsg( StringUtil.parseString(resultMap.get("DOMAIN_RULE")) );
			resultMap.remove("DOMAIN_RULE");
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}	
	/* ## *컬럼 도메인 관리* */
	/* #################################################################################################################################### */
	
}
