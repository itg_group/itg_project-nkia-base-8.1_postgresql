/*
 * @(#)systemServiceImpl.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.system.system.dao.SystemDAO;
import com.nkia.itg.system.system.service.SystemService;

@Service("systemService")
public class SystemServiceImpl implements SystemService{
	
	private static final Logger logger = LoggerFactory.getLogger(SystemServiceImpl.class);
	
	@Resource(name = "systemDAO")
	public SystemDAO systemDAO;
	
	public int searchSystemAuthListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return systemDAO.searchSystemAuthListCount(paramMap);
	}
	
	public List searchSystemAuthList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return systemDAO.searchSystemAuthList(paramMap);
	}
	
	public void insertSystemAuth(ModelMap paramMap) throws NkiaException {
		ArrayList menu_id;

		systemDAO.insertSystemAuth((Map)paramMap.get("insertNameValue"));
		menu_id = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingInfo")).get("menu_id");
		for(int i=0;i<menu_id.size();i++){
			((HashMap<String, Object>) paramMap.get("mappingInfo")).put("menu_id", menu_id.get(i));
			systemDAO.insertMappingMenu(paramMap.get("mappingInfo"));
		}
		
	}
	
	public void deleteSystemAuth(ModelMap paramMap) throws NkiaException {
		systemDAO.deleteSystemAuth(paramMap);
	}
	
	public void deleteMappingUserGrp(ModelMap paramMap) throws NkiaException {
		systemDAO.deleteMappingUserGrp(paramMap);
	}
	
	public void updateSystemAuth(ModelMap paramMap) throws NkiaException {
		ArrayList menu_id;

		systemDAO.updateSystemAuth(paramMap.get("insertNameValue"));
					
		systemDAO.deleteMappingMenu(paramMap.get("mappingInfo"));
					
		menu_id = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingInfo")).get("menu_id");
		for(int i=0;i<menu_id.size();i++){
			((HashMap<String, Object>) paramMap.get("mappingInfo")).put("menu_id", menu_id.get(i));
			systemDAO.insertMappingMenu(paramMap.get("mappingInfo"));
		}
	}
	
	public String checkAuthIdComp(ModelMap paramMap) throws NkiaException {
		return systemDAO.checkAuthIdComp(paramMap);
	}
	
	public String checkAuthNmIdComp(ModelMap paramMap) throws NkiaException {
		return systemDAO.checkAuthNmIdComp(paramMap);
	}
	
	public List searchCheckedMenuMappingTree(ModelMap paramMap) throws NkiaException {
		return systemDAO.searchCheckedMenuMappingTree(paramMap);
	}
	
	
	public void deleteMappingMenu(ModelMap paramMap) throws NkiaException {
		systemDAO.deleteMappingMenu(paramMap);
		systemDAO.deleteMappingUserGrp(paramMap);
		systemDAO.deleteSystemAuth(paramMap);
	}

	public List searchUserSystemAuthList(Map param) throws NkiaException {
		return systemDAO.searchUserSystemAuthList(param);
	}
	
	public HashMap selectSystemAuthInfo(ModelMap paramMap) throws NkiaException {
		return systemDAO.selectSystemAuthInfo(paramMap);
	}

	/* #################################################################################################################################### */
	/* ## *컬럼 도메인 관리* */	

	@Override
	public HashMap manageDBColumnNameDomainProcess(ModelMap paramMap) throws NkiaException {
		HashMap mapResult = new HashMap();
		String strCmd = StringUtil.parseString(paramMap.get("EXEC_CNTN"));
		String [] arrCmd = strCmd.trim().split("\n");
		
		List listProcess = systemDAO.manageDBColumnNameDomainProcess(paramMap);
		if ( listProcess != null ) {
			for ( int r = 0 ; r < listProcess.size() ; r++) {
				HashMap mapRecord = (HashMap) listProcess.get(r);
				mapResult.put(StringUtil.parseString(mapRecord.get("WORD_NAME")), StringUtil.parseString(mapRecord.get("DOMAIN_NAME")));
			}
		}
		
		if ( arrCmd[0].equals("(REGI)") == true) {
			HashMap mapResultRegi = new HashMap();
			if ( arrCmd.length > 1 ) { 
				for ( int d = 1 ; d < arrCmd.length ; d++ ) {
					String [] arrLine = arrCmd[d].trim().split("=");
					if ( arrLine.length > 1 ) { 
						HashMap mapDomain = new HashMap();
						mapDomain.put("WORD_NAME", arrLine[0].trim());
						mapDomain.put("DOMAIN_NAME", arrLine[1].trim());
						systemDAO.manageDBColumnNameDomainRegist(mapDomain);
						if ( StringUtil.parseString(mapResult.get(arrLine[0].trim())).equals("") == true ) {
							mapResultRegi.put(arrLine[0].trim(), arrLine[1].trim());
						}else {
							mapResultRegi.put(arrLine[0].trim(), "*중복*");
						}
					}
				} // for d
			}
			mapResult = mapResultRegi;
		}
		
		if ( arrCmd[0].equals("(DELT)") == true) {
			HashMap mapResultDelt = new HashMap();
			if ( arrCmd.length > 1 ) { 
				for ( int d = 1 ; d < arrCmd.length ; d++ ) {
					String [] arrLine = arrCmd[d].trim().split("=");
					if ( arrLine.length > 0 ) { 
						HashMap mapDomain = new HashMap();
						mapDomain.put("WORD_NAME", arrLine[0].trim());
						systemDAO.manageDBColumnNameDomainDelete(mapDomain);
						if ( StringUtil.parseString(mapResult.get(arrLine[0].trim())).equals("") == true ) {
							mapResultDelt.put(arrLine[0].trim(), "*대상없음*" );
						}else {
							mapResultDelt.put(arrLine[0].trim(), "*삭제*");
						}
					}
				} // for d
			}
			mapResult = mapResultDelt;
		}
		
		
		// 생성규칙 저장
		HashMap mapRule = new HashMap();
		mapRule.put("ENV_ID", "DB_COLUMN_MAKE_RULE");
		mapRule.put("ENV_NM", "DB컬럼명 생성 규칙");
		
		String strRuleValue = StringUtil.parseString(paramMap.get("DOMAIN_RULE"));
		if ( strRuleValue.equals("") == true ) {
			HashMap mapRuleValue = systemDAO.selectEnvironmentValue(mapRule);
			strRuleValue = StringUtil.parseString( mapRuleValue.get("ENV_TEXT") ); 
		} 
		mapRule.put("ENV_TEXT", strRuleValue );
		systemDAO.mergeEnvironmentValue(mapRule);
		mapResult.put("DOMAIN_RULE", strRuleValue );
		
		return mapResult;
	}
	
	/* ## *컬럼 도메인 관리* */	
	/* #################################################################################################################################### */
	
	
}
