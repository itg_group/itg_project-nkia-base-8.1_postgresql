/*
 * @(#)systemDAO.java              2013. 3. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.system.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("systemDAO")
public class SystemDAO extends EgovComAbstractDAO{
	
	public int searchSystemAuthListCount(ModelMap paramMap) throws NkiaException {
		int processListCount = (Integer) this.selectByPk("SystemDAO.searchSystemAuthListCount", paramMap);
		return processListCount;
	}
	
	public List searchSystemAuthList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("SystemDAO.searchSystemAuthList", paramMap);
		return resultList;
	}
	
	public void deleteSystemAuth(ModelMap paramMap) throws NkiaException {
		this.delete("SystemDAO.deleteSystemAuth", paramMap);
	}
	
	public void deleteMappingUserGrp(ModelMap paramMap) throws NkiaException {
		this.delete("SystemDAO.deleteMappingUserGrp", paramMap);
	}
	
	public void updateSystemAuth(Object object) throws NkiaException {
		this.update("SystemDAO.updateSystemAuth", object);
	}
	
	public void insertSystemAuth(Object object) throws NkiaException {
		this.insert("SystemDAO.insertSystemAuth", object);
	}
	
	public String checkAuthIdComp(ModelMap paramMap) throws NkiaException {
		return (String) this.selectByPk("SystemDAO.checkAuthIdComp", paramMap);
	}
	
	public String checkAuthNmIdComp(ModelMap paramMap) throws NkiaException {
		return (String) this.selectByPk("SystemDAO.checkAuthNmIdComp", paramMap);
	}
	
	public List searchCheckedMenuMappingTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("SystemDAO.searchCheckedMenuMappingTree", paramMap);
		return resultList;
	}
	
	public void deleteMappingMenu(Object object) throws NkiaException {
		this.delete("SystemDAO.deleteMappingMenu", object);
	}
	
	public void insertMappingMenu(Object object) throws NkiaException {
		this.insert("SystemDAO.insertMappingMenu", object);
	}
	
	public List searchUserSystemAuthList(Map param) throws NkiaException {
		List resultList = null;
		resultList = this.list("SystemDAO.searchUserSystemAuthList", param);
		return resultList;
	}
	
	public HashMap selectSystemAuthInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap) selectByPk("SystemDAO.selectSystemAuthInfo", paramMap);
	}

	/* #################################################################################################################################### */
	/* ## *컬럼 도메인 관리* */	
	
	public List manageDBColumnNameDomainProcess(ModelMap paramMap) {
		return this.list("SystemDAO.manageDBColumnNameDomainProcess", paramMap);
	}

	public void manageDBColumnNameDomainRegist(HashMap mapDomain) {
		this.insert("SystemDAO.manageDBColumnNameDomainRegist", mapDomain);
	}

	public void manageDBColumnNameDomainDelete(HashMap mapDomain) {
		this.delete("SystemDAO.manageDBColumnNameDomainDelete", mapDomain);
	}

	public void mergeEnvironmentValue(HashMap mapRule) {
		this.insert("SystemDAO.mergeEnvironmentValue", mapRule);
	}
	
	public HashMap selectEnvironmentValue(HashMap mapRule) {
		return (HashMap) selectByPk("SystemDAO.selectEnvironmentValue", mapRule);
	}
	
	/* ## *컬럼 도메인 관리* */	
	/* #################################################################################################################################### */
	
}
