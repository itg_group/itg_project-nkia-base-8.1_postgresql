package com.nkia.itg.system.board.web;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.board.service.NoticeBoardService;
import com.nkia.itg.system.user.service.UserService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({ "rawtypes" })
@Controller
public class NoticeBoardController {

	@Resource(name = "noticeboardService")
	private NoticeBoardService boardService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "userService")
	private UserService userService;

	/**
	 * 
	 * 공지사항 게시판
	 * 
	 * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/board/goNoticeBoard.do")
	public String goNoticeBoard(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		// 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		// 시스템 권한 조회
		String authString = "";
		String noticeAuth = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();

		if (isAuthenticate) {
			// 게시글 등록,수정,삭제 권한
			// 프로퍼티 Notice.Auth.List에 정의된 사용자그룹 권한이 있는 User
			List userGrpAuth = userVO.getGrpAuthList();
			if (userGrpAuth != null && !userGrpAuth.isEmpty()) {
				for (int i = 0; i < userGrpAuth.size(); i++) {
					if (i != 0) {
						authString += ",";
					}
					authString += (((HashMap) userGrpAuth.get(i)).get("user_grp_id"));
				}
			}
		}

		authString = authString.toUpperCase();
		paramMap.put("user_auth", authString);

		noticeAuth = NkiaApplicationPropertiesMap.getProperty("Notice.Auth.List");
		if (noticeAuth != null) {
			noticeAuth = noticeAuth.toUpperCase();
		}
		paramMap.put("notice_auth", noticeAuth);

		// 로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);

		// 로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);

		String forwarPage = "/itg/system/board/notice.nvf";
		return forwarPage;
	}

	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/searchNoticeBoardList.do")
	public @ResponseBody ResultVO searchBoardList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = boardService.searchBoardListCount(paramMap);
				resultList = boardService.searchBoardList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/system/board/deleteNoticeBoardInfo.do")
	public @ResponseBody ResultVO deleteBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			boolean authFlag = false;
			String noId = (String)paramMap.get("no_id");
			if(noId != null){
				authFlag = boardService.checkAuth(paramMap);
			}
			
			if(authFlag){
				boardService.deleteBoardInfo(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.system.result.00053"));
			}
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/updateNoticeBoardInfo.do")
	public @ResponseBody ResultVO updateBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boolean authFlag = false;
			String noId = (String)paramMap.get("no_id");
			if(noId != null){
				authFlag = boardService.checkAuth(paramMap);
			}
		
			if(authFlag){
				boardService.updateBoardInfo(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.system.result.00053"));
			}
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 글 등록
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 * @throws MessagingException 
	 * @throws IOException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(value="/itg/system/board/insertNoticeBoardInfo.do")
	public @ResponseBody ResultVO insertBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException, JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, MessagingException {
		ResultVO resultVO = new ResultVO();
		
		try{
			boolean isPass = boardService.insertBoardInfo(paramMap);
			resultVO.setSuccess(isPass);
			if(isPass) {
				// 등록 성공
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
				
				try {
					
					//알림 메일
					Map mailInfoMap = new HashMap();
					mailInfoMap.put("KEY", "NOTICE");
					mailInfoMap.put("TEMPLATE_ID", "NOTICE_NOTI_MAIL");
					mailInfoMap.put("BOARD_ID", (String)paramMap.get("BoardId"));
					
					
					EmailSetData emailSender = new EmailSetData(mailInfoMap);
					emailSender.sendMail();
					
				} catch(NkiaException e){
					throw new NkiaException(e);
				}
				
			} else {
				// 하루 최대 등록가능 게시글 수 초과
				resultVO.setResultMsg(messageSource.getMessage("msg.system.board.limit"));
			}
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/selectNoticeBoardInfo.do")
	public @ResponseBody ResultVO selectBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) boardService.selectBoardInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 메인 공지사항 팝업 화면
	 * 
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/noticeBoardPopup.do")
	public String searchNoticeBoardPopup(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String noti_id = request.getParameter("noti_id");
		String type = request.getParameter("type");
		
		paramMap.addAttribute("noti_id", noti_id);
		paramMap.addAttribute("type", type);
		
		return "/itg/system/board/popup/noticeDetailPopView.nvf";
	}
	
	/**
	 * 메인 공지사항 팝업 목록
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/searchNoticeBoardPopupList.do")
	public @ResponseBody ResultVO searchNoticeBoardPopupList(@RequestBody ModelMap paramMap)throws NkiaException {
		
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = boardService.searchBoardPopupList(paramMap);		
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(resultList.size());
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 메인 공지사항 게시판 팝업 화면
	 * 
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/goNoticeBoardPopView.do")
	public String goNoticeBoardPopView(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		
		String popupPosition = (String) request.getParameter("notice_position");
		paramMap.addAttribute("popupPosition", popupPosition);
		
		return "/itg/system/board/popup/noticePopView.nvf";
	}
	
	@RequestMapping(value="/itg/system/board/goNoticeBoardDetailPopView.do")
	public String goNoticeBoardDetailPopView(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String noti_id = request.getParameter("noti_id");
		String type = request.getParameter("type");
		
		paramMap.addAttribute("noti_id",noti_id);
		paramMap.addAttribute("type",type);
		
		return "/itg/system/board/popup/noticeDetailPopView.nvf";
	}
}
