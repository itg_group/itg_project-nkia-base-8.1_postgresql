package com.nkia.itg.system.board.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.security.sasl.AuthenticationException;

import com.nkia.itg.base.application.exception.NkiaException;

public interface BoardMultiService {
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(Map paramMap) throws NkiaException;
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(Map paramMap) throws NkiaException;
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void deleteBoardInfo(Map paramMap) throws NkiaException, IOException;
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws AuthenticationException 
	 */
	public void updateBoardInfo(Map paramMap) throws NkiaException; 
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws AuthenticationException 
	 */
	public void insertBoardInfo(Map paramMap) throws NkiaException; 
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(Map paramMap) throws NkiaException;
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(Map paramMap) throws NkiaException;
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 * @throws AuthenticationException 
	 */
	public Map selectBoardInfo(Map paramMap) throws NkiaException;
	
}
