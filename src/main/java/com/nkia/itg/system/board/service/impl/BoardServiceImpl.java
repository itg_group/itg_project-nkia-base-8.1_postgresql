package com.nkia.itg.system.board.service.impl;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.system.board.dao.BoardMultiDAO;
import com.nkia.itg.system.board.service.BoardService;

@Service("boardService")
public class BoardServiceImpl implements BoardService {

	private static final Logger logger = LoggerFactory.getLogger(BoardServiceImpl.class);
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "boardMultiDAO")
	public BoardMultiDAO boardDAO;
	
	public List searchBoardList(ModelMap paramMap) throws NkiaException {
		return boardDAO.searchBoardList(paramMap);
	}
	
	public int searchBoardListCount(ModelMap paramMap) throws NkiaException {
		return boardDAO.searchBoardListCount(paramMap);
	}
		
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException, IOException {
		boardDAO.deleteBoardInfo(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException {
		boardDAO.updateBoardInfo(paramMap);
	}
	
	public void insertBoardInfo(ModelMap paramMap) throws NkiaException {
		boardDAO.insertBoardInfo(paramMap);
	}
	
	public String selectBoardId(ModelMap paramMap) throws NkiaException {
		return boardDAO.selectBoardId(paramMap);
	}
	
	public void selectBoardCount(ModelMap paramMap) throws NkiaException {
		boardDAO.selectBoardCount(paramMap);
	}
	
}
