package com.nkia.itg.system.board.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.board.dao.BoardCategoryTreeDAO;
import com.nkia.itg.system.board.dao.FAQBoardDAO;
import com.nkia.itg.system.board.service.FAQBoardService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({"rawtypes", "unchecked"})
@Service("faqboardService")
public class FAQServiceImpl implements FAQBoardService {

	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "FAQBoardDAO")
	public FAQBoardDAO faqBoardDAO;
	
	@Resource(name = "BoardCategoryTreeDAO")
	public BoardCategoryTreeDAO categoryDAO;
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(Map paramMap) throws NkiaException {
		return faqBoardDAO.searchBoardList(paramMap);
	}
	
	/**
	 * FAQ 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(Map paramMap) throws NkiaException {
		return faqBoardDAO.searchBoardListCount(paramMap);
	}

	/**
	 * FAQ 피드백 글목록 리스트
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	@Override
	public List searchFaqFeedbackList(Map paramMap) throws NkiaException {
		return faqBoardDAO.searchFaqFeedbackList(paramMap);
	}
	
	/**
	 * FAQ 피드백 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	@Override
	public int searchFaqFeedbackListCount(Map paramMap) throws NkiaException {
		return faqBoardDAO.searchFaqFeedbackListCount(paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void deleteBoardInfo(Map paramMap) throws NkiaException, IOException {
		faqBoardDAO.deleteBoardInfo(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
		faqBoardDAO.updateBoardInfo(paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(Map paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
		String BoardId = selectBoardId(paramMap);
		paramMap.put("BoardId", BoardId);
		
		faqBoardDAO.insertBoardInfo(paramMap);
	}
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(Map paramMap) throws NkiaException {
		return faqBoardDAO.selectBoardId(paramMap);
	}
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(Map paramMap) throws NkiaException {
		faqBoardDAO.selectBoardCount(paramMap);
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(Map paramMap) throws NkiaException {
		if( paramMap.get("flag").equals(true) ){
			selectBoardCount(paramMap);
		}
		
		Map boardInfo = faqBoardDAO.selectBoardInfo(paramMap);
		Map addData = faqBoardDAO.searchBothInfoNo(paramMap);
		
		boardInfo.put("addData", addData);
		
		return boardInfo;
	}

	@Override
	public void insertFaqFeedbackInfo(Map paramMap) throws NkiaException {
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	paramMap.put("ins_user_id", userVO.getUser_id());
    	
    	String BoardId = faqBoardDAO.selectFaqFeedbackId(paramMap);
		paramMap.put("BoardId", BoardId);
		
		faqBoardDAO.insertFaqFeedbackInfo(paramMap);
	}

	@Override
	public void updateFaqFeedbackInfo(Map paramMap) throws NkiaException {
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    	paramMap.put("ins_user_id", userVO.getUser_id());
    	
		faqBoardDAO.updateFaqFeedbackInfo(paramMap);
	}

	@Override
	public void deleteFaqFeedbackInfo(Map paramMap) throws NkiaException {
		faqBoardDAO.deleteFaqFeedbackInfo(paramMap);
		
	}

	@Override
	public Map selectSrInfo(String srId) throws NkiaException {
		return faqBoardDAO.selectSrInfo(srId);
	}

	@Override
	public String selectInsUserId(Map searchMap) throws NkiaException {
		return faqBoardDAO.selectInsUserId(searchMap);
	}

	@Override
	public boolean checkAuth(ModelMap paramMap) throws NkiaException {
		boolean resultFlag = false;
		String noId = (String)paramMap.get("no_id");
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = userVO.getUser_id();
	
		// 로그인 사용자와 등록자가 동일한 경우 수정, 삭제 가능 
		Map searchMap = new HashMap();
		searchMap.put("no_id", noId);
		String insUserId = (String)selectInsUserId(searchMap);
		
		if(insUserId != null && loginUserId.equals(insUserId)){
			resultFlag = true;
			return resultFlag;
		}
		
		// 로그인 사용자의 권한에 따라 수정, 삭제 가능
		List userGrpAuth = userVO.getGrpAuthList();
		String noticeAuthList = NkiaApplicationPropertiesMap.getProperty("Faq.Auth.List");
		String[] noticeAuthArr = noticeAuthList.split(",");
		
		for(int i=0; i < noticeAuthArr.length; i++){
			String authStr = noticeAuthArr[i];
			if(authStr != null && !authStr.isEmpty()){
				authStr = authStr.toUpperCase();
				
				for(int j=0; j<userGrpAuth.size(); j++){
					Map userGrpAuthMap =(HashMap)userGrpAuth.get(j);
					String userAuth = (String) userGrpAuthMap.get("USER_GRP_ID");
					if(userAuth != null && !userAuth.isEmpty()){
						userAuth = userAuth.toUpperCase();
						if(userAuth != null && authStr.equals(userAuth)){
							resultFlag = true;
							return resultFlag;
						}
					}
				}
			}
		}
		
		return resultFlag;
	}
}
