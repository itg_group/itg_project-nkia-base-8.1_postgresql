package com.nkia.itg.system.board.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.board.service.QnaBoardService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Controller
public class QnaBoardController {

	@Resource(name = "qnaboardService")
	private QnaBoardService boardService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 
	 * QnA 게시판
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/goQnaBoard.do")
	public String goFaqBoard(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		// 시스템 권한 조회
		String authString = "";
		String qnaAuth = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();

		if(isAuthenticate){
			// 게시글 등록,수정,삭제 권한
			// 프로퍼티 Notice.Auth.List에 정의된 사용자그룹 권한이 있는 User
			List userGrpAuth = userVO.getGrpAuthList();
			if(userGrpAuth != null && !userGrpAuth.isEmpty()) {
				for(int i=0; i<userGrpAuth.size(); i++){
					if(i != 0){
						authString += ",";
					}
					authString += ( ( (HashMap)userGrpAuth.get(i)).get("USER_GRP_ID") );
				}
			}
		}
		
		authString = authString.toUpperCase();
		paramMap.put("user_auth", authString);
		
		qnaAuth = NkiaApplicationPropertiesMap.getProperty("Qna.Auth.List");
		if (qnaAuth != null) {
			qnaAuth = qnaAuth.toUpperCase();
		}
		paramMap.put("qna_auth", qnaAuth);
		
		//로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);
		
		//로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);
		
		String forwarPage = "/itg/system/board/qna.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/system/board/goQnaBoardExtJs.do")
	public String goFaqBoardExtJs(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/board/qna_extjs.nvf";
		
		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();

		if(isAuthenticate){
			List userAuth = userVO.getSysAuthList();
			authString = "@";
			if(userAuth != null && !userAuth.isEmpty()) {
				for(int i=0; i<userAuth.size(); i++){
					authString += ( ( (HashMap)userAuth.get(i)).get("AUTH_ID") + "@" );
				}
					paramMap.put("user_auth", authString);
			}
		}
				
		//로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);
		
		//로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);
		
		return forwarPage;
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/searchQnaBoardList.do")
	public @ResponseBody ResultVO searchBoardList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = boardService.searchBoardListCount(paramMap);
				resultList = boardService.searchBoardList(paramMap);
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/system/board/deleteQnaBoardInfo.do")
	public @ResponseBody ResultVO deleteBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			boardService.deleteBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/updateQnaBoardInfo.do")
	public @ResponseBody ResultVO updateBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boardService.updateBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/insertQnaBoardInfo.do")
	public @ResponseBody ResultVO insertBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boardService.insertBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/selectQnaBoardInfo.do")
	public @ResponseBody ResultVO selectBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) boardService.selectBoardInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 카테고리_트리생성
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/searchQnaCategoryTree.do")
	public @ResponseBody ResultVO searchCategoryTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = boardService.searchCategoryTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
