package com.nkia.itg.system.board.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings("rawtypes")
@Repository("QnaBoardDAO")
public class QnaBoardDAO extends EgovComAbstractDAO {
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(ModelMap paramMap)throws NkiaException {
		return list("QnaBoardDAO.searchBoardList", paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("QnaBoardDAO.searchBoardListCount", paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException {
		this.delete("QnaBoardDAO.deleteBoardInfo", paramMap);
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException {
		this.update("QnaBoardDAO.updateBoardInfo", paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(ModelMap paramMap) throws NkiaException {
		this.insert("QnaBoardDAO.insertBoardInfo", paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(ModelMap paramMap) throws NkiaException {
		String BoardID = (String) this.selectByPk("QnaBoardDAO.selectBoardId", paramMap);
		return BoardID;
	}
	
	/**
	 * 게시판 자식글 목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardNo(ModelMap paramMap) {
		String BoardNo = (String) this.selectByPk("QnaBoardDAO.selectBoardNo", paramMap);
		return BoardNo;
	}
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(ModelMap paramMap) throws NkiaException {
		this.update("QnaBoardDAO.selectBoardCount", paramMap);
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(ModelMap paramMap) throws NkiaException {
		return (Map) this.selectByPk("QnaBoardDAO.selectBoardInfo", paramMap);
	}
	
	/**
	 * 카테고리_트리생성
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public Map searchBothInfoNo(ModelMap paramMap)throws NkiaException {
		return (Map) this.selectByPk("QnaBoardDAO.searchBothInfoNo", paramMap);
	}

}
