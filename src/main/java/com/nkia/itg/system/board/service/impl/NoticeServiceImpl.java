package com.nkia.itg.system.board.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.board.dao.NoticeBoardDAO;
import com.nkia.itg.system.board.service.NoticeBoardService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.com.utl.fcc.service.EgovStringUtil;

@SuppressWarnings({"rawtypes", "unchecked"})
@Service("noticeboardService")
public class NoticeServiceImpl implements NoticeBoardService {

	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "NoticeBoardDAO")
	public NoticeBoardDAO boardDAO;
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(ModelMap paramMap) throws NkiaException {
		return boardDAO.searchBoardList(paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(ModelMap paramMap) throws NkiaException {
		return boardDAO.searchBoardListCount(paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException, IOException {
		boardDAO.deleteBoardInfo(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
		boardDAO.updateBoardInfo(paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public boolean insertBoardInfo(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
    	
		String BoardId = selectBoardId(paramMap);
		paramMap.put("BoardId", BoardId);
		
		boardDAO.insertBoardInfo(paramMap);
		
		return true;
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(ModelMap paramMap) throws NkiaException {
		return boardDAO.selectBoardId(paramMap);
	}
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(ModelMap paramMap) throws NkiaException {
		boardDAO.selectBoardCount(paramMap);
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(ModelMap paramMap) throws NkiaException {
		if( paramMap.get("flag").equals(true) ){
			selectBoardCount(paramMap);
		}
		
		Map boardInfo = boardDAO.selectBoardInfo(paramMap);
		Map addData = boardDAO.searchBothInfoNo(paramMap);
		
		boardInfo.put("addData", addData);
		
		return boardInfo;
	}
	
	/**
	 * 메인 공지사항 팝업 목록
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardPopupList(ModelMap paramMap) throws NkiaException {
		List<HashMap> searchBoardList = boardDAO.searchBoardPopupList(paramMap);
		List newSearchBoardList = new ArrayList();
		for (Map tempMap : searchBoardList){
			String tempContent = EgovStringUtil.checkHtmlView(String.valueOf(tempMap.get("CONTENT")));
			tempMap.put("CONTENT", tempContent);
			
			newSearchBoardList.add(tempMap);
		}
		return newSearchBoardList;
	}

	@Override
	public String selectInsUserId(Map searchMap) throws NkiaException {
		return boardDAO.selectInsUserId(searchMap);
	}

	@Override
	public boolean checkAuth(ModelMap paramMap) throws NkiaException {
		boolean resultFlag = false;
		String noId = (String)paramMap.get("no_id");
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = userVO.getUser_id();
	
		// 로그인 사용자와 등록자가 동일한 경우 수정, 삭제 가능 
		Map searchMap = new HashMap();
		searchMap.put("no_id", noId);
		String insUserId = (String)selectInsUserId(searchMap);
		
		if(insUserId != null && loginUserId.equals(insUserId)){
			resultFlag = true;
			return resultFlag;
		}
		
		// 로그인 사용자의 권한에 따라 수정, 삭제 가능
		List userGrpAuth = userVO.getGrpAuthList();
		String noticeAuthList = NkiaApplicationPropertiesMap.getProperty("Notice.Auth.List");
		String[] noticeAuthArr = noticeAuthList.split(",");
		
		for(int i=0; i < noticeAuthArr.length; i++){
			String authStr = noticeAuthArr[i];
			if(authStr != null && !authStr.isEmpty()){
				authStr = authStr.toUpperCase();
				
				for(int j=0; j<userGrpAuth.size(); j++){
					Map userGrpAuthMap =(HashMap)userGrpAuth.get(j);
					String userAuth = (String) userGrpAuthMap.get("USER_GRP_ID");
					if(userAuth != null && !userAuth.isEmpty()){
						userAuth = userAuth.toUpperCase();
						if(userAuth != null && authStr.equals(userAuth)){
							resultFlag = true;
							return resultFlag;
						}
					}
				}
			}
		}
		
		return resultFlag;
	}
	
}
