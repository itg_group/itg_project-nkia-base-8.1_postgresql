package com.nkia.itg.system.board.service;

import java.io.IOException;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface BoardService {
		
	public int searchBoardListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchBoardList(ModelMap paramMap) throws NkiaException;
	
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException, IOException;
	
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException; 
	
	public void insertBoardInfo(ModelMap paramMap) throws NkiaException; 
		
	public String selectBoardId(ModelMap paramMap) throws NkiaException;
	
	public void selectBoardCount(ModelMap paramMap) throws NkiaException; 
}
