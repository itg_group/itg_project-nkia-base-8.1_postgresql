package com.nkia.itg.system.board.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.security.sasl.AuthenticationException;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.board.dao.BoardCategoryTreeDAO;
import com.nkia.itg.system.board.dao.DownBoardDAO;
import com.nkia.itg.system.board.service.DownBoardService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({"rawtypes", "unchecked"})
@Service("downboardService")
public class DownServiceImpl implements DownBoardService {

	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "DownBoardDAO")
	public DownBoardDAO boardDAO;
	
	@Resource(name = "BoardCategoryTreeDAO")
	public BoardCategoryTreeDAO categoryDAO;
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 * @throws AuthenticationException 
	 */
	public List searchBoardList(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("cust_op_type", userVO.getCust_op_type());
    	}
    	
		return boardDAO.searchBoardList(paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("cust_op_type", userVO.getCust_op_type());
    	}
    	
		return boardDAO.searchBoardListCount(paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException, IOException {
		boardDAO.deleteBoardInfo(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
    	
		boardDAO.updateBoardInfo(paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("ins_user_id", userVO.getUser_id());
    	}
		String BoardId = selectBoardId(paramMap);
		paramMap.put("BoardId", BoardId);
		
		boardDAO.insertBoardInfo(paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(ModelMap paramMap) throws NkiaException {
		return boardDAO.selectBoardId(paramMap);
	}
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(ModelMap paramMap) throws NkiaException {
		boardDAO.selectBoardCount(paramMap);
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(ModelMap paramMap) throws NkiaException {
		if( paramMap.get("flag").equals(true) ){
			selectBoardCount(paramMap);
		}
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("cust_op_type", userVO.getCust_op_type());
    	}
		
		Map boardInfo = boardDAO.selectBoardInfo(paramMap);
		Map addData = boardDAO.searchBothInfoNo(paramMap);
		
		boardInfo.put("addData", addData);
		
		return boardInfo;
	}
	
	/**
	 * 카테고리_트리생성
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public List searchCategoryTree(ModelMap paramMap) throws NkiaException{
		return categoryDAO.searchCategoryTree(paramMap);
	}
}
