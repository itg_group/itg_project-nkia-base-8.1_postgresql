package com.nkia.itg.system.board.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings("rawtypes")
@Repository("DownBoardDAO")
public class DownBoardDAO extends EgovComAbstractDAO {
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(ModelMap paramMap)throws NkiaException {
		return list("DownBoardDAO.searchBoardList", paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("DownBoardDAO.searchBoardListCount", paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteBoardInfo(ModelMap paramMap) throws NkiaException {
		this.delete("DownBoardDAO.deleteBoardInfo", paramMap);
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(ModelMap paramMap) throws NkiaException {
		this.update("DownBoardDAO.updateBoardInfo", paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(ModelMap paramMap) throws NkiaException {
		this.insert("DownBoardDAO.insertBoardInfo", paramMap);
	}
	
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(ModelMap paramMap) throws NkiaException {
		String BoardID = (String) this.selectByPk("DownBoardDAO.selectBoardId", paramMap);
		return BoardID;
	}
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(ModelMap paramMap) throws NkiaException {
		this.update("DownBoardDAO.selectBoardCount", paramMap);
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(ModelMap paramMap) throws NkiaException {
		return (Map) this.selectByPk("DownBoardDAO.selectBoardInfo", paramMap);
	}
	
	/**
	 * 이전글 / 다음글 내역 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map searchBothInfoNo(ModelMap paramMap)throws NkiaException {
		return (Map) this.selectByPk("DownBoardDAO.searchBothInfoNo", paramMap);
	}

}
