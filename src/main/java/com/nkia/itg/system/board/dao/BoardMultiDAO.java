package com.nkia.itg.system.board.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("boardMultiDAO")
public class BoardMultiDAO extends EgovComAbstractDAO {
	
	public List searchBoardList(Map paramMap)throws NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		super.getSqlMapClientTemplate().queryWithRowHandler("BoardMultiDAO.searchBoardList", paramMap, handler);
		return handler.getReturnList();
	}
	
	public int searchBoardListCount(Map paramMap)throws NkiaException {
		return (Integer)selectByPk("BoardMultiDAO.searchBoardListCount", paramMap);
	}
	
	public void deleteBoardInfo(Map paramMap) throws NkiaException {
		this.delete("BoardMultiDAO.deleteBoardInfo", paramMap);
	}
	
	public void updateBoardInfo(Map paramMap) throws NkiaException {
		this.update("BoardMultiDAO.updateBoardInfo", paramMap);
	}
	
	public void insertBoardInfo(Map paramMap) throws NkiaException {
		this.insert("BoardMultiDAO.insertBoardInfo", paramMap);
	}
	
	public String selectBoardId(Map paramMap) throws NkiaException {
		String BoardID = (String) this.selectByPk("BoardMultiDAO.selectBoardId", paramMap);
		return BoardID;
	}
	
	public void selectBoardCount(Map paramMap) throws NkiaException {
		this.update("BoardMultiDAO.selectBoardCount", paramMap);
	}
	
	public Map selectBoardInfo(Map paramMap)throws NkiaException {
		MapDataRowHandler handler = new MapDataRowHandler();
		super.getSqlMapClientTemplate().queryWithRowHandler("BoardMultiDAO.selectBoardInfo", paramMap, handler);
		return handler.getReturnMap();
	}
	
	public Map searchBothInfoNo(Map paramMap)throws NkiaException {
		return (Map) this.selectByPk("BoardMultiDAO.searchBothInfoNo", paramMap);
	}

}
