package com.nkia.itg.system.board.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

@SuppressWarnings("rawtypes")
public interface FAQBoardService {
	
	/**
	 *FAQ 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(Map paramMap) throws NkiaException;
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(Map paramMap) throws NkiaException;
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public int searchFaqFeedbackListCount(Map paramMap) throws NkiaException;

	/**
	 * FAQ 피드백 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchFaqFeedbackList(Map paramMap) throws NkiaException;
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void deleteBoardInfo(Map paramMap) throws NkiaException, IOException;
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(Map paramMap) throws NkiaException; 
	
	/**
	 * 글 등록
	 * 
     * @param insertMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(Map insertMap) throws NkiaException; 
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(Map paramMap) throws NkiaException;
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(Map paramMap) throws NkiaException;
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(Map paramMap) throws NkiaException;

	/**
	 *  피드백 등록
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public void insertFaqFeedbackInfo(Map paramMap) throws NkiaException;

	/**
	 *  피드백 수정
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public void updateFaqFeedbackInfo(Map paramMap) throws NkiaException;

	/**
	 *  피드백 삭제
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public void deleteFaqFeedbackInfo(Map paramMap) throws NkiaException;
	
	/**
	 * 요청내용 조회
	 * 
     * @param srId
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectSrInfo(String srId) throws NkiaException;

	public String selectInsUserId(Map searchMap) throws NkiaException;

	public boolean checkAuth(ModelMap paramMap) throws NkiaException;

}
