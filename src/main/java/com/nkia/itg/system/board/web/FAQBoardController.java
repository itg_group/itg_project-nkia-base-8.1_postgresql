package com.nkia.itg.system.board.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.board.service.FAQBoardService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Controller
public class FAQBoardController {

	@Resource(name = "faqboardService")
	private FAQBoardService boardService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * FAQ 게시판 호출
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/goFaqBoard.do")
	public String goFaqBoard(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		// 시스템 권한 조회
		String authString = "";
		String faqAuth =  "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();

		if(isAuthenticate){
			// 게시글 등록,수정,삭제 권한
			// 프로퍼티 Faq.Auth.List에 정의된 사용자그룹 권한이 있는 User
			List userGrpAuth = userVO.getGrpAuthList();
			if(userGrpAuth != null && !userGrpAuth.isEmpty()) {
				for(int i=0; i<userGrpAuth.size(); i++){
					if(i != 0){
						authString += ",";
					}
					authString += ( ( (HashMap)userGrpAuth.get(i)).get("USER_GRP_ID") );
				}
			}
		}
		
		authString = authString.toUpperCase();
		paramMap.put("user_auth", authString);
			
		faqAuth = NkiaApplicationPropertiesMap.getProperty("Faq.Auth.List");
		faqAuth = faqAuth.toUpperCase();
		paramMap.put("faq_auth", faqAuth);
		
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);
		
		
		String forwarPage = "/itg/system/board/faq.nvf";
		return forwarPage;
	}
	
	/**
	 * FAQ 조회팝업 호출
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/goFaqBoardPopup.do")
	public String goFaqBoardPopup(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/board/popup/faqPopup.nvf";
		return forwarPage;
	}
	
	/**
	 * FAQ 조회 상세팝업 호출
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/goFaqDetailPopup.do")
	public String goFaqDetailPopup(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/board/popup/faqDetailPopup.nvf";
		paramMap.put("no_id", request.getParameter("no_id"));
		
		return forwarPage;
	}
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/searchFaqBoardList.do")
	public @ResponseBody ResultVO searchBoardList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = boardService.searchBoardListCount(paramMap);
				resultList = boardService.searchBoardList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/searchFaqFeedbackList.do")
	public @ResponseBody ResultVO searchFaqFeedbackList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			if(!initFlag) {
				totalCount = boardService.searchFaqFeedbackListCount(paramMap);
				resultList = boardService.searchFaqFeedbackList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/system/board/deleteFaqBoardInfo.do")
	public @ResponseBody ResultVO deleteBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			boolean authFlag = false;
			String noId = (String)paramMap.get("no_id");
			if(noId != null){
				authFlag = boardService.checkAuth(paramMap);
			}
			
			if(authFlag){
				boardService.deleteBoardInfo(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.system.result.00053"));
			}
		
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/updateFaqBoardInfo.do")
	public @ResponseBody ResultVO updateBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boolean authFlag = false;
			String noId = (String)paramMap.get("no_id");
			if(noId != null){
				authFlag = boardService.checkAuth(paramMap);
			}
		
			if(authFlag){
				boardService.updateBoardInfo(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
			
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.system.result.00053"));
			}
			
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/insertFaqBoardInfo.do")
	public @ResponseBody ResultVO insertBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			// 시스템 권한 조회
			boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			
			boolean isInsert = false;
			if(isAuthenticate){
				List userAuth = userVO.getSysAuthList();
				if(userAuth != null && !userAuth.isEmpty()) {
					for(int i=0; i<userAuth.size(); i++){
						if("SYSTEM_USER".equals(( (HashMap)userAuth.get(i)).get("AUTH_ID"))){
							isInsert = true;
							break;
						}
					}
				}
			}
			
			if(isInsert){
				boardService.insertBoardInfo(paramMap);
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
				
			}else{
				resultVO.setResultMsg(messageSource.getMessage("msg.system.result.00053"));
			}
			
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/selectFaqBoardInfo.do")
	public @ResponseBody ResultVO selectBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) boardService.selectBoardInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 피드백 등록
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/insertFaqFeedbackInfo.do")
	public @ResponseBody ResultVO insertFaqFeedbackInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boardService.insertFaqFeedbackInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 피드백 수정
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/updateFaqFeedbackInfo.do")
	public @ResponseBody ResultVO updateFaqFeedbackInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boardService.updateFaqFeedbackInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	

	/**
	 * 피드백 삭제
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/deleteFaqFeedbackInfo.do")
	public @ResponseBody ResultVO deleteFaqFeedbackInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boardService.deleteFaqFeedbackInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
