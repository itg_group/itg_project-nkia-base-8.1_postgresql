package com.nkia.itg.system.board.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.board.service.BoardMultiService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class BoardMultiController {
	
	@Resource(name = "boardMultiService")
	private BoardMultiService boardMultiService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 
	 * Extjs Prototype - 자료실 게시판
     * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/multi/goIncidentActionCurrentStatus.do")
	public String IncidentActionCurrentStatus(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/board/multi/incidentActionCurrentStatus.nvf";		
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/system/board/multi/goProgressWorkCurrentStatus.do")
	public String goProgressWorkCurrentStatus(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/board/multi/progressWorkCurrentStatus.nvf";		
		return forwarPage;
	}
	/**
	 * 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/multi/searchBoardList.do")
	public @ResponseBody ResultVO searchBoardList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			
			int totalCount = 0;
			
			boolean initFlag = false;
			if(paramMap.get("init") != null) {
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			PagingUtil.getFristEndNum(paramMap);
			
			// 사용자가 센터 사용자일 경우 센터용 게시물 조회 가능하도록 ..
			
			
			if(!initFlag) {
				totalCount = boardMultiService.searchBoardListCount(paramMap);
				resultList = boardMultiService.searchBoardList(paramMap);		
			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value="/itg/system/board/multi/deleteBoardInfo.do")
	public @ResponseBody ResultVO deleteBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			boardMultiService.deleteBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/multi/updateBoardInfo.do")
	public @ResponseBody ResultVO updateBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boardMultiService.updateBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/multi/insertBoardInfo.do")
	public @ResponseBody ResultVO insertBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			boardMultiService.insertBoardInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/board/multi/selectBoardInfo.do")
	public @ResponseBody ResultVO selectBoardInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) boardMultiService.selectBoardInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
