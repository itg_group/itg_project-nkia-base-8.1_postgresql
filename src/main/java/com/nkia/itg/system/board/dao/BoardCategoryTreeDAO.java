package com.nkia.itg.system.board.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings("rawtypes")
@Repository("BoardCategoryTreeDAO")
public class BoardCategoryTreeDAO extends EgovComAbstractDAO {
	
	/**
	 * 카테고리_트리생성
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchCategoryTree(ModelMap paramMap) throws NkiaException {
		return this.list("BoardCategoryTreeDAO.searchCategoryTree", paramMap);
	}
}
