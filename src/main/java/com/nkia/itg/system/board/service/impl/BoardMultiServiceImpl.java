package com.nkia.itg.system.board.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.security.sasl.AuthenticationException;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.board.dao.BoardMultiDAO;
import com.nkia.itg.system.board.service.BoardMultiService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("boardMultiService")
public class BoardMultiServiceImpl implements BoardMultiService {

	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "boardMultiDAO")
	public BoardMultiDAO dataDAO;
	
	@Override
	public int searchBoardListCount(Map paramMap) throws NkiaException {    	
		return dataDAO.searchBoardListCount(paramMap);
	}

	@Override
	public List searchBoardList(Map paramMap) throws NkiaException {
		return dataDAO.searchBoardList(paramMap);
	}

	@Override
	public void deleteBoardInfo(Map paramMap) throws NkiaException, IOException {
		dataDAO.deleteBoardInfo(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}

	@Override
	public void updateBoardInfo(Map paramMap) throws NkiaException {
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramMap.put("upd_user_id", userVO.getUser_id());
    	
		dataDAO.updateBoardInfo(paramMap);
	}

	@Override
	public void insertBoardInfo(Map paramMap) throws NkiaException {
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		paramMap.put("regist_user_id", userVO.getUser_id());
		String boardataId = selectBoardId(paramMap);
		paramMap.put("board_id", boardataId);
		
		dataDAO.insertBoardInfo(paramMap);
	}

	@Override
	public String selectBoardId(Map paramMap) throws NkiaException {
		return dataDAO.selectBoardId(paramMap);
	}

	@Override
	public void selectBoardCount(Map paramMap) throws NkiaException {
		dataDAO.selectBoardCount(paramMap);
	}

	@Override
	public Map selectBoardInfo(Map paramMap) throws NkiaException {
		if( paramMap.get("flag").equals(true) ){
			selectBoardCount(paramMap);
		}
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		
		Map boardInfo = dataDAO.selectBoardInfo(paramMap);
		Map addData = dataDAO.searchBothInfoNo(paramMap);
		if (boardInfo != null) {
			boardInfo.put("addData", addData);
		}
		
		return boardInfo;
	}

}
