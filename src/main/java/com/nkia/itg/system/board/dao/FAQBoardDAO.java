package com.nkia.itg.system.board.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings("rawtypes")
@Repository("FAQBoardDAO")
public class FAQBoardDAO extends EgovComAbstractDAO {
	
	/**
	 * FAQ 게시판 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchBoardList(Map paramMap)throws NkiaException {
		return list("FAQBoardDAO.searchBoardList", paramMap);
	}
	
	/**
	 * FAQ 게시판 글목록 리스트 총합
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public int searchBoardListCount(Map paramMap)throws NkiaException {
		return (Integer)selectByPk("FAQBoardDAO.searchBoardListCount", paramMap);
	}

	/**
	 * FAQ 피드백 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public List searchFaqFeedbackList(Map paramMap) {
		return list("FAQBoardDAO.searchFaqFeedbackList", paramMap);
	}

	/**
	 * FAQ 피드백 글목록 리스트
	 * 
     * @param paramMap
	 * @return List
	 * @throws NkiaException
	 */
	public int searchFaqFeedbackListCount(Map paramMap) {
		return (Integer)selectByPk("FAQBoardDAO.searchFaqFeedbackListCount", paramMap);
	}
	
	/**
	 * 글 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteBoardInfo(Map paramMap) throws NkiaException {
		this.delete("FAQBoardDAO.deleteBoardInfo", paramMap);
	}
	
	/**
	 * 글 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateBoardInfo(Map paramMap) throws NkiaException {
		this.update("FAQBoardDAO.updateBoardInfo", paramMap);
	}
	
	/**
	 * 글 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertBoardInfo(Map paramMap) throws NkiaException {
		this.insert("FAQBoardDAO.insertBoardInfo", paramMap);
	}
	
	/**
	 * FAQ 게시판 글목록 ID
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectBoardId(Map paramMap) throws NkiaException {
		String BoardID = (String) this.selectByPk("FAQBoardDAO.selectBoardId", paramMap);
		return BoardID;
	}
	
	/**
	 * 선택항목 조회수 검색
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void selectBoardCount(Map paramMap) throws NkiaException {
		this.update("FAQBoardDAO.selectBoardCount", paramMap);
	}
	
	/**
	 * 선택항목 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map selectBoardInfo(Map paramMap) throws NkiaException {
		return (Map) this.selectByPk("FAQBoardDAO.selectBoardInfo", paramMap);
	}
	
	/**
	 * 이전글 / 다음글 내역 조회
	 * 
     * @param paramMap
	 * @return Map
	 * @throws NkiaException
	 */
	public Map searchBothInfoNo(Map paramMap)throws NkiaException {
		return (Map) this.selectByPk("FAQBoardDAO.searchBothInfoNo", paramMap);
	}

	/**
	 * 피드백 ID
	 * 
     * @param paramMap
	 * @return String
	 * @throws NkiaException
	 */
	public String selectFaqFeedbackId(Map paramMap) throws NkiaException {
		String BoardID = (String) this.selectByPk("FAQBoardDAO.selectFaqFeedbackId", paramMap);
		return BoardID;
	}
	
	/**
	 * 피드백 등록
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void insertFaqFeedbackInfo(Map paramMap) throws NkiaException {
		this.insert("FAQBoardDAO.insertFaqFeedbackInfo", paramMap);
	}
	
	/**
	 * 피드백 수정
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void updateFaqFeedbackInfo(Map paramMap) throws NkiaException {
		this.update("FAQBoardDAO.updateFaqFeedbackInfo", paramMap);
	}
	
	/**
	 * 피드백 삭제
	 * 
     * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteFaqFeedbackInfo(Map paramMap) throws NkiaException {
		this.delete("FAQBoardDAO.deleteFaqFeedbackInfo", paramMap);
	}

	public HashMap selectSrInfo(String srId) {
		return (HashMap) this.selectByPk("FAQBoardDAO.selectSrInfo", srId);
	}

	public String selectInsUserId(Map searchMap) throws NkiaException {
		return(String)selectByPk("FAQBoardDAO.selectInsUserId", searchMap);
	}
}
