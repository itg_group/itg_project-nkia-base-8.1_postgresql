/*
 * @(#)ProcessWorkStateService.java              2013. 6. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ProcessWorkStateService {

	public List searchProcessTreeList(ModelMap paramMap) throws NkiaException;

	public List searchProcessNodeList(ModelMap paramMap) throws NkiaException;

	public HashMap selectProcessNodeDetail(ModelMap paramMap) throws NkiaException;

	public void updateProcessWorkState(ModelMap paramMap) throws NkiaException;

}
