package com.nkia.itg.system.process.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.process.dao.ProcessVersionDAO;
import com.nkia.itg.system.process.service.ProcessVersionService;

@Service("processVersionService")
public class ProcessVersionServiceImpl implements ProcessVersionService {

	@Resource(name = "processVersionDAO")
	public ProcessVersionDAO processVersionDAO;
	
	@Override
	public List searchProcessVersionList(Map paramMap) throws NkiaException {
		return processVersionDAO.searchProcessVersionList(paramMap);
	}

	@Override
	public Map selectProcessVersionDetail(Map paramMap) throws NkiaException {
		return processVersionDAO.selectProcessVersionDetail(paramMap);
	}

	
	@Override
	public void deleteProcess(String processId) {
		/*
		long deployId = processVersionDAO.searchDeployId(processId);
		processVersionDAO.deleteProcessVariable(deployId);
		processVersionDAO.deleteProcessNodeDetail(deployId);
		processVersionDAO.deleteProcessDeploy(deployId);
		*/
	}
	
	
}
