/*
 * @(#)ProcessWorkStateServiceImpl.java              2013. 6. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.process.dao.ProcessWorkStateDAO;
import com.nkia.itg.system.process.service.ProcessWorkStateService;

@Service("processWorkStateService")
public class ProcessWorkStateServiceImpl implements ProcessWorkStateService {
	
	@Resource(name = "processWorkStateDAO")
	public ProcessWorkStateDAO processWorkStateDAO;
	
	@Override
	public List searchProcessTreeList(ModelMap paramMap) throws NkiaException {
		return processWorkStateDAO.searchProcessTreeList(paramMap);
	}

	@Override
	public List searchProcessNodeList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return processWorkStateDAO.searchProcessNodeList(paramMap);
	}

	@Override
	public HashMap selectProcessNodeDetail(ModelMap paramMap) throws NkiaException {
		return processWorkStateDAO.selectProcessNodeDetail(paramMap);
	}

	@Override
	public void updateProcessWorkState(ModelMap paramMap) throws NkiaException {
		int count = processWorkStateDAO.searchNbpmWorkStateHistory(paramMap);
		if(count > 0) {
			processWorkStateDAO.updateNbpmWorkStateHistory(paramMap);
		} else {
			processWorkStateDAO.insertNbpmWorkStateHistory(paramMap);
		}
		processWorkStateDAO.updateProcessWorkState(paramMap);
	}

}
