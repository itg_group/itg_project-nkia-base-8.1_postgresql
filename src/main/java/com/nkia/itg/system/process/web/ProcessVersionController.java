package com.nkia.itg.system.process.web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.nbpm.process.service.DeployExecutor;
import com.nkia.itg.system.process.service.ProcessVersionService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class ProcessVersionController {

	@Resource(name = "processVersionService")
	private ProcessVersionService processVersionService;	
	
	@Resource(name = "deployCommonExecutor")
	private DeployExecutor deployCommonExecutor;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	
	/**
	 * 부서관리페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/process/goProcessVersionManager.do")
	public String goProcessWorkStateManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/system/process/processVersionManager.nvf";
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/system/process/searchProcessVersionList.do")
	public @ResponseBody ResultVO searchProcessVersionList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			List resultList = processVersionService.searchProcessVersionList(paramMap);		
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/system/process/selectProcessVersionDetail.do")
	public @ResponseBody ResultVO selectProcessVersionDetail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			Map resultMap = new HashMap();
			resultMap = processVersionService.selectProcessVersionDetail(paramMap);
			resultVO.setResultMap((HashMap)resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/system/process/deleteProcessVersion.do")
	public @ResponseBody ResultVO deleteProcessVersion(@RequestBody ModelMap paramMap)throws NkiaException, SQLException {
		ResultVO resultVO = new ResultVO();
		try{
			String processId = (String)paramMap.get("processId");
			deployCommonExecutor.deleteProcess(processId);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
