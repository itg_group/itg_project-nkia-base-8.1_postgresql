package com.nkia.itg.system.process.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("processVersionDAO")
public class ProcessVersionDAO extends EgovComAbstractDAO {

	public List searchProcessVersionList(Map paramMap) {
		return list("ProcessVersionDAO.searchProcessVersionList", paramMap);
	}

	public Map selectProcessVersionDetail(Map paramMap) {
		return (Map)selectByPk("ProcessVersionDAO.selectProcessVersionDetail", paramMap);
	}
	
	public void deleteProcessDeploy(long deployId) throws SQLException {
		delete("NbpmDAO.deleteProcessDeploy", deployId);
	}
	
	public void deleteProcessNodeDetail(long deployId) throws SQLException {
		delete("NbpmDAO.deleteProcessNodeDetail", deployId);
	}
	
	public void deleteProcessVariable(long deployId) throws SQLException {
		delete("NbpmDAO.deleteProcessVariable", deployId);
	}

	public List searchDeployIds(String processType) throws SQLException {
		return list("NbpmDAO.searchDeployIds", processType);
	}

}
