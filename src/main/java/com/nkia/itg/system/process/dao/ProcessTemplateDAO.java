package com.nkia.itg.system.process.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("processTemplateDAO")
public class ProcessTemplateDAO extends EgovComAbstractDAO{
	
	public List searchTemplate(ModelMap paramMap) throws NkiaException{
		return list("ProcessTemplateDAO.searchTemplate", paramMap);
	}

	public int searchTemplateCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("ProcessTemplateDAO.searchTemplateCount", paramMap);
	}
	
	public void insertTemplate(ModelMap paramMap) throws NkiaException {
		this.insert("ProcessTemplateDAO.insertTemplate", paramMap);
	}
	
	public void updateTemplate(ModelMap paramMap) throws NkiaException {
		this.update("ProcessTemplateDAO.updateTemplate", paramMap);
	}
	
	public void deleteTemplate(ModelMap paramMap) throws NkiaException {
		this.delete("ProcessTemplateDAO.deleteTemplate", paramMap);
	}	

	public HashMap selectFileName(String id) throws NkiaException{
		return (HashMap)selectByPk("ProcessTemplateDAO.selectFileName", id);
	}

	public HashMap selectTemplateInfo(ModelMap paramMap) throws NkiaException{
		return (HashMap) this.selectByPk("ProcessTemplateDAO.selectTemplateInfo", paramMap);
	}
	
	public void insertTemplateReqDoc(Map paramMap) throws NkiaException {
		this.insert("ProcessTemplateDAO.insertTemplateReqDoc", paramMap);
	}
	
	public void deleteTemplateReqDoc(Map paramMap) throws NkiaException {
		this.delete("ProcessTemplateDAO.deleteTemplateReqDoc", paramMap);
	}	
}