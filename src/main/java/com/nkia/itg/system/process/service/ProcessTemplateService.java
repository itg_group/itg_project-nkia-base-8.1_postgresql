/*
 * @(#)ProcessTemplateService.java              2013. 09. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.AtchFileVO;

public interface ProcessTemplateService {
	
	public List searchTemplate(ModelMap paramMap) throws NkiaException;

	public int searchTemplateCount(ModelMap paramMap) throws NkiaException;
	
	public Map uploadProcessTemplate(AtchFileVO atchFileVO, HttpServletRequest request)throws NkiaException, FileNotFoundException;
	
	public void insertTemplate(ModelMap paramMap) throws NkiaException;
	
	public void updateTemplate(ModelMap paramMap) throws NkiaException;
	
	public void deleteTemplate(ModelMap paramMap) throws NkiaException, IOException;
	
	public HashMap selectFileName(String id) throws NkiaException;

	public HashMap selectTemplateInfo(ModelMap paramMap) throws NkiaException;
}
