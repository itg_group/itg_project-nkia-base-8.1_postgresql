/*
 * @(#)ProcessWorkStateDAO.java              2013. 6. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("processWorkStateDAO")
public class ProcessWorkStateDAO extends EgovComAbstractDAO {

	public List searchProcessTreeList(ModelMap paramMap) throws NkiaException {
		return list("ProcessWorkStateDAO.searchProcessTreeList", paramMap);
	}

	public List searchProcessNodeList(ModelMap paramMap) throws NkiaException {
		return list("ProcessWorkStateDAO.searchProcessNodeList", paramMap);
	}

	public HashMap selectProcessNodeDetail(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("ProcessWorkStateDAO.selectProcessNodeDetail", paramMap);
	}

	public void updateProcessWorkState(ModelMap paramMap) throws NkiaException {
		update("ProcessWorkStateDAO.updateProcessWorkState", paramMap);
	}

	public int searchNbpmWorkStateHistory(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("ProcessWorkStateDAO.searchNbpmWorkStateHistory", paramMap);
	}

	public void updateNbpmWorkStateHistory(ModelMap paramMap) throws NkiaException {
		update("ProcessWorkStateDAO.updateNbpmWorkStateHistory", paramMap);
	}

	public void insertNbpmWorkStateHistory(ModelMap paramMap) throws NkiaException {
		insert("ProcessWorkStateDAO.insertNbpmWorkStateHistory", paramMap);
	}

}
