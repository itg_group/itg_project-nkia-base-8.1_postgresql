/*
 * @(#)ProcessAuthDAO.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("processAuthDAO")
public class ProcessAuthDAO extends EgovComAbstractDAO {
	
	public int seachProcessAuthListCount(ModelMap paramMap) throws NkiaException {
		int processListCount = (Integer) this.selectByPk("ProcessAuthDAO.seachProcessAuthListCount", paramMap);
		return processListCount;
	}
	
	public int searchProAuthIdCnt(ModelMap paramMap) throws NkiaException {
		int processListCount = (Integer) this.selectByPk("ProcessAuthDAO.searchProAuthIdCnt", paramMap);
		return processListCount;
	}
	
	public List seachProcessAuthList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("ProcessAuthDAO.seachProcessAuthList", paramMap);
		return resultList;
	}
	
	public void deleteProcessAuth(ModelMap paramMap) throws NkiaException {
		this.delete("ProcessAuthDAO.deleteProcessAuth", paramMap);
	}
	
	public void deleteMappingProcessAuth(ModelMap paramMap) throws NkiaException {
		this.delete("ProcessAuthDAO.deleteMappingProcessAuth", paramMap);
	}
	
	public void updateProcessAuth(ModelMap paramMap) throws NkiaException {
		this.update("ProcessAuthDAO.updateProcessAuth", paramMap);
	}
	
	public void insertProcessAuth(ModelMap paramMap) throws NkiaException {
		this.insert("ProcessAuthDAO.insertProcessAuth", paramMap);
	}
	
	public List searchUserProcessAuthList(Map param) {
		List resultList = null;
		resultList = this.list("ProcessAuthDAO.searchUserProcessAuthList", param);
		return resultList;
	}

	/**
	 * 프로세스 권한 ID 검증 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchProcessId(Map paramMap) throws NkiaException {
		return (Integer) selectByPk("ProcessAuthDAO.searchProcessId", paramMap);
	}

	public List searchProcessAuthTree(Map paramMap) {
		List resultList = null;
		resultList = this.list("ProcessAuthDAO.searchProcessAuthTree", paramMap);
		return resultList;
	}

	public HashMap searchProcessAuthInfo(Map paramMap) {
		return (HashMap) selectByPk("ProcessAuthDAO.searchProcessAuthInfo", paramMap);
	}
}
