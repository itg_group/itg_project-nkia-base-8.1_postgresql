package com.nkia.itg.system.process.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ProcessVersionService {

	public List searchProcessVersionList(Map paramMap) throws NkiaException;

	public Map selectProcessVersionDetail(Map paramMap) throws NkiaException;
	
	public void deleteProcess(String processId);
	/*
	public void deleteProcessDeploy(long deployId) throws SQLException {
		this.getSqlMapClient().delete("NbpmDAO.deleteProcessDeploy", deployId);
	}
	
	public void deleteProcessNodeAlam(long deployId) throws SQLException {
		//this.getSqlMapClient().delete("NbpmDAO.deleteProcessNodeAlam", deployId);
	}
	
	public void deleteProcessNodeDetail(long deployId) throws SQLException {
		this.getSqlMapClient().delete("NbpmDAO.deleteProcessNodeDetail", deployId);
	}
	
	public void deleteProcessVariable(long deployId) throws SQLException {
		this.getSqlMapClient().delete("NbpmDAO.deleteProcessVariable", deployId);
	}

	public List searchDeployIds(String processType) throws SQLException {
		return list("NbpmDAO.searchDeployIds", processType);
	}
	*/

}
