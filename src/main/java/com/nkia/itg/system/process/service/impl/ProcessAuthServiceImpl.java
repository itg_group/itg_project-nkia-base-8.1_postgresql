/*
 * @(#)ProcessServiceImpl.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.system.process.dao.ProcessAuthDAO;
import com.nkia.itg.system.process.service.ProcessAuthService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.web.WebUtil;

import egovframework.com.cmm.EgovMessageSource;

@Service("processAuthService")
public class ProcessAuthServiceImpl implements ProcessAuthService{
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	private static final Logger logger = LoggerFactory.getLogger(ProcessAuthServiceImpl.class);
	
	@Resource(name = "processAuthDAO")
	public ProcessAuthDAO processAuthDAO;
	
	public int seachProcessAuthListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return processAuthDAO.seachProcessAuthListCount(paramMap);
	}
	
	public List seachProcessAuthList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return processAuthDAO.seachProcessAuthList(paramMap);
	}
	
	public String insertProcessAuth(ModelMap paramMap) throws NkiaException {
		String resultMsg = new String();
		paramMap = (ModelMap) WebUtil.lowerCaseMapKey(paramMap);
		int cnt = processAuthDAO.searchProAuthIdCnt(paramMap);
		
		if(cnt > 0){
			resultMsg = messageSource.getMessage("msg.system.result.00045");
		}else{
			resultMsg = messageSource.getMessage("msg.common.result.00004");
			processAuthDAO.insertProcessAuth(paramMap);
		}
		return resultMsg;
	}
	
	public void deleteProcessAuth(ModelMap paramMap) throws NkiaException {
		paramMap = (ModelMap) WebUtil.lowerCaseMapKey(paramMap);
		processAuthDAO.deleteMappingProcessAuth(paramMap);
		processAuthDAO.deleteProcessAuth(paramMap);
	}
	
	public void updateProcessAuth(ModelMap paramMap) throws NkiaException {
		paramMap = (ModelMap) WebUtil.lowerCaseMapKey(paramMap);
		processAuthDAO.updateProcessAuth(paramMap);
	}

	public List searchUserProcessAuthList(Map param) throws NkiaException {
		return processAuthDAO.searchUserProcessAuthList(param);
	}

	/**
	 * 프로세스 권한 ID 검증 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchProcessId(Map paramMap) throws NkiaException {
		
		return processAuthDAO.searchProcessId(paramMap);
	}

	@Override
	public List searchProcessAuthTree(Map paramMap) throws NkiaException {
		
		return processAuthDAO.searchProcessAuthTree(paramMap);
	}

	@Override
	public HashMap searchProcessAuthInfo(Map paramMap) throws NkiaException {
		return processAuthDAO.searchProcessAuthInfo(paramMap);
	}
}
