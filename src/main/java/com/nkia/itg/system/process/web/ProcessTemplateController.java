/*
 * @(#)ProcessTemplateController.java              2013. 09. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.process.service.ProcessTemplateService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class ProcessTemplateController {
	
	@Resource(name = "processTemplateService")
	private ProcessTemplateService processTemplateService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/system/process/goProcessTemplate.do")
	public String processTemplate(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/process/processTemplate.nvf";
		return forwarPage;
	}
	
	/**
	 * 프로세스 템플릿 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/process/searchTemplate.do")
	public @ResponseBody ResultVO searchTemplate(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);

			resultList = processTemplateService.searchTemplate(paramMap);		
			totalCount = processTemplateService.searchTemplateCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 선택한 프로세스 템플릿 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/process/selectTemplateInfo.do")
	public @ResponseBody ResultVO selectTemplateInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) processTemplateService.selectTemplateInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 프로세스 템플릿 업로드
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value = "/itg/system/process/uploadProcessTemplate.do")
	public void uploadProcessTemplate(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException {
		JSONObject returnData = new JSONObject();
		
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			// 파일등록
			Map result = processTemplateService.uploadProcessTemplate(atchFileVO, request);
			
	        //sets success to true
			returnData.put("success", true);
			returnData.put("resultData", result);
	        
			//convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        //out.close();
		} catch (Exception e) {
			/*if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
*/			
			throw new NkiaException(e);
		}finally
		{
			 out.close();
		}
	}
	
	/**
	 * 프로세스 템플릿 신규등록
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/process/insertTemplate.do")
	public @ResponseBody ResultVO insertTemplate(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
	    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("login_user_id", userVO.getUser_id());
	    	}
	    	processTemplateService.insertTemplate(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 프로세스 템플릿 수정
	 * @param paramMap
	 * @return
	 * @
	 */		
	@RequestMapping(value="/itg/system/process/updateTemplate.do")
	public @ResponseBody ResultVO updateTemplate(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}
			processTemplateService.updateTemplate(paramMap);
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 프로세스 템플릿 삭제
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @
	 */		
	@RequestMapping(value="/itg/system/process/deleteTemplate.do")
	public @ResponseBody ResultVO deleteTemplate(@RequestBody ModelMap paramMap)throws NkiaException, IOException {
		ResultVO resultVO = new ResultVO();		
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			if(isAuthenticated) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}
			processTemplateService.deleteTemplate(paramMap);
			
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
	
	/**
	 * 
	 * 프로세스 템플릿 다운로드
	 * 
	 * @param request
	 * @param response
	 * @return 
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value = "/itg/system/process/downProcessTemplate.do")
	public void downProcessTemplate(HttpServletRequest request, HttpServletResponse response) throws NkiaException, IOException {
		ResultVO resultVO = new ResultVO();		
		try{
			HashMap fileInfo = processTemplateService.selectFileName(request.getParameter("tem_id"));
			String saveFileNm = (String) fileInfo.get("SAVE_FILE_NM");
			String origFileNm = (String) fileInfo.get("ORIGIN_FILE_NM");
			String yymm = saveFileNm.substring(0, 6) + "/";
			String stordFilePate = BaseConstraints.getTemplateFilePath() + yymm;
	
			if("".equals(origFileNm)){
				origFileNm = saveFileNm;
			}
		    boolean isSuccess = atchFileUtil.createOutputFile(request, response, saveFileNm, origFileNm, stordFilePate, "application/vnd.ms-excel");
		    
			if( !isSuccess ){
				PrintWriter out = response.getWriter();
				response.setContentType("text/html; charset=euc-kr");
				out.println("파일을 찾을 수 없습니다.");
			}
		    
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
	}
}
