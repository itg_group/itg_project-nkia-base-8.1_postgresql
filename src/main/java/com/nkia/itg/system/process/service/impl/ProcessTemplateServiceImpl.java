/*
 * @(#)ProcessTemplateServiceImpl.java              2013. 09. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.dao.AtchFileDAO;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.system.process.dao.ProcessTemplateDAO;
import com.nkia.itg.system.process.service.ProcessTemplateService;

import egovframework.com.cmm.EgovWebUtil;
import egovframework.com.cmm.service.EgovFileMngUtil;

@Service("processTemplateService")
public class ProcessTemplateServiceImpl implements ProcessTemplateService{
	
	@Resource(name = "atchFileDAO")
	public AtchFileDAO atchFileDAO;
	
	@Resource(name = "processTemplateDAO")
	public ProcessTemplateDAO processTemplateDAO;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil; 
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	public static final int BUFF_SIZE = 2048;
	
	public List searchTemplate(ModelMap paramMap) throws NkiaException {
		return processTemplateDAO.searchTemplate(paramMap);
	}

	public int searchTemplateCount(ModelMap paramMap) throws NkiaException {
		return processTemplateDAO.searchTemplateCount(paramMap);
	}
	
	/**
	 * 
	 * 프로세스 템플릿 업로드
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws FileNotFoundException 
	 */
	public Map uploadProcessTemplate(AtchFileVO atchFileVO, HttpServletRequest request)throws NkiaException, FileNotFoundException {
		MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
		InputStream stream = null;

		try {
		    File cFile = new File(EgovWebUtil.filePathBlackList(BaseConstraints.getTemplateFilePath()));
		    
		    cFile.setExecutable(false, true);
		    cFile.setReadable(true);
		    cFile.setWritable(false, true);
		    
		    if (!cFile.exists()){
		    	cFile.mkdir();
		    }

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		Map result = null;
		Iterator<String> fileIter = mptRequest.getFileNames();
		while (fileIter.hasNext()) {
			String fileName = (String) fileIter.next();
			MultipartFile mFile = mptRequest.getFile( fileName );
			if (mFile.getSize() > 0) {
				// 개별 최대 파일 Check
				result = EgovFileMngUtil.uploadFile(mFile, BaseConstraints.getTemplateFilePath());
			}
		}
		
		return result;
	}
	
	public void insertTemplate(ModelMap paramMap) throws NkiaException {
		processTemplateDAO.insertTemplate(paramMap);
	}
	
	public void updateTemplate(ModelMap paramMap) throws NkiaException {
		processTemplateDAO.updateTemplate(paramMap);
	}
	
	public void deleteTemplate(ModelMap paramMap) throws NkiaException, IOException {
		processTemplateDAO.deleteTemplate(paramMap);
		
		if( paramMap.containsKey("atch_file_id") && paramMap.get("atch_file_id") != null ){
			AtchFileVO atchFileVO = new AtchFileVO();
			atchFileVO.setAtch_file_id((String)paramMap.get("atch_file_id"));
			atchFileService.deleteRegistAtchFile(atchFileVO);
		}
	}
	
	public HashMap selectFileName(String id) throws NkiaException {
		HashMap saveFileName = processTemplateDAO.selectFileName(id);
		return saveFileName;
	}

	@Override
	public HashMap selectTemplateInfo(ModelMap paramMap) throws NkiaException {
		return processTemplateDAO.selectTemplateInfo(paramMap);
	}

}
