/*
 * @(#)ProcessService.java              2013. 3. 9.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.process.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ProcessAuthService {

	public int seachProcessAuthListCount(ModelMap paramMap) throws NkiaException;
	
	public List seachProcessAuthList(ModelMap paramMap) throws NkiaException;
	
	public String insertProcessAuth(ModelMap paramMap) throws NkiaException;
	
	public void deleteProcessAuth(ModelMap paramMap) throws NkiaException;
	
	public void updateProcessAuth(ModelMap paramMap) throws NkiaException;

	public List searchUserProcessAuthList(Map param) throws NkiaException;
	
	/**
	 * 프로세스 권한 ID 검증 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchProcessId(Map paramMap) throws NkiaException;

	public List searchProcessAuthTree(Map paramMap) throws NkiaException;

	public HashMap searchProcessAuthInfo(Map paramMap) throws NkiaException;
}
