package com.nkia.itg.system.email.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.system.email.dao.EmailHistoryDAO;
import com.nkia.itg.system.email.service.EmailHistoryService;


@Service("emailHistoryService")
public class EmailHistoryServiceImpl implements EmailHistoryService {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailHistoryServiceImpl.class);

	@Resource(name = "emailHistoryDAO")
	public EmailHistoryDAO emailHistoryDAO;
	
	
	public void insertMailSendHistory(EmailVO emailVO) throws NkiaException {
		
		try {
			
			String replaceErrMsg = "";
			if(!"".equals(emailVO.getErr_msg())) {
				byte[] bErrMsg = emailVO.getErr_msg().getBytes();
				int beLength = bErrMsg.length;
				if(beLength > 3999) {
					beLength = 3999;
				}
				replaceErrMsg = new String(bErrMsg, 0, beLength-1);
			}
			emailVO.setErr_msg(replaceErrMsg);
			
			String replaceToId = "";
			if (emailVO.getTo_user_id() != null) {
				if(!"".equals(emailVO.getTo_user_id())) {
					int beLength = emailVO.getTo_user_id().length();
					if(beLength > 3999) {
						beLength = 3999;
						replaceToId = emailVO.getTo_user_id().substring(0, beLength);
					} else {
						replaceToId = emailVO.getTo_user_id().substring(0, beLength-1);
					}
					
				}
				emailVO.setTo_user_id(replaceToId);
			}
			
			String replaceToEmail = "";
			if (emailVO.getTo() != null) {
				if(!"".equals(emailVO.getTo())) {
					int beLength = emailVO.getTo().length();
					if(beLength > 3999) {
						beLength = 3999;
						replaceToEmail = emailVO.getTo().substring(0, beLength);
					} else {
						replaceToEmail = emailVO.getTo().substring(0, beLength-1);
					}
				}
				emailVO.setTo(replaceToEmail);
			}
			
			emailHistoryDAO.insertMailSendHistory(emailVO);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
	}
	
	public List selectMailSendHistory(HashMap paramMap) throws NkiaException {
		return emailHistoryDAO.selectMailSendHistory(paramMap);
	}
	
	@Override
	public int selectMailSendHistoryCount(HashMap paramMap) throws NkiaException {
		return emailHistoryDAO.selectMailSendHistoryCount(paramMap);
	}
	
	
}
