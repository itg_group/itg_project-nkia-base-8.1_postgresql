package com.nkia.itg.system.email.service;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;

public interface EmailInfoService {
	
	public EmailVO searchEmailInfo(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectEmailInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateEmailInfo(ModelMap paramMap) throws NkiaException;
	
	public void sendTestEmail(ModelMap paramMap) throws NkiaException, JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, MessagingException;
	
	public void updateHappyCall(ModelMap paramMap) throws NkiaException;
	
	public String updateApproval(ModelMap paramMap) throws NkiaException, SQLException;
}
