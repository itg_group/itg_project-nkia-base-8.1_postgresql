package com.nkia.itg.system.email.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.security.CipherAES;
import com.nkia.itg.base.application.util.security.CipherDES;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.email.service.EmailInfoService;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;

import egovframework.com.cmm.EgovMessageSource;

/**
 * @version 1.0 2014. 11. 4.
 * @author <a href="mailto:jyjeong@nkia.co.kr"> JeongYoun Jeong
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */

@Controller
public class EmailInfoController {
	
	@Resource(name = "emailInfoService")
	private EmailInfoService emailInfoService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "postWorkDAO")
	private PostWorkDAO postWorkDAO;
	
	/**
	 * 이메일계정관리 화면 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/email/goEmailInfoManager.do")
	public String goEmailInfoManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/email/emailInfoManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 이메일계정 상세정보 조회
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/email/selectEmailInfo.do")
	public @ResponseBody ResultVO selectEmailInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			resultVO.setResultMap( (HashMap) emailInfoService.selectEmailInfo(paramMap) );
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 이메일계정 정보 업데이트
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/email/updateEmailInfo.do")
	public @ResponseBody ResultVO  updateEmailInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	emailInfoService.updateEmailInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테스트 이메일 보내기
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/email/sendTestEmail.do")
	public @ResponseBody ResultVO  sendTestEmail(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
	    	emailInfoService.sendTestEmail(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * HappyCall 팝업 이벤트
     * @param paramMap
	 * @return
	 * @throws SQLException 
	 * @
	 */
	@RequestMapping(value="/itg/system/email/goHappyCallPopup.do")
	public String goHappyCallPopup(ModelMap paramMap,HttpServletRequest request) throws NkiaException, SQLException {
		String srId = request.getParameter("srId"); 
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		
		Map processDetail = commonDAO.searchHappyCallDataList(srId);
		HashMap<String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("SR_ID", processDetail.get("SR_ID"));
		dataMap.put("TITLE", processDetail.get("TITLE"));
		dataMap.put("CONTENT", processDetail.get("CONTENT"));
		dataMap.put("COMPLETE_COMMENT", processDetail.get("COMPLETE_COMMENT"));
		dataMap.put("REQ_USER_NM", processDetail.get("REQ_USER_NM"));
		dataMap.put("REQ_DT", processDetail.get("REQ_DT"));
		dataMap.put("RCEPT_USER_NM", processDetail.get("RCEPT_USER_NM"));
		dataMap.put("RCEPT_DT", processDetail.get("RCEPT_DT"));
		dataMap.put("WORK_USER_NM", processDetail.get("WORK_USER_NM"));
		dataMap.put("END_DT", processDetail.get("END_DT"));
		dataMap.put("HAPPYCALL_GRADE", processDetail.get("STATIS_RESULT"));
		paramMap.put("dataMap", dataMap);
		String forwarPage = "/itg/customize/popup/happyCall.nvf";
		return forwarPage;
	}
	
	/**
	 * 승인 팝업 이벤트
     * @param paramMap
	 * @return
	 * @throws SQLException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws IOException 
	 * @
	 */
	@RequestMapping(value="/itg/system/email/goApprovalPopup.do")
	public String goApprovalPopup(ModelMap paramMap,HttpServletRequest request) throws NkiaException, SQLException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {
		String srId = request.getParameter("srId");
		String userId = request.getParameter("userId");
		
		/* SAMPLE
		srId = CipherDES.decode(srId);
		userId = CipherDES.decode(userId);
		*/
		
		long procId =  Long.parseLong(request.getParameter("procId"));
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("task_id", procId);
		Map processDetail = commonDAO.searchApprovalDataList(searchMap);
		HashMap<String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("SR_ID", processDetail.get("SR_ID"));
		dataMap.put("TITLE", processDetail.get("TITLE"));
		//dataMap.put("CONTENT", StringUtil.changeLineAlignmentForHtml((String) processDetail.get("CONTENT")));
		dataMap.put("CONTENT", processDetail.get("CONTENT"));
		dataMap.put("REQ_USER_NM", processDetail.get("REQ_USER_NM"));
		dataMap.put("REQ_DT", processDetail.get("REQ_DT"));
		dataMap.put("STATUS", processDetail.get("STATUS"));
		dataMap.put("TASKID", procId);
		dataMap.put("USERID", userId);
		paramMap.put("dataMap", dataMap);
		String forwarPage = "/itg/customize/popup/approval.nvf";
		return forwarPage;
	}
	/**
	 * 반려 팝업 이벤트
     * @param paramMap
	 * @return
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws SQLException 
	 * @throws IOException 
	 * @
	 */
	@RequestMapping(value="/itg/system/email/goApprovalOnlyPopup.do")
	public String goCancelPopup(ModelMap paramMap,HttpServletRequest request) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, SQLException, IOException {
		String srId = request.getParameter("srId");
		String userId = request.getParameter("userId");
		
		/* SAMPLE
		srId = CipherDES.decode(srId);
		userId = CipherDES.decode(userId);
		*/
		
		long procId =  Long.parseLong(request.getParameter("procId"));
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("task_id", procId);
		Map processDetail = commonDAO.searchApprovalDataList(searchMap);
		HashMap<String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("SR_ID", processDetail.get("SR_ID"));
		dataMap.put("TITLE", processDetail.get("TITLE"));
		dataMap.put("CONTENT", (String) processDetail.get("CONTENT"));
		dataMap.put("REQ_USER_NM", processDetail.get("REQ_USER_NM"));
		dataMap.put("REQ_DT", processDetail.get("REQ_DT"));
		dataMap.put("STATUS", processDetail.get("STATUS"));
		dataMap.put("TASKID", procId);
		dataMap.put("USERID", userId);
		paramMap.put("dataMap", dataMap);
		String forwarPage = "/itg/customize/popup/approvalOnly.nvf";
		return forwarPage;
	}
	/**
	 * 만족도조사 완료 이메일 보내기
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/email/updateHappyCall.do")
	public @ResponseBody ResultVO  updateHappyCall(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			String srId = (String)paramMap.get("SR_ID");
			String grade = (String)paramMap.get("GRADE");
			String comment = (String)paramMap.get("COMMENT");
			ModelMap searchMap = new ModelMap();
			searchMap.put("SR_ID", srId);
			searchMap.put("GRADE", grade);
			searchMap.put("COMMENT", comment);
	    	emailInfoService.updateHappyCall(searchMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 승인 시 종료메일 보내기
     * @param paramMap
	 * @return
	 * @throws SQLException 
	 * @throws MessagingException 
	 * @throws IOException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @
	 */
	@RequestMapping(value="/itg/system/email/updateApproval.do")
	public @ResponseBody ResultVO  updateApproval(@RequestBody ModelMap paramMap)throws NkiaException, SQLException, JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, MessagingException {
		ResultVO resultVO = new ResultVO();
//		try{
//	    	//String resultMsg = emailInfoService.updateApproval(paramMap);
//			resultVO.setResultMsg(resultMsg);
//		} catch(Exception e) {
//			throw new NkiaException(e);
//		}
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		String srId = (String)paramMap.get("SR_ID");
		int taskId = Integer.parseInt((String)paramMap.get("TASK_ID"));
		String userId = (String)paramMap.get("USER_ID");
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("task_id", taskId);
		List insertList = new ArrayList();
		insertList = commonDAO.updateApprovalList(searchMap);
		
		if(userId != null && !"".equals(userId)) {
			int count = postWorkDAO.isExistNbpmUser(userId);
			if(count == 0) {
				postWorkDAO.insertNbpmUser(userId);
			}
		}
		
		if(insertList.size() > 0){
			for(int i=0; i<insertList.size(); i++){
				Map processInfoMap = new HashMap();
				processInfoMap = (Map) insertList.get(i);
									
				Map insertMap = new HashMap();
				 
				insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
				insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
				insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
				insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
				insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
				insertMap.put("workerUserId", userId);
				insertMap.put("nbpm_task_name", String.valueOf(processInfoMap.get("TASK_NAME")));
				insertMap.put("nbpm_comment", (String)paramMap.get("COMMENT"));
				
				//nbpmCommonClient.workTaskForRule(insertMap);
				NbpmProcessVO processVO = NbpmUtil.setProcessParameter(insertMap);
			
				//프로세스에서 사용하기로 정의된 변수 명들을 조회하고 화면에서 넘어온 데이터를 비교하여 일치하는게 있다면 값을 세팅한다.
				//WORK_TYPE은 일반 처리로 반드시 세팅된다.
				String workType = (String)paramMap.get("STATE");
				if("cancel".equals(workType)){
					processVO.setWorkType(NbpmConstraints.WORKTYPE_RETURN_PROC);
				}else{
					processVO.setWorkType(NbpmConstraints.WORKTYPE_NORMAL_PROC);
				}
				Map returnMap = new HashMap();
				
				returnMap = processExecutor.workTask(processVO, insertMap);
				returnMap.put("email_yn", "Y");
				returnMap.put("workerUserId", userId);
				processExecutor.notifyTask(processVO, returnMap);
			}
		}else{
			resultMsg = "이미완료 되었습니다.";
		}
		resultVO.setResultMsg(resultMsg);
		
		return resultVO;
	}
}
