package com.nkia.itg.system.email.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("emailInfoDAO")
public class EmailInfoDAO extends EgovComAbstractDAO {
	
	public EmailVO searchEmailInfo(ModelMap paramMap) throws NkiaException {
		return (EmailVO) selectByPk("EmailInfoDAO.searchEmailInfo", paramMap);
	}
	
	public HashMap selectEmailInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap) this.selectByPk("EmailInfoDAO.selectEmailInfo", paramMap);
	}
	
	public List selectEmailSenderInfoList() {
		List resultList = new ArrayList();
		List infoList = list("EmailInfoDAO.selectEmailSenderInfoList", null);
		
		if (infoList.size() > 0) {
			for(int i = 0 ; i < infoList.size() ; i++) {
				Map temp = (Map)infoList.get(i);
				
				Map data = new HashMap();
				data.put("EMAIL_CONFIG_ID", temp.get("EMAIL_ID"));
				
				JSONObject json = new JSONObject();
				json.putAll(temp);
				
				data.put("EMAIL_CONFIG_DATA", json.toString());
				resultList.add(data);
			}
		}
		
		return resultList;
	}
	
	public List selectEmailSenderList() {
		return list("EmailInfoDAO.selectEmailSenderList", null);
	}
	
	public void updateEmailInfo(HashMap paramMap) throws NkiaException {
		this.update("EmailInfoDAO.updateEmailInfo", paramMap);
	}
	
	public void updateHappyCall(HashMap paramMap) throws NkiaException {
		this.update("EmailInfoDAO.updateHappyCall", paramMap);
	}
	
	
}
