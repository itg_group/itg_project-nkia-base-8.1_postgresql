package com.nkia.itg.system.email.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("emailHistoryDAO")
public class EmailHistoryDAO extends EgovComAbstractDAO {
	
	public void insertMailSendHistory(EmailVO emailVO) throws NkiaException {
		insert("emailHistoryDAO.insertMailSendHistory", emailVO);
	}
	
	public List selectMailSendHistory(HashMap paramMap) throws NkiaException {
		return list("emailHistoryDAO.selectMailSendHistory", paramMap);
	}

	public int selectMailSendHistoryCount(HashMap paramMap) throws NkiaException {
		return (Integer)selectByPk("emailHistoryDAO.selectMailSendHistoryCount", paramMap);
	}
	

}
