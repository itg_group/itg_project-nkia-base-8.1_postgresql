package com.nkia.itg.system.email.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("emailTemplateDAO")
public class EmailTemplateDAO extends EgovComAbstractDAO {
	
	public List selectEmailTemplateInfo() {
		List resultList = new ArrayList();
		List infoList = list("EmailTemplateDAO.selectEmailTemplateInfo", null);
		
		if (infoList.size() > 0) {
			for(int i = 0 ; i < infoList.size() ; i++) {
				Map temp = (Map)infoList.get(i);
				
				Map data = new HashMap();
				data.put("EMAIL_TEMPLATE_ID", temp.get("EMAIL_TEMPLATE_ID"));
				
				JSONObject json = new JSONObject();
				json.putAll(temp);
				
				data.put("EMAIL_TEMPLATE_DATA", json.toString());
				resultList.add(data);
			}
		}
		
		return resultList;
	}
	
}
