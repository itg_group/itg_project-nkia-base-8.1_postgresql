package com.nkia.itg.system.email.service.impl;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;

import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.email.EmailSetData;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.application.util.security.CryptionChange;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmConstraints;
import com.nkia.itg.nbpm.executor.AbstractNbpmCommand;
import com.nkia.itg.nbpm.executor.AfterWorkCmd;
import com.nkia.itg.nbpm.executor.WorkTaskCmd;
import com.nkia.itg.nbpm.process.dao.PostWorkDAO;
import com.nkia.itg.nbpm.process.service.PostWorkExecutor;
import com.nkia.itg.nbpm.provide.common.api.impl.NbpmCommonClient;
import com.nkia.itg.nbpm.rule.common.dao.CommonRuleDAO;
import com.nkia.itg.nbpm.util.NbpmUtil;
import com.nkia.itg.nbpm.vo.NbpmProcessVO;
import com.nkia.itg.system.email.dao.EmailInfoDAO;
import com.nkia.itg.system.email.service.EmailInfoService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;


@Service("emailInfoService")
public class EmailInfoServiceImpl implements EmailInfoService {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailInfoServiceImpl.class);

	@Resource(name = "emailInfoDAO")
	public EmailInfoDAO emailInfoDAO;
	
	@Resource(name = "cryptionChange")
	public CryptionChange cryptionChange;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	/**
	 * 이메일계정 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public EmailVO searchEmailInfo(ModelMap paramMap) throws NkiaException {
		
		EmailVO resultVO = emailInfoDAO.searchEmailInfo(paramMap);
		
		//String encryptPw = (String) resultMap.get("EMAIL_ADMIN_PW");
		//String decryptPw = cryptionChange.decryptPassWord(encryptPw);
		
		//resultMap.put("EMAIL_ADMIN_PW", decryptPw);
		
		return resultVO;
	}
	
	
	/**
	 * 이메일계정 상세정보 조회
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectEmailInfo(ModelMap paramMap) throws NkiaException {
		HashMap resultMap = new HashMap();
		resultMap = emailInfoDAO.selectEmailInfo(paramMap);
		//String encryptPw = (String) resultMap.get("EMAIL_ADMIN_PW");
		//String decryptPw = cryptionChange.decryptPassWord(encryptPw);
		
		//resultMap.put("EMAIL_ADMIN_PW", decryptPw);
		
		return resultMap;
	}
	
	/**
	 * 이메일계정 정보 업데이트
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateEmailInfo(ModelMap paramMap) throws NkiaException {
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
    	
    	String decryptPw = (String) paramMap.get("email_admin_pw");
    	String encryptPw = cryptionChange.encryptPassWord(decryptPw);
    	
    	paramMap.put("email_admin_pw", encryptPw);
    	
		emailInfoDAO.updateEmailInfo(paramMap);
	}
	
	/**
	 * 테스트 이메일 보내기
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws MessagingException 
	 * @throws IOException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public void sendTestEmail(ModelMap paramMap) throws NkiaException, JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, MessagingException {
		
		try {
			Map alarmMap = new HashMap();
			alarmMap.put("KEY", "TEST");
			//알림 메일
			Map dataMap = new HashMap();
			// 1. 메일수신자셋팅(LIST로)
			List user = new ArrayList();
			HashMap toData = new HashMap();
			toData.put("TARGET_USER_ID", (String) paramMap.get("email_admin_id"));
			toData.put("EMAIL", (String) paramMap.get("email_admin_addr"));
			user.add(toData);
			dataMap.put("TO_USER_LIST", user);
			// 2. 제목, 내용
			dataMap.put("TITLE", "[운영자동화] 메일계정 변경 테스트");
			dataMap.put("CONTENT", "<br/>메일계정이 성공적으로 변경되었습니다.<br/><br/>");
			alarmMap.put("DATA", dataMap);
			//메일발송유형, 템플릿ID, 알림정보, 추가정보
			EmailSetData emailSender = new EmailSetData(alarmMap);
			emailSender.sendMail();
			
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
	}
	
	/**
	 * 만족도조사 업데이트
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateHappyCall(ModelMap paramMap) throws NkiaException {
		emailInfoDAO.updateHappyCall(paramMap);
	}
	
	/**
	 * 승인 시 진행 업데이트
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */
	public String updateApproval(ModelMap paramMap) throws NkiaException, SQLException {
		CommonRuleDAO commonDAO = (CommonRuleDAO)NkiaApplicationContext.getCtx().getBean("commonRuleDAO");
		PostWorkExecutor processExecutor = (PostWorkExecutor) NkiaApplicationContext.getCtx().getBean("processCommonExecutor");
		String resultMsg = messageSource.getMessage("msg.common.result.00004");
		String srId = (String)paramMap.get("SR_ID");
		int taskId = Integer.parseInt((String)paramMap.get("TASK_ID"));
		String userId = (String)paramMap.get("USER_ID");
		Map searchMap = new HashMap();
		searchMap.put("sr_id", srId);
		searchMap.put("task_id", taskId);
		List insertList = new ArrayList();
		insertList = commonDAO.updateApprovalList(searchMap);
		
		if(insertList.size() > 0){
			for(int i=0; i<insertList.size(); i++){
				Map processInfoMap = new HashMap();
				processInfoMap = (Map) insertList.get(i);
									
				Map insertMap = new HashMap();
				 
				insertMap.put("nbpm_task_id", String.valueOf(processInfoMap.get("ID")));
				insertMap.put("sr_id", String.valueOf(processInfoMap.get("SR_ID")));
				insertMap.put("nbpm_processId", String.valueOf(processInfoMap.get("PROCESSID")));
				insertMap.put("nbpm_processInstanceId", String.valueOf(processInfoMap.get("PROCESSINSTANCEID")));
				insertMap.put("nbpm_process_type", String.valueOf(processInfoMap.get("REQ_TYPE")));
				insertMap.put("workerUserId", userId);
				insertMap.put("nbpm_task_name", String.valueOf(processInfoMap.get("TASK_NAME")));
				insertMap.put("nbpm_comment", (String)paramMap.get("COMMENT"));
				
				//nbpmCommonClient.workTaskForRule(insertMap);
				NbpmProcessVO processVO = NbpmUtil.setProcessParameter(insertMap);
			
				//프로세스에서 사용하기로 정의된 변수 명들을 조회하고 화면에서 넘어온 데이터를 비교하여 일치하는게 있다면 값을 세팅한다.
				//WORK_TYPE은 일반 처리로 반드시 세팅된다.
				String workType = (String)paramMap.get("STATE");
				if("cancel".equals(workType)){
					processVO.setWorkType(NbpmConstraints.WORKTYPE_RETURN_PROC);
				}else{
					processVO.setWorkType(NbpmConstraints.WORKTYPE_NORMAL_PROC);
				}
				processExecutor.workTask(processVO, insertMap);
				
				
			}
		}else{
			resultMsg = "이미완료 되었습니다.";
		}
		return resultMsg;
	}
	

}
