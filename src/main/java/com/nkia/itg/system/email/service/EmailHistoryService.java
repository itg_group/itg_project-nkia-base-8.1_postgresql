package com.nkia.itg.system.email.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;

public interface EmailHistoryService {
	
	public void insertMailSendHistory(EmailVO emailVO) throws NkiaException;
	
	public List selectMailSendHistory(HashMap paramMap) throws NkiaException;
	
	public int selectMailSendHistoryCount(HashMap paramMap) throws NkiaException;

}
