package com.nkia.itg.system.listWidget.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ListWidgetService {
	
	public List selectSearchPartArr(Map paramMap) throws NkiaException;
	
	public List selectGridPartArr(Map paramMap) throws NkiaException;
	
	public int searchWidgetListCount(Map paramMap) throws NkiaException;
	
	public List searchWidgetList(Map paramMap) throws NkiaException, SQLException;
	
	public String checkOverlap(Map paramMap) throws NkiaException;
	
	public void insertListMenuConf(Map paramMap) throws NkiaException;
	
	public void updateListMenuConf(Map paramMap) throws NkiaException;
	
	public List searchSchemaList(Map paramMap) throws NkiaException;
	
	public List searchSchemaColmnName(Map paramMap) throws NkiaException;
	
	public List selectSchemaRows(Map paramMap) throws NkiaException;
	
	public void insertSchemaRealationConf(Map paramMap) throws NkiaException;
	
	public void updateSchemaRealationConf(Map paramMap) throws NkiaException;
	
	public String selectTableInfo(Map paramMap) throws NkiaException;
	
	public String selectRelationInfo(Map paramMap) throws NkiaException;
	
	public void insertColumnConf(JSONObject jsonObject ) throws NkiaException;
	
	public void updateColumnConf(String list_id, JSONArray jsonArray) throws NkiaException;
	
	public List selectFunctionList(Map paramMap) throws NkiaException;
	
	public List selectColumnInfo(Map paramMap) throws NkiaException;
	
	public void insertSearchConf(Map paramMap) throws NkiaException;
	
	public void updateSearchConf(Map paramMap) throws NkiaException;
	
	public List selectMenuInfo(Map paramMap) throws NkiaException;
	
	public List selectSearchInfo(Map paramMap) throws NkiaException;
	
	public void updateSaveConf(JSONObject jsonObject) throws NkiaException;
	
	public String selectFixWhere(String list_id) throws NkiaException;

	public List searchListWidgetPopupList(Map paramMap) throws NkiaException;

	public void deleteListWidgetList(Map paramMap) throws NkiaException;
	
	public List searchExcelList(Map paramMap) throws NkiaException, SQLException;
	
	public List searchCiClassTree(ModelMap paramMap) throws NkiaException;
	
	public List searchServiceTree(ModelMap paramMap) throws NkiaException;

	//@@정중훈20131122 엑셀용 List 추가
	public List selectExcelPartArr(Map paramMap) throws NkiaException;
	
	//@@이미옥20170928 추가
	public List searchResultSchemaList(String search_table_name) throws NkiaException;
}
