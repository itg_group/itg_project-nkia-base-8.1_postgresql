package com.nkia.itg.system.listWidget.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.listWidget.dao.ListWidgetDAO;
import com.nkia.itg.system.listWidget.service.ListWidgetService;

@Service("listWidgetService")
public class ListWidgetServiceImpl implements ListWidgetService {

	private static final Logger logger = LoggerFactory.getLogger(ListWidgetServiceImpl.class);
	
	@Resource(name = "listWidgetDAO")
	public ListWidgetDAO listWidgetDAO;
	
	public List selectSearchPartArr(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectSearchPartArr(paramMap);
	}
	
	public List selectGridPartArr(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectGridPartArr(paramMap);
	}
	
	public int searchWidgetListCount(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return listWidgetDAO.searchWidgetListCount(paramMap);
	}
	
	public List searchWidgetList(Map paramMap) throws NkiaException, SQLException {
		// TODO Auto-generated method stub
		return listWidgetDAO.searchWidgetList(paramMap);
	}
	
	public String checkOverlap(Map paramMap) throws NkiaException {
		return listWidgetDAO.checkOverlap(paramMap);
	}
	
	public void insertListMenuConf(Map paramMap) throws NkiaException {
		listWidgetDAO.insertListMenuConf(paramMap);
	}
	
	public void updateListMenuConf(Map paramMap) throws NkiaException {
		listWidgetDAO.updateListMenuConf(paramMap);
	}
	
	public List searchSchemaList(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return listWidgetDAO.searchSchemaList(paramMap);
	}
	
	public List searchSchemaColmnName(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return listWidgetDAO.searchSchemaColmnName(paramMap);
	}
	
	public List selectSchemaRows(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return listWidgetDAO.selectSchemaRows(paramMap);
	}
	
	public void insertSchemaRealationConf(Map paramMap) throws NkiaException {
		listWidgetDAO.insertSchemaConf(paramMap);
		listWidgetDAO.insertRelataionConf(paramMap);
	}
	
	public void updateSchemaRealationConf(Map paramMap) throws NkiaException {
		listWidgetDAO.updateSchemaConf(paramMap);
		listWidgetDAO.updateRelataionConf(paramMap);
	}
	
	public String selectTableInfo(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectTableInfo(paramMap);
	}
	
	public String selectRelationInfo(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectRelationInfo(paramMap);
	}
	
	public void insertColumnConf(JSONObject jsonObject) throws NkiaException {
		listWidgetDAO.insertColumnConf(jsonObject);
	}
	
	public void deleteColumnConf(String list_id) throws NkiaException {
		listWidgetDAO.deleteColumnConf(list_id);
	}
	
	public void updateColumnConf(String list_id, JSONArray jsonArray) throws NkiaException {
		listWidgetDAO.deleteColumnConf(list_id);
		for (int i=0,ii=jsonArray.size(); i<ii; i++) {
			listWidgetDAO.insertColumnConf(jsonArray.getJSONObject(i));
		}
	}
	
	public List selectFunctionList(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectFunctionList(paramMap);
	}
	
	public List selectColumnInfo(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectColumnInfo(paramMap);
	}
	
	public void insertSearchConf(Map paramMap) throws NkiaException {
		JSONArray columnInfo = JSONArray.fromObject(JSONSerializer.toJSON(paramMap.get("insertParam")));
		for (int i=0,ii=columnInfo.size(); i<ii; i++) {
			listWidgetDAO.insertSearchConf(columnInfo.getJSONObject(i));
		}
		
		JSONObject fix_where = (JSONObject) JSONSerializer.toJSON(paramMap.get("fix_where"));
		String list_id = String.valueOf(paramMap.get("list_id"));
		
		Map param = new HashMap();
		param.put("fix_where", fix_where.getString("fix_where"));
		param.put("list_id", list_id);
		
		listWidgetDAO.updateFixWhere(param);
	}
	
	public void updateSearchConf(Map paramMap) throws NkiaException {
		JSONArray columnInfo = JSONArray.fromObject(JSONSerializer.toJSON(paramMap.get("insertParam")));
		String list_id = String.valueOf(paramMap.get("list_id"));
		
		int deleteCnt = listWidgetDAO.deleteSearchConf(list_id);
		if (columnInfo.size() > 0) {
			for (int i=0,ii=columnInfo.size(); i<ii; i++) {
				listWidgetDAO.insertSearchConf(columnInfo.getJSONObject(i));
			}
		}
		
		JSONObject fix_where = (JSONObject) JSONSerializer.toJSON(paramMap.get("fix_where"));
		
		Map param = new HashMap();
		param.put("fix_where", fix_where.getString("fix_where"));
		param.put("list_id", list_id);
		
		listWidgetDAO.updateFixWhere(param);
	}
	
	public List selectMenuInfo(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectMenuInfo(paramMap);
	}
	
	public List selectSearchInfo(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectSearchInfo(paramMap);
	}
	
	public void updateSaveConf(JSONObject jsonObject) throws NkiaException {
		listWidgetDAO.updateSaveConf(jsonObject);
	}
	
	public String selectFixWhere(String list_id) throws NkiaException{
		String fix_where = listWidgetDAO.selectFixWhere(list_id);
		return fix_where;
	}

	public List searchListWidgetPopupList(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = listWidgetDAO.searchListWidgetPopupList(paramMap);
		return resultList;
	}

	public void deleteListWidgetList(Map paramMap) throws NkiaException {
		
		String deleteListId = (String) paramMap.get("ListId");
		String[] deleteListArray =  deleteListId.split(",");
		String ListId;
		
		// 삭제 부분 시작 (총 5개)
		for (int i = 0; i < deleteListArray.length; i++) {
			ListId = deleteListArray[i];
			listWidgetDAO.deleteTableInfo(ListId);
			listWidgetDAO.deleteColumnInfo(ListId);
			listWidgetDAO.deleteJoinInfo(ListId);
			listWidgetDAO.deleteSearchInfo(ListId);
			listWidgetDAO.deleteListMaster(ListId);
		}
	}
	
	public List searchExcelList(Map paramMap) throws NkiaException, SQLException {
		// TODO Auto-generated method stub
		return listWidgetDAO.searchExcelList(paramMap);
	}
	
	public List searchCiClassTree(ModelMap paramMap) throws NkiaException {
		return listWidgetDAO.searchCiClassTree(paramMap);
	}
	
	public List searchServiceTree(ModelMap paramMap) throws NkiaException {
		return listWidgetDAO.searchServiceTree(paramMap);
	}

	//@@정중훈20131122 엑셀용 List 추가
	public List selectExcelPartArr(Map paramMap) throws NkiaException {
		return listWidgetDAO.selectExcelPartArr(paramMap);
	}
	
	public List searchResultSchemaList(String search_table_name) throws NkiaException {
		return listWidgetDAO.searchResultSchemaList(search_table_name);
	}
	
}
