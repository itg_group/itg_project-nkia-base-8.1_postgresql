package com.nkia.itg.system.listWidget.dao;


import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("listWidgetDAO")
public class ListWidgetDAO extends EgovComAbstractDAO {

	public List selectSearchPartArr(Map paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ListWidgetDAO.selectSearchPartArr", paramMap);
		return resultList;
	}
	
	public List selectGridPartArr(Map paramMap)throws NkiaException {
		List resultList = null;
		resultList = list("ListWidgetDAO.selectGridPartArr", paramMap);
		return resultList;
	}
	
	public int searchWidgetListCount(Map paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("ListWidgetDAO.searchWidgetListCount", paramMap);
	}
	
	public List searchWidgetList(Map paramMap) throws NkiaException, SQLException{
		
		//@@정중훈 20131122 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 시작		
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("ListWidgetDAO.searchWidgetList", paramMap, handler);
		return handler.getReturnList();
		//@@정중훈 20131122 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 종료
		
		//return list("ListWidgetDAO.searchWidgetList", paramMap);
	}
	
	public String checkOverlap(Map paramMap) throws NkiaException {
		return (String)selectByPk("ListWidgetDAO.checkOverlap", paramMap);
	}
	
	public void insertListMenuConf(Map paramMap) throws NkiaException {
		this.insert("ListWidgetDAO.insertListMenuConf", paramMap);
	}
	
	public void updateListMenuConf(Map paramMap) throws NkiaException {
		this.update("ListWidgetDAO.updateListMenuConf", paramMap);
	}
	
	public List searchSchemaList(Map paramMap) throws NkiaException{
		return list("ListWidgetDAO.searchSchemaList", paramMap);
	}
	
	public List searchSchemaColmnName(Map paramMap) throws NkiaException{
		return list("ListWidgetDAO.searchSchemaColmnName", paramMap);
	}
	
	public List selectSchemaRows(Map paramMap) throws NkiaException{
		return list("ListWidgetDAO.selectSchemaRows", paramMap);
	}
	
	public void insertSchemaConf(Map paramMap) throws NkiaException {
		this.insert("ListWidgetDAO.insertSchemaConf", paramMap);
	}
	
	public void insertRelataionConf(Map paramMap) throws NkiaException {
		this.insert("ListWidgetDAO.insertRelataionConf", paramMap);
	}
	
	public void updateSchemaConf(Map paramMap) throws NkiaException {
		this.update("ListWidgetDAO.updateSchemaConf", paramMap);
	}
	
	public void updateRelataionConf(Map paramMap) throws NkiaException {
		this.update("ListWidgetDAO.updateRelataionConf", paramMap);
	}
	
	public String selectTableInfo(Map paramMap) throws NkiaException {
		String resultString = null;
		resultString = (String) this.selectByPk("ListWidgetDAO.selectTableInfo", paramMap);
		return resultString;
	}
	
	public String selectRelationInfo(Map paramMap) throws NkiaException {
		String resultString = null;
		resultString = (String) this.selectByPk("ListWidgetDAO.selectRelationInfo", paramMap);
		return resultString;
	}
	
	public void insertColumnConf(JSONObject jsonObject) throws NkiaException {
		this.insert("ListWidgetDAO.insertColumnConf", jsonObject);
	}
	
	public void deleteColumnConf(String list_id) throws NkiaException {
		this.delete("ListWidgetDAO.deleteColumnConf", list_id);
	}
	
	public List selectFunctionList(Map paramMap) throws NkiaException{
		return list("ListWidgetDAO.selectFunctionList", paramMap);
	}
	
	public List selectColumnInfo(Map paramMap) throws NkiaException{
		return list("ListWidgetDAO.selectColumnInfo", paramMap);
	}
	
	public void insertSearchConf(JSONObject jsonObject) throws NkiaException {
		this.insert("ListWidgetDAO.insertSearchConf", jsonObject);
	}
	
	public int deleteSearchConf(String list_id) throws NkiaException {
		int num = (Integer)this.delete("ListWidgetDAO.deleteSearchConf", list_id);
		return num;
	}
	
	public List selectMenuInfo(Map paramMap) throws NkiaException{
		return list("ListWidgetDAO.selectMenuInfo", paramMap);
	}
	
	public List selectSearchInfo(Map paramMap) throws NkiaException{
		return list("ListWidgetDAO.selectSearchInfo", paramMap);
	}
	
	public void updateSaveConf(JSONObject jsonObject) throws NkiaException {
		this.update("ListWidgetDAO.updateSaveConf", jsonObject);
	}
	
	public void updateFixWhere(Map paramMap) throws NkiaException {
		this.update("ListWidgetDAO.updateFixWhere", paramMap);
	}
	
	public String selectFixWhere(String list_id)throws NkiaException {
		// TODO Auto-generated method stub
		return (String)selectByPk("ListWidgetDAO.selectFixWhere", list_id);
	}

	public List searchListWidgetPopupList(Map paramMap) throws NkiaException {

		return list("ListWidgetDAO.searchListWidgetPopupList", paramMap);
	}

	// 삭제 시작
	public void deleteTableInfo(String string) throws NkiaException  {
		
		delete("ListWidgetDAO.deleteTableInfo", string);
	}

	public void deleteColumnInfo(String string) throws NkiaException  {
		delete("ListWidgetDAO.deleteColumnInfo", string);
	}

	public void deleteJoinInfo(String string) throws NkiaException {
		delete("ListWidgetDAO.deleteJoinInfo", string);
	}

	public void deleteSearchInfo(String string) throws NkiaException {
		delete("ListWidgetDAO.deleteSearchInfo", string);
	}

	public void deleteListMaster(String string) throws NkiaException {
		delete("ListWidgetDAO.deleteListMaster", string);
	}
	
	public List searchExcelList(Map paramMap) throws NkiaException, SQLException{
		
		//@@정중훈 20131122 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 시작		
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("ListWidgetDAO.searchExcelList", paramMap, handler);
		return handler.getReturnList();
		//@@정중훈 20131122 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 종료
		
		//return list("ListWidgetDAO.searchExcelList", paramMap);
	}
	
	public List searchCiClassTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("ListWidgetDAO.searchCiClassTree", paramMap);
		return resultList;
	}
	
	public List searchServiceTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("ListWidgetDAO.searchServiceTree", paramMap);
		return resultList;
	}

	//@@정중훈20131122 엑셀용 List 추가
	public List selectExcelPartArr(Map paramMap) throws NkiaException{
		List resultList = null;
		resultList = list("ListWidgetDAO.selectExcelPartArr", paramMap);
		return resultList;
	}
	
	public List searchResultSchemaList(String search_table_name) throws NkiaException{
		return list("ListWidgetDAO.searchResultSchemaList", search_table_name);
	}
}
