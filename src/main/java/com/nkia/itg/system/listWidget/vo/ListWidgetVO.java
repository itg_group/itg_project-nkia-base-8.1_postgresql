package com.nkia.itg.system.listWidget.vo;

import java.io.Serializable;

public class ListWidgetVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String list_id;
	private String seq;
	private String column_nm;
	private String label_nm;
	private String rows_orderby;
	private String column_length;
	private String column_orderby;
	private String view_type;
	private String view_yn;
	private String display_no;
	private String data_type;
	private String upd_user_id;
	private String upd_dt;
	
	private String left_key;
	private String right_key;
	private String user_add_type;
	
	private String list_nm;
	private String menu_id;
	private String state;
	private String excel_type;
	private String link_info;

	private String field_type;
	private String field_width;
	private String field_type_condition;
	private String merge_yn;
	private String detail_used_yn;
	
	public String getList_id() {
		return list_id;
	}
	public void setList_id(String list_id) {
		this.list_id = list_id;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getColumn_nm() {
		return column_nm;
	}
	public void setColumn_nm(String column_nm) {
		this.column_nm = column_nm;
	}
	public String getLabel_nm() {
		return label_nm;
	}
	public void setLabel_nm(String label_nm) {
		this.label_nm = label_nm;
	}
	public String getRows_orderby() {
		return rows_orderby;
	}
	public void setRows_orderby(String rows_orderby) {
		this.rows_orderby = rows_orderby;
	}
	public String getColumn_length() {
		return column_length;
	}
	public void setColumn_length(String column_length) {
		this.column_length = column_length;
	}
	public String getColumn_orderby() {
		return column_orderby;
	}
	public void setColumn_orderby(String column_orderby) {
		this.column_orderby = column_orderby;
	}
	public String getView_type() {
		return view_type;
	}
	public void setView_type(String view_type) {
		this.view_type = view_type;
	}
	public String getView_yn() {
		return view_yn;
	}
	public void setView_yn(String view_yn) {
		this.view_yn = view_yn;
	}
	public String getDisplay_no() {
		return display_no;
	}
	public void setDisplay_no(String display_no) {
		this.display_no = display_no;
	}
	public String getData_type() {
		return data_type;
	}
	public void setData_type(String data_type) {
		this.data_type = data_type;
	}
	public String getUpd_user_id() {
		return upd_user_id;
	}
	public void setUpd_user_id(String upd_user_id) {
		this.upd_user_id = upd_user_id;
	}
	public String getUpd_dt() {
		return upd_dt;
	}
	public void setUpd_dt(String upd_dt) {
		this.upd_dt = upd_dt;
	}
	public String getLeft_key() {
		return left_key;
	}
	public void setLeft_key(String left_key) {
		this.left_key = left_key;
	}
	public String getRight_key() {
		return right_key;
	}
	public void setRight_key(String right_key) {
		this.right_key = right_key;
	}
	public String getUser_add_type() {
		return user_add_type;
	}
	public void setUser_add_type(String user_add_type) {
		this.user_add_type = user_add_type;
	}
	public String getList_nm() {
		return list_nm;
	}
	public void setList_nm(String list_nm) {
		this.list_nm = list_nm;
	}
	public String getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getExcel_type() {
		return excel_type;
	}
	public void setExcel_type(String excel_type) {
		this.excel_type = excel_type;
	}
	public String getLink_info() {
		return link_info;
	}
	public void setLink_info(String link_info) {
		this.link_info = link_info;
	}
	public String getField_type() {
		return field_type;
	}
	public void setField_type(String field_type) {
		this.field_type = field_type;
	}
	public String getMerge_yn() {
		return merge_yn;
	}
	public void setMerge_yn(String merge_yn) {
		this.merge_yn = merge_yn;
	}
	public String getDetail_used_yn() {
		return detail_used_yn;
	}
	public void setDetail_used_yn(String detail_used_yn) {
		this.detail_used_yn = detail_used_yn;
	}
	public String getField_type_condition() {
		return field_type_condition;
	}
	public void setField_type_condition(String field_type_condition) {
		this.field_type_condition = field_type_condition;
	}
	public String getField_width() {
		return field_width;
	}
	public void setField_width(String field_width) {
		this.field_width = field_width;
	}
}
