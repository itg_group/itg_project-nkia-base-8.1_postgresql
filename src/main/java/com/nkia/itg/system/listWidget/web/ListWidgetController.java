/*
 * @(#)ListWidgetController.java              2013. 2. 25.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.listWidget.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.dept.service.DeptService;
import com.nkia.itg.system.listWidget.service.ListWidgetService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * @version 1.0 2013. 2. 25.
 * @author <a href="mailto:sangha@nkia.co.kr"> SangHa
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
@Controller
public class ListWidgetController {

	@Resource(name = "listWidgetService")
	private ListWidgetService listWidgetService;
	
	@Resource(name = "deptService")
	private DeptService deptService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/system/listWidget/listWidgetMain.do")
	public String listWidgetMain(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/listWidget/listWidgetMain.nvf";
		String listId = request.getParameter("listId");
		paramMap.addAttribute("list_id", listId);
		
		//@@ 2017-05-25 HD용 김다희 계열사별 조회 추가
		String isCustAuth = request.getParameter("isCustAuth");
		if(!"".equals(isCustAuth) && isCustAuth != null){
			paramMap.addAttribute("isCustAuth", isCustAuth);
		}
		
		String proc_state = request.getParameter("PROC_STATE");
		if (proc_state != null && !"".equals(proc_state)) {
			paramMap.addAttribute("PROC_STATE", proc_state);
		}
		
		//@@ 20141003 정정윤 추가
		// 서치 맵 따로 빼놓음
		String searchValueMapStr = createConvertString(request.getParameter("searchValueMap"));
		
		//@@ 20131022 고한조 수정 Start
		// 서비스 포탈에서 접속시 일부 조건 추가 및 검색조건 변경
		String portalParam = request.getParameter("portal_param");
		paramMap.addAttribute("portal_param", portalParam);
		
		//@@ 20141003 정정윤 추가 로직
		//포탈에서 넘어올 경우 미리 파라메터 세팅(보안결함 사항)
		if(portalParam != null) {
			if(!searchValueMapStr.equals("")) {
				UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
				searchValueMapStr = searchValueMapStr.replace("}", ",'S_REQ_USER_ID':'" + userVO.getUser_id() + "'}");
				// {'키_1':'값_1'} -> {'키_1':'값_1','S_REQ_USER_ID':'omsadmin'} 으로 치환
			}
		}
		//@@ 20131022 고한조 수정 End
		
		if (null != request.getParameter("searchValueMap")) {
			//paramMap.addAttribute("searchValueMap", request.getParameter("searchValueMap"));
			JSONObject obj =  JSONObject.fromObject( searchValueMapStr );
			//createRequestMap(request, paramMap);
			paramMap.addAttribute("searchValueMap", obj);
		} else {
			createRequestMap(request, paramMap);
		}
		
		return forwarPage;
	}
	
	/**
	 * List Widget Init(Make Search Part & List Layout)
	 * 
	 * @param paramMap
	 * @return string
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/listWidgetList.do")
	public String listWidgetList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/listWidget/listWidgetList.nvf";
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		try {
			List searchPartArr = new ArrayList();
			List gridPartArr = new ArrayList();
			List menuArr = new ArrayList();
			//@@정중훈20131122 엑셀용 List 추가 시작
			List excelPartArr = new ArrayList();
			//@@정중훈20131122 엑셀용 List 추가 종료
			
			//KEY			
			String listId = request.getParameter("listId");
			paramMap.addAttribute("list_id", listId);
			
			paramMap.addAttribute("USER_CUST", userVO.getCust_id());
			
			//@@ 2017-05-25 HD용 김다희 계열사별 조회 추가
			String isCustAuth = request.getParameter("isCustAuth");
			if(!"".equals(isCustAuth) && isCustAuth != null){
				paramMap.addAttribute("isCustAuth", isCustAuth);
			}
			
			String proc_state = request.getParameter("PROC_STATE");
			if (proc_state != null && !"".equals(proc_state)) {
				paramMap.addAttribute("PARAM_PROC_STATE", proc_state);
			}
			
			searchPartArr = listWidgetService.selectSearchPartArr(paramMap);
			JSONArray searchPartJson = JSONArray.fromObject(searchPartArr);
			
			HashMap passSearchValueMap = new HashMap();
			Map tempMap = request.getParameterMap();
			if (tempMap != null) {
				Iterator keySet = tempMap.keySet().iterator();
				while(keySet.hasNext()) {
					String paramName = (String)keySet.next();
					String paramValue = request.getParameter(paramName);
					passSearchValueMap.put(paramName, paramValue);
				}
				paramMap.put("passSearchValueMap", passSearchValueMap);
			}
			JSONObject obj =  JSONObject.fromObject(passSearchValueMap);
			paramMap.put("passSearchValueJson", obj);
			
			
			
			gridPartArr = listWidgetService.selectGridPartArr(paramMap);
			JSONArray gridPartJson = JSONArray.fromObject(gridPartArr);
			
			//@@정중훈20131122 엑셀용 List 추가 시작
			excelPartArr = listWidgetService.selectExcelPartArr(paramMap);
			JSONArray excelPartJson = JSONArray.fromObject(excelPartArr);
			//@@정중훈20131122 엑셀용 List 추가 종료
			
			menuArr = listWidgetService.selectMenuInfo(paramMap);
			JSONArray menuInfoJson = JSONArray.fromObject(menuArr);
			
			//로그인 id 조회
			String loginUserId = userVO.getUser_id();
			
			//로그인 사용자의 부서 정보를 조건으로 부서검색권한 조회
			String search_cust_auth = deptService.selectSearchCustAuth(loginUserId);
			paramMap.addAttribute("search_cust_auth", search_cust_auth);
			
			paramMap.addAttribute("searchPartJson", searchPartJson);
			paramMap.addAttribute("gridPartJson", gridPartJson);
			paramMap.addAttribute("menuInfoJson", menuInfoJson);
			paramMap.addAttribute("excelPartJson", excelPartJson);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 
	 * Getting ListWidget List
	 * 
	 * @param paramMap
	 * @return
	 * @throws SQLException 
	 * @
	 */
	
	@RequestMapping(value="/itg/system/listWidget/searchWidgetList.do")
	public @ResponseBody ResultVO searchWidgetList(@RequestBody ModelMap paramMap)throws NkiaException, SQLException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();

		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			//@@ 페이징 관련 내용
			PagingUtil.getFristEndNum(paramMap);
			
			String sort_column = (String)paramMap.get("sort_column");
			
			String listId = (String)paramMap.get("listId");
			paramMap.put("list_id", listId);
			
			List columnList = listWidgetService.selectColumnInfo(paramMap);
			String column_str = "";
			String orderby_str = "";
			HashMap hashMap = null;
			
			for (int i=0,ii=columnList.size(); i<ii; i++) {
				hashMap = (HashMap)columnList.get(i);
				Iterator iterator = hashMap.entrySet().iterator();
				String column_nm = "";
				String table_nm = "";
				String view_type = "";
				String rows_orderby = "";
				
				while (iterator.hasNext()) {
					Entry entry = (Entry)iterator.next();
					if (entry.getValue() != null) {
						if ("COLUMN_NM".equals(entry.getKey())) {
							column_nm = "" + entry.getValue();
						} else if ("TABLE_NM".equals(entry.getKey())) {
							table_nm = "" + entry.getValue();
						} else if ("VIEW_TYPE".equals(entry.getKey())) {
							view_type = "" + entry.getValue();
						} else if ("ROWS_ORDERBY".equals(entry.getKey())) {
							rows_orderby = "" + entry.getValue();
						}
					}
				}
				
				if ("".equals(view_type)) {
					column_str += table_nm + "." + column_nm + ",";
				} else {
					/*if("REQ_ZONE".equals(column_nm)) {
						//column_str += view_type+" AS "+column_nm+",";
						// 2014.10.22. 정정윤 수정
						// 요청부대 앞에 [MDCD] 붙도록
						column_str +=
						"( SELECT '['||(SELECT CODE_NM FROM SYS_CODE WHERE CODE_GRP_ID='MDCD' AND CODE_ID = B.MDCD)||']'"
						+" || CUST_NM FROM SYS_REQ_ZONE B WHERE B.CUST_ID = PROC_MASTER.REQ_ZONE )" + " AS "+column_nm+",";
					} else {
						column_str += view_type+" AS "+column_nm+",";
					}*/
					
					if ("CONF_NM".equals(column_nm)) {
//						column_str += table_nm + "." + column_nm + ",";
						column_str += " ( "
							+ "	SELECT "
							+ "		LISTAGG(Z.CONF_NM,',') WITHIN GROUP(ORDER BY Z.SR_ID) "
							+ "	FROM ("
							+ "			SELECT AI.CONF_NM, PROC_MASTER.SR_ID AS SR_ID "
							+ "			FROM AM_INFRA AI "
							+ "			WHERE AI.CONF_ID IN ( SELECT C1.ITEM_ID "
							+ "						FROM PROC_REL_CONF C1 "	
							+ "						WHERE C1.SR_ID = PROC_MASTER.SR_ID "
							+ "							AND C1.SR_ID = PROC_SERVICE.SR_ID "
							+ "					) "
							+ "		UNION ALL "
							+ "			SELECT AW.CONF_NM, PROC_MASTER.SR_ID AS SR_ID "
							+ "			FROM AM_SW AW "
							+ "			WHERE AW.CONF_ID IN ( SELECT C2.ITEM_ID "								
							+ "						FROM PROC_REL_CONF C2 "								
							+ "						WHERE C2.SR_ID = PROC_MASTER.SR_ID "									
							+ "						AND C2.SR_ID = PROC_SERVICE.SR_ID "	
							+ "					)"
							+ "	) Z "
							+ ") AS " + column_nm + ",";
					} else {
						column_str += view_type + " AS " + column_nm + ",";
					}
				}
				
				if (!"".equals(rows_orderby)) {
					orderby_str += column_nm + " " + rows_orderby + ",";
				}
			}
			
			String schema_nm = listWidgetService.selectTableInfo(paramMap);
			List temp_list = listWidgetService.selectSearchInfo(paramMap);
			String fix_where = listWidgetService.selectFixWhere(listId);
			
			/* WHERE 조건문 절 같음. */
			String where_str = "WHERE 1 = 1 ";
			if (!"".equals(fix_where) && fix_where != null) {
				//20191028 파라미터가있으면 여기에 추가
				//where_str = listWidgetService.selectFixWhere(listId);
				where_str = fix_where.replace("#USER_ID#", userVO.getUser_id());
				//@@정중훈 20130917  계약대상 서치구문 추가 시작
				/*
				String reqZone = (String)paramMap.get("REQ_ZONE");
											
				if(null == reqZone
						|| reqZone.equals("")){
					where_str += "";
				}else{
					where_str += " AND PROC_MASTER.REQ_ZONE = '" + reqZone + "'";
				}
				*/
				//@@정중훈 20130917  계약대상 서치구문 추가 시작
				
			}
			
			//@@ 2017-05-25 HD용 김다희 계열사별 조회 추가
			String isCustAuth = (String)paramMap.get("isCustAuth");
			if (!"".equals(isCustAuth) && isCustAuth != null) {
				String custId = userVO.getCust_id();
				where_str = "WHERE 1=1 AND PROC_MASTER.SR_ID IN (SELECT SR_ID FROM PROC_OPER WHERE OPER_USER_ID IN (SELECT USER_ID FROM SYS_USER WHERE CUST_ID = '"+custId+"'))";
			}
			
			String where_value = "";
			int where_size = temp_list.size();
			
			//부서정보 조회가 검색조건에 있는지 확인
			//false : 없음, true : 있음
			Boolean searchCustAuth = false;
			//부서검색권한 정보
			String cust_auth = (String)paramMap.get("custAuth");
			HashMap cust_map = new HashMap();
			//커스터마이징 2018.05.12 molee
			//처리부서를 조건으로 추가하면서 추가
			//1.SYS_CUST, SYS_CUST_SEARCH_AUTH 테이블에서 로그인한 사용자의 검색가능한 부서를 구해
			//2.그 사용자가 PROC_OPER에 있으면 그 SR_ID를 구하여 결과를 구함
			//3.따라서 SR_ID 필드정보를 알아야 해서 선언(테이블정보+컬럼정보/PROC_MASTER.SR_ID형식이므로)
			if (where_size > 0) {
				for (int i=0,ii=where_size; i<ii; i++) {
					HashMap map = (HashMap)temp_list.get(i);
					String field_type = "" + map.get("FIELD_TYPE");
					boolean WHB = false;
					String table_nm = "";
					
					if (map.get("TABLE_NM")!= null && !"".equals(map.get("TABLE_NM"))) {
						table_nm = map.get("TABLE_NM")+".";
					}
					
					if ("TEXTFIELD_S".equals(field_type)) {
						where_value = (String)paramMap.get("T_"+map.get("COLUMN_NM"));
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					} else if ("DATE".equals(field_type)) {
						String sDate = (String)paramMap.get(map.get("COLUMN_NM") + "_startDate");
						String eDate = (String)paramMap.get(map.get("COLUMN_NM") + "_endDate");
						
						if ((!"".equals(sDate) && sDate != null) && (!"".equals(eDate) && eDate != null)) {
							WHB = true;
						}
					} else if ("MULTI_DATE".equals(field_type)) {
						String sDate = (String)paramMap.get(map.get("COLUMN_NM") + "_startDate");
						String eDate = (String)paramMap.get(map.get("COLUMN_NM") + "_endDate");
						
						if ((!"".equals(sDate) && sDate != null) || (!"".equals(eDate) && eDate != null)) {
							WHB = true;
						}
					} else if ("MULTI_DATE_S".equals(field_type)) {
						String sDate = (String)paramMap.get(map.get("COLUMN_NM")+"_startDate");
						String eDate = (String)paramMap.get(map.get("COLUMN_NM")+"_endDate");
						
						if ((!"".equals(sDate) && sDate != null) || (!"".equals(eDate) && eDate != null)) {
							WHB = true;
						}
					} else if ("SELECTBOX_M".equals(field_type)) {
						where_value = (String)paramMap.get("CC_" + map.get("COLUMN_NM"));
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					} else if ("SELECTBOX_CUST".equals(field_type)) {
						where_value = (String)paramMap.get("S_" + map.get("COLUMN_NM"));
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					} else if ("TREE_CUST".equals(field_type)) {
						searchCustAuth = true;
						cust_map.put("COLUMN_NM", map.get("COLUMN_NM"));
						where_value = (String)paramMap.get(map.get("COLUMN_NM") + "_field");
						if (!"".equals(where_value) && where_value != null) {
							//부서검색조건  값O
							WHB = true;
						}
					} else if ("TREE_REQ".equals(field_type)) {
						where_value = (String)paramMap.get(map.get("COLUMN_NM") + "_field");
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}	
					} else if ("TREE_PROBLEM".equals(field_type)) {
						where_value = (String)paramMap.get(map.get("COLUMN_NM") + "_field");
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}	
	
					} else if ("CI_CLASS_ID".equals(field_type)) {
						where_value = (String)paramMap.get("S_CLASS_ID");
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					} else if ("SERVICE_ID".equals(field_type)) {
						where_value = (String)paramMap.get("S_SERVICE_ID");
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					} else if ("multiCodePop".equals(field_type)) {
						where_value = (String)paramMap.get("S_"+map.get("COLUMN_NM"));
						if(!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					} else if ("REQ_ZONE_ID".equals(field_type)) {
						where_value = (String)paramMap.get("REQ_ZONE");
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					} else {
						where_value = (String)paramMap.get("S_"+map.get("COLUMN_NM"));
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					}
					
					if (WHB) {
						// 검색조건에 UPPER 추가 20140806 정영태
						if ("TEXTFIELD".equals(field_type)) {
							//textfield(ok)
							if (map.get("COLUMN_NM").equals("WORK_USER_NM")) {
								//작업자 조건
								where_str += " AND 0 <  (SELECT FC_GET_OPER_USER_CNT(SR_ID, '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "') FROM DUAL) ";
							} else { 
								if (map.get("COLUMN_NM").equals("CUSTOMER_NM")) {
									//고객사가 조회조건이면 서비스대상고객사도 추가 조건도 추가
									where_str += " AND (UPPER(" + map.get("COLUMN_NM") + ") like '%'||UPPER('" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "')||'%'"
											+"OR UPPER(SERVICE_TRGET_CUSTOMER_NM) LIKE '%'||UPPER('" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "')||'%')";
								}else{
									where_str += " AND UPPER(" + map.get("COLUMN_NM") + ") like '%'||UPPER('" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "')||'%'";	
								}

							}
						} else if("TEXTFIELD_P".equals(field_type)) {
							//textfield+button
							
						} else if("TEXTFIELD_S".equals(field_type)) {
							//selectbos+textfield(ok)
							boolean user_flag = false;
							if (paramMap.get("condition_user_flag") != null) {
								user_flag = (boolean) paramMap.get("condition_user_flag");
							}
							boolean group_flag = false;
							if (paramMap.get("condition_group_flag") != null) {
								group_flag = (boolean) paramMap.get("condition_group_flag");
							}
							 
							if (user_flag) {
//								where_str += " AND UPPER(" + (String)paramMap.get("C_" + map.get("COLUMN_NM")) + ") like UPPER('%" + (String)paramMap.get("T_" + map.get("COLUMN_NM")) + "%')";
								where_str += " AND 1 < FN_GET_LISTWIDGET_USER_CNT(SR_ID, " + cust_auth + ", " + (String)paramMap.get("C_" + map.get("COLUMN_NM")) + ", '" + (String)paramMap.get("T_" + map.get("COLUMN_NM")) + "') "; 
							} else if (group_flag) {
								where_str += " AND 1 < FN_GET_LISTWIDGET_GROUP_CNT(SR_ID, " + cust_auth + ", " + (String)paramMap.get("C_" + map.get("COLUMN_NM")) + ", '" + (String)paramMap.get("T_" + map.get("COLUMN_NM")) + "') "; 
							} else {
								
								where_str += " AND UPPER(" + (String)paramMap.get("C_" + map.get("COLUMN_NM")) + ") like UPPER('%" + (String)paramMap.get("T_" + map.get("COLUMN_NM")) + "%')";
							}
						} else if("SELECTBOX".equals(field_type)) {
							//selectbox(ok)
							// 2014.8.24 FREECHANG  수정 진행 상태
							if ("FC_GET_WORK_STATE_SEARCH_ID(WORK_STATE)".equals(map.get("COLUMN_NM"))) {
								if ("COMP".equals(paramMap.get("S_" + map.get("COLUMN_NM")))) {
									where_str += " AND FC_GET_WORK_STATE_SEARCH_ID(WORK_STATE) = 'END' "
											  +  " AND (END_TYPE IS NULL OR END_TYPE = 'COMP')";
									
								} else if ("REJECT".equals(paramMap.get("S_" + map.get("COLUMN_NM")))) {
									where_str += " AND FC_GET_WORK_STATE_SEARCH_ID(WORK_STATE) = 'END' "
											  +  " AND END_TYPE = 'REJECT' ";
								} else {
									// 종료유형 검색조건이 제대로 먹지 않아서 추가하였음 : 20140806 정영태
									where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "'";
								}
							} else {
								where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "'";
							}
							
						} else if ("SELECTBOX_M".equals(field_type)) {
							//mutil selectbox(ok)
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)paramMap.get("CC_" + map.get("COLUMN_NM")) + "'";
						} else if ("SELECTBOX_CUST".equals(field_type)) {
							//2018.05.12 molee
							where_str += " AND SR_ID IN ( "
									+ "SELECT "
									+ "		DISTINCT Z.SR_ID "
									+ "FROM ( "
									+ "			SELECT 'USER' AS GUBUN, A.SR_ID, B.OPER_TYPE, B.OPER_USER_ID, "
									+ "				C.CUST_ID AS OPER_CUST_ID "
									+ "			FROM PROC_MASTER A, PROC_OPER B, SYS_USER C "
									+ "			WHERE A.SR_ID = B.SR_ID "
									+ "				AND B.OPER_USER_ID = C.USER_ID "
									+ "				AND B.OPER_USER_ID != 'SYSTEM' "
									+ "				AND C.CUST_ID = '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "'"
									+ "		UNION ALL "
									+ "			SELECT 'GROUP' AS GUBUN, A.SR_ID, A.OPER_TYPE, B.CHARGER_ID AS OPER_USER_ID, "
									+ "				C.CUST_ID AS OPER_CUST_ID "
									+ "			FROM PROC_OPER A, WORK_CHARGER_MAPNG B, SYS_USER C "
									+ "			WHERE A.OPER_USER_ID = B.GRP_ID AND B.CHARGER_ID = C.USER_ID "
									+ "				AND B.CHARGER_ID != 'SYSTEM' "
									+ "				AND C.CUST_ID = '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "'"
									+ "	) Z "
									+ ")";
						} else if ("CHECKBOX".equals(field_type)) {
							//checkbox(ok)
							String[] checkValue = ((String)paramMap.get("S_" + map.get("COLUMN_NM"))).split(",");
							String in_str = "";
							for (int l=0,ll=checkValue.length; l<ll; l++) {
								in_str += "'" + checkValue[l] + "',";
							}
							in_str = in_str.substring(0, in_str.length()-1);
							
							where_str += " AND " + map.get("COLUMN_NM") + " IN (" + in_str + ")";
						} else if ("RADIO".equals(field_type)) {
							//radio(ok)
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "'";
						} else if ("DATE".equals(field_type)) {
							//date(ok)
							//where_str += " AND "+table_nm+map.get("COLUMN_NM")+" = '"+(String)paramMap.get("S_"+map.get("COLUMN_NM"))+"'";
							where_str += " AND " + map.get("COLUMN_NM")
									+" BETWEEN TO_DATE('" + (String)paramMap.get(map.get("COLUMN_NM") + "_startDate") + "' || '000000', ('YYYY-MM-DDHH24MISS'))"
									+" AND TO_DATE('" + (String)paramMap.get(map.get("COLUMN_NM") + "_endDate") + "' || '235959', ('YYYY-MM-DDHH24MISS'))";
						} else if ("MULTI_DATE".equals(field_type)) {
							String columnNm = (String) map.get("COLUMN_NM");
							String sDate = (String) paramMap.get(map.get("COLUMN_NM") + "_startDate");
							String eDate = (String) paramMap.get(map.get("COLUMN_NM") + "_endDate");

							if (!"".equals(sDate) && !"".equals(eDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD') AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, sDate, columnNm, eDate);
							} else if (!"".equals(sDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD')", columnNm, sDate);
							} else if (!"".equals(eDate)) {
								where_str += String.format(" AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, eDate);
							}
						} else if ("MULTI_DATE_S".equals(field_type)) {
							String columnNm = (String) paramMap.get("C_" + map.get("COLUMN_NM"));
							String sDate = (String) paramMap.get(map.get("COLUMN_NM") + "_startDate");
							String eDate = (String) paramMap.get(map.get("COLUMN_NM") + "_endDate");

							if (!"".equals(sDate) && !"".equals(eDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD') AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, sDate, columnNm, eDate);
							} else if (!"".equals(sDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD')", columnNm, sDate);
							} else if (!"".equals(eDate)) {
								where_str += String.format(" AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, eDate);
							}
						} else if ("TREE_CUST".equals(field_type)) {
							String fieldVal = (String)paramMap.get(map.get("COLUMN_NM") + "_field");
							if (fieldVal != null && !fieldVal.equals("")) {
								where_str += " AND " + map.get("COLUMN_NM") + " = '"
										+ (String)paramMap.get(map.get("COLUMN_NM") + "_field") + "'";
							} else {
								if (!cust_auth.equals("ALL")) {
									String user_cust_id = userVO.getCust_id();
									where_str += " AND " + map.get("COLUMN_NM") + " IN ( "
											+ "SELECT "
											+ "		A.AUTH_CUST_ID "
											+ "FROM SYS_CUST_SEARCH_AUTH A, SYS_CUST B "
											+ "WHERE A.AUTH_CUST_ID = B.CUST_ID "
											+ "AND A.CUST_ID = '"
											+ user_cust_id + "' )";
								}
							}
						} else if ("CI_CLASS_ID".equals(field_type)) {
							//요청현황 조회 전용(하드코딩/기타 다른 현황 제작시 별도 구현 필요)
							where_str += " AND PROC_MASTER.SR_ID IN(" +
									"SELECT" +
									"     SR_ID " +
									"FROM (" +
									"    SELECT" +
									"         A.SR_ID " +
									"     FROM" +
									"         PROC_REL_CONF A, AM_ASSET B,AM_INFRA C  " +
									"    WHERE 1=1 " +
									"    AND A.ITEM_ID = C.CONF_ID " +
									"    AND B.ASSET_ID = C.ASSET_ID " +
									"    AND C.CLASS_ID IN ( " +
									"        SELECT " +
									"             CLASS_ID " +
									"        FROM " +
									"             AM_CLASS " +
									"             START WITH CLASS_ID ='" + (String)paramMap.get("S_CLASS_ID") + "' CONNECT BY PRIOR CLASS_ID = UP_CLASS_ID " +
									"        )" +
									"	 UNION ALL " +
									"    SELECT " +
									"         A.SR_ID " +
									"     FROM " +
									"         PROC_REL_CONF A, AM_ASSET B,AM_SW C " +
									"    WHERE 1=1 " +
									"    AND A.ITEM_ID = C.CONF_ID " +
									"    AND B.ASSET_ID = C.ASSET_ID " +
									"    AND C.CLASS_ID IN ( " +
									"        SELECT " +
									"             CLASS_ID " +
									"        FROM " +
									"             AM_CLASS " +
									"             START WITH CLASS_ID ='" + (String)paramMap.get("S_CLASS_ID") + "'  CONNECT BY PRIOR CLASS_ID = UP_CLASS_ID" +
									"        ) " +
									"))";
						} else if ("SERVICE_ID".equals(field_type)) {
							//요청현황 조회 전용(하드코딩/기타 다른 현황 제작시 별도 구현 필요)
							where_str += " AND PROC_MASTER.SR_ID IN(" +
									"SELECT " +
									"     SR_ID " +
									"FROM " +
									"     PROC_REL_CONF A, AM_SERVICE B " +
									"WHERE 1=1 " +
									"AND A.ITEM_ID = B.SERVICE_ID AND B.SERVICE_ID IN ( " +
									"    SELECT " +
									"         SERVICE_ID " +
									"     FROM " +
									"         AM_SERVICE " +
									"         START WITH SERVICE_ID = '" + (String)paramMap.get("S_SERVICE_ID") + "' CONNECT BY PRIOR SERVICE_ID = UP_SERVICE_ID " +
									"    ) " +
									"GROUP BY SR_ID " +
									")";
						} else if ("multiCodePop".equals(field_type)) {
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "'";
						} else if ("REQ_ZONE_ID".equals(field_type)) {
							//where_str += " AND PROC_MASTER.REQ_ZONE = '" + (String)paramMap.get("REQ_ZONE") + "'";
							String reqZoneId = (String)paramMap.get("REQ_ZONE");
							if (reqZoneId!= null && !reqZoneId.equals("")) {
								where_str += (
										" AND PROC_MASTER.REQ_ZONE IN "
										+"("
										+ " SELECT CUST_ID FROM SYS_REQ_ZONE"
										+ " START WITH CUST_ID = '" + (String)paramMap.get("REQ_ZONE") + "'"
										+ " CONNECT BY PRIOR CUST_ID = UP_CUST_ID "
										+ ")"
										);
							}
						} else if ("TREE_REQ".equals(field_type)) {
							String reqId = (String)paramMap.get(map.get("COLUMN_NM") + "_field");
							if (reqId!= null && !reqId.equals("")) {
								where_str += (
										"AND REQ_DOC_ID IN (SELECT REQ_DOC_ID"
										+"	 FROM WORK_REQ_DOC WHERE USE_YN ='Y'"
										+"	 START WITH REQ_DOC_ID ='"+reqId +"'" 
										+"	 CONNECT BY PRIOR REQ_DOC_ID = UP_REQ_DOC_ID)"
										);
							}
						} else if ("TREE_PROBLEM".equals(field_type)) {
							String probId = (String)paramMap.get(map.get("COLUMN_NM") + "_field");
							if (probId!= null && !probId.equals("")) {
								where_str += (
										"AND INC_TYPE_CD IN (SELECT CODE_ID"
										+"	 FROM SYS_CODE WHERE USE_YN ='Y' AND CODE_GRP_ID ='INC_TYPE_CD'"
										+"	 START WITH CODE_ID ='"+probId +"'" 
										+"	 CONNECT BY PRIOR CODE_ID = UP_CODE_ID)"
										);
							}	
						} else {
							//default (textfield)(ok)
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)paramMap.get("S_" + map.get("COLUMN_NM")) + "'";
						}
					}
				}
				
				if (searchCustAuth) {
					if (cust_auth != null && !cust_auth.equals("ALL")) {
						String user_cust_id = userVO.getCust_id();
						if (cust_map.size() > 0) {
							for (int ii=0; ii<cust_map.size(); ii++) {
								where_str += " AND " + cust_map.get("COLUMN_NM") + " IN ( "
										+ "SELECT "
										+ "		A.AUTH_CUST_ID "
										+ "FROM SYS_CUST_SEARCH_AUTH A, SYS_CUST B "
										+ "WHERE A.AUTH_CUST_ID = B.CUST_ID "
										+ "AND A.CUST_ID = '"
										+ user_cust_id + "' )";
							}
						}
					}
				}
			}
			
			String join_key = listWidgetService.selectRelationInfo(paramMap);
			String join_str = "";
			
			if (!"".equals(join_key) && join_key != null) {
				//join_str = "WHERE 1=1 ";
				String[] join_arr = join_key.split(",");
				for (int i=0,ii=join_arr.length; i<ii; i++) {
					if (i == 0) {
						join_str += join_arr[i];
					} else {
						join_str += " and " + join_arr[i];
					}
					
				}
			}
			
			if (column_str.length() > 0) {
				column_str = column_str.substring(0, column_str.length()-1);
			}
			
			/*
			 * 2018.01.24 molee
			 * Rownum의 orderby로 주기 위해서 수정
			 * 
			 * 
			 * 
			if (orderby_str.length() > 0) {
				if (sort_column == null || "".equals(sort_column)) {
					orderby_str = " order by " + orderby_str.substring(0, orderby_str.length()-1);
				} else {
					orderby_str = "";
				}
			}*/
			
			if (orderby_str.length() > 0) {
				orderby_str = orderby_str.substring(0, orderby_str.length()-1);
			}
			
			//2018.01.25 molee 수정 -- start
			String sql_select_text = "";
			if (join_str != null && !join_str.equals("")) {
				sql_select_text = "select " + column_str
						+ " from " + schema_nm + " where "
						+ join_str;
			} else {
				sql_select_text = "select " + column_str
						+ " from " + schema_nm + " ";
			}
			
			String sql_where_text = where_str;
			String sql_rnum_orderby_text = orderby_str;
			
			paramMap.put("sql_select_text", sql_select_text);
			paramMap.put("sql_where_text", sql_where_text);
			paramMap.put("sql_rnum_orderby_text", sql_rnum_orderby_text);
			//2018.01.25 molee 수정 -- end
			
//			paramMap.put("first_num", start + 1);
//			paramMap.put("last_num", end);
		
			
//			if (!initFlag) {
				totalCount = listWidgetService.searchWidgetListCount(paramMap);
				resultList = listWidgetService.searchWidgetList(paramMap);		
//			}
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * List Widget Menu Configuration Page Call
	 * 
	 * @param paramMap
	 * @return string
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/listWidgetMenuConf.do")
	public String listWidgetMenuConf(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/listWidget/listWidgetMenuConf.nvf";
		try{
			List menuArr = new ArrayList();
			//KEY			
			String listId = request.getParameter("list_id");
			if(!"".equals(listId) && listId != null){
				paramMap.addAttribute("list_id", listId);
				paramMap.addAttribute("submit_type", "UPD");
			}
			menuArr = listWidgetService.selectMenuInfo(paramMap);
			JSONArray menuInfoJson = JSONArray.fromObject(menuArr);
			paramMap.addAttribute("menuInfoJson", menuInfoJson);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 중복확인
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/checkOverlap.do")
	public @ResponseBody ResultVO checkOverlap(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			resultVO.setResultString(listWidgetService.checkOverlap(paramMap));
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	
	@RequestMapping(value="/itg/system/listWidget/insertListMenuConf.do")
	public @ResponseBody ResultVO insertListMenuConf(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			listWidgetService.insertListMenuConf(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */	
	
	@RequestMapping(value="/itg/system/listWidget/updateListMenuConf.do")
	public @ResponseBody ResultVO updateListMenuConf(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			listWidgetService.updateListMenuConf(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * List Widget Table Configuration Page Call
	 * 
	 * @param paramMap
	 * @return string
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/listWidgetTableConf.do")
	public String listWidgetTableConf(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/listWidget/listWidgetTableConf.nvf";
		
		List manuArr = new ArrayList();
		
		try{
			//KEY			
			String listId = request.getParameter("listId");
			String submit_type = request.getParameter("submit_type");

			paramMap.addAttribute("list_id", listId);
			String schema_nm = listWidgetService.selectTableInfo(paramMap);
			if (!"".equals(submit_type) && submit_type != null) { 
				if (submit_type.equals("UPD")) {
					paramMap.addAttribute("submit_type", "UPD");
				}
			} else {
				if (!"".equals(schema_nm) && schema_nm != null) {
					paramMap.addAttribute("submit_type", "UPD");
				}
			}
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 
	 * Getting ListWidget List
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/searchSchemaList.do")
	public @ResponseBody ResultVO  searchSchemaList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		List resultList = null;
		
		try {
			GridVO gridVO = new GridVO();
			String search = (String) paramMap.get("search_param");
			
			if (!"".equals(search) && search != null) {
				String search_table_name = (String) paramMap.get("search_table_name");
				resultList = listWidgetService.searchResultSchemaList(search_table_name);
				gridVO.setRows(resultList);
			} else {
				String schema_nm = listWidgetService.selectTableInfo(paramMap);				
				if (!"".equals(schema_nm) && schema_nm != null) {
					String[] table_name = schema_nm.split(",");
					paramMap.addAttribute("table_name", table_name);
				}
				
				resultList = listWidgetService.searchSchemaList(paramMap);
				gridVO.setRows(resultList);
			}
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * Getting selected table's column info
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/searchSchemaColmnName.do")
	public @ResponseBody ResultVO searchSchemaColmnName(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			String list_id = (String)paramMap.get("list_id");
			String column_name = (String)paramMap.get("column_name");
			String type = (String)paramMap.get("type");
			//DB 사용자 계정을 "/egovframework/egovProps/application.properties" 가져온다
			String db_user = EgovProperties.getProperty("db.listwidget.user");
			paramMap.addAttribute("db_user", db_user);
			String[] column_list = null;
			
			paramMap.addAttribute("list_id", list_id);
			
			String schema_nm = listWidgetService.selectTableInfo(paramMap);				
			if (!"".equals(schema_nm) && schema_nm != null) {
				String[] table_name = schema_nm.split(",");
				paramMap.addAttribute("table_name", table_name);
			}
			
			if ("init".equals(type)) {
				List tempList = listWidgetService.selectColumnInfo(paramMap);
				
				if (column_name == null) {
					column_list = new String[tempList.size()];
					for(int i=0,ii=tempList.size();i<ii;i++){
						HashMap map = (HashMap)tempList.get(i);
						column_list[i] = ""+map.get("COLUMN_NM");
					}
				} else {
					if (!"".equals(column_name) && column_name != null) {
						column_list = column_name.split(",");
					}					
				}
				
				paramMap.addAttribute("column_list", column_list);
			}

			List resultList = listWidgetService.searchSchemaColmnName(paramMap);		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 
	 * Getting selected table's rows
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/selectSchemaRows.do")
	public @ResponseBody ResultVO selectSchemaRows(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			String list_id = (String)paramMap.get("list_id");
			
			if (!"".equals(list_id) && list_id != null) {
				paramMap.addAttribute("list_id", list_id);
				String schema_nm = listWidgetService.selectTableInfo(paramMap);
				String[] schema_arr = null;
				if(schema_nm != null){
					schema_arr = schema_nm.split(",");
					paramMap.addAttribute("table_name", schema_arr);
				}
				
				String relation_info = listWidgetService.selectRelationInfo(paramMap);
				String[] relation_arr = null;
				if(relation_info != null){
					relation_arr = relation_info.split(",");
					paramMap.addAttribute("relation_arr", relation_arr);
				}
			}
			
			List resultList = listWidgetService.selectSchemaRows(paramMap);		
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			if(e.getCause() instanceof SQLException){
				resultVO.setSuccess(false);
				resultVO.setResultMsg(messageSource.getMessage("msg.system.listWidget.00016"));
			}else{
				throw new NkiaException(e);
			}
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/listWidget/insertSchemaRealationConf.do")
	public @ResponseBody ResultVO insertSchemaRealationConf(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			listWidgetService.insertSchemaRealationConf(paramMap);
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/listWidget/updateSchemaRealationConf.do")
	public @ResponseBody ResultVO updateSchemaRealationConf(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			
			listWidgetService.updateSchemaRealationConf(paramMap);
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * List Widget Column Configuration Page Call
	 * 
	 * @param paramMap
	 * @return string
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/listWidgetColumnConf.do")
	public String listWidgetColumnConf(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/listWidget/listWidgetColumnConf.nvf";
		
		List manuArr = new ArrayList();
		
		try{
			//KEY			
			String listId = request.getParameter("listId");
			String submit_type = request.getParameter("submit_type");
			
			paramMap.addAttribute("list_id", listId);
			List columnInfo = listWidgetService.selectGridPartArr(paramMap);
			
			if (!"".equals(submit_type) && submit_type != null) { 
				if (submit_type.equals("UPD")) {
					paramMap.addAttribute("submit_type", "UPD");
				}
			} else {
				if (columnInfo.size() > 0) {
					paramMap.addAttribute("submit_type", "UPD");
				}
			}
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	@RequestMapping(value="/itg/system/listWidget/insertColumnConf.do")
	public @ResponseBody ResultVO insertColumnConf(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {

			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if (isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    		paramMap.put("updId", userVO.getUser_id());
	    	}
	    	
	    	JSONArray columnInfo = JSONArray.fromObject(JSONSerializer.toJSON(paramMap.get("insertParam")));
			for (int i=0,ii=columnInfo.size(); i<ii; i++) {
				listWidgetService.insertColumnConf(columnInfo.getJSONObject(i));
			}
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/listWidget/updateColumnConf.do")
	public @ResponseBody ResultVO upadteColumnConf(HttpServletRequest request, @RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			String list_id = request.getParameter("list_id");
			JSONArray columnInfo = JSONArray.fromObject(JSONSerializer.toJSON(paramMap.get("insertParam")));
			
			listWidgetService.updateColumnConf(list_id, columnInfo);
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * List Widget Search-Part Configuration Page Call
	 * 
	 * @param paramMap
	 * @return string
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/listWidgetSearchConf.do")
	public String listWidgetSearchConf(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/listWidget/listWidgetSearchConf.nvf";
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		try {
			//KEY			
			String listId = request.getParameter("listId");
			String submit_type = request.getParameter("submit_type");
			
			paramMap.addAttribute("list_id", listId);
			List searchInfo = listWidgetService.selectSearchInfo(paramMap);
			if (searchInfo.size() > 0) {
				String fix_where = listWidgetService.selectFixWhere(listId);
				paramMap.addAttribute("fix_where", fix_where);
			}
			
			if (!"".equals(submit_type) && submit_type != null) { 
				if (submit_type.equals("UPD")) {
					paramMap.addAttribute("submit_type", "UPD");
				}
			} else {
				if(searchInfo.size() > 0){
					paramMap.addAttribute("submit_type", "UPD");
				}
			}
			
			//로그인 id 조회
			String loginUserId = userVO.getUser_id();
			
			//로그인 사용자의 부서 정보를 조건으로 부서검색권한 조회
			String search_cust_auth = deptService.selectSearchCustAuth(loginUserId);
			paramMap.addAttribute("search_cust_auth", search_cust_auth);
			//파라미터화 하고싶은 파라미터는 여기에 추가 20191028
			paramMap.addAttribute("user_id", userVO.getUser_id());
			
		} catch(NkiaException e) {
			new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * List Widget Final Configuration Save Page Call
	 * 
	 * @param paramMap
	 * @return string
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/listWidgetSaveConf.do")
	public String listWidgetSaveConf(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/listWidget/listWidgetSaveConf.nvf";
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		try {
			List searchPartArr = new ArrayList();
			List gridPartArr = new ArrayList();
			List menuArr = new ArrayList();
			
			//KEY
			String listId = request.getParameter("listId");
			String submit_type = request.getParameter("submit_type");
			
			paramMap.addAttribute("list_id", listId);
			
			if (!"".equals(submit_type) && submit_type != null) { 
				if (submit_type.equals("UPD")) {
					paramMap.addAttribute("submit_type", "UPD");
				}
			}
			
			searchPartArr = listWidgetService.selectSearchPartArr(paramMap);
			JSONArray searchPartJson = JSONArray.fromObject(searchPartArr);
			
			gridPartArr = listWidgetService.selectGridPartArr(paramMap);
			JSONArray gridPartJson = JSONArray.fromObject(gridPartArr);
			
			menuArr = listWidgetService.selectMenuInfo(paramMap);
			JSONArray menuInfoJson = JSONArray.fromObject(menuArr);
			
			//로그인 id 조회
			String loginUserId = userVO.getUser_id();
			
			//로그인 사용자의 부서 정보를 조건으로 부서검색권한 조회
			String search_cust_auth = deptService.selectSearchCustAuth(loginUserId);
			paramMap.addAttribute("search_cust_auth", search_cust_auth);
			
			paramMap.addAttribute("searchPartJson", searchPartJson);
			paramMap.addAttribute("gridPartJson", gridPartJson);
			paramMap.addAttribute("menuInfoJson", menuInfoJson);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return forwarPage;
	}
	
	/**
	 * 
	 * Getting Selected Column List For Search-Part
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/listWidget/selectColumnInfo.do")
	public @ResponseBody ResultVO selectColumnInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		
		ResultVO resultVO = new ResultVO();

		try {
			GridVO gridVO = new GridVO();
			
			List resultList = new ArrayList();
			
			String list_id = (String)paramMap.get("list_id");
			String column_name = (String)paramMap.get("column_name");
			String type = (String)paramMap.get("type");
			String[] search_list = null;
			
			if ("init".equals(type)) {
				List temp_list = listWidgetService.selectSearchInfo(paramMap);
				int size = temp_list.size();
				if (size > 0) {
					search_list = new String[size];
					for (int i=0,ii=size; i < ii; i++) {
						HashMap map = (HashMap)temp_list.get(i);
						search_list[i] = ""+map.get("COLUMN_NM");
					}
				}
			} else {
				if(!"".equals(column_name) && column_name != null){
					search_list = column_name.split(",");
				}
			}
			
			paramMap.addAttribute("search_list", search_list);
			
			resultList = listWidgetService.selectColumnInfo(paramMap);		
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/listWidget/insertSearchConf.do")
	public @ResponseBody ResultVO insertSearchConf(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			listWidgetService.insertSearchConf(paramMap);
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/listWidget/updateSearchConf.do")
	public @ResponseBody ResultVO updateSearchConf(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			listWidgetService.updateSearchConf(paramMap);
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/selectTableInfo.do")
	public @ResponseBody ResultVO selectTableInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		
		ResultVO resultVO = new ResultVO(); 
		
		try{
			JSONArray list = new JSONArray();
			GridVO gridVO = new GridVO();
			
			String schema_nm = listWidgetService.selectTableInfo(paramMap);				
			String[] temp = null;
			if(schema_nm != null){
				temp = schema_nm.split(",");
				for(int i=0,ii=temp.length;i<ii;i++){
					JSONObject map = new JSONObject();
					map.put("TABLE_NAME" , temp[i]);
					list.add(map);
				}
			}
			
			gridVO.setRows(list);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/selectRelationInfo.do")
	public @ResponseBody ResultVO selectRelationInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		
		ResultVO resultVO = new ResultVO(); 
		
		try {
			JSONArray list = new JSONArray();
			GridVO gridVO = new GridVO();
			
			String relation_info = listWidgetService.selectRelationInfo(paramMap);				
			String[] temp = null;
			String[] key_temp = null;
			if(relation_info != null){
				temp = relation_info.split(",");
				for(int i=0,ii=temp.length;i<ii;i++){
					key_temp = temp[i].split("=");
					JSONObject map = new JSONObject();
					map.put("LEFT_KEY" , key_temp[0]);
					map.put("RIGHT_KEY" , key_temp[1]);
					list.add(map);
				}
			}
			
			gridVO.setRows(list);
			resultVO.setGridVO(gridVO);

		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 
	 * Getting Selected Search Object For Search-Part
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/selectSearchInfo.do")
	public @ResponseBody ResultVO  selectSearchInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		List resultList = new ArrayList();
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			resultList = listWidgetService.selectSearchInfo(paramMap);	
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * @param paramMap
	 * @return
	 * @
	 */	
	
	@RequestMapping(value="/itg/system/listWidget/updateSaveConf.do")
	public @ResponseBody ResultVO updateSaveConf(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			JSONArray columnInfo = JSONArray.fromObject(JSONSerializer.toJSON(paramMap.get("insertParam")));
			for (int i=0,ii=columnInfo.size(); i<ii; i++) {
				listWidgetService.updateSaveConf(columnInfo.getJSONObject(i));
			}
			resultVO.setSuccess(true);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 *  현황마법사 조회 삭제 팝업을 띄우기 위함  
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value = "/itg/system/listWidget/searchListWidgetPopupList.do")
	public @ResponseBody ResultVO searchListWidgetPopupList(@RequestBody ModelMap paramMap) throws NkiaException{
		ResultVO resultVO = new ResultVO();
		GridVO gridVO =  new GridVO(); 
		List resultList = null;
		
		try {
			//resultList = listWidgetService.selectColumnInfo(paramMap);
			resultList = listWidgetService.searchListWidgetPopupList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		

		return resultVO;
	}
	
	/**
	 * 현황 마법사 조회 삭제 
	 */
	@RequestMapping(value = "/itg/system/listWidget/deleteListWidgetList.do")
	public @ResponseBody ResultVO deleteListWidgetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			listWidgetService.deleteListWidgetList(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * Getting ListWidget ExcelDown
	 * 
	 * @param paramMap
	 * @return
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws WriteException 
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/searchExcelList.do")
	public @ResponseBody ResultVO searchExcelList(@RequestBody ModelMap paramMap) throws NkiaException, SQLException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		List resultList = new ArrayList();
		ModelMap excelMap = new ModelMap();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			Map param = (Map) paramMap.get("param");
			
			String listId = (String)param.get("listId");
			param.put("list_id", listId);
			
			List columnList = listWidgetService.selectColumnInfo(param);
			String column_str = "";
			String orderby_str = "";
			HashMap hashMap = null;
			
			for (int i=0,ii=columnList.size(); i<ii; i++) {
				hashMap = (HashMap)columnList.get(i);
				Iterator iterator = hashMap.entrySet().iterator();
				String column_nm = "";
				String table_nm = "";
				String view_type = "";
				String rows_orderby = "";
				
				while (iterator.hasNext()) {
					Entry entry = (Entry)iterator.next();
					if (entry.getValue() != null) {
						if ("COLUMN_NM".equals(entry.getKey())) {
							column_nm = ""+entry.getValue();
						} else if ("TABLE_NM".equals(entry.getKey())) {
							table_nm = ""+entry.getValue();
						} else if ("VIEW_TYPE".equals(entry.getKey())) {
							view_type = ""+entry.getValue();
						} else if ("ROWS_ORDERBY".equals(entry.getKey())) {
							rows_orderby = ""+entry.getValue();
						}
					}
				}
				
				if ("".equals(view_type)) {
//					column_str += "NVL(" + table_nm + "." + column_nm + " ,'-') AS " + column_nm + ",";
//					column_str += table_nm + "." + column_nm + " AS " + column_nm + ",";
					column_str += table_nm + "." + column_nm + ",";
				} else {
//					column_str += "NVL(" + view_type + " ,'-') AS " + column_nm + ",";
//					column_str += view_type + " AS " + column_nm + ",";
					if ("CONF_NM".equals(column_nm)) {
//						column_str += table_nm + "." + column_nm + ",";
						column_str += " ( "
							+ "	SELECT "
							+ "		LISTAGG(Z.CONF_NM,',') WITHIN GROUP(ORDER BY Z.SR_ID) "
							+ "	FROM ("
							+ "			SELECT AI.CONF_NM, PROC_MASTER.SR_ID AS SR_ID "
							+ "			FROM AM_INFRA AI "
							+ "			WHERE AI.CONF_ID IN ( SELECT C1.ITEM_ID "
							+ "						FROM PROC_REL_CONF C1 "	
							+ "						WHERE C1.SR_ID = PROC_MASTER.SR_ID "
							+ "							AND C1.SR_ID = PROC_SERVICE.SR_ID "
							+ "					) "
							+ "		UNION ALL "
							+ "			SELECT AW.CONF_NM, PROC_MASTER.SR_ID AS SR_ID "
							+ "			FROM AM_SW AW "
							+ "			WHERE AW.CONF_ID IN ( SELECT C2.ITEM_ID "								
							+ "						FROM PROC_REL_CONF C2 "								
							+ "						WHERE C2.SR_ID = PROC_MASTER.SR_ID "									
							+ "						AND C2.SR_ID = PROC_SERVICE.SR_ID "	
							+ "					)"
							+ "	) Z "
							+ ") AS " + column_nm + ",";
					} else {
						column_str += view_type + " AS " + column_nm + ",";
					}
				}
				
				if (!"".equals(rows_orderby)) {
					orderby_str += column_nm + " " + rows_orderby + ",";
				}
			}
			
			String schema_nm = listWidgetService.selectTableInfo(param);			
			List temp_list = listWidgetService.selectSearchInfo(param);
			String fix_where = listWidgetService.selectFixWhere(listId);
			
			String where_str = "WHERE 1 = 1 ";
			if (!"".equals(fix_where) && fix_where != null) {
				where_str = listWidgetService.selectFixWhere(listId);
				where_str = where_str.replace("#USER_ID#", userVO.getUser_id());
				//WHERE조건 파라미터화
			}
			
			String where_value = "";
			int where_size = temp_list.size();
			
			//부서정보 조회가 검색조건에 있는지 확인
			//false : 없음, true : 있음
			Boolean searchCustAuth = false;
			//부서검색권한 정보
			String cust_auth = (String)param.get("custAuth");
			HashMap cust_map = new HashMap();
			
			//커스터마이징 2018.05.12 molee
			//처리부서를 조건으로 추가하면서 추가
			//1.SYS_CUST, SYS_CUST_SEARCH_AUTH 테이블에서 로그인한 사용자의 검색가능한 부서를 구해
			//2.그 사용자가 PROC_OPER에 있으면 그 SR_ID를 구하여 결과를 구함
			//3.따라서 SR_ID 필드정보를 알아야 해서 선언(테이블정보+컬럼정보/PROC_MASTER.SR_ID형식이므로)
			if (where_size > 0) {
				for (int i=0,ii=where_size; i<ii; i++) {
					HashMap map = (HashMap)temp_list.get(i);
					String field_type = "" + map.get("FIELD_TYPE");
					boolean WHB = false;
					//@@ 고한조 20131209 수정 - 현황에서 Excel Export 할시 TableName이 Null인 경우의 처리가 안되어 있었음 Start
					String table_nm = "";
					
					if (map.get("TABLE_NM")!= null && !"".equals(map.get("TABLE_NM"))) {
						table_nm = map.get("TABLE_NM") + ".";
					}
					
					if ("TEXTFIELD_S".equals(field_type)) {
						where_value = (String)param.get("T_"+map.get("COLUMN_NM"));
						if(!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					} else if ("DATE".equals(field_type)) {
						String sDate = (String)param.get(map.get("COLUMN_NM") + "_startDate");
						String eDate = (String)param.get(map.get("COLUMN_NM") + "_endDate");
						
						if ((!"".equals(sDate) && sDate != null) && (!"".equals(eDate) && eDate != null)) {
							WHB = true;
						}
					} else if ("MULTI_DATE".equals(field_type)) {
						String sDate = (String)param.get(map.get("COLUMN_NM")+"_startDate");
						String eDate = (String)param.get(map.get("COLUMN_NM")+"_endDate");
						
						if ((!"".equals(sDate) && sDate != null) || (!"".equals(eDate) && eDate != null)) {
							WHB = true;
						}
					} else if ("MULTI_DATE_S".equals(field_type)) {
						String sDate = (String)param.get(map.get("COLUMN_NM")+"_startDate");
						String eDate = (String)param.get(map.get("COLUMN_NM")+"_endDate");
						
						if ((!"".equals(sDate) && sDate != null) || (!"".equals(eDate) && eDate != null)) {
							WHB = true;
						}
					} else if ("SELECTBOX_M".equals(field_type)) {
						where_value = (String)param.get("CC_"+map.get("COLUMN_NM"));
						if (!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					} else if ("SELECTBOX_CUST".equals(field_type)) {
						where_value = (String)param.get("S_" + map.get("COLUMN_NM"));
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}
					} else if ("TREE_CUST".equals(field_type)) {
						searchCustAuth = true;
						cust_map.put("COLUMN_NM", map.get("COLUMN_NM"));
						where_value = (String)param.get(map.get("COLUMN_NM") + "_field");
						if (!"".equals(where_value) && where_value != null) {
							//부서검색조건  값O
							WHB = true;
						}
					} else if ("TREE_REQ".equals(field_type)) {
						where_value = (String)param.get(map.get("COLUMN_NM") + "_field");
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}	
					} else if ("TREE_PROBLEM".equals(field_type)) {
						where_value = (String)param.get(map.get("COLUMN_NM") + "_field");
						if (!"".equals(where_value) && where_value != null) {
							WHB = true;
						}	
	
					} else if ("CI_CLASS_ID".equals(field_type)) {
						where_value = (String)param.get("S_CLASS_ID");
						if(!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					} else if ("SERVICE_ID".equals(field_type)) {
						where_value = (String)param.get("S_SERVICE_ID");
						if(!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					} else if ("multiCodePop".equals(field_type)) {
						where_value = (String)param.get("S_"+map.get("COLUMN_NM"));
						if(!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					} else if("REQ_ZONE_ID".equals(field_type)) {
						where_value = (String)param.get("REQ_ZONE");
						if(!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					} else {
						where_value = (String)param.get("S_"+map.get("COLUMN_NM"));
						if(!"".equals(where_value) && where_value != null){
							WHB = true;
						}
					}
					
					if (WHB) {
						//2018.01.25 molee searchWidgetList와 동일하게 수정함
						// 검색조건에 UPPER 추가 20140806 정영태
						if ("TEXTFIELD".equals(field_type)) {
							//textfield(ok)
							if (map.get("COLUMN_NM").equals("WORK_USER_NM")) {
								//작업자 조건
								where_str += " AND 0 < FC_GET_OPER_USER_CNT(SR_ID, '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "')";
							} else { 
								where_str += " AND UPPER(" + map.get("COLUMN_NM") + ") like '%'||UPPER('" + (String)param.get("S_" + map.get("COLUMN_NM")) + "')||'%'";
							}
						} else if("TEXTFIELD_P".equals(field_type)) {
							//textfield+button
							
						} else if("TEXTFIELD_S".equals(field_type)) {
							//selectbos+textfield(ok)
//							where_str += " AND UPPER(" + (String)param.get("C_" + map.get("COLUMN_NM")) + ") like UPPER('%" + (String)param.get("T_" + map.get("COLUMN_NM")) + "%')";
							//selectbos+textfield(ok)
							boolean user_flag = false;
							if (param.get("condition_user_flag") != null) {
								user_flag = (boolean) param.get("condition_user_flag");
							}
							boolean group_flag = false;
							if (param.get("condition_group_flag") != null) {
								group_flag = (boolean) param.get("condition_group_flag");
							}
							 
							if (user_flag) {
//								where_str += " AND UPPER(" + (String)paramMap.get("C_" + map.get("COLUMN_NM")) + ") like UPPER('%" + (String)paramMap.get("T_" + map.get("COLUMN_NM")) + "%')";
								where_str += " AND 1 < FN_GET_LISTWIDGET_USER_CNT(SR_ID, " + cust_auth + ", " + (String)param.get("C_" + map.get("COLUMN_NM")) + ", '" + (String)param.get("T_" + map.get("COLUMN_NM")) + "') "; 
							} else if (group_flag) {
								where_str += " AND 1 < FN_GET_LISTWIDGET_GROUP_CNT(SR_ID, " + cust_auth + ", " + (String)param.get("C_" + map.get("COLUMN_NM")) + ", '" + (String)param.get("T_" + map.get("COLUMN_NM")) + "') "; 
							} else {
								
								where_str += " AND UPPER(" + (String)param.get("C_" + map.get("COLUMN_NM")) + ") like UPPER('%" + (String)param.get("T_" + map.get("COLUMN_NM")) + "%')";
							}
						} else if("SELECTBOX".equals(field_type)) {
							//selectbox(ok)
							// 2014.8.24 FREECHANG  수정 진행 상태
							if ("FC_GET_WORK_STATE_SEARCH_ID(WORK_STATE)".equals(map.get("COLUMN_NM"))) {
								
								
								if ("COMP".equals(param.get("S_" + map.get("COLUMN_NM")))) {
									where_str += " AND FC_GET_WORK_STATE_SEARCH_ID(WORK_STATE) = 'END' "
											  +  " AND (END_TYPE IS NULL OR END_TYPE = 'COMP')";
									
								} else if ("REJECT".equals(param.get("S_" + map.get("COLUMN_NM")))) {
									where_str += " AND FC_GET_WORK_STATE_SEARCH_ID(WORK_STATE) = 'END' "
											  +  " AND END_TYPE = 'REJECT' ";
								} else {
									// 종료유형 검색조건이 제대로 먹지 않아서 추가하였음 : 20140806 정영태
									where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "'";
								}
							} else {
								where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "'";
							}
							
						} else if ("SELECTBOX_M".equals(field_type)) {
							//mutil selectbox(ok)
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)param.get("CC_" + map.get("COLUMN_NM")) + "'";
						} else if ("SELECTBOX_CUST".equals(field_type)) {
							//2018.05.12 molee
							where_str += " AND SR_ID IN ( "
									+ "SELECT "
									+ "		DISTINCT Z.SR_ID "
									+ "FROM ( "
									+ "			SELECT 'USER' AS GUBUN, A.SR_ID, B.OPER_TYPE, B.OPER_USER_ID, "
									+ "				C.CUST_ID AS OPER_CUST_ID "
									+ "			FROM PROC_MASTER A, PROC_OPER B, SYS_USER C "
									+ "			WHERE A.SR_ID = B.SR_ID "
									+ "				AND B.OPER_USER_ID = C.USER_ID "
									+ "				AND B.OPER_USER_ID != 'SYSTEM' "
									+ "				AND C.CUST_ID = '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "'"
									+ "		UNION ALL "
									+ "			SELECT 'GROUP' AS GUBUN, A.SR_ID, A.OPER_TYPE, B.CHARGER_ID AS OPER_USER_ID, "
									+ "				C.CUST_ID AS OPER_CUST_ID "
									+ "			FROM PROC_OPER A, WORK_CHARGER_MAPNG B, SYS_USER C "
									+ "			WHERE A.OPER_USER_ID = B.GRP_ID AND B.CHARGER_ID = C.USER_ID "
									+ "				AND B.CHARGER_ID != 'SYSTEM' "
									+ "				AND C.CUST_ID = '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "'"
									+ "	) Z "
									+ ")";
						} else if ("CHECKBOX".equals(field_type)) {
							//checkbox(ok)
							String[] checkValue = ((String)param.get("S_" + map.get("COLUMN_NM"))).split(",");
							String in_str = "";
							for (int l=0,ll=checkValue.length; l<ll; l++) {
								in_str += "'" + checkValue[l] + "',";
							}
							in_str = in_str.substring(0, in_str.length()-1);
							
							where_str += " AND " + map.get("COLUMN_NM") + " IN (" + in_str + ")";
						} else if ("RADIO".equals(field_type)) {
							//radio(ok)
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "'";
						} else if ("DATE".equals(field_type)) {
							//date(ok)
							//where_str += " AND "+table_nm+map.get("COLUMN_NM")+" = '"+(String)paramMap.get("S_"+map.get("COLUMN_NM"))+"'";
							where_str += " AND " + map.get("COLUMN_NM")
//									+" BETWEEN TO_DATE('" + (String)param.get(map.get("COLUMN_NM") + "_startDate") + "' || '000000', ('YYYY-MM-DD HH24MISS'))"
//									+" AND TO_DATE('" + (String)param.get(map.get("COLUMN_NM") + "_endDate") + "' || '235959', ('YYYY-MM-DD HH24MISS'))";
									+ " BETWEEN '" + (String)param.get(map.get("COLUMN_NM") + "_startDate") + " 00:00'"
									+ " AND '" + (String)param.get(map.get("COLUMN_NM") + "_endDate") + " 23:59'";
						} else if ("MULTI_DATE".equals(field_type)) {
							String columnNm = (String) map.get("COLUMN_NM");
							String sDate = (String) paramMap.get(map.get("COLUMN_NM") + "_startDate");
							String eDate = (String) paramMap.get(map.get("COLUMN_NM") + "_endDate");

							if (!"".equals(sDate) && !"".equals(eDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD') AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, sDate, columnNm, eDate);
							} else if (!"".equals(sDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD')", columnNm, sDate);
							} else if (!"".equals(eDate)) {
								where_str += String.format(" AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, eDate);
							}
						} else if ("MULTI_DATE_S".equals(field_type)) {
							String columnNm = (String)param.get("C_" + map.get("COLUMN_NM"));
							String sDate = (String) paramMap.get(map.get("COLUMN_NM") + "_startDate");
							String eDate = (String) paramMap.get(map.get("COLUMN_NM") + "_endDate");

							if (!"".equals(sDate) && !"".equals(eDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD') AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, sDate, columnNm, eDate);
							} else if (!"".equals(sDate)) {
								where_str += String.format(" AND %s >= TO_DATE('%s', 'YYYY-MM-DD')", columnNm, sDate);
							} else if (!"".equals(eDate)) {
								where_str += String.format(" AND %s < TO_DATE('%s', 'YYYY-MM-DD') + 1", columnNm, eDate);
							}
						} else if ("TREE_CUST".equals(field_type)) {
							String fieldVal = (String)param.get(map.get("COLUMN_NM") + "_field");
							if (fieldVal != null && !fieldVal.equals("")) {
								where_str += " AND " + map.get("COLUMN_NM") + " = '"
										+ (String)param.get(map.get("COLUMN_NM") + "_field") + "'";
							} else {
								if (!cust_auth.equals("ALL")) {
									String user_cust_id = userVO.getCust_id();
									where_str += " AND " + map.get("COLUMN_NM") + " IN ( "
											+ "SELECT "
											+ "		A.AUTH_CUST_ID "
											+ "FROM SYS_CUST_SEARCH_AUTH A, SYS_CUST B "
											+ "WHERE A.AUTH_CUST_ID = B.CUST_ID "
											+ "AND A.CUST_ID = '"
											+ user_cust_id + "' )";
								}
							}
						} else if ("TREE_REQ".equals(field_type)) {
							String reqId = (String)param.get(map.get("COLUMN_NM") + "_field");
							if (reqId!= null && !reqId.equals("")) {
								where_str += (
										"AND REQ_DOC_ID IN (SELECT REQ_DOC_ID"
										+"	 FROM WORK_REQ_DOC WHERE USE_YN ='Y'"
										+"	 START WITH REQ_DOC_ID ='"+reqId +"'" 
										+"	 CONNECT BY PRIOR REQ_DOC_ID = UP_REQ_DOC_ID)"
										);
							}
						} else if ("TREE_PROBLEM".equals(field_type)) {
							String probId = (String)param.get(map.get("COLUMN_NM") + "_field");
							if (probId!= null && !probId.equals("")) {
								where_str += (
										"AND INC_TYPE_CD IN (SELECT CODE_ID"
										+"	 FROM SYS_CODE WHERE USE_YN ='Y' AND CODE_GRP_ID ='INC_TYPE_CD'"
										+"	 START WITH CODE_ID ='"+probId +"'" 
										+"	 CONNECT BY PRIOR CODE_ID = UP_CODE_ID)"
										);
							}		
						} else if ("CI_CLASS_ID".equals(field_type)) {
							//요청현황 조회 전용(하드코딩/기타 다른 현황 제작시 별도 구현 필요)
							where_str += " AND PROC_MASTER.SR_ID IN(" +
									"SELECT" +
									"     SR_ID " +
									"FROM (" +
									"    SELECT" +
									"         A.SR_ID " +
									"     FROM" +
									"         PROC_REL_CONF A, AM_ASSET B,AM_INFRA C  " +
									"    WHERE 1=1 " +
									"    AND A.ITEM_ID = C.CONF_ID " +
									"    AND B.ASSET_ID = C.ASSET_ID " +
									"    AND C.CLASS_ID IN ( " +
									"        SELECT " +
									"             CLASS_ID " +
									"        FROM " +
									"             AM_CLASS " +
									"             START WITH CLASS_ID ='" + (String)param.get("S_CLASS_ID") + "' CONNECT BY PRIOR CLASS_ID = UP_CLASS_ID " +
									"        )" +
									"	 UNION ALL " +
									"    SELECT " +
									"         A.SR_ID " +
									"     FROM " +
									"         PROC_REL_CONF A, AM_ASSET B,AM_SW C " +
									"    WHERE 1=1 " +
									"    AND A.ITEM_ID = C.CONF_ID " +
									"    AND B.ASSET_ID = C.ASSET_ID " +
									"    AND C.CLASS_ID IN ( " +
									"        SELECT " +
									"             CLASS_ID " +
									"        FROM " +
									"             AM_CLASS " +
									"             START WITH CLASS_ID ='" + (String)param.get("S_CLASS_ID") + "'  CONNECT BY PRIOR CLASS_ID = UP_CLASS_ID" +
									"        ) " +
									"))";
						} else if ("SERVICE_ID".equals(field_type)) {
							//요청현황 조회 전용(하드코딩/기타 다른 현황 제작시 별도 구현 필요)
							where_str += " AND PROC_MASTER.SR_ID IN(" +
									"SELECT " +
									"     SR_ID " +
									"FROM " +
									"     PROC_REL_CONF A, AM_SERVICE B " +
									"WHERE 1=1 " +
									"AND A.ITEM_ID = B.SERVICE_ID AND B.SERVICE_ID IN ( " +
									"    SELECT " +
									"         SERVICE_ID " +
									"     FROM " +
									"         AM_SERVICE " +
									"         START WITH SERVICE_ID = '" + (String)param.get("S_SERVICE_ID") + "' CONNECT BY PRIOR SERVICE_ID = UP_SERVICE_ID " +
									"    ) " +
									"GROUP BY SR_ID " +
									")";
						} else if ("multiCodePop".equals(field_type)) {
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "'";
						} else if ("REQ_ZONE_ID".equals(field_type)) {
							//where_str += " AND PROC_MASTER.REQ_ZONE = '" + (String)param.get("REQ_ZONE") + "'";
							String reqZoneId = (String)param.get("REQ_ZONE");
							if (reqZoneId!= null && !reqZoneId.equals("")) {
								where_str += (
										" AND PROC_MASTER.REQ_ZONE IN "
										+"("
										+ " SELECT CUST_ID FROM SYS_REQ_ZONE"
										+ " START WITH CUST_ID = '" + (String)param.get("REQ_ZONE") + "'"
										+ " CONNECT BY PRIOR CUST_ID = UP_CUST_ID "
										+ ")"
										);
							}
						} else {
							//default (textfield)(ok)
							where_str += " AND " + map.get("COLUMN_NM") + " = '" + (String)param.get("S_" + map.get("COLUMN_NM")) + "'";
						}
					}
				}
				
				if (searchCustAuth) {
					if (cust_auth != null && !cust_auth.equals("ALL")) {
						String user_cust_id = userVO.getCust_id();
						if (cust_map.size() > 0) {
							for (int ii=0; ii<cust_map.size(); ii++) {
								where_str += " AND " + cust_map.get("COLUMN_NM") + " IN ( "
										+ "SELECT "
										+ "		A.AUTH_CUST_ID "
										+ "FROM SYS_CUST_SEARCH_AUTH A, SYS_CUST B "
										+ "WHERE A.AUTH_CUST_ID = B.CUST_ID "
										+ "AND A.CUST_ID = '"
										+ user_cust_id + "' )";
							}
						}
					}
				}
				
			}
			
			String join_key = listWidgetService.selectRelationInfo(param);
			String join_str = "";
			
			//2018.01.25 molee 수정
			if (!"".equals(join_key) && join_key != null) {
				String[] join_arr = join_key.split(",");
				for (int i=0,ii=join_arr.length; i<ii; i++) {
					if (i == 0) {
						join_str += join_arr[i];
					} else {
						join_str += " and " + join_arr[i];
					}
				}
			}
			
			if (column_str.length() > 0) {
				column_str = column_str.substring(0, column_str.length()-1);
			}
			
			/*
			 * 2018.01.24 molee
			 * Rownum의 orderby로 주기 위해서 수정
			 * 
			 * 
			 * 
			if (orderby_str.length() > 0) {
				if (sort_column == null || "".equals(sort_column)) {
					orderby_str = " order by " + orderby_str.substring(0, orderby_str.length()-1);
				} else {
					orderby_str = "";
				}
			}*/
			
			if (orderby_str.length() > 0) {
				orderby_str = orderby_str.substring(0, orderby_str.length()-1);
			}
			
			//2018.01.25 molee 수정 -- start
//			String sql_text = "select "+column_str+" from "+schema_nm+" "+where_str+join_str+orderby_str;
			String sql_select_text = "";
			if (join_str != null && !join_str.equals("")) {
				sql_select_text = "select " + column_str
						+ " from " + schema_nm + " where "
						+ join_str;
			} else {
				sql_select_text = "select " + column_str
						+ " from " + schema_nm + " ";
			}
			
			String sql_where_text = where_str;
			String sql_rnum_orderby_text = orderby_str;
			
			param.put("sql_select_text", sql_select_text);
			param.put("sql_where_text", sql_where_text);
			param.put("sql_rnum_orderby_text", sql_rnum_orderby_text);
			
			//2018.01.25 molee 수정 -- end
			
			resultList = listWidgetService.searchExcelList(param);
			
			Map excelParam = (Map) paramMap.get("excelParam");
			
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);
			
			resultVO.setResultMap(excelMap);
			
			resultVO.setResultMap(excelMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 구성분류체계 조회 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/searchCiClassTree.do")
	public @ResponseBody ResultVO searchCiClassTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = listWidgetService.searchCiClassTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 서비스 조회 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/listWidget/searchServiceTree.do")
	public @ResponseBody ResultVO searchServiceTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = listWidgetService.searchServiceTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 통계페이지에서 현황 조회시 검색영역에 입력값 저장을 위한 맵을 만드는 부분 
	 * @param request
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void createRequestMap(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		HashMap searchValueMap = new HashMap();
		Map tempMap = request.getParameterMap();
		if(tempMap != null) {
			Iterator keySet = tempMap.keySet().iterator();
			while(keySet.hasNext()) {
				String paramName = (String)keySet.next();
				String paramValue = request.getParameter(paramName);
				//String paramValue = new String(request.getParameter(paramName).getBytes("iso-8859-1"), "UTF-8");
				
				searchValueMap.put(paramName, paramValue);
			}
			paramMap.put("searchValueMap", searchValueMap);
		}
		JSONObject obj =  JSONObject.fromObject(searchValueMap);
		paramMap.put("searchValueMap", obj);
	}
	
	/**
	 * DB함수  등록 팝업
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/system/listWidget/goViewFunctionPop.do")
	public String goViewFunctionPop(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/listWidget/popup/listWidgetTextEditPop.nvf";
		
		String listId = request.getParameter("listId");
		paramMap.addAttribute("list_id", listId);
		
		Map gridData = (Map) paramMap.get("gridData");
		paramMap.addAttribute("gridData", gridData);
		
		return forwarPage;
	}
	
	/**
	 * 통계페이지에서 현황 조회시 검색영역에 입력값 저장을 위한 맵을 만드는 부분 
	 * @param request
	 * @param paramMap
	 * @throws NkiaException
	 */
	public String createConvertString(String param) throws NkiaException {
		if(param != null) {
			String result = param;
			result = result.replaceAll("\\{", "\\{'");
			result = result.replaceAll("=", "':'");
			result = result.replaceAll(", ", "','");
			result = result.replaceAll("}", "'}");
			return result;
		} else {
			return "";
		}
	}
}
