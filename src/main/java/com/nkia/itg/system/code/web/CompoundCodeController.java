package com.nkia.itg.system.code.web;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.code.service.CompoundCodeService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class CompoundCodeController {

	@Resource(name = "compoundCodeService")
	private CompoundCodeService compoundCodeService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 복합코드 관리 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/code/goCompoundCodeManager.do")
	public String userManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/code/compoundCodeManager.nvf";
		return forwarPage;
	}
		
	/**
	 * 복합코드 Value 조회
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/selectCompoundCodeValue.do")
	public @ResponseBody ResultVO selectCompoundCodeValue(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = compoundCodeService.selectCompoundCodeValue(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
