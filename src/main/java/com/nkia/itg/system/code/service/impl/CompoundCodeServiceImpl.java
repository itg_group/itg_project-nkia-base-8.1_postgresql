package com.nkia.itg.system.code.service.impl;

import java.util.HashMap;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.excel.ExcelReader;
import com.nkia.itg.base.dao.ExcelFileDAO;
import com.nkia.itg.system.code.dao.CompoundCodeDAO;
import com.nkia.itg.system.code.service.CompoundCodeService;

@Service("compoundCodeService")
public class CompoundCodeServiceImpl implements CompoundCodeService {

	private static final Logger logger = LoggerFactory.getLogger(CompoundCodeServiceImpl.class);
	
	@Resource(name = "compoundCodeDAO")
	public CompoundCodeDAO compoundCodeDAO;
	
	@Resource(name = "excelFileDAO")
	public ExcelFileDAO excelFileDAO;
	
	@Resource(name = "excelReader")
	private ExcelReader excelReader;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	@Override
	public HashMap selectCompoundCodeValue(ModelMap paramMap) throws NkiaException {
		return compoundCodeDAO.selectCompoundCodeValue(paramMap);
	}


	


}
