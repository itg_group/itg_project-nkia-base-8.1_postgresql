package com.nkia.itg.system.code.service;

import java.util.HashMap;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;


public interface CompoundCodeService {

	HashMap selectCompoundCodeValue(ModelMap paramMap) throws NkiaException;
	
	
}
