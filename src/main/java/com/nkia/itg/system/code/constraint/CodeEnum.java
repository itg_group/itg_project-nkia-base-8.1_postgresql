package com.nkia.itg.system.code.constraint;

public enum CodeEnum {

	SEARCH_TYPE_GROUP("GROUP"),
	SEARCH_TYPE_CODE("CODE");
		
	private String codeId;
	
	CodeEnum(String codeId){
		this.codeId = codeId;
    }
	
	public String getCodeId() {
		return this.codeId;
	}
	
}
