package com.nkia.itg.system.code.vo;

import java.io.Serializable;

public class CommonCodeVO implements Serializable {

	private static final long serialVersionUID = 7323639031585101035L;

	private String up_comm_code;
	
	private String grp_id;

	private String comm_id;
	
	private String comm_nm;
	
	private String comm_desc;
	
	private String rel_id;
	
	private String use_yn;
	
	private String sort;
	
	private String ins_user_id;
	
	private String ins_dt;
	
	private String upd_user_id;
	
	private String upd_dt;
	
	private String multi_id;
	
	private String table_id;
	
	private String modify_yn;

	public String getGrp_id() {
		return grp_id;
	}

	public void setGrp_id(String grp_id) {
		this.grp_id = grp_id;
	}

	public String getComm_id() {
		return comm_id;
	}

	public void setComm_id(String comm_id) {
		this.comm_id = comm_id;
	}

	public String getComm_nm() {
		return comm_nm;
	}

	public void setComm_nm(String comm_nm) {
		this.comm_nm = comm_nm;
	}

	public String getComm_desc() {
		return comm_desc;
	}

	public void setComm_desc(String comm_desc) {
		this.comm_desc = comm_desc;
	}

	public String getRel_id() {
		return rel_id;
	}

	public void setRel_id(String rel_id) {
		this.rel_id = rel_id;
	}

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getIns_user_id() {
		return ins_user_id;
	}

	public void setIns_user_id(String ins_user_id) {
		this.ins_user_id = ins_user_id;
	}

	public String getIns_dt() {
		return ins_dt;
	}

	public void setIns_dt(String ins_dt) {
		this.ins_dt = ins_dt;
	}

	public String getUpd_user_id() {
		return upd_user_id;
	}

	public void setUpd_user_id(String upd_user_id) {
		this.upd_user_id = upd_user_id;
	}

	public String getUpd_dt() {
		return upd_dt;
	}

	public void setUpd_dt(String upd_dt) {
		this.upd_dt = upd_dt;
	}

	public String getMulti_id() {
		return multi_id;
	}

	public void setMulti_id(String multi_id) {
		this.multi_id = multi_id;
	}

	public String getTable_id() {
		return table_id;
	}

	public void setTable_id(String table_id) {
		this.table_id = table_id;
	}

	public String getModify_yn() {
		return modify_yn;
	}

	public void setModify_yn(String modify_yn) {
		this.modify_yn = modify_yn;
	}

	public String getUp_comm_code() {
		return up_comm_code;
	}

	public void setUp_comm_code(String up_comm_code) {
		this.up_comm_code = up_comm_code;
	}
}
