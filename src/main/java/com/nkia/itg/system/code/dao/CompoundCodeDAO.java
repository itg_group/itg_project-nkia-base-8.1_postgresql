package com.nkia.itg.system.code.dao;

import java.util.HashMap;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("compoundCodeDAO")
public class CompoundCodeDAO extends EgovComAbstractDAO {

	public HashMap selectCompoundCodeValue(ModelMap paramMap) {
		return (HashMap)selectByPk("CompoundCodeDAO.selectCompoundCodeValue", paramMap);
	}
	
}
