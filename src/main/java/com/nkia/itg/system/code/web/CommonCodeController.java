package com.nkia.itg.system.code.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class CommonCodeController {

	@Resource(name = "commonCodeService")
	private CommonCodeService commonCodeService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 코드 관리 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/code/goCodeManager.do")
	public String userManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/code/codeManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 멀티코드 관리 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/code/goCodeBasicManager.do")
	public String codeBasicManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/code/codeBasicManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 엑셀데이터 변환
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @
	 **/
	@RequestMapping(value="/itg/system/code/parseSingleSheetExcelData.do")
	public @ResponseBody ResultVO parseSingleSheetExcelData(@RequestBody ModelMap paramMap) throws NkiaException, IOException {
		ResultVO resultVO = new ResultVO();
		try{
			HashMap resultMap = commonCodeService.parseSingleSheetExcelData(paramMap);
			
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 멀티 코드 트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/searchMultiTree.do")
	public @ResponseBody ResultVO searchMultiTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = commonCodeService.searchMultiCodeDataForTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			Integer expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 코드리스트 순서 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/updateCodeOrder.do")
	public @ResponseBody ResultVO updateCodeOrder(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			commonCodeService.updateCodeOrder(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 멀티코드 정보 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/selectMultiCodeDetailData.do")
	public @ResponseBody ResultVO selectMultiCodeDetailData(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = commonCodeService.selectMultiCodeDetailData(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 멀티 코드 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/insertMultiCodeInfo.do")
	public @ResponseBody ResultVO insertMultiCodeInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			commonCodeService.insertMultiCodeInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 멀티 코드 삭제 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/deleteMultiCodeInfo.do")
	public @ResponseBody ResultVO deleteMultiCodeInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			commonCodeService.deleteMultiCodeInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	/**
	 * 멀티 코드 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/updateMultiCodeInfo.do")
	public @ResponseBody ResultVO updateMultiCodeInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			commonCodeService.updateMultiCodeInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 코드 리스트 
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/code/searchCodeDataForChlid.do")
	public @ResponseBody ResultVO  searchCodeDataForChlid(@RequestBody ModelMap paramMap) throws NkiaException {
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			List list = new ArrayList();
			boolean initFlag 	= false;
			boolean paging		= false;
			int totCnt			= 0;
			
			paramMap = PagingUtil.getFristEndNum(paramMap);
			
			//공통 팝업에서 사용하기 위해 추가..
			if(!paramMap.containsKey("paging")) {
				paramMap.put("paging", paging);		
			}
			
			if(paramMap.containsKey("init")){
				initFlag = (Boolean)paramMap.get("init");		
			}
			
			if(paramMap.containsKey("searchKey")){
				String searchKey 	= (String)paramMap.get("searchKey");
				String searchValue 	= (String)paramMap.get("searchValue");
				
				if(searchKey.isEmpty() && !searchValue.isEmpty()){
					ModelMap codeMap = new ModelMap(); 
					codeMap.put("up_code_id", "CODE_POP_SEARCH_KEY");
					codeMap.put("use_yn"	, "Y");
					List codeList = commonCodeService.searchCodeDataForChild(codeMap);
					paramMap.put("searchKeyList", codeList);
				}
			}
			
			if(!initFlag) {
				totCnt 	= commonCodeService.searchCodeDataForChildCnt(paramMap);
				list 	= commonCodeService.searchCodeDataForChild(paramMap);		
			}
			
			gridVO.setTotalCount(totCnt);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(list);
			resultVO.setGridVO(gridVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
	
	/**
	 * 하위 코드 리스트 
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/code/searchCodePopup.do")
	public @ResponseBody ResultVO  searchCodePopup(@RequestBody ModelMap paramMap) throws NkiaException {
		GridVO gridVO = new GridVO();
		ResultVO resultVO = new ResultVO();
		try {
			paramMap = PagingUtil.getFristEndNum(paramMap);
			commonCodeService.getSearchList(paramMap,"searchKey","searchValue","CODE_POP_SEARCH_KEY");
			
			int totCnt 	= commonCodeService.searchCodePopupCnt(paramMap);
			List list 	= commonCodeService.searchCodePopupList(paramMap);	
			
			gridVO.setTotalCount(totCnt);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(list);
			resultVO.setGridVO(gridVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 그룹코드 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/searchGrpCodeList.do")
	public @ResponseBody ResultVO searchGrpCodeList(@RequestBody ModelMap paramMap)throws NkiaException   {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			paramMap = PagingUtil.getFristEndNum(paramMap);
			PagingUtil.setSortColumn(paramMap);
			
			int totCnt 	= commonCodeService.searchGrpCodeCnt(paramMap);
			List resultList = commonCodeService.searchGrpCodeList(paramMap);
			
			gridVO.setTotalCount(totCnt);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 코드 리스트 조회
	 * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/searchCodeList.do")
	public @ResponseBody ResultVO searchCodeList(@RequestBody ModelMap paramMap)throws NkiaException   {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			PagingUtil.setSortColumn(paramMap);
			List resultList = commonCodeService.searchCodeList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공통 코드 정보 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/searchCodeDetail.do")
	public @ResponseBody ResultVO  searchCodeDetail(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = commonCodeService.searchCodeDetail(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 공통 코드 수정 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/updateCodeData.do")
	public @ResponseBody ResultVO updateCodeData(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			commonCodeService.updateCodeData(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공통 코드 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/deleteCodeData.do")
	public @ResponseBody ResultVO deleteCodeData(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			commonCodeService.deleteCodeData(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공통 코드 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/deleteGrpCodeData.do")
	public @ResponseBody ResultVO deleteGrpCodeData(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			commonCodeService.deleteGrpCodeData(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 공통 코드 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/insertCodeData.do")
	public @ResponseBody ResultVO insertCodeData(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			commonCodeService.insertCodeData(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 코드 아이디 중복 체크
     * @param paramMap
	 * @return
	 * @throws NkiaException 
	 * @
	 */
	@RequestMapping(value="/itg/system/code/selectCodeIdComp.do")
	public @ResponseBody ResultVO selectCodeIdComp(@RequestBody ModelMap paramMap) throws NkiaException  {
		ResultVO resultVO = new ResultVO(); 
		try{
			String checkValue = commonCodeService.selectCodeIdComp(paramMap);
			resultVO.setResultString(checkValue);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 코드 엑셀다운로드
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 * @
	 */
	@RequestMapping(value="/itg/system/code/searchCodeExcelDown.do")
	public @ResponseBody ResultVO searchCodeExcelDown(@RequestBody ModelMap paramMap)throws NkiaException, WriteException, IOException   {
		ResultVO resultVO = new ResultVO();
		try {
			
			ModelMap excelMap = (ModelMap) commonCodeService.searchCodeExcelDown(paramMap);
			resultVO.setResultMap(excelMap);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
		
	/**
	 * 코드 엑셀데이터 업로드
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/code/insertCodeExcelUpload.do")
	public @ResponseBody ResultVO insertCodeExcelUpload(@RequestBody ModelMap paramMap)throws NkiaException    {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			commonCodeService.insertCodeExcelUpload(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
}
