package com.nkia.itg.system.code.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("commonCodeDAO")
public class CommonCodeDAO extends EgovComAbstractDAO {

	public List searchGrpCodeListTargetAll(HashMap paramMap) throws NkiaException {
		return this.list("CommonCodeDAO.searchGrpCodeListTargetAll", paramMap);
	}
	
	public List searchGrpCodeListTargetGroup(HashMap paramMap) throws NkiaException {
		return this.list("CommonCodeDAO.searchGrpCodeListTargetGroup", paramMap);
	}
	
	public List searchGrpCodeListTargetCode(HashMap paramMap) throws NkiaException {
		return this.list("CommonCodeDAO.searchGrpCodeListTargetCode", paramMap);
	}
	
	public List searchCodeList(HashMap paramMap) throws NkiaException {
		return this.list("CommonCodeDAO.searchCodeList", paramMap);
	}
	
	public HashMap searchCodeDetail(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("CommonCodeDAO.searchCodeDetail", paramMap);
	}
	
	public void updateCodeData(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.updateCodeData", paramMap);
	}
	
	public void updateChildCodeData(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.updateChildCodeData", paramMap);
	}
	
	public void updateCodeOrder(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.updateCodeOrder", paramMap);
	}
	
	public void deleteChildCodeData(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.deleteChildCodeData", paramMap);
	}
	
	public void deleteCodeData(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.deleteCodeData", paramMap);
	}
	
	public void deleteGrpCodeData(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.deleteCodeData", paramMap);
	}
	
	public void insertCodeData(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.insertCodeData", paramMap);
	}
	
	public HashMap selectCodeDetailData(Map paramMap) throws NkiaException {
		return (HashMap)selectByPk("CommonCodeDAO.selectCodeDetailData", paramMap);
	}
	
	public List searchCodeDataList(ModelMap paramMap) throws NkiaException {
		return this.list("CommonCodeDAO.searchCodeDataList", paramMap);
	}

	public List searchMultiCodeDataForTree(ModelMap paramMap) throws NkiaException {
		return this.list("CommonCodeDAO.searchMultiCodeDataForTree", paramMap);
	}
	
	public String selectCodeIdComp(ModelMap paramMap) throws NkiaException {
		return (String)selectByPk("CommonCodeDAO.selectCodeIdComp", paramMap);
	}
	
	public List searchCodeExcelDown(Map paramMap) throws NkiaException {
		return this.list("CommonCodeDAO.searchCodeExcelDown", paramMap);
	}
	
	public void insertCodeExcelUpload(Map paramMap) throws NkiaException {
		this.update("CommonCodeDAO.insertCodeExcelUpload", paramMap);
	}
	
	public void updateMultiCodeInfo(ModelMap paramMap) throws NkiaException {
		this.update("CommonCodeDAO.updateMultiCodeInfo", paramMap);
	}
	
	public void deleteMultiCodeInfo(ModelMap paramMap) throws NkiaException {
		this.update("CommonCodeDAO.deleteMultiCodeInfo", paramMap);
	}
	
	public void insertMultiCodeInfo(ModelMap paramMap) throws NkiaException {
		this.update("CommonCodeDAO.insertMultiCodeInfo", paramMap);
	}
	
	public HashMap selectMultiCodeDetailData(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("CommonCodeDAO.selectMultiCodeDetailData", paramMap);
	}
	
	public void updateMultiCodeGroupUseYnInfo(ModelMap paramMap) throws NkiaException {
		this.update("CommonCodeDAO.updateMultiCodeGroupUseYnInfo", paramMap);
	}
	
	public List searchCodeDataForChild(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("CommonCodeDAO.searchCodeDataForChild", paramMap);
		return resultList;
	}
	
	public int searchCodeDataForChildCnt(ModelMap paramMap) throws NkiaException{
		return (Integer)selectByPk("CommonCodeDAO.searchCodeDataForChildCnt", paramMap);
	}
	
	public List searchCodePopupList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return list("CommonCodeDAO.searchCodePopupList", paramMap);
	}
	
	public int searchCodePopupCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("CommonCodeDAO.searchCodePopupCnt", paramMap);
	}
	
	public int searchGrpCodeCnt(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("CommonCodeDAO.searchGrpCodeCnt", paramMap);
	}
}
