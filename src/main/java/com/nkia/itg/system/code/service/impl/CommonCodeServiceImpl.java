package com.nkia.itg.system.code.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jxl.write.WriteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.excel.ExcelReader;
import com.nkia.itg.base.application.util.web.WebUtil;
import com.nkia.itg.base.dao.ExcelFileDAO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.code.constraint.CodeEnum;
import com.nkia.itg.system.code.dao.CommonCodeDAO;
import com.nkia.itg.system.code.service.CommonCodeService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("commonCodeService")
public class CommonCodeServiceImpl implements CommonCodeService {

	private static final Logger logger = LoggerFactory.getLogger(CommonCodeServiceImpl.class);
	
	@Resource(name = "commonCodeDAO")
	public CommonCodeDAO commonCodeDAO;
	
	@Resource(name = "excelFileDAO")
	public ExcelFileDAO excelFileDAO;
	
	@Resource(name = "excelReader")
	private ExcelReader excelReader;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Override
	public List searchGrpCodeList(HashMap paramMap) throws NkiaException {
		String codeSearchType = (String)paramMap.get("code_search_type");
		List resultList = null;
		
		if(CodeEnum.SEARCH_TYPE_GROUP.getCodeId().equals(codeSearchType)){
			resultList = commonCodeDAO.searchGrpCodeListTargetGroup(paramMap);
		} else if(CodeEnum.SEARCH_TYPE_CODE.getCodeId().equals(codeSearchType)) {
			resultList = commonCodeDAO.searchGrpCodeListTargetCode(paramMap);
		} else {
			resultList = commonCodeDAO.searchGrpCodeListTargetAll(paramMap);
		}
		return resultList;
	}

	@Override
	public List searchCodeList(HashMap paramMap) throws NkiaException {
		paramMap = (HashMap) WebUtil.lowerCaseMapKey(paramMap);
		return commonCodeDAO.searchCodeList(paramMap);
	}
	
	@Override
	public void updateCodeData(ModelMap paramMap) throws NkiaException {
		ArrayList codeList = (ArrayList) paramMap.get("codeList");
		String updUserId = (String)paramMap.get("upd_user_id");
		
		for(int i = 0; i <codeList.size(); i++){
			Map codeMap = (Map) codeList.get(i);
			Map param = WebUtil.lowerCaseMapKey(codeMap);
			param.put("upd_user_id", updUserId);
			commonCodeDAO.updateCodeData(param);
			commonCodeDAO.updateChildCodeData(param);
		}
		
	}
	
	@Override
	public List searchMultiCodeDataForTree(ModelMap paramMap) throws NkiaException {
		return commonCodeDAO.searchMultiCodeDataForTree(paramMap);
	}
	
	@Override
	public HashMap searchCodeExcelDown(ModelMap paramMap) throws NkiaException, WriteException, IOException {
		Map excelParam  = (Map) paramMap.get("excelParam");
		Map param 		= (Map) paramMap.get("param");
		
		List resultList  = commonCodeDAO.searchCodeExcelDown(param);
		HashMap excelMap = excelMaker.makeExcelFile(excelParam, resultList);
		
		return excelMap; 
	}

	@Override
	public HashMap searchCodeDetail(ModelMap paramMap) throws NkiaException {
		return commonCodeDAO.searchCodeDetail(paramMap);
	}
	
	@Override
	public HashMap parseSingleSheetExcelData(HashMap paramMap) throws NkiaException, IOException {
		HashMap resultMap = new HashMap();
		
		HashMap fileInfoMap = excelFileDAO.searchFileInfo(paramMap);
		
		String filePathName 	= String.valueOf(fileInfoMap.get("SERVEREXCELFILENAME"));
		filePathName = filePathName.replace("/","\\");
		
		String userId = new String();
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		userId = userVO.getUser_id();
    	}
    	
    	paramMap.put("filePathName"	, filePathName);
    	paramMap.put("userId"		, userId);
    	
    	resultMap = excelReader.parseSingleSheetExcelData(paramMap);
		return resultMap;
	}
	
	@Override
	public HashMap selectCodeDetailData(ModelMap paramMap) throws NkiaException {
		return commonCodeDAO.selectCodeDetailData(paramMap);
	}
	
	@Override
	public String selectCodeIdComp(ModelMap paramMap) throws NkiaException {
		return commonCodeDAO.selectCodeIdComp(paramMap);
	}
	
	@Override
	public void deleteCodeData(ModelMap paramMap) throws NkiaException {
		ArrayList codeList = (ArrayList) paramMap.get("codeList");
		for(int i = 0; i < codeList.size(); i++){
			Map codeMap = (Map) codeList.get(i);
			Map param = WebUtil.lowerCaseMapKey(codeMap);
			commonCodeDAO.deleteCodeData(param);
		}
	}
	
	@Override
	public void deleteGrpCodeData(ModelMap paramMap) throws NkiaException {
		ArrayList codeList = (ArrayList) paramMap.get("codeList");
		for(int i = 0; i <codeList.size(); i++){
			Map codeMap = (Map) codeList.get(i);
			Map param = WebUtil.lowerCaseMapKey(codeMap);
			commonCodeDAO.deleteChildCodeData(param);
			commonCodeDAO.deleteCodeData(param);
		}
	}
	
	@Override
	public void insertCodeData(ModelMap paramMap) throws NkiaException {
		ArrayList codeList = (ArrayList) paramMap.get("codeList");
		String updUserId = (String)paramMap.get("upd_user_id");
		for(int i = 0; i <codeList.size(); i++){
			Map codeMap = (Map) codeList.get(i);
			codeMap.put("display_no", i+1);
			//대문자 파라메터를 Key를 소문자로 변경한다.
			Map param = WebUtil.lowerCaseMapKey(codeMap);
			param.put("upd_user_id", updUserId);
			commonCodeDAO.insertCodeData(param);
		}
	}
	
	@Override
	public void updateCodeOrder(ModelMap paramMap) throws NkiaException {
		ArrayList codeList = (ArrayList) paramMap.get("codeList");
		String updUserId = (String)paramMap.get("upd_user_id");
		if(codeList != null){
			for(int i = 0; i < codeList.size(); i++){
				Map codeMap = (Map) codeList.get(i);
				Map param = WebUtil.lowerCaseMapKey(codeMap);
				param.put("display_no", i+1);
				param.put("upd_user_id", updUserId);
				
				commonCodeDAO.updateCodeOrder(param);
			}
		}
	}
	
	@Override
	public void getSearchList(ModelMap paramMap, String keyNm, String valueNm,String grp_id) throws NkiaException {
		if(paramMap.containsKey(keyNm)){
			String searchKey 	= (String)paramMap.get(keyNm);
			String searchValue 	= (String)paramMap.get(valueNm);
			
			if(searchKey != null && searchValue != null && 
			   searchKey.isEmpty() && !searchValue.isEmpty()){
				ModelMap codeMap = new ModelMap(); 
				codeMap.put("grp_id", grp_id);
				List codeList = commonCodeDAO.searchCodeDataList(codeMap);
				paramMap.put(keyNm+"List", codeList);
			}
		}
	}
	
	@Override
	public void getSearchListNotIn(ModelMap paramMap,String keyNm,String valueNm,String grp_id,String notIn) throws NkiaException{
		if(paramMap.containsKey(keyNm)){
			String searchKey 	= (String)paramMap.get(keyNm);
			String searchValue 	= (String)paramMap.get(valueNm);
			
			if(searchKey != null && searchValue != null && 
			   searchKey.isEmpty() && !searchValue.isEmpty()){
				
				ModelMap codeMap = new ModelMap(); 
				codeMap.put("grp_id"	, grp_id);
				
				if(notIn != null){
					String[] notInArr = notIn.split(",");
					List notInList = new ArrayList();
					for(int i=0 ; i<notInArr.length ; i++){
						HashMap notMap = new HashMap();
						notMap.put("code_id", notInArr[i]);
						notInList.add(notMap);
					}
					codeMap.put("notInList", notInArr);
				}
				List codeList = commonCodeDAO.searchCodeDataList(codeMap);
				paramMap.put(keyNm+"List", codeList);
			}
		}
	}
	
	@Override
	public void insertCodeExcelUpload(ModelMap paramMap) throws NkiaException {
		ArrayList codeList = (ArrayList) paramMap.get("dataList");
		String updUserId = (String)paramMap.get("upd_user_id");
		for(int i = 0; i < codeList.size(); i++){
			Map codeMap = (Map) codeList.get(i);
			//대문자 파라메터를 Key를 소문자로 변경한다.
			Map param = WebUtil.lowerCaseMapKey(codeMap);
			param.put("upd_user_id", updUserId);
			commonCodeDAO.insertCodeExcelUpload(param);
		}
	}
	
	public void updateMultiCodeInfo(ModelMap paramMap) throws NkiaException {
		commonCodeDAO.updateMultiCodeInfo(paramMap);
		commonCodeDAO.updateMultiCodeGroupUseYnInfo(paramMap);
	}
	
	public void deleteMultiCodeInfo(ModelMap paramMap) throws NkiaException {
		commonCodeDAO.deleteMultiCodeInfo(paramMap);
	}
	
	public void insertMultiCodeInfo(ModelMap paramMap) throws NkiaException {
		commonCodeDAO.insertMultiCodeInfo(paramMap);
	}
	
	public HashMap selectMultiCodeDetailData(ModelMap paramMap) throws NkiaException {
		return commonCodeDAO.selectMultiCodeDetailData(paramMap);
	}
	
	public List searchCodeDataForChild(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return commonCodeDAO.searchCodeDataForChild(paramMap);
	}
	
	public int searchCodeDataForChildCnt(ModelMap paramMap) throws NkiaException{
		return commonCodeDAO.searchCodeDataForChildCnt(paramMap);
	}
	
	@Override
	public List searchCodePopupList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return commonCodeDAO.searchCodePopupList(paramMap);
	}

	@Override
	public int searchCodePopupCnt(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return commonCodeDAO.searchCodePopupCnt(paramMap);
	}

	@Override
	public int searchGrpCodeCnt(ModelMap paramMap) throws NkiaException {
		return commonCodeDAO.searchGrpCodeCnt(paramMap);
	}

}
