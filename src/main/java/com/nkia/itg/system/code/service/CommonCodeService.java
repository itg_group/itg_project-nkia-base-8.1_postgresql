package com.nkia.itg.system.code.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import jxl.write.WriteException;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface CommonCodeService {
	
	public List searchGrpCodeList(HashMap paramMap) throws NkiaException;
	
	public List searchCodeList(HashMap paramMap) throws NkiaException;
	
	public HashMap searchCodeDetail(ModelMap paramMap) throws NkiaException;
	
	public HashMap parseSingleSheetExcelData(HashMap paramMap) throws NkiaException, IOException;
	
	public void updateCodeData(ModelMap paramMap) throws NkiaException;
	
	public void updateCodeOrder(ModelMap paramMap) throws NkiaException;

	public void deleteCodeData(ModelMap paramMap) throws NkiaException;
	
	public void deleteGrpCodeData(ModelMap paramMap) throws NkiaException;
	
	public void insertCodeData(ModelMap paramMap) throws NkiaException;
	
	public String selectCodeIdComp(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectCodeDetailData(ModelMap paramMap) throws NkiaException;
	
	public void getSearchListNotIn(ModelMap paramMap,String keyNm,String valueNm,String grp_id,String notIn) throws NkiaException;
	
	public void getSearchList(ModelMap paramMap,String keyNm,String valueNm,String grp_id) throws NkiaException;
	
	public List searchMultiCodeDataForTree(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchCodeExcelDown(ModelMap paramMap) throws NkiaException, WriteException, IOException;
	
	public void insertCodeExcelUpload(ModelMap paramMap) throws NkiaException;
	
	public void updateMultiCodeInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteMultiCodeInfo(ModelMap paramMap) throws NkiaException;
	
	public void insertMultiCodeInfo(ModelMap paramMap) throws NkiaException;

	public HashMap selectMultiCodeDetailData(ModelMap paramMap) throws NkiaException;
	
	public List searchCodeDataForChild(ModelMap paramMap) throws NkiaException;
	
	public int searchCodeDataForChildCnt(ModelMap paramMap) throws NkiaException;
	
	public int searchCodePopupCnt(ModelMap paramMap) throws NkiaException;

	public List searchCodePopupList(ModelMap paramMap) throws NkiaException;
	
	public int searchGrpCodeCnt(ModelMap paramMap) throws NkiaException;
}
