package com.nkia.itg.system.reportHistory.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.system.reportHistory.dao.ReportHistoryDAO;
import com.nkia.itg.system.reportHistory.service.ReportHistoryService;

@SuppressWarnings({ "rawtypes" })
@Service("ReportHistoryService")
public class ReportHistoryServiceImpl implements ReportHistoryService {
	
	@Resource(name = "reportHistoryDAO")
	private ReportHistoryDAO reportHistoryDAO;

	/**
	 * 
	 * 엑셀 출력 내역 리스트 TotalCount 조회
	 * 
	 * @param param
	 * @return int
	 * @throws Exception
	 */
	public int searchReportHistoryListCount(ModelMap paramMap) {
		return reportHistoryDAO.searchReportHistoryListCount(paramMap);
	}

	/**
	 * 
	 * 엑셀 출력 내역 리스트 조회
	 * 
	 * @param param
	 * @return List
	 * @throws Exception
	 */
	public List searchReportHistoryList(ModelMap paramMap) {
		return reportHistoryDAO.searchReportHistoryList(paramMap);
	}
	
	/**
	 * 
	 * 엑셀 출력 내역 기록
	 * 
	 * @param param
	 * @return List
	 * @throws Exception
	 */
	public void insertReportHistory(ModelMap paramMap) {
		reportHistoryDAO.insertReportHistory(paramMap);
	}
}
