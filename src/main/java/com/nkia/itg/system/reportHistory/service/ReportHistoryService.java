package com.nkia.itg.system.reportHistory.service;

import java.util.List;

import org.springframework.ui.ModelMap;

@SuppressWarnings({ "rawtypes" })
public interface ReportHistoryService {
	
	/**
	 * 
	 * 엑셀 출력 내역 리스트 TotalCount 조회
	 * 
	 * @param param
	 * @return int
	 * @throws Exception
	 */
	public int searchReportHistoryListCount(ModelMap paramMap);
	
	/**
	 * 
	 * 엑셀 출력 내역 리스트 조회
	 * 
	 * @param param
	 * @return List
	 * @throws Exception
	 */
	public List searchReportHistoryList(ModelMap paramMap);
	
	/**
	 * 
	 * 엑셀 출력 내역 기록
	 * 
	 * @param param
	 * @throws Exception
	 */
	public void insertReportHistory(ModelMap paramMap);
}
