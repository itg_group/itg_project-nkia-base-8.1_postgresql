package com.nkia.itg.system.reportHistory.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.reportHistory.service.ReportHistoryService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@SuppressWarnings({ "rawtypes" })
@Controller
public class ReportHistoryController {
	
	@Resource(name="ReportHistoryService")
	ReportHistoryService reportHistoryService;
	
	/**
	 * reportHistory(리포트 출력 내역) 페이지 이동
	 * 
	 * @param paramMap
	 * @return String
	**/
	@RequestMapping(value="/itg/system/reportHistory/reportHistory.do")
	public String reportHistory(HttpServletRequest request, ModelMap paramMap)throws NkiaException{
		String forwarPage = "/itg/system/reportHistory/reportHistory.nvf";
		
		return forwarPage;
	}
	
	/**
	 * 엑셀 출력 내역 리스트 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException 
	 */
	@RequestMapping(value="/itg/system/reportHistory/searchReportHistoryList.do")
	public @ResponseBody ResultVO searchReportHistoryList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO result = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			paramMap.put("user_id", userId);
			
			totalCount = reportHistoryService.searchReportHistoryListCount(paramMap);
			List resultList = reportHistoryService.searchReportHistoryList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			result.setGridVO(gridVO);
			result.setSuccess(true);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		
		return result;
	}
	
	/**
	 * 엑셀 출력 내역 기록
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException 
	 */
	@RequestMapping(value="/itg/system/reportHistory/insertReportHistory.do")
	public @ResponseBody ResultVO insertReportHistory(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = userVO.getUser_id();
			
			paramMap.put("user_id", userId);
			
			reportHistoryService.insertReportHistory(paramMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
}
