package com.nkia.itg.system.reportHistory.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings({ "rawtypes" })
@Repository("reportHistoryDAO")
public class ReportHistoryDAO extends EgovComAbstractDAO {
	
	/**
	 * 
	 * 엑셀 출력 내역 리스트 조회
	 * 
	 * @param param
	 * @return List
	 * @throws Exception
	 */
	public List searchReportHistoryList(Map paramMap) {
		return list("reportHistory.searchReportHistoryList", paramMap);
	}
	
	/**
	 * 
	 * 엑셀 출력 내역 리스트 TotalCount 조회
	 * 
	 * @param param
	 * @return int
	 * @throws Exception
	 */
	public Integer searchReportHistoryListCount(Map paramMap) {
		return (Integer) selectByPk("reportHistory.searchReportHistoryListCount", paramMap);
	}
	
	/**
	 * 
	 * 엑셀 출력 내역 기록
	 * 
	 * @param param
	 * @throws Exception
	 */
	public void insertReportHistory(Map paramMap) {
		insert("reportHistory.insertReportHistory", paramMap);
	}
}
