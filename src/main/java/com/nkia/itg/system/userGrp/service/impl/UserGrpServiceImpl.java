/*
 * @(#)UserServiceImpl.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.code.dao.CommonCodeDAO;
import com.nkia.itg.system.system.service.impl.SystemServiceImpl;
import com.nkia.itg.system.userGrp.dao.UserGrpDAO;
import com.nkia.itg.system.userGrp.dao.UserGrpDutyDAO;
import com.nkia.itg.system.userGrp.service.UserGrpService;

@Service("userGrpService")
public class UserGrpServiceImpl implements UserGrpService{
	private static final Logger logger = LoggerFactory.getLogger(SystemServiceImpl.class);
	
	@Resource(name = "userGrpDAO")
	public UserGrpDAO userGrpDAO;
	
	@Resource(name = "userGrpDutyDAO")
	public UserGrpDutyDAO userGrpDutyDAO;
	
	@Resource(name = "commonCodeDAO")
	public CommonCodeDAO commonCode;
	
	public List searchStaticAllUserGrpTree(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchStaticAllUserGrpTree(paramMap);
	}
	
	public HashMap searchUserGrpBasicInfo(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchUserGrpBasicInfo(paramMap);
	}
	
	public void deleteUserGrpInfo(ModelMap paramMap) throws NkiaException {
		ArrayList resultList = new ArrayList();
		resultList = (ArrayList) userGrpDAO.searchUserGrpAllId(paramMap);
		
		userGrpDAO.deleteUserGrpInfo(paramMap);

		//권한정보
		userGrpDAO.deleteUserGrpProcess(paramMap);
		userGrpDAO.deleteUserGrpAuth(paramMap);
		
		Iterator iter = resultList.iterator();
		while(iter.hasNext()){
			HashMap<String, String> val = (HashMap) iter.next();
			paramMap.put("user_grp_id", val.get("USER_GRP_ID"));
			//기본정보
			userGrpDAO.deleteUserGrpInfo(paramMap);
			//권한정보
			userGrpDAO.deleteUserGrpProcess(paramMap);
			userGrpDAO.deleteUserGrpAuth(paramMap);
			
			// 추가된 장비 구성에 관한 데이터
			// userGrpDutyDAO.deleteUserGrpEquip(paramMap);
		}
	}
	
	public void updateUserGrpInfo(ModelMap paramMap) throws NkiaException {
		ArrayList pro_auth_id;
		ArrayList auth_id;
		String activeId = null;
		String userId = (String) paramMap.get("upd_user_id");
		
		Map infoMap = (Map)paramMap.get("infoMap");
		infoMap.put("upd_user_id",userId);
		
		if( !paramMap.containsKey("activeId") ){
			userGrpDAO.updateUserGrpInfo((Map)paramMap.get("infoMap"));
			userGrpDAO.updateUserGrpUseAll((Map)paramMap.get("infoMap"));
		}else{
			Map mappingMap = (Map)paramMap.get("mappingMap");
			mappingMap.put("upd_user_id", userId);
			
			activeId = (String) paramMap.get("activeId");
			
			if( activeId.equals("userGrpMappingGridForm") ){
				userGrpDAO.deleteUserGrpProcess((Map)paramMap.get("mappingMap"));
				userGrpDAO.deleteUserGrpAuth((Map)paramMap.get("mappingMap"));
				
				pro_auth_id = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingMap")).get("pro_auth_id");
				for(int i=0;i<pro_auth_id.size();i++){
					((HashMap<String, Object>) paramMap.get("mappingMap")).put("pro_auth_id", pro_auth_id.get(i));
					userGrpDAO.insertUserGrpProcess((Map)paramMap.get("mappingMap"));
				}
				
				auth_id = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingMap")).get("auth_id");
				for(int i=0;i<auth_id.size();i++){
					((HashMap<String, Object>) paramMap.get("mappingMap")).put("auth_id", auth_id.get(i));
					userGrpDAO.insertUserGrpAuth((Map)paramMap.get("mappingMap"));
				}
			}
		}
	}
	
	public void updateUserGrpUseAll(ModelMap paramMap) throws NkiaException {
		userGrpDAO.updateUserGrpUseAll(paramMap);
	}

	public void insertUserGrpInfo(ModelMap paramMap) throws NkiaException {
		ArrayList pro_auth_id;
		ArrayList auth_id;
		
		String userId = (String) paramMap.get("upd_user_id");
		
		Map infoMap = (Map)paramMap.get("infoMap");
		infoMap.put("upd_user_id",userId);
		userGrpDAO.insertUserGrpInfo(infoMap);
// Insert 안되게 수정		
//		Map mappingMap = (Map)paramMap.get("mappingMap");
//		mappingMap.put("upd_user_id", userId);
//		
//		pro_auth_id = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingMap")).get("pro_auth_id");
//		for(int i=0;i<pro_auth_id.size();i++){
//			((HashMap<String, Object>) paramMap.get("mappingMap")).put("pro_auth_id", pro_auth_id.get(i));
//			userGrpDAO.insertUserGrpProcess((Map)paramMap.get("mappingMap"));
//		}
//		
//		auth_id = (ArrayList) ((LinkedHashMap<String, Object>) paramMap.get("mappingMap")).get("auth_id");
//		for(int i=0;i<auth_id.size();i++){
//			((HashMap<String, Object>) paramMap.get("mappingMap")).put("auth_id", auth_id.get(i));
//			userGrpDAO.insertUserGrpAuth((Map)paramMap.get("mappingMap"));
//		}
		
		//@@ 고한조 - 추가 Start
		// 추가된 장비 구성에 관한 데이터
		/*
		userGrpDutyDAO.deleteUserGrpEquip(infoMap);
		
		ModelMap codeParam = new ModelMap();
		codeParam.put("grp_id", "CLASS_TYPE");
		List code_list = (List) commonCode.searchCodeDataList(codeParam);
		
		List equip_list = (List) mappingMap.get("equip_list");
		for( int i=0; i < equip_list.size(); i++ ){
			Map equip = (Map) equip_list.get(i);
			
			Iterator codeIterater = code_list.iterator();
			while( codeIterater.hasNext() ){
				Map code = (Map) codeIterater.next();
				Object value = equip.get( code.get("KEY") );
				
				if( value != null && value.equals(true) ){
					Map insertParam = new HashMap();
					
					insertParam.put("user_grp_id", infoMap.get("user_grp_id") );
					insertParam.put("cust_id", equip.get("CUST_ID") );
					insertParam.put("value", code.get("KEY") );
					
					userGrpDutyDAO.insertUserGrpEquip(insertParam);
				}
			}
		}
		*/
		
		// 추가된 개별 장비 구성에 관한 데이터
		//userGrpDutyDAO.deleteUserGrpEachConf(infoMap);
		/*
		List search_list = (List) mappingMap.get("search_list");
		for( int i=0; i < search_list.size(); i++ ){
			Map search = (Map) search_list.get(i);
			
			search.put("user_grp_id", infoMap.get("user_grp_id"));
			
			userGrpDutyDAO.insertUserGrpEachConf(search);
		}
		*/
		//@@ 고한조 - 추가 End
	}
	
	public List searchSystemAuthList(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchSystemAuthList(paramMap);
	}
	
	public List searchProcessAuthList(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchProcessAuthList(paramMap);
	}
	
	public List searchSystemAuthListPop(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchSystemAuthListPop(paramMap);
	}
	
	public List searchProcessAuthListPop(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchProcessAuthListPop(paramMap);
	}
	
	public String selectUserGrpIdComp(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.selectUserGrpIdComp(paramMap);
	}
	
	public void insertUserGrpProcess(ModelMap paramMap) throws NkiaException {
		userGrpDAO.insertUserGrpProcess(paramMap);
	}
	
	public void insertUserGrpAuth(ModelMap paramMap) throws NkiaException {
		userGrpDAO.insertUserGrpAuth(paramMap);
	}
	
	public void deleteUserGrpProcess(ModelMap paramMap) throws NkiaException {
		userGrpDAO.deleteUserGrpProcess(paramMap);
	}
	
	public void deleteUserGrpAuth(ModelMap paramMap) throws NkiaException {
		userGrpDAO.deleteUserGrpAuth(paramMap);
	}
	
	public List searchMappingMenu(ModelMap paramMap) throws NkiaException {
		String menuAuth = userGrpDAO.selectAuthMenu(paramMap);
		paramMap.put("menu_auth", menuAuth);
		return userGrpDAO.searchMappingMenu(paramMap);
	}
	
	public List searchUserGrpAllId(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchUserGrpAllId(paramMap);
	}
	
	public String selectCheckUserGrp(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.selectCheckUserGrp(paramMap);
	}

	@Override
	public List selectUserGrpAuthList(Map paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return userGrpDAO.selectUserGrpAuthList(paramMap);
	}
	
	public void updateUserGrpTree(Map paramMap) throws NkiaException {
		userGrpDAO.updateUserGrpTree(paramMap);
	}
	
	public int searchUserListCount(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchUserListCount(paramMap);
	}
	
	public List searchUserList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = userGrpDAO.searchUserList(paramMap); 
		return resultList;
	}
	
	public int searchUserGrpOrderListCount(ModelMap paramMap) throws NkiaException {
		return userGrpDAO.searchUserGrpOrderListCount(paramMap);
	}
	
	public List searchUserGrpOrderList(ModelMap paramMap) throws NkiaException {
		
		List resultList = null;
		resultList = userGrpDAO.searchUserGrpOrderList(paramMap); 
		return resultList;
	}
	public void updateUserGrpOrderList(ModelMap paramMap) throws NkiaException {
		List orderList = (List)paramMap.get("orderList");    //하위부서  리스트
		//
		//하위부서 순서변경
		if(orderList != null){
			for(int i = 0; i<orderList.size(); i++){
				HashMap orderData = (HashMap)orderList.get(i);
				
				orderData.put("user_grp_id", orderData.get("USER_GRP_ID"));
				orderData.put("display_no", i+1);
				orderData.put("upd_user_id", paramMap.get("upd_user_id"));
				
				userGrpDAO.updateUserGrpOrder(orderData);				
			}
		}
	}
}
