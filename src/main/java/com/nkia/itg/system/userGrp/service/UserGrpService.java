/*
 * @(#)UserGrpService.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface UserGrpService {
	
	public List searchStaticAllUserGrpTree(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchUserGrpBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteUserGrpInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateUserGrpInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateUserGrpUseAll(ModelMap paramMap) throws NkiaException;
	
	public void insertUserGrpInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchSystemAuthList(ModelMap paramMap) throws NkiaException;
	
	public List searchProcessAuthList(ModelMap paramMap) throws NkiaException;
	
	public List searchSystemAuthListPop(ModelMap paramMap) throws NkiaException;
	
	public List searchProcessAuthListPop(ModelMap paramMap) throws NkiaException;
	
	public String selectUserGrpIdComp(ModelMap paramMap) throws NkiaException;
	
	public void insertUserGrpProcess(ModelMap paramMap) throws NkiaException;
	
	public void insertUserGrpAuth(ModelMap paramMap) throws NkiaException;
	
	public void deleteUserGrpProcess(ModelMap paramMap) throws NkiaException;
	
	public void deleteUserGrpAuth(ModelMap paramMap) throws NkiaException;
	
	public List searchMappingMenu(ModelMap paramMap) throws NkiaException;
	
	public List searchUserGrpAllId(ModelMap paramMap) throws NkiaException;
	
	public String selectCheckUserGrp(ModelMap paramMap) throws NkiaException;
	
	public List selectUserGrpAuthList(Map paramMap) throws NkiaException;
	
	public void updateUserGrpTree(Map paramMap) throws NkiaException;
	
	public int searchUserListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchUserList(ModelMap paramMap) throws NkiaException;
	
	public int searchUserGrpOrderListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchUserGrpOrderList(ModelMap paramMap) throws NkiaException;
	
	public void updateUserGrpOrderList(ModelMap paramMap) throws NkiaException;
}
