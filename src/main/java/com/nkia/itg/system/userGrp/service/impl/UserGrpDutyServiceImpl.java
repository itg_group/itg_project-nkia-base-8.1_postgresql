/*
 * @(#)UserServiceImpl.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.code.dao.CommonCodeDAO;
import com.nkia.itg.system.userGrp.dao.UserGrpDutyDAO;
import com.nkia.itg.system.userGrp.service.UserGrpDutyService;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Service("userGrpDutyService")
public class UserGrpDutyServiceImpl implements UserGrpDutyService{
	
	@Resource(name = "userGrpDutyDAO")
	public UserGrpDutyDAO userGrpDutyDAO;
	
	@Resource(name = "commonCodeDAO")
	public CommonCodeDAO commonCode;
	
	/**
	 * 선택된 사용자 그룹별 요청 부서 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchUserGrpToDuty( Map param ) throws NkiaException {
		return userGrpDutyDAO.searchUserGrpToDuty(param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchUserGrpEquip( Map param ) throws NkiaException {
		
		// Custom Query - 공통 코드에 있는 구성으로 쿼리 생성하여 조회(동적 쿼리) 
		ModelMap codeParam = new ModelMap();
		codeParam.put("grp_id", "CLASS_TYPE");
		List code_list = (List) commonCode.searchCodeDataList(codeParam);
		
		Iterator codeIterater = code_list.iterator();
		StringBuffer appendQuery = new StringBuffer();
		while( codeIterater.hasNext() ){
			Map code = (Map) codeIterater.next();
			String key = (String) code.get("KEY");
			
			appendQuery.append(",MAX(DECODE(COL_TYPE,'");
			appendQuery.append(key);
			appendQuery.append("','true','false')) AS ");
			appendQuery.append(key);
		}
		
		param.put("appendQuery", appendQuery);
		
		return userGrpDutyDAO.searchUserGrpEquip(param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchEquip( Map param ) throws NkiaException {
		Object classType = param.get("class_type");
		
		if( classType != null && classType.getClass() == String.class ){
			param.put("class_type", ((String) classType).split(","));
		}
		
		Object assetState = param.get("asset_state");
		
		if( assetState != null && assetState.getClass() == String.class ){
			param.put("asset_state", ((String) assetState).split(","));
		}
		
		return userGrpDutyDAO.searchEquip(param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 TotalCount 조회
	 * 
     * @param param
	 * @return int
	 * @throws NkiaException
	 */
	public int searchEquipCount( Map param ) throws NkiaException {
		Object classType = param.get("class_type");
		
		if( classType != null && classType.getClass() == String.class ){
			param.put("class_type", ((String) classType).split(","));
		}
		
		Object assetState = param.get("asset_state");
		
		if( assetState != null && assetState.getClass() == String.class ){
			param.put("asset_state", ((String) assetState).split(","));
		}
		
		return userGrpDutyDAO.searchEquipCount(param);
	}
	
	/**
	 * 선택된 사용자 그룹별 선택 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchSelectEquip( Map param ) throws NkiaException {
		Object classType = param.get("class_type");
		
		if( classType != null && classType.getClass() == String.class ){
			param.put("class_type", ((String) classType).split(","));
		}
		
		Object assetState = param.get("asset_state");
		
		if( assetState != null && assetState.getClass() == String.class ){
			param.put("asset_state", ((String) assetState).split(","));
		}
		
		return userGrpDutyDAO.searchSelectEquip(param);
	}
	
	/**
	 * 선택된 사용자 그룹별 선택 구성 리스트 TotalCount 조회
	 * 
     * @param param
	 * @return int
	 * @throws NkiaException
	 */
	public int searchSelectEquipCount( Map param ) throws NkiaException {
		Object classType = param.get("class_type");
		
		if( classType != null && classType.getClass() == String.class ){
			param.put("class_type", ((String) classType).split(","));
		}
		
		Object assetState = param.get("asset_state");
		
		if( assetState != null && assetState.getClass() == String.class ){
			param.put("asset_state", ((String) assetState).split(","));
		}
		
		return userGrpDutyDAO.searchSelectEquipCount(param);
	}
	
	/**
	 * 개별 장비 등록 리스트 삭제
	 * 
     * @param param
	 * @throws NkiaException
	 */
	public void deleteEquip( Map param ) throws NkiaException {
		
		List deleteList = (List) param.get("deleteList");
		for( int i=0; i < deleteList.size(); i++ ){
			Map deleteMap = (Map) deleteList.get(i);
			deleteMap.put("user_grp_id", param.get("user_grp_id"));
			
			userGrpDutyDAO.deleteEquip(deleteMap);
		}
	}
}
