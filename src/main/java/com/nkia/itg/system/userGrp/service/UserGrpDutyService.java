/*
 * @(#)UserGrpService.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

@SuppressWarnings({ "rawtypes" })
public interface UserGrpDutyService {
	
	/**
	 * 선택된 사용자 그룹별 요청 부서 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchUserGrpToDuty( Map param ) throws NkiaException;
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchUserGrpEquip(Map param) throws NkiaException;
	
	/**
	 * 선택된 사용자 그룹별 선택 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchSelectEquip(Map param) throws NkiaException;
	
	/**
	 * 선택된 사용자 그룹별 선택 구성 리스트 TotalCount 조회
	 * 
     * @param param
	 * @return int
	 * @throws NkiaException
	 */
	public int searchSelectEquipCount(Map param) throws NkiaException;
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	List searchEquip(Map param) throws NkiaException;
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 TotalCount 조회
	 * 
     * @param param
	 * @return int
	 * @throws NkiaException
	 */
	int searchEquipCount(Map param) throws NkiaException;

	/**
	 * 개별 장비 등록 리스트 삭제
	 * 
     * @param param
	 * @throws NkiaException
	 */
	void deleteEquip(Map param) throws NkiaException;
}
