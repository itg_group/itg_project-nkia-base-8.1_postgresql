/*
 * @(#)UserGrpController.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.userGrp.service.UserGrpService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class UserGrpController {
	
	@Resource(name = "userGrpService")
	private UserGrpService userGrpService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 사용자그룹관리페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/userGrp/goUserGrpManager.do")
	public String userManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/userGrp/userGrpManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 사용자그룹관리트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchStaticAllUserGrpTree.do")
	public @ResponseBody ResultVO searchStaticAllUserGrpTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = userGrpService.searchStaticAllUserGrpTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹 기본정보 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchUserGrpBasicInfo.do")
	public @ResponseBody ResultVO searchUserGrpBasicInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = userGrpService.searchUserGrpBasicInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹 정보 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/deleteUserGrpInfo.do")
	public @ResponseBody ResultVO deleteUserGrpInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			userGrpService.deleteUserGrpInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹 정보 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/updateUserGrpInfo.do")
	public @ResponseBody ResultVO updateUserGrpInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			userGrpService.updateUserGrpInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹 정보 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/insertUserGrpInfo.do")
	public @ResponseBody ResultVO insertUserGrpInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			userGrpService.insertUserGrpInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹맵핑 시스템권한리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchSystemAuthList.do")
	public @ResponseBody ResultVO searchSystemAuthList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			List resultList = userGrpService.searchSystemAuthList(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹맵핑 프로세스권한리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchProcessAuthList.do")
	public @ResponseBody ResultVO searchProcessAuthList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = userGrpService.searchProcessAuthList(paramMap);
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹맵핑 선택할 시스템 권한리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchSystemAuthListPop.do")
	public @ResponseBody ResultVO searchSystemAuthListPop(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = userGrpService.searchSystemAuthListPop(paramMap);
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹맵핑 선택할 프로세스 권한리스트 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchProcessAuthListPop.do")
	public @ResponseBody ResultVO searchProcessAuthListPop(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = userGrpService.searchProcessAuthListPop(paramMap);
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹 아이디 존재 유무 체크 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/selectUserGrpIdComp.do")
	public @ResponseBody ResultVO selectUserGrpIdComp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{

			String checkValue = userGrpService.selectUserGrpIdComp(paramMap);

			resultVO.setResultString(checkValue);
				
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 시스템권한에 맵핑된 메뉴 목록 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchMappingMenu.do")
	public @ResponseBody ResultVO searchMappingMenu(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			CheckTreeVO treeVO = null;
			
			List resultList = userGrpService.searchMappingMenu(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expandLevel = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeCheckTreeToJson(resultList, up_node_id, true, expandLevel, false);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 사용자그룹에 포함된 사용자 유무 체크
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/selectCheckUserGrp.do")
	public @ResponseBody ResultVO selectCheckUserGrp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			String checkValue = userGrpService.selectCheckUserGrp(paramMap);
			resultVO.setResultString(checkValue);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 트리 Drag & Drop Event
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/updateUserGrpTree.do")
	public @ResponseBody ResultVO updateUserGrpTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			userGrpService.updateUserGrpTree(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 사용자 그룹을 소유하고 있는 사용자를 조회
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchUserList.do")
	public @ResponseBody ResultVO searchUserList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = userGrpService.searchUserListCount(paramMap);
			List resultList = userGrpService.searchUserList(paramMap);
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자 그룹을 소유하고 있는 사용자를 조회
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/userGrp/searchUserGrpOrderList.do")
	public @ResponseBody ResultVO searchUserGrpOrderList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = userGrpService.searchUserGrpOrderListCount(paramMap);
			List resultList = userGrpService.searchUserGrpOrderList(paramMap);
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	@RequestMapping(value="/itg/system/dept/updateUserGrpOrderList.do")
	public @ResponseBody ResultVO updateDeptInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	userGrpService.updateUserGrpOrderList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
