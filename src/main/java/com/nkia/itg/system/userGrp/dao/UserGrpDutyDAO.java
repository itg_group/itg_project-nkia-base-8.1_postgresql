/*
 * @(#)UserGrpDAO.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@SuppressWarnings({ "rawtypes" })
@Repository("userGrpDutyDAO")
public class UserGrpDutyDAO extends EgovComAbstractDAO {
	
	/**
	 * 선택된 사용자 그룹별 요청 부서 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchUserGrpToDuty( Map param ) throws NkiaException {
		return list("UserGrpDutyDAO.searchUserGrpToDuty", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchUserGrpEquip( Map param ) throws NkiaException {
		return list("UserGrpDutyDAO.searchUserGrpEquip", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchEquip( Map param ) throws NkiaException {
		return list("UserGrpDutyDAO.searchEquip", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 TotalCount 조회
	 * 
     * @param param
	 * @return int
	 * @throws NkiaException
	 */
	public int searchEquipCount( Map param ) throws NkiaException {
		return (Integer) selectByPk("UserGrpDutyDAO.searchEquipCount", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 선택 구성 리스트 조회
	 * 
     * @param param
	 * @return List
	 * @throws NkiaException
	 */
	public List searchSelectEquip( Map param ) throws NkiaException {
		return list("UserGrpDutyDAO.searchSelectEquip", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 선택 구성 리스트 TotalCount 조회
	 * 
     * @param param
	 * @return int
	 * @throws NkiaException
	 */
	public int searchSelectEquipCount( Map param ) throws NkiaException {
		return (Integer) selectByPk("UserGrpDutyDAO.searchSelectEquipCount", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 정보 등록
	 * 
     * @param param
	 * @throws NkiaException
	 */
	public void insertUserGrpEquip( Map param ) throws NkiaException {
		insert("UserGrpDutyDAO.insertUserGrpEquip", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 정보 삭제
	 * 
     * @param param
	 * @throws NkiaException
	 */
	public void deleteUserGrpEquip( Map param ) throws NkiaException {
		delete("UserGrpDutyDAO.deleteUserGrpEquip", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 개별 장비 등록
	 * 
     * @param param
	 * @throws NkiaException
	 */
	public void insertUserGrpEachConf( Map param ) throws NkiaException {
		insert("UserGrpDutyDAO.insertUserGrpEachConf", param);
	}
	
	/**
	 * 선택된 사용자 그룹별 개별 장비 삭제
	 * 
     * @param param
	 * @throws NkiaException
	 */
	public void deleteUserGrpEachConf( Map param ) throws NkiaException {
		delete("UserGrpDutyDAO.deleteUserGrpEachConf", param);
	}
	
	/**
	 * 개별 장비 등록 리스트 삭제
	 * 
     * @param param
	 * @throws NkiaException
	 */
	public void deleteEquip( Map param ) throws NkiaException {
		delete("UserGrpDutyDAO.deleteEquip", param);
	}
}
