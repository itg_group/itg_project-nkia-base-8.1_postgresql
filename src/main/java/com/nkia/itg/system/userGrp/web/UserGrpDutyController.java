/*
 * @(#)UserGrpController.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.userGrp.service.UserGrpDutyService;

import egovframework.com.cmm.EgovMessageSource;

@SuppressWarnings({ "rawtypes" })
@Controller
public class UserGrpDutyController {
	
	@Resource(name = "userGrpDutyService")
	private UserGrpDutyService userGrpDutyService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 선택된 사용자 그룹별 요청 부서 리스트 조회
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/userGrp/searchUserGrpToDuty.do")
	public @ResponseBody ResultVO searchUserGrpToDuty(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			List resultList = userGrpDutyService.searchUserGrpToDuty(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/userGrp/searchUserGrpEquip.do")
	public @ResponseBody ResultVO searchUserGrpEquip(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			List resultList = userGrpDutyService.searchUserGrpEquip(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 선택된 사용자 그룹별 구성 리스트 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/userGrp/searchEquip.do")
	public @ResponseBody ResultVO searchEquip(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			PagingUtil.getFristEndNum(paramMap);
			int totalCount = userGrpDutyService.searchEquipCount(paramMap);
			List resultList = userGrpDutyService.searchEquip(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 선택된 사용자 그룹별 선택 구성 리스트 조회
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/userGrp/searchSelectEquip.do")
	public @ResponseBody ResultVO searchSelectEquip(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			
			PagingUtil.getFristEndNum(paramMap);
			int totalCount = userGrpDutyService.searchSelectEquipCount(paramMap);
			List resultList = userGrpDutyService.searchSelectEquip(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 개별 장비 등록 리스트 삭제
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/userGrp/deleteEquip.do")
	public @ResponseBody ResultVO deleteEquip(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			userGrpDutyService.deleteEquip(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
}
