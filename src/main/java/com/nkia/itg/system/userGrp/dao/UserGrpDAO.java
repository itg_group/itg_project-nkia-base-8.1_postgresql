/*
 * @(#)UserGrpDAO.java              2013. 3. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.userGrp.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("userGrpDAO")
public class UserGrpDAO extends EgovComAbstractDAO {
	
	public List searchStaticAllUserGrpTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("UserGrpDAO.searchStaticAllUserGrpTree", paramMap);
		return resultList;
	}
	
	public HashMap searchUserGrpBasicInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("UserGrpDAO.searchUserGrpBasicInfo", paramMap);
	}
	
	public void deleteUserGrpInfo(Map object) throws NkiaException {
		this.delete("UserGrpDAO.deleteUserGrpInfo", object);
	}
	
	public void updateUserGrpInfo(Map object) throws NkiaException {
		this.update("UserGrpDAO.updateUserGrpInfo", object);
	}
	
	public void updateUserGrpUseAll(Map object) throws NkiaException {
		this.update("UserGrpDAO.updateUserGrpUseAll", object);
	}
	
	public void insertUserGrpInfo(Map object) throws NkiaException {
		this.insert("UserGrpDAO.insertUserGrpInfo", object);
	}
	
	public List searchSystemAuthList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("UserGrpDAO.searchSystemAuthList", paramMap);
		return resultList;
	}
	
	public List searchProcessAuthList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("UserGrpDAO.searchProcessAuthList", paramMap);
		return resultList;
	}
	
	public List searchSystemAuthListPop(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("UserGrpDAO.searchSystemAuthListPop", paramMap);
		return resultList;
	}
	
	public List searchProcessAuthListPop(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("UserGrpDAO.searchProcessAuthListPop", paramMap);
		return resultList;
	}
	
	public String selectUserGrpIdComp(ModelMap paramMap) throws NkiaException {
		return (String) this.selectByPk("UserGrpDAO.selectUserGrpIdComp", paramMap);
	}
	
	public void insertUserGrpProcess(Map paramMap) throws NkiaException {
		this.insert("UserGrpDAO.insertUserGrpProcess", paramMap);
	}
	
	public void insertUserGrpAuth(Map paramMap) throws NkiaException {
		this.insert("UserGrpDAO.insertUserGrpAuth", paramMap);
	}
	
	public void deleteUserGrpProcess(Map object) throws NkiaException {
		this.delete("UserGrpDAO.deleteUserGrpProcess", object);
	}
	
	public void deleteUserGrpAuth(Map object) throws NkiaException {
		this.delete("UserGrpDAO.deleteUserGrpAuth", object);
	}
	
	public List searchMappingMenu(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("UserGrpDAO.searchMappingMenu", paramMap);
		return resultList;
	}
	
	public List searchUserGrpAllId(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("UserGrpDAO.searchUserGrpAllId", paramMap);
		return resultList;
	}
	
	public String selectCheckUserGrp(ModelMap paramMap) throws NkiaException {
		return (String) this.selectByPk("UserGrpDAO.selectCheckUserGrp", paramMap);
	}
	
	/**
	 * 사용자 그룹권한 목록을 가져온다.
	 * @param pramaMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectUserGrpAuthList(Map paramMap) throws NkiaException{
		return list("UserGrpDAO.selectUserGrpAuthList", paramMap);
	}
	
	public String selectAuthMenu(Map paramMap) throws NkiaException{
		String menuAuth = (String) this.selectByPk("UserGrpDAO.selectAuthMenu", paramMap);
		return menuAuth;
	}	
	
	public void updateUserGrpTree(Map object) throws NkiaException {
		this.delete("UserGrpDAO.updateUserGrpTree", object);
	}
	
	/**
	 * 사용자 그룹을 소유하고 있는 사용자를 조회
	 * @param pramaMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchUserListCount(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("UserGrpDAO.searchUserListCount", paramMap);
	}
	
	public List searchUserList(ModelMap paramMap) throws NkiaException {
		return list("UserGrpDAO.searchUserList", paramMap);
	}
	
	public int searchUserGrpOrderListCount(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("UserGrpDAO.searchUserGrpOrderListCount", paramMap);
	}
	
	public List searchUserGrpOrderList(ModelMap paramMap) throws NkiaException {
		return list("UserGrpDAO.searchUserGrpOrderList", paramMap);
	}
	public void updateUserGrpOrder(Map object) throws NkiaException {
		this.update("UserGrpDAO.updateUserGrpOrder", object);
	}
}
