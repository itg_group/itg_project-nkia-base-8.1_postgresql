package com.nkia.itg.system.loginInfo.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.loginInfo.dao.LoginInfoDAO;
import com.nkia.itg.system.loginInfo.service.LoginInfoService;

@Service("loginInfoService")
public class LoginInfoServiceImpl implements LoginInfoService{
	
	private static final Logger logger = LoggerFactory.getLogger(LoginInfoServiceImpl.class);
	
	@Resource(name = "loginInfoDAO")
	public LoginInfoDAO loginInfoDAO;
	
	
	public List searchLoginList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return loginInfoDAO.searchLoginList(paramMap);
	}

	public int searchLoginListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return loginInfoDAO.searchLoginListCount(paramMap);
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = loginInfoDAO.searchExcelDown(paramMap); 
		return resultList;
	}
}
