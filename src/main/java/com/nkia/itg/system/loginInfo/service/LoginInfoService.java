package com.nkia.itg.system.loginInfo.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface LoginInfoService {

	public List searchLoginList(ModelMap paramMap) throws NkiaException;

	public int searchLoginListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchExcelDown(Map paramMap) throws NkiaException;
}
