package com.nkia.itg.system.loginInfo.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("loginInfoDAO")
public class LoginInfoDAO extends EgovComAbstractDAO{
	
	public List searchLoginList(ModelMap paramMap) throws NkiaException{
		return list("LoginInfoDAO.searchLoginList", paramMap);
	}

	public int searchLoginListCount(ModelMap paramMap)throws NkiaException {
		// TODO Auto-generated method stub
		return (Integer)selectByPk("LoginInfoDAO.searchLoginListCount", paramMap);
	}
	
	public List searchExcelDown(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("LoginInfoDAO.searchLoginList", paramMap);
		return resultList;
	}
}
