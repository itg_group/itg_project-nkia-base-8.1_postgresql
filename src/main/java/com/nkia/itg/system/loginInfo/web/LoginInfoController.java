package com.nkia.itg.system.loginInfo.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jxl.write.WriteException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.loginInfo.service.LoginInfoService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class LoginInfoController {
	
	@Resource(name = "loginInfoService")
	private LoginInfoService loginInfoService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@RequestMapping(value="/itg/system/loginInfo/loginInfoManager.do")
	public String loginInfoManager(ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/loginInfo/loginInfoManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 접속정보리스트 조회
     * @param paramMap
	 * @return
	 * @
	 */	
	@RequestMapping(value="/itg/system/loginInfo/searchLoginList.do")
	public @ResponseBody ResultVO searchLoginList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			//@@ 페이징 관련 내용 
			PagingUtil.getFristEndNum(paramMap);
			resultList = loginInfoService.searchLoginList(paramMap);		
			totalCount = loginInfoService.searchLoginListCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 * @
	 */			
	@RequestMapping(value="/itg/system/loginInfo/searchExcelDown.do")
    public @ResponseBody ResultVO searchExcelDown(@RequestBody ModelMap paramMap)throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = loginInfoService.searchExcelDown(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}	
}
