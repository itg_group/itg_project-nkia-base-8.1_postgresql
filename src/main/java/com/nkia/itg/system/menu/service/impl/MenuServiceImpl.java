/*
 * @(#)MenuServiceImpl.java              2013. 3. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.menu.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.system.menu.dao.MenuDAO;
import com.nkia.itg.system.menu.service.MenuService;


@Service("menuService")
public class MenuServiceImpl implements MenuService{
	private static final Logger logger = LoggerFactory.getLogger(MenuServiceImpl.class);
	
	@Resource(name = "menuDAO")
	public MenuDAO menuDAO;
	
	public List searchAuthMappingMenu(ModelMap paramMap) throws NkiaException {
		return menuDAO.searchAuthMappingMenu(paramMap);
	}
	
	public List searchMenuStore(ModelMap paramMap) throws NkiaException {
		return menuDAO.searchMenuStore(paramMap);
	}
	
	public List searchStaticAllMenuTree(ModelMap paramMap) throws NkiaException {
		return menuDAO.searchStaticAllMenuTree(paramMap);
	}
	
	public HashMap selectMenuBasicInfo(ModelMap paramMap) throws NkiaException {
		return menuDAO.selectMenuBasicInfo(paramMap);
	}
	
	//@@20130711 전민수 START
    //deleteMenuSystemAuth와 deleteMenuInfo의 위아래 실행순서를 바꿈
	public void deleteMenuInfo(ModelMap paramMap) throws NkiaException {
		menuDAO.deleteMenuSystemAuth(paramMap);
		menuDAO.deleteMenuInfo(paramMap);
	}
	//@@20130711 전민수 
    //END
	
	public void deleteMenuSystemAuth(ModelMap paramMap) throws NkiaException {
		menuDAO.deleteMenuSystemAuth(paramMap);
		
	}
	
	public void updateMenuInfo(ModelMap paramMap) throws NkiaException {
		menuDAO.updateMenuInfo(paramMap);
		//menuDAO.updateMenuUseALL(paramMap);
	}
	
	public void updateMenuUseALL(ModelMap paramMap) throws NkiaException {
		menuDAO.updateMenuUseALL(paramMap);
	}
	
	public void updateMenuOrder(ModelMap paramMap) throws NkiaException {
		menuDAO.updateMenuOrder(paramMap);
	}
	
	public void insertMenuInfo(ModelMap paramMap) throws NkiaException {
		menuDAO.insertMenuInfo(paramMap);
	}
	
	public int searchMenuGridListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return menuDAO.searchMenuGridListCount(paramMap);
	}
	
	public List searchMenuGridList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return menuDAO.searchMenuGridList(paramMap);
	}
	
	public List searchMenuAllId(ModelMap paramMap) throws NkiaException {
		return menuDAO.searchMenuAllId(paramMap);
	}
	
	public Map searchTopMenuForUser(Map paramMap) throws NkiaException {
		paramMap.put("type", "top");
		List resultList = menuDAO.searchTopMenuForUser(paramMap);
		List topMenuList = new ArrayList();
		List subMenuList = new ArrayList();
		for(int i = 0 ; i < resultList.size() ; i++) {
			TreeVO row = (TreeVO)resultList.get(i);
			if("1".equals(row.getNode_level())) {
				topMenuList.add(row);
			} else if("2".equals(row.getNode_level())) {
				subMenuList.add(row);
			}
		}
		Map resultMap = new HashMap();
		resultMap.put("topMenuList", topMenuList);
		resultMap.put("subMenuList", subMenuList);
		return resultMap;
	}

	@Override
	public Map selectTabMenuInfo(String url) throws NkiaException {
		return menuDAO.selectTabMenuInfo(url);
	}
	
	public void updateMenuTree(ModelMap paramMap) throws NkiaException {
		menuDAO.updateMenuTree(paramMap);
	}
}
