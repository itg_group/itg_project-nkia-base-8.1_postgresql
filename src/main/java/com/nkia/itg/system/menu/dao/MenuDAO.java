/*
 * @(#)MenuDAO.java              2013. 3. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.menu.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("menuDAO")
public class MenuDAO extends EgovComAbstractDAO {
	
	public List searchAuthMappingMenu(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("MenuDAO.searchAuthMappingMenu", paramMap);
		return resultList;
	}
	
	public List searchMenuStore(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("MenuDAO.searchMenuStore", paramMap);
		return resultList;
	}
	
	public List searchStaticAllMenuTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("MenuDAO.searchStaticAllMenuTree", paramMap);
		return resultList;
	}
	
	public HashMap selectMenuBasicInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("MenuDAO.selectMenuBasicInfo", paramMap);
	}
	
	public void deleteMenuInfo(ModelMap paramMap) throws NkiaException {
		this.delete("MenuDAO.deleteMenuInfo", paramMap);
	}
	
	public void deleteMenuSystemAuth(ModelMap paramMap) throws NkiaException {
		this.delete("MenuDAO.deleteMenuSystemAuth", paramMap);
	}
	
	public void updateMenuInfo(ModelMap paramMap) throws NkiaException {
		this.update("MenuDAO.updateMenuInfo", paramMap);
	}
	
	public void updateMenuUseALL(ModelMap paramMap) throws NkiaException {
		this.update("MenuDAO.updateMenuUseALL", paramMap);
	}
	
	public void updateMenuOrder(ModelMap paramMap) throws NkiaException {
		this.update("MenuDAO.updateMenuOrder", paramMap);
	}
	
	public void insertMenuInfo(ModelMap paramMap) throws NkiaException {
		this.insert("MenuDAO.insertMenuInfo", paramMap);
	}
	
	public int searchMenuGridListCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer) this.selectByPk("MenuDAO.searchMenuGridListCount", paramMap);
		return count;
	}
	
	public List searchMenuGridList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("MenuDAO.searchMenuGridList", paramMap);
		return resultList;
	}
	
	public List searchMenuAllId(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("MenuDAO.searchMenuAllId", paramMap);
		return resultList;
	}

	public List searchTopMenuForUser(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("MenuDAO.searchTopMenuForUser", paramMap);
		return resultList;
	}

	public Map selectTabMenuInfo(String url) throws NkiaException {
		return (Map)this.selectByPk("MenuDAO.selectTabMenuInfo", url);
	}

	public void updateMenuTree(ModelMap paramMap) throws NkiaException {
		this.update("MenuDAO.updateMenuTree", paramMap);
	}
}
