/*
 * @(#)MenuService.java              2013. 3. 6.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.menu.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface MenuService {
	
	public List searchAuthMappingMenu(ModelMap paramMap) throws NkiaException;
	
	public List searchMenuStore(ModelMap paramMap) throws NkiaException;
	
	public List searchStaticAllMenuTree(ModelMap paramMap) throws NkiaException;
	
	public HashMap selectMenuBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteMenuInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteMenuSystemAuth(ModelMap paramMap) throws NkiaException;
	
	public void updateMenuInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateMenuUseALL(ModelMap paramMap) throws NkiaException;
	
	public void updateMenuOrder(ModelMap paramMap) throws NkiaException;
	
	public void insertMenuInfo(ModelMap paramMap) throws NkiaException;
	
	public int searchMenuGridListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchMenuGridList(ModelMap paramMap) throws NkiaException;
	
	public List searchMenuAllId(ModelMap paramMap) throws NkiaException;
	
	public Map searchTopMenuForUser(Map param) throws NkiaException;

	public Map selectTabMenuInfo(String url) throws NkiaException;
	
	public void updateMenuTree(ModelMap paramMap) throws NkiaException;
}
