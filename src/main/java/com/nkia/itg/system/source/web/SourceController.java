/*
 * @(#)SourceController.java              2015. 5. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.source.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.source.service.SourceService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class SourceController {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "sourceService")
	private SourceService sourceService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/system/source/goSourceManager.do")
	public String goSourceManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/source/sourceManager.nvf";
		return forwarPage; 
	}
	
	@RequestMapping(value="/itg/system/source/doRefreshSqlMap.do")
	public @ResponseBody ResultVO doRefreshSqlMap(@RequestBody ModelMap paramMap)throws Exception {
		ResultVO resultVO = new ResultVO();
		try{
			sourceService.doRefreshSqlMap();
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/source/doReloadProperties.do")
	public @ResponseBody ResultVO doReloadProperties(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			sourceService.doReloadProperties();
			sourceService.doReloadEmailInfos();
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
