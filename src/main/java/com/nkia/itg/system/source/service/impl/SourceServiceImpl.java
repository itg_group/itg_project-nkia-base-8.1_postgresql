/*
 * @(#)SourceServiceImpl.java              2015. 5. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.source.service.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.config.NkiaApplicationEmailMap;
import com.nkia.itg.base.application.config.NkiaApplicationEmailTemplateMap;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.config.NkiaRuntimeContainer;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.reloadable.RefreshableSqlMapClientFactoryBean;
import com.nkia.itg.base.application.util.email.EmailEnum;
import com.nkia.itg.system.email.dao.EmailInfoDAO;
import com.nkia.itg.system.email.dao.EmailTemplateDAO;
import com.nkia.itg.system.source.service.SourceService;

/**
 * 
 * @version 1.0 2015. 6. 10.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * 소스관리 서비스 : Reloadble 등을 담당
 * </pre>
 *
 */
@Service("sourceService")
public class SourceServiceImpl implements SourceService {
	
	public static final Logger logger = LoggerFactory.getLogger(SourceServiceImpl.class);
	
	@Resource(name="nkiaRuntimeContainer")
	NkiaRuntimeContainer nkiaRuntimeContainer;
	
	@Resource(name = "emailInfoDAO")
	public EmailInfoDAO emailInfoDAO;
	
	@Resource(name = "emailTemplateDAO")
	public EmailTemplateDAO emailTemplateDAO;
	
	
	@Override
	public void doRefreshSqlMap() throws Exception {
		HashMap<String, Object> reloadableContainer = nkiaRuntimeContainer.getReloadableContainerMap();
		
		// SQL Container Object를 가져와서 Refresh 한다.
		Iterator<String> iter = reloadableContainer.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			Object container = nkiaRuntimeContainer.getReloadableContainerMapObject(key);
			if( container instanceof RefreshableSqlMapClientFactoryBean ){
				RefreshableSqlMapClientFactoryBean refreshableSqlMapClientFactoryBean = (RefreshableSqlMapClientFactoryBean)container;
				logger.info("Reloadable Factory Bean : [" + refreshableSqlMapClientFactoryBean + "]");
				refreshableSqlMapClientFactoryBean.refresh();
			}
		}
	}

	@Override
	public void doReloadProperties() throws NkiaException {
		NkiaApplicationPropertiesMap.loadProperty();
	}
	
	@Override
	public void doReloadEmailInfos() throws NkiaException {
		List infoList = emailInfoDAO.selectEmailSenderInfoList();
		if (infoList.size() > 0) {
			NkiaApplicationEmailMap.loadMailInfo();
		} else {
			String email_01 = EmailEnum.EMAIL_ID_1.getString();
			String email_01_data = NkiaApplicationEmailMap.getMailInfo(email_01);
			if (email_01_data != null) {
				System.clearProperty(email_01);
			}
			String email_02 = EmailEnum.EMAIL_ID_2.getString();
			String email_02_data = NkiaApplicationEmailMap.getMailInfo(email_02);
			if (email_02_data != null) {
				System.clearProperty(email_02);
			}
		}
		
		NkiaApplicationEmailTemplateMap.loadMailTemplateInfo();
	}
}
