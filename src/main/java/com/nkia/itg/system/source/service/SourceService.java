/*
 * @(#)SourceService.java              2015. 5. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.source.service;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SourceService {

	void doRefreshSqlMap() throws Exception;

	void doReloadProperties() throws NkiaException;
	
	void doReloadEmailInfos() throws NkiaException;
}
