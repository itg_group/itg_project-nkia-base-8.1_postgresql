package com.nkia.itg.system.customer.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.SessionService;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.customer.service.CustomerService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import net.sf.json.JSONObject;

@Controller
public class CustomerController {
	
	@Resource(name = "customerService")
	private CustomerService customerService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	@Resource(name = "sessionService")
	private SessionService sessionService;
	
	/**
	 * 부서관리페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/customer/goCustomerManager.do")
	public String customerManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/customer/customerManager.nvf";
		return forwarPage;
	}
		
	
	/**
	 * 부서관리트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/searchStaticAllCustomerTree.do")
	public @ResponseBody ResultVO searchStaticAllCustomerTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = null;
		
		try {
			if (paramMap.get("is_all_tree") != null && !paramMap.get("is_all_tree").equals("Y")) {
				loginUserId = userVO.getUser_id();
				paramMap.addAttribute("login_user_id", loginUserId);
			} else {
				paramMap.addAttribute("login_user_id", loginUserId);
			}

			paramMap.put("session_locale", sessionService.getSessionLocale());
			List resultList = customerService.searchStaticAllCustomerTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)(paramMap.get("expandLevel") != null? paramMap.get("expandLevel"): 0);
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서접근권한이 적용된 부서관리트리 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/customer/searchStaticAllCustomerTreeByUser.do")
	public @ResponseBody ResultVO searchStaticAllCustomerTreeByUser(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = null;
		
		try {
			if (userVO != null) {
				loginUserId = userVO.getUser_id();
				paramMap.addAttribute("login_user_id", loginUserId);
			} else {
				paramMap.addAttribute("login_user_id", loginUserId);
			}

			paramMap.put("session_locale", sessionService.getSessionLocale());
			List resultList = customerService.searchStaticAllCustomerTreeByUser(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)(paramMap.get("expandLevel") != null? paramMap.get("expandLevel"): 0);
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 컴퍼넌트용 부서관리트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/searchStaticAllCustomerTreeComp.do")
	public @ResponseBody ResultVO searchStaticAllCustomerTreeComp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = customerService.searchStaticAllCustomerTreeComp(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서정보 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/searchCustomerBasicInfo.do")
	public @ResponseBody ResultVO searchCustomerBasicInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = customerService.searchCustomerBasicInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 부서정보 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/deleteCustomerInfo.do")
	public @ResponseBody ResultVO deleteCustomerInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			customerService.deleteCustomerInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서정보 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/updateCustomerInfo.do")
	public @ResponseBody ResultVO updateCustomerInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			customerService.updateCustomerInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 부서정보 순서 수정
     * @param paramMap
	 * @return
	 * @
	 */
	/*@RequestMapping(value="/itg/system/customer/updateCustomerOrder.do")
	public @ResponseBody ResultVO updateCustomerOrder(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			customerService.updateCustomerOrder(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}*/
	
	/**
	 * 부서정보 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/insertCustomerInfo.do")
	public @ResponseBody ResultVO insertCustomerInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	
			customerService.insertCustomerInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위부서리스트
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/searchCustomerOrderList.do")
	public @ResponseBody ResultVO searchCustomerOrderList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			List resultList = customerService.searchCustomerOrderList(paramMap);		
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(resultList.size());
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 고객사ID검증
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/checkCustomerId.do")
	public @ResponseBody ResultVO selectCheckUserId(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			String checkValue = customerService.checkCustomerId(paramMap);
			resultVO.setResultString(checkValue);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 고객사 코드 목록 조회
     * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/system/customer/searchCustomerCodeList.do")
	public @ResponseBody ResultVO searchCustomerCodeList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();

		try{
			if(!StringUtils.isEmpty(paramMap.get("page"))) {
				PagingUtil.getFristEndNum(paramMap);
				int totalCount = customerService.searchCustomerCodeListCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			
			List resultList = customerService.searchCustomerCodeList(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 고객사 코드 목록 조회 (서비스수준관리>계약관리>계약대상설정 고객사등록팝업 전용 이원화)
     * @param paramMap
	 * @return
	 */
	@RequestMapping(value="/itg/system/customer/searchCustomerCodeView.do")
	public @ResponseBody ResultVO searchCustomerCodeView(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();

		try{
			if(!StringUtils.isEmpty(paramMap.get("page"))) {
				PagingUtil.getFristEndNum(paramMap);
				int totalCount = customerService.searchCustomerCodeViewCount(paramMap);
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			}
			
			List resultList = customerService.searchCustomerCodeView(paramMap);		
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 고객사 코드 조회 - 엑셀 다운로드
	 * @param paramMap
	 * @return
	 * @
	 */			
	@RequestMapping(value="/itg/system/customer/searchCustomerCodeListExcelDown.do")
    public @ResponseBody ResultVO searchCustomerCodeListExcelDown(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			ModelMap excelMap = new ModelMap();
			Map excelParam = (Map) paramMap.get("excelParam");
			Map param = (Map) paramMap.get("param");

			List resultList = customerService.searchCustomerCodeList(param);
			excelMap = excelMaker.makeExcelFile(excelParam, resultList);

			resultVO.setResultMap(excelMap);

		} catch (Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 사용자ID 별 고객사 코드목록 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/customer/searchCustomerCodeListByUserId.do")
	public @ResponseBody ResultVO searchSoftwareCodeDataListByUserId(@RequestBody ModelMap paramMap, @RequestParam Map<String, Object> reqMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("user_id", userVO.getUser_id());
	    	}
	    	
			gridVO.setRows(customerService.searchCustomerCodeListByUserId(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(NkiaException e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성
	/**
	 * EMS 리포트그룹 조회
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/customer/selectEmsReportGroupList.do")
	public @ResponseBody ResultVO selectEmsReportGroupList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			PagingUtil.getFristEndNum(paramMap);
			List resultList = customerService.selectEmsReportGroupList(paramMap);		
			int resultCount = customerService.selectEmsReportGroupCount(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setTotalCount(resultCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성 END
}
