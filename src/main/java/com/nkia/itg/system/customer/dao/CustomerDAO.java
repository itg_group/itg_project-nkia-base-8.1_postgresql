package com.nkia.itg.system.customer.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("customerDAO")
public class CustomerDAO extends EgovComAbstractDAO {

	public List searchStaticAllCustomerTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("CustomerDAO.searchStaticAllCustomerTree", paramMap);
		return resultList;
	}
	
	public List searchStaticAllCustomerTreeComp(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("CustomerDAO.searchStaticAllCustomerTreeComp", paramMap);
		return resultList;
	}
	
	public List searchStaticAllCustomerTreeByUser(ModelMap paramMap) throws NkiaException {
		return this.list("CustomerDAO.searchStaticAllCustomerTreeByUser", paramMap);
	}
	
	public HashMap searchCustomerBasicInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("CustomerDAO.searchCustomerBasicInfo", paramMap);
	}
	
	// 20200220_이원재_001_SYS_CUSTOMER 고객검색 서비스수준관리>계약관리>계약대상설정 전용으로 이원화
	public HashMap searchCustomerBasicView(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("CustomerDAO.searchCustomerBasicView", paramMap);
	}
	// 20200220_이원재_001_SYS_CUSTOMER 고객검색 서비스수준관리>계약관리>계약대상설정 전용으로 이원화 END
	
	public void deleteCustomerInfo(ModelMap paramMap) throws NkiaException {
		this.delete("CustomerDAO.deleteCustomerInfo", paramMap);
	}
	
	public void updateCustomerInfo(HashMap paramMap) throws NkiaException {
		this.update("CustomerDAO.updateCustomerInfo", paramMap);
	}
	
	public void updateCustomerUseAll(HashMap paramMap) throws NkiaException {
		this.update("CustomerDAO.updateCustomerUseAll", paramMap);
	}
	
	public void updateCustomerOrder(HashMap paramMap) throws NkiaException {
		this.update("CustomerDAO.updateCustomerOrder", paramMap);
	}
	
	public String insertCustomerInfo(ModelMap paramMap) throws NkiaException {
		return (String) this.insert("CustomerDAO.insertCustomerInfo", paramMap);
	}
	
	public int searchCustomerOrderListCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer) this.selectByPk("CustomerDAO.searchCustomerOrderListCount", paramMap);
		return count;
	}
	
	public List searchCustomerOrderList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("CustomerDAO.searchCustomerOrderList", paramMap);
		return resultList;
	}
	
	public List searchCustomerAllId(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("CustomerDAO.searchCustomerAllId", paramMap);
		return resultList;
	}
	
	public String checkCustomerId(ModelMap paramMap) throws NkiaException {
		return (String) this.selectByPk("CustomerDAO.checkCustomerId", paramMap);
	}

	public List searchCustomerCodeList(Map paramMap) throws NkiaException{
		return list("CustomerDAO.searchCustomerCodeList", paramMap);
	}
	
	public List searchCustomerCodeList(ModelMap paramMap) throws NkiaException{
		return list("CustomerDAO.searchCustomerCodeList", paramMap);
	}
	
	public int searchCustomerCodeListCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("CustomerDAO.searchCustomerCodeListCount", paramMap);
	}
	
	public List searchCustomerCodeListByUserId(Map paramMap) throws NkiaException{
		return list("CustomerDAO.searchCustomerCodeListByUserId", paramMap);
	}
	
	// 20200219_이원재_002_계약관리>계약대상설정 고객사등록팝업 이원화
	public List searchCustomerCodeView(Map paramMap) throws NkiaException{
		return list("CustomerDAO.searchCustomerCodeView", paramMap);
	}
	
	public int searchCustomerCodeViewCount(ModelMap paramMap)throws NkiaException {
		return (Integer)selectByPk("CustomerDAO.searchCustomerCodeViewCount", paramMap);
	}
	// 20200219_이원재_002_계약관리>계약대상설정 고객사등록팝업 이원화 END
	
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성
	public List selectEmsReportGroupList(Map paramMap) throws NkiaException {
		return list("CustomerDAO.selectEmsReportGroupList", paramMap);
	}
	
	public int selectEmsReportGroupCount(Map paramMap) throws NkiaException {
		return (Integer) selectByPk("CustomerDAO.selectEmsReportGroupCount", paramMap);
	}
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성 END
}
