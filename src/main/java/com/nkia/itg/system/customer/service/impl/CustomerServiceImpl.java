package com.nkia.itg.system.customer.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.customer.dao.CustomerDAO;
import com.nkia.itg.system.customer.service.CustomerService;


@Service("customerService")
public class CustomerServiceImpl implements CustomerService{
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	
	@Resource(name = "customerDAO")
	public CustomerDAO customerDAO;
	
	public List searchStaticAllCustomerTree(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchStaticAllCustomerTree(paramMap);
	}
	
	public List searchStaticAllCustomerTreeComp(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchStaticAllCustomerTreeComp(paramMap);
	}
	
	public List searchStaticAllCustomerTreeByUser(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchStaticAllCustomerTreeByUser(paramMap);
	}
	
	public HashMap searchCustomerBasicInfo(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchCustomerBasicInfo(paramMap);
	}
	
	public void deleteCustomerInfo(ModelMap paramMap) throws NkiaException {
		customerDAO.deleteCustomerInfo(paramMap);
	}
	
	public void updateCustomerInfo(ModelMap paramMap) throws NkiaException {
		HashMap customerInfo = (HashMap)paramMap.get("customerInfo");     //부서정보 업데이트
		List orderCustomerList = (List)paramMap.get("orderCustomerList");    //하위부서  리스트
		
		customerDAO.updateCustomerInfo(customerInfo);
		customerDAO.updateCustomerUseAll(customerInfo);
			
		//하위부서 순서변경
		if(orderCustomerList != null){
			
			for(int i = 0; i<orderCustomerList.size(); i++){
				HashMap orderCustomerData = (HashMap)orderCustomerList.get(i);
				
				orderCustomerData.put("customer_id", orderCustomerData.get("CUSTOMER_ID"));
				orderCustomerData.put("display_no", i+1);
				orderCustomerData.put("upd_user_id", paramMap.get("upd_user_id"));
				
				customerDAO.updateCustomerOrder(orderCustomerData);				
			}
		}
	}
	
	public void updateCustomerUseAll(ModelMap paramMap) throws NkiaException {
		customerDAO.updateCustomerUseAll(paramMap);
	}
	
	public void insertCustomerInfo(ModelMap paramMap) throws NkiaException {
		String custId = customerDAO.insertCustomerInfo(paramMap);
	}
	
	public int searchCustomerOrderListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return customerDAO.searchCustomerOrderListCount(paramMap);
	}
	
	public List searchCustomerOrderList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return customerDAO.searchCustomerOrderList(paramMap);
	}
	
	public List searchCustomerAllId(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchCustomerAllId(paramMap);
	}
	
	public String checkCustomerId(ModelMap paramMap) throws NkiaException {
		return customerDAO.checkCustomerId(paramMap);
	}

	public int searchCustomerCodeListCount(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchCustomerCodeListCount(paramMap);
	}
	
	public List searchCustomerCodeList(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchCustomerCodeList(paramMap);
	}
	
	public List searchCustomerCodeList(Map paramMap) throws NkiaException {
		return customerDAO.searchCustomerCodeList(paramMap);
	}
	
	public List searchCustomerCodeListByUserId(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchCustomerCodeListByUserId(paramMap);
	}
	
	// 20200219_이원재_001_계약관리>계약대상설정 고객사등록팝업 이원화
	public List searchCustomerCodeView(Map paramMap) throws NkiaException {
		return customerDAO.searchCustomerCodeView(paramMap);
	}
	
	public int searchCustomerCodeViewCount(ModelMap paramMap) throws NkiaException {
		return customerDAO.searchCustomerCodeViewCount(paramMap);
	}
	// 20200219_이원재_001_계약관리>계약대상설정 고객사등록팝업 이원화 END
	
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성
	public List selectEmsReportGroupList(Map paramMap) throws NkiaException {
		return customerDAO.selectEmsReportGroupList(paramMap);
	}
	
	public int selectEmsReportGroupCount(Map paramMap) throws NkiaException {
		return customerDAO.selectEmsReportGroupCount(paramMap);
	}
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성 END
}
