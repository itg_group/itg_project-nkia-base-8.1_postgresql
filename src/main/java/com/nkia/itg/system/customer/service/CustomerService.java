package com.nkia.itg.system.customer.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface CustomerService {
	
	public List searchStaticAllCustomerTree(ModelMap paramMap) throws NkiaException;
	
	public List searchStaticAllCustomerTreeComp(ModelMap paramMap) throws NkiaException;
	
	public List searchStaticAllCustomerTreeByUser(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchCustomerBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteCustomerInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchCustomerAllId(ModelMap paramMap) throws NkiaException;
	
	public void updateCustomerInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateCustomerUseAll(ModelMap paramMap) throws NkiaException;
	
	public void insertCustomerInfo(ModelMap paramMap) throws NkiaException;
	
	public int searchCustomerOrderListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchCustomerOrderList(ModelMap paramMap) throws NkiaException;
	
	public String checkCustomerId(ModelMap paramMap) throws NkiaException;
	
	public int searchCustomerCodeListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchCustomerCodeList(ModelMap paramMap) throws NkiaException;
	
	public List searchCustomerCodeList(Map paramMap) throws NkiaException;
	
	public List searchCustomerCodeListByUserId(ModelMap paramMap) throws NkiaException;
	
	// 20200219_이원재_002_계약관리>계약대상설정 고객사등록팝업 이원화
	public List searchCustomerCodeView(Map paramMap) throws NkiaException;
	
	public int searchCustomerCodeViewCount(ModelMap paramMap) throws NkiaException;
	// 20200219_이원재_002_계약관리>계약대상설정 고객사등록팝업 이원화 END
	
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성
	public List selectEmsReportGroupList(Map paramMap) throws NkiaException;
	
	public int selectEmsReportGroupCount(Map paramMap) throws NkiaException;
	// 20200325_이원재_002_월간보고서 조회 고객사 기준을 ITSM 에서 EMS 기준으로 변경하기 위한 조회쿼리 신규작성 END
}
