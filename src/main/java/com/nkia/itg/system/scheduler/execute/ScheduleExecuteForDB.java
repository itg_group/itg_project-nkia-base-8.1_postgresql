/*
 * @(#)ScheduleExecute.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.scheduler.execute;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.springframework.scheduling.quartz.CronTriggerBean;
import org.springframework.scheduling.quartz.SimpleTriggerBean;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.system.scheduler.service.SchedulerService;

public class ScheduleExecuteForDB {
	
	private static final String groupName = "ITG";
	private static final String executor = "SYSTEM";
	
	public void createSchedule()throws Exception{
		ModelMap paramMap = new ModelMap();
		SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");
		List scheduleList = schedulerService.searchScheduleList(paramMap);
		paramMap.put("use_yn", "Y");
		if( scheduleList != null && scheduleList.size() > 0 ){
			Scheduler scheduler = (Scheduler)NkiaApplicationContext.getCtx().getBean("scheduler");
			for( int i = 0; i < scheduleList.size(); i++ ){
				HashMap scheduleData = (HashMap)scheduleList.get(i);
				String scheduleId = (String)scheduleData.get("SCHEDULE_ID");
				String scheduleNm = (String)scheduleData.get("SCHEDULE_NM");
				String scheduleBeanName = (String)scheduleData.get("SCHEDULE_BEAN");
				String executeMethod = (String)scheduleData.get("EXECUTE_METHOD");
				String cronExpression = (String)scheduleData.get("CRONEXPRESSION");
				String scheduleConfig = (String)scheduleData.get("SCHEDULE_CONFIG");
				String scheduleType = (String)scheduleData.get("SCHEDULE_TYPE");
				String repeatInterval = (String)scheduleData.get("REPEAT_INTERVAL");
				//String serverHostName = (String)scheduleData.get("SERVER_HOSTNAME");
				
				String useYn = (String)scheduleData.get("USE_YN");
				
				// 등록된 Job과 트리거 삭제
				if("TRIGGER".equals(scheduleType)) {
					scheduler.unscheduleJob("TRIGGER_"+ scheduleId, "ITG");
				} else {
					scheduler.unscheduleJob("CRON_"+ scheduleId, "ITG");
				}
				scheduler.deleteJob(scheduleId, "ITG");
				
							
				//수행여부 'Y'인경우 스케줄 등록
				if("Y".equals(useYn)) {
					// Create JOB
					JobDetail jobDetail = new JobDetail();
			        // Class.newInstance()
					setJobClass(scheduleBeanName, jobDetail);
					
			        jobDetail.setName(scheduleId);
			        jobDetail.setGroup(groupName);
			        
			        JobDataMap jobDataMap = new JobDataMap();
			        jobDataMap.put("scheduleId", scheduleId);
			        jobDataMap.put("scheduleNm", scheduleNm);
			        jobDataMap.put("executeMethod", executeMethod);
			        jobDataMap.put("scheduleConfig", scheduleConfig);
			        jobDataMap.put("executor", executor);
			        jobDetail.setJobDataMap(jobDataMap);
			        
			        if("TRIGGER".equals(scheduleType)) {
			        	SimpleTriggerBean triggerBean = new SimpleTriggerBean();
			        	long repeatIntervallong = Long.parseLong(repeatInterval);//seconds
			        	triggerBean.setBeanName("TRIGGER_"+ scheduleId);
			        	triggerBean.setGroup(groupName);
			        	triggerBean.setRepeatInterval(repeatIntervallong);
			        	triggerBean.afterPropertiesSet();
				        
			        	scheduler.scheduleJob(jobDetail, triggerBean);
			        	/*
			        	//서버호스명과 스케줄잡에 등록된 서버호스트명이 동일한 경우 등록
			        	if(systemServerHostName.equals(serverHostName)) {
			        		scheduler.scheduleJob(jobDetail, triggerBean);
			        	}
			        	*/
			        } else {
			        	// Create CRON Trigger
				        CronTriggerBean cronTrigger = new CronTriggerBean();
				        cronTrigger.setBeanName("CRON_" + scheduleId);
				        cronTrigger.setGroup(groupName);	
				        // Execute of CronExpression
				        cronTrigger.setCronExpression(cronExpression);
				        cronTrigger.afterPropertiesSet();
				        
				        scheduler.scheduleJob(jobDetail, cronTrigger);
				        /*
				        //서버호스명과 스케줄잡에 등록된 서버호스트명이 동일한 경우 등록
				        if(systemServerHostName.equals(serverHostName)) {
				        	scheduler.scheduleJob(jobDetail, cronTrigger);
				        }
				        */
			        }
				}
			}
			
			// Start Scheduler        
	        scheduler.start();
		}
	}
	
	/**
	 * Job Class 설정
	 * 스케쥴 즉시 실행에서도 참조하여 사용.
	 * @param scheduleBeanName
	 * @param jobDetail
	 * @throws ClassNotFoundException 
	 */
	public void setJobClass(String scheduleBeanName, JobDetail jobDetail) throws ClassNotFoundException {
		try {
			Class clazz = Class.forName(scheduleBeanName);
	        jobDetail.setJobClass(clazz);
		} catch(ClassNotFoundException cnfe) {
			throw new ClassNotFoundException("ClassNotFoundException");
		}
		
		/*
		if("com.nkia.intergration.polestar.job.IairEAIJob".equalsIgnoreCase(scheduleBeanName)){
			jobDetail.setJobClass(IairEAIJob.class);
		} else if("com.nkia.itg.interfaces.job.PatrolDataEventJob".equalsIgnoreCase(scheduleBeanName)) {
			try {
				Class clazz = Class.forName(scheduleBeanName);
		        jobDetail.setJobClass(clazz);
			} catch(ClassNotFoundException cnfe) {
				
			}
			//jobDetail.setJobClass(PatrolDataEventJob.class);
		}
		*/
	}
}
