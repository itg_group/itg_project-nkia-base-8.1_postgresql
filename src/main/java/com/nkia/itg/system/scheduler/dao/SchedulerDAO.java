/*
 * @(#)SchedulerDAO.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.scheduler.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("schedulerDAO")
public class SchedulerDAO extends EgovComAbstractDAO{
	
	/**
	 * 
	 * 스케쥴러 목록 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchScheduleList(ModelMap paramMap) throws NkiaException {
		return list("SchedulerDAO.searchScheduleList", paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 실행 인덱스
	 * 
	 * @param scheduleId
	 * @return
	 * @throws NkiaException
	 */
	public int getSchedulerNextIndexById(String scheduleId)throws NkiaException {
		return (Integer)selectByPk("SchedulerDAO.getSchedulerNextIndexById", scheduleId);
	}
	
	/**
	 * 
	 * 스케쥴러 결과 초기 등록
	 * 
	 * @param scheduleMap
	 * @throws NkiaException
	 */
	public void insertSysSchedulerResult(HashMap scheduleMap) throws NkiaException {
		insert("SchedulerDAO.insertSysSchedulerResult", scheduleMap);
	}
	
	/**
	 * 
	 * 스케쥴러 결과 최종 등록(수정)
	 * 
	 * @param scheduleMap
	 * @throws NkiaException
	 */
	public void updateSysSchedulerResult(HashMap scheduleMap) throws NkiaException {
		update("SchedulerDAO.updateSysSchedulerResult", scheduleMap);
	}
	
	/**
	 * 
	 * 스케쥴러 결과 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSchedulerResultList(ModelMap paramMap) throws NkiaException {
		return list("SchedulerDAO.selectSchedulerResultList", paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 에러메세지 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectSchedulerResultErrorMsg(ModelMap paramMap) throws NkiaException {
		return  (HashMap) selectByPk("SchedulerDAO.selectSchedulerResultErrorMsg", paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 결과 리스트 총합
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectSchedulerResultListCount(ModelMap paramMap) throws NkiaException {
		return (Integer)selectByPk("SchedulerDAO.selectSchedulerResultListCount", paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 등록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertScheduler(ModelMap paramMap) throws NkiaException {
		insert("SchedulerDAO.insertScheduler", paramMap);
	}

	/**
	 * 
	 * 스케쥴러 수정
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateScheduler(ModelMap paramMap) throws NkiaException {
		update("SchedulerDAO.updateScheduler", paramMap);
	}

	/**
	 * 
	 * 스케쥴러 삭제
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteScheduler(ModelMap paramMap) throws NkiaException {
		delete("SchedulerDAO.deleteScheduler", paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 결과삭제
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteSchedulerResult(ModelMap paramMap) throws NkiaException {
		delete("SchedulerDAO.deleteSchedulerResult", paramMap);
	}

	
}
