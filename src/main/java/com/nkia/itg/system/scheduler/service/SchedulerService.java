/*
 * @(#)SchedulerService.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.scheduler.service;

import java.util.HashMap;
import java.util.List;

import org.quartz.SchedulerException;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SchedulerService {
	
	/**
	 * 
	 * 스케쥴러 목록 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchScheduleList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * 스케쥴러 실행 인덱스
	 * 
	 * @param scheduleId
	 * @return
	 * @throws NkiaException
	 */
	public int getSchedulerNextIndexById(String scheduleId) throws NkiaException;

	/**
	 * 
	 * 스케쥴러 결과 초기 등록
	 * 
	 * @param scheduleMap
	 * @throws NkiaException
	 */
	public void insertSysSchedulerResult(HashMap scheduleMap) throws NkiaException;

	/**
	 * 
	 * 스케쥴러 결과 최종 등록(수정)
	 * 
	 * @param scheduleMap
	 * @throws NkiaException
	 */
	public void updateSysSchedulerResult(HashMap scheduleMap) throws NkiaException;

	/**
	 * 
	 * 스케쥴러 즉시 실행
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 * @throws SchedulerException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws NoSuchMethodException 
	 */
	public void executeScheduler(ModelMap paramMap) throws NkiaException, SchedulerException, InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException;
	
	/**
	 * 
	 * 스케쥴러 결과 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectSchedulerResultList(ModelMap paramMap) throws NkiaException;

	/**
	 * 
	 * 스케쥴러 등록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void insertScheduler(ModelMap paramMap) throws NkiaException;

	/**
	 * 
	 * 스케쥴러 수정
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void updateScheduler(ModelMap paramMap) throws NkiaException;

	/**
	 * 
	 * 스케쥴러 삭제
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public void deleteScheduler(ModelMap paramMap) throws NkiaException;

	/**
	 * 
	 * 스케쥴러 결과 리스트 총합
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectSchedulerResultListCount(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * 스케쥴러 에러메세지 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectSchedulerResultErrorMsg(ModelMap paramMap) throws NkiaException;
}
