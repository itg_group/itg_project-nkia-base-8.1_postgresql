/*
 * @(#)ScheduleExecute.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.scheduler.execute;

import java.util.HashMap;
import java.util.List;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.springframework.scheduling.quartz.CronTriggerBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.system.scheduler.service.SchedulerService;

public class ScheduleExecute {
	
	public void createSchedule()throws Exception{
		ModelMap paramMap = new ModelMap();
		SchedulerService schedulerService = (SchedulerService) NkiaApplicationContext.getCtx().getBean("schedulerService");
		paramMap.put("use_yn", "Y");
		List scheduleList = schedulerService.searchScheduleList(paramMap);
		if( scheduleList != null && scheduleList.size() > 0 ){
			Scheduler scheduler = (Scheduler)NkiaApplicationContext.getCtx().getBean("scheduler");
			for( int i = 0; i < scheduleList.size(); i++ ){
				HashMap scheduleData = (HashMap)scheduleList.get(i);
				String scheduleId = (String)scheduleData.get("SCHEDULE_ID");
				String scheduleNm = (String)scheduleData.get("SCHEDULE_NM");
				String scheduleBeanName = (String)scheduleData.get("SCHEDULE_BEAN");
				String executeMethod = (String)scheduleData.get("EXECUTE_METHOD");
				String cronExpression = (String)scheduleData.get("CRONEXPRESSION");
				String scheduleConfig = (String)scheduleData.get("SCHEDULE_CONFIG");
				
				// Create JOB
		        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
		        // Class.newInstance()
		        jobDetail.setTargetObject(Class.forName(scheduleBeanName).newInstance());
				// Target Method
		        jobDetail.setTargetMethod(executeMethod);
		        jobDetail.setName(scheduleId);
		        jobDetail.setConcurrent(false);
		        jobDetail.afterPropertiesSet();
		        
		     // Job Arguments
		        String[] jobArgs = new String[4];
		        jobArgs[0] = scheduleId;		// 스케쥴ID
		        jobArgs[1] = scheduleNm;		// 스케쥴명
		        jobArgs[2] = scheduleConfig;	// 스케쥴설정 
		        jobArgs[3] = "SYSTEM";
		        jobDetail.setArguments(new Object[]{ jobArgs });

		        // Create CRON Trigger
		        CronTriggerBean cronTrigger = new CronTriggerBean();
		        cronTrigger.setBeanName("CRON_" + scheduleId);

		        // Execute of CronExpression
		        cronTrigger.setCronExpression(cronExpression);
		        cronTrigger.afterPropertiesSet();

		        scheduler.scheduleJob((JobDetail) jobDetail.getObject(), cronTrigger);
			}
			
			// Start Scheduler        
	        scheduler.start();
		}
	}
}
