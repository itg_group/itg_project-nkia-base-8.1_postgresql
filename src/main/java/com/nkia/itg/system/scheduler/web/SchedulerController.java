/*
 * @(#)SchedulerController.java              2014. 3. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.scheduler.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.quartz.SchedulerException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.system.scheduler.service.SchedulerService;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class SchedulerController {
	
	@Resource(name = "schedulerService")
	private SchedulerService schedulerService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	@RequestMapping(value="/itg/system/scheduler/goScheduler.do")
	public String goScheduler(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/scheduler/scheduler.nvf";
		return forwarPage;
	}
	
	/**
	 * 
	 * 스케쥴러 목록 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/scheduler/selectSchedulerList.do")
	public @ResponseBody ResultVO selectSchedulerList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			resultList = schedulerService.searchScheduleList(paramMap);		
			gridVO.setTotalCount(resultList.size());
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 스케쥴러 결과 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/scheduler/selectSchedulerResultList.do")
	public @ResponseBody ResultVO selectSchedulerResultList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			
			PagingUtil.getFristEndNum(paramMap);
			
			totalCount = schedulerService.selectSchedulerResultListCount(paramMap);
			resultList = schedulerService.selectSchedulerResultList(paramMap);	
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 스케쥴러 등록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/scheduler/insertScheduler.do")
	public @ResponseBody ResultVO insertScheduler(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			schedulerService.insertScheduler(paramMap);		
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 스케쥴러 수정
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/scheduler/updateScheduler.do")
	public @ResponseBody ResultVO updateScheduler(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			schedulerService.updateScheduler(paramMap);		
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 스케쥴러 삭제
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/scheduler/deleteScheduler.do")
	public @ResponseBody ResultVO deleteScheduler(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			schedulerService.deleteScheduler(paramMap);		
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 스케쥴러 즉시실행
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws SchedulerException 
	 * @throws NoSuchMethodException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	@RequestMapping(value="/itg/system/scheduler/executeScheduler.do")
	public @ResponseBody ResultVO executeScheduler(@RequestBody ModelMap paramMap)throws NkiaException, InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, SchedulerException {
		ResultVO resultVO = new ResultVO();
		try{
			schedulerService.executeScheduler(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 스케쥴러 결과 에러메세지 팝업
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */	
	@RequestMapping(value="/itg/system/scheduler/selectErrorMsgPop.do")
	public String selectErrorMsgPop(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
/*		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			HashMap resultList = new HashMap();
			String idx = request.getParameter("idx");
			String schedule_id = request.getParameter("schedule_id"); 
			paramMap.put("schedule_id", schedule_id);
			paramMap.put("idx", idx);
			
			resultList = schedulerService.selectSchedulerResultErrorMsg(paramMap);
			String errorMsg = (String) resultList.get("ERROR_MSG");
			//paramMap.addAttribute("errorMsg", errorMsg);
			
			//gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			resultVO.setResultString(errorMsg);
			
		}catch(NkiaException e) {
			throw new NkiaException(e);
		}*/
		String forwarPage = "/itg/system/scheduler/selectErrorMsgPop.nvf";
		return forwarPage;
	}	
}
