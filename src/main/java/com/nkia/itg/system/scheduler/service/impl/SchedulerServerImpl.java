/*
 * @(#)SchedulerServerImpl.java              2014. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.scheduler.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.scheduler.dao.SchedulerDAO;
import com.nkia.itg.system.scheduler.execute.ScheduleExecuteForDB;
import com.nkia.itg.system.scheduler.service.SchedulerService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("schedulerService")
public class SchedulerServerImpl implements SchedulerService{
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulerServerImpl.class);

	@Resource(name = "schedulerDAO")
	public SchedulerDAO schedulerDAO;
	
	/**
	 * 
	 * 스케쥴러 목록 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List searchScheduleList(ModelMap paramMap) throws NkiaException {
		return schedulerDAO.searchScheduleList(paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 실행 인덱스
	 * 
	 * @param scheduleId
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public int getSchedulerNextIndexById(String scheduleId) throws NkiaException {
		return schedulerDAO.getSchedulerNextIndexById(scheduleId);
	}
	
	/**
	 * 
	 * 스케쥴러 결과 초기 등록
	 * 
	 * @param scheduleMap
	 * @throws NkiaException
	 */
	@Override
	public void insertSysSchedulerResult(HashMap scheduleMap) throws NkiaException {
		schedulerDAO.insertSysSchedulerResult(scheduleMap);
	}
	
	/**
	 * 
	 * 스케쥴러 결과 최종 등록(수정)
	 * 
	 * @param scheduleMap
	 * @throws NkiaException
	 */
	@Override
	public void updateSysSchedulerResult(HashMap scheduleMap) throws NkiaException {
		schedulerDAO.updateSysSchedulerResult(scheduleMap);
	}
	
	/**
	 * 
	 * 스케쥴러 즉시실행
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws SchedulerException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws NoSuchMethodException 
	 */
	@Override
	public void executeScheduler(ModelMap paramMap) throws NkiaException, SchedulerException, InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException {
		Scheduler scheduler = (Scheduler)NkiaApplicationContext.getCtx().getBean("scheduler");
		String schedulerName = scheduler.getSchedulerName();
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		
		String scheduleId = (String)paramMap.get("schedule_id");
		String scheduleNm = (String)paramMap.get("schedule_nm");
		String scheduleBeanName = (String)paramMap.get("schedule_bean");
		String executeMethod = (String)paramMap.get("execute_method");
		String scheduleConfig = (String)paramMap.get("schedule_config");
		
		logger.info("EMMDIATE EXECUTE Start");
		scheduler.deleteJob("EMMDIATE_EXECUTE" + scheduleId, "DEFAULT");
		
		
		// Create CRON Trigger
        SimpleTrigger simpleTrigger = new SimpleTrigger();
        simpleTrigger.setName("SIMPLE" + scheduleId);
        simpleTrigger.setStartTime(new Date(System.currentTimeMillis() + 1000));
        simpleTrigger.setRepeatCount(0);
        simpleTrigger.setRepeatInterval(1000);
		
		if("MEMORY".equalsIgnoreCase(schedulerName)){
			// Create JOB
	        MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
	        // Class.newInstance()
	        jobDetail.setTargetObject(Class.forName(scheduleBeanName).newInstance());
			// Target Method
	        jobDetail.setTargetMethod(executeMethod);
	        jobDetail.setName("EMMDIATE_EXECUTE" + scheduleId);
	        jobDetail.setConcurrent(false);
	        jobDetail.afterPropertiesSet();
	        
	        // Job Arguments
	        String[] jobArgs = new String[4];
	        jobArgs[0] = scheduleId;		// 스케쥴ID
	        jobArgs[1] = scheduleNm;		// 스케쥴명
	        jobArgs[2] = scheduleConfig;	// 스케쥴설정 
	        
	        
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		jobArgs[3] = userVO.getUser_id();	// 실행자
	    	}else{
	    		jobArgs[3] = "Undefined User";
	    	}
	        jobDetail.setArguments(new Object[]{ jobArgs });
	        
	        scheduler.scheduleJob((JobDetail) jobDetail.getObject(), simpleTrigger);
		}else{
			ScheduleExecuteForDB scheduleExecuteForDB = new ScheduleExecuteForDB();
		
			// Create JOB
			JobDetail jobDetail = new JobDetail();
	        // Class.newInstance()
			scheduleExecuteForDB.setJobClass(scheduleBeanName, jobDetail);
			
	        jobDetail.setName("EMMDIATE_EXECUTE" + scheduleId);
	        
	        JobDataMap jobDataMap = new JobDataMap();
	        jobDataMap.put("scheduleId", scheduleId);
	        jobDataMap.put("scheduleNm", scheduleNm);
	        jobDataMap.put("executeMethod", executeMethod);
	        jobDataMap.put("scheduleConfig", scheduleConfig);
	        if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		jobDataMap.put("executor", userVO.getUser_id());
	    	}else{
	    		jobDataMap.put("executor", "Undefined User");
	    	}
	        jobDetail.setJobDataMap(jobDataMap);
	        
	        scheduler.scheduleJob(jobDetail, simpleTrigger);
		}
        logger.info("EMMDIATE EXECUTE End");
	}

	/**
	 * 
	 * 스케쥴러 결과 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectSchedulerResultList(ModelMap paramMap) throws NkiaException {
		return schedulerDAO.selectSchedulerResultList(paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 에러메세지 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public HashMap selectSchedulerResultErrorMsg(ModelMap paramMap) throws NkiaException {
		return schedulerDAO.selectSchedulerResultErrorMsg(paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 결과 리스트 총합
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public int selectSchedulerResultListCount(ModelMap paramMap) throws NkiaException {
		return schedulerDAO.selectSchedulerResultListCount(paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 등록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void insertScheduler(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
		schedulerDAO.insertScheduler(paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 수정
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void updateScheduler(ModelMap paramMap) throws NkiaException {
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(isAuthenticated) {
    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		paramMap.put("upd_user_id", userVO.getUser_id());
    	}
		schedulerDAO.updateScheduler(paramMap);
	}
	
	/**
	 * 
	 * 스케쥴러 삭제
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public void deleteScheduler(ModelMap paramMap) throws NkiaException {
		schedulerDAO.deleteSchedulerResult(paramMap);
		schedulerDAO.deleteScheduler(paramMap);
	}  

}
