package com.nkia.itg.system.dataset.util;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.util.common.StringUtil;

public class DatasetViewUtil {

	private static String indent = "\n    ";
	
	/**
	 * 조회영역 Ready 스크립트 생성
	 * 
	 * @param paramMap
	 */
	public static String createCondReadyScript(String view_mode, Map<String, Object> datasetMap, String condId, List<Map<String, Object>> itemList, Map<String, String> sessionMap, Map<String, String> optionMap) {
		StringBuffer sb = new StringBuffer();

		String fieldStr = "";
		String dataset_id = StringUtil.replaceNull(String.valueOf(datasetMap.get("DATASET_ID")), "");
		String ui_pattern_cd = StringUtil.replaceNull(String.valueOf(datasetMap.get("UI_PATTERN_CD")), "");
		String ui_type_cd = StringUtil.replaceNull(String.valueOf(datasetMap.get("UI_TYPE_CD")), "");

		String item_id = "";
		String cond_label = "";
		String cond_comp_type = "";
		String cond_code_grp_id = "";
		String cond_hidden_yn = "";
		String cond_required_yn = "";
		String cond_colspan = "";
		String cond_disable_yn = "";
		String cond_default_cvalue = "";
		String cond_op_nm = "";
		String cond_popup_id = "";
		
		String fields_hiddens = "dataset_id: '"+dataset_id+"', mode: 'selectList'";
		
		//sb.append(indent + "var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0}); ");
		//sb.append(indent + "var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0}); ");
		sb.append(indent + "fieldItems = []; ");
		Map<String, Object> item = null;
		for (int i = 0; i < itemList.size(); i++) {
			fieldStr = "";
			item = (Map<String, Object>) itemList.get(i);
			
			item_id = StringUtil.replaceNull(String.valueOf(item.get("ITEM_ID")), "");
			String item_id_lower = item_id.toLowerCase();
			cond_label = StringUtil.replaceNull(String.valueOf(item.get("COND_LABEL")), "");
			cond_comp_type = StringUtil.replaceNull(String.valueOf(item.get("COND_COMP_TYPE")), "");
			cond_code_grp_id = StringUtil.replaceNull(String.valueOf(item.get("COND_CODE_GRP_ID")), "");
			cond_hidden_yn = StringUtil.replaceNull(String.valueOf(item.get("COND_HIDDEN_YN")), "");
			cond_required_yn = StringUtil.replaceNull(String.valueOf(item.get("COND_REQUIRED_YN")), "");
			cond_colspan = StringUtil.replaceNull(String.valueOf(item.get("COND_COLSPAN")), "");
			cond_disable_yn = StringUtil.replaceNull(String.valueOf(item.get("COND_DISABLE_YN")), "");
			cond_default_cvalue = StringUtil.replaceNull(String.valueOf(item.get("COND_DEFAULT_CVALUE")), "");
			cond_op_nm = StringUtil.replaceNull(String.valueOf(item.get("COND_OP_CD_NM")), "");
			cond_popup_id = StringUtil.replaceNull(String.valueOf(item.get("COND_POPUP_ID")), "");
			
			if ("CHOICE_POPUP".equals(cond_comp_type) || "MULTI_POPUP".equals(cond_comp_type)) {
				fields_hiddens += ", " + item_id_lower + ": ''";
				cond_label += ("design".equals(view_mode)? "(" + cond_op_nm +")": "");
				sb.append(indent + "fieldItems.push({colspan: "+StringUtil.replaceNull(cond_colspan, "1")+", item: createUnionFieldComp({ ");
				sb.append(indent + "	items: [createSearchFieldComp({ label:'"+cond_label+"', name: '"+item_id_lower+"_nm', click:function(){ ");
				sb.append(indent + "					var param = {};  ");
				sb.append(indent + "					var callback_"+condId+"_"+item_id_lower+" = function(data){  ");
				sb.append(indent + "						if(data){ ");
				sb.append(indent + "							console.log(\">>> callback_"+condId+"_"+item_id_lower+" - data \", data); ");
				sb.append(indent + "							var paramMap = {}; ");
				sb.append(indent + "							paramMap['"+item_id_lower+"'] = data['ID']; ");
				sb.append(indent + "							paramMap['"+item_id_lower+"_nm'] = data['NAME']; ");
				sb.append(indent + "							$$('"+condId+"')._setValues(paramMap); ");
				sb.append(indent + "						} ");
				sb.append(indent + "					}; ");
				//sb.append(indent + "					nkia.ui.utils.window({ "); // 기존
				sb.append(indent + "					createDatasetViewWindowOpen({ "); // 이 함수안에서 조회ajax 후 nkia.ui.utils.window 호출한다.
				sb.append(indent + "						id: '"+condId+"_"+item_id_lower+"_pop', ");
				sb.append(indent + "						title: '"+cond_label+" 선택', ");
				sb.append(indent + "						type: 'SELPOPUP_CL', ");
				sb.append(indent + "						program_id: '"+cond_popup_id+"', ");
				sb.append(indent + "						width: 900, ");
				sb.append(indent + "						height: 750, ");
				sb.append(indent + "						checkbox: false, // checkbox: "+("MULTI_POPUP".equals(cond_comp_type)? "true": "false")+", ");
				sb.append(indent + "						params: param, ");
				sb.append(indent + "						reference: {form: $$('"+condId+"'), fields : {id: '"+item_id_lower+"', name: '"+item_id_lower+"_nm'}}, ");
				sb.append(indent + "						view_mode: '"+view_mode+"', ");
				sb.append(indent + "						callbackFunc: callback_"+condId+"_"+item_id_lower+", ");
				sb.append(indent + "						callFunc: createDatasetViewWindow ");
				sb.append(indent + "					}); ");
				sb.append(indent + "				}}), ");
				sb.append(indent + "			createBtnComp({ label: '초기화', click: function() { $$('"+condId+"')._setValues({"+item_id_lower+": '', "+item_id_lower+"_nm: ''}); }}) ");
				sb.append(indent + "		]})}); ");
			
			}else if ("SEARCH_ID_NAME".equals(cond_comp_type)) {
				sb.append(indent + "fieldItems.push({colspan: "+StringUtil.replaceNull(cond_colspan, "1")+", item: createUnionFieldComp({ ");
				sb.append(indent + "	items: [createCodeComboBoxComp({label: '검색', name:'search_type', code_grp_id: 'POPUP_SHOW_TYPE', value:'NAME', attachChoice:true }), ");
				sb.append(indent + "		createTextFieldComp({name:'search_value', on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);}}}), ");
				sb.append(indent + "		createBtnComp({label: '초기화', click: function() { $$('"+condId+"')._setValues({search_type:'NAME', search_value:'' }); }}) ");
				sb.append(indent + "	]})}); ");
			}else{
				fieldStr += "fieldItems.push({";
				fieldStr += "colspan: " + StringUtil.replaceNull(cond_colspan, "1");
				fieldStr += ", item:";
				// 컴포넌트 속성
				if ("TEXT".equals(cond_comp_type)) {
					fieldStr += " createTextFieldComp({";
//				}else if ("NUMBER".equals(cond_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("DOUBLE".equals(cond_comp_type)) {
//					fieldStr += " createTextFieldComp({";
				}else if ("COMBO".equals(cond_comp_type)) {
					fieldStr += " createCodeComboBoxComp({";
				}else if ("DATE".equals(cond_comp_type)) {
					fieldStr += " createDateFieldComp({";
				}else if ("RADIO".equals(cond_comp_type)) {
//					fieldStr += " createCodeRadioComp({";
					fieldStr += " createCodeComboBoxComp({";
//				}else if ("CHECKBOX".equals(cond_comp_type)) {
//					fieldStr += " createTextFieldComp({";
				}else if ("TEXTAREA".equals(cond_comp_type)) {
					fieldStr += " createTextAreaFieldComp({";
				}else if ("ATTACH_FILE".equals(cond_comp_type)) {
					fieldStr += " createAtchFileComp({";
				}else if ("PERIOD_DATE".equals(cond_comp_type)) {
					fieldStr += " createSearchDateFieldComp({";
//				}else if ("LABEL".equals(cond_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("PERIOD_NUMBER".equals(cond_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("PERIOD_TEXT".equals(cond_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("MULTI_COMBO".equals(cond_comp_type)) {
//					fieldStr += " createMultiSelectFieldComp({";
//				}else if ("MULTI_POPUP".equals(cond_comp_type)) {
//					fieldStr += " createUnionFieldComp({"; // createSearchFieldComp[팝업] + createBtnComp[초기화]
				}else{
					fieldStr += " createTextFieldComp({";
				}
				// 아이디속성
				//fieldStr += ", id: '" + condId + "_" + item_id + "'";
				// 이름속성
				fieldStr += "name: '" + item_id_lower + "'";
				// 라벨속성
				fieldStr += ", label: '" + cond_label + ("design".equals(view_mode)? "(" + cond_op_nm +")": "") + "'";
				// 기타속성설정
				if ("COMBO".equals(cond_comp_type) || "MULTI_COMBO".equals(cond_comp_type) || "RADIO".equals(cond_comp_type) || "CHECKBOX".equals(cond_comp_type)) {
					fieldStr += ", code_grp_id: '" + cond_code_grp_id + "'";
					fieldStr += ", attachAll: true";
				}else if ("TEXTAREA".equals(cond_comp_type)) {
					fieldStr += ", resizable: true, height: 100";
				}else if ("ATTACH_FILE".equals(cond_comp_type)) {
					fieldStr += ", register: '"+sessionMap.get("login_user_id")+"', height:100, allowFileExt: ['hwp','pdf','txt','pptx']";
				}else if ("DATE".equals(cond_comp_type)) {
					if("DATE_D".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':0, 'hour':0, 'min':0})";
					}else if("DATE_D-1".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':-1, 'hour':0, 'min':0})";
					}else if("DATE_D-7".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':-7, 'hour':0, 'min':0})";
					}else if("DATE_M-1".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':-30, 'hour':0, 'min':0})";
					}else if("DATE_Y-1".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':-1, 'month':0, 'day':0, 'hour':0, 'min':0})";
					}else{
						fieldStr += ", dateType:'D'";
					}
				}else if ("PERIOD_DATE".equals(cond_comp_type)) {
					if("FROM_D~TO_D".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_D-1~TO_D".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:-1, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_D-7~TO_D".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:-7, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_M-1~TO_D".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_Y-1~TO_D".equals(cond_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:-1, month:0, day:0, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else{
						fieldStr += ", dateType:'D'";
					}
				}
				// 기본값 속성
				if ("DATE".equals(cond_comp_type) || "PERIOD_DATE".equals(cond_comp_type)) { // 무시할 타입
					;
				}else{
					if ("login_user_id".equals(cond_default_cvalue)) {
						fieldStr += ", value: '" + sessionMap.get("login_user_id") + "'"; // 기본값
					} else if ("login_user_nm".equals(cond_default_cvalue)) {
						fieldStr += ", value: '" + sessionMap.get("login_user_nm") + "'"; // 기본값
					} else {
						fieldStr += ", value: '" + cond_default_cvalue + "'"; // 기본값
					}
				}
				// 필수 여부 속성
				if("Y".equals(cond_required_yn)){
					fieldStr += ", required: true"; // 필수 여부
				}
				// 숨김 여부 속성
				if("Y".equals(cond_hidden_yn)){
					fieldStr += ", visible: false"; // 숨김 여부
				}
				// disable 여부  속성
				if("Y".equals(cond_disable_yn)){
					fieldStr += ", readonly: true"; // disable 여부 
				}
				// enter 이벤트 속성
				if ("TEXT".equals(cond_comp_type) || "COMBO".equals(cond_comp_type) || "DATE".equals(cond_comp_type) || "TEXTAREA".equals(cond_comp_type)) {
					fieldStr += ", on:{ onKeyPress: function(keycode) { pressEnterKey"+condId+"(keycode);}}"; // Enter 시 자동조회
				}
				
				fieldStr += "})});";
				
				// 설계자용 메시지
				if("design".equals(view_mode)){
					fieldStr += " // item_id:" + item_id_lower;
					if(!"".equals(cond_comp_type)) fieldStr += ", cond_comp_type:" + cond_comp_type;
					if(!"".equals(cond_label)) fieldStr += ", cond_label:" + cond_label;
					if(!"".equals(cond_colspan)) fieldStr += ", cond_colspan:" + cond_colspan;
					if(!"".equals(cond_required_yn)) fieldStr += ", cond_required_yn:" + cond_required_yn;
					if(!"".equals(cond_code_grp_id)) fieldStr += ", cond_code_grp_id:" + cond_code_grp_id;
					if(!"".equals(cond_popup_id)) fieldStr += ", cond_popup_id:" + cond_popup_id;
				}
				if (0 < fieldStr.length()) {
					 sb.append(indent + "" + fieldStr);
				}
			}
		}
		
		sb.append(indent + "");
		sb.append(indent + "buttonItems = []; ");
		sb.append(indent + "buttonItems.push(createBtnComp({ id:'"+condId+"SearchBtn', label: '조회', type: 'form', click: function() { actionLink('search"+condId+"');}})); // [검색] ");
		sb.append(indent + "buttonItems.push(createBtnComp({ id:'"+condId+"ResetBtn', label: '초기화', click: function() { actionLink('reset"+condId+"Data');}})); // [초기화] ");
		sb.append(indent + "");
		sb.append(indent + ""+condId+" = createFormComp({ ");
		sb.append(indent + "	id: '"+condId+"', ");
		sb.append(indent + "	elementsConfig: { labelPosition: 'left', labelWidth:130 }, ");
		sb.append(indent + "	header: { ");
		sb.append(indent + "		title: '조회"+("design".equals(view_mode)? "(id:"+condId+", "+ui_type_cd+"_"+ui_pattern_cd +")": "")+"', ");
		sb.append(indent + "		icon: 'search' ");
		sb.append(indent + "	}, ");
		sb.append(indent + "	footer: {buttons: { align: 'center', items: buttonItems, css: 'webix_layout_form_bottom' }}, ");
		sb.append(indent + "	fields: { colSize:2, items: fieldItems, ");
		sb.append(indent + "		hiddens: {"+fields_hiddens+"} "); // {selectList, exceldown} 
		sb.append(indent + "	} ");
		sb.append(indent + "}); ");
		return sb.toString();
	}

	/**
	 * 조회영역 Form Init 스크립트 생성
	 * 
	 * @param paramMap
	 */
	public static String createCondFormInitScript(String view_mode, Map<String, Object> datasetMap, String condId, List<Map<String, Object>> itemList, Map<String, String> sessionMap, Map<String, String> optionMap) {
		StringBuffer sb = new StringBuffer();

		String item_id = "";
		String item_nm = "";
		String cond_hidden_yn = "";
		String cond_disable_yn = "";
		
		String hideFields = "";
		String hideFields_nm = "";
		String setControl = "";
		String setControl_nm = "";
		
		Map<String, Object> item = null;
		for (int i = 0; i < itemList.size(); i++) {
			item = (Map<String, Object>) itemList.get(i);

			item_id = StringUtil.replaceNull(String.valueOf(item.get("ITEM_ID")), "");
			String item_id_lower = item_id.toLowerCase();
			cond_hidden_yn = StringUtil.replaceNull(String.valueOf(item.get("COND_HIDDEN_YN")), ""); 
			cond_disable_yn = StringUtil.replaceNull(String.valueOf(item.get("COND_DISABLE_YN")), "");

			if("Y".equals(cond_hidden_yn)){
				hideFields += (0 < hideFields.length()? ",": "");
				hideFields += "'" + item_id_lower + "'";
				hideFields_nm += (0 < hideFields_nm.length()? ",": "");
				hideFields_nm += item_nm;
			}
			if("Y".equals(cond_disable_yn)){
				setControl += (0 < setControl.length()? ",": "");
				setControl += "'" + item_id_lower + "'";
				setControl_nm += (0 < setControl_nm.length()? ",": "");
				setControl_nm += item_nm;
			}
		}
		
		// 순서1
		if(0 < setControl.length()){
			sb.append(indent + "$$('"+condId+"')._setControl({editorMode:true, exceptions: ["+setControl+"]}); // " + setControl_nm);
		}
		// 순서2
		if(0 < hideFields.length()){
			sb.append(indent + "$$('"+condId+"')._hideFields("+hideFields+", true); // " + hideFields_nm);
		}
		return sb.toString();
	}
	
	/**
	 * 목록영역 Ready 스크립트 생성
	 * 
	 * @param paramMap
	 */
	public static String createGridReadyScript(String view_mode, Map<String, Object> datasetMap, String listId, List<Map<String, Object>> itemList, Map<String, String> sessionMap, Map<String, String> optionMap) {
		StringBuffer sb = new StringBuffer();
		
		String dataset_id = StringUtil.replaceNull(String.valueOf(datasetMap.get("DATASET_ID")), "");

		String item_id = "";
		String list_key_yn = "";

		String keys = "";
		
		Map<String, Object> item = null;
		for (int i = 0; i < itemList.size(); i++) {
			item = (Map<String, Object>) itemList.get(i);

			item_id = StringUtil.replaceNull(String.valueOf(item.get("ITEM_ID")), "");
			list_key_yn = StringUtil.replaceNull(String.valueOf(item.get("LIST_KEY_YN")), "");
			
			if("".equals(keys)){
				if("Y".equals(list_key_yn)){
					keys = item_id;
				}
			}
		}
		
		sb.append(indent + "buttonItems = []; ");
		sb.append(indent + "buttonItems.push(createBtnComp({ id:'"+listId+"ExcelDownloadBtn', label: '엑셀다운로드', type: 'form', width: 110, visible:true, click: function() { actionLink('"+listId+"ExcelDown'); } }) ); ");
		sb.append(indent + " ");
		sb.append(indent + ""+listId+" = createGridComp({ ");
		sb.append(indent + "	id: '"+listId+"', ");
		sb.append(indent + "	keys: ['" + keys + "'], ");
		sb.append(indent + "	resizeColumn: true, ");
		sb.append(indent + "	pageable: true, ");
		sb.append(indent + "	checkbox: false, ");
		sb.append(indent + "	pageSize: 10, ");
		if(!"".equals(StringUtil.replaceNull(String.valueOf(optionMap.get("grid_height")), ""))){
			sb.append(indent + "	height: "+optionMap.get("grid_height")+", ");
		}
		sb.append(indent + "	autoLoad :false, // 자동조회 안함 ");
		sb.append(indent + "	url: '/itg/system/datasetview/searchList.do', ");
		sb.append(indent + "	resource: 'grid.system.dataset', ");
		sb.append(indent + "	header: { title: '목록 "+("design".equals(view_mode)? "(id:"+listId+")": "")+"', buttons: { items: buttonItems } }, ");
		sb.append(indent + "	params : {dataset_id: '"+dataset_id+"'}, ");
		sb.append(indent + "	on: {onItemClick: "+listId+"CellClick, onItemDblClick: "+listId+"CellDbClick } ");
		sb.append(indent + "});	 ");
		return sb.toString();
	}
	
	/**
	 * 목록영역 엑셀다운로드 스크립트 생성
	 * 
	 * @param paramMap
	 */
	public static String createGridExcelDownloadScript(String view_mode, Map<String, Object> datasetMap, String condId, List<Map<String, Object>> itemList, Map<String, String> optionMap) {
		StringBuffer sb = new StringBuffer();

		String dataset_id = StringUtil.replaceNull(String.valueOf(datasetMap.get("DATASET_ID")), "");
		String dataset_nm = StringUtil.replaceNull(String.valueOf(datasetMap.get("DATASET_NM")), "");

		String item_id = "";
		String item_nm = "";
		
		String select_cols = "";
		String select_txts = "";
		
		Map<String, Object> item = null;
		for (int i = 0; i < itemList.size(); i++) {
			item = (Map<String, Object>) itemList.get(i);

			item_id = StringUtil.replaceNull(String.valueOf(item.get("ITEM_ID")), "");
			item_nm = StringUtil.replaceNull(String.valueOf(item.get("LIST_LABEL")), "");
			select_cols += (0<i?",":"") + item_id;
			select_txts += (0<i?",":"") + item_nm;
		}
		
		dataset_nm = dataset_nm.replace("*", "").replace("@", "").replace("$", "").replace("%", ""); // 특수문자제거
		
		sb.append(indent + "var paramMap = $$('"+condId+"')._getValues(); ");
		sb.append(indent + "paramMap['mode'] = 'exceldown'; ");
		sb.append(indent + "paramMap['dataset_id'] = '"+dataset_id+"'; ");
		sb.append(indent + "var excelParam = {}; ");
		sb.append(indent + "excelParam['fileName'] 		= '" + dataset_nm + "'; "); // 엑셀파일명
		sb.append(indent + "excelParam['sheetName'] 		= '" + dataset_nm + "'; "); // Sheet0 이름
		sb.append(indent + "excelParam['titleName'] 		= '" + dataset_nm + "'; "); // 제목
		sb.append(indent + "excelParam['includeColumns'] 	= '" + select_cols + "'; "); // 다운로드엑셀파업 ID 열
		sb.append(indent + "excelParam['headerNames'] 	= '" + select_txts + "'; "); // 다운로드엑셀파업 이름 열
		sb.append(indent + "excelParam['defaultColumns'] 	= '" + select_cols + "'; ");
		sb.append(indent + "excelParam['url'] 			= '/itg/system/datasetview/searchList.do'; ");
		sb.append(indent + "nkia.ui.utils.window({id: 'assetExcelPop', type: 'Dynamic', param: paramMap, excelParam: excelParam, title: '엑셀다운로드', callFunc: createExcelTabWindow }); ");
		// sb.append(indent + "} ");
		return sb.toString();
	}

	/**
	 * 폼영역 Ready 스크립트 생성
	 * 
	 * @param paramMap
	 */
	public static String createFormReadyScript(String view_mode, Map<String, Object> datasetMap, String formId, List<Map<String, Object>> itemList, Map<String, String> sessionMap, Map<String, String> optionMap) {
		StringBuffer sb = new StringBuffer();

		String fieldStr = "";

		String item_id = "";
		String form_label = "";
		String form_comp_type = "";
		String form_code_grp_id = "";
		String form_hidden_yn = "";
		String form_required_yn = "";
		String form_colspan = "";
		String form_disable_yn = "";
		String form_default_cvalue = "";
		String form_popup_id = "";
		
		String fields_hiddens = "";
		
		sb.append(indent + "buttonItems = []; ");
		sb.append(indent + "buttonItems.push(createBtnComp({ id:'"+formId+"SaveBtn', label: '저장', type: 'form',  ");
		sb.append(indent + "	click: function() { ");
		sb.append(indent + "		if($$('"+formId+"')._validate() != true){ ");
		sb.append(indent + "			return false; ");
		sb.append(indent + "		} ");
		sb.append(indent + "		var paramMap = $$('"+formId+"')._getValues(); ");
		sb.append(indent + "		fn_"+formId+"Save('S', nkia.ui.utils.copyObj(paramMap)); ");
		sb.append(indent + "	}})); ");
		sb.append(indent + "fieldItems = []; ");
		
		Map<String, Object> item = null;
		for (int i = 0; i < itemList.size(); i++) {
			fieldStr = "";
			item = (Map<String, Object>) itemList.get(i);

			item_id = StringUtil.replaceNull(String.valueOf(item.get("ITEM_ID")), "");
			String item_id_lower = item_id.toLowerCase();
			
			form_label = StringUtil.replaceNull(String.valueOf(item.get("FORM_LABEL")), "");
			form_comp_type = StringUtil.replaceNull(String.valueOf(item.get("FORM_COMP_TYPE")), "");
			form_code_grp_id = StringUtil.replaceNull(String.valueOf(item.get("FORM_CODE_GRP_ID")), "");
			form_hidden_yn = StringUtil.replaceNull(String.valueOf(item.get("FORM_HIDDEN_YN")), "");
			form_required_yn = StringUtil.replaceNull(String.valueOf(item.get("FORM_REQUIRED_YN")), "");
			form_colspan = StringUtil.replaceNull(String.valueOf(item.get("FORM_COLSPAN")), "");
			form_disable_yn = StringUtil.replaceNull(String.valueOf(item.get("FORM_DISABLE_YN")), "");
			form_default_cvalue = StringUtil.replaceNull(String.valueOf(item.get("FORM_DEFAULT_CVALUE")), "");
			form_popup_id = StringUtil.replaceNull(String.valueOf(item.get("FORM_POPUP_ID")), "");
			
			if ("CHOICE_POPUP__".equals(form_comp_type) || "MULTI_POPUP".equals(form_comp_type)) {
				fields_hiddens += ", "+item_id_lower+": ''";
				
				sb.append(indent + "fieldItems.push({colspan: "+StringUtil.replaceNull(form_colspan, "1")+", item: createUnionFieldComp({ ");
				sb.append(indent + "	items: [createSearchFieldComp({label:'"+form_label+"', name: '"+item_id_lower+"_nm', click:function(){ ");
				sb.append(indent + "					var param = {popup_id: '"+form_popup_id+"'};  ");
				sb.append(indent + "					var callback_"+formId+"_"+item_id_lower+" = function(data){  ");
				sb.append(indent + "						if(data){ ");
				sb.append(indent + "							var paramMap = {}; ");
				sb.append(indent + "							paramMap['"+item_id_lower+"'] = data['ID']; ");
				sb.append(indent + "							paramMap['"+item_id_lower+"_nm'] = data['NAME']; ");
				sb.append(indent + "							$$('"+formId+"')._setValues(paramMap); ");
				sb.append(indent + "						} ");
				sb.append(indent + "					}; ");
				//sb.append(indent + "					nkia.ui.utils.window({ ");
				sb.append(indent + "					createDatasetViewWindowOpen({ ");
				sb.append(indent + "						id: '"+formId+"_"+item_id_lower+"_pop', ");
				sb.append(indent + "						title: '"+form_label+" 선택', ");
				sb.append(indent + "						type: '', ");
				sb.append(indent + "						width: 900, ");
				sb.append(indent + "						height: 750, ");
				sb.append(indent + "						checkbox: false, // checkbox: "+("MULTI_POPUP".equals(form_comp_type)? "true": "false")+", ");
				sb.append(indent + "						popupParamData: param, ");
				sb.append(indent + "						reference: {form: $$('"+formId+"'), fields : {id: '"+item_id_lower+"', name: '"+item_id_lower+"_nm'}}, ");
				sb.append(indent + "						view_mode: '"+view_mode+"', ");
				sb.append(indent + "						callbackFunc: callback_"+formId+"_"+item_id_lower+", ");
				sb.append(indent + "						callFunc: createDatasetPopWindow ");
				sb.append(indent + "					}); ");
				sb.append(indent + "				}}), ");
				sb.append(indent + "				createBtnComp({label: '초기화', click: function() { $$('"+formId+"')._setValues({"+item_id_lower+": '', "+item_id_lower+"_nm: ''}); }}) ");
				sb.append(indent + "			]})}); ");
			}else{
				fieldStr += "fieldItems.push({";
				fieldStr += "colspan: " + StringUtil.replaceNull(form_colspan, "1");
				fieldStr += ", item:";
				
				// 컴포넌트 속성
				if ("TEXT".equals(form_comp_type)) {
					fieldStr += " createTextFieldComp({";
//				}else if ("NUMBER".equals(form_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("DOUBLE".equals(form_comp_type)) {
//					fieldStr += " createTextFieldComp({";
				}else if ("COMBO".equals(form_comp_type)) {
					fieldStr += " createCodeComboBoxComp({";
				}else if ("DATE".equals(form_comp_type)) {
					fieldStr += " createDateFieldComp({";
				}else if ("RADIO".equals(form_comp_type)) {
//					fieldStr += " createCodeRadioComp({";
					fieldStr += " createCodeComboBoxComp({";
//				}else if ("CHECKBOX".equals(form_comp_type)) {
//					fieldStr += " createTextFieldComp({";
				}else if ("TEXTAREA".equals(form_comp_type)) {
					fieldStr += " createTextAreaFieldComp({";
				}else if ("ATTACH_FILE".equals(form_comp_type)) {
					fieldStr += " createAtchFileComp({";
				}else if ("PERIOD_DATE".equals(form_comp_type)) { 
					fieldStr += " createSearchDateFieldComp({";
//				}else if ("LABEL".equals(form_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("PERIOD_NUMBER".equals(form_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("PERIOD_TEXT".equals(form_comp_type)) {
//					fieldStr += " createTextFieldComp({";
//				}else if ("MULTI_COMBO".equals(form_comp_type)) {
//					fieldStr += " createMultiSelectFieldComp({";
//				}else if ("MULTI_POPUP".equals(form_comp_type)) {
//					fieldStr += " createUnionFieldComp({"; // createSearchFieldComp[팝업] + createBtnComp[초기화]
//				}else if ("SEARCH_ID_NAME".equals(form_comp_type)) {
//					fieldStr += " createUnionFieldComp({"; // createCodeComboBoxComp[ID,NAME] + createTextFieldComp[]
				}else{
					fieldStr += " createTextFieldComp({";
				}
				// 아이디속성
				//fieldStr += ", id: '" + formId + "_" + item_id + "'";
				// 이름속성
				fieldStr += "name: '" + item_id_lower + "'";
				// 라벨속성
				fieldStr += ", label: '" + form_label + "'";
				// 기타속성설정
				if ("COMBO".equals(form_comp_type) || "MULTI_COMBO".equals(form_comp_type) || "RADIO".equals(form_comp_type) || "CHECKBOX".equals(form_comp_type)) {
					fieldStr += ", code_grp_id: '" + form_code_grp_id + "'";
					fieldStr += ", attachAll: true";
				}else if ("DATE".equals(form_comp_type)) {
					if("DATE_D".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':0, 'hour':0, 'min':0})";
					}else if("DATE_D-1".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':-1, 'hour':0, 'min':0})";
					}else if("DATE_D-7".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':-7, 'hour':0, 'min':0})";
					}else if("DATE_M-1".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':0, 'month':0, 'day':-30, 'hour':0, 'min':0})";
					}else if("DATE_Y-1".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', dateValue: createDateTimeStamp('DHM', {'year':-1, 'month':0, 'day':0, 'hour':0, 'min':0})";
					}else{
						fieldStr += ", dateType:'D'";
					}
				}else if ("PERIOD_DATE".equals(form_comp_type)) {
					if("FROM_D~TO_D".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_D-1~TO_D".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:-1, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_D-7~TO_D".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:-7, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_M-1~TO_D".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else if("FROM_Y-1~TO_D".equals(form_default_cvalue)){
						fieldStr += ", dateType:'D', start_dateValue: createDateTimeStamp('DHM', {year:-1, month:0, day:0, hour:0, min:0}), end_dateValue: createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})";
					}else{
						fieldStr += ", dateType:'D'";
					}
				}else if ("TEXTAREA".equals(form_comp_type)) {
					fieldStr += ", resizable: true, height: 100";
				}else if ("ATTACH_FILE".equals(form_comp_type)) {
					fieldStr += ", register: '"+sessionMap.get("login_user_id")+"', height:100, allowFileExt: ['hwp','pdf','txt','pptx']";
				}
				
				// 기본값 속성
				if ("DATE".equals(form_comp_type) || "PERIOD_DATE".equals(form_comp_type)) {
					;
				}else{
					if ("login_user_id".equals(form_default_cvalue)) {
						fieldStr += ", value: '" + sessionMap.get("login_user_id") + "'"; // 기본값
					} else if ("login_user_nm".equals(form_default_cvalue)) {
						fieldStr += ", value: '" + sessionMap.get("login_user_nm") + "'"; // 기본값
					} else {
						fieldStr += ", value: '" + form_default_cvalue + "'"; // 기본값
					}
				}
				// 필수 여부 속성
				if("Y".equals(form_required_yn)){
					fieldStr += ", required: true"; // 필수 여부
				}
				// 숨김 여부속성
				if("Y".equals(form_hidden_yn)){
					fieldStr += ", visible: false"; // 숨김 여부
				}
				// disable 여부  속성
				if("Y".equals(form_disable_yn)){
					fieldStr += ", readonly: true"; // disable 여부 
				}
				fieldStr += "})});";
				
				// 설계자용 메시지
				if("design".equals(view_mode)){
					fieldStr += " // item_id:" + item_id_lower;
					if(!"".equals(form_comp_type)) fieldStr += ", form_comp_type:" + form_comp_type;
					if(!"".equals(form_label)) fieldStr += ", form_label:" + form_label;
					if(!"".equals(form_colspan)) fieldStr += ", form_colspan:" + form_colspan;
					if(!"".equals(form_required_yn)) fieldStr += ", form_required_yn:" + form_required_yn;
					if(!"".equals(form_code_grp_id)) fieldStr += ", form_code_grp_id:" + form_code_grp_id;
					if(!"".equals(form_popup_id)) fieldStr += ", form_popup_id:" + form_popup_id;
				}
				if (0 < fieldStr.length()) {
					 sb.append(indent + "" + fieldStr);
				}
			}
		}
		sb.append(indent + "");
		sb.append(indent + ""+formId+" = createFormComp({ ");
		sb.append(indent + "	id: '"+formId+"', ");
		sb.append(indent + "	//elementsConfig: {labelPosition: 'left'}, ");
		sb.append(indent + "	header: { ");
		sb.append(indent + "		title: '상세" + ("design".equals(view_mode)? "(id:"+formId+")": "")+"', ");
		sb.append(indent + "		icon: 'form', ");
		sb.append(indent + "	}, ");
		sb.append(indent + "	footer: {buttons: {align: 'center', items: buttonItems, css: 'webix_layout_form_bottom' } }, ");
		sb.append(indent + "	fields: {colSize:2, items: fieldItems, ");
		sb.append(indent + "		hiddens: {"+fields_hiddens+"} ");
		sb.append(indent + "	} ");
		sb.append(indent + "}); ");
		return sb.toString();
	}

	/**
	 * 폼영역 Page Init 스크립트 생성
	 * 
	 * @param paramMap
	 */
	public static String createFormPageInitScript(String view_mode, Map<String, Object> datasetMap, String formId, List<Map<String, Object>> itemList, Map<String, String> sessionMap, Map<String, String> optionMap) {
		StringBuffer sb = new StringBuffer();
		
		String item_id = "";
		String item_nm = "";
		String form_hidden_yn = "";
		String form_disable_yn = "";
		
		String hideFields = "";
		String hideFields_nm = "";
		String setControl = "";
		String setControl_nm = "";
		
		Map<String, Object> item = null;
		for (int i = 0; i < itemList.size(); i++) {
			item = (Map<String, Object>) itemList.get(i);

			item_id = StringUtil.replaceNull(String.valueOf(item.get("ITEM_ID")), "");
			String item_id_lower = item_id.toLowerCase();
			item_nm = StringUtil.replaceNull(String.valueOf(item.get("ITEM_NM")), "");
			form_hidden_yn = StringUtil.replaceNull(String.valueOf(item.get("FORM_HIDDEN_YN")), "");
			form_disable_yn = StringUtil.replaceNull(String.valueOf(item.get("FORM_DISABLE_YN")), "");
			
			if("Y".equals(form_hidden_yn)){
				hideFields += (0 < hideFields.length()? ",": "");
				hideFields += "'" + item_id_lower + "'";
				hideFields_nm += (0 < hideFields_nm.length()? ",": "");
				hideFields_nm += item_nm;
			}
			if("Y".equals(form_disable_yn)){
				setControl += (0 < setControl.length()? ",": "");
				setControl += "'" + item_id_lower + "'";
				setControl_nm += (0 < setControl_nm.length()? ",": "");
				setControl_nm += item_nm;
			}
		}
		
		// 순서1
		if(0 < setControl.length()){
			sb.append(indent + "$$('"+formId+"')._setControl({editorMode:true, exceptions: ["+setControl+"]}); // " + setControl_nm);
		}
		// 순서2
		if(0 < hideFields.length()){
			sb.append(indent + "$$('"+formId+"')._hideFields("+hideFields+", true); // " + hideFields_nm);
		}
		return sb.toString();
	}
	
	/**
	 * 목록영역 Form Init 스크립트 생성
	 * 
	 * @param paramMap
	 */
	public static String createGridFormInitScript(String view_mode, Map<String, Object> datasetMap, String gridId, List<Map<String, Object>> itemList, Map<String, String> optionMap) {
		StringBuffer sb = new StringBuffer();

		String item_id = "";
		String item_nm = "";
		String list_width = "";
		String list_flex = "";
		String list_sortable = "";
		String list_xtype = "";
		String list_align = "";

		Map<String, Object> item = null;
		for (int i = 0; i < itemList.size(); i++) {
			item = (Map<String, Object>) itemList.get(i);
			item_id += (0 < i ? "," : "") + item.get("ITEM_ID");
			item_nm += (0 < i ? "," : "") + item.get("LIST_LABEL");
			list_width += (0 < i ? "," : "") + item.get("LIST_WIDTH");
			list_flex += (0 < i ? "," : "") + item.get("LIST_FLEX").toString().toLowerCase();
			list_sortable += (0 < i ? "," : "") + item.get("LIST_SORTABLE").toString().toLowerCase();
			list_xtype += (0 < i ? "," : "") + ("design" == view_mode? item.get("LIST_XTYPE").toString().toLowerCase(): "na");
			list_align += (0 < i ? "," : "") + item.get("LIST_ALIGN").toString().toLowerCase();
		}

		sb.append(indent + "var header_column = { ");
		sb.append(indent + "	gridHeader: '" + item_id + "', ");
		sb.append(indent + "	gridText: '" + item_nm + "', ");
		sb.append(indent + "	gridWidth: '" + list_width + "', ");
		sb.append(indent + "	gridFlex: '" + list_flex + "', ");
		sb.append(indent + "	gridSortable: '" + list_sortable + "', ");
		sb.append(indent + "	gridXtype: '" + list_xtype + "', ");
		sb.append(indent + "	gridAlign: '" + list_align + "' ");
		sb.append(indent + "} ");
		sb.append(indent + "$$('" + gridId + "')._updateHeader(header_column); ");
		sb.append(indent + "$$('" + gridId + "').refresh(); ");
		return sb.toString();
	}

}
