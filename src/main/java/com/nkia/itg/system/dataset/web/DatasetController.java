/*
 * @(#)DatasetController.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.system.dataset.service.DatasetService;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.NamedParamService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class DatasetController {

	private static final Logger logger = LoggerFactory.getLogger(DatasetController.class);

	@Resource(name = "datasetService")
	private DatasetService datasetService;

	@Resource(name = "namedParamService")
	private NamedParamService namedParamService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	/**
	 * 통합현황관리 화면 오픈
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/goDatasetManager.do")
	public String goDatasetManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/dataset/datasetManager.nvf";

		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		// 로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);

		// 로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);

		return forwarPage;
	}

	/**
	 * 통합현황관리 화면 오픈
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/goDatasetSetCLD.do")
	public String goDatasetSetCLD(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/dataset/datasetSetCLD.nvf";

		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		// 로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);

		// 로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);

		return forwarPage;
	}

	/**
	 * 통합현황조회 화면 오픈
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/goDatasetList.do")
	public String goDatasetList(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/dataset/datasetList.nvf";

		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		// 로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);

		// 로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);

		return forwarPage;
	}
	
	/**
	 * 통합현황조회 화면 오픈
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/goDatasetList2.do")
	public String goDatasetList2(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/dataset/datasetList2.nvf";

		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		// 로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);

		// 로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);

		return forwarPage;
	}

	/**
	 * 통합현황조회 화면 오픈
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/goDatasetList3.do")
	public String goDatasetList3(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/dataset/datasetList3.nvf";

		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		// 로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);

		// 로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);

		return forwarPage;
	}
	
	/**************
	 * SYS_DATASET CRUD
	 **************/

	/**
	 * 통합현황 목록출력
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/searchDatasetList.do")
	public @ResponseBody
	ResultVO searchDatasetList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);

			List<Map<String, Object>> resultList = datasetService.selectDatasetList(paramMap);
			totalCount = datasetService.selectDatasetCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);

			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 통합현황 등록
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/insertDataset.do")
	public @ResponseBody
	ResultVO insertDataset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Map<String, Object> resultMap = datasetService.insertDataset(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 통합현황 수정
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/updateDataset.do")
	public @ResponseBody
	ResultVO updateDataset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			datasetService.updateDataset(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 통합현황 삭제
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/deleteDataset.do")
	public @ResponseBody
	ResultVO deleteDataset(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			datasetService.deleteDataset(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성자동생성
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/itemAutoCreate.do")
	public @ResponseBody
	ResultVO itemAutoCreate(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			if (null == paramMap.get("dataset_id") || "".equals(paramMap.get("dataset_id"))) {
				throw new NkiaException("현황아이디는 필수입력입니다.");
			}
			datasetService.itemAutoCreate(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성 목록조회
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/searchDatasetItemList.do")
	public @ResponseBody
	ResultVO searchDatasetItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = datasetService.selectDatasetItemList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 속성 등록
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/insertDatasetItem.do")
	public @ResponseBody
	ResultVO insertDatasetItem(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			datasetService.insertDatasetItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성 수정
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/updateDatasetItem.do")
	public @ResponseBody
	ResultVO updateDatasetItem(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			datasetService.updateDatasetItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성 순번수정
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/updateDatasetItemDispNo.do")
	public @ResponseBody
	ResultVO updateDatasetItemDispNo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List<Map<String, Object>> input1List = (List<Map<String, Object>>) paramMap.get("INPUT1_LIST");
			datasetService.updateDatasetItemDispNo(input1List);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성 삭제
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/deleteDatasetItem.do")
	public @ResponseBody
	ResultVO deleteDatasetItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			datasetService.deleteDatasetItem(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성 목록삭제
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/deleteDatasetItemList.do")
	public @ResponseBody
	ResultVO deleteDatasetItemListList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List<Map<String, Object>> input1List = (List<Map<String, Object>>) paramMap.get("INPUT1_LIST");
			datasetService.deleteDatasetItemList(input1List);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성 목록저장
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/saveDatasetItemList.do")
	public @ResponseBody
	ResultVO saveDatasetItemList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			datasetService.saveDatasetItemList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch (NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 코드정보조회
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/searchCodeDataList.do")
	public @ResponseBody
	ResultVO searchCodeDataList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			gridVO.setRows(datasetService.searchCodeDataList(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 부서 목록조회
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/selectDatasetDataauthList.do")
	public @ResponseBody
	ResultVO selectDatasetDataauthList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			List resultList = datasetService.selectDatasetDataauthList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 코드그룹 목록출력
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/searchCodeGrpList.do")
	public @ResponseBody
	ResultVO searchCodeGrpList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);

			List<Map<String, Object>> resultList = datasetService.selectCodeGrpList(paramMap);
			totalCount = datasetService.selectCodeGrpCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);

			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 코드그룹 목록출력
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/searchBizPopupList.do")
	public @ResponseBody
	ResultVO searchPopupList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);

			List<Map<String, Object>> resultList = datasetService.selectPopupList(paramMap);
			totalCount = datasetService.selectPopupCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);

			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 기본값 목록출력
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/dataset/searchDefaultValueList.do")
	public @ResponseBody
	ResultVO searchDefaultValueList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);

			List<Map<String, Object>> resultList = datasetService.selectDefaultValueList(paramMap);
			totalCount = datasetService.selectDefaultValueCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);

			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 통합조회현황 동적리스트 출력
	 * 
	 * @param paramMap
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value = "/itg/system/dataset/searchDynamicList.do")
	public @ResponseBody
	ResultVO searchDynamicList(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		Map<String, Object> param = null;
		Map<String, Object> excelParam = null;
		Map<String, Object> mPs = null;
		try {
			if (null != paramMap.get("param")) {
				param = (Map<String, Object>) paramMap.get("param");
				excelParam = (Map<String, Object>) paramMap.get("excelParam");
			} else {
				PagingUtil.getFristEndNum(paramMap);
				param = paramMap;
			}
			if (null == param.get("mode") || "".equals(param.get("mode"))) { // {selectList, exceldown} 
				throw new NkiaException("처리모드정보는 필수입력입니다.");
			} else if (null == param.get("dataset_id") || "".equals(param.get("dataset_id"))) {
				throw new NkiaException("현황아이디는 필수입력입니다.");
			} else if (null == param.get("select_cols") || "".equals(param.get("select_cols"))) {
				throw new NkiaException("조회컬럼정보는 필수입력입니다.");
			}
			
			String mode = StringUtil.replaceNull(String.valueOf(param.get("mode")), ""); // 수행모드 {selectList, exceldown}
			String dataset_id = StringUtil.replaceNull(String.valueOf(param.get("dataset_id")), ""); // 현황 아이디
			String select_cols = StringUtil.replaceNull(String.valueOf(param.get("select_cols")), ""); // 조회컬럼정보
			String filter_col_nm = StringUtil.replaceNull(String.valueOf(param.get("filter_col_nm")), ""); // Filter 컬럼명
			String filter_col_op = StringUtil.replaceNull(String.valueOf(param.get("filter_col_op")), ""); // Filter 조건
			String filter_col_value = StringUtil.replaceNull(String.valueOf(param.get("filter_col_value")), ""); // Filter 조회값

			GridVO gridVO = new GridVO();
			int totalCount = 0;
			String query = "";
			StringBuffer querySb = null;

			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			Map<String, Object> datasetMap = datasetService.selectDataset(mPs);

			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			mPs.put("cond_yn", "Y");
			List<Map<String, Object>> itemList = datasetService.selectDatasetItemList(mPs);
			// logger.debug(">> itemList : " + itemList);

			String sBAS_SQL = StringUtil.replaceNull(String.valueOf(datasetMap.get("BAS_SQL")), "");
			String sORDER_SQL = StringUtil.replaceNull(String.valueOf(datasetMap.get("ORDER_SQL")), "");

			// logger.debug(">>>>> paramMap : " + paramMap);
			// logger.debug(">>>>> param : " + param);
			// logger.debug(">>>>> excelParam : " + excelParam);
			// logger.debug(">>>>> >>>>>>>>>>>>>>>>>>>>>>");
			// logger.debug(">>>>> mode : " + mode);
			// logger.debug(">>>>> dataset_id : " + dataset_id);
			// logger.debug(">>>>> select_cols : " + select_cols);
			// logger.debug(">>>>> filter_col_nm : " + filter_col_nm);
			// logger.debug(">>>>> filter_col_op : " + filter_col_op);
			// logger.debug(">>>>> filter_col_value : " + filter_col_value);
			// logger.debug(">>>>> >>>>>>>>>>>>>>>>>>>>>>");
			// logger.debug(">>>>> sBAS_SQL : " + sBAS_SQL);
			// logger.debug(">>>>> sORDER_SQL : " + sORDER_SQL);
			// logger.debug(">>>>> >>>>>>>>>>>>>>>>>>>>>>");
			// logger.debug(">>>>> datasetMap : " + datasetMap);
			// logger.debug(">>>>> itemList : " + itemList);

			/*******************
			 * 기본 SQL 생성
			 ******************/
			mPs = new HashMap<String, Object>();
			querySb = new StringBuffer();
			querySb.append("                SELECT " + select_cols + " ");
			querySb.append("\n                   FROM (");
			querySb.append("\n" + sBAS_SQL);
			
			// WHERE 설정
			String filterSqls = "";
			String sITEM_ID = "";
			String sCOND_SQL = "";
			String filtSql = "";
			String[] arr_filter_col_nm = filter_col_nm.split("\\^");
			String[] arr_filter_col_op = filter_col_op.split("\\^");
			String[] arr_filter_col_value = filter_col_value.split("\\^");
			
			for (int i = 0; i < arr_filter_col_nm.length; i++) {
				if (!"".equals(arr_filter_col_nm[i]) && !"".equals(arr_filter_col_op[i]) && !"".equals(arr_filter_col_value[i])) {
					for (Map<String, Object> item : itemList) {
						sITEM_ID = StringUtil.replaceNull(String.valueOf(item.get("ITEM_ID")), "");
						sCOND_SQL = StringUtil.replaceNull(String.valueOf(item.get("COND_SQL")), "");
						if (arr_filter_col_nm[i].equals(sITEM_ID.toUpperCase()) && !"".equals(sCOND_SQL)) {
							filtSql = StringUtil.replaceNull(String.valueOf(item.get("COND_SQL")), "");
							if (arr_filter_col_op[i].equals("EQUAL")) { // = :ITEM_ID
								filtSql = filtSql.replace("[OP1]", " = ").replace("[OP2]", "");
							} else if (arr_filter_col_op[i].equals("GREATER")) { // < :ITEM_ID
								filtSql = filtSql.replace("[OP1]", " < ").replace("[OP2]", "");
							} else if (arr_filter_col_op[i].equals("LESS")) { // > :ITEM_ID
								filtSql = filtSql.replace("[OP1]", " > ").replace("[OP2]", "");
							} else if (arr_filter_col_op[i].equals("GREATEREQUAL")) { // =< :ITEM_ID
								filtSql = filtSql.replace("[OP1]", " <= ").replace("[OP2]", "");
							} else if (arr_filter_col_op[i].equals("LESSEQUAL")) { // => :ITEM_ID
								filtSql = filtSql.replace("[OP1]", " >= ").replace("[OP2]", "");
							} else if (arr_filter_col_op[i].equals("LIKE")) { // LIKE '%' || :ITEM_ID || '%'
								filtSql = filtSql.replace("[OP1]", " LIKE '%' || ").replace("[OP2]", " || '%'");
							} else if (arr_filter_col_op[i].equals("LIKEL")) { // LIKE '%' || :ITEM_ID
								filtSql = filtSql.replace("[OP1]", " LIKE '%' || ").replace("[OP2]", " ");
							} else if (arr_filter_col_op[i].equals("LIKER")) { // LIKE :ITEM_ID || '%'
								filtSql = filtSql.replace("[OP1]", " LIKE ").replace("[OP2]", " || '%'");
							} else if (arr_filter_col_op[i].equals("IN")) { // IN (:ITEM_ID)
								filtSql = filtSql.replace("[OP1]", " IN (").replace("[OP2]", ")");
							} else if (arr_filter_col_op[i].equals("BETWEEN")) { // BETWEEN :ITEM_ID AND :ITEM_ID2
								filtSql = filtSql.replace("[OP1]", " BETWEEN").replace("[OP2]", " AND :" + arr_filter_col_nm[i] + "ZZZZ2");
							} else {
								filtSql = filtSql.replace("[OP1]", " = ").replace("[OP2]", ""); // 기본 = :ITEM_ID
							}
							if (arr_filter_col_op[i].equals("BETWEEN")) {
								String value = arr_filter_col_value[i];
								String[] arrValue = value.split(",");
								mPs.put(arr_filter_col_nm[i].toLowerCase(), (0 < arrValue.length ? arrValue[0] : ""));
								mPs.put(arr_filter_col_nm[i].toLowerCase() + "ZZZZ2", (1 < arrValue.length ? arrValue[1] : arrValue[0]));
							} else if (arr_filter_col_op[i].equals("IN")) { // 보류
								String value = arr_filter_col_value[i];
								String value2 = "'" + value.replaceAll(",", "','") + "'";
								filtSql = filtSql.replace(":" + arr_filter_col_nm[i], value2);
							} else {
								mPs.put(arr_filter_col_nm[i].toLowerCase(), arr_filter_col_value[i]);
							}
							filterSqls += "\n   AND " + filtSql + " /* filter" + i + " - " + arr_filter_col_nm[i] + " - " + arr_filter_col_op[i] + " */ ";
						}
					}
				}
			}
			// if("".equals(filterSqls)){
			// throw new NkiaException("조건정보를 존재하지 않습니다.");
			// }
			querySb.append(sORDER_SQL);
			querySb.append("\n                     ) XX1 ");
			querySb.append("\n                 WHERE 1=1 ");
			querySb.append("              " + filterSqls);

			if ("selectList".equals(mode)) {

				/*******************
				 * selectList-1) Count 조회한다.
				 ******************/
				String query2 = "";
				query2 += "\n           SELECT /*  /itg/system/dataset/searchDynamicList.do - selectCount */ ";
				query2 += "\n                  COUNT(1) AS CNT ";
				query2 += "\n             FROM ( ";
				query2 += "\n" + querySb.toString();
				query2 += "\n                ) XX2 ";
				query = query2;
				Map<String, Object> countMap = namedParamService.selectMap(query, mPs);
				totalCount = Integer.parseInt(String.valueOf(countMap.get("CNT")));

				/*******************
				 * selectList-2) ROWNUM 구하고 정렬한다
				 ******************/
				String query3 = "";
				if (!"".equals(sORDER_SQL)) {
					query3 += "           SELECT ROW_NUMBER() OVER(ORDER BY " + sORDER_SQL + ") AS RNUM ";
				} else {
					query3 += "           SELECT ROWNUM AS RNUM ";
				}
				query3 += "\n                , XX2.* ";
				query3 += "\n            FROM ( ";
				query3 += "\n" + querySb.toString();
				query3 += "\n                ) XX2 ";
				query3 += "\n         ORDER BY RNUM";
				query = query3;

				/*******************
				 * selectList-3) 페이징 적용한다.
				 ******************/
				if (!"".equals(StringUtil.replaceNull(String.valueOf(param.get("start")), ""))) {
					String query4 = "";
					query4 += "\n  SELECT /*  /itg/system/dataset/searchDynamicList.do - selectList */ ";
					query4 += "\n         XX3.*  ";
					query4 += "\n    FROM (   ";
					query4 += "\n" + query3;
					query4 += "\n       ) XX3 ";
					int page = (Integer) param.get("page");
					int limit = (Integer) param.get("limit");
					int start = (Integer) param.get("start");
					int end = limit * page;
					query4 += "\n  WHERE XX3.RNUM BETWEEN :first_num AND :last_num ";
					mPs.put("first_num", start + 1);
					mPs.put("last_num", end);
					query = query4;
				}
				List<Map<String, Object>> resultList = namedParamService.selectList(query, mPs);

				/*******************
				 * selectList-4) 결과조회
				 ******************/
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer) param.get(BaseEnum.PAGER_START_PARAM.getString()));
				gridVO.setRows(resultList);
				resultVO.setGridVO(gridVO);

			} else if ("exceldown".equals(mode)) {

				/*******************
				 * exceldown-1) 엑셀다운로드 한다.
				 ******************/
				query = querySb.toString();
				List<Map<String, Object>> resultList = namedParamService.selectList(query, mPs);
				ModelMap excelMap = excelMaker.makeExcelFile(excelParam, resultList);
				resultVO.setResultMap(excelMap);
			} else {
				throw new NkiaException("넘겨받은 mode 유형을 처리할 수 없습니다. [" + mode + "]");
			}
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

}
