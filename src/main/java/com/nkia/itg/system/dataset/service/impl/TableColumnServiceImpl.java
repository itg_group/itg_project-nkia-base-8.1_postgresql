/*
 * @(#)TableColumnServiceImpl.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.system.dataset.dao.TableColumnDAO;
import com.nkia.itg.system.dataset.service.TableColumnService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.dao.NamedParamDAO;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("tableColumnService")
public class TableColumnServiceImpl implements TableColumnService{
	
	private static final Logger logger = LoggerFactory.getLogger(TableColumnServiceImpl.class);
	
	@Resource(name = "tableColumnDAO")
	public TableColumnDAO tableColumnDAO;
	
	@Resource(name="namedParamDAO")
	private NamedParamDAO namedParamDAO;
	
	/**
	 * 테이블 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectTableList(Map<String, Object> paramMap) throws NkiaException {
		return tableColumnDAO.selectTableList(paramMap);
	}

	/**
	 * 테이블 건수조회
	 * @param paramMap
	 */
	public int selectTableCount(Map<String, Object> paramMap) throws NkiaException {
		return tableColumnDAO.selectTableCount(paramMap);
	}

	/**
	 * 테이블컬럼 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectTableColumn(Map<String, Object> paramMap) throws NkiaException {
		return tableColumnDAO.selectTableColumn(paramMap);
	}

	/**
	 * 테이블컬럼 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectTableColumnList(Map<String, Object> paramMap) throws NkiaException {
		return tableColumnDAO.selectTableColumnList(paramMap);
	}

	/**
	 * 테이블컬럼 등록
	 * @param paramMap
	 */
	public void insertTableColumn(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		paramMap.put("login_user_id", login_user_id);
		tableColumnDAO.insertTableColumn(paramMap); // INSERT SYS_DATASET 
	}
	
	/**
	 * 테이블컬럼 수정
	 * @param paramMap
	 */
	public void updateTableColumn(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		paramMap.put("login_user_id", login_user_id);
		tableColumnDAO.updateTableColumn(paramMap); // UPDATE SYS_DATASET 
	}

	/**
	 * 테이블컬럼 삭제
	 * @param paramMap
	 */
	public void deleteTableColumn(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		paramMap.put("login_user_id", login_user_id);
		Map<String, Object> detailMap = tableColumnDAO.selectTableColumn(paramMap);
		if("Y".equals(detailMap.get("AUTO_UPD_YN"))){
			paramMap.put("remarks", "");
			paramMap.put("user_table_comnt", "");
			paramMap.put("user_column_comnt", "");
			//paramMap.put("user_comp_type", "");
			//paramMap.put("user_code_grp_id", "");
			//paramMap.put("user_popup_id", "");
			tableColumnDAO.updateTableColumn(paramMap); // UPDATE SYS_DATASET_ITEM 
		}else{
			tableColumnDAO.deleteTableColumn(paramMap); // DELETE SYS_DATASET_ITEM 
		}
	}

	/**
	 * 테이블컬럼 목록삭제
	 * @param paramMap
	 */
	public Map<String, Object> deleteTableColumnList(List<Map<String, Object>> list) throws NkiaException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		int updCnt = 0;
		int delCnt = 0;
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		for(Map<String, Object> map: list){
			map.put("login_user_id", login_user_id);
			Map<String, Object> detailMap = tableColumnDAO.selectTableColumn(map);
			if("Y".equals(detailMap.get("AUTO_UPD_YN"))){
				map.put("remarks", "");
				map.put("user_table_comnt", "");
				map.put("user_column_comnt", "");
				//map.put("user_comp_type", "");
				//map.put("user_code_grp_id", "");
				//map.put("user_poopup_id", "");
				//tableColumnDAO.updateTableColumn(map); // UPDATE SYS_DATASET_ITEM 
				updCnt++;
			}else{
				tableColumnDAO.deleteTableColumn(map); // DELETE SYS_DATASET_ITEM 
				delCnt++;
			}
		}
		resultMap.put("updCnt", updCnt);
		resultMap.put("delCnt", delCnt);
		return resultMap;
	}

	/**
	 * 테이블컬럼 등록
	 * @param paramMap
	 */
	public Map<String, Object> insertTableColumnInfoWithSchedule(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String query = "";
		StringBuffer querySb = new StringBuffer();
		Map<String, Object> mPs = null;
		resultMap.put("RESULT_CD", ""); // default
		resultMap.put("RESULT_MSG", ""); // default
		
		String scheduleId = StringUtil.replaceNull(String.valueOf(paramMap.get("scheduleId")), "");
		//String scheduleNm = StringUtil.replaceNull(String.valueOf(paramMap.get("scheduleNm")), "");
		//String scheduleConfig = StringUtil.replaceNull(String.valueOf(paramMap.get("scheduleConfig")), "");
		String login_user_id = StringUtil.replaceNull(String.valueOf(paramMap.get("login_user_id")), "");
		
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		mPs = new HashMap<String, Object>();		
		querySb = new StringBuffer();
		querySb.append("\n UPDATE SYS_TABLE_COLUMN_INFO ");
		querySb.append("\n    SET AUTO_UPD_YN = NULL ");
		namedParamDAO.insert(querySb.toString(), mPs);
		
		querySb = new StringBuffer();
		querySb.append("\n MERGE INTO SYS_TABLE_COLUMN_INFO A /* scheduleId:"+ scheduleId +" */ ");
		querySb.append("\n      USING (  SELECT A.TABLE_NAME AS TABLE_NM ");
		querySb.append("\n                    , A.COLUMN_NAME AS COLUMN_NM ");
		querySb.append("\n                    , A.TABLE_COMMENTS AS TABLE_COMNT ");
		querySb.append("\n                    , A.COLUMN_COMMENTS AS COLUMN_COMNT ");
		querySb.append("\n                    , A.DATA_TYPE AS DATA_TYPE ");
		querySb.append("\n                    , A.DATA_LENGTH AS DATA_LENGTH ");
		querySb.append("\n                    , CASE WHEN P.POSITION < 99 THEN 'Y' ELSE ' ' END AS PK_YN ");
		querySb.append("\n                    , (CASE WHEN A.NULLABLE = 'Y' THEN '' ELSE 'Y' END) AS NOT_NULL_YN ");
		querySb.append("\n                    , A.COLUMN_ID AS COLUMN_SEQ ");
		querySb.append("\n                    , A.OWNER AS OWNER_NM ");
		querySb.append("\n                 FROM (SELECT /* 기본정보 */ C.OWNER ");
		querySb.append("\n                            , C.TABLE_NAME ");
		querySb.append("\n                            , C.COLUMN_NAME ");
		querySb.append("\n                            , C.DATA_TYPE ");
		querySb.append("\n                            , C.DATA_LENGTH ");
		querySb.append("\n                            , C.NULLABLE ");
		querySb.append("\n                            , C.COLUMN_ID ");
		querySb.append("\n                            , CNM.COMMENTS AS COLUMN_COMMENTS ");
		querySb.append("\n                            , TNM.COMMENTS AS TABLE_COMMENTS ");
		querySb.append("\n                         FROM ALL_TAB_COLUMNS C ");
		querySb.append("\n                            , ALL_TAB_COMMENTS TNM ");
		querySb.append("\n                            , ALL_COL_COMMENTS CNM ");
		querySb.append("\n                        WHERE TNM.OWNER = C.OWNER ");
		querySb.append("\n                          AND TNM.TABLE_NAME = C.TABLE_NAME ");
		querySb.append("\n                          AND CNM.OWNER = C.OWNER ");
		querySb.append("\n                          AND CNM.TABLE_NAME = C.TABLE_NAME ");
		querySb.append("\n                          AND CNM.COLUMN_NAME = C.COLUMN_NAME ");
		querySb.append("\n                          AND C.OWNER = :owner ");
		querySb.append("\n                          AND EXISTS(SELECT 1 FROM ALL_TABLES EX WHERE EX.OWNER = C.OWNER AND EX.TABLE_NAME = C.TABLE_NAME) ");
		querySb.append("\n                      ) A ");
		querySb.append("\n                    , (SELECT /* PK정보 */ C.OWNER ");
		querySb.append("\n                            , C.TABLE_NAME ");
		querySb.append("\n                            , C.COLUMN_NAME ");
		querySb.append("\n                            , C.POSITION ");
		querySb.append("\n                         FROM ALL_CONS_COLUMNS C ");
		querySb.append("\n                            , ALL_CONSTRAINTS S ");
		querySb.append("\n                        WHERE S.OWNER = C.OWNER ");
		querySb.append("\n                          AND S.CONSTRAINT_NAME  = C.CONSTRAINT_NAME");
		querySb.append("\n                          AND C.OWNER = :owner ");
		querySb.append("\n                          AND S.CONSTRAINT_TYPE = 'P' ");
		querySb.append("\n                      ) P ");
		querySb.append("\n                WHERE P.OWNER(+) = A.OWNER ");
		querySb.append("\n                  AND P.TABLE_NAME(+) = A.TABLE_NAME ");
		querySb.append("\n                  AND P.COLUMN_NAME(+) = A.COLUMN_NAME ");
		querySb.append("\n             ORDER BY OWNER_NM ");
		querySb.append("\n                    , TABLE_NM ");
		querySb.append("\n                    , PK_YN DESC ");
		querySb.append("\n                    , COLUMN_SEQ ");
		querySb.append("\n            ) B ");
		querySb.append("\n         ON (A.TABLE_NM = B.TABLE_NM ");
		querySb.append("\n             AND A.COLUMN_NM = B.COLUMN_NM) ");
		querySb.append("\n WHEN NOT MATCHED ");
		querySb.append("\n THEN ");
		querySb.append("\n    INSERT    ( TABLE_NM ");
		querySb.append("\n              , COLUMN_NM ");
		querySb.append("\n              , TABLE_COMNT ");
		querySb.append("\n              , COLUMN_COMNT ");
		querySb.append("\n              , DATA_TYPE ");
		querySb.append("\n              , DATA_LENGTH ");
		querySb.append("\n              , PK_YN ");
		querySb.append("\n              , NOT_NULL_YN ");
		querySb.append("\n              , COLUMN_SEQ ");
		querySb.append("\n              , OWNER_NM ");
		querySb.append("\n              , AUTO_UPD_YN ");
		querySb.append("\n              , AUTO_UPD_DT ");
		querySb.append("\n              , INS_USER_ID ");
		querySb.append("\n              , INS_DT ");
		querySb.append("\n              , UPD_USER_ID ");
		querySb.append("\n              , UPD_DT) ");
		querySb.append("\n       VALUES ( B.TABLE_NM ");
		querySb.append("\n              , B.COLUMN_NM ");
		querySb.append("\n              , B.TABLE_COMNT ");
		querySb.append("\n              , B.COLUMN_COMNT ");
		querySb.append("\n              , B.DATA_TYPE ");
		querySb.append("\n              , B.DATA_LENGTH ");
		querySb.append("\n              , B.PK_YN ");
		querySb.append("\n              , B.NOT_NULL_YN ");
		querySb.append("\n              , B.COLUMN_SEQ ");
		querySb.append("\n              , B.OWNER_NM ");
		querySb.append("\n              , 'Y' /*AUTO_UPD_YN*/ ");
		querySb.append("\n              , SYSDATE /*AUTO_UPD_DT*/ ");
		querySb.append("\n              , :login_user_id /*INS_USER_ID*/ ");
		querySb.append("\n              , SYSDATE/*INS_DT*/ ");
		querySb.append("\n              , :login_user_id /*UPD_USER_ID*/ ");
		querySb.append("\n              , SYSDATE/*UPD_DT*/ ");
		querySb.append("\n              ) ");
		querySb.append("\n WHEN MATCHED ");
		querySb.append("\n THEN ");
		querySb.append("\n    UPDATE SET A.TABLE_COMNT = B.TABLE_COMNT ");
		querySb.append("\n             , A.COLUMN_COMNT = B.COLUMN_COMNT ");
		querySb.append("\n             , A.DATA_TYPE = B.DATA_TYPE ");
		querySb.append("\n             , A.DATA_LENGTH = B.DATA_LENGTH ");
		querySb.append("\n             , A.PK_YN = B.PK_YN ");
		querySb.append("\n             , A.NOT_NULL_YN = B.NOT_NULL_YN ");
		querySb.append("\n             , A.COLUMN_SEQ = B.COLUMN_SEQ ");
		querySb.append("\n             , A.OWNER_NM = B.OWNER_NM ");
		querySb.append("\n             , A.AUTO_UPD_YN = 'Y' ");
		querySb.append("\n             , A.AUTO_UPD_DT = SYSDATE ");
		querySb.append("\n             , A.UPD_USER_ID = :login_user_id ");
		querySb.append("\n             , A.UPD_DT = SYSDATE  ");
		query = querySb.toString();
		mPs = new HashMap<String, Object>();
		mPs.put("owner", EgovProperties.getProperty("db.main.user").toUpperCase());
		mPs.put("login_user_id", login_user_id);
		namedParamDAO.insert(query, mPs);
		
		resultMap.put("RESULT_CD", "SUCCESS");
		return resultMap;
	}
	
	
}