/*
 * @(#)TableColumnDAO.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("tableColumnDAO")
public class TableColumnDAO extends EgovComAbstractDAO{

	/**
	 * 테이블 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectTableList(Map<String, Object> paramMap) throws NkiaException{
		return list("TableColumnDAO.selectTableList", paramMap);
	}

	/**
	 * 테이블컬럼 건수조회
	 * @param paramMap
	 */
	public int selectTableCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("TableColumnDAO.selectTableCount", paramMap);
	}
	
	/**
	 * 테이블 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectTableColumn(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("TableColumnDAO.selectTableColumn", paramMap);
	}

	/**
	 * 테이블컬럼 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectTableColumnList(Map<String, Object> paramMap) throws NkiaException{
		return list("TableColumnDAO.selectTableColumnList", paramMap);
	}
	
	/**
	 * 테이블컬럼 등록
	 * @param paramMap
	 */
	public void insertTableColumn(Map<String, Object> tableColumnInfo) throws NkiaException {
		this.insert("TableColumnDAO.insertTableColumn", tableColumnInfo);
	}

	/**
	 * 테이블컬럼 수정
	 * @param paramMap
	 */
	public void updateTableColumn(Map<String, Object> tableColumnInfo) throws NkiaException {
		this.update("TableColumnDAO.updateTableColumn", tableColumnInfo);
	}

	/**
	 * 테이블컬럼 삭제
	 * @param paramMap
	 */
	public void deleteTableColumn(Map<String, Object> paramMap) throws NkiaException {
		this.update("TableColumnDAO.deleteTableColumn", paramMap);
	}
	
}

