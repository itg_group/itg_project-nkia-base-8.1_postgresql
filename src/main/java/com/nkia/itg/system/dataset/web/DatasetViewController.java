/*
 * @(#)DatasetController.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import jxl.write.WriteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.system.dataset.service.DatasetService;
import com.nkia.itg.system.dataset.util.DatasetViewUtil;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.NamedParamService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class DatasetViewController {

	private static final Logger logger = LoggerFactory.getLogger(DatasetViewController.class);

	@Resource(name = "datasetService")
	private DatasetService datasetService;

	@Resource(name = "namedParamService")
	private NamedParamService namedParamService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;

	/**
	 * 통합현황조회 화면 오픈
	 * 
	 * @param paramMap
	 */
	@RequestMapping(value = "/itg/system/datasetview/goDatasetView.do")
	public String goDatasetViewCL(@RequestParam Map<String, Object> reqMap, HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/dataset/datasetView_SEARCH_CL.nvf";

		Map<String, Object> param = (Map<String, Object>) reqMap.get("param");
		Map<String, Object> mPs = null;
		Map<String, String> optionMap = null;

		if (null == reqMap.get("dataset_id") || "".equals(reqMap.get("dataset_id"))) {
			if (null == reqMap.get("program_id") || "".equals(reqMap.get("program_id"))) {
				throw new NkiaException("현황ID는 필수입력입니다.");
			}
		}

		String view_mode = StringUtil.replaceNull(String.valueOf(reqMap.get("view_mode")), ""); // 현황 아이디
		String dataset_id = StringUtil.replaceNull(String.valueOf(reqMap.get("dataset_id")), ""); // 현황 아이디
		String program_id = StringUtil.replaceNull(String.valueOf(reqMap.get("program_id")), ""); // 프로그램아이디 
		
		// 시스템 권한 조회
		String login_user_id = "";
		String login_user_nm = "";
		String login_cust_id = "";
		String login_cust_nm = "";
		String login_user_auth = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			login_user_id = userVO.getUser_id();
			login_user_nm = userVO.getUser_nm();
			login_cust_nm = userVO.getCust_nm();
			login_cust_id = userVO.getCust_id();
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			login_user_auth = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					login_user_auth += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
			}
		}
		Map<String, String> sessionMap = new HashMap<String, String>();
		sessionMap.put("login_user_id", login_user_id);
		sessionMap.put("login_user_nm", login_user_nm);
		sessionMap.put("login_cust_id", login_cust_id);
		sessionMap.put("login_cust_nm", login_cust_nm);
		sessionMap.put("login_user_auth", login_user_auth);
		
		Map<String, Object> datasetMap = null;
		if (!"".equals(dataset_id)) {
			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			mPs.put("check_auth_yn", "Y");// 권한체크하겠다.
			mPs.put("login_user_id", login_user_id);
			mPs.put("login_cust_id", login_cust_id);
			mPs.put("login_admin_yn", "Y");
			datasetMap = datasetService.selectDataset(mPs);
		} else if (!"".equals(program_id)) {
			mPs = new HashMap<String, Object>();
			mPs.put("program_id", program_id);
			mPs.put("check_auth_yn", "Y"); // 권한체크하겠다.
			mPs.put("login_user_id", login_user_id);
			mPs.put("login_cust_id", login_cust_id);
			mPs.put("login_admin_yn", "Y");
			datasetMap = datasetService.selectProgramDataset(mPs);
		}

		if (null == datasetMap) {
			throw new NkiaException("현황정보가 존재하지 않습니다.");
		}

		String program_nm = StringUtil.replaceNull(String.valueOf(datasetMap.get("PROGRAM_NM")), "");
		String ui_pattern_cd = StringUtil.replaceNull(String.valueOf(datasetMap.get("UI_PATTERN_CD")), "");
		String ui_type_cd = StringUtil.replaceNull(String.valueOf(datasetMap.get("UI_TYPE_CD")), "");
		
		mPs = new HashMap<String, Object>();
		mPs.put("dataset_id", dataset_id);
		mPs.put("cond_yn", "Y");
		mPs.put("sort_column", "COND_DISP_NO");
		List<Map<String, Object>> cond1ItemList = datasetService.selectDatasetItemList(mPs);
		
		mPs = new HashMap<String, Object>();
		mPs.put("dataset_id", dataset_id);
		mPs.put("list_yn", "Y");
		mPs.put("sort_column", "LIST_DISP_NO");
		List<Map<String, Object>> list1ItemList = datasetService.selectDatasetItemList(mPs);

		mPs = new HashMap<String, Object>();
		mPs.put("dataset_id", dataset_id);
		mPs.put("form_yn", "Y");
		mPs.put("sort_column", "FORM_DISP_NO");
		List<Map<String, Object>> detail1ItemList = datasetService.selectDatasetItemList(mPs);
		
		// Condition,List 정보 설정
		if ("CL".equals(ui_pattern_cd) || "CLD".equals(ui_pattern_cd)) {
			optionMap = new HashMap<String, String>();
			String condName = "baseSearch";
			String listName = "baseGrid";
			paramMap.put("COND1_NAME", condName);
			paramMap.put("COND1_READY_SCRIPT", DatasetViewUtil.createCondReadyScript(view_mode, datasetMap, condName, cond1ItemList, sessionMap, optionMap)); // 조회 영역
			paramMap.put("COND1_FORM_INIT_SCRIPT", DatasetViewUtil.createCondFormInitScript(view_mode, datasetMap, condName, cond1ItemList, sessionMap, optionMap)); // 조회 영역
			optionMap = new HashMap<String, String>();
			if ("CLD".equals(ui_pattern_cd)) {
				optionMap.put("grid_height", "200"); // 1줄에 40으로 게산
			}
			paramMap.put("LIST1_NAME", listName);
			paramMap.put("LIST1_READY_SCRIPT", DatasetViewUtil.createGridReadyScript(view_mode, datasetMap, listName, list1ItemList, sessionMap, optionMap)); // 목록 영역
			paramMap.put("LIST1_FORM_INIT_SCRIPT", DatasetViewUtil.createGridFormInitScript(view_mode, datasetMap, listName, list1ItemList, optionMap)); // 목록 헤더
			paramMap.put("LIST1_EXCELDOWNLOAD_SCRIPT", DatasetViewUtil.createGridExcelDownloadScript(view_mode, datasetMap, condName, list1ItemList, optionMap)); // 엑셀다운로드
		}
		// Detail 정보 설정
		if ("CLD".equals(ui_pattern_cd)) {
			optionMap = new HashMap<String, String>();
			String detailName = "detailForm";
			paramMap.put("DETAIL1_NAME", detailName);
			paramMap.put("DETAIL1_READY_SCRIPT", DatasetViewUtil.createFormReadyScript(view_mode, datasetMap, detailName, detail1ItemList, sessionMap, optionMap)); // 상세폼
			paramMap.put("DETAIL1_PAGE_INIT_SCRIPT", DatasetViewUtil.createFormPageInitScript(view_mode, datasetMap, detailName, detail1ItemList, sessionMap, optionMap)); // 상세폼
		}
		forwarPage = "/itg/system/dataset/datasetView_"+ui_type_cd+"_"+ui_pattern_cd+".nvf";
		
		paramMap.put("UI_NAME", forwarPage);
		paramMap.put("DATASET_MAP", datasetMap);
		paramMap.put("COND1_ITEM_LIST", cond1ItemList);
		paramMap.put("LIST1_ITEM_LIST", list1ItemList);
		paramMap.put("DETAIL1_ITEM_LIST", detail1ItemList);

		paramMap.put("REQ_MAP", reqMap);
		paramMap.put("login_user_auth", login_user_auth); // 권한명
		paramMap.put("login_user_id", login_user_id); // 로그인사용자아이디
		paramMap.put("login_user_nm", login_user_nm); // 로그인사용자명
		paramMap.put("login_cust_id", login_cust_id); // 로그인고객(부서)아이디
		paramMap.put("login_cust_nm", login_cust_nm); // 로그인고객(부서)아이디

		return forwarPage;
	}

	/**
	 * 프로그램정보 조회 (javascript 팝업생성에 사용)
	 * 
	 * @param paramMap
	 * @return map [DATASET_MAP, COND1_ITEM_LIST, LIST1_ITEM_LIST]
	 */
	@RequestMapping(value = "/itg/system/datasetview/searchProgramInfo.do")
	public @ResponseBody ResultVO searchProgramInfo(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			Map<String, Object> mPs = null;
			Map<String, Object> datasetMap = null;

			if (null == paramMap.get("program_id") || "".equals(paramMap.get("program_id"))) {
				throw new NkiaException("프로그램ID는 필수입력입니다.");
			}
			
			// 시스템 권한 조회
			String login_user_id = "";
			String login_user_nm = "";
			String login_cust_id = "";
			String login_cust_nm = "";
			String login_user_auth = "";
			boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
			UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
			if (isAuthenticate) {
				login_user_id = userVO.getUser_id();
				login_user_nm = userVO.getUser_nm();
				login_cust_nm = userVO.getCust_nm();
				login_cust_id = userVO.getCust_id();
				List<Map<String, Object>> userAuth = userVO.getSysAuthList();
				login_user_auth = "@";
				if (userAuth != null && !userAuth.isEmpty()) {
					for (int i = 0; i < userAuth.size(); i++) {
						login_user_auth += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
					}
				}
			}
			String program_id = StringUtil.replaceNull(String.valueOf(paramMap.get("program_id")), ""); // 프로그램아이디 
			
			Map<String, Object> resultMap = new HashMap<String, Object>();
			
			mPs = new HashMap<String, Object>();
			mPs.put("program_id", program_id);
			mPs.put("check_auth_yn", "Y"); // 권한체크하겠다.
			mPs.put("login_user_id", login_user_id);
			mPs.put("login_cust_id", login_cust_id);
			mPs.put("login_admin_yn", "Y");
			datasetMap = datasetService.selectProgramDataset(mPs);

			if (null == datasetMap) {
				throw new NkiaException("현황정보가 존재하지 않습니다.");
			}
			String dataset_id = StringUtil.replaceNull(String.valueOf(datasetMap.get("DATASET_ID")), "");
			
			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			mPs.put("cond_yn", "Y");
			mPs.put("sort_column", "COND_DISP_NO");
			List<Map<String, Object>> condItemList = datasetService.selectDatasetItemList(mPs);
			
			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			mPs.put("list_yn", "Y");
			mPs.put("sort_column", "LIST_DISP_NO");
			List<Map<String, Object>> listItemList = datasetService.selectDatasetItemList(mPs);
			
			resultMap.put("DATASET_MAP", datasetMap);
			resultMap.put("COND1_ITEM_LIST", condItemList);
			resultMap.put("LIST1_ITEM_LIST", listItemList);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 통합조회현황 목록조회/엑셀다운로드
	 * 
	 * @param paramMap
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@RequestMapping(value = "/itg/system/datasetview/searchList.do")
	public @ResponseBody
	ResultVO searchList(@RequestBody ModelMap paramMap) throws NkiaException, WriteException, IOException {
		ResultVO resultVO = new ResultVO();
		Map<String, Object> param = null;
		Map<String, Object> excelParam = null;
		Map<String, Object> mPs = null;
		try {
			if (null != paramMap.get("param")) {
				param = (Map<String, Object>) paramMap.get("param");
				excelParam = (Map<String, Object>) paramMap.get("excelParam");
			} else {
				PagingUtil.getFristEndNum(paramMap);
				param = paramMap;
			}
			if (null == param.get("mode") || "".equals(param.get("mode"))) { // {selectList, exceldown} 
				throw new NkiaException("처리모드정보는 필수입력입니다.");
			} else if (null == param.get("dataset_id") || "".equals(param.get("dataset_id"))) {
				throw new NkiaException("현황아이디는 필수입력입니다.");
			}

			String mode = StringUtil.replaceNull(String.valueOf(param.get("mode")), ""); // 수행모드 {selectList, exceldown}
			String dataset_id = StringUtil.replaceNull(String.valueOf(param.get("dataset_id")), ""); // 현황 아이디
			String sort_column = StringUtil.replaceNull(String.valueOf(param.get("sort_column")), ""); // 정렬컬럼
			
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			String query = "";
			StringBuffer querySb = null;

			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			Map<String, Object> datasetMap = datasetService.selectDataset(mPs);

			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			mPs.put("cond_yn", "Y");
			List<Map<String, Object>> itemList = datasetService.selectDatasetItemList(mPs);
			String sBAS_SQL = StringUtil.replaceNull(String.valueOf(datasetMap.get("BAS_SQL")), "");
			String sORDER_SQL = StringUtil.replaceNull(String.valueOf(datasetMap.get("ORDER_SQL")), "");
			
			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			datasetMap = datasetService.selectDataset(mPs);
			if (null == datasetMap) {
				throw new NkiaException("현황정보가 존재하지 않습니다.");
			}
			
			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			mPs.put("cond_yn", "Y");
			mPs.put("sort_column", "COND_DISP_NO");
			List<Map<String, Object>> condItemList = datasetService.selectDatasetItemList(mPs);
			
			mPs = new HashMap<String, Object>();
			mPs.put("dataset_id", dataset_id);
			mPs.put("list_yn", "Y");
			mPs.put("sort_column", "LIST_DISP_NO");
			List<Map<String, Object>> listItemList = datasetService.selectDatasetItemList(mPs);
			
			/*******************
			 * 기본 SQL 생성
			 ******************/
			String select_cols = "";
			for(int i = 0 ; i < listItemList.size() ; i ++) {
				Map<String, Object> listItem = listItemList.get(i);
				select_cols += (0<i?",":"") + listItem.get("ITEM_ID");
			}
			mPs = new HashMap<String, Object>();
			querySb = new StringBuffer();
			querySb.append("                SELECT " + select_cols + " ");
			querySb.append("\n                   FROM (");
			querySb.append("\n" + sBAS_SQL);

			// WHERE 설정
			String filterSqls = "";
			String condSql = "";

			String item_id = "";
			String cond_op_cd = "";
			
			int condCnt = 0; // 조건 Counter
			
			// 조회조건설정 1 (컬럼별)
			for (int i = 0; i < condItemList.size(); i++) {
				Map<String, Object> condItem = condItemList.get(i);
				item_id = StringUtil.replaceNull(String.valueOf(condItem.get("ITEM_ID")), "");
				String item_id_lower = item_id.toLowerCase();
				condSql = StringUtil.replaceNull(String.valueOf(condItem.get("COND_SQL")), "");
				cond_op_cd = StringUtil.replaceNull(String.valueOf(condItem.get("COND_OP_CD")), "");
				String condValue = StringUtil.replaceNull(String.valueOf(param.get(item_id_lower)), "");

				if ("".equals(cond_op_cd)) {
					continue;
				}
				
				// Condition 설정 
				if (param.containsKey(item_id_lower+"_startDate") && param.containsKey(item_id_lower+"_endDate")) { // 날자형식이면
					condSql = condSql.replace("[OP1]", " BETWEEN").replace("[OP2]", " AND :" + item_id_lower + "_ZZZ2");
					String startDate = String.valueOf(param.get(item_id_lower+"_startDate")).replace("-", "").replace("/", "");
					String endDate = String.valueOf(param.get(item_id_lower+"_endDate")).replace("-", "").replace("/", "");
					if("".equals(startDate) || "".equals(endDate)){
						continue;
					}
					if("".equals(condSql)){
						continue;
					}
					mPs.put(item_id_lower, startDate);
					mPs.put(item_id_lower + "_ZZZ2", endDate);
				}else{
					if("".equals(condValue)){
						continue;
					}
					if (cond_op_cd.equals("EQUAL")) { // = :ITEM_ID
						condSql = condSql.replace("[OP1]", " = ").replace("[OP2]", "");
					} else if (cond_op_cd.equals("GREATER")) { // < :ITEM_ID
						condSql = condSql.replace("[OP1]", " < ").replace("[OP2]", "");
					} else if (cond_op_cd.equals("LESS")) { // > :ITEM_ID
						condSql = condSql.replace("[OP1]", " > ").replace("[OP2]", "");
					} else if (cond_op_cd.equals("GREATEREQUAL")) { // =< :ITEM_ID
						condSql = condSql.replace("[OP1]", " <= ").replace("[OP2]", "");
					} else if (cond_op_cd.equals("LESSEQUAL")) { // => :ITEM_ID
						condSql = condSql.replace("[OP1]", " >= ").replace("[OP2]", "");
					} else if (cond_op_cd.equals("LIKE")) { // LIKE '%' || :ITEM_ID || '%'
						condSql = condSql.replace("[OP1]", " LIKE '%' || ").replace("[OP2]", " || '%'");
					} else if (cond_op_cd.equals("LIKEL")) { // LIKE '%' || :ITEM_ID
						condSql = condSql.replace("[OP1]", " LIKE '%' || ").replace("[OP2]", " ");
					} else if (cond_op_cd.equals("LIKER")) { // LIKE :ITEM_ID || '%'
						condSql = condSql.replace("[OP1]", " LIKE ").replace("[OP2]", " || '%'");
					} else if (cond_op_cd.equals("IN")) { // IN (:ITEM_ID)
						condSql = condSql.replace("[OP1]", " IN (").replace("[OP2]", ")");
					} else if (cond_op_cd.equals("BETWEEN")) { // BETWEEN :ITEM_ID AND :ITEM_ID2
						condSql = condSql.replace("[OP1]", " BETWEEN").replace("[OP2]", " AND :" + item_id_lower + "_ZZZ2");
					} else {
						condSql = condSql.replace("[OP1]", " = ").replace("[OP2]", ""); // 기본 = :ITEM_ID
					}
					if("".equals(condSql)){
						continue;
					}
					if (cond_op_cd.equals("BETWEEN")) {
						String[] arrValue = condValue.split("~");
						mPs.put(item_id_lower, (0 < arrValue.length ? arrValue[0] : ""));
						mPs.put(item_id_lower + "_ZZZ2", (1 < arrValue.length ? arrValue[1] : arrValue[0]));
					} else if (cond_op_cd.equals("IN")) { // 보류
						String condValue2 = "'" + condValue.replaceAll(",", "','") + "'";
						condSql = condSql.replace(":" + item_id_lower, condValue2);
					} else {
						mPs.put(item_id_lower, condValue);
					}
				}
				filterSqls += "\n   AND " + condSql + " /* filter" + ++condCnt + " - " + item_id_lower + " - " + cond_op_cd + " */ ";
			}

			// 조회조건설정 2 (search_type + search_value)
			for (int i = 0; i < condItemList.size(); i++) {
				Map<String, Object> condItem = condItemList.get(i);
				item_id = StringUtil.replaceNull(String.valueOf(condItem.get("ITEM_ID")), "");
				String item_id_lower = item_id.toLowerCase();
				
				if(param.containsKey("search_type") && param.containsKey("search_value")){
					String search_type = StringUtil.replaceNull(String.valueOf(param.get("search_type")), "");
					String search_value = StringUtil.replaceNull(String.valueOf(param.get("search_value")), "");
					if(item_id_lower.equals(search_type.toLowerCase()) && !"".equals(search_value)){
						condSql = "";
						if("ID".equals(search_type)){
							condSql = "ID LIKE '%'|| :search_type_ID ||'%'";
							mPs.put("search_type_ID", search_value);
						}else if("NAME".equals(search_type)){
							condSql = "NAME LIKE '%'|| :search_type_NAME ||'%'";
							mPs.put("search_type_NAME", search_value);
						}else if("IDNAME".equals(search_type)){
							condSql = "(ID LIKE '%'|| :search_type_ID ||'%' OR NAME LIKE '%'|| :search_type_NAME ||'%')";
							mPs.put("search_type_ID", search_value);
							mPs.put("search_type_NAME", search_value);
						}
						if (!"".equals(condSql)) {
							filterSqls += "\n   AND " + condSql + " /* filter" + ++condCnt + " search_type "+search_type+" */ ";
						}
					}
				}
			}
			
			querySb.append("\n                     ) XX1 ");
			querySb.append("\n                 WHERE 1=1 ");
			querySb.append("              " + filterSqls);
			
			if ("selectList".equals(mode)) {

				/*******************
				 * selectList-1) Count 조회한다.
				 ******************/
				String query2 = "";
				query2 += "\n           SELECT /*  /itg/system/dataset/searchDynamicList.do - selectCount */ ";
				query2 += "\n                  COUNT(1) AS CNT ";
				query2 += "\n             FROM ( ";
				query2 += "\n" + querySb.toString();
				query2 += "\n                ) XX2 ";
				query = query2;
				Map<String, Object> countMap = namedParamService.selectMap(query, mPs);
				totalCount = Integer.parseInt(String.valueOf(countMap.get("CNT")));

				/*******************
				 * selectList-2) ROWNUM 구하고 정렬한다
				 ******************/
				String query3 = "";
				
				if (!"".equals(sort_column)) {
					query3 += "           SELECT ROW_NUMBER() OVER(ORDER BY " + sort_column + ") AS RNUM ";
				}else if (!"".equals(sORDER_SQL)) {
					query3 += "           SELECT ROW_NUMBER() OVER(ORDER BY " + sORDER_SQL + ") AS RNUM ";
				} else {
					query3 += "           SELECT ROWNUM AS RNUM ";
				}
				query3 += "\n                , XX2.* ";
				query3 += "\n            FROM ( ";
				query3 += "\n" + querySb.toString();
				query3 += "\n                ) XX2 ";
				query3 += "\n         ORDER BY RNUM";
				query = query3;

				/*******************
				 * selectList-3) 페이징 적용한다.
				 ******************/
				if (!"".equals(StringUtil.replaceNull(String.valueOf(param.get("start")), ""))) {
					String query4 = "";
					query4 += "\n  SELECT /*  /itg/system/dataset/searchDynamicList.do - selectList */ ";
					query4 += "\n         XX3.*  ";
					query4 += "\n    FROM (   ";
					query4 += "\n" + query3;
					query4 += "\n       ) XX3 ";
					int page = (Integer) param.get("page");
					int limit = (Integer) param.get("limit");
					int start = (Integer) param.get("start");
					int end = limit * page;
					query4 += "\n  WHERE XX3.RNUM BETWEEN :first_num AND :last_num ";
					mPs.put("first_num", start + 1);
					mPs.put("last_num", end);
					query = query4;
				}
				List<Map<String, Object>> resultList = namedParamService.selectList(query, mPs);

				/*******************
				 * selectList-4) 결과조회
				 ******************/
				gridVO.setTotalCount(totalCount);
				gridVO.setCursorPosition((Integer) param.get(BaseEnum.PAGER_START_PARAM.getString()));
				gridVO.setRows(resultList);
				resultVO.setGridVO(gridVO);

			} else if ("exceldown".equals(mode)) {
				/*******************
				 * exceldown-1) 엑셀다운로드 한다.
				 ******************/
				String query2 = "";
				query2 += "\n           SELECT /*  /itg/system/dataset/searchDynamicList.do - exceldown */ ";
				query2 += "\n                  XX2.* ";
				query2 += "\n             FROM ( ";
				query2 += "\n" + querySb.toString();
				query2 += "\n                ) XX2 ";
				if (!"".equals(sORDER_SQL)) {
					query2 += "\n         ORDER BY " + sORDER_SQL;
				}
				query = query2;
				//query = querySb.toString();
				List<Map<String, Object>> resultList = namedParamService.selectList(query, mPs);
				ModelMap excelMap = excelMaker.makeExcelFile(excelParam, resultList);
				resultVO.setResultMap(excelMap);
			} else {
				throw new NkiaException("넘겨받은 mode 유형을 처리할 수 없습니다. [" + mode + "]");
			}
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
