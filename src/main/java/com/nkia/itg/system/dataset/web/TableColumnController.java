/*
 * @(#)TableColumnController.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.system.dataset.service.TableColumnService;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.excel.ExcelMaker;
import com.nkia.itg.base.service.NamedParamService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class TableColumnController {

	private static final Logger logger = LoggerFactory.getLogger(TableColumnController.class);
	
	@Resource(name = "tableColumnService")
	private TableColumnService tableColumnService;

	@Resource(name = "namedParamService")
	private NamedParamService namedParamService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "excelMaker")
	private ExcelMaker excelMaker;
	
	/**
	 * 테이블컬럼관리 화면 오픈
	 * @param paramMap
	 */
	@RequestMapping(value="/itg/system/dataset/goTableColumnManager.do")
	public String goTableColumnManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		String forwarPage = "/itg/system/dataset/tableColumnManager.nvf";
		
		// 시스템 권한 조회
		String authString = "";
		boolean isAuthenticate = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		if (isAuthenticate) {
			List<Map<String, Object>> userAuth = userVO.getSysAuthList();
			authString = "@";
			if (userAuth != null && !userAuth.isEmpty()) {
				for (int i = 0; i < userAuth.size(); i++) {
					authString += (((Map<String, Object>) userAuth.get(i)).get("AUTH_ID") + "@");
				}
				paramMap.put("user_auth", authString);
			}
		}

		//로그인 id 조회
		String loginUserId = userVO.getUser_id();
		paramMap.put("login_user_id", loginUserId);
		
		//로그인 사용자명 조회
		String loginUserName = userVO.getUser_nm();
		paramMap.put("ins_user_nm", loginUserName);
		
		return forwarPage;
	}

	/**
	 * 테이블컬럼 목록출력
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/dataset/searchTableList.do")
	public @ResponseBody ResultVO searchTableList(@RequestBody ModelMap paramMap) throws NkiaException {
			ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
			
			List<Map<String, Object>> resultList = tableColumnService.selectTableList(paramMap);		
			totalCount = tableColumnService.selectTableCount(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테이블컬럼 목록출력
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/dataset/searchTableColumnList.do")
	public @ResponseBody ResultVO searchTableColumnList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = tableColumnService.selectTableColumnList(paramMap);		
			gridVO.setRows(resultList);			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 테이블컬럼 등록
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/dataset/insertTableColumn.do")
	public @ResponseBody ResultVO insertTableColumn(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			tableColumnService.insertTableColumn(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 테이블컬럼 수정 
	 * @param paramMap
	 */	
	@RequestMapping(value="/itg/system/dataset/updateTableColumn.do")
	public @ResponseBody ResultVO updateTableColumn(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{			
			tableColumnService.updateTableColumn(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 테이블컬럼 삭제
	 * @param paramMap
	 */	
	@RequestMapping(value="/itg/system/dataset/deleteTableColumn.do")
	public @ResponseBody ResultVO deleteTableColumn(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			tableColumnService.deleteTableColumn(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

	/**
	 * 속성 목록삭제
	 * @param paramMap
	 */	
	@RequestMapping(value="/itg/system/dataset/deleteTableColumnList.do")
	public @ResponseBody ResultVO deleteTableColumnList(@RequestBody ModelMap paramMap)throws NkiaException  {
		ResultVO resultVO = new ResultVO();
		try{
			List<Map<String, Object>> input1List = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");	
			Map<String, Object> resMap = tableColumnService.deleteTableColumnList(input1List);
			String msg = "";
			int updCnt = Integer.parseInt(String.valueOf(resMap.get("updCnt")));
			int delCnt = Integer.parseInt(String.valueOf(resMap.get("delCnt")));
			if(0 < delCnt){
				msg += (0 < msg.length()? "<br/>": "") + delCnt + "건 삭제되었습니다.";
			}
			if(0 < updCnt){
				msg += (0 < msg.length()? "<br/>": "") + updCnt + "건 초기화되었습니다.";
			}
			resultVO.setResultMsg(msg);	
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}
	
	/**
	 * 테이블컬럼 등록
	 * @param paramMap
	 */		
	@RequestMapping(value="/itg/system/dataset/insertTableColumnInfoWithSchedule.do")
	public @ResponseBody ResultVO insertTableColumnInfoWithSchedule(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{		
			Map<String, Object> resMap = tableColumnService.insertTableColumnInfoWithSchedule(paramMap);
			if("SUCCESS".equals(resMap.get("RESULT_CD"))){
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
			}else{
				resultVO.setResultMsg("정상적으로 처리되지 않았습니다.");
			}
		} catch(NkiaException e) {
			resultVO.setSuccess(false);
			resultVO.setResultMsg(e.getMessage());
		}
		return resultVO;
	}

}
