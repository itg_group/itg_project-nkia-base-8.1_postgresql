/*
 * @(#)TableColumnService.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface TableColumnService {

	public List<Map<String, Object>> selectTableList(Map<String, Object> paramMap) throws NkiaException;

	public int selectTableCount(Map<String, Object> paramMap) throws NkiaException;

	public Map<String, Object> selectTableColumn(Map<String, Object> paramMap) throws NkiaException;

	public List<Map<String, Object>> selectTableColumnList(Map<String, Object> paramMap) throws NkiaException;

	public void insertTableColumn(Map<String, Object> paramMap) throws NkiaException;
	
	public void updateTableColumn(Map<String, Object> paramMap) throws NkiaException;
	
	public void deleteTableColumn(Map<String, Object> paramMap) throws NkiaException;

	public Map<String, Object> deleteTableColumnList(List<Map<String, Object>> list) throws NkiaException;
	
	public Map<String, Object> insertTableColumnInfoWithSchedule(Map<String, Object> paramMap) throws NkiaException;
	
}
