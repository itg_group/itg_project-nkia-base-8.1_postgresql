/*
 * @(#)DatasetServiceImpl.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.system.dataset.dao.DatasetDAO;
import com.nkia.itg.system.dataset.service.DatasetService;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.NamedParamDAO;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("datasetService")
public class DatasetServiceImpl implements DatasetService{
	
	private static final Logger logger = LoggerFactory.getLogger(DatasetServiceImpl.class);

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "datasetDAO")
	public DatasetDAO datasetDAO;
	
	@Resource(name="namedParamDAO")
	private NamedParamDAO namedParamDAO;

	/**
	 * 통합현황 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDataset(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectDataset(paramMap);
	}

	/**
	 * 통합현황 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectProgramDataset(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectProgramDataset(paramMap);
	}

	/**
	 * 통합현황 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDatasetList(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		String login_cust_id = "";
		String login_user_auth = "@";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			login_cust_id = userVO.getCust_id();
			List sysAuthList = userVO.getSysAuthList();
			for (Object item : sysAuthList) {
				HashMap<String, Object> items = (HashMap<String, Object>)item;
				login_user_auth += (items.get("AUTH_ID").toString().toUpperCase() + "@");
			}
		}
		paramMap.put("check_auth_yn", "Y");// 권한체크하겠다.
		paramMap.put("login_user_id", login_user_id);
		paramMap.put("login_cust_id", login_cust_id);
		//paramMap.put("login_user_auth", login_user_auth);
		paramMap.put("login_admin_yn", "Y");
		return datasetDAO.selectDatasetList(paramMap);
	}
	
	/**
	 * 통합현황 건수조회
	 * @param paramMap
	 */
	public int selectDatasetCount(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		String login_cust_id = "";
		String login_user_auth = "@";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
			login_cust_id = userVO.getCust_id();
			List sysAuthList = userVO.getSysAuthList();
			for (Object item : sysAuthList) {
				HashMap<String, Object> items = (HashMap<String, Object>)item;
				login_user_auth += (items.get("AUTH_ID").toString().toUpperCase() + "@");
			}
		}
		paramMap.put("check_auth_yn", "Y");// 권한체크하겠다.
		paramMap.put("login_user_id", login_user_id);
		paramMap.put("login_cust_id", login_cust_id);
		//paramMap.put("login_user_auth", login_user_auth);
		paramMap.put("login_admin_yn", "Y");
		return datasetDAO.selectDatasetCount(paramMap);
	}
	
	/**
	 * 통합현황 등록
	 * @param paramMap
	 */
	public Map<String, Object> insertDataset(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		
		String dataset_id = datasetDAO.selectDatasetNewId(paramMap); // SELECT PK FROM SYS_DATASET
		paramMap.put("dataset_id", dataset_id);
		paramMap.put("login_user_id", login_user_id);
		datasetDAO.insertDataset(paramMap); // INSERT SYS_DATASET 
		
		mPs = new HashMap<String, Object>();
		mPs.put("dataset_id", dataset_id);
		
		datasetDAO.deleteDatasetDataauthAll(mPs);
		List<Map<String, Object>> datasetDataauthList = (List<Map<String, Object>>)paramMap.get("INPUT1_DATAAUTHLIST");
		for(int i=0; i<datasetDataauthList.size(); i++){
			Map<String, Object> map = datasetDataauthList.get(i);
			map.put("dataset_id", dataset_id);
			map.put("login_user_id", login_user_id);
			if (datasetDAO.selectDatasetDataauthCount(map) < 1) {
				datasetDAO.insertDatasetDataauth(map);
			}
		}
		
		resultMap.put("dataset_id", dataset_id);
		return resultMap;
	}

	/**
	 * 통합현황 수정
	 * @param paramMap
	 */
	public void updateDataset(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		paramMap.put("login_user_id", login_user_id);
		datasetDAO.updateDataset(paramMap); // UPDATE SYS_DATASET 
		
		datasetDAO.deleteDatasetDataauthAll(paramMap);
		List<Map<String, Object>> datasetDataauthList = (List<Map<String, Object>>)paramMap.get("INPUT1_DATAAUTHLIST");
		for(int i=0; i<datasetDataauthList.size(); i++){
			Map<String, Object> map = datasetDataauthList.get(i);
			map.put("dataset_id", paramMap.get("dataset_id"));
			map.put("login_user_id", login_user_id);
			map.put("login_user_id", login_user_id);
			if(datasetDAO.selectDatasetDataauthCount(map)< 1){
				datasetDAO.insertDatasetDataauth(map);
			}
		}
		
	}
	
	/**
	 * 통합현황 삭제
	 * @param paramMap
	 */
	public void deleteDataset(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		paramMap.put("login_user_id", login_user_id);
		datasetDAO.deleteDataset(paramMap); // UPDATE SYS_DATASET USE_YN='N' 
		
		//datasetDAO.deleteDatasetDataauthAll(paramMap);
	}
	
	/**
	 * 속성 재생성 
	 * @param paramMap
	 */
	public void itemAutoCreate(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		StringBuffer querySb = null;
		Map<String, Object> param = null;
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		String dataset_id = String.valueOf(paramMap.get("dataset_id")); // SELECT PK FROM SYS_DATASET
		
		param = new HashMap<String, Object>();
		param.put("dataset_id", dataset_id);
		Map<String, Object> datasetMap = datasetDAO.selectDataset(param);
		if(null == datasetMap){
			throw new NkiaException("통합현황 상세정보를 조회하지 않았습니다."); 
		}
		
		if(null != datasetMap.get("BAS_SQL") && -1 < String.valueOf(datasetMap.get("BAS_SQL")).toUpperCase().indexOf("SELECT")){

			/*********************
			 * DELETE SYS_DATASET_ITEM LIST {dataset_id}
			 *********************/
			
			param = new HashMap<String, Object>();
			param.put("dataset_id", dataset_id);
			datasetDAO.deleteDatasetItemList(param);
			
			/*********************
			 * INSERT SYS_DATASET_ITEM 
			 *********************/
			
			String basSql = String.valueOf(datasetMap.get("BAS_SQL"));
			querySb = new StringBuffer();
			querySb.append("\n  SELECT ZZ.* ");
			querySb.append("\n   FROM ( ");
			querySb.append("\n" + basSql);
			querySb.append("\n      ) ZZ ");
			querySb.append("\n  WHERE ROWNUM < 2 ");
			param = new HashMap<String, Object>();
			Map<String, Object> resultMap = namedParamDAO.selectMap(querySb.toString(), param); // SELECT 1건
			if(null != resultMap){
				int cnt = 0;
				for( String key : resultMap.keySet() ){
					cnt++;
		            String item_id = key.toUpperCase();
					param = new HashMap<String, Object>();
					param.put("login_user_id", login_user_id);
					param.put("dataset_id", dataset_id); // PK
					param.put("item_id", item_id); // PK
					param.put("item_nm", item_id); // 항목명
					param.put("base_disp_no", cnt); // 기본정렬번호
					param.put("auto_ins_yn", "Y"); // 자동등록여부
					// Condition
					param.put("cond_yn", "Y"); //  = "N";
					param.put("cond_label", item_id); //  = label;
					param.put("cond_comp_type", "TEXT"); //  = "TEXT";
					param.put("cond_code_grp_id", ""); //  = "";
					param.put("cond_popup_id", ""); //  = "";
					param.put("cond_hidden_yn", ""); //  = "N";
					param.put("cond_required_yn", ""); //  = "N";
					param.put("cond_colspan", "1"); //  = "1";
					param.put("cond_op_cd", "EQUAL"); 
					param.put("cond_sql", item_id + " [OP1] :" + item_id.toLowerCase() + " [OP2]");
					param.put("cond_disp_no", "999");
					param.put("cond_length", "0");
					// List
					param.put("list_yn", "Y"); //  = "N";
					param.put("list_label", item_id); //  = label;
					param.put("list_xtype", "NA"); //  = "na";
					param.put("list_width", "100"); //  = "100";
					param.put("list_flex", "NA"); //  = "na";
					param.put("list_sortable", "0"); //  = "0";
					param.put("list_align", ""); //  = "left";
					param.put("list_required_yn", ""); //  = "N";
					param.put("list_disp_no", cnt);
					param.put("list_length", "0");
					// Form
					param.put("form_yn", "Y"); //  = "N";
					param.put("form_label", item_id); //  = label;
					param.put("form_comp_type", "TEXT"); //  = "TEXT";
					param.put("form_code_grp_id", ""); //  = "";
					param.put("form_popup_id", ""); //  = "";
					param.put("form_hidden_yn", ""); //  = "N";
					param.put("form_required_yn", ""); //  = "N";
					param.put("form_colspan", "1"); //  = "1";
					param.put("form_disp_no", cnt);
					param.put("form_length", "0");
					
					if(null == datasetDAO.selectDatasetItem(param)){
						datasetDAO.insertDatasetItem(param); // INSERT SYS_DATASET_ITEM 
					}
		        }
			}
			
			/*********************
			 * UPDATE SYS_DATASET_ITEM 부가정보 변경 {item_nm, list_align}  SYS_TABLE_COLUMN_INFO 에서 참조
			 *********************/
			
			querySb = new StringBuffer();
			querySb.append("\n   SELECT /* Table List */ A.TABLE_NM ") ;
			querySb.append("\n     FROM SYS_TABLE_COLUMN_INFO A ") ;
			querySb.append("\n GROUP BY TABLE_NM ") ;
			querySb.append("\n ORDER BY 1 ") ;
			List<Map<String, Object>> tableList = namedParamDAO.selectList(querySb.toString(), null); // SELECT SYS_TABLE_COLUMN_INFO
			
			List<String> tempTableList = new ArrayList<String>();
			for(Map<String, Object> tableMap: tableList){
				String tableStr = String.valueOf(tableMap.get("TABLE_NM")).toUpperCase();
				String basSqlUpper = basSql.toUpperCase();
				if(-1 < basSqlUpper.indexOf(tableStr)){
					tempTableList.add(tableStr);
				}
			}
			
			querySb = new StringBuffer();
			querySb.append(" UPDATE SYS_DATASET_ITEM A ") ;
			querySb.append("\n    SET A.UPD_DT = SYSDATE ") ;
			querySb.append("\n      , A.UPD_USER_ID = :login_user_id ") ;
			// ITEM_NM 한글명 설정
			querySb.append("\n      , A.ITEM_NM =    (SELECT NVL(MAX(NVL(S.USER_COLUMN_COMNT, S.COLUMN_COMNT)), A.ITEM_NM) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                         ) /* 타이틀명(1순위 사용자컬럼명, 2순위 테이블컬럼명) */ ") ;
			// LIST_KEY_YN 설정
			querySb.append("\n      , A.LIST_KEY_YN = (SELECT NVL(MAX(S.PK_YN), A.LIST_KEY_YN) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                         ) /* 타이틀명(1순위 사용자컬럼명, 2순위 테이블컬럼명) */ ") ;
			// LIST_REQUIRED_YN 설정
			querySb.append("\n      , A.LIST_REQUIRED_YN = (SELECT NVL(MAX(S.NOT_NULL_YN), A.LIST_REQUIRED_YN) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                         ) /* 타이틀명(1순위 사용자컬럼명, 2순위 테이블컬럼명) */ ") ;
			// LIST_LENGTH 설정
			querySb.append("\n      , A.LIST_LENGTH = (SELECT NVL(MAX(S.DATA_LENGTH), A.LIST_LENGTH) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                         ) /* 컬럼길이 */ ") ;
			// LIST_ALIGN 정렬값 설정
			querySb.append("\n      , A.LIST_ALIGN =   (SELECT (CASE NVL(MAX(S.DATA_TYPE), '') ") ;
			querySb.append("\n                                      WHEN 'NUMBER' THEN 'RIGHT' ") ;
			querySb.append("\n                                      WHEN 'DATE' THEN 'CENTER' ") ;
			querySb.append("\n                                      ELSE 'LEFT' END) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                        ) /* 정렬설정값(left,center,right) */ ") ;
			// COND_COMP_TYPE 조회컴포넌트유형 설정 
			querySb.append("\n      , A.COND_COMP_TYPE = (SELECT NVL(MAX(S.USER_COMP_TYPE), A.COND_COMP_TYPE) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                        ) ") ;
			// COND_CODE_GRP_ID 조회코드그룹아이디 설정
			querySb.append("\n      , A.COND_CODE_GRP_ID = (SELECT NVL(MAX(S.USER_CODE_GRP_ID), A.COND_CODE_GRP_ID) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                        ) ") ;
			// COND_POPUP_ID 조회코드그룹아이디 설정
			querySb.append("\n      , A.COND_POPUP_ID = (SELECT NVL(MAX(S.USER_POPUP_ID), A.COND_POPUP_ID) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                        ) ") ;
			// FORM_DEFAULT_SVALUE 조회코드그룹아이디 설정
			querySb.append("\n      , A.FORM_DEFAULT_SVALUE = (SELECT NVL(MAX(S.USER_DEFAULT_VALUE), A.FORM_DEFAULT_SVALUE) ") ;
			querySb.append("\n                           FROM SYS_TABLE_COLUMN_INFO S ") ;
			querySb.append("\n                          WHERE S.COLUMN_NM = A.ITEM_ID ") ;
			for(int i=0; i<tempTableList.size(); i++){
				if(i==0)
					querySb.append("\n                            AND S.TABLE_NM IN ( ") ;
				querySb.append((0 < i? ",": "") + " :table_nm_" + i) ;
				if(i==tempTableList.size()-1)
					querySb.append(" ) ") ;
			}
			querySb.append("\n                        ) ") ;
			// WHERE
			querySb.append("\n  WHERE A.DATASET_ID = :dataset_id ") ;
			param = new HashMap<String, Object>();
			for(int i=0; i<tempTableList.size(); i++){
				param.put("table_nm_" + i, tempTableList.get(i));
			}
			param.put("dataset_id", dataset_id);
			param.put("login_user_id", login_user_id);
			namedParamDAO.update(querySb.toString(), param); // UPDATE SYS_DATASET_ITEM
			
			querySb = new StringBuffer();
			querySb.append(" UPDATE SYS_DATASET_ITEM A ") ;
			querySb.append("\n    SET A.COND_LABEL = A.ITEM_NM ") ;
			querySb.append("\n      , A.LIST_LABEL = A.ITEM_NM ") ;
			querySb.append("\n      , A.FORM_LABEL = A.ITEM_NM ") ;
			querySb.append("\n      , A.FORM_COMP_TYPE = A.COND_COMP_TYPE ") ;
			querySb.append("\n      , A.FORM_CODE_GRP_ID = A.COND_CODE_GRP_ID ") ;
			querySb.append("\n      , A.FORM_POPUP_ID = A.COND_POPUP_ID ") ;
			querySb.append("\n      , A.FORM_KEY_YN = A.LIST_KEY_YN ") ;
			querySb.append("\n      , A.FORM_REQUIRED_YN = A.LIST_REQUIRED_YN ") ;
			querySb.append("\n      , A.COND_LENGTH = A.LIST_LENGTH ") ;
			querySb.append("\n      , A.FORM_LENGTH = A.LIST_LENGTH ") ;
			querySb.append("\n  WHERE A.DATASET_ID = :dataset_id ") ;
			param = new HashMap<String, Object>();
			param.put("dataset_id", dataset_id);
			namedParamDAO.update(querySb.toString(), param); // UPDATE SYS_DATASET_ITEM
		}
	}
	
	/**
	 * 속성 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDatasetItem(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectDatasetItem(paramMap);
	}

	/**
	 * 속성 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDatasetItemList(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectDatasetItemList(paramMap);
	}

	/**
	 * 현황 고객 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDatasetDataauthList(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectDatasetDataauthList(paramMap);
	}
	
	/**
	 * 코드그룹 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectCodeGrpList(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectCodeGrpList(paramMap);
	}
	
	/**
	 * 코드그룹 건수조회
	 * @param paramMap
	 */
	public int selectCodeGrpCount(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectCodeGrpCount(paramMap);
	}

	/**
	 * 팝업 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectPopupList(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectPopupList(paramMap);
	}
	
	/**
	 * 팝업 건수조회
	 * @param paramMap
	 */
	public int selectPopupCount(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectPopupCount(paramMap);
	}

	/**
	 * 코드그룹 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDefaultValueList(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectDefaultValueList(paramMap);
	}
	
	/**
	 * 코드그룹 건수조회
	 * @param paramMap
	 */
	public int selectDefaultValueCount(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.selectDefaultValueCount(paramMap);
	}
	
	/**
	 * 속성 등록
	 * @param paramMap
	 */
	public void insertDatasetItem(Map<String, Object> paramMap) throws NkiaException {	
		String login_user_id = "";	
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		String item_id = String.valueOf(paramMap.get("item_id")).toUpperCase().trim();
		paramMap.put("item_id", item_id);
		if(null != datasetDAO.selectDatasetItem(paramMap)){
			throw new NkiaException("이미 등록되어 있습니다.\n확인 후 처리바랍니다.");
		}
		if(null == paramMap.get("item_nm") || "".equals(String.valueOf(paramMap.get("item_nm")))){
			paramMap.put("item_nm", item_id);
		}
		paramMap.put("cond_sql", item_id + " [OP1] :" + item_id.toLowerCase() + " [OP2]");
		paramMap.put("login_user_id", login_user_id);
		datasetDAO.insertDatasetItem(paramMap); // INSERT SYS_DATASET_ITEM 
	}
	
	/**
	 * 속성 변경
	 * @param paramMap
	 */
	public void updateDatasetItem(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		paramMap.put("login_user_id", login_user_id);
		datasetDAO.updateDatasetItem(paramMap); // UPDATE SYS_DATASET_ITEM 
	}
	
	/**
	 * 속성 순서변경
	 * @param paramMap
	 */
	public void updateDatasetItemDispNo(List<Map<String, Object>> list) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		for(Map<String, Object> map: list){
			map.put("login_user_id", login_user_id);
			datasetDAO.updateDatasetItemDispNo(map); // UPDATE SYS_DATASET_ITEM 
		}
	}
	
	/**
	 * 속성 삭제
	 * @param paramMap
	 */
	public void deleteDatasetItem(Map<String, Object> paramMap) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if( isAuthenticated ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		paramMap.put("login_user_id", login_user_id);
		datasetDAO.deleteDatasetItem(paramMap); // DELETE SYS_DATASET_ITEM 
	}
	
	/**
	 * 코드정보조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> searchCodeDataList(Map<String, Object> paramMap) throws NkiaException {
		return datasetDAO.searchCodeDataList(paramMap);
	}
	
	/**
	 * 속성 목록삭제
	 * @param paramMap
	 */
	public void deleteDatasetItemList(List<Map<String, Object>> list) throws NkiaException {
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}
		for(Map<String, Object> map: list){
			map.put("login_user_id", login_user_id);
			datasetDAO.deleteDatasetItem(map); // UPDATE SYS_DATASET_ITEM 
		}
	}
	
	/**
	 * 속성 목록저장
	 * @param paramMap
	 */
	public void saveDatasetItemList(Map<String, Object> paramMap) throws NkiaException {
		Map<String, Object> mPs = null;
		String login_user_id = "";
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		UserVO userVO = new UserVO();
		if( isAuthenticated ){
			userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			login_user_id = userVO.getUser_id();
		}

		String name = (String)paramMap.get("INPUT1_NAME");
		String dataset_id = (String)paramMap.get("INPUT1_DATASET_ID");
		List<Map<String, Object>> list = (List<Map<String, Object>>)paramMap.get("INPUT1_LIST");
		for(Map<String, Object> map: list){
			if("".equals(map.get("dataset_id")) || "".equals(map.get("item_id"))){
				throw new NkiaException(messageSource.getMessage("msg.common.00011")); // 필수 입력 항목을 확인 바랍니다.
			}
		}
		
		mPs = new HashMap<String, Object>();
		mPs.put("dataset_id", dataset_id);
		if ("Condition".equals(name)) {
			mPs.put("cond_yn", "");
		} else if ("List".equals(name)) {
			mPs.put("list_yn", "");
		} else if ("Form".equals(name)) {
			mPs.put("form_yn", "");
		}
		mPs.put("login_user_id", login_user_id);
		datasetDAO.updateDatasetItemAreaReset(mPs); // UPDATE SYS_DATASET_ITEM [영역초기화]
		
		int cnt = 0;
		for(int i=0; i<list.size(); i++){
			Map<String, Object> map = list.get(i);
			map.put("dataset_id", dataset_id);
			map.put("login_user_id", login_user_id);
			
			if(0 < datasetDAO.selectDatasetItemCount(map)){
				cnt = datasetDAO.updateDatasetItem(map); // UPDATE SYS_DATASET_ITEM 
				if(cnt < 1){
					throw new NkiaException(messageSource.getMessage("msg.slms.contract.00010")); // 수정에 실패하였습니다.
				}
			}else{
				datasetDAO.insertDatasetItem(map); // INSERT SYS_DATASET_ITEM 
			}
		}
	}
}