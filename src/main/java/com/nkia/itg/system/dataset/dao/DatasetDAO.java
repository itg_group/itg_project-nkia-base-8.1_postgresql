/*
 * @(#)DatasetDAO.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("datasetDAO")
public class DatasetDAO extends EgovComAbstractDAO{

	/**
	 * 통합현황 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDataset(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("DatasetDAO.selectDataset", paramMap);
	}

	/**
	 * 프로그램 통합현황 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectProgramDataset(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("DatasetDAO.selectProgramDataset", paramMap);
	}

	/**
	 * 통합현황 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDatasetList(Map<String, Object> paramMap) throws NkiaException{
		return list("DatasetDAO.selectDatasetList", paramMap);
	}

	/**
	 * 통합현황 건수조회
	 * @param paramMap
	 */
	public int selectDatasetCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("DatasetDAO.selectDatasetCount", paramMap);
	}
	
	/**
	 * 통합현황 신규 Key값 조회
	 * @param paramMap
	 */
	public String selectDatasetNewId(Map<String, Object> paramMap) throws NkiaException {
		return (String) selectByPk("DatasetDAO.selectDatasetNewId", paramMap);
	}

	/**
	 * 통합현황 등록
	 * @param paramMap
	 */
	public void insertDataset(Map<String, Object> datasetInfo) throws NkiaException {
		this.insert("DatasetDAO.insertDataset", datasetInfo);
	}

	/**
	 * 통합현황 수정
	 * @param paramMap
	 */
	public void updateDataset(Map<String, Object> datasetInfo) throws NkiaException {
		this.update("DatasetDAO.updateDataset", datasetInfo);
	}

	/**
	 * 통합현황 삭제
	 * @param paramMap
	 */
	public void deleteDataset(Map<String, Object> paramMap) throws NkiaException {
		this.update("DatasetDAO.deleteDataset", paramMap);
	}

	/**
	 * 속성 건수조회
	 * @param paramMap
	 */
	public int selectDatasetItemCount(Map<String, Object> paramMap) throws NkiaException{
		return (Integer)selectByPk("DatasetDAO.selectDatasetItemCount", paramMap);
	}
	
	/**
	 * 속성 단건조회
	 * @param paramMap
	 */
	public Map<String, Object> selectDatasetItem(Map<String, Object> paramMap) throws NkiaException{
		return (Map<String, Object>)selectByPk("DatasetDAO.selectDatasetItem", paramMap);
	}
	
	/**
	 * 속성 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDatasetItemList(Map<String, Object> paramMap) throws NkiaException{
		return list("DatasetDAO.selectDatasetItemList", paramMap);
	}

	/**
	 * 속성 등록
	 * @param paramMap
	 */
	public void insertDatasetItem(Map<String, Object> datasetInfo) throws NkiaException {
		this.insert("DatasetDAO.insertDatasetItem", datasetInfo);
	}

	/**
	 * 속성 변경
	 * @param paramMap
	 */
	public int updateDatasetItem(Map<String, Object> datasetInfo) throws NkiaException {
		return this.update("DatasetDAO.updateDatasetItem", datasetInfo);
	}

	/**
	 * 속성 변경
	 * @param paramMap
	 */
	public int updateDatasetItemAreaReset(Map<String, Object> datasetInfo) throws NkiaException {
		return this.update("DatasetDAO.updateDatasetItemAreaReset", datasetInfo);
	}
	
	/**
	 * 속성 순번변경
	 * @param paramMap
	 */
	public void updateDatasetItemDispNo(Map<String, Object> datasetInfo) throws NkiaException {
		this.update("DatasetDAO.updateDatasetItemDispNo", datasetInfo);
	}

	/**
	 * 속성 삭제
	 * @param paramMap
	 */
	public void deleteDatasetItem(Map<String, Object> paramMap) throws NkiaException {
		this.delete("DatasetDAO.deleteDatasetItem", paramMap);
	}

	/**
	 * 속성 목록삭제
	 * @param paramMap
	 */
	public void deleteDatasetItemList(Map<String, Object> paramMap) throws NkiaException {
		this.delete("DatasetDAO.deleteDatasetItemList", paramMap);
	}
	
	/**
	 * 코드정보 조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> searchCodeDataList(Map<String, Object> paramMap) throws NkiaException {
		return list("DatasetDAO.searchCodeDataList", paramMap);
	}
	
	
	/**
	 * 통합현황 부서 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDatasetDataauthList(Map<String, Object> paramMap) throws NkiaException{
		return list("DatasetDAO.selectDatasetDataauthList", paramMap);
	}

	/**
	 * 통합현황 부서 건수조회
	 * @param paramMap
	 */
	public int selectDatasetDataauthCount(Map<String, Object> paramMap) throws NkiaException{
		return (Integer)selectByPk("DatasetDAO.selectDatasetDataauthCount", paramMap);
	}
	
	/**
	 * 통합현황 부서 수정
	 * @param paramMap
	 */
	public void deleteDatasetDataauthAll(Map<String, Object> datasetInfo) throws NkiaException {
		this.delete("DatasetDAO.deleteDatasetDataauthAll", datasetInfo);
	}
	
	/**
	 * 통합현황 부서 등록
	 * @param paramMap
	 */
	public void insertDatasetDataauth(Map<String, Object> paramMap) throws NkiaException {
		this.update("DatasetDAO.insertDatasetDataauth", paramMap);
	}
	
	/**
	 * 코드그룹 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectCodeGrpList(Map<String, Object> paramMap) throws NkiaException{
		return list("DatasetDAO.selectCodeGrpList", paramMap);
	}

	/**
	 * 코드그룹 건수조회
	 * @param paramMap
	 */
	public int selectCodeGrpCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("DatasetDAO.selectCodeGrpCount", paramMap);
	}

	/**
	 * 팝업 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectPopupList(Map<String, Object> paramMap) throws NkiaException{
		return list("DatasetDAO.selectPopupList", paramMap);
	}

	/**
	 * 팝업 건수조회
	 * @param paramMap
	 */
	public int selectPopupCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("DatasetDAO.selectPopupCount", paramMap);
	}
	
	/**
	 * 기본값 목록조회
	 * @param paramMap
	 */
	public List<Map<String, Object>> selectDefaultValueList(Map<String, Object> paramMap) throws NkiaException{
		return list("DatasetDAO.selectDefaultValueList", paramMap);
	}

	/**
	 * 기본값 건수조회
	 * @param paramMap
	 */
	public int selectDefaultValueCount(Map<String, Object> paramMap)throws NkiaException {
		return (Integer)selectByPk("DatasetDAO.selectDefaultValueCount", paramMap);
	}
	
}

