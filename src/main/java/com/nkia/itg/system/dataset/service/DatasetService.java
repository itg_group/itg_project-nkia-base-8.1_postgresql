/*
 * @(#)DatasetService.java              2018. 1. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.dataset.service;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DatasetService {

	public Map<String, Object> selectDataset(Map<String, Object> paramMap) throws NkiaException;

	public Map<String, Object> selectProgramDataset(Map<String, Object> paramMap) throws NkiaException;

	public List<Map<String, Object>> selectDatasetList(Map<String, Object> paramMap) throws NkiaException;

	public int selectDatasetCount(Map<String, Object> paramMap) throws NkiaException;
	
	public Map<String, Object> insertDataset(Map<String, Object> paramMap) throws NkiaException;
	
	public void updateDataset(Map<String, Object> paramMap) throws NkiaException;
	
	public void deleteDataset(Map<String, Object> paramMap) throws NkiaException;
	
	public void itemAutoCreate(Map<String, Object> paramMap) throws NkiaException;


	public List<Map<String, Object>> selectDatasetItemList(Map<String, Object> paramMap) throws NkiaException;

	public List<Map<String, Object>> selectDatasetDataauthList(Map<String, Object> paramMap) throws NkiaException;

	public List<Map<String, Object>> selectCodeGrpList(Map<String, Object> paramMap) throws NkiaException;

	public int selectCodeGrpCount(Map<String, Object> paramMap) throws NkiaException;


	public List<Map<String, Object>> selectPopupList(Map<String, Object> paramMap) throws NkiaException;

	public int selectPopupCount(Map<String, Object> paramMap) throws NkiaException;


	public List<Map<String, Object>> selectDefaultValueList(Map<String, Object> paramMap) throws NkiaException;

	public int selectDefaultValueCount(Map<String, Object> paramMap) throws NkiaException;
	
	
	public void insertDatasetItem(Map<String, Object> paramMap) throws NkiaException;

	public void updateDatasetItem(Map<String, Object> paramMap) throws NkiaException;

	public void updateDatasetItemDispNo(List<Map<String, Object>> list) throws NkiaException;
	
	public void deleteDatasetItem(Map<String, Object> paramMap) throws NkiaException;
	
	public void deleteDatasetItemList(List<Map<String, Object>> list) throws NkiaException;
	
	public void saveDatasetItemList(Map<String, Object> paramMap) throws NkiaException;
	
	public List searchCodeDataList(Map<String, Object> paramMap) throws NkiaException;
}
