package com.nkia.itg.system.amBookmark.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.amBookmark.dao.AmBookmarkDAO;
import com.nkia.itg.system.amBookmark.service.AmBookmarkService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("amBookmarkService")
public class AmBookmarkServiceImpl implements AmBookmarkService {
	private static final Logger logger = LoggerFactory.getLogger(AmBookmarkServiceImpl.class);
	
	@Resource(name = "amBookmarkDAO")
	public AmBookmarkDAO amBookmarkDAO;
	
	/**
	 * 나의 즐겨찾는 구성자원 리스트
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public List searchMyBookmarkCiList(HashMap paramMap) throws NkiaException {
		return amBookmarkDAO.searchMyBookmarkCiList(paramMap);
	}
	
	/**
	 * 나의 즐겨찾는 구성자원 등록
	 * 
     * @param paramMap
	 * @return 
	 * @throws NkiaException
	 */
	public void insertMyBookmarkCiList(HashMap paramMap) throws NkiaException {
		
		List gridList = (List)paramMap.get("grid_data");
		String userId = StringUtil.parseString(paramMap.get("user_id"));
		String updUserId = StringUtil.parseString(paramMap.get("upd_user_id"));
		
		if(gridList!=null && gridList.size()>0) {
			for(int i=0; i<gridList.size(); i++) {
				HashMap params = new HashMap();
				params = (HashMap)gridList.get(i);
				params.put("user_id", userId);
				params.put("upd_user_id", updUserId);
				amBookmarkDAO.insertMyBookmarkCiList(params);
			}
		}
	}
	
	/**
	 * 나의 즐겨찾는 구성자원 삭제
	 * 
     * @param paramMap
	 * @return 
	 * @throws NkiaException
	 */
	public void deleteMyBookmarkCiList(HashMap paramMap) throws NkiaException {
		
		List gridList = (List)paramMap.get("grid_data");
		String userId = StringUtil.parseString(paramMap.get("user_id"));
		String updUserId = StringUtil.parseString(paramMap.get("upd_user_id"));
		
		if(gridList!=null && gridList.size()>0) {
			for(int i=0; i<gridList.size(); i++) {
				HashMap params = new HashMap();
				params = (HashMap)gridList.get(i);
				params.put("user_id", userId);
				params.put("upd_user_id", updUserId);
				amBookmarkDAO.deleteMyBookmarkCiList(params);
			}
		}
	}
	
}
