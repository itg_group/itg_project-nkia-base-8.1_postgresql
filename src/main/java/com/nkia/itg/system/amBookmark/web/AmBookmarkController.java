/*
 * @(#)AmBookmarkController.java              2018. 4. 9.
 *
 * @jyjeong@nakia.co.kr
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.amBookmark.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.amBookmark.service.AmBookmarkService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class AmBookmarkController {
	
	@Resource(name = "amBookmarkService")
	private AmBookmarkService amBookmarkService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 즐겨찾는 CI 관리 화면 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/amBookmark/goAmBookmark.do")
	public String goAmdbServiceBsc(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		
		String forwarPage = "/itg/system/amBookmark/amBookmark.nvf";
		return forwarPage;
	}
	
	/**
	 * 나의 즐겨찾는 구성자원 리스트
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/amBookmark/searchMyBookmarkCiList.do")
	public @ResponseBody ResultVO searchMyBookmarkCiList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = null;
			GridVO gridVO = new GridVO();
//			int totalCount = 0;
//			
//			PagingUtil.getFristEndNum(paramMap);
//			
//			totalCount = amBookmarkService.searchAmdbServiceBscListCount(paramMap);
			resultList = amBookmarkService.searchMyBookmarkCiList(paramMap);		
			
//			gridVO.setTotalCount(totalCount);
//			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 나의 즐겨찾는 구성자원 등록
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/amBookmark/insertMyBookmarkCiList.do")
	public @ResponseBody ResultVO insertMyBookmarkCiList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			amBookmarkService.insertMyBookmarkCiList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 나의 즐겨찾는 구성자원 삭제
	 * 
     * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/amBookmark/deleteMyBookmarkCiList.do")
	public @ResponseBody ResultVO deleteMyBookmarkCiList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO= (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			amBookmarkService.deleteMyBookmarkCiList(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
}
