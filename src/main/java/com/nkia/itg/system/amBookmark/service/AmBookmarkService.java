package com.nkia.itg.system.amBookmark.service;

import java.util.HashMap;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AmBookmarkService {
	
	/**
	 * 나의 즐겨찾는 구성자원 리스트
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public List searchMyBookmarkCiList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 나의 즐겨찾는 구성자원 등록
	 * 
     * @param paramMap
	 * @return 
	 * @throws NkiaException
	 */
	public void insertMyBookmarkCiList(HashMap paramMap) throws NkiaException;
	
	/**
	 * 나의 즐겨찾는 구성자원 삭제
	 * 
     * @param paramMap
	 * @return 
	 * @throws NkiaException
	 */
	public void deleteMyBookmarkCiList(HashMap paramMap) throws NkiaException;
	
}
