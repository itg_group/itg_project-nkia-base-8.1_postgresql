package com.nkia.itg.system.amBookmark.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("amBookmarkDAO")
public class AmBookmarkDAO  extends EgovComAbstractDAO {
	
	/**
	 * 나의 즐겨찾는 구성자원 리스트
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public List searchMyBookmarkCiList(HashMap paramMap) throws NkiaException {
		return list("AmBookmarkDAO.searchMyBookmarkCiList", paramMap);
	}
	
	/**
	 * 나의 즐겨찾는 구성자원 등록
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public void insertMyBookmarkCiList(HashMap paramMap) throws NkiaException {
		insert("AmBookmarkDAO.insertMyBookmarkCiList", paramMap);
	}
	
	/**
	 * 나의 즐겨찾는 구성자원 삭제
	 * 
     * @param paramMap
	 * @return int
	 * @throws NkiaException
	 */
	public void deleteMyBookmarkCiList(HashMap paramMap) throws NkiaException {
		delete("AmBookmarkDAO.deleteMyBookmarkCiList", paramMap);
	}
	
}
