package com.nkia.itg.system.notification.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.notification.dao.NotificationManagerDAO;
import com.nkia.itg.system.notification.service.NotificationManagerService;

@Service("notificationManagerService")
public class NotificationManagerServiceImpl implements NotificationManagerService{
	
	private static final Logger logger = LoggerFactory.getLogger(NotificationManagerServiceImpl.class);
	
	@Resource(name = "notificationManagerDAO")
	public NotificationManagerDAO notificationManagerDAO;

	@Override
	public int selectNotificationListtCount(Map paramMap) throws NkiaException {
		return notificationManagerDAO.selectNotificationListtCount(paramMap);
	}

	@Override
	public List selectNotificationList(Map paramMap) throws NkiaException {
		return notificationManagerDAO.selectNotificationList(paramMap); 
	}

	@Override
	public Map searchNotificationInfo(Map paramMap) throws NkiaException, SQLException {
		return notificationManagerDAO.searchNotificationInfo(paramMap);
	}

	@Override
	public void registNotification(Map paramMap) throws NkiaException {
	
		notificationManagerDAO.registNotification(paramMap);
	}

	@Override
	public void updateNotification(Map paramMap) throws NkiaException {
		notificationManagerDAO.updateNotification(paramMap);
	}

	@Override
	public void deleteNotification(Map paramMap) throws NkiaException {
		notificationManagerDAO.deleteNotification(paramMap);
	}

	@Override
	public int selectNotificationBoxCount(Map paramMap) throws NkiaException {
		return notificationManagerDAO.selectNotificationBoxCount(paramMap);
	}

	@Override
	public int selectNotificationBoxListCount(Map paramMap) throws NkiaException {
		return notificationManagerDAO.selectNotificationBoxListCount(paramMap);
	}

	@Override
	public List selectNotificationBoxList(Map paramMap) throws NkiaException {
		return notificationManagerDAO.selectNotificationBoxList(paramMap); 
	}
	
	@Override
	public void updateNotificationBoxCnfirmYn(Map paramMap) throws NkiaException {
		notificationManagerDAO.updateNotificationBoxCnfirmYn(paramMap);
	}
			
}
