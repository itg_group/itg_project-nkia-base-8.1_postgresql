package com.nkia.itg.system.notification.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.notification.service.NotificationManagerService;
import com.tmax.tibero.jdbc.TbClob;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class NotificationManagerController {

	@Resource(name = "notificationManagerService")
	private NotificationManagerService notificationManagerService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 알림 설정 페이지 호출
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/goNotificationManager.do")
	public String goNotificationManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/notification/notificationManager.nvf";
		return forwarPage;
	}
	
	/**
	 * 알림 설정 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/selectNotificationList.do")  
	public @ResponseBody ResultVO selectNotificationList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = notificationManagerService.selectNotificationListtCount(paramMap);
			List resultList = notificationManagerService.selectNotificationList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 알림 상세 정보 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 * @throws SQLException 
	 */
	@RequestMapping(value="/itg/system/notification/searchNotificationInfo.do")  
	public @ResponseBody ResultVO searchNotificationInfo(@RequestBody ModelMap paramMap)throws NkiaException, SQLException {
		ResultVO resultVO = new ResultVO();
		HashMap resultMap = new HashMap();
		try {
			resultMap = (HashMap) notificationManagerService.searchNotificationInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	public static String tbClobToString(TbClob tbClob) throws NkiaException, IOException, SQLException {
		      StringBuffer s = new StringBuffer();
		      BufferedReader br = new BufferedReader(tbClob.getCharacterStream());
		      String ts = "";
		      while((ts = br.readLine()) != null) {
		         s.append(ts + "\n");
		      }
		      br.close();
		      return s.toString();
		    }
	
	/**
	 * 알림 설정 등록
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/registNotification.do")
	public @ResponseBody ResultVO registNotification(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			notificationManagerService.registNotification(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 알림 설정 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/updateNotification.do")
	public @ResponseBody ResultVO updateNotification(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			notificationManagerService.updateNotification(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 알림 설정 삭제
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/deleteNotification.do")
	public @ResponseBody ResultVO deleteNotification(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			notificationManagerService.deleteNotification(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 알림함 카운트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/selectNotificationBoxCount.do")  
	public @ResponseBody ResultVO selectNotificationBoxCount(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			int result = notificationManagerService.selectNotificationBoxCount(paramMap);
			resultVO.setResultInt(result);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 알림함 목록 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/selectNotificationBoxList.do")  
	public @ResponseBody ResultVO selectNotificationBoxList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = notificationManagerService.selectNotificationBoxListCount(paramMap);
			List resultList = notificationManagerService.selectNotificationBoxList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 알림 설정 수정
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/notification/updateNotificationBoxCnfirmYn.do")
	public @ResponseBody ResultVO updateNotificationBoxCnfirmYn(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			paramMap.put("upd_user_id", userVO.getUser_id());
			notificationManagerService.updateNotificationBoxCnfirmYn(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		
		} catch (NkiaException e) {
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00011"));
			throw new NkiaException(e);
		}
		return resultVO;
	}
}
