package com.nkia.itg.system.notification.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface NotificationManagerService {

	int selectNotificationListtCount(Map paramMap) throws NkiaException;

	List selectNotificationList(Map paramMap) throws NkiaException;

	Map searchNotificationInfo(Map paramMap) throws NkiaException, SQLException;

	void registNotification(Map paramMap) throws NkiaException;

	void updateNotification(Map paramMap) throws NkiaException;

	void deleteNotification(Map paramMap) throws NkiaException;

	int selectNotificationBoxCount(Map paramMap) throws NkiaException;

	int selectNotificationBoxListCount(Map paramMap) throws NkiaException;

	List selectNotificationBoxList(Map paramMap) throws NkiaException;

	void updateNotificationBoxCnfirmYn(Map paramMap) throws NkiaException;
}
