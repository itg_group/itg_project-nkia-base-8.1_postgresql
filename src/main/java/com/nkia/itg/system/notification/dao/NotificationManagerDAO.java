package com.nkia.itg.system.notification.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("notificationManagerDAO")
public class NotificationManagerDAO extends EgovComAbstractDAO{

	public int selectNotificationListtCount(Map paramMap) {
		return (Integer) this.selectByPk("NotificationManagerDAO.selectNotificationListCount", paramMap);
	}
	
	public List selectNotificationList(Map paramMap) {
		return this.list("NotificationManagerDAO.selectNotificationList", paramMap);
	}

	public Map searchNotificationInfo(Map paramMap) throws NkiaException, SQLException {
		//@@최양규 20160715 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 시작		
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("NotificationManagerDAO.searchNotificationInfo", paramMap, handler);
		return handler.getReturnMap();
		//@@최양규 20160715 리스트 조회시 Clob 도 가져올수 있게끔 핸들러 사용 종료
	}
	
	public void registNotification(Map paramMap) {
		insert("NotificationManagerDAO.registNotification", paramMap);
	}

	public void updateNotification(Map paramMap) {
		update("NotificationManagerDAO.updateNotification", paramMap);
	}

	public void deleteNotification(Map paramMap) {
		delete("NotificationManagerDAO.deleteNotification", paramMap);
	}

	public int selectNotificationBoxCount(Map paramMap) {
		return (Integer) (this.selectByPk("NotificationManagerDAO.selectNotificationBoxCount", paramMap));
	}

	public int selectNotificationBoxListCount(Map paramMap) {
		return (Integer) this.selectByPk("NotificationManagerDAO.selectNotificationBoxListCount", paramMap);
	}

	public List selectNotificationBoxList(Map paramMap) {
		return this.list("NotificationManagerDAO.selectNotificationBoxList", paramMap);
	}
	
	public void updateNotificationBoxCnfirmYn(Map paramMap) {
		update("NotificationManagerDAO.updateNotificationBoxCnfirmYn", paramMap);
	}
}
