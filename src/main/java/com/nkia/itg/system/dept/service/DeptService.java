package com.nkia.itg.system.dept.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface DeptService {
	
	public List searchStaticAllDeptTree(ModelMap paramMap) throws NkiaException;
	
	public List searchStaticAllDeptTreeComp(ModelMap paramMap) throws NkiaException;
	
	public HashMap searchDeptBasicInfo(ModelMap paramMap) throws NkiaException;
	
	public void deleteDeptInfo(ModelMap paramMap) throws NkiaException;
	
	public List searchDeptAllId(ModelMap paramMap) throws NkiaException;
	
	public void updateDeptInfo(ModelMap paramMap) throws NkiaException;
	
	public void updateDeptUseAll(ModelMap paramMap) throws NkiaException;
	
	public void insertDeptInfo(ModelMap paramMap) throws NkiaException;
	
	public int searchDeptOrderListCount(ModelMap paramMap) throws NkiaException;
	
	public List searchDeptOrderList(ModelMap paramMap) throws NkiaException;
	
	public String selectCheckUserId(ModelMap paramMap) throws NkiaException;
	
	public List searchRequstAreaList(Map paramMap) throws NkiaException;
	
	public List searchDeptSearchAuthList(ModelMap paramMap) throws NkiaException;
	
	public String selectSearchCustAuth(String user_id) throws NkiaException;
	
	public List searchStaticDeptCustAuthTree(ModelMap paramMap) throws NkiaException;
	
	public List searchStaticDeptCustAuthList(ModelMap paramMap) throws NkiaException;
}
