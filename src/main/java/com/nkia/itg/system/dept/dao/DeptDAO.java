package com.nkia.itg.system.dept.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("deptDAO")
public class DeptDAO extends EgovComAbstractDAO {

	public List searchStaticAllDeptTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("DeptDAO.searchStaticAllDeptTree", paramMap);
		return resultList;
	}
	
	public List searchStaticAllDeptTreeComp(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("DeptDAO.searchStaticAllDeptTreeComp", paramMap);
		return resultList;
	}
	
	public HashMap searchDeptBasicInfo(ModelMap paramMap) throws NkiaException {
		return (HashMap)selectByPk("DeptDAO.searchDeptBasicInfo", paramMap);
	}
	
	public void deleteDeptInfo(ModelMap paramMap) throws NkiaException {
		this.delete("DeptDAO.deleteDeptInfo", paramMap);
	}
	
	public void updateDeptInfo(HashMap paramMap) throws NkiaException {
		this.update("DeptDAO.updateDeptInfo", paramMap);
	}
	
	public void updateDeptUseAll(HashMap paramMap) throws NkiaException {
		this.update("DeptDAO.updateDeptUseAll", paramMap);
	}
	
	public void updateDeptOrder(HashMap paramMap) throws NkiaException {
		this.update("DeptDAO.updateDeptOrder", paramMap);
	}
	
	public String insertDeptInfo(ModelMap paramMap) throws NkiaException {
		return (String) this.insert("DeptDAO.insertDeptInfo", paramMap);
	}
	
	public int searchDeptOrderListCount(ModelMap paramMap) throws NkiaException {
		int count = (Integer) this.selectByPk("DeptDAO.searchDeptOrderListCount", paramMap);
		return count;
	}
	
	public List searchDeptOrderList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("DeptDAO.searchDeptOrderList", paramMap);
		return resultList;
	}
	
	public List searchDeptAllId(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("DeptDAO.searchDeptAllId", paramMap);
		return resultList;
	}
	
	public String selectCheckUserId(ModelMap paramMap) throws NkiaException {
		return (String) this.selectByPk("DeptDAO.selectCheckUserId", paramMap);
	}
	
	public List searchRequstAreaList(Map paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("DeptDAO.searchRequstAreaList", paramMap);
		return resultList;
	}
	
	public List searchDeptSearchAuthList(ModelMap paramMap) throws NkiaException {
		List resultList = this.list("DeptDAO.searchDeptSearchAuthList", paramMap);
		return resultList;
	}
	
	public void deleteDeptAuth(HashMap paramMap) throws NkiaException {
		delete("DeptDAO.deleteDeptAuth", paramMap);
	}	

	public void insertDeptAuth(Map paramMap) throws NkiaException {
		insert("DeptDAO.insertDeptAuth", paramMap);
	}
	
	public String selectSearchCustAuth(String user_id) throws NkiaException {
		return (String) this.selectByPk("DeptDAO.selectSearchCustAuth", user_id);
	}
	
	public List searchStaticDeptCustAuthTree(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("DeptDAO.searchStaticDeptCustAuthTree", paramMap);
		return resultList;
	}
	
	public void deleteDeptInfoTest(ModelMap paramMap) throws NkiaException {
		this.delete("DeptDAO.deleteDeptInfoTest", paramMap);
	}
	
	public void deleteDeptAuthTest(HashMap paramMap) throws NkiaException {
		delete("DeptDAO.deleteDeptAuthTest", paramMap);
	}
	
	public List searchStaticDeptCustAuthList(ModelMap paramMap) throws NkiaException {
		List resultList = null;
		resultList = this.list("DeptDAO.searchStaticDeptCustAuthList", paramMap);
		return resultList;
	}
}
