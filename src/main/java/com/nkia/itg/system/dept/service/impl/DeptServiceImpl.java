package com.nkia.itg.system.dept.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.dept.dao.DeptDAO;
import com.nkia.itg.system.dept.service.DeptService;


@Service("deptService")
public class DeptServiceImpl implements DeptService{
	
	private static final Logger logger = LoggerFactory.getLogger(DeptServiceImpl.class);
	
	@Resource(name = "deptDAO")
	public DeptDAO deptDAO;
	
	public List searchStaticAllDeptTree(ModelMap paramMap) throws NkiaException {
		return deptDAO.searchStaticAllDeptTree(paramMap);
	}
	
	public List searchStaticAllDeptTreeComp(ModelMap paramMap) throws NkiaException {
		return deptDAO.searchStaticAllDeptTreeComp(paramMap);
	}
	
	public HashMap searchDeptBasicInfo(ModelMap paramMap) throws NkiaException {
		return deptDAO.searchDeptBasicInfo(paramMap);
	}
	
	public void deleteDeptInfo(ModelMap paramMap) throws NkiaException {
		deptDAO.deleteDeptAuthTest(paramMap);
		deptDAO.deleteDeptInfoTest(paramMap);		
	}
	
	public void updateDeptInfo(ModelMap paramMap) throws NkiaException {
		HashMap deptInfo = (HashMap)paramMap.get("deptInfo");     //부서정보 업데이트
		List orderDeptList = (List)paramMap.get("orderDeptList");    //하위부서  리스트
		
		deptDAO.updateDeptInfo(deptInfo);
		deptDAO.updateDeptUseAll(deptInfo);
		
		//부서검색권한 정보
		String custSearchAuth = (String) deptInfo.get("cust_search_auth");
		
		//부서검색권한이 개별부서인 경우
		if (custSearchAuth.equals("IND")) {
			deptDAO.deleteDeptAuth(deptInfo);
			
			List authDept = (List)deptInfo.get("authDept");
			if (authDept != null) {
				int authDeptLength = authDept.size();
				for (int i=0; i<authDeptLength; i++) {
					Map insertMap = new HashMap();
					
					insertMap.put("cust_id", deptInfo.get("cust_id"));
					insertMap.put("auth_cust_id", (String)authDept.get(i));
					insertMap.put("upd_user_id", paramMap.get("upd_user_id"));
					
					deptDAO.insertDeptAuth(insertMap);
				}
			}
		} else if (custSearchAuth.equals("ALL")) {
			deptDAO.deleteDeptAuth(deptInfo);
		}
		
		//하위부서 순서변경
		if(orderDeptList != null){
			
			for(int i = 0; i<orderDeptList.size(); i++){
				HashMap orderDeptData = (HashMap)orderDeptList.get(i);
				
				orderDeptData.put("cust_id", orderDeptData.get("CUST_ID"));
				orderDeptData.put("display_no", i+1);
				orderDeptData.put("upd_user_id", paramMap.get("upd_user_id"));
				
				deptDAO.updateDeptOrder(orderDeptData);				
			}
		}
	}
	
	public void updateDeptUseAll(ModelMap paramMap) throws NkiaException {
		deptDAO.updateDeptUseAll(paramMap);
	}
	
	public void insertDeptInfo(ModelMap paramMap) throws NkiaException {
		String custId = deptDAO.insertDeptInfo(paramMap);
		paramMap.addAttribute("cust_id", custId);
		
		//부서검색권한 정보
		String custSearchAuth = (String) paramMap.get("cust_search_auth");
		
		//부서검색권한이 개별부서인 경우
		if (custSearchAuth.equals("IND")) {
			List authDept = (List)paramMap.get("authDept");
			if (authDept != null) {
				int authDeptLength = authDept.size();
				for (int i=0; i<authDeptLength; i++) {
					Map insertMap = new HashMap();
					
					insertMap.put("cust_id", custId);
					if ("-".equals((String)authDept.get(i))) {
						insertMap.put("auth_cust_id", custId);
					} else {
						insertMap.put("auth_cust_id", (String)authDept.get(i));
					}
					insertMap.put("upd_user_id", paramMap.get("upd_user_id"));
					
					deptDAO.insertDeptAuth(insertMap);
				}
			}
		}
	}
	
	public int searchDeptOrderListCount(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return deptDAO.searchDeptOrderListCount(paramMap);
	}
	
	public List searchDeptOrderList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return deptDAO.searchDeptOrderList(paramMap);
	}
	
	public List searchDeptAllId(ModelMap paramMap) throws NkiaException {
		return deptDAO.searchDeptAllId(paramMap);
	}
	
	public String selectCheckUserId(ModelMap paramMap) throws NkiaException {
		return deptDAO.selectCheckUserId(paramMap);
	}
	
	public List searchRequstAreaList(Map paramMap) throws NkiaException {
		return deptDAO.searchRequstAreaList(paramMap);
	}
	
	/**
	 * 부서검색권한 :개별로 설정된 부서 리스트
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchDeptSearchAuthList(ModelMap paramMap) throws NkiaException {
		return deptDAO.searchDeptSearchAuthList(paramMap);
	}
	
	/**
	 * 로그인 계정을 조건으로 부서검색권한 조회
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public String selectSearchCustAuth(String user_id) throws NkiaException {
		return deptDAO.selectSearchCustAuth(user_id);
	}
	
	public List searchStaticDeptCustAuthTree(ModelMap paramMap) throws NkiaException {
		return deptDAO.searchStaticDeptCustAuthTree(paramMap);
	}
	
	public List searchStaticDeptCustAuthList(ModelMap paramMap) throws NkiaException {
		return deptDAO.searchStaticDeptCustAuthList(paramMap);
	}
}
