package com.nkia.itg.system.dept.web;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.dept.service.DeptService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class DeptController {
	
	@Resource(name = "deptService")
	private DeptService deptService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 부서관리페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/dept/goDeptManager.do")
	public String deptManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/dept/deptManager.nvf";
		return forwarPage;
	}
		
	
	/**
	 * 부서관리트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/searchStaticAllDeptTree.do")
	public @ResponseBody ResultVO searchStaticAllDeptTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = null;
		
		try {
			if (paramMap.get("is_all_tree") != null && !paramMap.get("is_all_tree").equals("Y")) {
				loginUserId = userVO.getUser_id();
				paramMap.addAttribute("login_user_id", loginUserId);
			} else {
				paramMap.addAttribute("login_user_id", loginUserId);
			}
			List resultList = deptService.searchStaticAllDeptTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 컴퍼넌트용 부서관리트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/searchStaticAllDeptTreeComp.do")
	public @ResponseBody ResultVO searchStaticAllDeptTreeComp(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			TreeVO treeVO = null;
			List resultList = deptService.searchStaticAllDeptTreeComp(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서정보 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/searchDeptBasicInfo.do")
	public @ResponseBody ResultVO searchDeptBasicInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			HashMap resultMap = deptService.searchDeptBasicInfo(paramMap);
			resultVO.setResultMap(resultMap);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 부서정보 삭제
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/deleteDeptInfo.do")
	public @ResponseBody ResultVO deleteDeptInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			deptService.deleteDeptInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서정보 수정
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/updateDeptInfo.do")
	public @ResponseBody ResultVO updateDeptInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
			deptService.updateDeptInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위 부서정보 순서 수정
     * @param paramMap
	 * @return
	 * @
	 */
	/*@RequestMapping(value="/itg/system/dept/updateDeptOrder.do")
	public @ResponseBody ResultVO updateDeptOrder(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}	
			deptService.updateDeptOrder(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}*/
	
	/**
	 * 부서정보 등록
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/insertDeptInfo.do")
	public @ResponseBody ResultVO insertDeptInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		paramMap.put("upd_user_id", userVO.getUser_id());
	    	}
	    	
			deptService.insertDeptInfo(paramMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00004"));
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 하위부서리스트
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/searchDeptOrderList.do")
	public @ResponseBody ResultVO searchDeptOrderList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		try{
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);

			totalCount = deptService.searchDeptOrderListCount(paramMap);
			List resultList = deptService.searchDeptOrderList(paramMap);
			
 			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서에 포함된 사용자 유무 체크
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/selectCheckUserId.do")
	public @ResponseBody ResultVO selectCheckUserId(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO(); 
		try{
			String checkValue = deptService.selectCheckUserId(paramMap);
			resultVO.setResultString(checkValue);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/system/dept/searchRequstAreaList.do")
	public @ResponseBody ResultVO searchRequstAreaList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = deptService.searchRequstAreaList(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서검색권한 :개별로 설정된 부서 리스트
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/searchDeptSearchAuthList.do")
	public @ResponseBody ResultVO searchDeptAuthList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			PagingUtil.getFristEndNum(paramMap);
			List resultList = deptService.searchDeptSearchAuthList(paramMap);
			
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서관리트리 호출
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/system/dept/searchStaticDeptCustAuthTree.do")
	public @ResponseBody ResultVO searchStaticDeptCustAuthTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		String loginUserId = null;
		
		try {
			if (paramMap.get("is_all_tree") != null && !paramMap.get("is_all_tree").equals("Y")) {
				loginUserId = userVO.getUser_id();
				paramMap.addAttribute("login_user_id", loginUserId);
			} else if (paramMap.get("is_tree_view").equals("preview") ) {
				loginUserId = userVO.getUser_id();
				paramMap.addAttribute("login_user_id", loginUserId);
			} else {
				paramMap.addAttribute("login_user_id", loginUserId);
			}
			
			List resultList = deptService.searchStaticDeptCustAuthTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
			
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서관리목록 호출
	 *  - 부서와 검색부서 함께
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/dept/searchStaticDeptCustAuthList.do")
	public @ResponseBody ResultVO searchStaticDeptCustAuthList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		GridVO gridVO = new GridVO();
		
		try {
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String loginUserId = userVO.getUser_id();
			paramMap.addAttribute("login_user_id", loginUserId);
			
			//로그인 사용자의 부서 정보를 조건으로 부서검색권한 조회
			String search_cust_auth = deptService.selectSearchCustAuth(loginUserId);
			paramMap.addAttribute("search_cust_auth", search_cust_auth);
			
			List resultList = deptService.searchStaticDeptCustAuthList(paramMap);
			
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(NkiaException e) {
			throw new NkiaException(e);
		}
		
		return resultVO;
	}
}
