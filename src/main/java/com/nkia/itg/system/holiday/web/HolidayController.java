/*
 * @(#)HolidayController.java              2019. 11. 08.
 *
 * Copyright 2019 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.holiday.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.holiday.service.HolidayService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * 공휴일관리 Controller
 * 
 * @author NKIA
 */
@Controller
public class HolidayController {

	@Resource(name = "holidayService")
	private HolidayService holidayService;

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;

	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;

	/**
	 * 공휴일관리 화면
	 * 
	 * @param request
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/holiday/holidayManager.do")
	public String holidayManager(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		// 2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}
		String forwarPage = "/itg/system/holiday/holidayManager.nvf";
		return forwarPage;
	}

	/**
	 * 공휴일관리 리스트 호출
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/holiday/searchHoliday.do")
	public @ResponseBody ResultVO searchHoliday(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			List resultList = new ArrayList();
			GridVO gridVO = new GridVO();
			int totalCount = 0;

			// @@ 페이징 관련 내용
			PagingUtil.getFristEndNum(paramMap);
			// 공휴일 리스트
			resultList = holidayService.searchHoliday(paramMap);
			// 공휴일 카운트 수
			totalCount = holidayService.searchHolidayCount(paramMap);

			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer) paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);

			resultVO.setGridVO(gridVO);
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 공휴일관리 일괄등록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/holiday/insertHoliday.do")
	public @ResponseBody ResultVO insertHoliday(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();

			if (isAuthenticated) {
				UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}

			int insertedCount = 0;

			// 등록된 년도 유무 파악
			insertedCount = holidayService.selectHolidayYearCount(paramMap);

			if (insertedCount > 0) {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00012"));
			} else {
				int insertCount = 0;

				// SYS_CALENDAR 에 해당 년도 유무 파악
				insertCount = holidayService.selectCalendarYearCount(paramMap);

				if (insertCount > 0) {
					// 공휴일관리 일괄등록
					holidayService.insertHoliday(paramMap);
					resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00001"));
				} else {
					resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00013"));
				}
			}
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 공휴일관리 수정
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/holiday/updateHoliday.do")
	public @ResponseBody ResultVO updateHoliday(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();

			if (isAuthenticated) {
				UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}

			// 공휴일관리 수정
			holidayService.updateHoliday(paramMap);

			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00002"));
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 공휴일관리 일괄삭제
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/holiday/deleteHoliday.do")
	public @ResponseBody ResultVO deleteHoliday(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();

			if (isAuthenticated) {
				UserVO userVO = (UserVO) EgovUserDetailsHelper.getAuthenticatedUser();
				paramMap.put("login_user_id", userVO.getUser_id());
			}

			int insertedCount = 0;

			// 등록된 년도 유무 파악
			insertedCount = holidayService.selectHolidayYearCount(paramMap);

			if (insertedCount > 0) {
				// 공휴일 이력 등록
				holidayService.insertHolidayHistory(paramMap);

				// 공휴일관리 일괄삭제
				holidayService.deleteHoliday(paramMap);

				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00003"));
			} else {
				resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00014"));
			}

		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

	/**
	 * 선택한 공휴일 상세정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/system/holiday/selectHoliday.do")
	public @ResponseBody ResultVO selectHoliday(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try {
			// 선택한 공휴일 상세정보
			HashMap resultMap = holidayService.selectHoliday(paramMap);

			resultVO.setResultMap(resultMap);
			resultVO.setResultMsg(messageSource.getMessage("msg.common.error.00000"));
		} catch (NkiaException e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}

}