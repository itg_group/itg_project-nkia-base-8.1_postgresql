/*
 * @(#)HolidayDAO.java              2019. 11. 08.
 *
 * Copyright 2019 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.holiday.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

/**
 * 공휴일관리 DAO
 * 
 * @author NKIA
 */
@Repository("holidayDAO")
public class HolidayDAO extends EgovComAbstractDAO {

	/**
	 * 공휴일관리 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchHoliday(ModelMap paramMap) throws NkiaException {
		return list("HolidayDAO.searchHoliday", paramMap);
	}

	/**
	 * 공휴일관리 카운트 수
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchHolidayCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("HolidayDAO.searchHolidayCount", paramMap);
	}

	/**
	 * 공휴일관리 일괄등록
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertHoliday(ModelMap paramMap) throws NkiaException {
		this.insert("HolidayDAO.insertHoliday", paramMap);
	}

	/**
	 * 공휴일관리 수정
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateHoliday(ModelMap paramMap) throws NkiaException {
		this.update("HolidayDAO.updateHoliday", paramMap);
	}

	/**
	 * 공휴일관리 일괄삭제
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteHoliday(ModelMap paramMap) throws NkiaException {
		this.delete("HolidayDAO.deleteHoliday", paramMap);
	}

	/**
	 * 선택한 공휴일 상세정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectHoliday(ModelMap paramMap) throws NkiaException {
		return (HashMap) selectByPk("HolidayDAO.selectHoliday", paramMap);
	}

	/**
	 * 등록된 년도 유무 파악
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectHolidayYearCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("HolidayDAO.selectHolidayYearCount", paramMap);
	}

	/**
	 * SYS_CALENDAR 에 해당 년도 유무 파악
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectCalendarYearCount(ModelMap paramMap) throws NkiaException {
		return (Integer) selectByPk("HolidayDAO.selectCalendarYearCount", paramMap);
	}

	/**
	 * 공휴일 이력 등록
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertHolidayHistory(ModelMap paramMap) throws NkiaException {
		this.insert("HolidayDAO.insertHolidayHistory", paramMap);
	}

}