/*
 * @(#)HolidayService.java              2019. 11. 08.
 *
 * Copyright 2019 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.holiday.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * 공휴일관리 SERVICE
 * 
 * @author NKIA
 */
public interface HolidayService {

	/**
	 * 공휴일관리 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchHoliday(ModelMap paramMap) throws NkiaException;

	/**
	 * 공휴일관리 카운트 수
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int searchHolidayCount(ModelMap paramMap) throws NkiaException;

	/**
	 * 공휴일관리 일괄등록
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertHoliday(ModelMap paramMap) throws NkiaException;

	/**
	 * 공휴일관리 수정
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void updateHoliday(ModelMap paramMap) throws NkiaException;

	/**
	 * 공휴일관리 일괄삭제
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void deleteHoliday(ModelMap paramMap) throws NkiaException;

	/**
	 * 선택한 공휴일 상세정보
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectHoliday(ModelMap paramMap) throws NkiaException;

	/**
	 * 등록된 년도 유무 파악
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectHolidayYearCount(ModelMap paramMap) throws NkiaException;

	/**
	 * SYS_CALENDAR 에 해당 년도 유무 파악
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public int selectCalendarYearCount(ModelMap paramMap) throws NkiaException;

	/**
	 * 공휴일 이력 등록
	 * 
	 * @param paramMap
	 * @throws NkiaException
	 */
	public void insertHolidayHistory(ModelMap paramMap) throws NkiaException;

}