/*
 * @(#)HolidayServiceImpl.java              2019. 11. 08.
 *
 * Copyright 2019 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.system.holiday.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.holiday.dao.HolidayDAO;
import com.nkia.itg.system.holiday.service.HolidayService;

/**
 * 공휴일관리 SERVICEIMPL
 * 
 * @author NKIA
 */
@Service("holidayService")
public class HolidayServiceImpl implements HolidayService {

	private static final Logger logger = LoggerFactory.getLogger(HolidayServiceImpl.class);

	@Resource(name = "holidayDAO")
	public HolidayDAO holidayDAO;

	/**
	 * 공휴일관리 리스트
	 */
	public List searchHoliday(ModelMap paramMap) throws NkiaException {
		return holidayDAO.searchHoliday(paramMap);
	}

	/**
	 * 공휴일관리 카운트 수
	 */
	public int searchHolidayCount(ModelMap paramMap) throws NkiaException {
		return holidayDAO.searchHolidayCount(paramMap);
	}

	/**
	 * 공휴일관리 일괄등록
	 */
	public void insertHoliday(ModelMap paramMap) throws NkiaException {
		holidayDAO.insertHoliday(paramMap);
	}

	/**
	 * 공휴일관리 수정
	 */
	public void updateHoliday(ModelMap paramMap) throws NkiaException {
		holidayDAO.updateHoliday(paramMap);
	}

	/**
	 * 공휴일관리 일괄삭제
	 */
	public void deleteHoliday(ModelMap paramMap) throws NkiaException {
		holidayDAO.deleteHoliday(paramMap);
	}

	/**
	 * 선택한 공휴일 상세정보
	 */
	public HashMap selectHoliday(ModelMap paramMap) throws NkiaException {
		return holidayDAO.selectHoliday(paramMap);
	}

	/**
	 * 등록된 년도 유무 파악
	 */
	public int selectHolidayYearCount(ModelMap paramMap) throws NkiaException {
		return holidayDAO.selectHolidayYearCount(paramMap);
	}

	/**
	 * SYS_CALENDAR 에 해당 년도 유무 파악
	 */
	public int selectCalendarYearCount(ModelMap paramMap) throws NkiaException {
		return holidayDAO.selectCalendarYearCount(paramMap);
	}

	/**
	 * 공휴일 이력 등록
	 */
	public void insertHolidayHistory(ModelMap paramMap) throws NkiaException {
		holidayDAO.insertHolidayHistory(paramMap);
	}

}