/*
 * @(#)GuiDesignerUtil.java              2013. 3. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.gui.designer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.file.FileUtils;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.processFlowChart.service.ProcessFlowChartService;
import com.nkia.itg.nbpm.provide.common.service.NbpmProviderService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

public class GuiDesignerUtil {
	private static final Logger logger = LoggerFactory.getLogger(GuiDesignerUtil.class);
	
	@Resource(name = "jsonUtil")
	public JsonUtil jsonUtil; 
	
	@Resource(name = "fileUtils")
	public FileUtils fileUtils; 
	
	private static final String ITG_FOLDER = "itg";
	private static String NBPM_FOLDER = "nbpm/processPage/";
	private static final String LINKING = "-";
	
	private static final String PREVIOUS = "previous";
	private static final String AFTER = "after";
	private static final String FOOTER = "footer";
	
	private static final String SCRIPT_FILE_EXT = ".script";
	
	private static final String GLOBAL_EVENT_FILE = "global.event";
	
	private static final Pattern SPRING_MESSAGE_PATTERN  =  Pattern.compile("#springMessage\\('process.resource.{7}'\\)"); 
	
	private static final Pattern RESOURCE_PATTERN  =  Pattern.compile("process.resource.{7}");
	
	
	/**
	 * 배포경로를 반환한다.
	 * @return
	 */
	private String getDeployPath() {
		return NBPM_FOLDER + NkiaApplicationPropertiesMap.getProperty("Nbpm.itsm.site");
	}	
	/**
	 * 
	 * 선행스크립트 
	 * 
	 * @param request
	 * @param processType
	 * @param version
	 * @param previousScriptData
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public String getPreviousScript(HttpServletRequest request, String previousScriptData, HashMap dataMap)throws NkiaException, IOException{
		String processType = null;
		String subReqType = null;
		String version = null;
		
		if( dataMap != null ){
			processType = (String)dataMap.get("nbpm_process_type");
			if( dataMap.containsKey("sub_req_type") ){
				subReqType = (String)dataMap.get("sub_req_type");
			}
			if( !dataMap.containsKey("preview") ){
				version = (String)dataMap.get("nbpm_version");
			}
		}
		
		StringBuffer defaultScriptBuffer = new StringBuffer();
		if( previousScriptData != null && previousScriptData.length() > 0 ){
			JSONArray previousScriptArray =  getScriptJson(request, processType, version, previousScriptData, subReqType);
			if( previousScriptArray.size() > 0 ){
				for( int i = 0; i < previousScriptArray.size(); i++ ){
					JSONObject previousScriptJson = previousScriptArray.getJSONObject(i);
					String scriptDesign = previousScriptJson.getString("scriptDesign");
					if( !dataMap.isEmpty() ){
						Iterator iter = dataMap.keySet().iterator();
						while( iter.hasNext() ){
							String key = (String)iter.next();
							if( scriptDesign.indexOf("$!{dataMap."+key+"}") != -1 ){
								if( dataMap.get(key) != null ){
									scriptDesign = scriptDesign.replaceAll("\\$\\!\\{dataMap."+key+"\\}", dataMap.get(key).toString());	
								}
							}
						}
					}
					scriptDesign = scriptDesign.replaceAll("\\$\\!\\{dataMap.\\w*\\}", "");
					defaultScriptBuffer.append(parserSpringMessage(scriptDesign) + "\n");
				}
			}
		}
		return defaultScriptBuffer.toString();
	}
	
	/**
	 * 
	 * 후행스크립트
	 * 
	 * @param request
	 * @param processType
	 * @param version
	 * @param afterScriptData
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public String getAfterScript(HttpServletRequest request, String afterScriptData, HashMap dataMap)throws NkiaException, IOException{
		String processType = null;
		String subReqType = null;
		String version = null;
				
		if( dataMap != null ){
			processType = (String)dataMap.get("nbpm_process_type");
			if( dataMap.containsKey("sub_req_type") ){
				subReqType = (String)dataMap.get("sub_req_type");
			}
			if( !dataMap.containsKey("preview") ){
				version = (String)dataMap.get("nbpm_version");
			}
		}
		StringBuffer defaultScriptBuffer = new StringBuffer();
		if( afterScriptData != null && afterScriptData.length() > 0 ){
			JSONArray afterScriptArray =  getScriptJson(request, processType, version, afterScriptData, subReqType);
			if( afterScriptArray.size() > 0 ){
				for( int i = 0; i < afterScriptArray.size(); i++ ){
					JSONObject afterScriptJson = afterScriptArray.getJSONObject(i);
					String scriptDesign = afterScriptJson.getString("scriptDesign");
					if( !dataMap.isEmpty() ){
						Iterator iter = dataMap.keySet().iterator();
						while( iter.hasNext() ){
							String key = (String)iter.next();
							if( scriptDesign.indexOf("$!{dataMap."+key+"}") != -1 ){
								if( dataMap.get(key) != null ){
									scriptDesign = scriptDesign.replaceAll("\\$\\p{Punct}\\p{Punct}dataMap."+key+"\\p{Punct}", dataMap.get(key).toString());	
								}
							}
						}
					}
					scriptDesign = scriptDesign.replaceAll("\\$\\!\\{dataMap.\\w*\\}", "");
					defaultScriptBuffer.append(parserSpringMessage(scriptDesign) + "\n");
				}
			}
		}
		return defaultScriptBuffer.toString();
	}
	
	/**
	 * 
	 * 예외 스크립트(Process 흐름에 따른)
	 * 
	 * @param request
	 * @param processType
	 * @param version
	 * @param previousScriptData
	 * @param afterScriptData
	 * @param exceptionScriptData
	 * @param presentTask
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public String getExceptionScript(HttpServletRequest request, String previousScriptData,  String presentTask, String afterScriptData, String exceptionScriptData, String footerScriptData, HashMap dataMap)throws NkiaException, IOException{
		StringBuffer exceptionScriptBuffer = new StringBuffer();
		String subReqType = null;
		
		if( dataMap == null || !dataMap.containsKey("sr_id") ){
			return exceptionScriptBuffer.toString();
		}
		String processType = (String)dataMap.get("nbpm_process_type");
		String version = (String)dataMap.get("nbpm_version");
		if( dataMap.containsKey("sub_req_type") ){
			subReqType = (String)dataMap.get("sub_req_type");
		}
		String sr_id = (String)dataMap.get("sr_id");
		long taskId = 0;
		if(dataMap.get("nbpm_task_id") != null) {
			taskId = (Long)dataMap.get("nbpm_task_id");
		}
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		NbpmProviderService nbpmProviderService = (NbpmProviderService) NkiaApplicationContext.getCtx().getBean("nbpmProviderService");
		ArrayList processList = new ArrayList<String>();
		if(presentTask.equals("END_EDIT") || presentTask.equals("RECALL_EDIT")){
			processList = (ArrayList)nbpmProviderService.searchTaskListEndEdit(userVO, taskId, sr_id);
		}else{
			processList = (ArrayList)nbpmProviderService.searchTaskList(userVO, taskId, sr_id);
		}
		
		if( processList != null && processList.size() > 0 ){
			// 중복 Task 처리
			ArrayList<String> distinctTaskList = new ArrayList<String>(new LinkedHashSet<String>(processList));
			
			JSONArray dataArray = getExceptionScriptJson(distinctTaskList, request, processType, version, previousScriptData, presentTask, afterScriptData, exceptionScriptData, footerScriptData, subReqType);
			if( dataArray.size() > 0 ){
				for( int i = 0; i < dataArray.size(); i++ ){
					JSONObject dataJson = dataArray.getJSONObject(i);
					String scriptDesign = dataJson.getString("scriptDesign");
					if( !dataMap.isEmpty() ){
						Iterator iter = dataMap.keySet().iterator();
						while( iter.hasNext() ){
							String key = (String)iter.next();
							if( scriptDesign.indexOf("$!{dataMap."+key+"}") != -1 ){
								if( dataMap.get(key) != null ){
									scriptDesign = scriptDesign.replaceAll("\\$\\p{Punct}\\p{Punct}dataMap."+key+"\\p{Punct}", dataMap.get(key).toString());
								}else{
									scriptDesign = scriptDesign.replaceAll("\\$\\p{Punct}\\p{Punct}dataMap."+key+"\\p{Punct}", "");
								}
							}
						}
					}
					scriptDesign = scriptDesign.replaceAll("\\$\\!\\{dataMap.\\w*\\}", "");
					
					exceptionScriptBuffer.append(parserSpringMessage(scriptDesign) + "\n");
				}
			}
		}
		return exceptionScriptBuffer.toString();
	}
	public String getExceptionScript_EC(HttpServletRequest request, String previousScriptData,  String presentTask, String afterScriptData, String exceptionScriptData, String footerScriptData, HashMap dataMap)throws NkiaException, IOException{
		StringBuffer exceptionScriptBuffer = new StringBuffer();
		String subReqType = null;
		
		if( dataMap == null || !dataMap.containsKey("sr_id") ){
			return exceptionScriptBuffer.toString();
		}
		String processType = (String)dataMap.get("nbpm_process_type");
		String version = (String)dataMap.get("nbpm_version");
		if( dataMap.containsKey("sub_req_type") ){
			subReqType = (String)dataMap.get("sub_req_type");
		}
		String sr_id = (String)dataMap.get("sr_id");
		long taskId = 0;
		if(dataMap.get("nbpm_task_id") != null) {
			taskId = (Long)dataMap.get("nbpm_task_id");
		}
		
		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
		
		NbpmProviderService nbpmProviderService = (NbpmProviderService) NkiaApplicationContext.getCtx().getBean("nbpmProviderService");
		ArrayList processList = (ArrayList)nbpmProviderService.searchTaskList_EC(sr_id);
		
		if( processList != null && processList.size() > 0 ){
			// 중복 Task 처리
			ArrayList<String> distinctTaskList = new ArrayList<String>(new LinkedHashSet<String>(processList));
			
			JSONArray dataArray = getExceptionScriptJson(distinctTaskList, request, processType, version, previousScriptData, presentTask, afterScriptData, exceptionScriptData, footerScriptData, subReqType);
			if( dataArray.size() > 0 ){
				for( int i = 0; i < dataArray.size(); i++ ){
					JSONObject dataJson = dataArray.getJSONObject(i);
					String scriptDesign = dataJson.getString("scriptDesign");
					if( !dataMap.isEmpty() ){
						Iterator iter = dataMap.keySet().iterator();
						while( iter.hasNext() ){
							String key = (String)iter.next();
							if( scriptDesign.indexOf("$!{dataMap."+key+"}") != -1 ){
								if( dataMap.get(key) != null ){
									scriptDesign = scriptDesign.replaceAll("\\$\\p{Punct}\\p{Punct}dataMap."+key+"\\p{Punct}", dataMap.get(key).toString());
								}else{
									scriptDesign = scriptDesign.replaceAll("\\$\\p{Punct}\\p{Punct}dataMap."+key+"\\p{Punct}", "");
								}
							}
						}
					}
					scriptDesign = scriptDesign.replaceAll("\\$\\!\\{dataMap.\\w*\\}", "");
					
					exceptionScriptBuffer.append(parserSpringMessage(scriptDesign) + "\n");
				}
			}
		}
		return exceptionScriptBuffer.toString();
	}
	/**
	 * 
	 * Footer스크립트
	 * 
	 * @param request
	 * @param processType
	 * @param version
	 * @param afterScriptData
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public String getFooterScript(HttpServletRequest request, String footerScriptData, HashMap dataMap)throws NkiaException, IOException{
		String processType = null;
		String subReqType = null;
		String version = null;
				
		if( dataMap != null ){
			processType = (String)dataMap.get("nbpm_process_type");
			if( dataMap.containsKey("sub_req_type") ){
				subReqType = (String)dataMap.get("sub_req_type");
			}
			if( !dataMap.containsKey("preview") ){
				version = (String)dataMap.get("nbpm_version");
			}
		}
		StringBuffer defaultScriptBuffer = new StringBuffer();
		if( footerScriptData != null && footerScriptData.length() > 0 ){
			JSONArray footerScriptArray =  getScriptJson(request, processType, version, footerScriptData, subReqType);
			if( footerScriptArray.size() > 0 ){
				for( int i = 0; i < footerScriptArray.size(); i++ ){
					JSONObject footerScriptJson = footerScriptArray.getJSONObject(i);
					String scriptDesign = footerScriptJson.getString("scriptDesign");
					if( !dataMap.isEmpty() ){
						Iterator iter = dataMap.keySet().iterator();
						while( iter.hasNext() ){
							String key = (String)iter.next();
							if( scriptDesign.indexOf("$!{dataMap."+key+"}") != -1 ){
								if( dataMap.get(key) != null ){
									scriptDesign = scriptDesign.replaceAll("\\$\\p{Punct}\\p{Punct}dataMap."+key+"\\p{Punct}", dataMap.get(key).toString());	
								}
							}
						}
					}
					scriptDesign = scriptDesign.replaceAll("\\$\\!\\{dataMap.\\w*\\}", "");
					defaultScriptBuffer.append(parserSpringMessage(scriptDesign) + "\n");
				}
			}
		}
		return defaultScriptBuffer.toString();
	}
	
	/**
	 * 
	 * PanelList
	 * 
	 * @param request
	 * @param processType
	 * @param version
	 * @param previousScriptData
	 * @param afterScriptData
	 * @param defaultPanelData
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public String getPanelList(HttpServletRequest request, String previousScriptData, String presentTask, String afterScriptData, String exceptionScriptData, String footerScriptData, String otherPanels, String defaultPanelData, HashMap dataMap)throws NkiaException, IOException{
		ArrayList panelList = new ArrayList();
		
		String processType = null;
		String subReqType = null;
		String version = null;
		String sr_id = null;
		long taskId = 0;
		
		JSONObject otherPanelObject = jsonUtil.readDataToJsonObject(otherPanels);
		String commentPanel = otherPanelObject.getString("commentPanel");
		String commentLoc = otherPanelObject.getString("commentLoc");
		String processSubInfoPanel = otherPanelObject.getString("processSubInfoPanel");
		String processSubInfoLoc = otherPanelObject.getString("processSubInfoLoc");
		
		
		if( dataMap != null ){
			processType = (String)dataMap.get("nbpm_process_type");
			if( dataMap.containsKey("sub_req_type") ){
				subReqType = (String)dataMap.get("sub_req_type");
			}
			if(dataMap.get("nbpm_task_id") != null) {
				taskId = (Long)dataMap.get("nbpm_task_id");				
			}
			if( !dataMap.containsKey("preview") ){
				version = (String)dataMap.get("nbpm_version");
				sr_id = (String)dataMap.get("sr_id");
			}
		}
		
		// 선행 Panel List
		if( dataMap != null && previousScriptData != null && previousScriptData.length() > 0 ){
			JSONArray previousScriptArray =  getScriptJson(request, processType, version, previousScriptData, subReqType);
			if( previousScriptArray.size() > 0 ){
				for( int i = 0; i < previousScriptArray.size(); i++ ){
					JSONArray previousPanelList = previousScriptArray.getJSONObject(i).getJSONArray("panelList");
					panelList.add(previousPanelList.join(","));
				}
			}
		}
		
		// Previous Process Sub Info Attach
		attachOtherPanel(processSubInfoPanel, processSubInfoLoc, PREVIOUS, panelList);
		
		// Previous Comment Attach
		attachOtherPanel(commentPanel, commentLoc, PREVIOUS, panelList);
		
		// 현재 Panel List
		JSONArray defaultPanelList = jsonUtil.convertStringToJsonArray(defaultPanelData);
		if( defaultPanelList.size() > 0 ){
			panelList.add(defaultPanelList.join(","));
		}
		
		// After Process Sub Info Attach
		attachOtherPanel(processSubInfoPanel, processSubInfoLoc, AFTER, panelList);
		
		// After Comment Attach
		attachOtherPanel(commentPanel, commentLoc, AFTER, panelList);
		
		// 후행 Panel List
		if( dataMap != null && afterScriptData != null && afterScriptData.length() > 0 ){
			JSONArray afterScriptArray =  getScriptJson(request, processType, version, afterScriptData, subReqType);
			if( afterScriptArray.size() > 0 ){
				for( int i = 0; i < afterScriptArray.size(); i++ ){
					JSONArray afterPanelList = afterScriptArray.getJSONObject(i).getJSONArray("panelList");
					panelList.add(afterPanelList.join(","));
				}
			}
		}
		
		// 예외 Panel List(미포함 Process)
		if( dataMap != null ){
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			NbpmProviderService nbpmProviderService = (NbpmProviderService) NkiaApplicationContext.getCtx().getBean("nbpmProviderService");
			ArrayList processList = new ArrayList<String>();
			if(presentTask.equals("END_EDIT") || presentTask.equals("RECALL_EDIT")){
				processList = (ArrayList)nbpmProviderService.searchTaskListEndEdit(userVO, taskId, sr_id);
			}else{
				processList = (ArrayList)nbpmProviderService.searchTaskList(userVO, taskId, sr_id);
			}
			if( processList != null && processList.size() > 0 ){
				// 중복 Task 처리
				ArrayList<String> distinctTaskList = new ArrayList<String>(new LinkedHashSet<String>(processList));
				
				JSONArray dataArray = getExceptionScriptJson(distinctTaskList, request, processType, version, previousScriptData, presentTask, afterScriptData, exceptionScriptData, footerScriptData, subReqType);
				if( dataArray.size() > 0 ){
					for( int i = 0; i < dataArray.size(); i++ ){
						JSONObject dataJson = dataArray.getJSONObject(i);
						JSONArray exceptionPanelList = dataJson.getJSONArray("panelList");
						panelList.add(exceptionPanelList.join(","));
					}
				}
			}
		}
		
		
		// Footter
		if( dataMap != null && footerScriptData != null && footerScriptData.length() > 0 ){
			JSONArray footerScriptArray =  getScriptJson(request, processType, version, footerScriptData, subReqType);
			if( footerScriptArray.size() > 0 ){
				for( int i = 0; i < footerScriptArray.size(); i++ ){
					JSONArray footerPanelList = footerScriptArray.getJSONObject(i).getJSONArray("panelList");
					panelList.add(footerPanelList.join(","));
				}
			}
		}
		
		// Footer Process Sub Info Attach
		attachOtherPanel(processSubInfoPanel, processSubInfoLoc, FOOTER, panelList);
		
		// Footer Comment Attach
		attachOtherPanel(commentPanel, commentLoc, FOOTER, panelList);

		return panelList.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "");
	}
	/**
	 * 
	 * PanelList
	 * 
	 * @param request
	 * @param processType
	 * @param version
	 * @param previousScriptData
	 * @param afterScriptData
	 * @param defaultPanelData
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public String getPanelList_EC(HttpServletRequest request, String previousScriptData, String presentTask, String afterScriptData, String exceptionScriptData, String footerScriptData, String otherPanels, String defaultPanelData, HashMap dataMap)throws NkiaException, IOException{
		ArrayList panelList = new ArrayList();
		
		String processType = null;
		String subReqType = null;
		String version = null;
		String sr_id = null;
		long taskId = 0;
		
		JSONObject otherPanelObject = jsonUtil.readDataToJsonObject(otherPanels);
		String commentPanel = otherPanelObject.getString("commentPanel");
		String commentLoc = otherPanelObject.getString("commentLoc");
		String processSubInfoPanel = otherPanelObject.getString("processSubInfoPanel");
		String processSubInfoLoc = otherPanelObject.getString("processSubInfoLoc");
		
		
		if( dataMap != null ){
			processType = (String)dataMap.get("nbpm_process_type");
			if( dataMap.containsKey("sub_req_type") ){
				subReqType = (String)dataMap.get("sub_req_type");
			}
			if(dataMap.get("nbpm_task_id") != null) {
				taskId = (Long)dataMap.get("nbpm_task_id");				
			}
			if( !dataMap.containsKey("preview") ){
				version = (String)dataMap.get("nbpm_version");
				sr_id = (String)dataMap.get("sr_id");
			}
		}
		
		// 선행 Panel List
		if( dataMap != null && previousScriptData != null && previousScriptData.length() > 0 ){
			JSONArray previousScriptArray =  getScriptJson(request, processType, version, previousScriptData, subReqType);
			if( previousScriptArray.size() > 0 ){
				for( int i = 0; i < previousScriptArray.size(); i++ ){
					JSONArray previousPanelList = previousScriptArray.getJSONObject(i).getJSONArray("panelList");
					panelList.add(previousPanelList.join(","));
				}
			}
		}
		
		// Previous Process Sub Info Attach
		attachOtherPanel(processSubInfoPanel, processSubInfoLoc, PREVIOUS, panelList);
		
		// Previous Comment Attach
		attachOtherPanel(commentPanel, commentLoc, PREVIOUS, panelList);
		
		// 현재 Panel List
		JSONArray defaultPanelList = jsonUtil.convertStringToJsonArray(defaultPanelData);
		if( defaultPanelList.size() > 0 ){
			panelList.add(defaultPanelList.join(","));
		}
		
		// After Process Sub Info Attach
		attachOtherPanel(processSubInfoPanel, processSubInfoLoc, AFTER, panelList);
		
		// After Comment Attach
		attachOtherPanel(commentPanel, commentLoc, AFTER, panelList);
		
		// 후행 Panel List
		if( dataMap != null && afterScriptData != null && afterScriptData.length() > 0 ){
			JSONArray afterScriptArray =  getScriptJson(request, processType, version, afterScriptData, subReqType);
			if( afterScriptArray.size() > 0 ){
				for( int i = 0; i < afterScriptArray.size(); i++ ){
					JSONArray afterPanelList = afterScriptArray.getJSONObject(i).getJSONArray("panelList");
					panelList.add(afterPanelList.join(","));
				}
			}
		}
		
		// 예외 Panel List(미포함 Process)
		if( dataMap != null ){
			//UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			NbpmProviderService nbpmProviderService = (NbpmProviderService) NkiaApplicationContext.getCtx().getBean("nbpmProviderService");
			//ArrayList processList = (ArrayList)nbpmProviderService.searchTaskList(userVO, taskId, sr_id);
			ArrayList processList = (ArrayList)nbpmProviderService.searchTaskList_EC(sr_id);
			if( processList != null && processList.size() > 0 ){
				// 중복 Task 처리
				ArrayList<String> distinctTaskList = new ArrayList<String>(new LinkedHashSet<String>(processList));
				
				JSONArray dataArray = getExceptionScriptJson(distinctTaskList, request, processType, version, previousScriptData, presentTask, afterScriptData, exceptionScriptData, footerScriptData, subReqType);
				if( dataArray.size() > 0 ){
					for( int i = 0; i < dataArray.size(); i++ ){
						JSONObject dataJson = dataArray.getJSONObject(i);
						JSONArray exceptionPanelList = dataJson.getJSONArray("panelList");
						panelList.add(exceptionPanelList.join(","));
					}
				}
			}
		}
		
		
		// Footter
		if( dataMap != null && footerScriptData != null && footerScriptData.length() > 0 ){
			JSONArray footerScriptArray =  getScriptJson(request, processType, version, footerScriptData, subReqType);
			if( footerScriptArray.size() > 0 ){
				for( int i = 0; i < footerScriptArray.size(); i++ ){
					JSONArray footerPanelList = footerScriptArray.getJSONObject(i).getJSONArray("panelList");
					panelList.add(footerPanelList.join(","));
				}
			}
		}
		
		// Footer Process Sub Info Attach
		attachOtherPanel(processSubInfoPanel, processSubInfoLoc, FOOTER, panelList);
		
		// Footer Comment Attach
		attachOtherPanel(commentPanel, commentLoc, FOOTER, panelList);

		return panelList.toString().replaceAll("\"", "").replaceAll("\\[", "").replaceAll("\\]", "");
	}
	/**
	 * 
	 * 프로세스 전역이벤트를 가져오는 Method
	 * 
	 * @param dataMap
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public String getGlobalEvent(HashMap dataMap)throws NkiaException, IOException{
		StringBuffer globalEventScript = new StringBuffer();
		String processType = null;
		String version = null;
		String subReqType = null;
		
		String deployPath = getDeployPath();
		
		if( dataMap != null ){
			processType = (String)dataMap.get("nbpm_process_type");
			if( !dataMap.containsKey("preview") ){
				version = (String)dataMap.get("nbpm_version");
			}
			if( dataMap.containsKey("sub_req_type") ){
				subReqType = (String)dataMap.get("sub_req_type");
			}
			
			String rootContext = NkiaApplicationContext.getProjectPath();  
			String nbpmPath = rootContext + ITG_FOLDER + File.separator + deployPath;
			String processPath = null;
			if( version != null ){
				processPath = nbpmPath + File.separator + processType + (( subReqType != null && subReqType.length() > 0 ) ? "_" + subReqType : "" ) + LINKING + version;
			}else{
				processPath = nbpmPath + File.separator + processType + (( subReqType != null && subReqType.length() > 0 ) ? "_" + subReqType : "" );
			}
			String globalEventFilePath = processPath + File.separator + GLOBAL_EVENT_FILE;
			if( FileUtils.checkFile(globalEventFilePath) ){
				String scriptFile = FileUtils.readFileToString(globalEventFilePath);
				JSONArray globalEventArray = jsonUtil.convertStringToJsonArray(scriptFile);
				if( globalEventArray.size() > 0 ){
					for( int k = 0; k < globalEventArray.size(); k++ ){
						JSONObject globalEventData = globalEventArray.getJSONObject(k);
						globalEventScript.append(globalEventData.getString("event_script"));
					}
				}
			}
		}
		return globalEventScript.toString();
	}
	
	public JSONArray getScriptJson(HttpServletRequest request, String processType, String version, String scriptData, String subReqType)throws NkiaException, IOException{
		JSONArray dataList = new JSONArray();
		String deployPath = getDeployPath();
		
		String rootContext = NkiaApplicationContext.getProjectPath();  
		String nbpmPath = rootContext + ITG_FOLDER + File.separator + deployPath;
		String processPath = null;
		if( version != null ){
			processPath = nbpmPath + File.separator + processType + (( subReqType != null && subReqType.length() > 0 ) ? "_" + subReqType : "" ) + LINKING + version;
		}else{
			processPath = nbpmPath + File.separator + processType + (( subReqType != null && subReqType.length() > 0 ) ? "_" + subReqType : "" );
		}
		JSONArray defaultScriptList = jsonUtil.convertStringToJsonArray(scriptData);
		if( defaultScriptList.size() > 0 ){
			for( int i = 0; i < defaultScriptList.size(); i++ ){
				JSONObject defaultScriptJson = defaultScriptList.getJSONObject(i);
				String fileName = defaultScriptJson.getString("file");
				String scriptName = defaultScriptJson.getString("script");
				String scriptFile = FileUtils.readFileToString(processPath + File.separator + fileName);
				JSONArray scriptList = jsonUtil.convertStringToJsonArray(scriptFile);
				for( int j = 0; j < scriptList.size(); j++ ){
					JSONObject scriptJson = scriptList.getJSONObject(j);
					if( scriptName.equalsIgnoreCase(scriptJson.getString("scriptName")) ){
						dataList.add(scriptJson);
					}
				}
			}
		}
		return dataList;
	}
	
	/**
	 * 
	 * 예외처리 스크립트
	 * 
	 * @param distinctTaskList
	 * @param request
	 * @param processType
	 * @param version
	 * @param previousScriptData
	 * @param presentTask
	 * @param afterScriptData
	 * @param exceptionScriptData
	 * @param footerScriptData
	 * @param subReqType TODO
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public JSONArray getExceptionScriptJson(ArrayList<String> distinctTaskList, HttpServletRequest request, String processType, String version, String previousScriptData, String presentTask, String afterScriptData, String exceptionScriptData, String footerScriptData, String subReqType)throws NkiaException, IOException{
		JSONArray dataList = new JSONArray();
		
		String deployPath = getDeployPath();
		
		// 현재 Task 제거
		if( distinctTaskList.size() > 0 && presentTask != null && presentTask.length() > 0 ){
			for( int i = distinctTaskList.size()-1; i >= 0; i-- ){
				String taskName = (String)distinctTaskList.get(i);
				if( taskName.equalsIgnoreCase(presentTask) ){
					distinctTaskList.remove(i);
				}
			}
		}
		
		// PreviousScript Task 제거
		if( distinctTaskList.size() > 0 ){
			if( previousScriptData != null && previousScriptData.length() > 0 ){
				JSONArray previousScriptList = jsonUtil.convertStringToJsonArray(previousScriptData);
				for( int i = 0; i < previousScriptList.size(); i++ ){
					JSONObject scriptJson = previousScriptList.getJSONObject(i);
					for( int j = distinctTaskList.size()-1; j >= 0; j-- ){
						String taskName = (String)distinctTaskList.get(j);
						String file = scriptJson.getString("file");
						if( taskName.equalsIgnoreCase(file.substring(0, file.lastIndexOf(".")))){
							distinctTaskList.remove(j);
						}
					}
				}
			}
		}
		
		// AfterScript Task 제거
		if( distinctTaskList.size() > 0 ){
			if( afterScriptData != null && afterScriptData.length() > 0 ){
				JSONArray afterScriptList = jsonUtil.convertStringToJsonArray(afterScriptData);
				for( int i = 0; i < afterScriptList.size(); i++ ){
					JSONObject scriptJson = afterScriptList.getJSONObject(i);
					for( int j = distinctTaskList.size()-1; j >= 0; j-- ){
						String taskName = (String)distinctTaskList.get(j);
						String file = scriptJson.getString("file");
						if( taskName.equalsIgnoreCase(file.substring(0, file.lastIndexOf(".")))){
							distinctTaskList.remove(j);
						}
					}
				}
			}
		}
		
		// FooterScript Task 제거
		if( distinctTaskList.size() > 0 ){
			if( footerScriptData != null && footerScriptData.length() > 0 ){
				JSONArray footerScriptList = jsonUtil.convertStringToJsonArray(footerScriptData);
				for( int i = 0; i < footerScriptList.size(); i++ ){
					JSONObject scriptJson = footerScriptList.getJSONObject(i);
					for( int j = distinctTaskList.size()-1; j >= 0; j-- ){
						String taskName = (String)distinctTaskList.get(j);
						String file = scriptJson.getString("file");
						if( taskName.equalsIgnoreCase(file.substring(0, file.lastIndexOf(".")))){
							distinctTaskList.remove(j);
						}
					}
				}
			}
		}

		// 중복 데이터 제거 이후 Task정보 가져오기
		if( distinctTaskList.size() > 0 ){
			for( int i = 0; i < distinctTaskList.size(); i++ ){
				String taskName = distinctTaskList.get(i) + SCRIPT_FILE_EXT;
				String rootContext = request.getSession().getServletContext().getRealPath("");
				String nbpmPath = rootContext + File.separator + ITG_FOLDER + File.separator + deployPath;
				//String processPath = nbpmPath + File.separator + processType + LINKING + version;
				String processPath = nbpmPath + File.separator + processType + (( subReqType != null && subReqType.length() > 0 ) ? "_" + subReqType : "" ) + LINKING + version;
				String scriptFile = null;
				JSONArray scriptList = null;
				try {
					scriptFile = fileUtils.readFileToString(processPath + File.separator + taskName);
					scriptList = jsonUtil.convertStringToJsonArray(scriptFile);
				} catch (java.io.FileNotFoundException e) {
					logger.error(e.getMessage());
				}
				boolean isAddData = false;
				
				//@@20130814 김도원 START
				//기존의 로직 확인 결과 script의 이름을 지정을 하여도 default만 가져오게 끔 설정이 되어 있음
				//이에 로직을 수정함 -> 확인 필요
				JSONArray exceptionScriptList = null;
				if(!"".equals(exceptionScriptData)&& exceptionScriptData != null) {
					exceptionScriptList = jsonUtil.convertStringToJsonArray(exceptionScriptData);
				}

				for( int k = 0; exceptionScriptList != null && k < exceptionScriptList.size(); k++ ){
					JSONObject exceptionScriptJson = exceptionScriptList.getJSONObject(k);
					if( exceptionScriptData != null && exceptionScriptData.length() > 0 ){
						boolean isExist = false;
						for( int j = 0; scriptList != null && j < scriptList.size(); j++ ){
							JSONObject scriptJson = scriptList.getJSONObject(j);
							//@@20130802 김도원 START
							//Null 처리 추가
							if(exceptionScriptJson.containsKey("script")) {
								String file = exceptionScriptJson.getString("file");
								String exceptionScriptName = exceptionScriptJson.getString("script");
								if( taskName.equalsIgnoreCase(file) && exceptionScriptName.equalsIgnoreCase(scriptJson.getString("scriptName"))){
									isExist = true;
									isAddData = true;
									dataList.add(scriptJson);
									break;
								}
								if(j == scriptList.size()) {
									if( !isExist ){
										isAddData = true;
										dataList.add(scriptJson);
									}									
								}
							}
							////@@20130802 김도원 END 
						}
						/*
						 * 이름이 해당 Task는 있으나 일치하는 스크립트 명이 없을 경우엔 화면에 보이지 않는다.
						 * 기존에는 defaultScript를 찾게 하였으나 헷갈리는 경우가 생길 수 있으므로 일치하지 않으면
						 * 보여주지 않도록 수정
						if(!isAddData) {
							String exceptionScriptName = "defaultScript";
							for( int j = 0; j < scriptList.size(); j++ ){
								JSONObject scriptJson = scriptList.getJSONObject(j); 
								if( exceptionScriptName.equalsIgnoreCase(scriptJson.getString("scriptName")) ){
									isAddData = true;
									dataList.add(scriptJson);
									break;
								}
							}							
						}
						*/
					}
				}
				//@@20130814 김도원 END
			}
		}
		return dataList;
	}
	
	/**
	 * 
	 * Panel List에 Panel Attach
	 * 
	 * @param otherPanel
	 * @param otherPanelLoc
	 * @param matchValue
	 * @param panelList
	 * @throws NkiaException
	 */
	private void attachOtherPanel(String otherPanel, String otherPanelLoc, String matchValue, ArrayList panelList)throws NkiaException{
		if( !otherPanelLoc.equalsIgnoreCase("none") && otherPanelLoc.equalsIgnoreCase(matchValue) ){
			// After 배치 일 경우, footer 보다 위에 객체를 배치 하도록 분기 추가 2018.06.26.정정윤
			boolean isAttached = false;
			if(panelList!=null && panelList.size()>0 && AFTER.equalsIgnoreCase(matchValue)) {
				for(int i=0; i<panelList.size(); i++) {
					String s = StringUtil.parseString(panelList.get(i));
					if(s.indexOf("\"footer\"") > -1) {
						// 기존의 footer 위치에 패널ID를 넣고 footer를 뒤에 다시 붙여준다.
						String newPanel = s.replace("\"footer\"", "\"" + otherPanel + "\"") + ",\"footer\"";
						panelList.remove(i);
						panelList.add(i, newPanel);
						isAttached = true;
						break;
					}
				}
			}
			if(!isAttached) {
				panelList.add(otherPanel);
			}
		}
	}
	
	/**
	 * 
	 * 요청 업무 흐름도
	 * 
	 * @param sr_id
	 * @throws NkiaException
	 */
	public JSONArray searchProcessFlowList(String sr_id)throws NkiaException{
		ProcessFlowChartService processFlowChartService = (ProcessFlowChartService) NkiaApplicationContext.getCtx().getBean("processFlowChartService");
		JSONArray processFlowListJson = JSONArray.fromObject(processFlowChartService.searchProcessFlowList(sr_id));
		return processFlowListJson;
	}
	
	/**
	 * 
	 * Spring 메시지 파서
	 * 
	 * @param scriptDesign
	 * @return
	 * @throws NkiaException
	 */
	private String parserSpringMessage(String scriptDesign)throws NkiaException{
		EgovMessageSource egovMessageSource = (EgovMessageSource)NkiaApplicationContext.getCtx().getBean("egovMessageSource");
		
		Matcher match = SPRING_MESSAGE_PATTERN.matcher(scriptDesign);
		while(match.find()){ 
			String springMessage = match.group();
			Matcher resourceMatch = RESOURCE_PATTERN.matcher(springMessage);
			if(resourceMatch.find()){
				scriptDesign = scriptDesign.replace(springMessage, egovMessageSource.getMessage(resourceMatch.group()));
			}
		}
		return scriptDesign;
	}
	
}
