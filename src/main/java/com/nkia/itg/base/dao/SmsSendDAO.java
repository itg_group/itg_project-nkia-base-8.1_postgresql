package com.nkia.itg.base.dao;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.intergration.customize.datasource.IntergrationMessage;
import com.nkia.intergration.customize.datasource.IntergrationSMS;
import com.nkia.itg.base.application.exception.NkiaException;

@Repository("smsSendDAO")
public class SmsSendDAO extends IntergrationMessage{
		
	public void insertDoSMSList(Map param) throws NkiaException{
		insert("smsSendDAO.insertDoSMSList", param);
	}
	
	public void insertInSMSList(Map param) throws NkiaException{
		insert("smsSendDAO.insertInSMSList", param);
	}
	
	public void insertTalkList(Map param) throws NkiaException{
		insert("smsSendDAO.insertTalkList", param);
	}
}
