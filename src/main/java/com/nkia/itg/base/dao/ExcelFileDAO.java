/*
 * @(#)AtchFileDAO.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.AtchFileDetailVO;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("excelFileDAO")
public class ExcelFileDAO extends EgovComAbstractDAO{
	
	public List selectAtchFileList(ModelMap paramMap)throws NkiaException {
		return list("AtchFileDAO.selectAtchFileList", paramMap);
	} 
	
	public AtchFileDetailVO selectAtchFileDetail(AtchFileDetailVO atchFileDetailVO)throws NkiaException {
		AtchFileDetailVO dataAtchFileDetailVO = (AtchFileDetailVO)selectByPk("AtchFileDAO.selectAtchFileDetail", atchFileDetailVO);
		return dataAtchFileDetailVO;
	}  
	
	public Map selectExcelFileDetail(Map paramMap)throws NkiaException{
		return (Map) selectByPk("ExcelFileDAO.selectExcelFileDetail", paramMap);
	}
	
	public String selectMaxHistoryId()throws NkiaException{
		return (String)selectByPk("ExcelFileDAO.selectMaxHistoryId", null);
	}
	
	public void insertExcelFile(Map paramMap)throws NkiaException {
		insert("ExcelFileDAO.insertExcelFile", paramMap);
	}
	
	public void updateExcelNavigation(Map paramMap)throws NkiaException {
		insert("ExcelFileDAO.updateExcelNavigation", paramMap);
	}
	
	public HashMap searchFileInfo(HashMap paramMap) throws NkiaException {
		return (HashMap) this.selectByPk("ExcelFileDAO.searchFileInfo", paramMap);
	}

	/*Start 엑셀템플릿 자동 생성(By Poi)*/
	public List selectAllCodeListForExcel(HashMap mapQueryParam) {
		return this.list("ExcelFileDAO.selectAllCodeListForExcel",mapQueryParam);
	}

	public List selectPopupCodeListQueryForExcel(HashMap mapQueryParam) {
		return this.list("ExcelFileDAO.selectPopupCodeListQueryForExcel",mapQueryParam);
	}

	public List selectPopupCodeListDynamic(HashMap mapQueryParam) {
		return this.list("ExcelFileDAO.selectPopupCodeListDynamic",mapQueryParam);
	}
	/*End 엑셀템플릿 자동 생성(By Poi)*/
	
}
