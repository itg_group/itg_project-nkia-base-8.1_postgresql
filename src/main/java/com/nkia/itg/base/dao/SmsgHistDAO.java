package com.nkia.itg.base.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("smsgHistDAO")
public class SmsgHistDAO extends EgovComAbstractDAO {
	
	public List searchSmsgHistList(Map paramMap) throws NkiaException {
		return list("SmsgHistDAO.searchSmsgHistList", paramMap);
	}

	public int searchSmsgHistListCount(Map paramMap) throws NkiaException {
		return (Integer) selectByPk("SmsgHistDAO.searchSmsgHistListCount", paramMap);
	}

	public void insertSmsgHist(Map param) throws NkiaException {
		insert("SmsgHistDAO.insertSmsgHist", param);
	}
	public void insertTalkListHistory(Map param) throws NkiaException {
		insert("SmsgHistDAO.insertTalkListHistory", param);
	}
}
