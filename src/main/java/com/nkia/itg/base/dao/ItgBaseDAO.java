/*
 * @(#)ItgBaseDAO.java              2013. 1. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("itgBaseDAO")
public class ItgBaseDAO extends EgovComAbstractDAO {

	public List searchCodeDataList(Map param) throws NkiaException {
		return list("ItgBaseDAO.searchCodeDataList", param);
	}
	
	public List searchMutilCodeDataListForTree(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchMutilCodeDataListForTree", paramMap);
	}

	public List searchReqZoneDataList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchReqZoneDataList", paramMap);
	}
	
	public List searchConfTypeList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchConfTypeList", paramMap);
	}
	
	/**
	 * 
	 * TODO ems 연계 테이블 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchEmsTableList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchEmsTableList", paramMap);
	}
	
	/**
	 * 
	 * TODO ems 연계 테이블 컬럼
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchEmsColList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchEmsColList", paramMap);
	}
	
	public List searchClassType(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchClassType", paramMap);
	}
	
	public List searchServiceNodeName(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchServiceNodeName", paramMap);
	}
	
	public List searchChangeNodeName(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchChangeNodeName", paramMap);
	}
	
	public List searchIncidentNodeName(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchIncidentNodeName", paramMap);
	}
	
	public List searchProblemNodeName(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchProblemNodeName", paramMap);
	}
	
	public List searchCenterNodeName(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchCenterNodeName", paramMap);
	}
	
	public List searchDeadlineNodeName(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchDeadlineNodeName", paramMap);
	}

	public List searchAutoDiscoveryTableList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchAutoDiscoveryTableList", paramMap);
	}

	public List searchAutoDiscoveryColList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchAutoDiscoveryColList", paramMap);
	}
	
	public List searchCheckListSendList() throws NkiaException {
		return list("ItgBaseDAO.searchCheckListSendList", null);
	}
	
	public List searchAccountSendList() throws NkiaException {
		return list("ItgBaseDAO.searchAccountSendList", null);
	}

	public List searchDeptTree(Map paramMap) {
		return this.list("ItgBaseDAO.searchDeptTree", paramMap);
	}

	public int searchDeptUserGridListCount(Map paramMap) {
		return (Integer) this.selectByPk("ItgBaseDAO.searchDeptUserGridListCount", paramMap);
	}

	public List searchDeptUserGridList(Map paramMap) {
		return this.list("ItgBaseDAO.searchDeptUserGridList", paramMap);
	}
	
	public List selectMenuInfo(Map paramMap) throws NkiaException {
		return list("ItgBaseDAO.selectMenuInfo", paramMap);
	}

	public List selectAmClassTypeCode(ModelMap paramMap) {
		return list("ItgBaseDAO.selectAmClassTypeCode", paramMap);
	}
	
	public List selectAssetStateCodeList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.selectAssetStateCodeList", paramMap);
	}
	
	public List searchCustomerToUserList(ModelMap paramMap) throws NkiaException {
		return list("ItgBaseDAO.searchCustomerToUserList", paramMap);
	}
	
	//hmsong cmdb 개선 작업 Start
	public List searchConfTypeCodeList(Map param) throws NkiaException {
		return list("ItgBaseDAO.searchConfTypeCodeList", param);
	}
	
	public List searchClassIdCodeList(Map param) throws NkiaException {
		return list("ItgBaseDAO.searchClassIdCodeList", param);
	}
	
	//hmsong cmdb 개선 작업 End
}
