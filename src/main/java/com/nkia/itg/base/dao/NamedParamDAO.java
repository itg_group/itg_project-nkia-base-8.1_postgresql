/*
 * @(#)ItgBaseDAO.java              2018. 1. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("namedParamDAO")
public class NamedParamDAO {

	@Resource(name="namedParameterJdbcTemplate")
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public Map<String, Object> selectMap(String query, Map<String, Object> params) {
		try{
			Map<String, Object> result = namedParameterJdbcTemplate.queryForMap(query, params);
			return result;
		}catch(EmptyResultDataAccessException e){
			// Incorrect result size: expected 1, actual 0 >> Exception 발생하면 Null 로 반환시킨다.
			return null;
		}
	}

	public List<Map<String, Object>> selectList(String query, Map<String, Object> params) {
		List<Map<String, Object>> result = namedParameterJdbcTemplate.queryForList(query, params);
		return result;
	}

	public int insert(String query, Map<String, Object> params) {
		int result = namedParameterJdbcTemplate.update(query, params);
		return result;
	}
	
	public int update(String query, Map<String, Object> params) {
		int result = namedParameterJdbcTemplate.update(query, params);
		return result;
	}
	
	public int delete(String query, Map<String, Object> params) {
		int result = namedParameterJdbcTemplate.update(query, params);
		return result;
	}

}
