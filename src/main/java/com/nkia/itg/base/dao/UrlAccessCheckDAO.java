/*
 * @(#)UrlAccessCheckDAO.java              2018. 8. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("urlAccessCheckDAO")
public class UrlAccessCheckDAO extends EgovComAbstractDAO {

	public List selectAllMenuUrls() throws NkiaException {
		return list("UrlAccessCheckDAO.selectAllMenuUrls", null);
	}
	
	public int countIsReqUrlInMenuUrl(HashMap paramMap) throws NkiaException{
		return (Integer)selectByPk("UrlAccessCheckDAO.countIsReqUrlInMenuUrl", paramMap);
	}
	
	public int countIsReqUrlInUserAuthMenuUrl(HashMap paramMap) throws NkiaException{
		return (Integer)selectByPk("UrlAccessCheckDAO.countIsReqUrlInUserAuthMenuUrl", paramMap);
	}
	
}
