/*
 * @(#)AlarmBoxDAO.java              2014. 4. 23.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.framework.MapDataRowHandler;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;

@Repository("alarmBoxDAO")
public class AlarmBoxDAO extends EgovComAbstractDAO {
	
	public List selectAlarmBoxList(ModelMap paramMap) throws NkiaException, SQLException {
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("AlarmBoxDAO.selectAlarmBoxList", paramMap, handler);
		return (List)handler.getReturnList();
	}
	
	public HashMap selectAlarmBoxDetail(ModelMap paramMap) throws NkiaException, SQLException {
		MapDataRowHandler handler = new MapDataRowHandler();
		this.getSqlMapClient().queryWithRowHandler("AlarmBoxDAO.selectAlarmBoxDetail", paramMap, handler);
		return (HashMap)handler.getReturnMap();
	}
	
	public void updateAlarmBoxReadYn(ModelMap paramMap) throws NkiaException{
		update("AlarmBoxDAO.updateAlarmBoxReadYn", paramMap);
	}
	
	public void insertAlarmBoxInfo(Map parmaMap) throws NkiaException {
		insert("AlarmBoxDAO.insertAlarmBoxInfo", parmaMap);
	}

	public String selectAlarmBoxNextKey(Map parmaMap) {
		return (String)selectByPk("AlarmBoxDAO.selectAlarmBoxNextKey", parmaMap);
	}
}
