/*
 * @(#)AtchFileDAO.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.AtchFileVO;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;


@Repository("atchFileDAO")
public class AtchFileDAO extends EgovComAbstractDAO{
	
	public List selectAtchFileList(ModelMap paramMap)throws NkiaException {
		return list("AtchFileDAO.selectAtchFileList", paramMap);
	} 
	
	public List selectAtchFileListForProc(ModelMap paramMap)throws NkiaException {
		return list("AtchFileDAO.selectAtchFileListForProc", paramMap);
	}
	
	public List selectAtchFileListForMail(ModelMap paramMap)throws NkiaException {
		return list("AtchFileDAO.selectAtchFileListForMail", paramMap);
	}

	public List selectAtchFileListForTemplate(ModelMap paramMap)throws NkiaException {
		return list("AtchFileDAO.selectAtchFileListForTemplate", paramMap);
	}
	
	public List selectAtchFileListForZip(AtchFileVO atchFileVO)throws NkiaException {
		return list("AtchFileDAO.selectAtchFileListForZip", atchFileVO);
	}

	public AtchFileDetailVO selectAtchFileDetail(AtchFileDetailVO atchFileDetailVO)throws NkiaException {
		AtchFileDetailVO dataAtchFileDetailVO = (AtchFileDetailVO)selectByPk("AtchFileDAO.selectAtchFileDetail", atchFileDetailVO);
		return dataAtchFileDetailVO;
	}  
	
	public String selectNextAtchFileId()throws NkiaException{
		return (String)selectByPk("AtchFileDAO.selectNextAtchFileId", null);
	}
	
	public void insertNextAtchFileId(AtchFileVO atchFileVO)throws NkiaException {
		insert("AtchFileDAO.insertNextAtchFileId", atchFileVO);
	}
	
	public void insertAtchFile(AtchFileVO atchFileVO)throws NkiaException {
		insert("AtchFileDAO.insertAtchFile", atchFileVO);
	}
	
	public void insertAtchFileDetail(AtchFileDetailVO atchFileDetailVO)throws NkiaException {
		insert("AtchFileDAO.insertAtchFileDetail", atchFileDetailVO);
	}
	
	public void updateAtchFile(ModelMap paramMap)throws NkiaException {
		update("AtchFileDAO.updateAtchFile", paramMap);
	}
	
	public void deleteAtchFile(ModelMap paramMap)throws NkiaException {
		delete("AtchFileDAO.deleteAtchFile", paramMap);
	}

	public void updateEmptyBlobAtchFileDetail(AtchFileDetailVO atchFileDetailVO) throws NkiaException {
		update("AtchFileDAO.deleteAtchFileDetail", atchFileDetailVO);
	}
	
	public void updateAtchFileEmptyBlobAll(AtchFileVO atchFileVO) {
		update("AtchFileDAO.updateAtchFileEmptyBlobAll", atchFileVO);
	}

	public void updateAtchFileDetailDelYn(AtchFileDetailVO atchFileDetailVO)throws NkiaException {
		update("AtchFileDAO.updateAtchFileDetailDelYn", atchFileDetailVO);
	}

	public void updateAtchFileDetailDelAll(AtchFileVO atchFileVO) {
		update("AtchFileDAO.updateAtchFileDetailDelAll", atchFileVO);
	}
	
	public int selectSumFileSize(AtchFileVO atchFileVO)throws NkiaException {
		int sumFileSize = (Integer)selectByPk("AtchFileDAO.selectSumFileSize", atchFileVO);
		return sumFileSize;
	}

	public void updateAtchFileEmptyBlob(AtchFileVO atchFileVO)throws NkiaException {
		update("AtchFileDAO.updateAtchFileEmptyBlob", atchFileVO);
	}

	public List selectAtchFileDelList(AtchFileVO atchFileVO)throws NkiaException {
		return list("AtchFileDAO.selectAtchFileDelList", atchFileVO);
	}
	
	public HashMap selectFileName(String id) throws NkiaException{
		return (HashMap)selectByPk("AtchFileDAO.selectFileName", id);
	}

	public String getAtchFileSeq() throws NkiaException{
		return (String)selectByPk("AtchFileDAO.getAtchFileSeq", null);
	}

	public int selectAtchFileCount(AtchFileVO atchFileVO) throws NkiaException{
		return (Integer)selectByPk("AtchFileDAO.selectAtchFileCount", atchFileVO);
	}

	public void updateAtchFileDetailForStream(Map paramMap) throws NkiaException{
		update("AtchFileDAO.updateAtchFileDetailForStream", paramMap);
	}

	public HashMap selectAtchFileBinary(AtchFileDetailVO atchFileDetailVO) throws NkiaException{
		return (HashMap)selectByPk("AtchFileDAO.selectAtchFileBinary", atchFileDetailVO);
	}

	/**
	 * API로 첨부파일 다운로드 하기 위한 첨부파일 상세 정보 조회
	 * @param atchFileDetailVO
	 * @return
	 * @throws NkiaException
	 */
	public HashMap selectAtchFileDetailForService(AtchFileDetailVO atchFileDetailVO) throws NkiaException {
		return (HashMap)selectByPk("AtchFileDAO.selectAtchFileDetailForService", atchFileDetailVO);
	}

	
}
