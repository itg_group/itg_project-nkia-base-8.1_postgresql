/*
 * @(#)AtchFileController.java              2013. 1. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;

import egovframework.com.cmm.EgovMessageSource;

@Controller
public class AtchFileController {
	
	@Resource(name = "atchFileService")
	private AtchFileService atchFileService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil;
	
	/**
	 * 
	 * Grid Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/getAtchFileSeq.do")
	public @ResponseBody String getAtchFileSeq()throws NkiaException {
		String atchFileSeq = null;
		try{
			atchFileSeq = atchFileService.getAtchFileSeq();
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return atchFileSeq;
	}	
	
	/**
	 * 
	 * 파일첨부 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/base/selectAtchFileList.do")
	public @ResponseBody ResultVO selectAtchFileList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			List resultList = atchFileService.selectAtchFileList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {  
			throw new NkiaException(e);	  
		}
		return resultVO; 
	}
	
	
	/**
	 * 
	 * 프로세스 파일첨부 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/base/selectAtchFileListForProc.do")
	public @ResponseBody ResultVO selectAtchFileListForProc(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		List resultList = null;
		try{
			GridVO gridVO = new GridVO();
			resultList = atchFileService.selectAtchFileListForProc(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {  
			throw new NkiaException(e);	  
		}
		return resultVO; 
	}
	
	/**
	 * 
	 * 서식파일 다운로드 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/base/selectAtchFileListForTemplate.do")
	public @ResponseBody ResultVO selectAtchFileListForTemplate(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		List resultList = null;
		try{
			GridVO gridVO = new GridVO();
			resultList = atchFileService.selectAtchFileListForTemplate(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {  
			throw new NkiaException(e);	  
		}
		return resultVO; 
	}
	/**
	 * 
	 * 파일첨부 최초등록
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws JSONException 
	 */
	@RequestMapping(value = "/itg/base/initRegistAtchFile.do")
	public void initRegistAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException, JSONException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			// 파일등록
			MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
			
			
			Iterator<String> itr =  mptRequest.getFileNames();
//	        if(itr.hasNext()) {
//	            MultipartFile mpf = mptRequest.getFile(itr.next());
//	            try {
//	                //just temporary save file info into ufile
//	                System.out.println("file length : " + mpf.getBytes().length);
//	                System.out.println("file name : " + mpf.getOriginalFilename());
//	            } catch (IOException e) {
//	            	throw new NkiaException(e);
//	            }
//	        } 
			
			atchFileVO = atchFileService.initRegistAtchFile(atchFileVO, mptRequest.getFileMap());

	        //sets success to true
			returnData.put("success", true);
			returnData.put("atch_file_id", atchFileVO.getAtch_file_id());
	        
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			// @@ 2014.10.03.정정윤 추가 파일사이즈 0일때 예외처리
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Size_Zero") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.size.zero"));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Ext_Error") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.ext.error"));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
			
			new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * 보고서 파일첨부 최초등록
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws JSONException 
	 */
	@RequestMapping(value = "/itg/base/initReportRegistAtchFile.do")
	public void initReportRegistAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException, JSONException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			// 파일등록
			MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
			
			
			Iterator<String> itr =  mptRequest.getFileNames();
//	        if(itr.hasNext()) {
//	            MultipartFile mpf = mptRequest.getFile(itr.next());
//	            System.out.println(mpf.getOriginalFilename() +" uploaded!");
//	            try {
//	                //just temporary save file info into ufile
//	                System.out.println("file length : " + mpf.getBytes().length);
//	                System.out.println("file name : " + mpf.getOriginalFilename());
//	            } catch (IOException e) {
//	            	throw new NkiaException(e);
//	            }
//	        } 
			
			atchFileVO = atchFileService.initReportRegistAtchFile(atchFileVO, mptRequest.getFileMap());

	        //sets success to true
			returnData.put("success", true);
			returnData.put("atch_file_id", atchFileVO.getAtch_file_id());
	        
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			// @@ 2014.10.03.정정윤 추가 파일사이즈 0일때 예외처리
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Size_Zero") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.size.zero"));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Ext_Error") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.ext.error"));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
			
			new NkiaException(e);
		}
	}
	
	@RequestMapping(value = "/itg/base/initRegistAtchFileByAjax.do")
    @ResponseBody
    public void initRegistAtchFileByAjax(MultipartHttpServletRequest request) throws NkiaException {
        Iterator<String> itr =  request.getFileNames();

    }
	
	/**
	 * 
	 * 파일첨부 수정등록
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws JSONException 
	 */
	@RequestMapping(value = "/itg/base/modifyRegistAtchFile.do")
	public void modifyRegistAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException, JSONException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
	        atchFileService.modifyRegistAtchFile(atchFileVO, mptRequest.getFileMap());
	        //sets success to true
	        returnData.put("success", true);
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			// @@ 2014.10.03.정정윤 추가 파일사이즈 0일때 예외처리
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Size_Zero") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.size.zero"));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Ext_Error") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.ext.error"));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getCause().getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
			
			new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * 파일첨부 삭제 (Ajax 용)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value = "/itg/base/deleteAtchFileByAjax.do")
	public void deleteAtchFileByAjax(@RequestBody ModelMap paramMap, HttpServletResponse response) throws NkiaException, IOException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
        
        AtchFileVO atchFileVO = new AtchFileVO();
        
		try {
			
			String atchFileId = StringUtil.parseString(paramMap.get("atch_file_id"));
			String deleteFileInfo = StringUtil.parseString(paramMap.get("delete_file_info"));
			
			atchFileVO.setAtch_file_id(atchFileId);
			atchFileVO.setDelete_file_info(deleteFileInfo);
			
	        atchFileService.deletetAtchFileByAjax(atchFileVO);
	        //sets success to true
	        returnData.put("success", true);
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			throw new NkiaException(e);
/*			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
*/		}
	}
	
	/**
	 * 
	 * 파일첨부 삭제
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value = "/itg/base/deleteRegistAtchFile.do")
	public void deleteRegistAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
	        atchFileService.deleteRegistAtchFile(atchFileVO);
	        //sets success to true
	        returnData.put("success", true);
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			throw new NkiaException(e);
			/*if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
*/		}
	}
	
	/**
	 * 
	 * 파일다운로드(개별)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileDetailVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/base/downloadAtchFile.do")
	public void downloadAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO) throws NkiaException {
		try {
			boolean isSuccess = atchFileService.downloadAtchFile(request, response, atchFileDetailVO);
			if( !isSuccess ){
				PrintWriter out = response.getWriter();
				response.setContentType("text/html; charset=utf-8");
				out.println("FileNotFound");
			}
		} catch (Exception e) {
			throw new NkiaException(e);
/*			PrintWriter out = response.getWriter();
			response.setContentType("text/html; charset=utf-8");
			out.println("FileNotFound");*/
		}
	}
	
	/**
	 * 
	 * 파일 압축다운로드
	 * 
	 * @param request
	 * @param response 
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/base/downloadZipFile.do")
	public void downloadZipFile(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException {
		try {
			boolean isSuccess = atchFileService.downloadZipFile(request, response, atchFileVO);
			if( !isSuccess ){
				PrintWriter out = response.getWriter();
				response.setContentType("text/html; charset=utf-8");
				out.println("FileNotFound");
			}
		} catch (Exception e) {
			throw new NkiaException(e);
/*			PrintWriter out = response.getWriter();
			response.setContentType("text/html; charset=utf-8");
			out.println("FileNotFound");*/
		}
	}
	
	/**
	 * 
	 *  템플릿 다운로드
	 * 
	 * @param request
	 * @param response
	 * @return 
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/base/downloadTemplate.do")
	public void downloadTemplate(HttpServletRequest request, HttpServletResponse response) throws NkiaException {
		ResultVO resultVO = new ResultVO();		
		try{
			HashMap fileInfo = atchFileService.selectFileName(request.getParameter("atch_file_id"));
			String saveFileNm = (String) fileInfo.get("STORE_FILE_NAME");
			String origFileNm = (String) fileInfo.get("ORIGNL_FILE_NAME");

			String stordFilePate = BaseConstraints.getTemplateFilePath() ;

			if (saveFileNm != null && !"".equals(saveFileNm)) {
				saveFileNm = saveFileNm.replaceAll("/", "");
				saveFileNm = saveFileNm.replaceAll("\\.", "");
				saveFileNm = saveFileNm.replaceAll("&", "");
			}

			if (origFileNm != null && "".equals(origFileNm)) {
				origFileNm = saveFileNm;
			} else {
				origFileNm = saveFileNm.replaceAll("/", "");
				origFileNm = saveFileNm.replaceAll("\\.", "");
				origFileNm = saveFileNm.replaceAll("&", "");
			}
			
		    boolean isSuccess = atchFileUtil.createOutputFile(request, response, saveFileNm, origFileNm, stordFilePate, "application/vnd.ms-excel");
		    
			if ( !isSuccess ){
				PrintWriter out = response.getWriter();
				response.setContentType("text/html; charset=euc-kr");
				resultVO.setResultMsg("파일을 찾을 수 없습니다.");
			}
		    
		} catch(Exception e) {
			throw new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * 파일첨부 최초등록_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param AtchFileForStreamVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws JSONException 
	 */
	@RequestMapping(value = "/itg/base/initRegistAtchFileForStream.do")
	public void initRegistAtchFileForStream(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException, JSONException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			// 파일등록
			MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
			
			
//			Iterator<String> itr =  mptRequest.getFileNames();
//	        if(itr.hasNext()) {
//	            MultipartFile mpf = mptRequest.getFile(itr.next());
//	            System.out.println(mpf.getOriginalFilename() +" uploaded!");
//	            try {
//	                //just temporary save file info into ufile
//	                System.out.println("file length : " + mpf.getBytes().length);
//	                System.out.println("file name : " + mpf.getOriginalFilename());
//	            } catch (IOException e) {
//	            	throw new NkiaException(e);
//	            }
//	        } 
			
	        atchFileVO = atchFileService.initRegistAtchFileForStream(atchFileVO, mptRequest.getFileMap());

	        //sets success to true
			returnData.put("success", true);
			returnData.put("atch_file_id", atchFileVO.getAtch_file_id());
	        
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			// @@ 2014.10.03.정정윤 추가 파일사이즈 0일때 예외처리
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Size_Zero") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.size.zero"));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Ext_Error") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.ext.error"));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
			
			throw new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * 파일첨부 수정등록_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws JSONException 
	 */
	@RequestMapping(value = "/itg/base/modifyRegistAtchFileForStream.do")
	public void modifyRegistAtchFileForStream(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException, JSONException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
	        atchFileService.modifyRegistAtchFileForStream(atchFileVO, mptRequest.getFileMap());
	        //sets success to true
	        returnData.put("success", true);
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			// @@ 2014.10.03.정정윤 추가 파일사이즈 0일때 예외처리
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Size_Zero") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.size.zero"));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Ext_Error") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.ext.error"));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getCause().getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
			
			throw new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * 파일첨부 삭제 (Ajax 용)_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param AtchFileForStreamVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws JSONException 
	 */
	@RequestMapping(value = "/itg/base/deleteAtchFileForStreamByAjax.do")
	public void deleteAtchFileForStreamByAjax(@RequestBody ModelMap paramMap, HttpServletResponse response) throws NkiaException, IOException, JSONException {
		JSONObject returnData = new JSONObject();
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
        
        AtchFileVO atchFileVO = new AtchFileVO();
        
		try {
			
			String AtchFileForStreamId = StringUtil.parseString(paramMap.get("atch_file_id"));
			String deleteFileInfo = StringUtil.parseString(paramMap.get("delete_file_info"));
			
			atchFileVO.setAtch_file_id(AtchFileForStreamId);
			atchFileVO.setDelete_file_info(deleteFileInfo);
			
	        atchFileService.deletetAtchFileForStreamByAjax(atchFileVO);
	        //sets success to true
	        returnData.put("success", true);
	        //convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
			
			new NkiaException(e);
		}
	}
	
	/**
	 * 
	 * 파일다운로드(개별)_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileDetailVO
	 * @throws NkiaException
	 */
	@RequestMapping(value = "/itg/base/downloadAtchFileForStream.do")
	public void downloadAtchFileForStream(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO) throws NkiaException {
		try {
			boolean isSuccess = atchFileService.downloadAtchFileForStream(request, response, atchFileDetailVO);
			if( !isSuccess ){
				PrintWriter out = response.getWriter();
				response.setContentType("text/html; charset=utf-8");
				out.println("FileNotFound");
			}
		} catch (Exception e) {
			throw new NkiaException(e);
/*			PrintWriter out = response.getWriter();
			response.setContentType("text/html; charset=utf-8");
			out.println("FileNotFound");*/
		}
	}
	
	
}
