/*
 * @(#)ItgBaseController.java              2013. 1. 15.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.PagingUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.json.JsonUtil;
import com.nkia.itg.base.service.ItgBaseService;
import com.nkia.itg.base.service.SessionService;
import com.nkia.itg.base.vo.GridVO;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import net.sf.json.JSONObject;

/**
 * 
 * @version 1.0 2013. 1. 15.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * 	ITG 화면제어 베이스 Controller
 * </pre>
 *
 */
@Controller
public class ItgBaseController {

	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "itgBaseService")
	private ItgBaseService itgBaseService;
	
	@Resource(name = "sessionService")
	private SessionService sessionService;
	
	
	/**
	 * 
	 * Grid Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/getGridMsgSource.do")
	public @ResponseBody Map<String, String> getGridMsgSource(@RequestBody ModelMap paramMap)throws NkiaException {
		HashMap<String, String> getGridMsgSourceMap = new HashMap<String, String>();
		try{
			String resource_prefix = (String)paramMap.get("resource_prefix");
			
			getGridMsgSourceMap.put("groupheader", messageSource.getMessage(resource_prefix + ".groupheader"));
			getGridMsgSourceMap.put("groupcolspan", messageSource.getMessage(resource_prefix + ".groupcolspan"));
			getGridMsgSourceMap.put("gridText", messageSource.getMessage(resource_prefix + ".text"));
			getGridMsgSourceMap.put("gridHeader", messageSource.getMessage(resource_prefix + ".header"));
			getGridMsgSourceMap.put("gridWidth", messageSource.getMessage(resource_prefix + ".width"));
			getGridMsgSourceMap.put("gridFlex", messageSource.getMessage(resource_prefix + ".flex"));
			getGridMsgSourceMap.put("gridSortable", messageSource.getMessage(resource_prefix + ".sortable"));
			getGridMsgSourceMap.put("gridAlign", messageSource.getMessage(resource_prefix + ".align"));
			getGridMsgSourceMap.put("gridXtype", messageSource.getMessage(resource_prefix + ".xtype"));
			getGridMsgSourceMap.put("gridTpl", messageSource.getMessage(resource_prefix + ".tpl"));
			getGridMsgSourceMap.put("gridFormat", messageSource.getMessage(resource_prefix + ".format"));
			getGridMsgSourceMap.put("gridEtype", messageSource.getMessage(resource_prefix + ".etype"));
			getGridMsgSourceMap.put("summaryheader", messageSource.getMessage(resource_prefix + ".summaryheader"));
			getGridMsgSourceMap.put("summarycolspan", messageSource.getMessage(resource_prefix + ".summarycolspan"));
			getGridMsgSourceMap.put("summarycoltype", messageSource.getMessage(resource_prefix + ".summarycoltype"));
			getGridMsgSourceMap.put("mergeindex", messageSource.getMessage(resource_prefix + ".mergeindex"));
			getGridMsgSourceMap.put("bgColor", messageSource.getMessage(resource_prefix + ".bgcolor"));
			
			getGridMsgSourceMap.put("excelyn", messageSource.getMessage(resource_prefix + ".excelyn"));						/* 엑셀템플릿 자동 생성(By Poi) */
			getGridMsgSourceMap.put("excelText", messageSource.getMessage(resource_prefix + ".excelText"));					/* 엑셀템플릿 자동 생성(By Poi) */
			getGridMsgSourceMap.put("displayType", messageSource.getMessage(resource_prefix + ".displayType"));				/* 엑셀템플릿 자동 생성(By Poi) */
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return getGridMsgSourceMap;
	}	
	
	/**
	 * 
	 * Excel Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/getExcelMsgSource.do")
	public @ResponseBody Map<String, String> getExcelMsgSource(@RequestBody ModelMap paramMap)throws NkiaException {
		HashMap<String, String> getExcelMsgSourceMap = new HashMap<String, String>();
		try{
			String excel_prefix = (String)paramMap.get("excel_prefix");
			
			getExcelMsgSourceMap.put("colNm", messageSource.getMessage(excel_prefix + ".colnm"));
			getExcelMsgSourceMap.put("colId", messageSource.getMessage(excel_prefix + ".colid"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return getExcelMsgSourceMap;
	}	
	//@@20130711 전민수 END
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/getFormMsgSource.do")
	public @ResponseBody Map<String, String> getFormMsgSource(@RequestBody ModelMap paramMap)throws NkiaException {
		HashMap<String, String> getFormMsgSourceMap = new HashMap<String, String>();
		try{
			String resource_prefix = (String)paramMap.get("resource_prefix");
			
			getFormMsgSourceMap.put("formId", messageSource.getMessage(resource_prefix + ".id"));
			getFormMsgSourceMap.put("formLabel", messageSource.getMessage(resource_prefix + ".label"));
			getFormMsgSourceMap.put("formXtype", messageSource.getMessage(resource_prefix + ".xtype"));
			getFormMsgSourceMap.put("formWidth", messageSource.getMessage(resource_prefix + ".width"));
			getFormMsgSourceMap.put("formAllowBlank", messageSource.getMessage(resource_prefix + ".allowBlank"));
			getFormMsgSourceMap.put("formEmptyText", messageSource.getMessage(resource_prefix + ".emptyText"));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return getFormMsgSourceMap;
	}

	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchCodeDataList.do")
	public @ResponseBody ResultVO searchCodeDataList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			paramMap.put("session_locale", sessionService.getSessionLocale());
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchCodeDataList(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 소프트웨어 분류 공통코드 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchSWCodeDataList.do")
	public @ResponseBody ResultVO searchSoftwareCodeDataList(@RequestBody ModelMap paramMap, @RequestParam Map<String, Object> reqMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			paramMap.put("session_locale", sessionService.getSessionLocale());
			GridVO gridVO = new GridVO();
			
			String swTypeJson = NkiaApplicationPropertiesMap.getProperty("Globals.Software.Type.Code");
			JSONObject jsonObj = JsonUtil.readDataToJsonObject(swTypeJson);
			
			paramMap.put("code_grp_id", jsonObj.get(reqMap.get("class_id")));
			
			gridVO.setRows(itgBaseService.searchCodeDataList(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 사용 부대 (REQUEST ZONE) 의 코드 조회 - 통합센터 포함된 코드
	 * 
	 * @param paramMap
	 * @return ResultVO
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchReqZoneDataList.do")
	public @ResponseBody ResultVO searchReqZoneDataList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			if(userVO != null){
				String custId = userVO.getCust_id();
				paramMap.put("userCustId", custId);
				gridVO.setRows(itgBaseService.searchReqZoneDataList(paramMap));
			}
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 구성정보 조회용 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchConfTypeList.do")
	public @ResponseBody ResultVO searchConfTypeList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchConfTypeList(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/base/searchMutilCodeDataListForTree.do")
	public @ResponseBody ResultVO searchMutilCodeDataListForTree(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = itgBaseService.searchMutilCodeDataListForTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			Integer expand_level = (Integer)paramMap.get("expandLevel");
			TreeVO treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/excelTest.do")
	public ModelAndView excelTest(ModelMap paramMap) throws NkiaException {
		List<Map> lists = new ArrayList<Map>();
		 
		Map<String, String> mapCategory = new HashMap<String, String>();
		mapCategory.put("id", "0000000001");
		mapCategory.put("name", "Sample Test");
		mapCategory.put("description", "This is initial test data.");
		mapCategory.put("useyn", "Y");
		mapCategory.put("reguser", "test");
	 
		lists.add(mapCategory);
	 
		mapCategory.put("id", "0000000002");
		mapCategory.put("name", "test Name");
		mapCategory.put("description", "test Deso1111");
		mapCategory.put("useyn", "Y");
		mapCategory.put("reguser", "test");
	 
		lists.add(mapCategory);
	 
		return new ModelAndView("commonExcelView", "excelDataList", lists);
	}
	
	/**
	 * 
	 * Return to Args Message 
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/getMessage.do")
	public @ResponseBody Map<String, String> getMessage(@RequestBody ModelMap paramMap)throws NkiaException { 
		HashMap<String, String> getMessageMap = new HashMap<String, String>();
		try{
			String code = (String)paramMap.get("m_key");
			Object[] args = null;
			if( paramMap.containsKey("args") ){
				args = new String[]{StringUtil.parseString(paramMap.get("args"))};
				getMessageMap.put("returnMessage", messageSource.getMessage(code, args));
			}else{
				getMessageMap.put("returnMessage", messageSource.getMessage(code));
			}
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return getMessageMap;
	}
	
	
	@RequestMapping(value="/itg/base/searchEmsTableList.do")
	public @ResponseBody ResultVO searchEmsTableList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchEmsTableList(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	@RequestMapping(value="/itg/base/searchEmsColList.do")
	public @ResponseBody ResultVO searchEmsColList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchEmsColList(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/base/searchClassType.do")
	public @ResponseBody ResultVO searchClassType(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchClassType(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/base/searchServiceNodeName.do")
	public @ResponseBody ResultVO searchServiceNodeName(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchServiceNodeName(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/base/searchChangeNodeName.do")
	public @ResponseBody ResultVO searchChangeNodeName(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchChangeNodeName(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/base/searchIncidentNodeName.do")
	public @ResponseBody ResultVO searchIncidentNodeName(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchIncidentNodeName(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/base/searchProblemNodeName.do")
	public @ResponseBody ResultVO searchProblemNodeName(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchProblemNodeName(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/base/searchCenterNodeName.do")
	public @ResponseBody ResultVO searchCenterNodeName(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchCenterNodeName(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	@RequestMapping(value="/itg/base/searchDeadlineNodeName.do")
	public @ResponseBody ResultVO searchDeadlineNodeName(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchDeadlineNodeName(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * AutoDiscovery 연계 테이블 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchAutoDiscoveryTableList.do")
	public @ResponseBody ResultVO searchAutoDiscoveryTableList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			
			GridVO gridVO = new GridVO();
			List resultList = itgBaseService.searchAutoDiscoveryTableList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * AutoDiscovery 연계 테이블 컬럼
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchAutoDiscoveryColList.do")
	public @ResponseBody ResultVO searchAutoDiscoveryColList(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try{
			GridVO gridVO = new GridVO();
			List resultList = itgBaseService.searchAutoDiscoveryColList(paramMap);
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서 트리 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchDeptTree.do")
	public @ResponseBody ResultVO searchDeptTree(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		TreeVO treeVO = null;
		List resultList = new ArrayList();
		try{
			resultList = itgBaseService.searchDeptTree(paramMap);
			String up_node_id = (String)paramMap.get("up_node_id");
			int expand_level = (Integer)paramMap.get("expandLevel");
			treeVO = JsonUtil.changeTreeVOToJson(resultList, up_node_id, true, expand_level);
			resultVO.setTreeVO(treeVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 부서 사용자 그리드 호출 
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/base/searchDeptUserGrid.do")
	public @ResponseBody ResultVO searchDeptUserGrid(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			int totalCount = 0;
			PagingUtil.getFristEndNum(paramMap);
		
			totalCount = itgBaseService.searchDeptUserGridListCount(paramMap);
			List resultList = itgBaseService.searchDeptUserGridList(paramMap);
			
			gridVO.setTotalCount(totalCount);
			gridVO.setCursorPosition((Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString()));
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * TODO 메모리 그리드 사용시 dummy 데이터 호출용 (empty)
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/selectDummyDataForMemoryGrid.do")
	public @ResponseBody ResultVO selectDummyDataForMemoryGrid(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			GridVO gridVO = new GridVO();
			gridVO.setRows(new ArrayList());
			gridVO.setTotalCount(0);
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 메뉴정보
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/base/selectMenuInfo.do")
	public @ResponseBody ResultVO selectMenuInfo(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = itgBaseService.selectMenuInfo(paramMap);
			resultVO.setResultBoolean(true);
			resultVO.setResultMap((Map)resultList.get(0));
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * 분류체계목록 코드형식 조회
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/base/selectAmClassTypeCode.do")
	public @ResponseBody ResultVO selectAmClassTypeCode(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = itgBaseService.selectAmClassTypeCode(paramMap);
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	/**
	 * 
	 * Form Message Source
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchAssetStateCodeList.do")
	public @ResponseBody ResultVO searchAssetStateCodeList(@RequestBody ModelMap paramMap, HttpServletRequest request)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			String classId = NkiaApplicationPropertiesMap.getProperty("Globals.Disposable.ClassId");
			String[] classIdArr = classId.split(",");
			
			
			paramMap.put("session_locale", sessionService.getSessionLocale());
			paramMap.put("conf_id", StringUtil.parseString(request.getParameter("conf_id")));
			paramMap.put("classIdList", classIdArr);
			
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.selectAssetStateCodeList(paramMap));
			resultVO.setGridVO(gridVO);
			resultVO.setSuccess(true);
		} catch(Exception e) {
			throw new NkiaException(e); 
		}
		return resultVO;
	}
	
	/**
	 * 
	 * 권한있는 고객사 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/base/searchCustomerToUserList.do")
	public @ResponseBody ResultVO searchCustomerToUserList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
			String userId = "";
			userId = userVO.getUser_id();
			
			paramMap.put("login_user_id", userId);
			
			GridVO gridVO = new GridVO();
			gridVO.setRows(itgBaseService.searchCustomerToUserList(paramMap));
			resultVO.setGridVO(gridVO);
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	//hmsong cmdb 개선 작업 Start
	/**
	 * 
	 * CONF_TYPE 목록 코드형식 조회(트리)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/base/searchConfTypeCodeList.do")
	public @ResponseBody ResultVO searchConfTypeCodeList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = itgBaseService.searchConfTypeCodeList(paramMap);
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	
	
	/**
	 * CLASS_ID 목록 코드형식 조회(트리)
     * @param paramMap
	 * @return
	 * @
	 */
	@RequestMapping(value="/itg/base/searchClassIdCodeList.do")
	public @ResponseBody ResultVO searchClassIdCodeList(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		try{
			List resultList = itgBaseService.searchClassIdCodeList(paramMap);
			GridVO gridVO = new GridVO();
			gridVO.setRows(resultList);
			resultVO.setGridVO(gridVO);
			
		} catch(Exception e) {
			throw new NkiaException(e);
		}
		return resultVO;
	}
	//hmsong cmdb 개선 작업 End
}
