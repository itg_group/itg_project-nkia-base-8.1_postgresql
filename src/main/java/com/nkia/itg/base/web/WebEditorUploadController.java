/*
 * @(#)AtchFileController.java              2013. 1. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.WebEditorUploadService;
import com.nkia.itg.base.vo.AtchFileVO;

import egovframework.com.cmm.EgovMessageSource;

@SuppressWarnings("rawtypes")
@Controller
public class WebEditorUploadController {
	
	@Resource(name = "webEditorUploadService")
	private WebEditorUploadService webEditorUploadService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	/**
	 * 
	 * 파일첨부 게시판 이미지
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	@RequestMapping(value = "/itg/base/uploadWebEditorImage.do")
	public void uploadWebEditorImage(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO) throws NkiaException, IOException {
		JSONObject returnData = new JSONObject();
		
		//set content type
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
		
		//PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();
		try {
			// 파일등록
			Map result = webEditorUploadService.uploadWebEditorImage(atchFileVO, request);
			
	        //sets success to true
			returnData.put("success", true);
			returnData.put("resultData", result);
	        
			//convert the JSON object to string and send the response back
	        out.println(returnData.toString());
	        out.close();
		} catch (Exception e) {
/*			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_Unit_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.unit.max.size", new String[]{String.valueOf(atchFileVO.getFile_unit_max_size())}));
			}
			if( atchFileVO.getResultMsg() != null && atchFileVO.getResultMsg().equals("File_All_Max_Size_Over") ){
				atchFileVO.setResultMsg(messageSource.getMessage("msg.common.file.all.max.size", new String[]{String.valueOf(atchFileVO.getFile_all_max_size())}));
			}
			if( atchFileVO.getResultMsg() == null ){
				atchFileVO.setResultMsg(e.getMessage());
			}
			returnData.put("resultMsg", atchFileVO.getResultMsg());
			returnData.put("failure", true);
			
			out.println(returnData.toString());		// out.println으로 내보내기 위해서 new NKIAException을 사용하지 않음.
*/			throw new NkiaException(e);
		}
	}
}
