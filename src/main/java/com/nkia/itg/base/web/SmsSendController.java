package com.nkia.itg.base.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.SmsSendService;
import com.nkia.itg.base.vo.ResultVO;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.system.user.service.UserService;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Controller
public class SmsSendController {

	private static final Logger logger = LoggerFactory.getLogger(SmsSendController.class);
	
	@Resource(name="smsSendService")
	private SmsSendService smsSendService;
	
	@Resource(name = "userService")
	private UserService userService;
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	
	
	@RequestMapping(value = "/itg/base/smsSend.do")
	public @ResponseBody ResultVO smsSend(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
		SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
		try {
			List smsSendUserList = (List) paramMap.get("smsSendUserList");
			Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
			String upd_user_id = "SYSTEM";
	    	if(isAuthenticated) {
	    		UserVO userVO = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
	    		upd_user_id = userVO.getUser_id();
	    	}
			
			for (int i = 0; i < smsSendUserList.size(); i++) {
				
				Map param = new HashMap();
				//Map paramHist = new HashMap();
				param.putAll((Map)smsSendUserList.get(i));
				param.put("FROMHP",paramMap.get("fromHp"));
				param.put("IndoGbn",paramMap.get("IndoGbn"));
				param.put("MSG_TYPE","0");
				param.put("upd_user_id", upd_user_id);
				//param.put(", value)
				String toPhone = (String)param.get("TO_PHONE");
				String toName = (String)param.get("TO_NAME");
				String content = (String)param.get("MAP_CONTENT");
				if(!(null == toPhone || "".equals(toPhone)) && !(null == toName || "".equals(toName))) {
					/*
					 * 프로시저를 통해서 등록하는 경우
					 */
					smsSendService.callSmsSend(param);
					//smsSendService.insertSmsSend(param);
					//smsSendService.insertSmsgHist(param);
				} else {
					logger.info("Not found phone number");
				}
				
			}
			// 발송완료 메시지 
			resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00010"));

		} catch (Exception e) {

			throw new NkiaException(e);
		}

		return resultVO;
	}

	@RequestMapping(value = "/itg/base/pwChange.do")
	public @ResponseBody ResultVO pwChange(@RequestBody ModelMap paramMap) throws NkiaException {
		ResultVO resultVO = new ResultVO();
//		SmsSendService smsSendService = (SmsSendService)NkiaApplicationContext.getCtx().getBean("smsSendService");
//		try {
//			
//			
//				Map param = new HashMap();
//				//Map paramHist = new HashMap();
//				HashMap resultMap = new HashMap();
//				resultMap = userService.selectUserInfoDetail(paramMap);
//				if( resultMap.get("HP_NO") != null){
//					paramMap.put("TO_PHONE",resultMap.get("HP_NO").toString().replace("-", ""));
//					
//					char pwCollection[] = new char[] { 
//			                '1','2','3','4','5','6','7','8','9','0'
//			                };//배열에 선언 
//			
//			    	String ranPw = ""; 
//			    	Random r = new Random();
//			    	r.setSeed(new Date().getTime()); 
//			    	for (int i = 0; i < 4; i++) { 
//			    		int selectRandomPw = (int)(Math.random()*(pwCollection.length));//Math.rondom()은 0.0이상 1.0미만의 난수를 생성해 준다. 
//			    		ranPw += pwCollection[selectRandomPw]; 
//			    	} 
//			    	paramMap.put("TWOFACTOR", ranPw);
//			    	if("system".equals(paramMap.get("type"))){
//			    		paramMap.put("MSG_BODY", "[센터ITSM(시스템관리)] 본인 인증을 위해 [인증번호 : "+ranPw + "]를 입력해 주세요.");
//			    	}else{
//			    		paramMap.put("MSG_BODY", "[센터ITSM] 본인 인증을 위해 [인증번호 : "+ranPw + "]를 입력해 주세요.");
//			    	}
//			    	
//			    	paramMap.put("SENDER_KEY", "0cede52df082258ff06b59c92dffff975844c35f");
//			    	paramMap.put("TEMPLATE_CODE", "LMSG_20170628112406858657");
//			    	paramMap.put("RE_BODY", "[센터ITSM][인증번호 :" + ranPw + "]를 입력해 주세요.");
//			    	paramMap.put("SEND_PHONE", "0226264210"); // 보내는사람
//			    	paramMap.put("USER_NM", resultMap.get("USER_NM").toString()); 
//			    	
//			    	userService.updateTwofactor(paramMap);
//			    	//smsSendService.callSmsSend(paramMap);
//			    	smsSendService.insertTalkList(paramMap);
//			    	
//					//param.put(", value)
//		//			String toPhone = (String)param.get("TO_PHONE");
//		//			String toName = (String)param.get("TO_NAME");
//		//			String content = (String)param.get("MAP_CONTENT");
//		//			if(!(null == toPhone || "".equals(toPhone)) && !(null == toName || "".equals(toName))) {
//		//				/*
//		//				 * 프로시저를 통해서 등록하는 경우
//		//				 */
//		//				smsSendService.callSmsSend(param);
//		//				//smsSendService.insertSmsSend(param);
//		//				smsSendService.insertSmsgHist(param);
//		//			} else {
//		//				logger.info("Not found phone number");
//		//				logger.info("Map data : " + param);
//		//			}
//		//				
//					// 발송완료 메시지 
//					resultVO.setResultMsg(messageSource.getMessage("msg.common.result.00010"));
//			}else{
//				resultVO.setResultMsg("발송번호가 등록되어있지 않습니다. 관리자에게 문의를 주세요.");
//			}
//		} catch (Exception e) {
//
//			throw new NkiaException(e);
//		}

		return resultVO;
	}
}
