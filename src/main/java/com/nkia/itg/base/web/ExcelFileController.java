package com.nkia.itg.base.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.service.ExcelFileService;

@Controller
public class ExcelFileController {
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil;
	
	@Resource(name = "excelFileService")
	private ExcelFileService excelFileService;
	
	@RequestMapping(value = "/itg/base/downloadExcelFile")
	public void downloadExcelFile(HttpServletRequest request, HttpServletResponse response) throws NkiaException, IOException {
		String fileId = request.getParameter("fileId");
		//String naviInfo = URLUTF8Encoder.unescape(request.getParameter("naviInfo"));
		String naviInfo = request.getParameter("naviInfo");
		
		Map paramMap = new HashMap();
		paramMap.put("excel_file_id", fileId);
		paramMap.put("navigation", naviInfo);
		
		Map fileData = excelFileService.selectExcelFileDetail(paramMap);
		
	    String fileName = (String) fileData.get("REAL_FILE_NM");
	    String filePath = (String) fileData.get("UPLOAD_PATH");
		
	    boolean isSuccess = atchFileUtil.createOutputFile(request, response, fileName, fileName, filePath, "application/vnd.ms-excel");
		if( !isSuccess ){
			PrintWriter out = response.getWriter();
			response.setContentType("text/html; charset=euc-kr");
			out.println("파일을 찾을 수 없습니다.");
		} else {
			String deleteFlag = NkiaApplicationPropertiesMap.getProperty("Globals.ExcelFile.DeleteYN");
			if("Y".equals(deleteFlag)) {
				deleteFile(fileName, filePath);					
			}
			
			updateExcelNavigation(paramMap);
		}
	}
	
	public synchronized void deleteFile(String fileName, String filePath) {
		File deleteFile = new File(filePath+fileName);
		if ( deleteFile.exists() ){
			deleteFile.delete();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void updateExcelNavigation( Map paramMap ) throws NkiaException {
		try {
			excelFileService.updateExcelNavigation(paramMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
	}
}
