/*
 * @(#)EncyptPropertyController.java              2017. 9. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.web;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.EncryptPropertyService;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.base.vo.ResultVO;

import egovframework.com.cmm.EgovMessageSource;

/**
 * 
 * @version 1.0 2017. 9. 12.
 * @author <a href="mailto:jyjeong@nkia.co.kr"> JeongYun Jeong
 * @since JDK 1.7
 * <pre>
 * 	프로퍼티 파일 암호화 관련 컨트롤러
 * </pre>
 *
 */
@Controller
public class EncyptPropertyController {
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;	
	
	@Resource(name = "encryptPropertyService")
	private EncryptPropertyService encryptPropertyService;
	
	@Resource(name = "urlAccessCheckService")
	private UrlAccessCheckService urlAccessCheckService;
	
	/**
	 * 프로퍼티 암호화 값 생성 페이지 호출
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/encryptProperty/goEncryptProperty.do")
	public String goEncryptProperty(HttpServletRequest request, ModelMap paramMap) throws NkiaException {
		//2018.08.09 메뉴접근검증 추가
		if (!urlAccessCheckService.urlAuthCheck(request)) {
			return BaseEnum.ERROR_CODE_MENU_DENIAL.getString();
		}

		String forwarPage = "/itg/system/encryptProperty/encryptProperty.nvf";
		return forwarPage;
	}
	
	/**
	 * 프로퍼티 암호화 값 생성
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/encryptProperty/generatePropertyEncValue.do")
	public @ResponseBody ResultVO generatePropertyEncValue(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			HashMap resultMap = encryptPropertyService.generatePropertyEncValue(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
	
	/**
	 * 프로퍼티 복호화 값 생성
     * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@RequestMapping(value="/itg/system/encryptProperty/generatePropertyDecValue.do")
	public @ResponseBody ResultVO generatePropertyDecValue(@RequestBody ModelMap paramMap)throws NkiaException {
		ResultVO resultVO = new ResultVO();
		
		try {
			HashMap resultMap = encryptPropertyService.generatePropertyDecValue(paramMap);
			resultVO.setResultMap(resultMap);
		} catch (Exception e) {
			throw new NkiaException(e);
		}

		return resultVO;
	}
}
