package com.nkia.itg.base.application.util.email;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationEmailMap;
import com.nkia.itg.base.application.config.NkiaApplicationEmailTemplateMap;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;


public class EmailSetHappyCallData {
	
	public static EmailVO setData(Map mailInfoMap) throws NkiaException, JsonParseException, JsonMappingException, IOException {		
		
		String reqType = "";
		String templateId = "";
		Map dataMap = new HashMap();
		Map addInfoMap = new HashMap();
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(mailInfoMap != null) {
			reqType = (String) mailInfoMap.get("KEY"); //메일 요청 유형
			templateId = (String) mailInfoMap.get("TEMPLATE_ID"); //메일 템플릿
			dataMap = (Map) mailInfoMap.get("DATA"); //메일발송을 위한 데이터
			addInfoMap = (Map) mailInfoMap.get("ADD_DATA"); //메일 발송을 위한 추가 데이터
		}
		
		EmailVO emailVO = new EmailVO();
		String err_msg = ""; //에러메시지
		
		boolean setFlag = false;
		
		
		//SYS_MAIL_SENDER_INFO 테이블
		// MAIL_ID 구분으로 하나의 메일 서버인 경우는 변수로 기본 설정
		// 다중일 경우에는 조건에 추가 수정
		String emailId = EmailEnum.EMAIL_ID_1.getString();
		String emailConfigData = NkiaApplicationEmailMap.getMailInfo(emailId);
		String emailTemplateId = emailId + "==" + templateId;
		String emailTemplateData = NkiaApplicationEmailTemplateMap.getMailTemplateInfo(emailTemplateId);
		
		
		if (emailConfigData != null && emailTemplateData != null) {
			setFlag = true;
		} else {
			setFlag = false;
			err_msg = "NO DATA - CONFIG, TEMPLATE";
			
			if (emailConfigData == null) {
				err_msg = "NO DATA - CONFIG";
			}
			
			if (emailTemplateData == null) {
				err_msg = "NO DATA - TEMPLATE";
			}
		}
		emailVO.setConfig_flag(setFlag);
		emailVO.setErr_msg(err_msg);
		
		if (setFlag) {
			//메일발송정보
			ObjectMapper mapper = new ObjectMapper();
	        Map<String, Object> configMap = new HashMap<String, Object>();
	        configMap = mapper.readValue(emailConfigData, new TypeReference<Map<String, Object>>() {});
			
			emailVO.setEmail_job_type((String)configMap.get("EMAIL_JOB_TYPE"));
			emailVO.setHost((String)configMap.get("EMAIL_HOST"));
			int port = Integer.parseInt(String.valueOf(configMap.get("EMAIL_PORT")));
			emailVO.setPort(port);
			emailVO.setEmail_admin_id((String)configMap.get("EMAIL_ADMIN_ID"));
			emailVO.setEmail_admin_pw((String)configMap.get("EMAIL_ADMIN_PW"));
			emailVO.setEmail_admin_addr((String)configMap.get("EMAIL_ADMIN_ADDR"));
			emailVO.setEmail_multi_send_yn("N");
			
			//메일 템플릿 테이블 내의 발송 정보
	        Map<String, Object> templateMap = new HashMap<String, Object>();
	        templateMap = mapper.readValue(emailTemplateData, new TypeReference<Map<String, Object>>() {});
			
			//From (발송자) 정보
			emailVO.setFrom((String)templateMap.get("EMAIL_FROM"));
			
			
			
			String srId = (String)addInfoMap.get("SR_ID");
			dataMap.put("SR_ID", srId);
			dataMap.put("TITLE", (String)addInfoMap.get("TITLE"));
			dataMap.put("COMPLETE_COMMENT", (String)addInfoMap.get("COMPLETE_COMMENT"));
			dataMap.put("REQ_USER_NM", (String)addInfoMap.get("REQ_USER_NM"));
			dataMap.put("REQ_DT", (String)addInfoMap.get("REQ_DT"));
			dataMap.put("RCEPT_USER_NM", (String)addInfoMap.get("RCEPT_USER_NM"));
			dataMap.put("RCEPT_DT", (String)addInfoMap.get("RCEPT_DT"));
			dataMap.put("WORK_USER_NM", (String)addInfoMap.get("WORK_USER_NM"));
			dataMap.put("END_DT", (String)addInfoMap.get("END_DT"));
			String returnUrl = NkiaApplicationPropertiesMap.getProperty("Mail.Return.Url");
			returnUrl = returnUrl +  "/goHappyCallPopup.do";
			dataMap.put("HAPPYCALL_URL", returnUrl);
			
			//메일내용
			dataMap.put("CONTENT", (String)addInfoMap.get("CONTENT"));
			String content = getContent(templateId, dataMap);
			String title = "[롯데정보통신] 센터 ITSM 서비스요청 만족도조사";
				
			//발송 데이터 정보
			emailVO.setEmail_req_type(reqType);
			emailVO.setEmail_id(emailId);
			emailVO.setSubject(title);
			emailVO.setContent(content);
			emailVO.setTo_user_list((List)dataMap.get("TO_USER_LIST"));
			emailVO.setTemplate_id(templateId);
		}
		return emailVO;
	}
	
	public static String getContent(String templateName, Map dataInfo) throws NkiaException, IOException {
		EmailTemplateGenerator etg = new EmailTemplateGenerator();
		//String result = etg.generateContent(templateName, masterInfo);
		String result = etg.customGenerateContent(templateName, dataInfo);
		return result;
	}
	
}
