package com.nkia.itg.base.application.filter;

import java.util.Map;

public class SQLInjectionFilter {

	public static final String[] Check_Parameter_Name = {"property","title"};

	public static void runFilter(Map parameterObject) {
		for(int i = 0 ; i < Check_Parameter_Name.length ; i++) {
			String key = Check_Parameter_Name[i];
			String returnValue = (String)((Map) parameterObject).get(key);
			if(returnValue != null && "".equals(returnValue)) {
				returnValue = checkSQLParam(returnValue);
				((Map)parameterObject).put(key, returnValue);					
			}
		}
	}
	
	public static String checkSQLParam(String returnValue) {
		returnValue = returnValue.replace("--", "");
		return returnValue;
	}
}
