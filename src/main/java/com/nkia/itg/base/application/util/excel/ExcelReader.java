package com.nkia.itg.base.application.util.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;

@Service("excelReader")
public class ExcelReader {
	
	/**
	 * 엑셀데이터 변환
	 * @param paramMap
	 * @return
	 * @throws IOException 
	 * @
	 **/
	public HashMap parseSingleSheetExcelData(HashMap paramMap) throws NkiaException, IOException {
		FileInputStream inputStream = null;
		
		try {
			int startRowNum = 3;
			
			String hearders 	= (String) paramMap.get("hearders");
			String dataType 	= (String) paramMap.get("dataType");
			String filePathName = (String) paramMap.get("filePathName");
			String colArray[] 	= hearders.split(",");
			
	    	File file = new File(filePathName);
	    	
	    	if (!file.exists()) {
	    		paramMap.put("storeList", null);
			}
	    	
			inputStream = new FileInputStream(filePathName);
			POIFSFileSystem fileSystem  = new POIFSFileSystem(inputStream);
			HSSFWorkbook workbook       = new HSSFWorkbook(fileSystem);
			
			HSSFSheet sheet = workbook.getSheetAt(0);  //싱글시트이기 때문에  index값은 0이다.
			
			int lastRowNum 	= sheet.getPhysicalNumberOfRows();
			
			ArrayList<HashMap> storeList = new ArrayList();
			HashMap storeMap = null;
			
			for(int k = startRowNum; k < lastRowNum; k++){  
				storeMap = new HashMap();
				HSSFRow row = sheet.getRow(k); 
				if(row != null){
					int lastCellNum = row.getLastCellNum(); 
					for(int p = 0; p <lastCellNum; p++){
						HSSFCell val = row.getCell(p);
						storeMap.put("RNUM", k-2);
						
						if(val.getCellType() == 0){
							storeMap.put(colArray[p], (long)val.getNumericCellValue()+"");
						}else if(val.getCellType() == 1){
							storeMap.put(colArray[p], val.getStringCellValue());
						}
						
						if(lastCellNum == p+1){
							storeList.add(storeMap);
						}
					}
					
					if(lastRowNum == k+2){
						paramMap.put("storeList", storeList);
					}
				}
			}
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
					throw new NkiaException(e);
				}
			}
		}
		return paramMap;
	}
}
