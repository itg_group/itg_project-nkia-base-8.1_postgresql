package com.nkia.itg.base.application.util.excel;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jxl.Cell;
import jxl.JXLException;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.read.biff.BiffException;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.ExcelFileService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("excelMaker")
public class ExcelMaker {
	@Resource(name = "excelFileService")
	private ExcelFileService excelFileService;
	
	/**
	 * @method {} makeExcelFile : 엑셀다운로드
	 * 
	 * @param {} jsonObject : 엑셀 헤더 객체
	 * @param {} fileName : 파일이름
	 * @param {} result : 엑셀에 출력할 쿼리 결과
	 * @return {mapResult: 파일이름, 파일경로}
	 * @throws IOException 
	 * @throws WriteException 
	 * 
	 */
	public ModelMap makeExcelFile(Map excelParams, List result)throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		try {
			if(excelAttrs != null){
				List sheetList = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					int cutRowCount = 50000; // sheet 단위로 잘릴 Row 수...
					
					WritableSheet sheet = null;
					
					//BOLD
					jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					
					//제목
					jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					titleFont.setPointSize(18);
					titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
					
					//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
					jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.GRAY_25, true);
					
					jxl.write.WritableCellFormat bodyHeadFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
					
					jxl.write.Label label = null;
					jxl.write.Number number = null;
					jxl.write.Blank blank = null;
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List dataList = null;

						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						HashMap sheetMap = (HashMap)sheetList.get(i);
						String sheetName = (String)sheetMap.get("sheetName");
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List groupHeaders = (List)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, i+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
						label = new jxl.write.Label(0, 0, titleName, titleFormat);
						sheet.addCell(label);
						
						sheet.setRowView(1, 340);
						label = new jxl.write.Label(0, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//헤더부분을 처리한다.
						if( groupHeaders == null ){
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map data = (Map) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
								sheet.addCell(label);
								
								if( span > 1 ){
									sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
									cols = cols + (span-1);
								}else{
									sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						rows++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								HashMap map = (HashMap)dataList.get(j);
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										if(labelValue == null){
											labelValue = "-";
										}
										if(align != null) {
											if("left".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
											} else if("right".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
											} else if("center".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											}
										} else {
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										}
										sheet.addCell(label);
									}
								}else{
									Iterator iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										sheet.addCell(label);
									}            
								}
								rows++;
							}
						}else{
							String noneResult = "결과가 없습니다.";
							sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, rows, noneResult, bodyFormat);
							sheet.addCell(label);
						}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}
	/**
	 * @method {} makeExcelFileTab : 엑셀다운로드탭으로 정렬 excelParam의 tab이라곳에 들어온 데이터 기준으로  짤라서 보내줌
	 * 
	 * @param {} jsonObject : 엑셀 헤더 객체
	 * @param {} fileName : 파일이름
	 * @param {} result : 엑셀에 출력할 쿼리 결과
	 * @return {mapResult: 파일이름, 파일경로}
	 * @throws IOException 
	 * @throws WriteException 
	 * 
	 */
	public ModelMap makeExcelFileTab(Map excelParams, List result,List SheetName)throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		String tabparam = (String)excelParams.get("tab");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		try {
			if(excelAttrs != null){
				List sheetList = SheetName;
				List sheetParam = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					for(int z=0; z<sheetList.size();z++){
						WritableSheet sheet = null;
						
						//BOLD
						jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
						
						//제목
						jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						titleFont.setPointSize(18);
						titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
						
						//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
						jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
						
						jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
						
						jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.GRAY_25, true);
						
						jxl.write.WritableCellFormat bodyHeadFormat = getWritableCellFormat(null, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
								, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
								, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
						
						jxl.write.Label label = null;
						jxl.write.Number number = null;
						jxl.write.Blank blank = null;
						
						//Head부분을 설정한다.
						int cols = 0;
						int rows = 0;
						
						
						List dataList = result;
						
						HashMap sheetMap = (HashMap)sheetParam.get(0);
						HashMap sheetNameMap = (HashMap)sheetList.get(z);
						String sheetName = (String)sheetNameMap.get(tabparam);
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List groupHeaders = (List)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, z+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
						label = new jxl.write.Label(0, 0, titleName, titleFormat);
						sheet.addCell(label);
						
						sheet.setRowView(1, 340);
						label = new jxl.write.Label(0, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//헤더부분을 처리한다.
						if( groupHeaders == null ){
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map data = (Map) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
								sheet.addCell(label);
								
								if( span > 1 ){
									sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
									cols = cols + (span-1);
								}else{
									sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						rows++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								HashMap map = (HashMap)dataList.get(j);
								if(((HashMap)sheetList.get(z)).get(tabparam).equals(map.get(tabparam))){
									if( includeColumn != null ){
										for( int k = 0; k < includeColumn.length; k++){
											String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
											if(labelValue == null){
												labelValue = "-";
											}
											if(align != null) {
												if("left".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
												} else if("right".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
												} else if("center".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
												}
											} else {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											}
											sheet.addCell(label);
										}
									}else{
										Iterator iter = map.keySet().iterator();
										while(iter.hasNext()){
											String labelValue = setLabel(map.get(iter.next()));
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											sheet.addCell(label);
										}            
									}
									rows++;
								}
							}
						}else{
							String noneResult = "결과가 없습니다.";
							sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, rows, noneResult, bodyFormat);
							sheet.addCell(label);
						}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}
	/**
	 * @method {} makeExcelFileTab : 엑셀다운로드탭으로 정렬 excelParam의 tab이라곳에 들어온 데이터 기준으로  짤라서 보내줌
	 * 
	 * @param {} jsonObject : 엑셀 헤더 객체
	 * @param {} fileName : 파일이름
	 * @param {} result : 엑셀에 출력할 쿼리 결과
	 * @return {mapResult: 파일이름, 파일경로}
	 * @throws IOException 
	 * @throws WriteException 
	 * 
	 */
	public ModelMap makeExcelFileTabCarry(Map excelParams, List result,List SheetName)throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		String tabparam = (String)excelParams.get("tab");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		try {
			if(excelAttrs != null){
				List sheetList = SheetName;
				List sheetParam = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					for(int z=0; z<sheetList.size();z++){
						WritableSheet sheet = null;
						
						//BOLD
						jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
						
						//제목
						jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						titleFont.setPointSize(18);
						titleFont.setBoldStyle(jxl.write.WritableFont.BOLD);
						titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
						
						jxl.write.WritableFont bottomFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						bottomFont.setPointSize(12);
						bottomFont.setBoldStyle(jxl.write.WritableFont.BOLD);

						//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
						jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
						
						jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
						
						jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.GRAY_25, true);
						
						jxl.write.WritableCellFormat bodyHeadFormat = getWritableCellFormat(null, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
								, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
								, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bottomFormat = getWritableCellFormat(bottomFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);

						jxl.write.Label label = null;
						jxl.write.Number number = null;
						jxl.write.Blank blank = null;
						
						//Head부분을 설정한다.
						int cols = 0;
						int rows = 0;
						
						
						List dataList = result;
						
						HashMap sheetMap = (HashMap)sheetParam.get(0);
						HashMap sheetNameMap = (HashMap)sheetList.get(z);
						String sheetName = (String)sheetNameMap.get(tabparam) + (String)sheetNameMap.get("CUSTOMER_ID_NM");
						String titleName = "<"+(String)sheetNameMap.get("LOCATION_CD_NM")+"> <"+(String)sheetNameMap.get("CUSTOMER_ID_NM")+"> 반입 체크리스트";
						String nmInsert = "신청자 : " + (String)sheetNameMap.get("USER_NM") +"   반입신청일 : " +(String)sheetNameMap.get("INSERT_DATE_DT")+ "   실제반입일 :                        설치일:            ";
						String work = "그룹사 담당자 :                        (인)   센터 담당자 :                        (인)";

						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List groupHeaders = (List)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, z+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수

						label = new jxl.write.Label(0, 0, titleName, titleFormat);

						sheet.addCell(label);


						sheet.setRowView(1, 340);
						label = new jxl.write.Label(0, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//헤더부분을 처리한다.
						if( groupHeaders == null ){
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map data = (Map) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
								sheet.addCell(label);
								
								if( span > 1 ){
									sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
									cols = cols + (span-1);
								}else{
									sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						rows++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								HashMap map = (HashMap)dataList.get(j);
								if(((HashMap)sheetList.get(z)).get(tabparam).equals(map.get(tabparam))){
									if( includeColumn != null ){
										for( int k = 0; k < includeColumn.length; k++){
											String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
											if(labelValue == null){
												labelValue = " ";
											}
											if(align != null) {
												if("left".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
												} else if("right".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
												} else if("center".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
												}
											} else {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											}
											sheet.addCell(label);
										}
									}else{
										Iterator iter = map.keySet().iterator();
										while(iter.hasNext()){
											String labelValue = setLabel(map.get(iter.next()));
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											sheet.addCell(label);
										}            
									}
									rows++;
								}
							}

							sheet.mergeCells(0, rows+5, headerName.length -1, rows+6); // 타이틀 부분 셀병합 갯수

							sheet.mergeCells(0, rows+7, headerName.length -1, rows+8); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, rows+5, nmInsert, bottomFormat);
							sheet.addCell(label);

							label = new jxl.write.Label(0, rows+7, work, bottomFormat);
							sheet.addCell(label);
							sheet.setRowView(1, 340);

						}else{
							String noneResult = "결과가 없습니다.";
							sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, rows, noneResult, bodyFormat);
							sheet.addCell(label);
						}
					}
				}
				
				
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}

	/**
	 * 과금 엑셀파일다운로드
     * @param excelParams, result
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@SuppressWarnings("unchecked")
	public ModelMap makeBsExcelFile(Map<String, Object> excelParams, List<Map<String, Object>> result) throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}
		
		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map<String, Object> excelAttrs = (Map<String, Object>)excelParams.get("excelAttrs");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		
		// Rowspan 처리되야할 컬럼
		String strCellMerge = StringUtil.replaceNull(String.valueOf(excelParams.get("cellMerge")), "");
		String cellMergeAppend = StringUtil.replaceNull(String.valueOf(excelParams.get("cellMergeAppend")), "");
		
		try {
			if(excelAttrs != null){
				List<Map<String, Object>> sheetList = (List<Map<String, Object>>) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					int cutRowCount = 50000; // sheet 단위로 잘릴 Row 수...
					
					WritableSheet sheet = null;
					
					//BOLD
					jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					
					//제목
					jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					titleFont.setPointSize(18);
					titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
					
					//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
					jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.GRAY_25, true);
					
					jxl.write.WritableCellFormat bodyHeadFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.Label label = null;
					jxl.write.Number number = null;
					jxl.write.Blank blank = null;
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List<Map<String, Object>> dataList = null;
						
						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List<Map<String, Object>>)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						Map<String, Object> sheetMap = (Map<String, Object>)sheetList.get(i);
						String sheetName = (String)sheetMap.get("sheetName");
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List<Map<String, Object>> groupHeaders = (List<Map<String, Object>>)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, i+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
						label = new jxl.write.Label(0, 0, titleName, titleFormat);
						sheet.addCell(label);
						
						sheet.setRowView(1, 340);
						label = new jxl.write.Label(0, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//헤더부분을 처리한다.
						if( groupHeaders == null ){
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map<String, Object> data = (Map<String, Object>) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
								sheet.addCell(label);
								
								if( span > 1 ){
									sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
									cols = cols + (span-1);
								}else{
									sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						rows++;
						
						String strNowCellMergeData = "";
						String strBefCellMergeData = "";
						int iStartRows = 0;
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								Map<String, Object> map = (Map<String, Object>)dataList.get(j);
								int iCellMergeCols = 0; 
								
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										
										// Rowspan 대상 컬럼에 열 인덱스 구하기
										if(includeColumn[k].equals(strCellMerge)){
											iCellMergeCols = k;
										}
										
										if(labelValue == null){
											labelValue = "-";
										}
										if(align != null) {
											if("left".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
											} else if("right".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
											} else if("center".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											}
										} else {
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										}
										sheet.addCell(label);
									}
									
									// Rowspan 대상 컬럼이 있다면 처리
									if(!"".equals(strCellMerge)){
										if(j == 0){
											strNowCellMergeData = StringUtil.replaceNull(String.valueOf(map.get(strCellMerge)), "");
											strBefCellMergeData = StringUtil.replaceNull(String.valueOf(map.get(strCellMerge)), "");
											iStartRows = rows;
										}else{
											strNowCellMergeData = StringUtil.replaceNull(String.valueOf(map.get(strCellMerge)), "");
											
											// 이전 데이터와 현재 데이터가 다르다면
											if(!strBefCellMergeData.equals(strNowCellMergeData)){
												
												sheet.mergeCells(iCellMergeCols, iStartRows, iCellMergeCols, rows-1);
												
												int cntCellMergeAppend = 0;
												if(!"".equals(cellMergeAppend) && 0 < Integer.parseInt(cellMergeAppend)){
													cntCellMergeAppend = Integer.parseInt(cellMergeAppend);
													for(int m = 1; m <= cntCellMergeAppend; m++){
														sheet.mergeCells(iCellMergeCols+m, iStartRows, iCellMergeCols+m, rows-1);
													}
												}
												strBefCellMergeData = StringUtil.replaceNull(String.valueOf(map.get(strCellMerge)), "");
												iStartRows = rows;
											}
										}
									}									
								}else{
									Iterator<String> iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										sheet.addCell(label);
									}            
								}
								rows++;
							}
							//sheet.mergeCells(0, 3, 0, (dataList.size()-1)+3);
						}else{
							String noneResult = "결과가 없습니다.";
							sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, rows, noneResult, bodyFormat);
							sheet.addCell(label);
						}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e); 
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}
	
	/**
	 * 과금 다중 합성 엑셀파일다운로드
     * @param excelParams, result
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	@SuppressWarnings("unchecked")
	public ModelMap makeMultiCellExcelFile(Map<String, Object> excelParams, List<Map<String, Object>> result) throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}
		
		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map<String, Object> excelAttrs = (Map<String, Object>)excelParams.get("excelAttrs");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		
		// Rowspan 처리되야할 컬럼
		ArrayList < String > strCellMerges = ( ArrayList < String > ) excelParams.get("cellMerge");
		
		try {
			if(excelAttrs != null){
				List<Map<String, Object>> sheetList = (List<Map<String, Object>>) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					WritableSheet sheet = null;
					
					//BOLD
					jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					
					//제목
					jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					titleFont.setPointSize(18);
					titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
					
					//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
					jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.GRAY_25, true);
					
					jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.NumberFormat priceType = new jxl.write.NumberFormat("###,###,###,###,###,###,##0");
					jxl.write.WritableCellFormat price_int = new WritableCellFormat(priceType);
					price_int.setAlignment(jxl.format.Alignment.RIGHT);
					price_int.setBorder(Border.ALL, BorderLineStyle.THIN);
					
					jxl.write.WritableCellFormat format_float1 = new WritableCellFormat(jxl.write.NumberFormats.FLOAT);
					jxl.write.NumberFormat pricetype2 = new jxl.write.NumberFormat("###,###,###,###,###,###,##0.00");
					jxl.write.WritableCellFormat price_float2 = new WritableCellFormat(pricetype2);
					
					
					jxl.write.Label label = null;
					jxl.write.Number number = null;
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List<Map<String, Object>> dataList = null;
						
						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List<Map<String, Object>>)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						Map<String, Object> sheetMap = (Map<String, Object>)sheetList.get(i);
						String sheetName = (String)sheetMap.get("sheetName");
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List<Map<String, Object>> groupHeaders = (List<Map<String, Object>>)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						// 추가
						String excelXtype = "";
						String[] excelXtypeArr = null;
						
						if( sheetMap.get("includeColumns") != null ){
							if ( sheetMap.get("excelXtype") != null ){
								excelXtype = (String)sheetMap.get("excelXtype");
								excelXtypeArr = StringUtil.explode(",", excelXtype);
							}
						}
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, i+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
						label = new jxl.write.Label(0, 0, titleName, titleFormat);
						sheet.addCell(label);
						
						sheet.setRowView(1, 340);
						label = new jxl.write.Label(0, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//헤더부분을 처리한다.
						if( groupHeaders == null ){
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map<String, Object> data = (Map<String, Object>) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
								sheet.addCell(label);
								
								if( span > 1 ){
									sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
									cols = cols + (span-1);
								}else{
									sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						rows++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								HashMap map = (HashMap)dataList.get(j);
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										if(labelValue == null){
											labelValue = "-";
										}
										if(align != null) {
											if ( excelXtypeArr != null )
											{
												if ( "priceInt".equals(excelXtypeArr[k] ))
													number = new jxl.write.Number(cols++,rows,Integer.parseInt(labelValue),price_int);
												else if ( "priceNum".equals(excelXtypeArr[k] ))
													number = new jxl.write.Number(cols++,rows,Long.parseLong(labelValue),bodyFormatRight);
												else
												{
													if("left".equals(align[k])) {
														label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
													} else if("right".equals(align[k])) {
														label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
													} else if("center".equals(align[k])) {
														label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
													}
												}
											}
											else
											{
												if("left".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
												} else if("right".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
												} else if("center".equals(align[k])) {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
												}
											}
										} else {
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										}
										if ( align != null && excelXtypeArr != null && excelXtypeArr[k].indexOf("price") > -1 )
											sheet.addCell(number);
										else
											sheet.addCell(label);
									}
								}else{
									Iterator iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										sheet.addCell(label);
									}            
								}
								rows++;
							}
						}else{
							String noneResult = "결과가 없습니다.";
							sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, rows, noneResult, bodyFormat);
							sheet.addCell(label);
						}
						
						if( dataList != null && dataList.size() > 0 ){
							if ( strCellMerges != null )
							{
								int iCellMergeCols = 0;
								int cnt = 3;
								int start = 3;
								String str = "";
								
								for ( int b = 0 ; b < strCellMerges.size() ; b ++ )
								{
									for ( int c = 0 ; c < includeColumn.length ; c ++ )
									{
										if(includeColumn[c].equals(strCellMerges.get(b))){
											iCellMergeCols = c;
										}
									}
									
									start = 3;
									cnt = 3;
									str = "";
									
									for ( int a = 3 ; a < sheet.getRows() ; a ++ )
									{
										Cell[] xxx = sheet.getRow(a);
										if ( a == 3 )
										{
											str = xxx [ b ].getContents();
										}
										else
										{
											if ( xxx [ b ].getContents().equals ( str ) )
											{
												cnt ++;
											}
											else
											{
												str = xxx [ b ].getContents();
												sheet.mergeCells(iCellMergeCols, start, iCellMergeCols, cnt); // 타이틀 부분 셀병합 갯수
												cnt ++;
												start = cnt;
											}
										}
									}
								}
							}
						}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
			}
		} catch (Exception e) {
			throw new NkiaException(e); 
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}
	
	/**
	 * 품의서 템플릿 엑셀 다운로드
	 * @param fileName
	 * @param realFileName
	 * @param tempUploadPath
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 * @throws JXLException 
	 */
	@SuppressWarnings("unchecked")
	public ModelMap makeExpensiveReportExcelFile(Map<String, Object> excelParams, List<Map<String, Object>> result) throws NkiaException, IOException, WriteException, JXLException{
		ModelMap mapResult = new ModelMap();
		//Excel파일을 저장하기 위한 기본 설정
		String tempFullPath = NkiaApplicationPropertiesMap.getProperty("Globals.ExcelFile.Temp.Dir") + File.separator + "ExpensiveReportTemp.xls";
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String monthVal = excelParams.get("monthVal").toString();
		String preMonthVal = String.valueOf ( ( Integer.parseInt ( excelParams.get("prevMonths").toString() ) ) );
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}
		
		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map<String, Object> excelAttrs = (Map<String, Object>)excelParams.get("excelAttrs");
		Workbook workbookTemp = Workbook.getWorkbook(new File(tempFullPath));
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath), workbookTemp);
		
		try {
			if(excelAttrs != null){
				List<Map<String, Object>> sheetList = (List<Map<String, Object>>) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					WritableSheet sheet = workbook.getSheet(0);
					
					//BOLD
					jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					boldFont.setPointSize(12);
					
					//일반 12
					jxl.write.WritableFont normal12Font = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					normal12Font.setBoldStyle(jxl.write.WritableFont.NO_BOLD);
					normal12Font.setPointSize(12);
					
					//일반
					jxl.write.WritableFont normalFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					normalFont.setBoldStyle(jxl.write.WritableFont.NO_BOLD);
					normalFont.setPointSize(11);
					
					//일반 bold
					jxl.write.WritableFont normalBoldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					normalBoldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					normalBoldFont.setPointSize(11);
					
					//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
					jxl.write.WritableCellFormat boldLabel = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat body12 = getWritableCellFormat(normal12Font, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(normalFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(normalFont, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(normalFont, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyResultFormat = getWritableCellFormat(normalBoldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.AQUA, true);
					
					jxl.write.WritableCellFormat bodyResultFormatLeft = getWritableCellFormat(normalBoldFont, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.AQUA, true);
					
					jxl.write.WritableCellFormat bodyResultFormatRight = getWritableCellFormat(normalBoldFont, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.AQUA, true);
					
					jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.YELLOW, true);
					
					jxl.write.Label label = null;
					jxl.write.Number number = null;
					jxl.write.Blank blank = null;
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List<Map<String, Object>> dataList = null;
						
						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List<Map<String, Object>>)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						Map<String, Object> sheetMap = (Map<String, Object>)sheetList.get(i);
						String[] includeColumn = null;
						String[] align = null;
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						cols = 7;
						rows = 12; // Excel HEAD 삭제
						
						label = new jxl.write.Label(cols, rows, monthVal + "월 전산 호스팅(SK)비용 계열사 과금의(건)", boldLabel);
						sheet.addCell(label);
						
						cols = 0;
						rows = 14;
						label = new jxl.write.Label(cols, rows, "관련 " + preMonthVal + "월 사용분에 대한 과금을 아래와 같이 집행하고자 합니다.", body12);
						sheet.addCell(label);
						
						rows = 19;
						int addCols = 0;
						String headerNames = (String)sheetMap.get("headerNames");
						String[] headerName = StringUtil.explode(",", headerNames);
						
						for( int j = 0; j < headerName.length; j++){
							if ( j == 0 )
							{
								addCols = 12;
								cols = 2;
							}
							else
							{
								cols = cols + addCols;
								addCols = 9;
							}
							label = new jxl.write.Label(cols, rows, headerName[j], headFormat);
							sheet.mergeCells(cols, rows, cols + addCols - 1, rows);
							sheet.addCell(label);
						}
						rows ++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 2;
								////컬럼에 순번넣고싶으면 넣어라.
								HashMap map = (HashMap)dataList.get(j);
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										if(labelValue == null){
											labelValue = "-";
										}
										
										if ( k == 0 )
										{
											addCols = 12;
											cols = 2;
										}
										else
										{
											cols = cols + addCols;
											addCols = 9;
										}
										
										if(align != null) {
											if("left".equals(align[k])) {
												if ( dataList.size() == j + 1 )
													label = new jxl.write.Label(cols, rows, labelValue, bodyResultFormatLeft);
												else
													label = new jxl.write.Label(cols, rows, labelValue, bodyFormatLeft);
											} else if("right".equals(align[k])) {
												if ( dataList.size() == j + 1 )
													label = new jxl.write.Label(cols, rows, labelValue, bodyResultFormatRight);
												else
													label = new jxl.write.Label(cols, rows, labelValue, bodyFormatRight);
											} else if("center".equals(align[k])) {
												if ( dataList.size() == j + 1 )
													label = new jxl.write.Label(cols, rows, labelValue, bodyResultFormat);
												else
													label = new jxl.write.Label(cols, rows, labelValue, bodyFormat);
											}
										} else {
											if ( dataList.size() == j + 1 )
												label = new jxl.write.Label(cols, rows, labelValue, bodyResultFormat);
											else
												label = new jxl.write.Label(cols, rows, labelValue, bodyFormat);
										}
										
										sheet.mergeCells(cols, rows, cols + addCols - 1, rows);
										sheet.addCell(label);
									}
								}else{
									Iterator iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										sheet.addCell(label);
									}            
								}
								rows++;
							}
						}else{
							String noneResult = "결과가 없습니다.";
							//sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(2, rows, noneResult, bodyFormat);
							sheet.addCell(label);
						}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String excelDownload_History( String fileName, String realFileName, String tempUploadPath ) throws NkiaException{
		String excel_file_id = null;
		try {
			excel_file_id = excelFileService.selectMaxHistoryId();
			
			Map history_param = new HashMap();
			history_param.put("excel_file_id", excel_file_id);
			history_param.put("filename", fileName);
			history_param.put("real_filename", realFileName);
			history_param.put("temp_upload_path", tempUploadPath);
			history_param.put("file_extsn", "xls");
			history_param.put("down_user_id", EgovUserDetailsHelper.getSesssionUserId());
			
			// Insert
			excelFileService.insertExcelFile(history_param);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return excel_file_id;
	}
	
	private WritableCellFormat getWritableCellFormat(
			WritableFont cellFont
			, boolean flag_border
			, Alignment align
			, Colour bakColor, boolean flag_verticalAlign) throws WriteException {
		
		jxl.write.WritableCellFormat cellFormat = null;
		
		if(cellFont != null) {
			cellFormat = new WritableCellFormat(cellFont);			
		} else {
			cellFormat = new WritableCellFormat();
		}
		
		if(bakColor != null) {
			cellFormat.setBackground(bakColor);
		}
		
		if(flag_border){
			cellFormat.setBorder(jxl.format.Border.ALL ,jxl.format.BorderLineStyle.THIN );
		}

		cellFormat.setAlignment(align);
		
		if(flag_verticalAlign) {
			cellFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);			
		}
		
		cellFormat.setWrap(true);
		return cellFormat;
	}
	
	/**
	 * 등록용 엑셀템플릿 베이직
	 * 일괄수정용 엑셀파일 
	 * @param excelParams
	 * @param result
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws WriteException 
	 */
	public ModelMap makeExcelFileForBatchEdit(Map excelParams, List result)throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		try {
			if(excelAttrs != null){
				List sheetList = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					int cutRowCount = 50000; // sheet 단위로 잘릴 Row 수...
					
					WritableSheet sheet = null;
					
					/***************** 폰트 정의  ************/
					//BOLD
					jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					
					//제목
					jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					titleFont.setPointSize(18);
					titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
					
					// HIDDEN 헤더 폰트
					jxl.write.WritableFont hiddenFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					hiddenFont.setColour(jxl.format.Colour.WHITE);
					
					/***************** cell 포멧 정의  ************/
					
					/** 헤더 포멧 */
					jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, false
							, jxl.format.Alignment.CENTRE, null, true);
					
					jxl.write.WritableCellFormat infoMsgFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.RIGHT, null, true);
					
					// hidden 헤더 포멧
					jxl.write.WritableCellFormat hiddenHeadFormat = getWritableCellFormat(hiddenFont, false
							, jxl.format.Alignment.CENTRE, null, true);
					
					// 일반 헤더 포멧
					jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.YELLOW2, true);
					
					// 필수값 헤더 포멧
					jxl.write.WritableCellFormat notNullHeadFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.RED, true);
					
					// 코드컬럼 헤더 포멧
					jxl.write.WritableCellFormat codeHeadFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.SKY_BLUE, true);
					
					
					
					/** body 포멧 */
					jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.Label label = null;
					jxl.write.Number number = null;
					jxl.write.Blank blank = null;
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List dataList = null;

						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						HashMap sheetMap = (HashMap)sheetList.get(i);
						String sheetName = (String)sheetMap.get("sheetName");
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List groupHeaders = (List)sheetMap.get("groupHeaders");
						String notNull = (String)sheetMap.get("notNull");
						String tblCol = (String)sheetMap.get("tblCols");
						String htmlTypes = (String)sheetMap.get("htmlTypes");
						String colTypes = (String)sheetMap.get("colTypes");
						String unit = (String)sheetMap.get("unit");
						
						String[] notNullArr = StringUtil.explode(",", notNull);
						String[] tblColArr = StringUtil.explode(",", tblCol);
						String[] htmlTypesArr = StringUtil.explode(",", htmlTypes);
						String[] colTypesArr = StringUtil.explode(",", colTypes);
						String[] unitArr = null;
						String[] includeColumn = null;
						String[] align = null;
						
						if(unit != null) {
							unitArr = unit.split(",");
						}
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, i+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, 1, rows); // 타이틀 부분 셀병합 갯수
						label = new jxl.write.Label(0, 0, titleName, titleFormat);
						sheet.addCell(label);
						
//						label = new jxl.write.Label(2, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
//						sheet.addCell(label);
						
						
						sheet.setRowView(1, 1500); // Row Height
						String InfoMsg = "※ 작성 요령 \n- 빨간색 * 항목은 필수값입니다.\n- 파란색 항목은 코드값으로 작성해주시기 바랍니다. \n- 엑셀 양식은 수정하지 마십시오.";
						sheet.mergeCells(0, 1, 1, 1); // 셀병합 갯수
						label = new jxl.write.Label(0, 1, InfoMsg, infoMsgFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//hidden 헤더부분을 처리한다. (테이블명.컬럼명)
						if( groupHeaders == null ){
							for( int j = 0; j < tblColArr.length; j++){
								label = new jxl.write.Label(cols, rows, tblColArr[j], hiddenHeadFormat); 
								sheet.addCell(label);
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map data = (Map) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
								sheet.addCell(label);
								
								if( span > 1 ){
									sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
									cols = cols + (span-1);
								}else{
									sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						cols = 0;
						rows++;
						
						// 헤더명 영역
						for( int j = 0; j < headerName.length; j++){
							String headerNmDesc = "";
							String unitDesc = "";
							String unitType = unitArr[j];
							String colType = colTypesArr[j];
							
							if(!StringUtils.isEmpty(unitType)
									&& !"null".equals(unitType)) {
								unitDesc = "(" + unitType + ")";
							}
							
							if("NUMBER".equals(colType)){
								headerNmDesc = " (숫자형)";
							}else if("DATE".equals(colType)){
								headerNmDesc = " (yyyy-mm-dd)";
							}
							
							String headerNm = headerName[j] + unitDesc + headerNmDesc;
							label = new jxl.write.Label(cols, rows, headerNm, headFormat);
							// notNull서식
							if("Y".equals(notNullArr[j])){
								headerNm = "* ".concat(headerNm);
								label = new jxl.write.Label(cols, rows, headerNm, notNullHeadFormat);
							}
							//코드값 서식
							if("SELECT".equals(htmlTypesArr[j]) || htmlTypesArr[j].matches("POPUP.*")) {
								label = new jxl.write.Label(cols, rows, headerNm, codeHeadFormat); 
							}
							
							sheet.addCell(label);
							if(headerWidth == null){//컬럼헤더 Width
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}else{
								if( j == 0 ){
									sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						
						// 데이터 영역
						cols = 0;
						rows++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								HashMap map = (HashMap)dataList.get(j);
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										if(labelValue == null){
											labelValue = "";
										}else {
											// 데이트타입 형식
											String htmlType = htmlTypesArr[k];
											if("DATE".equals(htmlType)){
//												System.out.println(labelValue);
											}
										}
										if(align != null) {
											if("left".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
											} else if("right".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
											} else if("center".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											}
										} else {
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										}
										sheet.addCell(label);
									}
								}else{
									Iterator iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										sheet.addCell(label);
									}            
								}
								rows++;
							}
						}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}

	/**
	 * 일반테이블 일괄등록용 엑셀파일
	 * @param excelParams
	 * @param result
	 * @return
	 * @throws IOException 
	 * @throws WriteException 
	 * @throws NkiaException
	 */
	public ModelMap makeExcelFileForBatchExcel(Map excelParams, List result) throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		try {
			if(excelAttrs != null){
				List sheetList = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					int cutRowCount = 50000; // sheet 단위로 잘릴 Row 수...
					
					WritableSheet sheet = null;
					
					/***************** 폰트 정의  ************/
					//BOLD
					jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					
					//제목
					jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					titleFont.setPointSize(18);
					titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
					
					// HIDDEN 헤더 폰트
					jxl.write.WritableFont hiddenFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					hiddenFont.setColour(jxl.format.Colour.WHITE);
					
					/***************** cell 포멧 정의  ************/
					
					/** 헤더 포멧 */
					jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, false
							, jxl.format.Alignment.CENTRE, null, true);
					
					jxl.write.WritableCellFormat infoMsgFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.RIGHT, null, true);
					
					// hidden 헤더 포멧
					jxl.write.WritableCellFormat hiddenHeadFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.YELLOW2, true);
					
					// 일반 헤더 포멧
					jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.YELLOW2, true);
					
					// 필수값 헤더 포멧
					jxl.write.WritableCellFormat notNullHeadFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.RED, true);
					
					// 코드컬럼 헤더 포멧
					jxl.write.WritableCellFormat codeHeadFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.SKY_BLUE, true);
					
					
					
					/** body 포멧 */
					jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, true);
					
					jxl.write.Label label = null;
					jxl.write.Number number = null;
					jxl.write.Blank blank = null;
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List dataList = null;

						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						HashMap sheetMap = (HashMap)sheetList.get(i);
						String sheetName = (String)sheetMap.get("sheetName");
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, i+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setColumnView(2, 30);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, 1, rows); // 타이틀 부분 셀병합 갯수
						label = new jxl.write.Label(0, 0, titleName, titleFormat);
						sheet.addCell(label);
						
						label = new jxl.write.Label(2, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
						sheet.addCell(label);
						
						
						sheet.setRowView(1, 1500); // Row Height
						String InfoMsg = "※ 작성 요령 \n- 3번 행은 테이블.컬럼ID 를 입력하여 주십시오.(필수) \n- 4번 행은 속성명을 입력하여 주십시오.(선택) \n- 5번 행부터 등록할 데이터를 입력하여 주십시오.";
						sheet.mergeCells(0, 1, 1, 1); // 셀병합 갯수
						label = new jxl.write.Label(0, 1, InfoMsg, infoMsgFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//hidden 헤더부분을 처리한다. (테이블명.컬럼명)
						for( int j = 0; j < includeColumn.length; j++){
							label = new jxl.write.Label(cols, rows, includeColumn[j], hiddenHeadFormat); 
							sheet.addCell(label);
							if(headerWidth == null){//컬럼헤더 Width
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}else{
								if( j == 0 ){
									sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						cols = 0;
						rows++;
						
						// 헤더명 영역
						for( int j = 0; j < headerName.length; j++){
							String headerNm = headerName[j];
							label = new jxl.write.Label(cols, rows, headerNm, headFormat);
							sheet.addCell(label);
							
							if(headerWidth == null){//컬럼헤더 Width
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}else{
								if( j == 0 ){
									sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						
						// 데이터 영역
						cols = 0;
						rows++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								HashMap map = (HashMap)dataList.get(j);
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										if(labelValue == null){
											labelValue = "";
										}
										if(align != null) {
											if("left".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
											} else if("right".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
											} else if("center".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											}
										} else {
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										}
										sheet.addCell(label);
									}
								}else{
									Iterator iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										sheet.addCell(label);
									}            
								}
								rows++;
							}
						}
					}
				}
				workbook.write();
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e); 
		} finally {
			if (workbook!=null) workbook.close();
		}
		
		return mapResult;
	}
	
	
	private String setLabel(Object objData){
		String labelValue = null;
		if( objData instanceof java.lang.String ){
			labelValue = (String)objData;
		}else if( objData instanceof BigDecimal ){
			BigDecimal getBigDecimalData = (BigDecimal)objData;
			labelValue = getBigDecimalData.toString();
		}else if( objData instanceof Date ){
			labelValue = ((Date)objData).toString();
		}else if( objData instanceof Timestamp ){
			labelValue = ((Timestamp)objData).toString();
		}else{
			labelValue = (String)objData;
		}
		return labelValue;
	}
	
	/**
	 * @method {} makeExcelFileTab : 엑셀다운로드탭으로 정렬 excelParam의 tab이라곳에 들어온 데이터 기준으로  짤라서 보내줌
	 * 
	 * @param {} jsonObject : 엑셀 헤더 객체
	 * @param {} fileName : 파일이름
	 * @param {} result : 엑셀에 출력할 쿼리 결과
	 * @return {mapResult: 파일이름, 파일경로}
	 * @throws IOException 
	 * @throws WriteException 
	 * 
	 */
	public ModelMap makeExcelFileCarryOut(Map excelParams, List result,List SheetName)throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		String tabparam = (String)excelParams.get("tab");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		try {
			if(excelAttrs != null){
				List sheetList = SheetName;
				List sheetParam = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					for(int z=0; z<sheetList.size();z++){
						WritableSheet sheet = null;
						
						//BOLD
						jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
						
						//제목
						jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						titleFont.setPointSize(18);
						titleFont.setBoldStyle(jxl.write.WritableFont.BOLD);
						titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
						
						jxl.write.WritableFont bottomFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
						bottomFont.setPointSize(12);
						bottomFont.setBoldStyle(jxl.write.WritableFont.BOLD);

						//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
						jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
						
						jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
						
						jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.GRAY_25, true);
						
						jxl.write.WritableCellFormat bodyHeadFormat = getWritableCellFormat(null, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
								, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
								, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
								, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
						
						jxl.write.WritableCellFormat bottomFormat = getWritableCellFormat(bottomFont, true
								, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);

						jxl.write.Label label = null;
						jxl.write.Number number = null;
						jxl.write.Blank blank = null;
						
						//Head부분을 설정한다.
						int cols = 0;
						int rows = 0;
						
						
						List dataList = result;
						
						HashMap sheetMap = (HashMap)sheetParam.get(0);
						HashMap sheetNameMap = (HashMap)sheetList.get(z);
						String sheetName = (String)sheetNameMap.get(tabparam) + (String)sheetNameMap.get("CUSTOMER_ID_NM") ;
						String titleName = "<"+(String)sheetNameMap.get("CENTER_CD_NM")+"> <"+(String)sheetNameMap.get("CUSTOMER_ID_NM")+"> 반출/폐기자산 리스트";
						String nmInsert = "신청자 : " + (String)sheetNameMap.get("USER_NM") +"   반출신청일 : " +(String)sheetNameMap.get("UPD_DT")+ "   실제반출일 :            ";
						String work = "그룹사 담당자 :            (인)   센터 담당자 :            (인)";

						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List groupHeaders = (List)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
							
							cols = 0;
							rows = 0;		
							
							sheet = workbook.createSheet(sheetName, z+1);
							sheet.setColumnView(0, 50);
							sheet.setColumnView(1, 20);
							sheet.setRowView(0, 600); // Row Height
							sheet.mergeCells(cols, 0, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, 0, titleName, titleFormat);
							sheet.addCell(label);
							sheet.mergeCells(cols, dataList.size()+5, headerName.length -1, dataList.size()+5); // 타이틀 부분 셀병합 갯수
							sheet.mergeCells(cols, dataList.size()+6, headerName.length -1, dataList.size()+6); // 타이틀 부분 셀병합 갯수
	
							label = new jxl.write.Label(0, dataList.size()+5, nmInsert, bottomFormat);
							sheet.addCell(label);
	
							label = new jxl.write.Label(0, dataList.size()+6, work, bottomFormat);
							sheet.addCell(label);
	
							sheet.setRowView(1, 340);
							label = new jxl.write.Label(0, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
							sheet.addCell(label);
							sheet.setRowView(2, 500);
							rows = 2; // Excel HEAD 표시 위치
							
							cols = 0;
							
							//헤더부분을 처리한다.
							if( groupHeaders == null ){
								for( int j = 0; j < headerName.length; j++){
									label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
									sheet.addCell(label);
									if(headerWidth == null){//컬럼헤더 Width
										if( j == 0 ){
											sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
										}else{
											sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
										}
									}else{
										if( j == 0 ){
											sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
										}else{
											sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
										}
									}
								}
							}else{
								for( int j = 0; j < groupHeaders.size(); j++){
									Map data = (Map) groupHeaders.get(j);
									int span = Integer.parseInt((String) data.get("span"));
									
									label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
									sheet.addCell(label);
									
									if( span > 1 ){
										sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
										cols = cols + (span-1);
									}else{
										sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
									}
									
									cols++;
								}
								
								cols = 0;
								rows++;
								
								for( int j = 0; j < headerName.length; j++){
									label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
									sheet.addCell(label);
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
							
							rows++;
							
							if( dataList != null && dataList.size() > 0 ){
								for( int j = 0; j < dataList.size(); j++){
									cols = 0;
									////컬럼에 순번넣고싶으면 넣어라.
									//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
									//sheet.addCell(label);
									HashMap map = (HashMap)dataList.get(j);
									if(((HashMap)sheetList.get(z)).get(tabparam).equals(map.get(tabparam))){
										if( includeColumn != null ){
											for( int k = 0; k < includeColumn.length; k++){
												String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
												if(labelValue == null){
													labelValue = " ";
												}
												if(align != null) {
													if("left".equals(align[k])) {
														label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
													} else if("right".equals(align[k])) {
														label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
													} else if("center".equals(align[k])) {
														label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
													}
												} else {
													label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
												}
												sheet.addCell(label);
											}
										}else{
											Iterator iter = map.keySet().iterator();
											while(iter.hasNext()){
												String labelValue = setLabel(map.get(iter.next()));
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
												sheet.addCell(label);
											}            
										}
										rows++;
									}
								}
								
							}else{
								String noneResult = "결과가 없습니다.";
								sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
								label = new jxl.write.Label(0, rows, noneResult, bodyFormat);
								sheet.addCell(label);
							}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}

	
}
