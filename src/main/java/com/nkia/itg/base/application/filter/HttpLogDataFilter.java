package com.nkia.itg.base.application.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.MDC;

import com.nkia.itg.base.application.util.web.AgentUtils;
import com.nkia.itg.base.application.util.web.MDCUtil;
import com.nkia.itg.base.application.util.web.RequestWrapper;
import com.nkia.itg.base.application.util.web.WebUtil;

public class HttpLogDataFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		RequestWrapper requestWrapper = RequestWrapper.of(request);      
		String uri = requestWrapper.getRequestUri();
		
		MDCUtil.set(MDCUtil.SERVER_NAME_MDC, requestWrapper.getServerName());
		MDCUtil.set(MDCUtil.REQUEST_URI_MDC, uri);
		MDCUtil.setJsonValue(MDCUtil.HEADER_MAP_MDC, requestWrapper.headerMap());
		MDCUtil.set(MDCUtil.PARAMETER_JSON_MDC, requestWrapper.parameterJson());
		MDCUtil.setJsonValue(MDCUtil.USER_INFO_MDC, WebUtil.getCurrentUser());
		MDCUtil.setJsonValue(MDCUtil.AGENT_DETAIL_MDC, AgentUtils.getAgentDetail((HttpServletRequest) request));
		MDCUtil.set(MDCUtil.REQUEST_URI_MDC, requestWrapper.getRequestUri());

		try {
			chain.doFilter(request, response);
		} finally {
			MDC.clear();
		}
		
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		// TODO Auto-generated method stub
		
	}
}
