package com.nkia.itg.base.application.util.common;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.nkia.itg.base.application.exception.NkiaException;

/**
 * 
 * @version 1.0 2013. 2. 8.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 *  Date Utilities
 * </pre>
 *
 */
public class DateUtil {

	/**
	 * 
	 * YYYYMMDD형식의 오늘일자 반환
	 * 
	 * @return
	 */
	public static String getDate() {
		return getDate("yyyymmdd", "");
	}

	/**
	 * 
	 * 오늘일자를 구분자에 따라 Return (ex:)YYYY-MM-DD
	 * 
	 * @param gubn
	 * @return
	 */
	public static String getDate(String gubn) {
		return getDate("yyyymmdd", gubn);
	}

	/**
	 * 
	 * 오늘일자 Format과 구분자에 따라 Return
	 * 
	 * @param format
	 * @param gubn
	 * @return
	 */
	public static String getDate(String format, String gubn) {

		if(format==null) return null;
		if(format.equals("")) return "";

		Calendar cal = Calendar.getInstance();
		int yy = cal.get(Calendar.YEAR);
		int mo = cal.get(Calendar.MONTH)+1;
		int dd = cal.get(Calendar.DAY_OF_MONTH);

		String yyy = null;
		String mmo = null;
		String ddd = null;

		yyy = "" + yy;
		if(mo < 10) mmo = "0" + mo;
		else mmo = "" + mo;
		if(dd < 10) ddd = "0" + dd;
		else ddd = "" + dd;

		String addDate="";
		if(format.equals("yyyymmdd"))
			addDate = "" + yyy + gubn +mmo + gubn + ddd;
		else if(format.equals("yyyymm"))
			addDate = "" + yyy + gubn +mmo;
		else if(format.equals("mmdd"))
			addDate = "" + mmo + gubn + ddd;
		else if(format.equals("yyyy"))
			addDate = "" + yyy;
		else if(format.equals("mm"))
			addDate = "" + mmo;
		else if(format.equals("dd"))
			addDate = "" + ddd;

		return addDate;
	}
	
	//@@김건범-ITG-0002
	//START
	/**
	 * yyyymmddhhmmss 로 출력
	 * @return
	 */
	public static String getDateFormat() {
		return getDateFormat("yyyyMMddhhmmss");
	}
	
	/**
	 * 현재 날짜 시간 구하기  format 지정 (yyyyMMddhhmmss , yyyy-MM-dd,등등)
	 * @param format
	 * @return
	 */
	public static String getDateFormat(String format) {
		long date = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(new Date(date));
	}
	//END
	//@@김건범-ITG-0002
	
	public static String changeFormt(String dateInfo, String fromFormat, String toFormat) throws NkiaException, ParseException {
		String returnDate = "";
		if(dateInfo != null) {
			SimpleDateFormat fFormat = new SimpleDateFormat(fromFormat);
			SimpleDateFormat tFormat = new SimpleDateFormat(toFormat);
			
			Date chageDate = fFormat.parse(dateInfo);
			returnDate = tFormat.format(chageDate);			
		}
		
		return returnDate;
	}
	
    public static String getTimeStamp() throws NkiaException {
		String rtnStr = null;
		// 문자열로 변환하기 위한 패턴 설정(년도-월-일 시:분:초:초(자정이후 초))
		String pattern = "yyyyMMddhhmmssSSS";

		SimpleDateFormat sdfCurrent = new SimpleDateFormat(pattern, Locale.KOREA);
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		rtnStr = sdfCurrent.format(ts.getTime());

		return rtnStr;
    }
	
    /**
	 * 해당하는 년월의 최대 일수 구하기..
	 *
	 * @param String year(ex: 2006)
	 * @param String month(ex: 12)
	 * @return int(ex:31)
     * @throws NkiaException 
	 */
	public static int getMaxDayOfMonth(String year, String month) throws NkiaException {
		int day = 0;
		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(year), Integer.parseInt(month) - 1,
				Integer.parseInt("1"));

		day = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return day;
	}
	
	/**
	 *  원하는 달의 지정한 개월 수 만큼의 이전달을 출력
	 *  
	 * @param num(int: 구하고자 하는 이전 달의 수)
	 * @param work_ym(String: 기준이 되는 월) 
	 * @return String yyyymm
	 */
	public static String getPreviousMonth(String month, int num) {

		Calendar calendar = Calendar.getInstance();

		calendar.set(Integer.parseInt(month.substring(0, 4)), Integer
				.parseInt(month.substring(4)) - 1, 1);

		calendar.add(Calendar.MONTH, -(num));
		String year = String.valueOf(calendar.get(Calendar.YEAR));
		String _month = String.valueOf(calendar.get(Calendar.MONTH) + 1);

		if (_month.length() == 1)
			_month = "0" + _month;

		return year + _month;
	}
	
	/**
	 *  특정 날을 기준으로 지정한 일 수 만큼의 이전 일을 출력
	 *  지정한 날짜는 포함 안됨..(ex: 지정 일이 20060321 일때, 3일 이전은 20060318 이 됨)
	 * @param date(int: 구하고자 하는 이전 일의 수)
	 * @return String yyyymmdd
	 */
	public static String getPreviosDate(String date, int num) throws NkiaException {
		String strYear = "";
		String strMonth = "";
		String strDay = "";

		try {

			int year = Integer.parseInt(date.substring(0, 4));
			int month = Integer.parseInt(date.substring(4, 6));
			int day = Integer.parseInt(date.substring(6, 8));
			Calendar cal = Calendar.getInstance();
			//Calendar cal = new GregorianCalendar();
			cal.set(year, (month - 1), day, 0, 0, 0);
			cal.add(Calendar.DATE, -(num));

			strYear = String.valueOf(cal.get(Calendar.YEAR));
			strMonth = String.valueOf(cal.get(Calendar.MONTH) + 1);
			strDay = String.valueOf(cal.get(Calendar.DATE));

			if (strMonth.length() == 1)
				strMonth = "0" + strMonth;
			if (strDay.length() == 1)
				strDay = "0" + strDay;
		} catch (Exception e) {
			throw new NkiaException(e);
		}

		return strYear + strMonth + strDay;
	}
	
	/**
	 *  날짜형태로  리턴
	 * @param date: 변경하고자 하는 날짜(ex: yyyy-mm-dd)
	 * @return String yyyymmdd
	 */
	public static String chgWithoutFormat(String date) throws NkiaException {

		String year = null;
		String month = null;
		String day = null;
		String result = "";

		if (date == null) {
			return result;
		}

		year = date.substring(0, 4);
		month = date.substring(5, 7);
		day = date.substring(8);

		result = year.concat(month);
		result = result.concat(day);

		return result;
	}
}