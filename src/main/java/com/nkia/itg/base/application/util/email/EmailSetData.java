package com.nkia.itg.base.application.util.email;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.system.email.service.EmailHistoryService;

public class EmailSetData {
	
	public Map mailInfoMap = null;
	private EmailVO emailVO = new EmailVO();
	
	public EmailSetData(Map mailInfoMap) {
		this.mailInfoMap = mailInfoMap;
	}
	
	public void sendMail() throws NkiaException, JsonParseException, JsonMappingException, IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, MessagingException {
		try {
			//setter 호출 start
			if(mailInfoMap.get("KEY").equals("NOTICE")){
				//notice SETTER.CALL(emailVO)
				emailVO = EmailSetBoardData.setData(mailInfoMap);
			} else if (mailInfoMap.get("KEY").equals("PROCESS")) {
				//PROCESS SETTER.CALL(emailVO)
				if(mailInfoMap.get("TEMPLATE_ID").equals("STATIS_NOTI_MAIL")){ //만족도메일의경우 별도의페이지
					emailVO = EmailSetHappyCallData.setData(mailInfoMap);
				}else if(mailInfoMap.get("TEMPLATE_ID").equals("APPROVAL_MAIL")) {
					emailVO = EmailSetApprovalData.setData(mailInfoMap);
				}else if(mailInfoMap.get("TEMPLATE_ID").equals("APPROVAL_ONLY_MAIL")) {
					emailVO = EmailSetApprovalOnlyData.setData(mailInfoMap);
				}else{
					emailVO = EmailSetProcessData.setData(mailInfoMap);
				}
			}
			else if (mailInfoMap.get("KEY").equals("PWRESET")) {
				//PROCESS SETTER.CALL(emailVO)
				emailVO = EmailSetPwResetData.setData(mailInfoMap);
			}
			else if (mailInfoMap.get("KEY").equals("REPORT")) {
				//PROCESS SETTER.CALL(emailVO)
				emailVO = EmailSetReportData.setData(mailInfoMap);
			}
			else if (mailInfoMap.get("KEY").equals("INCREASE_END_MAIL")) {
				emailVO = EmailSetIncreaseEndData.setData(mailInfoMap);
			}
			//setter 호출 END
			
			if (emailVO.isConfig_flag()) {
				//메일 발송 모듈을 가져온다.
 				AbstractEmailSendJob aMailSender = EmailSendJobContainer.getEmailSendJob(emailVO.getEmail_job_type());
				aMailSender.setMailVO(emailVO);
				Thread t = new Thread(aMailSender);
				t.start();
			} else {
                // 설정정보가 없거나 메일 템플릿 정보가 없는 경우의 처리
				insertSendMailHistory("FAIL", emailVO, emailVO.getErr_msg());
			}
		} catch (NkiaException e) {//예외처리 주소를 입력하지 않을 경우
			insertSendMailHistory("FAIL", emailVO, e.toString());
			new NkiaException(e);
		}
	}
	
	// 메일발송 이력 관리
	public void insertSendMailHistory(String status, EmailVO emailVO, String errMsg) throws NkiaException {
		emailVO.setStatus(status);
		emailVO.setErr_msg(errMsg);
		
		EmailHistoryService emailHistoryService = (EmailHistoryService) NkiaApplicationContext.getCtx().getBean("emailHistoryService");
		emailHistoryService.insertMailSendHistory(emailVO);
	}
}
