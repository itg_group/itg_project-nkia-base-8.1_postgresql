/*
 * @(#)RsaPassWordChange.java              2013. 6. 27.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;

@Component("cryptionChange")
public class CryptionChange {

	//public String CRYPTO_SEED_PASSWORD = EgovProperties.getProperty("cryption.privateKey"); 
	
	// 패스워드 암호화 하기 
	public String encryptPassWord(String value) throws NkiaException{
		
		String passWord = ACipher.encrypt(ACipher.CIPHER_SEED, value);
		/*
		AsCryptionBasic rsa = new AsCryptionBasic();
		String p a s s / W o r d = rsa.encrypt(CRYPTO_SEED_PASSWORD, value);
		*/
		return passWord;
	}
	
	// 패스워드 복호화 하기 
	public String decryptPassWord(String value) throws NkiaException{
		
		String passWord = null;
		// 사용자 정보를 연계한 경우 초기 비밀번호로(암호화 되지 않은) 로그인 할 수 있도록 허용 한다.
		String basicPassWord = NkiaApplicationPropertiesMap.getProperty("Globals.Security.BasicPassword");
		if(basicPassWord != null){
			if(basicPassWord.equals(value)){
				passWord = basicPassWord;
			}else{
				
				passWord = ACipher.decrypt(ACipher.CIPHER_SEED, value);
				/*
				AsCryptionBasic rsa = new AsCryptionBasic();
				p a s s / W o r d = rsa.decrypt(CRYPTO_SEED_PASSWORD, value);
				*/
			}
		}
				
		return passWord;
	}
	
/*	public static void main(String[] aa) throws NkiaException {
		CryptionChange a1 = new CryptionChange();
		System.out.println(a1.decryptPassWord("3y8mzosMIo13kf/vny57qQ=="));
	}*/
	
}
