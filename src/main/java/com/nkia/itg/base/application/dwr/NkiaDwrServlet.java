package com.nkia.itg.base.application.dwr;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.directwebremoting.WebContextFactory.WebContextBuilder;
import org.directwebremoting.extend.ScriptSessionManager;
import org.directwebremoting.servlet.DwrServlet;

public class NkiaDwrServlet extends DwrServlet {

	private WebContextBuilder webContextBuilder = null;
	
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		webContextBuilder = super.getContainer().getBean(WebContextBuilder.class);
		ScriptSessionManager manager = super.getContainer().getBean(ScriptSessionManager.class);
		//DWR 자체 에러로 인해 리스너를 두개 등록한다. for문에서 리스너가 두개 이상등록 되어야 실행됨
		manager.addScriptSessionListener(new NkiaScriptSessrionListener());
		manager.addScriptSessionListener(new NkiaScriptSessrionListener());
	}
}
