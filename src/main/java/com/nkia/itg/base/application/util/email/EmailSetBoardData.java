package com.nkia.itg.base.application.util.email;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationEmailMap;
import com.nkia.itg.base.application.config.NkiaApplicationEmailTemplateMap;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.system.board.service.NoticeBoardService;
import com.nkia.itg.system.email.service.EmailHistoryService;
import com.nkia.itg.system.email.service.EmailInfoService;
import com.nkia.itg.system.user.service.UserService;

public class EmailSetBoardData {
	
	
	public static EmailVO setData(Map mailInfoMap) throws NkiaException, JsonParseException, JsonMappingException, IOException {
		
		String reqType = (String) mailInfoMap.get("KEY"); //메일 요청 유형
		String templateId = (String) mailInfoMap.get("TEMPLATE_ID"); //메일 템플릿
		String boardId = (String) mailInfoMap.get("BOARD_ID");
		Map dataMap = new HashMap(); //메일 발송을 위한 데이터
		EmailVO emailVO = new EmailVO();
		String err_msg = ""; //에러메시지
		
		boolean setFlag = false;
	
		//SYS_MAIL_SENDER_INFO 테이블
		// MAIL_ID 구분으로 하나의 메일 서버인 경우는 변수로 기본 설정
		// 다중일 경우에는 조건에 추가 수정
		String emailId = EmailEnum.EMAIL_ID_1.getString();
		String emailConfigData = NkiaApplicationEmailMap.getMailInfo(emailId);
		String emailTemplateId = emailId + "==" + templateId;
		String emailTemplateData = NkiaApplicationEmailTemplateMap.getMailTemplateInfo(emailTemplateId);
		
		if (emailConfigData != null && emailTemplateData != null) {
			setFlag = true;
		} else {
			setFlag = false;
			err_msg = "NO DATA - CONFIG, TEMPLATE";
			
			if (emailConfigData == null) {
				err_msg = "NO DATA - CONFIG";
			}
			
			if (emailTemplateData == null) {
				err_msg = "NO DATA - TEMPLATE";
			}
		}
		emailVO.setConfig_flag(setFlag);
		emailVO.setErr_msg(err_msg);
		
		if (setFlag) {
			//메일발송정보
			ObjectMapper mapper = new ObjectMapper();
	        Map<String, Object> configMap = new HashMap<String, Object>();
	        configMap = mapper.readValue(emailConfigData, new TypeReference<Map<String, Object>>() {});
			
			emailVO.setEmail_job_type((String)configMap.get("EMAIL_JOB_TYPE"));
			emailVO.setHost((String)configMap.get("EMAIL_HOST"));
			int port = Integer.parseInt(String.valueOf(configMap.get("EMAIL_PORT")));
			emailVO.setPort(port);
			emailVO.setEmail_admin_id((String)configMap.get("EMAIL_ADMIN_ID"));
			emailVO.setEmail_admin_pw((String)configMap.get("EMAIL_ADMIN_PW"));
			emailVO.setEmail_admin_addr((String)configMap.get("EMAIL_ADMIN_ADDR"));
			emailVO.setEmail_multi_send_yn((String)configMap.get("EMAIL_MULTI_SEND_YN"));
			
			//메일 템플릿 테이블 내의 발송 정보
	        Map<String, Object> templateMap = new HashMap<String, Object>();
	        templateMap = mapper.readValue(emailTemplateData, new TypeReference<Map<String, Object>>() {});
			
			//From (발송자) 정보
			emailVO.setFrom((String)templateMap.get("EMAIL_FROM"));
			
			//메일에 들어갈 링크 URL을 넣는다.
			String systemUrl = (String)templateMap.get("SITE_URL");
			dataMap.put("LINK_URL", systemUrl);
			
			NoticeBoardService noticeboardService = (NoticeBoardService) NkiaApplicationContext.getCtx().getBean("noticeboardService");
			ModelMap paramMap = new ModelMap();
			paramMap.put("no_id", boardId);
			paramMap.put("flag", false);
			dataMap = noticeboardService.selectBoardInfo(paramMap);
			
			//dataMap에 담김 제목 정보
			String title = (String) dataMap.get("TITLE");
			dataMap.put("TITLE", title);
			
			//메일내용
			String content = "";
			// 메일 템플릿을 사용여부 확인하지 않으면 메일 내용을 직접 담는다고 판단
			 if (dataMap.get("CONTENT") != null && !"".equals((String) dataMap.get("CONTENT"))) {
			//StringUtil.changeLineAlignmentForHtml - <br> 처리
			content = StringUtil.changeLineAlignmentForHtml((String) dataMap.get("CONTENT"));
			} else {
			content = getContent(templateId, dataMap);
			}
			
			//수신자
			UserService userService = (UserService)NkiaApplicationContext.getCtx().getBean("userService");
			paramMap.put("user_id", "lbass");
			List userList = userService.searchUserListForMail(paramMap);
			
			//발송 데이터 정보
			emailVO.setEmail_req_type(reqType);
			emailVO.setEmail_id(emailId);
			emailVO.setSubject(title);
			emailVO.setContent(content);
			emailVO.setTo_user_list(userList);
			emailVO.setTemplate_id(templateId);
			emailVO.setUrl(systemUrl);
		}
		
		return emailVO;
	}
	
	public static String getContent(String templateName, Map dataInfo) throws NkiaException, IOException {
		EmailTemplateGenerator etg = new EmailTemplateGenerator();
		//String result = etg.generateContent(templateName, masterInfo);
		String result = etg.customGenerateContent(templateName, dataInfo);
		return result;
	}
	
}
