/*
 * @(#)NkiaSessionListener.java              2014. 8. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.listener;

import java.io.Serializable;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.certification.service.CertificationService;
import com.nkia.itg.certification.vo.UserVO;

/**
 * 세션 사용자 총 접속 수 및 로그아웃에 대한 업데이트
 * @version 1.0 2015. 6. 8.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * TODO
 * </pre>
 *
 */
public class NkiaSessionListener implements HttpSessionListener, Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(NkiaSessionListener.class);
	
	private static int totalActiveSessions;
	
	public static int getTotalActiveSession() {
		return totalActiveSessions;
	}

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		totalActiveSessions++;
		logger.info("sessionCreated - add one session into counter");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
		// 로그인된 사용자ID 를 가지고 접속정보 테이블에 로그아웃 정보를 입력한다
		try {
			UserVO user = (UserVO)httpSessionEvent.getSession().getAttribute("userVO");
			if( user != null ){
				String userID = user.getUser_id();
				CertificationService certificationService = (CertificationService) NkiaApplicationContext.getCtx().getBean("certificationService");
				certificationService.updateLoginInfo(userID);
			}
			totalActiveSessions--;
		} catch (BeansException e) {
			logger.info("exception :" + e.getMessage());
		} catch (Exception e) {
			logger.info("exception :" + e.getMessage());
		}
	}

}
