package com.nkia.itg.base.application.util.web;

import eu.bitwalker.useragentutils.*;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.exception.NkiaException;

import java.util.HashMap;
import java.util.Map;

public class AgentUtils {

	private static Logger logger = LoggerFactory.getLogger(AgentUtils.class);
	
	public static String getUserAgentString(HttpServletRequest request) {
		return request.getHeader("User-Agent");
	}

	public static String getUserAgentString() {
		return getUserAgentString(WebUtil.getCurrentRequest());
	}

	public static UserAgent getUserAgent(HttpServletRequest request) {
		try {
			String userAgentString = getUserAgentString(request);
			return UserAgent.parseUserAgentString(userAgentString);
		} catch (Exception e) {
			new NkiaException(e);
		}
		return null;
	}

	public static UserAgent getUserAgent() {
		return getUserAgent(WebUtil.getCurrentRequest());
	}

	public static OperatingSystem getUserOs(HttpServletRequest request) {
		UserAgent userAgent = getUserAgent(request);
		return userAgent == null ? OperatingSystem.UNKNOWN : userAgent.getOperatingSystem();
	}

	public static OperatingSystem getUserOs() {
		return getUserOs(WebUtil.getCurrentRequest());
	}

	public static Browser getBrowser(HttpServletRequest request) {
		UserAgent userAgent = getUserAgent(request);
		return userAgent == null ? Browser.UNKNOWN : userAgent.getBrowser();
	}

	public static Browser getBrowser() {
		return getBrowser(WebUtil.getCurrentRequest());
	}

	public static Version getBrowserVersion(HttpServletRequest request) {
		UserAgent userAgent = getUserAgent(request);
		return userAgent == null ? new Version("0", "0", "0") : userAgent.getBrowserVersion();
	}

	public static BrowserType getBrowserType(HttpServletRequest request) {
		Browser browser = getBrowser(request);
		return browser == null ? BrowserType.UNKNOWN : browser.getBrowserType();
	}

	public static BrowserType getBrowserType() {
		return getBrowserType(WebUtil.getCurrentRequest());
	}

	public static RenderingEngine getRenderingEngine(HttpServletRequest request) {
		Browser browser = getBrowser(request);
		return browser == null ? RenderingEngine.OTHER : browser.getRenderingEngine();
	}

	public static RenderingEngine getRenderingEngine() {
		return getRenderingEngine(WebUtil.getCurrentRequest());
	}

	public static Version getBrowserVersion() {
		return getBrowserVersion(WebUtil.getCurrentRequest());
	}

	public static DeviceType getDeviceType(HttpServletRequest request) {
		OperatingSystem operatingSystem = getUserOs(request);
		return operatingSystem == null ? DeviceType.UNKNOWN : operatingSystem.getDeviceType();
	}

	public static DeviceType getDeviceType() {
		return getDeviceType(WebUtil.getCurrentRequest());
	}

	public static Manufacturer getManufacturer(HttpServletRequest request) {
		OperatingSystem operatingSystem = getUserOs(request);
		return operatingSystem == null ? Manufacturer.OTHER : operatingSystem.getManufacturer();
	}

	public static Manufacturer getManufacturer() {
		return getManufacturer(WebUtil.getCurrentRequest());
	}

	public static Map<String, String> getAgentDetail(HttpServletRequest request) {
		Map<String, String> agentDetail = new HashMap<String, String>();
		try {
			agentDetail.put("browser", getBrowser(request).toString());
			agentDetail.put("browserType", getBrowserType(request).toString());
			agentDetail.put("browserVersion", getBrowserVersion(request).toString());
			agentDetail.put("renderingEngine", getRenderingEngine(request).toString());
			agentDetail.put("os", getUserOs(request).toString());
			agentDetail.put("deviceType", getDeviceType(request).toString());
			agentDetail.put("manufacturer", getManufacturer(request).toString());
		} catch (Exception e) {
			logger.warn("Agent is null");
			new NkiaException(e);
		}
		return agentDetail;
	}
}
