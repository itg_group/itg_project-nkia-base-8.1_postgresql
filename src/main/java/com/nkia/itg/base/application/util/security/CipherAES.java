/*
 * @(#)CipherAES.java	2005. 11. 03
 *
 * Copyright 2005 Nkia.com, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.nkia.itg.base.application.exception.NkiaException;
import com.sun.xml.bind.v2.TODO;

import egovframework.com.cmm.service.EgovProperties;

/**
 * AES 알고리즘을 이용한 암호 
 * <pre>
 * Usage :
 *     사용방법 예시.
 * </pre>
 *
 * @see TODO 참고 클래스 나열.
 * @version <tt>$ Version: 1.0 $</tt>    date:2005. 11. 3
 * @author <a href="mailto:jipark@nkia.co.kr"> 박종일 </a>
 * 
 * <pre></pre>
 * 클래스 수정 내용 및 설명.
 * @version <tt>$ Reversion: 1.x $</tt>    date:2005. 11. 3
 * @author <a href="mailto:jipark@nkia.co.kr"> 박종일 </a>
 */
public class CipherAES implements CipherWorld {
	private final String algorithm 	= "AES";
	//private final String mod 		= "ECB";
	private final String mod 		= "GCM";
	//private final String padding 	= "PKCS5Padding";	
	private final String padding 	= "NoPadding";
	public byte[] key = null;	// 암호화에 사용할 Key값,Encrypt/Decrypt하기전 반드시 값을 셋팅하여야 한다.
	
	/**
	 * 생성자 : 암호화에 사용할 패스워드 설정
	 * @param pass : 패스워드   
	 */
	public CipherAES(byte[] pass){
		this.key = pass;
	}

	/**
	 * 주어진 메시지를 암호화 128bit AES방식으로 암호 
	 * @param pass : 패스워드 
	 * @return 암호화된 메시지   
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public byte[] getEncryptedByte(String message) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		// Get the KeyGenerator
		KeyGenerator kgen = KeyGenerator.getInstance(algorithm);
		kgen.init(128); // 192 and 256 bits may not be available


		// Generate the secret key specs.
		// SecretKey skey = kgen.generateKey();
		// byte[] raw = skey.getEncoded();
	    // instead of using the SecretKey object just use the encoded version.
 
		//-- for a 128-bit version, use 128/8=16 bytes
		SecretKeySpec skeySpec = new SecretKeySpec(key , algorithm);
		
		// Instantiate the cipher
		Cipher cipher = Cipher.getInstance(algorithm+"/"+mod+"/"+padding);
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

	//	byte[] encrypted = cipher.doFinal(message.getBytes("euc-kr"));
		byte[] encrypted = cipher.doFinal(message.getBytes());	// encoding 옵션을 사용하지 않는 방법으로 사용함

		//return asHex(encrypted);
		return encrypted;
	}
	
	/**
	 * 주어진 메시지를 암호화 128bit AES방식으로 암호 
	 * @param pass : 패스워드 
	 * @return 암호화된 메시지를 HexString로 리턴   
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public String getEncryptedHexString(String message) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		return CipherUtil.asHex(getEncryptedByte(message));
	}
	
	
	/**
	 * 암호화된  메시지를 128bit AES방식으로 복호화 
	 * @param message 암호화된 메시지 
	 * @return 복호화된 스트링    
	 * @throws NoSuchAlgorithmException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public String getDecryptedString(byte[] message) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		// Get the KeyGenerator
		KeyGenerator kgen = KeyGenerator.getInstance(algorithm);
		kgen.init(128); // 192 and 256 bits may not be available

		// Generate the secret key specs.
		//SecretKey skey = kgen.generateKey();
		//byte[] raw = skey.getEncoded();	 
	    //instead of using the SecretKey object just use the encoded version.
 
		//-- for a 128-bit version, use 128/8=16 bytes
		SecretKeySpec skeySpec = new SecretKeySpec(key , algorithm);

		// Instantiate the cipher
		Cipher cipher = Cipher.getInstance(algorithm+"/"+mod+"/"+padding);
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);		
		
		byte[] original = cipher.doFinal(message);
		
	//	return new String(original, "euc-kr");
		return new String(original);	// encoding 옵션을 사용하지 않는 방법으로 사용함
	}

	/**
	 * 암호화된  메시지를 128bit AES방식으로 복호화 
	 * @param message 암호화된 메시지 
	 * @return 복호화된 스트링    
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public String getDecryptedString(String message) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{
		return getDecryptedString(message.getBytes());
	}
	/**롯데UBIT용 암호화
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * 
	 * 20210413 임현대 : 시큐어코딩 관련(취약한 암호화 알고리즘 사용(RSA padding), AES/GCM/NoPadding ) 소스코드 변경 
	 * 		- EmailSetApprovalData , EmailSetApprovalOnlyData 클래스에 영향 있음
	 */
	public static String encode(String param) throws NkiaException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		byte[] textBytes = param.getBytes("UTF-8");
		String key = EgovProperties.getProperty("des.key");
    	String iv = "18c399e5MOINmoin";
		byte[] ivBytes = iv.getBytes("US-ASCII");
		IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
		byte[] key_bytes = key.getBytes("US-ASCII");
		SecretKeySpec newKey = new SecretKeySpec(key_bytes, "AES");
		Cipher cipher = null;
		//cipher = Cipher.getInstance("AES/GCM/NoPadding");
		cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); 
		cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
		byte[] result = cipher.doFinal(textBytes);
		String strResult =Base64.encodeBase64String(result);
		return strResult.replaceAll("[+]", "-").replaceAll("/", "_");
	}
	
}
