﻿package com.nkia.itg.base.application.util.common;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.StringTokenizer;

import com.nkia.itg.base.application.exception.NkiaException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 
 * @version 1.0 2013. 2. 8.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 *  String Utilities
 * </pre>
 *
 */
public class StringUtil {
	
	/**
	 * 
	 * 원본 문자열에 Lpad 부여
	 * 
	 * @param str
	 * @param len
	 * @param addStr
	 * @return
	 */
	public static String lpad(String str, int len, String addStr) {
		String result = str;
		int templen = len - result.length();

		for (int i = 0; i < templen; i++) {
			result = addStr + result;
		}
		return result;
	}
	
	/**
	 * null 인지 판단.
	 * TODO
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isNull(Object obj) {
		boolean result = false;
		if(obj == null){
			result = true;
		}
		return result;
	}

	//@@20130716 김건범-ITG-0001
    //신규추가
	/**
	 * 문제 변환
	 */
	public static String replaceAll(String source, String ex, String rep){
	    if ((source == null) || 
	      (source.trim().equals("")) || 
	      (source.trim().equals("null")))
	    {
	      return "";
	    }
	    return source.replaceAll(ex, rep);
	}
    //END

	//@@20130711 전민수 START
    //신규추가
	/**
	 * 문자열을 특정 delimiter를 이용하여 분리한다.
	 *
	 * @param delim delimiter
	 * @param buf 분리할 문자열
	 * @return 분리된 문자열의 String[]
	 */
	public static String[] explode(String delim, String buf) {
		StringTokenizer token = new StringTokenizer(buf, delim);
		ArrayList arrList = new ArrayList();
		for (; token.hasMoreTokens();) {
			arrList.add(token.nextToken());
		}
		String retval[] = new String[arrList.size()];
		for (int i = 0; i < retval.length; i++) {
			retval[i] = (String) arrList.get(i);
		}
		return retval;
	}
	//@@20130711 전민수 
    //END
	
	/**
	 * 문자열의 개행문자를 <br>로 변경한다.
	 *
	 * @param value 변경하려는 문자열
	 * @return 변경된 문자열
	 */
	//@@20130711 김도원 START
	//개행문자 -> <br> 변경기능 추가
	public static String changeLineAlignmentForHtml(String value) {
		if(value != null) {
			value = value.replaceAll("\r\n", "<br>");
			value = value.replaceAll("\n", "<br>");
			value = value.replaceAll("\r", "<br>");			
		} else {
			value = "";
		}
		return value;
	}
	//@@20130711 김도원 END

	/**
	 * 문자열의 길이를 반환한다. Null->0
	 *
	 * @param value 변경하려는 문자열
	 * @return 변경된 문자열
	 */	
	//@@20130802 김도원 START
	//문자열의 길이를 반환한다. Null->0
	public static int getLength(String value) {
		int length = 0;
		if(value != null) {
			length = value.length();
		}
		return length;
	}
	//@@20130802 김도원 END
	
	/**
	 * 특수문자를 삭제한다.
	 *
	 * @param value 변경하려는 문자열
	 * @return 변경된 문자열
	 */	
	//@@20130802 김도원 START
	//특수문자를 삭제한다.
	public static String removeSpecialLetters(String str){       
		String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
		str = str.replaceAll(match, "");
		return str;
	}
	//@@20130802 김도원 END
	
	
	/**
	 * boolean 파싱
	 * @param obj
	 * @return
	 */
	public static boolean parseBoolean(Object obj){
		boolean result = false;
		result  = parseBoolean(obj,"true");
		return result;
	}
	
	/**
	 * boolean 파싱 (target String 을 지정)
	 * TODO
	 * 
	 * @param obj
	 * @param trueTargetStr
	 * @return
	 */
	public static boolean parseBoolean(Object obj,String trueTargetStr){
		boolean result = false;
		if(obj != null){
			if(obj instanceof java.lang.String){
				String objStr = obj.toString();
				if(objStr.equals(trueTargetStr)){
					result = true;
				}else{
					result = false;
				}
			}else if(obj instanceof java.math.BigDecimal){
				BigDecimal  c = (BigDecimal)obj;
				String val  = String.valueOf((Integer)c.intValue());
				if(val.equalsIgnoreCase(trueTargetStr)){
					result = true;
				}else{
					result = false;
				}
			}else if(obj instanceof java.lang.Boolean){
				result = (Boolean)obj;
			}
		}else{
			result = false;
		}
		return result;
	}
	
	
	/**
	 * integer 파싱
	 * @param obj
	 * @return
	 */
	public static int parseInteger(Object obj){
		int result = 0;
		if(obj != null){
			if(obj instanceof java.math.BigDecimal){
				BigDecimal  c = (BigDecimal)obj;
				result = (Integer)c.intValue();
			}else if(obj instanceof java.lang.String){
				result = Integer.parseInt((String)obj);
			}else if(obj instanceof java.lang.Integer){
				result = (Integer)obj;
			}
		}
		return result;
	}
	
	
	/**
	 * String 파싱
	 * @param obj
	 * @return
	 */
	public static String parseString(Object obj){
		String result = "";
		if(obj != null){
			if(obj instanceof java.math.BigDecimal){
				BigDecimal  c = (BigDecimal)obj;
				int val = (Integer)c.intValue();
				result = String.valueOf(val);
			}else if(obj instanceof java.lang.String){
				result = (String)obj;
			}else if(obj instanceof java.lang.Integer){
				int val = (Integer)obj;
				result = String.valueOf(val);
			}else if(obj instanceof java.lang.Long){
				long val = (Long)obj;
				result = String.valueOf(val);
			}else if(obj instanceof java.lang.Double){
				double val = (Double)obj;
				result = String.valueOf(val);
			}else if(obj instanceof java.lang.Boolean){
				boolean val = (Boolean)obj;
				result  = String.valueOf(val);
			}
		}
		return result;
	}
	
	public static String replaceNullToNA(String str){
		if(str == null || "null".equals(str)) {
			str = "N/A";
		}
		return str;
	}
	
	public static String replaceNull(String source, String replace) {
		if (source == null || source.trim().equals("") || source.trim().equals("null") || source.trim().equals("undefined")) {
			return replace;
		} else {
			return source;
		}
	}
	
	public static String trim(String str){
		String result = "";
		if(str != null) {
			result = str.trim();
		} 
		return result;
	}
	
    /**
     * 16진 문자열을 byte 배열로 변환한다.
     * @param hex
     * @return
     */
    public static byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() % 2 != 0) {
            return new byte[]{};
        }

        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < hex.length(); i += 2) {
            byte value = (byte)Integer.parseInt(hex.substring(i, i + 2), 16);
            bytes[(int) Math.floor(i / 2)] = value;
        }
        return bytes;
    }
    
    /**
     * 
     * 데이터 싱글쿼테이션 치환
     * 
     * @param source
     * @return
     */
    public static String replaceSingleQuotation(String source) {
		return source.replace("'", "''");
	}

    /**
	 * From a base 64 representation, returns the corresponding byte[]
	 * 
	 * @param data
	 *            String The base64 representation
	 * @return byte[]
	 * @throws IOException
	 */
	public static byte[] base64ToByte(String data) throws NkiaException, IOException {
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] result = null;

		try {
			result = decoder.decodeBuffer(data);
		} catch (IOException e) {
			throw new NkiaException(e);
		}

		return result;
	}

	/**
	 * From a byte[] returns a base 64 representation
	 * 
	 * @param data
	 *            byte[]
	 * @return String
	 * @throws IOException
	 */
	public static String byteToBase64(byte[] data)throws NkiaException {
		BASE64Encoder endecoder = new BASE64Encoder();
		String result = "";
		
		try {
			result = endecoder.encode(data);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return result;
	}
}
