/*
 * @(#)CipherRSA.java              2015. 10. 27.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;

public class CipherRSA {
	
	
	/**
	 * 
	 * PrivateKey Return
	 * 
	 * @return
	 * @throws NkiaException
	 */
	public static PrivateKey getPrivateKey() throws NkiaException {
		HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
    	HttpSession session = req.getSession();
		PrivateKey privateKey = (PrivateKey)session.getAttribute("__rsaPrivateKey__");
        return privateKey;
    }
	
	/**
	 * 
	 * Client RSA 복호화
	 * 
	 * @param privateKey
	 * @param securedValue
	 * @return
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws UnsupportedEncodingException 
	 * @throws NkiaException
	 */
    public static String getDecrypt(PrivateKey privateKey, String securedValue) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("RSA");
        byte[] encryptedBytes = StringUtil.hexToByteArray(securedValue);
        
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
       
        // 복호화된 userId와 passWord를 가지고 있습니다. 
        String decryptedValue = new String(decryptedBytes, "UTF-8"); // 문자 인코딩 주의.
        return decryptedValue;
    }
}
