package com.nkia.itg.base.application.constraint;

public enum BaseEnum {
	
	// ERROR
	ERROR_PAGE_400("/itg/base/error_page_400"),
	ERROR_PAGE_401("/itg/base/error_page_401"),
	ERROR_PAGE_403("/itg/base/error_page_403"),
	ERROR_PAGE_404("/itg/base/error_page_404"),
	ERROR_PAGE_500("/itg/base/error_page_500"),
	ERROR_CODE_401("/itg/base/error_code_401.nvf"),
	ERROR_CODE_500("/itg/base/error_code_500.nvf"),
	ERROR_CODE_MULTI_LOGIN("/itg/base/error_code_multi_login.nvf"),
	ERROR_CODE_MENU_DENIAL("/itg/base/error_page_menu_denial"),
	
	// Session
	SESSION_EXPIRED_STATUS("EXPIRED"),
	
	// Page
	PAGER_START_PARAM("start"),
	PAGER_PAGE_PARAM("page"),
	PAGER_LIMIT_PARAM("limit");
	
	
	private String message;
	
	BaseEnum(String message){
		this.message = message;
    }
	
	public String getString() {
		return this.message;
	}
}
