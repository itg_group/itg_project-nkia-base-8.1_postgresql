package com.nkia.itg.base.application.config;

import java.util.List;
import java.util.Map;

import com.nkia.itg.system.email.dao.EmailInfoDAO;

public class NkiaApplicationEmailMap {
	
	public static String getMailInfo(String mailId) {
		String result = "";
		if(mailId != null && !"".equals(mailId)) {
			result = System.getProperty(mailId);
		}
		return result;
	}
	
	
	public static void loadMailInfo() {
		EmailInfoDAO emailInfoDAO = (EmailInfoDAO)NkiaApplicationContext.getCtx().getBean("emailInfoDAO");
		List emailList = emailInfoDAO.selectEmailSenderInfoList();
		for(int i = 0 ; i < emailList.size() ; i++) {
			Map emailMap = (Map)emailList.get(i);
			String pKey = (String)emailMap.get("EMAIL_CONFIG_ID");
			String pValue = (String)emailMap.get("EMAIL_CONFIG_DATA");
			if(pValue != null && !"".equals(pValue)) {
				System.setProperty(pKey, pValue);
			} else {
				System.setProperty(pKey, "");
			}
		}
	}
}
