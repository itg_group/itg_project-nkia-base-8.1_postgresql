package com.nkia.itg.base.application.log;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.nkia.itg.base.application.util.json.JsonMapper;

public class HttpErrorLog {

	private Long id;
	private String system;
	private String loggerName;
	private String serverName;
	private String path;
	private String message;
	private String headerMap;
	private String parameterString;
	private String userInfo;
	private String agentDetail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getLoggerName() {
		return loggerName;
	}

	public void setLoggerName(String loggerName) {
		this.loggerName = loggerName;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getHeaderMap() {
		return headerMap;
	}

	public void setHeaderMap(String headerMap) {
		this.headerMap = headerMap;
	}

	public String getParameterString() {
		return parameterString;
	}

	public void setParameterString(String parameterString) {
		this.parameterString = parameterString;
	}

	public String getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(String userInfo) {
		this.userInfo = userInfo;
	}

	public String getAgentDetail() {
		return agentDetail;
	}

	public void setAgentDetail(String agentDetail) {
		this.agentDetail = agentDetail;
	}

	public String getErrorTime() {
		long time = System.currentTimeMillis(); 
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return dayTime.format(new Date(time));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\r\n");
		builder.append(getColumnData("[Error Message]", message)); 
		builder.append(getColumnData("[Error Time]", this.getErrorTime()));
		builder.append(getColumnData("[Request Url]", path));
		builder.append(getColumnData("[System Name]", system));
		builder.append(getColumnData("[Server Name]", serverName));
		builder.append(getColumnData("[USER Info]", JsonMapper.toPrettyJson(userInfo)));
		
		builder.append(getColumnData("[Http Header Info]", JsonMapper.toPrettyJson(headerMap)));
		builder.append(getColumnData("[Http Body Info]", JsonMapper.toPrettyJson(parameterString)));
		builder.append(getColumnData("[User Env Info]", JsonMapper.toPrettyJson(agentDetail)));
		builder.append("\r\n");
		
		return builder.toString();
	}

	private Object getColumnData(String title, String message) {
//		return title + System.lineSeparator() + message + System.lineSeparator();
		return title + System.getProperty("line.separator") + message + System.getProperty("line.separator");
		
	}
}
