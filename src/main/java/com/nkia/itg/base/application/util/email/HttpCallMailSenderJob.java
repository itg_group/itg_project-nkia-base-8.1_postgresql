package com.nkia.itg.base.application.util.email;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.system.email.service.EmailHistoryService;

public class HttpCallMailSenderJob extends AbstractEmailSendJob {
	
	private static final Logger logger = LoggerFactory.getLogger(HttpCallMailSenderJob.class);
	private EmailVO mailVO;
	private final String ENCODING = "euc-kr";
	
	public void setMailVO(EmailVO mailVO) {
		this.mailVO = mailVO;
	}
	
	class EmailAuthenticator extends Authenticator {
		private String id;
		private String password;
		
		public EmailAuthenticator(String id, String password) {
			this.id = id;
			this.password = password;
		}
		
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(id, password);
		}
	}

	@Override
	public void run() {
		try {
			sendMail();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}

	@Override
	public void sendMail() throws NkiaException {
		OutputStreamWriter out = null;
		
		try {
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			
			params.put("SNDR_EMPNO", mailVO.getFrom());
			params.put("SNDR_NM", mailVO.getFrom_user_nm());
			params.put("EML_TL", mailVO.getSubject());
			params.put("EML_CTS", mailVO.getContent());
			params.put("RCVR_EMPNO", mailVO.getTo());
			
			StringBuilder postData = new StringBuilder();
			for(Map.Entry<String,Object> param : params.entrySet()) {
				if (postData.length() != 0) {
					postData.append('&');
				};
				postData.append(param.getKey());
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), ENCODING));
			}
			byte[] postDataBytes = postData.toString().getBytes(ENCODING);
	
			String urlStr = mailVO.getHost();
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			
			conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataBytes.length));

			out = new OutputStreamWriter(conn.getOutputStream());
			out.write(postData.toString());
			out.flush();
			
			String suc = conn.getHeaderField("SUC");
			String msg = conn.getHeaderField("MSG");
			
			if(msg!=null){
				msg = URLDecoder.decode(msg, ENCODING);
			}
			
			logger.debug("처리결과: " + suc);
			logger.debug("처리내용: " + msg);
			logger.debug("열결여부: " + conn.getHeaderField(0));
			
			//out.close();
			
			logger.debug("===================== [Send Mail] Success =====================");
			// 메일 전송 이력 등록
			insertSendMailHistory("SUCCESS", mailVO, "");
		} catch (IOException e) {
			logger.debug("===================== [Send Mail] Fail =====================");
			// 메일 전송 이력 등록
			insertSendMailHistory("FAIL", mailVO, e.toString());
			throw new NkiaException(e.getMessage());
		} catch (Exception e) {
			logger.debug("===================== [Send Mail] Fail =====================");
			// 메일 전송 이력 등록
			insertSendMailHistory("FAIL", mailVO, e.toString());
			throw new NkiaException(e.getMessage());
		} finally {
			try {
				if(out != null){
					out.close();
				}
			} catch (IOException e){
				logger.debug("===================== [Send Mail] Fail =====================");
				// 메일 전송 이력 등록
				insertSendMailHistory("FAIL", mailVO, e.toString());
				throw new NkiaException(e.getMessage());
			} catch (Exception e){
				logger.debug("===================== [Send Mail] Fail =====================");
				// 메일 전송 이력 등록
				insertSendMailHistory("FAIL", mailVO, e.toString());
				throw new NkiaException(e.getMessage());
			}
		}
	}
	
	// 메일발송 이력 관리
	public void insertSendMailHistory(String status, EmailVO emailVO, String errMsg) throws NkiaException {
		emailVO.setStatus(status);
		emailVO.setErr_msg(errMsg);

		EmailHistoryService emailHistoryService = (EmailHistoryService) NkiaApplicationContext.getCtx().getBean("emailHistoryService");
		emailHistoryService.insertMailSendHistory(emailVO);
	}
}
