package com.nkia.itg.base.application.util.web;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.nkia.itg.base.application.exception.NkiaException;

import eu.bitwalker.useragentutils.Browser;

public class WebUtil {

	public static Map lowerCaseMapKey(Map param) {
		Map resultMap = null;
		try {
			Class<?> clas = Class.forName(param.getClass().getName());
			Constructor<?> constructor = clas.getConstructor();
			resultMap = (Map)constructor.newInstance();
			Iterator keySet = param.keySet().iterator();
			while(keySet.hasNext()) {
				String key = (String)keySet.next();
				if(key != null) {
					resultMap.put(key.toLowerCase(), param.get(key));			
				}
			}
		} catch (ClassNotFoundException ce) {
			throw new RuntimeException("Confirm parameter map object");
		} catch (SecurityException se) {
			throw new RuntimeException("Confirm parameter map object");
		} catch (NoSuchMethodException ne) {
			throw new RuntimeException("Confirm parameter map object");
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("Can not use custom map object");
		} catch (InstantiationException e) {
			throw new RuntimeException("Can not use custom map object");
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Can not use custom map object");
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Can not use custom map object");
		}
		return resultMap;
	}
	
	public static HttpServletRequest getCurrentRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}

	public static String getJsonContentType(HttpServletRequest request) {
		Browser browser = AgentUtils.getBrowser(request);

		if (browser != null && browser == Browser.IE) {
			return "text/plain; charset=UTF-8";
		}

		return "application/json; charset=UTF-8";
	}

	public static Map<String, String> getCurrentUser() {
		Map<String, String> mockUser = new HashMap<String, String>();
		mockUser.put("name", "brant");
		mockUser.put("userGroup", "STANDARD");
		return mockUser;
	}
}
