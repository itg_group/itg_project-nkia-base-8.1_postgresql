/**
 * @file : ApplicationAccessFilter.java
 * @author : 정영태
 * @since : 2010. 12. 10. 오전 9:30:07
 * @version : 1.0
 * @see
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -----------------------------------------
 *  2010. 12. 10. NKIA_YT 
 *  
 *  </pre>
 * Copyright 2010(C) by NKIA. All right reserved.
 */

package com.nkia.itg.base.application.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.web.MDCUtil;
import com.nkia.itg.certification.constraint.LoginResultEnum;
import com.nkia.itg.certification.vo.UserVO;
import com.nkia.itg.nbpm.common.NbpmRuntimeContainer;
import com.nkia.itg.system.loginManager.service.LoginManagerService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

/**
 * URL / Context / Session 인증관련 필터
 * @version 1.0 2011. 1. 19.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 *
 */
public class ApplicationAccessFilter implements Filter{
	
	private static final Logger logger = LoggerFactory.getLogger(NbpmRuntimeContainer.class);
    /**
     * The filter configuration object we are associated with.  If this value
     * is null, this filter instance is not currently configured.
     */
//	protected FilterConfig filterConfig = null; 
    protected FilterConfig filterConfig; //202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
	@SuppressWarnings("unchecked")
	private HashMap unProtectedUris = new HashMap();
	@SuppressWarnings("unchecked")
	private HashMap unProtectedContexts = new HashMap();
	private String RedirectPage = null;
    private boolean use = true;
    private boolean isCors = false;
    
    
    public void init(FilterConfig filterConfig) throws ServletException {
    	this.filterConfig = filterConfig;
		
        // Access Filter user flag
        String sUse = filterConfig.getInitParameter("use");
        if ( sUse == null || sUse.equalsIgnoreCase("true") )
            this.use = true;
        else
            this.use = false;
        
		// UnProtected Uri
		String uri = filterConfig.getInitParameter("unprotected.uri");
	
		if ( uri == null )	uri = "";
		StringTokenizer tok = new StringTokenizer(uri, ",");
		
		while (tok.hasMoreTokens())
		{
			String url = tok.nextToken().trim();
			unProtectedUris.put(url, url);
		}
		
		// UnProtected Contexts
		String Contexts = filterConfig.getInitParameter("unprotected.context");
		if ( Contexts == null )	Contexts = "";
		tok = new StringTokenizer(Contexts, ",");

		while (tok.hasMoreTokens())
		{
			String Context = tok.nextToken();
			unProtectedContexts.put(Context, Context);
		}
		
		// Redirect Page
		RedirectPage = filterConfig.getInitParameter("redirect");
		if ( RedirectPage == null )	RedirectPage = "/itg/certification/index.do";
	}
    
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	try {
			HttpServletRequest req = (HttpServletRequest) request;		
			HttpServletResponse res = (HttpServletResponse) response;
			
			// Uri 
			String Uri = req.getRequestURI();
			String Context = getContext(Uri);
			String Page = getPage(Uri);
			
			MDCUtil.set(MDCUtil.REQUEST_URI_MDC, Uri);
			
			/**
			 * Unprotected된 페이지인지 체크
			 * Login된 Session인지 체크
			 */
			if ( !isUnProtectedContext(Context) && !isUnProtectedUri(Page) && !isSignedIn(req) ){
				String accept = req.getHeader("Accept").toLowerCase();
				if (accept.indexOf("json") > -1) {
					filterConfig.getServletContext().getRequestDispatcher(BaseEnum.ERROR_CODE_401.getString()).forward(request,response);
				}else{
					filterConfig.getServletContext().getRequestDispatcher(RedirectPage).forward(request,response);
				}
			}else if(!isUnProtectedUri(Page) && !isPassedMultiLogin(req)){
				String accept = req.getHeader("Accept").toLowerCase();
				if (accept.indexOf("json") > -1) {
					filterConfig.getServletContext().getRequestDispatcher(BaseEnum.ERROR_CODE_MULTI_LOGIN.getString()).forward(request,response);
				}else{
					request.setAttribute("filter_message", LoginResultEnum.MULTI_LOGIN_ERR.getMessage());
					filterConfig.getServletContext().getRequestDispatcher(RedirectPage).forward(request,response);
				}
			}else{
				chain.doFilter(request, response);
				return;
			}
		} catch (Exception e) {
			new NkiaException(e);
		}
	}
	
	public void destroy() {
		this.filterConfig = null;
	}

	/**
     * 요청된 페이지가 비보호된 페이지(Session Tracking) 여부 체크  
     * @param uri
     * @return 비보호된 페이지=true
     */
    private boolean isUnProtectedUri(String uri){
    	String page = (String)unProtectedUris.get(uri);
    	if((page != null) || (uri.indexOf("sso/") > 0) || (uri.indexOf("/login.do") > 0 )) {
    		return true; 
    	} else {
    		return false;
    	}
	}

    /**
     * 요청된 URI 에서 Context부분이 비보호된 Context 인지 여부체크
     * @param context
     * @return 비보호된 Context=true
     */
    private boolean isUnProtectedContext(String context)
	{
    	if ( context == null ) return false;
		return (unProtectedContexts.get(context)==null ? false : true);
	}

    private String getPage(String uri)
    {
    	String page = uri;
    	
    	if(page != null) {
    		int i = page.indexOf("?");
    		if (i >= 0)
    		{
    			page = page.substring(0, i);
    		}
    	}
    	
		return page;
    }
    
    /**
     * 입력된 URL중 Context를 return한다
     * @param uri 주소
     * @return Context 문자열
     */
    private String getContext(String uri)
    {	
    	String Context = null;
    	int nPos = 0;

    	if ( uri != null && uri.length() > 0 ) {
    		if ( uri.charAt(0) != '/' )
        		return null;
    		
        	nPos = uri.indexOf('/', 1);
        	if ( nPos == -1 )
        		return null;
        	
    		Context = uri.substring(1, nPos);
    	}

    	return Context;
    }

    /**
     * 요청된 Request가 인증된 Session인지 체크한다
     * @param request
     * @return 인증된 Session이면 true, 인증되지않으면 false를 리턴한다.
     */
    private boolean isSignedIn(HttpServletRequest request)throws NkiaException{
    	boolean bRslt = true;
    	try {
        	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
        	UserVO user = null;
        	if(isAuthenticated) {
        		user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
        	} else {
        		bRslt = false;        		
        	}
        	
			if ( user == null ) {
				bRslt = false;
			}else if ( user.getUser_id() == null ){
			    bRslt = false;
			} else {
			    bRslt = true;
			}
		} catch (Exception e) {
			bRslt = false;
			throw new NkiaException(e);
		}
    	// 크로스 브라우져 허용이 되면 세션 인증처리에서 true를 리턴한다.
    	if( isCors ) bRslt = true;
		return bRslt;
	}
    
    /**
     * 중복로그인 설정에 따른 허용 여부
     * @param request
     * @return
     * @throws NkiaException
     */
    private boolean isPassedMultiLogin(HttpServletRequest request)throws NkiaException{
    	
    	LoginManagerService loginManagerService = (LoginManagerService) NkiaApplicationContext.getCtx().getBean("loginManagerService");
    	
    	boolean result = true;
    	
    	Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	UserVO user = null;
    	
    	if(isAuthenticated) {
    		
    		HttpSession session = request.getSession();
    		
    		user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
    		
    		List loginProperty = loginManagerService.searchLoginManagerList(null);
    		String userId = user.getUser_id();
    		
    		String ip = request.getHeader("X-FORWARDED-FOR");
        	if(ip == null){
        		ip = request.getRemoteAddr();
        	}
    		
    		if(loginProperty != null && loginProperty.size()>0) {
    			
    			HashMap loginMngMap = (HashMap)loginProperty.get(0);
    			String multiLoginYn = StringUtil.parseString(loginMngMap.get("MULTI_LOGIN_YN"));
    			
    			if("N".equals(multiLoginYn)) {
    				// 중복로그인 허용 불가일 경우, 중복로그인에 대한 처리 실행
    				List mySessionList = new ArrayList();
    				HashMap mySessionMap = new HashMap();
    				
    				mySessionMap.put("USER_ID", userId);
    				mySessionMap.put("SESSION_ID", session.getId());
    				mySessionMap.put("LOGIN_IP", ip);
    				
    				mySessionList = loginManagerService.searchLoginSessionList(mySessionMap);
    				
    				if(mySessionList!=null && mySessionList.size()>0) {
    					HashMap mySession = (HashMap)mySessionList.get(0);
    					String status = StringUtil.parseString(mySession.get("STATUS"));
    					if(status.equals(BaseEnum.SESSION_EXPIRED_STATUS.getString())) {
    						result = false;
    					}
    				}
    			}
			}
    	}
    	
    	return result;
    }
}
