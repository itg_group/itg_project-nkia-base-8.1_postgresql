package com.nkia.itg.base.application.util.email;

public enum EmailEnum {
	
	// Y or N
	COMMON_Y("Y"),
	COMMON_N("N"),
	
	//메일구분 = 메일발송정보(SYS_MAIL_SEND_HISTORY)의 MAIL_ID 컬럼의 값으로 교체하면 됨
	// 2개의 메일서버 이용시 MAIL_ID_A, MAIL_ID_B 등 분리해서 각 데이터 값을 담아줌
	// 메일 발송하는 로직에서 구분 조건에 대한 분기를 설정하고 각각의 사용할 변수로 셋팅 한다. 
	EMAIL_ID_1("MAIL-1"),
	EMAIL_ID_2("MAIL-2"),
	//메일의 From으로 
	EMAIL_SYSTEM_NAME("ITSM시스템"),
	EMAIL_SYSTEM_PREFIX("[ITSM시스템]")
	;
	
	
	private String message;
	
	EmailEnum(String message){
		this.message = message;
    }
	
	public String getString() {
		return this.message;
	}
}
