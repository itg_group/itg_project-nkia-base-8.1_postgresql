package com.nkia.itg.base.application.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.nkia.itg.base.application.log.HttpErrorLog;
import com.nkia.itg.base.application.util.web.MDCUtil;
 
 
@ControllerAdvice
public class ControllerExceptionAdvicer {
	
	private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionAdvicer.class);
	
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
	public String handleException(Throwable throwable) throws Exception {
		logging(throwable);
		throw (Exception) throwable;
	}
     
	protected void logging(Throwable throwable) {
		HttpErrorLog errorLog = new HttpErrorLog();
		errorLog.setSystem("itg");
		errorLog.setServerName(MDCUtil.get(MDCUtil.SERVER_NAME_MDC));
		errorLog.setMessage(throwable.getMessage());
		errorLog.setPath(MDCUtil.get(MDCUtil.REQUEST_URI_MDC));
		errorLog.setHeaderMap(MDCUtil.get(MDCUtil.HEADER_MAP_MDC));
		errorLog.setParameterString(MDCUtil.get(MDCUtil.PARAMETER_JSON_MDC));
		errorLog.setUserInfo(MDCUtil.get(MDCUtil.USER_INFO_MDC));
		errorLog.setAgentDetail(MDCUtil.get(MDCUtil.AGENT_DETAIL_MDC));
		
		String printMessage = errorLog.toString() 
				+ "========================="
				+ "	error stack trace "
				+ "=========================";
		logger.error(printMessage);
		logger.error("ERROR", throwable);
	}
}
