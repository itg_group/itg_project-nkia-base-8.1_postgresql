package com.nkia.itg.base.application.util.email;

import com.nkia.itg.customize.email.CustomizeMailSendUtil;
import com.sun.star.uno.RuntimeException;

/**
 * Email 연계 방식을 결정하는 클래스
 * SMTP 방식과 JSP 방식 두가지 중에 선택 할 수 있다.
 * @author NKIA
 *
 */
public class EmailSendJobContainer {
	
	public static AbstractEmailSendJob getEmailSendJob(String jobType) {
		return createEmailSendJob(jobType);
	}
	
	private static AbstractEmailSendJob createEmailSendJob(String jobType) {
		//job Type에 따라 연동 모듈을 변경한다.(프라퍼티에 정의 되어 있음)
		switch (jobType) {
			case "SMTP" :
				return new SmtpCallEmailSendJob();
			case "JSP" :
				return new JspCallEmailSenderJob();
			case "HTTP" :
				return new JspCallEmailSenderJob();
			case "CUSTOM" :
				return new CustomizeMailSendUtil();
			default:
				throw new RuntimeException("not found EmailSendJob Module");
		}
	}
	
}
