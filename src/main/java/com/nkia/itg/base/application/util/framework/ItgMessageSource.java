package com.nkia.itg.base.application.util.framework;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;

import egovframework.com.cmm.EgovMessageSource;

public class ItgMessageSource {
	
	public String getMessage(String code) throws NkiaException {
		EgovMessageSource egovMessageSource = (EgovMessageSource)NkiaApplicationContext.getCtx().getBean("egovMessageSource");
		return egovMessageSource.getMessage(code);
	}
}
 