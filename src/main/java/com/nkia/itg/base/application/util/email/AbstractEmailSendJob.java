package com.nkia.itg.base.application.util.email;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;

public abstract class AbstractEmailSendJob implements Runnable {

	public abstract void setMailVO(EmailVO mailVO);
	
	public abstract void sendMail() throws NkiaException;
}
