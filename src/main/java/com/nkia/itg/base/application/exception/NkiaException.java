package com.nkia.itg.base.application.exception;

import java.io.IOException;
import java.sql.SQLException;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class NkiaException extends Exception {
	
	private static final Logger logger = LoggerFactory.getLogger(NkiaException.class);
	
	public NkiaException(){
		super();
	}  
	public NkiaException(String message){
		super(message);
		//super.printStackTrace();
	}
	
	public NkiaException(Exception e) {
		super(e);
		//debug(e);
	}
	
	public NkiaException(Exception e, String message) {
		super(message, e);
		//debug(e);
	}
	 
	/**
	 * 시스템 로그를 출력한다.
	 * @param obj Object
	 */
	private static void debug(Object obj) {
		if(obj != null){
			if (obj instanceof NullPointerException) {
				logger.error("NullPointerException LOG : " + ((Exception)obj).getMessage());
			} else if (obj instanceof SQLException) {
				logger.error("SQLException LOG : " + ((Exception)obj).getMessage());
			} else if (obj instanceof IOException) {
				logger.error("IOException LOG : " + ((Exception)obj).getMessage());
			} else if (obj instanceof EncryptionOperationNotPossibleException) {
				logger.error("EncryptionOperationNotPossibleException LOG : " + ((Exception)obj).getMessage());
			} else if (obj instanceof java.lang.Exception) {
				logger.error("java.lang.Exception LOG : " + ((Exception)obj).getMessage());
			} else {
				logger.error("exception :" + ((Exception)obj).getMessage());
			}
		}
	}
}
