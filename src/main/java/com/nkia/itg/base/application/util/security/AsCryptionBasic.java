/*
 * @(#)RsaPassWordBasic.java              2013. 6. 27.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nkia.itg.base.application.exception.NkiaException;

public class AsCryptionBasic {

//	public static String encrypt(String seed, String cleartext)
//			throws NkiaException{
//
//		byte[] rawKey = getRawKey(seed.getBytes());
//		byte[] result = encrypt(rawKey, cleartext.getBytes());
//		return toHex(result);
//
//	}
//
//	public static String decrypt(String seed, String encrypted)
//			throws NkiaException{
//
//		byte[] rawKey = getRawKey(seed.getBytes());
//		byte[] enc = toByte(encrypted);
//		byte[] result = decrypt(rawKey, enc);
//		return new String(result);
//
//	}
//
//	private static byte[] getRawKey(byte[] seed) throws NkiaException{
//
//		KeyGenerator kgen = KeyGenerator.getInstance("AES");
//		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
//		sr.setSeed(seed);
//		kgen.init(128, sr); // 192 and 256 bits may not be available
//		SecretKey skey = kgen.generateKey();
//		byte[] raw = skey.getEncoded();
//		return raw;
//
//	}
//
//	private static byte[] encrypt(byte[] raw, byte[] clear) throws NkiaException{
//
//		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//		Cipher cipher = Cipher.getInstance("AES");
//		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
//		byte[] encrypted = cipher.doFinal(clear);
//		return encrypted;
//
//	}
//
//	private static byte[] decrypt(byte[] raw, byte[] encrypted)
//			throws NkiaException	{
//
//		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//		Cipher cipher = Cipher.getInstance("AES");
//		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
//		byte[] decrypted = cipher.doFinal(encrypted);
//		return decrypted;
//
//	}
//
//	public static String toHex(String txt){
//
//		return toHex(txt.getBytes());
//
//	}
//
//	public static String fromHex(String hex){
//
//		return new String(toByte(hex));
//
//	}
//
//	public static byte[] toByte(String hexString){
//
//		int len = hexString.length() / 2;
//		byte[] result = new byte[len];
//		for (int i = 0; i < len; i++)
//			result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2),
//					16).byteValue();
//		return result;
//
//	}
//
//	public static String toHex(byte[] buf){
//
//		if (buf == null)
//			return "";
//		StringBuffer result = new StringBuffer(2 * buf.length);
//		for (int i = 0; i < buf.length; i++) {
//			appendHex(result, buf[i]);
//		}
//
//		return result.toString();
//
//	}
//
//	private final static String HEX = "0123456789ABCDEF";
//
//	private static void appendHex(StringBuffer sb, byte b){
//
//		sb.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
//
//	}
//	
	
	// 2016.08.01 최양규 추가 - 포탈메인추가
	private final static int KEY_SIZE = 2048;
	
	public static void getRsaPublicKey(HttpServletRequest request)throws NkiaException, NoSuchAlgorithmException, InvalidKeySpecException{
		KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
		generator.initialize(KEY_SIZE);

		KeyPair keyPair = generator.genKeyPair();
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");

		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();

		HttpSession session = request.getSession();
		// 세션에 공개키의 문자열을 키로하여 개인키를 저장한다.
		session.setAttribute("__rsaPrivateKey__", privateKey);

		// 공개키를 문자열로 변환하여 JavaScript RSA 라이브러리 넘겨준다.
		RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(
				publicKey, RSAPublicKeySpec.class);

		String publicKeyModulus = publicSpec.getModulus().toString(16);
		String publicKeyExponent = publicSpec.getPublicExponent().toString(16);

		request.setAttribute("publicKeyModulus", publicKeyModulus);
		request.setAttribute("publicKeyExponent", publicKeyExponent);
		
	}

}
