package com.nkia.itg.base.application.config;

import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.system.property.dao.PropertyDAO;

import egovframework.com.cmm.service.EgovProperties;

public class NkiaApplicationPropertiesMap {
	
	public static void addProperty(String propertyName, String propertyValue) {
		System.setProperty(propertyName, propertyValue);
	}
	
	public static String getProperty(String propertyName) {
		String result = "";
		if(propertyName != null && !"".equals(propertyName)) {
			result = System.getProperty(propertyName);
		}
		return result;
	}
	
	public static long getPropertyToLong(String propertyName) {
		long result = 0;
		if(propertyName != null && !"".equals(propertyName)) {
			result = Long.parseLong(System.getProperty(propertyName));
		}
		return result;
	}
	
	public static int getPropertyToInt(String propertyName) throws NkiaException {
		int result = 0;
		if(propertyName != null && !"".equals(propertyName)) {
			try {
				result = Integer.parseInt(System.getProperty(propertyName));				
			} catch ( Exception e ) {
				throw new NkiaException(e);
			}
		}
		return result;
	}
	
	public static boolean getPropertyToBoolean(String propertyName) {
		boolean result = true;
		if(propertyName != null && !"".equals(propertyName)) {
			result = Boolean.parseBoolean(System.getProperty(propertyName));
		}
		return result;
	}
	
	public static void loadProperty() {
		PropertyDAO propertyDAO = (PropertyDAO)NkiaApplicationContext.getCtx().getBean("propertyDAO");
		String propertyUserId = EgovProperties.getProperty("application.property.user");
		List properties = propertyDAO.searchPropertyListForApplicationLoad(propertyUserId);
		for(int i = 0 ; i < properties.size() ; i++) {
			Map property = (Map)properties.get(i);
			String pKey = (String)property.get("prpty_id");
			String pValue = (String)property.get("prpty_val");
			if(pValue != null && !"".equals(pValue)) {
				System.setProperty(pKey, pValue);				
			} else {
				System.setProperty(pKey, "");
			}
		}
	}
}
