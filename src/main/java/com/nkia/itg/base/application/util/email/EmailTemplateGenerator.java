/*
 * @(#)EmailTemplateGenerator.java              2014. 4. 10.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.email;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;

@Component("emailTemplateGenerator")
public class EmailTemplateGenerator {

	private final String CLASS_PATH = "classes";
	private final String TEMPLATE_PATH = "egovframework/template/mail";
	private final String PARAMETER_MARK = "$";
	private final String PARAMETER = "PARAMETER";
	private final String LOOP_START = "LOOP_START";
	private final String LOOP_END = "LOOP_END";
	private final String DATA_MAP_MARK = "@";
	private final String DATA_MAP = "DATA_MAP";
	
	public String generateContent(String templateName, Map param) throws NkiaException, IOException {		
		URL resourceUrl = EmailTemplateGenerator.class.getResource(EmailTemplateGenerator.class.getSimpleName() + ".class");
		String path = "";
		String filePath = "";
		if (resourceUrl != null) {
			path = resourceUrl.getFile();
			filePath = path.substring(0, path.indexOf(CLASS_PATH) + CLASS_PATH.length()) + File.separator + TEMPLATE_PATH + File.separator + templateName + ".html";
		} else {
			filePath = File.separator + TEMPLATE_PATH + File.separator + templateName + ".html";
		}
		
		FileInputStream inputStream = null;
		StringBuffer resultContent = new StringBuffer();
		
		try {
			inputStream = new FileInputStream(filePath);
			ReadableByteChannel source = Channels.newChannel(inputStream);
			Reader channelReader = Channels.newReader(source, "UTF-8");
			BufferedReader reader = new BufferedReader(channelReader);
			
			boolean readable = true;
			while(readable) {
				String strLine = reader.readLine();
				String lineInfo = "";
				if(strLine != null) {
					lineInfo = parseStrLine(strLine);
					if(lineInfo.indexOf(PARAMETER) != -1) {
						strLine = mappingParameterData(strLine, param);
						resultContent.append(strLine);
						resultContent.append("\n");
					}
					
					if(lineInfo.equals(LOOP_START)) {
						String dataListMapKey = reader.readLine();
						List dataList = null;
						if (dataListMapKey != null) {
							dataListMapKey = dataListMapKey.substring(1, dataListMapKey.length() -1);
							if(dataListMapKey != null){
								dataList = (List)param.get(dataListMapKey);
							}
						}							
						
						//Loop문 추출
						String loopStr = "";
						boolean tCondition = true;
						while(tCondition) {
							String loopStrLine = reader.readLine();
							if (loopStrLine != null){
								if(!"".equals(loopStrLine)) {
									String checkStr = parseStrLine(loopStrLine);
									if(checkStr.equals(LOOP_END)) {
										tCondition = false;
									} else {
										loopStr = loopStr + loopStrLine + "\n";
									}
								}
							}
						}
						
						//데이터리스트 치환작업
						String loopStrResult = "";
						for(int i = 0 ; i < dataList.size() ; i++) {
							String tempLoopStr = loopStr;
							if (!"".equals(tempLoopStr)) {
								Map dataMap = (Map)dataList.get(i);
								Iterator keySet = dataMap.keySet().iterator();
								while(keySet.hasNext()) {
									String key = (String)keySet.next();
									tempLoopStr = tempLoopStr.replaceAll("[$]"+ key +"[$]", StringUtil.replaceNull(String.valueOf(dataMap.get(key)),""));
								}
							}
							loopStrResult = loopStrResult + tempLoopStr;
						}
						resultContent.append(loopStrResult);
					}
				} else {
					readable = false;
				}
			}
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
					throw new NkiaException(e);
				}
			}
		}
		return resultContent.toString();	
	}
	
	public String customGenerateContent(String templateName, Map param) throws NkiaException, IOException {
		URL resourceUrl = EmailTemplateGenerator.class.getResource(EmailTemplateGenerator.class.getSimpleName() + ".class");
		String path = "";
		String filePath = "";
		if (resourceUrl != null) {
			path = resourceUrl.getFile();
			filePath = path.substring(0, path.indexOf(CLASS_PATH) + CLASS_PATH.length()) + File.separator + TEMPLATE_PATH + File.separator + templateName + ".html";
		} else {
			filePath = File.separator + TEMPLATE_PATH + File.separator + templateName + ".html";
		}
		
		String resultStr = "";
		BufferedReader in = new BufferedReader(new FileReader(filePath));
        String line = null;
        String result = "";
		try{
	        while ((line = in.readLine()) != null) {
	        	resultStr += line;
	        }
	        result = mappingParameterData(resultStr, param);
			
		}catch(Exception e){
			result = "error";
		}finally{
			in.close();
		}
		return result;

	}

	private String mappingParameterData(String strLine, Map param) {
		Iterator keySet = param.keySet().iterator();
		while(keySet.hasNext()) {
			String key = String.valueOf(keySet.next());
			if (key.equals("CONTENT")) {
				//StringUtil.changeLineAlignmentForHtml - <br> 처리
				strLine = strLine.replaceAll("[$]"+ key +"[$]", StringUtil.changeLineAlignmentForHtml(String.valueOf(param.get(key))));
			} else {
				strLine = strLine.replaceAll("[$]"+ key +"[$]", StringUtil.replaceNull(String.valueOf(param.get(key)),""));
			}
		}
		return strLine;
	}

	private String parseStrLine(String strLine) {
		String result = "";
		if(strLine.indexOf(PARAMETER_MARK) != -1) {
			result = PARAMETER;
		}
		if(strLine.indexOf(LOOP_START) != -1) {
			result = LOOP_START;
		}
		
		if(strLine.indexOf(LOOP_END) != -1) {
			result = LOOP_END;
		}
		
		if(strLine.indexOf(DATA_MAP_MARK) != -1) {
			result = DATA_MAP;
		};

		return result;
	}
	
}
