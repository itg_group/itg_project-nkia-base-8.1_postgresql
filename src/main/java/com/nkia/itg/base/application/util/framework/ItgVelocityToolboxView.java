/*
 * @(#)ItgVelocityToolboxView.java              2013. 1. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.framework;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.context.Context;
import org.apache.velocity.tools.Scope;
import org.apache.velocity.tools.ToolboxFactory;
import org.apache.velocity.tools.config.XmlFactoryConfiguration;
import org.apache.velocity.tools.view.ViewToolContext;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.servlet.view.velocity.VelocityToolboxView;

import com.nkia.itg.base.application.exception.NkiaException;

public class ItgVelocityToolboxView extends VelocityToolboxView {

	protected Context createVelocityContext(Map model, HttpServletRequest request, HttpServletResponse response) throws NkiaException, IllegalStateException, IOException {
		// Create a ViewToolContext instance since ChainedContext is deprecated
		// in Velocity Tools 2.0.
		
		ViewToolContext velocityContext = new ViewToolContext(
				getVelocityEngine(), request, response, getServletContext());
		velocityContext.putAll(model);

		// Load a Configuration and publish toolboxes to the context when
		// necessary
		if (getToolboxConfigLocation() != null) {
			XmlFactoryConfiguration cfg = new XmlFactoryConfiguration();
			cfg.read(new ServletContextResource(getServletContext(), getToolboxConfigLocation()).getURL());
			ToolboxFactory factory = cfg.createFactory();

			velocityContext.addToolbox(factory.createToolbox(Scope.APPLICATION));
			velocityContext.addToolbox(factory.createToolbox(Scope.REQUEST));
			velocityContext.addToolbox(factory.createToolbox(Scope.SESSION));
		}
		return velocityContext;

	}

}
