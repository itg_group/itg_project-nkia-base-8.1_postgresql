package com.nkia.itg.base.application.util;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.file.FileUtils;

public class MsgLoader {
	
	private static final Logger logger = LoggerFactory.getLogger(MsgLoader.class);
	
	public HashMap aHashMap = new HashMap();
    
	public MsgLoader(String path) {
		load(path);
    }  
	
    public void load(String path) {
    	
    	try {
    		
			String content = FileUtils.getFileContents(path);
			if(content != null) {
				
				String [] fileLines = content.split("\\r\\n");							
				
				for(int i=0; i < fileLines.length ;i++) {
					
					if(fileLines[i].indexOf("=")>-1) {						
					
						String [] lineFields = fileLines[i].split("=");  
						if(lineFields.length < 1) {
							continue;
						}
						
						String headerTemp = lineFields[0].trim();
						if( headerTemp.substring(0, 1).equals("#") ) {
						
						} else 
							aHashMap.put(lineFields[0].trim(), lineFields[1].trim());
					}
					           
				}	
			}
		
		} catch (Exception e) {
			new NkiaException(e);
		}		
	
    }
    
}
