package com.nkia.itg.base.application.util.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public class FileUtils {

	/**
	 * Moves the file to the new file name.
	 * 
	 * @param oldFile
	 *            the file to be moved
	 * @param newFile
	 *            the new file. If the new file is an existing directory then
	 *            the file will be moved into it and keep the same file name
	 * 
	 * @throws RuntimeException
	 *             if the source file does not exist or is a directory or if the
	 *             destination file exists
	 */
	public static void moveFile(File oldFile, File newFile)throws NkiaException {
		if (!oldFile.exists())
			throw new RuntimeException("File not found [" + oldFile.getName() + "]");
			
		

		if (oldFile.isDirectory())
			throw new RuntimeException("File is a directory");

		if (newFile.exists()) {
			if (newFile.isDirectory()) {
				newFile = new File(newFile.getPath() + File.separator + oldFile.getName());

				if (newFile.exists())
					throw new RuntimeException("New file name [" + newFile.getPath() + "] exists. Cannot move file");
			} else {
				throw new RuntimeException("New file name [" + newFile.getPath() + "] exists. Cannot move file");
			}
		}

		oldFile.renameTo(newFile);
	}

	/**
	 * Creates a directory based on the supplied directory name. Note that any
	 * parent directories will also be created as needed.
	 * 
	 * @param dirName
	 *            the path to the new directory
	 * 
	 * @throws RuntimeException
	 *             if the path points to an existing file or there was a problem
	 *             creatinmg the directory
	 */
	public static void mkdir(String dirName) {
		File d = new File(dirName);
		
		d.setExecutable(false, true);
		d.setReadable(true);
		d.setWritable(false, true);

		if (d.exists()) {
			if (d.isDirectory()) {
				return;
			} else {
				throw new RuntimeException("Path [" + d.getPath() + "] exists and is a file");
			}
		}

		if (!d.mkdirs())
			throw new RuntimeException("Failed to create directory [" + d.getPath() + "]");

	}

	/**
	 * Creates a temporary file called fooXXXXbar (where XXXX is replaced by a
	 * system generated number) in the specified directory. It will be
	 * automatically deleted once the app exits.
	 * 
	 * @param directory
	 *            the directory to create the temp file in
	 * 
	 * @return reference to the file
	 * 
	 * @throws IOException
	 *             if there was a problem creating the file
	 */
	public static File createTempFile(File directory) throws IOException {
		File f = File.createTempFile("foo", "bar", directory);
		f.deleteOnExit();

		return f;
	}

	/**
	 * Creates a temporary file in the local system tmp directory (or the
	 * current working directory if this doesn't exist)
	 * 
	 * @return reference to the file
	 * 
	 * @throws IOException
	 *             if there was a problem creating the file
	 */
	public static File createTempFile() throws IOException {
		File dir = getTmpDir();
		if (dir == null)
			dir = new File(".");

		return createTempFile(dir);
	}

	/**
	 * Creates a temporary file in the local system tmp directory (or the
	 * current working directory if this doesn't exist) and sets its contents to
	 * be those supplied.
	 * 
	 * @param content
	 *            the text to write into the file
	 * 
	 * @return reference to the file
	 * 
	 * @throws IOException
	 *             if there was a problem creating the file
	 */
	public static File createTempFile(String content) throws IOException {
		File dir = getTmpDir();
		if (dir == null)
			dir = new File(".");

		return createTempFile(dir, content);
	}

	/**
	 * Creates a temporary file in the specified directory. If the directory is
	 * null then the default temporary-file directory is used.
	 * 
	 * @param directory
	 *            the directory to create the temp file in
	 * @param content
	 *            the text to wrwite into the file
	 * 
	 * @return reference to the file
	 * 
	 * @throws IOException
	 *             if there was a problem creating the file
	 */
	public static File createTempFile(File directory, String content) throws IOException {
		File f = createTempFile(directory);

		FileWriter out = new FileWriter(f);
		out.write(content);
		out.flush();
		out.close();

		return f;
	}

	/**
	 * Creates a temporary directory called "fooXXXXXbar" (where XXXX is
	 * replaced by a system generated number) which will be automatically
	 * deleted once the test exits.
	 * 
	 * @return reference to the directory
	 * 
	 * @throws IOException
	 *             if the file could not be created
	 */
	public static File createTempDir() throws IOException {
		File f = createTempFile();
		
		f.setExecutable(false, true);
		f.setReadable(true);
		f.setWritable(false, true);
		
		String uniqueName = f.getName();
		f.delete();

		FileUtils.mkdir(uniqueName);
		File d = new File(uniqueName);
		d.deleteOnExit();

		return d;
	}

	/**
	 * Uses the "java.io.tmpdir" system property to get a reference to the local
	 * temporary directory
	 * 
	 * @return a reference to the directory or null if not defined or does not
	 *         exist or is not a directory or is read-only
	 */
	public static File getTmpDir() {
		String tmpDir = System.getProperty("java.io.tmpdir");
		if (tmpDir == null)
			return null;

		File f = new File(tmpDir);
		if (f.exists() && f.isDirectory() && f.canWrite())
			return f;

		return null;
	}

	/**
	 * Get the file name extension from the supplied file. Note this is defined
	 * as the part of the name after the last "." character. Also, this will be
	 * converted to lowercase.
	 * 
	 * @param f
	 *            the file to process
	 * 
	 * @return the file extention of the supplied file or null if none present
	 */
	public static String getExtension(File f) {
		String ext = null;

		String s = f.getName();
		int i = s.lastIndexOf('.');

		if (i > 0 && i < s.length() - 1)
			ext = s.substring(i + 1).toLowerCase();

		return ext;
	}

	/**
	 * Returns an URL to the supplied file. Will search the classpath if
	 * required.
	 * 
	 * @param path
	 *            the path to convert
	 * 
	 * @return a URL pointing to the file represented by the supplied path
	 *         string or null if it does not exist
	 */
	public static URL toURL(String path) {
		URL url;
		try {
			/*
			 * Let java do the work instead of us... if ( path.indexOf(":") ==
			 * -1 ) url = new URL("file:" + path); else url = new URL(path);
			 */
			// todo: the File() constructor will not fail so we will not use the
			// Classloasder
			url = new File(path).toURL();
		} catch (MalformedURLException e) {
			url = ClassLoader.getSystemResource(path);
		}

		return url;
	}

	/**
	 * Uses the toURL() call to convert the path and as a result can find files
	 * on the classpath.
	 * 
	 * @param path
	 *            the path to the file
	 * 
	 * @return the contents of the file referenced to by the supplied path
	 * 
	 * @throws IOException
	 *             if the file doe not exist
	 */
	public static String getFileContents(String path) throws IOException {
		URL url = toURL(path);
		if (url == null)
			throw new IOException("File not found: " + path);

		return getFileContents(new FileInputStream(url.getPath()));
	}

	/**
	 * @param file
	 *            reference to the file
	 * 
	 * @return the contents of the file referenced to by the supplied file and
	 *         returns its contents as a string
	 * 
	 * @throws IOException
	 *             if the file doe not exist
	 */
	public static String getFileContents(File file) throws IOException {
		if (file == null)
			throw new IOException("Null file passed");

		if (!file.exists())
			throw new IOException("File not found: " + file.getPath());

		return getFileContents(new FileInputStream(file));
	}

	/**
	 * @param fstream
	 *            the stream used to access the file contents
	 * 
	 * @return the contents of the file referenced by the input stream supplied.
	 *         The file is processed in 1K chunks.
	 * 
	 * @throws IOException
	 *             if the file doe not exist
	 */
	public static String getFileContents(InputStream fstream) throws IOException {
		InputStreamReader in = null;
		StringWriter writer = null;
		String returnValue = "";
		try {
			in = new InputStreamReader(fstream);

			writer = new StringWriter();
			char[] buf = new char[1024];
			int count;

			while ((count = in.read(buf)) != -1)
				writer.write(buf, 0, count);
		} catch (IOException e) {
			throw new IOException(e);
		} catch (Exception e) {
			new NkiaException(e);
		} finally {
			if(in != null){
				in.close();
			}
			if(fstream != null){
				fstream.close();			
			}
		}
		if(writer != null){
			returnValue = writer.toString();
		}
		return returnValue;
	}

	/**
	 * writes the supplied contents tot he supplied file
	 * 
	 * @param f
	 *            the file to be updated
	 * @param contents
	 *            the text to write into the file
	 * @throws NkiaException 
	 * @throws IOException 
	 */
	public static void writeToFile(File f, String contents) throws NkiaException, IOException {
		FileWriter fsStream = null;

		try {
			fsStream = new FileWriter(f);

			BufferedWriter out = new BufferedWriter(fsStream);
			out.write(contents);
			out.close();
		} catch (Exception e) {
			throw new NkiaException(e);
			//throw new RuntimeException("Error writing to file [" + f.getPath() + "}: " + e.toString(), null);
		} finally {
			if (fsStream != null)
				fsStream.close();
		}
	}

	public static void copy(File fOrg, File fTarget) throws IOException {
		FileInputStream io = null;
		FileOutputStream out = null;
		
		try {
			io = new FileInputStream(fOrg);
			
			if (!fTarget.isFile()) {
				File fParent = new File(fTarget.getParent());
				
				fParent.setExecutable(false, true);
				fParent.setReadable(true);
				fParent.setWritable(false, true);
				
				if (!fParent.exists()) {
					fParent.mkdir();
				}
				fTarget.createNewFile();
			}
	
			out = new FileOutputStream(fTarget);
	
			byte[] bBuffer = new byte[1024 * 8];
	
			int nRead;
			while ((nRead = io.read(bBuffer)) != -1) {
				out.write(bBuffer, 0, nRead);
			}
		} finally {
			if (io != null) {
				io.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}
	
	public static File writeFileFromBlob(String fileName, Blob blobData) throws NkiaException, IOException, SQLException {
		File blobFile = null;
    	FileOutputStream outStream = null; 
    	InputStream inStream = null;
    	
		try {
	    	blobFile = new File(fileName); 
	    	outStream = new FileOutputStream(blobFile); 
	    	inStream = blobData.getBinaryStream();
	    	
	    	int length = -1; 
	    	int size = 1024;
	    	byte[] buffer  = new byte[size]; 
	    	while ((length = inStream.read(buffer)) != -1) {
	    		outStream.write(buffer, 0, length); 
	    		outStream.flush(); 
	    	}
		} finally {
			if (inStream != null) {
				inStream.close();
			}
			if (outStream != null) {
				outStream.close();
			}
		}
    	
    	return blobFile;
	}
	
	public static File writeFileFromByte(String fileName, byte[] byteData) throws IOException {
		File blobFile = null;
		FileOutputStream outStream = null;
		try {
			blobFile = new File(fileName); 
	    	outStream = new FileOutputStream(blobFile);
	    	outStream.write(byteData, 0, byteData.length); 
	    	outStream.flush();
		} finally {
			if (outStream != null) {
				outStream.close();
			}
		}
    	return blobFile;
	}
	
	public static String checkEncording(File checkFile) throws IOException {
		FileInputStream fis = null;
		String result = "";
		
		try {
			fis = new FileInputStream(checkFile);
	        
	    	// 2. 파일 읽기 (4Byte)
	    	byte[] BOM = new byte[4];
	    	fis.read(BOM, 0, 4);
	    	
	    	// 3. 파일 인코딩 확인하기
	    	if( (BOM[0] & 0xFF) == 0xEF && (BOM[1] & 0xFF) == 0xBB && (BOM[2] & 0xFF) == 0xBF )
	    		result = "UTF-8";
	    	else if( (BOM[0] & 0xFF) == 0xFE && (BOM[1] & 0xFF) == 0xFF )
	    		result =  "UTF-16BE";
	    	else if( (BOM[0] & 0xFF) == 0xFF && (BOM[1] & 0xFF) == 0xFE )
	    		result = "UTF-16LE";
	    	else if( (BOM[0] & 0xFF) == 0x00 && (BOM[1] & 0xFF) == 0x00 && 
	    	         (BOM[0] & 0xFF) == 0xFE && (BOM[1] & 0xFF) == 0xFF )
	    		result = "UTF-32BE";
	    	else if( (BOM[0] & 0xFF) == 0xFF && (BOM[1] & 0xFF) == 0xFE && 
	    	         (BOM[0] & 0xFF) == 0x00 && (BOM[1] & 0xFF) == 0x00 )
	    		result = "UTF-32LE";
	    	else
	    		result = "EUC-KR";
		} finally {
			if (fis != null) {
				fis.close();
			}
		}
    	return result;
	}
	
	public static void convertEncoding(File file, String encoding) throws NkiaException, IOException {
		BufferedReader in = null;
		BufferedWriter out = null;
		
		try {
			in = new BufferedReader(new FileReader(file));
			String read = null;
			List list = new ArrayList();
			while ((read = in.readLine()) != null) {
				list.add(read);
			}
			
			
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoding));
			for (int i = 0, j = list.size(); i < j; i++) {
				out.write((String) list.get(i));
				out.newLine();
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
    }
	
	/**
	 * 
	 * 파일읽기 => String 전체붙이기.
	 * 
	 * @param fileFullPath
	 * @return
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public static String readFileToString(String fileFullPath)throws NkiaException, IOException{
		StringBuffer readDataSb = new StringBuffer();
		BufferedReader bufferReader = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileFullPath);
			bufferReader = new BufferedReader(new InputStreamReader( fis , "UTF-8" ));
			String data = "";
			readDataSb = new StringBuffer();
			while( (data = bufferReader.readLine()) != null){
				readDataSb.append(data);
			}
			bufferReader.close();
			fis.close();
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if( bufferReader != null ){ bufferReader.close(); }
			if( fis != null ){ fis.close(); }
		}
		readDataSb.toString();
		return readDataSb.toString();
	}
	
	/**
	 * 
	 * 파일복사
	 * 
	 * @param inFileName
	 * @param outFileName
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public static void copyFile(String inFileName, String outFileName)throws NkiaException, IOException {
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		try {
			inputStream = new FileInputStream(inFileName);
			outputStream = new FileOutputStream(outFileName);

			outputStream.close();
			inputStream.close();
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		}
	}
	
	/**
	 * 
	 * 파일복사
	 * 
	 * @param inFileName
	 * @param outFileName
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public static void copyFile(File inFileName, String outFileName)throws NkiaException, IOException {
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		try {
			inputStream = new FileInputStream(inFileName);
			outputStream = new FileOutputStream(outFileName);
			int bytesRead = 0;
			byte[] buffer = new byte[1024];
			while((bytesRead = inputStream.read(buffer, 0, 1024)) != -1 ){
				outputStream.write(buffer, 0, bytesRead);
			}
			outputStream.close();
			inputStream.close();
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (outputStream != null) { outputStream.close(); }
			if (inputStream != null) { inputStream.close(); }
		}
	}
	
	/**
	 * 
	 * 파일이동
	 * 
	 * @param inFileName
	 * @param outFileName
	 * @throws NkiaException
	 */
	public static void moveFile(String inFileName, String outFileName)throws NkiaException,IOException {
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		File inFile = new File(inFileName);
		try {
			if( inFile.isFile() ){
				inputStream = new FileInputStream(inFile);
				outputStream = new FileOutputStream(outFileName);

				int bytesRead = 0;
				byte[] buffer = new byte[1024];
				while((bytesRead = inputStream.read(buffer, 0, 1024)) != -1 ){
					outputStream.write(buffer, 0, bytesRead);
				}
				
				outputStream.close();
				inputStream.close();
				
				//복사한뒤 원본파일을 삭제함
				deleteFile(inFileName);
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		}
		
	}
	
	/**
	 * 
	 * 파일이동
	 * 
	 * @param inFileName
	 * @param outFileName
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public static void moveFile(File inFileName, String outFileName)throws NkiaException, IOException {
		
		// 파일 디렉토리 생성
		File outFile = new File(outFileName);
		if (!outFile.isFile()) {
			File fParent = new File(outFile.getParent());

			fParent.setExecutable(false,true);
			fParent.setReadable(true);
			fParent.setWritable(false,true);

			if (!fParent.exists()) {
				fParent.mkdirs();
			}
		}
		
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		try {
			inputStream = new FileInputStream(inFileName);
			outputStream = new FileOutputStream(outFileName);
			int bytesRead = 0;
			byte[] buffer = new byte[1024];
			while((bytesRead = inputStream.read(buffer, 0, 1024)) != -1 ){
				outputStream.write(buffer, 0, bytesRead);
			}
			outputStream.close();
			inputStream.close();
			
			//복사한뒤 원본파일을 삭제함
			deleteFile(inFileName.getAbsolutePath());
			
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (outputStream != null) { outputStream.close(); }
			if (inputStream != null) { inputStream.close(); }
		}
	}
	
	/**
	 *
	 * 폴더전체 Copy
	 *
	 * @param inFolder
	 * @param outFolder
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public static void copyFolder(File inFolder, File outFolder, String[] excludeFolder)throws NkiaException, IOException {

		boolean isCopyDir = true;

		if( excludeFolder != null && excludeFolder.length > 0 ){
			for( int i = 0; i < excludeFolder.length; i++ ){
				if( inFolder.getName().equalsIgnoreCase(excludeFolder[i]) ){
					isCopyDir = false;
				}
			}
		}

		if( isCopyDir ){
			if( inFolder.isDirectory() ){

				outFolder.setExecutable(false,true);
				outFolder.setReadable(true);
				outFolder.setWritable(false,true);

				if( !outFolder.exists() ){
					outFolder.mkdirs();
				}

				File[] childFiles = inFolder.listFiles();
				for( int i = 0 ; childFiles != null && i < childFiles.length; i++ ){
					copyFolder(childFiles[i], new File(outFolder, childFiles[i].getName()), excludeFolder);
				}
			}else{
				FileInputStream fis = null;
				FileOutputStream fos = null;
				try {
					fis = new FileInputStream(inFolder);
					fos = new FileOutputStream(outFolder) ;
					byte[] b = new byte[4096];
					int cnt = 0;
					while((cnt=fis.read(b)) != -1){
						fos.write(b, 0, cnt);
					}
				} catch (Exception e) {
					throw new NkiaException(e);
				} finally{
					fis.close();
					fos.close();
				}

			}
		}
	}
	
	/**
	 * 
	 * 폴더 이동
	 * 
	 * @param inFileName
	 * @param outFileName
	 * @throws NkiaException
	 */
	public static void moveFolder(String inFolderName, String outFolderName)throws NkiaException {
		File inFolder = new File(inFolderName); 
		File outFolder = new File(outFolderName); 
 		if(inFolder.exists() && inFolder.isDirectory()) {
			inFolder.renameTo(outFolder);	
		}
	}
	
	/**
	 * 
	 * 파일삭제
	 * 
	 * @param filePath
	 * @param folderName
	 * @throws NkiaException
	 */
	public static synchronized void deleteFile(String filePath)throws NkiaException{
		File file = new File(filePath);
		if (file.exists() && !file.isDirectory() ) {
			file.delete();
		}
	}
	
	/**
	 * 
	 * 폴더삭제
	 * 
	 * @param filePath
	 * @param folderName
	 * @throws NkiaException
	 */
	public static synchronized boolean deleteFolder(String folderFullPath)throws NkiaException{
		File folder = new File(folderFullPath);
		  
		if(folder.exists() && folder.isDirectory()) {
			File[] allFiles = folder.listFiles();
			if (allFiles != null) {
				for(File delAllDir : allFiles) {
					deleteFolder(delAllDir.getAbsolutePath());
				}
			}
		}
		return folder.delete();
	}
	
	/**
	 * 
	 * 파일체크
	 * 
	 * @param fileFullPath
	 * @return
	 * @throws NkiaException
	 */
	public static boolean checkFile(String fileFullPath)throws NkiaException{
		String src = fileFullPath;
		File file = new File(src);
		return file.isFile();
	}
}
