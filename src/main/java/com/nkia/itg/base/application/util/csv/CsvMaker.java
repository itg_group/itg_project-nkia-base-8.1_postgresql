package com.nkia.itg.base.application.util.csv;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.ExcelFileService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
import au.com.bytecode.opencsv.CSVWriter;

@Service("csvMaker")
public class CsvMaker {
	
	@Resource(name = "excelFileService")
	private ExcelFileService excelFileService;
	
	// 2016.02.03. 정정윤
	// CSV 파일을 생성하기 위한 Util 예제.
	// 추후 컴포넌트 화 예정입니다. 
	
	// 에러 시 :
	// CVS - pom.xml 업데이트 -> offline 체크 해제 후 -> update maven
	
//	public ModelMap makeCsvFile(Map params, List<HashMap> dataList) throws NkiaException {
//		
//		/**
//         * csv 파일을 쓰기위한 설정
//         * 설명
//         * D:\\test.csv : csv 파일저장할 위치+파일명
//         * EUC-KR : 한글깨짐설정을 방지하기위한 인코딩설정(UTF-8로 지정해줄경우 한글깨짐)
//         * ',' : 배열을 나눌 문자열
//         * '"' : 값을 감싸주기위한 문자
//         **/
//		
//	    ModelMap mapResult = new ModelMap();
//		boolean isSucess = false;
//		//Excel파일을 저장하기 위한 기본 설정
//		String fileName = (String)params.get("fileName");
//		String realFileName = fileName+"_"+System.currentTimeMillis()+".csv";
//		String fullPath = null;
//		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
//		
//		File directory = new File(tempExcelUploadPath);
//
//		directory.setExecutable(false,true);
//		directory.setReadable(true);
//    	directory.setWritable(false,true); 
//
//		if(!directory.exists()){
//			directory.mkdirs();
//		}
//
//		fullPath = tempExcelUploadPath+File.separator+realFileName;
//		String columns = (String)params.get("includeColumns");
//		
//		String[] dataCols = columns.split(",");
//		String[] emptyCols = new String[] {"No data."};
//		
//		try {
//			CSVWriter cw = new CSVWriter(new OutputStreamWriter(new FileOutputStream(fullPath), "EUC-KR"),',', '"');
//			try {
//				if(dataList != null && dataList.size() > 0) {
//					
//					// 먼저 최상단에 헤더부터 write
//					cw.writeNext(dataCols);
//					 
//					for(int i=0; i<dataList.size(); i++) {
//						
//						HashMap rowData = new HashMap();
//						rowData = dataList.get(i);
//						
//						String[] newRowData = new String[dataCols.length];
//						
//						for(int j=0; j<dataCols.length; j++) {
//							newRowData[j] = String.valueOf(rowData.get(dataCols[j]));
//						}
//				        cw.writeNext(newRowData);
//					}
//					
//					
//				} else {
//					cw.writeNext(emptyCols);
//				}
//				mapResult.put("file_id", insertAndGetFileId( fileName, realFileName, tempExcelUploadPath ) );
//				isSucess = true;
//			} catch (Exception e) {
//				throw new NkiaException(e);
//			} finally {
//				cw.close();
//			}   
//		} catch (Exception e) {
//			throw new NkiaException(e);
//		}
//		return mapResult;
//	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String insertAndGetFileId( String fileName, String realFileName, String tempUploadPath ) throws NkiaException{
		String excel_file_id = null;
		try {
			excel_file_id = excelFileService.selectMaxHistoryId();
			
			Map history_param = new HashMap();
			history_param.put("excel_file_id", excel_file_id);
			history_param.put("filename", fileName);
			history_param.put("real_filename", realFileName);
			history_param.put("temp_upload_path", tempUploadPath);
			history_param.put("file_extsn", "csv");
			history_param.put("down_user_id", EgovUserDetailsHelper.getSesssionUserId());
			
			// Insert
			excelFileService.insertExcelFile(history_param);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return excel_file_id;
	}

}
