package com.nkia.itg.base.application.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.nkia.itg.base.application.constraint.BaseEnum;

/**
 * Nkia Exception Resolver : Exception 발생에 대한 후 처리(페이지 포워드)를 담당한다.
 * @author nkia
 *
 */
public class NkiaSimpleMappingExceptionResolver extends SimpleMappingExceptionResolver {

	@Override
	protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		String accept = request.getHeader("Accept").toLowerCase();
		String viewName = determineViewName(ex, request);
		Integer statusCode = determineStatusCode(request, viewName);
		if( statusCode != null ){
			applyStatusCodeIfPossible(request, response, statusCode);
		}
		ModelAndView mv = null;
		if (accept.indexOf("json") > -1) {
			// json 요청(Ajax)인 경우에 에러코드 500으로 포워드.
			mv = new ModelAndView(viewName);
			mv.addObject("errorCode", statusCode);
			return mv;
		} else {
			// 일반요청(submit)인 경우에는 에러페이지로 직접포워드.
			if( statusCode == 400 ){
				mv = new ModelAndView(BaseEnum.ERROR_PAGE_400.getString());
			}else if( statusCode == 401 ){
				mv = new ModelAndView(BaseEnum.ERROR_PAGE_401.getString());
			}else if( statusCode == 403 ){
				mv = new ModelAndView(BaseEnum.ERROR_PAGE_403.getString());
			}else if( statusCode == 404 ){
				mv = new ModelAndView(BaseEnum.ERROR_PAGE_404.getString());
			}else if( statusCode == 500 ){
				mv = new ModelAndView(BaseEnum.ERROR_PAGE_500.getString());
			}else{
				mv = new ModelAndView(BaseEnum.ERROR_PAGE_500.getString());
			}
			mv.addObject("errorCode", statusCode);
		}   
		return mv;
	}
	
}
