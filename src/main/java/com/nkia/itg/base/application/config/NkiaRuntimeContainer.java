/*
 * @(#)NkiaReloadableContainer.java              2015. 5. 21.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.config;

import java.util.HashMap;

/**
 * 
 * @version 1.0 2015. 5. 21.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 *  NKIA Runtime Container
 * </pre>
 *
 */
public class NkiaRuntimeContainer {
	
	// Reloadable Container - HashMap으로 선언하여 SqlMap Reload나 Message Resource에 이용됨
	public HashMap<String, Object> reloadableContainerMap;
	
	public void setReloadableContainerMap(HashMap<String, Object> reloadableContainerMap){
		this.reloadableContainerMap = reloadableContainerMap;
	}
	
	public HashMap<String, Object> getReloadableContainerMap(){
		HashMap<String, Object> reloadableContainerMapReturn;
		reloadableContainerMapReturn = reloadableContainerMap;
		return reloadableContainerMapReturn;
	}
	
	public void putReloadableContainerMap(String key, Object value){
		if( reloadableContainerMap != null ){
			reloadableContainerMap.put(key, value);
		}
	}
	
	public Object getReloadableContainerMapObject(String key){
		HashMap<String, Object> reloadableContainerMapReturn;
		reloadableContainerMapReturn = reloadableContainerMap;
		if( reloadableContainerMapReturn != null ){
			return reloadableContainerMapReturn.get(key);
		}
		return null;
	}
}
