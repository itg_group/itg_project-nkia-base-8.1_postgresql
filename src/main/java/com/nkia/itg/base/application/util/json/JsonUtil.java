package com.nkia.itg.base.application.util.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.vo.CheckTreeVO;
import com.nkia.itg.base.vo.TreeVO;
import com.nkia.itg.itam.oper.vo.OperTreeGridVO;

@Component("jsonUtil")
public class JsonUtil {
	
	//1행 데이터에 대한 util(for[i][j])
	public static String changeListToJsonOneWidth(String rowName, List targetList) {
		StringBuffer json_data = new StringBuffer();
		json_data.append("{");
		json_data.append("\""+rowName+"\":");
		json_data.append("[");
		json_data.append("[");
		for(int i = 0 ; i < targetList.size() ; i++) {
			HashMap dataRow = (HashMap)targetList.get(i);
			Iterator iter_key = dataRow.keySet().iterator();
			json_data.append("{");
			while(iter_key.hasNext()) {
				String attr_name = (String)iter_key.next();
				String attr_value = (String)dataRow.get(attr_name);

				json_data.append("\"");
				json_data.append(attr_name.toLowerCase());
				json_data.append("\"");
				json_data.append(":");
				json_data.append("\"");
				json_data.append(attr_value);
				json_data.append("\"");
				if(iter_key.hasNext()) {
					json_data.append("}");
					json_data.append(",");
					json_data.append("{");
				}
				
			}
			json_data.append("}");
			if(i != targetList.size() - 1) {
				json_data.append(",");	
			}
		}
		json_data.append("]");
		json_data.append("]");
		json_data.append("}");
		return json_data.toString();
	}
	
	//1열 데이터에 대한 util(for[i])
	public static String changeListToJsonOneHeight(String rowName, List targetList) {
		StringBuffer json_data = new StringBuffer();
		json_data.append("{");
		json_data.append("\""+rowName+"\":");
		json_data.append("[");
		for(int i = 0 ; i < targetList.size() ; i++) {
			HashMap dataRow = (HashMap)targetList.get(i);
			Iterator iter_key = dataRow.keySet().iterator();
			json_data.append("{");
			while(iter_key.hasNext()) {
				String attr_name = (String)iter_key.next();
				String attr_value = (String)dataRow.get(attr_name);

				json_data.append("\"");
				json_data.append(attr_name.toLowerCase());
				json_data.append("\"");
				json_data.append(":");
				json_data.append("\"");
				json_data.append(attr_value);
				json_data.append("\"");
				if(iter_key.hasNext()) {
					json_data.append("}");
					json_data.append(",");
					json_data.append("{");
				}
				
			}
			json_data.append("}");
			if(i != targetList.size() - 1) {
				json_data.append(",");	
			}
		}
		json_data.append("]");
		json_data.append("}");
		return json_data.toString();
	}
	
	//i열 데이터에 대한 util(for[i][j])
	public static String changeListToJsonMix(String rowName, List targetList, int lineNum) {
		StringBuffer json_data = new StringBuffer();
		json_data.append("{");
		json_data.append("\""+rowName+"\":");
		json_data.append("[");
		
		for(int i = 0 ; i < targetList.size() ; i++) {
			if(i == lineNum){
				json_data.append("[");
				lineNum = lineNum*2;
			}else{
				json_data.append("[");
			}
			
			HashMap dataRow = (HashMap)targetList.get(i);
			Iterator iter_key = dataRow.keySet().iterator();
			json_data.append("{");
			while(iter_key.hasNext()) {
				String attr_name = (String)iter_key.next();
				String attr_value = (String)dataRow.get(attr_name);

				json_data.append("\"");
				json_data.append(attr_name.toLowerCase());
				json_data.append("\"");
				json_data.append(":");
				json_data.append("\"");
				json_data.append(attr_value);
				json_data.append("\"");
				if(iter_key.hasNext()) {
					json_data.append("}");
					json_data.append(",");
					json_data.append("{");
				}
				
			}
			json_data.append("}");
			
			if(i == lineNum){
				json_data.append("]");
				json_data.append(",");
			}else if(i != lineNum && i != targetList.size()){
				json_data.append("]");
			}
			
			if(i != targetList.size() - 1) {
				json_data.append(",");	
			}
		}
		
		json_data.append("]");
		json_data.append("}");
		return json_data.toString();
	}
	
	public static String changeListToJson(String rowName, List targetList, HashMap headerMap) {
		StringBuffer json_data = new StringBuffer();
		json_data.append("{");

		if(headerMap != null) {
			Iterator header_key = headerMap.keySet().iterator();			
			while(header_key.hasNext()) {
				String attr_name = (String)header_key.next();
				String attr_value = (String)headerMap.get(attr_name);
				json_data.append("\"");
				json_data.append(attr_name);
				json_data.append("\"");
				json_data.append(":");
				json_data.append("\"");
				json_data.append(attr_value);
				json_data.append("\"");
				json_data.append(",");					
			}
		}
		
		json_data.append("\""+rowName+"\":");
		json_data.append("[");
		for(int i = 0 ; i < targetList.size() ; i++) {
			HashMap dataRow = (HashMap)targetList.get(i);
			Iterator iter_key = dataRow.keySet().iterator();
			json_data.append("{");
			while(iter_key.hasNext()) {
				String attr_name = (String)iter_key.next();
				String attr_value = (String)dataRow.get(attr_name);
				json_data.append("\"");
				json_data.append(attr_name);
				json_data.append("\"");
				json_data.append(":");
				json_data.append("\"");
				json_data.append(attr_value);
				json_data.append("\"");
				if(iter_key.hasNext()) {
					json_data.append(",");					
				}
			}
			json_data.append("}");
			if(i != targetList.size() - 1) {
				json_data.append(",");	
			}
		}
		json_data.append("]");
		json_data.append("}");
		return json_data.toString();
	}

	public static TreeVO changeTreeVOToJson(List<TreeVO> resultList, String up_node_id, boolean dynamic_flag, Integer expand_level)  {
		TreeVO rootTreeVO = new TreeVO();
		if(expand_level == null) {
			expand_level = 0;
		}
		if(resultList != null && resultList.size() > 0) {
			
			TreeVO firstTreeVO = resultList.get(0);
			rootTreeVO.setId(firstTreeVO.getUp_node_id());
			rootTreeVO.setNode_id(firstTreeVO.getUp_node_id());
			//rootTreeVO.setExpanded(true);
			rootTreeVO.setChildren(new ArrayList<TreeVO>());
			//rootTreeVO.getChildren().add(firstTreeVO);
			
			for(int i = 0 ; i < resultList.size() ; i++) {
				
				TreeVO treeVO = (TreeVO)resultList.get(i);
				if(!treeVO.isLeaf() && treeVO.getChildren() == null){
					treeVO.setChildren(new ArrayList());
				}
				
				getParentNode(rootTreeVO, treeVO, dynamic_flag, expand_level);
			}
		}
		return rootTreeVO;
	}
	
	public static TreeVO changeTreeMapToJson(List resultList, String up_node_id, boolean dynamic_flag, Integer expand_level)  {
		TreeVO rootTreeVO = new TreeVO();
		if(expand_level == null) {
			expand_level = 0;
		}
		if(resultList != null && resultList.size() > 0) {
			
			Map firstTreeMap = (Map)resultList.get(0);
			rootTreeVO.setId((String)firstTreeMap.get("UP_NODE_ID"));
			rootTreeVO.setNode_id((String)firstTreeMap.get("UP_NODE_ID"));
			//rootTreeVO.setExpanded(true);
			rootTreeVO.setChildren(new ArrayList<TreeVO>());
			//rootTreeVO.getChildren().add(firstTreeVO);
			
			rootTreeVO.setOtherData(getOtherData((HashMap)firstTreeMap));
			
			if( firstTreeMap.containsKey("CLS") ){
				rootTreeVO.setCls((String)firstTreeMap.get("CLS"));
			}
			
			rootTreeVO.setChildren(new ArrayList());  
			
			
			
			 
			
			for(int i = 0 ; i < resultList.size() ; i++) {
				getMapParentNode(rootTreeVO, (HashMap)resultList.get(i), dynamic_flag, expand_level);
			}
		}
		return rootTreeVO;
	}

	public static void getParentNode(TreeVO upTreeVO, TreeVO treeVO, boolean dynamic_flag, int expand_level)  {
		if(upTreeVO.getNode_id().equals(treeVO.getUp_node_id())) {
			if(upTreeVO.getChildren() == null) {
				upTreeVO.setChildren(new ArrayList<TreeVO>());
			}
			if( expand_level > Integer.parseInt(treeVO.getNode_level()) && !(treeVO.isLeaf())) {
				treeVO.setExpanded(true);
			}
			upTreeVO.getChildren().add(treeVO);
		} else {
			if(!dynamic_flag) {
				if(upTreeVO.getChildren() == null) {
					upTreeVO.setChildren(new ArrayList<TreeVO>());
				}
			}
			for(int i = 0 ; upTreeVO.getChildren() != null && i < upTreeVO.getChildren().size() ; i++) {
				getParentNode(upTreeVO.getChildren().get(i), treeVO, dynamic_flag, expand_level);
			}
		}
	}
	
	public static void getMapParentNode(TreeVO upTreeVO, HashMap treeData, boolean dynamic_flag, int expand_level)  {
		TreeVO treeVO = new TreeVO();
		treeVO.setId((String)treeData.get("ID"));
		treeVO.setNode_id((String)treeData.get("NODE_ID"));
		treeVO.setText((String)treeData.get("TEXT"));
		treeVO.setUp_node_id((String)treeData.get("UP_NODE_ID"));
		treeVO.setLeaf((String)treeData.get("LEAF"));
		treeVO.setOtherData(getOtherData(treeData));
		if( treeData.containsKey("CLS") ){
			treeVO.setCls((String)treeData.get("CLS"));
		}
		
		if(upTreeVO.getNode_id().equals(treeVO.getUp_node_id())) {
			if(upTreeVO.getChildren() == null) {
				upTreeVO.setChildren(new ArrayList());
			}
			if( expand_level > Integer.parseInt(treeVO.getNode_level()) && !(treeVO.isLeaf())) {
				treeVO.setExpanded(true);
			}
			upTreeVO.getChildren().add(treeVO);
		} else {  
			if(!dynamic_flag) {
				if(upTreeVO.getChildren() == null) {
					upTreeVO.setChildren(new ArrayList());
				}
			}
			for(int i = 0 ; upTreeVO.getChildren() != null && i < upTreeVO.getChildren().size() ; i++) {
				getMapParentNode(upTreeVO.getChildren().get(i), treeData, dynamic_flag, expand_level);
			}
		}
	}	
	
	public static CheckTreeVO changeCheckTreeToJson(List resultList, String up_node_id, boolean dynamic_flag, Integer expand_level, boolean includeColumn)  {
		CheckTreeVO rootTreeVO = new CheckTreeVO();
		if(expand_level == null) {
			expand_level = 0;
		}
		if(resultList != null && resultList.size() > 0) {
			HashMap firstTreeData = (HashMap)resultList.get(0);
			rootTreeVO.setId((String)firstTreeData.get("UP_NODE_ID"));
			rootTreeVO.setNode_id((String)firstTreeData.get("UP_NODE_ID"));
			rootTreeVO.setNode_level(String.valueOf(firstTreeData.get("NODE_LEVEL")));
			if( includeColumn ){
				rootTreeVO.setOtherData(getOtherData(firstTreeData));
			}
			if( firstTreeData.containsKey("CLS") ){
				rootTreeVO.setCls((String)firstTreeData.get("CLS"));
			}
			
			rootTreeVO.setChildren(new ArrayList());
			
			for(int i = 0 ; i < resultList.size() ; i++) {
				getCheckParentNode(rootTreeVO, (HashMap)resultList.get(i), dynamic_flag, expand_level, includeColumn);
			}
		}
		return rootTreeVO;
	}
		
	public static void getCheckParentNode(CheckTreeVO upTreeVO, CheckTreeVO treeVO, boolean dynamic_flag, int expand_level)  {
		CheckTreeVO childTreeVO = treeVO;
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(upTreeVO.getNode_id() != null && childTreeVO.getUp_node_id() != null) {
			if(upTreeVO.getNode_id().equals(childTreeVO.getUp_node_id())) {
				if(upTreeVO.getChildren() == null) {
					upTreeVO.setChildren(new ArrayList());
				}
				if( expand_level > Integer.parseInt(treeVO.getNode_level()) && !(treeVO.isLeaf())) {
					treeVO.setExpanded(true);
				}
				upTreeVO.getChildren().add(childTreeVO);
			} else {
				if(!dynamic_flag) {
					if(upTreeVO.getChildren() == null) {
						upTreeVO.setChildren(new ArrayList());
					}
				}
				for(int i = 0 ; upTreeVO.getChildren() != null && i < upTreeVO.getChildren().size() ; i++) {
					getCheckParentNode((CheckTreeVO)upTreeVO.getChildren().get(i), childTreeVO, dynamic_flag, expand_level);
				}
			}
		}
		
	}	
	
	/**
	 * 
	 * 체크 Tree를 생성하며 그외에 필요한 데이터를 셋해주는 Method
	 * 
	 * @param upTreeVO
	 * @param treeData
	 * @param dynamic_flag
	 * @param expand_level
	 * @param includeColumn
	 * @
	 */
	public static void getCheckParentNode(CheckTreeVO upTreeVO, HashMap treeData, boolean dynamic_flag, int expand_level, boolean includeColumn)  {
		CheckTreeVO childTreeVO = new CheckTreeVO();
		childTreeVO.setId((String)treeData.get("ID"));
		childTreeVO.setNode_id((String)treeData.get("NODE_ID"));
		childTreeVO.setText((String)treeData.get("TEXT"));
		childTreeVO.setUp_node_id((String)treeData.get("UP_NODE_ID"));
		childTreeVO.setLeaf((String)treeData.get("LEAF"));
		childTreeVO.setChecked((String)treeData.get("CHECKED"));
		childTreeVO.setExpanded(Boolean.valueOf((String)treeData.get("EXPANDED")));
		childTreeVO.setNode_level(String.valueOf(treeData.get("NODE_LEVEL")));
		if( includeColumn ){
			childTreeVO.setOtherData(getOtherData(treeData));
		}
		if( treeData.containsKey("CLS") ){
			childTreeVO.setCls((String)treeData.get("CLS"));
		}
		if(upTreeVO.getNode_id().equals(childTreeVO.getUp_node_id())) {
			if(upTreeVO.getChildren() == null) {
				upTreeVO.setChildren(new ArrayList());
			}
			if( expand_level > Integer.parseInt(childTreeVO.getNode_level()) && !(childTreeVO.isLeaf())) {
				childTreeVO.setExpanded(true);
			}
			upTreeVO.getChildren().add(childTreeVO);
		} else {
			if(!dynamic_flag) {
				if(upTreeVO.getChildren() == null) {
					upTreeVO.setChildren(new ArrayList());
				}
			}
			for(int i = 0 ; upTreeVO.getChildren() != null && i < upTreeVO.getChildren().size() ; i++) {
				getCheckParentNode((CheckTreeVO)upTreeVO.getChildren().get(i), treeData, dynamic_flag, expand_level, true);
			}
		}
	}
	
	/**
	 * 
	 * jsonData ToArray
	 * 
	 * @param jsonData
	 * @return
	 * @
	 */
	public static JSONArray convertStringToJsonArray(String jsonData){
		return JSONArray.fromObject(JSONSerializer.toJSON(jsonData));
	}
	
	/**
	 * 
	 * jsonData ToJsonObject
	 * 
	 * @param jsonData
	 * @return
	 * @
	 */
	public static JSONObject readDataToJsonObject(String jsonData){
		return JSONObject.fromObject(JSONSerializer.toJSON(jsonData));
	}
	
	/**
	 * 
	 * Tree생성시 Tree이외의 데이터를 만들어주는 Method
	 * 
	 * @param dataMap
	 * @return
	 * @
	 */
	public static HashMap getOtherData(HashMap dataMap){
		HashMap otherData = new HashMap();
		Iterator iter = (Iterator)dataMap.keySet().iterator();
		while( iter.hasNext() ){
			String key = (String)iter.next();
			if( !key.equalsIgnoreCase("NODE_ID") && !key.equalsIgnoreCase("ID") && !key.equalsIgnoreCase("TEXT")
					&& !key.equalsIgnoreCase("LEAF") && !key.equalsIgnoreCase("CHECKED") ){
				if(dataMap.get(key) == null) {
					otherData.put(key, null);
				} else {
					otherData.put(key, String.valueOf(dataMap.get(key)));
				}
			}
		}
		return otherData;
	}	
}
