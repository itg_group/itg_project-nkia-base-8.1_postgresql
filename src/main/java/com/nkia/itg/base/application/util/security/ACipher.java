/*
 * @(#)ACipher.java	2005. 11. 03
 *
 * Copyright 2005 Nkia.com, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.nkia.itg.base.application.exception.NkiaException;
import com.sun.xml.bind.v2.TODO;

/**
 * 암호화/복호화 랩퍼 클래스  
 * 입력받은 암호를 sha-1방식으로 해쉬(복호화가 불가능한 압축 암호 알고리즘) 한 후 
 * 그 결과값의 16바이트만을 취하여 AES암호화 알고리즘의 키로 사용
 * 만약 AES에 사용할 키의 길이를 늘이면 더 안전한 암호화가 가능하지만 
 * 이것은 unlimited version 을 따로 받던가 아니면 JDK 1.5 버전 이상을 사용하셔야 합니다. 
 * <pre>
 * Usage :
 *     사용방법 예시.
 * </pre>
 *
 * @see TODO 참고 클래스 나열.
 * @version <tt>$ Version: 1.0 $</tt>    date:2005. 11. 3
 * @author <a href="mailto:jipark@nkia.co.kr"> 박종일 </a>
 * 
 * <pre></pre>
 * 클래스 수정 내용 및 설명.
 * @version <tt>$ Reversion: 1.x $</tt>    date:2005. 11. 3
 * @author <a href="mailto:jipark@nkia.co.kr"> 박종일 </a>
 */
public class ACipher {
	
	/**
	 * 암호화를 위하여 사용되는 고정키 값을 나타내는 상수.
	 */
	public static String SEED_KEY = "nkia-nnp";
	public static final String CIPHER_SEED = "dkduddlghkdlxldj";
	
	
	/**
	 * 암호화 
	 * @param seed
	 * @param plain
	 * @return
	 */
	public static String encrypt(String seed, String plain) throws NkiaException {
		CipherWorld acw = null;
		byte[] abt 		= null;
		
		try{
			acw = new CipherAES(CipherUtil.generateAESKey(seed, "SHA-1"));
			abt = acw.getEncryptedByte(plain);
		}catch(NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e){
			throw new NkiaException(e);
		}
		
		return CipherUtil.getBase64EncodeString(abt);
	}
	
	/**
	 * 복호화
	 * @param seed
	 * @param msg
	 * @return
	 */
	public static String decrypt(String seed, String msg) throws NkiaException {
		CipherWorld acw = null;
		byte[] temp		= null;
		String result 	= null;
		
		try{
			acw  	= new CipherAES(CipherUtil.generateAESKey(seed, "SHA-1"));
			temp 	= CipherUtil.getBase64DecodeBuffer(msg);
			result 	= acw.getDecryptedString(temp);
			
		}catch(NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e){
			result = msg;	// 복호화시 에러가 날경우 원 메시지를 리턴한다.
			//new NkiaException(e); // 오류가 발생하더라도 리턴값이 존재해야함. 시큐어코딩을 위해 throw 처리하는 경우 리턴값이 없음.
		}
		
		return result;
	}

}
