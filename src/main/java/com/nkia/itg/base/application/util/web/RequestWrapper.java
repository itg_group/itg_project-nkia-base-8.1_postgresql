package com.nkia.itg.base.application.util.web;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import com.nkia.itg.base.application.util.json.JsonMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class RequestWrapper {

	private HttpServletRequest request;

	private RequestWrapper(HttpServletRequest request) {
		this.request = request;
	}

	public static RequestWrapper of(HttpServletRequest request) {
		return new RequestWrapper(request);
	}

	public static RequestWrapper of(ServletRequest request) {
		return of((HttpServletRequest) request);
	}

	public Map<String, String> headerMap() {
		Map<String, String> convertedHeaderMap = new HashMap<String, String>();

		Enumeration<String> headerMap = request.getHeaderNames();

		while (headerMap.hasMoreElements()) {
			String name = headerMap.nextElement();
			String value = request.getHeader(name);

			convertedHeaderMap.put(name, value);
		}
		return convertedHeaderMap;
	}

	public String parameterJson() throws IOException {
		StringBuffer jsonParam = new StringBuffer();
		if("application/json".equals(request.getContentType())) {
			String line = null;
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				jsonParam.append(line);
			}
		} else {
			Map<String, String> convertedParameterMap = new HashMap<String, String>();
			Map<String, String[]> parameterMap = request.getParameterMap();
			for (String key : parameterMap.keySet()) {
				String[] values = parameterMap.get(key);
				String result = "";
				for (int i = 0; i < values.length; i++) {
					result += values[i];
					if (i < values.length - 1) result += ",";
				}
				convertedParameterMap.put(key, result);
			}
			jsonParam.append(JsonMapper.toJson(convertedParameterMap));
		}
		return jsonParam.toString();
	}

	public String getRequestUri() {
		return request.getRequestURI();
	}
	
	public String getServerName() {
		return request.getServerName();
	}
}
