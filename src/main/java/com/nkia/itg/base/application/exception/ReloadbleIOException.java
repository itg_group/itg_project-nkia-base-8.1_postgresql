package com.nkia.itg.base.application.exception;

import java.io.IOException;

public class ReloadbleIOException extends IOException {
	
	public ReloadbleIOException(String msg) {
		super(msg);
	}
	
	public ReloadbleIOException(String msg, Throwable cause) {
		super(msg);
		initCause(cause);
	}
}
