package com.nkia.itg.base.application.util.email;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.nkia.itg.base.application.config.NkiaApplicationEmailMap;
import com.nkia.itg.base.application.config.NkiaApplicationEmailTemplateMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;

public class EmailSetIncreaseEndData {
	
	
	public static EmailVO setData(Map mailInfoMap) throws NkiaException, JsonParseException, JsonMappingException, IOException {
		
		String reqType = "";
		String templateId = "";
		String linkURL = "";
		List toUserList = new ArrayList();
		Map assetLNdataMap = new HashMap();
		String emailTitle = "";
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(mailInfoMap != null) {
			reqType = (String) mailInfoMap.get("KEY"); //메일 요청 유형
			templateId = (String) mailInfoMap.get("TEMPLATE_ID"); //메일 템플릿
			linkURL = (String) mailInfoMap.get("LINK_URL"); //이메일 링크주소
			toUserList = (List) mailInfoMap.get("TO_USER_LIST"); //수신자
			assetLNdataMap = (Map) mailInfoMap.get("DATA"); //회선 데이터
			emailTitle = (String) mailInfoMap.get("EMAIL_TITLE"); //이메일 제목
			
			emailTitle += "(" + assetLNdataMap.get("LN_CNT") + "건)";
		}		
		
		Map dataMap = new HashMap(); //메일 발송을 위한 데이터
		EmailVO emailVO = new EmailVO();
		String err_msg = ""; //에러메시지
		
		boolean setFlag = false;
	
		//SYS_MAIL_SENDER_INFO 테이블
		// MAIL_ID 구분으로 하나의 메일 서버인 경우는 변수로 기본 설정
		// 다중일 경우에는 조건에 추가 수정
		String emailId = EmailEnum.EMAIL_ID_1.getString();
		String emailConfigData = NkiaApplicationEmailMap.getMailInfo(emailId);
		String emailTemplateId = emailId + "==" + templateId;
		String emailTemplateData = NkiaApplicationEmailTemplateMap.getMailTemplateInfo(emailTemplateId);
		
		if (emailConfigData != null && emailTemplateData != null) {
			setFlag = true;
		} else {
			setFlag = false;
			err_msg = "NO DATA - CONFIG, TEMPLATE";
			
			if (emailConfigData == null) {
				err_msg = "NO DATA - CONFIG";
			}
			
			if (emailTemplateData == null) {
				err_msg = "NO DATA - TEMPLATE";
			}
		}
		emailVO.setConfig_flag(setFlag);
		emailVO.setErr_msg(err_msg);
		
		if (setFlag) {
			//메일발송정보
			ObjectMapper mapper = new ObjectMapper();
	        Map<String, Object> configMap = new HashMap<String, Object>();
	        configMap = mapper.readValue(emailConfigData, new TypeReference<Map<String, Object>>() {});
			
			emailVO.setEmail_job_type((String)configMap.get("EMAIL_JOB_TYPE"));
			emailVO.setHost((String)configMap.get("EMAIL_HOST"));
			int port = Integer.parseInt(String.valueOf(configMap.get("EMAIL_PORT")));
			emailVO.setPort(port);
			emailVO.setEmail_admin_id((String)configMap.get("EMAIL_ADMIN_ID"));
			emailVO.setEmail_admin_pw((String)configMap.get("EMAIL_ADMIN_PW"));
			emailVO.setEmail_admin_addr((String)configMap.get("EMAIL_ADMIN_ADDR"));
			emailVO.setEmail_multi_send_yn((String)configMap.get("EMAIL_MULTI_SEND_YN"));
			
			//메일 템플릿 테이블 내의 발송 정보
	        Map<String, Object> templateMap = new HashMap<String, Object>();
	        templateMap = mapper.readValue(emailTemplateData, new TypeReference<Map<String, Object>>() {});
			
			//From (발송자) 정보
			emailVO.setFrom((String)templateMap.get("EMAIL_FROM"));
			
			//메일에 들어갈 링크 URL
			dataMap.put("LINK_URL", linkURL);
			
			dataMap.put("SERVICE_CUSTOMER_ID_NM", assetLNdataMap.get("SERVICE_CUSTOMER_ID_NM"));
			dataMap.put("INCREASE_END", assetLNdataMap.get("INCREASE_END"));
			dataMap.put("LN_INFO", assetLNdataMap.get("LN_INFO"));
			
			String content = getContent(templateId, dataMap);
			
			//발송 데이터 정보
			emailVO.setEmail_req_type(reqType);
			emailVO.setEmail_id(emailId);
			emailVO.setSubject(emailTitle);
			emailVO.setContent(content);
			emailVO.setTo_user_list(toUserList);
			emailVO.setTemplate_id(templateId);
			emailVO.setUrl(linkURL);
		}
		
		return emailVO;
	}
	
	public static String getContent(String templateName, Map dataInfo) throws NkiaException, IOException {
		EmailTemplateGenerator etg = new EmailTemplateGenerator();
		//String result = etg.generateContent(templateName, masterInfo);
		String result = etg.customGenerateContent(templateName, dataInfo);
		return result;
	}
	
}
