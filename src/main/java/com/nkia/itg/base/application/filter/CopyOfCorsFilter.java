package com.nkia.itg.base.application.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nkia.itg.certification.vo.UserVO;

public class CopyOfCorsFilter implements Filter {
	
	private boolean isEnable;
	private String dummySessionUserId;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.isEnable = Boolean.valueOf(filterConfig.getInitParameter("enable"));
		this.dummySessionUserId = filterConfig.getInitParameter("dummySessionUserId");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if(isEnable) {
			HttpServletRequest req = (HttpServletRequest)request;
			HttpServletResponse res = (HttpServletResponse)response;
			//setDummySessionUser(req, dummySessionUserId);
			setCorsEnable(req, res);		
		}
		chain.doFilter(request, response);
	}
	
	private void setCorsEnable(HttpServletRequest request, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		if (request.getHeader("Access-Control-Request-Method") != null && "OPTIONS".equals(request.getMethod())) {
			response.addHeader("Access-Control-Allow-Methods", "POST");
			response.addHeader("Access-Control-Allow-Headers", "Content-Type");
			response.addHeader("Access-Control-Max-Age", "1");// 30 min
		}	
	}

	private void setDummySessionUser(HttpServletRequest request, String dummySessionUserId) {
		UserVO userVO = new UserVO();
		userVO.setUser_id(dummySessionUserId);
		request.getSession().setAttribute("userVO", userVO);
	}

	@Override
	public void destroy() {
	}
}