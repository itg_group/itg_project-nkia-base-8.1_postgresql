/*
 * @(#)ZipUtil.java              2013. 2. 5.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Stack;

import net.sf.jazzlib.ZipEntry;
import net.sf.jazzlib.ZipOutputStream;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.nkia.itg.base.application.exception.NkiaException;

@Component("zipUtil")
public class ZipUtil {
	
	private static final int COMPRESSION_LEVEL = 8;

    private static final int BUFFER_SIZE = 1024 * 2;
    
/*    public static void main(String[] args) throws NkiaException {
		ZipUtil ziputil = new ZipUtil();
		String sourcePath = "D:\\egov_workspace\\itg_project\\temp\\zip\\1360061598004";
		String output = "D:\\egov_workspace\\itg_project\\temp\\archive";
		String outputFileName = "test";
		String outputPath = "D:\\egov_workspace\\itg_project\\temp\\archive";
		ziputil.zip(sourcePath, output);
		
		ziputil.zip( new File( sourcePath ), new File(output), outputFileName, Charset.defaultCharset( ).name() , false ); // 압축시 루트디렉터리에 파일이 들어감


	}*/
    
    /** 
     * 지정된 폴더를 Zip 파일로 압축한다.
     * @param sourcePath - 압축 대상 디렉토리
     * @param output - 저장 zip 파일 이름
     * @throws NkiaException
     * @throws IOException 
     */
    public static boolean zip(String sourcePath, String output) throws NkiaException, IOException {
    	boolean isSuccess = false;

        // 압축 대상(sourcePath)이 디렉토리나 파일이 아니면 리턴한다.
        File sourceFile = new File(sourcePath);
        if (!sourceFile.isFile() && !sourceFile.isDirectory()) {
            throw new NkiaException("압축 대상의 파일을 찾을 수가 없습니다.");
        }

        // output 의 확장자가 zip이 아니면 리턴한다.
        if (!(StringUtils.substringAfterLast(output, ".")).equalsIgnoreCase("zip")) {
            throw new NkiaException("압축 후 저장 파일명의 확장자를 확인하세요");
        }

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        ZipOutputStream zos = null;
      
        
        try {
	    	fos = new FileOutputStream(output); // FileOutputStream
	        bos = new BufferedOutputStream(fos); // BufferedStream
	        zos = new ZipOutputStream(bos); // ZipOutputStream
            zos.setLevel(COMPRESSION_LEVEL); // 압축 레벨 - 최대 압축률은 9, 디폴트 8
            zipEntry(sourceFile, sourcePath, zos); // Zip 파일 생성
            zos.finish(); // ZipOutputStream finish
            
            isSuccess = true;
        }catch(NkiaException e){
        	throw new NkiaException(e);
        } finally {
            if (zos != null) {
                zos.close();
            }
            if (bos != null) {
                bos.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
        
        return isSuccess;
    }

    /**
     * 압축
     * @param sourceFile
     * @param sourcePath
     * @param zos
     * @throws NkiaException
     * @throws IOException 
     */
    private static void zipEntry(File sourceFile, String sourcePath, ZipOutputStream zos) throws NkiaException, IOException {
        // sourceFile 이 디렉토리인 경우 하위 파일 리스트 가져와 재귀호출
        if (sourceFile.isDirectory()) {
            if (sourceFile.getName().equalsIgnoreCase(".metadata")) { // .metadata 디렉토리 return
                return;
            }
            File[] fileArray = sourceFile.listFiles(); // sourceFile 의 하위 파일 리스트
            for (int i = 0; fileArray != null && i < fileArray.length; i++) {
                zipEntry(fileArray[i], sourcePath, zos); // 재귀 호출
            }
        } else { // sourcehFile 이 디렉토리가 아닌 경우
            BufferedInputStream bis = null;
            try {
                String sFilePath = sourceFile.getPath();
                String zipEntryName = sFilePath.substring(sourcePath.length() + 1, sFilePath.length());

                bis = new BufferedInputStream(new FileInputStream(sourceFile));
                ZipEntry zentry = new ZipEntry(zipEntryName);
                zentry.setTime(sourceFile.lastModified());
                zos.putNextEntry(zentry);

                byte[] buffer = new byte[BUFFER_SIZE];
                int cnt = 0;
                while ((cnt = bis.read(buffer, 0, BUFFER_SIZE)) != -1) {
                    zos.write(buffer, 0, cnt);
                }
                zos.closeEntry();
            } finally {
                if (bis != null) {
                    bis.close();
                }
            }
        }
    }
    
    private final static int    BUF_SIZE    = 1024 * 8;

    /**
     * 압축파일을 해제
     * 
     * @param zippedFile
     *            : 압축풀 대상파일
     * @param destDir
     *            : 압축이 해제될 디렉터리경로
     * @throws IOException
     */
    public static void unzip( File zippedFile , File destDir ) throws IOException
    
    {

    	destDir.setExecutable(false,true);
    	destDir.setReadable(true);
    	destDir.setWritable(false,true); 

        if ( destDir.exists( ) == false )
        {
            destDir.mkdir( );
        }
        unzip( new FileInputStream( zippedFile ) , destDir , Charset.defaultCharset( ).name( ) );
    }

    /**
     * 압축파일을 해제
     * 
     * @param zippedFile
     *            : 압축풀 대상파일
     * @param destDir
     *            : 압축이 해제될 디렉터리경로
     * @param charsetName
     *            : 캐릭터셋 지정
     * @throws IOException
     */
    public static void unzip( File zippedFile , File destDir , String charsetName ) throws IOException
    {
    	destDir.setExecutable(false, true);
    	destDir.setReadable(true);
    	destDir.setWritable(false, true);
    	
        if ( destDir.exists( ) == false )
        {
            destDir.mkdir( );
        }
        unzip( new FileInputStream( zippedFile ) , destDir , charsetName );
    }

    public static void unzip( InputStream is , File destDir , String charsetName ) throws IOException
    {
        ZipArchiveInputStream zis = null;
        ZipArchiveEntry entry = null;
        int nWritten = 0;
        byte[] buf = new byte[BUF_SIZE];
        zis = new ZipArchiveInputStream( is , charsetName , false );
        try
        {
            while ( ( entry = zis.getNextZipEntry( ) ) != null )
            {
                File target = null;
                target = new File( destDir , entry.getName( ) );
                target.setExecutable(false,true);
                target.setReadable(true);
                target.setWritable(true,true); // 2021.04 폼디자이너 디플로이시 오류로 인해 수정
                if ( entry.isDirectory( ) )
                {
                    target.mkdirs(); /* does it always work? */
                }
                else
                {
                    File f = new File( target.getParent( ) );
                    
                    f.setExecutable(false, true);
                	f.setReadable(true);
                	f.setWritable(false, true);
                    
                    if ( f.exists( ) == false )
                    {
                        f.mkdirs( );
                    }
                    target.createNewFile( );
                    BufferedOutputStream bos = null;
                    try
                    {
                        bos = new BufferedOutputStream( new FileOutputStream( target ) );
                        while ( ( nWritten = zis.read( buf ) ) >= 0 )
                        {
                            bos.write( buf , 0 , nWritten );
                        }
                    }
                    finally
                    {
                        if ( bos != null )
                        {
                            bos.close( );
                        }
                    }
                }
            }
        }
        finally
        {
        	if ( is != null )
            {
                is.close( );
            }
        	if ( zis != null )
            {
                zis.close( );
            }
            
        }
    }

    /**
     * 디렉터리를 압축
     * 
     * @param src
     *            : 압축할 디렉터리 경로
     * @param includeSrc
     *            : 압축시 루트디렉터리 안에 압축할지 여부
     * @throws IOException
     */
    public static void zip( File src , boolean includeSrc ) throws IOException, Exception
    {
        zip( src , Charset.defaultCharset( ).name( ) , includeSrc );
    }

    /**
     * 디렉터리를 압축
     * 
     * @param src
     *            : 압축할 디렉터리 경로
     * @param includeSrc
     *            : 압축시 루트디렉터리 안에 압축할지 여부
     * @param charsetName
     *            : 캐릭터셋 지정
     * @throws IOException
     */
    public static void zip( File src , boolean includeSrc , String charsetName ) throws IOException, Exception
    {
        zip( src , charsetName , includeSrc );
    }

    public static void zip( File src , String charSetName , boolean includeSrc ) throws IOException, Exception
    {
        zip( src , src.getParentFile(), null , charSetName , includeSrc );
    }

    public static boolean zip( File src , File destDir, String destFileName, String charSetName , boolean includeSrc ) throws IOException, Exception{
    	boolean isSuccess = false;
    	try {
			String fileName = src.getName();
			if( destFileName != null ){
				fileName = destFileName;
			}
			if ( src.isDirectory( ) == false )
			{
			    final int pos = fileName.lastIndexOf( "." );
			    if ( pos > 0 )
			    {
			        fileName = fileName.substring( 0 , pos );
			    }
			}
			fileName += ".zip";
			File zippedFile = new File( destDir , fileName );
			if ( zippedFile.exists( ) != false )
			{
			    zippedFile.createNewFile( );
			}
			zip( src , new FileOutputStream( zippedFile ) , charSetName , includeSrc );
			isSuccess = true;
			
		} catch (Exception e) {
			throw new NkiaException(e);
		} 
        return isSuccess;
    }

    public static void zip( File src , OutputStream os , String charsetName , boolean includeSrc ) throws IOException
    {
        ZipArchiveOutputStream zos = new ZipArchiveOutputStream( os );
        zos.setEncoding( charsetName );
        FileInputStream fis = null;
        int length;
        ZipArchiveEntry ze = null;
        byte[] buf = new byte[BUF_SIZE];
        String name = null;
        Stack< File > stack = new Stack< File >( );
        File root = null;
        if ( src.isDirectory( ) )
        {
            if ( includeSrc )
            {
                stack.push( src );
                root = src.getParentFile( );
            }
            else
            {
                File[] fs = src.listFiles( );
                for ( int i = 0 ; fs != null && i < fs.length ; i++ )
                {
                    stack.push( fs[ i ] );
                }
                root = src;
            }
        }
        else
        {
            stack.push( src );
            root = src.getParentFile( );
        }
        try
        {
            while ( stack.isEmpty( ) == false )
            {
                File f = stack.pop( );
                if (root != null) {
                	name = toPath( root , f );
                }
                
                if ( f.isDirectory( ) )
                {
                    File[] fs = f.listFiles( );
                    if(fs != null){
                    	for ( int i = 0 ; i < fs.length ; i++ )
                        {
                            if ( fs[ i ].isDirectory( ) )
                            {
                                stack.push( fs[ i ] );
                            }
                            else
                            {
                                stack.add( 0 , fs[ i ] );
                            }
                        }
                    }
                    
                }
                else
                {
                    ze = new ZipArchiveEntry( name );
                    zos.putArchiveEntry( ze );
                    fis = new FileInputStream( f );
                    try
                    {
                        while ( ( length = fis.read( buf , 0 , buf.length ) ) >= 0 )
                        {
                            zos.write( buf , 0 , length );
                        }
                    }
                    finally
                    {
                        if ( fis != null )
                        {
                            fis.close( );
                        }
                        zos.closeArchiveEntry( );
                    }
                }
            }
        }
        finally
        {
            if ( zos != null )
            {
                zos.close( );
            }
            if(os != null) {
            	os.close();
            }
        }
    }

    private static String toPath( File root , File dir )
    {
        String path = dir.getAbsolutePath( );
        path = path.substring( root.getAbsolutePath( ).length( ) ).replace( File.separatorChar , '/' );
        if ( path.startsWith( "/" ) )
        {
            path = path.substring( 1 );
        }
        if ( dir.isDirectory( ) && path.endsWith( "/" ) == false )
        {
            path += "/";
        }
        return path;
    }


}
