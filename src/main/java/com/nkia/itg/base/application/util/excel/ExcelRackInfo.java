package com.nkia.itg.base.application.util.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.ExcelFileService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

@Service("excelRackInfo")
public class ExcelRackInfo {
	@Resource(name = "excelFileService")
	private ExcelFileService excelFileService;
	
	/**
	 * @method {} makeExcelFile : 엑셀다운로드
	 * 
	 * @param {} jsonObject : 엑셀 헤더 객체
	 * @param {} fileName : 파일이름
	 * @param {} result : 엑셀에 출력할 쿼리 결과
	 * @return {mapResult: 파일이름, 파일경로}
	 * @throws IOException 
	 * @throws WriteException 
	 * 
	 */
	public ModelMap makeExcelFile(Map excelParams, List result)throws NkiaException, IOException, WriteException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xls";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		WritableWorkbook workbook = Workbook.createWorkbook(new File(fullPath));
		try {
			if(excelAttrs != null){
				List sheetList = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					int cutRowCount = 50000; // sheet 단위로 잘릴 Row 수...
					
					WritableSheet sheet = null;
					
					//BOLD
					jxl.write.WritableFont boldFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					boldFont.setBoldStyle(jxl.write.WritableFont.BOLD);
					
					//제목
					jxl.write.WritableFont titleFont = new jxl.write.WritableFont(jxl.write.WritableFont.ARIAL);
					titleFont.setPointSize(18);
					titleFont.setUnderlineStyle(jxl.format.UnderlineStyle.SINGLE);
					
					//Cell의 포멧방식을 헤더와 바디부분으로 관리하여 저장한다.
					jxl.write.WritableCellFormat titleFormat = getWritableCellFormat(titleFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat dateFormat = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, true);
					
					jxl.write.WritableCellFormat headFormat = getWritableCellFormat(boldFont, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.GRAY_25, true);
					
					jxl.write.WritableCellFormat bodyHeadFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat bodyFormat = getWritableCellFormat(null, true
							, jxl.format.Alignment.CENTRE, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat bodyFormatLeft = getWritableCellFormat(null, true
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat bodyFormatRight = getWritableCellFormat(null, true
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat giganFormatLeft = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.LEFT, jxl.format.Colour.WHITE, false);
					
					jxl.write.WritableCellFormat giganFormatRight = getWritableCellFormat(boldFont, false
							, jxl.format.Alignment.RIGHT, jxl.format.Colour.WHITE, false);
					
					jxl.write.Label label = null;
					jxl.write.Number number = null;
					jxl.write.Blank blank = null;
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List dataList = null;

						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						HashMap sheetMap = (HashMap)sheetList.get(i);
						String sheetName = (String)sheetMap.get("sheetName");
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List groupHeaders = (List)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}			
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName, i+1);
						sheet.setColumnView(0, 50);
						sheet.setColumnView(1, 20);
						sheet.setRowView(0, 600); // Row Height
						sheet.mergeCells(cols, 0, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
						label = new jxl.write.Label(0, 0, titleName, titleFormat);
						sheet.addCell(label);
						
						sheet.setRowView(1, 340);
						label = new jxl.write.Label(0, 1, "[다운로드일] "+ DateUtil.getDate("-"), dateFormat);
						sheet.addCell(label);
						sheet.setRowView(2, 500);
						rows = 2; // Excel HEAD 표시 위치
						
						cols = 0;
						
						//헤더부분을 처리한다.
						if( groupHeaders == null ){
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnView(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map data = (Map) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								label = new jxl.write.Label(cols, rows, (String) data.get("text"), headFormat); 
								sheet.addCell(label);
								
								if( span > 1 ){
									sheet.mergeCells(cols, rows, cols + (span-1), rows); // 타이틀 부분 셀병합 갯수
									cols = cols + (span-1);
								}else{
									sheet.mergeCells(cols, rows, cols, (rows+1)); // 타이틀 부분 셀병합 갯수
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							for( int j = 0; j < headerName.length; j++){
								label = new jxl.write.Label(cols, rows, headerName[j], headFormat); 
								sheet.addCell(label);
								if( j == 0 ){
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnView(cols++, 30); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						rows++;
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								////컬럼에 순번넣고싶으면 넣어라.
								//label = new jxl.write.Label(cols++, rows, String.valueOf(j+1), bodyFormat); 
								//sheet.addCell(label);
								HashMap map = (HashMap)dataList.get(j);
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										if(labelValue == null){
											labelValue = "-";
										}
										if(align != null) {
											if("left".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatLeft);
											} else if("right".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormatRight);
											} else if("center".equals(align[k])) {
												label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
											}
										} else {
											label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										}
										sheet.addCell(label);
									}
								}else{
									Iterator iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										label = new jxl.write.Label(cols++, rows, labelValue, bodyFormat);
										sheet.addCell(label);
									}            
								}
								rows++;
							}
						}else{
							String noneResult = "결과가 없습니다.";
							sheet.mergeCells(0, rows, headerName.length -1, rows); // 타이틀 부분 셀병합 갯수
							label = new jxl.write.Label(0, rows, noneResult, bodyFormat);
							sheet.addCell(label);
						}
					}
				}
				workbook.write();
				
				// 파일명 및 파일위치를 전송하지 않고 db에 저장된 path를 사용하여 전송
				//mapResult.put("fileName", realFileName);
				//mapResult.put("filePath", tempUploadPath);
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			if (workbook!=null) workbook.close();
		}
		return mapResult;
	}
	
	public ModelMap makeExcelFileXlsx(Map excelParams, List result)throws NkiaException{
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		//Excel파일을 저장하기 위한 기본 설정
		String fileName = (String)excelParams.get("fileName");
		String realFileName = fileName+"_"+System.currentTimeMillis()+".xlsx";
		String fullPath = null;
		String tempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		
		File directory = new File(tempExcelUploadPath);

		directory.setExecutable(false,true);
		directory.setReadable(true);
		directory.setWritable(false,true);

		if(!directory.exists()){
			directory.mkdirs();
		}

		fullPath = tempExcelUploadPath+File.separator+realFileName;
		
		Map excelAttrs = (Map)excelParams.get("excelAttrs");
		org.apache.poi.ss.usermodel.Workbook workbook = new XSSFWorkbook();
		
		try {
			if(excelAttrs != null){
				List sheetList = (List) excelAttrs.get("sheet");
				if( sheetList.size() > 0 ){
					int cutRowCount = 50000; // sheet 단위로 잘릴 Row 수...
					
					Sheet sheet = null;
					
					Font fontTitle = workbook.createFont();
					fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
					fontTitle.setFontHeightInPoints((short)14);
					fontTitle.setUnderline(Font.U_SINGLE);
					fontTitle.setFontName("맑은 고딕"); // Malgun Gothic

					Font fontBold = workbook.createFont();
					fontBold.setBoldweight(Font.BOLDWEIGHT_BOLD);
					fontBold.setFontHeightInPoints((short)9);
					fontBold.setFontName("맑은 고딕");// Malgun Gothic
					
					Font fontBody = workbook.createFont();
					fontBody.setFontHeightInPoints((short)9);
					fontBody.setFontName("맑은 고딕");// Malgun Gothic
					
					Font styleLegendFont = workbook.createFont();
					styleLegendFont.setFontHeightInPoints((short)9);
					styleLegendFont.setColor(HSSFColor.RED.index);
					styleLegendFont.setFontName("맑은 고딕");// Malgun Gothic
					
					Font styleDateFont = workbook.createFont();
					styleDateFont.setFontHeightInPoints((short)9);
					styleDateFont.setFontName("맑은 고딕");// Malgun Gothic
					
					XSSFCellStyle styleTitle = (XSSFCellStyle) workbook.createCellStyle();
					styleTitle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					styleTitle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleTitle.setFillForegroundColor(HSSFColor.WHITE.index);
					styleTitle.setFont(fontTitle);
					
					XSSFCellStyle styleDate = (XSSFCellStyle) workbook.createCellStyle();
					styleDate.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					styleDate.setFillBackgroundColor(HSSFColor.WHITE.index);
					styleDate.setFont(styleDateFont);
					
					XSSFCellStyle styleLegend = (XSSFCellStyle) workbook.createCellStyle();
					styleLegend.setAlignment(XSSFCellStyle.ALIGN_LEFT);
					styleLegend.setFont(styleLegendFont);
					styleLegend.setWrapText(true);
					
					XSSFCellStyle styleHeader = (XSSFCellStyle) workbook.createCellStyle();
					styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleHeader.setFillForegroundColor(new XSSFColor(new java.awt.Color(200, 200, 200)));
					styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
					styleHeader.setFont(fontBold);
					styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleHeader.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleHeader.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleHeader.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleHeader.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleHeader.setWrapText(true);
					
					XSSFCellStyle styleBody = (XSSFCellStyle) workbook.createCellStyle();
					styleBody.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					styleBody.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleBody.setFillBackgroundColor(HSSFColor.WHITE.index);
					styleBody.setFont(fontBody);
					styleBody.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleBody.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBody.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleBody.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBody.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleBody.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBody.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleBody.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBody.setWrapText(true);

					XSSFCellStyle styleBodyLeft = (XSSFCellStyle) workbook.createCellStyle();
					styleBodyLeft.setAlignment(XSSFCellStyle.ALIGN_LEFT);
					styleBodyLeft.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleBodyLeft.setFillBackgroundColor(HSSFColor.WHITE.index);
					styleBodyLeft.setFont(fontBody);
					styleBodyLeft.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleBodyLeft.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyLeft.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleBodyLeft.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyLeft.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleBodyLeft.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyLeft.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleBodyLeft.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyLeft.setWrapText(true);

					XSSFCellStyle styleBodyRight = (XSSFCellStyle) workbook.createCellStyle();
					styleBodyRight.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
					styleBodyRight.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleBodyRight.setFillBackgroundColor(HSSFColor.WHITE.index);
					styleBodyRight.setFont(fontBody);
					styleBodyRight.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleBodyRight.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRight.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleBodyRight.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRight.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleBodyRight.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRight.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleBodyRight.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRight.setWrapText(true);
					
					XSSFCellStyle styleBodyRack = (XSSFCellStyle) workbook.createCellStyle();
					styleBodyRack.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					styleBodyRack.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleBodyRack.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 192, 0)));
					styleBodyRack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
					styleBodyRack.setFont(fontBody);
					styleBodyRack.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleBodyRack.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRack.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleBodyRack.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRack.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleBodyRack.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRack.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleBodyRack.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyRack.setWrapText(true);
					
					XSSFCellStyle styleOverlapRack = (XSSFCellStyle) workbook.createCellStyle();
					styleOverlapRack.setAlignment(XSSFCellStyle.ALIGN_CENTER);
					styleOverlapRack.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleOverlapRack.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
					styleOverlapRack.setFillPattern(FillPatternType.SOLID_FOREGROUND);
					styleOverlapRack.setFont(fontBody);
					styleOverlapRack.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleOverlapRack.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleOverlapRack.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleOverlapRack.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleOverlapRack.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleOverlapRack.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleOverlapRack.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleOverlapRack.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleOverlapRack.setWrapText(true);
					
					XSSFDataFormat formatPrice = (XSSFDataFormat)workbook.createDataFormat();
					XSSFDataFormat formatFloat = (XSSFDataFormat)workbook.createDataFormat();
					short floatType = formatFloat.getFormat("#,##0.0##########");
					short priceType = formatPrice.getFormat("#,##0");
					
					XSSFCellStyle styleBodyPrice = (XSSFCellStyle) workbook.createCellStyle();
					styleBodyPrice.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
					styleBodyPrice.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleBodyPrice.setFillBackgroundColor(HSSFColor.WHITE.index);
					styleBodyPrice.setFont(fontBody);
					styleBodyPrice.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleBodyPrice.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyPrice.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleBodyPrice.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyPrice.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleBodyPrice.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyPrice.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleBodyPrice.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyPrice.setDataFormat(priceType);
					styleBodyPrice.setWrapText(true);
					
					XSSFCellStyle styleBodyFloat = (XSSFCellStyle) workbook.createCellStyle();
					styleBodyFloat.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
					styleBodyFloat.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
					styleBodyFloat.setFillBackgroundColor(HSSFColor.WHITE.index);
					styleBodyFloat.setFont(fontBody);
					styleBodyFloat.setBorderBottom(XSSFCellStyle.BORDER_THIN);
					styleBodyFloat.setBottomBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyFloat.setBorderLeft(XSSFCellStyle.BORDER_THIN);
					styleBodyFloat.setLeftBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyFloat.setBorderRight(XSSFCellStyle.BORDER_THIN);
					styleBodyFloat.setRightBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyFloat.setBorderTop(XSSFCellStyle.BORDER_THIN);
					styleBodyFloat.setTopBorderColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
					styleBodyFloat.setDataFormat(floatType);
					styleBodyFloat.setWrapText(true);
					
					//Head부분을 설정한다.
					int cols = 0;
					int rows = 0;
					
					Object firstObject = null;
					if(result != null && result.size() > 0) {
						firstObject = result.get(0);						
					}
					
					for( int i = 0; i < sheetList.size(); i++){
						List dataList = null;

						if(firstObject instanceof List) {
							//List의 첫번째 데이터가 List라면 다중시트를 생성하여야 한다.
							dataList = (List)(result.get(i));
						} else if(firstObject instanceof Map) {
							//List의 첫번째 데이터가 Map이라면 단일시트를 생성하여야 한다.						
							dataList = result;
						}
						
						HashMap sheetMap = (HashMap)sheetList.get(i);
						String sheetName = (String)sheetMap.get("sheetName");
						String titleName = (String)sheetMap.get("titleName");
						String headerNames = (String)sheetMap.get("headerNames");
						String headerWidths = (String)sheetMap.get("headerWidths");
						List groupHeaders = (List)sheetMap.get("groupHeaders");
						String[] includeColumn = null;
						String[] align = null;
						String[] xtype = null;
						
						if( sheetMap.get("includeColumns") != null ){
							String includeColumns = (String)sheetMap.get("includeColumns");
							includeColumn = StringUtil.explode(",", includeColumns);
						}
						
						if( sheetMap.get("align") != null ){
							String aligns = (String)sheetMap.get("align");
							align = StringUtil.explode(",", aligns);
						}
						
						if( sheetMap.get("excelXtype") != null ){
							String excelXtype = (String)sheetMap.get("excelXtype");
							xtype = StringUtil.explode(",", excelXtype);
						}
						
						String[] headerName = StringUtil.explode(",", headerNames);
						String[] headerWidth = null;
						if(headerWidths != null){
							headerWidth = StringUtil.explode(",", headerWidths);
						}
						
						
						cols = 0;
						rows = 0;
						sheet = workbook.createSheet(sheetName);
						sheet.setColumnWidth(0, 20 * 40);
						sheet.setColumnWidth(1, 20 * 30);
						
//						org.apache.poi.ss.usermodel.Row rowThis = sheet.createRow(0);
//						rowThis.setHeight((short) 600);
//						sheet.addMergedRegion(new CellRangeAddress(0,0,0,headerName.length -1));
//						
//						org.apache.poi.ss.usermodel.Cell cellThis = rowThis.createCell(0);
//						cellThis.setCellValue(titleName);
//						cellThis.setCellStyle(styleTitle);
						
						org.apache.poi.ss.usermodel.Row rowThis = sheet.createRow(0);
						rowThis.setHeight((short) 340);

						org.apache.poi.ss.usermodel.Cell cellThis = rowThis.createCell(0);
						cellThis.setCellValue( "[다운로드일] "+ DateUtil.getDate("-") );
						cellThis.setCellStyle(styleDate);
						cellThis.getCellStyle().setAlignment(XSSFCellStyle.ALIGN_LEFT);
						
						cellThis = rowThis.createCell(6);
						cellThis.setCellValue("■ : 중첩,다중");
						cellThis.setCellStyle(styleLegend);
						cellThis.getCellStyle().setAlignment(XSSFCellStyle.ALIGN_RIGHT);
						sheet.addMergedRegion(new CellRangeAddress(0,0,6,9));
						
						rowThis = sheet.createRow(1);
//						rowThis.setHeight((short) 500);
						
						rows = 1; // Excel HEAD 표시 위치
						cols = 0;
						
						//헤더부분을 처리한다.
						if( groupHeaders == null ){
							for( int j = 0; j < headerName.length; j++){
								cellThis = rowThis.createCell(j);
								cellThis.setCellValue( headerName[j] );
								cellThis.setCellStyle(styleHeader);
								
								if(headerWidth == null){//컬럼헤더 Width
									if( j == 0 ){
										sheet.setColumnWidth(cols++, 256 * (Integer.parseInt(headerWidth[j]) / 5)); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnWidth(cols++, 256 * (Integer.parseInt(headerWidth[j]) / 5)); // COLUMN 넓이(다음칸 시작위치)
									}
								}else{
									if( j == 0 ){
										sheet.setColumnWidth(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}else{
										sheet.setColumnWidth(cols++, Integer.parseInt(headerWidth[j])); // COLUMN 넓이(다음칸 시작위치)
									}
								}
							}
						}else{
							for( int j = 0; j < groupHeaders.size(); j++){
								Map data = (Map) groupHeaders.get(j);
								int span = Integer.parseInt((String) data.get("span"));
								
								cellThis = rowThis.createCell(cols);
								cellThis.setCellValue( (String) data.get("text") );
								cellThis.setCellStyle(styleHeader);
								
								if( span > 1 ){
									// 병합된 셀 수 만큼 스타일 셋팅
									for(int k=(cols + 1); k<(cols + span); k++){
										rowThis.createCell(k).setCellStyle(styleHeader);
									}
									
									sheet.addMergedRegion(new CellRangeAddress(rows,rows,cols,cols + (span-1)));
									cols = cols + (span-1);
								}else{
									sheet.addMergedRegion(new CellRangeAddress(rows,(rows+1),cols,cols));
								}
								
								cols++;
							}
							
							cols = 0;
							rows++;
							
							if ( headerName.length > 0 ){
								rowThis = sheet.createRow(rows);
							}
							
							for( int j = 0; j < headerName.length; j++){
								cellThis = rowThis.createCell(j);
								cellThis.setCellValue( (String) headerName[j] );
								cellThis.setCellStyle(styleHeader);
								
								if( j == 0 ){
									sheet.setColumnWidth(cols++, 256 * (Integer.parseInt(headerWidth[j]) / 5)); // COLUMN 넓이(다음칸 시작위치)
								}else{
									sheet.setColumnWidth(cols++, 256 * (Integer.parseInt(headerWidth[j]) / 5)); // COLUMN 넓이(다음칸 시작위치)
								}
							}
						}
						
						// 타이틀 셋팅
						sheet.getRow(1).getCell(0).setCellValue(titleName);
						
						rows++;
						rowThis = sheet.createRow(rows);
						
						if( dataList != null && dataList.size() > 0 ){
							for( int j = 0; j < dataList.size(); j++){
								cols = 0;
								HashMap map = (HashMap)dataList.get(j);
								int rowSpan = 0;
								
								//추가
								if(map.get("U_SIZE") != null) {
									rowSpan = Integer.parseInt(map.get("U_SIZE").toString()) - 1;
								}
								
								if( includeColumn != null ){
									for( int k = 0; k < includeColumn.length; k++){
										String labelValue = setLabel(map.get(includeColumn[k].toUpperCase()));
										if(labelValue == null){
											labelValue = "";
										}
										
										//추가
										if(rowSpan > 0) {
											if(!"STAGE".equals(includeColumn[k].toUpperCase())) {
												sheet.addMergedRegion(new CellRangeAddress(rows,(rows+rowSpan),cols,cols));
											}
										}
										
										cellThis = rowThis.createCell(cols++);
										cellThis.setCellType(cellThis.CELL_TYPE_STRING);
										cellThis.setCellValue( (String) labelValue );
										
										if(align != null) {
											if("left".equals(align[k])) {
												cellThis.setCellStyle(styleBodyLeft);
											} else if("right".equals(align[k])) {
												cellThis.setCellStyle(styleBodyRight);
												
												if(xtype != null && xtype.length == includeColumn.length) {
													if(!"-".equals(labelValue.toString())) {
														if("price".equals(xtype[k])
																|| "number".equals(xtype[k])) {
															cellThis.setCellStyle(styleBodyPrice);
															cellThis.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC);
															cellThis.setCellValue( Long.parseLong(labelValue.toString().replaceAll(",", "")) );
														} else if("float".equals(xtype[k])) {
															cellThis.setCellStyle(styleBodyFloat);
															cellThis.setCellType(org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC);
															cellThis.setCellValue( Double.parseDouble(labelValue.toString().replaceAll(",", "")) );
														}
													}
												}
											} else if("center".equals(align[k])) {
												cellThis.setCellStyle(styleBody);
											}
										} else {
											cellThis.setCellStyle(styleBody);
										}
										
										//추가
										if(xtype != null && xtype.length == includeColumn.length) {
											if("rack".equals(xtype[k])) {
												if(!"".equals(labelValue)) {
													if("Y".equals(map.get("OVERLAP_YN"))
															|| "Y".equals(map.get("MULTI_YN"))) {
														cellThis.setCellStyle(styleOverlapRack);
													} else {
														if(k == 1) {
															cellThis.setCellStyle(styleBodyRack);
														} else {
															cellThis.setCellStyle(styleBody);
														}
													}
												}
											}
										}
									}
								}else{
									Iterator iter = map.keySet().iterator();
									while(iter.hasNext()){
										String labelValue = setLabel(map.get(iter.next()));
										cellThis = rowThis.createCell(cols++);
										cellThis.setCellType(cellThis.CELL_TYPE_STRING);
										cellThis.setCellValue( (String) labelValue );
										cellThis.setCellStyle(styleBody);
									}            
								}
								
								if(j < (dataList.size() - 1)) {
									rows++;
									rowThis = sheet.createRow(rows);
								}
							}
						}else{
							String noneResult = "결과가 없습니다.";
							
							sheet.addMergedRegion(new CellRangeAddress(rows,rows,0,headerName.length -1));
							cellThis = rowThis.createCell(0);
							cellThis.setCellValue( noneResult );
							cellThis.setCellStyle(styleBody);
							
							// 병합된 셀 수 만큼 스타일 셋팅
							for(int k=1; k<headerName.length; k++){
								rowThis.createCell(k).setCellStyle(styleBody);
							}
						}
					}
				}
				
				File xlsxFile = new File(fullPath);
				FileOutputStream fileOut = new FileOutputStream(xlsxFile);				
				workbook.write(fileOut);
				fileOut.close();
				
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( fileName, realFileName, tempExcelUploadPath ) );
				isSucess = true;
				
			}
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			//if (FileOutputStream!=null) FileOutputStream.close();
		}
		return mapResult;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String excelDownload_History( String fileName, String realFileName, String tempUploadPath ) throws NkiaException{
		String excel_file_id = null;
		try {
			excel_file_id = excelFileService.selectMaxHistoryId();
			
			Map history_param = new HashMap();
			history_param.put("excel_file_id", excel_file_id);
			history_param.put("filename", fileName);
			history_param.put("real_filename", realFileName);
			history_param.put("temp_upload_path", tempUploadPath);
			history_param.put("file_extsn", "xls");
			history_param.put("down_user_id", EgovUserDetailsHelper.getSesssionUserId());
			
			// Insert
			excelFileService.insertExcelFile(history_param);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return excel_file_id;
	}
	
	private WritableCellFormat getWritableCellFormat(
			WritableFont cellFont
			, boolean flag_border
			, Alignment align
			, Colour bakColor, boolean flag_verticalAlign) throws WriteException {
		
		jxl.write.WritableCellFormat cellFormat = null;
		
		if(cellFont != null) {
			cellFormat = new WritableCellFormat(cellFont);			
		} else {
			cellFormat = new WritableCellFormat();
		}
		
		if(bakColor != null) {
			cellFormat.setBackground(bakColor);
		}
		
		if(flag_border){
			cellFormat.setBorder(jxl.format.Border.ALL ,jxl.format.BorderLineStyle.THIN );
		}

		cellFormat.setAlignment(align);
		
		if(flag_verticalAlign) {
			cellFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);			
		}
		
		cellFormat.setWrap(true);
		return cellFormat;
	}
	
	private String setLabel(Object objData){
		String labelValue = null;
		if( objData instanceof java.lang.String ){
			labelValue = (String)objData;
		}else if( objData instanceof BigDecimal ){
			BigDecimal getBigDecimalData = (BigDecimal)objData;
			labelValue = getBigDecimalData.toString();
		}else if( objData instanceof Date ){
			labelValue = ((Date)objData).toString();
		}else if( objData instanceof Timestamp ){
			labelValue = ((Timestamp)objData).toString();
		}else{
			labelValue = (String)objData;
		}
		return labelValue;
	}
}
