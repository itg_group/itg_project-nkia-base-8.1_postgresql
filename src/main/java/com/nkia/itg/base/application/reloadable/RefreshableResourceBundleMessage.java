package com.nkia.itg.base.application.reloadable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;
import org.springframework.util.ResourceUtils;

/**
 * 
 * @version 1.0 2015. 5. 18.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * Refresh Resource Bundle
 * </pre>
 *
 */
public class RefreshableResourceBundleMessage extends ReloadableResourceBundleMessageSource{

	private static final String PROPERTIES_SUFFIX = ".properties";

	private static final String XML_SUFFIX = ".xml";

	private long cacheMillis = -1;	// Cache Milliseconds

	private ResourceLoader resourceLoader = new DefaultResourceLoader();

	private final Map<String, PropertiesHolder> cachedProperties = new HashMap<String, PropertiesHolder>();

	public void setBasename(String basename) {
		super.setBasenames(new String[] {basename});
	}

	public void setCacheSeconds(int cacheSeconds) {
		this.cacheMillis = (cacheSeconds * 1000);
		super.setCacheSeconds(cacheSeconds);
	}
	
	protected PropertiesHolder getProperties(String filename) {
		synchronized (this.cachedProperties) {
			PropertiesHolder propHolder = this.cachedProperties.get(filename);
			if (propHolder != null &&
					(propHolder.getRefreshTimestamp() < 0 ||
					 propHolder.getRefreshTimestamp() > System.currentTimeMillis() - this.cacheMillis)) {
				// up to date
				return propHolder;
			}
			return refreshProperties(filename, propHolder);
		}
	}
	
	/**
	 * refreshProperties
	 * 프로퍼티 refresh
	 */
	protected PropertiesHolder refreshProperties(String filename, PropertiesHolder propHolder) {
		long refreshTimestamp = (this.cacheMillis < 0) ? -1 : System.currentTimeMillis();

		try {
			// 이전에 Classloader가 아닌 getURL을 이용하여 직접 호출
			URL realPath = ResourceUtils.getURL(filename + PROPERTIES_SUFFIX);
			Resource resource = new UrlResource(realPath);
			
			if (!resource.exists()) {
				resource = this.resourceLoader.getResource(filename + XML_SUFFIX);
				realPath = ResourceUtils.getURL(filename + XML_SUFFIX);
				resource = new UrlResource(realPath);
			}

			if (resource.exists()) {
				long fileTimestamp = -1;
				if (this.cacheMillis >= 0) {
					// Last-modified timestamp of file will just be read if caching with timeout.
					try {
						fileTimestamp = resource.lastModified();
						if (propHolder != null && propHolder.getFileTimestamp() == fileTimestamp) {
							if (logger.isDebugEnabled()) {
								logger.debug("Re-caching properties for filename [" + filename + "] - file hasn't been modified");
							}
							propHolder.setRefreshTimestamp(refreshTimestamp);
							return propHolder;
						}
					}
					catch (IOException ex) {
						// Probably a class path resource: cache it forever.
						if (logger.isDebugEnabled()) {
							logger.debug(
									resource + " could not be resolved in the file system - assuming that is hasn't changed", ex);
						}
						fileTimestamp = -1;
					}
				}
				try {
					Properties props = loadProperties(resource, filename);
					propHolder = new PropertiesHolder(props, fileTimestamp);
				}
				catch (IOException ex) {
					if (logger.isWarnEnabled()) {
						logger.warn("Could not parse properties file [" + resource.getFilename() + "]", ex);
					}
					// Empty holder representing "not valid".
					propHolder = new PropertiesHolder();
				}
			}

			else {
				// Resource does not exist.
				if (logger.isDebugEnabled()) {
					logger.debug("No properties file found for [" + filename + "] - neither plain properties nor XML");
				}
				// Empty holder representing "not found".
				propHolder = new PropertiesHolder();
			}
			propHolder.setRefreshTimestamp(refreshTimestamp);
			this.cachedProperties.put(filename, propHolder);
		} catch (FileNotFoundException e) {
			// Resource does not exist.
			if (logger.isDebugEnabled()) {
				logger.debug("No properties file found for [" + filename + "] - neither plain properties nor XML");
			}
			// Empty holder representing "not found".
			propHolder = new PropertiesHolder();
		}
		return propHolder;
	}

}
