package com.nkia.itg.base.application.reloadable;


public interface SqlMapClientRefreshable {

	void refresh() throws Exception;
	
	void setCheckInterval(int ms);
}
