/**
 * @file : NTopsApplicationContext.java
 * @author : 김도원
 * @since : 2010. 10. 15. 오전 10:34:52
 * @version : 1.0
 * @see
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -----------------------------------------
 *  2010. 10. 15. 김도원 
 *  
 *  </pre>
 * Copyright 2010(C) by NKIA. All right reserved.
 */

package com.nkia.itg.base.application.config;

import javax.servlet.http.HttpServlet;

import org.springframework.context.ApplicationContext;

public class NkiaApplicationContext extends HttpServlet {
	//protected final static Log logger = LogFactory.getLog(NTopsApplicationContext.getClass());

	/** serialVersionUID */
	private static final long			serialVersionUID	= 1536775354753870957L;
	private static ApplicationContext	ctx					= null;
	private static String projectPath;
	/**
	 * <p>Spring Context를 얻는다.
	 *
	 * @return web application context  
	 */
	public static ApplicationContext getCtx() {
		return ctx;
	}
	
	public static String getProjectPath() {
		return projectPath;
	}
	
	/**
	 * <p>Spring Context를 저장한다.
	 *
	 * @param ctx Spring Application Context
	 */
	public static void setCtx(ApplicationContext ctx) {
		NkiaApplicationContext.ctx = ctx;
	}

	public static void setProjectPath(String projectPath) {
		NkiaApplicationContext.projectPath = projectPath;
	}
}
