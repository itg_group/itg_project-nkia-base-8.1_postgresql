/*
 * @(#)ignoreSSLClient.java              2018. 5. 29. jyjeong@nkia.co.kr
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.nkia.itg.base.application.exception.NkiaException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

public class IgnoreSSLClient {
	
	public static Client getIgnoringSSLClient() throws NkiaException {
		
		TrustManager[] trustManager = new X509TrustManager[] { new X509TrustManager() {

		    @Override
		    public X509Certificate[] getAcceptedIssuers() {
		        return null;
		    }

		    @Override
		    public void checkClientTrusted(X509Certificate[] certs, String authType) {

		    }

		    @Override
		    public void checkServerTrusted(X509Certificate[] certs, String authType) {

		    }
		}};

		ClientConfig config = new DefaultClientConfig();
		
		try {
			//SSLContext sslContext = SSLContext.getInstance("SSL");
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, trustManager, null);
	
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
			
        
		    config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(
		        new HostnameVerifier() {
					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
		        }, 
		        sslContext
		    ));
	    } catch(NoSuchAlgorithmException | KeyManagementException e) {
	    	throw new NkiaException(e);
	    }
		
        return Client.create(config);
    }
}
