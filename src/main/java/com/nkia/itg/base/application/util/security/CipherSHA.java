/*
 * @(#)CipherSHA.java              2015. 10. 26.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.StringUtil;

public class CipherSHA {
	
/*	public static void main(String[] args) throws NkiaException{
		String password = "";

		// salt 생성
		java.util.Random random = new java.util.Random();
		byte[] saltBytes = new byte[8];
		random.nextBytes(saltBytes);

		StringBuffer salt = new StringBuffer();
		for (int i = 0; i < saltBytes.length; i++)
		{
		    // byte 값을 Hex 값으로 바꾸기.
		    salt.append(String.format("%02x",saltBytes[i]));
		}
		
		// 사용자가 만든 Password와 랜덤으로 생성한 salt를 섞어서 SHA256 암호화를 한다.
		String encrypt = getEncrypt(password, saltBytes);
		System.out.println(encrypt);
		String saltStr = StringUtil.byteToBase64(saltBytes);
		System.out.println(saltStr);
		encrypt = getEncrypt(password, StringUtil.base64ToByte(saltStr));
		System.out.println(encrypt);
		
		//48d6e481bb76ccdbf9bde47f18fe591928a4f402c327429f741bdbcf0df4131b
		//bbad4be31cf98f704408f7d6def48cc94a7d5ce2be64914fe4b16ecaf5d08807
		//214a869cd3c3e7229b8acce8642685783c90c38d3cd88ab3542d5b4f3da50a78
	}*/
	
	public static byte[] getSalt()throws NkiaException{
		java.util.Random random = new java.util.Random();
		byte[] saltBytes = new byte[8];
		random.nextBytes(saltBytes);

		StringBuilder salt = new StringBuilder();
		for (int i = 0; i < saltBytes.length; i++)
		{
		    // byte 값을 Hex 값으로 바꾸기.
		    salt.append(String.format("%02x",saltBytes[i]));
		}
		
		return saltBytes;
	}
	
	public static String getEncrypt(String source, byte[] salt)throws NkiaException, NoSuchAlgorithmException, UnsupportedEncodingException
    {
		// SALT 미사용시 시큐어코딩 가이드에 위배됨. 
		/*솔트사용시*/
		//*
        String encrypt = "";
        try
        {
            byte[] sourceBytes = source.getBytes();
            byte[] encryptBytes = new byte[sourceBytes.length + salt.length];
            System.arraycopy(sourceBytes, 0, encryptBytes, 0, sourceBytes.length);
            System.arraycopy(salt, 0, encryptBytes, sourceBytes.length, salt.length);

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(encryptBytes);

            byte[] byteData = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; ++i)
            {
                sb.append(Integer.toString((byteData[i] & 0xFF) + 256, 16).substring(1));
            }

            encrypt = sb.toString();
        } catch (NoSuchAlgorithmException e)
        {
            throw e;
        }
        return encrypt;
		//*/
        
        
        /*솔트미사용시*/
        /*
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(source.getBytes("UTF-8"));
        StringBuffer hexString = new StringBuffer();

        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
        	if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
        //*/
    }
	
	
}
