/*
 * @(#)PagingUtil.java              2013. 6. 3.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.common;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.constraint.BaseEnum;
import com.nkia.itg.base.application.util.json.JsonUtil;

public class PagingUtil {
	
	/**
	 * firstNum 과 lastNum을 구해서 넘겨 줍니다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException 
	 */
	public static ModelMap getFristEndNum(ModelMap paramMap){
		
		int start 	= (Integer)paramMap.get(BaseEnum.PAGER_START_PARAM.getString());
		int page 	= (Integer)paramMap.get(BaseEnum.PAGER_PAGE_PARAM.getString());
		int limit 	= (Integer)paramMap.get(BaseEnum.PAGER_LIMIT_PARAM.getString());
		int end 	= limit * page;
		
		paramMap.put("first_num", start + 1);
		paramMap.put("last_num", end);

		setSortColumn(paramMap);
		
		return paramMap;
	}
	
	/**
	 * firstNum 과 lastNum을 구해서 넘겨 줍니다.
	 * TODO
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException 
	 */
	public static ModelMap setSortColumn(ModelMap paramMap) {
		if(paramMap.containsKey("sort")){
			if(paramMap.get("sort") != null){
				JSONArray sorts = JsonUtil.convertStringToJsonArray((String) paramMap.get("sort"));
				if(sorts != null && sorts.size() > 0){
					JSONObject sortMap =  sorts.getJSONObject(0);
					String sortColumn = sortMap.get("property") + " " + sortMap.get("direction");
					paramMap.put("sort_column", sortColumn);
					paramMap.put("sort_property", sortMap.get("property"));
					paramMap.put("sort_direction", sortMap.get("direction"));
				}
			}
		}
		return paramMap;
	}
}
