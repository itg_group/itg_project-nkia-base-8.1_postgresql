/*
 * @(#)CipherWorld.java	2005. 11. 03
 *
 * Copyright 2005 Nkia.com, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.nkia.itg.base.application.exception.NkiaException;
import com.sun.xml.bind.v2.TODO;

/**
 * 암호화/복호화 에 사용하는 method 정의 
 * <pre>
 * Usage :
 *     사용방법 예시.
 * </pre>
 *
 * @see TODO 참고 클래스 나열.
 * @version <tt>$ Version: 1.0 $</tt>    date:2005. 11. 3
 * @author <a href="mailto:jipark@nkia.co.kr"> 박종일 </a>
 * 
 * <pre></pre>
 * 클래스 수정 내용 및 설명.
 * @version <tt>$ Reversion: 1.x $</tt>    date:2005. 11. 3
 * @author <a href="mailto:jipark@nkia.co.kr"> 박종일 </a>
 */
public interface CipherWorld {
	
	/**
	 * 주어진 메시지를 암호화 
	 * @param message 평문 
	 * @return 암호화된  byte[]
	 * @throws NkiaException
	 * @throws NoSuchAlgorithmException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public byte[] getEncryptedByte(String message) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException;
	
	/**
	 * 주어진 메시지를 암호화
	 * @param message 평문
	 * @return 암호화된 HexString 
	 * @throws NkiaException
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public String getEncryptedHexString(String message) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException;
	
	/**
	 * 암호화된 메시지를 복호화 
	 * @param message 암호화 메시지 byte[]
	 * @return 복호화된 String
	 * @throws NkiaException
	 * @throws NoSuchAlgorithmException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public String getDecryptedString(byte[] message) throws NkiaException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException;
	
	/**
	 * 암호화된 메시지를 복호화 
	 * @param message 암호화 메시지 String
	 * @return 복호화된 String
	 * @throws NkiaException
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public String getDecryptedString(String message) throws NkiaException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException;
}
