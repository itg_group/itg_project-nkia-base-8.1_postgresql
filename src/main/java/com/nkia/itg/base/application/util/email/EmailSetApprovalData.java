package com.nkia.itg.base.application.util.email;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationEmailMap;
import com.nkia.itg.base.application.config.NkiaApplicationEmailTemplateMap;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.application.util.security.CipherAES;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.nbpm.provide.common.api.NbpmClient;
import com.nkia.itg.system.user.service.UserService;


public class EmailSetApprovalData {
	
	public static EmailVO setData(Map mailInfoMap) throws NkiaException, JsonParseException, JsonMappingException, IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		String reqType = "";
		String templateId= "";
		Map dataMap = new HashMap();
		Map addInfoMap = new HashMap();
		
		//202104 시큐어코딩가이드 기준 적용 수정 - null포인트 역참조 처리.
		if(mailInfoMap != null) {
			reqType = (String) mailInfoMap.get("KEY"); //메일 요청 유형
			templateId = (String) mailInfoMap.get("TEMPLATE_ID"); //메일 템플릿
			dataMap = (Map) mailInfoMap.get("DATA"); //메일발송을 위한 데이터
			addInfoMap = (Map) mailInfoMap.get("ADD_DATA"); //메일 발송을 위한 추가 데이터
		}
		
		EmailVO emailVO = new EmailVO();
		String err_msg = ""; //에러메시지
		
		boolean setFlag = false;
		
		
		//SYS_MAIL_SENDER_INFO 테이블
		// MAIL_ID 구분으로 하나의 메일 서버인 경우는 변수로 기본 설정
		// 다중일 경우에는 조건에 추가 수정
		String emailId = EmailEnum.EMAIL_ID_1.getString();
		String emailConfigData = NkiaApplicationEmailMap.getMailInfo(emailId);
		String emailTemplateId = emailId + "==" + templateId;
		String emailTemplateData = NkiaApplicationEmailTemplateMap.getMailTemplateInfo(emailTemplateId);
		
		
		if (emailConfigData != null && emailTemplateData != null) {
			setFlag = true;
		} else {
			setFlag = false;
			err_msg = "NO DATA - CONFIG, TEMPLATE";
			
			if (emailConfigData == null) {
				err_msg = "NO DATA - CONFIG";
			}
			
			if (emailTemplateData == null) {
				err_msg = "NO DATA - TEMPLATE";
			}
		}
		emailVO.setConfig_flag(setFlag);
		emailVO.setErr_msg(err_msg);
		
		if (setFlag) {
			//메일발송정보
			ObjectMapper mapper = new ObjectMapper();
	        Map<String, Object> configMap = new HashMap<String, Object>();
	        configMap = mapper.readValue(emailConfigData, new TypeReference<Map<String, Object>>() {});
			
			emailVO.setEmail_job_type((String)configMap.get("EMAIL_JOB_TYPE"));
			emailVO.setHost((String)configMap.get("EMAIL_HOST"));
			int port = Integer.parseInt(String.valueOf(configMap.get("EMAIL_PORT")));
			emailVO.setPort(port);
			emailVO.setEmail_admin_id((String)configMap.get("EMAIL_ADMIN_ID"));
			emailVO.setEmail_admin_pw((String)configMap.get("EMAIL_ADMIN_PW"));
			emailVO.setEmail_admin_addr((String)configMap.get("EMAIL_ADMIN_ADDR"));
			emailVO.setEmail_multi_send_yn("N");
			
			//메일 템플릿 테이블 내의 발송 정보
	        Map<String, Object> templateMap = new HashMap<String, Object>();
	        templateMap = mapper.readValue(emailTemplateData, new TypeReference<Map<String, Object>>() {});
			
			//From (발송자) 정보
			emailVO.setFrom((String)templateMap.get("EMAIL_FROM"));
			
			
			
			String srId = (String)addInfoMap.get("SR_ID");
			dataMap.put("SR_ID", srId);
			dataMap.put("TITLE", (String)addInfoMap.get("TITLE"));
			dataMap.put("COMPLETE_COMMENT", (String)addInfoMap.get("COMPLETE_COMMENT"));
			dataMap.put("REQ_USER_NM", (String)addInfoMap.get("REQ_USER_NM"));
			dataMap.put("REQ_DT", (String)addInfoMap.get("REQ_DT"));
			dataMap.put("RCEPT_USER_NM", (String)addInfoMap.get("RCEPT_USER_NM"));
			dataMap.put("RCEPT_DT", (String)addInfoMap.get("RCEPT_DT"));
			dataMap.put("WORK_USER_NM", (String)addInfoMap.get("WORK_USER_NM"));
			dataMap.put("END_DT", (String)addInfoMap.get("END_DT"));
			dataMap.put("MAIL_CUSTOMER_ID_NM", (String)addInfoMap.get("MAIL_CUSTOMER_ID_NM"));
			
			//메일내용
			dataMap.put("CONTENT", (String)addInfoMap.get("CONTENT"));
			dataMap.put("REQ_TYPE_NM",(String)addInfoMap.get("REQ_TYPE_NM"));
			dataMap.put("TASKNAME",(String)addInfoMap.get("TASK_NAME"));
			dataMap.put("TASKID",(Long)addInfoMap.get("TASK_ID"));
			dataMap.put("USERID",(String)addInfoMap.get("TARGET_USER_ID"));
			dataMap.put("USERID_ENC",CipherAES.encode((String)addInfoMap.get("TARGET_USER_ID")));		// 시큐어코딩 관련으로 CipherAES 수정됨. 시큐어코딩 예외처리 적용 사이트에서 encode 값이 비정상일 수 있음. CipherAES 에서 algorithm/mod/padding 수정 필요
			dataMap.put("SR_ID_ENC", CipherAES.encode(srId));											// 시큐어코딩 관련으로 CipherAES 수정됨. 시큐어코딩 예외처리 적용 사이트에서 encode 값이 비정상일 수 있음. CipherAES 에서 algorithm/mod/padding 수정 필요
			String returnUrl = NkiaApplicationPropertiesMap.getProperty("Mail.Return.Url");
			returnUrl = returnUrl +  "/goApprovalPopup.do";
			dataMap.put("APPROVAL_URL", returnUrl);
			String content = getContent(templateId, dataMap);
			String title = "["+(String)dataMap.get("NODENAME")+"]"+(String)addInfoMap.get("TITLE");
			
			
//			List<HashMap> AtchList = selectAtchFileListForMail(null);
//			fileName = File.separator + reportName.substring(0, reportName.lastIndexOf(".")) + "_" + reportDate + reportName.substring(reportName.lastIndexOf("."),reportName.length());
//			String reportFullpath = reportPath + reportName;
//			File attach_File = new File(reportFullpath);
//			String attach_FileName = attach_File.getName();
//			Multipart mp = new MimeMultipart();
//			MimeBodyPart content_MimeBodyPart = new MimeBodyPart();
//			/////////내용설정
//			String strContent = "";
//			strContent = "대한항공 일일 종합 운영현황을 첨부와 같이 보고드립니다.<br/>";
//			strContent += " <br/>";
//			strContent += "감사합니다.<br/>";
//			content_MimeBodyPart.setContent(strContent, "text/html;charset=EUC-KR");
//			////////내용 설정 끝
//			content_MimeBodyPart.setHeader("Content-Transfer-Encoding", "base64");
//			mp.addBodyPart(content_MimeBodyPart);
//			MimeBodyPart attacheFile_MimeBodyPart = new MimeBodyPart();
//			attacheFile_MimeBodyPart.setFileName(MimeUtility.encodeText(attach_FileName,"EUC-KR","B"));
//			
//			FileDataSource filedataSource = new FileDataSource(attach_File);
//			DataHandler dataHandler = new DataHandler(filedataSource);
//			attacheFile_MimeBodyPart.setDataHandler(dataHandler);
//			
//			Path path = Paths.get(attach_File.getCanonicalPath());
//			String mimeType = Files.probeContentType(path);
//			attacheFile_MimeBodyPart.setHeader("Content-Type", mimeType);
//			
//			attacheFile_MimeBodyPart.setDescription(attach_FileName.split("\\.")[0],"EUC-KR");
//			mp.addBodyPart(attacheFile_MimeBodyPart);
//			///첨부파일
//			//메일내용
//			String content = "";
//			// 메일 템플릿을 사용여부 확인하지 않으면 메일 내용을 직접 담는다고 판단
//			 if (dataMap.get("CONTENT") != null && !"".equals((String) dataMap.get("CONTENT"))) {
//			//StringUtil.changeLineAlignmentForHtml - <br> 처리
//			content = StringUtil.changeLineAlignmentForHtml((String) dataMap.get("CONTENT"));
//			} else {
//			//content = getContent(templateId, dataMap);
//				content = "리포트메일";
//			}
//			
//			//수신자
//			ModelMap paramMap = new ModelMap();
//			UserService userService = (UserService)NkiaApplicationContext.getCtx().getBean("userService");
//			paramMap.put("user_id", "");
//			List userList = userService.searchUserListForReport(paramMap);
//			
//			//참조자
//			List userListCC = userService.searchUserListForReportCC(paramMap);
//			
//			//발송 데이터 정보
//			emailVO.setEmail_req_type(reqType);
//			emailVO.setEmail_id(emailId);
//			emailVO.setSubject(title);
//			emailVO.setContent(content);
//			emailVO.setTo_user_list(userList);
//			emailVO.setTo_cc_list(userListCC);
//			emailVO.setTemplate_id(templateId);
//			emailVO.setUrl(systemUrl);
//			emailVO.setMp(mp); 
//				
			//발송 데이터 정보
			emailVO.setEmail_req_type(reqType);
			emailVO.setEmail_id(emailId);
			emailVO.setSubject(title);
			emailVO.setContent(content);
			emailVO.setTo_user_list((List)dataMap.get("TO_USER_LIST"));
			emailVO.setTemplate_id(templateId);
		}
		return emailVO;
	}
	
	public static String getContent(String templateName, Map dataInfo) throws NkiaException, IOException {
		EmailTemplateGenerator etg = new EmailTemplateGenerator();
		//String result = etg.generateContent(templateName, masterInfo);
		String result = etg.customGenerateContent(templateName, dataInfo);
		return result;
	}
}
