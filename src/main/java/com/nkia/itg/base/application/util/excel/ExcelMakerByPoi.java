package com.nkia.itg.base.application.util.excel;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFBorderFormatting;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jivesoftware.smack.util.StringUtils;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTSheetProtection;
//import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.common.StringUtil;
import com.nkia.itg.base.service.ExcelFileService;
import com.nkia.itg.itam.batchEdit.service.BatchEditDownService;

import egovframework.com.cmm.util.EgovUserDetailsHelper;
import jxl.Cell;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/* 엑셀템플릿 자동 생성(By Poi) */
@Service("excelMakerByPoi")
public class ExcelMakerByPoi {

	@Resource(name = "excelFileService")
	private ExcelFileService excelFileService;
	
	public final static boolean FONT_ITALIC = true;
	public final static boolean FONT_ITALIC_NONE = false;
	public final static boolean STYLE_WRAP = true;
	public final static boolean STYLE_WRAP_NONE = false;
	public final static boolean STYLE_LOCK = true;
	public final static boolean STYLE_UNLOCK = false;
	

	
	/*
	 * [Map] mapExcelParams
	 * 	- [String] fileName 
	 *  - [String] optionsModifyValue : ( Y = 엑셀일괄수정을 위한 컬럼 생성 , N = 일반 엑셀 다운로드 , R = 엑셀일괄등록용 )
	 *  - [Map] excelAttrs 
	 *  	: [Map] sheet : sheet 가 List 인 경우 복수 Sheet 생성
	 *  		; *필수*[String] sheetName
	 *  		; *필수*[String] titleName
	 *  		; [MAP] infoMessage : 2행에 표시할 메시지( null == 다운로드 일시)
	 *  			. [String] message : 메시지
	 *  			. [Short] height : 행높이 ( 0 or null == 300 ( sDefaultRowHeight )
	 *  		; *필수*[String] headerNames : 콤마로 구분된 헤더명 목록
	 *  		* 이하 속성의 경우 headerNames 의 콤마분할 배열수와 같아야함. 다른 경우 무시될 수 있음.
	 *  		; *가급적필수*[String] includeColumns : 콤마로 구분된 헤더ID 목록 ( 헤더Column과 내용Column 일치를 위해 반드시 필요.)
	 *             *  headerNames 개수와 일치하지 않는 경우 내용은 MAP에 적재된 순서대로 임의로 생성 및 tblCols/notNull/{colTypes/htmlTypes/displayTypes} 기능 미적용  
	 *  		; [String] headerWrap : Y / N , 헤더 줄바꿈 적용 여부
	 *  		; [String] headerAutoSize : Y / N , 헤더 내용에 맞춰 컬럼With 자동 적용 ( columnWidths 가 있는 경우 무시 됨 )
	 *  		; [String] headerAligns : (일단 미구현) 콤마로 구분된 HeaderColumn Align 목록
	 *  		; [String] headerComments : 콤마로 구분된 Header Comment 목록
	 *  		; [String] columnWidths : 콤마로 구분된 Column width 목록
	 *  		; [String] columnAligns : 콤마로 구분된 Column Align 목록
	 *  		; [List] groupHeaders : (일단 미구현)
	 *  		; [String] freezeRow : 행 고정 , Row/Col 한쪽만 없는 경우 다른 한쪽은 자동으로 1로 지정
	 *  		; [String] freezeCol : 열 고정, Row/Col 한쪽만 없는 경우 다른 한쪽은 자동으로 1로 지정
	 *  		; [String] tblCols : 콤마로 구분된 테이블&컬럼 목록( TABLE_NAME.COLUM_NAME = AM_ASSET.ASSET_NM )
	 *  		; [String] notNull or notNulls : 콤마로 구분된 필수여부 목록
	 *  		; [String] colTypes : 콤마로 구분된 COLUMN TYPE 목록 ( VARCHAR2=VARCHAR=CHAR , DATE , NUMBER )
	 *  		; [String] htmlTypes : 콤마로 구분된 HTML TYPE 목록 ( SELECT=CHECK=RADIO, POPUP, DATE, TEXT=TEXTAREA ) 
	 *  		; [String] displayTypes : 콤마로 구분된 표시유형 목록 ( Y = 숨김 , U = 수정 , R = 조회 )
	 *  		; [List-Map] cellDisplayTypes : 셀 별로 lock 여부 결정
	 *  			. KEYVALUE : 첫번째 COLUMN 의 값
	 *  			. TBLCOL : 수정을 허용할 테이블.컬럼 정보(tblCols 에 존재하는 값만 허용됨)
	 * 
	 * [List] listData : First Object 가 List 인 경우 복수 Sheet ( mapExcelParams.excelAttrs.sheet 도 반드시 List 이어야 함 )
	 * 	- [MAP]
	 * 			; {Key} in includeColumns = {Value}
	 * 
	 * */
	public ModelMap makeExcelFileByPoi(Map mapExcelParams, List listData)throws Exception{
		//
		// 파라미터 및 주요 객체 선언
		ModelMap mapResult = new ModelMap();
		boolean isSucess = false;
		Map mapExcelAttrs = (Map)mapExcelParams.get("excelAttrs");
		String classId = (String)mapExcelParams.get("class_id");
		String classType = (String)mapExcelParams.get("class_type");
		
		//
		//Excel파일을 저장하기 위한 기본 설정
		String strFileName = (String)mapExcelParams.get("fileName");
		//String strRealFileName = strFileName+"_"+System.currentTimeMillis()+".xlsx";
		String strRealFileName = strFileName+"_"+DateUtil.getDateFormat()+".xlsx";
		String strTempExcelUploadPath = BaseConstraints.getExcelFileTempPath();
		String strFullPath = null;
		String strOptionsModifyValue = makeStringByMapValue(mapExcelParams.get("optionsModifyValue"));
		String addMsgYn = makeStringByMapValue(mapExcelParams.get("addMsgYn"));
		
		File directory = new File(strTempExcelUploadPath);
		if(!directory.exists()){	directory.mkdirs();	}

		strFullPath = strTempExcelUploadPath+File.separator+strRealFileName;
		
		//
		// 워트북 및 Default 서식 설정(Font/CellStyle) 
		short sDefaultRowHeight = (short)(15 * 20); // 실제 엑셀 높이 15 
		org.apache.poi.ss.usermodel.Workbook wbXlsx = new XSSFWorkbook();
		//org.apache.poi.ss.usermodel.Workbook wbXlsx = new XSSFWorkbook();
		//org.apache.poi.ss.usermodel.Workbook wbXlsx = new SXSSFWorkbook();
		
		HashMap<String,XSSFCellStyle> mapStyle = makeFontAndStyle(wbXlsx);
		
		boolean bHtmlTypeEqlWord4Popup = false; // 팝업속성 식별 : true(POPUP), false(POPUP 포함), QUERY(excelFile.selectAllCodeListForExcel/selectPopupCodeListQueryForExcel)도 동일하게 적용 
		
		
		try{
			if ( mapExcelAttrs != null ){
				Object objectFirst = null; 
				boolean bMultiSheet = false;
				List listSheet = (List) mapExcelAttrs.get("sheet");
				
				if ( listData != null ){ if ( listData.size() > 0 ){ objectFirst = listData.get(0);}} // 데이터목록의 첫번째 객체 로딩
				if ( objectFirst instanceof List ){	bMultiSheet = true; } // 복수 Sheet 여부 결정(by 데이터목록의 첫번째 객체의 자료형)
				
				if ( listSheet.size() > 0 ){
					//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
					// Sheet 관련 객체 선언
					org.apache.poi.ss.usermodel.Sheet sheetCrnt = null;
					org.apache.poi.ss.usermodel.Row rowThis = null;
					org.apache.poi.ss.usermodel.Cell cellThis = null;
					int nRow = 0, nCol = 0;
					
					for ( int s = 0 ; s < listSheet.size() ; s ++ ){
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// Sheet 환결 로딩 및 초기화
						int nStartDataRow = 0;
						HashMap mapTblColtoColumn = new HashMap();
						HashMap mapTblColCodeRow = new HashMap(); // CODE목록에서 각 코드(열)별로 마지막 행
						HashMap mapSheetConfig = (HashMap)listSheet.get(s);
						String strSheetName = makeStringByMapValue(mapSheetConfig.get("sheetName"));
						String strTitleName = makeStringByMapValue(mapSheetConfig.get("titleName"));
						String strHeaderNames = makeStringByMapValue(mapSheetConfig.get("headerNames"));
						String strHeaderAligns = makeStringByMapValue(mapSheetConfig.get("headerAligns"));
						String strHeaderComments = makeStringByMapValue(mapSheetConfig.get("headerComments"));
						String strIncludeColumns = makeStringByMapValue(mapSheetConfig.get("includeColumns"));
						
						String strTblCols = makeStringByMapValue(mapSheetConfig.get("tblCols"));
						String strNotNulls = makeStringByMapValue(mapSheetConfig.get("notNull"));
						if ( strNotNulls.equals("") == true ) { strNotNulls = makeStringByMapValue(mapSheetConfig.get("notNulls")); }
						String strColTypes = makeStringByMapValue(mapSheetConfig.get("colTypes"));
						String strHtmlTypes = makeStringByMapValue(mapSheetConfig.get("htmlTypes"));
						String strDisplayTypes = makeStringByMapValue(mapSheetConfig.get("displayTypes"));
						
						String strColumnWidths = makeStringByMapValue(mapSheetConfig.get("columnWidths"));
						String strColumnAligns = makeStringByMapValue(mapSheetConfig.get("columnAligns"));
						
						String strHeaderWrap = makeStringByMapValue(mapSheetConfig.get("headerWrap"));
						String strHeaderAutoSize = makeStringByMapValue(mapSheetConfig.get("headerAutoSize"));
						String strFreezeRow = makeStringByMapValue(mapSheetConfig.get("freezeRow"));
						String strFreezeCol = makeStringByMapValue(mapSheetConfig.get("freezeCol"));
						
						HashMap mapInfoMessage = (HashMap) mapSheetConfig.get("infoMessage");
						List listCellDisplayTypes = (List) mapSheetConfig.get("cellDisplayTypes");
						
						//List groupHeaders = (List)mapSheetConfig.get("groupHeaders");
						
						String[] strarrHeaderName = StringUtil.explode(",", strHeaderNames);
						String[] strarrHeaderAlign = StringUtil.explode(",", strHeaderAligns);
						String[] strarrHeaderComment = StringUtil.explode(",", strHeaderComments);
						String[] strarrIncludeColumn = StringUtil.explode(",", strIncludeColumns);
						
						String[] strarrTblCol = StringUtil.explode(",", strTblCols);
						String[] strarrNotNull = StringUtil.explode(",", strNotNulls);
						String[] strarrColType = StringUtil.explode(",", strColTypes);
						String[] strarrHtmlType = StringUtil.explode(",", strHtmlTypes);
						String[] strarrDisplayType = StringUtil.explode(",", strDisplayTypes);
						
						String[] strarrColumnWidth = StringUtil.explode(",", strColumnWidths);
						String[] strarrColumnAlign = StringUtil.explode(",", strColumnAligns);
						
						//System.out.println( "strarrTblCol.length : " + String.valueOf(strarrTblCol.length));
						
						// 셀별로 수정을 허용할지를 결정 
						HashMap mapCellDisplayTypes = null;
						if ( listCellDisplayTypes != null ){
							if ( listCellDisplayTypes.size() > 0 ){
								mapCellDisplayTypes = new HashMap();
								for ( int cd = 0 ; cd < listCellDisplayTypes.size() ; cd++){
									HashMap tempCellTypes = (HashMap) listCellDisplayTypes.get(cd);
									if ( tempCellTypes != null ){
										mapCellDisplayTypes.put(
												StringUtil.parseString(tempCellTypes.get("KEYVALUE"))+"_"+StringUtil.parseString(tempCellTypes.get("TBLCOL"))
												,"EDIT");
									}
								} // for cd
							}
						} // if ( listCellDisplayTypes != null ){
						
						// 첫컬럼을 KEY 사용
						HashMap mapFirstColumnValue = new HashMap(); // key : row , value : 값

						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 유효성 검증(단일 시트일때만 적용 가능) - 코드 탭 포함 ( 하단에 코드 적용 소스 별도로 있음 ) 
						HashMap mapCodePosition = new HashMap();	// CODELIST 탭에 코드(CODE_GRP_ID)가 위치한 COLUMN 위치정보 	>> {CODE_ID} : {CODELIST.EXCEL_CODE_COLUMN_NUM} ....
						HashMap mapColumn2CodePosition = new HashMap(); // 실제 DATA COLUMN 위치와 CODELIST 컬럼 위치 매핑정보  	>> {DATA.EXCEL_DATA_COLUMN_NUM} : {CODELIST.EXCEL_CODE_COLUMN_NUM}
						int nCodeCount = 0;
						
						if ( ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true )
								&& strarrIncludeColumn.length == strarrHeaderName.length && strarrTblCol.length == strarrHeaderName.length 
								&& listSheet.size() == 1 && s == 0) {
							// 컬럼별 위치(열)
							for ( int h = 0 ; h < strarrTblCol.length ; h++){
								mapTblColtoColumn.put(strarrTblCol[h], h);
							} // for 
							
							// SYS_CODE & POPUP_CODE 로딩  --------------------------------------------------------------------------------------
							///*
							HashMap mapQueryParam = new HashMap(); // 팝업코드 조회시 옵션에서 사용되는 파라미터도 추가되어야 함
							mapQueryParam.put("arrTblCol",strarrTblCol);
							mapQueryParam.put("class_id", classId);
							mapQueryParam.put("class_type", classType);
							
							// 엑셀콤보서식 출력을 위한 팝업코드 목록 조회(AM_POPUP_MNG.POPUP_CODE_QRY_OPT => 구분자 : 더블콤마(,,) , 파라미터구분자 : ==
							List listPopupCodeQuery = excelFileService.selectPopupCodeListQueryForExcel(mapQueryParam); // 파라미터는 NULL 이어도 무방함.
							String PopupCodeQuery = "";
							if ( listPopupCodeQuery != null ) {
								for ( int pq = 0 ; pq < listPopupCodeQuery.size() ; pq++) {
									HashMap mapPopupCode = (HashMap) listPopupCodeQuery.get(pq);
									if ( StringUtil.parseString(mapPopupCode.get("POPUP_CODE_QRY")).trim().equals("") == false ) {
										PopupCodeQuery += "\n UNION ALL " + StringUtil.parseString(mapPopupCode.get("POPUP_CODE_QRY")).trim();
										String strTempPCQOption = StringUtil.parseString(mapPopupCode.get("POPUP_CODE_QRY_OPT")).trim();
										if ( strTempPCQOption.equals("") == false ) {
											String [] arrTemp1 = strTempPCQOption.split(",,");
											if ( arrTemp1 != null ) {
												for ( int po = 0 ; po < arrTemp1.length ; po++) {
													if ( arrTemp1[po].trim().equals("") == false ) {
														String [] arrTemp2 = arrTemp1[po].trim().split("==");
														String strTempOption = arrTemp2[0];
														for ( int poo = 1 ; poo < arrTemp2.length ; poo++) {
															if ( arrTemp2[poo].trim().equals("") == false && StringUtil.parseString(mapQueryParam.get(arrTemp2[poo].trim())).equals("")== false) { 
																strTempOption = strTempOption.replace("{"+(poo-1)+"}", StringUtil.parseString(mapQueryParam.get(arrTemp2[poo].trim())) );
															}
														}
														if ( arrTemp2.length == 1 || strTempOption.equals(arrTemp2[0]) == false ) { // 파라미터가 있는 경우 파라미터 값이 존재하는 경우만 적용
															PopupCodeQuery += " AND " + strTempOption;
														}
														/*
														if ( arrTemp2.length >= 2 && StringUtil.parseString(mapQueryParam.get(arrTemp2[1].trim())).equals("") == false ) {
															PopupCodeQuery += " AND " + arrTemp2[0].trim() + " = '" + StringUtil.parseString(mapQueryParam.get(arrTemp2[1].trim()))+ "'";
														}
														*/
													}
												} // for po
											}
										}
									}
								} // for pq
							} // if ( listPopupCodeQuery != null ) {
								
							// 코드목록 조회 ( 팝업코드 포함 ) 
							mapQueryParam.put("popup_code_query", PopupCodeQuery);
							List listSysCode = excelFileService.selectAllCodeListForExcel(mapQueryParam); // NEW_ORDER ASC 로 정렬된 상태이어야 함.
												
							// 코드정보 Sheet 생성
							if ( listSysCode != null ) { if ( listSysCode.size() > 0 ){
								sheetCrnt = wbXlsx.createSheet("CODELIST");
								nRow = 0; nCol = 0;
								int nRowCrnt = -1;
								int nRowTarget = -1;
								int nColTarget = -1;
								int nMakeColumn = -1;
								for ( int sc = 0 ; sc < listSysCode.size() ; sc++){
									HashMap mapCode = (HashMap) listSysCode.get(sc);
									int nColCode = -1; // 실제 코드가 저장되는 COLUMN , 0부터 시작
									nRowTarget = Integer.valueOf(this.makeStringByMapValue(mapCode.get("NEW_ORDER")));
									
									if ( mapCodePosition.get(makeStringByMapValue(mapCode.get("CODE_GRP_ID"))) == null ){
										nCodeCount++;
										mapCodePosition.put(makeStringByMapValue(mapCode.get("CODE_GRP_ID")), nCodeCount);
										//mapColumn2CodePosition.put(nRowTarget, nCodeCount);
										nColCode = nCodeCount-1;
										
										System.out.print(" -- EXCEL , CODE = " + makeStringByMapValue(mapCode.get("CODE_GRP_ID")) + " : " + String.valueOf(nColCode)   );
									}else{
										nColCode = Integer.valueOf(this.makeStringByMapValue( mapCodePosition.get(makeStringByMapValue(mapCode.get("CODE_GRP_ID"))))) - 1;
									}
									
									// NEW_ORDER == ROW NUMBER
									if ( nRowTarget > nRowCrnt ){
										nRowCrnt = nRowTarget;
										rowThis = sheetCrnt.createRow(nRow++);
										nMakeColumn = -1;
									} // if ( nRowTarget > nRowCrnt ){
									
									
									String strColTarget = makeStringByMapValue(mapTblColtoColumn.get(  makeStringByMapValue(mapCode.get("CKEY")) ));
									if ( strColTarget.equals("") == false  ){
										nColTarget = Integer.valueOf( strColTarget );
										
										mapColumn2CodePosition.put(nColTarget, nColCode); // 실제 COLUMN 의 코드값 COLUMN 위치
										// 중간에 비어 있는 경우 자동 생성
										/*
										if ( nMakeColumn < nColTarget-1 ) {
											for ( int mc = nMakeColumn + 1 ; mc < nColTarget ; mc++){
												cellThis = rowThis.createCell(mc);
											}
										}
										cellThis = rowThis.createCell(nColTarget);
										if ( nMakeColumn < nColTarget ) { nMakeColumn = nColTarget;}
										*/
										if ( nMakeColumn < nColCode-1 ) {
											for ( int mc = nMakeColumn + 1 ; mc < nColCode ; mc++){
												cellThis = rowThis.createCell(mc);
											}
										}
										cellThis = rowThis.createCell(nColCode);
										if ( nMakeColumn < nColCode ) { nMakeColumn = nColCode;}

										
										cellThis.setCellValue(makeStringByMapValue(mapCode.get("CODE_ID_NM")));
										//cellThis.setCellStyle(mapStyle.get("styleBodyLeft")); 속도/메모리 개선
										sheetCrnt.setColumnWidth(nColTarget, 1024 );
										//sheetCrnt.autoSizeColumn(nColTarget); 속도/메모리 개선
										
										mapTblColCodeRow.put(nColTarget, String.valueOf(nRow-1));
									}
								} // for sc
								sheetCrnt.setSelected(false);
								wbXlsx.setSheetHidden(wbXlsx.getSheetIndex("CODELIST"), true);
								//mapTblColtoColumn
							} } // if ( listSysCode != null ) { if ( listSysCode.size() > 0 ){
							//*/
						} // if 유효성 검증
						
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 데이터 시트생성
						sheetCrnt = wbXlsx.createSheet(strSheetName);
						nRow = 0;
						nCol = 0;
						
						// 일괄수정모드 일 경우, 잠금기능 활성화
						if ( strOptionsModifyValue.toUpperCase().equals("Y") == true ) { 
							((XSSFSheet)sheetCrnt).enableLocking();
							CTSheetProtection sheetProtection = ((XSSFSheet) sheetCrnt).getCTWorksheet().getSheetProtection();
							sheetProtection.setFormatColumns(false); // 셀 너비 조정 등 잠금 해제
							sheetProtection.setFormatRows(false); // 셀 높이 조정 등 장금 해제
							// System.out.println("isFormatColumnsLocked : " + String.valueOf ( ((XSSFSheet)sheetCrnt).isFormatColumnsLocked() ) );
						}
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 타이틀 ( 다운로드 일시 포함 )
						rowThis = sheetCrnt.createRow(nRow); // nRow = 0
						rowThis.setHeight((short)600);
						nCol=0;
						sheetCrnt.setColumnWidth(0, 20 * 40); // 임시( 헤더에서 재지정 )
						sheetCrnt.setColumnWidth(1, 20 * 30); // 임시( 헤더에서 재지정 )
						sheetCrnt.addMergedRegion(new CellRangeAddress(nRow,nRow,0,strarrHeaderName.length -1));
						cellThis = rowThis.createCell(0);
						cellThis.setCellValue(strTitleName);
						cellThis.setCellStyle(mapStyle.get("styleTitle"));
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 타이틀 하단 메시지( 다운로드 일시 등 )
						nRow++;
						rowThis = sheetCrnt.createRow(nRow); // nRow = 1
						nCol=0;
						cellThis = rowThis.createCell(0);
						if ( mapInfoMessage == null ){
							mapInfoMessage = new HashMap(); mapInfoMessage.put("message","DOWNLOAD DATE"); mapInfoMessage.put("height",(short)(sDefaultRowHeight + 60));
						}
						if ( mapInfoMessage.get("height") == null ){ mapInfoMessage.put("height",(short)(sDefaultRowHeight + 60) ); }
						else{
							if ( makeStringByMapValue(mapInfoMessage.get("height")).matches("-?\\d+(\\.\\d+)?") == false ){
								mapInfoMessage.put("height",(short)(sDefaultRowHeight + 60));
							}
						}
						if ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true ){
							if(addMsgYn == "") {
								int nTempSubTitleHeight = 75;
								String strTempMsg = "※ 작성 요령 \n - 빨간색(또는 *표시)항목은 필수입력 대상입니다. \n - 파란색 항목은 반드시 선택목록 중에서만 선택/입력/붙여넣기 해주십시오.";
								strTempMsg += "\n - 엑셀 양식/서식은 절대 수정하지 마십시오.";
								if ( strOptionsModifyValue.toUpperCase().equals("R") == true ){
									strTempMsg += "\n    (셀 선택목록은 1000행까지만 있습니다. 추가 필요시 기존 행 전체를 복사해서 사용하십시오. )";
								}else {
								}
								strTempMsg += "\n - 다른 엑셀 파일 등에서 [복사&붙여넣기]를 할때에는 반드시 [값 붙여넣기]를 이용하십시오.(셀 서식 유지)";
								mapInfoMessage.put("message",strTempMsg);
								mapInfoMessage.put("height",(short)(nTempSubTitleHeight * 20));
							}
							sheetCrnt.addMergedRegion(new CellRangeAddress(nRow,nRow,0,2));
							cellThis.setCellStyle(mapStyle.get("styleBodyLeftWrap"));
						}
						else {
							cellThis.setCellStyle(mapStyle.get("styleBodyLeft"));
						}
						
						rowThis.setHeight((short)mapInfoMessage.get("height"));

						String strTempValue = makeStringByMapValue(mapInfoMessage.get("message"));
						if ( strTempValue.equals("DOWNLOAD DATE") == true ){ cellThis.setCellValue( "[다운로드일] "+ DateUtil.getDate("-") ); }
						else { cellThis.setCellValue( strTempValue ); }
						if ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true){
							cellThis = rowThis.createCell(1);	cellThis.setCellStyle(mapStyle.get("styleBodyLeftWrap"));
							cellThis = rowThis.createCell(2);	cellThis.setCellStyle(mapStyle.get("styleBodyLeftWrap"));
						}
						
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 히든 정보 ( TABLE_NAME.COLUMN_NAME ) 
						if ( strarrTblCol.length == strarrHeaderName.length ){
							nRow++;
							rowThis = sheetCrnt.createRow(nRow); // nRow = 2
							rowThis.setHeight((short) sDefaultRowHeight);
							nCol=0;
							for ( int h = 0 ; h < strarrTblCol.length ; h++){
								nCol++;
								cellThis = rowThis.createCell(h);
								cellThis.setCellValue( strarrTblCol[h] );
								cellThis.setCellStyle( mapStyle.get("styleHidden") );
							} // for 
						}
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 헤더
						nRow++;
						rowThis = sheetCrnt.createRow(nRow); // nRow = 2 + 히든정보YN
						rowThis.setHeight((short) 500);
						nCol=0;
						for ( int h = 0 ; h < strarrHeaderName.length ; h++){
							nCol++;
							cellThis = rowThis.createCell(h);
							XSSFCellStyle styleThisHeader = mapStyle.get("styleHeader");
							String strTempHeaderName = strarrHeaderName[h];
							
							// 필수(수정모드Only)
							if ( ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true )
									&& strarrNotNull.length == strarrHeaderName.length){
								if ( strarrNotNull[h].toUpperCase().equals("Y") == true ){ 
									strTempHeaderName = "*" + strTempHeaderName;
									styleThisHeader = mapStyle.get("styleHeaderRed"); /*cellThis.setCellStyle(mapStyle.get("styleHeaderRed"));*/
								}
							}
							// 선택형(수정모드Only)
							if ( ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true )
									&& strarrHtmlType.length == strarrHeaderName.length){
								if ( strarrHtmlType[h].toUpperCase().equals("SELECT") == true ){ styleThisHeader = mapStyle.get("styleHeaderSkyblue"); /*cellThis.setCellStyle(mapStyle.get("styleHeaderSkyblue"));*/ }
								else if ( ( bHtmlTypeEqlWord4Popup == true && strarrHtmlType[h].toUpperCase().equals("POPUP") == true ) || ( bHtmlTypeEqlWord4Popup == false && strarrHtmlType[h].toUpperCase().indexOf("POPUP") > -1 )){ 
									styleThisHeader = mapStyle.get("styleHeaderSkyblue");/*cellThis.setCellStyle(mapStyle.get("styleHeaderSkyblue"));*/ 
								}
								else {
									if ( styleThisHeader.equals(mapStyle.get("styleHeader")) == true ){ styleThisHeader = mapStyle.get("styleHeaderYellow");/*cellThis.setCellStyle(mapStyle.get("styleHeaderYellow"));*/ }
								}
							}
							
							// 입력값 유형 안내(수정모드Only) , strarrColType
							if ( ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true )
									&& strarrColType.length == strarrHeaderName.length){
								if ( strarrColType[h].toUpperCase().equals("NUMBER") == true ){strTempHeaderName += "\n(콤마제외. 숫자만)";}
								if ( strarrColType[h].toUpperCase().equals("DATE") == true ){strTempHeaderName += "\n(YYYY-MM-DD)";}
								//System.out.println("-- EXCEL : HEADER : " + strarrHeaderName[h] + ", " + strarrColType[h] + "");
							}

							// 헤더 줄바꿈
							if ( strHeaderWrap.toUpperCase().equals("Y") == true ){
								styleThisHeader.setWrapText(true);
								//cellThis.setCellStyle(mapStyle.get("styleHeaderWrap"));
							}
							else {
								styleThisHeader.setWrapText(false);
							}
							
							cellThis.setCellStyle(styleThisHeader);

							cellThis.setCellValue( strTempHeaderName );
							boolean bColWidth = false;
							if ( strarrColumnWidth.length-1 >= h ){ bColWidth = strarrColumnWidth[h].matches("-?\\d+(\\.\\d+)?"); }
							if ( bColWidth == true ){ // 지정된 컬럼Width 가 있는 경우
								sheetCrnt.setColumnWidth(nCol-1, Integer.valueOf(strarrColumnWidth[h]) * 40 ); // COLUMN 넓이
							} else { // 지정된 컬럼Width 가 없는 경우
								if ( strHeaderAutoSize.toUpperCase().equals("Y") == true ){
									int nTempSize = 512 + ( strarrHeaderName[h].length() * 256 ) 
											+ ( ( strarrHeaderName[h].getBytes().length - strarrHeaderName[h].length() ) * 128 );
									sheetCrnt.setColumnWidth(nCol-1, nTempSize ); // COLUMN 넓이 ( 1 Bytes = 대략 256 ) , 헤더Name 길이에 맞춰 자동 생성
								}else {
									sheetCrnt.setColumnWidth(nCol-1, 256 * 25 ); // COLUMN 넓이 ( 1 Bytes = 대략 256 ) , 고정Width
								}
								//System.out.println(strarrHeaderName[h] + " : " + String.valueOf( nTempSize )); // ########################################################################################################### LOG
							}
							if ( strarrHeaderComment.length-1  > h ){ 
								String strComment = String.valueOf(strarrHeaderComment[h].trim());
								if ( strComment.equals("null") == false && strComment.equals("") == false ){ 
									CreationHelper heaperHeader = wbXlsx.getCreationHelper();
									ClientAnchor anchorHeader = heaperHeader.createClientAnchor();
									anchorHeader.setCol1(nCol);		anchorHeader.setCol2(nCol+1);
									anchorHeader.setRow1(nRow-1);	anchorHeader.setRow2(nRow);
									Comment commentThis = (sheetCrnt.createDrawingPatriarch()).createCellComment(anchorHeader);
									RichTextString rtstrComment = heaperHeader.createRichTextString(strComment);
									commentThis.setString(rtstrComment);
									commentThis.setVisible(true);
									cellThis.setCellComment(commentThis);
								} // if ( strComment.equals("null") == false && strComment.equals("") == false ){
							} // if ( strarrHeaderComment.length-1  > h ){ 
							
						} // for h ( header )
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 데이터목록 로딩
						List listSheetData = null;
						if ( bMultiSheet == true ) {
							if ( listData.size() < listSheet.size() ) { listSheetData = null; } 
							else { listSheetData = (List) listData.get(s); }  
						} else {listSheetData = listData; }
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 내용CELL
						boolean bExistContent = false;
						if ( listSheetData != null ) {if( listSheetData.size() > 0 ){ bExistContent = true;}}
						// 등록용일때에는 빈 행 추가
						if ( bExistContent == false && strOptionsModifyValue.toUpperCase().equals("R") == true ){
							bExistContent = true;
							int nSampleLine = 10;
							listSheetData = new ArrayList();
							for ( int r = 0 ; r < nSampleLine ; r++){
								HashMap mapTempRecord = new HashMap();
								for ( int c = 0 ; c < strarrIncludeColumn.length ; c++ ){
									mapTempRecord.put(strarrIncludeColumn[c].toUpperCase(), "");
								}
								listSheetData.add(mapTempRecord);
							}
						}
						
						if ( bExistContent == true ){
							boolean bUseColumnId = false;
							nStartDataRow = nRow+1;
							if (strarrIncludeColumn.length == strarrHeaderName.length ){ bUseColumnId = true;}
							for ( int r = 0 ; r < listSheetData.size() ; r ++ ){
								nRow++;
								rowThis = sheetCrnt.createRow(nRow); // nRow = ( 2 + 히든정보YN )++
								if("RELEASE_LIST".equals(strFileName)){  //파일명이 RELEASE_LIST 일경우에
									rowThis.setHeight((short) -1); 
								}else{
									rowThis.setHeight(sDefaultRowHeight);
								}
								nCol=0;
								HashMap mapRecord = (HashMap) listSheetData.get(r);
								if ( bUseColumnId == true ){
									for ( int c = 0 ; c < strarrIncludeColumn.length ; c++ ){
										cellThis = rowThis.createCell(nCol++);
										String strLabelValue = setLabel(mapRecord.get(strarrIncludeColumn[c].toUpperCase()));
										if ( strLabelValue == null ) 	{ strLabelValue = ""; } // 빈 값일 때 표시 내용
										cellThis.setCellValue(strLabelValue);
										if ( c == 0 ){ mapFirstColumnValue.put(StringUtil.parseString(r), strLabelValue);}
										if ( strarrColumnAlign.length == strarrHeaderName.length){
											if ( strarrColumnAlign[c].toUpperCase().equals("LEFT") == true )		{ cellThis.setCellStyle(mapStyle.get("styleBodyLeft"));} 
											else if ( strarrColumnAlign[c].toUpperCase().equals("RIGHT") == true )	{ cellThis.setCellStyle(mapStyle.get("styleBodyRight"));} 
											else { cellThis.setCellStyle(mapStyle.get("styleBodyCenter")); }
										}else { cellThis.setCellStyle(mapStyle.get("styleBodyCenter"));}
/*										
										if("RELEASE_LIST".equals(strFileName)){  //파일명이 RELEASE_LIST 일경우에
											if("PAGE_TITLE".equals(strarrIncludeColumn[c]) || "FILE_PATH".equals(strarrIncludeColumn[c]) || "CHANGE_SET".equals(strarrIncludeColumn[c])){
												if ( strarrColumnAlign[c].toUpperCase().equals("LEFT") == true )		{ cellThis.setCellStyle(mapStyle.get("styleBodyLeftEnter"));}
												else{cellThis.setCellStyle(mapStyle.get("styleBodyCenterEnter"));}
											}
										}
*/										
										
										if ( ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true )
												&& strarrDisplayType.length == strarrHeaderName.length){
											// 셀 잠금(컬럼 기준)
											if ( strarrDisplayType[c].toUpperCase().equals("R") == true ){ 
												cellThis.setCellStyle(mapStyle.get("styleBodyLock")); 
											}else {
												if ( strarrColType.length == strarrHeaderName.length ){
													if ( strarrColType[c].equals("NUMBER") == true ){
														cellThis.setCellStyle(mapStyle.get("styleBodyRightNumber"));
													}
												}
											}
											// 셀 잠금(셀 기준)
											if ( mapCellDisplayTypes != null && strarrTblCol.length == strarrHeaderName.length ){
												String strTempKey = StringUtil.parseString(mapFirstColumnValue.get(StringUtil.parseString(r))) + "_" + strarrTblCol[c];
												if ( StringUtil.parseString(mapCellDisplayTypes.get(strTempKey)).equals("EDIT") == false ){
													cellThis.setCellStyle(mapStyle.get("styleBodyLock")); 
												}
											}
										} // EDIT MODE ( ModifyValue : Y or R )


										// ########################################################################################################### LOG
										//System.out.println("LOCKED ["+String.valueOf(nRow)+", "+String.valueOf(c)+"]: " + String.valueOf(cellThis.getCellStyle().getLocked()));
									} // for c ( column )
								} /*if ( bUseColumnId == true ){*/
								else{ 
									Iterator iter = mapRecord.keySet().iterator();
									while ( iter.hasNext() ){
										cellThis = rowThis.createCell(nCol++);
										String strLabelValue = setLabel(mapRecord.get(iter.next()));
										if ( strLabelValue == null ) { strLabelValue = ""; } // 빈 값일 때 표시 내용
										cellThis.setCellValue(strLabelValue);
										/*  // strarrIncludeColumn 가 없는 경우 적용 불가
										if ( strarrColumnAlign.length == strarrHeaderName.length){
											if ( strarrColumnAlign[nCol-1].toUpperCase().equals("LEFT") == true )		{ cellThis.setCellStyle(styleBodyLeft);} 
											else if ( strarrColumnAlign[nCol-1].toUpperCase().equals("RIGHT") == true )	{ cellThis.setCellStyle(styleBodyRight);} 
											else { cellThis.setCellStyle(styleBodyCenter); }
										}else { cellThis.setCellStyle(styleBodyCenter);}
										*/
									} // while
								} /*else ( bUseColumnId == true ){*/
							} // for r ( row )
							
							// 틀고정
							if ( strFreezeRow.equals("") == false || strFreezeCol.equals("") == false ){
								int nFreezeRow = -1;
								int nFreezeCol = -1;
							    if ( strFreezeRow.equals("") == false && strFreezeRow.matches("(^[0-9]*$)") == true ) { nFreezeRow = Integer.valueOf(strFreezeRow); } 
							    if ( strFreezeCol.equals("") == false && strFreezeCol.matches("(^[0-9]*$)") == true ) { nFreezeCol = Integer.valueOf(strFreezeCol); } 
								sheetCrnt.createFreezePane((nFreezeCol==-1 ? 0 : nFreezeCol - 1 ), (nFreezeRow==-1 ? 0 : nFreezeRow - 1 )); // 열 , 행 고정
							}
						} // if ( bExistContent == true )
						else {
							nRow++;
							rowThis = sheetCrnt.createRow(nRow);
							rowThis.setHeight(sDefaultRowHeight);
							nCol=0;
							sheetCrnt.addMergedRegion(new CellRangeAddress(nRow,nRow,0,strarrHeaderName.length -1));
							cellThis = rowThis.createCell(0);
							cellThis.setCellValue( "결과가 없습니다." );
							cellThis.setCellStyle(mapStyle.get("styleBodyLeft"));
							for ( int c = 1 ; c < strarrHeaderName.length ; c++ ){ rowThis.createCell(c).setCellStyle(mapStyle.get("styleBodyLeft"));}
						} // else ( bExistContent == false )
						
						wbXlsx.setSheetOrder(strSheetName, s);
						wbXlsx.setActiveSheet(wbXlsx.getSheetIndex(strSheetName));
						sheetCrnt.getRow(0).getCell(0).setAsActiveCell();
						
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
						// 유효성 검증(단일 시트일때만 적용 가능) - 적용 ( 상단에 코드 소스 별도로 있음) , nStartDataRow
						// 매핑정보 : mapColumn2CodePosition
						if ( ( strOptionsModifyValue.toUpperCase().equals("Y") == true || strOptionsModifyValue.toUpperCase().equals("R") == true ) 
								&& strarrIncludeColumn.length == strarrHeaderName.length && strarrTblCol.length == strarrHeaderName.length 
								&& listSheet.size() == 1 && s == 0) {
							int nLastRow = nRow;
							if ( strOptionsModifyValue.toUpperCase().equals("R") == true ) { nLastRow = 1000; } // 등록용인경우 1000 line 까지 적용
							for ( int h = 0 ; h < strarrHeaderName.length ; h++){
								// NUMBER ( 0 이상 )
								if ( strarrColType.length == strarrHeaderName.length){
									if ( strarrColType[h].equals("NUMBER") == true ){
										XSSFDataValidationHelper dvhCrnt = new XSSFDataValidationHelper((XSSFSheet) sheetCrnt);
										CellRangeAddressList cralContent = new CellRangeAddressList(nStartDataRow, nLastRow, h, h);
										XSSFDataValidationConstraint dvcNumber 
											= (XSSFDataValidationConstraint) dvhCrnt.createNumericConstraint(XSSFDataValidationConstraint.ValidationType.INTEGER, XSSFDataValidationConstraint.OperatorType.GREATER_OR_EQUAL, "0", null); 
										XSSFDataValidation validContent = (XSSFDataValidation) dvhCrnt.createValidation(dvcNumber, cralContent);
										validContent.setShowErrorBox(true);
										sheetCrnt.addValidationData(validContent);
									}
								}
								// SELECT / POPUP 등
								if ( makeStringByMapValue(mapTblColCodeRow.get(h)).equals("") == false ){
									int nMaxRowforCode = Integer.valueOf( makeStringByMapValue(mapTblColCodeRow.get(h)) )+1; // poi 는 0 부터 시작, 엑셀은 1부터 시작
									//int nH = h;
									int nH = Integer.valueOf(makeStringByMapValue(mapColumn2CodePosition.get(h)));
									int nColPos = nH;
									int nColPosFirst = -1;
									if ( nH+1 > 26 ){
										nColPos = ( (nH+1) % 26 ) - 1;
										nColPosFirst = ( ( (nH+1) - ( (nH+1) % 26 ) ) / 26 ) - 1;
									}
									String strColPos = (nColPosFirst > -1 ? Character.toString((char)((int)'A' + nColPosFirst )) : "") + Character.toString((char)((int)'A' + nColPos ));
									String strColPosName = "CODELIST!$"+strColPos+"$1:$"+strColPos+"$" + String.valueOf(nMaxRowforCode);
									XSSFDataValidationHelper dvhCrnt = new XSSFDataValidationHelper((XSSFSheet) sheetCrnt);
									CellRangeAddressList cralContent = new CellRangeAddressList(nStartDataRow, nLastRow, h, h);
									XSSFDataValidationConstraint dvcCode = (XSSFDataValidationConstraint) 
											dvhCrnt.createFormulaListConstraint(strColPosName );
									XSSFDataValidation validContent = (XSSFDataValidation) dvhCrnt.createValidation(dvcCode, cralContent);
									validContent.setSuppressDropDownArrow(true);
									validContent.setShowErrorBox(true);
									sheetCrnt.addValidationData(validContent);
								} // if ( makeStringByMapValue(mapTblColCodeRow.get(h)).equals("") == false ){
							} // for h
						} // if 유효성 검증(단일 시트일때만 적용 가능) - 적용 ( 상단에 코드 소스 별도로 있음)
						
					} // for s ( sheet )
					
				} // if ( listSheet.size() > 0 ){
				
				
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
				// 엑셀파일 생성 및 워크북 내용 저장
				File fileXlsx = new File(strFullPath); // PATH_TRAVERSAL_IN
				//File fileXlsx = new File(FilenameUtils.getFullPath(strTempExcelUploadPath+File.separator) , FilenameUtils.getName(strRealFileName)) ; // PATH_TRAVERSAL_IN Fixed
				String strTempPath = FilenameUtils.getFullPath(strFullPath); // strTempPath 가 하드코딩인 경우 오류 발생 안함. EX> "C:/egov_workspace/upload/"
				String strTempFileName = FilenameUtils.getName(strRealFileName);
				//File fileXlsx = new File(strTempPath , strTempFileName ) ; // PATH_TRAVERSAL_IN Fixed
				FileOutputStream fileOut = new FileOutputStream(fileXlsx);				
				wbXlsx.write(fileOut);
				fileOut.close();
				
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
				// 엑셀 Download History 등록
				mapResult.put("file_id", excelDownload_History( strFileName, strRealFileName, strTempExcelUploadPath ) );
				isSucess = true;
				
			} // if ( mapExcelAttrs != null ){
		} catch (Exception e) {
			throw new NkiaException(e);
		} finally {
			//if (fileOut!=null) fileOut.close();
		}
		
		return mapResult;
	} // makeExcelFileByPoi
	
	
	
	
	private HashMap makeCodeRecord(String strCKEY, String strCodeName, String strNewOrder){
		HashMap mapRslt = new HashMap();
		mapRslt.put("CKEY", strCKEY); 
		mapRslt.put("CODE_NM",strCodeName); 
		mapRslt.put("NEW_ORDER",strNewOrder);
		
		return mapRslt;
		
	} // makeCodeRecord
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String excelDownload_History( String fileName, String realFileName, String tempUploadPath ) throws Exception{
		String excel_file_id = null;
		try {
			excel_file_id = excelFileService.selectMaxHistoryId();
			
			Map history_param = new HashMap();
			history_param.put("excel_file_id", excel_file_id);
			history_param.put("filename", fileName);
			history_param.put("real_filename", realFileName);
			history_param.put("temp_upload_path", tempUploadPath);
			history_param.put("file_extsn", "xls");
			history_param.put("down_user_id", EgovUserDetailsHelper.getSesssionUserId());
			
			// Insert
			excelFileService.insertExcelFile(history_param);
		} catch (Exception e) {
			throw new NkiaException(e);
		}
		
		return excel_file_id;
	} // excelDownload_History
	

	
	private String setLabel(Object objData){
		String labelValue = null;
		if( objData instanceof java.lang.String ){
			labelValue = (String)objData;
		}else if( objData instanceof BigDecimal ){
			BigDecimal getBigDecimalData = (BigDecimal)objData;
			labelValue = getBigDecimalData.toString();
		}else if( objData instanceof Date ){
			labelValue = ((Date)objData).toString();
		}else if( objData instanceof Timestamp ){
			labelValue = ((Timestamp)objData).toString();
		}else if ( objData instanceof Integer ){
			labelValue = ((Integer)objData).toString();
		}else if ( objData instanceof Long ){
			labelValue = ((Long)objData).toString();
		}else{
			labelValue = (String)objData;
		}
		return labelValue;
	} // setLabel

	
	// org.apache.poi.ss.usermodel.Font
	private Font createCustomFont( Font fontBase , String strFontName, short shortSize , short shortColor, short shortBold, byte byteUnderLine, boolean bItalic){
		fontBase.setFontName(strFontName);   // 폰트명 : 맑은 고딕 , Malgun Gothic
		fontBase.setFontHeightInPoints((short)shortSize); // 폰트크기 : 14
		fontBase.setBoldweight(shortBold); // 폰트굴기 : Font.BOLDWEIGHT_NORMAL, Font.BOLDWEIGHT_BOLD
		fontBase.setUnderline(byteUnderLine);  // 밑줄 : Font.U_NONE, Font.U_SINGLE, Font.U_DOUBLE, Font.U_SINGLE_ACCOUNTING, Font.U_DOUBLE_ACCOUNTING
		fontBase.setItalic(bItalic); 
		fontBase.setColor(shortColor); // // org.apache.poi.hssf.util.HSSFColor
		return fontBase;
	} // createCustomFont
	
	private XSSFCellStyle createCustomCellStyle(XSSFCellStyle styleBase, short sAlign, short sVAlign, Font fontCell
			, XSSFColor xcForeGColor, String strBorderStyle, XSSFColor xcBorderColor, short sDataFormat, boolean bWrap, boolean bLock){
		styleBase.setAlignment(sAlign); // XSSFCellStyle.ALIGN_CENTER , XSSFCellStyle.ALIGN_LEFT , XSSFCellStyle.ALIGN_RIGHT
		styleBase.setVerticalAlignment(sVAlign); // XSSFCellStyle.VERTICAL_CENTER , XSSFCellStyle.VERTICAL_TOP , XSSFCellStyle.VERTICAL_BOTTOM
		styleBase.setFont(fontCell);
		if ( xcForeGColor == null ) {
			//styleBase.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 255))); // new XSSFColor(new java.awt.Color(200, 200, 200))	
		}
		else {
			styleBase.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			styleBase.setFillForegroundColor(xcForeGColor); // new XSSFColor(new java.awt.Color(200, 200, 200))
		}
		
		XSSFColor xcApplyBorderColor = xcBorderColor;
		if ( xcBorderColor == null ) { xcApplyBorderColor = new XSSFColor(new java.awt.Color(0, 0, 0));}
		
		if ( strBorderStyle.indexOf("T") > -1 ){
			styleBase.setBorderTop(XSSFCellStyle.BORDER_THIN);
			styleBase.setTopBorderColor(xcApplyBorderColor); // new XSSFColor(new java.awt.Color(0, 0, 0))
		}
		if ( strBorderStyle.indexOf("B") > -1 ){
			styleBase.setBorderBottom(XSSFCellStyle.BORDER_THIN);
			styleBase.setBottomBorderColor(xcApplyBorderColor); // new XSSFColor(new java.awt.Color(0, 0, 0))
		}
		if ( strBorderStyle.indexOf("L") > -1 ){
			styleBase.setBorderLeft(XSSFCellStyle.BORDER_THIN);
			styleBase.setLeftBorderColor(xcApplyBorderColor); // new XSSFColor(new java.awt.Color(0, 0, 0))
		}
		if ( strBorderStyle.indexOf("R") > -1 ){
			styleBase.setBorderRight(XSSFCellStyle.BORDER_THIN);
			styleBase.setRightBorderColor(xcApplyBorderColor); // new XSSFColor(new java.awt.Color(0, 0, 0))
		}
		if ( sDataFormat != 0 ){	styleBase.setDataFormat(sDataFormat);} // ( (XSSFDataFormat)wbXlsx.createDataFormat() ).getFormat("#,##0")
		
		styleBase.setWrapText(bWrap);
		styleBase.setLocked(bLock);
		return styleBase;
	} // createCustomCellStyle
	
	
	private String makeStringByMapValue(Object objValue){
		String strResult = String.valueOf(objValue);
		if ( strResult == null ) { strResult = ""; }
		if ( strResult.equals("null")== true ) { strResult = ""; }
		return strResult;
	} // makeStringByMapValue
	
	
	private HashMap makeFontAndStyle(org.apache.poi.ss.usermodel.Workbook wbBase){
		HashMap mapStyle = new HashMap();
		XSSFColor colorDefaultHeaderForeG = new XSSFColor(new java.awt.Color(200, 200, 200));
		
		XSSFDataFormat dfBase = (XSSFDataFormat)wbBase.createDataFormat();
		//short sTypeFloat = dfBase.getFormat("#,##0.0##########");
		short sTypeGeneral = dfBase.getFormat("0");
		short sTypeText = dfBase.getFormat("@");
		
		Font fontTitle = createCustomFont( wbBase.createFont() , "맑은 고딕", (short)14, HSSFColor.BLACK.index, Font.BOLDWEIGHT_BOLD , Font.U_SINGLE , this.FONT_ITALIC_NONE);
		Font fontBold = createCustomFont( wbBase.createFont() , "맑은 고딕", (short)10, HSSFColor.BLACK.index, Font.BOLDWEIGHT_BOLD , Font.U_NONE , this.FONT_ITALIC_NONE );
		Font fontBody = createCustomFont( wbBase.createFont() , "맑은 고딕", (short)9, HSSFColor.BLACK.index, Font.BOLDWEIGHT_NORMAL , Font.U_NONE , this.FONT_ITALIC_NONE );
		Font fontHidden = createCustomFont( wbBase.createFont() , "맑은 고딕", (short)9, HSSFColor.WHITE.index, Font.BOLDWEIGHT_NORMAL , Font.U_NONE , this.FONT_ITALIC_NONE );
		
		XSSFCellStyle styleTitle = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, fontTitle
				, null/*ForeGroundColor-default:white*/, "", null/*BorderColor-default:black*/ , (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_UNLOCK);

		XSSFCellStyle styleHeader = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBold
				, colorDefaultHeaderForeG, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_LOCK);
		XSSFCellStyle styleHeaderWrap = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBold
				, colorDefaultHeaderForeG, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP, this.STYLE_LOCK);
		XSSFCellStyle styleHeaderRed = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBold
				, new XSSFColor(new java.awt.Color(255, 0, 0)), "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_LOCK);
		XSSFCellStyle styleHeaderYellow = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBold
				, new XSSFColor(new java.awt.Color(255, 255, 0)), "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_LOCK);
		XSSFCellStyle styleHeaderSkyblue = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBold
				, new XSSFColor(new java.awt.Color(0, 204, 255)), "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_LOCK);

		XSSFCellStyle styleBodyCenter = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, null/*ForeGroundColor-default:white*/, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_UNLOCK);
		XSSFCellStyle styleBodyLeft = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, null/*ForeGroundColor-default:white*/, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_UNLOCK);
		XSSFCellStyle styleBodyRight = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, null/*ForeGroundColor-default:white*/, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_UNLOCK);
		XSSFCellStyle styleHidden = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontHidden
				, null/*ForeGroundColor-default:white*/, "", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_LOCK);
		XSSFCellStyle styleBodyLeftWrap = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, null/*ForeGroundColor-default:white*/, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP, this.STYLE_UNLOCK);
		XSSFCellStyle styleBodyLock = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, new XSSFColor(new java.awt.Color(211, 211, 211)), "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_LOCK);
		XSSFCellStyle styleBodyRightNumber = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_RIGHT, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, null/*ForeGroundColor-default:white*/, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP_NONE, this.STYLE_UNLOCK);
		
		XSSFCellStyle styleBodyLeftEnter = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_LEFT, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, null/*ForeGroundColor-default:white*/, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP, this.STYLE_UNLOCK);

		XSSFCellStyle styleBodyCenterEnter = createCustomCellStyle( (XSSFCellStyle) wbBase.createCellStyle()
				, XSSFCellStyle.ALIGN_CENTER, XSSFCellStyle.VERTICAL_CENTER, fontBody
				, null/*ForeGroundColor-default:white*/, "TBLR", null/*BorderColor-default:black*/, (short)0/*DataFormat-default:0*/ , this.STYLE_WRAP, this.STYLE_UNLOCK);

		
		
		styleTitle.setDataFormat(sTypeText);
		styleHeader.setDataFormat(sTypeText);
		styleHeaderWrap.setDataFormat(sTypeText);
		styleHeaderRed.setDataFormat(sTypeText);
		styleHeaderYellow.setDataFormat(sTypeText);
		styleHeaderSkyblue.setDataFormat(sTypeText);
		styleBodyCenter.setDataFormat(sTypeText);
		styleBodyLeft.setDataFormat(sTypeText);
		styleBodyLeft.setDataFormat(sTypeText);
		styleBodyRight.setDataFormat(sTypeText);
		styleHidden.setDataFormat(sTypeText);
		styleBodyLock.setDataFormat(sTypeText);
		styleBodyRightNumber.setDataFormat(sTypeGeneral);
		styleBodyLeftEnter.setDataFormat(sTypeText);
		styleBodyCenterEnter.setDataFormat(sTypeText);
		
		mapStyle.put("styleTitle", styleTitle);
		mapStyle.put("styleHeader", styleHeader);
		mapStyle.put("styleHeaderWrap", styleHeaderWrap);
		mapStyle.put("styleHeaderRed", styleHeaderRed);
		mapStyle.put("styleHeaderYellow", styleHeaderYellow);
		mapStyle.put("styleHeaderSkyblue", styleHeaderSkyblue);
		mapStyle.put("styleBodyCenter", styleBodyCenter);
		mapStyle.put("styleBodyLeft", styleBodyLeft);
		mapStyle.put("styleBodyRight", styleBodyRight);
		mapStyle.put("styleHidden", styleHidden);
		mapStyle.put("styleBodyLeftWrap", styleBodyLeftWrap);
		mapStyle.put("styleBodyLock", styleBodyLock);
		mapStyle.put("styleBodyRightNumber", styleBodyRightNumber);
		mapStyle.put("styleBodyLeftEnter", styleBodyLeftEnter);
		mapStyle.put("styleBodyCenterEnter", styleBodyCenterEnter);
		
		return mapStyle;
	} // makeFontAndStyle
	
	
	
	
	
	
	
}
