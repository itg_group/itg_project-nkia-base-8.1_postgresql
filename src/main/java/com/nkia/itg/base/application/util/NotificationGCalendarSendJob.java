/*
 * @(#)NotificationEmailSendJob.java              2016. 7. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.service.NotificationSender;

public class NotificationGCalendarSendJob implements NotificationSender {
	
	private static final Logger logger = LoggerFactory.getLogger(NotificationGCalendarSendJob.class);
	
	@Override
	public void sendNotification(HashMap<String, Object> paramMap) {
		logger.info("#############################################################################################Google Canlendar");
		// TODO 
	}
}
