/*
 * @(#)PasswordCreater.java	2005. 10. 20
 *
 * Copyright 2005 Nkia.com, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.security;

/**
 * 특정 문자를 인자값으로 받아서 암호화한 결과값을 반화한한다.
 * 
 * <pre>
 *  Usage :
 *      사용방법 예시.
 * </pre>
 * 
 * @see TODO 참고 클래스 나열.
 * @version <tt>$ Version: 1.0 $</tt> date:2005. 10. 20
 * @author <a href="mailto:smilegun@nkia.co.kr"> 남상훈 </a>
 * 
 * <pre></pre>
 * 
 * 클래스 수정 내용 및 설명.
 * @version <tt>$ Reversion: 1.x $</tt> date:2005. 10. 20
 * @author <a href="mailto:smilegun@nkia.co.kr"> 남상훈 </a>
 */
