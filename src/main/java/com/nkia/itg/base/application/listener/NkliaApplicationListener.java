/**
 * @file : NTopsAppInit.java
 * @author : 김도원
 * @since : 2010. 10. 15. 오전 10:29:05
 * @version : 1.0
 * @see
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -----------------------------------------
 *  2010. 10. 15. 김도원 
 *  
 *  </pre>
 * Copyright 2010(C) by NKIA. All right reserved.
 */

package com.nkia.itg.base.application.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.config.NkiaApplicationEmailMap;
import com.nkia.itg.base.application.config.NkiaApplicationEmailTemplateMap;
import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.nbpm.common.NbpmEngineInitializer;
import com.nkia.itg.system.scheduler.execute.ScheduleExecute;
import com.nkia.itg.system.scheduler.execute.ScheduleExecuteForDB;

public class NkliaApplicationListener implements ServletContextListener {
		/** 프로그램 로그를 남기기 위한 변수 */
		private static System	sys;
		

		protected final Logger logger = LoggerFactory.getLogger(NkliaApplicationListener.class);
		/**
		 * <p>시스템 설정 값을 가져온다.
		 *
		 * @return          
		 */
		public static System getSys() {
			return sys;
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
		 */
		public void contextDestroyed(ServletContextEvent sce) {

		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
		 */
		public void contextInitialized(ServletContextEvent sce) {
			logger.info("***********************************************************************************");
			logger.info("********************************* Starting Server *********************************");
			logger.info("********************** Web Application Server loading... **************************");
			logger.info("***********************************************************************************");
			logger.info("***********************************************************************************");
			ServletContext ctx = null;
			ctx = sce.getServletContext();
			String projectPath = ctx.getRealPath("/");
			WebApplicationContext springctx = (WebApplicationContext) ctx.getAttribute(
					WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
			NkiaApplicationContext.setCtx(springctx);
			NkiaApplicationContext.setProjectPath(projectPath);
			
			NkiaApplicationPropertiesMap.loadProperty();
			
			//메일 설정 정보
			NkiaApplicationEmailMap.loadMailInfo();
			
			//메일 템플릿 정보
			NkiaApplicationEmailTemplateMap.loadMailTemplateInfo();
			
			if("true".equals(NkiaApplicationPropertiesMap.getProperty("Nbpm.Engine.Enable"))) {
				NbpmEngineInitializer.startEngine();
			}
			
			if("true".equals(NkiaApplicationPropertiesMap.getProperty("Globals.Schedule.Enable"))) {
				try {
					// Memory or DB 방식에 대한 분기 처리
					Scheduler scheduler = (Scheduler)NkiaApplicationContext.getCtx().getBean("scheduler");
					String schedulerName = scheduler.getSchedulerName();
					if("MEMORY".equalsIgnoreCase(schedulerName)){
						ScheduleExecute scheduleExecute = new ScheduleExecute();
						scheduleExecute.createSchedule();
					}else{
						ScheduleExecuteForDB scheduleExecute = new ScheduleExecuteForDB();
						scheduleExecute.createSchedule();
					}
				} catch (Exception e) {
					logger.error("Fail Server Starting - Schedule", e);
					new NkiaException(e);
				}
			}
			logger.info("***********************************************************************************");
			logger.info("*********************** Server Starting Complete... *******************************");
			logger.info("***********************************************************************************");
		}
}