/*
 * @(#)MapDataRowHandler.java              2013. 3. 29.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.framework;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibatis.sqlmap.client.event.RowHandler;
import com.nkia.itg.base.application.exception.NkiaException;

public class MapDataRowHandler implements RowHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(MapDataRowHandler.class);
	
	private List returnList = new ArrayList();
	
	public Map getReturnMap() {
		Map returnMap = null;
		if(returnList != null && returnList.size() > 0){
			if(returnList.size() > 1) {
				throw new RuntimeException("No Map result");
			} else {
				returnMap = (Map) returnList.get(0);
			}
		}
		return returnMap;
	}
	
	public List getReturnList() {
		List returnListReturn = new ArrayList();
		returnListReturn = returnList;
		return returnListReturn;
	}

	public void handleRow(Object rowMap) {
		if(rowMap != null) {
			Map returnMap = new HashMap();
			if(rowMap instanceof Map) {
				Map tempMap = (Map)rowMap;
				
				Iterator keySet = tempMap.keySet().iterator();
				while(keySet.hasNext()) {
					String key = (String)keySet.next();
					Object rowValue = tempMap.get(key);
					if(rowValue instanceof Clob) {
						Clob clobData = (Clob)rowValue;
						String clobAsString = "";
						Reader in = null;
						StringWriter strWriter = new StringWriter();
						try {
							in = clobData.getCharacterStream();
							IOUtils.copy(in, strWriter);
							clobAsString = strWriter.toString();
						} catch (Exception e) {
							new NkiaException(e);
						} finally {
							if(in != null) {
								try {
									in.close();
									strWriter.close();
								} catch (IOException e) {
									logger.info("exception :" + e.getMessage());
									new NkiaException(e);
								}
							}
						}
						if(clobAsString != null) {
							returnMap.put(key, clobAsString);
						} else {
							returnMap.put(key, "");
						}
					} else if(rowValue instanceof BigDecimal) {
						BigDecimal bigDValue = (BigDecimal)rowValue;
						if(rowValue != null) {
							returnMap.put(key, bigDValue.longValue());
						} else {
							returnMap.put(key, null);
						}
					} else {
						if(rowValue != null) {
							returnMap.put(key, rowValue);
						} else {
							returnMap.put(key, "");
						}
					}
				}
				
			}
			returnList.add(returnMap);
		}
		
	}

}
