package com.nkia.itg.base.application.dwr;

import org.directwebremoting.event.ScriptSessionEvent;
import org.directwebremoting.event.ScriptSessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

public class NkiaScriptSessrionListener implements ScriptSessionListener {
	private static final Logger logger = LoggerFactory.getLogger(NkiaScriptSessrionListener.class);
	
	public void sessionCreated(ScriptSessionEvent ev) {
		UserVO userVO = null;
		try {
			userVO = (UserVO)EgovUserDetailsHelper.getSesssionUserVO();
		} catch (Exception e) {
			new NkiaException(e);
		}
		ev.getSession().setAttribute("userVO", userVO);
	}

	public void sessionDestroyed(ScriptSessionEvent ev) { }
	
}
