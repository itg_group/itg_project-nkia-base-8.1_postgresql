package com.nkia.itg.base.application.config;

import java.util.List;
import java.util.Map;

import com.nkia.itg.system.email.dao.EmailTemplateDAO;

public class NkiaApplicationEmailTemplateMap {
	
	
	public static String getMailTemplateInfo(String emailTemplateId) {
		String result = "";
		if(emailTemplateId != null && !"".equals(emailTemplateId)) {
			result = System.getProperty(emailTemplateId);
		}
		return result;
	}
	
	
	public static void loadMailTemplateInfo() {
		EmailTemplateDAO emailTemplateDAO = (EmailTemplateDAO)NkiaApplicationContext.getCtx().getBean("emailTemplateDAO");
		List templateList = emailTemplateDAO.selectEmailTemplateInfo();
		for(int i = 0 ; i < templateList.size() ; i++) {
			Map template = (Map)templateList.get(i);
			String pKey = (String)template.get("EMAIL_TEMPLATE_ID");
			String pValue = (String)template.get("EMAIL_TEMPLATE_DATA");
			if(pValue != null && !"".equals(pValue)) {
				System.setProperty(pKey, pValue);				
			} else {
				System.setProperty(pKey, "");
			}
		}
	}
}
