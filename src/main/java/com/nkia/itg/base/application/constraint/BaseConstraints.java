/*
 * @(#)GlobalConstraints.java              2013. 1. 11.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.constraint;

import java.io.Serializable;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;


/**
 * 
 * @version 1.0 2013. 1. 14.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * 시스템 기본 제약조건
 * </pre>
 *
 */
public class BaseConstraints implements Serializable{
	
	private static final long serialVersionUID = 7647050674233837645L;

	// Root Context 일 경우 "" 으로 처리됨. ex) /itam 
	public static final String CONTEXT = "";
	public static final String ORIGIN_FILE_NM = "originalFileName";
	public static final String FILE_EXT = "fileExtension";
	public static final String FILE_SIZE = "fileSize";
	public static final String UPLOAD_FILE_NM = "uploadFileName";
	public static final String FILE_PATH = "filePath";
	public static final String FILE_CONTENT_TYPE = "fileContentType";
	

	private static final String FILE_UPLOAD_DEFAULT_PATH = "default/";
	private static final String PDA_FILE_UPLOAD_PATH = "pdaData/";
	private static final String TEMPLATE_FILE_PATH = "template/";
	
	private static final String EXCEL_FILE_TEMP_PATH = "temp/excel/";
	private static final String ZIP_FILE_TEMP_PATH = "temp/zip/";
	private static final String ARCHIVE_FILE_TEMP_PATH = "temp/archive/";
	private static final String DELETE_MOVE_PATH = "temp/delete/";
	
	public static final String CLASSES_PATH = "WEB-INF/classes/";
	public static final String RESOURCES_SOURCE_PATH = "src/main/resources/";
	public static final String MESSAGE_RESOURCE_PATH = "egovframework/message/";
	
	public static final String WEB_SOURCE_PATH = "src/main/webapp/";
	public static final String NBPM_BPMN2FILE_PATH = "bpmn2/";
	public static final String NBPM_PROCESS_PATH = "itg/nbpm/processPage/";
	
	public static final String NBPM_SQL_FILE_PATH = "egovframework/sqlmap/itg/nbpm/oracle/ProcDetail.xml";
	public static final String NBPM_SQL_SOURCE_FILE_PATH = "src/main/resources/egovframework/sqlmap/itg/nbpm/oracle/ProcDetail.xml";
	
	public static final String APP_DATABASE_CHARSET = "EUC-KR";
	
	
	public static String getUploadFileNm() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + UPLOAD_FILE_NM;
	}

	public static String getFileUploadDefaultPath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + FILE_UPLOAD_DEFAULT_PATH;
	}

	public static String getPdaFileUploadPath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + PDA_FILE_UPLOAD_PATH;
	}

	public static String getExcelFileTempPath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + EXCEL_FILE_TEMP_PATH;
	}

	public static String getZipFileTempPath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + ZIP_FILE_TEMP_PATH;
	}

	public static String getArchiveFileTempPath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + ARCHIVE_FILE_TEMP_PATH;
	}

	public static String getDeleteMovePath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + DELETE_MOVE_PATH;
	}

	public static String getTemplateFilePath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + TEMPLATE_FILE_PATH;
	}
	
	public static String getNbpmBpmn2filePath() {
		return NkiaApplicationPropertiesMap.getProperty("Globals.FileStore.Path") + NBPM_BPMN2FILE_PATH;
	}
	
	public static int getDefaultFieldWidth() throws NkiaException {
		return NkiaApplicationPropertiesMap.getPropertyToInt("Default.Field.Width");
	}
	
	public static String getReportFilePath() throws NkiaException {
		return NkiaApplicationPropertiesMap.getProperty("Globals.Report.FileStore.Path");
	}

	public static String getCONTEXT() {
		return CONTEXT;
	}
	
	public static String getLocale() {
		return NkiaApplicationPropertiesMap.getProperty("application.property.locale");
	}
}
