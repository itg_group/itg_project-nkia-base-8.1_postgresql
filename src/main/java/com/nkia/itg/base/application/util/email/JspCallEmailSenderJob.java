package com.nkia.itg.base.application.util.email;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.system.email.service.EmailHistoryService;

import egovframework.com.cmm.service.EgovProperties;

public class JspCallEmailSenderJob extends AbstractEmailSendJob {

	private static final Logger logger = LoggerFactory.getLogger(JspCallEmailSenderJob.class);
	private EmailVO mailVO;
	
	@Override
	public void setMailVO(EmailVO mailVO) {
		this.mailVO = mailVO;
	}
	
	public void sendMail() throws NkiaException {
		try {
			String enc = new java.io.OutputStreamWriter(System.out).getEncoding();
			logger.debug("default encoding = " + enc);
			
			URL url = new URL(mailVO.getHost());
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			setParameter(params);
	
			StringBuilder postData = new StringBuilder();
			for(Map.Entry<String,Object> param : params.entrySet()) {
				if (postData.length() != 0) {
					postData.append('&');
				};
				postData.append(param.getKey());
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			}
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");
	
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=UTF-8");
			conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			conn.setDoOutput(true);
			conn.getOutputStream().write(postDataBytes);
	       
			Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
//			for (int c = in.read(); c != -1; c = in.read()) {
//				System.out.print((char) c);
//			}
			logger.debug("===================== [Send Mail] Success =====================");
			// 메일 전송 이력 등록
			insertSendMailHistory("SUCCESS", mailVO, "");
		}
		
		catch(Exception e) {
			logger.debug("===================== [Send Mail] Fail =====================");
			// 메일 전송 이력 등록
			insertSendMailHistory("FAIL", mailVO, e.toString());
			throw new NkiaException(e);
		}
	}
	
	private void setParameter(Map<String, Object> params) {
		params.put("DOC_SUBJECT", mailVO.getSubject());
		params.put("DOC_MESSAGE", mailVO.getContent());
		params.put("DOC_WRITER", mailVO.getFrom());
		params.put("DOC_RECEVERS", mailVO.getTo());
	}

	public void run() {
		try {
			sendMail();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}
	
	// 메일발송 이력 관리
	public void insertSendMailHistory(String status, EmailVO emailVO, String errMsg) throws NkiaException {
		emailVO.setStatus(status);
		emailVO.setErr_msg(errMsg);

		EmailHistoryService emailHistoryService = (EmailHistoryService) NkiaApplicationContext.getCtx().getBean("emailHistoryService");
		emailHistoryService.insertMailSendHistory(emailVO);
	}
}