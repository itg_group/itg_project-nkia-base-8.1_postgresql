package com.nkia.itg.base.application.aop;

import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.exception.NkiaException;

public class ItgServiceLogger {
	
	public static final Logger logger = LoggerFactory.getLogger("ITG.LOG");
	
	public void printStartLog(JoinPoint thisJoinPoint) throws NkiaException {
		if(!"EgovUserDetailsSessionServiceImpl".equals(thisJoinPoint.getTarget().getClass().getSimpleName())) {
			String type = null;
			String className = thisJoinPoint.getTarget().getClass().getName();
			String simpleName = thisJoinPoint.getSignature().getDeclaringTypeName();
			
	        if (simpleName.indexOf("Controller") > -1) {
	        	type = "Controller";
	        	logger.info("Excute " + type + " : [" + className + "$" + thisJoinPoint.getSignature().getName() + "]");
	        }
	        else if (simpleName.indexOf("Service") > -1) {
	        	type = "ServiceImpl";
	        	logger.info("Excute " + type + " : [" + className + "$" + thisJoinPoint.getSignature().getName() + "]");
	        }
	        else if (simpleName.indexOf("Executor") > -1) {
	        	type = "Nbpm Executor";
	        	logger.info("Excute " + type + " : [" + className + "$" + thisJoinPoint.getSignature().getName() + "]");
	        }
		}
	}
	
	public synchronized void printEndLog(JoinPoint thisJoinPoint) throws NkiaException {
		if(!"EgovUserDetailsSessionServiceImpl".equals(thisJoinPoint.getTarget().getClass().getSimpleName())) {
				String className = thisJoinPoint.getTarget().getClass().getName();
				String simpleName = thisJoinPoint.getSignature().getDeclaringTypeName();
				String type = null;
				
		        if (simpleName.indexOf("Controller") > -1) {
		        	type = "Controller";
		        	logger.info("End " + type + " : [" + className + "$" + thisJoinPoint.getSignature().getName() + "]");
		        }
		        else if (simpleName.indexOf("Service") > -1) {
		        	type = "ServiceImpl";
		        	logger.info("End " + type + " : [" + className + "$" + thisJoinPoint.getSignature().getName() + "]");
		        }
		        else if (simpleName.indexOf("Executor") > -1) {
		        	type = "Nbpm Executor";
		        	logger.info("End " + type + " : [" + className + "$" + thisJoinPoint.getSignature().getName() + "]");
		        }
		}
	}
}
