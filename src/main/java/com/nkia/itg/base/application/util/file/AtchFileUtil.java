/*
 * @(#)AtchFileUtil.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.application.util.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.common.URLUTF8Encoder;
import com.nkia.itg.base.application.util.common.ZipUtil;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.AtchFileVO;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.service.EgovFileMngUtil;

@Repository("atchFileUtil")
public class AtchFileUtil {
	
	
	@Resource(name = "egovMessageSource")
	private EgovMessageSource messageSource;
	//private static final String default_upload_path = BaseConstraints.getFileUploadDefaultPath();
	//private static final String temp_zip_file_path = BaseConstraints.getZipFileTempPath();
	//private static final String temp_archive_path = BaseConstraints.getArchiveFileTempPath();
	//private static final String temp_delete_move_path = BaseConstraints.getDeleteMovePath();
	
	/**
	 * 
	 * 파일 사이즈 체크
	 * 
	 * @param atchFileVO
	 * @param mptRequest
	 * @throws NkiaException
	 */
	public void checkFileSize(AtchFileVO atchFileVO, Map fileMap) throws NkiaException{
		// 전체 File 최대 사이즈
		long fileAllMaxSize = 0;
		if( atchFileVO.getFile_all_max_size() != 0 ){
			fileAllMaxSize = Long.valueOf(atchFileVO.getFile_all_max_size() * 1024 * 1024);
		}
		
		// 개별 File 최대 사이즈
		long fileUnitMaxSize = 0;
		if( atchFileVO.getFile_unit_max_size() != 0 ){
			fileUnitMaxSize = Long.valueOf(atchFileVO.getFile_unit_max_size() * 1024 * 1024);
		}
		
		// 첨부파일 Size 합
		long fileSizeSum = 0;       
		if( atchFileVO.getSum_file_size() > 0 ){
			fileSizeSum = Long.valueOf(atchFileVO.getSum_file_size());
		}

		Iterator fileIter = fileMap.keySet().iterator();
		while (fileIter.hasNext()) {
			MultipartFile mFile = (MultipartFile) fileMap.get((String) fileIter.next());
			if (mFile.getSize() > 0) {
				// 개별 최대 파일 Check
				if( fileUnitMaxSize != 0 ){
					if( mFile.getSize() > fileUnitMaxSize ){
						atchFileVO.setResultMsg("File_Unit_Max_Size_Over");
						throw new NkiaException("File_Unit_Max_Size_Over");
					}
				}
				if( fileAllMaxSize != 0 ){
					fileSizeSum += mFile.getSize();
				}
			}
			
			if( fileAllMaxSize != 0 ){
				if( fileSizeSum > fileAllMaxSize ){
					atchFileVO.setResultMsg("File_All_Max_Size_Over");
					throw new NkiaException("File_All_Max_Size_Over");
				}
			}
		}
	}
	
	public AtchFileVO uploadAtchFile(AtchFileVO atchFileVO, Map fileMap) throws NkiaException {
		// File Size Check
		if(fileMap != null) {
			checkFileSize(atchFileVO, fileMap);
			
			String fileType = atchFileVO.getFile_type();
			String[] fileTypeArr = {};
			
			if(!StringUtils.isEmpty(fileType)) {
				fileTypeArr = fileType.split(",");
			}
			
			Iterator fileIter = fileMap.keySet().iterator();
			List atchFileDetailList = new ArrayList();
			int uploadFileCount = 0;
			while (fileIter.hasNext()) {
				MultipartFile mFile = (MultipartFile) fileMap.get((String) fileIter.next());
				if (mFile.getSize() > 0) {
					// 개별 최대 파일 Check
					HashMap map = null;
					try {
						map = EgovFileMngUtil.uploadFile(mFile, BaseConstraints.getFileUploadDefaultPath(), uploadFileCount); //default_upload_path => BaseConstraints.getFileUploadDefaultPath()로 수정함 문제있을시 전민수 문의요망						
					} catch (NkiaException fe) {
						throw new NkiaException(fe, messageSource.getMessage("msg.common.file.path.error"));
					}
					
					AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO();	 
					
					atchFileDetailVO.setFile_store_cours((String)map.get(BaseConstraints.FILE_PATH));
					atchFileDetailVO.setStore_file_name((String)map.get(BaseConstraints.UPLOAD_FILE_NM));
					atchFileDetailVO.setOrignl_file_name((String)map.get(BaseConstraints.ORIGIN_FILE_NM));
					atchFileDetailVO.setFile_content_type((String)map.get(BaseConstraints.FILE_CONTENT_TYPE));  
					atchFileDetailVO.setFile_extsn((String)map.get(BaseConstraints.FILE_EXT));
					atchFileDetailVO.setFile_size((String)map.get(BaseConstraints.FILE_SIZE));
					
					if(fileTypeArr.length > 0) {
						atchFileDetailVO.setTask_id(fileTypeArr[uploadFileCount]);
					}
					
					StringTokenizer st = new StringTokenizer(NkiaApplicationPropertiesMap.getProperty("Globals.File.Allow.Ext.Default"), ","); 
					int fileExtChkCount = 0;
					
					String extsn = atchFileDetailVO.getFile_extsn();
					extsn = extsn.toUpperCase();
					
					while(st.hasMoreTokens()){
						String temp = st.nextToken();
						temp = temp.toUpperCase();
						if(temp.equals(extsn)){
							fileExtChkCount++;
						}
					}
					if(fileExtChkCount == 0){
						atchFileVO.setResultMsg("File_Ext_Error");
						throw new NkiaException("File_Ext_Error");
					}
					
					atchFileDetailList.add(atchFileDetailVO);
					uploadFileCount++;
					
				}
			}
			atchFileVO.setUpload_file_count(uploadFileCount);
			atchFileVO.setAtch_file_detail_list(atchFileDetailList);
		}
		return atchFileVO;
	}
	
	public boolean downloadAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO)throws NkiaException, IOException{
		boolean isSuccess = createOutputFile(request, response, atchFileDetailVO.getStore_file_name(), atchFileDetailVO.getOrignl_file_name(), atchFileDetailVO.getFile_store_cours(), atchFileDetailVO.getFile_content_type());
		return isSuccess;
	}
	
	public AtchFileVO uploadAtchFileForStream(AtchFileVO atchFileVO, Map fileMap) throws NkiaException, IllegalStateException, IOException {
		// File Size Check
		if(fileMap != null) {
		    checkFileSize(atchFileVO, fileMap);
			
			Iterator fileIter = fileMap.keySet().iterator();
			List atchFileDetailList = new ArrayList();
			int uploadFileCount = 0;
			while (fileIter.hasNext()) {
				MultipartFile mFile = (MultipartFile) fileMap.get((String) fileIter.next());
				if (mFile.getSize() > 0) {
					// 개별 최대 파일 Check
					HashMap map = null;
					map = EgovFileMngUtil.uploadFileForStream(mFile);					
					
					AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO();	 
					atchFileDetailVO.setFile_binary(map.get("file_binary"));
					atchFileDetailVO.setFile_store_cours((String)map.get(BaseConstraints.FILE_PATH));
					atchFileDetailVO.setStore_file_name((String)map.get(BaseConstraints.UPLOAD_FILE_NM));
					atchFileDetailVO.setOrignl_file_name((String)map.get(BaseConstraints.ORIGIN_FILE_NM));
					atchFileDetailVO.setFile_content_type((String)map.get(BaseConstraints.FILE_CONTENT_TYPE));  
					atchFileDetailVO.setFile_extsn((String)map.get(BaseConstraints.FILE_EXT));
					atchFileDetailVO.setFile_size((String)map.get(BaseConstraints.FILE_SIZE));
					atchFileDetailVO.setStream_file_yn("Y");
					
					StringTokenizer st = new StringTokenizer(NkiaApplicationPropertiesMap.getProperty("Globals.File.Allow.Ext.Default"), ","); 
					int fileExtChkCount = 0;
					
					String extsn = atchFileDetailVO.getFile_extsn();
					extsn = extsn.toUpperCase();
					
					while(st.hasMoreTokens()){
						String temp = st.nextToken();
						temp = temp.toUpperCase();
						if(temp.equals(extsn)){
							fileExtChkCount++;
						}
					}
					if(fileExtChkCount == 0){
						atchFileVO.setResultMsg("File_Ext_Error");
						throw new NkiaException("File_Ext_Error");
					}
					
					atchFileDetailList.add(atchFileDetailVO);
					uploadFileCount++;
					
				}
			}
			atchFileVO.setUpload_file_count(uploadFileCount);
			atchFileVO.setAtch_file_detail_list(atchFileDetailList);
		}
		return atchFileVO;
	}
	
	
	public boolean downloadAtchFileForStream(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO, File blobFile)throws NkiaException, IOException{
		boolean isSuccess = createOutputFileForStream(request, response, atchFileDetailVO.getStore_file_name(), atchFileDetailVO.getOrignl_file_name(), atchFileDetailVO.getFile_store_cours(), atchFileDetailVO.getFile_content_type(), atchFileDetailVO.getFile_binary(), blobFile);
		return isSuccess;
	}

	public long copyAtchFile(List selectAtchFileList)throws NkiaException, IOException {
		long millisSec = System.currentTimeMillis();
		if( selectAtchFileList.size() > 0 ){
			for( int i = 0; i < selectAtchFileList.size(); i++ ){
				AtchFileDetailVO atchFileDetailVO = (AtchFileDetailVO)selectAtchFileList.get(i);
				String storeFileName = atchFileDetailVO.getStore_file_name();
				String orignlFileName = atchFileDetailVO.getOrignl_file_name();
				String fileStoreCours = atchFileDetailVO.getFile_store_cours();
				File sourceFile = new File(fileStoreCours + storeFileName);
				if( sourceFile.exists() ){
					File targetFile = new File(BaseConstraints.getZipFileTempPath() + millisSec + File.separator + orignlFileName);
					FileUtils.copy(sourceFile, targetFile);
				}else{
					throw new NkiaException("FileNotFound");
				}
			}
		}
		return millisSec;
	}

	public boolean zipAtchFile(HttpServletRequest request, HttpServletResponse response, long millisSec, String zipFileName)throws IOException, Exception {
		String sourcePath = BaseConstraints.getZipFileTempPath() + millisSec;
		String tempZipFileName = BaseConstraints.getArchiveFileTempPath();
		boolean isSuccess = ZipUtil.zip( new File( sourcePath ), new File(tempZipFileName), null, Charset.defaultCharset( ).name() , false );
		if( isSuccess ){
			String outputFileName = zipFileName + "_" + DateUtil.getDate() + ".zip";
			isSuccess = createOutputFile(request, response, millisSec+".zip", outputFileName, BaseConstraints.getArchiveFileTempPath(), null);
		}
		return isSuccess; 
	}
	
	public boolean createOutputFile(HttpServletRequest request, HttpServletResponse response, String storeFileName, String orignlFileName, String fileStoreCours, String contentType)throws NkiaException, IOException{
		boolean isSuccess = false;
    	String downloadPath = fileStoreCours + storeFileName;
    	File file = new File(downloadPath);
    	if (file.exists()){
    		String strClient = request.getHeader("User-Agent");

    		if( contentType != null && !contentType.equals("") ){
    			response.setContentType(contentType);
    		}else{
    			response.setContentType("application/octet-stream");
    		}
    		
			response.setContentLength((int)file.length());

			if (strClient.indexOf("MSIE 5.5") != -1) {
				response.setHeader("Content-Type", "doesn/matter;");
				response.setHeader("Content-Disposition", "filename="+URLUTF8Encoder.encode(orignlFileName)+";");
			} else {
				response.setHeader("Content-Disposition", "attachment;filename="+URLUTF8Encoder.encode(orignlFileName)+";");
			}
			
			response.setHeader("Content-Length", String.valueOf(file.length()));
			response.setHeader("Content-Transfer-Encoding", "binary;");

    		BufferedOutputStream bout = null;
    		BufferedInputStream bin = null;
    		try 
    		{
    			byte b[] = new byte[4096];
    			bin = new BufferedInputStream(new FileInputStream(file));
    			bout = new BufferedOutputStream(response.getOutputStream());
    			int read = 0;
    			while ((read = bin.read(b)) != -1){
    				bout.write(b,0,read);
    			}
    		} finally {
    			try {
    				if(bout != null) bout.close();
    				if(bin != null) bin.close();
    			} catch(Exception ex) {
    				throw new NkiaException(ex);
    			} finally{
    				if(bout != null) bout.close();
    				if(bin != null) bin.close();
    			} 
    		}
    		isSuccess = true;
    	}
	    return isSuccess;
	}
	
	public boolean createOutputFileForStream(HttpServletRequest request, HttpServletResponse response, String storeFileName, String orignlFileName, String fileStoreCours, String contentType, Object fileBinary, File blobFile)throws NkiaException, IOException{
		boolean isSuccess = false;
    	String downloadPath = fileStoreCours + storeFileName;
    	File file = new File(downloadPath);
    	
    	if (blobFile.exists()){
    		String strClient = request.getHeader("User-Agent");

    		if( contentType != null && !contentType.equals("") ){
    			response.setContentType(contentType);
    		}else{
    			response.setContentType("application/octet-stream");
    		}
    		
			response.setContentLength((int)blobFile.length());

			if (strClient.indexOf("MSIE 5.5") != -1) {
				response.setHeader("Content-Type", "doesn/matter;");
				response.setHeader("Content-Disposition", "filename="+URLUTF8Encoder.encode(orignlFileName)+";");
			} else {
				response.setHeader("Content-Disposition", "attachment;filename="+URLUTF8Encoder.encode(orignlFileName)+";");
			}
			
			response.setHeader("Content-Length", String.valueOf(blobFile.length()));
			response.setHeader("Content-Transfer-Encoding", "binary;");

    		BufferedOutputStream bout = null;
    		BufferedInputStream bin = null;
    		try 
    		{
    			byte b[] = new byte[4096];
    			bin = new BufferedInputStream(new FileInputStream(blobFile));
    			bout = new BufferedOutputStream(response.getOutputStream());
    			int read = 0;
    			while ((read = bin.read(b)) != -1){
    				bout.write(b,0,read);
    			}
    		} finally {
    			try {
    				if(bout != null) bout.close();
    				if(bin != null) bin.close();
    			} catch(Exception ex) {
    				throw new NkiaException(ex);
    			} finally{
    				if(bout != null) bout.close();
    				if(bin != null) bin.close();
    			} 
    		}
    		isSuccess = true;
    	}
	    return isSuccess;
	}
	
	public synchronized boolean deleteAtchFile(AtchFileDetailVO atchFileDetailVO)throws NkiaException {
		boolean isSuccess = false;
		String deleteFilePath = atchFileDetailVO.getFile_store_cours() + atchFileDetailVO.getStore_file_name();
		File deleteFile = new File(deleteFilePath);
		if ( deleteFile.exists() ){
			deleteFile.delete();
			isSuccess = true;
		}
		return isSuccess; 
	}

	public void moveAtchFileForDelete(AtchFileDetailVO atchFileDetailVO)throws NkiaException,IOException {
		String sourceFilePath = atchFileDetailVO.getFile_store_cours() + atchFileDetailVO.getStore_file_name();
		String moveFilePath = BaseConstraints.getFileUploadDefaultPath() + File.separator + DateUtil.getDate("yyyymm", "") + File.separator + atchFileDetailVO.getStore_file_name();
		FileUtils.moveFile(sourceFilePath, moveFilePath);
	}
	
	public AtchFileVO uploadReportAtchFile(AtchFileVO atchFileVO, Map fileMap) throws NkiaException {
		// File Size Check
		if(fileMap != null) {
			checkFileSize(atchFileVO, fileMap);
			
			String fileType = atchFileVO.getFile_type();
			String[] fileTypeArr = {};
			
			if(!StringUtils.isEmpty(fileType)) {
				fileTypeArr = fileType.split(",");
			}
			
			Iterator fileIter = fileMap.keySet().iterator();
			List atchFileDetailList = new ArrayList();
			int uploadFileCount = 0;
			while (fileIter.hasNext()) {
				MultipartFile mFile = (MultipartFile) fileMap.get((String) fileIter.next());
				if (mFile.getSize() > 0) {
					// 개별 최대 파일 Check
					HashMap map = null;
					try {
						map = EgovFileMngUtil.uploadFile(mFile, BaseConstraints.getReportFilePath(), uploadFileCount);						
					} catch (NkiaException fe) {
						throw new NkiaException(fe, messageSource.getMessage("msg.common.file.path.error"));
					}
					
					AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO();	 
					
					atchFileDetailVO.setFile_store_cours((String)map.get(BaseConstraints.FILE_PATH));
					atchFileDetailVO.setStore_file_name((String)map.get(BaseConstraints.UPLOAD_FILE_NM));
					atchFileDetailVO.setOrignl_file_name((String)map.get(BaseConstraints.ORIGIN_FILE_NM));
					atchFileDetailVO.setFile_content_type((String)map.get(BaseConstraints.FILE_CONTENT_TYPE));  
					atchFileDetailVO.setFile_extsn((String)map.get(BaseConstraints.FILE_EXT));
					atchFileDetailVO.setFile_size((String)map.get(BaseConstraints.FILE_SIZE));
					
					if(fileTypeArr.length > 0) {
						atchFileDetailVO.setTask_id(fileTypeArr[uploadFileCount]);
					}
					
					StringTokenizer st = new StringTokenizer(NkiaApplicationPropertiesMap.getProperty("Globals.File.Allow.Ext.Default"), ","); 
					int fileExtChkCount = 0;
					
					String extsn = atchFileDetailVO.getFile_extsn();
					extsn = extsn.toUpperCase();
					
					while(st.hasMoreTokens()){
						String temp = st.nextToken();
						temp = temp.toUpperCase();
						if(temp.equals(extsn)){
							fileExtChkCount++;
						}
					}
					if(fileExtChkCount == 0){
						atchFileVO.setResultMsg("File_Ext_Error");
						throw new NkiaException("File_Ext_Error");
					}
					
					atchFileDetailList.add(atchFileDetailVO);
					uploadFileCount++;
					
				}
			}
			atchFileVO.setUpload_file_count(uploadFileCount);
			atchFileVO.setAtch_file_detail_list(atchFileDetailList);
		}
		return atchFileVO;
	}
}
