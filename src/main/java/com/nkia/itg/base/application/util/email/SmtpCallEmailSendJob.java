package com.nkia.itg.base.application.util.email;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.config.NkiaApplicationContext;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.EmailVO;
import com.nkia.itg.system.email.service.EmailHistoryService;

public class SmtpCallEmailSendJob extends AbstractEmailSendJob {
	
	private static final Logger logger = LoggerFactory.getLogger(SmtpCallEmailSendJob.class);
	private EmailVO emailVO;
	
	public void setMailVO(EmailVO emailVO) {
		this.emailVO = emailVO;
	}

	public void sendMail() throws NkiaException {
		
		try {
			String fromEmail = emailVO.getFrom();
			
			if (!"".equals(fromEmail) && fromEmail != null) {
				String subject = emailVO.getSubject();
				Multipart content = null;
				String content2 = "";
				if(emailVO.getEmail_req_type() == "REPORT"){
					content = emailVO.getMp();
				}else{
					content2 = emailVO.getContent();
				}
				
				Properties props = new Properties();
				props.put("mail.smtp.starttls.enable", "true");
				//props.put("mail.smtp.auth", "false"); //운영
				props.put("mail.smtp.auth", "true"); //개발
				props.put("mail.smtp.host", emailVO.getHost());
				props.put("mail.smtp.port", emailVO.getPort());
				
				
				EmailAuthenticator authenticator = new EmailAuthenticator(emailVO.getEmail_admin_id(), emailVO.getEmail_admin_pw());
				Session session = Session.getDefaultInstance(props, authenticator); //개발
				//Session session = Session.getDefaultInstance(props); //운영
				
				//session.setDebug(true);
				Message msg = new MimeMessage(session);
				// 보내는 사람 설정
				// 1. 보내는사람명<메일주소> 형식으로 발송할 경우
				String fromEmailName = EmailEnum.EMAIL_SYSTEM_NAME.getString();
				msg.setFrom(new InternetAddress(fromEmail, fromEmailName));
				// 2. <메일주소> 형식으로 발송할 경우
				//msg.setFrom(new InternetAddress(fromEmail));// 보내는 사람 설정
				
				msg.setSubject(MimeUtility.encodeText(subject,"UTF-8", "B"));
				msg.setSentDate(new java.util.Date());// 보내는 날짜 설정
				if(emailVO.getEmail_req_type() == "REPORT"){
					msg.setContent(content); // 내용 설정
				}else{
					msg.setContent(content2, "text/html;charset=euc-kr"); // 내용 설정
				}
				String multiSendYn = emailVO.getEmail_multi_send_yn();
				List to_user_list = emailVO.getTo_user_list();
				List to_cc_list = emailVO.getTo_cc_list();
				if(emailVO.getEmail_req_type() == "REPORT"){  //레포트일땐 다수로 메일발송
					multiSendYn = "Y";
				}
				// 수신자 설정
				if (multiSendYn.equals(EmailEnum.COMMON_N.getString())) {
					//단일
					for (int z=0; z < to_user_list.size(); z++){
						Map targetMap = (Map)to_user_list.get(z);
						String to_user_email = (String)targetMap.get("EMAIL");
						String to_user_nm = (String)targetMap.get("TARGET_USER_NM");
						
						if (to_user_email != null && !"".equals(to_user_email)) {
							InternetAddress to = new InternetAddress(to_user_email, to_user_nm);
							msg.setRecipient(Message.RecipientType.TO, to);
							// 메일 송신
							Transport.send(msg);
							
							emailVO.setTo_user_id((String)targetMap.get("TARGET_USER_ID"));
							emailVO.setTo(to_user_email);
							// 메일 전송 이력 등록
							insertSendMailHistory("SUCCESS", emailVO, "");
						} else {
							// 메일 전송 이력 등록
							insertSendMailHistory("FAIL", emailVO, "FROM USER EMAIL NOT");
						}
					}
					
				} else {
					//이력정보
					String history_to_id = "";
					String history_to = "";
					
					//다중
					InternetAddress[] to_multi = new InternetAddress[to_user_list.size()];
					for (int zz=0; zz < to_user_list.size(); zz++){
						Map multiMap = (Map)to_user_list.get(zz);
						String to_user_email = (String)multiMap.get("EMAIL");
						String to_user_nm = (String)multiMap.get("TARGET_USER_NM");
						if("미등록유저".equals(to_user_nm)){
							to_user_nm = "";
						}
						
						if (to_user_email != null && !"".equals(to_user_email)) {
							to_multi[zz] = new InternetAddress(to_user_email, to_user_nm);
							
							if(!"".equals(history_to)){
								history_to += ",";
							}if(!"".equals(history_to_id)){
								history_to_id += ",";
							}
							history_to_id += multiMap.get("TARGET_USER_ID");
							history_to += to_user_email;
						}
					}
					
					msg.setRecipients(Message.RecipientType.TO, to_multi);
					if(to_cc_list != null){
						if(to_cc_list.size() > 0){
							InternetAddress[] to_multi_cc = new InternetAddress[to_cc_list.size()];
							for (int zz=0; zz < to_cc_list.size(); zz++){
								Map multiMap = (Map)to_cc_list.get(zz);
								String to_user_email = (String)multiMap.get("EMAIL");
								String to_user_nm = (String)multiMap.get("TARGET_USER_NM");
								if("미등록유저".equals(to_user_nm)){
									to_user_nm = "";
								}
								
								if (to_user_email != null && !"".equals(to_user_email)) {
									to_multi_cc[zz] = new InternetAddress(to_user_email, to_user_nm);
								}
							}
							msg.setRecipients(Message.RecipientType.CC, to_multi_cc);
						}
					}
					// 메일 송신
					Transport.send(msg);
					
					emailVO.setTo_user_id(history_to_id);
					emailVO.setTo(history_to);
					
					// 메일 전송 이력 등록
					insertSendMailHistory("SUCCESS", emailVO, "");
				}
			}
			
			logger.debug("===================== [Send Mail] Success =====================");
		}
		
		catch(Exception e) {
			e.printStackTrace();
			logger.debug("===================== [Send Mail] Fail =====================");
			// 메일 전송 이력 등록
			insertSendMailHistory("FAIL", emailVO, e.toString());
			throw new NkiaException(e);
		}
	}
	
	class EmailAuthenticator extends Authenticator {
		
		private String id;
		private String password;
		
		public EmailAuthenticator(String emailId, String emailPassWord) {
			
			this.id = emailId;
			this.password = emailPassWord;
		}
		
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(id, password);
		}
	}

	/*
	public static void main(String[] args) {
		try {
			String to = "x0005062@skhynix.com";

			String subject = "제목";
			String content = "내용";

			MailSender aMailSender = new MailSender();
			MailVO mailVO = new MailVO();
			mailVO.setTo(to);
			mailVO.setSubject(subject);
			mailVO.setContent(content);

			aMailSender.sendMail();

		} catch (Exception e) {
			
			e.printStackTrace();
		} 
	}
	*/
	
	// 메일발송 이력 관리
	public void insertSendMailHistory(String status, EmailVO emailVO, String errMsg) throws NkiaException {
		emailVO.setStatus(status);
		emailVO.setErr_msg(errMsg);

		EmailHistoryService emailHistoryService = (EmailHistoryService) NkiaApplicationContext.getCtx().getBean("emailHistoryService");
		emailHistoryService.insertMailSendHistory(emailVO);
	}
	
	
	@Override
	public void run() {
		try {
			sendMail();
		} catch (Exception e) {
			new NkiaException(e);
		}
	}

}
