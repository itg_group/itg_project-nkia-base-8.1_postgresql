package com.nkia.itg.base.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class TreeVO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String text;
	private String total;
	private String id;
	private String up_node_nm;
	private String up_node_id;
	private String node_id;
	private String node_level;
	private String menu_url;
	private String img;
	private String use_yn;
	private String display_no;
	private String class_yn;
	private String cust_type;
	
	private boolean leaf;
	public List<TreeVO> children = null;
	private boolean expanded;
	private String cls;
	
	public HashMap otherData;		// Tree 데이터 외에 필요한 데이터를 HashMap으로 받는다.
	
	
	public String getClass_yn() {
		return class_yn;
	}
	public void setClass_yn(String class_yn) {
		this.class_yn = class_yn;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getMenu_url() {
		return menu_url;
	}
	public void setMenu_url(String menu_url) {
		this.menu_url = menu_url;
	}
	public String getUp_node_id() {
		return up_node_id;
	}
	public void setUp_node_id(String up_node_id) {
		this.up_node_id = up_node_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isLeaf() {
		return leaf;
	}
	public void setLeaf(String leaf) {
		this.leaf = Boolean.parseBoolean(leaf);
	}
	public String getNode_level() {
		if(node_level == null || "null".equals(node_level)) {
			node_level = "99";
		}
		return node_level;
	}
	public void setNode_level(String node_level) {
		this.node_level = node_level;
	}
	public List<TreeVO> getChildren() {
		return children;
	}
	public void setChildren(List<TreeVO> children) {
		this.children = children;
	}
	public String getNode_id() {
		return node_id;
	}
	public void setNode_id(String node_id) {
		this.node_id = node_id;
	}
	public boolean isExpanded() {
		return expanded;
	}
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}
	public HashMap getOtherData() {
		return otherData;
	}
	public void setOtherData(HashMap otherData) {
		this.otherData = otherData;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getDisplay_no() {
		return display_no;
	}
	public void setDisplay_no(String display_no) {
		this.display_no = display_no;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}	
	public String getCls() {
		return cls;
	}
	public void setCls(String cls) {
		this.cls = cls;
	}
	public String getUp_node_nm() {
		return up_node_nm;
	}
	public void setUp_node_nm(String up_node_nm) {
		this.up_node_nm = up_node_nm;
	}
	public String getCust_type() {
		return cust_type;
	}
	public void setCust_type(String cust_type) {
		this.cust_type = cust_type;
	}
}
