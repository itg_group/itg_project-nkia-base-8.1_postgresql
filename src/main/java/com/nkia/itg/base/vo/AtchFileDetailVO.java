/*
 * @(#)AtchFileDetailVO.java              2013. 1. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.vo;

import com.nkia.itg.base.application.exception.NkiaException;

public class AtchFileDetailVO extends AtchFileVO {
	
	private String atch_file_id;		// 첨부파일 ID
	private String file_idx;			// 파일 IDX
	private String file_store_cours;	// 파일저장경로
	private String store_file_name;		// 파일저장명
	private String orignl_file_name;	// 실제파일명
	private String file_content_type;	// 파일 ContentType
	private String file_size;			// 파일사이즈
	private String file_extsn;			// 파일확장자
	private Object file_binary;			// 파일바이너리
	private String temp_save_yn;		// 임시저장여부
	private String del_yn;				// 삭제여부
	private String task_id;				// TASK_ID(For Process)
	private String upd_user_id;			// 수정자
	private String upd_dt;				// 수정일
	private String stream_file_yn;		// DB저장 방식여부

	public String getAtch_file_id() {
		return atch_file_id;
	}
	public void setAtch_file_id(String atch_file_id) throws NkiaException {
		this.atch_file_id = atch_file_id;
	}
	public String getFile_idx() {
		return file_idx;
	}
	public void setFile_idx(String file_idx) {
		this.file_idx = file_idx;
	}
	public String getFile_store_cours() {
		return file_store_cours;
	}
	public void setFile_store_cours(String file_store_cours) {
		this.file_store_cours = file_store_cours;
	}
	public String getStore_file_name() {
		return store_file_name;
	}
	public void setStore_file_name(String store_file_name) {
		this.store_file_name = store_file_name;
	}
	public String getOrignl_file_name() {
		return orignl_file_name;
	}
	public void setOrignl_file_name(String orignl_file_name) {
		this.orignl_file_name = orignl_file_name;
	}
	public String getFile_content_type() {
		return file_content_type;
	}
	public void setFile_content_type(String file_content_type) {
		this.file_content_type = file_content_type;
	}
	public String getFile_size() {
		return file_size;
	}
	public void setFile_size(String file_size) {
		this.file_size = file_size;
	}
	public String getFile_extsn() {
		return file_extsn;
	}
	public void setFile_extsn(String file_extsn) {
		this.file_extsn = file_extsn;
	}
	public Object getFile_binary() {
		return file_binary;
	}
	public void setFile_binary(Object file_binary) {
		this.file_binary = file_binary;
	}
	public String getTemp_save_yn() {
		return temp_save_yn;
	}
	public void setTemp_save_yn(String temp_save_yn) {
		this.temp_save_yn = temp_save_yn;
	}
	public String getDel_yn() {
		return del_yn;
	}
	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getUpd_user_id() {
		return upd_user_id;
	}
	public void setUpd_user_id(String upd_user_id) {
		this.upd_user_id = upd_user_id;
	}
	public String getUpd_dt() {
		return upd_dt;
	}
	public void setUpd_dt(String upd_dt) {
		this.upd_dt = upd_dt;
	}
	public String getStream_file_yn() {
		return stream_file_yn;
	}
	public void setStream_file_yn(String stream_file_yn) {
		this.stream_file_yn = stream_file_yn;
	}
}
