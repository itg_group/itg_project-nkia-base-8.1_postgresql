package com.nkia.itg.base.vo;

import org.apache.commons.lang.builder.ToStringBuilder;

import egovframework.com.cmm.service.EgovProperties;

public class SmsVO {

	private String seq;
	private String smsCode;
	private String toName;
	private String toPhone;
	private String fromName = EgovProperties.getProperty("Globals_SMS_UserName");
	private String fromPhone = EgovProperties.getProperty("Globals_SMS_SendNo");
	private String regDate;
	private String map1;
	private String map2;
	private String map3;
	private String map4;
	private String map5;
	private String mapContent;
	private String targetFlag;
	private String targetDate;
	
	/** toString 생성 부분 */
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}
	
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getSmsCode() {
		return smsCode;
	}
	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}
	public String getToName() {
		return toName;
	}
	public void setToName(String toName) {
		this.toName = toName;
	}
	public String getToPhone() {
		return toPhone;
	}
	public void setToPhone(String toPhone) {
		this.toPhone = toPhone;
	}
	public String getFromName() {
		return fromName;
	}
	public void setFromName(String fromName) {
		this.fromName = fromName;
	}
	public String getFromPhone() {
		return fromPhone;
	}
	public void setFromPhone(String fromPhone) {
		this.fromPhone = fromPhone;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getMap1() {
		return map1;
	}
	public void setMap1(String map1) {
		this.map1 = map1;
	}
	public String getMap2() {
		return map2;
	}
	public void setMap2(String map2) {
		this.map2 = map2;
	}
	public String getMap3() {
		return map3;
	}
	public void setMap3(String map3) {
		this.map3 = map3;
	}
	public String getMap4() {
		return map4;
	}
	public void setMap4(String map4) {
		this.map4 = map4;
	}
	public String getMap5() {
		return map5;
	}
	public void setMap5(String map5) {
		this.map5 = map5;
	}
	public String getMapContent() {
		return mapContent;
	}
	public void setMapContent(String mapContent) {
		this.mapContent = mapContent;
	}
	public String getTargetFlag() {
		return targetFlag;
	}
	public void setTargetFlag(String targetFlag) {
		this.targetFlag = targetFlag;
	}
	public String getTargetDate() {
		return targetDate;
	}
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}
	
	
}
