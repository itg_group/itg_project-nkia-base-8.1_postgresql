package com.nkia.itg.base.vo;

/**
 * 
 * @version 1.0 2015. 6. 8.
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * @since JDK 1.6
 * <pre>
 * SQL 에서 사용
 * </pre>
 *
 */
public class TreeGridVO extends TreeVO {
	
	private static final long serialVersionUID = 1L;
	
	private String status_001;
	private String status_002;
	private String status_003;
	private String status_004;
	private String status_005;
	private String status_006;
	private String status_007;
	private String status_008;
	private String status_009;
	private String status_010;
	private String status_011;
	private String status_012;
	private String status_013;
	private String status_014;
	private String status_015;
	private String status_016;
	private String status_017;
	private String status_018;
	private String status_019;
	private String status_020;
	private String status_021;
	private String status_022;
	private String status_023;
	private String status_024;
	private String status_025;
	private String status_026;
	private String status_027;
	private String status_028;
	private String status_029;
	private String status_030;
	private String status_031;
	private String status_032;
	private String status_033;
	private String status_034;
	private String status_035;
	private String status_036;
	
	
	private String param_001;
	private String param_002;
	private String param_003;
	private String param_004;
	private String param_005;
	private String param_006;
	private String param_007;
	private String param_008;
	private String param_009;
	private String param_010;
	private String param_011;
	private String param_012;
	private String param_013;
	private String param_014;
	private String param_015;
	
	public String getStatus_001() {
		return status_001;
	}
	public void setStatus_001(String status_001) {
		this.status_001 = status_001;
	}
	public String getStatus_002() {
		return status_002;
	}
	public void setStatus_002(String status_002) {
		this.status_002 = status_002;
	}
	public String getStatus_003() {
		return status_003;
	}
	public void setStatus_003(String status_003) {
		this.status_003 = status_003;
	}
	public String getStatus_004() {
		return status_004;
	}
	public void setStatus_004(String status_004) {
		this.status_004 = status_004;
	}
	public String getStatus_005() {
		return status_005;
	}
	public void setStatus_005(String status_005) {
		this.status_005 = status_005;
	}
	public String getStatus_006() {
		return status_006;
	}
	public void setStatus_006(String status_006) {
		this.status_006 = status_006;
	}
	public String getStatus_007() {
		return status_007;
	}
	public void setStatus_007(String status_007) {
		this.status_007 = status_007;
	}
	public String getStatus_008() {
		return status_008;
	}
	public String getStatus_009() {
		return status_009;
	}
	public void setStatus_009(String status_009) {
		this.status_009 = status_009;
	}
	
	public String getStatus_010() {
		return status_010;
	}
	public void setStatus_010(String status_010) {
		this.status_010 = status_010;
	}
	public String getStatus_011() {
		return status_011;
	}
	public void setStatus_011(String status_011) {
		this.status_011 = status_011;
	}
	public String getStatus_012() {
		return status_012;
	}
	public void setStatus_012(String status_012) {
		this.status_012 = status_012;
	}
	public String getStatus_013() {
		return status_013;
	}
	public void setStatus_013(String status_013) {
		this.status_013 = status_013;
	}
	public String getStatus_014() {
		return status_014;
	}
	public void setStatus_014(String status_014) {
		this.status_014 = status_014;
	}
	public String getStatus_015() {
		return status_015;
	}
	public void setStatus_015(String status_015) {
		this.status_015 = status_015;
	}
	public String getStatus_016() {
		return status_016;
	}
	public void setStatus_016(String status_016) {
		this.status_016 = status_016;
	}
	public String getStatus_017() {
		return status_017;
	}
	public void setStatus_017(String status_017) {
		this.status_017 = status_017;
	}
	public String getStatus_018() {
		return status_018;
	}
	public void setStatus_018(String status_018) {
		this.status_018 = status_018;
	}
	public String getStatus_019() {
		return status_019;
	}
	public void setStatus_019(String status_019) {
		this.status_019 = status_019;
	}
	public String getStatus_020() {
		return status_020;
	}
	public void setStatus_020(String status_020) {
		this.status_020 = status_020;
	}
	public String getStatus_021() {
		return status_021;
	}
	public void setStatus_021(String status_021) {
		this.status_021 = status_021;
	}
	public String getStatus_022() {
		return status_022;
	}
	public void setStatus_022(String status_022) {
		this.status_022 = status_022;
	}
	public String getStatus_023() {
		return status_023;
	}
	public void setStatus_023(String status_023) {
		this.status_023 = status_023;
	}
	public String getStatus_024() {
		return status_024;
	}
	public void setStatus_024(String status_024) {
		this.status_024 = status_024;
	}
	public String getStatus_025() {
		return status_025;
	}
	public void setStatus_025(String status_025) {
		this.status_025 = status_025;
	}
	public String getStatus_026() {
		return status_026;
	}
	public void setStatus_026(String status_026) {
		this.status_026 = status_026;
	}
	public String getStatus_027() {
		return status_027;
	}
	public void setStatus_027(String status_027) {
		this.status_027 = status_027;
	}
	public String getStatus_028() {
		return status_028;
	}
	public void setStatus_028(String status_028) {
		this.status_028 = status_028;
	}
	public String getStatus_029() {
		return status_029;
	}
	public void setStatus_029(String status_029) {
		this.status_029 = status_029;
	}
	public String getStatus_030() {
		return status_030;
	}
	public void setStatus_030(String status_030) {
		this.status_030 = status_030;
	}
	public String getStatus_031() {
		return status_031;
	}
	public void setStatus_031(String status_031) {
		this.status_031 = status_031;
	}
	public String getStatus_032() {
		return status_032;
	}
	public void setStatus_032(String status_032) {
		this.status_032 = status_032;
	}
	public String getStatus_033() {
		return status_033;
	}
	public void setStatus_033(String status_033) {
		this.status_033 = status_033;
	}
	public String getStatus_034() {
		return status_034;
	}
	public void setStatus_034(String status_034) {
		this.status_034 = status_034;
	}
	public String getStatus_035() {
		return status_035;
	}
	public void setStatus_035(String status_035) {
		this.status_035 = status_035;
	}
	public String getStatus_036() {
		return status_036;
	}
	public void setStatus_036(String status_036) {
		this.status_036 = status_036;
	}
	public void setStatus_008(String status_008) {
		this.status_008 = status_008;
	}
	public String getParam_001() {
		return param_001;
	}
	public void setParam_001(String param_001) {
		this.param_001 = param_001;
	}
	public String getParam_002() {
		return param_002;
	}
	public void setParam_002(String param_002) {
		this.param_002 = param_002;
	}
	public String getParam_003() {
		return param_003;
	}
	public void setParam_003(String param_003) {
		this.param_003 = param_003;
	}
	public String getParam_004() {
		return param_004;
	}
	public void setParam_004(String param_004) {
		this.param_004 = param_004;
	}
	public String getParam_005() {
		return param_005;
	}
	public void setParam_005(String param_005) {
		this.param_005 = param_005;
	}
	public String getParam_006() {
		return param_006;
	}
	public void setParam_006(String param_006) {
		this.param_006 = param_006;
	}
	public String getParam_007() {
		return param_007;
	}
	public void setParam_007(String param_007) {
		this.param_007 = param_007;
	}
	public String getParam_008() {
		return param_008;
	}
	public void setParam_008(String param_008) {
		this.param_008 = param_008;
	}
	public String getParam_009() {
		return param_009;
	}
	public void setParam_009(String param_009) {
		this.param_009 = param_009;
	}
	public String getParam_010() {
		return param_010;
	}
	public void setParam_010(String param_010) {
		this.param_010 = param_010;
	}
	public String getParam_011() {
		return param_011;
	}
	public void setParam_011(String param_011) {
		this.param_011 = param_011;
	}
	public String getParam_012() {
		return param_012;
	}
	public void setParam_012(String param_012) {
		this.param_012 = param_012;
	}
	public String getParam_013() {
		return param_013;
	}
	public void setParam_013(String param_013) {
		this.param_013 = param_013;
	}
	public String getParam_014() {
		return param_014;
	}
	public void setParam_014(String param_014) {
		this.param_014 = param_014;
	}
	public String getParam_015() {
		return param_015;
	}
	public void setParam_015(String param_015) {
		this.param_015 = param_015;
	}
}
