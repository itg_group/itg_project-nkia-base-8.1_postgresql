package com.nkia.itg.base.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;

public class EmailVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * host
	 */
	private String host;
	
	/**
	 * port
	 */
	private int port;
	
	/**
	 * 메일 작업 유형
	 */
	private String email_job_type;
	
	/**
	 * 메일 id
	 */
	private String email_id;
	
	/**
	 * 메일 관리자 ID
	 */
	private String email_admin_id;
	
	/**
	 * 메일 관리자 PW
	 */
	private String email_admin_pw;
	
	/**
	 * 메일 관리자 이메일
	 */
	private String email_admin_addr;
	
	/**
	 * 메일 멀티 발송 여부
	 * 단일 사용자 : 받는 사람이 단일
	 * 다중 사용자 : 받는 사람이 구분자(, 또는 ;)를 붙여서 사용자들이 수신
	 */
	private String email_multi_send_yn;
	
	/**
	 * 제목
	 */
	private String subject;
	
	/**
	 * 내용
	 */
	private String content;
	
	/**
	 * 메일 내 사이트 url
	 */
	private String url;
	
	/**
	 * 발송자 정보
	 */
	private String from_user_id;
	private String from_user_nm;
	private String from;
	
	/**
	 * 수신자 정보
	 */
	private String to_user_id;
	private String to_user_nm;
	private String to;
	public List to_user_list;
	
	private String cc;
	public List to_cc_list;

	private String bcc;
	
	/**
	 * 메일 요청 유형(PROCESS: 절차, BOARD: 게시판 등)
	 */
	private String email_req_type;
	
	/**
	 * 템플릿 명
	 */
	private String template_id;
	
	/**
	 * 발송일
	 */
	private String send_dt;
	
	/**
	 * 메일 처리 상태
	 */
	private String status;
	
	/**
	 * 에러 메시지
	 * */
	private String err_msg;
	
	/**
	 * 커스텀 등의 확장 필요 시 사용 가능
	 * */
	public Map customMap = null;
	
	/**
	 * 데이터 셋팅 시 설정 또는 템플릿 정보가 없는 경우 체크하는 상태
	 * */
	private boolean config_flag; 
	
	public Multipart mp;
	
	
	public List getTo_cc_list() {
		return to_cc_list;
	}

	public void setTo_cc_list(List to_cc_list) {
		this.to_cc_list = to_cc_list;
	}


	public Multipart getMp() {
		return mp;
	}

	public void setMp(Multipart mp) {
		this.mp = mp;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getEmail_job_type() {
		return email_job_type;
	}

	public void setEmail_job_type(String email_job_type) {
		this.email_job_type = email_job_type;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getEmail_admin_id() {
		return email_admin_id;
	}

	public void setEmail_admin_id(String email_admin_id) {
		this.email_admin_id = email_admin_id;
	}

	public String getEmail_admin_pw() {
		return email_admin_pw;
	}

	public void setEmail_admin_pw(String email_admin_pw) {
		this.email_admin_pw = email_admin_pw;
	}

	public String getEmail_admin_addr() {
		return email_admin_addr;
	}

	public void setEmail_admin_addr(String email_admin_addr) {
		this.email_admin_addr = email_admin_addr;
	}

	public String getEmail_multi_send_yn() {
		return email_multi_send_yn;
	}

	public void setEmail_multi_send_yn(String email_multi_send_yn) {
		this.email_multi_send_yn = email_multi_send_yn;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFrom_user_id() {
		return from_user_id;
	}

	public void setFrom_user_id(String from_user_id) {
		this.from_user_id = from_user_id;
	}

	public String getFrom_user_nm() {
		return from_user_nm;
	}

	public void setFrom_user_nm(String from_user_nm) {
		this.from_user_nm = from_user_nm;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo_user_id() {
		return to_user_id;
	}

	public void setTo_user_id(String to_user_id) {
		this.to_user_id = to_user_id;
	}

	public String getTo_user_nm() {
		return to_user_nm;
	}

	public void setTo_user_nm(String to_user_nm) {
		this.to_user_nm = to_user_nm;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public List getTo_user_list() {
		return to_user_list;
	}

	public void setTo_user_list(List to_user_list) {
		this.to_user_list = to_user_list;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getEmail_req_type() {
		return email_req_type;
	}

	public void setEmail_req_type(String email_req_type) {
		this.email_req_type = email_req_type;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getSend_dt() {
		return send_dt;
	}

	public void setSend_dt(String send_dt) {
		this.send_dt = send_dt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErr_msg() {
		return err_msg;
	}

	public void setErr_msg(String err_msg) {
		this.err_msg = err_msg;
	}

	public Map getCustomMap() {
		return customMap;
	}

	public void setCustomMap(Map customMap) {
		this.customMap = customMap;
	}

	public boolean isConfig_flag() {
		return config_flag;
	}

	public void setConfig_flag(boolean config_flag) {
		this.config_flag = config_flag;
	}

}