/*
 * @(#)AtchFileVO.java              2013. 1. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.vo;

import java.util.List;

import com.nkia.itg.base.application.exception.NkiaException;

public class AtchFileVO {
	
	private String atch_file_id;
	private String upd_user_id;
	private String upd_dt;
	private String task_id;
	
	public List atch_file_detail_list;
	private boolean isSuccess;
	private String resultMsg;
	private String zip_file_name;
	private String file_store_cours;
	private String delete_file_info;
	private int upload_file_count = 0;
	private int file_all_max_size = 0;
	private int file_unit_max_size = 0;
	private int sum_file_size = 0;
	private String deleteRowYN;
	
	private String file_type;	// 보고서용 이미지 파일 유형

	public String getAtch_file_id() {
		return atch_file_id;
	}
	public void setAtch_file_id(String atch_file_id) throws NkiaException {
		this.atch_file_id = atch_file_id;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public List getAtch_file_detail_list() {
		return atch_file_detail_list;
	}
	public void setAtch_file_detail_list(List atch_file_detail_list) {
		this.atch_file_detail_list = atch_file_detail_list;
	}
	public String getZip_file_name() {
		return zip_file_name;
	}
	public void setZip_file_name(String zip_file_name) {
		this.zip_file_name = zip_file_name;
	}
	public String getDelete_file_info() {
		return delete_file_info;
	}
	public void setDelete_file_info(String delete_file_info) {
		this.delete_file_info = delete_file_info;
	}
	public int getUpload_file_count() {
		return upload_file_count;
	}
	public void setUpload_file_count(int upload_file_count) {
		this.upload_file_count = upload_file_count;
	}
	public int getFile_unit_max_size() {
		return file_unit_max_size;
	}
	public void setFile_unit_max_size(int file_unit_max_size) {
		this.file_unit_max_size = file_unit_max_size;
	}
	public int getFile_all_max_size() {
		return file_all_max_size;
	}
	public void setFile_all_max_size(int file_all_max_size) {
		this.file_all_max_size = file_all_max_size;
	}
	public int getSum_file_size() {
		return sum_file_size;
	}
	public void setSum_file_size(int sum_file_size) {
		this.sum_file_size = sum_file_size;
	}
	public String getUpd_user_id() {
		return upd_user_id;
	}
	public void setUpd_user_id(String upd_user_id) {
		this.upd_user_id = upd_user_id;
	}
	public String getUpd_dt() {
		return upd_dt;
	}
	public void setUpd_dt(String upd_dt) {
		this.upd_dt = upd_dt;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getFile_store_cours() {
		return file_store_cours;
	}
	public void setFile_store_cours(String file_store_cours) {
		this.file_store_cours = file_store_cours;
	}
	public String getDeleteRowYN() {
		return deleteRowYN;
	}
	public void setDeleteRowYN(String deleteRowYN) {
		this.deleteRowYN = deleteRowYN;
	}
	public String getFile_type() {
		return file_type;
	}
	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}
	
}
