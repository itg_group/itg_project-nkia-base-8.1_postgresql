package com.nkia.itg.base.vo;

import java.util.List;

public class GridVO {
	
	private String gridHeader;
	private String gridWidths;
	private String gridColAlign;
	private String gridColTypes;
	private String gridColSorting;
	
	public List rows;
	private int totalCount;
	
	private String page;
	private String xmlString;
	private int cursorPosition;
	
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	@Override
	public String toString() {
		return "GridVO [gridHeader=" + gridHeader + ", gridWidths="
				+ gridWidths + ", gridColAlign=" + gridColAlign
				+ ", gridColTypes=" + gridColTypes + ", gridColSorting="
				+ gridColSorting + ", rows=" + rows + ", totalCount="
				+ totalCount + ", page=" + page + ", xmlString=" + xmlString
				+ "]";
	}
	public String getGridHeader() {
		return gridHeader;
	}
	public void setGridHeader(String gridHeader) {
		this.gridHeader = gridHeader;
	}
	public String getGridWidths() {
		return gridWidths;
	}
	public void setGridWidths(String gridWidths) {
		this.gridWidths = gridWidths;
	}
	public String getGridColAlign() {
		return gridColAlign;
	}
	public void setGridColAlign(String gridColAlign) {
		this.gridColAlign = gridColAlign;
	}
	public String getGridColSorting() {
		return gridColSorting;
	}
	public void setGridColSorting(String gridColSorting) {
		this.gridColSorting = gridColSorting;
	}
	public String getGridColTypes() {
		return gridColTypes;
	}
	public void setGridColTypes(String gridColTypes) {
		this.gridColTypes = gridColTypes;
	}
	public List getRows() {
		return rows;
	}
	public void setRows(List rows) {
		this.rows = rows;
	}
	public String getXmlString() {
		return xmlString;
	}
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public int getCursorPosition() {
		return cursorPosition;
	}
	public void setCursorPosition(int cursorPosition) {
		this.cursorPosition = cursorPosition;
	}
}
