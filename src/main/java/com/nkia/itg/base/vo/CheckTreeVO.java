/*
 * @(#)CheckTreeVO.java              2013. 3. 19.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.vo;

import java.io.Serializable;

public class CheckTreeVO extends TreeVO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private boolean checked;
	
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = Boolean.parseBoolean(checked);
	}
}

