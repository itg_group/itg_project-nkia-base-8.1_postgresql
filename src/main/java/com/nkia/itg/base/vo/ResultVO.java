/*
 * @(#)ResultVO.java              2013. 5. 16.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.vo;

import java.util.Map;

public class ResultVO {
	
	private GridVO gridVO;				// 결과값이 GridVO
	private TreeVO treeVO;				// 결과값이 TreeVO
	public Map resultMap;			// 결과값이 HashMap
	private String resultString;		// 결과값이 String
	private int resultInt;			// 결과값이 int
	private boolean resultBoolean;		// 결과값이 Boolean
	private boolean isSuccess = true;	// 성공실패/여부
	private String resultMsg;			// 결과메시지
	
	public GridVO getGridVO() {
		return gridVO;
	}
	public void setGridVO(GridVO gridVO) {
		this.gridVO = gridVO;
	}
	public TreeVO getTreeVO() {
		return treeVO;
	}
	public void setTreeVO(TreeVO treeVO) {
		this.treeVO = treeVO;
	}
	public Map getResultMap() {
		return resultMap;
	}
	public void setResultMap(Map resultMap) {
		this.resultMap = resultMap;
	}
	public String getResultString() {
		return resultString;
	}
	public void setResultString(String resultString) {
		this.resultString = resultString;
	}
	public int getResultInt() {
		return resultInt;
	}
	public void setResultInt(int resultInt) {
		this.resultInt = resultInt;
	}
	public boolean isResultBoolean() {
		return resultBoolean;
	}
	public void setResultBoolean(boolean resultBoolean) {
		this.resultBoolean = resultBoolean;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
}
