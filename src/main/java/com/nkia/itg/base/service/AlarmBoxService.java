/*
 * @(#)AlarmBoxService.java              2014. 4. 23.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface AlarmBoxService {
	
    // 변경정보 리스트
    public List selectAlarmBoxList(ModelMap paramMap) throws NkiaException, SQLException;
	
    // 상세화면 클릭시 상세화면 로드 후 읽음정보 Y로 변경
	public HashMap selectAlarmBoxDetail(ModelMap paramMap) throws NkiaException, SQLException;

	public void insertAlarmBoxInfo(Map parmaMap) throws NkiaException;
}
