/*
 * @(#)UrlAccessCheckService.java              2018. 8. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import javax.servlet.http.HttpServletRequest;

import com.nkia.itg.base.application.exception.NkiaException;

public interface UrlAccessCheckService {
	
	/**
	 * 요청한 request의 사용자가 메뉴 접근권한이 있는지 체크
	 * @param userRequest
	 * @return
	 * @throws NkiaException
	 */
	public boolean urlAuthCheck(HttpServletRequest userRequest) throws NkiaException;

}
