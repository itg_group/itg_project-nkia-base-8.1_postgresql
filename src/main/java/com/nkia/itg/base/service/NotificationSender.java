/*
 * @(#)NotificationSender.java              2016. 7. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.util.HashMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface NotificationSender {

	public void sendNotification(HashMap<String, Object> paramMap) throws NkiaException;
	
}
