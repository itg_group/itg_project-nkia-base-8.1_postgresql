/*
 * @(#)AlarmBoxServiceImpl.java              2014. 4. 23.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.AlarmBoxDAO;
import com.nkia.itg.base.service.AlarmBoxService;

@Service("alarmBoxService")
public class AlarmBoxServiceImpl implements AlarmBoxService {
	
	private static final Logger logger = LoggerFactory.getLogger(AlarmBoxServiceImpl.class);
	
	@Resource(name = "alarmBoxDAO")
	public AlarmBoxDAO alarmBoxDAO;
	
    public List selectAlarmBoxList(ModelMap paramMap) throws NkiaException, SQLException {
		return alarmBoxDAO.selectAlarmBoxList(paramMap);
	}
	
    // 상세화면 클릭시 상세화면 로드 후 읽음정보 Y로 변경
	public HashMap selectAlarmBoxDetail(ModelMap paramMap) throws NkiaException, SQLException {
		HashMap resultMap = alarmBoxDAO.selectAlarmBoxDetail(paramMap);
		alarmBoxDAO.updateAlarmBoxReadYn(paramMap);
		return resultMap;
	}
	
	@Override
	public void insertAlarmBoxInfo(Map parmaMap) throws NkiaException {
		String alarmKey = alarmBoxDAO.selectAlarmBoxNextKey(parmaMap);
		parmaMap.put("alarm_id", alarmKey);
		alarmBoxDAO.insertAlarmBoxInfo(parmaMap);
	}
}
