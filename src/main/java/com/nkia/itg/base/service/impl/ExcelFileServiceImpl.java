/*
 * @(#)AtchFileServiceImpl.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.ExcelFileDAO;
import com.nkia.itg.base.service.ExcelFileService;

@Service("excelFileService")
public class ExcelFileServiceImpl implements ExcelFileService{
	
	@Resource(name = "excelFileDAO")
	private ExcelFileDAO excelFileDAO;
	
	public String selectMaxHistoryId()throws NkiaException {
		return excelFileDAO.selectMaxHistoryId();
	}
	
	public Map selectExcelFileDetail(Map paramMap)throws NkiaException {
		return excelFileDAO.selectExcelFileDetail(paramMap);
	}
	
	public void insertExcelFile(Map paramMap)throws NkiaException {
		excelFileDAO.insertExcelFile(paramMap);
	}
	
	public void updateExcelNavigation(Map paramMap)throws NkiaException {
		excelFileDAO.updateExcelNavigation(paramMap);
	}

	/*Start 엑셀템플릿 자동 생성(By Poi)*/
	@Override
	public List selectAllCodeListForExcel(HashMap mapQueryParam) throws NkiaException {
		return excelFileDAO.selectAllCodeListForExcel(mapQueryParam);
	}

	@Override
	public List selectPopupCodeListQueryForExcel(HashMap mapQueryParam) throws NkiaException {
		return excelFileDAO.selectPopupCodeListQueryForExcel(mapQueryParam);
	}

	@Override
	public List selectPopupCodeListDynamic(HashMap mapQueryParam) throws NkiaException {
		return excelFileDAO.selectPopupCodeListDynamic(mapQueryParam);
	}
	/*End 엑셀템플릿 자동 생성(By Poi)*/
	
}

