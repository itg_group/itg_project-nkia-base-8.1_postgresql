/*
 * @(#)SessionService.java              2015. 11. 16.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

public interface SessionService {
	
	public String getSessionLocale();
}
