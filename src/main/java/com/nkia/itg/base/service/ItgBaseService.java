/*
 * @(#)ItgBaseService.java              2013. 1. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ItgBaseService {
	
	public List searchCodeDataList(Map param) throws NkiaException;

	public List searchMutilCodeDataListForTree(ModelMap paramMap) throws NkiaException;

	public List searchReqZoneDataList(ModelMap paramMap) throws NkiaException;
	
	public List searchConfTypeList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * TODO ems 연계 테이블 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchEmsTableList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * TODO ems 연계 테이블 컬럼
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchEmsColList(ModelMap paramMap) throws NkiaException;
	
	public List searchClassType(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * 요청현황 작업단계 조회
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	
	public List searchServiceNodeName(ModelMap paramMap) throws NkiaException;
	
	public List searchChangeNodeName(ModelMap paramMap) throws NkiaException;
	
	public List searchIncidentNodeName(ModelMap paramMap) throws NkiaException;
	
	public List searchProblemNodeName(ModelMap paramMap) throws NkiaException;
	
	public List searchCenterNodeName(ModelMap paramMap) throws NkiaException;
	
	public List searchDeadlineNodeName(ModelMap paramMap) throws NkiaException;

	/**
	 * 
	 * AutoDiscovery 연계 테이블 리스트
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAutoDiscoveryTableList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 
	 * AutoDiscovery 연계 테이블 컬럼
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List searchAutoDiscoveryColList(ModelMap paramMap) throws NkiaException;
	
	public List searchCheckListSendList() throws NkiaException;
	public List searchAccountSendList() throws NkiaException;
	/**
	 * 부서 트리 호출
	 * @param paramMap
	 * @return
	 */
	public List searchDeptTree(Map paramMap) throws NkiaException;
	/**
	 * 부서 사용자 그리드 호출
	 * @param paramMap
	 * @return
	 */
	public int searchDeptUserGridListCount(Map paramMap) throws NkiaException;
	public List searchDeptUserGridList(Map paramMap) throws NkiaException;

	public List selectMenuInfo(Map paramMap) throws NkiaException;

	public List selectAmClassTypeCode(ModelMap paramMap) throws NkiaException;
	
	public List selectAssetStateCodeList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * 권한 있는 고객사 리스트 조회
	 * 
	 */
	public List searchCustomerToUserList(ModelMap paramMap) throws NkiaException;
	
	//hmsong cmdb 개선 작업 Start
	
	/**
	 * CONF_TYPE 목록 코드형식 조회(트리)
	 * 
	 */
	public List searchConfTypeCodeList(ModelMap paramMap) throws NkiaException;
	
	/**
	 * CLASS_ID 목록 코드형식 조회(트리)
	 * 
	 */
	public List searchClassIdCodeList(ModelMap paramMap) throws NkiaException;
	//hmsong cmdb 개선 작업 End
}
