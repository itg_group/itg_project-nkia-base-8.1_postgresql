/*
 * @(#)ExcelFileService.java              2013. 10. 23.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface ExcelFileService {
	
	public void updateExcelNavigation(Map paramMap)throws NkiaException;
	
	public void insertExcelFile(Map paramMap)throws NkiaException;
		
	public String selectMaxHistoryId()throws NkiaException;
	
	public Map selectExcelFileDetail(Map paramMap)throws NkiaException;

	/*Start 엑셀템플릿 자동 생성(By Poi)*/
	public List selectAllCodeListForExcel(HashMap mapQueryParam) throws NkiaException;
	public List selectPopupCodeListQueryForExcel(HashMap mapQueryParam) throws NkiaException;
	public List selectPopupCodeListDynamic(HashMap mapQueryParam) throws NkiaException;
	/*End 엑셀템플릿 자동 생성(By Poi)*/
	
}
