/*
 * @(#)ItgBaseService.java              2017. 9. 12.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.util.HashMap;

import com.nkia.itg.base.application.exception.NkiaException;


public interface EncryptPropertyService {
	
	public HashMap generatePropertyEncValue(HashMap paramMap) throws NkiaException;

	public HashMap generatePropertyDecValue(HashMap paramMap) throws NkiaException;

}
