package com.nkia.itg.base.service;

import java.util.Map;

import com.nkia.itg.base.application.exception.NkiaException;

public interface SmsSendService {

	public void insertSmsSend(Map param) throws NkiaException;

	public void insertSmsgHist(Map param) throws NkiaException;
	
	public void callSmsSend(Map param) throws NkiaException;
	
	public void insertTalkList(Map param) throws NkiaException;
	
}
