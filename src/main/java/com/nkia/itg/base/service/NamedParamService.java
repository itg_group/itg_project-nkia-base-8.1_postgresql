/*
 * @(#)NamedParamService.java              2018. 1. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.util.List;
import java.util.Map;

public interface NamedParamService {
 
	/**
	 * 단건을 조회한다.
	 */
	public Map<String, Object> selectMap(String query, Map<String, Object> reqMap); 

	/**
	 * 다건을 조회한다.
	 */
	public List<Map<String, Object>> selectList(String query, Map<String, Object> reqMap); 
	
	/**
	 * 등록한다.
	 */
	public int insert(String query, Map<String, Object> reqMap); 
	
	/**
	 * 변경한다.
	 */
	public int update(String query, Map<String, Object> reqMap); 
	
	/**
	 * 삭제한다.
	 */
	public int delete(String query, Map<String, Object> reqMap); 

}
