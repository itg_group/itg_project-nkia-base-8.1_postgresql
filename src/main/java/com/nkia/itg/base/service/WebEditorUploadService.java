/*
 * @(#)AtchFileService.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.AtchFileVO;

@SuppressWarnings("rawtypes")
public interface WebEditorUploadService {
	
	/**
	 * 
	 * 파일첨부 게시판 이미지
	 * 
	 * @param request
	 * @param atchFileVO
	 * @return Map
	 * @throws NkiaException
	 */
	public Map uploadWebEditorImage(AtchFileVO atchFileVO, HttpServletRequest request)throws NkiaException;
}
