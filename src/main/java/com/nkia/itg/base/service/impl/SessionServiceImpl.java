/*
 * @(#)SessionServiceImpl.java              2015. 11. 16.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service.impl;

import java.util.Locale;

import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.nkia.itg.base.service.SessionService;

@Service("sessionService")
public class SessionServiceImpl implements SessionService{

	@Override
	public String getSessionLocale() {
		Locale locale = (Locale)RequestContextHolder.getRequestAttributes().getAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, RequestAttributes.SCOPE_SESSION);
		return locale.toString();
	}

}
