/*
 * @(#)AtchFileServiceImpl.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service.impl;

import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.common.DateUtil;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.application.util.file.FileUtils;
import com.nkia.itg.base.dao.AtchFileDAO;
import com.nkia.itg.base.service.AtchFileService;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.AtchFileVO;
import com.nkia.itg.nbpm.util.Base64;

import egovframework.com.cmm.util.EgovUserDetailsHelper;

@Service("atchFileService")
public class AtchFileServiceImpl implements AtchFileService{
	
	@Resource(name = "atchFileDAO")
	public AtchFileDAO atchFileDAO;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil; 
	
	/**
	 * 
	 * 파일첨부 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAtchFileList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub 
		return atchFileDAO.selectAtchFileList(paramMap); 
	}
	
	/**
	 * 
	 * 프로세스 파일첨부목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAtchFileListForProc(ModelMap paramMap) throws NkiaException {
		return atchFileDAO.selectAtchFileListForProc(paramMap); 
	}
	
	/**
	 * 
	 * 서식파일 다운로드 목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public List selectAtchFileListForTemplate(ModelMap paramMap) throws NkiaException {
		return atchFileDAO.selectAtchFileListForTemplate(paramMap); 
	}
	
	/**
	 * 
	 * 파일첨부 최초등록
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	public AtchFileVO initRegistAtchFile(AtchFileVO atchFileVO, Map fileMap)throws NkiaException {
		// TODO Auto-generated method stub
		// 파일 저장
		if(fileMap != null) {
			atchFileVO = atchFileUtil.uploadAtchFile(atchFileVO, fileMap);
			atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());	// UserId
			if( atchFileVO.getUpload_file_count() > 0 ){
				// 파일정보 DB에 저장
				atchFileVO = insertAtchFile(atchFileVO);		
			}			
		}
		
		return atchFileVO;
	}
	
	/**
	 * 
	 * 보고서 파일첨부 최초등록
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	public AtchFileVO initReportRegistAtchFile(AtchFileVO atchFileVO, Map fileMap)throws NkiaException {
		// TODO Auto-generated method stub
		// 파일 저장
		if(fileMap != null) {
			atchFileVO = atchFileUtil.uploadReportAtchFile(atchFileVO, fileMap);
			atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());	// UserId
			if( atchFileVO.getUpload_file_count() > 0 ){
				// 파일정보 DB에 저장
				atchFileVO = insertAtchFile(atchFileVO);		
			}			
		}
		
		return atchFileVO;
	}
	
	/**
	 * 
	 * 파일첨부 수정등록
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void modifyRegistAtchFile(AtchFileVO atchFileVO, Map fileMap) throws NkiaException, IOException {
		// 파일정보등록
		atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());	// UserId
		// 파일정보 DB삭제삭제
		if( atchFileVO.getDelete_file_info() != null && atchFileVO.getDelete_file_info().length() > 0 ){
			// 파일삭제정보가 있을 경우.
			String[] deleteFiles = atchFileVO.getDelete_file_info().split(",");
			for( int i = 0; i < deleteFiles.length; i++ ){
				String deleteFileIdx = deleteFiles[i];
				AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO(); 
				atchFileDetailVO.setAtch_file_id(atchFileVO.getAtch_file_id());
				atchFileDetailVO.setFile_idx(deleteFileIdx);
				atchFileDetailVO.setUpd_user_id(atchFileVO.getUpd_user_id());
				atchFileDetailVO = selectAtchFileDetail(atchFileDetailVO);
				 
				// 파일삭제처리
				if(atchFileDetailVO != null) {
					// 파일정보 DB 삭제(Y/N)
					atchFileDAO.updateAtchFileDetailDelYn(atchFileDetailVO);
					
					// 파일삭제를 위해 삭제폴더로 이동
					atchFileUtil.moveAtchFileForDelete(atchFileDetailVO);
				}
			}
		}
		
		// 첨부파일들의 파일 Size Sum
		int sumFileSize = atchFileDAO.selectSumFileSize(atchFileVO);
		if( sumFileSize > 0 ){
			atchFileVO.setSum_file_size(sumFileSize);
		}
		
		// 파일 저장
		atchFileVO = atchFileUtil.uploadAtchFile(atchFileVO, fileMap);
		if( atchFileVO.getUpload_file_count() > 0 ){
			// 파일정보 DB에 저장
			insertAtchFileDetail(atchFileVO);
		}  
	}
	
	/**
	 * 
	 * 첨부파일 삭제
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public void deletetAtchFileByAjax(AtchFileVO atchFileVO) throws NkiaException, IOException {
		// 파일정보등록
		atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());	// UserId
		// 파일정보 DB삭제삭제
		if( atchFileVO.getDelete_file_info() != null && atchFileVO.getDelete_file_info().length() > 0 ){
			// 파일삭제정보가 있을 경우.
			String[] deleteFiles = atchFileVO.getDelete_file_info().split(",");
			if(deleteFiles!=null && deleteFiles.length>0) {
				for( int i = 0; i < deleteFiles.length; i++ ){
					String deleteFileIdx = deleteFiles[i];
					AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO(); 
					atchFileDetailVO.setAtch_file_id(atchFileVO.getAtch_file_id());
					atchFileDetailVO.setFile_idx(deleteFileIdx);
					atchFileDetailVO.setUpd_user_id(atchFileVO.getUpd_user_id());
					atchFileDetailVO = selectAtchFileDetail(atchFileDetailVO);
					 
					// 파일삭제처리
					if(atchFileDetailVO != null) {
						// 파일정보 DB삭제(Y/N)
						atchFileDAO.updateAtchFileDetailDelYn(atchFileDetailVO);
						
						// 파일삭제를 위해 삭제폴더로 이동
						atchFileUtil.moveAtchFileForDelete(atchFileDetailVO);
					}
				}
			}
		}
	}
	
	public void deleteRegistAtchFile(AtchFileVO atchFileVO) throws NkiaException, IOException {
		
		// 파일정보 DB삭제(Y/N)
		atchFileDAO.updateAtchFileDetailDelAll(atchFileVO);
		
		if( atchFileVO.getDeleteRowYN() != null && "Y".equals(atchFileVO.getDeleteRowYN())){
			// 첨부파일 세부 정보 중 BLOB 데이터 초기화(전체)
			atchFileDAO.updateAtchFileEmptyBlobAll(atchFileVO);
		}
		
		List delFileList = atchFileDAO.selectAtchFileDelList(atchFileVO);
		if( delFileList.size() > 0 ){
			// 파일삭제정보가 있을 경우.
			for( int i = 0; i < delFileList.size(); i++ ){
				AtchFileDetailVO atchFileDetailVO = (AtchFileDetailVO)delFileList.get(i); 
				
				//DB파일저장 방식이 아닌 경우 
				if(!"Y".equals(atchFileDetailVO.getStream_file_yn())){
					// 파일삭제를 위해 삭제폴더로 이동
					atchFileUtil.moveAtchFileForDelete(atchFileDetailVO);
				}else{
					
					if( atchFileVO.getDeleteRowYN() != null && "Y".equals(atchFileVO.getDeleteRowYN())){
						// 첨부파일 세부 정보 중 BLOB 데이터 초기화
						atchFileDAO.updateAtchFileEmptyBlob(atchFileDetailVO);
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * 파일다운로드(개별)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileDetailVO
	 * @throws NkiaException
	 * @throws IOException 
	 */
	public boolean downloadAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO)throws NkiaException, IOException {
		// DB에서 파일정보 가져오기.
		atchFileDetailVO = selectAtchFileDetail(atchFileDetailVO); 
		
		// 파일다운로드
		boolean isSuccess = atchFileUtil.downloadAtchFile(request, response, atchFileDetailVO);
		return isSuccess;
	}

	/**
	 * 
	 * 파일 압축다운로드
	 * 
	 * @param request
	 * @param response 
	 * @param atchFileVO
	 * @throws Exception 
	 * @throws IOException 
	 */
	public boolean downloadZipFile(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO)throws IOException, Exception {
		// DB에서 파일정보 가져오기.
		List selectAtchFileList = selectAtchFileListForZip(atchFileVO);
		
		// 파일정보를 임시파일디렉토리에 Copy
		long millisSec = atchFileUtil.copyAtchFile(selectAtchFileList);
		
		// 파일정보를 Zip으로 압축/다운로드
		boolean isSuccess = atchFileUtil.zipAtchFile(request, response, millisSec, atchFileVO.getZip_file_name());
		return isSuccess;
	}
	
	/**
	 * 
	 * 첨부파일 DB저장
	 * 
	 * @param atchFileVO
	 * @return
	 * @throws NkiaException
	 */
	public AtchFileVO insertAtchFile(AtchFileVO atchFileVO) throws NkiaException {
		// 파일정보 DB에 저장
		int insertAtchFileCount = atchFileDAO.selectAtchFileCount(atchFileVO);
		if( insertAtchFileCount == 0 ){
			
		}
		insertAtchFileDetail(atchFileVO);
		return atchFileVO;
	}
	
	/**
	 * 
	 * 첨부파일상세 DB저장
	 * 
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	public void insertAtchFileDetail(AtchFileVO atchFileVO)throws NkiaException{
		List atchFileDetailList = atchFileVO.getAtch_file_detail_list();
		if( atchFileDetailList.size() > 0 ){
			for( int i = 0; i < atchFileDetailList.size(); i++ ){
				AtchFileDetailVO atchFileDetailVO = (AtchFileDetailVO)atchFileDetailList.get(i);
				atchFileDetailVO.setAtch_file_id(atchFileVO.getAtch_file_id());
				
				if(!StringUtils.isEmpty(atchFileVO.getTask_id())) {
					atchFileDetailVO.setTask_id(atchFileVO.getTask_id());
				}
				
				atchFileDetailVO.setUpd_user_id(atchFileVO.getUpd_user_id());
				atchFileDAO.insertAtchFileDetail(atchFileDetailVO);
			}
		}
	}
	
	/**
	 * 
	 * 압축파일정보
	 * 
	 * @param atchFileVO
	 * @return
	 * @throws NkiaException
	 */
	private List selectAtchFileListForZip(AtchFileVO atchFileVO) throws NkiaException {
		return atchFileDAO.selectAtchFileListForZip(atchFileVO);
	}  
	
	/**
	 * 
	 * 첨부파일 상세정보
	 * 
	 * @param atchFileDetailVO
	 * @return
	 * @throws NkiaException
	 */
	public AtchFileDetailVO selectAtchFileDetail(AtchFileDetailVO atchFileDetailVO) throws NkiaException {
		return atchFileDAO.selectAtchFileDetail(atchFileDetailVO); 
	}
	
	public HashMap selectFileName(String id) throws NkiaException {
		HashMap saveFileName = atchFileDAO.selectFileName(id);
		return saveFileName;
	}

	@Override
	public String getAtchFileSeq() throws NkiaException {
		String seq = atchFileDAO.getAtchFileSeq(); 
		AtchFileVO atchFileVO = new AtchFileVO();
		atchFileVO.setAtch_file_id(DateUtil.getDate("yyyymm", "") + seq);
		atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());
		atchFileDAO.insertAtchFile(atchFileVO);
		return atchFileVO.getAtch_file_id();
	}

	/**
	 * 
	 * 파일첨부 최초등록_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@Override
	public AtchFileVO initRegistAtchFileForStream(AtchFileVO atchFileVO, Map fileMap)throws NkiaException, IllegalStateException, IOException {
		// 파일 저장
		if(fileMap != null) {
			atchFileVO = atchFileUtil.uploadAtchFileForStream(atchFileVO, fileMap);
			atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());	// UserId
			if( atchFileVO.getUpload_file_count() > 0 ){
				// 파일정보 DB에 저장
				atchFileVO = insertAtchFileForStream(atchFileVO);		
			}			
		}
		
		return atchFileVO;
	}

	/**
	 * 
	 * 파일첨부 수정등록_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	@Override
	public void modifyRegistAtchFileForStream(AtchFileVO atchFileVO, Map fileMap) throws NkiaException, IllegalStateException, IOException {
		// 파일정보등록
		atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());	// UserId
		// 파일정보 DB삭제삭제
		if( atchFileVO.getDelete_file_info() != null && atchFileVO.getDelete_file_info().length() > 0 ){
			// 파일삭제정보가 있을 경우.
			String[] deleteFiles = atchFileVO.getDelete_file_info().split(",");
			for( int i = 0; i < deleteFiles.length; i++ ){
				String deleteFileIdx = deleteFiles[i];
				AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO(); 
				atchFileDetailVO.setAtch_file_id(atchFileVO.getAtch_file_id());
				atchFileDetailVO.setFile_idx(deleteFileIdx);
				atchFileDetailVO.setUpd_user_id(atchFileVO.getUpd_user_id());
				atchFileDetailVO = selectAtchFileDetail(atchFileDetailVO);

				// deleteRowYN 속성값에 따라 삭제 로직 다름
				if( atchFileVO.getDeleteRowYN() != null && "Y".equals(atchFileVO.getDeleteRowYN())){
					// 첨부파일 세부 정보 중 BLOB 데이터 초기화
					atchFileDAO.updateAtchFileEmptyBlob(atchFileDetailVO);
				
				}
				// 파일정보 DB삭제(Y/N)
				atchFileDAO.updateAtchFileDetailDelYn(atchFileDetailVO);
				
			}
		}
		
		// 첨부파일들의 파일 Size Sum
		int sumFileSize = atchFileDAO.selectSumFileSize(atchFileVO);
		if( sumFileSize > 0 ){
			atchFileVO.setSum_file_size(sumFileSize);
		}
		
		// 파일 저장
		atchFileVO = atchFileUtil.uploadAtchFileForStream(atchFileVO, fileMap);
		if( atchFileVO.getUpload_file_count() > 0 ){
			// 파일정보 DB에 저장
			insertAtchFileDetailForStream(atchFileVO);
		}  

	}

	/**
	 * 
	 * 첨부파일 삭제_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	@Override
	public void deletetAtchFileForStreamByAjax(AtchFileVO atchFileVO) throws NkiaException {
		// 파일정보등록
		atchFileVO.setUpd_user_id(EgovUserDetailsHelper.getSesssionUserId());	// UserId
		// 파일정보 DB삭제삭제
		if( atchFileVO.getDelete_file_info() != null && atchFileVO.getDelete_file_info().length() > 0 ){
			// 파일삭제정보가 있을 경우.
			String[] deleteFiles = atchFileVO.getDelete_file_info().split(",");
			if(deleteFiles!=null && deleteFiles.length>0) {
				for( int i = 0; i < deleteFiles.length; i++ ){
					String deleteFileIdx = deleteFiles[i];
					AtchFileDetailVO atchFileDetailVO = new AtchFileDetailVO(); 
					atchFileDetailVO.setAtch_file_id(atchFileVO.getAtch_file_id());
					atchFileDetailVO.setFile_idx(deleteFileIdx);
					atchFileDetailVO.setUpd_user_id(atchFileVO.getUpd_user_id());
					atchFileDetailVO = selectAtchFileDetail(atchFileDetailVO);
					 
					// 파일정보 DB삭제(Y/N)
					atchFileDAO.updateAtchFileDetailDelYn(atchFileDetailVO);
				}
			}
		}
	}
	
	/**
	 * 
	 * 파일다운로드(개별)_forStream (DB저장용)
	 * 
	 * @param request
	 * @param response
	 * @param atchFileDetailVO
	 * @throws NkiaException
	 * @throws SQLException 
	 * @throws IOException 
	 */
	@Override
	public boolean downloadAtchFileForStream(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO)throws NkiaException, SQLException, IOException {
		// DB에서 파일정보 가져오기.
		atchFileDetailVO = selectAtchFileDetail(atchFileDetailVO); 

		File blobFile = null;
		HashMap atchFileBinaryMap = atchFileDAO.selectAtchFileBinary(atchFileDetailVO);
		Blob fileBinary = (Blob)atchFileBinaryMap.get("FILE_BINARY");
		if (fileBinary != null) {
			String fileBinaryString = new String(fileBinary.getBytes(1, (int)fileBinary.length()));
			byte[] buf = null;
					
			try {
				blobFile = FileUtils.writeFileFromByte(atchFileDetailVO.getOrignl_file_name(), Base64.decodeBase64(fileBinaryString.getBytes()));		
			} catch(Exception e) {
				throw new NkiaException(e);
			}
		}
		
		// 파일다운로드
		boolean isSuccess;
		if(blobFile != null) {
			isSuccess = atchFileUtil.downloadAtchFileForStream(request, response, atchFileDetailVO, blobFile);
		}else{
			isSuccess = false;
		}
		
		return isSuccess;
	}

	/**
	 * 
	 * 첨부파일 DB저장_forStream (DB저장용)
	 * 
	 * @param atchFileVO
	 * @return
	 * @throws NkiaException
	 */
	@Override
	public AtchFileVO insertAtchFileForStream(AtchFileVO atchFileVO) throws NkiaException {
		// 파일정보 DB에 저장
		int insertAtchFileCount = atchFileDAO.selectAtchFileCount(atchFileVO);
		if( insertAtchFileCount == 0 ){
			
		}
		insertAtchFileDetailForStream(atchFileVO);
		return atchFileVO;
	}

	/**
	 * 
	 * 첨부파일상세 DB저장_forStream (DB저장용)
	 * 
	 * @param atchFileVO
	 * @throws NkiaException
	 */
	public void insertAtchFileDetailForStream(AtchFileVO atchFileVO)throws NkiaException{
		List atchFileDetailList = atchFileVO.getAtch_file_detail_list();
		if( atchFileDetailList.size() > 0 ){
			for( int i = 0; i < atchFileDetailList.size(); i++ ){
				AtchFileDetailVO atchFileDetailVO = (AtchFileDetailVO)atchFileDetailList.get(i);
				atchFileDetailVO.setAtch_file_id(atchFileVO.getAtch_file_id());
				atchFileDetailVO.setTask_id(atchFileVO.getTask_id());
				atchFileDetailVO.setUpd_user_id(atchFileVO.getUpd_user_id());
				atchFileDAO.insertAtchFileDetail(atchFileDetailVO);
				
				Map forStreamMap = new HashMap();
				forStreamMap.put("atch_file_id", atchFileDetailVO.getAtch_file_id());
				forStreamMap.put("orignl_file_name", atchFileDetailVO.getOrignl_file_name());
				forStreamMap.put("file_binary", atchFileDetailVO.getFile_binary());
				atchFileDAO.updateAtchFileDetailForStream(forStreamMap);
			}
		}
	}
	
	/**
	 * API로 첨부파일 다운로드 하기 위한 첨부파일 상세 정보 조회
	 * 
	 * @param atchFileDetailVO
	 * @throws NkiaException
	 */
	public HashMap selectAtchFileDetailForService(AtchFileDetailVO atchFileDetailVO) throws NkiaException {
		HashMap fileData = atchFileDAO.selectAtchFileDetailForService(atchFileDetailVO);
		return fileData;
	}
}
