/*
 * @(#)AtchFileServiceImpl.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service.impl;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.file.AtchFileUtil;
import com.nkia.itg.base.dao.AtchFileDAO;
import com.nkia.itg.base.service.WebEditorUploadService;
import com.nkia.itg.base.vo.AtchFileVO;

import egovframework.com.cmm.EgovWebUtil;
import egovframework.com.cmm.service.EgovFileMngUtil;

@SuppressWarnings("rawtypes")
@Service("webEditorUploadService")
public class WebEditorUploadServiceImpl implements WebEditorUploadService{
	
	@Resource(name = "atchFileDAO")
	public AtchFileDAO atchFileDAO;
	
	@Resource(name = "atchFileUtil")
	public AtchFileUtil atchFileUtil; 
	
	public static final int BUFF_SIZE = 2048;
	
	/**
	 * 
	 * 파일첨부 게시판 이미지
	 * 
	 * @param request
	 * @param atchFileVO
	 * @return Map
	 * @throws NkiaException
	 */
	public Map uploadWebEditorImage(AtchFileVO atchFileVO, HttpServletRequest request)throws NkiaException {
		MultipartHttpServletRequest mptRequest = (MultipartHttpServletRequest) request;
		
		// 시스템상 Web Root 폴더에서 WebEditor 라는 폴더가 존재하는지 확인
		// 폴더를 webeditor/images/ 처럼 이중으로 생성할 시 webeditor라는 폴더가 존재해야 생성이 됨
		String uploadPath = request.getRealPath("/") + "webeditor/";
		
		try {
		    File cFile = new File(EgovWebUtil.filePathBlackList(uploadPath));
		    
		    cFile.setExecutable(false, true);
		    cFile.setReadable(true);
		    cFile.setWritable(false, true);
		    
		    if (!cFile.exists()){
		    	cFile.mkdir();
		    }
		    
		    // 위의 webeditor 폴더가 없다면 생성한 이후 진행
		    uploadPath = uploadPath + "images/";
		    
		} catch (Exception e) {
			throw new NkiaException();
		}
		// 여기까지 추가
		
		Map result = null;
		Iterator<String> fileIter = mptRequest.getFileNames();
		int upload_cnt = 0;
		while (fileIter.hasNext()) {
			String fileName = EgovWebUtil.filePathReplaceAll((String) fileIter.next());
			MultipartFile mFile = mptRequest.getFile( fileName );
			if (mFile.getSize() > 0) {
				// 개별 최대 파일 Check
				result = EgovFileMngUtil.uploadFile(mFile, uploadPath, upload_cnt);
			}
			upload_cnt++;
		}
		
		return result;
	}
}
