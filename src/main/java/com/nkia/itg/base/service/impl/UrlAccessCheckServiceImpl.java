/*
 * @(#)UrlAccessCheckServiceImpl.java              2018. 8. 8.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.UrlAccessCheckDAO;
import com.nkia.itg.base.service.UrlAccessCheckService;
import com.nkia.itg.certification.vo.UserVO;

@Service("urlAccessCheckService")
public class UrlAccessCheckServiceImpl implements UrlAccessCheckService {

	private static final Logger logger = LoggerFactory.getLogger(UrlAccessCheckServiceImpl.class);
	
	@Resource(name = "urlAccessCheckDAO")
	public UrlAccessCheckDAO urlAccessCheckDAO;
	
	/**
	 * 요청한 request 의 사용자가 메뉴 접근권한이 있는지 체크
	 * @param userRequest
	 * @return true:접근가능URL/false:접근불가URL
	 * @throws NkiaException
	 */
	public boolean urlAuthCheck(HttpServletRequest userRequest) throws NkiaException {
		
		boolean result = false;
		
		// request session 에서 접속한 사용자 정보를 가지고 온다.
		UserVO userVO = (UserVO)userRequest.getSession().getAttribute("userVO");
		
		if( userRequest!=null && userVO!=null ){
			
			// 요청된 URI 정보 변수 세팅
			String reqUri = userRequest.getRequestURI();
			String reqQstr = userRequest.getQueryString();
			String fullUri = reqUri;
			if(reqQstr!=null && !reqQstr.equals("")) {
				fullUri += ("?"+reqQstr);
			}
			
			// SYS_MENU에 등록된 전체 메뉴 URL을 가지고 온다.
			List allMenuUrls = urlAccessCheckDAO.selectAllMenuUrls();
			
			if(allMenuUrls!=null && allMenuUrls.size()>0) {
				
				HashMap paramMap = new HashMap();
				
				/*
				 * 요청된 URL이 SYS_MENU에 있는 URL일 경우에만 검증 실행
				 * : SYS_MENU에 없는 URL은 Ajax 요청 등으로 보내는 URL 이므로 검증 불필요
				 */
				// 요청된 URL이 SYS_MENU에 있는 URL 인지 카운트
				paramMap.put("request_url", fullUri);
				int reqUrlInMenuUrlCnt = urlAccessCheckDAO.countIsReqUrlInMenuUrl(paramMap);
				
				/*
				 * 요청된 URL이 SYS_MENU에 없는 URL일 경우 검증 실행 안함 (true 리턴)
				 */
				if(reqUrlInMenuUrlCnt < 1) {
					result = true;
				} else {
					/*
					 * 요청된 URL이 SYS_MENU에 있는 URL일 경우 검증 실행
					 */
					// 요청된 URL이 현재 사용자가 권한을 가지고 있는 URL 인지 카운트
					paramMap.put("user_id", userVO.getUser_id());
					int reqUrlInUserAuthMenuUrlCnt = urlAccessCheckDAO.countIsReqUrlInUserAuthMenuUrl(paramMap);
					
					// 요청된 URL이 현재 사용자가 권한이 있는 URL일 경우 true 리턴
					if(reqUrlInUserAuthMenuUrlCnt > 0) {
						result = true;
					}
				}
			}
		}
		return result;
	}

}
