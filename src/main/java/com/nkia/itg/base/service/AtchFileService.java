/*
 * @(#)AtchFileService.java              2013. 2. 4.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.vo.AtchFileDetailVO;
import com.nkia.itg.base.vo.AtchFileVO;

public interface AtchFileService {
	
	public List selectAtchFileList(ModelMap paramMap)throws NkiaException;
	
	public AtchFileVO initRegistAtchFile(AtchFileVO atchFileVO, Map fileMap)throws NkiaException;
	
	public AtchFileVO initReportRegistAtchFile(AtchFileVO atchFileVO, Map fileMap)throws NkiaException;

	public void modifyRegistAtchFile(AtchFileVO atchFileVO, Map fileMap)throws NkiaException, IOException;
	
	public boolean downloadAtchFile(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO)throws NkiaException, IOException;

	public boolean downloadZipFile(HttpServletRequest request, HttpServletResponse response, AtchFileVO atchFileVO)throws NkiaException, IOException, Exception;
	
	public void deletetAtchFileByAjax(AtchFileVO atchFileVO) throws NkiaException, IOException;

	public void deleteRegistAtchFile(AtchFileVO atchFileVO)throws NkiaException, IOException;
	
	public AtchFileVO insertAtchFile(AtchFileVO atchFileVO) throws NkiaException;

	/**
	 * 
	 * 프로세스 파일첨부목록
	 * 
	 * @param paramMap
	 * @return
	 * @throws NkiaException
	 */
	public List selectAtchFileListForProc(ModelMap paramMap)throws NkiaException;

	public HashMap selectFileName(String id) throws NkiaException;
	
	
	public String getAtchFileSeq()throws NkiaException;

	public List selectAtchFileListForTemplate(ModelMap paramMap)throws NkiaException;

	public AtchFileDetailVO selectAtchFileDetail( AtchFileDetailVO atchFileDetailVO) throws NkiaException ;
	
	public AtchFileVO initRegistAtchFileForStream(AtchFileVO atchFileVO, Map fileMap)throws NkiaException, IllegalStateException, IOException;

	public void modifyRegistAtchFileForStream(AtchFileVO atchFileVO, Map fileMap)throws NkiaException, IllegalStateException, IOException;
	
	public void deletetAtchFileForStreamByAjax(AtchFileVO atchFileVO) throws NkiaException;

	public boolean downloadAtchFileForStream(HttpServletRequest request, HttpServletResponse response, AtchFileDetailVO atchFileDetailVO)throws NkiaException, SQLException, IOException;
	
	public AtchFileVO insertAtchFileForStream(AtchFileVO atchFileVO) throws NkiaException;
	
	/**
	 * API 에서 첨부파일 상세 정보 조회
	 * 
	 */
	public HashMap selectAtchFileDetailForService(AtchFileDetailVO atchFileDetailVO) throws NkiaException;
}
