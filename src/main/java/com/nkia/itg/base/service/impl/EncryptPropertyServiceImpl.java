package com.nkia.itg.base.service.impl;

import java.security.Security;
import java.util.HashMap;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.service.EncryptPropertyService;

import egovframework.com.cmm.service.EgovProperties;

@Service("encryptPropertyService")
public class EncryptPropertyServiceImpl implements EncryptPropertyService {

	private static final Logger logger = LoggerFactory.getLogger(EncryptPropertyServiceImpl.class);
	
	
	public HashMap generatePropertyEncValue(HashMap paramMap) throws NkiaException {
		
		Security.addProvider(new BouncyCastleProvider());
		StandardPBEStringEncryptor pbeEnc = new StandardPBEStringEncryptor();
		
		pbeEnc.setProviderName("BC"); // jce provider name
		pbeEnc.setAlgorithm("PBEWITHSHA256AND128BITAES-CBC-BC"); // AES 256bit도 가능
		String jasyptPass = EgovProperties.getProperty("jasyptPass");
		pbeEnc.setPassword(jasyptPass);
		
	    String dbUrl = (String)paramMap.get("db_url");
	    String dbUser = (String)paramMap.get("db_user");
	    String dbPw = (String)paramMap.get("db_pw");
	    String etc = (String)paramMap.get("etc");
	    
	    String resultDbUrl = "입력한 값이 없습니다.";
	    String resultDbUser = "입력한 값이 없습니다.";
	    String resultDbPw = "입력한 값이 없습니다.";
	    String resultEtc = "입력한 값이 없습니다.";
	    
	    if(dbUrl != null && !"null".equals(dbUrl) && !"".equals(dbUrl)) {
	    	resultDbUrl = pbeEnc.encrypt(dbUrl);
	    }
	    if(dbUser != null && !"null".equals(dbUser) && !"".equals(dbUser)) {
	    	resultDbUser = pbeEnc.encrypt(dbUser);
	    }
	    if(dbPw != null && !"null".equals(dbPw) && !"".equals(dbPw)) {
	    	resultDbPw = pbeEnc.encrypt(dbPw);
	    }
	    if(etc != null && !"null".equals(etc) && !"".equals(etc)) {
	    	resultEtc = pbeEnc.encrypt(etc);
	    }
	    
		HashMap resultMap = new HashMap();
		resultMap.put("result_db_url", resultDbUrl);
		resultMap.put("result_db_user", resultDbUser);
		resultMap.put("result_db_pw", resultDbPw);
		resultMap.put("result_etc", resultEtc);

        return resultMap;
	}


	@Override
	public HashMap generatePropertyDecValue(HashMap paramMap) throws NkiaException {
		Security.addProvider(new BouncyCastleProvider());
		StandardPBEStringEncryptor pbeEnc = new StandardPBEStringEncryptor();
		
		pbeEnc.setProviderName("BC"); // jce provider name
		pbeEnc.setAlgorithm("PBEWITHSHA256AND128BITAES-CBC-BC"); // AES 256bit도 가능
		String jasyptPass = EgovProperties.getProperty("jasyptPass");
		pbeEnc.setPassword(jasyptPass);
		
	    String dbUrl = (String)paramMap.get("db_url");
	    String dbUser = (String)paramMap.get("db_user");
	    String dbPw = (String)paramMap.get("db_pw");
	    String etc = (String)paramMap.get("etc");
	    
	    String resultDbUrl = "입력한 값이 없습니다.";
	    String resultDbUser = "입력한 값이 없습니다.";
	    String resultDbPw = "입력한 값이 없습니다.";
	    String resultEtc = "입력한 값이 없습니다.";
	    
	    if(dbUrl != null && !"null".equals(dbUrl) && !"".equals(dbUrl)) {
	    	resultDbUrl = pbeEnc.decrypt(dbUrl);
	    }
	    if(dbUser != null && !"null".equals(dbUser) && !"".equals(dbUser)) {
	    	resultDbUser = pbeEnc.decrypt(dbUser);
	    }
	    if(dbPw != null && !"null".equals(dbPw) && !"".equals(dbPw)) {
	    	resultDbPw = pbeEnc.decrypt(dbPw);
	    }
	    if(etc != null && !"null".equals(etc) && !"".equals(etc)) {
	    	resultEtc = pbeEnc.decrypt(etc);
	    }
	    
		HashMap resultMap = new HashMap();
		resultMap.put("result_db_url", resultDbUrl);
		resultMap.put("result_db_user", resultDbUser);
		resultMap.put("result_db_pw", resultDbPw);
		resultMap.put("result_etc", resultEtc);

        return resultMap;
	}


}
