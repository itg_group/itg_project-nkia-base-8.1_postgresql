/*
 * @(#)ItgBaseServiceImpl.java              2013. 1. 30.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nkia.itg.base.application.constraint.BaseConstraints;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.ItgBaseDAO;
import com.nkia.itg.base.service.ItgBaseService;

@Service("itgBaseService")
public class ItgBaseServiceImpl implements ItgBaseService {

	private static final Logger logger = LoggerFactory.getLogger(ItgBaseServiceImpl.class);
	
	@Resource(name = "itgBaseDAO")
	public ItgBaseDAO itgBaseDAO;
	
	
	public List searchCodeDataList(Map param) throws NkiaException {
		
		boolean isPortalReq = false;
		
		List tempList = new ArrayList();
		List resultList = new ArrayList();
		
		String locale = BaseConstraints.getLocale();
		param.put("LOCALE", locale);
		
		tempList = itgBaseDAO.searchCodeDataList(param);
		
		String isPortalReqPop = null;
		isPortalReqPop = (String)param.get("is_portal_req_pop");
		
		if(isPortalReqPop != null 
				&& !isPortalReqPop.equals("") 
				&& isPortalReqPop.equals("true")) {
			
			isPortalReq = true;
			
			for(int i=0; i<tempList.size(); i++) {
				
				HashMap tempMap = new HashMap();
				tempMap = (HashMap)tempList.get(i);
				
				String multiLineText = (String)tempMap.get("CODE_DESC");
				multiLineText = "<div class='multiLine-text'>"+multiLineText+"</div>";
				
				tempMap.put("CODE_DESC", multiLineText);
				resultList.add(tempMap);
			}
		}
		
		if(isPortalReq) {
			return resultList;
		} else {
			return tempList;
		}
	}

	@Override
	public List searchMutilCodeDataListForTree(ModelMap paramMap) throws NkiaException {
		return itgBaseDAO.searchMutilCodeDataListForTree(paramMap);
	}

	@Override
	public List searchReqZoneDataList(ModelMap paramMap) throws NkiaException {
		return itgBaseDAO.searchReqZoneDataList(paramMap);
	}
	
	@Override
	public List searchConfTypeList(ModelMap paramMap) throws NkiaException {
		return itgBaseDAO.searchConfTypeList(paramMap);
	}

	@Override
	public List searchEmsTableList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchEmsTableList(paramMap);
	}

	@Override
	public List searchEmsColList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchEmsColList(paramMap);
	}
	
	@Override
	public List searchClassType(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchClassType(paramMap);
	}

	@Override
	public List searchServiceNodeName(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchServiceNodeName(paramMap);
	}
	
	@Override
	public List searchChangeNodeName(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchChangeNodeName(paramMap);
	}
	
	@Override
	public List searchIncidentNodeName(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchIncidentNodeName(paramMap);
	}
	
	@Override
	public List searchProblemNodeName(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchProblemNodeName(paramMap);
	}
	
	@Override
	public List searchCenterNodeName(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchCenterNodeName(paramMap);
	}
	
	@Override
	public List searchDeadlineNodeName(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchDeadlineNodeName(paramMap);
	}
	
	@Override
	public List searchAutoDiscoveryTableList(ModelMap paramMap) throws NkiaException {
		return itgBaseDAO.searchAutoDiscoveryTableList(paramMap);
	}

	@Override
	public List searchAutoDiscoveryColList(ModelMap paramMap) throws NkiaException {
		return itgBaseDAO.searchAutoDiscoveryColList(paramMap);
	}
	
	public List searchCheckListSendList() throws NkiaException {
		
		return itgBaseDAO.searchCheckListSendList();
	}
	
	public List searchAccountSendList() throws NkiaException {
		
		return itgBaseDAO.searchAccountSendList();
	}

	@Override
	public List searchDeptTree(Map paramMap) throws NkiaException {
		return itgBaseDAO.searchDeptTree(paramMap);
	}

	@Override
	public int searchDeptUserGridListCount(Map paramMap) throws NkiaException {
		return itgBaseDAO.searchDeptUserGridListCount(paramMap);
	}

	@Override
	public List searchDeptUserGridList(Map paramMap) throws NkiaException {
		return itgBaseDAO.searchDeptUserGridList(paramMap);
	}
	
	@Override
	public List selectMenuInfo(Map paramMap) throws NkiaException {
		return itgBaseDAO.selectMenuInfo(paramMap);
	}

	@Override
	public List selectAmClassTypeCode(ModelMap paramMap) throws NkiaException {
		return itgBaseDAO.selectAmClassTypeCode(paramMap);
	}

	@Override
	public List selectAssetStateCodeList(ModelMap paramMap) throws NkiaException {
		return itgBaseDAO.selectAssetStateCodeList(paramMap);
	}
	
	@Override
	public List searchCustomerToUserList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchCustomerToUserList(paramMap);
	}

	//hmsong cmdb 개선 작업 Start
	@Override
	public List searchClassIdCodeList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchClassIdCodeList(paramMap);
	}

	@Override
	public List searchConfTypeCodeList(ModelMap paramMap) throws NkiaException {
		// TODO Auto-generated method stub
		return itgBaseDAO.searchConfTypeCodeList(paramMap);
	}
	//hmsong cmdb 개선 작업 End
}
