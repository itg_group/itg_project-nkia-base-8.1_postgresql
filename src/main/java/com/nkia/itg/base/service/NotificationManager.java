/*
 * @(#)NotificationManager.java              2016. 7. 18.
 *
 * Copyright 2013 nkia.co.kr, Inc. All rights reserved.
 * NKIA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nkia.itg.base.service;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.application.util.NotificationGCalendarSendJob;
import com.nkia.itg.base.application.util.NotificationSmsSendJob;
import com.nkia.itg.base.application.util.email.EmailSetBoardData;
import com.nkia.itg.base.application.util.email.EmailSetData;

public class NotificationManager {
	private static final Logger logger = LoggerFactory.getLogger(NotificationManager.class);
	private static final String EMAIL = "EMAIL";
	private static final String SMS = "SMS";
	private static final String GCALENDAR = "GCALENDAR";

	public void getNotificationSendJob(HashMap<String, Object> useMap, HashMap ntcnMap) throws NkiaException, JsonParseException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException, MessagingException{
        
		for( String key : useMap.keySet() ){
        	if(key.equals(EMAIL)){
        		Map alarmMap = new HashMap();
        		alarmMap.put("KEY", "NOTI_JOB");
        		alarmMap.put("DATA", ntcnMap);
        		alarmMap.put("ADD_DATA", useMap);
        		EmailSetData emailSender = new EmailSetData(alarmMap);
				emailSender.sendMail();
            	
            } else if(key.equals(SMS)){
            	NotificationSmsSendJob nssj = new NotificationSmsSendJob();
				nssj.sendNotification(ntcnMap);
            	
            } else if(key.equals(GCALENDAR)){
            	NotificationGCalendarSendJob ngsj = new NotificationGCalendarSendJob();
				ngsj.sendNotification(ntcnMap);
            	
            }
        }
	}
}
