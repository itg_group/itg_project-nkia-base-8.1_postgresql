package com.nkia.itg.base.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap;
import com.nkia.itg.base.application.exception.NkiaException;
import com.nkia.itg.base.dao.SmsSendDAO;
import com.nkia.itg.base.dao.SmsgHistDAO;
import com.nkia.itg.base.service.SmsSendService;
import com.nkia.itg.certification.vo.UserVO;

import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.cmm.util.EgovUserDetailsHelper;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

@Service("smsSendService")
public class SmsSendServiceImpl implements SmsSendService {
	
	private static final Logger logger = LoggerFactory.getLogger(SmsSendServiceImpl.class);
	//IdGen 사용하기 위함
	@Resource(name="egovSmsSendIdGnrService")
	private EgovIdGnrService smsSendIdGenService;
	
	@Resource(name="smsSendDAO")
	private SmsSendDAO smsSendDAO;
	
	@Resource(name="smsgHistDAO")
	private SmsgHistDAO smsgHistDAO;
	
	/**
	 * param 정리 
	 * TO_NAME : 수신자이름 (사용자 테이블의 user_id를 넣어줍니다. 사용자 테이블에 없는 사람의 경우 "tempId" 로 넣어줍니다.)
	 * TO_PHONE : 수신자핸드폰번호("-" 없이보내주세요)
	 * FROM_NAME : 발신자 이름 (사용자 테이블의 user_id를 넣어줍니다)
	 * FROM_PHONE : 발신자 핸드폰 번호("-" 없이보내주세요)
	 * REQ_DATE :  예약 날짜시간 (기본값 SYSDATE, DATE 타입으로 넣어주세요)
	 * MAP1 : 메핑 1 (사용법 문의중)
	 * MAP2 : 메핑 2
	 * MAP3 : 메핑 3
	 * MAP4 : 메핑 4
	 * MAP5 : 메핑 5
	 * MAP_CONTENT : 전달할 문자메시지 내용 (길이는 80 바이트)
	 * TARGET_FLAG : 타게팅 여부(기본값 'N', sms업체에서 업데이트) 
	 * TARGET_DATE : 타게팅 시점(sms업체에서 업데이트)
	 */
	public void insertSmsSend(Map param) throws NkiaException {
		
//		String SEQ = "";
//		SEQ = smsSendDAO.selectSmsSendSeq(param);
//		
//		// Idgen을 이용하여 SEQ를 들고옵니다.
//		param.put("SEQ", smsSendIdGenService.getNextIntegerId());
//		//param.put("SEQ", SEQ);
//		
//		if(null == param.get("FROM_NAME")) {
//			// 발신자 이름
//			param.put("FROM_NAME", EgovProperties.getProperty("Globals_SMS_UserName"));
//		}
//
//		if(null == param.get("FROM_NAME")) {
//			// 발신자 전화번호
//			param.put("FROM_PHONE", EgovProperties.getProperty("Globals_SMS_SendNo"));
//		}
//
//		smsSendDAO.insertSmsSend(param);
		
		
	}

	@Override
	public void insertSmsgHist(Map param) throws NkiaException {
//		try {
//			UserVO user = (UserVO)EgovUserDetailsHelper.getAuthenticatedUser();
//			String content = (String)param.get("MAP_CONTENT");
//			if(content != null && content.length() > 300) {
//				content.substring(0, 300);
//				content = content + "...";
//			}
//			param.put("MAP_CONTENT", content);
//			param.put("login_user_id", user.getUser_id());
//			smsgHistDAO.insertSmsgHist(param);			
//		} catch (Exception e) {
//			throw new NkiaException(e);
//		}
	}		
	
	
	public void callSmsSend(Map param) throws NkiaException {
		
		param.put("MSG_BODY",param.get("MAP_CONTENT"));
		
		if("INTER".equals(param.get("IndoGbn"))){
			param.put("MSG_BODY","82"+param.get("MAP_CONTENT"));
			smsSendDAO.insertInSMSList(param); //해외
		}else{
			param.put("SEND_PHONE",param.get("FROMHP"));
			smsSendDAO.insertDoSMSList(param); //국내
			smsgHistDAO.insertTalkListHistory(param);
		}
	
		
	}
	
	public void insertTalkList(Map param) throws NkiaException {
		
		smsSendDAO.insertTalkList(param);
		smsgHistDAO.insertTalkListHistory(param);
	}
}
