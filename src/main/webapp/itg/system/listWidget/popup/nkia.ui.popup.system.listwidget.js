/********************************************
 * Date: 2017-02-16
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  엑셀다운로드 팝업
 ********************************************/
// 일반 엑셀 다운로드
nkia.ui.popup.system.ExcelByDynamic = function() {
	// 속성 Properties
	this.id = "";
	this.param = {};	// 조회조건
	this.excelParam = {};	// excelParam 필수 항목 :  fileName, sheetName, titleName, includeColumns, headerNames, defaultColumns, url 
	this.title = "엑셀다운로드";
	this.closable = true;
	this.width = 450;
	this.height = 500;
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var propSelectPop = null;
		var propNmArray = [];
		var propIdArray = [];
		var colNm;
		var colId;
		var addData = {};
		
		var tempExcelParam = this.excelParam;
		var param = this.param;
		
		this.excelAttrGrid = createGridComp({
			id : this.id+'_grid',
			resizeColumn : true,
			checkbox : true,
			resource : "grid.popup.excel",
			autoLoad : false,
			header : {
				title : "속성 선택",
			}
		});
		
	} // end this.setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.excelAttrGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.downExcelFile.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
		
	} // end setWindow
	
	this.setGridAttrData = function() {
		var popGrid = $$(this.id+'_grid');
		var gridData = new Array();
		var colNm = this.excelParam['headerNames'].split(",");
		var colId = this.excelParam['includeColumns'].split(",");
		
//		popGrid._addRow(gridData);
		var rowDataArr = new Array();
		for ( var i = 0; i < colNm.length; i++) {
			var rowDataMap = {
					'RNUM' : i,
					'COL_NM' : colNm[i],
					'COL_ID' : colId[i],
			}
			rowDataArr.push(rowDataMap);
		}
		popGrid._addRow(rowDataArr);
		
	} // end setGridArrtData
	
	
	this.downExcelFile = function(){
		var _this = this;
		var propNmArray = [];
		var propIdArray = [];
		var selectedRecords = $$(this.id+'_grid')._getSelectedRows();
		if (selectedRecords.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택한 속성이 없습니다.'
			});
		} else {
			for ( var i = 0; i < selectedRecords.length; i++) {
				var col_nm = nullToSpace(selectedRecords[i].COL_NM);
				var col_id = nullToSpace(selectedRecords[i].COL_ID);
				propNmArray.push(col_nm);
				propIdArray.push(col_id);
			}
			
			var headerNm = propNmArray.join();
			var headerId = propIdArray.join();
			
			var excelAttrs = {
					sheet : [ {
						sheetName : this.excelParam['sheetName'],
						titleName : this.excelParam['titleName'],
						headerWidths : this.excelParam['headerWidths'],
						headerNames : headerNm,
						includeColumns : headerId,
						groupHeaders : this.excelParam['groupHeaders'],
						align : this.excelParam['align']
					} ]
				};
			
			var excelParam = {};
			excelParam['fileName'] = this.excelParam["fileName"];
			excelParam['excelAttrs'] = excelAttrs;

			goExcelDownLoad(this.excelParam['url'], this.param, excelParam);
			_this.window.close();
		}
		
	} // downExcelFile
	
} // end nkia.ui.popup.itam.excel.ExcelByDynamic 

nkia.ui.popup.system.DeletePop = function() {
	this.id = "";
	this.targetGridId = "";
	this.param = {};
	this.title = "삭제대상리스트";
	this.closable = true;
	this.width = 600;
	this.height = 500;
	this.url = "";
	this.deleteUrl = "";
	
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var param = this.param;
		
		this.grid = createGridComp({
			id : this.id+'_grid',
			resizeColumn: true,
			pageable: true,
			pageSize: 10,
			resource : 'grid.system.listWidget.popup', 
			url : this.url,
			checkbox : true,
			header : {
				title : "삭제대상목록",
			}
		});
		
	} // end this.setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.grid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '삭 제', type: "form", click: this.deleteSeletctedRecords.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
	}
	
	this.deleteSeletctedRecords = function(){
		var _this = this;
		var selectedRecords = $$(this.id+'_grid')._getSelectedRows();
		
		if(!confirm('삭제하시겠습니까?')){
			return false;
		}
		
		if(selectedRecords != undefined && selectedRecords.length != 0){
			var paramMap = {};
			var listId = "";
			
			for(var i=0; i<selectedRecords.length; i++){
				if(i == 0){
					listId += selectedRecords[i].LIST_ID;
				}else{
					listId += "," + selectedRecords[i].LIST_ID;
				}
			}
			paramMap["ListId"] = listId;
			
			nkia.ui.utils.ajax({
				url: this.deleteUrl,
				params: paramMap,
				success: function(data) {
					if (data.success) {
						nkia.ui.utils.notification({
							message: "삭제되었습니다."
						});
						$$(_this.targetGridId)._reload();
						
						_this.window.close();
						
					} else {
						nkia.ui.utils.notification({
							message: data.resultMsg
						});
					}
				}			
			});
		
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택 대상이 없습니다.'
            });
        }
	}
		
} 

nkia.ui.popup.system.EditPop = function() {
	this.id = "";
	this.targetGridId = "";
	this.param = {};
	this.title = "화면표기방식";
	this.closable = true;
	this.width = 600;
	this.height = 500;
	
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var param = this.param;
		
		this.form = createFormComp({
			id: this.id + '_Form',
			elementsConfig: {
				labelWidth : 105,
				labelPosition: "left"
			},
			fields: {
				colSize: 1,
				items: [
			        { colspan: 1, item: createTextAreaFieldComp({ name: 'editTextArea', resizable: false, height: 250, value : param.inputVal }) }
				]
			}
		});
		
		this.textPanel = {
			cols : [{
				view : "template", height : 35, template : param.selectedInfo
			}]
		};
	} // end this.setRender
	
	
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.textPanel, this.form)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '입 력', type: "form", id:'sendBtn', click: this.sendEditVal.bind(_this) })
    		        ]
	    		}
	    	},
			closable: true
		});
	}
	
	this.sendEditVal = function(){
		var _this = this;
		var _param = this.param;
		var item = {};
		
		var editVal = $$(this.id + '_Form')._getFieldValue("editTextArea");
		item[_param.targetRow] = editVal;
		$$(_this.targetGridId).updateItem(_param.row_id, item);
		
		_this.window.close();
	}
		
} 

/**
 * 엑셀 다운로드 속성선택 팝업
 * @param {}
 */
function creatListWidgetPop(popupProps) {
	var props = popupProps||{};
	
	var popupObject;
	try {
		switch (props.type) { // dynamic 일반 엑셀 다운로드
			case 'Dynamic' :
				popupObject = new nkia.ui.popup.system.ExcelByDynamic();
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
				popupObject.setGridAttrData();
			break;
			case 'listWidgetDeletePop' :
				popupObject = new nkia.ui.popup.system.DeletePop();
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
			break;
			case 'listWidgetEditPop' :
				popupObject = new nkia.ui.popup.system.EditPop();
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
			break;	
		}
	} catch(e) {
		alert(e.message);
	}
}

/**
 * 트리 팝업으로 부서 정보 출력으로 한정
 * 이후 공통 작업 진행 예정
 * 
 * 부서 팝업창 오픈
 * 
 */
function createTreePopup(url, fields_id, fields_name, form_name){
	var that = this;
	var paramData ={is_all_tree : "Y", is_tree_view : "preview"};
	if(url == "/itg/base/searchMutilCodeDataListForTree.do"){
		paramData ={grpCodeId : 'INC_TYPE_CD'};
	}
	if(url == "/itg/workflow/workReqDoc/searchWorkReqDocListTree.do"){
		paramData ={display_yn : 'Y'};
	}
	
	var tree = createTreeComponent({
		id: fields_id + '_tree',
        url: url,
        header: {
            title: fields_name
        },
        expColable: true,
        filterable: true,
        width: 300,
        params: paramData,
        expandLevel: 3,
        on: {
        	onItemClick: function(nodeId, e, element){
        		var clickNode = this.getItem(nodeId); //webix API Method
        		var hidden_name = fields_id + "_field";
        		
        		var values = {};
        		values[fields_id] = clickNode.text;
        		values[hidden_name] = clickNode.node_id;
        		$$(form_name)._setValues(values);
        		
        		popupWindow.close();
        	}
        }
    });
	
	var popupWindow = createWindow({
		id: 'popupWindow',
		width: 800,
		height: 650,
		header: {
			title : '팝업',
		},
		body: {
			rows: new Array().concat(tree)
		},
		closable: true
	});
	
	$$('popupWindow').show();
}

/**
 * 팝업 필드 초기화
 */
function initPopupField(setPopUpProp) {
	var that = this;
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var values = {};
	values[that['id']] = '';
	values[that['target_form_hidden']] = '';
	$$(that["target_form_id"])._setValues(values);
}

