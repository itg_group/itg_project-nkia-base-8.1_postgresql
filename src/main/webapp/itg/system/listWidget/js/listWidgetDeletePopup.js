function createListWidgetDeletePopup(setPopUpProp){
	var that = this;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var result;
	var tableProps;
	var url = that["url"];
	var paramMap = that["paramMap"];
	
	// 버튼 묶음
	var deletePopupBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.delete' }), id:'deletePopupBtn', ui:'correct', scale:'medium'});
	var cancelPopupBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.cancel' }), id:'cancelPopupBtn', scale:'medium'});
	
	
	if(null == popUp_comp[that['id']]){
		
		// 그리드 생성
		var setGridProp = {	
			id: 'listWidgetDeleteGrid',							// Grid id
			height: 405,
			border : false,						
			resource_prefix: 'grid.system.listWidget.popup',				// Resource Prefix
			url: url,	
			selModel : true
		}
		var listWidgetDeleteGrid = createGridComp(setGridProp);
		
		listWidgetDeletePopup = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: getConstText({ isArgs: true, m_key: 'res.label.system.listWidget.00034' }),  
			width: 550,  
			autoDestroy: false,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			buttons: [deletePopupBtn, cancelPopupBtn],
			bodyStyle: that['bodyStyle'],
			items: [listWidgetDeleteGrid],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = listWidgetDeletePopup;
		
	}else {
		listWidgetDeletePopup = popUp_comp[that['id']];
	}
	
	// 삭제버튼을 눌렀을 경우
	attachBtnEvent(deletePopupBtn, btnActionLink,  'deletePopupBtn');
	// 취소버튼을 눌렀을 경우
	attachBtnEvent(cancelPopupBtn, btnActionLink,  'cancelPopupBtn');
	
	return listWidgetDeletePopup;
}


function btnActionLink(command){
	
	switch(command) {
		case 'deletePopupBtn' : 
			// 마스크를 띄웁니다.
			setViewPortMaskTrue();

			// 리로드 부분
			var theFrom = Ext.getCmp('listWidgetDeleteGrid');
			var theForm1 = Ext.getCmp('listWidgetGrid');
			
			var selectedRecords = theFrom.view.selModel.getSelection();
			
			if(!confirm(getConstText({ isArgs: true, m_key: 'msg.common.confirm.00003' }))){
				getGrid('listWidgetDeleteGrid').view.selModel.deselectAll();
				// 마스크를 숨깁니다.
				setViewPortMaskFalse();
				return false;
			}
			
			var paramMap = {};
			var ListId ="";
			
			for ( var i = 0; i < selectedRecords.length; i++) {
				if(0 == i){
					ListId += selectedRecords[i].data.LIST_ID;
				}else {
					ListId +="," +  selectedRecords[i].data.LIST_ID;
				}
			}
			paramMap["ListId"] = ListId;
			
			jq.ajax({ type:"POST"
				, url:'/itg/system/listWidget/deleteListWidgetList.do'
				, contentType: "application/json"
				, dataType: "json"
				, async: true
				, data : getArrayToJson(paramMap)
				, success:function(data){
					theFrom.getStore().load();
					theForm1.getStore().load();

					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
				}
			});

			break;
		
		case 'cancelPopupBtn' : 
			getGrid('listWidgetDeleteGrid').view.selModel.deselectAll();
			listWidgetDeletePopup.hide();
		break;
	}
} 


