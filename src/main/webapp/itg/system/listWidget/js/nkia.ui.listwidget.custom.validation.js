/**
 * 엑셀 다운로드 버튼
 */
function isValid(listId) {
	
	var param = $$('searchForm')._getValues();
	var isValid = true;
	
	switch(listId){
		case "workMngtList" :
			//작업계획일
			var C_PLAN_DT = param["C_PLAN_DT"];
			var PLAN_DT_startDate = param["PLAN_DT_startDate"];
			var PLAN_DT_endDate = param["PLAN_DT_endDate"];
			//작업일
			var C_WORK_DT = param["C_WORK_DT"];
			var WORK_DT_startDate = param["WORK_DT_startDate"];
			var WORK_DT_endDate = param["WORK_DT_endDate"];
			//최종결재자
			var C_MANAGER_NM  = param["C_MANAGER_NM"];
			var T_MANAGER_NM  = param["T_MANAGER_NM"];
			
			if (PLAN_DT_startDate != "") {
				if (C_PLAN_DT == "") {
					isValid = false;
				} 
			}
			
			if (WORK_DT_startDate != "") {
				if (C_WORK_DT == "") {
					isValid = false;
				} 
			}
			
			if (T_MANAGER_NM != "") {
				if (C_MANAGER_NM == "") {
					isValid = false;
				} 
			}
			
			break;
		
	}
	
	return isValid;
}