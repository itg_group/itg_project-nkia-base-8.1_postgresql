<!doctype html>
<html lang="ko">
	<!--
	*
	* 현황 마법사 등록 - 2단계
	* 2017.8.17
	* 
	*
	-->
	<head>
		<!-- ITG Base Page parse -->
		#parse("/itg/base/constraints/itg_constraint.nvf")
		<!-- ITG Base Javascript parse -->
		#parse("/itg/base/include/page_webix_inc.nvf")
	</head>
	<body>
		#parse("/itg/base/include/page_loading.nvf")
	
		<script type="text/javascript" charset="utf-8">
		
		var listId = '$!{list_id}';
		var submit_type = '$!{submit_type}';
		
		var preview_grid_column = {};	//미리보기 그리드 column
		var select_columns = {};	//미리보기 그리드 column
		
		webix.ready(function(){
			var schemaSearch = createUnionFieldComp({
				items: [
					createTextFieldComp({
						name: "search_table_name",
						label: "#springMessage('res.label.system.listWidget.00013')",
						placeholder: '#springMessage("msg.placeholder.search")',
						on:{
							onKeyPress: function(keycode) {
								var searchText = this.getValue();
								// Webix API에서 onKeyPress 이벤트 호출시 press 된 key의 값을 인자값으로 전달해줍니다.
								pressEnterKey(keycode, searchText); // 엔터키 이벤트 구현을 위한 함수
							}
						}
					})
				]
			});
			
			
			/**
			 * 엔터키 이벤트(Enter Key = 13)
			 */
			function pressEnterKey(keyCode, searchText){
				if (keyCode) {
					if (keyCode == "13") { //enter key
						searchList(searchText);
					}
				}
			}
			
			var schemaGrid = createGridComp({
				id : 'schemaGrid',
				keys: ["TABLE_NAME"],
				resizeColumn : true,
				checkbox : true,
				pageable : false,
				url : "/itg/system/listWidget/searchSchemaList.do",
				params : {
					list_id : listId
				},
				resource : "grid.system.listWidget.schema",
				header : {
					title : '#springMessage("res.label.system.listWidget.00048")'
				},
				dependency : { // 의존관계 그리드 사용
					targetGridId: "selectedGrid" // 대상 그리드 id
				},
				actionColumns: [{
					column : "ACT_COLUMN", // 액션 컬럼의 헤더명 (그리드 리소스)
					template : nkia.ui.html.icon("search", "show_row_preview"), // 버튼 아이콘("아이콘 명", "생성할 CSS명(onClick 이벤트 명과 매칭되어야 함)")
					onClick : { // 이벤트 연결(실행 할 이벤트명 매칭)
						"show_row_preview" : function(e, row, html){
							var info = this.getItem(row);
							var table_name = info.TABLE_NAME;

							var selected_table = new Array();
							selected_table.push(table_name);
							
							//미리보기 그리드 구현(선택한 테이블, relation 설정 반영여부)
							makePreview(selected_table, "N");
						}
					}
				}]
			});
			
			var selectedGrid = createGridComp({
				id : 'selectedGrid',
				keys: ["TABLE_NAME"],
				resizeColumn : true,
				checkbox : true,
				url : "/itg/system/listWidget/selectTableInfo.do",
				params : {
					list_id : listId
				},
				resource : "grid.system.listWidget.table",
				header : {
					title : '#springMessage("res.label.system.listWidget.00049")',
					headerNoCnt : true,
					buttons: {
						items: [
							createBtnComp({
								id : "btn1",
								label : '#springMessage("btn.common.preview")',
								click : function(){
									var data = $$("selectedGrid")._getRows();
									if (data.length > 0) {
										var selected_table = new Array();
										for (var i in data){
											var table_name = data[i].TABLE_NAME;
											selected_table.push(table_name);
										}
										
										//미리보기 그리드 구현(선택한 테이블, relation 설정 반영여부)
										makePreview(selected_table, "N");
									} else {
										nkia.ui.utils.notification({
							                type: 'error',
							                message: '#springMessage("msg.system.listWidget.00018")'
							            });
										return false;
									}
								}
							}),
							createBtnComp({
								id : "btn2",
								label : '#springMessage("btn.common.remove.trim")',
								click : function(){
									removeData();
								}
							})
						]
					}
				},
				// 액션 컬럼에 대한 정보를 세팅합니다. 액션 컬럼은 특정한 Action을 일으킬 수 있는 역할을 하는 컬럼을 의미합니다.
				// 행을 삭제하는 Action을 수행할 버튼을 배치합니다.
				actionColumns: [{
					column: "DEL_COLUMN", // 액션 컬럼의 헤더명 (그리드 리소스)
					template: nkia.ui.html.icon("remove", "remove_row"), // 버튼 아이콘("아이콘 명", "생성할 CSS명(onClick 이벤트 명과 매칭되어야 함)")
					onClick: { // 이벤트 연결(실행 할 이벤트명 매칭)
						"remove_row": removeData
					}
				}]
			});
			
			var relationGrid = createGridComp({
				id : 'relationGrid',
				keys: ["LEFT_KEY", "RIGHT_KEY"],
				resizeColumn : true,
				checkbox : true,
				url : "/itg/system/listWidget/selectRelationInfo.do",
				params : {
					list_id : listId
				},
				resource : "grid.system.listWidget.relation",
				header : {
					title : '#springMessage("res.label.system.listWidget.00050")',
					headerNoCnt : true,
					buttons: {
						items: [
							createBtnComp({
								id : "btn3",
								label : '#springMessage("btn.common.preview")',
								click : function(){
									var data = $$("selectedGrid")._getRows();
									if (data.length > 0) {
										var selected_table = new Array();
										for (var i in data){
											var table_name = data[i].TABLE_NAME;
											selected_table.push(table_name);
										}
										
										//미리보기 그리드 구현(선택한 테이블, relation 설정 반영여부)
										makePreview(selected_table, "Y");
									} else {
										nkia.ui.utils.notification({
							                type: 'error',
							                message: '#springMessage("msg.system.listWidget.00018")'
							            });
										return false;
									}
								}
							}),
							createBtnComp({
								id : "btn4",
								label : '#springMessage("btn.common.append.trim")',
								click : function(){
									addRowData();
								}
							}),
							createBtnComp({
								id : "btn5",
								label : '#springMessage("btn.common.remove.trim")',
								click : function(){
									removeRowData();
								}
							})
						]
					}
				}
			});
			
			var previewGrid = createGridComp({
				id : 'previewGrid',
				resizeColumn : true,
				autoLoad : false, // 메모리 그리드 사용 (데이터 로드 없음)
				select : false,
				height : 76,
				header : {
					title : '#springMessage("res.label.system.listWidget.00005")',
					headerNoCnt : true
				}
			});
			
			var stepImg;
			if (submit_type == "UPD") {
				stepImg =
					'<html>'
					+ '	<table style="margin-left: auto; margin-right: auto;">'
					+ '		<tr>'
					+ '			<td width="73">'
					+ '				<a href="$!{context}/itg/system/listWidget/listWidgetMenuConf.do?list_id='+listId+'&submit_type='+submit_type+'">'
					+ '					<img src="/itg/base/images/listWidget/Numbers-1-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '				</a>'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<img src="/itg/base/images/listWidget/Numbers-2-icon_on.png" width="68" height="68" border="0" alt="">'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<a href="$!{context}/itg/system/listWidget/listWidgetColumnConf.do?listId='+listId+'&submit_type='+submit_type+'">'
					+ '					<img src="/itg/base/images/listWidget/Numbers-3-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '				</a>'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<a href="$!{context}/itg/system/listWidget/listWidgetSearchConf.do?listId='+listId+'&submit_type='+submit_type+'">'
					+ '					<img src="/itg/base/images/listWidget/Numbers-4-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '				</a>'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<a href="$!{context}/itg/system/listWidget/listWidgetSaveConf.do?listId='+listId+'&submit_type='+submit_type+'">'
					+ '					<img src="/itg/base/images/listWidget/Numbers-5-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '				</a>'
					+ '			</td>'
					+ '		</tr>'
					+ '	</table>'
					+ '</html>';
			} else {
				stepImg =
					'<html>'
					+ '	<table style="margin-left: auto; margin-right: auto;">'
					+ '		<tr>'
					+ '			<td width="73">'
					+ '				<img src="/itg/base/images/listWidget/Numbers-1-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<img src="/itg/base/images/listWidget/Numbers-2-icon_on.png" width="68" height="68" border="0" alt="">'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<img src="/itg/base/images/listWidget/Numbers-3-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<img src="/itg/base/images/listWidget/Numbers-4-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '			</td>'
					+ '			<td width="73">'
					+ '				<img src="/itg/base/images/listWidget/Numbers-5-icon_off.png" width="68" height="68" border="0" alt="">'
					+ '			</td>'
					+ '		</tr>'
					+ '	</table>'
					+ '</html>';
			}
			
			var stepPanel = {
				cols : [{
					view : "template", height : 93, template : stepImg
				}]
			};
			
			var view = {
				view: "scrollview",
				id: "app",
				body: {
					rows: [
						{
							rows: new Array().concat(stepPanel)
						},
					    {
							cols: [
							    {
					    	   		rows: new Array().concat(schemaSearch, schemaGrid)
					    	   	},{ // 화살표 버튼
							    	view: "template",
							    	template: nkia.ui.html.icon("arrow_right", "add_row"),
							    	width: 30,
								 	onClick: { // 이벤트 연결
								 		"add_row": addData
								 	}
						       },{
						    	   width: 380,
						    	   height: 480,
						    	   rows: new Array().concat(selectedGrid)
						       },{ // 여백
						    	   view: "template",
						    	   width: 10
						       },{
						    	   rows: new Array().concat(relationGrid)
						       }
							]
						}, {
							rows: new Array().concat(previewGrid)
						}, createButtons({
					        align: "center",
					        items: [
					            createBtnComp({visible: true, label: '#springMessage("btn.system.listWidget.00002")', id:'prevBtn', click:"movePrev"}),
					            createBtnComp({visible: true, label: '#springMessage("btn.system.listWidget.00001")', type:'form', id:'nextBtn', click:"validateGrid"})
					        ]
					    })
				    ]
				}
			};
		
			nkia.ui.render(view);
			
		});
		
		
		/**
		 * 검색
		 */
		function searchList(searchText){
			// 검색폼의 값들 추출
			var paramMap = {};
			paramMap["search_param"] = "search";
			paramMap["search_table_name"] = searchText;
			
			// 검색폼의 데이터를 인자값으로 그리드 리로드
			$$("schemaGrid")._reload(paramMap);
		}
		
		/**
		 * 이전단계
		 */
		function movePrev(){
			if (confirm("#springMessage('msg.system.listWidget.00004')")){
				window.location.href = '$!{context}/itg/system/listWidget/listWidgetMenuConf.do?list_id='+listId+'&submit_type='+submit_type;
			}
		}
		
		/**
		 * 저장
		 */
		function validateGrid(){
			//row count
			var selected_row_count = $$("selectedGrid").count();
			var relation_row_count = $$("relationGrid").count();
			//table data
			var selectedData = $$("selectedGrid")._getRows();
			var relationData = $$("relationGrid")._getRows();
			
			var table_name = "";
			for (var i in selectedData) {
				table_name += selectedData[i].TABLE_NAME + ",";
			}
			table_name = table_name.substring(0, table_name.length-1);
			
			var relation = "";
			for (var i in relationData) {
				relation += relationData[i].LEFT_KEY.toUpperCase() + "=" + relationData[i].RIGHT_KEY.toUpperCase() + ",";
			}
			relation = relation.substring(0, relation.length-1);
			
			var url;
			if (submit_type == "UPD") {
				if (!confirm('#springMessage("msg.common.confirm.00002")')) {
					return false;
				}
				
				url = '$!{context}/itg/system/listWidget/updateSchemaRealationConf.do';
			} else {
				if (!confirm('#springMessage("msg.common.confirm.00001")')) {
					return false;
				}
				
				url = '$!{context}/itg/system/listWidget/insertSchemaRealationConf.do';
			}
			
			//validation
			if (selected_row_count < 1) {
				nkia.ui.utils.notification({
	                type: 'info',
	                message: '#springMessage("msg.system.listWidget.00020")'
	            });
				return false;
			}
			
			if (selected_row_count > 1) {
				if (relation_row_count < 1) {
					nkia.ui.utils.notification({
		                type: 'info',
		                message: '#springMessage("msg.system.listWidget.00021")'
		            });
					return false;
				}
				
				for (var i = 0; i < relation_row_count; i++) {
					if (relationData[i]['LEFT_KEY'] == "" || typeof relationData[i]['LEFT_KEY'] == "undefined") {
						nkia.ui.utils.notification({
			                type: 'info',
			                message: '#springMessage("msg.system.listWidget.00021")'
			            });
						return false;
					}
					
					if (relationData[i]['RIGHT_KEY'] == "" || typeof relationData[i]['RIGHT_KEY'] == "undefined") {
						nkia.ui.utils.notification({
			                type: 'info',
			                message: '#springMessage("msg.system.listWidget.00021")'
			            });
						return false;
					}
				}
			}
			
			
			// 실제 데이터 등록 ajax
			nkia.ui.utils.ajax({
				url: url,
				params: {
					schema_nm : table_name,
					join_key : relation,
					list_id : listId
				},
				success : function(data){
					if (data.success) {
//						alert(data.resultMsg);
						nkia.ui.utils.notification({
			                type: 'info',
			                message: data.resultMsg,
			                expire: 2000
			            });
						window.location.href = '$!{context}/itg/system/listWidget/listWidgetColumnConf.do?listId='+listId;
					}
				},
				error : function(){
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '#springMessage("msg.system.error")'
		            });
				}
			});
		}
		
			
		/**
		 * 데이터 추가
		 */
		function addData(){
			// 체크 된 데이터 추출
			var data = $$("schemaGrid")._getSelectedRows();
			
			// 선택 한 데이터가 없을 시, 메시지를 띄워줍니다.
			if(data.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '#springMessage("msg.itam.opms.00010")'
				});
				return false;
			}
			
			// 체크된 데이터를 대상 그리드에 add 합니다.
			$$("selectedGrid")._addRow(data);
			
			// 2개 이상의 테이블이 선택되었으면 Relation 설정 테이블에 row 추가
			var schemaCnt = Number($$("schemaGrid")._getSelectedRows().length);
			var selectedCnt = Number($$("selectedGrid")._getRows().length);
			
			if (schemaCnt == 1 && selectedCnt > 1) {
				//체크한 값이 하나이고, 선택된 테이블이 다수 행일때
				//하나의 행만 처음에 선택할 때 relation 설정에 행이 추가되지 않도록 조건 추가
				addRowData();
			} else {
				//체크한 schema 보다 하나 작게 relation 설정에 행이 추가되도록
				for (var i = 1; i < schemaCnt; i++){
					addRowData();
				}
			}
			
			// add 된 데이터는 선택하지 못하도록 하기 위해 그리드를 다시 로드합니다.
			// 단, 현재 포커스 되어 있는 페이지를 유지합니다.
			$$("schemaGrid")._reload({}, false); // 두 번째 인자값 : 페이지 유지 여부
		}
		
		/**
		 * 데이터 삭제
		 */
		function removeData(e, row, html){
			$$("selectedGrid")._removeRow(row);
			
			// 선택하지 못하는 데이터를 다시 선택가능으로 되돌리기 위해 그리드를 다시 로드합니다.
			// 단, 현재 포커스 되어 있는 페이지를 유지합니다.
			$$("schemaGrid")._reload({}, false); // 두 번째 인자값 : 페이지 유지 여부
			
			//선택된 테이블에서 하나의 행 삭제시 마지막 행이 남기 전까지 relation 설정의 행도 함께 삭제
			//relation 설정에서는 마지막 행이 삭제되어야 하므로 상단의 row제거(전체삭제)와 구분하기 위해 마지막 id 부여
			var selectedCnt = Number($$("selectedGrid")._getRows().length);
			if (selectedCnt > 0) {
				removeRowData($$("relationGrid").getLastId());
			}
		}
		
		//미리보기를 위한 컬럼 정보 및 헤더 정보
		function makePreview(selected_table, relSetYn){
			// 컬럼 조회 ajax
			nkia.ui.utils.ajax({
				url: '$!{context}/itg/system/listWidget/searchSchemaColmnName.do',
				params: {
					table_name : selected_table
				},
				success:function(data){
					//조회된 컬럼정보
					var temp = data.gridVO.rows;
					preview_grid_column = new Array(temp.length);
					select_columns = new Array(temp.length);
					
					if (relSetYn == 'Y') {
						//Relation 설정 테이블
						var relationData = $$("relationGrid")._getRows();
						var relation_arr;
						var relation = "";
						
						if ($$("relationGrid").count() > 0){
							for (var i in relationData) {
								var leftKeyVal = relationData[i].LEFT_KEY.toUpperCase();
								var rightKeyVal = relationData[i].RIGHT_KEY.toUpperCase();
//								
//								leftKeyVal.replace("\"", "'");
//								rightKeyVal.replace("\"", "'");
								
								relation += leftKeyVal + "=" + rightKeyVal + ",";
							}
							relation = relation.substring(0, relation.length-1);
							relation_arr = relation.split(",");
							var validateChk = true;
							
							for (var i in relationData) {
								if (relationData[i].LEFT_KEY == undefined || relationData[i].LEFT_KEY == "") {
									validateChk = false;
								}
								
								if (relationData[i].RIGHT_KEY == undefined || relationData[i].RIGHT_KEY == "") {
									validateChk = false;
								}
							}
							
							if (!validateChk) {
								nkia.ui.utils.notification({
					                type: 'error',
					                message: '#springMessage("msg.system.listWidget.00019")'
					            });
								return false;
							}
						} else {
							nkia.ui.utils.notification({
				                type: 'error',
				                message: '#springMessage("msg.system.listWidget.00021")'
				            });
							return false;
						}
					}
					
					//미리보기를 위한 컬럼값을 그리드의 header 정보로 사용하기 위해 처리
					var header_grid_texts;
					var header_grid_headers;
					var header_gird_widths;
					var header_flexs;
					var header_sortable;
					var header_align;
					var header_xtype;
					
					for (var i=0, ii=temp.length; i<ii; i++) {
						var column = temp[i].TABLE_NM + "." + temp[i].COLUMN_NM;
						
						if ("CLOB" == temp[i].DATA_TYPE || "BLOB" == temp[i].DATA_TYPE) {
							select_columns[i] = "dbms_lob.substr(" + column + ", 100, 1) as " + temp[i].COLUMN_NM;
						} else {
							select_columns[i] = column + " as " + temp[i].COLUMN_NM;
						}
						
						if (header_grid_texts == undefined) {
							header_grid_texts = temp[i].COLUMN_NM;
							header_grid_headers = temp[i].COLUMN_NM;
							header_gird_widths = "100";
							header_flexs = 'na';
							header_sortable = 'na';
							header_align = 'left';
							header_xtype = 'na';
						} else {
							header_grid_texts += "," + temp[i].COLUMN_NM;
							header_grid_headers += "," + temp[i].COLUMN_NM;
							header_gird_widths += "," + "100";
							header_flexs += "," + '1';
							header_sortable += "," + 'na';
							header_align += "," + 'left';
							header_xtype += "," + 'na';
						}
					}
					
					preview_grid_column = {
						gridText : header_grid_texts,
						gridHeader : header_grid_headers,
						gridWidth : header_gird_widths,
						gridFlex : header_flexs,
						gridSortable : header_sortable,
						gridAlign : header_align,
						gridXtype : header_xtype
					};
					
					$$("previewGrid").clearAll();
					
					reCreateGrid(selected_table, select_columns, relation_arr, preview_grid_column);
				},
				error: function(){
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '#springMessage("msg.system.error")'
		            });
					return false;
				}
			});
		}
		
		//미리보기 그리드에 조회된 정보 셋팅하여 출력
		function reCreateGrid(table_name, select_columns, relation_arr, header_column){
			var paramMap = {};
			
			nkia.ui.utils.ajax({
				url : "/itg/system/listWidget/selectSchemaRows.do",
				params : {
					table_name : table_name,
					select_columns : select_columns,
					relation_arr : relation_arr
				},
				success : function(data){
					if(data.success){
						paramMap = data.gridVO.rows;
						$$("previewGrid").parse(paramMap);
					} else {
						nkia.ui.utils.notification({
			                type: 'error',
			                message: data.resultMsg
			            });
						return false;
					}
				},
				error : function(){
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '#springMessage("msg.system.error")'
		            });
					return false;
				}
			});
			
			// _updateHeader 함수 사용
			$$("previewGrid")._updateHeader(header_column);
			$$("previewGrid").refresh();
			$$("previewGrid").define("height", 215);
			$$("previewGrid").resize();
		}
		
		/**
		 * relation 테이블 행 추가
		 */
		function addRowData() {
			$$("relationGrid")._addRow();
		}

		/**
		 * relation 테이블 행 삭제
		 */
		function removeRowData(row) {
			if (row != undefined) {
				$$("relationGrid")._removeRow(row);
			} else {
				$$("relationGrid")._removeRow();
			}
		}
		</script>
	</body>
</html>