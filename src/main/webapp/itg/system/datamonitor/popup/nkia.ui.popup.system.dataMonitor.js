/********************************************
 * User
 * Date: 2018-02-06
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.dataMonitorDtl = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = "/itg/system/datamonitor/searchDataMonitorCheckDataList.do";  // 점검결과 데이타 조회 
	this.resource = ""; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	
	// 구성요소 Properties
	this.window = {};
	this.detailForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
		var buttonItems = [];
		
		fieldItems = [];
		fieldItems.push(/*데이타모니터id*/{ colspan:1, item: createTextFieldComp({label:'데이타모니터id', name:'datamon_id', readonly:true})});
		fieldItems.push(/*데이타모니터명*/{ colspan:1, item: createTextFieldComp({label:'데이타모니터명', name:'datamon_nm', readonly:true})});
		fieldItems.push(/*점검일시*/{ colspan:1, item: createTextFieldComp({label:'점검일시', name:'run_dt', readonly:true})});
		fieldItems.push(/*점검상태*/{ colspan:1, item: createTextFieldComp({label:'점검상태', name:'check_status', readonly:true})});
		
		this.detailForm = createFormComp({
			id: this.id + '_detailForm',
			header: {
				icon: "search",
	        	title: "점검상세"
		    },
		    fields: {
		    	elementsConfig: { labelWidth: 120, labelPosition: "left" },
		        colSize: 2,
		        items:fieldItems
		    }
		});
		
		buttonItems = [];
		buttonItems.push(/*[엑셀다운로드]*/createBtnComp({ id: this.id + 'excelBtn', label:"엑셀다운로드", type: "form", width : 120, click: this.excelDownloadPop.bind(_this)}) );
        
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: "점검결과 데이타",
	        	buttons: { items: buttonItems }
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			autoLoad :false, // 자동조회 안함 (조회 후 callback을 임으로 주기위함) 
			pageSize: this.pageSize,
			keys: ["CHECK_NO"],
			dependency: this.dependency
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.detailForm, this.grid)
	                }]
	    		}]
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		if(null == this.params.out_id_list){
			//alert("조회컬럼정보가 없습니다.");
			return;
		}
		var paramMap = {};
		paramMap["mode"] = "selectList"; // {selectList, exceldown} 
		paramMap["check_no"] = this.params.check_no;
		paramMap["out_id_list"] = this.params.out_id_list;
		$$(this.id + '_grid')._removeAllRow();
		$$(this.id + '_grid')._reload(paramMap, 0, this.baseGrid_reload_callback());
	}
	
	/**
	 * 조회 후 Callback 
	 **/
	this.baseGrid_reload_callback = function(){

		var out_id_list = this.params.out_id_list;
		var out_nm_list = this.params.out_nm_list;
		
		var out_id_array = (""+out_id_list).split(",");
		var out_nm_array = (""+out_nm_list).split(",");
		var header_id = "ROW_NO,";
		var text_val = "번호,";
		var width_val = "80,";
		var flex_val = "0,";
		var sortable_val = "na,";
		var xtype_val = "na,";
		var align_val = "center,";
		
		for(var i = 0 ; i < out_id_array.length ; i ++) {
			header_id += (0<i?",":"") + out_id_array[i];
			text_val += (0<i?",":"") + out_nm_array[i];
			width_val += (0<i?",":"") + "100";
			flex_val += (0<i?",":"") + "1";
			sortable_val += (0<i?",":"") + "na";
			xtype_val += (0<i?",":"") + "na";
			align_val += (0<i?",":"") + "center";
		}
		
		var header_column = {
			gridHeader : header_id, 
			gridText : text_val, 
			gridWidth : width_val,
			gridFlex : flex_val,
			gridSortable : sortable_val,
			gridXtype : xtype_val,
			gridAlign : align_val
		}
		
		$$(this.id + '_grid')._updateHeader(header_column);
		$$(this.id + '_grid').refresh();
	}

	/**
	 * 엑셀다운로드
	 **/
	this.excelDownloadPop = function(){
		
		var datamon_nm = this.params.datamon_nm;
		var select_cols = this.params.out_id_list;
		var select_txts = this.params.out_nm_list;
		
		var paramMap = {};
		paramMap["mode"] = "exceldown"; // {selectList, exceldown} 
		paramMap["check_no"] = this.params.check_no;
		paramMap["out_id_list"] = this.params.out_id_list;
		
		var excelParam = {};
		excelParam['fileName'] 			= datamon_nm; // 엑셀파일명
		excelParam['sheetName'] 		= datamon_nm; // Sheet0 이름
		excelParam['titleName'] 		= datamon_nm; // 제목
		excelParam['includeColumns'] 	= "ROW_NO," + select_cols; // 다운로드엑셀파업 ID 열
		excelParam['headerNames'] 		= "번호," + select_txts; // 다운로드엑셀파업 이름 열
		excelParam['defaultColumns'] 	= "ROW_NO," + select_cols;
		excelParam['url'] 				= this.url; // 다운로드 URL
		nkia.ui.utils.window({id: "assetExcelPop", type: "Dynamic", param: paramMap, excelParam: excelParam, title: "엑셀다운로드", callFunc: createExcelTabWindow }); 
	}
};

/**
 * 유지보수 팝업
 * @param {} popupProps
 */
function createDataMonitorWindow(popupProps) {
	
	var props = popupProps||{};
	var popupObj;
	try {
		switch (props.type) {
			case "dataMonitorCheckData":
				popupObj = new nkia.ui.popup.system.dataMonitorDtl();
				break;	
			default:
				popupObj = new nkia.ui.popup.system.dataMonitorDtl();
		}
		
		popupObj.setProps(props);
		popupObj.setRender(props);
		popupObj.setWindow();
		
		// 초기화
		switch (props.type) {
			case "dataMonitorCheckData":
				popupObj.doSearch(); // 조회실행
				var paramMap = {};
				$$(popupObj.id + '_detailForm')._setValues(nkia.ui.utils.copyObj(popupObj.params));
				break;	
			default:
				;
		}
		
	} catch(e) {
		alert(e.message);
	}
}
