<!doctype html>
<html lang="ko">
<!--
*//**
 * 시스템권한관리 페이지
 * @author <a href="mailto:minsoo@nkia.co.kr"> Minsoo Jeon
 * 
 *//*
-->
<head>
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>

<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<script>
var chkAuthNm = null;
Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	var systemAuthInsertModeBtn = createBtnComp({visible: true, label: '#springMessage("btn.system.00005")', id:'systemAuthInsertModeBtn'});
	var systemAuthInsertBtn = createBtnComp({visible: false, label: '#springMessage("btn.common.insert")', id:'systemAuthInsertBtn', ui:'correct', scale:'medium'});
	var systemAuthDeleteBtn = createBtnComp({visible: false, label: '#springMessage("btn.common.delete")', id:'systemAuthDeleteBtn', scale:'medium'});
	var systemAuthUpdateBtn = createBtnComp({visible: false, label: '#springMessage("btn.common.insert")', id:'systemAuthUpdateBtn', ui:'correct', scale:'medium'});
	var systemAuthInsertBackBtn = createBtnComp({visible: false, label: '#springMessage("btn.common.cancel")', id:'systemAuthInsertBackBtn', scale:'medium'});
	
	attachBtnEvent(systemAuthInsertModeBtn, actionLink,  'modeChange');	
	attachBtnEvent(systemAuthInsertBtn, actionLink,  'insert');	
	attachBtnEvent(systemAuthDeleteBtn, actionLink,  'delete');	
	attachBtnEvent(systemAuthUpdateBtn, actionLink,  'update');	
	attachBtnEvent(systemAuthInsertBackBtn, actionLink,  'back');	
    
	//메뉴체크트리 
	var setSystemMenuMappingTreeProp = {
		context: '${context}',						
		id : 'setSystemMenuMappingTreeProp',
		url : '$!{context}/itg/system/systemAuth/searchCheckedMenuMappingTree.do',
		title : '#springMessage("res.title.system.auth.tree")',
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		collapseAllBtn : true,
		params: null,
		rnode_text : '#springMessage("res.00003")',
		expandLevel: 4
	}
	var checkedMenuMappingTree = createTreeComponent(setSystemMenuMappingTreeProp);
	
	var setSystemAuthGridProp = {	
		context: '${context}',						
		id: 'setSystemAuthGridProp',	
		title : '#springMessage("res.title.system.authList")',
		height: 300,
		resource_prefix: 'grid.system.systemauth',				
		url: '${context}/itg/system/systemAuth/searchSystemAuthList.do',	
		params: null,
		autoLoad: true,
		dragPlug: false,
		border: true,
		emptyMsg: '#springMessage("msg.system.result.00012")',
		toolBarComp: []
	}
	var systemAuthGrid = createGridComp(setSystemAuthGridProp);
	
	var setEditAuthProp = {
		id: 'setEditAuthProp',
		title: '#springMessage("res.label.system.00011")',
		columnSize: 2,
		editOnly: true,
		border: false,
		formBtns: [systemAuthInsertModeBtn],
		tableProps :[					
					{colspan:1, item : createTextFieldComp({label:"#springMessage('res.label.system.00007')", name:'auth_nm', maxLength : FIELD_DOMAIN.NM_50, notNull : true})}
					,{colspan:1, item : createUnionCheckTextContainer({label:"#springMessage('res.label.system.00019')", name:'auth_id', btnId: 'systemCheckBtn', btnLabel:'#springMessage("btn.common.validation")', targetForm:'setEditAuthProp', checkUrl:'/itg/system/systemAuth/checkAuthIdComp.do', disabled: true, successMsg: '#springMessage("msg.system.result.00035")', failMsg: '#springMessage("msg.system.result.00034")', maxLength : FIELD_DOMAIN.NM_100, vtype : 'bigAlpha', notNull : true})}
					,{colspan:2, item : createCodeRadioComp({label:'#springMessage("res.00008")', name:'use_yn', code_grp_id: 'YN', columns:2, isNotColspan: true})}	
			        ,{colspan:2, item : createTextAreaFieldComp({label:'#springMessage("res.00009")', name:'auth_desc', anchor: '94%', maxLength : FIELD_DOMAIN.VAL_1000})}
					,{colspan:1, item : createTextFieldComp({label:'#springMessage("res.common.validationYn")', name:'overLapYNauth_id', id:'overLapYNauth_id', value: 'N', notNull:false, maxLength: FIELD_DOMAIN.NM_100})}
					],
		hiddenFields:[]
	}
	var systemEditForm = createInnerEditorFormComp(setEditAuthProp);
	
	// Viewport 설정
	var viewportProperty = {
		west: { 
			minWidth: 210, 
			maxWidth: 350, 
			items : [systemAuthGrid] 
		},
		center: { 
			items : [systemEditForm, checkedMenuMappingTree], 
			buttons: [systemAuthInsertBtn, systemAuthUpdateBtn, systemAuthDeleteBtn, systemAuthInsertBackBtn]  
		}
	}
	
	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	
	attachCustomEvent('cellclick', systemAuthGrid, gridCellClick);
	Ext.getCmp('overLapYNauth_id').hide();
	
	formEditable({id : 'setEditAuthProp', editorFlag : false, excepArray : ['systemAuthInsertModeBtn'] });
	
	//2019.03.08 molee 사용자그룹ID검증 추가
	attachCustomEvent('change', Ext.getCmp("setEditAuthProp").getFormField('auth_id'), function(){
		var editForm = Ext.getCmp('setEditAuthProp');
		editForm.setFieldValue('overLapYNauth_id', '');
	});
	
});

//Layout Resise Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["setEditAuthProp"], "setSystemMenuMappingTreeProp");
}

function gridCellClick( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {
	// 마스크를 띄웁니다.
	setViewPortMaskTrue();
	
	// 필요한 필드 readOnly 속성 주기 위함
	formEditable({id : 'setEditAuthProp', editorFlag : false, excepArray : ['systemAuthInsertModeBtn'] });
	
	var theForm = Ext.getCmp('setEditAuthProp');
	//히든값 setValue
	var auth_id = record.raw.AUTH_ID;
	chkAuthNm = record.raw.AUTH_NM;	
	//필드값 setValue
	var clickRecord = record.raw;
	
	jq.ajax({ type:"POST"
		, url:'$!{context}/itg/system/system/selectSystemAuthInfo.do'
		, contentType: "application/json"
		, dataType: "json"
		, async: true
		, data : getArrayToJson({auth_id : auth_id})
		, success:function(data){
			theForm.setDataForMap(data.resultMap);
		}
	});
	
	//버튼셋팅 초기화 
	var hideIds = ['systemAuthInsertBtn', 'systemAuthInsertBackBtn'];	
	hideBtns(hideIds);	//버튼을 숨긴다.
	var showIds = ['systemAuthInsertModeBtn', 'systemAuthDeleteBtn', 'systemAuthUpdateBtn'];	
	showBtns(showIds);
	
	var menuMappingTree = getGrid("setSystemMenuMappingTreeProp");
	
	var menuMappingTreeStore = menuMappingTree.getStore();
	
	var rootNode = menuMappingTreeStore.getRootNode();
	while(rootNode.firstChild){ // Root의 ChildNode가 있을 경우 
		rootNode.removeChild(rootNode.firstChild);
	}
	menuMappingTreeStore.sync();
	
	menuMappingTreeStore.proxy.jsonData={auth_id: auth_id, expandLevel: 1};
	menuMappingTreeStore.load();
	menuMappingTreeStore.sync();
	
	var storeLoaded = setInterval(function () {
		if (!menuMappingTreeStore.isLoading()) {	// Store Loading 시에 Tree 전체 확장
			menuMappingTree.getExpandBtn().fireEvent("click");
			// 마스크를 숨깁니다.
			setViewPortMaskFalse();
			
	    	clearInterval(storeLoaded);
	 	}
		//rest of function
	}, 100); // refresh every 100 milliseconds
	
	formEditable({id : 'setEditAuthProp', editorFlag : true, excepArray : ['auth_id'] });
	
	Ext.getCmp('systemCheckBtn').setDisabled(true);
}

function pageInit(){
	
	var hideIds = ['systemAuthInsertBtn', 'systemAuthInsertBackBtn', 'systemAuthDeleteBtn', 'systemAuthUpdateBtn'];	
	hideBtns(hideIds);	//버튼을 숨긴다.
	
	var showIds = ['systemAuthInsertModeBtn'];	
	showBtns(showIds);
	
	formEditable({id : 'setEditAuthProp', editorFlag : false, excepArray : ['systemAuthInsertModeBtn'] });
	Ext.getCmp('setEditAuthProp').initData();	
	
	var systemGrid = getGrid("setSystemAuthGridProp").getStore();
	systemGrid.proxy.jsonData={};
	systemGrid.load();
}

function actionLink(command) {
	var theForm = Ext.getCmp('setEditAuthProp');

	switch(command) {
		case 'modeChange' :
			Ext.getCmp('setEditAuthProp').initData();	
			
		   	var hideIds = ['systemAuthInsertModeBtn', 'systemAuthDeleteBtn', 'systemAuthUpdateBtn'];	
		   	hideBtns(hideIds);	//버튼을 숨긴다.
		    	
		   	var showIds = ['systemAuthInsertBtn', 'systemAuthInsertBackBtn'];	
		   	showBtns(showIds);
		    	
		   	var menuMappingTree = getGrid("setSystemMenuMappingTreeProp").getStore();
		   	menuMappingTree.proxy.jsonData={auth_id: '0', expandLevel: 4};
			menuMappingTree.load();
			
			formEditable({id : 'setEditAuthProp', editorFlag : true, excepArray : ['systemAuthInsertModeBtn'] });
			Ext.getCmp('systemCheckBtn').setDisabled(false);
			
			
			
		break;
		case 'insert' :
			if(theForm.isValid()){
				var paramMap = {};
				var theForm = Ext.getCmp('setEditAuthProp');
				var nullCheck = theForm.getFieldValue('auth_nm');
				var nullCheckId = theForm.getFieldValue('auth_id');
				var idCheckYN = Ext.getCmp('setEditAuthProp').getFieldValue('overLapYNauth_id');
				
				if(nullCheck == "" || nullCheck == null){
					showMessage('#springMessage("msg.system.result.00004")');
					return;
				}
				if(nullCheckId == "" || nullCheckId == null){
					showMessage('#springMessage("msg.system.result.00006")');
					return;
				}
				if(idCheckYN == "N"  || idCheckYN == ""){
					showMessage('#springMessage("msg.system.result.00022")');
					return;
				}
				/*if(nmCheckYN == "N"  || nmCheckYN == ""){
					showMessage('#springMessage("msg.system.result.00037")');
					return;
				}*/
				
				// 마스크를 띄웁니다.
				setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00001")')){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return false;
				}
				
				//체크트리 getValue
				var selectedRecords = Ext.getCmp('setSystemMenuMappingTreeProp').view.getChecked();
	           	node_id = [];
	           	
	            Ext.Array.each(selectedRecords, function(rec){
	               	node_id.push(rec.raw.node_id);
	            });
	                
	            var mappingInfo = {};
	            mappingInfo['menu_id'] = node_id;
	            mappingInfo['auth_id'] = nullCheckId;
	               
				var insertNameValue = Ext.getCmp('setEditAuthProp').getInputData();
					
	
				paramMap['insertNameValue'] = insertNameValue;
				paramMap['mappingInfo'] = mappingInfo;
					
				jq.ajax({ 
					type:"POST", 
					url:'$!{context}/itg/system/systemAuth/insertSystemAuth.do', 
					contentType: "application/json", 
					dataType: "json", 
					async: true, 
					data: getArrayToJson(paramMap), 
					success:function(data){
		    			if( data.success ){
			    			
			    			showMessage(data.resultMsg);
							//Ext.getCmp('auth_id').setReadOnly(true);
							//Ext.getCmp('auth_nm').setReadOnly(true);
							pageInit();
							Ext.getCmp('systemCheckBtn').disable();
							Ext.getCmp('setEditAuthProp').initData();	
							var menuMappingTree = getGrid("setSystemMenuMappingTreeProp").getStore();
						   	menuMappingTree.proxy.jsonData={auth_id: '0', expandLevel: 4};
							menuMappingTree.load();
		    			}else{
		    				showMessage(data.resultMsg);
		    			}
		    			
		    			// 마스크를 숨깁니다.
						setViewPortMaskFalse();
		    		}
				})	
			}else{
				showMessage('#springMessage("msg.common.00011")');
				return false;
			}
		break;
		case 'update' :
			if(theForm.isValid()){

				var paramMap = {};
					
				Ext.getCmp('setEditAuthProp').setFieldValue('overLapYNauth_id', 'Y');
				
				var theForm = Ext.getCmp('setEditAuthProp');
				var nullCheckId = theForm.getFieldValue('auth_id');
				var nullCheckNm = theForm.getFieldValue('auth_nm');
				
				
				if(nullCheckId == "" || nullCheckId == null){
					showMessage('#springMessage("msg.system.result.00017")');
					return;
				}
				if(nullCheckNm == "" || nullCheckNm == null){
					showMessage('#springMessage("msg.system.result.00004")');
					return;
				}			
				
				// 마스크를 띄웁니다.
				setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00002")')){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return false;
				}
				
				//체크트리 getValue
				var selectedRecords = Ext.getCmp('setSystemMenuMappingTreeProp').view.getChecked();
	           	node_id = [];
	            	
	            Ext.Array.each(selectedRecords, function(rec){
	              	node_id.push(rec.raw.node_id);
	            });
	                
	            var mappingInfo = {};
	            mappingInfo['menu_id'] = node_id;
	            mappingInfo['auth_id'] = nullCheckId;
	              
				var insertNameValue = Ext.getCmp('setEditAuthProp').getInputData();
					
				paramMap['insertNameValue'] = insertNameValue;
	            paramMap['mappingInfo'] = mappingInfo;
	            
				jq.ajax({ 
					type:"POST", 
					url:'$!{context}/itg/system/systemAuth/updateSystemAuth.do', 
					contentType: "application/json", 
					dataType: "json", 
					async: true, 
					data : getArrayToJson(paramMap), 
					success:function(data){
		    			if( data.success ){
			    			
			    			showMessage(data.resultMsg);
			    			//showMessage('#springMessage("msg.common.result.00002")');
			    			Ext.getCmp('setEditAuthProp').setFieldValue('overLapYNauth_id', 'N');
							pageInit();
							Ext.getCmp('systemCheckBtn').disable();
		    			}else{
		    				showMessage(data.resultMsg);
		    			}
		    			
		    			// 마스크를 숨깁니다.
						setViewPortMaskFalse();
		    		}
				});
			}else{
				showMessage('#springMessage("msg.common.00011")');
				return false;
			}
		break;
		case 'delete' :
			var theForm = Ext.getCmp('setEditAuthProp');
			var nullCheckId = theForm.getFieldValue('auth_id');
			if(nullCheckId == "" || nullCheckId == null){
				showMessage('#springMessage("msg.system.result.00040")');
				return;
			}
			
			// 마스크를 띄웁니다.
			setViewPortMaskTrue();
			
			if(!confirm('#springMessage("msg.common.confirm.00003")')){
				// 마스크를 숨깁니다.
				setViewPortMaskFalse();
				return false;
			}
			
			var deleteValue = Ext.getCmp('setEditAuthProp').getInputData();
			
			jq.ajax({ 
				type:"POST", 
				url:'$!{context}/itg/system/systemAuth/deleteSystemAuth.do', 
				contentType: "application/json", 
				dataType: "json", 
				async: true, 
				data : getArrayToJson(deleteValue), 
				success:function(data){
	    			if( data.success ){
		    			
		    			showMessage(data.resultMsg);
		    			Ext.getCmp('setEditAuthProp').setFieldValue('overLapYNauth_id', 'N');
		    			formEditable({id : 'setEditAuthProp', editorFlag : false, excepArray : ['systemAuthInsertModeBtn'] });
		    			pageInit();
		    			Ext.getCmp('systemCheckBtn').disable();
		    			Ext.getCmp('setEditAuthProp').initData();	
		    			var menuMappingTree = getGrid("setSystemMenuMappingTreeProp").getStore();
						menuMappingTree.proxy.jsonData={auth_id: '0', expandLevel: 4};
						menuMappingTree.load();
	    			}else{
	    				showMessage(data.resultMsg);
	    			}
	    			
	    			// 마스크를 숨깁니다.
					setViewPortMaskFalse();
	    		}
			});
		break;
		case 'back' :
				Ext.getCmp('systemCheckBtn').disable();
				formEditable({id : 'setEditAuthProp', editorFlag : false, excepArray : ['systemAuthInsertModeBtn'] });
				pageInit();
				Ext.getCmp('setEditAuthProp').initData();	
				var menuMappingTree = getGrid("setSystemMenuMappingTreeProp").getStore();
				menuMappingTree.proxy.jsonData={auth_id: '0', expandLevel: 4};
				menuMappingTree.load();
	    break;
	}
}

</script>
</head>
<body>
<div id="loading-mask"></div>
<div id="loading">
  <div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>
</body>
</html>