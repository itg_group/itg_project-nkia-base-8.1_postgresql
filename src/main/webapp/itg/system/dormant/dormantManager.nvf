<!doctype html>
<html lang="ko">
<!--
/**
 * 시스템관리
 * 휴면계정 관리페이지
 * @author <a href="mailto:minsoo@nkia.co.kr"> Minsoo Jeon
 * 
 */
-->
<head>
<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>
<script>

Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	var centerCode = getConstText({isArgs : true,m_key : 'Globals.CenterCustId'});
	
	var dormantFreeBtn = createBtnComp({visible: true, label: '휴면 해제', id:'dormantFreeBtn', ui:'correct', scale:'medium'});
	var setUseNBtn = createBtnComp({visible: true, label: '계정 사용불가', id:'setUseNBtn', ui:'correct', scale:'medium'});
	var userSearchBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.search")', id:'userSearchBtn', ui:'correct'});
	var userSearchInitBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.reset")', id:'userSearchInitBtn'});

    attachBtnEvent(dormantFreeBtn, actionLink,  'dormantFree');
    attachBtnEvent(setUseNBtn, actionLink,  'setUseN');
	attachBtnEvent(userSearchBtn, actionLink,  'search');	
	attachBtnEvent(userSearchInitBtn, actionLink,  'searchInit');
    
	//부대트리
	var setsetSystemUserDeptTreePropProp = {
		context: '${context}',						
		id : 'setSystemUserDeptTreeProp',
		url : '$!{context}/itg/system/user/searchDeptForTree.do',
		title : '#springMessage("res.00034")'+'목록',
		searchable : true,
		expandAllBtn : false,
		collapseAllBtn : false,
		params: null,
		rnode_text : '#springMessage("res.00003")',
		expandLevel: 3
	}
	var tree = createTreeComponent(setsetSystemUserDeptTreePropProp);
	
	//사용자 리스트 그리드
	var setSystemUserGridProp = {	
		context: '${context}',						
		id: 'setSystemUserGridProp',	
		title : '#springMessage("res.00012")',
		gridHeight: 500,
		resource_prefix: 'grid.system.user.dormant',				
		url: '${context}/itg/system/user/searchUserList.do',	
		params: {isDormant : "Y"},								
		pagingBar : true,
		pageSize : 25,
		autoLoad: true,
		selModel: true
	}
	var grid = createGridComp(setSystemUserGridProp);
	
	//검색 폼
	var setSearchFormProp = {
		id: 'searchSystemUser',
		title: '#springMessage("res.common.search")',
		target: 'setSystemUserGridProp',
		formBtns: [userSearchBtn, userSearchInitBtn],
		columnSize: 2,
		setTarget: 'setSystemUserDeptTreeProp',
		tableProps : [
		              {
						colspan:1, tdHeight: 30, item: createCodeComboBoxComp({label:'#springMessage("res.label.system.00012")', name:'search_type', code_grp_id:'SEARCH_USER_SYSTEM', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})
		              },{
						colspan:1, tdHeight: 30, item: createTextFieldComp({name:'search_value', width: 300, padding: getConstValue('WITH_FIELD_PADDING')})
		              },{
		            	colspan:1, tdHeight: 30, item: createTextFieldComp({label:'휴면기간', name:'dormant_dt_cnt', width: 250, padding: getConstValue('WITH_FIELD_PADDING'), vtype:'number'})
		              },{
		            	colspan:1, tdHeight: 30, item: createDisplayTextFieldComp({name:'display_string', value: '&nbsp;&nbsp;일 이상', width: 50})
		              },{
		            	colspan:2, tdHeight: 30, item : createCodeComboBoxComp({label:'#springMessage("res.00008")', name:'use_yn', code_grp_id: 'YN', width: 250, padding: getConstValue('WITH_FIELD_PADDING'), attachAll:true})
		              }
					]
	}
	var searchUserFormPanel = createSeachFormComp(setSearchFormProp);
	
	// Viewport 설정
	var viewportProperty = {
		west: { 
			minWidth: 210, 
			maxWidth: 350, 
			items : [tree] 
		},
		center: { 
			items : [searchUserFormPanel, grid], 
			buttons: [dormantFreeBtn, setUseNBtn]  
		}
	}
	
	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	
	attachCustomEvent('itemclick', tree, treeNodeClick);
	
	
});

// Layout Resise Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["searchSystemUser"], "setSystemUserGridProp");
	//syncPanelHeight("setEditorFormProp");	// 하위패널 height Sync
}

/**
 * 검색 폼에서 text에서 엔터키 칠때 호출 
 */
function searchBtnEvent(){
	actionLink('search');
}


function treeNodeClick( tree, record, index, eOpts ) {
	var cust_id, dept_nm;
	var searchForm = Ext.getCmp('searchSystemUser');
	//var paramMap = searchForm.getInputData();
	var paramMap = {};
	
	
	var grid_store = getGrid("setSystemUserGridProp").getStore();
	if( getConstValue("CENTER_CUST_ID") == record.raw.up_node_id ){
		cust_id = record.raw.up_node_id;
		dept_nm = record.raw.text;
	}else{
		cust_id = record.raw.node_id;
		dept_nm = "";
	}
	paramMap["cust_id"] = cust_id;
	paramMap["dept_nm"] = dept_nm;
	paramMap["isDormant"] = "Y";
	
	grid_store.proxy.jsonData = paramMap;
	//grid_store.reload();
	grid_store.loadPage(1);
	
	searchForm.getFormField("search_value").setValue("");
	
}


function actionLink(command) {

	switch(command) {
	
		case 'dormantFree' :
			
			var grid = Ext.getCmp('setSystemUserGridProp');
			var selectedItems = grid.view.selModel.getSelection();
			
			if(selectedItems.length < 1) {
				alert("휴면상태를 해제할 계정을 선택해주시기 바랍니다.");
				return false;
			}
			
			if(!confirm('선택하신 계정의 휴면상태를 해제하시겠습니까?')){
				return false;
			}
			
			var userIdList = new Array();
			
			for(var i=0; i<selectedItems.length; i++) {
				userIdList.push(selectedItems[i].raw.USER_ID);
			}
			
			var paramMap = {};
			paramMap['user_id_list'] = userIdList;
			
			jq.ajax({ 
				type:"POST", 
				url:'$!{context}/itg/system/dormant/updateDormantFree.do', 
				contentType: "application/json", 
				dataType: "json", 
				async: false, 
				data : getArrayToJson(paramMap), 
				success:function(data){
					alert("휴면상태가 해제되었습니다.");
					
					setViewPortMaskTrue();
					
					var searchForm = Ext.getCmp('searchSystemUser');
					var subParam = searchForm.getInputData();
					var custId =Ext.getCmp('setSystemUserDeptTreeProp').view.selModel.getSelection();
					
					subParam["isDormant"] = "Y";
					
					if(custId.length > 0){				
						var treeRecord = custId[0];
						if( getConstValue("CENTER_CUST_ID") == treeRecord.raw.up_node_id ){
							cust_id = treeRecord.raw.up_node_id;
							dept_nm = treeRecord.raw.text;
						}else{
							cust_id = treeRecord.raw.node_id;
							dept_nm = "";
						}
						subParam["cust_id"] = cust_id;
						subParam["dept_nm"] = dept_nm;
					}
					grid.searchList(subParam);	
					
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
				}
			})
			
			
		break;
		
		case 'setUseN' :
			
			var grid = Ext.getCmp('setSystemUserGridProp');
			var selectedItems = grid.view.selModel.getSelection();
			
			if(selectedItems.length < 1) {
				alert("사용불가로 적용하실 계정을 선택해주시기 바랍니다.");
				return false;
			}
			
			if(!confirm('선택하신 계정을 사용불가로 적용하시겠습니까?')){
				return false;
			}
			
			var userIdList = new Array();
			
			for(var i=0; i<selectedItems.length; i++) {
				userIdList.push(selectedItems[i].raw.USER_ID);
			}
			
			var paramMap = {};
			paramMap['user_id_list'] = userIdList;
			
			jq.ajax({ 
				type:"POST", 
				url:'$!{context}/itg/system/dormant/updateSetUseN.do', 
				contentType: "application/json", 
				dataType: "json", 
				async: false, 
				data : getArrayToJson(paramMap), 
				success:function(data){
					alert("사용불가로 적용되었습니다.");
					
					setViewPortMaskTrue();
					
					var searchForm = Ext.getCmp('searchSystemUser');
					var subParam = searchForm.getInputData();
					var custId =Ext.getCmp('setSystemUserDeptTreeProp').view.selModel.getSelection();
					
					subParam["isDormant"] = "Y";
					
					if(custId.length > 0){				
						var treeRecord = custId[0];
						if( getConstValue("CENTER_CUST_ID") == treeRecord.raw.up_node_id ){
							cust_id = treeRecord.raw.up_node_id;
							dept_nm = treeRecord.raw.text;
						}else{
							cust_id = treeRecord.raw.node_id;
							dept_nm = "";
						}
						subParam["cust_id"] = cust_id;
						subParam["dept_nm"] = dept_nm;
					}
					grid.searchList(subParam);	
					
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
				}
			})
			
		break;
		
		case 'search' :
			
			var searchForm = Ext.getCmp('searchSystemUser');
			var paramMap = searchForm.getInputData();
			var custId =Ext.getCmp('setSystemUserDeptTreeProp').view.selModel.getSelection();
			
			if(searchForm.isValid()){
				
				// 마스크를 띄웁니다.
				setViewPortMaskTrue();
			
				paramMap["isDormant"] = "Y";
				
				if(custId.length > 0){				
					var treeRecord = custId[0];
					if( getConstValue("CENTER_CUST_ID") == treeRecord.raw.up_node_id ){
						cust_id = treeRecord.raw.up_node_id;
						dept_nm = treeRecord.raw.text;
					}else{
						cust_id = treeRecord.raw.node_id;
						dept_nm = "";
					}
					paramMap["cust_id"] = cust_id;
					paramMap["dept_nm"] = dept_nm;
				}
				if(paramMap['search_type']=="TEL_NO") {
					paramMap['search_value'] = paramMap['search_value'].split("-").join("");
				}
				Ext.getCmp('setSystemUserGridProp').searchList(paramMap);	
				
				// 마스크를 숨깁니다.
				setViewPortMaskFalse();
				
			} else {
				
				alert("휴면기간은 숫자만 입력 가능합니다.");
				return false;
			}
		break;
		case 'searchInit' :
			var searchForm = Ext.getCmp('searchSystemUser');
			searchForm.initData();
			searchForm.getFormField('display_string').setValue("&nbsp;&nbsp;일 이상");
		break;
	}
}

</script>
</head>
<body>
<form name="theForm" method="post">
<div id="loading-mask"></div>
<div id="loading">
  <div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>
</form>
</body>
</html>