<!doctype html>
<html lang="ko">
<!--
*//**
 * 사용자그룹관리 페이지
 * @author <a href="mailto:minsoo@nkia.co.kr"> Minsoo Jeon
 * 
 *//*
-->
<head>
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>

<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<script type="text/javascript" src="/itg/base/js/system/userGrpPop.js?_ct=$!{cachedate}"></script>
<script>

Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	//기본정보 컨트롤 버튼
	var userGrpInsertModeBtn = createBtnComp({visible: true, label: "#springMessage('btn.system.00001')", id:'userGrpInsertModeBtn'});
	var userGrpInsertBtn = createBtnComp({visible: false, label: "#springMessage('btn.common.insert')", id:'userGrpInsertBtn', ui:'correct', scale:'medium'});
	var userGrpUpdateBtn = createBtnComp({visible: false, label: "#springMessage('btn.common.modify')", id:'userGrpUpdateBtn', ui:'correct', scale:'medium'});
	var userGrpDeleteBtn = createBtnComp({visible: false, label: "#springMessage('btn.common.delete')", id:'userGrpDeleteBtn', scale:'medium'});
	var userGrpInsertBackBtn = createBtnComp({visible: false, label: "#springMessage('btn.common.cancel')", id:'userGrpInsertBackBtn', scale:'medium'});
	
	//권한맵핑그리드 컨트롤 버튼
	var proAuthAddBtn = createBtnComp({visible: true, label: "#springMessage('btn.common.append')", id:'proAuthAddBtn'});
	var proAuthDelBtn = createBtnComp({visible: true, label: "#springMessage('btn.common.remove')", id:'proAuthDelBtn'});
	var sysAuthAddBtn = createBtnComp({visible: true, label: "#springMessage('btn.common.append')", id:'sysAuthAddBtn'});
	var sysAuthDelBtn = createBtnComp({visible: true, label: "#springMessage('btn.common.remove')", id:'sysAuthDelBtn'});
	
	var updateBtn = createBtnComp({visible: true, label: "#springMessage('btn.common.modify')", id:'updateBtn', ui:'correct', scale:'medium'});
	
	//기본정보 컨트롤 버튼 액션
	attachBtnEvent(userGrpInsertModeBtn, actionLink,  'modeChange');
	attachBtnEvent(userGrpInsertBtn, actionLink,  'insert');
	attachBtnEvent(userGrpUpdateBtn, actionLink,  'update');
	attachBtnEvent(userGrpDeleteBtn, actionLink,  'delete');
	attachBtnEvent(userGrpInsertBackBtn, actionLink,  'back');
	
	//맵핑정보 컨트롤 버튼 액션
	attachBtnEvent(proAuthAddBtn, actionLink,  'proAuthAddBtn');
	attachBtnEvent(proAuthDelBtn, actionLink,  'proAuthDelBtn');
	attachBtnEvent(sysAuthAddBtn, actionLink,  'sysAuthAddBtn');
	attachBtnEvent(sysAuthDelBtn, actionLink,  'sysAuthDelBtn');
	
	attachBtnEvent(updateBtn, actionLink,  'updateBtn');
	
	//사용자그룹트리
	var setStaticUserGrpTreeProp = {
		context: '${context}',
		id : 'setStaticUserGrpTreeProp',
		url : '$!{context}/itg/system/userGrp/searchStaticAllUserGrpTree.do',
		title : '#springMessage("res.title.system.usergrp.tree")',
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : false,
		collapseAllBtn : false,
		params: null,
		updateUrl: '$!{context}/itg/system/userGrp/updateUserGrpTree.do',
		dragPlug : true,
		rnode_text : '#springMessage("res.00003")',
		expandLevel: 4
	}
	var staticUserGrpTree = createTreeComponent(setStaticUserGrpTreeProp);
	
	// ==================================== 권한설정 탭 Layout 구성 Start ==================================================
	//시스템권한그리드
	var setSystemAuthGridProp = {
		id: 'setSystemAuthGridProp',
		title : "#springMessage('res.title.system.authList')",
		gridHeight: '100%',
		resource_prefix: 'grid.system.usergrp.systemauth',
		url: '${context}/itg/system/userGrp/searchSystemAuthList.do',
		params: null,		
		selModel: true,
		pagingBar : false,
		autoLoad: true,
		dragPlug: false,
		border: true,
		actionIcon: '/itg/base/images/ext-js/common/icons/menu_left02.gif',
		actionBtnEvent: actionBtnEvent,
		emptyMsg: "#springMessage('msg.system.result.00012')",
		toolBarComp: [sysAuthAddBtn, sysAuthDelBtn]
	}
	var systemAuthGrid = createGridComp(setSystemAuthGridProp);
	
	//프로세스권한그리드
	var setProcessAuthGridProp = {	
		id: 'setProcessAuthGridProp',	
		title : "#springMessage('res.title.system.processList')",
		gridHeight: '100%',
		resource_prefix: 'grid.system.usergrp.processauth',				
		url: '${context}/itg/system/userGrp/searchProcessAuthList.do',	
		params: null,		
		selModel: true,
		pagingBar : false,
		autoLoad: true,
		dragPlug: false,
		border: true,
		emptyMsg: "#springMessage('msg.system.result.00011')",
		toolBarComp: [proAuthAddBtn, proAuthDelBtn]
	}
	var processAuthGrid = createGridComp(setProcessAuthGridProp);
	
	//사용자그룹정보 폼
	var setEditUserGrpProp = {
		id: 'setEditUserGrpProp',
		title: "#springMessage('res.title.system.usergrp')",
		columnSize: 2,
		editOnly: true,
		border: true,
		buttonAlign: 'center',
		formBtns: [userGrpInsertModeBtn],
		alignBottomBtns: [userGrpUpdateBtn, userGrpDeleteBtn],
		tableProps :[
					 {colspan:1, item : createTextFieldComp({label:'#springMessage("res.label.system.00017")', readOnly:true, name:'up_user_grp_nm'})}
					,{colspan:1, item : createUnionCheckTextContainer({
							label:'#springMessage("res.label.system.00006")',
							notNull: true,
							name:'user_grp_id',
							btnId: 'userGrpCheckBtn',
							btnLabel:'#springMessage("btn.common.validation")',
							targetForm:'setEditUserGrpProp',
							checkUrl:'/itg/system/userGrp/selectUserGrpIdComp.do',
							disabled: true,
							successMsg: '#springMessage("msg.system.result.00035")',
							failMsg: '#springMessage("msg.system.result.00034")',
							maxLength : FIELD_DOMAIN.NM_50
						})
					  }
					,{colspan:1, item : createTextFieldComp({label:'#springMessage("res.label.system.00018")', name:'user_grp_nm', notNull:true, maxLength : FIELD_DOMAIN.NM_100})}
					,{colspan:1, item : createCodeRadioComp({label:'#springMessage("res.00008")', name:'use_yn', code_grp_id: 'YN', columns:2})}				        
					,{colspan:1, item : createTextFieldComp({name:'overLapYNuser_grp_id', id:'overLapYNuser_grp_id', value: 'N', notNull:false})}
					],
		hiddenFields:[{name:'up_user_grp_id'}]
	}
	var userGrpEditForm = createEditorFormComp(setEditUserGrpProp);
	
	var userGrpEditorHboxProp = {
			id: 'userGrpMappingGridForm',
			title: "#springMessage('res.title.system.usergrp.hbox.auth')",
	        panelItems: [{
		    	flex: 1,
		    	items: systemAuthGrid
			},{
			    flex: 1,
			    items: processAuthGrid
			    }
			],
			panelTools: []
	}
	var userGrpEditorHboxPanel = createHorizonPanel(userGrpEditorHboxProp);
	// ==================================== 권한설정 탭 Layout 구성 End ==================================================
	
	// ==================================== 사용자목록 탭 Layout 구성 Start ==================================================
	// 사용자 리스트 그리드
	var setSystemUserGridProp = {
		context: '${context}',
		id: 'setSystemUserGridProp',
		title : "#springMessage('res.00012')",
		gridHeight: '100%',
		resource_prefix: 'grid.system.user',
		url: '${context}/itg/system/userGrp/searchUserList.do',
		params: null,
		pagingBar : true,
		pageSize : 25,
		autoLoad: true,
		toolBarComp: []
	}
	var userListGrid = createGridComp(setSystemUserGridProp);
	
	var userListHboxProp = {
			id: 'userListGridForm',
			title: "#springMessage('res.00012')",
	        panelItems: [{
		    	flex: 1,
		    	items: userListGrid
			}],
			panelTools: []
	}
	var userListHboxPanel = createHorizonPanel(userListHboxProp);
	// ==================================== 사용자목록 탭 Layout 구성 End ==================================================
	
	var setTabComponent = {
		tab_id: 'mainTab',
		cls: 'dualTab',
		autoScroll: false,
		item: [userGrpEditorHboxPanel, userListHboxPanel]
	};
	
	var tabComponent = createTabComponent(setTabComponent);
	
	// Viewport 설정
	var viewportProperty = {
		west: { 
			minWidth: 210, 
			maxWidth: 350, 
			items : [staticUserGrpTree] 
		},
		center: { 
			items : [userGrpEditForm, tabComponent],
			buttons: [updateBtn, userGrpInsertBtn, userGrpInsertBackBtn] 
		}
	}
	
	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	
	// 권한설정 및 장비관리 탭이 변경될 때 높이값이 맞지 않는 부분으로 인해 추가
	attachCustomEvent('tabchange', tabComponent, mainTabChange);
	attachCustomEvent('itemclick', staticUserGrpTree, treeNodeClick);
	
	Ext.getCmp('overLapYNuser_grp_id').hide();
	
	formEditable({id : 'setEditUserGrpProp', editorFlag : false, excepArray : ['userGrpInsertModeBtn'] });
	
	//2019.03.08 molee 사용자그룹ID검증 추가
	attachCustomEvent('change', Ext.getCmp("setEditUserGrpProp").getFormField('user_grp_id'), function(){
		var editForm = Ext.getCmp('setEditUserGrpProp');
		editForm.setFieldValue('overLapYNuser_grp_id', '');
	});
});

//Layout Resise Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["setEditUserGrpProp"], "mainTab");
	var selecTab = Ext.getCmp('mainTab').activeTab.id;
	if(selecTab == 'userGrpMappingGridForm'){
		syncPanelHeight("userGrpMappingGridForm");	// 하위패널 height Sync
		
	}else if(selecTab == 'userListGridForm'){
		syncPanelHeight("userListGridForm");
		
	}
}

//Layout Resise Event Handler
function mainTabChange( tabComp, activeTab, prevTab, eObj ){
	if( activeTab.id == "userListGridForm" ) {
		hideBtns(["updateBtn"]);
		
	} else if( activeTab.id == "userGrpMappingGridForm" ) {
		showBtns(["updateBtn"]);
		
	} else {
		syncPanelHeight(activeTab.id);
	}
	callResizeLayout();
}

function gridDataLoad( user_grp_id ){
	var param = {user_grp_id: user_grp_id};
	
	var systemGrid = getGrid("setSystemAuthGridProp").getStore();
	systemGrid.proxy.jsonData=param;
	systemGrid.load();
	
	var processGrid = getGrid("setProcessAuthGridProp").getStore();
	processGrid.proxy.jsonData=param;
	processGrid.load();
	
	// 사용자 목록
	var setSystemUserGridProp = getGrid('setSystemUserGridProp').getStore();
	setSystemUserGridProp.proxy.jsonData=param;
	setSystemUserGridProp.load();
}


function treeNodeClick( tree, record, index, eOpts ) {
	var param = {};
	var up_user_grp_id = record.raw.up_node_id;
	var user_grp_id = record.raw.node_id;
	param['user_grp_id'] = user_grp_id;
	
	jq.ajax({ type:"POST"
		, url:'/itg/system/userGrp/searchUserGrpBasicInfo.do'
		, contentType: "application/json"
		, dataType: "json"
		, async: false
		, data : getArrayToJson(param)
		, success:function(data){
			if( data.success ){
				var clickRecord = data.resultMap;
				
				var dataForm = Ext.getCmp('setEditUserGrpProp');
				dataForm.setDataForMap(clickRecord);

				Ext.getCmp('userGrpCheckBtn').disable();
				Ext.getCmp('userGrpCheckBtn').setDisabled(true);
				formEditable({id : 'setEditUserGrpProp', editorFlag : true, excepArray : ['up_user_grp_nm', 'user_grp_id'] });
				
				//@@ 20131125 고한조 사용여부가 N일 경우 하위항목 추가 못하도록 수정 Start
				if( clickRecord["USE_YN"] == "Y" ){
					hideBtns(['userGrpInsertBtn','userGrpInsertBackBtn']);
					showBtns(['userGrpInsertModeBtn', 'userGrpUpdateBtn']);
				} else {
					hideBtns(['userGrpInsertModeBtn', 'userGrpInsertBtn','userGrpInsertBackBtn']);
					showBtns(['userGrpUpdateBtn']);
				}
				
				//권한설정 탭인경우 수정버튼활성화
				var selecTab = Ext.getCmp('mainTab').activeTab.id;
				if(selecTab == 'userGrpMappingGridForm') {
					showBtns(['updateBtn']);
				}
				
				if(user_grp_id == 0){
					hideBtns(['userGrpDeleteBtn']);
				}else{
					showBtns(['userGrpDeleteBtn']);
				}
				
				gridDataLoad(user_grp_id);
				
				callResizeLayout();
			}else{
				showMessage(data.resultMsg);
			}
		}
	});
}

function actionBtnEvent(grid, rowIndex, colIndex){
	var auth_id = getGrid("setSystemAuthGridProp").getStore().data.items[rowIndex].data.AUTH_ID;
	var setPopUpProp = {
		context: '${context}',
        id: 'menuViewPop',
        popUpText: '#springMessage("res.title.system.usergrpauthmap")',
        treeId: 'menuViewPopTree',
        height: 440,
        width: 300,
        url: '/itg/system/userGrp/searchMappingMenu.do',
        auth_id: auth_id,
        bodyStyle: 'background-color: white; ',
        border: false
    }
	var popUp = createMenuViewPop(setPopUpProp);
	popUp.show();
}

function gridInit(){
	var processGrid = getGrid("setProcessAuthGridProp").getStore();
	processGrid.proxy.jsonData={};
	processGrid.load();
	
	var systemGrid = getGrid("setSystemAuthGridProp").getStore();
	systemGrid.proxy.jsonData={};
	systemGrid.load();
	
	// 사용자 목록
	var setSystemUserGridProp = getGrid('setSystemUserGridProp').getStore();
	setSystemUserGridProp.proxy.jsonData={};
	setSystemUserGridProp.load();
}

function treeInit(){
	var tree = Ext.getCmp('setStaticUserGrpTreeProp');
	var path = tree.getSelectionModel().getSelection()[0].getPath();
	var tree_store = tree.getStore();
    tree_store.load();
    tree.expandPath(path);
}

function pageInit(){
	var hideIds = ['userGrpDeleteBtn', 'userGrpUpdateBtn', 'updateBtn', 'userGrpInsertBtn', 'userGrpInsertBackBtn'];	
	hideBtns(hideIds);	//버튼을 숨긴다.
	var showIds = ['userGrpInsertModeBtn'];	
	showBtns(showIds);
	
	formEditable({id : 'setEditUserGrpProp', editorFlag : false, excepArray : ['userGrpInsertModeBtn'] });
	Ext.getCmp('setEditUserGrpProp').initData();
	Ext.getCmp('setEditUserGrpProp').setFieldValue('user_class', '0');
}

function actionLink(command) {
	var theForm = Ext.getCmp('setEditUserGrpProp');

	switch(command) {
		case 'modeChange' :
				var seletedObject = Ext.getCmp('setStaticUserGrpTreeProp').view.selModel.getSelection();
				
				if(seletedObject == "" ){
					showMessage("#springMessage('msg.common.00005')");
					return;
				} 
			
				//폼초기화 
				theForm.initData();
				//필요한 데이터값 setValue()
				var user_grp_id = seletedObject[0].raw.node_id;
				var up_user_grp_nm = seletedObject[0].raw.text;
				
				theForm.setHiddenValue('up_user_grp_id', user_grp_id);
				theForm.setFieldValue('up_user_grp_nm', up_user_grp_nm);
				
				formEditable({id : 'setEditUserGrpProp', editorFlag : true, excepArray : ['up_user_grp_nm'] });
				Ext.getCmp('userGrpCheckBtn').setDisabled(false);
				
				//신규등록모드로 버튼 전환
				var hideIds = ['userGrpInsertModeBtn', 'userGrpDeleteBtn', 'userGrpUpdateBtn', 'updateBtn'];	
				hideBtns(hideIds);	//버튼을 숨긴다.
				var showIds = ['userGrpInsertBtn','userGrpInsertBackBtn'];	
				showBtns(showIds);
				
		break;
		case 'insert' :
			if(theForm.isValid()){
				var editForm = Ext.getCmp('setEditUserGrpProp');
				var idCheckYN = editForm.getFieldValue('overLapYNuser_grp_id');
				var user_grp_id = editForm.getFieldValue('user_grp_id');
				var user_grp_nm = editForm.getFieldValue('user_grp_nm');
				
				if(user_grp_id == "" || user_grp_id == null){
					showMessage('#springMessage("msg.system.result.00018")');
					return;
				}	
				if(user_grp_nm == "" || user_grp_nm == null){
					showMessage('#springMessage("msg.system.result.00019")');
					return;
				}
				if(idCheckYN == "N" || idCheckYN == ""){
					showMessage('#springMessage("msg.system.result.00022")');
					return;
				}
				
				// 마스크를 띄웁니다.
				setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00001")')){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return false;
				}
				
				var paramMap = {};
				
				//맵핑권한 등록
				var mappingMap = {};
				//1.등록할 사용자그룹ID 등록
				var user_grp_id = editForm.getFieldValue('user_grp_id');

				mappingMap['user_grp_id'] = user_grp_id;
				//2.그리드값 가져오기
				var proGrid = getGrid("setProcessAuthGridProp").getStore();
				var pro_auth_id = [];
				for(var i=0; i<proGrid.data.items.length;i++){
					pro_auth_id.push(proGrid.data.items[i].data.PROC_AUTH_ID);
				}
				mappingMap['pro_auth_id'] = pro_auth_id;
				var authGrid = getGrid("setSystemAuthGridProp").getStore();
				var auth_id = [];
				for(var i=0; i<authGrid.data.items.length;i++){
					auth_id.push(authGrid.data.items[i].data.AUTH_ID);
				}
				mappingMap['auth_id'] = auth_id;
				
				var infoMap = theForm.getInputData();
				
				paramMap['infoMap'] = infoMap;
	            paramMap['mappingMap'] = mappingMap;
	            
	            callUserGrpAjaxCUD('insert', '/itg/system/userGrp/insertUserGrpInfo.do', theForm, editForm, paramMap);
				
			}else{
				showMessage('#springMessage("msg.common.00011")');
				return false;
			}
				
		break;
		case 'update' :
			if(theForm.isValid()){
				var tree = Ext.getCmp('setStaticUserGrpTreeProp');
				var editForm = Ext.getCmp('setEditUserGrpProp');
				
				editForm.setFieldValue('overLapYNuser_grp_id', 'Y');
				var user_grp_id = editForm.getFieldValue('user_grp_id');
				var user_grp_nm = editForm.getFieldValue('user_grp_nm');
				
				if(user_grp_id == "" || user_grp_id == null){
					showMessage('#springMessage("msg.system.result.00018")');
					return;
				}else if(user_grp_nm == "" || user_grp_nm == null){
					showMessage('#springMessage("msg.system.result.00019")');
					return;
				}
				var seletedObject = tree.view.selModel.getSelection();
					
				if(seletedObject == "" ){
					showMessage("#springMessage('msg.common.00005')");
					return;
				} 
				
				// 마스크를 띄웁니다.
				setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00002")')){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return false;
				}
					
				var infoMap = theForm.getInputData();
				
				var paramMap = {};
				
				paramMap['infoMap'] = infoMap;
				paramMap['mappingMap'] = {};
		        
		        jq.ajax({ type:"POST"
					, url: '$!{context}/itg/system/userGrp/selectCheckUserGrp.do'
					, contentType: "application/json"
					, dataType: "json" 
					, async: false
					, data : getArrayToJson(infoMap)
					, success:function(data){
						if(data.resultString > 0 && infoMap.use_yn == "N"){ //그룹에 사용자가 존재하고 사용유무를 N으로 변경할시 수정 불가 
							showMessage('#springMessage("msg.system.result.00036")');
							
						}else{
							callUserGrpAjaxCUD('update', '/itg/system/userGrp/updateUserGrpInfo.do', theForm, editForm, paramMap);
						}
						// 마스크를 숨깁니다.
						setViewPortMaskFalse();
					}
		        });
			}else{
				showMessage('#springMessage("msg.common.00011")');
				return false;
			}
		break;
		case 'delete' :
			var seletedObject = Ext.getCmp('setStaticUserGrpTreeProp').view.selModel.getSelection();
			
			if (seletedObject == "" ) {
				showMessage("#springMessage('msg.common.00005')");
				return false;
			}
			
			//2019.03.08 molee 사용자가 있는 그룹인 경우 삭제할 수 없다는 메시지 출력
			var systemUserObject = getGrid('setSystemUserGridProp').getStore();
			if (systemUserObject.data.items.length > 0){
				showMessage("#springMessage('msg.user.grp.00001')");
				return false;
			}
			
			
			// 마스크를 띄웁니다.
			setViewPortMaskTrue();
			
			if (!confirm('#springMessage("msg.common.confirm.00003")')) {
				// 마스크를 숨깁니다.
				setViewPortMaskFalse();
				return false;
			}
			paramMap = {};
			paramMap['user_grp_id'] = theForm.getFieldValue('user_grp_id');
			
			callUserGrpAjaxCUD('delete', '/itg/system/userGrp/deleteUserGrpInfo.do', theForm, editForm, paramMap);
		break;
		case 'back' :
			theForm.initData();
			var up_user_grp_id = Ext.getCmp('setEditUserGrpProp').getHiddenValue('up_user_grp_id');

			Ext.getCmp('setStaticUserGrpTreeProp').view.selModel.deselectAll();//selectTree초기화
			formEditable({id : 'setEditUserGrpProp', editorFlag : false, excepArray : [] });
			pageInit();
			Ext.getCmp('userGrpCheckBtn').disable();
			
		break;
		case 'proAuthAddBtn' :
			//이미 포함된 권한은 제외하고 팝업에 뿌림 
			var proGrid = getGrid("setProcessAuthGridProp").getStore();
			//@@ 20130923 고은규 기존 변수명 pro_auth_id 를 proc_auth_id 로 변경함.
			var proc_auth_id = [];
			for (var i=0; i<proGrid.data.items.length;i++) {
				proc_auth_id.push(proGrid.data.items[i].data.PROC_AUTH_ID);
			}

			var setPopUpProp = {
				context: '${context}',
            	id: 'proAuthAddPop',
            	popUpText: "#springMessage('res.title.system.processList')",
            	gridId: 'proAuthAddPopGrid',
            	height: 440,								
            	width: 500,
            	url: '/itg/system/userGrp/searchProcessAuthListPop.do',	
            	resource_prefix: 'grid.system.usergrp.processauth',
            	buttonAlign : 'center',
            	//bodyPadding: '5 5 5 5 ',
            	proc_auth_id_map: proc_auth_id,
            	bodyStyle: 'background-color: white; ',
            	border: false,
            	emptyMsg: "#springMessage('msg.system.result.00010')"
        	}
			
			var popUp = createProAuthAddPop(setPopUpProp);
			popUp.show();
			
		break;
		case 'proAuthDelBtn' :
			var grid_store = Ext.getCmp('setProcessAuthGridProp').getStore();
			var selectRow = Ext.getCmp('setProcessAuthGridProp').view.selModel.getSelection();
			
			//2019.03.08 molee 선택된 권한이 없는 경우의 메시지 출력
			if (selectRow.length > 0) {
				for (var i=0; i<selectRow.length; i++) {
					var selectIndex = grid_store.indexOf(selectRow[i]);
					grid_store.removeAt(selectIndex); 
				}
			} else {
				showMessage("#springMessage('msg.user.grp.00002')");
				return false;
			}
		break;
		case 'sysAuthAddBtn' :
			//이미 포함된 권한은 제외하고 팝업에 뿌림 
			var authGrid = getGrid("setSystemAuthGridProp").getStore();
			var auth_id = [];
			
			for(var i=0; i<authGrid.data.items.length;i++){
				auth_id.push(authGrid.data.items[i].data.AUTH_ID);
			}

			var setPopUpProp = {
				context: '${context}',
            	id: 'AuthAddPop',
            	popUpText: "#springMessage('res.title.system.authList')",
            	gridId: 'AuthAddPopGrid',
            	height: 440,								
            	width: 450,
            	url: '/itg/system/userGrp/searchSystemAuthListPop.do',	
            	resource_prefix: 'grid.system.usergrp.systemauthpop',
            	buttonAlign : 'center',
            	//bodyPadding: '5 5 5 5 ',
            	auth_id_map: auth_id,
            	bodyStyle: 'background-color: white; ',
            	border: false,
            	emptyMsg: "#springMessage('msg.system.result.00021')"
        	}
			
			var popUp = createAuthAddPop(setPopUpProp);
			popUp.show();
			
		break;
		case 'sysAuthDelBtn' :
			var grid_store = Ext.getCmp('setSystemAuthGridProp').getStore();
			var selectRow = Ext.getCmp('setSystemAuthGridProp').view.selModel.getSelection();
			
			//2019.03.08 molee 선택된 권한이 없는 경우의 메시지 출력
			if (selectRow.length > 0) {
				for(var i=0; i<selectRow.length;i++){
					var selectIndex = grid_store.indexOf(selectRow[i]);
					grid_store.removeAt(selectIndex); 
				}
			} else {
				showMessage("#springMessage('msg.user.grp.00002')");
				return false;
			}
			
		break;
		case "updateBtn":
			if(theForm.isValid()){
				var tree = Ext.getCmp('setStaticUserGrpTreeProp');
				var editForm = Ext.getCmp('setEditUserGrpProp');
				
				editForm.setFieldValue('overLapYNuser_grp_id', 'Y');
				var user_grp_id = editForm.getFieldValue('user_grp_id');
				var user_grp_nm = editForm.getFieldValue('user_grp_nm');
				
				if(user_grp_id == "" || user_grp_id == null){
					showMessage('#springMessage("msg.system.result.00018")');
					return;
					
				}else if(user_grp_nm == "" || user_grp_nm == null){
					showMessage('#springMessage("msg.system.result.00019")');
					return;
					
				}
				
				var seletedObject = tree.view.selModel.getSelection();
					
				if(seletedObject == "" ){
					showMessage("#springMessage('msg.common.00005')");
					return;
				} 
				
				// 마스크를 띄웁니다.
				//setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00002")')){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return false;
				}
					
				//맵핑권한 수정하기
				var mappingMap = {};
				var mainTab = Ext.getCmp("mainTab");
				var activeId = mainTab.getActiveTab().id;
				if( activeId == "userGrpMappingGridForm" ){
					var user_grp_id = editForm.getFieldValue('user_grp_id');
					mappingMap['user_grp_id'] = user_grp_id;
						
					var proGrid = getGrid("setProcessAuthGridProp").getStore();
					var pro_auth_id = [];
					for(var i=0; i<proGrid.data.items.length; i++){
						pro_auth_id.push(proGrid.data.items[i].data.PROC_AUTH_ID);
					}
					mappingMap['pro_auth_id'] = pro_auth_id;
					var authGrid = getGrid("setSystemAuthGridProp").getStore();
					var auth_id = [];
					for(var i=0; i<authGrid.data.items.length; i++){
						auth_id.push(authGrid.data.items[i].data.AUTH_ID);
					}
					mappingMap['auth_id'] = auth_id;
				}
				
				var infoMap = theForm.getInputData();
				var paramMap = {};
					
				paramMap['infoMap'] = infoMap;
		        paramMap['mappingMap'] = mappingMap;
		        paramMap['activeId'] = activeId;
		        
		        jq.ajax({ type:"POST"
					, url: '$!{context}/itg/system/userGrp/selectCheckUserGrp.do'
					, contentType: "application/json"
					, dataType: "json" 
					, async: false
					, data : getArrayToJson(infoMap)
					, success:function(data){
						if(data.resultString > 0 && infoMap.use_yn == "N"){ //부서에 사용자가 존재하고 사용유무를 N으로 변경할시 수정 불가 
							showMessage('#springMessage("msg.system.result.00036")');
						}else{
							callUserGrpAjaxCUD('updateUserGrp', '/itg/system/userGrp/updateUserGrpInfo.do', theForm, editForm, paramMap);
						}
						// 마스크를 숨깁니다.
						setViewPortMaskFalse();
					}
		        });
			}else{
				showMessage('#springMessage("msg.common.00011")');
				return false;
			}
		break;
	}
}


function callUserGrpAjaxCUD(type, ajaxUrl, theForm, editForm, paramMap) {
	// 최양규 추가 : actionLink에서 공통으로 사용하는 ajax
	jq.ajax({ type:"POST",
			   url: ajaxUrl,
			 async: false,
	   contentType: "application/json",
	      dataType: "json",
	         data : getArrayToJson(paramMap),
		   success:function(data){

		   	if( data.success ){
		   		
				if(type == 'insert') {
					theForm.initData();
			        treeInit();
			        var tree = Ext.getCmp('setStaticUserGrpTreeProp');
			        tree.view.selModel.deselectAll();  //selectTree초기화
					Ext.getCmp('userGrpCheckBtn').disable();
					formEditable({id : 'setEditUserGrpProp', editorFlag : false, excepArray : [] });
					showMessage(data.resultMsg);
					
				} else if (type == 'update'){
					selectAfterTree("setStaticUserGrpTreeProp");
					showMessage("#springMessage('msg.common.result.00001')");
					// grid select clean
					gridDataLoad( editForm.getFieldValue('user_grp_id') );
					// grid select clean End
					editForm.setFieldValue('overLapYNuser_grp_id', 'N');
					formEditable({id : 'setEditUserGrpProp', editorFlag : false, excepArray : [] });

				} else if (type == 'delete'){
					showMessage(data.resultMsg);
					theForm.initData();
			        treeInit();
			        Ext.getCmp('setStaticUserGrpTreeProp').view.selModel.deselectAll();//selectTree초기화
			        formEditable({id : 'setEditUserGrpProp', editorFlag : false, excepArray : [] });

				} else if (type == 'updateUserGrp'){
					showMessage("#springMessage('msg.common.result.00001')");
					getGrid("setProcessAuthGridProp").getSelectionModel().deselectAll();
					getGrid("setSystemAuthGridProp").getSelectionModel().deselectAll();
					
					editForm.setFieldValue('overLapYNuser_grp_id', 'N');
					
					formEditable({id : 'setEditUserGrpProp', editorFlag : false, excepArray : [] });
					Ext.getCmp('setStaticUserGrpTreeProp').view.selModel.deselectAll();
				}
				
		        pageInit();
				gridInit();
				Ext.getCmp('userGrpCheckBtn').disable();
				
			}else{
				showMessage(data.resultMsg);
			}
			// 마스크를 숨깁니다.
			setViewPortMaskFalse();
		}
	});
};

</script>
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
</body>
</html>