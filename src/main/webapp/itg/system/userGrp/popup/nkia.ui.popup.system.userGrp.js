var popUp_comp={};

/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */
function createAuthAddPop(setPopUpProp){
	
	var storeParam = {};
	var that = {};
	var systemAuthSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	if(popUp_comp[that['id']] == null) {
		var auth_map = {};
		var auth_id_array = that['auth_id_map'];
		auth_map['auth_id'] = auth_id_array;
		
		var setAuthGridProp = {	
				context: that['context'],						
				id: that['gridId'],	
				height: '100%',
				resource_prefix: that['resource_prefix'],				
				url: that['url'],	
				params: auth_map,		
				selModel: true,
				pagingBar : false,
				gridWidth: '100%',
				autoLoad: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/menu_left02.gif',
				actionBtnEvent: actionBtnEventMenu,
				emptyMsg: that['emptyMsg']
		}
		var authGrid = createGridComp(setAuthGridProp);		
		var insertBtn = new Ext.button.Button({
	        text: '적용',
	        handler: function() {
	        	var selectedRecords = Ext.getCmp(that['gridId']).view.selModel.getSelection();
				if(selectedRecords.length <1){
					showMessage("권한을 선택하지 않았습니다.");
				}else{	
					checkGridRowCopyToGrid(that['gridId'], 'setSystemAuthGridProp', { primaryKeys: ['AUTH_ID'] });
					systemAuthSelectPop.close();
				}
	        }
		});
		
		systemAuthSelectPop = Ext.create('nkia.custom.windowPop', {
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn],
			bodyStyle: that['bodyStyle'],
			modal : true,
			layout : 'fit',
			items: [authGrid],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null;
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = systemAuthSelectPop;
		
	} else {
		systemAuthSelectPop = popUp_comp[that['id']];
		
		var auth_map = {};
		var auth_id_array = that['auth_id_map'];
		auth_map['auth_id'] = auth_id_array;
		var proGrid = getGrid(that['gridId']).getStore();
		proGrid.proxy.jsonData = auth_map;
		proGrid.load();
	}
	
	return systemAuthSelectPop;
}

/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */
nkia.ui.popup.system.chargerAuthAdd = function() {
	// 속성 Properties
	this.params = {};
	this.resource = {};
	this.proc_auth_id_map = {};
	this.id = "";	
	this.url = "";
	this.type = "";
	this.title = "";   
	this.gridId = "";
	this.width = "";
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.checkbox = true;
	this.closeble = true;
	
	// 구성요소 Properties
	this.window = {};
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		var proc_auth_map = {};
		var proc_auth_id_array = this.params;
		proc_auth_map['proc_auth_id'] = proc_auth_id_array;
		
		this.grid = createGridComp({
			id: this.id+"_grid",
			header: {
	        	title: this.title,
		    },
			checkbox: this.checkbox,
		    resource: this.resource,
		    params: proc_auth_map,
			url: this.url,
			pageable: false,
			
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	    		    	//width: this.width,
	    		    	//height: this.height,
	                    rows: this.grid
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적   용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	}
	// 적용(선택) 이벤트
	this.choice = function(id, e){
    	var selectedRecords = $$(this.id+"_grid")._getSelectedRows();
    	if(selectedRecords.length <1){
			alert("권한을 선택하지 않았습니다.");
		}else{
			var paramMap= {};
			var authResultStr = [];
			for (var i in selectedRecords) {
				authResultStr.push(selectedRecords[i].PROC_AUTH_ID);
			 }
			paramMap['proc_auth_id'] = authResultStr;
		    paramMap['grp_id'] = $$("chargerGrpTree")._getSelectedItem().node_id;
			nkia.ui.utils.ajax({
				url: "/itg/workflow/charger/insertChargerAuth.do",
				params: paramMap,
				async: false,
				success: function(result){
					if(result.success){
						alert("등록 되었습니다.");
					}
				}
			});
			$$("authGrid")._reload({grp_id : $$("chargerGrpTree")._getSelectedItem().node_id})
			this.window.close();
			
		}
	};
}
nkia.ui.popup.system.proAuthAdd = function() {
//function createProAuthAddPop(setPopUpProp){
	// 속성 Properties
	this.params = {};
	this.resource = {};
	this.proc_auth_id_map = {};
	this.id = "";	
	this.url = "";
	this.type = "";
	this.title = "";   
	this.gridId = "";
	this.width = "";
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.checkbox = true;
	this.closeble = true;
	
	// 구성요소 Properties
	this.window = {};
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		var proc_auth_map = {};
		var proc_auth_id_array = this.params;
		proc_auth_map['proc_auth_id'] = proc_auth_id_array;
		
		this.grid = createGridComp({
			id: this.id+"_grid",
			header: {
	        	title: this.title,
		    },
			checkbox: this.checkbox,
		    resource: this.resource,
		    params: proc_auth_map,
			url: this.url,
			pageable: false,
			
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		var bodyScrollView = {
				view : "scrollview",
				body : {
					cols : [{
					rows : new Array().concat(this.grid)
					}]
				}
			};
		this.window = createWindow({
			id: this.id,
	    	height: _this.height,
			header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows : [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적   용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	}
	// 적용(선택) 이벤트
	this.choice = function(id, e){
    	var selectedRecords = $$(this.id+"_grid")._getSelectedRows();
		if(selectedRecords.length <1){
			alert("권한을 선택하지 않았습니다.");
		}else{
			$$("processAuthGrid")._addRow(selectedRecords);
			this.window.close();
		}
	};
}
nkia.ui.popup.system.sysAuthAdd = function() {
		// 속성 Properties
		this.params = {};
		this.resource = {};
		this.proc_auth_id_map = {};
		this.id = "";	
		this.url = "";
		this.type = "";
		this.title = "";   
		this.gridId = "";
		this.width = "";
		this.height = nkia.ui.utils.getWidePopupHeight();
		this.checkbox = true;
		this.closeble = true;
		
		// 구성요소 Properties
		this.window = {};
		this.grid = [];
		
		
		this.setProps = function(props) {
			for(var i in props) {
				if(this[i] !== undefined && props.hasOwnProperty(i)) {
					this[i] = props[i];
				}
			}
		};
		
		this.setRender = function(props) {
			var _this = this;
			var auth_map = {};
			var auth_id_array = this.params;
			auth_map['auth_id'] = auth_id_array;
			
			var gridProps = {
					id: this.id+"_grid",
					header: {
			        	title: this.title,
				    },
					checkbox: this.checkbox,
				    resource: this.resource,
				    params: auth_map,
					url: this.url,
					pageable: false,
					on: this.on
			};
			var grid = null;
			this.grid = createGridComp(gridProps);
		};
		
		this.setWindow = function(){
			var _this = this;
			var bodyScrollView = {
					view : "scrollview",
					body : {
						cols : [{
							rows : new Array().concat(this.grid)
						}]
					}
				};
			this.window = createWindow({
		    	id: this.id,
				height: _this.height,
		    	header: {
		    		title: this.title
		    	},
		    	body: {
		    		rows : [bodyScrollView]
		    	},
		    	footer: {
		    		buttons: {
		    			align: "center",
		    			items: [
	    		            createBtnComp({label: '적   용', type: "form", click: this.choice.bind(_this)})
	    		        ]
		    		}
		    	},
		    	closable: true
		    });
		}
		// 적용(선택) 이벤트
		this.choice = function(id, e){
	    	var selectedRecords = $$(this.id+"_grid")._getSelectedRows();
			if(selectedRecords.length <1){
				alert("권한을 선택하지 않았습니다.");
			}else{
				$$("systemAuthGrid")._addRow(selectedRecords);
				this.window.close();
			}
		};
	}
/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */

nkia.ui.popup.system.proAuthDetail = function() {
	// 구성요소 Properties
	this.params = {};
	this.resource = {};
	this.pro_auth_map = {};
	this.auth_id = "";
	this.id = "";	
	this.url = "";
	this.type = "";
	this.title = "";   
	this.gridId = "";
	this.width = "";
	this.heigth = "";
	this.checkbox = true;
	this.closeble = true;
	this.window = {};
	var storeParam = {};
	var menuViewPop = null;
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		//var params = ( typeof props.params == "undefined" ) ? {} : props.params;
		pro_auth_map = {
			auth_id : this.auth_id
		};
		var treeProps = {
			id: props.id + "_tree",
	        url: props.url,
	        filterable: true,
	        params: pro_auth_map,
	        expandLevel: 3,
	        checkbox : true
	    };
		var tree = null;
		this.tree = createTreeComponent(treeProps);
	};
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows : this.tree
	    	},
	    	closable: true
	    });
	}
}
function createMenuViewPop(setPopUpProp){
	
	var storeParam = {};
	var that = {};
	var menuViewPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var pro_auth_map = {};
		var auth_id = that['auth_id'];
		pro_auth_map['auth_id'] = auth_id;
		
		var setTreeProp = {
				context: '${context}',						
				id : that['treeId'],
				url : that['url'],
				dynamicFlag : false,
				searchable : true,
				expandAllBtn : false,
				collapseAllBtn : false,
				params: pro_auth_map,
				expandLevel: 4,
				rootVisible: false
		}
		var menuTree = createTreeComponent(setTreeProp);
		
		menuViewPop = new Ext.Window({
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			bodyStyle: that['bodyStyle'],
			items: [menuTree],
			autoScroll: false,
			layout: 'fit',
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = menuViewPop;
		
	} else {
		menuViewPop = popUp_comp[that['id']];
	}
	
	return menuViewPop;
}

// 2016-04-18 최양규: 권한설정 > 시스템권한목록 > 추가 버튼 클릭후 팝업에서 상세메뉴 보기 버튼 이벤트.
// ***** 시스템 권한 추가 시에 시스템권한에 맵핑 되어 있는 메뉴를 볼 수 있도록 개선한다 ***** // 
function actionBtnEventMenu(grid, rowIndex, colIndex){
	var auth_id = grid.getStore().data.items[rowIndex].data.AUTH_ID;
	var setPopUpProp = {
		context: '${context}',
        id: 'menuViewPop1',
        popUpText: getConstText({ isArgs: true, m_key: 'res.title.system.usergrpauthmap'}),
        treeId: 'menuViewPopTree1',
        height: 440,
        width: 300,
        url: '/itg/system/userGrp/searchMappingMenu.do',
        auth_id: auth_id,
        bodyStyle: 'background-color: white; ',
        border: false
    }
	
	var popUp = createMenuViewPop(setPopUpProp);
	popUp.show();
}

/**
 * @method {} createDeptWindow : Form Function
 * @property {string} id			: Form Id
 * @property {object} header 		: Form Header
 * @property {object} fields 		: Form Fields
 * @property {object} footer 		: Form Footer
 * @returns {object} form
 */
function createUserGrpWindow(popupProps) {
	var props = popupProps||{};
	var userGrp;
	try {
		switch(props.type){
			case('proAuthAdd') :
				userGrp = new nkia.ui.popup.system.proAuthAdd();
				break;
			case('sysAuthAdd') :
				userGrp = new nkia.ui.popup.system.sysAuthAdd();
				break;
			case('proAuthDetail') :
				userGrp = new nkia.ui.popup.system.proAuthDetail();
				break;
			case('chargerAuthAdd') :
				userGrp = new nkia.ui.popup.system.chargerAuthAdd();
				break;
			default : 
				userGrp = new nkia.ui.popup.system.DeptTree();
		}
		userGrp.setProps(props);
		userGrp.setRender(props);
		userGrp.setWindow();
	} catch(e) {
		alert(e.message);
	}
}
