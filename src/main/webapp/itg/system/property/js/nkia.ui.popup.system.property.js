<script type="text/javascript" charset="utf-8">
function propertyManagerPopup(){
	// 팝업 그리드 선언
	var popPrptyGrid = createGridComp({
		id: 'popPrptyGrid',
		header: {
        	title: '#springMessage("res.label.system.prpty.title.00004")',
        	buttons : {
	            items : [
	                createBtnComp({label : '#springMessage("btn.common.newInsert")', id : 'popNewInsertBtn', type : "form", click : "actionLinkForPop('newInsert')" })
	            ]
	        }
	    },
	    resource: 'grid.system.property.propertyUserGrid',
		url: '/itg/system/property/searchPropertyUserList.do',
		select: true,
		resizeColumn : true,
		pageable : false,
		pageSize : 100,
		autoLoad : true,
		height: 280,
		on:{onItemDblClick: gridRowClickForPop},
		params : {}
	});
	
	// property 하단 상세 폼
	var popPrptyForm = createFormComp({
		id: 'popPrptyForm',
		header: {
        	title: '#springMessage("res.label.system.prpty.title.00005")'
	    },
	    fields: {
	        colSize: 2,
	        items: [
					{item : createTextFieldComp({label:'#springMessage("res.label.system.prpty.00008")', name:'user_id', required:true})}
					,{item : createCodeComboBoxComp({label: '#springMessage("res.label.system.prpty.00010")', id:'ref_user_id', disabled:false, name: 'ref_user_id', attachChoice: true, url:'/itg/system/property/searchPropertyUserList.do',displayField:'USER_NM',valueField:'USER_ID'})}
					,{colspan: 2, item : createTextFieldComp({label:'#springMessage("res.label.system.prpty.00009")', name:'user_nm', required:true})}
					,{colspan: 2, item : createTextAreaFieldComp({label:'#springMessage("res.label.system.prpty.00007")', name:'user_desc', height: 100})}
			]
	    }
	});
	
	// 팝업 UI
	var popWindow = createWindow({
    	id: "popWindow",
    	width: 800,
    	height: 750,
    	header: {
    		title: '#springMessage("res.label.system.prpty.title.00004")'
    	},
    	body: {
    		rows: new Array().concat(popPrptyGrid,popPrptyForm)
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items : [
	                createBtnComp({label : '#springMessage("btn.common.insert")', id : 'popInsertBtn', visible: false, type : "form", click : "actionLinkForPop('insert')" }),
	                createBtnComp({label : '#springMessage("btn.common.modify")', id : 'popUpdateBtn', visible: false, type : "form", click : "actionLinkForPop('update')" }),
	                createBtnComp({label : '#springMessage("btn.common.delete")', id : 'popDeleteBtn', visible: false, click : "actionLinkForPop('delete')" }),
	                createBtnComp({label : '#springMessage("btn.common.cancel")', id : 'popCancelBtn', visible: false, click : "actionLinkForPop('cancel')" }),
	                createBtnComp({label : '#springMessage("btn.common.close")', id : 'popCloseBtn', visible: true, click : "actionClose()" })
	            ]
    		}
    	},
    	closable: false
    });
	
	// 팝업창 오픈
	$$("popWindow").show();
	$$("popPrptyForm")._setControl({editorMode : false});
}

//팝업용 grid row 클릭 시 상세 selected
function gridRowClickForPop() {
	var popPrptyForm = $$('popPrptyForm');
	var popPrptyGrid = $$('popPrptyGrid');
	
	var userInfo = popPrptyGrid._getSelectedRows();
	
	var paramMap = {};
	paramMap["search_user_id"] = userInfo[0].USER_ID;
	
	nkia.ui.utils.ajax({
        url: '/itg/system/property/selectPropertyUserInfo.do',
		params: paramMap,
		isMask: false,
        success: function(data){
        	if(data.success){
        		var data = data.resultMap;

				// resultMap 데이터 담기
				var _data = {
					user_id : data.USER_ID,
					user_nm : data.USER_NM,
					user_desc : data.USER_DESC
				}
				// 수정 모드로 변경(폼 활성화)
				popPrptyForm._setControl({editorMode : true});
				buttonHandlersForPop("update");

				// 폼 데이터 셋팅
				popPrptyForm._setValues(_data);
				return false;
        	}else{
    			nkia.ui.utils.notification({
    				type: 'error',
    				message: data.resultMsg
    			});
    		}
		}
	});
}

//모든 버튼에 대해 command값을 주고 switch문으로 해당 버튼 내용 호출(팝업용)
function buttonHandlersForPop(command) {
	var popPrptyForm = $$("popPrptyForm");
	
	switch(command) {
		case 'newInsert' :
			popPrptyForm._reset(); 
			popPrptyForm._setControl({editorMode : true});
			
			nkia.ui.utils.hides(['popNewInsertBtn']);
			nkia.ui.utils.shows(['popInsertBtn','popCancelBtn']);
		break;
		case 'insert' :
			popPrptyForm._reset(); 
			popPrptyForm._setControl({editorMode : true});
			
			nkia.ui.utils.hides(['popUpdateBtn','popDeleteBtn','popNewInsertBtn']);
			nkia.ui.utils.shows(['popInsertBtn','popCancelBtn']);
		break;
		case 'init' :
			popPrptyForm._reset(); 
			popPrptyForm._setControl({editorMode : false});
			
			nkia.ui.utils.hides(['popUpdateBtn','popDeleteBtn','popInsertBtn','popCancelBtn']);	
			nkia.ui.utils.shows(['popNewInsertBtn']);
			
			$$("ref_user_id").define("disabled", true);
			$$("ref_user_id").refresh();
		break;
		case 'update' :
			popPrptyForm._setControl({editorMode : true});
			
			nkia.ui.utils.hides(['popNewInsertBtn','popInsertBtn']);	
			nkia.ui.utils.shows(['popUpdateBtn','popDeleteBtn','popCancelBtn']);
			
			$$("ref_user_id").define("disabled", true);
			$$("ref_user_id").refresh();
		break;
		case 'cancel' :
			popPrptyForm._reset(); 
			popPrptyForm._setControl({editorMode : false});
			
			nkia.ui.utils.hides(['popInsertBtn','popCancelBtn']);
			nkia.ui.utils.shows(['popNewInsertBtn']);
		break;
	}	
}

//팝업용 버튼 ajax 처리(저장, 수정, 삭제)
function actionLinkForPop(command) {
	var popPrptyForm = $$('popPrptyForm');

	switch(command) {
		case 'newInsert' :
			buttonHandlersForPop("insert");
		break;
		
		case 'cancel' :
			buttonHandlersForPop("init");
		break;
		
		case 'insert' :
			if(popPrptyForm._validate()){
				if(!confirm('#springMessage("msg.common.confirm.00001")')){
					return false;
				}
				
				var paramMap = popPrptyForm._getValues();
	
				nkia.ui.utils.ajax({
			        url: '/itg/system/property/insertPropertyUser.do',
					params: paramMap,
					isMask: false,
			        success: function(data){
			        	if(data.success){
			        		if(data.resultMsg == "false"){
			        			alert('#springMessage("msg.system.listWidget.00003")')
			        		}else{
								$$('popPrptyGrid')._reload();
								buttonHandlersForPop("init");
								
								nkia.ui.utils.notification({
				    				type: 'info',
				    				message: data.resultMsg
				    			});
			        		}
			        	}
					}
				})
			}
		break;
		case 'update' :
			if(popPrptyForm._validate()){
				
				// selected용
				var selectedUserId = $$('popPrptyGrid')._getSelectedRows()[0].USER_ID;
				var formData = {};
				
				if(!confirm('#springMessage("msg.common.confirm.00002")')){
					return false;
				}
				
				var paramMap = popPrptyForm._getValues();
	
				nkia.ui.utils.ajax({
			        url: '/itg/system/property/updatePropertyUser.do',
					params: paramMap,
					isMask: false,
			        success: function(data){
			        	if(data.success){
			        		buttonHandlersForPop("init");
		        			
			        		// 리로드 후 부모 코드 selected 
			        		$$("popPrptyGrid")._reload(formData, false, function(){
								var _this = this;
								var rows = _this._getRows();
								for(var i=0; i<rows.length; i++) {
									var row = rows[i];
									if(row && row.USER_ID && row.USER_ID == selectedUserId) {
										var rowId = row.id;
										//이전에 선택된 행을 다시 셀렉트
										_this.select(rowId, false);
									}
								}
			        		});	
			        		
							nkia.ui.utils.notification({
			    				type: 'info',
			    				message: data.resultMsg
			    			});
			        	}
					}
				})
			}else{
				showMessage('#springMessage("msg.common.00011")');
				return false;
			}
		break;
		case 'delete' :
			if(popPrptyForm._validate()){
				if(!confirm('삭제 시 해당 사용자의 Property 정보도 같이 삭제됩니다.\n정말 삭제하시겠습니까?')){
					return false;
				}
				
				var paramMap = popPrptyForm._getValues();
	
				nkia.ui.utils.ajax({
			        url: '/itg/system/property/deletePropertyUser.do',
					params: paramMap,
					isMask: false,
			        success: function(data){
			        	if(data.success){
							$$('popPrptyGrid')._reload();
							buttonHandlersForPop("init");

							nkia.ui.utils.notification({
			    				type: 'info',
			    				message: data.resultMsg
			    			});
			        	}
					}
				})
			}else{
				showMessage('#springMessage("msg.common.00011")');
				return false;
			}
		break;
	}
}

//팝업창 닫기 시 모든 프로세스 초기화 (refresh)
function actionClose(){
	var paramMap = {"user_id": ""};
	
	$$("popWindow").close();
	$$('userTree')._reload();
	$$('searchForm')._reset();
	$$('prptyGrid')._reload(paramMap);
	$$('prptyForm')._reset();
	
	buttonHandlers("init");
}
</script>