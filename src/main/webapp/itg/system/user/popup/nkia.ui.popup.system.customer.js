/********************************************
 * User
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.Customer = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 900;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.url = "";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 150;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_customerTree',
	        url: this.url,
	        header: {
	            title: getConstText({isArgs : true, m_key : 'res.00034'})
	        },
	        expColable: true,
	        filterable: true,
	        width: 250,
	        params: this.params,
	        expandLevel: 3,
	        on: {
	        	onItemDblClick: this.doTreeClick.bind(_this)
	        }
	    });
	};
	
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
			                rows: this.tree
			            }]
					}
				};

		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e){
		var treeItem = $$(this.id + '_customerTree').getItem(id);
		if(treeItem){
			
			var customer_id = treeItem.node_id;	
			var customer_nm = treeItem.text;	 

			if(treeItem.leaf == false){
				alert('상위 노드는 선택할 수 없습니다.');
			}else{
				var _data = {
						customer_id : customer_id,
						customer_nm : customer_nm,
				}
				$$("userForm")._setValues(_data);
			}
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 부서정보가 없습니다.'
            });
        }
	}

	// 적용(선택) 이벤트
	this.choice = function(){
		var treeItem = $$(this.id + "_customerTree").getSelectedItem();
		if(treeItem){
			var customer_id = treeItem.node_id;	
			var customer_nm = treeItem.text;
			if(treeItem.leaf == false){
				alert('상위 노드는 선택할 수 없습니다.');
			}else{
				$$("userForm")._setFieldValue("customer_id",customer_id);
				$$("userForm")._setFieldValue("customer_nm",customer_nm);
			}
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 부서정보가 없습니다.'
            });
        }
	};
};

/**
 * 사용자 팝업
 * @param {} popupProps
 */
function createCustomerWindow(popupProps) {
	var props = popupProps||{};
	var customer;
	try {
		switch (props.type) {
			case "process":
				break;
			default:
				customer = new nkia.ui.popup.system.Customer();
		}
		customer.setProps(props);
		customer.setRender(props);
		customer.setWindow();
	} catch(e) {
		alert(e.message);
	}
}
