/********************************************
 * User
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/

nkia.ui.popup.system.bbsComntRegPop = function() { // 코멘트 신규등록팝업
	console.log(">>> nkia.ui.popup.system.bbs.js - bbsComntRegPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = ""; 
	this.resource = ""; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
	    fieldItems.push({colspan: 2, item: createTextFieldComp({name: 'title', label: '코멘트', required: true})});
		//fieldItems.push({colspan: 2, item: createTextAreaFieldComp({name: "contnt", label:"내용", height: 150})});
		
		this.detailForm = createFormComp({
			id: _this.id + '_detailForm',
			header: {
				icon: "form",
	        	title: "코멘트등록"
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems
		    }
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.detailForm)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저장', type: "form", click: this.save.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	}
	
	// 저장 이벤트
	this.save = function(id, e){
		var _this = this;
		if(!$$(_this.id + '_detailForm')._validate()){
			return false;
		}
		
		if(!confirm('저장하시겠습니까?')){ // 저장하시겠습니까?        
			return false;
		}
		
		var detailFormMap = $$(_this.id + '_detailForm')._getValues();
		_this.params["title"] = detailFormMap["title"]; // 제목
		_this.params["contnt"] = detailFormMap["contnt"]; // 내용
		nkia.ui.utils.ajax({url:'/itg/system/integratedboard/insertBbsComnt.do', // 코멘트 저장
			params: _this.params, 
			isMask: true,
			success:function(data){
				if(data.success){
					nkia.ui.utils.notification({type:'info', message:data.resultMsg, expire:2000});
					_this.window.close();
					if(_this.callbackFunc){
						_this.params["RESULT_CD"] = "OK";
						_this.callbackFunc(_this.params);
					}
				}else{
					nkia.ui.utils.notification({type:'error', message:data.resultMsg});
				}
			}
		});
	};
};

nkia.ui.popup.system.bbsCategorySelPop = function() { // 코멘트 신규등록팝업
	console.log(">>> nkia.ui.popup.system.bbs.js - bbsCategorySelPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = ""; 
	this.resource = ""; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		console.log(">>> nkia.ui.popup.system.bbs.js :: bbsCategorySelPop :: setProps : this.params :: ", this.params);
		alert(">>>>> nkia.ui.popup.system.bbsCategorySelPop 준비중입니다 ");
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
	    fieldItems.push({colspan: 2, item: createTextFieldComp({name: 'title', label: '코멘트', required: true})});
		//fieldItems.push({colspan: 2, item: createTextAreaFieldComp({name: "contnt", label:"내용", height: 150})});
		
		this.detailForm = createFormComp({
			id: _this.id + '_detailForm',
			header: {
				icon: "form",
	        	title: "코멘트등록"
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems
		    }
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.detailForm)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저장', type: "form", click: this.save.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	}
	
	// 저장 이벤트
	this.save = function(id, e){
		var _this = this;
		if(!$$(_this.id + '_detailForm')._validate()){
			return false;
		}
		
		if(!confirm('저장하시겠습니까?')){ // 저장하시겠습니까?        
			return false;
		}
		
		var detailFormMap = $$(_this.id + '_detailForm')._getValues();
		_this.params["title"] = detailFormMap["title"]; // 제목
		_this.params["contnt"] = detailFormMap["contnt"]; // 내용
		nkia.ui.utils.ajax({url:'/itg/system/integratedboard/insertBbsComnt.do', // 코멘트 저장
			params: _this.params, 
			isMask: true,
			success:function(data){
				if(data.success){
					nkia.ui.utils.notification({type:'info', message:data.resultMsg, expire:2000});
					_this.window.close();
					if(_this.callbackFunc){
						_this.params["RESULT_CD"] = "OK";
						_this.callbackFunc(_this.params);
					}
				}else{
					nkia.ui.utils.notification({type:'error', message:data.resultMsg});
				}
			}
		});
	};
};

/**
 * 통합게시물 팝업
 * @param {} popupProps
 */
function createBbsWindow(popupProps) { // 참조 createMaintWindow
	var props = popupProps||{};
	var popupComp;
	try {
		switch (props.type) {
		case "bbsComntRegPop":
			popupComp = new nkia.ui.popup.system.bbsComntRegPop(); // 코멘트신규등록 팝업
			break;
		case "bbsCategorySelPop":
			popupComp = new nkia.ui.popup.system.bbsCategorySelPop(); // 코멘트신규등록 팝업
			break;
		default:
			alert("팝업유형이 존재하지 않습니다. ["+props.type+"] ");
			return;
		}
		popupComp.setProps(props);
		popupComp.setRender(props);
		popupComp.setWindow();
		popupComp.setInit();
	} catch(e) {
		alert(e.message);
	}
}
