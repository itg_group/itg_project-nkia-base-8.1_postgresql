/********************************************
 * User
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/

nkia.ui.popup.system.categorySelectPop = function() { // 코멘트 신규등록팝업
	console.log(">>> nkia.ui.popup.system.category.js - categorySelectPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 300;
	this.height = 600;
	this.url = ""; 
	this.resource = ""; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.widthByWindowSizePercent = 0; // *팝업창 크기 최대치 자동 설정* -- 단위 % , null or 0~10 : 기능 사용안함 , 11~100 : 비율
	this.heightByWindowSizePercent = 0; // *팝업창 크기 최대치 자동 설정* -- 단위 % , null or 0~10 : 기능 사용안함 , 11~100 : 비율
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		
		// *팝업창 크기 최대치 자동 설정*
		if ( this.widthByWindowSizePercent == null || isNaN(Number(this.widthByWindowSizePercent)) == true || Number(this.widthByWindowSizePercent) <= 10 ) {
			this.widthByWindowSizePercent = 0;
		} else {
			this.width = Math.ceil( nkia.ui.utils.getWidePopupWidth() * ( this.widthByWindowSizePercent / 100 ) ) ;
		}
		if ( this.heightByWindowSizePercent == null || isNaN(Number(this.heightByWindowSizePercent)) == true || Number(this.heightByWindowSizePercent) <= 10 ) {
			this.heightByWindowSizePercent = 0;
		} else {
			this.height = Math.ceil( nkia.ui.utils.getWidePopupHeight() * ( this.heightByWindowSizePercent / 100 ) ) ;
		}
		
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id : _this.id + "_tree",
			url : this.url,
			expColable: true, 
			filterable: true, 
			params: this.params, 
			expandLevel: 3,
			header: { 
				title: this.title 
			},
			on: { 
				onItemDblClick: function() {
					var item = $$(_this.id + "_tree").getSelectedItem();
					
					_this.callbackFunc(item);
				}
			}
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.tree)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({ label: '선택', type: "form", click: function() {
    		            	var item = $$(_this.id + "_tree").getSelectedItem();
    		            	
    						_this.callbackFunc(item);
    		            }})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
		
		$$(_this.id).show();
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	}
};

/**
 * 게시판 카테고리 선택 팝업
 * @param {} popupProps
 */
function createCategoryWindow(popupProps) { // 참조 createMaintWindow
	var props = popupProps || {};
	var popupComp;
	try {
		popupComp = new nkia.ui.popup.system.categorySelectPop();
		popupComp.setProps(props);
		popupComp.setRender(props);
		popupComp.setWindow();
		popupComp.setInit();
	} catch(e) {
		alert(e.message);
		console.error(e);
	}
}
