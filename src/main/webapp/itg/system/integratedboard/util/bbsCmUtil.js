


var bbsCmUtil = {};
bbsCmUtil.echo = function(str) {
	alert(">>>>> bbsCmUtil.echo(str) :: str : " + str);
};



/**
 * 그리드 헤더 프로퍼티 생성
 */
bbsCmUtil.makeGridHeaderProp = function(header_array) {
	var s_gridHeader = "";
	var s_gridText = "";
	var s_gridWidth = "";
	var s_gridFlex = "";
	var s_gridSortable = "";
	var s_gridXtype = "";
	var s_gridAlign = "";
	
	for(var i = 0; i < header_array.length; i++) {
		s_gridHeader += (0 < s_gridHeader.length? ",": "") + header_array[i].header;
		s_gridText += (0 < s_gridText.length? ",": "") + header_array[i].text;
		s_gridWidth += (0 < s_gridWidth.length? ",": "") + header_array[i].width;
		s_gridFlex += (0 < s_gridFlex.length? ",": "") + header_array[i].flex;
		s_gridSortable += (0 < s_gridSortable.length? ",": "") + header_array[i].sortable;
		s_gridXtype += (0 < s_gridXtype.length? ",": "") + header_array[i].xtype;
		s_gridAlign += (0 < s_gridAlign.length? ",": "") + header_array[i].align;
	}
	
	var header_column = { 
			gridHeader: s_gridHeader,
			gridText: s_gridText,
			gridWidth: s_gridWidth,
			gridFlex: s_gridFlex,
			gridSortable: s_gridSortable,
			gridXtype: s_gridXtype,
			gridAlign: s_gridAlign
		}
	return header_column;
}

/**
 * 콤보정보 refresh
 * 
 * exam) 
 * createFormComp({id: "filterForm"});
 * createCodeComboBoxComp({id:'filter_col1_nm'});
 * 
 * var opt = {formName:'filterForm', comboName:'filter_col1_nm', url:'/itg/system/dataset/searchCodeDataList.do', attachChoice:true};
 * var paramMap = {code_grp_id: clickRecord.DATASET_ID};
 * bbsCmUtil.refreshComboData(opt, paramMap);
 */
bbsCmUtil.refreshComboData = function(opt, paramMap) {
	var formComp = $$(opt.formName); // 폼 
	var comboComp = formComp._getField(opt.comboName); // 콤보
	var options = [];
	if (opt.attachChoice) {
		options.push({id: "", value: "-- 선 택 --", title: "선택"  });
	}else if (opt.attachAll) {
		options.push({id: "", value: "-- 전 체 --", title: "전체"  });
	}
	comboComp.data.options = options;
	comboComp.refresh(); // 콤보박스 값 초기화
	
	if(opt.url){
		nkia.ui.utils.ajax({
			url: opt.url,
			params: paramMap,
			success: function(response){
				var gridRows = response.gridVO.rows; 
				for(var i=0; i<gridRows.length; i++){
					options.push({id: gridRows[i].CODE_ID, value: gridRows[i].CODE_TEXT, title: gridRows[i].CODE_TEXT });
				}
				comboComp.data.options = options;
				comboComp.refresh(); // 콤보박스 조회된값으로 재설정
			} 
		});
	}
}

/**
* 월의 마지막 일자
*/
bbsCmUtil.lastday = function(strDate) {
	var calyear = strDate.substring(0, 4);
	var calmonth = strDate.substring(4, 6);
	var monthDays = new Array(31, 28, 31, 30, 31, 30, 31, 31,30, 31, 30, 31);
	if (((calyear % 4 == 0) && (calyear % 100 != 0)) || (calyear % 400 == 0)){
		monthDays[1] = 29;
	}
	var nDays = monthDays[calmonth - 1];
	return nDays;
}

/**
* List를 소문자이름으로 복사한다.
*/
bbsCmUtil.copyListLower = function(sourceList){
	var targetList = [];
	for (var r=0; r<sourceList.length; r++) {
		var sourceMap = sourceList[r];
		var targetMap = new Object();
		for(var sourceName in sourceMap){
			targetMap[sourceName.toLowerCase()] = sourceMap[sourceName]; // 소문자로 복사
		}
		targetList.push(targetMap);
	}
	return targetList;
}

/**
* 강제지연시키는 함수
*/
bbsCmUtil.sleep = function(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds){
			break;
		}
	}
}

/******
 * dev tip: 그리드 행을 특정색으로 변경, 참조: /fp/reportPoint.nvf
var baseGrid_reload_callback = function(){ 
	// 보고서형식그리드 price 적용 및 소계,총계 행칼러적용처리
	var gridComp = $$("baseGrid");
	var gridRows = gridComp._getRows();
	var gridCols = gridComp.config.columns;
	for(var r=0; r<gridRows.length; r++){
		var row = gridRows[r];
		var rowId = gridRows[r].id;
		//gridComp.removeRowCss(rowId, "disabled"); 
		//gridComp.addRowCss(rowId, "disabled"); // grid row class 적용 
		//gridComp.updateItem(rowId, row); // grid row 데이타 적용
		for(var c=0; c<gridCols.length; c++){
			var col = gridCols[c];
			var colId = gridCols[c].id;
			if("1" == gridRows[r]["GROUPING_NO2"]){ // 소계
				gridComp.removeCellCss(rowId, colId, 'disabled'); 
				gridComp.addCellCss(rowId, colId, 'disabled'); 
			}else if("1" == gridRows[r]["GROUPING_NO1"]){ // 총계
				gridComp.removeCellCss(rowId, colId, 'disabled'); 
				gridComp.addCellCss(rowId, colId, 'disabled'); 
			}else{
				gridComp.removeCellCss(rowId, colId, 'normal'); 
				gridComp.addCellCss(rowId, colId, 'normal'); // grid cell class 적용 
			}
		}
	}
};
$$("baseGrid")._reload(paramMap, 0, baseGrid_reload_callback);
******/
