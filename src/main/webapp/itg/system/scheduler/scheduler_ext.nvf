<!doctype html>
<html lang="ko">
<!--
 /**
 * 스케줄러 관리 페이지
 * 2014.03.04
 * @author <a href="mailto:jyt@nkia.co.kr"> 
 * 
 */
-->
<head>
#parse("/itg/base/include/page_meta.nvf")
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>
<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
#parse("/itg/base/include/page_extjs_inc.nvf")
<script>
Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	var insertBtn = createBtnComp({visible: true, label: '#springMessage("res.common.insert")', id:'insertBtn', ui:'correct'});
	var updateBtn = createBtnComp({visible: true, label: '#springMessage("res.common.update")', id:'updateBtn'});
	var deleteBtn = createBtnComp({visible: true, label: '#springMessage("res.common.delete")', id:'deleteBtn'});
	var executeBtn = createBtnComp({visible: true, label: "즉시실행", id:'executeBtn', ui:'correct'});
	
	var setSchedulerGridProp = {						
		id: 'schedulerGrid',	
		title : "스케줄러 목록",
		resource_prefix: 'grid.system.scheduler',				
		url: '${context}/itg/system/scheduler/selectSchedulerList.do',
		pagingBar : false,
		autoLoad: true,
		border: false,
		toolBarComp: [insertBtn, updateBtn, deleteBtn, executeBtn]
	}
	var schedulerGrid = createGridComp(setSchedulerGridProp);
	
	var setSchedulerResultGridProp = {						
		id: 'schedulerResultGrid',	
		title : "스케줄러 결과",
		resource_prefix: 'grid.system.schedulerResult',				
		url: '${context}/itg/system/scheduler/selectSchedulerResultList.do',
		pagingBar : false,
		autoLoad: false,
		border: false
	}
	var schedulerResultGrid = createGridComp(setSchedulerResultGridProp);
	
	// Viewport 설정
	var viewportProperty = {
		center: { 
			items : [schedulerGrid, schedulerResultGrid]
		}
	}
	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	
	attachCustomEvent('cellclick', schedulerGrid, gridCellClick);
	
	attachBtnEvent(insertBtn, actionLink, 'insertAction');
	attachBtnEvent(updateBtn, actionLink, 'updateAction');
	attachBtnEvent(deleteBtn, actionLink, 'delete');
	attachBtnEvent(executeBtn, actionLink, 'executeScheduler');
	
});

function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["schedulerGrid"], "schedulerResultGrid");
}

/**
 * Grid를 선택하였을 경우 실행한다.
 */
function gridCellClick( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {
	Ext.getCmp('schedulerResultGrid').searchList({schedule_id : record.raw.SCHEDULE_ID});
}

function scheduleInfoPop(action, rawData){
	var setSchedulerFormProp = {
		id: 'schedulerForm',
		title: '스케쥴러 정보',
		editOnly: true,
		columnSize: 2,
		tableProps :[
			{colspan:2, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00001")', name:'schedule_id',readOnly:true, isNotColspan: true})},
			{colspan:2, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00002")', name:'schedule_nm', notNull: true})},
			{colspan:2, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00003")', name:'schedule_bean', notNull: true})},
			{colspan:1, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00004")', name:'execute_method', notNull: true})},
			{colspan:1, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00005")', name:'cronexpression', notNull: true})},
			{colspan:1, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00006")', name:'schedule_config'})},
			{colspan:1, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00011")', name:'cron_remarks'})},
			{colspan:1, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00007")', name:'server_ip', notNull: true, vtype : 'ip'})},
			{colspan:1, item : createCodeRadioComp({label:'#springMessage("res.label.system.scheduler.00009")', name:'use_yn', code_grp_id: 'YN', columns:2, notNull: true})},
			{colspan:2, item : createTextFieldComp({label:'#springMessage("res.label.system.scheduler.00008")', name:'develop_ip', notNull: true, isNotColspan: true, vtype : 'ip'})},
			{colspan:2, item : createTextAreaFieldComp({label:'#springMessage("res.label.system.scheduler.00010")', name:'remarks'})}
		]
	};
	var schedulerFormPanel = createInnerEditorFormComp(setSchedulerFormProp);
	
	var saveBtn = createBtnComp({
		visible : true,
		label : '#springMessage("btn.common.insert")',
		id : 'saveBtn',
		ui : 'correct',
		scale : 'medium'
	});
	
	if( action == "insert" ){
		attachBtnEvent(saveBtn, actionLink, 'insert');
	}else{
		schedulerFormPanel.setDataForMap(rawData);
		attachBtnEvent(saveBtn, actionLink, 'update');
	}
	
	var scheduleInfoPopProp	= {
		id				: "scheduleInfoPop",
		title 			: '스케쥴러 정보',
		width			: 750,
		height			: 400,
		buttons			: [saveBtn],
		closeBtn 		: true,
		items			: schedulerFormPanel
	 };
	
	var scheduleInfoPop = createWindowComp(scheduleInfoPopProp);
	scheduleInfoPop.show();
	
	formEditable({id : 'schedulerForm', editorFlag : true, excepArray :[] });
}

function actionLink(command) {
	switch(command) {
		case 'insertAction' :
			scheduleInfoPop('insert', null);
		break;
		case 'updateAction' :
			var gridSelModel = getGrid("schedulerGrid").view.selModel;
			if( gridSelModel.hasSelection() ){
				var dataModel = gridSelModel.getLastSelected();
				scheduleInfoPop('update', dataModel.raw);
			}else{
				alert("선택된 스케줄러가 없습니다.");
			}
		break;
		case 'insert' :
			var schedulerForm = Ext.getCmp("schedulerForm");
			if( schedulerForm.isValid() ){
				var params = Ext.getCmp("schedulerForm").getInputData();
				
				setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00001")')){
					setViewPortMaskFalse();
					return false;
				}
				
				jq.ajax({ type:"POST"
					, url: "$!{context}/itg/system/scheduler/insertScheduler.do"
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(params)
					, success:function(data){
						if( data.success ){
							alert(data.resultMsg);
						}else{
							alert(data.resultMsg);
						}
						Ext.getCmp("schedulerGrid").searchList();
						setViewPortMaskFalse();
						Ext.getCmp("scheduleInfoPop").close();
			        }
				});
			}else{
				alert('#springMessage("msg.common.00011")');
			}
		break;
		case 'update' :
			var schedulerForm = Ext.getCmp("schedulerForm");
			if( schedulerForm.isValid() ){
				var params = Ext.getCmp("schedulerForm").getInputData();
				
				setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00001")')){
					setViewPortMaskFalse();
					return false;
				}
				
				jq.ajax({ type:"POST"
					, url: "$!{context}/itg/system/scheduler/updateScheduler.do"
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(params)
					, success:function(data){
						if( data.success ){
							alert(data.resultMsg);
						}else{
							alert(data.resultMsg);
						}
						Ext.getCmp("schedulerGrid").searchList();
						setViewPortMaskFalse();
						Ext.getCmp("scheduleInfoPop").close();
			        }
				});
			}else{
				alert('#springMessage("msg.common.00011")');
			}
		break;
		case 'delete' :
			var gridSelModel = getGrid("schedulerGrid").view.selModel;
			if( gridSelModel.hasSelection() ){
				setViewPortMaskTrue();
				
				if(!confirm('#springMessage("msg.common.confirm.00003")')){
					setViewPortMaskFalse();
					return false;
				}  
				
				var dataModel = gridSelModel.getLastSelected();
				var rowData = dataModel.data;
				
				jq.ajax({ 
					type:"POST", 
					url: getConstValue('CONTEXT') + '/itg/system/scheduler/deleteScheduler.do', 
					contentType: "application/json", 
					dataType: "json", 
					async: false, 
					data : getArrayToJson({schedule_id : rowData["SCHEDULE_ID"]}), 
					success:function(data){
						if( data.success ){
							alert(data.resultMsg);
						}else{
							alert(data.resultMsg);
						}
						Ext.getCmp("schedulerGrid").searchList();
						setViewPortMaskFalse();
					}
				});
			}else{
				alert("선택된 스케줄러가 없습니다.");
			}
		break;
		case 'executeScheduler' :
			var gridSelModel = getGrid("schedulerGrid").view.selModel;
			if( gridSelModel.hasSelection() ){
				setViewPortMaskTrue();
				
				if(!confirm("선택된 스케줄러를 실행하시겠습니까?")){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return;
				}
				
				var dataModel = gridSelModel.getLastSelected();
				var rowData = dataModel.data;
				
				jq.ajax({ 
					type:"POST", 
					url: getConstValue('CONTEXT') + '/itg/system/scheduler/executeScheduler.do', 
					contentType: "application/json", 
					dataType: "json", 
					async: false, 
					data : getArrayToJson({schedule_id : rowData["SCHEDULE_ID"], schedule_nm : rowData["SCHEDULE_NM"], schedule_bean : rowData["SCHEDULE_BEAN"], execute_method : rowData["EXECUTE_METHOD"], schedule_config : rowData["SCHEDULE_CONFIG"]}), 
					success:function(data){
						if( data.success ){
							alert(data.resultMsg);
						}else{
							alert(data.resultMsg);
						}
						setViewPortMaskFalse();
					}
				});
			}else{
				alert("선택된 스케줄러가 없습니다.");
			}
		break;
	}
}

-->
</script>
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
</body>
</html>
