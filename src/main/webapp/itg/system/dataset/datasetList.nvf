<!-- 
	통합현황관리 > 통합현황관리 
	@version 1.0
	@author 송성일
	@since 2018. 01. 16
-->
<!DOCTYPE HTML>
<html>
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
<script type="text/javascript" src='/itg/itam/statistics/popup/nkia.ui.popup.itam.excel.js'></script><!-- 자산 엑셀다운로드 팝업 createExcelTabWindow() -->
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
<script type="text/javascript" charset="utf-8">

var searchForm_initData = {}; // 검색 폼 초기데이타
var filterForm_initData = {}; // 검색 폼 초기데이타

webix.ready(function() {
	console.clear();
	console.log(">>> webix.ready() :: datasetList.nvf ");
	var fieldItems = [];
	var buttonItems = [];
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	/**************************
	 * 검색 영역 : [Form]
	 **************************/ 
	fieldItems = [];
	
	fieldItems.push(/*현황그룹*/{ colspan:1, item: createCodeComboBoxComp({label:'현황그룹', name:'dataset_grp_cd', code_grp_id: 'DATASET_GRP_CD', attachAll:true }) } );
	//fieldItems.push(/*사용여부*/{ colspan:1, item: createCodeComboBoxComp({label:'사용여부', name:'use_yn', code_grp_id: 'YN', value:'Y', attachAll:true }) } );
	//fieldItems.push(/*아이디*/{ colspan:1, item: createTextFieldComp({label:'아이디', name:"dataset_id", on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);}} }) } );
	fieldItems.push(/*현황명*/{ colspan:1, item: createTextFieldComp({label:'현황명', name:"dataset_nm", on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);}} }) } );
	
	buttonItems = [];
	buttonItems.push(/*[검색]*/createBtnComp({ label:"#springMessage('btn.common.search')", type: "form", click: function() { actionLink("searchAttr"); } }) );
	buttonItems.push(/*[초기화]*/createBtnComp({ label:"#springMessage('res.common.reset')", click: function() { actionLink("searchFormInit"); } }) );
	
	var searchForm = createFormComp({
		id: "searchForm",
		elementsConfig: { labelPosition: "left", labelWidth:130 },
		header: {
			title: "#springMessage('res.common.search')",
			icon: "search" 
		},
		footer: {buttons: { align: "center", items: buttonItems, css: "webix_layout_form_bottom" } },
		fields: { colSize:2, items:fieldItems,
		    hiddens: { }
		}
	});
	
	/**************************
	 * 통합조회 영역 : [Grid]
	 **************************/
	buttonItems = [];
	buttonItems.push(/*[등록]*/createBtnComp({ id:"mainGridNewBtn", label:"등록", type: "form", width : 120, visible:false, click: function() { actionLink("mainGridNew"); } }) );
	buttonItems.push(/*[수정]*/createBtnComp({ id:"mainGridEditBtn", label:"수정", type: "form", width : 120, visible:false, click: function() { actionLink("mainGridEdit"); } }) );
	buttonItems.push(/*[삭제]*/createBtnComp({ id:"mainGridDeleteBtn", label:"삭제", visible:false, click: function() { actionLink("mainGridDelete"); } }) );
	
	var mainGrid = createGridComp({
		id:"mainGrid",
		keys: ["MB_ID"],
		resizeColumn : true,
		pageable : true,
		checkbox : false, 
		pageSize : 10,
		autoLoad :false, // 자동조회 안함
		url :'/itg/system/dataset/searchDatasetList.do', 
		resource:"grid.system.dataset",
		height: 200,
		header: { title: '현황목록', buttons: { items: buttonItems } },
		params : {},
		on:{ onItemClick: mainGridCellClick, onItemDblClick: mainGridCellDbClick }
	});	
	
	/**************************
	 * 통합조회속성 영역 : [Grid]
	 **************************/
	buttonItems = [];
	buttonItems.push(/*[등록]*/createBtnComp({ id:"subGridNewBtn", label:"등록", type: "form", width : 120, visible:false, click: function() { actionLink("subGridNew"); } }) );
	buttonItems.push(/*[수정]*/createBtnComp({ id:"subGridEditBtn", label:"수정", type: "form", width : 120, visible:false, click: function() { actionLink("subGridEdit"); } }) );
	buttonItems.push(/*[삭제]*/createBtnComp({ id:"subGridDeleteListBtn", label:"삭제", visible:false, click: function() { actionLink("subGridDeleteList"); } }) );
	
	var subGrid = createGridComp({
		id:"subGrid",
		keys: ["DATASET_ID"],
		pageable:false,
		autoLoad:false,
		checkbox:true, 
		height: 200,
		url :'/itg/system/dataset/searchDatasetItemList.do', 
		resource:"grid.system.datasetitem",
		header: { title:'속성목록', 
		buttons: { items: buttonItems } },
		params : {},
		on:{ onItemDblClick: subGridCellDbClick}
	});	
	
	/**************************
	 * 필터 영역 : [Form]
	 **************************/ 
	fieldItems = [];
	
	fieldItems.push(/*컬럼1*/{ colspan:1, item: createCodeComboBoxComp({label:'컬럼1', id:'filter_col1_nm', name:'filter_col1_nm', url:"/itg/system/dataset/searchCodeDataList.do", code_grp_id:'__TEMP__', attachChoice:true })});
	fieldItems.push(/*조건*/{ colspan:1, item: createCodeComboBoxComp({label:'조건', name:'filter_col1_op', code_grp_id: 'FILTER_OP_CD', value:'EQUAL', attachChoice:true }) } );
	fieldItems.push(/*조건값*/{ colspan:1, item: createTextFieldComp({label:'조건값', name:"filter_col1_value" }) } );
	fieldItems.push(/*컬럼2*/{ colspan:1, item: createCodeComboBoxComp({label:'컬럼2', name:'filter_col2_nm', url:"/itg/system/dataset/searchCodeDataList.do", code_grp_id:'__TEMP__', attachChoice:true })});
	fieldItems.push(/*조건*/{ colspan:1, item: createCodeComboBoxComp({label:'조건', name:'filter_col2_op', code_grp_id: 'FILTER_OP_CD', value:'EQUAL', attachChoice:true }) } );
	fieldItems.push(/*조건값*/{ colspan:1, item: createTextFieldComp({label:'조건값', name:"filter_col2_value" }) } );
	
	buttonItems = [];
	buttonItems.push(/*[검색]*/createBtnComp({ label:"#springMessage('btn.common.search')", type: "form", click: function() { actionLink("searchDynamicAttr"); } }) );
	buttonItems.push(/*[초기화]*/createBtnComp({ label:"#springMessage('res.common.reset')", click: function() { actionLink("searchDynamicFormInit"); } }) );
	
	var filterForm = createFormComp({
		id: "filterForm",
		elementsConfig: { labelPosition: "left", labelWidth:130 },
		header: {
			title: "필터설정",
			icon: "search",
			buttons: {align: "right", items: buttonItems}
		},
		fields: { colSize:3, items:fieldItems,
		    hiddens: { dataset_id:''}
		}
	});
	
	/**************************
	 * dynamic 영역 : [Grid]
	 **************************/
	buttonItems = [];
	buttonItems.push(/*[엑셀다운로드]*/createBtnComp({ id:"excelBtn", label:"#springMessage('btn.common.exceldown')", type: "form", width : 120, click: function() { actionLink("excelDown"); } }) );
	
	var dynamicGrid = createGridComp({
		id:"dynamicGrid",
		keys: ["DATASET_ID"],
		resizeColumn : true,
		pageable : true,
		checkbox : false, 
		pageSize : 10,
		autoLoad :false, // 자동조회 안함
		url :'/itg/system/dataset/searchDynamicList.do', 
		resource:"grid.system.dataset",
		header: { title: '조회결과', buttons: { items: buttonItems } },
		params : {},
		on:{}
	});	
	
	/**************************
	 * 화면 생성
	 **************************/
	
	var view = {
		view: "scrollview",
		id: "app",
		body: {
			rows: [{
				rows: [{
					rows: searchForm
				},{
					cols: [{
						rows: mainGrid
					},{
						rows: subGrid
					}]
				},{
					rows: filterForm
				},{
					rows: dynamicGrid
				}]
			}]
		}
	};
	nkia.ui.render(view );
	searchForm_initData = $$("searchForm")._getValues();
	filterForm_initData = $$("filterForm")._getValues();
	formInit();
	searchList();
});

/**
 * 액션 클릭 
 */
function actionLink(command) {
	//console.log(">>> actionLink() => command="+command+"");
	switch(command) {
		case "searchAttr" : searchList(); break; // [조회] 기본
		case "searchFormInit" : resetSearchFormData(); break; // [초기화] 기본 
		case "searchDynamicAttr" : searchDynamicList(); break; // [조회] 동적쿼리
		case "searchDynamicFormInit" : resetDynamicFormData(); break; // [초기화] 동적쿼리
		case "excelDown" : // 엑셀다운로드
			createExcelTabPop();
			break; 
			
        default:
        	alert("[개발메시지] actionLink() command="+command+" 유형이 정의되지 않았습니다. ");
        	return;
	}
}

/**
 * 폼 초기화 
 */
function formInit(){
	//var paramMap = {};
	//$$("searchForm")._setValues(paramMap);
}

/**
 * 페이지 초기화 
 */
function pageInit(){
	$$("mainGrid")._removeAllRow(); // 현황정보 초기화
	$$("subGrid")._removeAllRow(); // 속성정보 초기화
	$$("filterForm")._reset();
	$$("filterForm")._setValues(nkia.ui.utils.copyObj(filterForm_initData)); // 필터정보 초기화
	$$("dynamicGrid")._removeAllRow(); // 조회결과 초기화
	var header_column = {gridHeader:'temp', gridText:'조회결과정보', gridWidth:'100', gridFlex:'1', gridSortable:'na', gridXtype:'na', gridAlign:'center'}
	$$("dynamicGrid")._updateHeader(header_column);
	$$("dynamicGrid").refresh();
}

/**
 * 검색 폼 초기화
 */
function resetSearchFormData(){
	pageInit();
	$$("searchForm")._reset();
	$$("searchForm")._setValues(nkia.ui.utils.copyObj(searchForm_initData));
}

/**
 * 검색 폼 초기화 : 동적그리드
 */
function resetDynamicFormData(){
	$$("dynamicGrid")._removeAllRow();
	$$("filterForm")._reset();
	$$("filterForm")._setValues(nkia.ui.utils.copyObj(filterForm_initData));
}

/**
 * 검색 
 */
function searchList(){
	pageInit();
	var paramMap = $$("searchForm")._getValues();
	paramMap.use_yn = "Y";
	$$("mainGrid")._removeAllRow();
	$$("mainGrid")._reload(paramMap);
}

/**
 * 검색 : 동적그리드
 */
function searchDynamicList(){
	var selectedRows = $$("subGrid")._getSelectedRows();
	if(selectedRows.length < 1) {
		nkia.ui.utils.notification({ type: 'error', message: '그리드에 표시할 속성정보를 선택하지 않았습니다.' });
		return false;
	}
	var filterFormMap = $$("filterForm")._getValues();
	//if("" == nullToSpace(filterFormMap.filter_col1_nm) || "" == nullToSpace(filterFormMap.filter_col1_op) || "" == nullToSpace(filterFormMap.filter_col1_value)){
	//	nkia.ui.utils.notification({ type: 'error', message: '필터설정 컬럼1을 필수로 입력해야됩니다.' });
	//	return false;
	//}

	var paramMap = {};
	paramMap["mode"] = "selectList"; // {selectList, exceldown} 
	paramMap["dataset_id"] = selectedRows[0].DATASET_ID;
	
	var select_cols = "";
	for(var i = 0 ; i < selectedRows.length ; i ++) {
		select_cols += (0<i?",":"") + selectedRows[i].ITEM_ID;
	}
	paramMap["select_cols"] = select_cols;

	var filter_col_nm = "";
	var filter_col_op = "";
	var filter_col_value = "";
	filter_col_nm = filterFormMap.filter_col1_nm + "^" + filterFormMap.filter_col2_nm + "^na";
	filter_col_op = filterFormMap.filter_col1_op + "^" + filterFormMap.filter_col2_op + "^na";
	filter_col_value = filterFormMap.filter_col1_value + "^" + filterFormMap.filter_col2_value + "^na";
	paramMap["filter_col_nm"] = filter_col_nm;
	paramMap["filter_col_op"] = filter_col_op;
	paramMap["filter_col_value"] = filter_col_value;
	
	$$("dynamicGrid")._removeAllRow();
	$$("dynamicGrid")._reload(paramMap, 0, dynamicGrid_reload_callback);
}

/**
 * dynamicGrid 재조회 callback :  그리드 헤더정보 변경한다.
 **/
function dynamicGrid_reload_callback(){
	var selectedRows = $$("subGrid")._getSelectedRows();
	
	var item_id = "";
	var item_nm = "";
	var list_width = "";
	var list_flex = "";
	var list_sortable = "";
	var list_xtype = "";
	var list_align = "";
	
	for(var i = 0 ; i < selectedRows.length ; i ++) {
		item_id += (0<i?",":"") + selectedRows[i].ITEM_ID;
		item_nm += (0<i?",":"") + selectedRows[i].ITEM_NM;
		list_width += (0<i?",":"") + selectedRows[i].LIST_WIDTH;
		list_flex += (0<i?",":"") + selectedRows[i].LIST_FLEX.toLowerCase();
		list_sortable += (0<i?",":"") + selectedRows[i].LIST_SORTABLE.toLowerCase();
		list_xtype += (0<i?",":"") + "na"; // (0<i?",":"") + selectedRows[i].LIST_XTYPE.toLowerCase();
		list_align += (0<i?",":"") + selectedRows[i].LIST_ALIGN.toLowerCase();
	}
	
	var header_column = {
		gridHeader : item_id, 
		gridText : item_nm, 
		gridWidth : list_width,
		gridFlex : list_flex,
		gridSortable : list_sortable,
		gridXtype : list_xtype,
		gridAlign : list_align
	}
	$$("dynamicGrid")._updateHeader(header_column);
	$$("dynamicGrid").refresh();
}

/**
 * 키 이벤트 (검색)
 */
function pressEnterKey(keyCode){
	if(keyCode){
		if(keyCode == "13"){ //enter key
			searchList();
		}	
	}
}

/**
 * 현황그리드 셀 클릭
 **/
function mainGridCellClick(rowId, event, node){
	var opt = null;
	var paramMap = null;
	var clickRecord = $$("mainGrid").getItem(rowId);
    var selectedItemColumn = rowId.column;

	/******************
	* subGrid 정보조회
	*******************/
	$$("subGrid")._removeAllRow();
	var rowMap = {};
	for(var key in clickRecord){
		rowMap[key.toLowerCase()] = clickRecord[key]; // 소문자로 복사
	}
	$$("subGrid")._reload(rowMap, 0, subGrid_reload_callback);
	
	/******************
	* filterForm 정보조회
	*******************/
	$$("filterForm")._setValues({dataset_id:clickRecord.DATASET_ID});
	
	opt = {formName:'filterForm', comboName:'filter_col1_nm', url:'/itg/system/dataset/searchCodeDataList.do', attachChoice:true};
	paramMap = {code_grp_id:clickRecord.DATASET_ID};
	fn_refreshComboData(opt, paramMap);
	
	opt = {formName:'filterForm', comboName:'filter_col2_nm', url:'/itg/system/dataset/searchCodeDataList.do', attachChoice:true};
	paramMap = {code_grp_id:clickRecord.DATASET_ID};
	fn_refreshComboData(opt, paramMap);
	
	$$("filterForm")._setValues(filterForm_initData);

	/******************
	* 동적그리드 초기화
	*******************/
	$$("dynamicGrid")._removeAllRow();
	var header_column = {gridHeader:'temp', gridText:'조회결과정보', gridWidth:'100', gridFlex:'1', gridSortable:'na', gridXtype:'na', gridAlign:'center'}
	$$("dynamicGrid")._updateHeader(header_column);
	$$("dynamicGrid").refresh();
}

/**
 * subGrid 재조회 callback :: 그리드 체크박스 전체선택 한다.
 **/
function subGrid_reload_callback(){
	var grid = $$("subGrid");
	var gridRows = grid._getRows();
	if(0 < gridRows.length){
		var checkBoxColumnId = grid.config.id + __CHECK_HEADER_SUFFIX;
		var item = this._convertGridDataValue(gridRows[0]);
		if(typeof item[checkBoxColumnId] === "undefined" || item[checkBoxColumnId] === false ){
			grid.getHeaderContent(__CHECK_BOX_HEADER_ID).check();
			for(var i=0; i<gridRows.length; i++){
				gridRows[i][checkBoxColumnId] = true;
			}
		}
	}
}

/**
 * main그리드 셀 더블클릭
 **/
function mainGridCellDbClick(rowId, event, node){ 
	var selectedRow = $$("mainGrid").getItem(rowId);
	var param = {};
	for(var key in selectedRow){
		param[key.toLowerCase()] = selectedRow[key]; // 소문자로 복사
	}
	fn_baseFormPopup("S", param); 
}

/**
 * sub그리드 셀 더블 클릭
 **/
function subGridCellDbClick(rowId, event, node){ 
	var selectedRow = $$("subGrid").getItem(rowId);
	var param = {};
	for(var key in selectedRow){
		param[key.toLowerCase()] = selectedRow[key]; // 소문자로 복사
	}
	fn_subFormPopup("S", param); 
}

// 현황정보팝업
function fn_baseFormPopup(saveMode, param){
	
	/**************************
	 * (팝업) Form 영역 생성
	 **************************/
	 
	var buttonItems = [];
	var fieldItems = [];
	
	fieldItems.push(/*현황아이디*/{ colspan:1, item: createTextFieldComp({label:'현황아이디', name:"dataset_id", required:true  }) } );
	fieldItems.push(/*현황명*/{ colspan:1, item: createTextFieldComp({label:'현황명', name:"dataset_nm", required:true }) } );
	fieldItems.push(/*현황그룹*/{ colspan:1, item: createCodeComboBoxComp({label:'현황그룹', name:'dataset_grp_cd', code_grp_id: 'DATASET_GRP_CD', attachChoice:true, required:true }) } );
	fieldItems.push(/*사용여부*/{ colspan:1, item: createCodeComboBoxComp({label:'사용여부', name:'use_yn', code_grp_id: 'YN', value:'Y', required:true }) } );
	fieldItems.push(/*기본SQL*/{colspan:2, item: createTextAreaFieldComp({label:'기본SQL', name:"bas_sql", placeholder:'설명) SELECT 쿼리 및 JOIN조건 및 기본 Filter조건을 넣어주세요.\nex)\nSELECT A.MENU_ID\n     , A.MENU_NM\n     , A.UPD_USER_ID\n     , B.USER_NM AS UPD_USER_NM\n  FROM SYS_MENU A\n          , SYS_USER B\n WHERE B.USER_ID = A.UPD_USER_ID', resizable:true, height:130 }) } );
	fieldItems.push(/*정렬SQL*/{ colspan:2, item: createTextFieldComp({label:'정렬SQL', placeholder:'ex) MENU_ID, UPD_USER_NM', name:"order_sql" }) } );
	fieldItems.push(/*작업사용자*/{ colspan:1, item: createUnionFieldComp({
	    items: [ createSearchFieldComp({ label:'작업사용자', name:'work_user_id_nm', required:true, click:function(){
						var param = {}; 
						nkia.ui.utils.window({
							id: "userSelectPop",
							title:'작업사용자 선택', // 작업처리자 선택 
							width: 900,
							popupParamData: param,
							reference: {form: $$('baseForm'), fields : {id: "work_user_id", name: "work_user_id_nm", cust_id: "work_cust_id", cust_nm: "work_cust_id_nm"} },
							callFunc: createUserWindow // nkia.ui.popup.system.user.js
						});
	    			}}),
				createBtnComp({ label:"초기화", click: function() { $$("baseForm")._setValues({ work_user_id:"", work_user_id_nm:"", work_cust_id:"", work_cust_id_nm:"" }); } })
	           ] }) } ); 
	fieldItems.push(/*작업부서*/{ colspan:1, item: createTextFieldComp({label:'작업부서', name:"work_cust_id_nm" }) } );
	fieldItems.push(/*작업사용자구분*/{ colspan:1, item: createCodeComboBoxComp({label:'작업사용자구분', name:'out_user_type', code_grp_id: 'WORK_USER_TYPE', required:true }) } );
	fieldItems.push(/*_tempColumn1*/{ colspan: 1, item: createTextFieldComp({label:"_tempColumn1",id:"_tempColumn1",name:"_tempColumn1",readonly:true}) } );
	
	var baseForm = createFormComp({
		id: "baseForm",
		header: {
			title:'현황정보' + ("I"==saveMode?" 등록":"") + ("U"==saveMode?" 수정":""), 
			icon: "form", // 검색폼은 헤더에 아이콘을 넣어줍니다.
			buttons: {align: "right", items: buttonItems}
		},
		fields: { colSize:2, items: fieldItems,
		    hiddens: {}
		}
	});
	
	/**************************
	 * (팝업) 화면 생성
	 **************************/
	buttonItems = [];
	if("I" == saveMode || "U" == saveMode){
		buttonItems.push(/*[저장]*/createBtnComp({ label:"저장", type: "form", 
			click: function() { 
				if($$("baseForm")._validate() != true){
					return false;
				}
				var paramMap = $$("baseForm")._getValues();
				fn_mainGridSave(saveMode, nkia.ui.utils.copyObj(paramMap));
			}}));
	}
	
	nkia.ui.utils.window({
		id:"baseFormPop",
		callFunc: function() {
			this.window = createWindow({
		    	id: "baseFormPop", width: 850,
		    	header: { title: "현황정보 팝업" },
		    	body: { rows: [{ cols: [{ rows: new Array().concat(baseForm) }] }] },
		    	footer:{
		    		buttons: {
						align: "center",
						items: buttonItems,
				        css: "webix_layout_form_bottom" 
					}
		    	},
		    	closable: true
		    })
		}
	}); 
	
	/**************************
	 * 초기화
	 **************************/
	if($$("_tempColumn1")) $$("_tempColumn1").$view.style.display = "none"; // 임시컬럼 숨기기 
	
	$$("baseForm")._setControl({editorMode:false, exceptions:[]}); 
	$$("baseForm")._setValues(param);
}

// 속성정보팝업
function fn_subFormPopup(saveMode, param){
	
	/**************************
	 * (팝업) Form 영역 생성
	 **************************/
	
	var buttonItems = [];
	var fieldItems = [];
	
	fieldItems.push(/*현황아이디*/{ colspan:1, item: createTextFieldComp({label:'현황아이디', name:"dataset_id", required:true }) } );
	fieldItems.push(/*컬럼아이디*/{ colspan:1, item: createTextFieldComp({label:'컬럼아이디', name:"item_id", required:true }) } );
	fieldItems.push(/*컬럼타이틀*/{ colspan:1, item: createTextFieldComp({label:'컬럼타이틀', name:"item_nm", placeholder:"값을 안 넣으면 컬럼아이디값으로 적용됩니다." }) } );
	fieldItems.push(/*컬럼폭*/{ colspan:1, item: createTextFieldComp({label:'컬럼폭', name:"list_width" }) } );
	fieldItems.push(/*잠금여부*/{ colspan:1, item: createTextFieldComp({label:'잠금여부', name:"list_flex" }) } );
	fieldItems.push(/*컬럼정렬여부*/{ colspan:1, item: createTextFieldComp({label:'컬럼정렬여부', name:"list_sortable" }) } );
	fieldItems.push(/*컬럼특성*/{ colspan:1, item: createTextFieldComp({label:'컬럼특성', name:"list_xtype" }) } );
	fieldItems.push(/*텍스트정렬방식*/{ colspan:1, item: createTextFieldComp({label:'텍스트정렬방식', name:"list_align" }) } );
	fieldItems.push(/*표시순시*/{colspan:1, item: createTextFieldComp({label:'표시순시', name:"list_disp_no", placeholder:"숫자을 입력해주세요.", inputType:"price" }) } );
	fieldItems.push(/*필터여부*/{ colspan:1, item: createCodeComboBoxComp({label:'필터여부', name:'cond_yn', code_grp_id: 'YN', value:'Y' }) } );
	fieldItems.push(/*필터SQL*/{colspan:2, item: createTextAreaFieldComp({label:'필터SQL', name:"cond_sql", placeholder:'설명) 입력값이 없으면 case1 형식으로 자동생성됩니다.\ncase1) A.MENU_ID [OP1] :menu_id [OP2]\ncase2) EXISTS ( SELECT 1 FROM SYS_USER EX\n                    WHERE EX.USER_ID = A.UPD_USER_ID\n                       AND EX.USER_NM [OP1] :user_nm [OP2] )', resizable:true, height:120 }) } );
	fieldItems.push(/*필터컴포넌트유형*/{ colspan:1, item: createCodeComboBoxComp({label:'필터컴포넌트유형', name:'cond_comp_type', code_grp_id: 'COLUMN_COMP_TYPE', value:'TEXT', attachChoice:true }) } );
	fieldItems.push(/*필터코드그룹아이디*/{colspan:1, item:createTextFieldComp({label:'필터코드그룹아이디', name:'cond_code_grp_id', placeholder:'공통코드 입니다'})});
	fieldItems.push(/*필터팝업아이디*/{colspan:1, item:createTextFieldComp({label:'필터팝업아이디', name:'cond_popup_id'})});
	
	var baseForm = createFormComp({
		id: "subForm",
		elementsConfig: { labelPosition: "left", labelWidth:120 },
		header: {
			title:'현황속성정보' + ("I"==saveMode?" 등록":"") + ("U"==saveMode?" 수정":""), 
			icon: "form", // 검색폼은 헤더에 아이콘을 넣어줍니다.
			buttons: {align: "right", items: buttonItems}
		},
		fields: { colSize:2, items: fieldItems,
		    hiddens: {}
		}
	});
	
	/**************************
	 * (팝업) 화면 생성
	 **************************/
	buttonItems = [];
	if("I" == saveMode || "U" == saveMode){
		buttonItems.push(/*[저장]*/createBtnComp({ label:"저장", type: "form", 
			click: function() {
				if($$("subForm")._validate() != true){
					return false;
				}
				var paramMap = $$("subForm")._getValues();
				fn_subGridSave(saveMode, nkia.ui.utils.copyObj(paramMap));
			}}));
	}
	
	nkia.ui.utils.window({
		id:"subFormPop",
		callFunc: function() {
			this.window = createWindow({
		    	id: "subFormPop", width: 850,
		    	header: { title: "현황속성정보 팝업" },
		    	body: { rows: [{ cols: [{ rows: new Array().concat(baseForm) }] }] },
		    	footer:{
		    		buttons: {
						align: "center",
						items: buttonItems,
				        css: "webix_layout_form_bottom" 
					}
		    	},
		    	closable: true
		    })
		}
	}); 

	$$("subForm")._setControl({editorMode:false, exceptions:[]}); 
	$$("subForm")._setValues(param);
}

/**
 * 콤보정보 refresh
 */
function fn_refreshComboData(opt, paramMap){
    var formComp = $$(opt.formName); // 폼 
    var comboComp = formComp._getField(opt.comboName); // 콤보
	var options = [];
	if (opt.attachChoice) {
		options.push({id: "", value: "-- 선 택 --", title: "선택"  });
	}else if (opt.attachAll) {
		options.push({id: "", value: "-- 전 체 --", title: "전체"  });
	}
	comboComp.data.options = options;
	comboComp.refresh(); // 콤보박스 값 초기화
	
	if(opt.url){
		nkia.ui.utils.ajax({
			url: opt.url,
			params: paramMap,
			success: function(response){
				var gridRows = response.gridVO.rows; 
				for(var i=0; i<gridRows.length; i++){
					options.push({id: gridRows[i].CODE_ID, value: gridRows[i].CODE_TEXT, title: gridRows[i].CODE_TEXT });
				}
				comboComp.data.options = options;
				comboComp.refresh(); // 콤보박스 조회된값으로 재설정
			} 
		});
	}
}

/**
 * 엑셀다운로드 팝업 생성
 */
function createExcelTabPop(){

	var mainGridRows = $$("mainGrid")._getSelectedRows();
	var subGrids = $$("subGrid")._getSelectedRows();
	var filterFormMap = $$("filterForm")._getValues();
	if(mainGridRows.length < 1) {
		nkia.ui.utils.notification({ type: 'error', message: '현황 선택정보가 없습니다.' });
		return false;
	}
	if(subGrids.length < 1) {
		nkia.ui.utils.notification({ type: 'error', message: '그리드에 표시할 속성정보를 선택하지 않았습니다.' });
		return false;
	}
	//if("" == nullToSpace(filterFormMap.filter_col1_nm) || "" == nullToSpace(filterFormMap.filter_col1_op) || "" == nullToSpace(filterFormMap.filter_col1_value)){
	//	nkia.ui.utils.notification({ type: 'error', message: '필터설정 컬럼1을 필수로 입력해야됩니다.' });
	//	return false;
	//}
	
	var dataset_id = mainGridRows[0].DATASET_ID; // 현황ID
	var dataset_nm = mainGridRows[0].DATASET_NM; // 현황명
	dataset_nm = dataset_nm.replace("*", "");
	dataset_nm = dataset_nm.replace("@", "");
	dataset_nm = dataset_nm.replace("$", "");
	dataset_nm = dataset_nm.replace("%", "");
	
	var paramMap = {};
	paramMap["mode"] = "exceldown"; // {selectList, exceldown} 
	paramMap["dataset_id"] = dataset_id;
	var select_cols = "";
	var select_txts = "";
	for(var i = 0 ; i < subGrids.length ; i ++) {
		select_cols += (0<i?",":"") + subGrids[i].ITEM_ID;
		select_txts += (0<i?",":"") + subGrids[i].ITEM_NM;
	}
	paramMap["select_cols"] = select_cols;
	var filter_col_nm = "";
	var filter_col_op = "";
	var filter_col_value = "";
	filter_col_nm = filterFormMap.filter_col1_nm + "^" + filterFormMap.filter_col2_nm + "^na";
	filter_col_op = filterFormMap.filter_col1_op + "^" + filterFormMap.filter_col2_op + "^na";
	filter_col_value = filterFormMap.filter_col1_value + "^" + filterFormMap.filter_col2_value + "^na";
	paramMap["filter_col_nm"] = filter_col_nm;
	paramMap["filter_col_op"] = filter_col_op;
	paramMap["filter_col_value"] = filter_col_value;
	
	var date = new Date();
	var dateStr = date.getFullYear();
	dateStr += ""+webix.Date.toFixed((date.getMonth()+1));
	dateStr += ""+webix.Date.toFixed(date.getDate());
	dateStr += "_";
	dateStr += ""+webix.Date.toFixed(date.getHours());
	dateStr += ""+webix.Date.toFixed(date.getMinutes());
	dateStr += ""+webix.Date.toFixed(date.getSeconds());
	
	var excelParam = {};
	excelParam['fileName'] 			= dataset_nm; // 엑셀파일명
	excelParam['sheetName'] 		= dataset_nm; // Sheet0 이름
	excelParam['titleName'] 		= dataset_nm; // 제목
	excelParam['includeColumns'] 	= select_cols; // 다운로드엑셀파업 ID 열
	excelParam['headerNames'] 		= select_txts; // 다운로드엑셀파업 이름 열
	excelParam['defaultColumns'] 	= select_cols;
	excelParam['url'] 				= '/itg/system/dataset/searchDynamicList.do'; // 다운로드 URL
	nkia.ui.utils.window({id: "assetExcelPop", type: "Dynamic", param: paramMap, excelParam: excelParam, title: "엑셀다운로드", callFunc: createExcelTabWindow }); 
}

</script>
</body>
</html>