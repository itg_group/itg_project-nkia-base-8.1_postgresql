/********************************************
 * User
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/

/**
 * 현황 선택팝업 (단건선택)
 */
nkia.ui.popup.system.datasetView_SELPOPUP_CL = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.popuId = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/system/datasetview/searchList.do";
	this.resource = "grid.system.datasetPop1";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.view_mode = "";
	this.DATASET_MAP = null;
	this.COND1_ITEM_LIST = null;
	this.LIST1_ITEM_LIST = null;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		//console.log(">>> datasetView_SELPOPUP_CL.setRender - props.DATASET_MAP \n", props.DATASET_MAP);
		var fieldItems = this.createCondReadyScript();
		fieldItems.push({ item : createUnionFieldComp({
            items: [createCodeComboBoxComp({
        				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
        				name: 'search_type',
        				code_grp_id: 'POPUP_SHOW_TYPE',
        				attachChoice: true,
        				width: 250
        			}),
        			createTextFieldComp({
        				name: "search_value",
        				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
        				on:{
								onKeyPress: this.doKeyPress.bind(_this)
							}	
        			})
                ]
				})
			});
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 2,
		        items: fieldItems
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "현황목록";
		
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}

		this.params.use_yn = "Y";
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			autoLoad :false, // 자동조회 안함
			pageSize: this.pageSize,
			keys: ["DATASET_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		//console.log(">>> doSearch - this.DATASET_MAP ", this.DATASET_MAP);
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		data["dataset_id"] = this.DATASET_MAP.DATASET_ID; // {selectList, exceldown} 
		data["mode"] = "selectList"; // {selectList, exceldown} 
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색영역 컨트롤 생성
	this.createCondReadyScript = function(){
		//console.log(">>> createCondReadyScript - this.COND1_ITEM_LIST ", this.COND1_ITEM_LIST);
		var fieldItems = [];

		var item_id = "";
		var cond_label = "";
		var cond_comp_type = "";
		var cond_code_grp_id = "";
		var cond_hidden_yn = "";
		var cond_required_yn = "";
		var cond_colspan = "";
		var cond_disable_yn = "";
		var cond_default_cvalue = "";
		var cond_op_nm = "";
		var cond_popup_id = "";
		
		for(var i = 0 ; i < this.COND1_ITEM_LIST.length ; i ++) {

			item_id = this.COND1_ITEM_LIST[i].ITEM_ID;
			cond_label = this.COND1_ITEM_LIST[i].COND_LABEL;
			cond_comp_type = this.COND1_ITEM_LIST[i].COND_COMP_TYPE;
			cond_code_grp_id = this.COND1_ITEM_LIST[i].COND_CODE_GRP_ID;
			cond_hidden_yn = this.COND1_ITEM_LIST[i].COND_HIDDEN_YN;
			cond_required_yn = this.COND1_ITEM_LIST[i].COND_REQUIRED_YN;
			cond_colspan = this.COND1_ITEM_LIST[i].COND_COLSPAN;
			cond_disable_yn = this.COND1_ITEM_LIST[i].COND_DISABLE_YN;
			cond_default_cvalue = this.COND1_ITEM_LIST[i].COND_DEFAULT_CVALUE;
			cond_op_nm = this.COND1_ITEM_LIST[i].COND_OP_CD_NM;
			cond_popup_id = this.COND1_ITEM_LIST[i].COND_POPUP_ID;
			
			cond_label += ("design" == this.view_mode? "(" + cond_op_nm +")": "");
			
			if ("TEXT" == cond_comp_type) {
				fieldItems.push({ colspan: cond_colspan, item: createTextFieldComp({label: cond_label, name: item_id})});
			}else if ("COMBO" == cond_comp_type) {
				fieldItems.push({ colspan: cond_colspan, item: createCodeComboBoxComp({label: cond_label, name: item_id, code_grp_id: cond_code_grp_id, attachAll: true})});
			}else if ("TEXTAREA" == cond_comp_type) {
				fieldItems.push({ colspan: cond_colspan, item: createTextAreaFieldComp({label: cond_label, name: item_id})});
			}else{
				fieldItems.push({ colspan: cond_colspan, item: createTextFieldComp({label: cond_label, name: item_id})});
			}
		}
		return fieldItems;
	}
	
	// 그리드헤더 변경
	this.resetGridHeader = function(){
		//console.log(">>> resetGridHeader - this.LIST1_ITEM_LIST ", this.LIST1_ITEM_LIST);
		
		var item_id = "";
		var item_nm = "";
		var list_width = "";
		var list_flex = "";
		var list_sortable = "";
		var list_xtype = "";
		var list_align = "";
		
		for(var i = 0 ; i < this.LIST1_ITEM_LIST.length ; i ++) {
			item_id += (0<i?",":"") + this.LIST1_ITEM_LIST[i].ITEM_ID;
			item_nm += (0<i?",":"") + this.LIST1_ITEM_LIST[i].ITEM_NM;
			list_width += (0<i?",":"") + this.LIST1_ITEM_LIST[i].LIST_WIDTH;
			list_flex += (0<i?",":"") + this.LIST1_ITEM_LIST[i].LIST_FLEX.toLowerCase();
			list_sortable += (0<i?",":"") + this.LIST1_ITEM_LIST[i].LIST_SORTABLE.toLowerCase();
			list_xtype += (0<i?",":"") + ("hidden" == this.LIST1_ITEM_LIST[i].LIST_XTYPE.toLowerCase()? "hidden": "na");
			list_align += (0<i?",":"") + this.LIST1_ITEM_LIST[i].LIST_ALIGN.toLowerCase();
		}
		var header_column = {
			gridHeader : item_id, 
			gridText : item_nm, 
			gridWidth : list_width,
			gridFlex : list_flex,
			gridSortable : list_sortable,
			gridXtype : list_xtype,
			gridAlign : list_align
		}
		$$(this.id + '_grid')._updateHeader(header_column);
		$$(this.id + '_grid').refresh();
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.DATASET_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.DATASET_NM);
					
					var values = {};
					values[fields["id"]] = id;			// 통합현황ID
					values[fields["name"]] = nm;		// 통합현황명
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};

/**
 * 통합현황 팝업
 * @param {} popupProps
 */
function createDatasetViewWindow(popupProps) {
	//console.log(">>> createDatasetViewWindow - popupProps \n", popupProps);
	var props = popupProps||{};
	var comp;
	try {
//		console.log(">>> createDatasetViewWindow - props.DATASET_MAP \n", props.DATASET_MAP);
//		console.log(">>> createDatasetViewWindow - props.COND1_ITEM_LIST \n", props.COND1_ITEM_LIST);
//		console.log(">>> createDatasetViewWindow - props.LIST1_ITEM_LIST \n", props.LIST1_ITEM_LIST);
		switch (props.type) {
			case "SELPOPUP_CL":
				comp = new nkia.ui.popup.system.datasetView_SELPOPUP_CL();
				break;
			default:
				comp = new nkia.ui.popup.system.datasetView_SELPOPUP_CL();
		}
		comp.setProps(props);
		comp.setRender(props);
		comp.setWindow();
		comp.doSearch(); // 조회실행
		comp.resetGridHeader();
	} catch(e) {
		alert(e.message);
	}
}

/**
 * 통합현황 팝업
 * @param {} popupProps
 */
function createDatasetViewWindowOpen(popupProps) {
	console.log(">>> createDatasetViewWindow - popupProps \n", popupProps);
	var props = popupProps||{};
	try {
		var paramMap = {};
		paramMap["program_id"] = props.program_id;
		nkia.ui.utils.ajax({url: '/itg/system/datasetview/searchProgramInfo.do',  // 팝업정보
			params: paramMap,
			viewId: 'search',
			isMask: true,
			success:function(data){
				if( data.success ){
					//console.log(">>> createDatasetViewWindowOpen - data.resultMap \n", data.resultMap);
					var DATASET_MAP = data.resultMap.DATASET_MAP;
					var COND1_ITEM_LIST = data.resultMap.COND1_ITEM_LIST;
					var LIST1_ITEM_LIST = data.resultMap.LIST1_ITEM_LIST;
					//console.log(">>> createDatasetViewWindowOpen - DATASET_MAP \n", DATASET_MAP);
					//console.log(">>> createDatasetViewWindowOpen - COND1_ITEM_LIST \n", COND1_ITEM_LIST);
					//console.log(">>> createDatasetViewWindowOpen - LIST1_ITEM_LIST \n", LIST1_ITEM_LIST);
				    nkia.ui.utils.window({ 
				        id: props.id, 
				        title: props.title, 
				        type: props.type, 
				        program_id: props.program_id, 
				        width: props.width, 
				        height: props.height, 
				        checkbox: props.checkbox, 
				        view_mode: props.view_mode, 
				        DATASET_MAP: DATASET_MAP, 
				        COND1_ITEM_LIST: COND1_ITEM_LIST, 
				        LIST1_ITEM_LIST: LIST1_ITEM_LIST, 
				        params: props.params, 
				        reference: props.reference, 
				        callbackFunc: props.callbackFunc, 
				        callFunc: props.callFunc 
				    }); 
				}else{
					showMessage(data.resultMsg);
				}
			}
		});
	} catch(e) {
		alert(e.message);
	}
}
