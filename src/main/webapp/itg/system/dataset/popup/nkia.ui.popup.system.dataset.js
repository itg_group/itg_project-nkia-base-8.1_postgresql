/********************************************
 * User
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/

/**
 * 현황 선택팝업 (단건선택)
 */
nkia.ui.popup.system.Dataset = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/system/dataset/searchDatasetList.do";
	this.resource = "grid.system.datasetPop1";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_type',
	                    				code_grp_id: 'POPUP_SHOW_TYPE',
	                    				attachChoice: true,
	                    				width: 250
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "현황목록";
		
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}

		this.params.use_yn = "Y";
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["DATASET_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.DATASET_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.DATASET_NM);
					
					var values = {};
					values[fields["id"]] = id;			// 통합현황ID
					values[fields["name"]] = nm;		// 통합현황명
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};

/**
 * 속성 선택팝업 (멀티체크)
 */
nkia.ui.popup.system.DatasetItemPop1 = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.subtype = "";
	this.width = 500;
	this.height = 600;
	this.url = "/itg/system/dataset/searchDatasetItemList.do";
	this.resource = "grid.system.datasetitemPop1";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;

		var fieldItems = [];
		fieldItems.push(/*현황아이디*/{ colspan:1, item: createTextFieldComp({label:'현황아이디', name:"dataset_id", readonly: true, value:this.params.dataset_id}) } );
		fieldItems.push(/*현황명*/{ colspan:1, item: createTextFieldComp({label:'현황명', name:"dataset_nm", readonly: true, value:this.params.dataset_nm }) } );
		
		console.log(">>>>>>" + this.id + '_searchForm');
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 2,
		        items: fieldItems,
		        hiddens: {}
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "현황속성목록";
		
		/*
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}
		*/
		
		if("Condition" == this.subtype){
			this.resource = "grid.system.datasetitemPop1Cond";
		}else if("List" == this.subtype){
			this.resource = "grid.system.datasetitemPop1List";
		}else if("Form" == this.subtype){
			this.resource = "grid.system.datasetitemPop1Form";
		}
		this.grid = createGridComp({
			id: this.id + '_grid',
			keys: ["RNUM"],
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: false,
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var selectRows = $$(this.id + "_grid")._getSelectedRows();
		//var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(selectRows){
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(selectRows);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};

/**
 * 코드그룹 선택팝업 (단건선택)
 */
nkia.ui.popup.system.CodeGrpPop = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.subtype = "";
	this.width = 500;
	this.height = 600;
	this.url = "/itg/system/dataset/searchCodeGrpList.do";
	this.resource = "grid.system.datasetitem.codeGrp";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_type',
	                    				code_grp_id: 'POPUP_SHOW_TYPE',
	                    				attachChoice: true,
	                    				width: 250
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "코드그룹목록";
		
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["DATASET_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.CODE_GRP_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.CODE_GRP_NM);
					
					var values = {};
					values[fields["id"]] = id;			// ID
					values[fields["name"]] = nm;		// 명
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};

/**
 * 업무팝업아이디 선택팝업 (단건선택)
 */
nkia.ui.popup.system.BizPopupPop = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.subtype = "";
	this.width = 500;
	this.height = 600;
	this.url = "/itg/system/dataset/searchBizPopupList.do";
	this.resource = "grid.system.datasetitem.bizPopup";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_type',
	                    				code_grp_id: 'POPUP_SHOW_TYPE',
	                    				attachChoice: true,
	                    				width: 250
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "팝업목록";
		
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["DATASET_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.CODE_GRP_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.CODE_GRP_NM);
					
					var values = {};
					values[fields["id"]] = id;			// ID
					values[fields["name"]] = nm;		// 명
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};

/**
 * 기본값 선택팝업 (단건선택)
 */
nkia.ui.popup.system.DevalueValuePop = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.subtype = "";
	this.width = 500;
	this.height = 600;
	this.url = "/itg/system/dataset/searchDefaultValueList.do";
	this.resource = "grid.system.datasetitem.defaultValue";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_type',
	                    				code_grp_id: 'POPUP_SHOW_TYPE',
	                    				attachChoice: true,
	                    				width: 250
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "기본값목록";
		
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["DATASET_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.DEFAULT_VALUE);
					var nm = nkia.ui.utils.check.nullValue(gridItem.BIGO);
					
					var values = {};
					values[fields["id"]] = id;			// ID
					values[fields["name"]] = nm;		// 명
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};


/**
 * 부서Tree 선택팝업 (멀티체크)
 */
nkia.ui.popup.system.DeptTreePop1 = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.subtype = "";
	this.width = 400;
	this.height = 500;
	this.url = "/itg/system/dept/searchStaticAllDeptTree.do";
	this.resource = "xxx";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	this.tree = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		this.tree = createTreeComponent({
			id: this.id + "_tree",
	        url: this.url,
	        filterable: true,
			checkbox: this.checkbox,
	        params: this.params,
	        expandLevel: 4
	    });
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.tree)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var selectRows = $$(this.id + "_tree")._getCheckedItems();
		if(selectRows){
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(selectRows);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};

/**
 * 사용자 선택팝업 (멀티체크)
 */
nkia.ui.popup.system.UserSelPop1 = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.subtype = "";
	this.width = 500;
	this.height = 600;
	this.url = "/itg/system/user/searchUserList.do";
	this.resource = "grid.system.user.pop";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.tree = [];
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_deptTree',
	        url: '/itg/base/searchDeptTree.do',
	        header: {
	            title: getConstText({isArgs : true, m_key : 'res.00034'})
	        },
	        expColable: true,
	        filterable: true,
	        width: 250,
	        params: this.params,
	        expandLevel: 3,
	        on: {
	        	onItemClick: this.doTreeClick.bind(_this)
	        }
	    });
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_type',
	                    				code_grp_id: 'POPUP_SHOW_TYPE',
	                    				attachChoice: true,
	                    				width: 250
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "기본값목록";
		
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["DATASET_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: this.tree
	                },{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e){
		var treeItem = $$(this.id + '_deptTree').getItem(id);
		
    	var form = $$(this.id + '_searchForm');
    	var values = form._getValues();
    	
    	if( getConstValue("CENTER_CUST_ID") == treeItem.up_node_id ){
			values.dept_nm = treeItem.text;
		}else{
			values.cust_id = treeItem.node_id;
		}
    	
    	if(this.params){
			if(this.params.oper_type){
				values["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				values["process_type"] = this.params.process_type;
			}
		}
    	
		$$(this.id + '_grid')._reload(values);
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var selectRows = [];
		if(this.checkbox){
			selectRows = $$(this.id + "_grid")._getSelectedRows();
		}else{
			var row = $$(this.id + "_grid").getSelectedItem();
			selectRows.push(row);
		}
		if(selectRows || 0 < selectRows.length){
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(selectRows);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 항목이 없습니다.'
            });
        }
	};
};

/**
 * 통합현황 팝업
 * @param {} popupProps
 */
function createDatasetWindow(popupProps) {
	var props = popupProps||{};
	var comp;
	try {
		switch (props.type) {
			case "datasetPop":
				comp = new nkia.ui.popup.system.Dataset(); // 통합현홍선택팝업
				break;	
			case "datasetItemPop1":
				comp = new nkia.ui.popup.system.DatasetItemPop1(); // 현황항목선택팝업
				break;	
			case "codeGrpPop":
				comp = new nkia.ui.popup.system.CodeGrpPop(); // 코드그룹선택팝업
				break;
			case "bizPopupPop":
				comp = new nkia.ui.popup.system.BizPopupPop(); // 업무선택팝업
				break;
			case "devalueValuePop":
				comp = new nkia.ui.popup.system.DevalueValuePop(); // 기본값선택팝업
				break;	
			case "deptTreePop1":
				comp = new nkia.ui.popup.system.DeptTreePop1(); // 기본값선택팝업
				break;	
			case "userSelPop1":
				comp = new nkia.ui.popup.system.UserSelPop1(); // 사용자값선택팝업
				break;	
			default:
				comp = new nkia.ui.popup.system.Dataset(); // 통합현홍선택팝업
		}
		comp.setProps(props);
		comp.setRender(props);
		comp.setWindow();
	} catch(e) {
		alert(e.message);
	}
}
