<!doctype html>
<html lang="ko">

<head>
	<!-- ITG Base Page parse -->
	#parse("/itg/base/constraints/itg_constraint.nvf")
	<!-- ITG Base Javascript parse -->
	#parse("/itg/base/include/page_webix_inc.nvf")
</head>

<body>
	#parse("/itg/base/include/page_loading.nvf")

	<script type="text/javascript" charset="utf-8">
		webix.ready(function() {

			var searchForm = createFormComp({
				id: 'searchForm',
				elementsConfig: {
					labelPosition: "left"
				},
				header: {
					icon: "search",
					title: '#springMessage("res.common.search")'
				},
				fields: {
					elementsConfig: {
						labelPosition: "left"
					},
					colSize: 3,
					items: [
					        {colspan: 1, item: createDateFieldComp({label: '#springMessage("res.label.system.holiday.00001")', name: "search_year", dateType:'yyyy', required: true, value: new Date()})},
			            	{colspan: 1, item :createCodeComboBoxComp({label:'#springMessage("res.label.system.holiday.00002")', id: "search_month", name: "search_month", code_grp_id:"SEARCH_MONTH", value: leadingZeros(new Date().getMonth() + 1, 2), attachChoice: true})},
					        {colspan: 1, item :createTextFieldComp({label: '#springMessage("res.label.system.holiday.00003")', id: "search_descr", name: "search_descr", placeholder: '#springMessage("msg.placeholder.search")', on: {onKeyPress:"pressEnterKey"}})}
		            ]
				},
				footer: {
					buttons: {
						align: "center",
						items: [
						        createBtnComp({label: '#springMessage("btn.common.search")', id:'holidaySearchBtn', type:"form", click:"searchHoliday" }),
						        createBtnComp({label: '#springMessage("btn.common.reset")', id:'holidaySearchInitBtn', click:"resetFormData" })
						]
					}
				}
			});

			var grid = createGridComp({
				id: 'holidayGrid',
				header: {
					title: '#springMessage("res.title.system.holiday")',
				},
				resource: 'grid.system.holiday',
				url: '/itg/system/holiday/searchHoliday.do',
				select: true,
				pageable: true,
				pageSize: 50,
				height: 300,
				on:{
					onItemDblClick: gridCellClick
				}
			});

			var form = createFormComp({
				id: 'editorForm',
				header: {
					title: '#springMessage("res.title.system.holiday.detail")'
				},
				fields: {
					elementsConfig: {
						labelPosition: "left"
					},
					colSize: 2,
					items: [
					        // row1
					        {colspan: 1, item : createTextFieldComp({label:'#springMessage("res.label.system.holiday.00004")', name:"solar", required: true})},
					        {colspan: 1, item : createCodeComboBoxComp({label:'#springMessage("res.label.system.holiday.00005")', name: "holi_yn", code_grp_id:"YN"})},
					        // row2
					        {colspan: 2, item : createTextFieldComp({label:'#springMessage("res.label.system.holiday.00006")', name:"descr"})}
		           ]
				}
			});

			var insForm = createFormComp({
				id: 'insertForm',
				header: {
					title: '#springMessage("res.title.system.holiday.insert")'
				},
				fields: {
					elementsConfig: {
						labelPosition: "left"
					},
					colSize: 3,
					items: [
					        {colspan: 1, item : createDateFieldComp({label:'#springMessage("res.label.system.holiday.00007")', id: "insert_year", name: "insert_year", dateType:'yyyy', required: true})},
					        {colspan: 1, item : createBtnComp({label: '#springMessage("btn.common.regist")', id:'holidayInsertBtn', type:"form", click:"actionLink('holidayInsertBtn')"})}
		           ]
				}
			});

			var delForm = createFormComp({
				id: 'deleteForm',
				header: {
					title: '#springMessage("res.title.system.holiday.delete")'
				},
				fields: {
					elementsConfig: {
						labelPosition: "left"
					},
					colSize: 3,
					items: [
					        {colspan: 1, item : createDateFieldComp({label:'#springMessage("res.label.system.holiday.00008")', id: "delete_year", name: "delete_year", dateType:'yyyy', required: true})},
					        {colspan: 1, item : createBtnComp({label: '#springMessage("btn.common.delete")', id:'holidayDeleteBtn', type:"form", click:"actionLink('holidayDeleteBtn')"})}
		           ]
				}
			});

		var footer = createButtons({
	        align: "center",
	        items: [
				createBtnComp({label: '#springMessage("btn.common.modify")', visible: false, id:'holidayUpdateBtn', type:"form", click:"actionLink('holidayUpdateBtn')"}),
	        ]
	    });

	    var view = {
	        view: "scrollview",
		    id: "app",
	        body: {
	        	rows: new Array().concat(searchForm, grid, form, footer, insForm, delForm)
	        }, 
		};

	    nkia.ui.render(view);
	    $$("editorForm")._setControl({editorMode:false});
	});

	// 검색 버튼 클릭
	function searchHoliday() {
		if($$("searchForm")._validate()) {
			var data = $$("searchForm")._getValues();
			$$("holidayGrid")._reload(data);
			$$("editorForm")._reset();
			$$("editorForm")._setControl({editorMode:false});
			nkia.ui.utils.hides(['holidayUpdateBtn']);
			$$("insertForm")._reset();
			$$("deleteForm")._reset();
		}

		return false;
	}

	// 초기화 버튼 클릭 시
	function resetFormData() {
		$$("search_month").setValue("");
		$$("search_descr").setValue("");
		return false;
	}

	// Grid Cell Click (선택한 게시물 상세 조회 )
	function gridCellClick(id, e) {
		var clickRecord = this.getItem(id);
		var solar = clickRecord.SOLAR;

		var param = {};
		param["solar"] = clickRecord.SOLAR;

		var success = function( response ) {

			var data = response.resultMap;

			pageInit();
			$$("editorForm")._setControl({editorMode:true, exceptions: ['solar']});
			$$("editorForm")._syncData(data);

		   	nkia.ui.utils.shows(['holidayUpdateBtn']);		
		};

		dataControls( "/itg/system/holiday/selectHoliday.do", param, success );

		return false;
	}

	function pageInit(reload) {
		nkia.ui.utils.hides(['holidayUpdateBtn']);

		if(reload) {
			$$("holidayGrid")._reload();	
		}

		$$("editorForm")._reset();

		return false;
	}

	function actionLink(command) {

		switch(command) {
			case 'holidayUpdateBtn' :

				if($$("editorForm")._validate()) {
					var paramMap = $$("editorForm")._getValues();

					if(!confirm('#springMessage("msg.common.confirm.00002")')) {
						break;
					}

					var success = function( data ) {
						if( data.success ) {
							pageInit();
							$$("holidayGrid")._reload();
						}

						nkia.ui.utils.notification({
							message: data.resultMsg,
							expire:2000
						});
					};
					dataControls( '/itg/system/holiday/updateHoliday.do', paramMap, success );
				}
			break;
			case 'holidayInsertBtn' :
				
				if($$("insertForm")._validate()) {
					var paramMap = $$("insertForm")._getValues();

					if(!confirm('#springMessage("msg.common.confirm.00010")')) {
						break;
					}
					var success = function( data ) {

						if (data.resultMsg == '#springMessage("msg.common.result.00001")') {
							pageInit();
							$$("holidayGrid")._reload();

							nkia.ui.utils.notification({
								message: data.resultMsg,
								expire:2000
							});
						} else {
							nkia.ui.utils.notification({
								type : 'error',
								message: data.resultMsg
							});
						}

						$$("insert_year").setValue("");
						$$("delete_year").setValue("");
					};
					dataControls( '/itg/system/holiday/insertHoliday.do', paramMap, success );
				}
			break;
			case 'holidayDeleteBtn' :
				
				if($$("deleteForm")._validate()) {
					var paramMap = $$("deleteForm")._getValues();

					if(!confirm('#springMessage("msg.common.confirm.00003")')) {
						break;
					}
					var success = function( data ) {

						if (data.resultMsg == '#springMessage("msg.common.result.00003")') {
							pageInit();
							$$("holidayGrid")._reload();

							nkia.ui.utils.notification({
								message: data.resultMsg,
								expire:2000
							});
						} else {
							nkia.ui.utils.notification({
								type : 'error',
								message: data.resultMsg
							});
						}

						$$("insert_year").setValue("");
						$$("delete_year").setValue("");
					};
					dataControls( '/itg/system/holiday/deleteHoliday.do', paramMap, success );
				}
			break;
		}
		return false;
	}

	function pageInit() {

		$$("editorForm")._reset();
	   	$$("editorForm")._setControl({editorMode:false});
	   	nkia.ui.utils.hides(['holidayUpdateBtn']);

	   	return false;
	}

	// 엔터키 입력 시
	function pressEnterKey(keyCode) {
		if (keyCode) {
			if (keyCode == "13") { // enter key
				searchHoliday();
			}else{
				return keyCode;
			}
		}
		return false;
	}

	function dataControls( url, param, func ) {
		nkia.ui.utils.ajax({
			url: url,
			params: param,
			async: false,
			success: function(data){func(data);}
		});
		return false;
	}

	</script>
</body>
</html>