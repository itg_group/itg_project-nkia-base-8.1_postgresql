/********************************************
 * Asset
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회
 ********************************************/
nkia.ui.popup.itam.selectAppConfSelectAsset = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/workflow/fp/searchAssetList.do"; // 자산 목록
	this.resource = 'grid.workflow.fp.appConf2.assetGrid';
	//url: "/itg/itam/maint/searchAsset.do", // 자산 목록
	//resource: "grid.itam.maint.asset", 
	this.gridParams = {"class_id":"4","class_type":"SW","class_mng_type":"AM","tangible_asset_yn":"Y"};
	this.gridHeight = 300;
	this.treeParams = {class_id:'4'}; // this.treeParams = {classType: "SW"};
	this.checkbox = false;
	this.expandLevel = 1;
	this.reference = {};
	this.keys = ["CONF_ID"];
	this.dependency = {};
	this.type = "itam";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_classTree',
			url: '/itg/workflow/fp/searchClassTree.do',
	        header: {
	            title: getConstText({isArgs : true, m_key : 'res.title.itam.class'})
	        },
	        expColable: true,
	        filterable: true,
	        width: 300,
	        params: this.treeParams,
	        expandLevel: 2,
	        on: {
	        	onItemClick: this.doTreeClick.bind(_this)
	        }
	    });
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item:createUnionFieldComp({
                            items: [
                                createCodeComboBoxComp({
                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}), // 조회분류
                    				name: 'search_type',
                    				code_grp_id: 'SEARCH_ASSET',
                    				attachChoice: true,
                    				width: 250
                    			}),
                    			createTextFieldComp({
                    				name: "search_value",
                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}), // 조회값 
                    				on:{
       									onKeyPress: this.doKeyPress.bind(_this)
     								}	
                    			})
                            ]
                        })
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}), // 조회 
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)}) // 초기화 
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = getConstText({isArgs : true, m_key : 'res.title.itam.maint.assetlist'}); // 자산목록
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			height : 150,
		    resource: this.resource,
		    params: this.gridParams,
			url: this.url,
			select: false,
			pageable: true,
			checkbox: this.checkbox,
			dependency: this.dependency,
			keys: this.keys,
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: this.tree
	                },{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트 - 검색폼 초기화
	this.doTreeClick = function(id, e){
		$$(this.id + '_searchForm')._reset();
		this.doSearch(id);  
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(treeId){
		var form = $$(this.id + '_searchForm');
		var values = form._getValues();
		
		// Tree 데이터
		var treeItem;
		if(treeId !== undefined){
			treeItem = $$(this.id + '_classTree').getItem(treeId);
		}else{
			treeItem = $$(this.id + '_classTree')._getSelectedItem();
		}
		
		if(treeItem){
			values.class_id = treeItem.node_id;
		}
		
		values.class_type = 'SW';
		values.class_mng_type = 'AM';
		values.tangible_asset_yn = 'Y';
		if("" == nullToSpace(values.search_type)){
			values.search_value = "";
		}
		$$(this.id + '_grid')._reload(values);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid")._getSelectedRows();
		if(gridItem.length > 0){
			//var reference = this.reference;
            //reference.grid._addRow(assetRows);
            
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 자산이 없습니다.'
            });
        }
	};
};


/********************************************
 * AssetAndService
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회 + 서비스 선택 팝업(프로세스에서 사용)
 ********************************************/
nkia.ui.popup.itam.selectAppConf = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.reference = {};
	this.actionColumns = [];
	
	// 구성요소 Properties
	this.asset = null;
	this.selectedAssetGrid = {};
	this.serviceGrid = {};
	this.helpContent = {};
	this.window = {};
	this.callbackFunc = null;
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		props.bindEvent = this;
		props.checkbox = true;
		this.asset = new nkia.ui.popup.itam.selectAppConfSelectAsset();
		this.asset.setProps(props);
		this.asset.setRender(props);
		
		this.selectedAssetGrid = createGridComp({
			id: props.id + '_selectedAssetGrid',
			header: {
				title: "선택된 자산"
			},
			height : 150,
		    resource: 'grid.workflow.fp.appConf2.assetGrid',
			select: false,
			pageable: false,
			autoLoad: false,
			keys: ["CONF_ID"],
			actionColumns: [{
				column: "ACTION",
				template: function(data){
					return nkia.ui.html.icon("remove", "delete_row");
				},
				onClick: {
					"delete_row": function(e, row, html){
						this._removeRow(row);
					}
				}
			}]
		});
		
		this.helpContent = {
 			view: "template",
 			template: "<font style='color:red'><b>*&nbsp;하단 연관 서비스 목록에서 선택(체크)하시면<br/>&nbsp;&nbsp;서비스 조회에도 적용이 됩니다.</b></font>",
 			height: 46
 		}
	};
	
	this.setWindow = function(){
		var _this = this;
		
		var popupFooter = createButtons({
			align: "center",
			items: [
				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
			]
		});
		
		var split = {
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_down", "add_asset"),
			 		height: 30,
			 		onClick: {
			 			"add_asset": function(e, row, html){
			 				var data = $$(_this.id + '_grid')._getSelectedRows();
			 				if(data.length > 0){
			 					$$(_this.id + '_selectedAssetGrid')._addRow(data);
			 				}
			 			}
			 		}
                };

		var body = {
			rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    header: "분류체계정보",
                    body: {
                    	rows: this.asset.tree
                    }
                },{
                    //rows: new Array().concat(this.asset.searchForm, this.asset.grid, split, this.selectedAssetGrid, this.helpContent, popupFooter)
					rows: new Array().concat(this.asset.searchForm, this.asset.grid, split, this.selectedAssetGrid, popupFooter)
                }]
    		}]
		}
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	closable: this.closable 
	    });
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_selectedAssetGrid")._getRows();
		if(gridItem.length > 0){
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 자산이 없습니다.'
            });
        }
	};
};

/**
 * 자산 팝업
 * @param {} popupProps
 */
function createAppConfWindow(popupProps) {
	var props = popupProps||{};
	//console.log(">>> createApplicationConfWindow > props=", props);
	var asset;
	try {
		switch (props.type) {
			case "ConfMulti":
				asset = new nkia.ui.popup.itam.selectAppConf();
				break;
			default:
				alert("화면유형이 잘못되었습니다. ");
		}
		asset.setProps(props);
		asset.setRender(props);
		asset.setWindow();
	} catch(e) {
		alert(e.message);
	}
}



