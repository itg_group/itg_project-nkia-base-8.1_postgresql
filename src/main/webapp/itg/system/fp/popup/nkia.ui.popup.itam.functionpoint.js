/********************************************
 * User
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.itam.fpReqFuncRegPop = function() { // 기능점수관리 팝업
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - fpReqFuncRegPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 1800;
	this.height = 630;
	this.url = "/itg/workflow/fp/selectReqFuncList.do"; 
	this.resource = "grid.workflow.fp.fpReqFuncRegPop"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	this.popup_mode = ""; // edit/view
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		//console.log(">>> _this.params : ", _this.params);
		
		var buttonItems = [];
		if("edit" == _this.popup_mode){
			buttonItems.push(createBtnComp({label:"행추가", type:"form", click: this.doRowAdd.bind(_this)}));
			buttonItems.push(createBtnComp({label:"행복사", type:"form", click: this.doRowCopy.bind(_this)}));
			buttonItems.push(createBtnComp({label:"행삭제", type:"form", click: this.doRowDelete.bind(_this)}));
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: '어플리케이션 변경기능 목록',
	        	buttons: { items: buttonItems }
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			//pageable: true,
			autoLoad :false,
			pageSize: this.pageSize,
			keys: ["RNUM"],
			resizeColumn: true,
			dependency: this.dependency,
			on: this.on, 
			editprops: [
				{column: "F_APP", attachChoice: true, params: {code_grp_id : "FP_APP_CD"}},
				{column: "F_LANG", attachChoice: true, params: {code_grp_id : "FP_LANG_CD"}},
				{column: "FUNC_TYPE_CD", attachChoice: true, params: {code_grp_id : "FP_FUNC_TYPE_CD"}}],
			requiredColumns: ["F_TITLE", "F_LANG", "FUNC_TYPE_CD"],
			headerMerges: [[
							{column: "F_EI_C", merge_range: 3, view_text: "외부입력"}
						]
					]
		});
		
		var fieldItems = [];
		
		fieldItems.push({colspan:1, item: createUnionFieldComp({
		    items: [createTextFieldComp({name:"function_point", label:'총계', readonly: true}),
					createBtnComp({label: "산출 미리보기", visible: ("edit" == _this.popup_mode? true: false), click: this.doCalcTotalPoint.bind(_this)})
		           ]})}); 
	    
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: "총계"
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left", labelWidth: 130 },
		        colSize: 2,
		        items: fieldItems
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		var buttonItems = [];
		//alert(">>> setWindow - _this : " + JSON.stringify(_this));
		if("edit" == _this.popup_mode){
			buttonItems.push(createBtnComp({label:"저 장", type:"form", click: this.save.bind(_this)}));
		}
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: '어플리케이션 변경기능 목록' + ("" != nullToSpace(_this.params.req_title)? " (" + _this.params.req_title + ")": "")
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.grid, this.searchForm)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: buttonItems
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
	    //var header_column = { 
    	//	gridHeader: 'CONF_ID,CONF_NM,F_TITLE,FUNC_TYPE_CD,F_LANG,F_ILF,F_EIF,F_EI_C,F_EI_U,F_EI_D,F_EO,F_EQ',
    	//	gridText: '구성아이디,구성명,변경제목,기능분류,언어,내부논리,외부연계,추가,수정,삭제,외부출력,외부조회',
    	//	gridWidth: '100,150,200,80,100,80,80,50,50,50,80,80',
    	//	gridFlex: 'na,na,1,na,na,na,na,na,na,na,na,na',
    	//	gridSortable: '0,0,0,0,0,0,0,0,0,0,0,0',
    	//	gridXtype: 'na,na,text,combo,combo,text,text,text,text,text,text,text',
    	//	gridAlign: 'center,left,left,center,center,center,center,center,center,center,center,center'
	    //}
	    //$$(this.id + '_grid')._updateHeader(header_column); 
	    //$$(this.id + '_grid').refresh();
		this.doSearch();
	}

	// 검색 버튼 이벤트
	this.doSearch = function(){
		var _this = this;
		var paramMap = {};
		paramMap = _this.params;
		paramMap["use_yn"] = "Y";
		$$(this.id + '_grid')._reload(paramMap);
	}
	
	// 행 추가
	this.doRowAdd = function(){
		var _this = this;
		if("" == nullToSpace(_this.params.sr_id)){ // update/delete시 PK값 존재여부 체크 
			nkia.ui.utils.notification({type:'error', message:"선택된 내역이 없습니다."});
			return;
		}
    	var paramMap = {req_zone: ""};
		var callback_appConfWindow_ConfMulti = function(listData){
			for (var i in listData) {
				var bStatus = true; // 중복등록체크용
				var gridRows = $$(_this.id + '_grid')._getRows();
				for (var r in gridRows) {
					if(listData[i]["CONF_ID"] == gridRows[r]["CONF_ID"]){
						bStatus = false; // 중복되면 false
						break;
					}
				}
				
				if(bStatus){
					$$(_this.id + '_grid')._addRow();
					var rows = $$(_this.id + '_grid')._getRows();
					rows[rows.length -1]["SR_ID"] = _this.params.sr_id;
					rows[rows.length -1]["CONF_ID"] = listData[i]["CONF_ID"];
					rows[rows.length -1]["CONF_NM"] = listData[i]["CONF_NM"];
					rows[rows.length -1]["FUNC_TYPE_CD"] = "2"; // 1:신규, 2:재개발
				}
			}
			$$(_this.id + '_grid').refresh();
		};
    	nkia.ui.utils.window({
    		id: "appConfWindow_ConfMulti",
    		title:'Application 구성정보', // 자산조회
    		width : 900,
    		height : 800,
    		type: "ConfMulti", // basic, ciAndService
    		selectType: null,
    		params:paramMap,
			callbackFunc: callback_appConfWindow_ConfMulti,
    		callFunc: createAppConfWindow // nkia.ui.popup.itam.applicationConf.js
    	});
		
	}
	
	// 행 복사
	this.doRowCopy = function(){
		var _this = this;
		var selectedRows = $$(_this.id + '_grid')._getSelectedRows();
		if(selectedRows.length < 1) {
			nkia.ui.utils.notification({ type: 'error', message: '선택행 정보가 없습니다.' });
			return false;
		}
		var selectedRow = selectedRows[0];
		$$(_this.id + '_grid')._addRow();
		var rows = $$(_this.id + '_grid')._getRows();
		rows[rows.length -1]["RNUM"] = rows.length;
		rows[rows.length -1]["SR_ID"] = _this.params.sr_id;
		rows[rows.length -1]["CONF_ID"] = selectedRow["CONF_ID"];
		rows[rows.length -1]["CONF_NM"] = selectedRow["CONF_NM"];
		rows[rows.length -1]["F_TITLE"] = selectedRow["F_TITLE"] + " 복제";
		rows[rows.length -1]["FUNC_TYPE_CD"] = "1"; // 신규로 추가한다.
		$$(_this.id + '_grid').refresh();
	}
	
	// 행 삭제
	this.doRowDelete = function(){
		var _this = this;
		$$(_this.id + '_grid')._removeRow();
		$$(_this.id + '_grid').refresh();
	}
	
	// 산출 미리보기
	this.doCalcTotalPoint = function(){
		var _this = this;
		this.saveReqFunc("preview");
	}
	
	// 적용(선택) 이벤트
	this.save = function(id, e){
		var _this = this;
		this.saveReqFunc("save");
	};
	
	// 적용(선택) 이벤트
	this.saveReqFunc = function(mode){
		var _this = this;
		var bStatus = true;
		var saveList = $$(_this.id + '_grid')._getRows();
		
		// 필수입력 체크.
		var cnt = 0;
		for (var r in saveList) {
			cnt++
			if("" == nullToSpace(saveList[r]["CONF_ID"])){
				nkia.ui.utils.notification({type:'error', message: cnt + '행 구성아이디는 필수 입력입니다.'});
				bStatus = false;
				break;
			}
			if("" == nullToSpace(saveList[r]["F_TITLE"])){
				nkia.ui.utils.notification({type:'error', message: cnt + '행 제목은 필수 입력입니다.'});
				bStatus = false;
				break;
			}
			if("" == nullToSpace(saveList[r]["FUNC_TYPE_CD"])){
				nkia.ui.utils.notification({type:'error', message: cnt + '행 기능분류는 필수 입력입니다.'});
				bStatus = false;
				break;
			}
			//if("" == nullToSpace(saveList[r]["F_APP"])){
			//	nkia.ui.utils.notification({type:'error', message: cnt + '행 어플리케이션은 는 필수 입력입니다.'});
			//	bStatus = false;
			//	break;
			//}
			if("" == nullToSpace(saveList[r]["F_LANG"])){
				nkia.ui.utils.notification({type:'error', message: cnt + '행 언어는 필수 입력입니다.'});
				bStatus = false;
				break;
			}
		}
		if(!bStatus){
			return false;
		}
		
		if("preview" == mode){
			if(!confirm('미리보기 하시겠습니까?')){ 
				return false;
			}
		}else if("save" == mode){
			if(!confirm('등록하시겠습니까?')){ // 저장하시겠습니까? 
				return false;
			}
		}else{
			return false;
		}
		
		var bStatus = true;
		saveList = this.copyListLower(saveList);
		
		if(!bStatus){
			return false;
		}
		
		var paramMap = {};
		paramMap["INPUT1_MODE"] = mode;
		paramMap["sr_id"] = _this.params.sr_id;
		paramMap["INPUT1_LIST"] = saveList;
		//console.log(">>> doCalcTotalPoint => paramMap : ", paramMap);
		nkia.ui.utils.ajax({url: '/itg/workflow/fp/insertReqFuncList.do', // 저장
			params: paramMap,
			viewId: 'save',
			isMask: true,
			success: function(data){
				if(data.success){
					var paramMap = data.resultMap;
					$$(_this.id + '_searchForm')._setValues({function_point: data.resultMap.function_point});
					if("save" == mode){
						_this.window.close();
						if(_this.callbackFunc){
							_this.callbackFunc(paramMap);
						}
					}
				}else{
					nkia.ui.utils.notification({type:'error', message:data.resultMsg});
				}
			}
		});
	};
	

	// List를 소문자이름으로 복사한다.
	this.copyListLower = function(sourceList){
		var targetList = [];
		for (var r=0; r<sourceList.length; r++) {
			var sourceMap = sourceList[r];
			var targetMap = new Object();
			for(var sourceName in sourceMap){
				targetMap[sourceName.toLowerCase()] = sourceMap[sourceName]; // 소문자로 복사
			}
			targetList.push(targetMap);
		}
		return targetList;
	}
};

nkia.ui.popup.itam.fpCiSelPop = function() { // 구성 선택팝업
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - fpCiSelPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = "/itg/workflow/fp/selectCiSelPopList.do"; 
	this.resource = "grid.workflow.fp.fpCiSelPop"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
		fieldItems.push({colspan:1, item: createUnionFieldComp({
		    items: [createCodeComboBoxComp({label:'검색', name:'search_type', code_grp_id: 'POPUP_SHOW_TYPE', value:'NAME', attachChoice:true }),
		            createTextFieldComp({name:"search_value", on:{onKeyPress: this.doKeyPress.bind(_this)}}),
					createBtnComp({label:"초기화", click: function() { $$("searchForm")._setValues({search_type:"NAME", search_value:"" }); } })
		           ]})}); 
		
		this.searchForm = createFormComp({
			id: _this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "모듈";
		
		if(this.type == "multitask"){
			this.on = {};
		}else{
			this.on = {
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		}
		
		this.grid = createGridComp({
			id: _this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			autoLoad :false,
			pageSize: this.pageSize,
			keys: ["RNUM"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	    //var header_column = { 
        //	gridHeader: 'CI_ID,CI_NM',
        //	gridText: '구성아이디,구성명',
        //	gridWidth: '200,250',
        //	gridFlex: 'na,1,',
        //	gridSortable: '0,0',
        //	gridXtype: 'na,na',
        //	gridAlign: 'center,left'
	    //} 
	    //$$(_this.id + '_grid')._updateHeader(header_column); 
	    //$$(_this.id + '_grid').refresh();
		this.doSearch();
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var _this = this;
		var form = $$(_this.id + '_searchForm');
		var paramMap = form._getValues();
		paramMap["use_yn"] = "Y";
		$$(_this.id + '_grid')._reload(paramMap);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		var _this = this;
		$$(_this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var _this = this;
		var gridItem = $$(_this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.MODEL_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.MODEL_TITLE);
					
					var values = {};
					values[fields["id"]] = id;
					values[fields["name"]] = nm;
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};

nkia.ui.popup.itam.fpCiTypeSelPop = function() { // 구성분류 선택팝업
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - fpCiTypeSelPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = "/itg/workflow/fp/selectCiTypeSelPopList.do"; 
	this.resource = "grid.workflow.fp.fpCiTypeSelPop"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
		fieldItems.push({colspan:1, item: createUnionFieldComp({
		    items: [createCodeComboBoxComp({label:'검색', name:'search_type', code_grp_id: 'POPUP_SHOW_TYPE', value:'NAME', attachChoice:true }),
		            createTextFieldComp({name:"search_value", on:{onKeyPress: this.doKeyPress.bind(_this)}}),
					createBtnComp({label:"초기화", click: function() { $$("searchForm")._setValues({search_type:"NAME", search_value:"" }); } })
		           ]})}); 
		
		this.searchForm = createFormComp({
			id: _this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "모델";
		
		if(this.type == "multitask"){
			this.on = {};
		}else{
			this.on = {
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		}
		
		this.grid = createGridComp({
			id: _this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			autoLoad :false,
			pageSize: this.pageSize,
			keys: ["RNUM"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	    //var header_column = { 
        //	gridHeader: 'CI_TYPE_ID,CI_TYPE_NM',
        //	gridText: '구성분류아이디,구성분류명',
        //	gridWidth: '200,250',
        //	gridFlex: 'na,1,',
        //	gridSortable: '0,0',
        //	gridXtype: 'na,na',
        //	gridAlign: 'center,left'
        //} 
	    //$$(_this.id + '_grid')._updateHeader(header_column); 
	    //$$(_this.id + '_grid').refresh();
		this.doSearch();
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var _this = this;
		var form = $$(_this.id + '_searchForm');
		var paramMap = form._getValues();
		paramMap["use_yn"] = "Y";
		$$(_this.id + '_grid')._reload(paramMap);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		var _this = this;
		$$(_this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var _this = this;
		var gridItem = $$(_this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.MODEL_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.MODEL_TITLE);
					
					var values = {};
					values[fields["id"]] = id;
					values[fields["name"]] = nm;
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};

nkia.ui.popup.itam.fpModelSelPop = function() { // 모델 선택팝업 
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - fpModelSelPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = "/itg/workflow/fp/selectModelSelPopList.do"; 
	this.resource = "grid.workflow.fp.fpModelSelPop"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
		fieldItems.push({colspan:1, item: createUnionFieldComp({
		    items: [createCodeComboBoxComp({label:'검색', name:'search_type', code_grp_id: 'POPUP_SHOW_TYPE', value:'NAME', attachChoice:true }),
		            createTextFieldComp({name:"search_value", on:{onKeyPress: this.doKeyPress.bind(_this)}}),
					createBtnComp({label:"초기화", click: function() { $$("searchForm")._setValues({search_type:"NAME", search_value:"" }); } })
		           ]})}); 
		
		this.searchForm = createFormComp({
			id: _this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "모델";
		
		if(this.type == "multitask"){
			this.on = {};
		}else{
			this.on = {
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		}
		
		this.grid = createGridComp({
			id: _this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			autoLoad :false,
			pageSize: this.pageSize,
			keys: ["RNUM"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	    //var header_column = { 
        //	gridHeader: 'MODEL_ID,MODEL_TITLE',
        //	gridText: '모델번호,모델제목',
        //	gridWidth: '200,250',
        //	gridFlex: 'na,1,',
        //	gridSortable: '0,0',
        //	gridXtype: 'na,na',
        //	gridAlign: 'center,left'
        //} 
	    //$$(_this.id + '_grid')._updateHeader(header_column); 
	    //$$(_this.id + '_grid').refresh();
		this.doSearch();
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var _this = this;
		var form = $$(_this.id + '_searchForm');
		var paramMap = form._getValues();
		paramMap["use_yn"] = "Y";
		$$(_this.id + '_grid')._reload(paramMap);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		var _this = this;
		$$(_this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var _this = this;
		var gridItem = $$(_this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.MODEL_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.MODEL_TITLE);
					
					var values = {};
					values[fields["id"]] = id;
					values[fields["name"]] = nm;
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};

nkia.ui.popup.itam.fpModuleSelPop = function() { // 모듈 선택팝업
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - fpModuleSelPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = "/itg/workflow/fp/selectModuleSelPopList.do"; 
	this.resource = "grid.workflow.fp.fpModuleSelPop"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
		fieldItems.push({colspan:1, item: createUnionFieldComp({
		    items: [createCodeComboBoxComp({label:'검색', name:'search_type', code_grp_id: 'POPUP_SHOW_TYPE', value:'NAME', attachChoice:true }),
		            createTextFieldComp({name:"search_value", on:{onKeyPress: this.doKeyPress.bind(_this)}}),
					createBtnComp({label:"초기화", click: function() { $$("searchForm")._setValues({search_type:"NAME", search_value:"" }); } })
		           ]})}); 
		
		this.searchForm = createFormComp({
			id: _this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "모델";
		
		if(this.type == "multitask"){
			this.on = {};
		}else{
			this.on = {
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		}
		
		this.grid = createGridComp({
			id: _this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			//pageable: true,
			autoLoad :false,
			//pageSize: this.pageSize,
			keys: ["RNUM"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	    //var header_column = { 
        //	gridHeader: 'Z_MAINT_VENDOR_NM,MODULE_ID,MODULE_NM',
        //	gridText: '업체명,모듈아이디,모듈명',
        //	gridWidth: '200,120,200',
        //	gridFlex: 'na,na,1',
        //	gridSortable: '0,0,0',
        //	gridXtype: 'na,na,na',
        //	gridAlign: 'left,center,left'
        //} 
	    //$$(_this.id + '_grid')._updateHeader(header_column); 
	    //$$(_this.id + '_grid').refresh();
		this.doSearch();
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var _this = this;
		var form = $$(_this.id + '_searchForm');
		var paramMap = form._getValues();
		paramMap["use_yn"] = "Y";
		paramMap["root_class_id"] = _this.params.root_class_id;
		paramMap["filter_class_id"] = _this.params.filter_class_id;
		paramMap["up_class_id"] = _this.params.up_class_id;
		paramMap["class_level"] = _this.params.class_level;
		$$(_this.id + '_grid')._reload(paramMap);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		var _this = this;
		$$(_this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var _this = this;
		var gridItem = $$(_this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;

					var id = nkia.ui.utils.check.nullValue(gridItem.MODULE_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.MODULE_NM);
					
					var values = {};
					values[fields["id"]] = id;
					values[fields["name"]] = nm;
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};

nkia.ui.popup.itam.fpVendorSelPop = function() { // 용역업체 선택팝업
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - fpVendorSelPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;
	this.url = "/itg/workflow/fp/selectVendorSelPopList.do"; 
	this.resource = "grid.workflow.fp.fpVendorSelPop"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
		fieldItems.push({colspan:1, item: createUnionFieldComp({
		    items: [createCodeComboBoxComp({label:'검색', name:'search_type', code_grp_id: 'POPUP_SHOW_TYPE', value:'NAME', attachChoice:true }),
		            createTextFieldComp({name:"search_value", on:{onKeyPress: this.doKeyPress.bind(_this)}}),
					createBtnComp({label:"초기화", click: function() { $$("searchForm")._setValues({search_type:"NAME", search_value:"" }); } })
		           ]})}); 
		
		this.searchForm = createFormComp({
			id: _this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "모델";
		
		if(this.type == "multitask"){
			this.on = {};
		}else{
			this.on = {
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		}
		
		this.grid = createGridComp({
			id: _this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			autoLoad :false,
			pageSize: this.pageSize,
			keys: ["RNUM"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
	    //var header_column = { 
        //	gridHeader: 'VENDOR_ID,VENDOR_NM',
        //	gridText: '업체아이디,업체명',
        //	gridWidth: '200,250',
        //	gridFlex: 'na,1,',
        //	gridSortable: '0,0',
        //	gridXtype: 'na,na',
        //	gridAlign: 'center,left'
        //} 
	    //$$(_this.id + '_grid')._updateHeader(header_column); 
	    //$$(_this.id + '_grid').refresh();
		this.doSearch();
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var _this = this;
		var form = $$(_this.id + '_searchForm');
		var paramMap = form._getValues();
		paramMap["use_yn"] = "Y";
		$$(_this.id + '_grid')._reload(paramMap);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		var _this = this;
		$$(_this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var _this = this;
		var gridItem = $$(_this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.MODEL_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.MODEL_TITLE);
					
					var values = {};
					values[fields["id"]] = id;
					values[fields["name"]] = nm;
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};

nkia.ui.popup.itam.fpRsltCostPop = function() { // 용역업체 선택팝업
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - fpRsltCostPop 00 : ");
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 680;
	this.url = "/itg/workflow/fp/selectRsltCostSelPopList.do"; 
	this.resource = "grid.workflow.fp.fpRsltCostPop"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	this.callbackFunc = null;
	this.searchForm_initData = {};
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		var fieldItems = [];
		fieldItems.push({ colspan:1, item: createUnionFieldComp({
		    items: [ createSearchFieldComp({ label:'모델', name:'model_title', required:true, click:function(){
							var param = {}; 
							var searchForm_model_callback = function(data){ 
								if(data){
									;
								}
								$$(_this.id + '_grid')._removeAllRow();
							};
							nkia.ui.utils.window({
								id: "fpModelSelPop",
								type: "fpModelSelPop",
								title:'모델 선택', // 요청자 선택 
								width: 900,
								params: param,
								callbackFunc: searchForm_model_callback,
								reference:{form: $$(_this.id + '_searchForm'), fields:{id:"model_id", name:"model_title"}},
								callFunc: createFunctionPointWindow // nkia.ui.popup.itam.functionpoint.js
							});
		    			}}),
					createBtnComp({ label:"초기화", click: function() { $$(_this.id + '_searchForm')._setValues({ model_id:"", model_title:"" }); } })
		           ]})}); 
		fieldItems.push({colspan: 1, item: createDateFieldComp({name: 'calc_ym', label: '산출년월', dateType:'mm', value: new Date(), required:true, on:{ onKeyPress: function(keycode) { pressEnterKeybaseSearch(keycode);}, onChange: function(val){ $$("baseGrid")._removeAllRow(); }}})});
		fieldItems.push({colspan:1, item: createUnionFieldComp({
		    items: [createCodeComboBoxComp({label:'검색', name:'search_type', code_grp_id: 'POPUP_SHOW_TYPE', value:'NAME', attachChoice:true }),
		            createTextFieldComp({name:"search_value", on:{onKeyPress: this.doKeyPress.bind(_this)}}),
					createBtnComp({label:"초기화", click: function() { $$(_this.id + '_searchForm')._setValues({search_type:"NAME", search_value:"" }); } })
		           ]})}); 
		
		this.searchForm = createFormComp({
			id: _this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: fieldItems,
			    hiddens: {model_id:''}
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = "산출목록";
		
		if(this.type == "multitask"){
			this.on = {};
		}else{
			this.on = {
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		}
		
		this.grid = createGridComp({
			id: _this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			autoLoad :false,
			pageSize: this.pageSize,
			keys: ["RNUM"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: _this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}

	// 초기화
	this.setInit = function(){
		var _this = this;
		//var header_column = { 
	    //	gridHeader: 'CALC_YM,MODEL_ID_NM,Z_MAINT_VENDOR_NM,MODULE_ID_NM,REQ_FUNC_CNT,CALC_STATUS,INS_DT', 
	    //	gridText: '계산년월,모델명,용역업체명,모듈명,기능건수,계산상태,등록일시', 
	    //	gridWidth: '70,130,140,200,80,80,140', 
	    //	gridFlex: 'na,na,na,1,na,na,na', 
	    //	gridSortable: '1,1,1,1,1,1,1', 
	    //	gridXtype: 'na,na,na,na,na,na,na', 
	    //	gridAlign: 'center,left,left,left,right,center,center' 
		//} 
	    //$$(_this.id + '_grid')._updateHeader(header_column); 
	    //$$(_this.id + '_grid').refresh();

		nkia.ui.utils.ajax({
			url: '/itg/workflow/fp/selectCodeDataList.do',
			params: {code_grp_id: 'MODEL_BASE_Y'},
			success: function(response){
				var gridRows = response.gridVO.rows; 
				for(var i=0; i<gridRows.length; i++){
					$$(_this.id + '_searchForm')._setValues({model_id: gridRows[i].CODE_ID, model_title: gridRows[i].CODE_TEXT});
					_this.searchForm_initData = $$(_this.id + '_searchForm')._getValues();
					_this.doSearch();
					return;
				}
			}
		});
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var _this = this;
		var form = $$(_this.id + '_searchForm');
		var paramMap = form._getValues();
		if("" != nullToSpace(paramMap["calc_ym"])){
			var calc_ym = replaceAll(paramMap["calc_ym"], "-", "");
			paramMap["calc_ym"] = calc_ym;
		}
		paramMap["use_yn"] = "Y";
		paramMap["sort_column"] = "INS_DT DESC";
		$$(_this.id + '_grid')._reload(paramMap);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		var _this = this;
		$$(_this.id + '_searchForm')._reset();
		$$(_this.id + '_searchForm')._setValues(nkia.ui.utils.copyObj(_this.searchForm_initData));
		$$(_this.id + '_grid')._removeAllRow();
		debugger;
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var _this = this;
		var gridItem = $$(_this.id + "_grid").getSelectedItem();
		if(gridItem){
			if(this.reference){
				if(this.reference.fields){
					var reference = this.reference;
					var fields = reference.fields;
					
					var id = nkia.ui.utils.check.nullValue(gridItem.MODEL_ID);
					var nm = nkia.ui.utils.check.nullValue(gridItem.MODEL_TITLE);
					
					var values = {};
					values[fields["id"]] = id;
					values[fields["name"]] = nm;
					
		            reference.form._setValues(values);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(gridItem);
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};

/**
 * SW기능점수산정 팝업
 * @param {} popupProps
 */
function createFunctionPointWindow(popupProps) { // 참조 createMaintWindow
	//console.clear();
	//console.log(">>> nkia.ui.popup.itam.functionpoint.js - createFunctionPointWindow 00 : ");
	var props = popupProps||{};
	var popupComp;
	try {
		switch (props.type) {
		case "fpReqFuncRegPop":
			popupComp = new nkia.ui.popup.itam.fpReqFuncRegPop(); // 기능점수관리 팝업
			break;
		case "fpCiSelPop":
			popupComp = new nkia.ui.popup.itam.fpCiSelPop(); // 구성 선택팝업
			break;
		case "fpCiTypeSelPop":
			popupComp = new nkia.ui.popup.itam.fpCiTypeSelPop(); // 구성분류 선택팝업
			break;
		case "fpModelSelPop":
			popupComp = new nkia.ui.popup.itam.fpModelSelPop(); // 모델 선택팝업
			break;
		case "fpModuleSelPop":
			popupComp = new nkia.ui.popup.itam.fpModuleSelPop(); // 모듈 선택팝업
			break;
		case "fpVendorSelPop":
			popupComp = new nkia.ui.popup.itam.fpVendorSelPop(); // 용역업체 선택팝업
			break;
		case "fpRsltCostPop":
			popupComp = new nkia.ui.popup.itam.fpRsltCostPop(); // 계산결과 선택팝업
			break;
		default:
			alert("팝업유형이 존재하지 않습니다. ["+props.type+"] ");
			return;
		}
		popupComp.setProps(props);
		popupComp.setRender(props);
		popupComp.setWindow();
		popupComp.setInit();
	} catch(e) {
		alert(e.message);
	}
}
