/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.DeptTree = function() {
	this.url = "/itg/system/dept/searchStaticAllDeptTree.do";
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		var params = ( typeof props.params == "undefined" ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: this.url,
	        filterable: true,
	        params: params,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		var bodyScrollView = {
				view : "scrollview",
				body : {
					rows : tree
				}
			};
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if(treeItem){
			var reference = this.props.reference;
			var fields = reference.fields;
			
			var values = {};
			values[fields["id"]] = treeItem.node_id;	// 부서 ID
			values[fields["name"]] = treeItem.text;		// 부서 명 

			if(treeItem.text == "조직정보"){
				alert('최상위 노드는 선택할 수 없습니다.');
			}else{
				reference.form._setValues(values);
			}
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 부서정보가 없습니다.'
            });
        }
	};
};
/********************************************
 * DeptAuthInsert 부서관리 Webix화 ysju
 * Date: 2018-08-01
 * Version:
 ********************************************/
nkia.ui.popup.system.searchAbleTree = function() {
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		var params = ( typeof props.params == "undefined" ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/system/dept/searchStaticAllDeptTree.do',
	        header: {
	        	title: props.title
	        },
	        expColable: true,
	        filterable: true,	        
	        params: params,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: "부서정보"
	    	},	    	
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });  
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if(treeItem){
			var reference = this.props.reference;				
			var deptGrid = this.props.deptGrid;
			var deptList = deptGrid._getRows();
			var valArr = [];
			var values = {};

			values["USE_YN"] = "";
			values["RNUM"] = deptList.length+1;
			values["AUTH_CUST_NM"] = treeItem.text;		// 부서 명
			values["AUTH_CUST_ID"] = treeItem.node_id;	// 부서 ID
			values["ID"] = treeItem.id;
			values["CONCAT_KEY_STRING"] = "";
			values["deptAuthGrid_CHECK_HEADER"] = "";
			
			if(treeItem.cls=="use_y"){
				values["USE_YN_NM"] = "사용";
			}else{
				values["USE_YN_NM"] = "사용안함";
			}
			
			valArr[0] = values;
			
			if(treeItem.node_id == 0){
				alert('최상위 노드는 선택할 수 없습니다.');
			}else{
				deptGrid._addRow(valArr);
				this.window.close();			            
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 부서정보가 없습니다.'
            });
        }
	};
};
/********************************************
 * Form 담당자 관리 그룹 부서 트리 팝업
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.ChargerGrpDeptTree = function() {
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		var params = props.popupParamData.params;
		params['is_all_tree'] = "Y";
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/workflow/charger/searchStaticAllDeptTree.do',
	        expColable: true,
			filterable: true,
			checkbox: true,
			params: params,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){

		var inputData = $$('chargerGrpEditForm').getValues();
		
		var selectedRecords = $$(this.props.id + "_tree")._getCheckedItems();
		
		if(selectedRecords){
			var grp_id   = inputData.GRP_ID;
	        var node_id  = [];
	        var USER_YN  = "N";
	        
	        for(var i=0; i<selectedRecords.length; i++){
	        	node_id.push(selectedRecords[i].node_id);
	        }
	        var paramMap = {};
	        paramMap['grp_id']     = grp_id;
	        paramMap['charger_id'] = node_id;
	        paramMap['user_yn']    = USER_YN;
	
			var requestData	= paramMap;
			var url			= '/itg/workflow/charger/insertChargerGrpCharger.do';
			var callback	= function(resultData){
				if(resultData.success){
							reloadChargerGrid(false, grp_id);
							//selectTreeNode(true, true);
							nkia.ui.utils.notification({
								message: resultData.resultMsg
							});
					}else{
							nkia.ui.utils.notification({
								message: resultData.resultMsg
							});
							return;
						}
				}
			callAjax(url, requestData, callback);
			this.window.close();
		}else{
	          nkia.ui.utils.notification({
	          type: 'error',
	          message: '선택된 부서정보가 없습니다.'
	          });
	    }
		////
	};
};


/********************************************
 * Form 담당자 관리 그룹 요청유형 트리 팝업
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.searchWorkdoc = function() {
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		var params = props.popupParamData.params;
		params['is_all_tree'] = "Y";
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/workflow/workReqDoc/searchWorkReqDocListTree.do',
	        expColable: true,
			filterable: true,
			params: params,
	        expandLevel: 3,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		_this.window = createWindow({
	    	id: props.id,
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){

		//var inputData = $$('searchForm').getValues();
		
		var selectedRecords = $$(this.props.id + "_tree")._getSelectedItem();
		
		//if(selectedRecords.leaf){
			$$('searchForm')._setFieldValue("work_req_doc_nm",selectedRecords.text);
			$$('searchForm')._setFieldValue("work_req_doc_id",selectedRecords.node_id);
			this.window.close();
//		}else{
//	          nkia.ui.utils.notification({
//	          type: 'error',
//	          message: '최하위 노드를 선택해주세요.'
//	          });
//	    }
		////
	};
};

/**
 * @method {} createDeptWindow : Form Function
 * @property {string} id			: Form Id
 * @property {object} header 		: Form Header
 * @property {object} fields 		: Form Fields
 * @property {object} footer 		: Form Footer
 * @returns {object} form
 */
function createDeptWindow(popupProps) {
	var props = popupProps||{};
	var dept;
	try {
//		if(props.type){
//			// 추후 List나 다른형태를 구현할때 쓰시오.
//		}else{
//			dept = new nkia.ui.popup.system.DeptTree();
//		}
		switch(props.type){
			case('chargerGrp') :
				dept = new nkia.ui.popup.system.ChargerGrpDeptTree();
				break;
			case('deptAuthInsert') :
				dept = new nkia.ui.popup.system.searchAbleTree();
				break;
			case('workdoc') :
				dept = new nkia.ui.popup.system.searchWorkdoc();
				break;
			default : 
				dept = new nkia.ui.popup.system.DeptTree();
		}
		dept.setProps(props);
		dept.setRender(props);
	} catch(e) {
		alert(e.message);
	}
}
