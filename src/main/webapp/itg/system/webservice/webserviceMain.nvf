<!doctype html>
<html lang="ko">
<!--
*//**
* 웹서비스 관리 페이지
* @author <a href="mailto:ejjwa@nkia.co.kr">
*
*//*
-->
<head>
<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<style type="text/css">
html {overflow-x: auto; overflow-y: scroll; }
</style>

<script>

Ext.onReady(function() {
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	// 버튼 선언
	var insertWsBtn = createBtnComp({label: '#springMessage("btn.common.regist")', id: 'insertWsBtn', visible: true});
	var updateWsBtn = createBtnComp({label: '#springMessage("btn.common.modify")', id: 'updateWsBtn', visible: true});
	var deleteWsBtn = createBtnComp({label: '#springMessage("btn.common.delete")', id: 'deleteWsBtn', visible: true});
	var procWsBtn = createBtnComp({label: '웹서비스 실행', id: 'procWsBtn', visible: true, ui: 'correct'});
	
	attachBtnEvent(insertWsBtn, actionLink, 'insertWs');
	attachBtnEvent(updateWsBtn, actionLink, 'updateWs');
	attachBtnEvent(deleteWsBtn, actionLink, 'deleteWs');
	attachBtnEvent(procWsBtn, actionLink, 'procWs');
	
	// 웹서비스 목록 grid
	var setWsGridProp = {
		id: 'wsGrid',
		title: '웹서비스 목록',
		context : '${context}',
		resource_prefix: 'grid.system.webservice',
		url: '${context}/itg/system/webservice/searchWsList.do',
		toolBarComp: [insertWsBtn, updateWsBtn, deleteWsBtn, procWsBtn],
		param: null,
		gridHeight: 270,
		pagingBar: true,
		autoLoad: true
	}
	var wsGrid = createGridComp(setWsGridProp);
	
	// parameter 폼
	var setParamFormProp = {
		id: 'paramForm',
		title: 'parameter',
		editOnly: true,
		formBtns: [procWsBtn],
		tableProps: [
		             {colspan: 3, item: createTextAreaFieldComp({name: 'parameter', height: 150})}
		             , {colspan: 1, item: createTextFieldComp({name: 'webservice_id', hidden: true})}
		             , {colspan: 1, item: createTextFieldComp({name: 'url', hidden: true})}
		             , {colspan: 1, item: createTextFieldComp({name: 'trnsmis_type', hidden: true})}
		             , {colspan: 1, item: createTextFieldComp({name: 'parameter_type', hidden: true})}
		]
	}
	var paramForm = createEditorFormComp(setParamFormProp);
	
	// 웹서비스 실행 결과 폼
	var setResultFormProp = {
		id: 'resultForm',
		title: '결과',
		editOnly: true,
		colspan: 3,
		tableProps: [
		             {colspan: 3, item: createTextAreaFieldComp({name: 'result', readOnly: true, height: 150})}
		]
	}
	var resultForm = createEditorFormComp(setResultFormProp);
	
	// 웹서비스 실행 결과 이력
	var setWsResultGridProp = {
		id: 'wsResultGrid',
		title: '결과 이력 목록',
		context: '${context}',
		resource_prefix: 'grid.system.webservice.result',
		url: '${context}/itg/system/webservice/searchWsResultList.do',
		gridHeight: 270,
		param: null,
		pagingBar: true,
		autoLoad: false
	}
	var wsResultGrid = createGridComp(setWsResultGridProp);
	
	var viewportProperty = {
		center: {
			items: [wsGrid, paramForm, resultForm, wsResultGrid]
		}
	}
	createBorderViewPortComp(viewportProperty, {isLoading: true, isResize: true, resizeFunc: 'callResizeLayout'});
	
	// 이벤트 정의
	attachCustomEvent('cellclick', wsGrid, wsGridCellClick);
	attachCustomEvent('celldblclick', wsResultGrid, resultGridCellDbClick);
});

//Layout Resize Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ['wsGrid', 'paramForm', 'wsResultGrid'], 'resultForm');
}

// 웹서비스 목록 그리드 셀 클릭
function wsGridCellClick(gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {
	Ext.getCmp('resultForm').initData();
	Ext.getCmp('paramForm').initData();
	var paramMap = {webservice_id: record.raw.WEBSERVICE_ID};
	// 웹서비스 파라메터 양식 조회
	jq.ajax({
		type: "post",
		url: '/itg/system/webservice/selectWsInfo.do',
		contentType: "application/json",
		dataType: "json",
		async: false,
		data: getArrayToJson(paramMap),
		success: function(data) {
			Ext.getCmp('paramForm').setDataForMap(data.resultMap);
		}
	});
	
	//웹서비스 결과 이력 목록 새로고침
	Ext.getCmp('wsResultGrid').searchList(paramMap);
}

function actionLink(command) {
	var paramMap = {};
	var selRow = Ext.getCmp('wsGrid').getSelectedGridData();
	var selRowData = selRow[0];
	switch (command) {
		case 'procWs' :
			if (selRow == "") {
				alert("웹서비스를 선택하여 주십시오.");
				return;
			} else {
				paramMap = Ext.getCmp('paramForm').getInputData();
			}
			jq.ajax({
				type: "POST",
				url: "/itg/system/webservice/callWebserviceClient.do" ,
				contentType: "application/json",
				data: getArrayToJson(paramMap),
				async: false,
				success: function(data) {
					if(data.success) {
						Ext.getCmp('resultForm').setFieldValue('result', data.resultMap.result);
						// 웹서비스 호출이력 저장
						var map = {webservice_id: selRowData.webservice_id};
						Ext.getCmp('wsResultGrid').searchList(map);
					} else {
						alert("웹서비스 호출 중 오류가 발생하였습니다.");
					}
				},
				error: function() {
					alert("웹서비스 호출 중 오류가 발생하였습니다.");
				}
			});
		break;
		case 'deleteWs' :
			if (selRow == "") {
				alert("웹서비스를 선택하여 주십시오.");
				return;
			}
			if(!confirm('#springMessage("msg.common.confirm.00003")')){
				return;
			}
			paramMap["webservice_id"] = selRowData.webservice_id;
			jq.ajax({
				type: 'post',
				url: '/itg/system/webservice/deleteWsInfo.do',
				contentType: "application/json",
				data: getArrayToJson(paramMap),
				async: false,
				success: function(data) {
					alert(data.resultMsg);
					allFormInit();
				}
			});
		break;
		case 'insertWs' :
			var wsEditPopup = wsEditPopupPop({command: command});
			wsEditPopup.show();
			break;
		case 'updateWs' :
			if (selRow == "") {
				alert("웹서비스를 선택하여 주십시오.");
				return;
			}
			var wsEditPopup = wsEditPopupPop({command: command});
			wsEditPopup.show();
			//수정팝업에 웹서비스 정보 세팅
			setWsData(selRowData.webservice_id);
			break;
	}
}

// 팝업생성(등록/ 수정)
function wsEditPopupPop(map) {
	// 버튼 생성
	var saveBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.insert")', id: 'saveBtn', scale: 'medium', ui: 'correct'});
	
	attachBtnEvent(saveBtn, saveWsEvent, map["command"]);
	
	// 정보 에디터폼 작성
	var setWsInfoFromProp = {
			id: 'wsInfoForm',
			title: '정보',
			editOnly: true,
			columnSize: 2,
			tableProps: [
			             {colspan:2, item: createTextFieldComp({label: '웹서비스 ID', name: 'webservice_id', readOnly: true})},
			             {colspan:2, item: createTextFieldComp({label: '웹서비스 명', name: 'webservice_nm', notNull: true})},
			             {colspan:2, item: createTextFieldComp({label: 'URL', name: 'url', notNull: true})},
			             {colspan:1, item: createCodeComboBoxComp({label: '송신 시스템', name: 'req_system', code_grp_id: 'REQ_SYSTEM', attachChoice: true, notNull: true})},
			             {colspan:1, item: createCodeComboBoxComp({label: '수신 시스템', name: 'res_system', code_grp_id: 'RES_SYSTEM', attachChoice: true, notNull: true})},
			             {colspan:1, item: createCodeComboBoxComp({label:'전송 방식', name:'trnsmis_type', code_grp_id: 'TRNSMIS_TYPE', notNull: true})},
			             {colspan:1, item: createCodeComboBoxComp({label:'파라메터 타입', name:'parameter_type', code_grp_id: 'PARAMETER_TYPE', notNull: true})},
			             {colspan:2, item: createCodeComboBoxComp({label:'사용 여부', name:'use_yn', code_grp_id: 'YN', isNotColspan: true})},
			             {colspan:2, item: createTextAreaFieldComp({label: '파라메터 양식', name: 'parameter', height: 130})},
			             {colspan:2, item: createTextAreaFieldComp({label: '설명', name: 'webservice_desc'})}
			             ]
	}
	var wsInfoForm = createEditorFormComp(setWsInfoFromProp);

	var setWsEditorFormPopProp = {
			id: 'wsEditorFormPop',
			title: '웹서비스',
			width: 800,
			height: 580,
			closeBtn: true,
			buttons: [saveBtn],
			items: wsInfoForm
	};
	var wsEditorFormPop = createWindowComp(setWsEditorFormPopProp);
	return wsEditorFormPop;
}

//웹서비스 수정 팝업에서 데이터 세팅
function setWsData(webservice_id) {
	jq.ajax({
		url: "/itg/system/webservice/selectWsInfo.do",
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		async: false,
		data: getArrayToJson({webservice_id: webservice_id}),
		success: function(data) {
			Ext.getCmp('wsInfoForm').setDataForMap(data.resultMap);
		}
	});
}

// 웹서비스 저장 이벤트(insert/ update)
function saveWsEvent(command) {
	var thatForm = Ext.getCmp('wsInfoForm');
	if(!thatForm.isValid()) {
		alert('#springMessage("msg.common.00011")');
		return;
	}
	var url = '';
	switch(command) {
		case 'insertWs' :
			url = '/itg/system/webservice/insertWsInfo.do'; 
			if(!confirm('#springMessage("msg.common.confirm.00001")')){
				return;
			}
		break;
		case 'updateWs' :
			url = '/itg/system/webservice/updateWsInfo.do';
			if(!confirm('#springMessage("msg.common.confirm.00002")')){
				return;
			}
		break;
	}
	
	var paramMap = thatForm.getInputData();
	
	jq.ajax({
		type: "POST",
		url: url,
		contentType: "application/json",
		dataType: "json",
		async: false,
		data: getArrayToJson(paramMap),
		success : function(data) {
			alert(data.resultMsg);
			Ext.getCmp('wsEditorFormPop').close();
			//초기화
			allFormInit();
		}
	});
}

//웹서비스 결과 이력 목록 더블클릭
function resultGridCellDbClick(gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e){
	var wsResultPopup = wsResultPopProp();
	wsResultPopup.show();
	jq.ajax({
		url: "/itg/system/webservice/selectWsResultInfo.do",
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		async: false,
		data: getArrayToJson({result_id: record.raw.RESULT_ID}),
		success: function(data) {
			Ext.getCmp('wsResultFrom').setDataForMap(data.resultMap);
		}
	});
}

// 웹서비스 결과 상세 팝업
function wsResultPopProp(){
	var setWsResultFromProp = {
			id: 'wsResultFrom',
			title: '결과',
			editOnly: true,
			columnSize: 2,
			tableProps: [
			             {colspan:2, item: createTextFieldComp({label: '웹서비스 명', name: 'webservice_nm', readOnly: true})},
			             {colspan:1, item: createTextFieldComp({label: '실행자', name: 'user_nm', readOnly: true})},
			             {colspan:1, item: createTextFieldComp({label: '실행일시', name: 'reg_dt', readOnly: true})},
			             {colspan:2, item: createTextAreaFieldComp({label: '결과', name: 'result', height: 400, readOnly: true})},
			             ]
	}
	var wsResultFrom = createEditorFormComp(setWsResultFromProp);
	
	var setWsResultFormPopProp = {
			id: 'wsResultFormPop',
			title: '웹서비스 실행 결과',
			width: 800,
			height: 580,
			closeBtn: true,
			items: [wsResultFrom]
	};
	var wsResultFormPop = createWindowComp(setWsResultFormPopProp);
	return wsResultFormPop;
}

function allFormInit(){
	Ext.getCmp('wsGrid').searchList();
	Ext.getCmp('paramForm').initData();
	Ext.getCmp('resultForm').initData();
	Ext.getCmp('wsResultGrid').getStore().removeAll();
}

</script>
</head>
<body>
<form name="theForm" method="post">
<div id="loading-mask"></div>
<div id="loading">
<div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>


</body>

</html> 