<!doctype html>
<html lang="ko">
<!--
*//**
* 웹서비스 이력조회 페이지
* @author <a href="mailto:ejjwa@nkia.co.kr">
*
*//*
-->
<head>
<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<style type="text/css">
html {overflow-x: auto; overflow-y: scroll; }
</style>

<script>

Ext.onReady(function() {
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	var excelBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.exceldown")', id:'excelBtn',icon :'${context}/itg/base/images/ext-js/common/icons/excel-icon.png'});
	var searchBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.search")', id: 'searchBtn', ui: 'correct'});
	
	attachBtnEvent(excelBtn, actionLink,  'excelDown');
	attachBtnEvent(searchBtn, actionLink, 'searchAttr');
	
	var startDateObj = createDateTimeStamp('D', {year:0, month:0, day:-30, hour:0, min:0});
	var endDateObj = createDateTimeStamp('D', {year:0, month:0, day:0, hour:0, min:10});
	
	var setSearchFormProp = {
		id: 'searchForm',
		title: '검색',
		formBtns: [searchBtn],
		columnSize: 2,
		enterFunction: searchBtnEvent,
		tableProps: [
		    {colspan: 1, item: createCodeComboBoxComp({label: '결과', name: 'search_ws_result', id: 'search_ws_result', code_grp_id: 'WS_RESULT', attachAll: true})},
		    {colspan: 1, item: createTextFieldComp({label: '웹서비스 명', name: 'search_ws_name', id: 'search_ws_name'})},
		    {colspan: 1, item: createTextFieldComp({label: '웹서비스 메시지', name: 'search_ws_msg', id: 'search_ws_msg'})},
		    {colspan: 1, item: createFromToDateContainer({label:'웹서비스 호출 기간', name:'search_ins_dt', dateType : 'D', format: 'Y-m-d', start_dateValue: startDateObj, end_dateValue: endDateObj})}
		]
	}
	var searchForm = createSeachFormComp(setSearchFormProp);
	
	// 웹서비스 이력 그리드
	var setWsHisGridProp = {
		id: 'wsHisGrid',
		title: '웹서비스 이력 조회',
		resource_prefix: 'grid.system.webservice.his',
		url: '${context}/itg/system/webservice/selectWebserviceHisList.do',
		params: {search_ins_dt_startDate: startDateObj['day'], search_ins_dt_endDate: endDateObj['day']},
		toolBarComp: [excelBtn],
		gridHeight: 300,
		pagingBar: true,
		autoLoad: true
	}
	var wsHisGrid = createGridComp(setWsHisGridProp);
	
	// 웹서비스 실행 결과
	var setDetailFormProp = {
		id: 'detailForm',
		title: '상세내용',
		editOnly: true,
		colspan: 2,
		tableProps: [
		             {colspan: 2, item: createTextFieldComp({label: '호출일시', name: 'ins_dt', readOnly: true})},
		             {colspan: 1, item: createTextFieldComp({label: '웹서비스 명', name: 'ws_name', readOnly: true})},
		             {colspan: 1, item: createTextFieldComp({label: '실행 Method', name: 'ws_method', readOnly: true})},
		             {colspan: 1, item: createTextFieldComp({label: '결과', name: 'ws_result', readOnly: true})},
		             {colspan: 1, item: createTextFieldComp({label: '웹서비스 타입', name: 'ws_ty', readOnly: true})},
		             {colspan: 2, item: createTextAreaFieldComp({label: '웹서비스 메시지', name: 'ws_msg', readOnly: true, height: 300})},
		             {colspan: 2, item: createTextAreaFieldComp({label: '에러메시지', name: 'ws_error_msg', readOnly: true, height: 200})}
		]
	}
	var detailForm = createEditorFormComp(setDetailFormProp);
	
	var viewportProperty = {
		center: {
			items: [searchForm, wsHisGrid, detailForm]
		}
	}
	createBorderViewPortComp(viewportProperty, {isLoading: true, isResize: true, resizeFunc: 'callResizeLayout'});
	
	// 이벤트 정의
	attachCustomEvent('cellclick', wsHisGrid, gridCellClick);
});

//Layout Resize Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ['searchForm', 'wsHisGrid'], 'detailForm');
}

var searchBtnEvent = function(){
	actionLink('searchAttr');
}

// 웹서비스 목록 그리드 셀 클릭
function gridCellClick(gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {
	var detailForm = Ext.getCmp('detailForm');
	detailForm.initData();
	detailForm.setDataForMap(record.raw);
}

function actionLink(command) {
	switch(command) {
	case 'searchAttr' :
		var searchForm = Ext.getCmp('searchForm');
		var paramMap = searchForm.getInputData();
		Ext.getCmp('wsHisGrid').searchList(paramMap);
		break;
	case 'excelDown' :
		var searchForm = Ext.getCmp('searchForm');
		var paramMap = searchForm.getInputData();
		
		var excelAttrs = {
			sheet: [
				{ 
				  sheetName		: "웹서비스 이력조회", 
				  titleName		: "웹서비스 이력조회", 
				  includeColumns: "#springMessage('grid.system.webservice.his.header')",
				  headerNames	: "#springMessage('grid.system.webservice.his.text')"
				}
			]
		};
		var url = '/itg/system/webservice/searchWebserviceHisExcelDown.do';
	
		var excelParam = {};
		excelParam['fileName'] = "웹서비스 이력조회";
		excelParam['excelAttrs'] = excelAttrs;
		goExcelDownLoad(url, paramMap, excelParam);
	break;
	}
}



</script>
</head>
<body>
<form name="theForm" method="post">
<div id="loading-mask"></div>
<div id="loading">
<div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>


</body>

</html> 