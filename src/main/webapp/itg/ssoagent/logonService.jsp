<%@ page import="com.nets.sso.agent.authcheck.AuthCheck" %>
<%@ page import="com.nets.sso.common.AgentException" %>
<%@ page import="com.nets.sso.common.AgentException" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.nkia.itg.certification.service.CertificationService" %>
<%@ page import="com.nkia.itg.base.application.config.NkiaApplicationContext" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    try {
        AuthCheck auth = new AuthCheck(request, response);
        boolean logonYN = auth.logon(false);
        if(logonYN){
            //인증됨
            String userid = auth.getUserID();
            
            /*
             * 여기에 각 Application 에서 필요한 로그인 처리가 있다면 기술합니다.
             */
        }else{
        	CertificationService certificationService = (CertificationService) NkiaApplicationContext.getCtx().getBean("certificationService");
        	 String errorCode = request.getParameter("errorCode");
        	 String id = request.getParameter("returnURL");
        	 id = id.substring(id.indexOf("?"),id.length());
        	 id = id.replace("?userId=", "");
             if(errorCode.equals("11020004")){
            	String ip = request.getHeader("X-FORWARDED-FOR");
				if (ip == null) {
					ip = request.getRemoteAddr();
				}
     			// 파라미터 맵에 기한설정 셋팅
     			HashMap<String, String> insertMap = new HashMap<String, String>();
     			insertMap.put("client_ip", ip);
     			insertMap.put("login_user_id", id);
     			insertMap.put("message", "PASSWORD_ERR");
     			insertMap.put("login_type", "00002"); // 접속방법(00001 = 일반접속, 00002 =
     													// 인증서 접속)
     			insertMap.put("login_location", "0002"); // 운영자동화 로그인시 0002

     			// 접속정보ID 조회 및 접속정보 INSERT
     			certificationService.insertLoginInfo(insertMap);
             }	
        }

        /*
         * 페이지는 자동으로 이동되니, 페이지 이동은 추가로 기술하지 않습니다.
         */
    } catch (AgentException ae){}
%>
