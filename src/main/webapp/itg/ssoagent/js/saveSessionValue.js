function saveSessionValue(form, sessionValue) {
    window.indexedDB = window.indexedDB || window.msIndexedDB;
    if (!window.indexedDB) {
        console.log("Does not support IndexDB.");
        if (!form === false) {
            console.log("form submit.");
            form.submit();
        }
        return;
    }

    var request = window.indexedDB.open("NssoDatabase", 1), db, tx, store, index;

    request.onupgradeneeded = function (e) {
        var db = request.result, store = db.createObjectStore("SessionStore", {keyPath: "sv"});
    };

    request.onerror = function (e) {
        console.log("An error occurred while connecting to IndexDB. (error:" + e + ")");
        alert("IndexDB에 연결하는 동안 오류가 발생했습니다. (오류:" + e + ")");
    };

    request.onsuccess = function (e) {
        db = request.result;
        tx = db.transaction("SessionStore", "readwrite");
        store = tx.objectStore("SessionStore");
        db.onerror = function (e) {
            console.log("There was an error storing the value in IndexDB. (error:" + e + ")");
            alert("IndexDB에 값을 저장하는데 오류가 발생하였습니다. (오류:" + e + ")");
        };

        var duplicated = false;
        var sv = store.get(1);
        sv.onsuccess = function () {
            if (typeof sv.result !== 'undefined' && typeof sv.result.val !== 'undefined') {
                console.log(sv.result.val);
                if (sv.result.val === sessionValue)
                    duplicated = true;
            }
        };
        if (!duplicated)
            store.put({sv: 1, val: sessionValue});
        tx.oncomplete = function () {
            db.close();
            if (!form === false) {
                console.log("form submit.");
                form.submit();
            }
        };
    };
}