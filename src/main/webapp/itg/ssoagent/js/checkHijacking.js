﻿var Ajax = function (url, params, callback, method) {
    this.url = url;
    this.params = params;
    this.callback = callback;
    this.method = method !== null && method.length > 0 ? method : 'POST';
    this.type = 'TEXT';
    this.async = true;
    this.request = null;
    this.errHandler = null;
    this.tmpData = null;
};
Ajax.prototype.setType = function (type) {
    if (type === 'XML' || type === 'TEXT') this.type = type;
    else alert("${ajax.invalidtype}".format(type));
};
Ajax.prototype.setAsync = function (flag) {
    this.async = flag;
};
Ajax.prototype.setErrHandler = function (handler) {
    this.errHandler = handler;
};
Ajax.prototype.setTmpData = function (tmp) {
    this.tmpData = tmp;
};
Ajax.prototype.newXMLHttpRequest = function () {
    if (window.ActiveXObject) {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e1) {
            try {
                return new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e2) {
                return null;
            }
        }
    } else if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else {
        return null;
    }
};
Ajax.prototype.send = function () {
    this.request = this.newXMLHttpRequest();
    var httpMethod = this.method ? this.method : 'GET';
    if (httpMethod !== 'GET' && httpMethod !== 'POST') httpMethod = 'GET';

    var httpParams = (this.params === null || this.params === '') ? null : this.params;
    var httpUrl = this.url;
    if (httpMethod === 'GET' && httpParams !== null) httpUrl = httpUrl + "?" + httpParams;

    this.request.open(httpMethod, httpUrl, this.async);
    this.request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

    var req = this;
    this.request.onreadystatechange = function () {
        req.onStateChange.call(req);
    };
    this.request.send(httpMethod === 'POST' ? httpParams : null);

    return !this.async ? (this.type === 'XML' ? this.request.responseXML : this.request.responseText) : null;
};
Ajax.prototype.onStateChange = function () {
    if (this.async && this.request.readyState === 4) {
        if (this.request.status === 200) {
            if (this.type === 'XML') {
                if (this.tmpData === null) this.callback(this.request.responseXML);
                else this.callback(this.request.responseXML, this.tmpData);
            } else {
                if (this.tmpData === null) this.callback(this.request.responseText);
                else this.callback(this.request.responseText, this.tmpData);
            }
        } else {
            if (this.errHandler === null) {
                alert("HTTP error " + this.request.status + ": " + this.request.statusText);
            } else {
                if (this.tmpData === null) this.errHandler(this.request);
                else this.errHandler(this.request, this.tmpData);
            }
        }
    }
};

function checkHijacking(checkUrl, logoffUrl, message) {
    window.indexedDB = window.indexedDB || window.msIndexedDB;
    if (window.indexedDB) {
        var request = window.indexedDB.open("NssoDatabase", 1), db, tx, store, index;

        request.onupgradeneeded = function (e) {
            var db = request.result, store = db.createObjectStore("SessionStore", { keyPath: "sv" });
        };

        request.onerror = function (e) {
            console.log("There was an error: " + e);
        };

        request.onsuccess = function (e) {
            db = request.result;
            tx = db.transaction("SessionStore", "readwrite");
            store = tx.objectStore("SessionStore");
            db.onerror = function (e) {
                console.log("ERROR: " + e);
            };
            var sv = store.get(1);
            sv.onsuccess = function () {
                if (typeof sv.result !== 'undefined' && typeof sv.result.val !== 'undefined') {
                    console.log(sv.result.val);
                    var savedSessionValue = sv.result.val;
                    var value = "sv=" + savedSessionValue + "&dt=" + new Date().getTime();
                    var req = new Ajax(checkUrl, value, "", "POST");
                    req.async = false;
                    req.tmpData = value;
                    var responsedSV = req.send();
                    if (responsedSV !== savedSessionValue) {
                        console.log("saved:" + savedSessionValue);
                        doHijacked(message, logoffUrl);
                    }
                } else {
                    console.log('There is no sv value in indexedDB.');
                    doHijacked(message, logoffUrl);
                }
            };

            tx.oncomplete = function () {
                db.close();
            };
        };
    }
}

function doHijacked(message, logoffUrl) {
    if (message === "")
        message = "토큰이 손상되어 로그오프를 수행합니다.";
    alert(message);
    window.location.href = logoffUrl;
}