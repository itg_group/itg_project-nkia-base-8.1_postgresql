<%@ page import="com.nets.sso.agent.authcheck.AuthCheck" %>
<%@ page import="com.nets.sso.common.AgentException" %>
<%@ page import="com.nets.sso.common.Utility" %>
<%@ page import="com.nets.sso.common.enums.AuthStatus" %>
<%@ page import="java.util.Enumeration" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%
    String jsonData;
    StringBuilder sb = new StringBuilder();
    sb.append("{");
    try {
        AuthCheck authCheck = new AuthCheck(request, response);
        AuthStatus status = authCheck.checkLogon();
        sb
                .append("\"result\": true,").append("\"authStatus\": \"").append(status.toString()).append("\",").append("\"errorCode\": ").append(authCheck.getErrCode()).append(",");
        if (status == AuthStatus.SSOSuccess) {
            sb.append("\"userId\": \"").append(authCheck.getUserID()).append("\",").append("\"userAttribute\": [");
            int i = 0;
            for (Enumeration<String> e = authCheck.getUserInfoCollection().keys(); e.hasMoreElements(); ) {
                String key = e.nextElement();
                if (i > 0)
                    sb.append(",");
                sb.append("{\"key\":\"").append(key).append("\", \"value\":\"").append(authCheck.getUserInfoCollection().get(key)).append("\"}");
                i++;
            }
            sb.append("]");
        } else {
            sb.append("\"errorMessage\": \"").append(Utility.EMPTY_STRING).append("\"");
        }
    } catch (AgentException ae) {
        sb
                .append("\"result\": false,")
                .append("\"authStatus\": \"SSOFail\",")
                .append("\"errorCode\": " + ae.getExceptionCode().getValue() + ",")
                .append("\"errorMessage\": \"" + ae.getMessage() + "\"");

    }
    sb.append("}");
    jsonData = sb.toString();
%><%=jsonData%>
