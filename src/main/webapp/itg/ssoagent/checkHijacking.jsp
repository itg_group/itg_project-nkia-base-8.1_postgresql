<%@ page import="com.nets.sso.agent.authcheck.AuthCheck" %><%@ page import="com.nets.sso.common.AgentException" %><%@ page import="com.nets.sso.common.Utility" %><%@ page import="com.nets.sso.common.enums.AuthStatus" %><%
    String sv = Utility.isNullOrEmpty(request.getParameter("sv")) ? Utility.EMPTY_STRING : request.getParameter("sv");
    String tokenSessionValue = Utility.EMPTY_STRING;
    try {
        AuthCheck auth = new AuthCheck(request, response);
        AuthStatus status = auth.checkLogon();
        if (status == AuthStatus.SSOSuccess) {
            tokenSessionValue = auth.getSessionValue();
        }
    } catch (AgentException ae) {
    }
    if (tokenSessionValue.equalsIgnoreCase(sv)) {
        response.getWriter().write(tokenSessionValue);
    } else {
        response.getWriter().write("hijacked");
    }%>