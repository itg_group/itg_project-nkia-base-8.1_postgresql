﻿** 전자결재 다음 흐름에 따른 샘플 가이드 작성(2018.09.07)
작성자 molee

전자결재 최초 작성
	1. 서비스요청 유형의 등록화면에서 전자결재 호출
	2. 흐름
		1) 서비스요청 등록화면 > 등록 버튼 클릭
			A. 서비스요청 임시저장
			B. 전자결재 화면 호출(window.open)
		2) 전자결재 화면 내용 출력(API)
			A. 요청번호를 조건으로 서비스요청 내용을 조회
			B. 내용 : 요청번호, 요청자ID, 제목 등
		3) 전자결재 화면에서 > 상신 버튼 클릭
			A. 서비스요청 등록화면의 Function 호출 및 팝업 닫기
			B. 서비스요청 등록
	3. 작업
		1) 폼디자이너
			A. 서비스요청 등록화면의 버튼
				a. 임시저장 버튼 삭제
				b. 등록 버튼은 work type은 등록(전자결재)로 변경

	* 등록(전자결재) 가 확인되지 않을 경우 다음을 복사하여 추가해주고 사이트에 맞게 설정 변경
	{ 'display': '등록(전자결재)', 'text': '등록(전자결재)', 'value': 'register_appr', 'internal_id': 'register_appr', 'action': 'register_appr', 'actionType': 'direct', 'actionScript': 'SG_registerTempSrData(paramMap);', 'ui':'correct' }

			B. Schema 변경
				a. PROC_MASTER에 필요한 컬럼 추가
				b. 샘플로는 SG_APPR_NO(전자결재번호, varchar2, 1000byte), SG_APPR_STATUS(전자결재상태, varchar2, 100byte)를 추가함
		2) 소스
			A. nbpmProcess.js
				a. 등록 버튼의 actionScript 속성에 기재된 function 구현
				b. 샘플로는 SG_registerTempSrData, SG_registerSrData, SG_close가 작성되어 있음
			B. sample_if_popup.nvf
				a. 전자결재화면 샘플 추가
				b. API 조회 결과 출력 및 버튼이 간단히 작성되어 있음
			C. API 추가 소스
				a. WsSampleProvider.java
				b. WsSampleService, WsSampleServiceImpl, WsSampleDAO.java
				c. WsSample.xml
				d. sql-map-config-interfaces.xml에 WsSample.xml 추가
			D. Nbpm.xml
				a. PROC_MASTER 의 추가된 컬럼 반영
				b. id : insertProcessMaster, updateProcessMaster, selectProcessDetail
			E. SampleController.java
				a. 팝업 호출 부분 추가
				b. API 조회에 필요한 변수처리를 위해 SampleEnum.java 추가됨
				c. SampleEnum.java 파일에는 service ip, port, path 정보를 기재하게 되어 있으므로, 테스트 전 구동중인 was 의  정보를 수정해야 함

** 임시등록 후 요청을 등록하는 API 추가(2018.09.20)
작성자 molee

전자결재 흐름이 다음과 같이 변경
	1. 기존
		임시저장 - 팝업(전자결재) -- 팝업에서 opener 함수 호출 -- 팝업닫힘 - 부모창의 등록 함수 실행
	2. 변경
		임시저장 - 팝업(전자결재) -- 팝업에서 전자결재 url 호출 -- 팝업닫힘, 부모창의 탭 닫힘 -  호출된 url에서 요청을 등록하는 API 실행
다음의 파일 작업 추가
	1. nbpmProcess.js
		1) function SG_tabClose 추가 - 부모창 탭 닫기
		2) function SG_registerTempSrData 에서 workType, queryKey 를 파라미터에 추가
			A. 해당 쿼리키는 요청의 서브 테이블(PROC_SERVICE)의 INSERT/UPDATE/SELECT 쿼리문을 실행하기 위함
	2. sample_if_popup.nvf
		1) function fn_proc 추가
			A. API를 호출할 수 있는 URL 실행 및 setTimeout을 선언하여 설정한 시간텀 이후 팝업창과 탭이 닫히도록 실행
			B. Form 추가 - 요청번호와 전자결재번호, 전자결재상태 값을 전송
	3. SampleController.java
		1) updateSampleSrApprData 메소드 추가
	4. WsSampleProvider.java
		1) sendSGApprData 메소드 및 API 호출하도록 PATH 설정
		2) WsSampleManager.java의 selectTempSrData 메소드를 호출하여 임시등록된 요청 정보를 조회
		3) WsSampleManager.java의 setProcessParameter 메소드를 호출하여 엔진 실행을 위한 파라미터 셋팅
		4) WsSampleManager.java의 updateSGProcessTask 메소드를 호출하여 엔진실행