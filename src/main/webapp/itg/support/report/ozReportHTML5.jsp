<!DOCTYPE html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<%@page import="jsx3.gui.Alerts"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap" %>

<script type="text/javascript" src="/itg/support/report/js/html5/jquery-1.8.3.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="/itg/support/report/js/html5/jquery-ui.css" type="text/css"/>
<script type="text/javascript" src="/itg/support/report/js/html5/jquery-ui.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="/itg/support/report/js/html5/ui.dynatree.css" type="text/css"/>
<script type="text/javascript" src="/itg/support/report/js/html5/jquery.dynatree.js" charset="utf-8"></script>
<script type="text/javascript" src="/itg/support/report/js/html5/OZJSViewer.js" charset="utf-8"></script>
</head>
<body style="width:98%;height:98%">
<div id="OZViewer" style="width:98%;height:98%"></div>
<script type="text/javascript" >

var paramMap = {};

var query = window.location.search.substring(1);
var vars = query.split("&");
for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (typeof paramMap[pair[0]] === "undefined") {
    	paramMap[pair[0]] = decodeURIComponent(pair[1]);
    } else if (typeof paramMap[pair[0]] === "string") {
      var arr = [ paramMap[pair[0]],decodeURIComponent(pair[1]) ];
      paramMap[pair[0]] = arr;
    } else {
    	paramMap[pair[0]].push(decodeURIComponent(pair[1]));
    }
} 

var connection 	= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.Connection.Servlet")%>';
var html5codebase = '<%=NkiaApplicationPropertiesMap.getProperty("Oz.HTML5Viewer")%>';

var report_name = paramMap.report_name;
var year 		= paramMap.year;
var month 		= paramMap.month;
var sr_id 		= paramMap.sr_id;
var cust_id 	= paramMap.cust_id;
var end_month 	= paramMap.end_month;
var end_year 	= paramMap.end_year;
var search_dt 	= paramMap.search_dt;

function SetOZParamters_OZViewer(){
    var oz;
    oz = document.getElementById("OZViewer");
    oz.sendToActionScript("connection.servlet",connection);
    oz.sendToActionScript("connection.reportname",report_name);
    oz.sendToActionScript("viewer.isframe","false"); //브라우져에 임베딩하기
    oz.sendToActionScript("viewer.showerrormessage","true"); //오류메세지팝업 출력여부
    
//     var yearValue = "year="+year;
       var srIdValue = "SR_ID="+sr_id;
//     var monthValue = "month="+month;
//     var cust_idValue = "cust_id="+cust_id;
//     var end_yearValue = "end_year="+end_year;
//     var end_monthValue = "end_month="+end_month;

		var searchDt = "SEARCH_DT="+search_dt;
    
//     oz.sendToActionScript("connection"+".pcount","1");	// 파라메터 개수
//     oz.sendToActionScript("connection"+".args1",srIdValue);
//     oz.sendToActionScript("connection"+".args2",srIdValue);
//     oz.sendToActionScript("connection"+".args3",monthValue);
//     oz.sendToActionScript("connection"+".args4",cust_idValue);
//     oz.sendToActionScript("connection"+".args5",end_yearValue);
//     oz.sendToActionScript("connection"+".args6",end_monthValue);
    //2019.04.30-워드 다운로드시 글자 깨짐 방지 
    oz.sendToActionScript("word.saveasxml",true);
    oz.sendToActionScript("word.saveastable",false);
    
    switch(report_name) {
    case "MONTHLY_REPORT.ozr": {
    	oz.sendToActionScript("connection.pcount", "4");
    	// oz.sendToActionScript("connection.args1", "customer_id=" + paramMap.customer_id);
    	// oz.sendToActionScript("connection.args2", "apply_ym=" + paramMap.apply_ym);
    	oz.sendToActionScript("connection.args1", "TARGET_RESOURCE_ID=" + paramMap.TARGET_RESOURCE_ID);
    	oz.sendToActionScript("connection.args2", "SEARCH_TYPE=" + paramMap.SEARCH_TYPE);
    	oz.sendToActionScript("connection.args3", "START_DATE=" + paramMap.START_DATE);
    	oz.sendToActionScript("connection.args4", "END_DATE=" + paramMap.END_DATE);
    } break;
    case "CENTER_SUPERVISOR.ozr": {
    	oz.sendToActionScript("connection"+".pcount","1");	// 파라메터 개수
     	oz.sendToActionScript("connection"+".args1",srIdValue);
    } break;
    case "CENTER_CUSTOMER.ozr": {
    	oz.sendToActionScript("connection"+".pcount","1");	// 파라메터 개수
     	oz.sendToActionScript("connection"+".args1",srIdValue);
    } break;
    }
    
    return true;
}

start_ozjs("OZViewer",html5codebase);
</script>
</body>
</html> 