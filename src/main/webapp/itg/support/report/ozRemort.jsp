<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=7"/>
<%@page import="jsx3.gui.Alerts"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.nkia.itg.base.application.config.NkiaApplicationPropertiesMap" %>

<script language="JavaScript" src="/itg/support/report/js/ztransferx_browers.js"></script>
<script language="JavaScript" src="/itg/support/report/js/ozviewer_browers.js"></script>
<script language="JavaScript">

	var paramMap = {};
	
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
	    var pair = vars[i].split("=");
	    if (typeof paramMap[pair[0]] === "undefined") {
	    	paramMap[pair[0]] = decodeURIComponent(pair[1]);
	    } else if (typeof paramMap[pair[0]] === "string") {
	      var arr = [ paramMap[pair[0]],decodeURIComponent(pair[1]) ];
	      paramMap[pair[0]] = arr;
	    } else {
	    	paramMap[pair[0]].push(decodeURIComponent(pair[1]));
	    }
	} 
	
	var connection 	= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.Connection.Servlet")%>';
	var codebase 	= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.Viewer")%>';		//CAB파일 경로 및 버전 URL
	var server 		= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.Server")%>';	    //CAB파일 배포위한 오즈 서버경로
	var port 		= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.Port")%>';       //CAB파일 배포위한 오즈 서버포트
	var nameSpace 	= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.NameSpace")%>';	//DB 커넥션정보
	var plugInExe 	= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.Viewer")%>/OZViewerPlugIn_1018.exe';	//크롬일경우 플러그인파일의 위치 
	var plugInJpg 	= '<%=NkiaApplicationPropertiesMap.getProperty("Oz.Viewer")%>/OZViewerPlugIn.jpg';	    //크롬일경우 배포화면내에 사용자에게 보여줄 이미지 파일  

	var report_name = paramMap.report_name;
	var year 		= paramMap.year;
	var month 		= paramMap.month;
	var sr_id 		= paramMap.sr_id;
	var cust_id 	= paramMap.cust_id;
	var end_month 	= paramMap.end_month;
	var end_year 	= paramMap.end_year;
	
    function ZTInstallEndCommand_ZTransferX(param1, param2) {
        
		Create_Div();
		
		Initialize_OZViewer("OZReportViewer","CLSID:0DEF32F8-170F-46f8-B1FF-4BF7443F5F25","100%","100%","application/OZRViewer");
		Insert_OZViewer_Param("connection.servlet",connection);
		Insert_OZViewer_Param("connection.reportname",report_name);
		Insert_OZViewer_Param("viewer.isframe","false"); //브라우져에 임베딩하기
		Insert_OZViewer_Param("viewer.showerrormessage","true"); //오류메세지팝업 출력여부
		
		var yearValue = "year="+year;
        var srIdValue = "sr_id="+sr_id;
        var monthValue = "month="+month;
        var cust_idValue = "cust_id="+cust_id;
        var end_yearValue = "end_year="+end_year;
        var end_monthValue = "end_month="+end_month;
        
		Insert_OZViewer_Param("connection"+".pcount","6");
		Insert_OZViewer_Param("connection"+".args1",yearValue);
		Insert_OZViewer_Param("connection"+".args2",srIdValue);
		Insert_OZViewer_Param("connection"+".args3",monthValue);
		Insert_OZViewer_Param("connection"+".args4",cust_idValue);
		Insert_OZViewer_Param("connection"+".args5",end_yearValue);
		Insert_OZViewer_Param("connection"+".args6",end_monthValue);
		
		/*
		
		//Insert_OZViewer_Param("odi.odinames","$!{ozReportForm.detailMap.ODI_NAME}");
        // 그룹
        Insert_OZViewer_Param("GROUP_ID","$!{ozReportForm.detailMap.GROUP_ID}");
        
        // 장비
        Insert_OZViewer_Param("LOCATION_ID","$!{ozReportForm.detailMap.LOCATION_ID}");
        
        // 맵그룹
        Insert_OZViewer_Param("MAP_ID","$!{ozReportForm.detailMap.MAP_ID}");
        
        // 기간
        Insert_OZViewer_Param("START_DATE","$!{ozReportForm.detailMap.START_DATE}");
        Insert_OZViewer_Param("END_DATE","$!{ozReportForm.detailMap.END_DATE}");
        
        // 업무시간
        Insert_OZViewer_Param("START_TIME","$!{ozReportForm.detailMap.START_TIME}");
        Insert_OZViewer_Param("END_TIME","$!{ozReportForm.detailMap.END_TIME}");
        
        // TOP N
		Insert_OZViewer_Param("TOPN","$!{ozReportForm.detailMap.TOPN}");
		*/
		
		//Insert_OZViewer_Param("toolbar.xls",""); 		// xls 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.pdf",""); 		// pdf 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.doc",""); 		// doc 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.han97",""); 	// han97 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.ppt",""); 		// ppt 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.html",""); 	// html 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.csv",""); 		// csv 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.txt",""); 		// txt 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.jpg",""); 		// jpg 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.tiff",""); 	// tiff 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.svg",""); 		// svg 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.hml",""); 		// hml 아이콘을 툴바에 표시할지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.mail",""); 	// 툴바에 메일 전송 버튼을 보여줄지 여부. (true/false - default:false )
		//Insert_OZViewer_Param("toolbar.about",""); 	// 오즈 정보 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.addmemo",""); 	// 메모 추가 메뉴와 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.all",""); 		// 메뉴바와 툴바 사용 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.bgcolor",""); 	// 사용하는 색을 bgcolor의 기본값으로 사용. ( value="ffffffff" )
		//Insert_OZViewer_Param("toolbar.bottom",""); 	// 수직으로 맨 마지막 페이지로 이동하는 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.close",""); 	// 보고서 닫기 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.down",""); 	// 아래쪽 페이지로 이동하는 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.file",""); 	// 파일 메뉴 및 관련 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.find","");		// 찾기 아이콘의 활성화 여부. ( true/false - default:true)
		//Insert_OZViewer_Param("toolbar.help","");		// 리포트 뷰어 도움말 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.iconheight",""); // 아이콘의 세로 크기를 설정. ( n:1부터 숫자값 사용 가능 - default:16(단위:pixel) )
		//Insert_OZViewer_Param("toolbar.open",""); 	// 파일 오픈 메뉴와 아이콘의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.option",""); 	// 뷰어 옵션 관련 메뉴 및 아이콘 그룹의 활성화 여부. ( true/false - default:true )
		//Insert_OZViewer_Param("toolbar.page",""); 	// 뷰어 페이지 활성화 여부. ( true/false - default:false )
		//Insert_OZViewer_Param("toolbar.position",""); //오즈 뷰어에서 툴바가 표시되는 위치를 설정. ( bottom/top/left/right - default:top )

        Start_OZViewer();
    }
    
    function init() {
    	//document.popupForm.submit();
    }
    
</script>

</head>
<body onload="javascript:init();">
<form name="popupForm" method="post">

	<div id= "InstallOZViewer">
		<script language="JavaScript">			
			Initialize_ZT("ZTransferX","CLSID:C7C7225A-9476-47AC-B0B0-FF3B79D55E67","0","0","/oz/ozviewer/ZTransferX_2,2,3,7.cab#version=2,2,3,7","application/OZTransferX_1018");
			Insert_ZT_Param("download.server",server); //오즈뷰어 경로에서 포트번호만 뺀 경로
			Insert_ZT_Param("download.port",port);
			Insert_ZT_Param("download.instruction","ozrviewer.idf");
			Insert_ZT_Param("install.base","<PROGRAMS>/Forcs");
			Insert_ZT_Param("install.nameSpace",nameSpace); // 고유 이름으로 변경
			Start_ZT();
		</script>
	</div>

</form>
</body>
</html> 