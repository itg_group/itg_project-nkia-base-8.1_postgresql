<!DOCTYPE HTML>
<html>
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
<script type="text/javascript" charset="utf-8">

webix.ready(function() {
	
	var searchform = createFormComp({
		id: "searchform",
		header: {
			title: "월간 운영 보고",
			icon: "search", // 검색폼은 헤더에 아이콘을 넣어줍니다.
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchList })
				],
		        css: "webix_layout_form_bottom" // 검색폼의 Footer CSS 문제 때문에 좌측 CSS를 입력해주셔야 합니다.
			}
		},
		fields: {
			colSize: 2, // 열 수
			items: [
				{ colspan: 1, item: createDateFieldComp({ label: "검색일자", name: "apply_ym", dateType: "mm", value: new Date(), required: true }) },
				{ colspan: 1, item: createSearchFieldComp({ label: "고객사", id: "customer_nm", name: 'customer_nm', click: customerPopup, required: true })}
			], 
			hiddens: {
				customer_id: ""
			}
		}
	});

	var view = {
		view: "scrollview",
		id: "app",
		body: {
			rows: [{
				cols: [{
					rows: new Array().concat(searchform)
				}]
			}]
		}
	};
	
	nkia.ui.render(view);
});

function customerPopup() {
	if(!($$("popupWindow"))) {
		var findform = createFormComp({
			id: "findform",
			header: {
				title: "고객사 검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: searchReportGroup })
					],
			        css: "webix_layout_form_bottom" // 검색폼의 Footer CSS 문제 때문에 좌측 CSS를 입력해주셔야 합니다.
				}
			},
			fields: {
				colSize: 1, // 열 수
				items: [
					{ colspan: 1, item: createTextFieldComp({ label: "고객사 검색", id: "search_customer", name: 'search_customer', required: false, on: { onKeyPress: searchReportGroupByKey }})}
				]
			}
		});
		
		var grid = createGridComp({
			id : 'grid',
			header : {
				title: "고객사 선택"
			},
			resizeColumn : true,
			pageable : true,
			pageSize : 20,
			checkbox : false,
			autoLoad : true, 
			url : "/itg/system/customer/selectEmsReportGroupList.do",
			resource : "grid.support.report.reportgroup", 
			on : {
				onItemDblClick: selectReportGroup
			}
		});
		
		var popupWindow = createWindow({
	    	id: "popupWindow",    	
	    	width: 700, 
	    	height: nkia.ui.utils.getWidePopupHeight(), 
	    	header: {
	    		title: "고객사 선택"
	    	},
	    	body: {
	    		rows: [{
					cols: [{
						rows: new Array().concat(findform, grid)
					}]
				}]
	    	},
	    	footer : {
	    		buttons : {
	    			align: "center",
	     	        items: [
	     	        	createBtnComp({label: '선택', type:'form', id:'selectReportGroup', click: selectReportGroup})
	     	        ]
	    		} 	        
	 	    },
	    	closable: true
	    });
	 	
		$$("popupWindow").show();
	}
}

function searchReportGroupByKey(keyCode) {
	if(keyCode && keyCode == 13) {
		searchReportGroup();
	}
}

function searchReportGroup() {
	var paramMap = $$("findform").getValues();
	
	$$("grid")._reload(paramMap);
}

function selectReportGroup() {
	var item = $$("grid").getSelectedItem();
	
	if(!item) {
		nkia.ui.utils.notification({
			type: "error",
			message: "고객사를 선택하세요."
		});
		return false;
	}
	
	$$("searchform")._setValues({
		customer_nm: item.NAME, 
		customer_id: item.ID
	});
	
	$$("popupWindow").close();
}

function searchList(){
	if($$("searchform")._validate()) {
		var data = $$("searchform")._getValues();
		data.apply_ym = data.apply_ym.replace(/(-)/gi, "");
		
		openReportPopup(data);		
	}
}

//리포트 오픈 팝업
function openReportPopup(data){
	var reportName = "MONTHLY_REPORT.ozr";
	var param = $$("searchform")._getValues();
	
	var apply_year = param["apply_ym"].substring(0, 4);
	var apply_month = param["apply_ym"].substring(5, 7);
	var apply_date = "01";
	
	var start_date = new Date(apply_year + "-" + fillZero(parseInt(apply_month), 2) + "-" + apply_date);
	start_date.setHours(0);
	start_date.setMinutes(0);
	start_date.setSeconds(0);
	start_date.setMilliseconds(0);
	var end_date = new Date(apply_year + "-" + fillZero(parseInt(apply_month), 2) + "-" + apply_date);
	end_date.setMonth(end_date.getMonth() + 1);
	end_date.setDate(end_date.getDate() - 1);
	end_date.setHours(23);
	end_date.setMinutes(59);
	end_date.setSeconds(59);
	end_date.setMilliseconds(999);
	
	data["TARGET_RESOURCE_ID"] = param["customer_id"];
	data["SEARCH_TYPE"] = "SLMS";
	/* 
	 * data["START_DATE"] = getFullDateString(start_date);
	 * data["END_DATE"] = getFullDateString(end_date);
	 */
	data["START_DATE"] = start_date.getTime();
	data["END_DATE"] = end_date.getTime();
	
	createReportPopup(reportName, data);
}

function fillZero(value, length) {
	if(("" + value).length >= length) return value;
	else return fillZero("0" + value, length);
}

function getFullDateString(date) {
	return date.getFullYear() + "-" + 
		   fillZero(date.getMonth() + 1, 2) + "-" + 
		   fillZero(date.getDate(), 2) + " " + 
		   fillZero(date.getHours(), 2) + ":" + 
		   fillZero(date.getMinutes(), 2) + ":" + 
		   fillZero(date.getSeconds(), 2) + "." + 
		   fillZero(date.getMilliseconds(), 3);
}

</script>
</body>
</html>		
