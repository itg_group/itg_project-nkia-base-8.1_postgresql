<!DOCTYPE HTML>
<html>
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
<script type="text/javascript" charset="utf-8">

var date_default;

webix.ready(function() {
	
	date_default = new Date();
	date_default = {
		day : date_default.getFullYear() + "-" + (date_default.getMonth() + 1) + "-" + date_default.getDate(),
		month : date_default.getFullYear() + "-" + (date_default.getMonth() + 1),
		hour : "00",
		min : "00",
		date : date_default.getFullYear() + "-0" + (date_default.getMonth()+1) + "-" + date_default.getDate() + " 00:00"
	};
	
	var searchform = createFormComp({
		elementsConfig: {
			labelPosition: "left",
			labelWidth: 100
		},
		id: "searchform",
		header: {
			title: "만족도 통계 검색",
			icon: "search", // 검색폼은 헤더에 아이콘을 넣어줍니다.
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchSatisfactionStatistics })
				],
		        css: "webix_layout_form_bottom"
			}
		},
		fields: {
			colSize: 2, // 열 수
			items: [
				{ colspan: 1, item :createCodeComboBoxComp({label: '조회조건', id: 'search_dt_type', name: 'search_dt_type', code_grp_id: 'SEARCH_DT_TYPE', value: "MONTH", on: { onChange: setDtType }})}, 
				{ colspan: 1, item: createCodeComboBoxComp({ label: "구분", id: "req_doc_id", name: "req_doc_id", value: "CD0000000154", attachChoice: false, required: true }) }, 
				{ colspan: 1, item: createUnionFieldComp({
					items: [
						createUnionFieldComp({
	                    	items: [
	                            createLabelComp({ label: "월별", id : 'dummy_m', name: 'dummy_m', width : 100 }),
	                			createSearchDateFieldComp({ id: "searchDate_m", name: "searchDate_m", dateType: "mm", required: false, start_dateValue: date_default, end_dateValue: date_default })
	                        ]
	                    }), 
	                    createUnionFieldComp({
	                    	items: [
	                            createLabelComp({ label: "일별", id : 'dummy_d', name: 'dummy_d', width : 100 }),
	                			createSearchDateFieldComp({ id: "searchDate_d", name: "searchDate_d", dateType: "D", required: false, start_dateValue: date_default, end_dateValue: date_default })
	                        ]
	                    })
					]
				}) }, 
				{ colspan: 1, item: createTextFieldComp({ name: "dummy", on: { onAfterRender: function() { this.$setSize(this.$width, 0) }}}) }
			]
		}
	});
	
	var satisfaction_statistics_grid = createGridComp({
		id : 'grid',
		header : {
			title: "만족도 통계 조회 결과", 
			buttons: {
				items: [
					createBtnComp({ id:"printExcel", label:"엑셀다운로드", width: "100", type: "form", click: satisfactionStatisticsExcelDownload })
				]
			}
		},
		resizeColumn : true,
		pageable : true,
		pageSize : 50,
		checkbox : false,
		autoLoad : false, 
		url : "/itg/support/report/selectSatisfactionStatisticsList.do",
		resource : "grid.support.report.satisfactionstatistics", 
		on : {
			onItemDblClick: searchSatisfactionStatisticsDetail
		}
	});

	var view = {
		view: "scrollview",
		id: "app",
		body: {
			rows: [{
				cols: [{
					rows: new Array().concat(searchform, satisfaction_statistics_grid)
				}]
			}]
		}
	};
	
	nkia.ui.render(view);
	$$("dummy_d").hide();
	$$("searchDate_d").hide();
	
	$$("req_doc_id").config.options.push({ id: "CD0000000154", title: "(롯데)그룹사용 ", value: "(롯데)그룹사용 " });
	$$("req_doc_id").config.options.push({ id: "CD0000000163", title: "대외 고객사용", value: "대외 고객사용" });
	$$("req_doc_id").refresh();
});

function setDtType() {
	if($$("search_dt_type").getValue() == "DAY") {
		$$("dummy_m").hide();
		$$("searchDate_m").hide();
		$$("dummy_d").show();
		$$("searchDate_d").show();
	} else {
		$$("dummy_d").hide();
		$$("searchDate_d").hide();
		$$("dummy_m").show();
		$$("searchDate_m").show();
	}
}

function searchSatisfactionStatistics(){
	if($$("searchform")._validate()) {
		var data = $$("searchform")._getValues();
		
		if($$("search_dt_type") && $$("search_dt_type").getValue() == "MONTH") {
			if(data.searchDate_m_startDate == "" || data.searchData_m_endDate == "") {
				nkia.ui.utils.notification({
					type: "error", 
					message: "기간 조회조건을 선택해주세요."
				});
				return false;
			}
			
			var searchDate = new Date();
			var year = null;
			var month = null;
			var date = null;
			
			searchDate.setFullYear(data.searchDate_m_startDate.substring(0, 4));
			searchDate.setMonth(data.searchDate_m_startDate.substring(5, 7) - 1);
			searchDate.setDate("01");
			
			year = searchDate.getFullYear();
			month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
			date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
			
			data.searchDate_startDate = year + "-" + month + "-" + date;
			
			searchDate.setFullYear(data.searchDate_m_endDate.substring(0, 4));
			searchDate.setMonth(data.searchDate_m_endDate.substring(5, 7) - 1);
			searchDate.setMonth(searchDate.getMonth() + 1);
			searchDate.setDate("01");
			searchDate.setDate(searchDate.getDate() - 1);
			
			year = searchDate.getFullYear();
			month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
			date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
			
			data.searchDate_endDate = year + "-" + month + "-" + date;
			
			$$("searchform").config.params = data;
		} else {
			if(data.searchDate_d_startDate == "" || data.searchData_d_endDate == "") {
				nkia.ui.utils.notification({
					type: "error", 
					message: "기간 조회조건을 선택해주세요."
				});
				return false;
			}
			
			data.searchDate_startDate = data.searchDate_d_startDate;
			data.searchDate_endDate = data.searchDate_d_endDate;
		}
		
		$$("grid")._reload(data);
	}
}

function searchSatisfactionStatisticsDetail(id, e) {
	var searchform = $$("searchform").getValues();
	var data = $$("grid")._getRows()[$$("grid").getIndexById(id.row)];
	var mid_doc_id = ["CD0000000155", "CD0000000160", "CD0000000180", "CD0000001072", "CD0000001074"];
	var lv = data.DOC_ID;
	var lv_nm = data.STATIS_RESULT_NM;
	var clickedColumn = id.column;
	
	console.log("data: ");
	console.log(data);
	console.log("id: " + id);
	console.log("lv: " + lv + "(" + lv_nm + ")");
	console.log("clickedColumn: " + clickedColumn);
	
	if($$("search_dt_type") && $$("search_dt_type").getValue() == "MONTH") {
		if(data.searchDate_m_startDate == "" || data.searchData_m_endDate == "") {
			nkia.ui.utils.notification({
				type: "error", 
				message: "기간 조회조건을 선택해주세요."
			});
			return false;
		}
		
		var searchDate = new Date();
		var year = null;
		var month = null;
		var date = null;
		
		searchDate.setFullYear(searchform.searchDate_m_startDate.substring(0, 4));
		searchDate.setMonth(searchform.searchDate_m_startDate.substring(5, 7) - 1);
		searchDate.setDate("01");
		
		year = searchDate.getFullYear();
		month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
		date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
		
		searchform.searchDate_startDate = year + "-" + month + "-" + date;
		
		searchDate.setFullYear(searchform.searchDate_m_endDate.substring(0, 4));
		searchDate.setMonth(searchform.searchDate_m_endDate.substring(5, 7) - 1);
		searchDate.setMonth(searchDate.getMonth() + 1);
		searchDate.setDate("01");
		searchDate.setDate(searchDate.getDate() - 1);
		
		year = searchDate.getFullYear();
		month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
		date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
		
		searchform.searchDate_endDate = year + "-" + month + "-" + date;
		
		$$("searchform").config.params = data;
	} else {
		if(data.searchDate_d_startDate == "" || data.searchData_d_endDate == "") {
			nkia.ui.utils.notification({
				type: "error", 
				message: "기간 조회조건을 선택해주세요."
			});
			return false;
		}
		
		searchform.searchDate_startDate = searchform.searchDate_d_startDate;
		searchform.searchDate_endDate = searchform.searchDate_d_endDate;
	}
	
	if(!($$("popupWindow")) && getMidDocNm(clickedColumn) && data.DOC_ID != "CD9999999RAT") {
		var popupGrid = createGridComp({
			id : "popupGrid",
			pageable : true,
			pageSize : 10,
			checkbox : false,
			autoLoad : true, 
			url : "/itg/support/report/selectSatisfactionStatisticsDetail.do",
			resource : "grid.support.report.satisfactionstatisticsdetail",
			params: {
				searchDate_startDate: searchform.searchDate_startDate, 
				searchDate_endDate: searchform.searchDate_endDate, 
				req_doc_id: searchform.req_doc_id, 
				statis_result: getStatisResultId(clickedColumn), 
				mid_doc_id: data.DOC_ID != "CD9999998TOT"? data.DOC_ID: ""
			},
			header : {
				title : "만족도 통계 조회 상세 (" + getMidDocNm(clickedColumn) + ")", 
				buttons: {
					items: [
						createBtnComp({ id:"printExcel", label:"엑셀다운로드", width: "100", type: "form", click: function() { satisfactionStatisticsDetailExcelDownload(getStatisResultId(clickedColumn), data.STATIS_RESULT_NM, data.DOC_ID); }})
					]
				}
			}
		});
		
		var popupWindow = createWindow({
	    	id: "popupWindow",
	    	width: 800,
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: "만족도 통계 조회 상세 - " + data.STATIS_RESULT_NM
	    	},
	    	body: {
	    		rows: new Array().concat(popupGrid)
	    	},
	    	closable: true
	    });
		
		// 팝업창 오픈
		$$("popupWindow").show();
	}
}

function satisfactionStatisticsExcelDownload() {
	if(!($$("searchform")._validate())) {
		return false;
	}
	
	var searchform = $$("searchform")._getValues();
	searchform.page = 1;
	searchform.start = 0;
	searchform.limit = 1000000;
	searchform.type = "list";
	var typeArr = [];
	var sheet = [];
	
	if($$("search_dt_type") && $$("search_dt_type").getValue() == "MONTH") {
		if(searchform.searchDate_m_startDate == "" || searchform.searchData_m_endDate == "") {
			nkia.ui.utils.notification({
				type: "error", 
				message: "기간 조회조건을 선택해주세요."
			});
			return false;
		}
		
		var searchDate = new Date();
		var year = null;
		var month = null;
		var date = null;
		
		searchDate.setFullYear(searchform.searchDate_m_startDate.substring(0, 4));
		searchDate.setMonth(searchform.searchDate_m_startDate.substring(5, 7) - 1);
		searchDate.setDate("01");
		
		year = searchDate.getFullYear();
		month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
		date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
		
		searchform.searchDate_startDate = year + "-" + month + "-" + date;
		
		searchDate.setFullYear(searchform.searchDate_m_endDate.substring(0, 4));
		searchDate.setMonth(searchform.searchDate_m_endDate.substring(5, 7) - 1);
		searchDate.setMonth(searchDate.getMonth() + 1);
		searchDate.setDate("01");
		searchDate.setDate(searchDate.getDate() - 1);
		
		year = searchDate.getFullYear();
		month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
		date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
		
		searchform.searchDate_endDate = year + "-" + month + "-" + date;
		
		$$("searchform").config.params = searchform;
	} else {
		if(searchform.searchDate_d_startDate == "" || searchform.searchData_d_endDate == "") {
			nkia.ui.utils.notification({
				type: "error", 
				message: "기간 조회조건을 선택해주세요."
			});
			return false;
		}
		
		searchform.searchDate_startDate = searchform.searchDate_d_startDate;
		searchform.searchDate_endDate = searchform.searchDate_d_endDate;
	}
	
	nkia.ui.utils.ajax({
		async: false, 
		url: "/itg/support/report/selectSatisfactionStatisticsList.do", 
		params: searchform, 
		isMask: false, 
		success: function(response) {
			if(response.gridVO && response.gridVO.rows && response.gridVO.rows.length > 0) {
				var includeColumns = "STATIS_RESULT_NM,A1_CNT,A2_CNT,A3_CNT,A4_CNT,A5_CNT,TOT_CNT,TOT_RATE";
				var headerNames = "평가점수,매우 불만족,불만족,보통,만족,매우 만족,건수합계,비율";
				var htmlTypes = "TEXT,TEXT,TEXT,TEXT,TEXT,TEXT,TEXT,TEXT";
				var popupShowTypes = "NA,NA,NA,NA,NA,NA,NA,NA";
				
				headerNames = headerNames.replace(/<[^>]*>/g, '');
				
				var sheetArr = {
					"sheetName": "만족도 통계", 
					"titleName": "만족도 통계 조회 (" + $$("req_doc_id")._getText().trim() + ", " + 
							(searchform.searchDate_startDate == searchform.searchDate_endDate? 
									searchform.searchDate_startDate: 
									searchform.searchDate_startDate + " ~ " + searchform.searchDate_endDate) + ")", 
					"headerNames": headerNames, 
					"includeColumns": includeColumns, 
					"htmlTypes": htmlTypes, 
					"popupShowTypes": popupShowTypes
				};
				sheet.push(sheetArr);
				
				var excelAttrs = { "sheet": sheet };
				var paramMap = searchform;
				
				var getResultUrl = "/itg/support/report/selectSatisfactionStatisticsExcel.do";
				
				var excelParam = {};
				excelParam["fileName"] = "만족도_통계_조회";
				excelParam["excelAttrs"] = excelAttrs;
				goExcelDownLoad(getResultUrl, paramMap, excelParam);
			} else {
				nkia.ui.utils.notification({
					type: 'error',
					message: "출력되는 정보가 없습니다."
				});
				return;
			}
		}
	});
}

function satisfactionStatisticsDetailExcelDownload(lv, statis_result_nm, mid_doc_id) {
	if(!($$("searchform")._validate())) {
		return false;
	}
	
	var searchform = $$("searchform")._getValues();
	searchform.page = 1;
	searchform.start = 0;
	searchform.limit = 1000000;
	searchform.type = "detail";
	searchform.statis_result = lv;
	searchform.mid_doc_id = mid_doc_id != "CD9999998TOT"? mid_doc_id: "";
	var typeArr = [];
	var sheet = [];
	
	if($$("search_dt_type") && $$("search_dt_type").getValue() == "MONTH") {
		if(searchform.searchDate_m_startDate == "" || searchform.searchData_m_endDate == "") {
			nkia.ui.utils.notification({
				type: "error", 
				message: "기간 조회조건을 선택해주세요."
			});
			return false;
		}
		
		var searchDate = new Date();
		var year = null;
		var month = null;
		var date = null;
		
		searchDate.setFullYear(searchform.searchDate_m_startDate.substring(0, 4));
		searchDate.setMonth(searchform.searchDate_m_startDate.substring(5, 7) - 1);
		searchDate.setDate("01");
		
		year = searchDate.getFullYear();
		month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
		date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
		
		searchform.searchDate_startDate = year + "-" + month + "-" + date;
		
		searchDate.setFullYear(searchform.searchDate_m_endDate.substring(0, 4));
		searchDate.setMonth(searchform.searchDate_m_endDate.substring(5, 7) - 1);
		searchDate.setMonth(searchDate.getMonth() + 1);
		searchDate.setDate("01");
		searchDate.setDate(searchDate.getDate() - 1);
		
		year = searchDate.getFullYear();
		month = searchDate.getMonth().toString().length == "2"? (searchDate.getMonth() + 1): "0" + (searchDate.getMonth() + 1);
		date = searchDate.getDate().toString().length == "2"? searchDate.getDate(): "0" + searchDate.getDate();
		
		searchform.searchDate_endDate = year + "-" + month + "-" + date;
	} else {
		if(data.searchDate_d_startDate == "" || data.searchData_d_endDate == "") {
			nkia.ui.utils.notification({
				type: "error", 
				message: "기간 조회조건을 선택해주세요."
			});
			return false;
		}
		
		searchform.searchDate_startDate = searchform.searchDate_d_startDate;
		searchform.searchDate_endDate = searchform.searchDate_d_endDate;
	}
	
	nkia.ui.utils.ajax({
		async: false, 
		url: "/itg/support/report/selectSatisfactionStatisticsDetail.do", 
		params: searchform, 
		isMask: false, 
		success: function(response) {
			if(response.gridVO && response.gridVO.rows && response.gridVO.rows.length > 0) {
				var includeColumns = "SR_ID,REQ_DOC_NM,TITLE,CENTER_NM,CUSTOMER_NM,SERVICE_TRGET_CUSTOMER_NM,REQ_USER_NM,STATIS_RESULT_NM,STATIS_CONTENT,REG_DT";
				var headerNames = "요청번호,세부유형,제목,센터,고객사,서비스대상,요청자,평가등급,평가내용,등록일자";
				var htmlTypes = "TEXT,TEXT,TEXT,TEXT,TEXT,TEXT,TEXT,TEXT,TEXT,TEXT";
				var popupShowTypes = "NA,NA,NA,NA,NA,NA,NA,NA,NA,NA";
				
				headerNames = headerNames.replace(/<[^>]*>/g, '');
				
				var sheetArr = {
					"sheetName": "만족도 통계 상세 (" + statis_result_nm + ", " + getMidDocNm(lv) + ")", 
					"titleName": "만족도 통계 상세 조회 (" + statis_result_nm + ", " + getMidDocNm(lv) + ", " + 
							(searchform.searchDate_startDate == searchform.searchDate_endDate? 
									searchform.searchDate_startDate: 
									searchform.searchDate_startDate + " ~ " + searchform.searchDate_endDate) + ")", 
					"headerNames": headerNames, 
					"includeColumns": includeColumns, 
					"htmlTypes": htmlTypes, 
					"popupShowTypes": popupShowTypes
				};
				sheet.push(sheetArr);
				
				var excelAttrs = { "sheet": sheet };
				var paramMap = searchform;
				
				var getResultUrl = "/itg/support/report/selectSatisfactionStatisticsExcel.do";
				
				var excelParam = {};
				excelParam["fileName"] = "만족도_통계_상세_조회";
				excelParam["excelAttrs"] = excelAttrs;
				goExcelDownLoad(getResultUrl, paramMap, excelParam);
			} else {
				nkia.ui.utils.notification({
					type: 'error',
					message: "출력되는 정보가 없습니다."
				});
				return;
			}
		}
	});
}

function getStatisResultId(column) {
	switch(column) {
	case "A1_CNT": return "1";
	case "A2_CNT": return "2";
	case "A3_CNT": return "3";
	case "A4_CNT": return "4";
	case "A5_CNT": return "5";
	
	default: return "";
	}
}

function getMidDocNm(column) {
	switch(column) {
	case "1": return "매우 불만족";
	case "2": return "불만족";
	case "3": return "보통";
	case "4": return "만족";
	case "5": return "매우 만족";
	
	case "A1_CNT": return "매우 불만족";
	case "A2_CNT": return "불만족";
	case "A3_CNT": return "보통";
	case "A4_CNT": return "만족";
	case "A5_CNT": return "매우 만족";
	
	case "CD0000000155": return "매우 불만족";
	case "CD0000000160": return "불만족";
	case "CD0000000180": return "보통";
	case "CD0000001072": return "만족";
	case "CD0000001074": return "매우 만족";
	
	default: return "";
	}
}

</script>
</body>
</html>		
