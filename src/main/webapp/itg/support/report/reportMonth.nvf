<!doctype html>
<html lang="ko">
<!--
 /**
 * 월간보고서조회
 * 2013.3.25
 * @author <a href="mailto:minsoo@nkia.co.kr"> 
 * 
 */
-->
<head>
<style type="text/css">

	html {overflow-x: auto; overflow-y: scroll; }

</style>

<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")

<script>
Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-3, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});	
	
	var reportSearchBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.search")', id:'reportSearchBtn', "ui":"correct"});
	var reportInitSearchBtn = createBtnComp({visible: true, label: '#springMessage("btn.system.00003")', id:'reportInitSearchBtn'});

	var reportType = createCodeComboBoxComp({label:'#springMessage("res.report.label.00001")', id:'report_type', name:'report_type', code_grp_id:'REPORT_TYPE', width: 220})
	attachBtnEvent(reportSearchBtn, reportActionLink,  'search');	
	attachBtnEvent(reportInitSearchBtn, reportActionLink,  'searchInit');	
	
	Ext.getCmp('report_type').on('select', comboLink);
	
	var setReportSearchFormProp = {
		id: 'searchReportForm', 
		formBtns: [reportSearchBtn, reportInitSearchBtn],
		columnSize: 2,
		enterFunction : searchReportBtnEvent,
		tableProps : [
						//{colspan:1, item : createSearchDateFieldComp({label:'#springMessage("res.label.itam.lifecycle.req.00014")', id : 'monthDate' , name:'search', dateType : 'M', format: 'Ym', start_dateValue : startDateObj, end_dateValue: endDateObj})},
						{colspan:1, item :createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate' , name:'search', start_dateValue : startDateObj, end_dateValue: endDateObj, notNull:false, dateType : 'D', format: 'Y-m', submitFormat : 'Ym'})},
						{colspan:1, tdHeight: 28,item : contractComboBox()}
					 ]
	}
	var searchReportForm = createSeachFormComp(setReportSearchFormProp);
	
	var searchForm = Ext.getCmp('searchReportForm');
	var paramMap = searchForm.getInputData();
	//paramMap['report_type'] = "ITAM";
	paramMap['report_type'] = "INCDNT";
	
	var reportListProp = {	
		context: '${context}',						
		id: 'reportList',
		toolBarComp : [reportType],
		title : '#springMessage("res.report.label.00002")',
		resource_prefix: 'grid.support.report',				
		url: '${context}/itg/itam/report/searchReportList.do',	
		params: paramMap,								
		pagingBar : true,
		pageSize : 35,
		autoLoad: false,
		border: false,
		emptyMsg: '#springMessage("msg.asset.tableMng.00010")'
	}
	var reportList = createGridComp(reportListProp);
	
	// Viewport 설정
	var viewportProperty = {
		center: { 
			items : [searchReportForm, reportList]
		}
	}
	
	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	
	attachCustomEvent('itemdblclick', reportList, gridRowClick);
	
	Ext.getCmp('report_type').getStore().on('load', function(store){
		var record = store.getAt(0);
		setVisibleCust(record.data.CODE_ID);
		reportActionLink('search');
	});
	
	
});

//Layout Resise Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["searchReportForm"], "reportList");
}

/**
 * 검색 폼에서 text에서 엔터키 칠때 호출 
 */
function searchReportBtnEvent(){
	reportActionLink('search');
}



/**
 * Grid를 선택하였을 경우 실행한다.
 */
function gridRowClick( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {	
	
	var report_type = Ext.getCmp('report_type').value;
	
	var param = {};
	param["year"] = tdEl.raw.YEAR;
	param["month"] = tdEl.raw.MONTH;

	if(report_type == 'ITAM'){
		createReportPopup("itam_monthly_report.ozr", param);
	}else if(report_type == 'SLMS'){
		var cust_id = Ext.getCmp('CUST_ID').value;
		param["cust_id"] = cust_id;
		createReportPopup("service_monthly_report.ozr", param);
	}else if(report_type == 'PROC'){
		createReportPopup("proc_monthly_report.ozr", param);
	}else if(report_type == 'INCDNT'){
		createReportPopup("incdnt_report_ING.ozr", param);
	}
}


/**
 * 버튼 이벤트를 정의한다.
 */
function reportActionLink(command) {
	switch(command) {
		case 'search' :
			var searchForm = Ext.getCmp('searchReportForm');
			var paramMap = searchForm.getInputData();
			
			var endYear = paramMap.search_endDate.substring(0,4);
			var endMonth = paramMap.search_endDate.substring(4,6);
			var newEndDate =(endYear-1) + endMonth;
			
			// 날짜 데이터 체크
			if(paramMap.search_startDate > paramMap.search_endDate){
				showMessage("#springMessage('msg.slms.monitoring.endDateCheck')");
				
				return false;
			}
			
			// 조회버튼 클릭시 기간이 1년 미만이 되도록 합니다.
			if(newEndDate >= paramMap.search_startDate){
				showMessage("#springMessage('msg.slms.monitoring.dateTermCheck')");
				
				return false;
			}
			Ext.getCmp('reportList').searchList(paramMap);	
		break;
		case 'searchInit' :
			var searchForm = Ext.getCmp('searchReportForm').initData();
		break;
	}
}
/**
 * 대상부서 콤보박스 생성 
 * @returns
 */
function contractComboBox(){
	var param = {};
	var contract_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/slms/common/searchCustCombo.do',
		//url: '/itg/slms/rawdata/searchContractCodeList.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var contractProp_val = {label:getConstText({isArgs: true, m_key: 'res.00023'}), id: 'CUST_ID', name:'CUST_ID', width: 300};
		contractProp_val['proxy'] = contract_proxy;
		contractProp_val['valueField'] = 'CODE_ID';
	    contractProp_val['displayField'] = 'CODE_TEXT';
		//contractProp_val['valueField'] = 'CUST_ID';
	    //contractProp_val['displayField'] = 'CUST_NM';
	    
	    
	var contractComboBox =  createComboBoxComp(contractProp_val);
	
	//Ext.getCmp('CUST_ID').setVisible(false);
	//var report_type = Ext.getCmp('report_type').value;
	//setVisibleCust(report_type);
	
    return contractComboBox;
}

function comboLink(command){

	var report_type = Ext.getCmp('report_type').value;
	
	switch(report_type) {
		case 'ITAM':
			var searchForm = Ext.getCmp('searchReportForm');
			var paramMap = searchForm.getInputData();
			paramMap['report_type'] = "ITAM";
			Ext.getCmp('reportList').searchList(paramMap);	
			
			setVisibleCust(report_type);
			
		break;
		case 'PROC':
			var searchForm = Ext.getCmp('searchReportForm');
			var paramMap = searchForm.getInputData();
			paramMap['report_type'] = "PROC";		
			Ext.getCmp('reportList').searchList(paramMap);	
			
			setVisibleCust(report_type);
			
		break;
		case 'SLMS':
			var searchForm = Ext.getCmp('searchReportForm');
			var paramMap = searchForm.getInputData();
			paramMap['report_type'] = "SLMS";		
			Ext.getCmp('reportList').searchList(paramMap);	
			
			setVisibleCust(report_type);
			
		break;
	}
};

function setVisibleCust(report_type){
	
	switch(report_type) {
	case 'SLMS':
		if (!Ext.getCmp('CUST_ID').isVisible()) {
			Ext.getCmp('CUST_ID').setVisible(true);
			callResizeLayout();
		} 
	break;
	default :
		if (Ext.getCmp('CUST_ID').isVisible()) {
			Ext.getCmp('CUST_ID').setVisible(false);
			callResizeLayout();
		}
	break;
	}
}

</script>
</head>
<body>
<form name="theForm" method="post">

<div id="loading-mask"></div>
<div id="loading">
	<div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>
</form>
</body>
</html>