/**
 * 상단 검색폼 만들기 
 */
function createSearchForm(){
	// 검색영역 시작
	var contractComboBox = createContractComboBox();
	var searchBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct', scale : 'medium'});
	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-6, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var setSearchFormProp = {
			id: 'searchForm',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchBtn],
			tableProps : [{colspan:1, tdHeight: 30,
								item : contractComboBox 
						},{colspan:1, tdHeight: 30,
								item :createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate', name:'searchDate', start_dateValue : startDateObj, end_dateValue: endDateObj, notNull:false, dateType : 'D', format: 'Y-m', submitFormat : 'Ym'})  
						}]
	}
	var searchFormPanel = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	
	// 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn, actionLink,  'search');
	
	return searchFormPanel;
}

/**
 * 계정요청건수 그리드 생성 
 */
function createSumGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();
	/*
	var setGridProp = {	
			id: "summaryGrid"	
			,title: "계정요청구분별 완료건수"
			,url : "/itg/support/statistics/account/searchAccountReqGrid.do"
			,height : 300
			,groupField : "ACC_REQ_TYPE_NM"
			,sumField : "CNT" 
			,headerField : "ACC_REQ_DETAIL_TYPE_NM,ACC_TASK_TYPE_NM,ACC_WORK_RESULT_NM,CNT"
			,textField : "요청세부구분,요청타입,요청결과,건수"
			,widthField : "300,300,300,150"
			,alignField : "center,center,center,center,center"
			,paramMap: paramMap
		};

	var summaryGrid = createTotalSummaryGrid(setGridProp);
	*/
	
	// 점검일지 토탈 그리드
	var setGridProp = {
		id: 'summaryGrid',							// Grid id
		title: '계정요청별건수',		// Grid Title
		resource_prefix :'grid.statistics.accountTotal',
		url: '/itg/support/statistics/account/searchAccountReqGrid.do',	// Data Url
		params: paramMap,
		autoLoad: true,
		emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
	};
	var summaryGrid = createSummaryGridComp(setGridProp);
	
	return summaryGrid;
}

/**
 * 월별 계정 요청 차트 
 * @returns
 */
function createAccountChart(){

	// 차트가 그려질 영역 먼저 선택
	var paramMap = Ext.getCmp('searchForm').getInputData();
	
	var chartDiv = Ext.get("chartDiv1");
	var fusionChartProp = {
		location : 'chartDiv1' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/account/searchAccountMothlyChart.do' //필수
		, nameFeild : 'WORK_YM'
		, valueFeild : 'CNT'			
		, chartType : '2dBar' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		, paramMap : paramMap //필수
	}; 
	var setChartPanelProp = {
		id: 'accountChart',
		title: "월별계정 요청건수",
		panelHeight : 300,
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%', 
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var accountChart = createChartPanel(setChartPanelProp);
	
	return accountChart;
}


/**
 * Ext에서 제공하는 서머리 그리드 생성 부분 시작 
 * @param setGridProp
 * @returns
 */
function createTotalSummaryGrid(setGridProp){
	
	var that = this;
	for( var prop in setGridProp ){
		that[prop] = setGridProp[prop];
	}
	var id = that["id"];
	var paramMap = that["paramMap"];
	
	// 기본 데이터 + 그룹을 지을 필드까지 넣어줍니다.
	// 접었을 경우에 데이터를 보여주기 위함
	var gridHeader = that["headerField"].split(",");
	gridHeader.push(that["groupField"]);
	
	var gridText = that["textField"].split(",");
	var gridWidth = that["widthField"].split(",");
	var gridAlign = that["alignField"].split(",");
	
	var storeField = new Array();
	for ( var i = 0; i < gridHeader.length; i++) {
		var storeFieldColumn = {};
		
		if(that["sumField"] == gridHeader[i]){
			storeFieldColumn["name"] = gridHeader[i];
			storeFieldColumn["type"] = "float";
			storeField.push(storeFieldColumn);
		}else{
			storeFieldColumn["name"] = gridHeader[i];
			storeFieldColumn["type"] = "string";
			storeField.push(storeFieldColumn);
		}
	}
	
	var storeProxy = createGridProxyComp('storeProxy',that["url"], paramMap);
	
	var gridStore = Ext.create('Ext.data.Store', {
		fields : storeField
		,proxy : storeProxy
		,groupField: that["groupField"]
		,autoLoad: true
	});
	
	// columns 생성
	var columns = createSumColumns(setGridProp);
	
	var grid = Ext.create('Ext.grid.Panel', {

		width : '100%'
		,height: that["height"]
		,id : that["id"]
		,title: that["title"]
		,store: gridStore
		,forceFit : true
		,frame : true
		,features: [{
			id: 'group'
			,ftype: 'groupingsummary'
				// 이 옵션을 안주면 -> 컬럼명 : 데이터 <- 형태로 나옵니다.  
			,groupHeaderTpl: '{name}'
		}]
		,columns : columns
		, viewConfig: {
			emptyText:( that['emptyMsg'] ) ? that['emptyMsg'] : getConstText({ isArgs: true, m_key: 'msg.common.00001' })
		}
	});
	
	return grid;
} 

/**
 * Ext에서 제공하는 서머리 그리드 내부의 컬럼 생성  
 * @param setGridProp
 * @returns {Array}
 */
function createSumColumns(setGridProp){
	
	var that = this;
	for( var prop in setGridProp ){
		that[prop] = setGridProp[prop];
	}
	
	var gridHeader = that["headerField"].split(",");
	var gridText = that["textField"].split(",");
	var gridWidth = that["widthField"].split(",");
	var gridAlign = that["alignField"].split(",");
	
	var columns = new Array();
	
	for ( var i = 0; i < gridHeader.length; i++) {
		var tempColumn = {};
		if(that["sumField"] == gridHeader[i]){
			
			tempColumn["dataIndex"] = gridHeader[i];
			tempColumn["text"] = gridText[i];
			tempColumn["width"] = parseInt(gridWidth[i]);
			tempColumn["align"] = gridAlign[i];
			tempColumn["summaryType"] = "sum";
			tempColumn["renderer"] = renderer;
			tempColumn["summaryRenderer"] = summaryRenderer;
			tempColumn["field"] = {xtype: 'numberfield'};
			columns.push(tempColumn);
			
		}else{

			tempColumn["dataIndex"] = gridHeader[i];
			tempColumn["text"] = gridText[i];
			tempColumn["width"] = parseInt(gridWidth[i]);
			tempColumn["align"] = gridAlign[i];
			columns.push(tempColumn);
		}
	}
	
	return columns;
}

// columns내부의 renderer에 들어갈 펑션
function renderer(value, metaData, record, rowIdx, colIdx, store, view){
	return value;
}

//columns내부의 summaryRenderer에 들어갈 펑션
function summaryRenderer(value, summaryData, dataIndex) {
	return value;
}


