function createRecoveryConform(){
	
	
	
	
}

/**
 * 복구 성공율 그리드 생성 
 * @returns
 */
function createRecoverySuccessGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();

	var setGridProp = {	
			id: 'recoverySuccessGrid',
			title: '복구성공율' ,
			resource_prefix: 'grid.support.statistics.recoverySuccess',
			url: '/itg/support/statistics/recovery/searchRecoverySuccessGrid.do',
			params: paramMap,
			gridHeight : 100,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var recoverySuccessGrid = createGridComp(setGridProp);
	
	return recoverySuccessGrid;
}

/**
 * 긴급복구 수행건 그리드
 * @returns
 */
function createRecoveryEmergencyGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();

	var setGridProp = {	
			id: 'recoveryEmergencyGrid',
			title: '긴급복구 수행건 그리드' ,
			resource_prefix: 'grid.support.statistics.recoveryEmergency',
			url: '/itg/support/statistics/recovery/searchRecoveryEmergencyGrid.do',
			params: paramMap,
			gridHeight : 100,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var recoveryEmergencyGrid = createGridComp(setGridProp);
	
	return recoveryEmergencyGrid;
}


function createRecoveryMonthlyChart(){
	
var paramMap = Ext.getCmp('searchForm').getInputData();
	
	var chartDiv = Ext.get("chartDiv1");
	var fusionChartProp = {
		location : 'chartDiv1' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/recovery/searchRecoveryMonthlyChart.do' //필수
		, nameFeild : 'WORK_YM'
		, valueFeild : 'CNT'			
		, chartType : '2dBar' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		, paramMap : paramMap //필수
	}; 
	var setChartPanelProp = {
		id: 'recoveryMonthlyChart',
		title: "월별 복구 요청",
		panelHeight : 300,
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%', 
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var recoveryMonthlyChart = createChartPanel(setChartPanelProp);
	
	return recoveryMonthlyChart;
}

