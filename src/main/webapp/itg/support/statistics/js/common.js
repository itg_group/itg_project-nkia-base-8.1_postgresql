/**
 * 계약대상 콤보박스 생성
 * @returns
 */
function createContractComboBox(){
	var param = {};
	var contract_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/base/searchReqZoneCombo.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var contractProp_val = {label:getConstText({isArgs: true, m_key: 'res.00032'}), name:'REQ_ZONE', width: 300, notNull:true, attachAll:true};
		contractProp_val['proxy'] = contract_proxy;
		contractProp_val['valueField'] = 'CODE_ID';
	    contractProp_val['displayField'] = 'CODE_TEXT';
    var contractComboBox =  createComboBoxComp(contractProp_val);
	   
    return contractComboBox;
}

/**
 * 상단 검색폼 만들기 
 */
function createMonthSearchForm(){
	// 검색영역 시작
	var contractComboBox = createContractComboBox();
	var searchBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct', scale : 'medium'});
	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-6, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var setSearchFormProp = {
			id: 'searchForm',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchBtn],
			tableProps : [{colspan:1, tdHeight: 30,
								item : contractComboBox 
						},{colspan:1, tdHeight: 30,
								item :createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate', name:'searchDate', start_dateValue : startDateObj, end_dateValue: endDateObj, notNull:false, dateType : 'D', format: 'Y-m', submitFormat : 'Ym'})  
						}]
	}
	var searchFormPanel = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	
	// 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn, actionLink,  'search');
	
	return searchFormPanel;
}
