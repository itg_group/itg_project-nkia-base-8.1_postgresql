/**
 * 신규작업등록성공율 그리드 생성
 * @returns
 */
function createWorkReqNewSuccessGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();
	// 데이터 그리드 시작
	var setGridProp = {	
			id: 'workReqNewSuccessGrid',										// Grid id
			title: '작업타입별성공율' ,  // Grid Title
			resource_prefix: 'grid.support.statistics.reqNewSuccess',					// Resource Prefix
			url: '/itg/support/statistics/work/searchWorkReqNewSuccessGrid.do',			// Data Url
			params: paramMap,											// Data Parameters
			gridHeight : 200,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var workReqNewSuccessGrid = createGridComp(setGridProp);
	// 데이터 그리드 종료
	return workReqNewSuccessGrid;
}

/**
 * 작업변경 성공률 그리드 생성 
 * @returns
 
function createWorkChangeSuccessGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();
	// 데이터 그리드 시작
	var setGridProp = {	
			id: 'workChangeSuccessGrid',										// Grid id
			title: '작업변경성공율' ,  // Grid Title
			resource_prefix: 'grid.support.statistics.reqChanSuccess',					// Resource Prefix
			url: '/itg/support/statistics/work/searchWorkChangeSuccessGrid.do',			// Data Url
			params: paramMap,											// Data Parameters
			gridHeight : 100,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var workChangeSuccessGrid = createGridComp(setGridProp);
	// 데이터 그리드 종료
	return workChangeSuccessGrid;
}
*/


/**
 * 긴급 작업 수행건 그리드 생성
 * @returns
 */
function createWorkEmergencyGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();
	// 데이터 그리드 시작
	var setGridProp = {	
			id: 'workEmergencyGrid',										// Grid id
			title: '작업구분별건수' ,  // Grid Title
			resource_prefix: 'grid.support.statistics.reqEmergency',					// Resource Prefix
			url: '/itg/support/statistics/work/searchWorkEmergencyGrid.do',			// Data Url
			params: paramMap,											// Data Parameters
			gridHeight : 200,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var workEmergencyGrid = createGridComp(setGridProp);
	// 데이터 그리드 종료
	return workEmergencyGrid;
}

/**
 * 월별작업 요청차트 
 * @returns
 */
function createWorkChart(){

	// 차트가 그려질 영역 먼저 선택
	var paramMap = Ext.getCmp('searchForm').getInputData();
	
	var chartDiv = Ext.get("chartDiv1");
	var fusionChartProp = {
		location : 'chartDiv1' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/work/searchWorkChart.do' //필수
		, nameFeild : 'WORK_YM'
		, valueFeild : 'CNT'			
		, chartType : '2dBar' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		, paramMap : paramMap //필수
	}; 
	var setChartPanelProp = {
		id: 'workChart',
		title: "월별 작업 요청",
		panelHeight : 300,
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%', 
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var workChart = createChartPanel(setChartPanelProp);
	
	return workChart;
}


