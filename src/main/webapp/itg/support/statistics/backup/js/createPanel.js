/**
 * 백업정책 준수율 그리드 생성 
 * @returns
 */
function createBackupConform(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();

	var setGridProp = {	
			id: 'backupConformGrid',
			title: '백업정책준수율',
			resource_prefix: 'grid.support.statistics.backupConform',
			url: '/itg/support/statistics/backup/searchBackupConformGrid.do',
			params: paramMap,
			gridHeight : 100,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var backupConformGrid = createGridComp(setGridProp);
	
	return backupConformGrid;
	
}

/**
 * 백업성공율 그리드 생성
 * @returns
 */
function createBackupSuccessGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();

	var setGridProp = {	
			id: 'backupSuccessGrid',
			title: '백업성공율' ,
			resource_prefix: 'grid.support.statistics.backupSuccess',
			url: '/itg/support/statistics/backup/searchBackupSuccessGrid.do',
			params: paramMap,
			gridHeight : 100,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var backupSuccessGrid = createGridComp(setGridProp);
	
	return backupSuccessGrid;
	
}

/**
 * 긴급 백업 수행건 그리드 생성 
 * @returns
 */
function createBackupEmergencyGrid(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();

	var setGridProp = {	
			id: 'backupEmergencyGrid',
			title: '긴급백업수행건' ,
			resource_prefix: 'grid.support.statistics.backupEmergency',
			url: '/itg/support/statistics/backup/searchBackupEmergencyGrid.do',
			params: paramMap,
			gridHeight : 140,
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var backupEmergencyGrid = createGridComp(setGridProp);
	
	return backupEmergencyGrid;
	
}

/**
 * 백업월별 추이차트 생성
 * @returns
 */
function createBackupMonthlyChart(){
	
	var paramMap = Ext.getCmp('searchForm').getInputData();
	
	var chartDiv = Ext.get("chartDiv1");
	var fusionChartProp = {
		location : 'chartDiv1' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/backup/searchBackupMonthlyChart.do' //필수
		, nameFeild : 'WORK_YM'
		, valueFeild : 'CNT'			
		, chartType : '2dBar' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		, paramMap : paramMap //필수
	}; 
	var setChartPanelProp = {
		id: 'backupMonthlyChart',
		title: "월별 백업 요청",
		panelHeight : 300,
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%', 
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var backupMonthlyChart = createChartPanel(setChartPanelProp);
	
	return backupMonthlyChart;
}
