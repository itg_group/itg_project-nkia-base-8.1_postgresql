/**
 * 상위 검색탭 부분 
 * @returns
 */
function createSearchForm(){
	
	// 검색영역 시작
	var searchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct', scale : 'medium'});

	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var setSearchFormProp = {
			id: 'search_form',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchBtn],
			tableProps : [{colspan:1, tdHeight: 30,
								//item : createContractComboBox() 
								item : createReqZonePopupComp({label:"요청부대", id:"REQ_ZONE", name:"REQ_ZONE", width:246, leafSelect:false})
						},{colspan:1, tdHeight: 30,
								item :createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate', name:'search', start_dateValue : startDateObj, end_dateValue: endDateObj, dateType : 'D', format: 'Y-m', submitFormat : 'Ym', gubnCombo: false, gubnComboId : 'DATE_GUBN'})  
						}]
	}
	var searchFormPanel = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	
	// 상위 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn, actionLink,  'search');
	
	return searchFormPanel;
}

/**
 * 그리드 생성 
 */
function createGrid(){
	
	var paramMap = Ext.getCmp('search_form').getInputData();
	
	// 데이터 그리드 시작
	var setGridProp = {	
			id: 'grid',
			//title: '변경지표그리드',
			title: '작업 진행 현황',//getConstText({isArgs: true, m_key: 'res.graph.title.cloudIndexGrid'}),
			resource_prefix: 'grid.support.statistics.cloudGrid',
			url: '/itg/support/statistics/cloud/searchCloudGridList.do',
			params: paramMap,
			autoLoad: false,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var grid = createGridComp(setGridProp);
	// 데이터 그리드 종료

	return grid;
}
