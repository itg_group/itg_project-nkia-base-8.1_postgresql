/**
 * 상위 검색탭 부분 
 * @returns
 */
function createSearchForm(){
	
	// 검색영역 시작
	var searchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct', scale : 'medium'});

	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var setSearchFormProp = {
			id: 'search_form',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			hiddenFields:[{name: 'page'}, {name: 'limit'},{name: 'start'}],
			formBtns: [searchBtn],
			tableProps : [{colspan:1, tdHeight: 30,
								//item : createContractComboBox() 
								item : createReqZonePopupComp({label:"요청부대", id:"REQ_ZONE", name:"REQ_ZONE", width:246, leafSelect:false})
						},{colspan:1, tdHeight: 30,
								item :createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate', name:'search', start_dateValue : startDateObj, end_dateValue: endDateObj, dateType : 'D', format: 'Y-m', submitFormat : 'Ym', gubnCombo: false, gubnComboId : 'DATE_GUBN_PORTAL'})  
						}]
	}
	var searchFormPanel = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	
	// 상위 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn, actionLink,  'searchAttr');
	
	return searchFormPanel;
}

/**
 * 그리드 생성 
 */
function createReleaseGrid(){
	
	var paramMap = Ext.getCmp('search_form').getInputData();
	
	// 릴리즈 유형별 현황 그리드
	var setReleaseGridProp = {
			id: 'releaseGrid',
			//title: '릴리즈 유형별 현황',
			title: '작업완료 현황',//getConstText({isArgs: true, m_key: 'res.graph.title.releaseTypeList'}),
			resource_prefix: 'grid.support.statistics.release',
			url: '/itg/support/statistics/release/searchReleaseType.do',
			params: paramMap,
			pagingBar : false,
			autoLoad: false,
			gridHeight: 150
	}
	var releaseGrid = createGridComp(setReleaseGridProp);
	// 데이터 그리드 종료

	return releaseGrid;
}

/**
 * 차트 생성 
 * @returns
 */
function createReleaseChart(){

	var paramMap = Ext.getCmp('search_form').getInputData();
	
	// 릴리즈 성공율 및 roll-back 발생율 그래프
	// 차트가 그려질 영역 먼저 선택
	var chartDiv = Ext.get("chartDiv");
	var chartProp = {
			location : 'chartDiv' // 필수(차트가 위치할 곳)
			, title : getConstText({isArgs: true, m_key: 'res.label.proc.release.00001'})
			, url : '/itg/support/statistics/release/searchReleaseSuccessful.do' // 필수
			, nameFeild : 'REL_WORK_RESULT_NM' // 필수
			, valueFeild : 'PERCENT' // 필수
			, chartType : 'pie' // 필수
			, bgColor : '#FFFFFF'
			, showValue : true // 보임 : true, 숨김 : false
			, paramMap : paramMap // 필수
			, autoLoad: false
	}; 
	
	// 릴리즈 성공율 및 roll-back 발생율 그래프 패널 생성 
	var setServiceChartPanelProp = {
			id: 'relPanel',
			title : getConstText({isArgs: true, m_key: 'res.label.proc.release.00001'}),
			panelHeight : 330,
		 	fusionChartProp : chartProp
	};
	var relPanel = createChartPanel(setServiceChartPanelProp);
	
	return relPanel;
}

/**
 * 차트 생성 
 * @returns
 */
function createMonthReleaseChart(){
	
	var paramMap = Ext.getCmp('search_form').getInputData();
	
	// 월별 릴리즈 성공 추이 그래프
	var chartDivSecond = Ext.get("chartDivSecond");
	var fusionChartProp = {
			location : 'chartDivSecond'
			, url : '/itg/support/statistics/release/searchReleaseGraph.do' // 필수
			, xfeildColumn : 'WORK_YM' // 필수
			, barColumn : 'TOTAL_CNT' // 필수
			, barLabel : getConstText({isArgs: true, m_key: 'res.label.proc.release.00002'})
			, lineColumn : 'SUCCESS_CNT' // 필수
			, lineLabel : getConstText({isArgs: true, m_key: 'res.label.proc.release.00003'})
			, chartType : 'barLine' // 필수
			, bgColor : '#FFFFFF'
			, showValue : false // 보임 : true, 숨김 : false
			, paramMap : paramMap // 필수
			, autoLoad: false
	};

	// 월별 릴리즈 성공 추이 그래프 패널 생성 
	var setChartPanelProp = {
			id: 'monthlyChartPanel',
			title : getConstText({isArgs: true, m_key: 'res.label.proc.release.00004'}),
			panelHeight : 330,
			fusionChartProp : fusionChartProp
	};
	var monthlyChartPanel = createChartPanel(setChartPanelProp);
	
	return monthlyChartPanel;
}

