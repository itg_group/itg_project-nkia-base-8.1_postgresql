/**
 * 검색탭 부분 
 * @returns
 */
function createSearchForm(){
	
	//var contractComboBox = createContractComboBox();
	// 검색영역 시작
	var searchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct'});
	var searchInitBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.reset'}), id:'searchInitBtn'});

	// 날짜 셋팅
	var dateProp = getDefaultFromToDate("1:M");
	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-6, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});

	var setSearchFormProp = {
			id: 'search_form',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchBtn, searchInitBtn],
			tableProps : [
			              	{colspan:1, rowspan:1 , item:createReqZoneComp({label:"요청지역", name:"req_zone", attachAll: true, width :350, notNull :true, selectType :"combo", userReqZoneId : 'user_req_zone_id', userReqZoneNm : 'user_req_zone_nm'})}
							,{colspan:1, tdHeight: 30, item : createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate', name:'searchDate', start_dateValue : startDateObj, end_dateValue: endDateObj, notNull:false, dateType : 'D', format: 'Y-m', submitFormat : 'Ym'})}
							]
	}
	var searchFormPanel = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	// 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn, actionLink,  'search');
	attachBtnEvent(searchInitBtn, actionLink,  'searchInitBtn');
	
	return searchFormPanel;
	
}


/**
 *  만족도 분포건수 차트 
 * @returns
 */
function createStackChart(){

	var searchForm = Ext.getCmp("search_form");
	// 차트가 그려질 영역 먼저 선택
	var paramMap = searchForm.getInputData();
	
	var chartDiv = Ext.get("chartDiv1");
	var fusionChartProp = {
		location : 'chartDiv1' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/service/searchStatisScoreChart.do' //필수
		, nameFeild : 'REQ_TYPE_NM'
		, valueFeild : 'TOTAL_SCORE'			
		, chartType : '2dBar' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		, paramMap : paramMap //필수
		//, autoLoad: false
	}; 
	
	var setChartPanelProp = {
		id: 'stackChart',
		//title: "요청유형별 만족도 점수",
		title: getConstText({isArgs: true, m_key: 'res.graph.title.reqTypeSatisPoint'}),
		fusionChartProp : fusionChartProp
	};
	var stackChart = createChartPanel(setChartPanelProp);
	
	return stackChart;
}

/**
 * 만족도 원형차트
 * @returns
 */
function createPieChart(){

	var searchForm = Ext.getCmp("search_form");
	// 차트가 그려질 영역 먼저 선택
	var paramMap = searchForm.getInputData();
	
	var chartDiv = Ext.get("chartDiv2");
	var fusionChartProp = {
		location : 'chartDiv2' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/service/searchDistributionSatisfactionGraphList.do' //필수
		, nameFeild : '매우만족,만족,보통,불만족,매우불만족' //필수
		, valueFeild : 'NAME1_VALUE,NAME2_VALUE,NAME3_VALUE,NAME4_VALUE,NAME5_VALUE' //필수
		, chartType : 'pie' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, paramMap : paramMap //필수
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		//, autoLoad: false
	}; 
	var setChartPanelProp = {
		id: 'pieChart',
		//title: "만족도 비율",
		title: getConstText({isArgs: true, m_key: 'res.graph.title.satisPercentage'}),
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%', 
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var pieChart = createChartPanel(setChartPanelProp);
	
	return pieChart;
}

//만족도 통계 그리드 
function createTotlaGrid(){
	
	var excelBtn = createBtnComp({
		visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.exceldown'}), 
		id:'excelBtn',icon :'/itg/base/images/ext-js/common/icons/excel-icon.png'
	});
	
	var searchForm = Ext.getCmp("search_form");
	var paramMap = searchForm.getInputData();
	var setTotalGridProp = {	
			id: 'totalGrid',
			//title: '만족도통계그리드',
			gridHeight : '100%',
			title: getConstText({isArgs: true, m_key: 'res.graph.title.satisStatisGrid'}),
			resource_prefix: 'grid.statistics.satisfactionGrid',
			url: '/itg/support/statistics/service/searchTotalGrid.do',
			params: paramMap,
			toolBarComp:[excelBtn],
			forceFit : true,
			pagingBar : false
	}
	var totalGrid = createGridComp(setTotalGridProp);
	
	//엑셀 버튼을 눌렀을 경우 
	attachBtnEvent(excelBtn, createExcelDown,  'SATIS');
	
	return totalGrid;
}

//신속도 통계 그리드 
function createSpeedTotlaGrid(){
	
	var excelBtn = createBtnComp({
		visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.exceldown'}), 
		id:'excelSpeedBtn',icon :'/itg/base/images/ext-js/common/icons/excel-icon.png'
	});
	
	var searchForm = Ext.getCmp("search_form");
	var paramMap = searchForm.getInputData();
	
	var setTotalGridProp = {	
			id: 'speedGrid',
			gridHeight : 180,
			title: getConstText({isArgs: true, m_key: 'res.graph.title.satisSpeedGrid'}),
			resource_prefix: 'grid.statistics.satisfactionSpeedGrid',
			url: '/itg/support/statistics/service/searchSpeedGrid.do',
			params: paramMap,
			toolBarComp:[excelBtn],
			forceFit : true,
			pagingBar : false
	}
	var totalGrid = createGridComp(setTotalGridProp);
	
	//엑셀 버튼을 눌렀을 경우 
	attachBtnEvent(excelBtn, createExcelDown,  'SPEED');
	
	return totalGrid;
}

//정확도 통계 그리드 
function createAccuracyTotlaGrid(){
	
	var excelBtn = createBtnComp({
		visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.exceldown'}), 
		id:'excelAccuracyBtn',icon :'/itg/base/images/ext-js/common/icons/excel-icon.png'
	});
	
	var searchForm = Ext.getCmp("search_form");
	var paramMap = searchForm.getInputData();
	var setTotalGridProp = {	
			id: 'accuracyGrid',
			gridHeight : 180,
			title: getConstText({isArgs: true, m_key: 'res.graph.title.satisAccuracyGrid'}),
			resource_prefix: 'grid.statistics.satisfactionAccuracyGrid',
			url: '/itg/support/statistics/service/searchAccuracyGrid.do',
			params: paramMap,
			toolBarComp:[excelBtn],
			forceFit : true,
			pagingBar : false
	}
	var totalGrid = createGridComp(setTotalGridProp);
	
	//엑셀 버튼을 눌렀을 경우 
	attachBtnEvent(excelBtn, createExcelDown,  'ACCUR');
	
	return totalGrid;
}

//계약대상 콤보박스 
function createContractComboBox(){
	var param = {};
	var contract_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/slms/common/searchCustCombo.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var contractProp_val = {label:getConstText({isArgs: true, m_key: 'res.00032'}), id:'REQ_ZONE', name:'REQ_ZONE', width: 300, notNull:true, attachAll:true};
		contractProp_val['proxy'] = contract_proxy;
		contractProp_val['valueField'] = 'CODE_ID';
	    contractProp_val['displayField'] = 'CODE_TEXT';
    var contractComboBox =  createComboBoxComp(contractProp_val);
	   
    return contractComboBox;
}