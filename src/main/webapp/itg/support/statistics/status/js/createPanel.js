/**
 * 계약대상 콤보박스 생성
 * @returns
 */
function createContractCombo(id, name){
	var param = {};
	var contract_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/base/searchReqZoneCombo.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var contractProp_val = {label:getConstText({isArgs: true, m_key: 'res.00032'}), id: id, name: name, width: 300, notNull:true, attachAll:true};
		contractProp_val['proxy'] = contract_proxy;
		contractProp_val['valueField'] = 'CODE_ID';
	    contractProp_val['displayField'] = 'CODE_TEXT';
    var contractComboBox =  createComboBoxComp(contractProp_val);
	   
    return contractComboBox;
}

/**
 * 검색탭 부분 
 * @returns
 */
function createSearchFormMonth(){
	
	// 검색영역 시작
	var monthlySearchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'monthlySearchBtn', ui : 'correct'});
	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-6, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var setMonthlySearchformProp = {
			id: 'monthlySearchform',
			title : getConstText({isArgs: true, m_key: 'res.label.proc.status.00001'}),
			columnSize: 2,
			formBtns: [monthlySearchBtn],
			hiddenFields:[{name: 'page'}, {name: 'limit'},{name: 'start'}],
			tableProps : [
							  //{colspan:1, tdHeight: 30, item :createContractCombo('CUST_ID_01', 'CUST_ID_01')}
							  {colspan:1, tdHeight: 30, item :createReqZonePopupComp({label:"요청부대", id:"CUST_ID_01", name:"CUST_ID_01", width:246, leafSelect:false})}
							, {colspan:1, tdHeight: 30, item :createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate_01' , name:'search', start_dateValue : startDateObj, end_dateValue: endDateObj, notNull:false, dateType : 'D', format: 'Y-m', submitFormat : 'Ym'})}
						]
	}
	var monthlySearchFormPanel = createSeachFormComp(setMonthlySearchformProp);
	// 검색영역 종료
	// 검색버튼을 눌렀을 경우 
	attachBtnEvent(monthlySearchBtn, actionLink,  'monthlySearchAttr');
	
	return monthlySearchFormPanel;
	
}

/**
 * 검색탭 부분 
 * @returns
 */
function createSearchForm(){
	
	// 검색영역 시작
	var dailySearchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'dailySearchBtn', ui : 'correct'});
	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-6, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var setDailySearchformProp = {
			id: 'dailySearchform',
			title : getConstText({isArgs: true, m_key: 'res.label.proc.status.00002'}),
			columnSize: 2,
			formBtns: [dailySearchBtn],
			hiddenFields:[{name: 'page'}, {name: 'limit'},{name: 'start'}],
			tableProps : [
							  //{colspan:1, tdHeight: 30, item :createContractCombo('CUST_ID_02', 'CUST_ID_02')}
							  {colspan:1, tdHeight: 30, item :createReqZonePopupComp({label:"요청부대", id:"CUST_ID_02", name:"CUST_ID_02", width:246, leafSelect:false})}
							, {colspan:1, tdHeight: 30, item :createMonthFieldComp({label:getConstText({isArgs: true, m_key: 'res.00022'}), id:'searchDate_02', name:'search', dateValue: endDateObj, notNull: true, dateType:'D', format:'Y-m', submitFormat : 'Y-m'})}
						]
	}
	var dailySearchFormPanel = createSeachFormComp(setDailySearchformProp);	
	// 검색영역 종료
	// 검색버튼을 눌렀을 경우 
	attachBtnEvent(dailySearchBtn, actionLink,  'dailySearchAttr');
	
	return dailySearchFormPanel;
	
}


/**
 *  월간 차트 
 * @returns
 */
function createStatusMonthChart(){

	var searchFormParam = Ext.getCmp('monthlySearchform').getInputData();
	var params = {};
	params['startDate'] = searchFormParam['search_startDate'];
	params['endDate'] = searchFormParam['search_endDate'];
	
	// 차트생성
	var chartDiv = Ext.get("chartDiv");
	var fusionChartProp = {
			location : 'chartDiv' // 필수(차트가 위치할 곳) 
			, url : '/itg/support/statistics/status/searchMonthlyGraph.do' //필수
			, xfeildColumn : 'WORK_YM' //필수
			, barColumn : 'TOTAL_CNT' //필수
			, barLabel : getConstText({isArgs: true, m_key: 'res.label.proc.status.00003'})
			, lineColumn : 'NEW_CNT,CHANGE_CNT' //필수
			, lineLabel : getConstText({isArgs: true, m_key: 'res.label.proc.status.00004'})
			, chartType : 'barLine' //필수
			, bgColor : '#FFFFFF'
			, showValue : false // 보임 : true, 숨김 : false
			, paramMap : params //필수
			, autoLoad: false
	}; 
	
	var setMonthlyChartPanelProp = {
			id: 'monthlyChartPanel',
			title : '월별 작업완료 검색',//getConstText({isArgs: true, m_key: 'res.label.proc.status.00005'}),
			panelHeight : 330,
			fusionChartProp : fusionChartProp,
			items: [{
		    	columnWidth: '100%',
		    	height: 320,
	            baseCls:'x-plain',
	            items:[chartDiv]
	    	}]
	};
	var monthlyPanel = createChartPanel(setMonthlyChartPanelProp);
	
	return monthlyPanel;
}

/**
 * 만족도 비율 원형차트
 * @returns
 */
function createStatusChart(){

	var paramMap = Ext.getCmp('dailySearchform').getInputData();
	paramMap['searchDate'] = paramMap['search_day']+'-01';
	
	// 차트2
	var chartDivSecond = Ext.get("chartDivSecond");
	fusionChartProp = {
			location : 'chartDivSecond'
			, url : '/itg/support/statistics/status/searchDailyGraph.do' //필수
			, xfeildColumn : 'WORK_YM' //필수
			, barColumn : 'TOTAL_CNT' //필수
			, barLabel : getConstText({isArgs: true, m_key: 'res.label.proc.status.00003'})
			, lineColumn : 'NEW_CNT,CHANGE_CNT' //필수
			, lineLabel : getConstText({isArgs: true, m_key: 'res.label.proc.status.00004'})
			, chartType : 'barLine' //필수
			, bgColor : '#FFFFFF'
			, showValue : false // 보임 : true, 숨김 : false
			, paramMap : paramMap //필수
			, autoLoad: false
	};
	
	var setDaylyChartPanelProp = {
			id: 'dailyChartPanel',
			title : '일별 작업완료 검색',//getConstText({isArgs: true, m_key: 'res.label.proc.status.00006'}),
			panelHeight : 320,
			fusionChartProp : fusionChartProp,
			items: [{
		    	columnWidth: '100%',
		    	height: 330,
	            baseCls:'x-plain',
	            items:[chartDivSecond]
	    	}]
	};
	var dailyChartPanel = createChartPanel(setDaylyChartPanelProp);
	
	return dailyChartPanel;
}
