/**
 * 상위 검색탭 부분 
 * @returns
 */
function createSearchForm(){
	
	// 검색영역 시작
	var searchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct'});
	var searchInitBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.reset'}), id:'searchInitBtn'});

	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var setSearchFormProp = {
			id: 'search_form',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchBtn, searchInitBtn],
			tableProps : [{colspan:1, tdHeight: 30,
								item : createReqZoneComp({label:"요청지역", name:"req_zone", attachAll: true, width :350, notNull :true, selectType :"combo", userReqZoneId : 'user_req_zone_id', userReqZoneNm : 'user_req_zone_nm'})},
						  {colspan:1, tdHeight: 30,
								item :createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate', name:'search', start_dateValue : startDateObj, end_dateValue: endDateObj, dateType : 'D', format: 'Y-m', submitFormat : 'Ym', gubnCombo: false, gubnComboId : 'DATE_GUBN'})  
						}]
	}
	var searchFormPanel = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	
	// 상위 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn, actionLink,  'search');
	attachBtnEvent(searchInitBtn, actionLink,  'searchInitBtn');
	
	return searchFormPanel;
}

/**
 * 그리드 생성 
 */
function createGrid(){
	
	var paramMap = Ext.getCmp('search_form').getInputData();
	
	// 데이터 그리드 시작
	var setGridProp = {	
			id: 'grid',
			//title: '변경지표그리드',
			title: '작업 진행 현황',//getConstText({isArgs: true, m_key: 'res.graph.title.problemIndexGrid'}),
			resource_prefix: 'grid.support.statistics.problemGrid',
			url: '/itg/support/statistics/change/searchProblemGridList.do',
			params: paramMap,
			gridHeight : '100%',
			autoLoad: false,
			border : false,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var changeGrid = createGridComp(setGridProp);
	// 데이터 그리드 종료
	
	//attachCustomEvent('cellclick', changeGrid, gridCellClick);
	
	return changeGrid;
}
