/**
 * 상위 검색탭
 * @returns
 */
function createSearchPanel1(){
	
	// 검색영역 시작
	var searchBtn1 = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn1', ui : 'correct', scale : 'medium'});
	var searchInitBtn1 = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.system.00003'}), id:'searchInitBtn1', scale : 'medium'});

	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-2, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var setSearchFormProp = {
			id: 'search_form1',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchBtn1, searchInitBtn1],
			tableProps : [
			            {colspan:1, 
			            			item : createReqZoneComp({label:getConstText({isArgs: true, m_key: 'res.00036'}), name:"cust_id1", attachAll: true, width :350, notNull :true, selectType :"combo", userReqZoneId : 'user_req_zone_id', userReqZoneNm : 'user_req_zone_nm'})},
			            {colspan:1, tdHeight: 30,
									item : createSearchDateFieldComp({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate1' , name:'searchDate1', start_dateValue : startDateObj, end_dateValue: endDateObj, notNull:false, dateType : 'D', format: 'Y-m-d'}) 
						}]
	}
	var searchFormPanel1 = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	
	// 상위 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn1, actionLink,  'upPanelSearch');
	attachBtnEvent(searchInitBtn1, actionLink,  'upPanelSearchInit');
	
	return searchFormPanel1;
}

/**
 * 유형별 상담건수 차트 패널 생성   
 * @returns
 */
function createUpPanel(){

	// 차트가 그려질 영역 먼저 선택
	var paramMap = Ext.getCmp('search_form1').getInputData();
	paramMap["searchDate1_startDate"] = replaceAll(paramMap.searchDate1_startDate,"-","");
	paramMap["searchDate1_endDate"] = replaceAll(paramMap.searchDate1_endDate,"-","");
	
	var chartDiv = Ext.get("chartDiv1");
	var fusionChartProp = {
		location : 'chartDiv1' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/serviceDesk/searchTotalTypeCounsel.do' //필수
		, nameFeild : 'REQ_TYPE_NM'
		, valueFeild : 'TOT_CNT'			
		, chartType : '2dBar' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		, paramMap : paramMap //필수
		, barEventJavascript : "createCounselorTypePopup"
		, barEventData : "RNUM"
	}; 
	var setChartPanelProp = {
		id: 'upPanel',
		title: "유형별 상담건수",
		panelHeight : 300,
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%', 
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var upPanel = createChartPanel(setChartPanelProp);
	
	return upPanel;
}

/**
 * 중앙 검색탭 
 * @returns
 */
function createSearchPanel2(){
	
	// 검색영역 시작
	var searchBtn2 = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn2', ui : 'correct', scale : 'medium'});
	var searchInitBtn2 = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.system.00003'}), id:'searchInitBtn2', scale : 'medium'});
	
	// 시작 종료 날짜 셋팅
	var startDateObj2 = createDateTimeStamp('DHM', {year:0, month:-6, day:0, hour:0, min:0});
	var endDateObj2 = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	var setSearchFormProp = {
			id: 'search_form2',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchBtn2, searchInitBtn2],
			tableProps : [{colspan:1, 
    							item : createReqZoneComp({label:getConstText({isArgs: true, m_key: 'res.00036'}), name:"cust_id2", attachAll: true, width :350, notNull :true, selectType :"combo", userReqZoneId : 'user_req_zone_id', userReqZoneNm : 'user_req_zone_nm'})},
    					  {colspan:1, tdHeight: 30,
								item : createSearchMonthPicker({label:getConstText({isArgs: true, m_key: 'res.00022'}), id : 'searchDate2', name:'searchDate2', start_dateValue : startDateObj2, end_dateValue: endDateObj2, notNull:false, dateType : 'D', format: 'Y-m', submitFormat : 'Ym'}) 
						}]
	}
	var searchFormPanel2 = createSeachFormComp(setSearchFormProp);
	// 검색영역 종료
	
	// 상위 검색버튼을 눌렀을 경우 
	attachBtnEvent(searchBtn2, actionLink,  'downPanelSearch');
	attachBtnEvent(searchInitBtn2, actionLink,  'downPanelSearchInit');
	
	return searchFormPanel2;
	
}

/**
 * 기간별 상담수 차트 패널 생성 
 * @returns
 */
function createDownPanel(){
	
	var paramMap = Ext.getCmp('search_form2').getInputData();
	
	// 차트가 그려질 영역 먼저 선택
	var chartDiv = Ext.get("chartDiv2");
	var fusionChartProp = {
		location : 'chartDiv2' // 필수(차트가 위치할 곳) 
		, url : '/itg/support/statistics/serviceDesk/searchTermTotalCounsel.do' //필수
		, nameFeild : 'WORK_YM'
		, valueFeild : 'CNT'			
		, chartType : '2dBar' //필수
		, showValue : true // 보임 : true, 숨김 : false
		, bgColor : '#ffffff'
		, canvasBgColor : '#ffffff'
		, paramMap : paramMap //필수
		, barEventJavascript : "clickDownChartPanel"
		, barEventData : "WORK_YM"
	}; 
	var setChartPanelProp = {
		id: 'downPanel',
		title: "기간별 상담수",
		panelHeight : 300,
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%',
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var downPanel = createChartPanel(setChartPanelProp);
	
	return downPanel;
}
