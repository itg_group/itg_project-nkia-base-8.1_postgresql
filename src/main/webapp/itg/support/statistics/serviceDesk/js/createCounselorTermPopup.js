var counselorTermPopup_comp = {};
/**
 * 기간별 상담수 차트 클릭
 * @param workYm
 */
function clickDownChartPanel(workYm){

	var paramMap = Ext.getCmp("search_form2").getInputData();
	paramMap["searchDate"] = workYm; 
	
	// 팝업 파라미터 
	var counselorTermPopUpProp ={
		id: 'counselorTermPopUp',
    	buttonAlign : 'center',
    	bodyPadding: '5 5 5 5 ',
    	bodyStyle: 'background-color: white; ',
    	border: false,
    	paramMap : paramMap
	};
	
	// 탭 페이지 이동 
	var counselorTermPopUp = createCounselorTermPopup(counselorTermPopUpProp);
	counselorTermPopUp.show();
	
}


function createCounselorTermPopup(setPopUpProp){
	
	var that = this;
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	var paramMap = that["paramMap"];
	
	// 버튼 묶음
	var closeTermPopupBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.close' }), ui:'correct', scale:'medium'});
	
	
	// 데이터 그리드 시작
	var setGridProp = {	
			id: 'monthCounselorTypeGrid',										// Grid id
			title: '상담원별 건수' ,  // Grid Title
			resource_prefix: 'grid.support.statistics.serviceDeskMonthTypeList',					// Resource Prefix
			url: '/itg/support/statistics/serviceDesk/searchMonthCounselorTypeList.do',			// Data Url
			params: paramMap,											// Data Parameters
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({ isArgs: true, m_key: 'msg.common.00001' })
		};
	
	var monthCounselorTypeGrid = createSummaryGridComp(setGridProp);
	
	counselorTermPopUp = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		width: 700,  
		autoDestroy: true,
		resizable:false, 
		buttonAlign: that['buttonAlign'],
		bodyStyle: that['bodyStyle'],
		buttons: [closeTermPopupBtn],
		items: [monthCounselorTypeGrid],
		listeners: {
			destroy:function(p) {
				counselorPopUp_comp[that['id']] = null
 			},
 			hide:function(p) {
 			}
 		}
	});
	
	// 닫기버튼을 눌렀을 경우
	attachBtnEvent(closeTermPopupBtn, termPopupActionLink,  'closeTermPopupBtn');
	
	
	return counselorTermPopUp;
	
}

function termPopupActionLink(command){
	
	switch(command) {
	
		case 'closeTermPopupBtn' :
			counselorTermPopUp.doClose();
		break;
	}
}


