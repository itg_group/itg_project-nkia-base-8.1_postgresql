function createCounselorTypePopup(order){
	
	var  paramMap = Ext.getCmp('search_form1').getInputData();
	paramMap["searchDate1_startDate"] = replaceAll(paramMap.searchDate1_startDate,"-","");
	paramMap["searchDate1_endDate"] = replaceAll(paramMap.searchDate1_endDate,"-","");
	paramMap["RNUM"] = order;
	
	// 파라미터를 먼저 구합니다. 
	jq.ajax({ type:"POST"
		, url: '/itg/support/statistics/serviceDesk/detailTotalTypeCounsel.do' 
		, contentType: "application/json"
		, dataType: "json"
		, async: false
		, data : getArrayToJson(paramMap)
		, success:function(data){
			var REQ_TYPE = data.resultMap.REQ_TYPE;
			paramMap["REQ_TYPE"] = REQ_TYPE;
		}
	});
	
	// 팝업 파라미터 
	var counselorPopUpProp ={
		id: 'counselorPopUp',
    	buttonAlign : 'center',
    	bodyPadding: '5 5 5 5 ',
    	bodyStyle: 'background-color: white; ',
    	border: false,
    	paramMap : paramMap
	};
	
	// 탭 페이지 이동 
	var counselorPopUp = createCounselorPopUp(counselorPopUpProp);
	counselorPopUp.show();
		
}

var counselorPopUp_comp = {};
function createCounselorPopUp(setPopUpProp){
	
	var that = this;
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	var paramMap = that["paramMap"];
	
	// 버튼 묶음
	var closeBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.close' }), ui:'correct', scale:'medium'});
	
	// 데이터 그리드 시작
	var setGridProp = {	
			id: 'counselorGridList',										// Grid id
			title: '상담원별 건수' ,  // Grid Title
			resource_prefix: 'grid.support.statistics.serviceDeskList',					// Resource Prefix
			url: '/itg/support/statistics/serviceDesk/searchCounselorTypeList.do',			// Data Url
			params: paramMap,											// Data Parameters
			autoLoad: true,
			border : false,
			emptyMsg: getConstText({ isArgs: true, m_key: 'msg.common.00001' })
		};
	
	var counselorGrid = createGridComp(setGridProp);
	
	
	/*
	var chartDiv = Ext.get("chartDiv3");
	var fusionChartProp = {
			location : 'chartDiv3' // 필수(차트가 위치할 곳) 
			, url : '/itg/support/statistics/serviceDesk/searchCounselorTypeList.do' //필수
			, yLabel : 'OPER_USER_NM'
			, valueLabel : '1선처리,진행,종료'
			, valueField : 'FIRST_CNT,ING_CNT,END_CNT'			
			, chartType : 'multi2DBar' //필수
			, showValue : true // 보임 : true, 숨김 : false
			, bgColor : '#ffffff'
			, canvasBgColor : '#ffffff'
			, paramMap : paramMap //필수
		}; 
	var setChartPanelProp = {
		id: 'counselorChartPanel',
		title : '상담원별 건수',
		panelHeight : 300,
		fusionChartProp : fusionChartProp,
		items: [{
			columnWidth: '100%', 
		    baseCls:'x-plain',
		    bodyStyle:'padding:10px 10px 10px 10px;',
		    items:[chartDiv]
		}]
	};
	var counselorChartPanel = createChartPanel(setChartPanelProp);
	*/
	
	if(null == counselorPopUp_comp[that['id']]){
		
		counselorPopUp = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			width: 700,  
			autoDestroy: true,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			bodyStyle: that['bodyStyle'],
			buttons: [closeBtn],
			items: [counselorGrid],
			listeners: {
				destroy:function(p) {
					counselorPopUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		counselorPopUp_comp[that['id']] = counselorPopUp;
		
	}else{
		
		counselorPopUp = counselorPopUp_comp[that['id']];
		/*
		var fusionChartProp = {
				location : 'chartDiv3' // 필수(차트가 위치할 곳) 
				, url : '/itg/support/statistics/serviceDesk/searchCounselorTypeList.do' //필수
				, yLabel : 'OPER_USER_NM'
				, valueLabel : '1선처리,진행,종료'
				, valueField : 'FIRST_CNT,ING_CNT,END_CNT'			
				, chartType : 'multi2DBar' //필수
				, showValue : true // 보임 : true, 숨김 : false
				, bgColor : '#ffffff'
				, canvasBgColor : '#ffffff'
				, paramMap : paramMap //필수
			}; 
		createFusionChart(fusionChartProp);
		*/
		
	}
	
	// 닫기버튼을 눌렀을 경우
	attachBtnEvent(closeBtn, popupActionLink,  'popupCloseBtn');
	
	return counselorPopUp;
	
}

/**
 * 팝업 버튼 액션 
 * @param command
 */
function popupActionLink(command){
	switch(command) {
		
		case 'popupCloseBtn' :
			counselorPopUp.doClose();
		break;
	}
}
