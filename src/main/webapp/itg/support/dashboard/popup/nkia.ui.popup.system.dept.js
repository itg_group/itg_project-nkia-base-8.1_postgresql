/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.DeptTree = function() {
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/system/dept/searchStaticAllDeptTree.do',
	        filterable: true,
	        params: params,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if(treeItem){
			var reference = this.props.reference;
			var fields = reference.fields;
			
			var values = {};
			values[fields["id"]] = treeItem.node_id;	// 부서 ID
			values[fields["name"]] = treeItem.text;		// 부서 명 
			
            reference.form._setValues(values)
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 부서정보가 없습니다.'
            });
        }
	};
};

/********************************************
 * Form 담당자 관리 그룹 부서 트리 팝업
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.ChargerGrpDeptTree = function() {
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		var params = props.popupParamData.params;
		params['is_all_tree'] = "Y";
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/workflow/charger/searchStaticAllDeptTree.do',
	        expColable: true,
			filterable: true,
			checkbox: true,
			params: params,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){

		var inputData = $$('chargerGrpEditForm').getValues();
		
		var selectedRecords = $$(this.props.id + "_tree")._getCheckedItems();
		
		if(selectedRecords){
			var grp_id   = inputData.GRP_ID;
	        var node_id  = [];
	        var USER_YN  = "N";
	        
	        for(var i=0; i<selectedRecords.length; i++){
	        	node_id.push(selectedRecords[i].node_id);
	        }
	        var paramMap = {};
	        paramMap['grp_id']     = grp_id;
	        paramMap['charger_id'] = node_id;
	        paramMap['user_yn']    = USER_YN;
	
			var requestData	= paramMap;
			var url			= '/itg/workflow/charger/insertChargerGrpCharger.do';
			var callback	= function(resultData){
				if(resultData.success){
							reloadChargerGrpEditForm(true, "editTrueBtn");
							reloadChargerGrid(false, grp_id);
							//selectTreeNode(true, true);
							nkia.ui.utils.notification({
								message: resultData.resultMsg
							});
					}else{
							nkia.ui.utils.notification({
								message: resultData.resultMsg
							});
							return;
						}
				}
			callAjax(url, requestData, callback);
			this.window.close();
		}else{
	          nkia.ui.utils.notification({
	          type: 'error',
	          message: '선택된 부서정보가 없습니다.'
	          });
	    }
		////
	};
};

/**
 * @method {} createDeptWindow : Form Function
 * @property {string} id			: Form Id
 * @property {object} header 		: Form Header
 * @property {object} fields 		: Form Fields
 * @property {object} footer 		: Form Footer
 * @returns {object} form
 */
function createDeptWindow(popupProps) {
	var props = popupProps||{};
	var dept;
	try {
//		if(props.type){
//			// 추후 List나 다른형태를 구현할때 쓰시오.
//		}else{
//			dept = new nkia.ui.popup.system.DeptTree();
//		}
		
		switch(props.type){
			case('chargerGrp') :
				dept = new nkia.ui.popup.system.ChargerGrpDeptTree();
				break;
			default : 
				dept = new nkia.ui.popup.system.DeptTree();
		}
		
		dept.setRender(props);
	} catch(e) {
		alert(e.message);
	}
}
