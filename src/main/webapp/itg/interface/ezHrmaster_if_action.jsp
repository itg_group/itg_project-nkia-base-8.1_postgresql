<%@ page language="java" contentType="text/html; charset=utf-8"   pageEncoding="utf-8"%>
<%@ include file="ezHrmaster_dbconn.jsp" %> <!-- dbCon.jsp 파일을 불러오는 부분입니다. -->
<%@ page import="java.util.*,java.text.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ezHrmaster_if_action</title>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="630" border="0" cellspacing="0" cellpadding="0">
 <%
 try{
 sql="SELECT DEPTID,DEPTNM,PARENTDEPTID from VIEW_DEPT"; //쿼리 부분, 실제 접속 계정에 저장 되어 있는 table을 입력해야 합니다.
   stmt = con.createStatement();
   rs = stmt.executeQuery(sql);

   while(rs.next()) {
	   
	   out.print(rs.getString(1)+":"); // 출력부분
	   out.print(rs.getString(2)+":"); // 출력부분
	   out.print(rs.getString(3)+"<BR>"); // 출력부분
   }
   
   rs.close();
   out.print("----------------------------------------------"); // 출력부분 
   sql="SELECT USERID,USERNAME,DEPTID from VIEW_USER"; //쿼리 부분, 실제 접속 계정에 저장 되어 있는 table을 입력해야 합니다.
   
   rs = stmt.executeQuery(sql);

   while(rs.next()) {
	   
	   out.print(rs.getString(1)+":"); // 출력부분
	   out.print(rs.getString(2)+":"); // 출력부분
	   out.print(rs.getString(3)+"<BR>"); // 출력부분
   }
	   
 }catch(Exception e){
	 out.println(e);
 }finally{
	 if(rs != null) rs.close();
	   if(stmt != null)stmt.close();
	   if(con != null)con.close();
 }

   
   
 %>

</html>