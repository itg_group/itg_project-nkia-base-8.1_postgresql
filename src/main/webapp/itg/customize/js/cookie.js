/**
 * 쿠키 설정, 쿠키 값 얻기, 삭제 
 * 
 */


function JsCookie() {}

var jsCookie = new JsCookie();
/**
 * 쿠키 세팅
 * @param {String} cookieName
 * @param {String} cookieValue
 */
function setSimpleCookie(cookieName, cookieValue)
{
	document.cookie = cookieName + "=" + escape(cookieValue);
}

JsCookie.prototype.setCookie = setCookie;

/**
 * 브라우져가 떠있는 동안만 사용할 초간단 임시 쿠키
 */
JsCookie.prototype.setSimpleCookie = setSimpleCookie;

function setCookie(cookieName, cookieValue, cookiePath, cookieExpires, cookieDomain){
	cookieValue = escape(cookieValue);
	var cookieExpress = cookieName + "=" + cookieValue;
	if (cookieExpires == null || cookieExpires == "") {
		var nowDate = new Date();
		nowDate.setMonth(nowDate.getMonth() + 6);
		cookieExpires = nowDate.toGMTString();
	}
	cookieExpress += "; expires=" + cookieExpires;

	if (cookiePath != null && cookiePath != "") {
		cookiePath = "; path=" + cookiePath;
		cookieExpress += cookiePath;
	}

	if (cookieDomain != null && cookieDomain != "") {
		cookieDomain = "; domain=" + cookieDomain;
		cookieExpress += cookieDomain;
	}
	document.cookie = cookieExpress;
}

JsCookie.prototype.getCookieValue = getCookieValue;

function getCookie(name){  
	var nameOfCookie = name + "=";  
	var x = 0;  
	while ( x <= document.cookie.length ) {
		var y = (x+nameOfCookie.length);
		if ( document.cookie.substring( x, y ) == nameOfCookie ) {
			if ( (endOfCookie=document.cookie.indexOf( ";", y )) == -1 )  
				endOfCookie = document.cookie.length;
			return unescape( document.cookie.substring( y, endOfCookie ) );
		}
		x = document.cookie.indexOf( " ", x ) + 1;
		if ( x == 0 )
			break;
	}
	return "";  
}

JsCookie.prototype.clearCookie = clearCookie;

function clearCookie(cookieName){
	var expireDate = new Date();
	expireDate.setTime(expireDate.getTime()-10); 
	setCookie(cookieName, "", null, expireDate);
}