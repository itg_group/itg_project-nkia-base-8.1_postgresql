/********************************************
 * Date: 2018-05-17
 * Version: 1.0 - 최초등록
 * Description :  메인화면 공지사항팝업 webix
 ********************************************/
function createDetailPop(noti_id,type){
		//공지사항 팝업 정보
		var noticeEditorForm = createFormComp({
		    id: "noticeEditorForm",
			header: {
		        title: "공지사항 정보"
		    },
		    fields: {
		        colSize: 1,
		        items: [
		            { item : createTextFieldComp({label:'제목', name:'title', readonly : true})},
		            {item: createTextAreaFieldComp({label:'내용', name: "content", height: 250, readonly: true})},
		            { item : createAtchFileComp({label:'첨부파일', name:'atch_file_id', id: 'atch_file_id', height: 105, isTitle: false, readonly: true, css: 'margin-left=50px'})}
		        ],
		        hiddens: {
		        	ins_user_id:''
		        }
		    }
		});
		
		//공지사항 팝업창
		var popupWindow = createWindow({
	    	id: "popupWindow",
	    	width: 700,
	    	height: 800,
	    	header: {
		        title: "공지사항 상세보기"
		    },
	    	body: {
	    		rows: new Array().concat(noticeEditorForm)
	    	},
	    	footer: {
	    		buttons: {
		   			align: "center",
		   			items: [
   			        ]
		   		}
		   	},
	    	closable: true
	    });
		
		nkia.ui.utils.ajax({
			url: "/itg/system/board/selectNoticeBoardInfo.do",
			params: {no_id : noti_id, flag : true},
			async: false,
			success: function(result){
				
				if(result.success){
					$$("noticeEditorForm")._syncData(result.resultMap);
					$$("atch_file_id")._getFileList(result.resultMap.ATCH_FILE_ID);
				}
			}
		});
		
		$$("popupWindow").show();
		
	}