function createDivWorkPopComp(formId, readOnly) {
	var itemArray = new Array();
	var textField = null;
	var notNull = false;
	if(readOnly) {
		textField = createTextFieldComp({name : 'div_work_nm', width : 220, notNull : notNull, readOnly : true, readOnlyClass: true});
	} else {
		notNull = true;
		textField = createTextFieldComp({name : 'div_work_nm', width : 220, notNull : notNull, readOnly : true});
	}
	var hiddenField = createHiddenFieldComp({name : 'div_work'});
	var popBtn = createPopBtnComp({handler : function() {
							var theForm = Ext.getCmp(formId);
							var targetIdObj = theForm.getForm().findField("div_work");
							var targetNmObj = theForm.getForm().findField("div_work_nm");
							var popId = "divPop";
							var setPopUpProp = {
								popId : popId,
								treeId : popId + '_treeId',
								popUpTitle : getConstText({
											isArgs : true,
											m_key : '업무분류 선택'}),
								url : '/itg/itam/opms/searchWorkTreeComp.do',
								title : getConstText({
											isArgs : true,
											m_key : '업무분류 선택'}),
								targetField : targetNmObj,
								targetHiddenField : targetIdObj,
								leafSelect : true,
								rootSelect : true,
								expandAllBtn : popId + "__expandAllBtn",
								collapseAllBtn : popId + "__collapseAllBtn",
								expandLevel : 3
							}
							var popUp = createTreeSelectPop(setPopUpProp);
							popUp.show();
						}});
	var clearBtn = createClearBtnComp({handler : function() {
							var theForm = Ext.getCmp(formId);
							theForm.setFieldValue("div_work_nm", "");
							theForm.setFieldValue("div_work", "");
						}});
	itemArray.push(textField);
	itemArray.push(hiddenField);
	if(!readOnly) {
		itemArray.push(popBtn);
		itemArray.push(clearBtn);
	}
							
	var resultComp = createManualFieldcontainer({
						label : '업무분류',
						width : 400,
						notNull : notNull,
						items : itemArray
	});
	
	return resultComp;
}


function createLocCodePopComp(formId) {
	var resultComp = createManualFieldcontainer({
				id : formId + '_Container',
				label : '위치정보',
				label : '위치정보',
				width : 400,
				items : [createTextFieldComp({
								label : 'LOC_CODE',
								name : 'loc_code_nm',
								width : '60%',
								hideLabel : true,
								readOnly : true
							}), createHiddenFieldComp({
								name : 'loc_code'
							}), createPopBtnComp({
								id : formId + '_PopOpBtn',
								handler : function() {
									openAmPopup(this,'LOC_CODE','loc_code','loc_code_nm','I','','');
								}
							}), createPopInfoBtnComp({
								id : formId + '_PopInfoBtn',
								icon : getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/bullet_detail.gif',
								tooltip : "상세보기",
								handler : function() {
									openAmPopup(this,'LOC_CODE','loc_code','loc_code_nm','I','','');
								}
							}), createClearBtnComp({
								id : formId + '_PopClrBtn',
								handler : function() {
									var theForm = Ext.getCmp(formId);
									theForm.setFieldValue("loc_code_nm", "");
									theForm.setFieldValue("loc_code", "");
								}
							})]
	});
	return resultComp;
}

function createAppUserList(compProperty){
	var that 		= convertDomToArray(compProperty);
	var userGrid 	= "";
	var id			= that["id"];
	var title		= that["title"];
	var selModel 	= false;
	var btns 	  	= [];
	var cellModel		= false;
	
	var params = {};
	for(var key in that["params"]) {
		params[key] = that["params"][key]
	}
	
	//추가 버튼 핸들러
	var userListSelectPop = function(){
		createAppUserListPop(compProperty); // TODO 해당 팝업 호출 메소드로 변경
	}

	//삭제 버튼 핸들러
	var removeUserGridRow = function(){
		var selRows 	= gridSelectedRows(id);
		if(isSelected(id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				userGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
		}
	} 
	
	if(that["editorMode"]) {
		var addUserBtn	= createBtnComp({visible: true, label:'추가', id: id + '_addUserBtn'	, icon :'/itg/base/images/ext-js/common/icons/add.png' 		,margin : '0 2 0 0'});
		var delUserBtn	= createBtnComp({visible: true, label:'삭제', id: id + '_delUserBtn'	, icon :'/itg/base/images/ext-js/common/icons/delete.png' 	,margin : '0 0 0 0'});
		btns = [addUserBtn,delUserBtn];
		
		attachBtnEvent(delUserBtn, removeUserGridRow);
		attachBtnEvent(addUserBtn, userListSelectPop);
		cellEditingFlag = true;
	}	
	
	var appUserIdsArr = new Array();
	if(selRecord != null) {
		var appUserIds = selRecord.raw.APP_USER_IDS;
		if(appUserIds != null && appUserIds != "") {
			appUserIdsArr = appUserIds.split(",");
			params["appUserIds"] = appUserIdsArr;
		}
	}
			
	var gridProps = {				
			id					: id
			,title 				: that['title']
			,resource_prefix	: 'grid.process.operate.sub.appoper.pop'
			,url				: getConstValue('CONTEXT') + '/itg/nbpm/provide/skh/searchAppUserList.do' //TODO 해당 서비스로 붙일 것.
			,gridHeight			: 150
			,params				: params
			,autoLoad			: true
			,multiSelect		: true
			,cellEditing		: false
			,selModel			: cellModel
			,pagingBar 			: false
			,editorMode			: true
			,border 			: true
			,toolBarComp		: btns
			,cellEditing		: true
			,remoteSort			: false
	}
	//그리드 생성
	userGrid = createGridComp(gridProps);
	
	return userGrid;
}

function createAppUserListPop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	var id			= that["id"];
	var conf_id		= that["conf_id"];
	var userGrid	= "";
	var selUserGrid = "";
	var window_comp = "";
	var parentGrid 	= Ext.getCmp(id);
	var selParentData = parentGrid.getGridDataUpper();
	
	var searchUserList = function (){
		
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		
		if(selUserGridStore.count() > 0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//추가
	var addSeluser = function (){
		var paramMap = {};
		var selUserGridStore = selUserGrid.store;
		var selRows 	= gridSelectedRows(userGrid.id);
		
		if(isSelected(userGrid.id)){
			Ext.suspendLayouts();

			for(var i=0;i<selRows.length;i++){
				selUserGrid.store.add(selRows[i]);
			}
			var selUserData = selUserGrid.getGridDataUpper();
			var selUserAmount = selUserData.length;
			Ext.resumeLayouts(true);
			selUserGridStore.update();
		}else{
			showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
			return;
		}
		
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//삭제
	var deSeluser = function (){
		var paramMap = {};
		var selUserGridStore = selUserGrid.store;
		var selRows = gridSelectedRows(selUserGrid.id);
		
		if(isSelected(selUserGrid.id)){
			Ext.suspendLayouts();
			
			for(var i=0;i<selRows.length;i++){
				selUserGrid.store.remove(selRows[i]);
			}
			
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
			return;
		}
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//적용
	var confirmMng = function (){
		var selUserData = selUserGrid.getGridDataUpper();
		var useRateTot = 0;
		for(var i=0;i<selUserData.length;i++){
			useRateTot += Number(selUserGrid.selModel.store.data.items[i].data.USE_RATE);
		}
		parentGrid.store.removeAll();
		if(selUserData.length>0){
			Ext.suspendLayouts();
			for(var i=0;selUserData.length>i;i++){
				parentGrid.store.add(selUserData[i]);
			}
			Ext.resumeLayouts(true);
		}
		window_comp.close();
	}
	
	//검색
	var searchUserBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchUserBtn' ,ui:'correct'});
	//적용
	var conFirmBtn	 	= createBtnComp({visible: true,label:"적용" ,id:"conFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	//사용자 추가
	var addSeluserBtn	= createIconBtnComp({id:'addSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif' 	,margin : '2 2 2 0'});
	//사용자 삭제
	var deSeluserBtn	= createIconBtnComp({id:'deSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif' ,margin : '2 0 2 2'});
	
	addSeluserBtn.vline = false;
	deSeluserBtn.vline 	= false;
	
	
	var searchDivNm			= createTextFieldComp({name:'searchDivNm' ,width : 400 , label: '조회분류', readOnly: true, readOnlyClass: true });
	var searchDivId			= createHiddenFieldComp({name:'searchDivId'});
	var setSearchFormProp 	= {
								 id				: id+'SearchForm'
								, columnSize	: 2
								, tableProps : [{colspan:1,tdHeight: 30, item : searchDivNm}, {colspan:1,tdHeight: 30, item : searchDivId}]
							  };
	var searchForm			= createSeachFormComp(setSearchFormProp);
	
	var btnContainer		= createHorizonPanel({align : 'center' ,panelItems : [addSeluserBtn,deSeluserBtn]});
		
	var setTreeProp = {
		id : id + 'Tree',
		url : getConstValue('CONTEXT') + '/itg/itam/opms/searchHISOSTreeComp.do',
		title : "HISOS분류",
		dynamicFlag : false,
		searchable : true,
		height : 490,
		expandAllBtn : false,
		collapseAllBtn : false,
		params : that['params'],
		rnode_text : getConstText({
			isArgs : true,
			m_key : 'res.00003'
		}),
		expandLevel : 2
	}
	var hisosTree = createTreeComponent(setTreeProp);
	
	function treeNodeClick(store, record, index, eOpts) {
		var treeComp = Ext.getCmp(id + 'Tree');
		var valueMap = treeComp.getSelectedTreeHierarchiValue();
		searchDivNm.setValue(valueMap.node_nm);
		searchDivId.setValue(valueMap.node_id);
		var param = {};
		var otherData = record.raw.otherData;
		var nodeLevel = otherData.NODE_LEVE;
		var nodeId = record.raw.node_id;
		var nodeIdArr = nodeId.split("_");
		if(otherData.NODE_LEVEL == 2) {
			param["UP_CODE"] = nodeIdArr[0];
		} else if(otherData.NODE_LEVEL == 3) {
			param["SUB_CODE"] = nodeIdArr[1];
		} else if(otherData.NODE_LEVEL == 4) {
			param["FAB_CODE"] = nodeIdArr[2];
		} else {
			
		}
		if(selUserGrid.getStore().count()>0){
			param["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(param);
	}
	
	attachCustomEvent('select', hisosTree, treeNodeClick);
	
	var left_panel = Ext.create('Ext.panel.Panel', {
		region : 'west',
		height : '100%',
		autoScroll : true,
		items : [ hisosTree ]
	});

	//담당자 리스트
	var setUserGridProp 	= {	 
									 id					: id+"UserList"
									,title				: '사용자정보'
									,gridHeight			: 210
									,resource_prefix	: 'grid.process.operate.sub.appoper.pop'
									,url				: getConstValue('CONTEXT') + '/itg/itam/opms/selectOpmsUserList.do'//사용자정보 관련 서비스 붙일 것.
									,params				: {selUserList : selParentData}
									,pagingBar 			: true
									,multiSelect		: true
									,selModel			: true
									,autoScroll			: true
									,selfScroll			: true
									,border 			: true
									,pageSize 			: 5
								};
	
	userGrid = createGridComp(setUserGridProp);
	
	//선택된 담당자 리스트
	var setSelGridProp 	= {	 
									 id					: id+"SelUserList"
									,title				: '사용자정보'
									,gridHeight			: 190
									,resource_prefix	: 'grid.process.operate.sub.appoper.pop'
									,isMemoryStore		: true
									,pagingBar 			: false
									,selModel			: true
									,multiSelect		: true
									,border 			: true
									,cellEditing		: true
									,autoScroll			: true
									,selfScroll			: true
									};
	
	selUserGrid = createGridComp(setSelGridProp);
	
	
	//그리드 로드시.. 선택된 사용자 정보를 셋해준다.. 팝업이 열리고 최초 한번만..
	var selUserGirdLoadFn = function(){
										if(selParentData.length>0){
											for(var y=0 ; y < selParentData.length ; y++){
												selUserGrid.store.add(selParentData[y]);
											}
										}
										selUserGrid.store.un("load",selUserGirdLoadFn);
									};
	
	selUserGrid.store.on("load",selUserGirdLoadFn);

	// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
	var right_panel = Ext.create('Ext.panel.Panel', {
		border : false,
		region : 'center',
		flex : 1,
		items : [searchForm,userGrid,btnContainer,selUserGrid]
	});

	var complexProp = {
		height : '100%',
		panelItems : [ {
			flex : 1.2,
			items : left_panel
		}, {
			flex : 3.3,
			items : right_panel
		} ]
	};
	var complexPanel = createHorizonPanel(complexProp);
	
	var codePopupProp		= {
											id:   id+"Pop"
											,title: '사용자선택'
											,width: 950
											,height: 568
											,modal: true
											,layout: 'fit'
											,buttonAlign: 'center'
											,bodyStyle: 'background-color: white; '
											,buttons: [conFirmBtn]
											,closeBtn : true
											,items: complexPanel
											,resizable:true
									  };
	
	window_comp = createWindowComp(codePopupProp);
	//버튼 이벤트
	
	
	attachBtnEvent(conFirmBtn		,confirmMng);
	//attachBtnEvent(searchUserBtn	,searchUserList);
	attachBtnEvent(addSeluserBtn	,addSeluser);
	attachBtnEvent(deSeluserBtn		,deSeluser);
	
	window_comp.show();
}

function createOperSelGrid(setGridProp){
		
	var id = setGridProp["id"];
	var btns = null;
	var cellEditingFlag = false;	
	if(setGridProp["editorMode"]) {
		var addMngBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.append' }), id: id+'_addMngBtn'	, icon :'/itg/base/images/ext-js/common/icons/add.png' 		,margin : '0 2 0 0'});
		var delMngBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.remove' }), id: id+'_delMngBtn'	, icon :'/itg/base/images/ext-js/common/icons/delete.png' 	,margin : '0 0 0 0'});
		btns = [addMngBtn,delMngBtn];
		attachBtnEvent(delMngBtn, removeGridRow, id + '_OperSelList');
		attachBtnEvent(addMngBtn, createMngUserListPop, id + '_OperSelList');
		cellEditingFlag = true;
	}
	
	var params = {};
	for(var key in setGridProp["params"]) {
		params[key] = setGridProp["params"][key]
	}
	
	var userIdsArr = new Array();
	var mainYnArr = new Array();
	if(selRecord != null) {
		var userIds = selRecord.raw.OPER_USER_IDS;
		if(userIds != null && userIds != "") {
			userIdsArr = userIds.split(",");
			params["userIds"] = userIdsArr;
		}
		
		var mainYn = selRecord.raw.OPER_MAIN_YN;
		if(mainYn != null && mainYn != "") {
			mainYnArr = mainYn.split(",");
		}
	}
	
	var mainYNCombo = createCodeComboBoxComp({name:'MAIN_YN',code_grp_id:'MAIN_YN'	,width : 60,hideLabel : true, attachChoice:true, notNull: true});
	var setGridProp = {						// Context
			id: id + '_OperSelList',							// Grid id
			title: setGridProp["title"],		// Grid Title
			//height: getConstValue('TALL_GRID_HEIGHT'),
			//anchor: '100% 50%',							// Grid Anchor(width, height)
			resource_prefix: 'grid.process.operate.sub.oper',	// Resource Prefix
			url: getConstValue('CONTEXT') + '/itg/base/selectedUserList.do',	// Data Url
			params: params,
			gridHeight : 160,
			border : true,
			autoLoad : true,
			multiSelect	: true,
			editorMode : true,
			remoteSort : false,
			cellEditing : cellEditingFlag,
			comboComp  : {MAIN_YN : mainYNCombo},
			toolBarComp	: btns
		};
	
	var grid = createGridComp(setGridProp);
	grid.getStore().on("load", function(store, records, successful, eOpts) {
		for(var i = 0 ; i < records.length ; i++) {
			if(userIdsArr.length > 0) {
				for(var j = 0 ; j < userIdsArr.length ; j++) {
					if(records[i].raw.USER_ID == userIdsArr[j]) {
						records[i].set("MAIN_YN", mainYnArr[j]);
					}
				}
			}
		}
	});
	return grid;
}

function createMngUserListPop(id){
	var userGrid	= "";
	var selUserGrid= "";
	var window_comp= "";
	var parentGrid 	= Ext.getCmp(id);
	var selParentData= parentGrid.getGridDataUpper();
	var searchUserList = function (){
		
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		/*
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		*/
		userGrid.searchList(paramMap);
	}
	
	var addSeluser = function (){
		var paramMap = searchForm.getInputData();
		var selUserGridStore = selUserGrid.store;
		var selRows 	= gridSelectedRows(userGrid.id);
		
		if(isSelected(userGrid.id)){
			Ext.suspendLayouts();
			
			checkGridRowCopyToGrid(userGrid.id, selUserGrid.id, {
				primaryKeys : ["USER_ID"]
			});
			
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }),type : "ALERT"});
			return;
		}
		
		/*
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
		*/
	}
	
	var deSeluser = function (){
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		var selRows 	= gridSelectedRows(selUserGrid.id);
		
		if(isSelected(selUserGrid.id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				selUserGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }) ,type : "ALERT"});
			return;
		}
		/*
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
		*/
	}
	
	var confirmMng = function (){
		parentGrid.store.removeAll();
		var selUserData = selUserGrid.getGridDataUpper();
		if(selUserData.length>0){
			Ext.suspendLayouts();
			for(var i=0;selUserData.length>i;i++){
				if(i == 0) {
					selUserData[i].MAIN_YN = "Y";
				} else {
					selUserData[i].MAIN_YN = "N";
				}
				parentGrid.store.add(selUserData[i]);
			}
			Ext.resumeLayouts(true);
		}
		window_comp.close();
	}
	
	var searchUserBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchUserBtn' ,ui:'correct'});
	var conFirmBtn	 	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }) ,id:"conFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	var addSeluserBtn	= createIconBtnComp({id:'addSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif' 	,margin : '2 2 2 0'});
	var deSeluserBtn	= createIconBtnComp({id:'deSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif' ,margin : '2 0 2 2'});
	
	addSeluserBtn.vline = false;
	deSeluserBtn.vline 	= false;
	
	//검색 ==
	//검색조건
	var searchKey		= createCodeComboBoxComp({name:'searchKey'	,code_grp_id:'SEARCH_USER'	,hideLabel : true 	,width : 100 ,attachAll : true});
	//검색값
	var searchValue	= createTextFieldComp({name:'searchValue'	,padding 	: '0 0 0 5' ,width : 200 ,hideLabel : true });
	//컨테이너
	var searchContainer		= createManualFieldcontainer({label : getConstText({ isArgs: true, m_key: 'btn.common.search' }) ,items : [searchKey,searchValue] ,id : "searchContainer"});
	
	var setSearchFormProp 	= {
								 id				: id+'SearchForm'
								,columnSize	: 1
								,formBtns	: [searchUserBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [{colspan:1, tdHeight: 30, item : searchContainer}]
								,enterFunction : searchUserList
							  }
	//검색폼
	var searchForm	= createSeachFormComp(setSearchFormProp);
	//추가,삭제 버튼 컨테이너
	var btnContainer		= createHorizonPanel({align : 'center' ,panelItems : [addSeluserBtn,deSeluserBtn]})
	
	var setTreeProp = {
		id : id + 'Tree',
		url : '/itg/system/dept/searchStaticAllDeptTreeComp.do',
		title : getConstText({
			isArgs : true,
			m_key : 'res.00013'
		}),
		dynamicFlag : false,
		searchable : true,
		height : 500,
		expandAllBtn : false,
		collapseAllBtn : false,
		params : null,
		rnode_text : getConstText({
			isArgs : true,
			m_key : 'res.00003'
		}),
		expandLevel : 2
	}
	var deptTree = createTreeComponent(setTreeProp);
	deptTree['syncSkip'] = true;
	attachCustomEvent('select', deptTree, treeNodeClick);
	
	function treeNodeClick(tree, record, index, eOpts) {
		var cust_id = record.raw.node_id;
		var grid_store = getGrid(id+"UserList").getStore();
		var treeParams = {};
		treeParams["cust_id"] = cust_id;
		grid_store.proxy.jsonData = treeParams;
		grid_store.reload();
	}
	
	// 좌측 패널 : Tree
	var left_panel = Ext.create('Ext.panel.Panel', {
		region : 'west',
		height : '100%',
		autoScroll : true,
		items : [ deptTree ]
	});	
	//담당자 리스트
	var setUserGridProp 	= {	 
									 id					: id+"UserList"
									,title				: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00011' })
									,gridHeight			: 192
									,resource_prefix	: 'grid.process.operate.sub.oper.pop'
									,url				: '/itg/itam/opms/selectOpmsMngUserList.do'
									,params				: {cust_id: getConstValue('LOGIN_USER_CUST_ID')}
									,pagingBar 			: true
									,multiSelect		: true
									,selModel			: true
									,border 			: true
									,pageSize 			: 5
								};
	
	userGrid = createGridComp(setUserGridProp);
	
	//선택된 담당자 리스트
	var setSelGridProp 	= {	 
									 id					: id+"SelUserList"
									,title				: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00011' })
									,gridHeight			: 192
									,resource_prefix	: 'grid.process.operate.sub.oper.pop.sel'
									,isMemoryStore		: true
									,pagingBar 			: false
									,selModel			: true
									,multiSelect		: true
									,border 			: true
									};
	
	selUserGrid = createGridComp(setSelGridProp);
		
	//그리드 로드시.. 선택된 사용자 정보를 셋해준다.. 팝업이 열리고 최초 한번만..
	var selUserGirdLoadFn = function(){
		selUserGrid.store.removeAll();
		if(selParentData.length > 0){
			for(var i = 0 ; i < selParentData.length ; i++){ 
				selUserGrid.store.add(selParentData[i]);
			}
		}
		selUserGrid.store.un("load",selUserGirdLoadFn);
	}
	
	selUserGrid.store.on("load",selUserGirdLoadFn);
	
	
	var flexPanel = createFlexPanel({items : [searchForm,userGrid,btnContainer,selUserGrid]});
	
	var complexProp = { height : '100%', panelItems : [ { flex : 0.9, items : left_panel }, { flex : 3.0, items : flexPanel} ]};	
	var complexPanel = createHorizonPanel(complexProp);
	
	var codePopupProp		= {
											id:   id+"Pop"
											,title: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00012' })
											,width: 850
											,height: 568
											,modal: true
											,layout: 'fit'
											,buttonAlign: 'center'
											,bodyStyle: 'background-color: white; '
											,buttons: [conFirmBtn]
											,closeBtn : true
											,items: complexPanel
											,resizable:true
								}
	
	window_comp = createWindowComp(codePopupProp);
	//버튼 이벤트
	
	
	attachBtnEvent(conFirmBtn		,confirmMng);
	attachBtnEvent(searchUserBtn	,searchUserList);
	attachBtnEvent(addSeluserBtn	,addSeluser);
	attachBtnEvent(deSeluserBtn		,deSeluser);
	
	window_comp.show();
}

//사용할지 말지 미지수
function createLogicalServerGrid(setGridProp){
	var id = setGridProp["id"];
	var btns = [];

	var params = {};
	for(var key in setGridProp["params"]) {
		params[key] = setGridProp["params"][key]
	}
	
	var cellEditingFlag = false;
	if(setGridProp["editorMode"]) {
		cellEditingFlag = true;	
	}
	
	var operStateCombo = createCodeComboBoxComp({name:'OPER_STATE',code_grp_id:'OPER_STATE'	,width : 60,hideLabel : true, attachChoice:true, notNull: true});
	var confId = selRecord.raw.CONF_ID;
	params["conf_id"] = confId;
	
	var l_serverId = selRecord.raw.LOGIC_SERVER_IDS;
	var l_serverIdArr = new Array();
	if(l_serverId != null && l_serverId != "") {
		l_serverIdArr = l_serverId.split(",");
	}
	
	var l_serverOperState = selRecord.raw.LOGIC_SERVER_OPER_STATES;
	var l_serverOperStateArr = new Array();
	if(l_serverOperState != null && l_serverOperState != "") {
		l_serverOperStateArr = l_serverOperState.split(",");
	}	
	
	var setGridProp = {						// Context
			id: id + "_logicalServerGrid",							// Grid id
			title: "논리서버",		// Grid Title
			//height: getConstValue('TALL_GRID_HEIGHT'),
			//anchor: '100% 50%',							// Grid Anchor(width, height)
			resource_prefix: 'grid.process.operate.sub.logicserver',	// Resource Prefix
			url: getConstValue('CONTEXT') + '/itg/nbpm/provide/skh/searchLogicalServerInPhysicalList.do',	// Data Url
			params: params,
			gridHeight : 160,
			comboComp : {OPER_STATE : operStateCombo},
			border : true,
			autoLoad : true,
			editorMode : true,
			cellEditing : cellEditingFlag,
			toolBarComp	: btns
		};	
	var grid = createGridComp(setGridProp);
	grid.getStore().on("load", function(store, records, successful, eOpts) {
		for(var i = 0 ; i < records.length ; i++) {
			if(l_serverIdArr.length > 0) {
				for(var j = 0 ; j < l_serverIdArr.length ; j++) {
					if(records[i].raw.CONF_ID == l_serverIdArr[j]) {
						records[i].set("OPER_STATE", l_serverOperStateArr[j]);
					}
				}
			}
		}
	});	
	return grid;
}

function comboUpCodeSelect(){
	
	var sel_up_code = Ext.getCmp('search_up_code').value;
	var sub_code_cmp = Ext.getCmp('search_sub_code');
	sub_code_cmp.store.proxy.jsonData['sel_up_code'] = sel_up_code;
	sub_code_cmp.store.load();
	sub_code_cmp.setValue("");
}

function contractComboBox(id, codeType, label, name){
	var param = {};
		param['code_type'] = codeType;
		
	var contract_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/itam/opms/searchCodeDataList.do', //코드 서비스 붙일 것.
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var contractProp_val = {label: label, id: id, name: name, width: 250, attachAll:true};
	contractProp_val['proxy'] = contract_proxy;
	contractProp_val['valueField'] = 'CODE_ID';
    contractProp_val['displayField'] = 'CODE_TEXT';
	    
	var contractComboBox =  createComboBoxComp(contractProp_val);
	   
	return contractComboBox;
}

function showAssetDetailPop(param){
	//var url = "/itg/itam/opms/opmsDetailInfo.do" + param;
	var url = "/itg/itam/automation/goAssetDetailViewer.do" + param;
	window.open(getConstValue('CONTEXT') +url, "assetPop", "width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

function removeGridRow(gridId){
	var selGrid = Ext.getCmp(gridId);
	var selRows = gridSelectedRows(gridId);
	if(isSelected(gridId)){
		Ext.suspendLayouts();
		for(var i = 0 ; i < selRows.length ; i++){
			selGrid.store.remove(selRows[i]);
		}
		Ext.resumeLayouts(true);
	}else{
		showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
	}
}

function checkOperUser(selOperGridId, rowData, reqType) {
	if(reqType == "OPERATE" || reqType == "REUSE") {
		var selOperGrid = Ext.getCmp(selOperGridId);
		var operUserData = null
		if(selOperGrid != null) {
			operUserData =  selOperGrid.getGridDataUpper();
			if( !(operUserData != null && operUserData.length > 0) ) {
				alert("운영자가 지정되지 않았습니다.")
				return false;
			}
		}
		var operUserIds = "";
		var operUserNms = "";
		var mainYns = "";
		if(operUserData != null ) {
			var mainYCount = 0; //정담당자 카운트
			for(var i = 0 ; i < operUserData.length ; i++) {
				var operInfo = operUserData[i];
	
				var mainYn = operInfo.MAIN_YN;
				if(mainYn != null && mainYn != "") {
					if(mainYn == "Y") {
						mainYCount++;
					}
				} else {
					alert("[정]/[부] 담당자 정보를 입력하여 주세요.");
					return false;
				}
				operUserIds = operUserIds + operInfo.USER_ID;
				operUserNms = operUserNms + operInfo.USER_NM
				mainYns = mainYns + operInfo.MAIN_YN;
				if(i < operUserData.length - 1) {
					operUserIds = operUserIds + ",";
					operUserNms = operUserNms + ",";
					mainYns = mainYns + ",";
				}
			}
			if(mainYCount != 1) {
				alert("[정] 담당자는 반드시 1명 이어야 합니다.");
				return false;					
			}
			rowData["OPER_USER_IDS"] = operUserIds;
			rowData["OPER_USER_NMS"] = operUserNms;
			rowData["OPER_MAIN_YN"] = mainYns;
		}
	}
	return true;
}

function checkAppUserGrid(appUserGridId, rowData, reqType) {
	if(reqType == "OPERATE" || reqType == "REUSE") {
		var appUserGrid = Ext.getCmp(appUserGridId);
		var appUserData = null;
		if(appUserGrid != null) {
			appUserData =  appUserGrid.getGridDataUpper();
			if( !(appUserData != null && appUserData.length > 0)) {
				alert("APPLICATION 담당자가 지정되지 않았습니다.")
				return false;
			}
		}
		var appUserIds = "";
		var appUserNms = "";
		if(appUserData != null ) {
			for(var i = 0 ; i < appUserData.length ; i++) {
				var appUserInfo = appUserData[i];
				var app_user_id = appUserInfo.UP_CODE + '_' + appUserInfo.SUB_CODE + '_' + appUserInfo.FAB_CODE;
				appUserIds = appUserIds + app_user_id;
				if(i < appUserData.length - 1) {
					appUserIds = appUserIds + ",";
				}
			}
			rowData["APP_USER_IDS"] = appUserIds;
		}
	}
	return true;
}


function createLocCodePop(formId, targetId, targetNm, view_type) {
	var theForm = Ext.getCmp(formId);
	var targetIdObj = theForm.getForm().findField(targetId);
	var targetNmObj = theForm.getForm().findField(targetNm);
	if(view_type == 'V'){
		var fieldValue = targetIdObj.getValue(); 

		if(fieldValue == null || fieldValue == ""){
			alert("설치위치를 등록해주세요.");
			return false;
		}
		var gubun = 'sel'; 
		if(fieldValue.length == 8){
			gubun = 'all';
		}
		
		window.open("/itg/itam/topView/topViewPop.do?loc_plan_code="+fieldValue+"&gubun="+gubun,
				"topViewPop", "width=1150,height=600,history=no,resizable=no,status=no,scrollbars=yes,member=no");			
	}else{
		var setPopUpProp = {
				id: formId + '_LocSelectPop',
            	tagetIdField: targetIdObj,
            	tagetNameField: targetNmObj,
            	view_type: view_type,
    			height: 507,								
                width: 650,
    			buttonAlign : 'center',
    			bodyStyle: 'background-color: white;'
		}
		var popUp = locationSelectPop(setPopUpProp);
		popUp.show();
	}
}
