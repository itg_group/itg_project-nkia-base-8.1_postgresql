//추가 팝업
function addPop(){
	//사용자조회 폼
	var addPopupForm = createFormComp({
		id: 'addPopupForm',
		elementsConfig: {
    		labelPosition: "left" // 라벨 좌측위치를 위해 입력, 검색폼은 왼쪽 정렬로 사용합니다.
    	},
		header: {
			title: '사용자 조회',
			icon: "search"
		},		
		fields:{
			colSize: 2,
			items: [
					{
						colspan: 2, 
						item : createUnionFieldComp({
							items: [
							createCodeComboBoxComp({
								label: getConstText({isArgs : true, m_key : 'res.common.search'}),
								name: 'search_type',
								code_grp_id: 'SEARCH_CONDITION',
								attachAll: true,
								width: 250
							}),
							createTextFieldComp({
								name: "search_value",
								placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
								on:{
									onKeyPress: function(keycode) {
										// Webix API에서 onKeyPress 이벤트 호출시 press
										// 된 key의 값을 인자값으로 전달해줍니다.
										pressEnterKeyPop(keycode); // 엔터키 이벤트 구현을
																// 위한 함수
									}
								}
							})
						]
					})
				}
	       ]      
		},
		footer:{
			buttons: {
				align: "center",
				items:[
				       createBtnComp({visible: true, label: getConstText({isArgs : true, m_key : 'res.common.search'}), type: 'form', id: 'searchUserPop', click: searchUserPop}),
				       createBtnComp({visible: true, label: getConstText({isArgs : true, m_key : 'btn.common.reset'}), id: 'searchInitAddPop', click: searchInitAddPop})
				]
			}
		}
	});	
	
	//사용자목록 그리드
	var userListPop = createGridComp({
		id: 'userListPop',
		keys: [],
		resizeColumn: true,
		pageable: true,
		//pageSize: 10,
		checkbox: false,
		url: '/itg/nbpm/common/searchProcessOperUserList.do',
		resource: 'grid.popup.user',
		params: {},
		header: {
			title: '사용자 목록'
 		}
	});
	
	//변경정보 폼
	var changeInfoForm = createFormComp({
		id: 'changeInfoForm',
		elementsConfig: {
			labelWidth: 110,
    		labelPosition: "left" // 라벨 좌측위치를 위해 입력, 검색폼은 왼쪽 정렬로 사용합니다.
    	},
		header: {
			title: '변경정보'
		},fields:{
			colSize: 2,
			items: [
			        {colspan: 1, item: createTextFieldComp({label:'변경 전 담당자', name: 'before_charger_nm', required: true, readonly: true})},
			        {colspan: 1, item: createTextFieldComp({label:'변경 후 담당자', name: 'after_charger_nm', required: true, readonly: true})}
			        ],
			        hiddens: {
						before_charger_id: '',
						after_charger_id: ''
					}   
		}
	});	
	
	var UserPopupWindow = createWindow({
    	id: "UserPopupWindow",
    	width: 800,
    	height: 650,
    	header: {
    		title: "사용자 조회"
    	},
    	body: {
    		//rows: new Array().concat(addPopupForm,userListPop,changeInfoForm)
    		rows: [
    		       {
    		    	   rows: new Array().concat(addPopupForm,userListPop)   	   
    		       },{
    		    	   cols: [
			    	    	  { // 화살표 버튼
    			    	    	  view: "template",
    			    	    	  id: "arrowBefore",
    			    	    	  template: nkia.ui.html.icon("arrow_down", "add_data"),		    	    	  
    			    	    	  height: 30,
    							  onClick: { // 이벤트 연결
//    							 	"add_data": "addData('before')"
									  "add_data": function(){
										  addData('before');  
									  }
    							  }
    			    	      },{ // 화살표 버튼
    			    	    	  view: "template",
    			    	    	  id: "arrowAfter",
    			    	    	  template: nkia.ui.html.icon("arrow_down", "add_data"),		    	    	  
    			    	    	  height: 30,
    							  onClick: { // 이벤트 연결
    							 	//"add_data": "addData('after')"
    								  "add_data": function(){
										  addData('after');  
									  }
    							  }
    			    	      }
    	    	          ]
    				   },{
    					   rows: new Array().concat(changeInfoForm)
    				   }
    	       ]
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
    				createBtnComp({label: '적 용', type: "form", click: function() {
    					if($$("changeInfoForm")._getFieldValue("before_charger_nm")=='' || $$("changeInfoForm")._getFieldValue("after_charger_nm")==''){
    						nkia.ui.utils.notification({
    							type: 'error',
    							message: '필수 항목을 확인해주세요.'
    						});
    						return false;
    					}
    					if($$("changeInfoForm")._getFieldValue("before_charger_id") == $$("changeInfoForm")._getFieldValue("after_charger_id")){
    						nkia.ui.utils.notification({
    							type: 'error',
    							message: '변경전 담당자와 변경후 담당자가 동일합니다.'
    						});
    						return false;
    					}
    					var changeRecord = $$("changeInfoForm")._getValues();
    					var sendRow = [];
    					var sendData =  {};
    					
    					sendData['BEFORE_CHARGER_ID'] 	= changeRecord['before_charger_id'];
    					sendData['BEFORE_CHARGER_NM'] 	= changeRecord['before_charger_nm'];
    					sendData['AFTER_CHARGER_ID'] 	= changeRecord['after_charger_id'];
    					sendData['AFTER_CHARGER_NM']	= changeRecord['after_charger_nm'];
    					
    					sendRow[0] = sendData;
    					$$("chargerEditGrid")._addRow(sendRow);    					
    					UserPopupWindow.close();
    				}})
    			]
    		}
    	},
    	closable: true
    });
	
	// 팝업창 오픈
	$$("UserPopupWindow").show();
	
	//변경담당자 추가 팝업 검색
	function searchUserPop(){
		// 검색폼의 값들 추출
		var data = $$("addPopupForm")._getValues();
		
		// 검색폼의 데이터를 인자값으로 그리드 리로드
		$$("userListPop")._reload(data);
	}
	
	function pressEnterKeyPop(keyCode){
		if(keyCode){
			if(keyCode == "13"){ //enter key
				searchUserPop();
			}	
		}
	}
	
	//화살표이벤트
	function addData(data){
		var selectedData = $$("userListPop")._getSelectedRows();
		
		// 선택 한 데이터가 없을 시, 메시지를 띄워줍니다.
		if(selectedData.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '#springMessage("msg.itam.opms.00010")'
			});
			return false;
		}
		
		var userSet = {};
		
		if(data == 'before'){
			var compareValue = false;
			var chargerEditGridUser = $$("chargerEditGrid")._getRows();
			for(var i = 0; i<chargerEditGridUser.length; i++){
				if(selectedData[0].USER_ID == chargerEditGridUser[i].BEFORE_CHARGER_ID){
					compareValue = true;
					break;
				}
			}
			
			if(compareValue){
				nkia.ui.utils.notification({
					type: 'error',
					message: '이미 등록된 담당자 입니다.'
				});
				return false;
			}
			
			/*if(selectedData[0].USER_ID == $$("changeInfoForm")._getFieldValue("after_charger_id")){
				nkia.ui.utils.notification({
					type: 'error',
					message: '변경전 담당자와 변경후 담당자가 동일합니다.'
				});
				return false;
			}*/
			
			userSet['before_charger_id']   = selectedData[0].USER_ID;
			userSet['before_charger_nm'] = selectedData[0].USER_NM;
			$$("changeInfoForm")._setValues(userSet);
			
		}else if(data == 'after'){
			/*if(selectedData[0].USER_ID == $$("changeInfoForm")._getFieldValue("before_charger_id")){
				nkia.ui.utils.notification({
					type: 'error',
					message: '변경전 담당자와 변경후 담당자가 동일합니다.'
				});
				return false;
			}*/
			
			userSet['after_charger_id']   = selectedData[0].USER_ID;
			userSet['after_charger_nm'] = selectedData[0].USER_NM;
			$$("changeInfoForm")._setValues(userSet);
		}
	}
	
	function searchInitAddPop(){
		$$("addPopupForm")._reset();
	}
}



//이력조회 팝업
function chargerHistoryListPop(){
	//사용자조회 폼
	var historyPopupForm = createFormComp({
		id: 'historyPopupForm',
		elementsConfig: {
    		labelPosition: "left" // 라벨 좌측위치를 위해 입력, 검색폼은 왼쪽 정렬로 사용합니다.
    	},
		header: {
			title: '일괄변경 이력조회',
			icon: "search"
		},		
		fields:{
			colSize: 2,
			items: [
					{
						colspan: 2, 
						item : createUnionFieldComp({
							items: [
							createCodeComboBoxComp({
								label: getConstText({isArgs : true, m_key : 'res.common.search'}),
								name: 'search_type',
								code_grp_id: 'HISTORY_SEARCH',
								attachAll: true,
								width: 250
							}),
							createTextFieldComp({
								name: "search_value",
								placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
								on:{
									onKeyPress: function(keycode) {
										// Webix API에서 onKeyPress 이벤트 호출시 press
										// 된 key의 값을 인자값으로 전달해줍니다.
										pressEnterKeyHistory(keycode); // 엔터키 이벤트 구현을
																// 위한 함수
									}
								}
							})
						]
					})
				}
	       ]      
		},
		footer:{
			buttons: {
				align: "center",
				items:[
				       createBtnComp({visible: true, label: getConstText({isArgs : true, m_key : 'res.common.search'}), type: 'form', id: 'searchHistoryPop', click: searchHistoryPop}),
				       createBtnComp({visible: true, label: getConstText({isArgs : true, m_key : 'btn.common.reset'}), id: 'searchInitHistoryPop', click: searchInitHistoryPop})
				]
			}
		}
	});	
	
	//일괄변경 이력목록 그리드
	var historyListPop = createGridComp({
		id: 'historyListPop',
		keys: [],
		resizeColumn: true,
		pageable: true,
		//pageSize: 10,
		checkbox: false,
		url: '/itg/nbpm/edit/searchChargerHistoryList.do',
		resource: 'grid.nbpm.charger.history',
		params: {},
		header: {
			title: '일괄변경 이력목록',
 		}
	});
	
	var HistoryPopupWindow = createWindow({
    	id: "HistoryPopupWindow",
    	width: 800,
    	height: 650,
    	header: {
    		title: '일괄변경 이력조회',
    	},
    	body: {
    		rows: [
    		       {
    		    	   rows: new Array().concat(historyPopupForm,historyListPop)   	   
    		       }
    	       ]
    	},
    	closable: true
    });
	
	// 팝업창 오픈
	$$("HistoryPopupWindow").show();
	
	//일괄변경 이력조회 팝업 검색
	function searchHistoryPop(){
		// 검색폼의 값들 추출
		var data = $$("historyPopupForm")._getValues();
		
		// 검색폼의 데이터를 인자값으로 그리드 리로드
		$$("historyListPop")._reload(data);
	}
	
	function pressEnterKeyHistory(keyCode){
		if(keyCode){
			if(keyCode == "13"){ //enter key
				searchHistoryPop();
			}	
		}
	}
	
	function searchInitHistoryPop(){
		$$("historyPopupForm")._reset();
	}
	
}