// Form invalid clear
var OPER_USER_POP_HEIGHT = 429;

function clearAllInvaildForm(presentPanelList) {
	if (presentPanelList.length > 0) {
		for (var i = 0; i < presentPanelList.length; i++) {
			var panel = presentPanelList[i];
			if (panel.xtype != "tabpanel" && panel.xtype != "gridpanel") {
				panel.getForm().clearInvalid();
			}
		}
	}
}

function setProcessData(properties) {
	var srId = properties['sr_id'];
	var taskId = properties['task_id'];
	var panelList = properties['panelList'];
	var queryKey = properties['queryKey'];
	var subQueryKey = properties['subQueryKey'];
	var params = {};
	var url = getConstValue('CONTEXT') + '/itg/nbpm/selectProcessDetail.do';
	params['sr_id'] = srId;
	params['task_id'] = taskId;
	params['nbpm_task_id'] = taskId;
	params['queryKey'] = queryKey;
	params['subQueryKey'] = subQueryKey;

	if (PROCESS_META.MULTI_YN == "Y") {
		params['task_group_id'] = PROCESS_META.TASK_GROUP_ID;
	}

	var reqType = "";
	for (var i = 0; i < panelList.length; i++) {
		if (panelList[i] != null && panelList[i].getHiddenValue != null
				&& panelList[i].getHiddenValue("req_type") != null) {
			reqType = panelList[i].getHiddenValue("req_type");
		}
	}
	params['req_type'] = reqType;

	if (properties['relationYn'] == "true") {
		// 신규등록과 연계요청을 구분하여 파라메터처리를 다르게 한다.
		if (parent.checkRequestPop != null && parent.checkRequestPop()) {
			parent.addRequestParam(params);
		} else {
			parent.addRelationParam(params);
			var refUrl = params['ref_url'];
			if (!(refUrl != null && refUrl != "")) {
				// 프로세스에서 연계되는 경우
				url = getConstValue('CONTEXT')
						+ '/itg/nbpm/selectParentProcessDetail.do';
				for (var i = 0; i < panelList.length; i++) {
					if (panelList[i] != null
							&& panelList[i].isProcessGrid != null
							&& panelList[i].isProcessGrid) {
						panelList[i].getStore().proxy.jsonData["sr_id"] = params.parent_sr_id;
					}
				}
			} else {
				// 프로세스 외적으로(커스텀 선행프로세스 기능 같은 곳에서 내용을 가져와야 하는 경우)
				url = refUrl;
			}
		}
	}
	jq.ajax({
		type : "POST",
		url : url,
		contentType : "application/json",
		dataType : "json",
		async : false,
		data : getArrayToJson(params),
		success : function(result) {
			var data = result.resultMap;
			if (data != null) {
				var commentList = data.nbpm_commentList;
				var operList = data.operList;
				for (var i = 0; i < panelList.length; i++) {
					if (panelList[i] != null) {
						if (panelList[i].isForm) {
							data.operList = null;
							panelList[i].setDataForMap(data);

							for (var j = 0; operList != null && j < operList.length; j++) {
								var operFieldContainer = panelList[i].getFormFieldContainer(operList[j].OPER_TYPE);
								// 현재 타스크가 멀티 타스크의 한 티켓을 경우
								if (operFieldContainer != null && !(operFieldContainer.reassign)) {
									if(operList[j].TASK_GROUP_ID) {
										if(operList[j].TASK_GROUP_ID.toString() === PROCESS_META.TASK_GROUP_ID) {
											operFieldContainer.operUserField.setValue(operList[j].OPER_USER_NM);
											operFieldContainer.operUserIdField.setValue(operList[j].OPER_USER_ID);
										}
									} else {
										operFieldContainer.operUserField.setValue(operList[j].OPER_USER_NM);
										operFieldContainer.operUserIdField.setValue(operList[j].OPER_USER_ID);											
									}
								}
							}

							if (commentList && panelList[i].getFormField("nbpm_comment") != null) {
								for (var j = 0; j < commentList.length; j++) {
									if (commentList[j].TEXT != null
											&& commentList[j].TEXT != ""
											&& panelList[i].taskName == commentList[j].TASK_NAME) {
										panelList[i].setFieldValue(
												"nbpm_comment",
												commentList[j].TEXT);
										commentList.splice(j, 1);
										break;
									}
								}
							}
						}
						if (panelList[i].isTab) {
							var tabs = panelList[i];
							var tabsLength = tabs.items.items.length;
							var itemList = new Array();
							if (tabsLength > 0) {
								for (var j = 0; j < tabsLength; j++) {
									var tabInnerPanel = tabs.items.items[j];
									if (tabInnerPanel.id == "atch_file_id_grid") {
										tabInnerPanel.atch_file_id = data["ATCH_FILE_ID"]
										tabInnerPanel.countFileList();
										// setFileGrid.countFileList();
									}
									if (tabInnerPanel.isForm) {
										tabInnerPanel.setDataForMap(data);
									}
								}
							}
						}
					}
				}
			}
		}
	});
}

/**
 * getProcessData
 * 
 * @param {}
 *            properties
 * @return {}
 */
function getProcessData(properties) {
	var resultMap = {};
	var operList = new Array();
	var gridMapList = new Array();
	var originSrData = new Array();

	for (var i = 0; i < panelList.length; i++) {
		var inputData = new Array();
		if (panelList[i] != null) {
			if (panelList[i].isForm && panelList[i].editorMode) {
				inputData = panelList[i].getInputData();
				for (var key in inputData) {
					if (key == "nbpm_operList") {
						for (var j = 0; inputData[key] != null && j < inputData[key].length; j++) {
							operList.push(inputData[key][j]);
						}
					} else {
						resultMap[key] = inputData[key];
					}
				}
				// Form들의 SelectedRecord 다 똑같으므로 반복문이 돌면서 덮어 씌워도 상관없다.
				// SR 원본 데이터(수정기능을 위해 추가한것 임)
				originSrData = panelList[i].getSelectedRecord();
			}
			// Tab객체가 있을 경우
			if (panelList[i].isTab) {
				var tabs = panelList[i];
				var tabsLength = tabs.items.items.length;
				var itemList = new Array();
				if (tabsLength > 0) {
					for (var j = 0; j < tabsLength; j++) {
						var tabInnerPanel = tabs.items.items[j];
						// if( tabInnerPanel.isGrid && tabInnerPanel.editorMode
						// ){
						if (tabInnerPanel.isGrid) {
							var gridDataList = tabInnerPanel.getGridData();
							if (tabInnerPanel.id == "setSubTabCiAssetGrid") {
								// 요청업무 구성데이터
								parserCiData(gridDataList, itemList);
							} else if (tabInnerPanel.id == "setSubTabServGrid") {
								// 요청업무 서비스데이터
								parserServiceData(gridDataList, itemList);
							}
						}
					}
				}
				if (itemList.length > 0) {
					resultMap["nbpm_itemList"] = itemList;
				}
			}
			// grid객체가 있을 경우
			if (panelList[i].isGrid) {
				var gridDataList = panelList[i].getGridData();
				// 담당자 선택 그리드일경우
				if (panelList[i].operSel && panelList[i].editorMode) {
					var oper = {};
					oper["select_type"] = "MULTI";
					oper["oper_type"] = panelList[i].roleId;
					var operUserIds = new Array();
					for (var j = 0; j < gridDataList.length; j++) {
						operUserIds.push(gridDataList[j].user_id);
						// oper["oper_user_id"] = gridDataList[j].USER_ID;
						// oper["oper_user_name"] = gridDataList[j].USER_NM;
						// operList.push(oper);
					}
					oper["oper_user_id"] = operUserIds;
					operList.push(oper);
				} else {
					// 데이터성 그리드 일경우
					var gridMap = {};
					gridMap["gridInsertQueryKey"] = panelList[i].insertQuerykey;
					gridMap["gridDeleteQueryKey"] = panelList[i].deleteQuerykey;
					gridMap["gridData"] = gridDataList;
					gridMapList.push(gridMap);
				}
			}
		}
	}
	resultMap["gridMapList"] = gridMapList;
	// @@20130711 김도원 START
	// 상담이력 데이터 수집
	var counselHistoryCmp = Ext.getCmp('counselingHistoryCmp');
	if (counselHistoryCmp != null) {
		resultMap["counseling_content"] = counselHistoryCmp.getValue();
	}
	// @@20130711 김도원 END
	var beforeChildSrDataList = resultMap["child_sr_data"];
	var resultChildSrDataList = new Array();
	for (var i = 0; beforeChildSrDataList != null
			&& i < beforeChildSrDataList.length; i++) {
		var childDataJson = beforeChildSrDataList[i];
		var childDataMap = getJsonToArray(childDataJson);
		resultChildSrDataList.push(childDataMap);
	}

	resultMap["nbpm_operList"] = operList;
	resultMap["originSrData"] = originSrData;
	resultMap["child_sr_data"] = resultChildSrDataList;

	resultMap["multi_yn"] = PROCESS_META.MULTI_YN;
	resultMap["task_group_id"] = PROCESS_META.TASK_GROUP_ID;
	return resultMap;
}

// Ci Data를 요청업무 데이터에 맞게끔 Parser
function parserCiData(gridDataList, itemList) {
	var listSize = gridDataList.length;
	if (listSize > 0) {
		for (var i = 0; i < listSize; i++) {
			var itemJson = {};
			itemJson["item_id"] = gridDataList[i]["conf_id"];
			itemJson["item_type"] = "CI";
			itemList.push(itemJson);
		}
	}
}

// Service Data를 요청업무 데이터 맞게끔 Parser
function parserServiceData(gridDataList, itemList) {
	var listSize = gridDataList.length;
	if (listSize > 0) {
		for (var i = 0; i < listSize; i++) {
			var itemJson = {};
			itemJson["item_id"] = gridDataList[i]["service_id"];
			itemJson["item_type"] = "SERVICE";
			itemList.push(itemJson);
		}
	}
}

function clickReqUserBtn(userNameField, userIdField, userDetailField) {
	var setPopUpProp = {
		tagetUserIdField : userIdField,
		tagetUserNameField : userNameField,
		tagetEtcField : userDetailField,
		height : OPER_USER_POP_HEIGHT,
		width : 800,
		//url : getConstValue('CONTEXT') + '/itg/system/user/searchUserList.do',
		url : getConstValue('CONTEXT') + '/itg/nbpm/common/searchProcessOperUserList.do',
		buttonAlign : 'center',
		bodyStyle : 'background-color: white; ',
		params : null
	}
	 var popUp = createUserSelectPop(setPopUpProp);
	//var popUp = createProcessUserSelectPop(setPopUpProp);
	popUp.show();
}

// Process SubTab
function createProcessSubInfo(processSubProp) {
	var srId = processSubProp['srId'];
	var originSrId = processSubProp['srId'];
	if (processSubProp['relationYn'] == "true") {
		if (!(parent.checkRequestPop != null && parent.checkRequestPop())) {
			srId = parent.getUpSrId();
		}
		// URL도 바꿔얄듯
	}
	var nodeName = processSubProp['nodeName'];
	var views = processSubProp['views'];
	var sorts = processSubProp['sorts'].split(",");
	var editors = processSubProp['editors'];
	var required = processSubProp['required'];
	var ciButtons = processSubProp['ciButtons'];
	var ciModes = processSubProp['ciModes'];
	var serviceModes = processSubProp['serviceModes'];
	var serviceButtons = processSubProp['serviceButtons'];

	// @@20130711 김도원 START
	// 연계정보, 상담이력 변수 선언
	var ciCmp, serviceCmp, fileCmp, commentCmp, srRelationCmp;
	var counselHistoryCmp = null;
	//
	for (var i = 0; views != null && i < views.length; i++) {
		var tabView = views[i];
		var isEditor = isExistDataForArray(editors, tabView); // 편집여부 판단
		var isRequired = isExistDataForArray(required, tabView); // 필수여부 판단
		if (tabView == "CI") {
			ciCmp = createCiComponent(srId, isEditor, isRequired, ciButtons,
					ciModes);
		} else if (tabView == "SERVICE") {
			serviceCmp = createServiceComponent(srId, isEditor, isRequired,
					serviceButtons, serviceModes);
		} else if (tabView == "FILE") {
			fileCmp = createProcessFileComponent(srId, nodeName, isEditor,
					isRequired);
		} else if (tabView == "COMMENT") {
			commentCmp = createCommentComponent();
		}
	}
	// @@20130711 김도원 END

	// Tab 순서
	var tabItem = new Array();
	for (var i = 0; i < sorts.length; i++) {
		var tabSort = sorts[i];
		if (tabSort == "CI") {
			tabItem.push(ciCmp);
		} else if (tabSort == "SERVICE") {
			tabItem.push(serviceCmp);
		} else if (tabSort == "FILE") {
			tabItem.push(fileCmp);
		} else if (tabSort == "COMMENT") {
			tabItem.push(commentCmp);
		}
	}

	srRelationCmp = createSrRelationComp({"id":"srRelation", "srId" : originSrId}); 
	//@@20130711 김도원 END
	tabItem.push(srRelationCmp);

	// @@20130711 김도원 END
	var subTabs = null;
	if (tabItem.length > 0) {
		var tabProperty = {
			tab_id : 'processSubTab',
			cls : 'dualTab',
			width : '100%',
			height : '100%',
			autoScroll : false,
			item : tabItem
		}
		subTabs = createTabComponent(tabProperty);
	}
	return subTabs;
}

// 구성조회 Component 생성
function createCiComponent(srId, isEditor, isRequired, ciButtons, ciModes) {
	if (isEditor && ciButtons != null) {
		ciButtons.unshift('->')
	}

	var setCiAssetProp = {
		"id" : "setSubTabCiAssetGrid",
		"title" : getConstText({
					isArgs : true,
					m_key : 'res.title.common.ciasset'
				}),
		"resource_prefix" : "grid.nbpm.asset",
		"url" : "/itg/nbpm/common/searchRelConfList.do",
		params : {
			sr_id : srId,
			item_type : 'CI'
		},
		"pagingBar" : false,
		"autoLoad" : (srId) ? true : false,
		"gridHeight" : CONSTRAINT.SHORT_GRID_HEIGHT,
		"dockComp" : ciButtons,
		"dockLoc" : "bottom",
		"dockedType" : "toolbar",
		required : isRequired,
		editorMode : isEditor,
		multiSelect : true
	};
	var setCiAssetGrid = createGridComp(setCiAssetProp);

	// CI Mode 설정
	if (ciModes != null && ciModes.length > 0) {
		for (var i = 0; i < ciModes.length; i++) {
			var ciMode = ciModes[i];
			if (ciMode == "VIEW") {
				var addCiViewColumn = ciViewColumn();
				setCiAssetGrid.headerCt.add(addCiViewColumn);
			} else if (ciMode == "EDIT") {
				var addCiEditColumn = ciEditColumn();
				setCiAssetGrid.headerCt.add(addCiEditColumn);
			} else if (ciMode == "PROC_HISTORY") {
				var addCiProcHistoryColumn = ciProcHistoryColumn();
				setCiAssetGrid.headerCt.add(addCiProcHistoryColumn);
			}
		}
		setCiAssetGrid.getView().refresh();
	}

	setCiAssetGrid.getStore().on('load', function(thisStore) {
		var forms = Ext.ComponentQuery.query("form");
		for (var i = 0; i < forms.length; i++) {
			if (forms[i].editorMode) {
				var lvlFields = forms[i].query("inclevel");
				if (lvlFields.length > 0) {
					url = getConstValue('CONTEXT')
							+ "/itg/nbpm/common/searchIncLevelInfo.do";
				} else {
					lvlFields = forms[i].query("chalevel");
					url = getConstValue('CONTEXT')
							+ "/itg/nbpm/common/searchChaLevelInfo.do";
				}
				if (lvlFields != null && lvlFields.length > 0) {
					if (!(lvlFields[0].getValue() != null && lvlFields[0]
							.getValue() != "")) {
						var ciSelGird = setCiAssetGrid;
						var gridDataList = ciSelGird.getGridData();
						var listSize = gridDataList.length;
						var maxLevel = "";
						var maxLevelNm = "";
						var intMaxLevel = 0;
						if (listSize > 0) {
							for (var i = 0; i < listSize; i++) {
								var confLevel = gridDataList[i]["conf_level"]
								var confLevelNm = gridDataList[i]["conf_level_nm"]
								var intConfLevel = parseInt(confLevel);
								if (intMaxLevel < intConfLevel) {
									intMaxLevel = intConfLevel;
									maxLevel = confLevel;
									maxLevelNm = confLevelNm;
								}
							}
							if (maxLevel != "") {
								var param = {};
								param["effect"] = maxLevel
								jq.ajax({
											type : "POST",
											url : url,
											contentType : "application/json",
											dataType : "json",
											async : false,
											data : getArrayToJson(param),
											success : function(data) {
												lvlFields[0]
														.selectedCell(
																data.resultMap.inc_level,
																data.resultMap.inc_level_time,
																data.resultMap.inc_level_nm,
																data.resultMap.effect_nm,
																data.resultMap.uegency_nm)
											}
										});
							}
						}
					}
				}
			}
		}
	});
	return setCiAssetGrid;
}

// 구성보기
function ciViewColumn() {
	var ciViewColumn = Ext.create('Ext.grid.column.Action', {
				text : getConstText({
							isArgs : true,
							m_key : 'res.common.label.detailview'
						}),
				width : 50,
				align : 'center',
				items : [{
					icon : '/itg/base/images/ext-js/common/icons/bullet_search.gif',
					tooltip : getConstText({
								isArgs : true,
								m_key : 'res.common.label.detailview'
							}),
					handler : function(grid, rowIndex, colIndex) {
						var rec = grid.getStore().getAt(rowIndex);
						var recordRaw = rec.raw;
						var asset_id = recordRaw["ASSET_ID"];
						var conf_id = recordRaw["CONF_ID"];
						var class_id = recordRaw["CLASS_ID"];
						var class_type = recordRaw["CLASS_TYPE"];
						var logical_yn = recordRaw["LOGICAL_YN"];
						var tangible_asset_yn = recordRaw["TANGIBLE_ASSET_YN"];
						var view_type = "pop_view";
						var entity_class_id = "";

						if (tangible_asset_yn != "Y" && logical_yn == "Y") {
							entity_class_id = class_id + "-0";
						} else {
							entity_class_id = class_id;
						}
						var param = "?asset_id=" + asset_id + "&conf_id="
								+ conf_id + "&class_id=" + class_id
								+ "&view_type=" + view_type
								+ "&entity_class_id=" + entity_class_id;
						showAssetDetailPop(param);
					}
				}]
			});

	return ciViewColumn;
}

// 구성편집 컬럼
function ciEditColumn() {
	var ciEditColumn = Ext.create('Ext.grid.column.Action', {
				text : getConstText({
							isArgs : true,
							m_key : 'res.common.label.change'
						}),
				width : 50,
				align : 'center',
				items : [{
					icon : '/itg/base/images/ext-js/common/icons/menu_maintenance.gif',
					tooltip : getConstText({
								isArgs : true,
								m_key : 'res.common.label.change'
							}),
					handler : function(grid, rowIndex, colIndex) {
						var rec = grid.getStore().getAt(rowIndex);
						var recordRaw = rec.raw;
						var asset_id = recordRaw["ASSET_ID"];
						var conf_id = recordRaw["CONF_ID"];
						var class_id = recordRaw["CLASS_ID"];
						var class_type = recordRaw["CLASS_TYPE"];
						var logical_yn = recordRaw["LOGICAL_YN"];
						var tangible_asset_yn = recordRaw["TANGIBLE_ASSET_YN"];
						var view_type = "view";
						var entity_class_id = "";

						if (tangible_asset_yn != "Y" && logical_yn == "Y") {
							entity_class_id = class_id + "-0";
						} else {
							entity_class_id = class_id;
						}
						var param = "?asset_id=" + asset_id + "&conf_id="
								+ conf_id + "&class_id=" + class_id
								+ "&view_type=" + view_type
								+ "&entity_class_id=" + entity_class_id;
						showAssetDetailPop(param);
					}
				}]
			});
	return ciEditColumn;
}

// 구성 프로세스 이력 컬럼
function ciProcHistoryColumn() {
	var ciProcHistoryColumn = Ext.create('Ext.grid.column.Action', {
				text : getConstText({
							isArgs : true,
							m_key : 'res.common.label.history'
						}),
				width : 50,
				align : 'center',
				items : [{
					icon : '/itg/base/images/ext-js/common/icons/time.png',
					tooltip : getConstText({
								isArgs : true,
								m_key : 'res.common.label.history'
							}),
					handler : function(grid, rowIndex, colIndex) {
						var rec = grid.getStore().getAt(rowIndex);
						var recordRaw = rec.raw;
						var setPopUpProp = {
							height : 473,
							width : 870,
							id : 'processGridPop',
							url : getConstValue('CONTEXT')
									+ '/itg/nbpm/common/searchProcessList.do',
							buttonAlign : 'center',
							params : {
								item_id : recordRaw["CONF_ID"],
								item_type : "CI"
							},
							acceptBtnVisible : false
						}
						var popUp = createProcessSelectPop(setPopUpProp);
						popUp.show();
					}
				}]
			});
	return ciProcHistoryColumn;
}

// 서비스조회 Component 생성
function createServiceComponent(srId, isEditor, isRequired, serviceButtons,
		serviceModes) {
	if (isEditor && serviceButtons != null) {
		serviceButtons.unshift('->')
	}

	var setServProp = {
		"id" : "setSubTabServGrid",
		"title" : getConstText({
					isArgs : true,
					m_key : 'res.title.common.service'
				}),
		"resource_prefix" : "grid.nbpm.service",
		"url" : "/itg/nbpm/common/searchRelConfList.do",
		params : {
			sr_id : srId,
			item_type : 'SERVICE'
		},
		"pagingBar" : false,
		"autoLoad" : (srId) ? true : false,
		"gridHeight" : CONSTRAINT.SHORT_GRID_HEIGHT,
		"dockComp" : serviceButtons,
		"dockLoc" : "bottom",
		"dockedType" : "toolbar",
		required : isRequired,
		editorMode : isEditor,
		multiSelect : true
	};
	var setServGrid = createGridComp(setServProp);

	// CI Mode 설정
	if (serviceModes != null && serviceModes.length > 0) {
		for (var i = 0; i < serviceModes.length; i++) {
			var serviceMode = serviceModes[i];
			if (serviceMode == "PROC_HISTORY") {
				var addServiceProcHistoryColumn = serviceProcHistoryColumn();
				setServGrid.headerCt.add(addServiceProcHistoryColumn);
			}
		}
		setServGrid.getView().refresh();
	}

	return setServGrid;
}

// 서비스 프로세스 요청이력 컬럼
function serviceProcHistoryColumn() {
	var serviceProcHistoryColumn = Ext.create('Ext.grid.column.Action', {
				text : getConstText({
							isArgs : true,
							m_key : 'res.common.label.history'
						}),
				width : 50,
				align : 'center',
				items : [{
					icon : '/itg/base/images/ext-js/common/icons/time.png',
					tooltip : getConstText({
								isArgs : true,
								m_key : 'res.common.label.history'
							}),
					handler : function(grid, rowIndex, colIndex) {
						var rec = grid.getStore().getAt(rowIndex);
						var recordRaw = rec.raw;
						var setPopUpProp = {
							height : 473,
							width : 870,
							id : 'processGridPop',
							url : getConstValue('CONTEXT')
									+ '/itg/nbpm/common/searchProcessList.do',
							buttonAlign : 'center',
							params : {
								item_id : recordRaw["CONF_ID"],
								item_type : "CI"
							},
							acceptBtnVisible : false
						}
						var popUp = createProcessSelectPop(setPopUpProp);
						popUp.show();
					}
				}]
			});
	return serviceProcHistoryColumn;
}

// 프로세스 파일첨부 Component 생성
function createProcessFileComponent(srId, nodeName, isEditor, isRequired) {
	var setFileProp = {
		"title" : getConstText({
					isArgs : true,
					m_key : 'res.title.common.fileInfo'
				}),
		"fileHeight" : CONSTRAINT.SHORT_GRID_HEIGHT,
		params : {
			sr_id : srId
		},
		editorMode : isEditor,
		allowFileExt : CONSTRAINT.DEFAULT_FILE_ALLOW_EXT,
		required : isRequired,
		nodeName : nodeName
	};
	var setFileGrid = createAtchFileGirdCompByProcess(setFileProp);
	return setFileGrid;
}

// Comment 이력 Component 생성
function createCommentComponent() {
	var setComment = createCommenHistoryFormComp({
				"id" : "commentHistory",
				commentHeight : CONSTRAINT.SHORT_GRID_HEIGHT
			});
	return setComment;
}

/**
 * Process Sub Info Event ActionLink
 * 
 * @param {}
 *            command
 */
function procSubActionLink(command) {
	switch (command) {
		case "subCiAddBtn" :
			ciAddBtnFunc("setSubTabCiAssetGrid");
			break;

		case "subCiDelBtn" :
			selectedRowDel("setSubTabCiAssetGrid");
			break;

		case "subServAddBtn" :
			serviceAddBtnFunc("setSubTabServGrid");
			break;

		case "subServDelBtn" :
			selectedRowDel("setSubTabServGrid");
			break;
	}
}

// @@20130711 김도원 START
// 연계정보 상세보기 함수 추가
function viewSrRelationDetail(srId) {
	var setPopUpProp = {
		popupId : 'srRelationDetailPopup',
		popupWidth : 950,
		popupHeight : 550,
		param : {
			sr_id : srId
		}
	}
	var popUp = createSrDetailPop(setPopUpProp);
}
// @@20130711 김도원 END

// 구성조회팝업
function ciAddBtnFunc(openerTargetGridId) {
	var ciSelGird = Ext.getCmp("setSubTabCiAssetGrid");
	var gridDataList = ciSelGird.getGridData();
	var param = {};
	var multiYn = true;
	if (Ext.getCmp("subCiAddBtn")["multiYn"]) {
		multiYn = Ext.getCmp("subCiAddBtn")["multiYn"];
	}
	var popType = getConstValue('CI_POP_TYPE');
	var setPopUpProp = {
		popupId : 'ciSearchPopup',
		popupWidth : 1050,
		popupHeight : 660,
		openerTargetGridId : openerTargetGridId,
		selectedList : gridDataList,
		primaryKeys : ["CONF_ID"],
		targetPrefix : 'grid.nbpm.asset.basic.sel',
		sourcePrefix : 'grid.nbpm.asset.basic',
		sourceUrl : getConstValue('CONTEXT')
				+ '/itg/nbpm/common/searchConfList.do',
		multiYn : multiYn,
		param : param,
		popType : popType
	}

	if (popType == "ciAndService") {
		var reqZoneField = Ext.ComponentQuery
				.query("textfield[name='req_zone']");
		if (reqZoneField.length > 0) {
			param["req_zone"] = reqZoneField[0].getValue();
		} else {
			param["req_zone"] = getConstValue('DEFAULT_REQ_ZONE');
		}
		setPopUpProp["param"] = param;
		setPopUpProp["targetPrefix"] = 'grid.nbpm.asset.serviceexp.sel';
		setPopUpProp["sourcePrefix"] = 'grid.nbpm.asset.serviceexp';

		var serviceSelGird = Ext.getCmp("setSubTabServGrid");
		setPopUpProp["servicePrimaryKeys"] = ['SERVICE_ID'];
		if (serviceSelGird != null) {
			var serviceGridDataList = serviceSelGird.getGridData();
			setPopUpProp["serviceSelectedList"] = serviceGridDataList;
			setPopUpProp["openerTargetServiceGridId"] = ['setSubTabServGrid'];
		} else {
			setPopUpProp["serviceSelectedList"] = null;
			setPopUpProp["openerTargetServiceGridId"] = null;
		}
	}

	var popUp = createCiAssetPop(setPopUpProp);
	if (popUp != null) {
		popUp.show();
	}
}

// 서비스조회팝업
function serviceAddBtnFunc(openerTargetGridId) {
	var ciSelGird = Ext.getCmp("setSubTabServGrid");
	var gridDataList = ciSelGird.getGridData();
	var multiYn = true;
	if (Ext.getCmp("subServAddBtn")["multiYn"] != null
			&& Ext.getCmp("subServAddBtn")["multiYn"] != "undefined") {
		multiYn = Ext.getCmp("subServAddBtn")["multiYn"];
	}

	var popType = getConstValue('CI_POP_TYPE');
	var param = {};
	var reqZoneField = Ext.ComponentQuery.query("textfield[name='req_zone']");
	if (reqZoneField.length > 0) {
		param["req_zone"] = reqZoneField[0].getValue();
	} else {
		param["req_zone"] = getConstValue('DEFAULT_REQ_ZONE');
	}
	var setPopUpProp = {
		popupId : 'serviceSearchPopup',
		popupWidth : 400,
		popupHeight : 500,
		openerTargetGridId : openerTargetGridId,
		param : param,
		selectedList : gridDataList,
		multiYn : multiYn,
		popType : popType
	}

	if (popType == "ciAndService") {
		var ciSelGird = Ext.getCmp("setSubTabCiAssetGrid");
		setPopUpProp["assetPrimaryKeys"] = ['CONF_ID'];
		if (ciSelGird != null) {
			var assetGridDataList = ciSelGird.getGridData();
			setPopUpProp["assetSelectedList"] = assetGridDataList;
			setPopUpProp["openerTargetAseetGridId"] = ['setSubTabCiAssetGrid'];
		} else {
			setPopUpProp["assetSelectedList"] = null;
			setPopUpProp["openerTargetAseetGridId"] = null;
		}
	}

	var popUp = createCiServicePop(setPopUpProp);
	if (popUp != null) {
		popUp.show();
	}
}

function gridActionRemoveBtnEvent(grid, rowIndex, colIndex) {
	grid.getStore().removeAt(rowIndex);
}

function clickSearchProcessBtn(relIncField, processType, srId) {
	var height = 523;
	var params = {
		"cur_sr_id" : srId
	};
	var agt = navigator.userAgent;
	if (agt.indexOf("Chrome") != -1) {
		height = height - 7;
	}
	if (processType != null) {
		params["req_type"] = processType;
	}
	var setPopUpProp = {
		tagetUpSrIdField : relIncField,
		height : height,
		width : 870,
		id : 'processGridPop',
		url : getConstValue('CONTEXT')
				+ '/itg/nbpm/common/searchProcessList.do',
		buttonAlign : 'center',
		// bodyPadding: '5 5 5 5 ',
		// bodyStyle: 'background-color: white;',
		params : params
	}
	var popUp = createProcessSelectPop(setPopUpProp);
	popUp.show();
}

// 프로세스 에디터폼 : 폴딩기능 제어
function createProcEditorFormComp(setEditorFormProp) {
	/*
	 * if( CONSTRAINT.NBPM_PANEL_COLLAPSIBLE ){ setEditorFormProp["collapsible"] =
	 * true; setEditorFormProp["collapsed"] = false; }
	 */
	var REQUEST_PAGE = "REQUST_REGIST";
	var acceptBtn = null;
	if (PROCESS_META.CLAIM_YN == "Y"
			&& setEditorFormProp.taskName == REQUEST_PAGE) {
		var acceptBtn = createBtnComp({
					visible : true,
					label : "접수하기",
					id : 'acceptBtn',
					ui : "correct",
					scale : "medium"
				});
		setEditorFormProp["buttonAlign"] = "center";
		setEditorFormProp["alignBottomBtns"] = [acceptBtn];
		function clickAcceptBtn() {
			jq.ajax({
						"type" : "POST",
						"url" : getConstValue('CONTEXT')
								+ "/itg/nbpm/process/acceptGroupAssignTask.do",
						"contentType" : "application/json",
						"dataType" : "json",
						"async" : false,
						"data" : getArrayToJson({
									task_id : PROCESS_META.TASK_ID
								}),
						"success" : function(data) {
							if (data.success) {
								alert("등록되었습니다.");
								var queryParam = getUrlParams();
								queryParam["claim_yn"] = "N"
								var queryString = ""; 
								for(var key in queryParam) {
									queryString = queryString + key + "=" + queryParam[key];
									queryString = queryString + "&";
								}
								location.href = location.pathname + "?" + queryString;
							} else {
								alert(data.resultMsg);
							}
						}
					});
		}
		attachBtnEvent(acceptBtn, clickAcceptBtn);
		acceptBtn.ignore = true;
	}

	var editorForm = createEditorFormComp(setEditorFormProp);
	var taskName = PROCESS_META.TASK_NAME == 'start'
			? REQUEST_PAGE
			: PROCESS_META.TASK_NAME;
	if (setEditorFormProp.taskName == taskName
			&& PROCESS_META.SANCTN_LINE_SLCT_YN == "Y") {
		var lineSelectBtn = createBtnComp({
			visible : true,
			label : "결제자라인 불러오기",
			id : 'lineSelectBtn',
			ui : 'correct',
			icon : CONSTRAINT.CONTEXT
					+ '/itg/base/images/ext-js/common/icons/menu_servicelevel.gif'
		});
		// var lineViewBtn = createBtnComp({visible: true, label: "결제자정보 보기",
		// id:'lineViewBtn', ui: 'correct', icon : CONSTRAINT.CONTEXT +
		// '/itg/base/images/ext-js/simple/btn-icons/search.gif'});

		function clickLineSelectBtn() {

			var params = {};
			var process_type = "";
			var line_user_id = "";

			// params['process_type'] = "SVC"; // 요청 타입
			params['process_type'] = PROCESS_META.PROCESS_TYPE; // 요청 타입
			params['user_id'] = "omsadmin"; // 사용자

			var setOperUserInfo = function(operUserList) {
				var operUserInfoParam = {};
				operUserInfoParam["operUserList"] = operUserList;
				operUserInfoParam["srId"] = PROCESS_META.SR_ID;

				if (operUserList != null && operUserList.length > 0) {
					jq.ajax({
								"type" : "POST",
								"url" : getConstValue('CONTEXT')
										+ "/itg/nbpm/registerSanctionLineInfo.do",
								"contentType" : "application/json",
								"dataType" : "json",
								"async" : false,
								"data" : getArrayToJson(operUserInfoParam),
								"success" : function(data) {
									if (data.success) {
										alert("등록되었습니다.");
									} else {

									}
									// setViewPortMaskFalse();
								}
							});
				}

			}

			var setPopUpProp = {
				id : "sanctionAuthPop",
				height : 300,
				width : 750,
				url : '/itg/workflow/sanction/searchSanctionLineProcAuth.do',
				buttonAlign : 'center',
				bodyStyle : 'background-color: white; ',
				callBackFunction : setOperUserInfo,
				params : params
			}
			var popUp = createSanctionLineSelectPop(setPopUpProp);
			popUp.show();
		}

		function clickLineViewBtn() {
			// var pop = createLineViePopup();

		}

		attachBtnEvent(lineSelectBtn, clickLineSelectBtn);
		// attachBtnEvent(lineViewBtn, clickLineViewBtn);

		editorForm.tools = [lineSelectBtn];
		// editorForm.tools = [lineSelectBtn, lineViewBtn];
	}

	if ("start" == PROCESS_META.TASK_NAME
			&& "REQUST_REGIST" == setEditorFormProp.taskName) {
		var templateBtn = createBtnComp({
			visible : false,
			label : "템플릿보기",
			id : 'templateBtn',
			ui : 'correct',
			icon : CONSTRAINT.CONTEXT
					+ '/itg/base/images/ext-js/common/icons/menu_servicelevel.gif'
		});

		function clickTemplateBtn() {
			// 템플릿 팝업
			createTemplatPopup();
		}
		attachBtnEvent(templateBtn, clickTemplateBtn);
		editorForm.tools.push(templateBtn);
	}

	/*
	 * if( CONSTRAINT.NBPM_PANEL_COLLAPSIBLE ){ editorForm.on('expand',
	 * function(panel){ callResizeLayout(); });
	 * 
	 * editorForm.on('collapse', function(panel){ callResizeLayout(); }); }
	 */
	return editorForm;
}

// 프로세스 그리드 : 폴딩기능 제어
function createProcGridComp(setGridProp) {
	if (CONSTRAINT.NBPM_PANEL_COLLAPSIBLE) {
		setGridProp["collapsible"] = true;
		setGridProp["collapsed"] = false;
	}

	var gridComp = createGridComp(setGridProp);

	if (CONSTRAINT.NBPM_PANEL_COLLAPSIBLE) {
		gridComp.on('expand', function(panel) {
					callResizeLayout();
				});

		gridComp.on('collapse', function(panel) {
					callResizeLayout();
				});
	}
	return gridComp;
}

function setEditorPanels(panelList, presentPanelList) {

	var presentPanelCount = presentPanelList.length;

	for (var i = 0; i < panelList.length; i++) {
		// var isCollapse = true;
		var panel = panelList[i];
		if (panel != null && panel.xtype != "tabpanel"
				&& panel.xtype != "gridpanel") {
			// if( CONSTRAINT.NBPM_PANEL_COLLAPSIBLE && panel.id == "bass_info"
			// ){
			// isCollapse = false;
			// }

			if (PROCESS_META.TASK_NAME == "END_EDIT") {
				if (panelList[i].editorMode) {
					formEditable({
								id : panelList[i].id,
								editorFlag : panelList[i].editorMode,
								excepArray : ["nbpm_comment"]
							});
				} else {
					formEditable({
								id : panelList[i].id,
								editorFlag : panelList[i].editorMode,
								excepArray : []
							});
				}
			} else {
				formEditable({
							id : panelList[i].id,
							editorFlag : panelList[i].editorMode,
							excepArray : []
						});
			}
		}
		/*
		 * if( panel !=null && CONSTRAINT.NBPM_PANEL_COLLAPSIBLE &&
		 * presentPanelCount > 0 ){ for( var j = 0; j < presentPanelCount; j++ ){
		 * var presentPanel = presentPanelList[j];
		 * 
		 * if( presentPanel != null && panel.id == presentPanel.id){ isCollapse =
		 * false; break; } } //if( panel.xtype != "tabpanel" && panel.xtype !=
		 * "gridpanel" ){ //if( isCollapse ){ //panel.toggleCollapse(); //}else{
		 * //panel.header.clearListeners(); //panel.collapseTool.disable(); // } } }
		 */
	}

	if (presentPanelCount > 0) {
		for (var i = 0; i < presentPanelCount; i++) {
			var presentPanel = presentPanelList[i];
			presentPanel.setBodyStyle("border: 2px double rgb(85, 227, 240);");
		}
	}

	callResizeLayout();
}

function resizeProcBorderLayout(panelList, presentPanelList) {
	var calculatePanels = new Array();
	var adjustPanelId = "";

	var isTabPanel = false;
	var tabPanelId;
	for (var i = 0; i < panelList.length; i++) {
		if (typeof panelList[i] != "undefined" && panelList[i] != null) {
			if (panelList[i].xtype == "tabpanel") {
				isTabPanel = true;
				tabPanelId = panelList[i].id;
				adjustPanelId = panelList[i].id;
			} else {
				calculatePanels.push(panelList[i].id);
			}
		}
	}

	if (adjustPanelId == "" && presentPanelList.length > 0) {
		adjustPanelId = presentPanelList[presentPanelList.length - 1].id;
	}
	resizeBorderLayout("viewportPanel", "borderPanel", calculatePanels,
			adjustPanelId);

	if (isTabPanel) { // Tab Panel 하위의 Panel Height동기화
		syncPanelHeight(tabPanelId);
	}
}

function checkSelfWorker(data) {
	if (data.resultMap.selfWork != null && data.resultMap.selfWork) {
		return true;
	} else {
		return false;
	}
}

function getNextTaskUrl(data) {
	return getConstValue('CONTEXT') + "/itg/nbpm/goTaskPage.do?nbpm_task_id="
			+ data.resultMap.nextTaskId;
}

/**
 * 필드값 가져오기
 * 
 * @param {}
 *            formId
 * @param {}
 *            fieldName
 * @return {}
 */
function procGetFieldValue(formId, xtype, fieldName) {
	var value = "";
	var form = Ext.getCmp(formId);
	if (form) {
		var field = form.getFormField(fieldName);
		if (field != null && field.isFieldContainer) {
			value = form.getFormFieldContainer(fieldName).getContainerValue();
		} else {
			value = form.getFieldValue(fieldName);
		}
	}
	return nullToSpace(value);
}

/**
 * 필드값 Clear
 * 
 * @param {}
 *            formId
 * @param {}
 *            fieldName
 */
function procClearFieldValue(formId, xtype, fieldName) {
	var form = Ext.getCmp(formId);
	if (form) {
		var field = Ext.getCmp(formId).getFormField(fieldName);
		if (field != null && field.isFieldContainer) {
			form.getFormFieldContainer(fieldName).clearContainerValue();
		} else {
			form.setFieldValue(fieldName, "");
		}
	}
}

/**
 * 필수해제 Clear
 * 
 * @param {}
 *            formId
 * @param {}
 *            fieldName
 */
function procRequiredClearField(formId, xtype, fieldName) {
	var form = Ext.getCmp(formId);
	if (form) {
		Ext.suspendLayouts();
		var field = Ext.getCmp(formId).getFormField(fieldName);
		if (field != null && field.isFieldContainer) {
			var containerFields = form.getFormFieldContainer(fieldName);
			var containerFieldsCount = containerFields.items.items.length;
			for (var i = 0; i < containerFieldsCount; i++) {
				var containerField = containerFields.items.items[i];
				if (containerField.xtype && containerField.xtype != "label"
						&& containerField.xtype != "button") {
					containerField.clearInvalid();
					containerField.allowBlank = true;
					containerField.validator = null;
				}
			}
		} else {
			var field = form.getFormField(fieldName);
			field.clearInvalid();
			field.allowBlank = true;
			field.validator = null;
		}
		Ext.resumeLayouts();
	}
}

function procCheckRequriedField(formId, xtype, fieldName) {
	var readOnlyStyle = CONSTRAINT.APP_READONLY_STYLE;
	var highlightStyle = "field-readonly-highlight";
	if (readOnlyStyle) {
		switch (readOnlyStyle) {
			case "block" :
				readOnlyStyle = "field-readonly-block";
				break;
			case "underline" :
				readOnlyStyle = "field-readonly-underline";
				break;
			case "highlight" :
				readOnlyStyle = "field-readonly-underline";
				break;
		}
	}

	var form = Ext.getCmp(formId);
	if (form) {
		var field = Ext.getCmp(formId).getFormField(fieldName);
		if (field.isFieldContainer) {
			var containerFields = form.getFormFieldContainer(fieldName);
			var containerFieldsCount = containerFields.items.items.length;
			for (var i = 0; i < containerFieldsCount; i++) {
				var containerField = containerFields.items.items[i];
				if (containerField.xtype && containerField.xtype != "label"
						&& containerField.xtype != "button") {
					containerField.allowBlank = false;
					removeClass(
							containerField.inputEl.dom,
							(!containerField.allowBlank && CONSTRAINT.APP_READONLY_STYLE)
									? highlightStyle
									: readOnlyStyle);
					removeClass(containerField.inputEl.dom,
							'field-readonly-underline');
					var errors = containerField.validate();
					containerField.allowBlank = true;
					// containerField.focus(true, 10);
				}
			}

		} else {
			var field = form.getFormField(fieldName);
			field.allowBlank = false;
			removeClass(field.inputEl.dom,
					(!field.allowBlank && CONSTRAINT.APP_READONLY_STYLE)
							? highlightStyle
							: readOnlyStyle);
			removeClass(field.inputEl.dom, 'field-readonly-underline');
			var errors = field.validate();
			field.allowBlank = true;
			// field.focus(true, 10);
		}
	}
}

// 필수 필드 부여
function procSetRequriedField(formId, xtype, fieldName) {
	var form = Ext.getCmp(formId);
	if (form) {
		var field = Ext.getCmp(formId).getFormField(fieldName);
		if (field.isFieldContainer) {
			var containerFields = form.getFormFieldContainer(fieldName);
			var containerFieldsCount = containerFields.items.items.length;
			for (var i = 0; i < containerFieldsCount; i++) {
				var containerField = containerFields.items.items[i];
				if (containerField.xtype && containerField.xtype != "label"
						&& containerField.xtype != "button") {
					containerField.allowBlank = false;
					if (containerField.fieldLabel) {
						containerField
								.setFieldLabel(getConstValue('NOT_NULL_SYMBOL')
										+ containerField.fieldLabel);
					}
				}
			}
		} else {
			var field = form.getFormField(fieldName);
			field.allowBlank = false;

			if (field.fieldLabel) {
				field.setFieldLabel(getConstValue('NOT_NULL_SYMBOL')
						+ field.fieldLabel);
			}
		}
	}
}

function procRequiredRegainField(formId, xtype, fieldName) {
	var form = Ext.getCmp(formId);
	if (form) {
		var field = Ext.getCmp(formId).getFormField(fieldName);
		if (field.isFieldContainer) {
			var containerFields = form.getFormFieldContainer(fieldName);
			var containerFieldsCount = containerFields.items.items.length;
			for (var i = 0; i < containerFieldsCount; i++) {
				var containerField = containerFields.items.items[i];
				if (containerField.xtype && containerField.xtype != "label"
						&& containerField.xtype != "button") {
					containerField.allowBlank = false;
				}
			}
		} else {
			field.allowBlank = false;
		}
	}
}

/**
 * 서브탭 Validation
 * 
 * @param {}
 *            panelList
 * @param {}
 *            nodeName
 * @return {}
 */
function procSubInfoTabValid(panelList, nodeName) {
	var isValidation = true;
	for (var i = 0; i < panelList.length; i++) {
		if (panelList[i] != null) {
			if (panelList[i].isTab) {
				var tabs = panelList[i];
				var tabsLength = tabs.items.items.length;
				var itemList = new Array();
				if (tabsLength > 0) {
					for (var j = 0; j < tabsLength; j++) {
						var tabInnerPanel = tabs.items.items[j];
						if (tabInnerPanel.required) {
							if (tabInnerPanel.id == "atch_file_id_grid") {
								if (tabInnerPanel.readyState) {
									// 첨부파일일 경우
									if (tabInnerPanel.isNoFile()) {
										alert("[" + tabInnerPanel.originTitle
												+ "] " + getConstText({
															isArgs : true,
															m_key : 'msg.common.00004'
														}));
										isValidation = false;
										break;
									} else {
										var isFileUpload = false;
										tabInnerPanel.listFilesStore.each(
												function(record, index, count) {
													if (record.raw.NODENAME == nodeName) {
														// 현재 요청정보로 등록된 파일이 있을
														// 경우
														isFileUpload = true;
														return false;
													}
												})

										if (!isFileUpload) {
											alert("["
													+ tabInnerPanel.originTitle
													+ "] " + getConstText({
														isArgs : true,
														m_key : 'msg.common.00004'
													}));
											isValidation = false;
											break;
										}
									}
								} else {
									alert("[" + tabInnerPanel.originTitle
											+ "] " + getConstText({
														isArgs : true,
														m_key : 'msg.common.00004'
													}));
									isValidation = false;
									break;
								}

							} else {
								if (tabInnerPanel.store.count() == 0) {
									alert("[" + tabInnerPanel.originTitle
											+ "] " + getConstText({
														isArgs : true,
														m_key : 'msg.common.00004'
													}));
									isValidation = false;
									break;
								}
							}
						}
					}
				}
			}
		}
	}
	return isValidation;
}

// 구성 또는 서비스 벨리드
function procCiOrServiceValid() {
	var isValidation = false;
	if (Ext.getCmp("setSubTabCiAssetGrid")) {
		var setSubTabCiAssetGrid = Ext.getCmp("setSubTabCiAssetGrid");
		if (setSubTabCiAssetGrid.store.count() > 0) {
			isValidation = true;
		}
	}

	if (Ext.getCmp("setSubTabServGrid")) {
		var setSubTabServGrid = Ext.getCmp("setSubTabServGrid");
		if (setSubTabServGrid.store.count() > 0) {
			isValidation = true;
		}
	}

	if (!isValidation) {
		alert("[" + setSubTabCiAssetGrid.originTitle + "] " + getConstText({
					isArgs : true,
					m_key : 'msg.common.or'
				}) + " [" + setSubTabServGrid.originTitle + "] "
				+ getConstText({
							isArgs : true,
							m_key : 'msg.common.00004'
						}));
	}

	return isValidation;
}

// @@20130711 김도원 START
// 연계정보 컴포넌트 생성 함수
function createSrRelationComp(setProp) {
	var that = {};
	for (var prop in setProp) {
		that[prop] = setProp[prop];
	}

	var srId = that["srId"];
	var gridText = '';
	var gridHeader = '';
	var gridWidth = '';
	var gridFlex = '';
	var gridAlign = '';
	var gridSortable = '';
	var gridXtype = '';
	var gridTpl = '';
	var gridFormat = '';
	var gridHidden = '';

	jq.ajax({
				type : "POST",
				url : getConstValue('CONTEXT')
						+ "/itg/base/getGridMsgSource.do",
				contentType : "application/json",
				dataType : "json",
				async : false,
				data : getArrayToJson({
							resource_prefix : 'grid.nbpm.srrelation'
						}),
				success : function(data) {
					if (data.gridText != null) {
						gridText = data.gridText.split(",");
						gridHeader = data.gridHeader.split(",");
						gridWidth = data.gridWidth.split(",");
						gridAlign = data.gridAlign.split(",");
						gridFlex = (data.gridFlex)
								? data.gridFlex.split(",")
								: "";
						gridSortable = (data.gridSortable) ? data.gridSortable
								.split(",") : "";
						gridXtype = (data.gridXtype) ? data.gridXtype
								.split(",") : "";
						gridTpl = (data.gridTpl) ? data.gridTpl.split(",") : "";
						gridFormat = (data.gridFormat) ? data.gridFormat
								.split(",") : "";
					}
				}
			});

	var gridColumns = new Array(gridHeader.length);
	var gridFields = new Array(gridHeader.length);
	for (var i = 0; i < gridHeader.length; i++) {
		var gridColumn = {};
		gridColumn["text"] = gridText[i];
		gridColumn["width"] = parseInt(gridWidth[i]);
		gridColumn["dataIndex"] = gridHeader[i].toLowerCase();
		if (gridAlign.length > 0) { // Column Data Align;
			gridColumn["align"] = gridAlign[i];
		}
		if (gridXtype[i] != 'na') {
			gridColumn["xtype"] = gridXtype[i];
		}
		if (gridFlex.length > 0) { // flex
			if (isNa(gridFlex[i])) {
				gridColumn["flex"] = gridFlex[i];
			}
		}
		if (gridSortable.length > 0) { // sortable
			gridColumn["sortable"] = compareValueToBoolean(gridSortable[i]);
		}

		gridColumns[i] = gridColumn;
		gridFields[i] = {
			name : gridHeader[i].toLowerCase(),
			type : 'string'
		};
	}

	var params = {};
	params["sr_id"] = srId;
	params["expandLevel"] = 99;
	var url = getConstValue('CONTEXT')
			+ '/itg/nbpm/common/searchSrRelationList.do';
	var tree_proxy = createTreeProxyComp(that["id"] + "TreeProxy", url, params);

	Ext.define('SrRelation', {
				extend : 'Ext.data.Model',
				fields : gridFields
			});

	var store = Ext.create('Ext.data.TreeStore', {
				model : 'SrRelation',
				folderSort : true,
				proxy : tree_proxy
			});

	// Ext.ux.tree.TreeGrid is no longer a Ux. You can simply use a
	// tree.TreePanel
	var tree = Ext.create('nkia.custom.TreePanel', {
				// icon:'$!{context}/itg/base/images/ext-js/common/icons/bullet_list.gif',
				title : getConstText({
							isArgs : true,
							m_key : 'res.title.common.reqRelation'
						}),
				width : '100%',
				height : 270,
				// renderTo: 'tree-example',
				collapsible : true,
				useArrows : true,
				rootVisible : false,
				store : store,
				multiSelect : false,
				singleExpand : true,
				columns : gridColumns,
				viewConfig : {
					toggleOnDblClick : false,
					forceFit : true,
					getRowClass : function(record, index) {
						this.data = record.data;
						if (this.data.sr_id == srId) {
							return 'bg-yellow';
						} else {
							return '';
						}
					}
				},
				listeners : {
					celldblclick : function(grid, td, cellIndex, record, tr,
							rowIndex, e, eOpts) {
						showProcessDetailPop(record.raw.sr_id);
					}
				}
			});
	return tree;
}
// @@20130711 김도원 END

// @@20130711 김도원 START
// 상담이력 컴포넌트 생성 함수
function createCounselingHistoryComponent(srId) {
	var counselHistoryCmp = createTextAreaFieldComp({
				id : "counselingHistoryCmp",
				"name" : "counseling_content",
				"value" : "",
				"width" : '100%',
				height : 240
			})

	var counselHistoryPanel = Ext.create('nkia.custom.CounselingHistory', {
				title : getConstText({
							isArgs : true,
							m_key : 'res.title.process.00001'
						}),
				items : counselHistoryCmp,
				height : 210,
				srId : srId
			});
	return counselHistoryPanel;
}

// @@20130711 김도원 START
// 요청이력 컴포넌트 생성 함수
function createCommenHistoryFormComp(setProp) {
	var that = {};
	for (var prop in setProp) {
		that[prop] = setProp[prop];
	}

	/*
	 * var commentHistoryCmp = createTextAreaFieldComp({id:"commentHistoryCmp",
	 * "name":"commentHistory","value":"","width":'100%',height:240, readOnly:
	 * true})
	 */

	var commentHistoryCmp = Ext.create('Ext.panel.Panel', {
				cls : "commentHistory",
				// height: 100,
				// border: true,
				// frame: true,
				autoScroll : true
			})

	var commentForm = Ext.create('nkia.custom.CommentForm', {
				id : that["id"],
				title : getConstText({
							isArgs : true,
							m_key : 'res.title.process.00003'
						}),
				items : commentHistoryCmp
			});

	return commentForm;
}
// @@20130711 김도원 END

function createRequestRelationComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var requestRelationcomp = Ext.create('nkia.custom.RequestRelation', {
				srId : that["srId"],
				name : that["name"] ? that['name'] : 'child_sr_id',
				label : that["label"] ? that['label'] : getConstText({
							m_key : "REL_PROC"
						}),
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				updateCol : that['updateCol'],
				updateTable : that['updateTable'],
				submitType : that['submitType'],
				processType : that['processType'] ? that['processType'] : '',
				targetFormId : that['targetFormId'] ? that['targetFormId'] : '',
				width : getConstValue('DEFAULT_FIELD_WIDTH')
			});

	return requestRelationcomp;
}

function createSrSelectComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var srSelectcomp = Ext.create('nkia.custom.SrSelectField', {
				label : that['label'],
				value : that['value'],
				name : that['name'],
				processTypeFilter : that['processTypeFilter'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				isNotColspan : that['isNotColspan'],
				srId : that['srId'],
				notNull : that['notNull']
			});

	return srSelectcomp;
}

function createRelIncViewComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var relIncViewComp = Ext.create('nkia.custom.RelIncViewField', {
				label : getConstText({
							m_key : "REL_INC_SEARCH"
						}),
				value : that['value'],
				name : that['name'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				width : that['width']
						? that['width']
						: getConstValue('DEFAULT_FIELD_WIDTH')
			});

	return relIncViewComp;
}

function createOperSelectComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var label = that['label'];
	var operType = that['name'];
	var name = that["name"];
	var processType = that['processType'];
	var notNull = that['notNull'];
	var operListData = that["oper_list_data"];

	var allowBlank = true;
	var requier = '';
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var operUserId = '';
	var operUserName = '';
	if (operListData != null && operListData.length > 0) {
		operUserId = operListData[0].OPER_USER_ID;
		operUserName = operListData[0].OPER_USER_NM;
	}

	var operSelComp = createTextFieldComp({
				readOnly : true,
				name : operType,
				value : operUserName,
				label : label,
				notNull : notNull
			});
	operSelComp.operField = true;
	operSelComp.selected_user_id = operUserId;

	var operSelCompHidden = createHiddenFieldComp({
				name : operType + '_user_id'
			});

	var grpSelectYnCompHidden = createHiddenFieldComp({
				name : operType + '_grp_select_yn'
			});

	var operSelBtn = createIconBtnComp({
				icon : getConstValue('CONTEXT')
						+ '/itg/base/images/ext-js/simple/btn-icons/search.gif',
				handler : function(btn, e, eOpts) {
					var param = {};
					if (operType != "CNSLR") {
						param["oper_type"] = operType;
						param["process_type"] = processType;
					} else {
						param["oper_type"] = operType;
					}
					var setPopUpProp = {
						id : operType + "pop",
						tagetUserIdField : operSelCompHidden,
						tagetUserNameField : operSelComp,
						tagetGrpSelectYnField : grpSelectYnCompHidden,
						height : 700,
						width : 1000,
						url : '/itg/nbpm/common/searchProcessOperUserList.do',
						buttonAlign : 'center',
						bodyStyle : 'background-color: white; ',
						params : param
					}
					var popUp = createProcessUserSelectPop(setPopUpProp);
					// var popUp = createUserSelectPop(setPopUpProp);
					popUp.show();
				}
			});

	var operClearBtn = createIconBtnComp({
				icon : getConstValue('CONTEXT')
						+ '/itg/base/images/ext-js/simple/btn-icons/reformat.gif',
				handler : function(btn, e, eOpts) {
					operSelComp.setValue("");
					operSelCompHidden.setValue("");
					grpSelectYnCompHidden.setValue("");
				}
			});

	var selfSelBtn = Ext.create('Ext.button.Button', {
		margin : getConstValue('WITH_BUTTON_MARGIN'),
		width : 60,
		text : '본인지정',
		listeners : {
			click : function(btn, e, eOpts) {
				var param = {};
				if (operType != "CNSLR") {
					param["oper_type"] = operType;
					param["process_type"] = processType;
				} else {
					param["oper_type"] = operType;
				}
				jq.ajax({
							"type" : "POST",
							"url" : getConstValue('CONTEXT')
									+ "/itg/nbpm/process/selectSelfProcessAuth.do",
							"contentType" : "application/json",
							"dataType" : "json",
							"async" : true,
							"data" : getArrayToJson(param),
							"success" : function(data) {
								if (data.success) {
									operSelComp.setValue(data.resultMap.userNm);
									operSelCompHidden
											.setValue(data.resultMap.userId);
									grpSelectYnCompHidden.setValue("");
								} else {
									alert(data.resultMsg);
								}
							}
						});
			}
		}
	});

	t_comp = {
		xtype : 'nkiafieldcontainer',
		isNotColspan : true,
		name : that["name"],
		labelAlign : 'right',
		width : getConstValue('DEFAULT_FIELD_WIDTH'),
		layout : 'hbox',
		items : [operSelComp, operSelBtn, selfSelBtn, operClearBtn,
				operSelCompHidden, grpSelectYnCompHidden],
		listeners : {}
	}
	t_comp.operUserField = operSelComp;
	t_comp.operUserIdField = operSelCompHidden;
	t_comp.grpSelectYnField = grpSelectYnCompHidden;

	t_comp.getSelecedOperUser = function() {
		var me = this;
		var userId = me.operUserIdField.getValue();;
		var userNm = me.operField.getValue();
		var grpSlctYn = me.grpSelectYn.getValue();
		return {
			oper_user_id : userId,
			oper_user_nm : userNm,
			grp_slct_yn : grpSlctYn
		};
	};
	t_comp.getLabel = function() {
		var me = this;
		return me.label;
	};
	t_comp.getSelecedOperUserId = function() {
		var me = this;
		var userId = me.operUserIdField.getValue();
		return userId;
	};
	t_comp.getSelecedOperUserNm = function() {
		var me = this;
		var userNm = me.operField.getValue();
		return userNm;
	};
	t_comp.getValue = function() {
		var me = this;
		return me.operUserIdField.getValue();
	};

	return t_comp;
}

function createReOperSelectComp(compProperty) {
	var t_comp = createOperSelectComp(compProperty)
	t_comp.reassign = true;
	return t_comp;
}

// @@20130731 김도원 START
// 프로세스타입을 속성에 추가함
/*
 * srId : srId name : name processType : processType targetFormId : targetFormId
 * updateCol : updateCol updateTable : updateTable
 * 
 */
function clickRelationRequestBtn(props) {
	window
			.open(
					"/itg/nbpm/common/goRelationRequestPop.do?processType="
							+ props.processType + "&up_sr_id=" + props.srId
							+ "&field_name=" + props.name + "&targetFormId="
							+ props.targetFormId + "&updateCol="
							+ props.updateCol + "&updateTable="
							+ props.updateTable + "&submitType="
							+ props.submitType,
					"pop",
					"width=1050,height=750,history=no,resizable=yes,status=no,scrollbars=no,member=no");
}

function clickAddBtnInOperUserGrid(gridId) {
	var gridComp = Ext.getCmp(gridId);
	var params = {};
	if (gridComp.roleId != "servicedesk") {
		params["auth_id"] = gridComp.processType + "_" + gridComp.roleId;
	} else {
		params["auth_id"] = gridComp.roleId;
	}
	var setPopUpProp = {
		targetGrid : gridId,
		url : '/itg/nbpm/common/searchProcessOperUserList.do',
		buttonAlign : 'center',
		bodyPadding : '5 5 5 5 ',
		bodyStyle : 'background-color: white; ',
		params : params,
		isMulti : true
	}
	var popUp = createUserMultiSelectPop(setPopUpProp);
	popUp.show();
}

function clickDeleteBtnInOperUserGrid(gridId) {
	var gridComp = Ext.getCmp(gridId);
	var selectedIndex = gridComp.getSelectionModel().getSelection();
	gridComp.getStore().remove(selectedIndex);
}
// @@20130731 김도원 END

// 패널리스트 Form Validation Logic
function isValidFormList(panelList) {
	var isValid = true;
	var reqType = PROCESS_META.REQ_TYPE;

	for (var i = 0; i < panelList.length; i++) {
		var panel = panelList[i];
		if (panel != null && panel.xtype == "form" && panel.editorMode) {
			if (!panel.isValid()) {
				isValid = false;
				break;
			}
		}
	}

	return isValid;
}

function deleteSrData(paramMap) {
	setViewPortMaskFalse();
	var delReasonPop = createProcessDelReasonPop({
				id : "delReasonPop",
				popUpTitle : "삭제사유",
				param : paramMap
			});
	delReasonPop.show();
}

function updateSrData(paramMap) {
	var parentClose = false;
	jq.ajax({
				"type" : "POST",
				"url" : getConstValue('CONTEXT')
						+ "/itg/nbpm/edit/updateSrData.do",
				"contentType" : "application/json",
				"dataType" : "json",
				"async" : false,
				"data" : getArrayToJson(paramMap),
				"success" : function(data) {
					if (data.success) {
						alert(data.resultMsg);
						parentClose = true;
					} else {
						alert(data.resultMsg);
					}
					setViewPortMaskFalse();
				}
			});
	if (parentClose) {
		var multiYn = PROCESS_META.MULTI_YN;
		if (multiYn == "Y") {
			self.close();
		} else {
			parent.closeTab();
		}
	}
}

function createIncLevelComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var incidentGradecomp = Ext.create('nkia.custom.IncidentGradeField', {
				label : that['label'],
				name : that['name'],
				targetFormId : that['targetFormId'],
				value : that['value'],
				width : that['width'],
				notNull : that['notNull'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN')
			});

	return incidentGradecomp;
}

function clickIncidentGradeSelBtn(name, targetFormId, maxLevel) {

	var setPopUpProp = {
		popId : 'confSelPop',
		popUpTitle : getConstText({
					m_key : "INC_LEVEL_POP"
				}),
		targetName : name,
		url : getConstValue('CONTEXT')
				+ '/itg/nbpm/common/searchIncLevelMetrix.do',
		buttonAlign : 'center',
		bodyPadding : '5 5 5 5 ',
		bodyStyle : 'background-color: white; ',
		maxLevel : maxLevel,
		targetFormId : targetFormId
	}
	var popUp = createIncidentGradeSelectPop(setPopUpProp);
	popUp.show();

	return popUp;
}

function createChaLevelComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var chaLevelcomp = Ext.create('nkia.custom.ChangeGradeField', {
				label : that['label'],
				targetFormId : that['targetFormId'],
				value : that['value'],
				width : that['width'],
				notNull : that['notNull'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				width : getConstValue('DEFAULT_FIELD_WIDTH')
			});

	return chaLevelcomp;
}

function clickChaLevelSelBtn(name, targetFormId, maxLevel) {

	var setPopUpProp = {
		popId : 'confSelPop',
		popUpTitle : getConstText({
					m_key : "CHA_LEVEL_POP"
				}),
		targetName : name,
		url : getConstValue('CONTEXT')
				+ '/itg/nbpm/common/searchIncLevelMetrix.do',
		buttonAlign : 'center',
		bodyPadding : '5 5 5 5 ',
		bodyStyle : 'background-color: white; ',
		maxLevel : maxLevel,
		targetFormId : targetFormId
	}
	var popUp = createChangeGradeSelectPop(setPopUpProp);
	popUp.show();

	return popUp;
}

/**
 * 
 * @param {}
 *            formId
 * @param {}
 *            xtype
 * @param {}
 *            id
 * @param {}
 *            visible
 */
function procSetVisibleComp(formId, xtype, id, visible) {
	Ext.suspendLayouts();
	if (xtype == "form" || xtype == "addoperatorgridpanel"
			|| xtype == "xeusvmconfigpanel" || xtype == "xeusvmrequestpanel"
			|| xtype == "processbutton") {
		if (visible) {
			Ext.getCmp(id).show();
		} else {
			Ext.getCmp(id).hide();
		}
	} else {
		if (visible) {
			Ext.getCmp(formId).getFormField(id).show();
			if (Ext.getCmp(formId).getFormField(id)["originValue"]
					&& Ext.getCmp(formId).getFormField(id)["originValue"] != "") {
				Ext.getCmp(formId).setFieldValue(id,
						Ext.getCmp(formId).getFormField(id)["originValue"]);
				if (xtype == "datefield") {
					var fieldId = Ext.getCmp(formId).getFormField(id).id;
					Ext.getCmp(fieldId).setValue(Ext.getCmp(formId)
							.getFormField(id)["originValue"]);
				}
			}
		} else {
			Ext.getCmp(formId).getFormField(id).hide();

			var field = Ext.getCmp(formId).getFormField(id);
			// Container Field는 원본값을 저장하지 않는다. datefield 예외
			if (field != null
					&& (!field.isFieldContainer || xtype == "datefield")) {
				Ext.getCmp(formId).getFormField(id)["originValue"] = procGetFieldValue(
						formId, xtype, id);
			}
			procClearFieldValue(formId, xtype, id);
		}
	}
	Ext.resumeLayouts(true);
}

function procCheckVisibleComp(formId, xtype, id, visible) {
	if (xtype == "form" || xtype == "processbutton") {
		return Ext.getCmp(id).isVisible();
	} else {
		return Ext.getCmp(formId).getFormField(id).isVisible();
	}
}

function procSetFieldValue(eventField, formId, xtype, fieldName, value) {
	var form = Ext.getCmp(formId);
	if (form && procCheckVisibleComp(formId, xtype, fieldName, true)) {
		if (xtype == "datefield") {
			if (eventField.isHourField) {
				form.getFormFieldContainer(fieldName)
						.setHourValue(eventField.rawValue);
			} else if (eventField.isMinField) {
				form.getFormFieldContainer(fieldName)
						.setMinValue(eventField.rawValue);
			} else {
				var values = value;
				var sDateValue;
				var sHourValue;
				var sMinValue;
				if (values.standard_field) {
					var fullDate = eventField.ownerCt.getValue();
					sDateValue = fullDate.substr(0, 10);
					sHourValue = fullDate.substr(10, 2);
					sMinValue = fullDate.substr(12, 2);
				} else if (values.reference_field) {
					var value = values.reference_value;
					var fullDate = eval(value);
					sDateValue = fullDate.substr(0, 10);
					sHourValue = fullDate.substr(10, 2);
					sMinValue = fullDate.substr(12, 2);
				} else {
					sDateValue = form.getFormFieldContainer(fieldName).dateField.rawValue;
				}
				if (sDateValue != null && sDateValue != "") {
					var cDate;
					var tDate = sDateValue.split("-");
					// tDate[1]은 month정보로써 -1을 해주여야 현재 달이 맞게 나옴
					cDate = new Date(tDate[0], tDate[1] - 1, tDate[2]);
					if (values.day > 0) {
						cDate = new Date(Date.parse(cDate) + values.day * 1000
								* 60 * 60 * 24);
					}
					var cMonth, cDay, cYear;
					cYear = cDate.getFullYear();
					if (cDate.getMonth() + 1 <= 9) {
						cMonth = '0' + (cDate.getMonth() + 1);
					} else {
						cMonth = cDate.getMonth() + 1;
					}

					if (cDate.getDate() <= 9) {
						cDay = '0' + cDate.getDate();
					} else {
						cDay = cDate.getDate();
					}
					var cFullDate = cYear + "-" + cMonth + "-" + cDay;
					form.getFormFieldContainer(fieldName)
							.setDayValue(cFullDate);
					form.getFormFieldContainer(fieldName)
							.setHourValue(sHourValue);
					form.getFormFieldContainer(fieldName)
							.setMinValue(sMinValue);
				}
			}
		}else if (xtype == "textarea") { 
		
			var editValue = null;
			if (value) {
				// Browser에 따라 Placeholder이 달라서 아래와 같이 처리함.
				if (Ext.isGecko || Ext.isWebKit || Ext.isIE9) { editValue = replaceAll(value, '/n', "" + String.fromCharCode(10));
				} else {
					editValue = replaceAll(value, '/n', "" + String.fromCharCode(13) + String.fromCharCode(10));
				}
			}
			
			form.setFieldValue(fieldName, editValue);
		
		}else {
			form.setFieldValue(fieldName, value);
		}
	}
}

function createPopBtnFieldComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH');
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var comp = Ext.create('nkia.custom.PopButtonField', {
				label : that['label'],
				name : that['name'],
				popUpType : that['popUpType'],
				text : that['text'],
				width : fieldWidth,
				notNull : that['notNull'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				srId : that['srId']
			});

	return comp;
}

function createAccountReqComboField(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var accountReqComboField = Ext.create('nkia.custom.AccountReqComboField', {
				label : that['label'],
				notNull : (that['notNull']) ? that['notNull'] : true,
				width : that['width'],
				editorMode : that['editorMode']
			});

	return accountReqComboField;
}

function createMultiTaskGrid(id, setGridProp) {
	var insertBtn = null;
	var deleteBtn = null;
	setGridProp["isProcessGrid"] = true;
	setGridProp["toolTip"] = false;
	var gridId = setGridProp.id;
	if (setGridProp["editorMode"]) {
		setGridProp["dockLoc"] = "bottom";
		setGridProp["dockedType"] = "toolbar";
		setGridProp["remoteSort"] = false;
		setGridProp["multiSelect"] = false;
		setGridProp["selModel"] = true;
	} else {
		setGridProp["multiSelect"] = false;
		setGridProp["selModel"] = false;
	}
	// 그리드 생성
	var gridComp;
	var params = setGridProp["params"];
	var reqType = PROCESS_META.REQ_TYPE;
	var roleId = setGridProp["roleId"];
	setGridProp["reqType"] = reqType;
	insertBtn = createBtnComp({
				"id" : gridId + "_add_btn",
				"label" : "추가",
				"visible" : true,
				"ui" : "correct"
			});
	deleteBtn = createBtnComp({
				"id" : gridId + "_del_btn",
				"label" : "삭제",
				"visible" : true,
				"ui" : "default"
			});
	// var gridBtns = [insertBtn];
	 var gridBtns = [insertBtn, deleteBtn];
	gridBtns.unshift('->');
	setGridProp["dockComp"] = gridBtns;

	params["type"] = "worker";
	setGridProp["params"] = params;

	setGridProp["insertBtn"] = insertBtn;
	setGridProp["deleteBtn"] = deleteBtn;

	var paramMap = {};
	var srId = setGridProp.params.sr_id;

	var createMultiTask = function(gridView, rowIndex, colIndex) {
		var record = gridView.getStore().getAt(rowIndex);
		if (record.raw.NODENAME == null) {
			var userId = record.raw.USER_ID;
			params["oper_type"] = roleId;
			params["worker_user_id"] = userId;
			params["nbpm_processInstanceId"] = PROCESS_META.PROCESSINSTANCEID;
			jq.ajax({
				type : "POST",
				url : getConstValue('CONTEXT') + "/itg/nbpm/createMultiTask.do",
				contentType : "application/json",
				dataType : "json",
				async : false,
				data : getArrayToJson(params),
				success : function(data) {
					if (data.success) {
						alert(data.resultMsg);
						record.set("TASK_ID", data.resultMap.task_id);
						record.set("TASK_NAME", data.resultMap.task_name);
						record.set("NODENAME", data.resultMap.nodename);
						record.set("ACTION", "");
						record.raw.TASK_GROUP_ID = data.resultMap.task_group_id;
						record.raw.TASK_ID = data.resultMap.task_id;
						gridView.refreshNode(rowIndex);
					} else {
						alert(data.resultMsg);
					}
				}
			});
		}
	};

	setGridProp["actionBtnEvent"] = createMultiTask;
	setGridProp["actionIcon"] = getConstValue('CONTEXT')
			+ '/itg/base/images/ext-js/common/icons/menu_system.gif';

	// "<img alt=\"\"
	// src=\"/itg/base/images/ext-js/common/icons/menu_system.gif\"
	// class=\"x-action-col-icon x-action-col-0\">"
	gridComp = createGridComp(setGridProp);
	for (var i = 0; i < gridComp.columns.length; i++) {
		if (gridComp.columns[i].dataIndex == "ACTION") {
			gridComp.columns[i].renderer = function(value, meta, record) {
				if (record.data.NODENAME != null && record.data.NODENAME != "") {
					return "";
				} else {
					return "<img alt=\"\" src=\"/itg/base/images/ext-js/common/icons/menu_system.gif\" class=\"x-action-col-icon x-action-col-0\">";
				}
			}
		}
	}

	if (insertBtn != null) {
		insertBtn.on("click", function() {
					// 사용자 팝업을 표시하고
					var gridComp = Ext.getCmp(gridId);
					var params = {};
					if (gridComp.roleId != "servicedesk") {
						params["auth_id"] = gridComp.processType + "_" + gridComp.roleId;
					} else {
						params["auth_id"] = gridComp.roleId;
					}
					
					params["oper_type"]  = roleId;
					params["process_type"] = reqType;
					
					var setPopUpProp = {
						target_grid_id : gridId,
						height : 600,
						width : 800,
						//url : getConstValue('CONTEXT') + '/itg/system/user/searchUserList.do',
						url : getConstValue('CONTEXT') + '/itg/nbpm/common/searchProcessOperUserList.do',
						buttonAlign : 'center',
						bodyStyle : 'background-color: white; ',
						params : params
					}
					var popUp = createUserSelectPopForMultiTask(setPopUpProp);
					popUp.show();
				});
	}

	if (deleteBtn != null) {
		deleteBtn.on("click", function() {
			if (confirm("해당 업무를 삭제합니다.\n계속 하시려면 확인을 눌러주세요")) {
					var sourceGrid = getGrid(gridId);
					var sourceGridStore = sourceGrid.getStore();
					var sourceSelRows = sourceGrid.view.selModel.getSelection();
					if (sourceSelRows.length > 0) {
						for (var i = 0; i < sourceSelRows.length; i++) {
							var record = sourceSelRows[i];
							var recordRaw = record.raw;
							paramMap["sr_id"] = recordRaw.SR_ID;
							paramMap["process_type"] = recordRaw.REQ_TYPE;
							paramMap["nbpm_task_id"] = recordRaw.TASK_GROUP_ID.toString();
							paramMap["task_group_id"] = recordRaw.TASK_ID.toString();
							jq.ajax({
									type : "POST",
									url : getConstValue('CONTEXT') + "/itg/nbpm/cancelMultiTask.do",
									contentType : "application/json",
									dataType : "json",
									data : getArrayToJson(paramMap),
									success : function(data) {
										if (data.success) {
											sourceGridStore.remove(record);
											alert(data.resultMsg);
										} else {
											alert(data.resultMsg);
										}
									}
								});
						}
					}
				}
			});
	}

	gridComp.on('beforeitemdblclick', function(grid, record, item, rowIndex) {
				var taskId = record.get("TASK_ID");
				if (taskId != null && taskId != "") {
					showMultiTaskDetailPop(PROCESS_META.SR_ID, taskId, grid.panel.id);
				} else {
					alert("티켓생성 후 조회 가능합니다.");
				}
			});
	return gridComp;
}

// 프로세스 상세보기 팝업
function showProcessDetailPop(srId) {
	window.open(
					getConstValue('CONTEXT')
							+ "/itg/nbpm/provide/goDetailPage.do?sr_id=" + srId,
					"reqDetailPop",
					"width=1200,height=750,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

function showMultiTaskDetailPop(srId, taskId, gridId) {
	window.open(getConstValue('CONTEXT')
					+ "/itg/nbpm/provide/goMultiTaskPop.do?sr_id="
					+ srId + "&multi_task_id=" + taskId
					+ "&type=MULTI_VIEW&grid_id=" + gridId,
					"reqDetailPop",
					"width=1100,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

// 프로세스 Template Grid 생성
function createProcessTemplate(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var params = {
		req_type : that['req_type'],
		tem_code : that['tem_code']
	};

	var setGridProp = {
		// id: (that['id']) ? that['id'] : 'templateGrid',
		gridHeight : (that['gridHeight']) ? that['gridHeight'] : 100,
		resource_prefix : 'grid.process.templateDown',
		url : getConstValue('CONTEXT')
				+ '/itg/system/process/searchTemplate.do',
		style : 'border-top-width: 1px; border-top-color: #b8c8d9; border-top-style: solid; border-left-width: 1px; border-left-color: #b8c8d9; border-left-style: solid; border-right-width: 1px; border-right-color: #b8c8d9; border-right-style: solid;',
		params : params
	}
	var grid = createGridComp(setGridProp);

	var id = that['req_type'] + that['tem_code'];

	var downloader = Ext.create('nkia.custom.FileDownloadFrame', {
				id : id + "_process_template_download_frame",
				renderTo : Ext.getBody()
			});

	grid.on('beforeitemdblclick', function(grid, record, item, rowIndex) {
		var tem_id = record.raw.TEM_ID;
		var originFileNm = record.raw.ORIGIN_FILE_NM
		if (originFileNm != null) {
			// document.theForm.target = "";
			// document.theForm.action =
			// "/itg/system/process/downProcessTemplate.do?tem_id="+tem_id;
			// document.theForm.submit();

			downloader.load({
						url : getConstValue('CONTEXT')
								+ '/itg/system/process/downProcessTemplate.do?tem_id='
								+ tem_id
					});
		} else {
			alert("등록된 파일이 없습니다.");
		}
	});

	t_comp = {
		id : that['name'] + "_container",
		xtype : 'fieldcontainer',
		fieldLabel : that['label'],
		labelSeparator : '',
		labelAlign : 'right',
		labelWidth : getConstValue('LABEL_WIDTH'),
		width : '100%',
		combineErrors : true,
		layout : 'hbox',
		defaults : {
			width : '100%',
			hideLabel : true
		},
		items : [grid]
	}
	return t_comp;

};

// 프로세스 요청참조 Field
function createProcReferenceComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var procReferenceComp = Ext.create('nkia.custom.ProcReferenceField', {
				label : that['label'],
				name : that['name'],
				value : that['value'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				fieldWidth : that['fieldWidth'],
				popupId : that['popupId'],
				popupTitle : that['popupTitle'],
				popupWidth : (that['popupWidth']) ? that['popupWidth'] : 650,
				popupHeight : (that['popupHeight']) ? that['popupHeight'] : 370,
				targetFormId : that['targetFormId'],
				selectQuerykey : that['selectQuerykey'],
				url : that['url'],
				resource_prefix : that['resource_prefix'],
				switchCommand : that['switchCommand']
			});

	return procReferenceComp;
}

function isNotNullSubProcessGrid() {
	var panel = Ext.getCmp("linkRel_request");
	var gridDataList = panel.getGridData();
	if (gridDataList.length > 0) {
		return true;
	} else {
		return false;
	}
}

function checkLinkRelGrid() {
	var result = true;
	var panel = Ext.getCmp("linkRel_request");
	var gridDataList = panel.getGridData();
	for (var i = 0; gridDataList != null && i < gridDataList.length; i++) {
		if (!(gridDataList[i].work_state == "WORKCOMP"
				|| gridDataList[i].work_state == "NOTI" || gridDataList[i].work_state == "END")) {
			result = false;
			alert("[연계요청] 중 종료되지 않은 요청이 있습니다.");
		}
	}
	return result;
}

function checkMultiTaskGrid(gridId) {
	var result = true;
	var panel = Ext.getCmp(gridId);
	var gridDataList = panel.getGridData();
	for (var i = 0; gridDataList != null && i < gridDataList.length; i++) {
		if (gridDataList[i].multi_task_name != "END") {
			result = false;
		}
	}
	if (!result) {
		alert("종료되지 않은 작업이 있습니다.");
	}
	return result;
}

function afterProcessHandler(dataResult) {
	var data = dataResult;
	var isParentClose = false;
	if (data.success) {
		alert(data.resultMsg);
	} else {
		alert(data.resultMsg);
	}
	setViewPortMaskFalse();
	if (data.success) {
		if (data.resultMap.selfWork) {
			var taskInfo = {
				'TASK_ID' : data.resultMap.nextTaskId,
				'SR_ID' : data.resultMap.sr_id,
				'TASK_NAME' : data.resultMap.nextNodeName
			};
			if (parent.reWorkTab != null) {
				parent.reWorkTab(taskInfo);
			} else {
				isParentClose = true;
			}
		} else {
			isParentClose = true;
		}
		if (isParentClose) {
			parent.closeTab();
		}
	}
}

// 패널리스트 Form Validation Logic
function isValidFormList(panelList) {
	var isValid = true;
	var reqType = PROCESS_META.REQ_TYPE;

	for (var i = 0; i < panelList.length; i++) {
		var panel = panelList[i];
		if (panel != null && panel.xtype == "form" && panel.editorMode) {
			if (!panel.isValid()) {
				isValid = false;
				break;
			}
		}
	}
	return isValid;
}

function checkChangeResultType() {
	var isReturn = true;
	if (procGetFieldValue('change_opert_result_regist', 'codecombobox',
			'change_result_type') == 'FAILR'
			|| procGetFieldValue('change_opert_result_regist', 'codecombobox',
					'change_result_type') == 'CANCL') {
		if (procGetFieldValue('change_opert_result_regist', 'textarea',
				'failr_cancl_resn') == '') {
			alert('[실패 및 취소사유] 을(를) 입력해 주십시요.');
			isReturn = false;
			return isReturn;
		}
	}
	return isReturn;
}

// 변경 유형 체크
function checkChangeType() {
	if (procGetFieldValue('change_type_cl', 'codecombobox', 'change_type') == 'GNRL') {
		// 일반변경인 경우에 변경승인자를 필수로 만들어 준다.
		procSetRequriedField('change_plan_foundng', 'operselect',
				'ROLE_OPETR_02');
	}
}

function createUserSelectPopForMultiTask(compProperty) {
	var that = convertDomToArray(compProperty);
	var id = that["id"];
	var target_grid_id = that["target_grid_id"];
	var userGrid = "";
	var selUserGrid = "";
	var window_comp = "";
	var targetGrid = Ext.getCmp(target_grid_id);
	var selParentData = targetGrid.getGridDataUpper();
	var searchForm = null;

	var searchUserList = function() {

		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();

		if (selUserGridStore.count() > 0) {
			paramMap["selUserList"] = selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}

	// 추가
	var addSeluser = function() {
		var paramMap = searchForm.getInputData();
		var selUserGridStore = selUserGrid.store;
		var selRows = gridSelectedRows(userGrid.id);

		if (isSelected(userGrid.id)) {
			Ext.suspendLayouts();
			checkGridRowCopyToGrid(userGrid.id, selUserGrid.id, {
						primaryKeys : ["USER_ID"]
					});

			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						msg : getConstText({
									isArgs : true,
									m_key : 'msg.itam.opms.00010'
								}),
						type : "ALERT"
					});
			return;
		}
	}

	// 삭제
	var deSeluser = function() {
		var paramMap = {};
		var selUserGridStore = selUserGrid.store;
		var selRows = gridSelectedRows(selUserGrid.id);

		if (isSelected(selUserGrid.id)) {
			Ext.suspendLayouts();

			for (var i = 0; i < selRows.length; i++) {
				selUserGrid.store.remove(selRows[i]);
			}

			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						title : "경고",
						msg : "선택된 정보가 없습니다.",
						type : "ALERT"
					});
			return;
		}
		if (selUserGridStore.count() > 0) {
			paramMap["selUserList"] = selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}

	// 적용
	var confirmMng = function() {
		var selUserData = selUserGrid.getGridDataUpper();
		var useRateTot = 0;
		for (var i = 0; i < selUserData.length; i++) {
			useRateTot += Number(selUserGrid.selModel.store.data.items[i].data.USE_RATE);
		}
		targetGrid.store.removeAll();
		if (selUserData.length > 0) {
			Ext.suspendLayouts();
			for (var i = 0; selUserData.length > i; i++) {
				targetGrid.store.add(selUserData[i]);
			}
			Ext.resumeLayouts(true);
		}
		window_comp.close();
	}

	// 검색
	var searchUserBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.search'
						}),
				id : 'searchUserBtn',
				ui : 'correct'
			});
	// 적용
	var conFirmBtn = createBtnComp({
				visible : true,
				label : "적용",
				id : "conFirmBtn",
				ui : "correct",
				scale : "medium"
			});
	// 사용자 추가
	var addSeluserBtn = createIconBtnComp({
				id : 'addSeluserBtn',
				icon : '/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif',
				margin : '2 2 2 0'
			});
	// 사용자 삭제
	var deSeluserBtn = createIconBtnComp({
				id : 'deSeluserBtn',
				icon : '/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif',
				margin : '2 0 2 2'
			});

	addSeluserBtn.vline = false;
	deSeluserBtn.vline = false;

	function clickUserSearchBtn() {
		var conditionVal = searchConditionComp.getComboBoxField().getValue();
		var textVal = searchConditionComp.getTextField().getValue();
		var userGridStore = userGrid.getStore();
		var param = {};
		if (that['params'] != null) {
			for (var key in that['params']) {
				param[key] = that['params'][key];
			}
		}
		param["search_type"] = conditionVal;
		param["search_value"] = textVal;
		// userGridStore.remove('currentPage');
		userGridStore.currentPage = 1;
		userGridStore.proxy.jsonData = param;
		userGridStore.load();
	}

	searchConditionComp = createUnionCodeComboBoxTextContainer({label:getConstText({ isArgs: true, m_key: 'res.common.search' }), comboboxName: 'searchCondition', textFieldName: 'USER_NM', code_grp_id: 'SEARCH_USER', widthFactor: 0.25 });

	var userSearchBtn = createBtnComp({
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.search'
						}),
				margin : getConstValue('WITH_BUTTON_MARGIN')
			});

	// @@고은규 2013.09.04 익스 구분
	var agt = navigator.userAgent;
	if (agt.indexOf("MS") != -1) {
		userSearchBtn.margin = '0 0 5 5';
	}
	attachBtnEvent(userSearchBtn, clickUserSearchBtn);
	
	// 사용자 조회 폼
	var setSearchUserFormProp = {
		id : that['id'] + 'searchUserForm',
		title : getConstText({
					isArgs : true,
					m_key : 'res.common.search'
				}),
		target : that['id'] + '_grid',
		columnSize : 2,
		enterFunction : clickUserSearchBtn,
		tableProps : [{
			colspan : 1,
			tdHeight : 30,
			item : searchConditionComp
		}, {
			colspan : 1,
			tdHeight : 30,
			item : userSearchBtn
		}]
	};
	searchForm = createSeachFormComp(setSearchUserFormProp);

	var btnContainer = createHorizonPanel({
				align : 'center',
				panelItems : [addSeluserBtn, deSeluserBtn]
			});

	var setTreeProp = {
		id : id + 'Tree',
		url : getConstValue('CONTEXT') + '/itg/base/searchDeptTree.do',
		title : getConstText({
					isArgs : true,
					m_key : 'res.00013'
				}),
		dynamicFlag : false,
		searchable : true,
		height : 490,
		expandAllBtn : false,
		collapseAllBtn : false,
		params : that['params'],
		rnode_text : getConstText({
					isArgs : true,
					m_key : 'res.00003'
				}),
		expandLevel : 2
	}
	var deptTree = createTreeComponent(setTreeProp);

	function treeNodeClick(store, record, index, eOpts) {
		var cust_id = record.raw.node_id;
		var grid_store = getGrid(that['id'] + '_grid').getStore();
		var param = {};
		if (that['params'] != null) {
			for (var key in that['params']) {
				param[key] = that['params'][key];
			}
		}
		param["cust_id"] = cust_id;
		grid_store.proxy.jsonData = param;
		grid_store.reload();
	}

	attachCustomEvent('select', deptTree, treeNodeClick);

	var left_panel = Ext.create('Ext.panel.Panel', {
				region : 'west',
				height : '100%',
				autoScroll : true,
				items : [deptTree]
			});

	// 담당자 리스트
	var setUserGridProp = {
		id : that['id'] + '_grid',
		title : '사용자정보',
		gridHeight : 210,
		resource_prefix : 'grid.nbpm.multitask.user.pop',
		url : that["url"]// 사용자정보 관련 서비스 붙일 것.
		// ,params : {selUserList : selParentData}
		,
		params : that['params'],
		pagingBar : true,
		multiSelect : true,
		selModel : true
		// ,autoScroll : true
		// ,selfScroll : true
		// ,border : true
		,
		pageSize : 5
	};

	userGrid = createGridComp(setUserGridProp);

	// 선택된 담당자 리스트
	var setSelGridProp = {
		id : id + "SelUserList",
		title : '사용자정보',
		gridHeight : 190,
		resource_prefix : 'grid.nbpm.multitask.user.pop',
		isMemoryStore : true,
		pagingBar : false,
		selModel : true,
		multiSelect : true
		// ,border : true
		,
		cellEditing : true
		// ,autoScroll : true
		// ,selfScroll : true
	};

	selUserGrid = createGridComp(setSelGridProp);

	// 그리드 로드시.. 선택된 사용자 정보를 셋해준다.. 팝업이 열리고 최초 한번만..
	var selUserGirdLoadFn = function() {
		if (selParentData.length > 0) {
			for (var y = 0; y < selParentData.length; y++) {
				selUserGrid.store.add(selParentData[y]);
			}
		}
		selUserGrid.store.un("load", selUserGirdLoadFn);
	};

	selUserGrid.store.on("load", selUserGirdLoadFn);

	// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
	var right_panel = Ext.create('Ext.panel.Panel', {
				border : false,
				region : 'center',
				flex : 1,
				items : [searchForm, userGrid, btnContainer, selUserGrid]
			});

	var complexProp = {
		height : '100%',
		panelItems : [{
					flex : 1.2,
					items : left_panel
				}, {
					flex : 3.3,
					items : right_panel
				}]
	};
	var complexPanel = createHorizonPanel(complexProp);

	var codePopupProp = {
		id : id + "Pop",
		title : '사용자선택',
		width : 950,
		height : 568,
		modal : true,
		layout : 'fit',
		buttonAlign : 'center',
		bodyStyle : 'background-color: white; ',
		buttons : [conFirmBtn],
		closeBtn : true,
		items : complexPanel,
		resizable : true
	};

	window_comp = createWindowComp(codePopupProp);
	// 버튼 이벤트

	attachBtnEvent(conFirmBtn, confirmMng);
	// attachBtnEvent(searchUserBtn ,searchUserList);
	attachBtnEvent(addSeluserBtn, addSeluser);
	attachBtnEvent(deSeluserBtn, deSeluser);

	return window_comp;
}

function createUserFieldComp(setProp) {
	var that = {};
	for (var prop in setProp) {
		that[prop] = setProp[prop];
	}

	var userComp = Ext.create('nkia.custom.RequestUserField', {
				label : that['label'],
				userNm : that['userNm'],
				userId : that['userId'],
				userDetail : that['userDetail'],
				userSel : that['userSel'],
				btnAction : that['btnAction'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				width : that['width']
			});

	return userComp;
}

/**
 * 조건에 따른 필드항목 필수여부 설정
 * 
 * @author : 석보람
 * @date : 20150916
 * @param {}
 *            task
 * @param {}
 *            xtype
 * @param {}
 *            name
 * @param {}
 *            isRequired
 */
function setRequiredField(task, xtype, name, isRequired) {
	var field = Ext.getCmp(task).getFormField(name);

	if (field) {
		if (xtype == "operselect") {
			field = field.items.items[0];
		}

		var fieldLabel = field.getFieldLabel();
		field.setFieldLabel(fieldLabel.replace(
				getConstValue('NOT_NULL_SYMBOL'), ""));

		if (!isRequired) {
			procRequiredClearField(task, xtype, name);
			// field.notNull = false;
		} else {
			fieldLabel = field.getFieldLabel();
			field.setFieldLabel(getConstValue('NOT_NULL_SYMBOL') + fieldLabel);
			procRequiredRegainField(task, xtype, name);
			// field.notNull = true;
		}
	}
}

/**
 * @function
 * @summary 결재라인 팝업
 * @param {json}
 *            setPopUpProp
 * @return {object} popup
 */
function createSanctionLineSelectPop(setPopUpProp) {
	var storeParam = {};
	var that = {};
	var userSelectPop = null;

	for (var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {

		var setTreeProp = {
			id : that['id'] + 'Tree',
			url : '/itg/workflow/sanction/searchAllSanctionLineTree.do',
			title : getConstText({
						isArgs : true,
						m_key : 'res.00048'
					}),
			dynamicFlag : false,
			height : 363,
			expandAllBtn : false,
			collapseAllBtn : false,
			params : that['params'],
			rnode_text : getConstText({
						isArgs : true,
						m_key : 'res.00048'
					}),
			expandLevel : 3
		}
		var deptTree = createTreeComponent(setTreeProp);
		// 높이 길이 체크 무시
		deptTree['syncSkip'] = true;
		attachCustomEvent('select', deptTree, treeNodeClick);

		var setGridProp = {
			id : that['id'] + '_grid',
			title : getConstText({
						isArgs : true,
						m_key : 'res.00050'
					}),
			gridHeight : 265,
			resource_prefix : (that['resource_prefix'])
					? that['resource_prefix']
					: 'grid.workflow.sanction.auth.pop',
			url : that['url'],
			params : that['params'],
			autoLoad : false
		};
		var userGrid = createGridComp(setGridProp);

		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel', {
					region : 'west',
					height : '100%',
					autoScroll : true,
					items : [deptTree]

				});

		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel', {
			border : false,
			region : 'center',
			flex : 1,
			items : [userGrid]
				// userSearchForm, userGrid ]
			});

		var complexProp = {
			height : '100%',
			panelItems : [{
						flex : 1.5,
						items : left_panel
					}, {
						flex : 3.0,
						items : right_panel
					}]
		};
		var complexPanel = createHorizonPanel(complexProp);

		var acceptBtn = createBtnComp({
					label : getConstText({
								isArgs : true,
								m_key : 'btn.common.apply'
							}),
					scale : 'medium',
					ui : 'correct'
				});

		// 닫기버튼 생성
		var closeButton = new Ext.button.Button({
					text : getConstText({
								isArgs : true,
								m_key : 'btn.common.close'
							}),
					scale : 'medium',
					handler : function() {
						userSelectPop.close();
					}
				});

		function treeNodeClick(tree, record, index, eOpts) {
			var line_id = record.raw.node_id;

			var grid_store = getGrid(that['id'] + '_grid').getStore();
			var param = {};
			if (that['params'] != null) {
				for (var key in that['params']) {
					param[key] = that['params'][key];
				}
			}

			param["line_id"] = line_id;

			grid_store.proxy.jsonData = param;
			grid_store.reload();
			// grid_store.loadPage(1);
		}
		var callOpenerFunction = that['callBackFunction'];
		function dbSelectEvent() {
			var lineTree = Ext.getCmp(that['id'] + 'Tree');
			var selectedTree = lineTree.getSelectionModel().getSelection();

			var lineGrid = Ext.getCmp(that['id'] + '_grid');
			var lineGridStore = lineGrid.getStore().data.items;;

			var selectedRecords = userGrid.view.selModel.getSelection();
			if (selectedTree.length < 1) {
				showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.workflow.sanction.0001'
						}));
			} else {

				if (lineGridStore.length < 1) {
					// 해당 요청에 담당자가 없다
				} else {
					var paramList = [];
					for (var i = 0; i < lineGridStore.length; i++) {
						var dataMap = lineGridStore[i].raw;
						paramList.push(dataMap);
					}
					callOpenerFunction(paramList);
					userSelectPop.destroy();
				}
			}
		}

		attachBtnEvent(acceptBtn, dbSelectEvent);

		userSelectPop = Ext.create('nkia.custom.windowPop', {
					id : that['id'] + '_Pop',
					title : that['popUpText'],
					height : that['height'],
					width : that['width'],
					autoDestroy : false,
					resizable : false,
					bodyPadding : that['bodyPadding'],
					buttonAlign : that['buttonAlign'],
					buttons : [acceptBtn, closeButton],
					bodyStyle : that['bodyStyle'],
					items : [complexPanel],
					listeners : {
						destroy : function(p) {
							popUp_comp[that['id']] = null

						},
						hide : function(p) {
						},
						cellclick : function(grid, td, cellIndex, record, tr,
								rowIndex, e, eOpts) {
						}
					}
				});

		popUp_comp[that['id']] = userSelectPop;

	} else {
		userSelectPop = popUp_comp[that['id']];
	}

	return userSelectPop;
}

function createProcessUserSelectPop(properties) {
	var id = properties["id"];
	var height = properties['height'];
	var width = properties['width'];
	var url = properties['url'];
	var tagetUserIdField = properties['tagetUserIdField'];
	var tagetUserNameField = properties['tagetUserNameField'];
	var tagetGrpSelectYnField = properties['tagetGrpSelectYnField'];

	var userSelectPop = null;

	var acceptBtn = createBtnComp({
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.apply'
						}),
				scale : 'medium',
				ui : 'correct'
			});

	var closeButton = new Ext.button.Button({
				text : getConstText({
							isArgs : true,
							m_key : 'btn.common.close'
						}),
				scale : 'medium',
				handler : function() {
					userSelectPop.close();
				}
			});

	properties["id"] = "userSelectPanel";
	var userSelectPanel = setUserSelectPopPanel(properties);
	userSelectPanel.titleAlign = "center";
	userSelectPanel
			.setTitle("<font style='color:red'><b>*&nbsp;사용자목록에서 사용자를 선택하여 주세요.</b></font>");
	userSelectPanel.getHeader()["height"] = 30;

	properties["id"] = "groupSelectPanel";
	var groupSelectPanel = setGroupSelectPopPanel(properties);
	groupSelectPanel.titleAlign = "center";
	groupSelectPanel
			.setTitle("<font style='color:red'><b>*&nbsp;담당자 그룹목록 트리에서 그룹을 선택하여 주세요.</b></font>");
	groupSelectPanel.getHeader()["height"] = 30;

	var tabPanel = Ext.widget('tabpanel', {
				id : 'workerSelectTab',
				resizeTabs : true,
				enableTabScroll : false,
				border : true,
				height : '100%',
				bodyBorder : false,
				autoHeight : false,
				cls : 'dualTab',
				items : [{
							title : '담당자선택',
							id : "userSelectPanel",
							items : userSelectPanel
						}, {
							title : '담당자그룹선택',
							id : "groupSelectPanel",
							items : groupSelectPanel
						}],
				listeners : {
					afterrender : function(tabPanel) {
						var agt = navigator.userAgent;
						if (agt.indexOf("Chrome") != -1) {
							setOverflow(
									tabPanel.body.dom.childNodes[0].childNodes[0],
									"hidden");
						}
					},
					tabchange : function(tabPanel) {
					}
				}
			});

	userSelectPop = Ext.create('nkia.custom.windowPop', {
				id : id + '_Pop',
				title : properties['popUpText'],
				height : height,
				width : width,
				autoDestroy : false,
				resizable : false,
				bodyPadding : properties['bodyPadding'],
				buttonAlign : properties['buttonAlign'],
				buttons : [acceptBtn, closeButton],
				bodyStyle : properties['bodyStyle'],
				items : [tabPanel],
				listeners : {
					destroy : function(p) {
						popUp_comp[id] = null

					},
					hide : function(p) {
					},
					cellclick : function(grid, td, cellIndex, record, tr,
							rowIndex, e, eOpts) {
					}
				}
			});

	function callbackUserdbSelectEvent(grid, record, item, index, e, eOpts) {
		var selectedRecords = grid.selModel.getSelection();
		if (selectedRecords.length < 1) {
			showMessage(getConstText({
						isArgs : true,
						m_key : 'msg.system.result.00031'
					}));
		} else {
			var user_id = nullToSpace(selectedRecords[0].raw.USER_ID);
			var user_nm = nullToSpace(selectedRecords[0].raw.USER_NM);
			var absent_yn = nullToSpace(selectedRecords[0].raw.ABSENT_YN);
			if (absent_yn != null && absent_yn == "Y") {
				user_id = nullToSpace(selectedRecords[0].raw.ABSENT_ID);
				user_nm = nullToSpace(selectedRecords[0].raw.ABSENT_NM);
			}
			tagetUserIdField.setValue(user_id);
			tagetUserNameField.setValue(user_nm);
			tagetGrpSelectYnField.setValue("N");
			userSelectPop.destroy();
		}
	}

	function callbackGroupdbSelectEvent(tree, record, item, index, e, eOpts) {
		var selectedRecords = tree.selModel.getSelection();
		if (selectedRecords.length < 1) {
			showMessage(getConstText({
						isArgs : true,
						m_key : 'msg.system.result.00031'
					}));
		} else {
			tagetUserIdField.setValue(selectedRecords[0].raw.node_id);
			
			var textArray = selectedRecords[0].raw.text.split('[');
			tagetUserNameField
					.setValue(textArray[0]);
			tagetGrpSelectYnField.setValue("Y");
			userSelectPop.destroy();
		}
	}

	attachCustomEvent('itemdblclick', userSelectPanel.getUserGrid(),
			callbackUserdbSelectEvent);

	attachBtnEvent(acceptBtn, function() {
				if (tabPanel.getActiveTab().id == "userSelectPanel") {
					callbackUserdbSelectEvent(userSelectPanel.getUserGrid());
				} else {
					callbackGroupdbSelectEvent(groupSelectPanel.getGrpTree());
				}
			});
	attachCustomEvent('itemdblclick', groupSelectPanel.getGrpTree(),
			callbackGroupdbSelectEvent);

	return userSelectPop;
}

function setGroupSelectPopPanel(properties) {
	var id = properties["id"];
	var url = properties["url"];
	var params = properties["params"];
	var height = properties['height'];
	var resourcePrefix = properties["resource_prefix"];

	var chargerGrpTreeProp = {
		context : '',
		id : 'chargerGrpTree',
		url : '/itg/workflow/charger/searchAllChargerGrpTree.do',
		title : '담당자 그룹목록',
		height : height,
		searchable : true,
		expandAllBtn : true,
		collapseAllBtn : true,
		params : params,
		rnode_text : '코드',
		expandLevel : 3
	}

	var chargerGrpTree = createTreeComponent(chargerGrpTreeProp);
	chargerGrpTree['syncSkip'] = true;
	attachCustomEvent('select', chargerGrpTree, treeNodeClick);

	var chargerUserGridProp = {
		id : 'chargerUserGrid',
		title : '담당자 그룹 사용자 목록',
		resource_prefix : 'grid.system.charger.user',
		url : '/itg/workflow/charger/searchChargerGrpUser.do',
		params : null,
		gridHeight : CONSTRAINT.MIDDLE_GRID_HEIGHT - 70,
		selModel : false,
		pagingBar : false,
		autoLoad : false
	}

	var chargerUserGrid = createGridComp(chargerUserGridProp);

	var chargerDeptGridProp = {
		id : 'chargerDeptGrid',
		title : '담당자 그룹 부서 목록',
		resource_prefix : 'grid.system.charger.dept',
		url : '/itg/workflow/charger/searchChargerGrpDept.do',
		params : null,
		selModel : false,
		pagingBar : false,
		autoLoad : false
	}
	var chargerDeptGrid = createGridComp(chargerDeptGridProp);

	var leftPanel = Ext.create('Ext.panel.Panel', {
				region : 'west',
				height : '100%',
				autoScroll : true,
				items : [chargerGrpTree]
			});

	var rightPanel = Ext.create('Ext.panel.Panel', {
				border : false,
				region : 'center',
				height : '100%',
				flex : 1,
				items : [chargerUserGrid, chargerDeptGrid]
			});

	var complexProp = {
		height : '100%',
		panelItems : [{
					flex : 1.1,
					items : leftPanel
				}, {
					flex : 3.0,
					items : rightPanel
				}]
	};
	var complexPanel = createHorizonPanel(complexProp);

	function treeNodeClick(tree, record, index, eOpts) {
		var paramMap = {};
		var up_cust_id = record.raw.up_node_id;
		var cust_id = record.raw.node_id;
		paramMap['grp_id'] = cust_id;

		if (!(record.raw.id == "grpRoot")) {
			var paramMap = {
				'grp_id' : cust_id
			};
			chargerUserGrid.searchList(paramMap);
			chargerDeptGrid.searchList(paramMap);
		}
	}

	complexPanel.getGrpTree = function() {
		return chargerGrpTree;
	}

	return complexPanel;
}

function eCamsSrContact(paramMap){
	
	var gridSizeZero = true;
	var panel = Ext.getCmp('srEmbdMultiTaskGrid');
	var gridDataList = panel.getGridData();
	if(gridDataList.length < 1){
		gridSizeZero = true;
	}else{
		gridSizeZero = false;
	}

	if(gridSizeZero){
	 alert("[요구사항처리] 개발자 목록을 확인바랍니다.");
	 setViewPortMaskFalse();
	}else{
	
		jq.ajax({
			type : "POST",
			url : "/itg/customize/ecamsSrContact.do",
			contentType : "application/json",
			dataType : "json",
			async : false,
			data : getArrayToJson(paramMap),
			success : function(data) {
				 setViewPortMaskFalse();
				if(data.success){
					alert(data.resultMsg);
					
				} 
				
	
			}
		});
	}
}