/**
 * 프로세스 데이터 셋팅
 * @param {} props
 */
function setProcessData(props) {
	var srId = props['sr_id'];
	var taskId = props['task_id'];
	var panelList = props['panelList'];
	var queryKey = props['queryKey'];
	var subQueryKey = props['subQueryKey'];
	var params = {};
	var url = getConstValue('CONTEXT') + '/itg/nbpm/selectProcessDetail.do';
	params['sr_id'] = srId;
	params['task_id'] = taskId;
	params['nbpm_task_id'] = taskId;
	params['queryKey'] = queryKey;
	params['subQueryKey'] = subQueryKey;

	if (PROCESS_META.MULTI_YN == "Y") {
		params['task_group_id'] = PROCESS_META.TASK_GROUP_ID;
	}
	var procAuthId = "";
	// 담당자 지정기능 추가.
	// ajax - list로 리턴됨 ex)변수명 authChargerList
	var authChargerList = [];
	params['process_type'] = PROCESS_META.PROCESS_TYPE;
	nkia.ui.utils.ajax({
        url: getConstValue('CONTEXT') + '/itg/nbpm/searchReqChargerInfo.do',
		params: params,
		isMask: false,
		async: false,
        success: function(response){
			authChargerList = response.resultMap.resultList;
			
        }
	});

	var reqType = "";
	for (var i = 0; i < panelList.length; i++) {
		var panel = panelList[i];
		if(panel instanceof nkia.ui.form.Form) {
			reqType = $$(panel.id)._getFieldValue("req_type");
			var formItems = $$(panel.id)._getValues();

			if(PROCESS_META.TASK_NAME != "END_VIEW"  && PROCESS_META.MYHISTORYLIST_YN != "Y"){
				for(var j=0; j<authChargerList.length; j++) {
					
					var fieldId = authChargerList[j].SELECT_CHARGER_ID;
					var field = formItems[fieldId];
					
					if(typeof field != undefined) {
						var formMap = {};
						//지정 담당자 세팅
						formMap[authChargerList[j].OPER_TYPE] = authChargerList[j].SELECT_CHARGER_NM;
						formMap[authChargerList[j].OPER_TYPE+"_user_id"] = authChargerList[j].SELECT_CHARGER_ID;
						formMap[authChargerList[j].OPER_TYPE+"_grp_select_yn"] = authChargerList[j].GRP_SELECT_YN;
						
						//지정 담당자 그룹 세팅
						if(authChargerList[j].GRP_APPOINT_ID){
							formMap[authChargerList[j].OPER_TYPE] = authChargerList[j].GRP_APPOINT_NM;
							formMap[authChargerList[j].OPER_TYPE+"_user_id"] = authChargerList[j].GRP_APPOINT_ID;
							formMap[authChargerList[j].OPER_TYPE+"_grp_select_yn"] = authChargerList[j].GRP_SELECT_YN;
								
						}
						$$(panel.id)._setValues(formMap);
						
					}
				}
			}
			
//			if(reqType != ""){
//				break;
//			}
		}
	}
	params['req_type'] = reqType;

	if (props['relationYn'] == "true") {
		// 신규등록과 연계요청을 구분하여 파라메터처리를 다르게 한다.
		if (parent.checkRequestPop != null && parent.checkRequestPop()) {
			parent.addRequestParam(params);
		} else {
			parent.addRelationParam(params);
			var refUrl = params['ref_url'];
			if (!(refUrl != null && refUrl != "")) {
				// 프로세스에서 연계되는 경우
				url = getConstValue('CONTEXT') + '/itg/nbpm/selectParentProcessDetail.do';
				for (var i = 0; i < panelList.length; i++) {
					if (panelList[i] != null && panelList[i].isProcessGrid != null && panelList[i].isProcessGrid) {
						panelList[i].getStore().proxy.jsonData["sr_id"] = params.parent_sr_id;
					}
				}
			} else {
				// 프로세스 외적으로(커스텀 선행프로세스 기능 같은 곳에서 내용을 가져와야 하는 경우)
				url = refUrl;
			}
		}
	}
	
	nkia.ui.utils.ajax({
        url: url,
		params: params,
		isMask: false,
		async: false,
        success: function(response){
			var data = response.resultMap;
			// Form Data 처리
			if (data != null) {
				for(key in data){
					if(data[key] == ""){
						delete data[key];
					}
				}
				
				// 의견 목록
				var commentList = data.nbpm_commentList;
				// 담당자 처리 목록
				var operList = data.operList;
				data.operList = null;
				for (var i = 0; i < panelList.length; i++) {
					var panel = panelList[i];
					if(panel instanceof nkia.ui.form.Form){
						var form = $$(panel.id);
						form._syncData(data, false);
						
						// 담당자 처리 부분
						if(operList != null){
							for (var j = 0; operList != null && j < operList.length; j++) {
								if( operList[j] != null && form._getField(operList[j].OPER_TYPE) != null ){
									if (PROCESS_META.MULTI_YN == "Y") {
										// 세팅하려는 담당자가 현재 멀티 타스크 중 일치하는 타스크 일 경우
										if (operList[j].TASK_GROUP_ID == PROCESS_META.TASK_GROUP_ID) {
											setOperUser(panel, operList[j]);
										}
									} else {
										// 담당자 Data 셋팅하기
										setOperUser(panel, operList[j]);
									}
								}
							}
						}
						
						// 의견 처리 부분
						if(commentList != null){
							if( form._getField("nbpm_comment") != null ){
								for (var j = 0; j < commentList.length; j++) {
									if (commentList[j].TEXT != null && commentList[j].TEXT != "" && panel.taskName == commentList[j].TASK_NAME) {
										form._setFieldValue("nbpm_comment", commentList[j].TEXT);
										commentList.splice(j, 1);
										break;
									}
								}
							}
						}
					}
				}
				
				// 의견 이력 처리
				if($$("commentHistory") && data.commentHistory){
					var historyList = data.commentHistory;
					var commentHistory = "";
					if (historyList && historyList.length > 0) {
						for (var i = 0; i < historyList.length; i++) {
							var workUserNm = historyList[i].WORK_USER_NM;
							var workDate = historyList[i].WORK_DATE;
							var nodeName = historyList[i].NODENAME;
							var text = historyList[i].TEXT;
							if (i == 0) {
								commentHistory = commentHistory
										+ "<font class='commentCurrentText'>";
							} else {
								commentHistory = commentHistory
										+ "<font class='commentText'>";
							}
							commentHistory = commentHistory + "[";
							commentHistory = commentHistory + nodeName;
							commentHistory = commentHistory + "/";
							if (workUserNm == "") {
								workUserNm = "정보없음";
							}
							commentHistory = commentHistory + workUserNm;
							commentHistory = commentHistory + "/";
							commentHistory = commentHistory + workDate;
							commentHistory = commentHistory + "] - ";
							if (text == "") {
								text = "내용없음";
							}
							commentHistory = commentHistory + text;
							commentHistory = commentHistory + "<br><br>";
							if (i == 0) {
								commentHistory = commentHistory + "</font>";
							} else {
								commentHistory = commentHistory + "</font>";
							}
						}
					}
					$$("commentHistory").setHTML(commentHistory);
				}
				
				// 첨부 파일 부분
				$$("atch_file_id")._getFileList(data.ATCH_FILE_ID, data.SR_ID);
/*				if($$("atch_file_id")){
					if(PROCESS_META.TASK_NAME == "start" && data.UP_SR_ID !== undefined){
						$$("atch_file_id")._getFileList(data.ATCH_FILE_ID, data.UP_SR_ID);	
					}else{
					}
				} */
				
				// 서식파일 다운로드 부분
				if($$("template_downloader")){
					$$("template_downloader")._getFileList(data.ATCH_FILE_ID, data.SR_ID);
				}
			}
		}
	});
	
	// 숨김 대상 필드들을 숨겨준다.
	for (var i = 0; i < panelList.length; i++) {
		var panel = panelList[i];
		if(panel instanceof nkia.ui.form.Form) {
			var afterHideFields = [];
			var elements = $$(panel.id).elements;
			for(var elName in elements) {
				var isAfterHide = elements[elName].config.isProcAfterHide;
				if(isAfterHide === true) {
					afterHideFields.push(elements[elName].config.name);
				}
			}
			if(afterHideFields.length > 0) {
				$$(panel.id)._hideFields(afterHideFields);
			}
		}
	}
}

// 담당자 데이터 처리
function setOperUser(panel, operData){
	panel.elements.forEach(function(item){
		if(item.cols){
			item.cols.forEach(function(field){
				if(field instanceof nkia.nbpm.fields.OperSelect){
					if(operData.OPER_TYPE == field.name && !field.reassign){
						field._setValue({
							oper_user_id: operData.OPER_USER_ID,
							oper_user_nm: operData.OPER_USER_NM,
							oper_grp_select_yn: operData.GRP_SLCT_YN
						});	
					}
				}
			});
		}
	});
}

/**
 * 프로세스 데이터 취합
 * @param {} props
 * @return {}
 */
function getProcessData(props) {
	var resultMap = {};
	var operList = new Array();
	var gridMapList = new Array();
	var originSrData = new Array();
	var gridFieldList = new Array();

	for (var i = 0; i < panelList.length; i++) {
		var inputData = new Array();
		var panel = panelList[i];
		if (panel != null){
			if(panel instanceof nkia.ui.form.Form && panel.editorMode){
				// Form Data 가져오기				
				var formValues = $$(panel.id)._getValues();
				for (var key in formValues) {
					resultMap[key] = formValues[key];
				}
				
				// 담당자 Data 가져오기
				panel.elements.forEach(function(item){
					if(item.cols){
						item.cols.forEach(function(field){
							if(field instanceof nkia.nbpm.fields.OperSelect){
								var values = field._getValue();
								// 담당자 데이터가 있는 경우에만 List에 담는다
								if(values.oper_user_id != ""){
									operList.push(field._getValue());
								}
							}
							
							if(field.rows){
								field.rows.forEach(function(record){
									if(record instanceof nkia.ui.grid.BaseGrid){
										if($$(record.id).count() > 0) {
											var gridFieldMap = {};
											var insertQuerykey = record.params.insertQuerykey;
											var deleteQuerykey = record.params.deleteQuerykey; 
											var gridDataList = null;
											
											//[등록]후 저장 해야하는 그리드 필드 데이터가 있는 경우(선택저장 그리드, 직접입력 그리드)
											if(insertQuerykey && deleteQuerykey){
												var editorMode = record.params.editorMode;
												
												//직접입력 그리드 필드의 데이터 저장
												if(editorMode === true){
													gridDataList = $$(record.id)._getRows();
													
												}else{
													//선택저장 그리드 필드의 데이터 저장
													gridDataList = $$(record.id)._getSelectedRows();
												}
												
												if(gridDataList.length != 0){
													gridFieldMap["insertQuerykey"] = insertQuerykey;
													gridFieldMap["deleteQuerykey"] = deleteQuerykey;
													
													gridFieldMap["gridList"] = gridDataList;
													gridFieldList.push(gridFieldMap);
												}											
											}
										}
									}
								})
							}
						});
					}
				});
				// Form들의 SelectedRecord 다 똑같으므로 반복문이 돌면서 덮어 씌워도 상관없다.
				// SR 원본 데이터(수정기능을 위해 추가한것 임)
				originSrData = $$(panel.id)._getCachedData();
			} 
			
			if(panel instanceof nkia.ui.grid.BaseGrid){
				
				// 그리드 일 때, 후 처리로 editorMode를 변경하므로 변경된 값을 그리드의 속성으로 세팅
				panel.editorMode = $$(panel.id).config.editorMode;
				
				if(panel.editorMode) {
					// grid객체가 있을 경우
					var gridDataList = $$(panel.id)._getRows();
					// 담당자 선택 그리드일경우
					if (panel.operSel && panel.editorMode) {
						var oper = {};
						oper["select_type"] = "MULTI";
						oper["oper_type"] = panel.roleId;
						var operUserIds = new Array();
						for (var j = 0; j < gridDataList.length; j++) {
							operUserIds.push(gridDataList[j].user_id);
						}
						oper["oper_user_id"] = operUserIds;
						operList.push(oper);
					} else {
						// 데이터성 그리드 일경우
						var gridMap = {};
						gridMap["gridInsertQueryKey"] = panelList[i].insertQuerykey;
						gridMap["gridDeleteQueryKey"] = panelList[i].deleteQuerykey;
						gridMap["gridData"] = gridDataList;
						gridMapList.push(gridMap);
					}
				}
			}
		}
	}
	
	// Tab객체가 있을 경우 - ProcessSubTab
	if ($$("processSubTab")) {
		var itemList = new Array();
		if($$("setSubTabCiAssetGrid")){
			var gridDataList = $$("setSubTabCiAssetGrid")._getRows();
			parserCiData(gridDataList, itemList);
		}
		
		if($$("setSubTabServGrid")){
			var gridDataList = $$("setSubTabServGrid")._getRows();
			parserServiceData(gridDataList, itemList);
		}
		
		if (itemList.length > 0) {
			resultMap["nbpm_itemList"] = itemList;
		}
	}
	resultMap["gridMapList"] = gridMapList;
	resultMap["gridFieldList"] = gridFieldList;
	// @@20130711 김도원 START
	// 상담이력 데이터 수집
	if ($$('counselingHistoryCmp')) {
		resultMap["counseling_content"] = counselHistoryCmp.getValue();
	}
	// @@20130711 김도원 END
	var beforeChildSrDataList = resultMap["child_sr_data"];
	var resultChildSrDataList = new Array();
	for (var i = 0; beforeChildSrDataList != null && i < beforeChildSrDataList.length; i++) {
		var childDataJson = beforeChildSrDataList[i];
		var childDataMap = getJsonToArray(childDataJson);
		resultChildSrDataList.push(childDataMap);
	}

	resultMap["nbpm_operList"] = operList;
	resultMap["originSrData"] = originSrData;
	resultMap["child_sr_data"] = resultChildSrDataList;

	resultMap["multi_yn"] = PROCESS_META.MULTI_YN;
	resultMap["task_group_id"] = PROCESS_META.TASK_GROUP_ID;
	return resultMap;
}

// Ci Data를 요청업무 데이터에 맞게끔 Parser
function parserCiData(gridDataList, itemList) {
	var listSize = gridDataList.length;
	if (listSize > 0) {
		for (var i = 0; i < listSize; i++) {
			var itemJson = {};
			itemJson["item_id"] = gridDataList[i]["CONF_ID"];
			itemJson["ASSET_ID"] = gridDataList[i]["ASSET_ID"];
			itemJson["CONF_NM"] = gridDataList[i]["CONF_NM"];
			itemJson["CLASS_NM"] = gridDataList[i]["CLASS_NM"];
			itemJson["EMS_ID"] = gridDataList[i]["EMS_ID"];
			itemJson["item_type"] = "CI";
			itemList.push(itemJson);
		}
	}
}

// Service Data를 요청업무 데이터 맞게끔 Parser
function parserServiceData(gridDataList, itemList) {
	var listSize = gridDataList.length;
	if (listSize > 0) {
		for (var i = 0; i < listSize; i++) {
			var itemJson = {};
			itemJson["item_id"] = gridDataList[i]["SERVICE_ID"];
			itemJson["item_type"] = "SERVICE";
			itemList.push(itemJson);
		}
	}
}

/**
 * 프로세스 하단 탭 생성(SubTab)
 * @param {} processSubProp
 * @return {}
 */
function createProcessSubInfo(processSubProp) {
	var srId = processSubProp['srId'];
	var originSrId = processSubProp['srId'];
	if (processSubProp['relationYn'] == "true") {
		if (!(parent.checkRequestPop != null && parent.checkRequestPop())) {
			srId = parent.getUpSrId();
		}
	}
	
	var userName = processSubProp['userName'];
	var taskId = PROCESS_META.TASK_ID;
	var taskName = processSubProp['nodeName'];
	var views = processSubProp['views'];
	var sorts = processSubProp['sorts'].split(",");
	var editors = processSubProp['editors'];
	var required = processSubProp['required'];
	var ciButtons = processSubProp['ciButtons'];
	var ciModes = processSubProp['ciModes'];
	var serviceModes = processSubProp['serviceModes'];
	var serviceButtons = processSubProp['serviceButtons'];
	
	// @@20130711 김도원 START
	// 연계정보, 상담이력 변수 선언
	var ciCmp, serviceCmp, fileCmp, commentCmp, srRelationCmp, incRelationCmp;
	var counselHistoryCmp = null;
	
	// Sub Tab 컴포넌트 생성
	for (var i = 0; views != null && i < views.length; i++) {
		var tabView = views[i];
		var isEditor = isExistDataForArray(editors, tabView); // 편집여부 판단
		var isRequired = isExistDataForArray(required, tabView); // 필수여부 판단
		if (tabView == "CI") {
			ciCmp = createCiComponent(srId, isEditor, isRequired, ciButtons, ciModes);
		}else if (tabView == "SERVICE") {
			serviceCmp = createServiceComponent(srId, isEditor, isRequired, serviceButtons, serviceModes);
		}else if (tabView == "FILE") {
			fileCmp = createProcessFileComponent(srId, taskId, taskName, userName, isEditor, isRequired);
		}else if (tabView == "COMMENT") {
			commentCmp = createCommentComponent();
		}
	}
	
	// Tab 순서
	var cells = [];
	var REQUIRED_HTML = "<font color='red' style='padding-left:4px'>*</font>";

	for (var i = 0; i < sorts.length; i++) {
		var tabSort = sorts[i];
		if (tabSort == "CI") {
			var title = getConstText({isArgs : true, m_key : 'res.title.common.ciasset'});
			if(ciCmp[1].required){
				title = title + REQUIRED_HTML;
			}
			cells.push({
				header: title + '<span id="setSubTabCiGrid_badge" class="webix_badge">0</span>',
			 	body: {
			 		rows: ciCmp
			 	}
			});
		} else if (tabSort == "SERVICE") {
			var title = getConstText({isArgs : true, m_key : 'res.title.common.service'});
			if(serviceCmp[1].required){
				title = title + REQUIRED_HTML;
			}
			cells.push({
				header: title + '<span id="setSubTabServGrid_badge" class="webix_badge">0</span>',
			 	body: {
			 		rows: serviceCmp
			 	}
			});
		} else if (tabSort == "FILE") {
			var title = getConstText({isArgs : true, m_key : 'res.title.common.fileInfo'});
			if(fileCmp.required){
				title = title + REQUIRED_HTML;
			}
			cells.push({
				header: title + '<span id="atch_file_id_badge" class="webix_badge">0</span>',
			 	body: {
			 		rows: fileCmp.rows
			 	}
			});
		} else if (tabSort == "COMMENT") {
			cells.push({
				header: getConstText({ isArgs : true, m_key : 'res.title.process.00003'}),
				badge: 10,
			 	body: {
			 		rows: [commentCmp]
			 	}
			});
		}
	}

	cells.push({
		header: getConstText({ isArgs : true, m_key : 'res.title.common.reqRelation' }),
	 	body: {
	 		rows: createSrRelationComp({id:"srRelation", srId: originSrId})
	 	}
	});
	
	//장애요청인지 확인 후 연관장애컴포넌트 추가
	if(PROCESS_META.PROCESS_TYPE == "INCIDENT"){
		cells.push({
			header: "연관장애정보",
		 	body: {
		 		rows: createIncRelationComp({id:"incRelation", srId: originSrId})
		 	}
		});
	}
	
	var subTabs = null;
	if (cells.length > 0) {
		subTabs = {
			rows: [{
				view:"tabview", 
				id:'processSubTab', 
				animate:false,
	          	cells: cells
		 	}]
		}
	}
	return subTabs;
}

// 구성조회 Component 생성
function createCiComponent(srId, isEditor, isRequired, ciButtons, ciModes) {
	var header = {
		title : getConstText({isArgs : true, m_key : 'res.title.common.ciasset'})
	}
	
	if(ciButtons != null && ciButtons.length > 0){
		header.buttons = {
			items: ciButtons
		};
	}
	
	var ciAssetProp = {
		id : "setSubTabCiAssetGrid",
		header: header,
		resource : "grid.nbpm.asset",
		url : "/itg/nbpm/common/searchRelConfList.do",
		badge: true,
		params : {
			sr_id : srId,
			item_type : 'CI'
		},
		pageable: false,
		autoLoad: (srId) ? true : false,
		height: CONSTRAINT.SHORT_GRID_HEIGHT,
		required: isRequired,
		editorMode: isEditor,
		checkbox: isEditor,
		keys: ["CONF_ID"],
		sortConfig: 'text',
		badgeId: "setSubTabCiGrid_badge"
	};
	
	// CI Mode 설정
	var appendColumns = [];
	if (ciModes != null && ciModes.length > 0) {
		for (var i = 0; i < ciModes.length; i++) {
			var ciMode = ciModes[i];
			if (ciMode == "VIEW") {
				appendColumns.push(ciViewColumn());
			} else if (ciMode == "EDIT") {
				appendColumns.push(ciEditColumn());
			} else if (ciMode == "PROC_HISTORY") {
				appendColumns.push(ciProcHistoryColumn());
			}
		}
	}
	
	if(appendColumns.length > 0){
		ciAssetProp.appendColumns = appendColumns;
	}
	
	var setCiAssetGrid = createGridComp(ciAssetProp);
	
	/*
	setCiAssetGrid.getStore().on('load', function(thisStore) {
		var forms = Ext.ComponentQuery.query("form");
		for (var i = 0; i < forms.length; i++) {
			if (forms[i].editorMode) {
				var lvlFields = forms[i].query("inclevel");
				if (lvlFields.length > 0) {
					url = getConstValue('CONTEXT')
							+ "/itg/nbpm/common/searchIncLevelInfo.do";
				} else {
					lvlFields = forms[i].query("chalevel");
					url = getConstValue('CONTEXT')
							+ "/itg/nbpm/common/searchChaLevelInfo.do";
				}
				if (lvlFields != null && lvlFields.length > 0) {
					if (!(lvlFields[0].getValue() != null && lvlFields[0]
							.getValue() != "")) {
						var ciSelGird = setCiAssetGrid;
						var gridDataList = ciSelGird.getGridData();
						var listSize = gridDataList.length;
						var maxLevel = "";
						var maxLevelNm = "";
						var intMaxLevel = 0;
						if (listSize > 0) {
							for (var i = 0; i < listSize; i++) {
								var confLevel = gridDataList[i]["conf_level"]
								var confLevelNm = gridDataList[i]["conf_level_nm"]
								var intConfLevel = parseInt(confLevel);
								if (intMaxLevel < intConfLevel) {
									intMaxLevel = intConfLevel;
									maxLevel = confLevel;
									maxLevelNm = confLevelNm;
								}
							}
							if (maxLevel != "") {
								var param = {};
								param["effect"] = maxLevel
								jq.ajax({
											type : "POST",
											url : url,
											contentType : "application/json",
											dataType : "json",
											async : false,
											data : getArrayToJson(param),
											success : function(data) {
												lvlFields[0]
														.selectedCell(
																data.resultMap.inc_level,
																data.resultMap.inc_level_time,
																data.resultMap.inc_level_nm,
																data.resultMap.effect_nm,
																data.resultMap.uegency_nm)
											}
										});
							}
						}
					}
				}
			}
		}
	});
	*/
	
	
	return setCiAssetGrid;
}

// 구성보기
function ciViewColumn() {
	var ciViewColumn = {
		id: "CI_VIEW",
		text: getConstText({ isArgs : true, m_key : 'res.common.label.detailview'}),
		width: 50,
		template: nkia.ui.html.icon("arrow_down", "view_ci"),
		onClick: {
			"view_ci": function(e, row, html){
				var gridId = this.config.id;
				var item = $$(gridId).getItem(row);
				
				var asset_id = item["ASSET_ID"];
				var conf_id = item["CONF_ID"];
				var class_id = item["CLASS_ID"];
				var class_type = item["CLASS_TYPE"];
				var logical_yn = item["LOGICAL_YN"];
				var tangible_asset_yn = item["TANGIBLE_ASSET_YN"];
				var center_asset_yn = item["CENTER_ASSET_YN"];
				var view_type = "pop_view";
				var entity_class_id = "";

				if (tangible_asset_yn != "Y" && logical_yn == "Y") {
					entity_class_id = class_id + "-0";
				} else {
					entity_class_id = class_id;
				}
				var param = "?asset_id=" + asset_id + "&conf_id=" + conf_id + "&class_id=" + class_id + "&view_type=" + view_type + "&entity_class_id=" + entity_class_id + "&center_asset_yn=" + center_asset_yn + "&class_type="+class_type +"&tangible_asset_yn="+tangible_asset_yn;
				showAssetDetailPop(param);
			}
		}
	}
	return ciViewColumn;
}

// 구성편집 컬럼
function ciEditColumn() {
	var ciEditColumn = {
		id: "CI_EDIT",
		text: getConstText({ isArgs : true, m_key : 'res.common.label.change'}),
		width: 80,
		template: nkia.ui.html.icon("arrow_down", "edit_ci"),
		onClick: {
			"edit_ci": function(e, row, html){
				var gridId = this.config.id;
				var item = $$(gridId).getItem(row);
				
				if(item.CLASS_TYPE != 'HW'){
					var asset_id = item["ASSET_ID"];
					var conf_id = item["CONF_ID"];
					var class_id = item["CLASS_ID"];
					var class_type = item["CLASS_TYPE"];
					var logical_yn = item["LOGICAL_YN"];
					var tangible_asset_yn = item["TANGIBLE_ASSET_YN"];
					var center_asset_yn = item["CENTER_ASSET_YN"];
					var view_type = "pop_edit";
					var entity_class_id = "";

					if (tangible_asset_yn != "Y" && logical_yn == "Y") {
						entity_class_id = class_id + "-0";
					} else {
						entity_class_id = class_id;
					}
					var param = "?asset_id=" + asset_id + "&conf_id=" + conf_id + "&class_id=" + class_id + "&view_type=" + view_type + "&entity_class_id=" + entity_class_id + "&center_asset_yn=" + center_asset_yn+ "&class_type="+class_type +"&tangible_asset_yn="+tangible_asset_yn +"&sr_id="+PROCESS_META.SR_ID;
					showAssetDetailPop(param);
				}else{
					
					nkia.ui.utils.notification({
						type: 'error',
						message: '변경할 수 없는 구성자원입니다.'
					});
					
				}
				
				
			}
		}
	}
	return ciEditColumn;
}

// 구성 프로세스 이력 컬럼
function ciProcHistoryColumn() {
	var ciProcHistoryColumn = {
		id: "CI_PROC_HISTORY",
		text: getConstText({ isArgs : true, m_key : 'res.common.label.history'}),
		width: 80,
		template: nkia.ui.html.icon("arrow_down", "history_ci_proc"),
		onClick: {
			"history_ci_proc": function(e, row, html){
				var gridId = this.config.id;
				var item = $$(gridId).getItem(row);
				
				var setPopUpProp = {
					height : 473,
					width : 870,
					id : 'processGridPop',
					url : getConstValue('CONTEXT') + '/itg/nbpm/common/searchProcessList.do',
					buttonAlign : 'center',
					params : {
						item_id : recordRaw["CONF_ID"],
						item_type : "CI"
					},
					acceptBtnVisible : false
				}
				var popUp = createProcessSelectPop(setPopUpProp);
				popUp.show();
			}
		}
	}
	return ciProcHistoryColumn;
}

// 서비스조회 Component 생성
function createServiceComponent(srId, isEditor, isRequired, serviceButtons, serviceModes) {
	var header = {
		title: getConstText({isArgs : true, m_key : 'res.title.common.service'})
	}
	
	if(serviceButtons != null && serviceButtons.length > 0){
		header.buttons = {
			items: serviceButtons
		};
	}
	
	var servProp = {
		id : "setSubTabServGrid",
		header: header,
		resource: "grid.nbpm.service",
		url: "/itg/nbpm/common/searchRelConfList.do",
		params : {
			sr_id : srId,
			item_type : 'SERVICE'
		},
		pageable: false,
		autoLoad: (srId) ? true : false,
		height: CONSTRAINT.SHORT_GRID_HEIGHT,
		required: isRequired,
		editorMode: isEditor,
		checkbox: isEditor,
		keys: ["SERVICE_ID"],
		badgeId: "setSubTabServGrid_badge"
	};

	// CI Mode 설정
	var appendColumns = [];
	if (serviceModes != null && serviceModes.length > 0) {
		for (var i = 0; i < serviceModes.length; i++) {
			var serviceMode = serviceModes[i];
			if (serviceMode == "PROC_HISTORY") {
				appendColumns.push(serviceProcHistoryColumn());
			}
		}
	}
	
	if(appendColumns.length > 0){
		servProp.appendColumns = appendColumns;
	}
	
	var setServGrid = createGridComp(servProp);

	return setServGrid;
}

// 서비스 프로세스 요청이력 컬럼
function serviceProcHistoryColumn() {
	var serviceProcHistoryColumn = {
		id: "SERV_PROC_HISTORY",
		text: getConstText({ isArgs : true, m_key : 'res.common.label.history'}),
		width: 80,
		template: nkia.ui.html.icon("arrow_down", "history_ci_proc"),
		onClick: {
			"history_ci_proc": function(e, row, html){
				var gridId = this.config.id;
				var item = $$(gridId).getItem(row);
				
				var setPopUpProp = {
					height : 473,
					width : 870,
					id : 'processGridPop',
					url : getConstValue('CONTEXT')
							+ '/itg/nbpm/common/searchProcessList.do',
					buttonAlign : 'center',
					params : {
						item_id : recordRaw["CONF_ID"],
						item_type : "CI"
					},
					acceptBtnVisible : false
				}
				var popUp = createProcessSelectPop(setPopUpProp);
				popUp.show();
			}
		}
	}
	return serviceProcHistoryColumn;
}

// 프로세스 파일첨부 Component 생성
function createProcessFileComponent(srId, taskId, taskName, userName, isEditor, isRequired) {
	if(PROCESS_META.TASK_NAME == "start" && taskName == ""){
		// 요청등록(임시저장)에서 작업명이 비어있는 경우
		nkia.ui.utils.ajax({
	        url: '/itg/nbpm/common/getNbpmNodeName.do',
			params: { processid: PROCESS_META.PROCESSID, task_name: "REQUST_REGIST" },
			async: false,
			isMask: false,
	        success: function(response){
	        	taskName = response.resultString;
			}
		});
	}
	
	var fileProp = {
		id: "atch_file_id",
		//height : CONSTRAINT.UBIT_SHORT_GRID_HEIGHT,
		height : CONSTRAINT.SHORT_GRID_HEIGHT,
		isProcess: true,
		params : {
			sr_id: srId,
			task_id: ( PROCESS_META.TASK_NAME == "start" ) ? "REQUST_REGIST" : PROCESS_META.TASK_NAME 
		},
		readonly: !isEditor,
		allowFileExt: CONSTRAINT.DEFAULT_FILE_ALLOW_EXT,
		required: isRequired,
		taskName: taskName,
		register: userName,
		badgeId: "atch_file_id_badge"
	};
	var file = createAtchFileComp(fileProp);
	return file;
}

// Comment 이력 Component 생성
function createCommentComponent() {
	var comment = {
		view: "template",
		id: "commentHistory",
		css: "commentHistory",
		height: CONSTRAINT.SHORT_GRID_HEIGHT,
		scroll: "y"
	};
	return comment;
}

/**
 * procSubActionLink
 * 서브탭 객체에서의 이벤트 제어
 * @param {} command
 */
function procSubActionLink(command) {
	switch (command) {
		case "subCiAddBtn" :
			ciAddBtnFunc("setSubTabCiAssetGrid");
			break;
		case "subCiDelBtn" :
			if(PROCESS_META.TASK_NAME == "CHANGE_PLAN"){
				var paramMap = {};
				paramMap["gridData"] = $$("setSubTabCiAssetGrid")._getSelectedRows();
				paramMap["srId"] = PROCESS_META.SR_ID;
				nkia.ui.utils.ajax({
					url: "/itg/customize/ubit/deleteSnapshotDetail.do",
					params: paramMap,
					isMask: true,
					notification: true,
					success: function(data){
						if (data.success) {
							if($$('change_plan')._getFieldValue("snapshot_set") != "스냅샷 설정전입니다."){
								alert("구성자원 삭제로 인하여 스냅샷설정을 다시 진행하여 주십시오.");
								$$("setSubTabCiAssetGrid")._removeRow();
				        		$$('change_plan')._setValues({snapshot_set:'스냅샷 설정전입니다.'});
							}else{
								$$("setSubTabCiAssetGrid")._removeRow();
							}
						} 
					}
				});
			}else{
				$$("setSubTabCiAssetGrid")._removeRow();
			}
			
			break;
		case "subServAddBtn" :
			serviceAddBtnFunc("setSubTabServGrid");
			break;
		case "subServDelBtn" :
			$$("setSubTabServGrid")._removeRow();
			break;
	}
}

/**
 * 연계정보 상세보기
 * @param {} srId
 */
function viewSrRelationDetail(srId) {
	var setPopUpProp = {
		popupId : 'srRelationDetailPopup',
		popupWidth : 950,
		popupHeight : 550,
		param : {
			sr_id : srId
		}
	}
	var popUp = createSrDetailPop(setPopUpProp);
}

/**
 * 구성조회팝업
 * @param {} openerTargetGridId
 */
function ciAddBtnFunc(openerTargetGridId) {
	var type = getConstValue('CI_POP_TYPE');

	var req_zone = "";
	if(type == "ciAndService"){
		for (var i = 0; i < panelList.length; i++) {
			var panel = panelList[i];
			if(panel instanceof nkia.ui.form.Form) {
				req_zone = $$(panel.id)._getFieldValue("req_zone");
				break;
			}
		}
		
		if(req_zone == ""){
			req_zone = getConstValue('DEFAULT_REQ_ZONE');
		}
	}
	
	var params = {
		req_zone: req_zone
	}
	params['checkbox'] = true; //구성단일인 경우
	
	//요청유형이 '서버폐기'인 경우이면 구성조회 리스트는 운영유무가 '아니오'인 데이터만 조회 기본은 운영유무가 '아니오'가 아닌 데이터만 조회
	var operState = "Y";
	if(PROCESS_META.PROCESS_TYPE == 'DISUSE'){
		if($$('requst_regist')){
			var disuseCd = $$('requst_regist')._getFieldValue('disuse_cd');
			if(disuseCd){
				if(disuseCd == '02'){
					operState = 'N';
				}
			}
		}else{
			if($$('sec_requst_regist')){
				disuseCd = $$('sec_requst_regist')._getFieldValue('disuse_cd');
				if(disuseCd){
					if(disuseCd == '02'){
						operState = 'N';
					}
				}
			}
		}
	}
	params['oper_state'] = operState;

	
	var width = nkia.ui.utils.getWidePopupWidth();
	var height = nkia.ui.utils.getWidePopupHeight();
	
	nkia.ui.utils.window({
		id: "ciSearchWindow",
		title: "구성 조회",
		width : width,
		height : height,
		type: 'ciAndService',//getConstValue('CI_POP_TYPE'), //구성+서비스
		selectType: $$("subCiAddBtn").config.selectType,
		gridParams: params,
		reference: {
			data: {
				asset: $$(openerTargetGridId)._getRows()		// 구성자원 Grid
				//, service: $$("setSubTabServGrid")._getRows() // 서비스 Grid
			},
			grid: {
				asset: $$(openerTargetGridId)			// 구성자원 Grid
				//, service: $$("setSubTabServGrid")	// 서비스 Grid
			}
		},
		callFunc: createAssetWindow
	});
}

// 서비스조회팝업
function serviceAddBtnFunc(openerTargetGridId) {
	var type = getConstValue('CI_POP_TYPE');

	var req_zone = "";
	if(type == "ciAndService"){
		for (var i = 0; i < panelList.length; i++) {
			var panel = panelList[i];
			if(panel instanceof nkia.ui.form.Form) {
				req_zone = $$(panel.id)._getFieldValue("req_zone");
				break;
			}
		}
		
		if(req_zone == ""){
			req_zone = getConstValue('DEFAULT_REQ_ZONE');
		}
	}
	
	var params = {
		req_zone: req_zone
	}
	
	var width = nkia.ui.utils.getWidePopupWidth();
	var height = nkia.ui.utils.getWidePopupHeight();
	
	nkia.ui.utils.window({
		id: "serviceSearchWindow",
		title: "서비스 조회",
		width : width,
		height : height,
		type: getConstValue('CI_POP_TYPE'),
		selectType: $$("subServAddBtn").config.selectType,
		params: params,
		reference: {
			data: {
				asset: $$("setSubTabCiAssetGrid")._getRows(),
				service: $$(openerTargetGridId)._getRows()
			},
			grid: {
				asset: $$("setSubTabCiAssetGrid"),	// 구성자원 Grid
				service: $$(openerTargetGridId)		// 서비스 Grid
			}
		},
		callFunc: createServiceWindow
	});
}

/**
 * 프로세스 에디터 폼 생성
 * @param {} formProp
 * @return {}
 */
function createProcEditorFormComp(formProp) {
	var REQUEST_PAGE = "REQUST_REGIST";
	if (PROCESS_META.CLAIM_YN == "Y" && formProp.taskName == REQUEST_PAGE) {
		var acceptBtn = createBtnComp({
			id: 'acceptBtn', 
			label: "접수하기", 
			type: "form", 
			exclude: true,
			click: function(){
				nkia.ui.utils.ajax({
			        url: '/itg/nbpm/process/acceptGroupAssignTask.do',
					params: { task_id : PROCESS_META.TASK_ID},
					isMask: false,
			        success: function(response){
			        	var queryParam = getUrlParams();
						queryParam["claim_yn"] = "N"
						var queryString = ""; 
						for(var key in queryParam) {
							queryString = queryString + key + "=" + queryParam[key];
							queryString = queryString + "&";
						}
						location.href = location.pathname + "?" + queryString;
					}
				});
			}
		});
		nkia.ui.utils.attach.footerButton(formProp, acceptBtn);
	}        
	
	var taskName = ( PROCESS_META.TASK_NAME == 'start' ) ? REQUEST_PAGE: PROCESS_META.TASK_NAME;
	if (formProp.taskName == taskName && PROCESS_META.SANCTN_LINE_SLCT_YN == "Y") {
		var lineSelectBtn = createBtnComp({
			id : 'lineSelectBtn',
			label : "결재자라인 불러오기",
			type : 'form',
			click: function(){
				var params = {};
				var process_type = "";
				var line_user_id = "";
				
				params['editFormId'] = formProp.id;
				params['process_type'] = PROCESS_META.PROCESS_TYPE; // 요청 타입
				
				nkia.ui.utils.window({
					id: "sanctionAuthPop",
					title: "결재자라인",
					params : params,
					type: "sanctionLine",
					reference: {},
					callFunc: createProcessWindow
				});
			}
		});
		
		nkia.ui.utils.attach.headerButton(formProp, lineSelectBtn);
		
	}
	
	if (formProp.taskName == taskName && PROCESS_META.FAQ_YN == "Y") {
		var faqSelectBtn = createBtnComp({
			id : 'faqSelectBtn',
			label : "FAQ조회",
			type : 'form',
			click: function(){
				var url = getConstValue('CONTEXT') + "/itg/system/board/goFaqBoardPopup.do";
				fnPopupOpen(url, "faqPopup", 1000, 600, "no", "no", "no");
			}
		});
		
		nkia.ui.utils.attach.headerButton(formProp, faqSelectBtn);
		
	}
	
	if ((formProp.taskName == taskName && PROCESS_META.PROCESS_STATIS_YN == "Y") && (PROCESS_META.CURRENT_WORKER_ID == PROCESS_META.NBPM_USER_ID)) {
		var statisSelectBtn = createBtnComp({
			id : 'statisSelectBtn',
			label : "고객만족도 평가",
			type : 'form',
			click: function(){
				var url = "/itg/system/statis/goStatisPopup.do?"+"sr_id="+PROCESS_META.SR_ID;
				statisPopupOpen(url, "statisPopup", 700, 650, "no", "no", "no");
			}
		});
		
		nkia.ui.utils.attach.headerButton(formProp, statisSelectBtn);
		
	}
	
	/* BMT용
	if ("start" == PROCESS_META.TASK_NAME && REQUEST_PAGE == formProp.taskName) {
		var templateBtn = createBtnComp({
			id : 'templateBtn',
			label : "템플릿보기",
			type : 'form',
			click: function(){
				// 템플릿 팝업
				createTemplatPopup();
			}
		});
		nkia.ui.utils.attach.headerButton(formProp, templateBtn);
	}
	*/
	var editorForm = createFormComp(formProp);
	return editorForm;
}

/**
 * 프로세스 Grid 생성
 * @param {} setGridProp
 * @return {}
 */
function createProcGridComp(setGridProp) {
	var gridComp = createGridComp(setGridProp);
	return gridComp;
}

/**
 * 프로세스 편집 패널 설정 및 제어
 * @param {} panelList
 * @param {} presentPanelList
 */
function setEditorPanels(panelList, presentPanelList) {
	var presentPanelCount = presentPanelList.length;
	
	// Form Panel Control
	for (var i = 0; i < panelList.length; i++) {
		var panel = panelList[i];
		if(panel instanceof nkia.ui.form.Form){
			if (PROCESS_META.TASK_NAME == "END_EDIT") {
				if (panel.editorMode) {
					$$(panel.id)._setControl({editorMode: panel.editorMode, exceptions: ['nbpm_comment']});
				} else {
					$$(panel.id)._setControl({editorMode: panel.editorMode});
				}
			} else {
				$$(panel.id)._setControl({editorMode: panel.editorMode});
			}
		}
		var templateDownloader = $$('template_downloader');
		if(templateDownloader) {
			templateDownloader.disable();
		}
	}  

	// Form Panel CSS 처리
	if (presentPanelCount > 0) {
		for (var i = 0; i < presentPanelCount; i++) {
			var presentPanel = presentPanelList[i];
			for(var j = 0; j < presentPanel.length; j++){
				if(presentPanel[j].view == "toolbar"){
					$$(presentPanel[j].id).define("css", "presentPanel");
				}
			}
		}
	}
	
	// 첨부파일 Header와 Drag&Drop 부여
	var file = "atch_file_id";
	if($$(file)){
		$$(file).addDropZone($$(file + "_list").$view, "");
	}
	
}

/**
 * 폼 벨리데이션 체크
 * @param {} panelList
 * @return {}
 */
function isValidFormList(panelList) {
	var isValid = true;
	var multiWorkGridValid = true;
	for(var i = 0; i < panelList.length; i++){
		var panel = panelList[i];
		if(panel instanceof nkia.ui.form.Form){
			if(!$$(panel.id)._validate()) {
				isValid = false;
				break;
			}
			
			panel.elements.forEach(function(item){
				if(item.cols){
					item.cols.forEach(function(field){
						if(field.rows){
							field.rows.forEach(function(record){
								if(record instanceof nkia.ui.grid.BaseGrid){
									var processGrid = $$(record.id);
									
										//프로세스 그리드 컨포넌트의 행에 대한 밸리데이션 체크(데이터가 있는 경우)
										if(processGrid.count() != undefined && processGrid.count() > 0){
											processGrid.eachRow(function(row){ 
											var myRow = processGrid.getItem(row);
											processGrid.eachColumn(function(columnid) {
													
												//프로세스 그리드 컨포넌트 밸리데이션 체크
												var requiredColumns = this.config.requiredColumns;
												if(requiredColumns.indexOf(columnid) > -1) {	
													if(typeof myRow[columnid]=="undefined" || (myRow[columnid]+"")=="" || myRow[columnid]==null) {
														
														if(this.config.id == 'multi_work_grid'){
															multiWorkGridValid = false;
														}else{
															isValid = false;
														}
														
													} else {
													}
												}
											}, true);
										});
									}else{
										//프로세스 그리드 컨포넌트의 행에 대한 밸리데이션 체크(데이터가 없는 경우)
									}
									//프로세스 그리드 컨포넌트 필수값인 경우
									if(processGrid.config.required===true && processGrid.count() == 0){
										if(processGrid.config.id == 'multi_work_grid'){
											multiWorkGridValid = false;
										}else{
											isValid = false;
										}
									}	
								}
							});
						}
					});
				}
			});
			if(!isValid){
				nkia.ui.utils.notification({
					type: 'error',
					message: '필수 입력 항목(그리드)을 확인바랍니다.'
				});
				return false;
			}else{
				
			}
			if(!multiWorkGridValid){
				nkia.ui.utils.notification({
					type: 'error',
					message: '부서승인자를 선택해주세요.'
				});
				return false;
			}else{
				
			}
		}
	}
	return isValid;
}

/**
 * 필드값 가져오기
 * @param {} formId
 * @param {} xtype
 * @param {} fieldName
 * @return {}
 */
function procGetFieldValue(formId, xtype, fieldName) {
	var value = "";
	var form = $$(formId);
	if (form) {
		value = form._getFieldValue(fieldName);
	}
	return nkia.ui.utils.check.nullValue(value);
}

/**
 * 필드값 Clear
 * @param {} formId
 * @param {} xtype
 * @param {} fieldName
 */
function procClearFieldValue(formId, xtype, fieldName) {
	var form = $$(formId);
	if (form) {
		$$(formId)._setFieldValue(fieldName, "");
	}
}

/**
 * 필수해제 Clear
 * @param {} formId
 * @param {} xtype
 * @param {} fieldName
 */
function procRequiredClearField(formId, xtype, fieldName) {
	var form = $$(formId);
	if (form) {
		var field = form._getField(fieldName);
		if(field){
			field.config.required = false;
		}
	}
}

/**
 * 필드에 Validation을 체크한 다음에 다시 풀어준다.
 * @param {} formId
 * @param {} xtype
 * @param {} fieldName
 */
function procCheckRequriedField(formId, xtype, fieldName) {
	var form = $$(formId);
	if(form){
		var field = form._getField(fieldName);
		if(field){
			field.config.required = true;
			field.validate();
			field.config.required = false;	
		}
	}
}

/**
 * 필수 필드 부여
 * @param {} formId
 * @param {} xtype
 * @param {} fieldName
 */
function procSetRequriedField(formId, xtype, fieldName) {
	var form = $$(formId);
	if (form) {
		var field = form._getField(fieldName);
		if(field){
			field.config.required = true;
		}
	}
}

/**
 * 필드의 필수여부를 가져온다
 * @param {} formId
 * @param {} xtype
 * @param {} fieldName
 */
function procRequiredRegainField(formId, xtype, fieldName) {
	var form = $$(formId);
	if (form) {
		var field = form._getField(fieldName);
		if(field){
			field.config.required = true;
		}
	}
}

/**
 * 서브탭 Validation
 * @param {} panelList
 * @param {} nodeName
 * @return {Boolean}
 */
function procSubInfoTabValid(panelList, nodeName, taskName) {
	var processSubTab = $$("processSubTab");
	
	// 임시저장이 아닐 때 Validation 체크
	if(taskName != "tempRegister"){
		if($$("processSubTab")){
			// 구성 체크
			if($$("setSubTabCiAssetGrid")){
				var ciAssetGrid = $$("setSubTabCiAssetGrid");
				if(ciAssetGrid.config.required){
					var isAttachCiAsset = false;
					if(ciAssetGrid._getRows().length > 0){
						isAttachCiAsset = true;
					}
					if(!isAttachCiAsset){
						nkia.ui.utils.notification({
							type: 'error',
							message: '등록된 구성 정보가 없습니다.'
						});
						return false;
					}
				}
			}
			
			// 서비스 체크
			if($$("setSubTabServGrid")){
				var serviceGrid = $$("setSubTabServGrid");
				if(serviceGrid.config.required){
					var isAttachService = false;
					if(serviceGrid._getRows().length > 0){
						isAttachService = true;
					}
					if(!isAttachService){
						nkia.ui.utils.notification({
							type: 'error',
							message: '등록된 서비스 정보가 없습니다.'
						});
						return false;
					}
				}
			}
			
			// 파일 체크
			if($$("atch_file_id")){
				var file = $$("atch_file_id");
				if(file.config.required){
					var isAttachFile = false;
					var fileRows = file._getRows();
					if(fileRows.length > 0){
						fileRows.some(function(item){
						    if(item.taskName == nodeName){
						    	isAttachFile = true;
						    	return true;
						    }
						});
					}
					if(!isAttachFile){
						nkia.ui.utils.notification({
							type: 'error',
							message: '등록된 첨부 파일이 없습니다.'
						});
						return false;
					}
				}
			}
		}
	}
	
	
	return true;
}

// 구성 또는 서비스 벨리드
function procCiOrServiceValid() {
	var isValidation = false;
	if ($$("setSubTabCiAssetGrid")) {
		var subTabCiAssetGrid = $$("setSubTabCiAssetGrid");
		if (subTabCiAssetGrid._getCount() > 0) {
			isValidation = true;
		}
	}

	if ($$("setSubTabServGrid")) {
		var subTabServGrid = $$("setSubTabServGrid");
		if (subTabServGrid._getCount() > 0) {
			isValidation = true;
		}
	}

	if (!isValidation) {
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + setSubTabCiAssetGrid.originTitle + "] " + getConstText({isArgs : true, m_key : 'msg.common.or'}) + " [" + setSubTabServGrid.originTitle + "] " + getConstText({isArgs : true, m_key : 'msg.common.00004'})
		});
	}
	return isValidation;
}

/**
 * 연계정보 생성
 * @param {} props
 * @return {}
 */
function createSrRelationComp(props) {
	var grid = createGridComp({
		id: 'srRelation',
		header: {
        	title: getConstText({isArgs : true, m_key : 'res.title.common.reqRelation'})
	    },
	    //height: CONSTRAINT.UBIT_SHORT_GRID_HEIGHT,
	    height : CONSTRAINT.SHORT_GRID_HEIGHT,
	    resource: "grid.nbpm.srrelation",
	    params: {sr_id: props.srId, expandLevel: 99, id: 'root', node: 'root'},
		url: "/itg/nbpm/common/searchSrRelationList.do",
		pageable: false,
		on:{
			onItemDblClick: function(id, e){
				var item = this.getItem(id);
				showProcessDetailPop(item.SR_ID);
			}
		}
	});		
	
	return grid;
}

/**
 * 연계정보 생성
 * @param {} props
 * @return {}
 */
function createIncRelationComp(props) {
	var grid = createGridComp({
		id: 'incRelation',
		header: {
        	title: "연관장애정보"
	    },
	    //height: CONSTRAINT.UBIT_SHORT_GRID_HEIGHT,
	    height : CONSTRAINT.SHORT_GRID_HEIGHT,
	    resource: "grid.nbpm.increlation",
	    params: {sr_id: props.srId, expandLevel: 99, id: 'root', node: 'root'},
		url: "/itg/nbpm/common/searchIncRelationList.do",
		pageable: false,
		on:{
			onItemDblClick: function(id, e){
				var item = this.getItem(id);
				showProcessDetailPop(item.SR_ID);
			}
		}
	});		
	
	return grid;
}

// 상담이력 컴포넌트 생성 함수
function createCounselingHistoryComponent(srId) {
	var counselHistoryCmp = createTextAreaFieldComp({
		id: "counselingHistoryCmp",
		name: "counseling_content",
		value: "",
		width: '100%',
		height: 240
	})

	var counselHistoryPanel = Ext.create('nkia.custom.CounselingHistory', {
		title: getConstText({ isArgs : true, m_key : 'res.title.process.00001'}),
		items: counselHistoryCmp,
		height: 210,
		srId: srId
	});
	return counselHistoryPanel;
}

// @@20130711 김도원 START
// 요청이력 컴포넌트 생성 함수
function createCommenHistoryFormComp(setProp) {
	var that = {};
	for (var prop in setProp) {
		that[prop] = setProp[prop];
	}

	/*
	 * var commentHistoryCmp = createTextAreaFieldComp({id:"commentHistoryCmp",
	 * "name":"commentHistory","value":"","width":'100%',height:240, readOnly:
	 * true})
	 */
	
	/*
	var commentHistoryCmp = Ext.create('Ext.panel.Panel', {
				cls : "commentHistory",
				// height: 100,
				// border: true,
				// frame: true,
				autoScroll : true
			})

	var commentForm = Ext.create('nkia.custom.CommentForm', {
				id : that["id"],
				title : getConstText({
							isArgs : true,
							m_key : 'res.title.process.00003'
						}),
				items : commentHistoryCmp
			});
	*/
	return commentForm;
}
// @@20130711 김도원 END

function createRequestRelationComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var requestRelationcomp = Ext.create('nkia.custom.RequestRelation', {
				srId : that["srId"],
				name : that["name"] ? that['name'] : 'child_sr_id',
				label : that["label"] ? that['label'] : getConstText({
							m_key : "REL_PROC"
						}),
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				updateCol : that['updateCol'],
				updateTable : that['updateTable'],
				submitType : that['submitType'],
				processType : that['processType'] ? that['processType'] : '',
				targetFormId : that['targetFormId'] ? that['targetFormId'] : '',
				width : getConstValue('DEFAULT_FIELD_WIDTH')
			});

	return requestRelationcomp;
}

function createRelIncViewComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var relIncViewComp = Ext.create('nkia.custom.RelIncViewField', {
		label : getConstText({
					m_key : "REL_INC_SEARCH"
				}),
		value : that['value'],
		name : that['name'],
		labelAlign : that['labelAlign']
				? that['labelAlign']
				: getConstValue('DEFAULT_LABEL_ALIGN'),
		width : that['width']
				? that['width']
				: getConstValue('DEFAULT_FIELD_WIDTH')
	});

	return relIncViewComp;
}

// @@20130731 김도원 START
// 프로세스타입을 속성에 추가함
/*
 * srId : srId name : name processType : processType targetFormId : targetFormId
 * updateCol : updateCol updateTable : updateTable
 * 
 */
function clickRelationRequestBtn(props) {
	window.open(
		"/itg/nbpm/common/goRelationRequestPop.do?processType="
				+ props.processType + "&up_sr_id=" + props.srId
				+ "&field_name=" + props.name + "&targetFormId="
				+ props.targetFormId + "&updateCol="
				+ props.updateCol + "&updateTable="
				+ props.updateTable + "&submitType="
				+ props.submitType,
		"pop",
		"width=1050,height=750,history=no,resizable=yes,status=no,scrollbars=no,member=no"
	);
}

function clickAddBtnInOperUserGrid(gridId) {
	var gridComp = Ext.getCmp(gridId);
	var params = {};
	if (gridComp.roleId != "servicedesk") {
		params["auth_id"] = gridComp.processType + "_" + gridComp.roleId;
	} else {
		params["auth_id"] = gridComp.roleId;
	}
	var setPopUpProp = {
		targetGrid : gridId,
		url : '/itg/nbpm/common/searchProcessOperUserList.do',
		buttonAlign : 'center',
		bodyPadding : '5 5 5 5 ',
		bodyStyle : 'background-color: white; ',
		params : params,
		isMulti : true
	}
	var popUp = createUserMultiSelectPop(setPopUpProp);
	popUp.show();
}

function clickDeleteBtnInOperUserGrid(gridId) {
	var gridComp = Ext.getCmp(gridId);
	var selectedIndex = gridComp.getSelectionModel().getSelection();
	gridComp.getStore().remove(selectedIndex);
}
// @@20130731 김도원 END

// @@ NICE BMT용 - 사업수행 예산 추가
function addInspectionBudge(gridId){
	alert('ADD'+gridId);
}

//@@ NICE BMT용 - 사업수행 예산 추가
function deleteInspectionBudge(gridId){
	alert('DELETE'+gridId);
}

function deleteSrData(paramMap) {

	nkia.ui.utils.window({
		id: "delReasonPop",
		title: "프로세스 삭제사유",
		params : paramMap,
		type: "processDelReason",
		reference: {},
		callFunc: createProcessWindow
	});
}

function forceQuitSrData(paramMap){
	console.log("강제종료 테스트");
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/forceQuitSrData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
				parent.closeTab();
			} else {
				alert(data.resultMsg);
			}
		}
	});
	parent.closeTab();
}

function updateSrData(paramMap) {
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateSrData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var multiYn = PROCESS_META.MULTI_YN;
					var pType = PROCESS_META.PTYPE;
					if (multiYn == "Y" || pType == "REQUST_REGIST_EDIT" || pType == "CHANGE_PLAN_EDIT" ) {
						self.close();
					} else {
						parent.closeTab();
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}

function updateSrCarryData(paramMap) {	
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateSrCarryData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var multiYn = PROCESS_META.MULTI_YN;
					var pType = PROCESS_META.PTYPE;
					if (multiYn == "Y" || pType == "REQUST_REGIST_EDIT") {
						self.close();
					} else {
						parent.closeTab();
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}

function updateSrMonitorData(paramMap) {	
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateSrMonitorData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var multiYn = PROCESS_META.MULTI_YN;
					var pType = PROCESS_META.PTYPE;
					if (multiYn == "Y" || pType == "REQUST_REGIST_EDIT") {
						self.close();
					} else {
						parent.closeTab();
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}


function updateSrMonitorPocData(paramMap) {	
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateSrMonitorPocData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var multiYn = PROCESS_META.MULTI_YN;
					var pType = PROCESS_META.PTYPE;
					if (multiYn == "Y" || pType == "REQUST_REGIST_EDIT") {
						self.close();
					} else {
						parent.closeTab();
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}

function updateSrDelMonitorData(paramMap) {	
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateSrDelMonitorData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var multiYn = PROCESS_META.MULTI_YN;
					var pType = PROCESS_META.PTYPE;
					if (multiYn == "Y" || pType == "REQUST_REGIST_EDIT") {
						self.close();
					} else {
						parent.closeTab();
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}

function updateCarryOutData(paramMap) {	
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateCarryOutData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var multiYn = PROCESS_META.MULTI_YN;
					var pType = PROCESS_META.PTYPE;
					if (multiYn == "Y" || pType == "REQUST_REGIST_EDIT") {
						self.close();
					} else {
						parent.closeTab();
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}

function updateCenterMoveData(paramMap) {	
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateCenterMoveData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var multiYn = PROCESS_META.MULTI_YN;
					var pType = PROCESS_META.PTYPE;
					if (multiYn == "Y" || pType == "REQUST_REGIST_EDIT") {
						self.close();
					} else {
						parent.closeTab();
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}

/**
 * 
 * @param {}
 *            formId
 * @param {}
 *            xtype
 * @param {}
 *            id
 * @param {}
 *            visible
 */
function procSetVisibleComp(formId, xtype, id, visible) {
	if (xtype == "form" || xtype == "addoperatorgridpanel" || xtype == "processbutton") {
		if($$(id)){
			if (visible) {
				$$(id).show();
			} else {
				$$(id).hide();
			}	
		}
	} else {
		var form = $$(formId);
		var field = (form) ? form._getField(id) : null;
		if(field){
			if (visible) {
				form._showFields([id]);
				
				if(field.config.originValue && field.config.originValue != "") {
					form.setFieldValue(id, field.config.originValue);
				}
			} else {
				form._hideFields([id]);
				if (field != null && field.config.view == "datefield") {
					field.config.originValue = procGetFieldValue(formId, xtype, id);
				}
				//procClearFieldValue(formId, xtype, id);
			}
		}
	}
}

function procCheckVisibleComp(formId, xtype, id, visible) {
	if (xtype == "form" || xtype == "processbutton") {
		return $$(id).isVisible();
	} else {
		return $$(formId)._getField(id).isVisible();
	}
}

function procSetFieldValue(eventField, formId, xtype, fieldName, value) {
	var form = $$(formId);
	if (form && procCheckVisibleComp(formId, xtype, fieldName, true)) {
		if (xtype == "datefield") {
			if(eventField.config.view == "datepicker"){
				var date = new Date(eventField.config.value);
				var hour = "";
				var minute = "";
				
				if (eventField.config.dateType.indexOf('H') != -1) {
					hour = form._getFieldValue(eventField.config.name + '_hour');
				}
		
				if (eventField.config.dateType.indexOf('M') != -1) {
					minute = form._getFieldValue(eventField.config.name + '_min');
				}
				
				// 기본설정(eventField값에 날짜를 더해서 셋팅)
				if(value.standard_field){
					date = new Date(Date.parse(date) + value.day * 1000 * 60 * 60 * 24);
				}else if (value.reference_field) {
					var day = value.reference_value;
					date = new Date(Date.parse(date) + day * 1000 * 60 * 60 * 24);
				}
				
				if (date != null && date != "") {
					var cMonth, cDay, cYear;
					cYear = date.getFullYear();
					if (date.getMonth() + 1 <= 9) {
						cMonth = '0' + (date.getMonth() + 1);
					} else {
						cMonth = date.getMonth() + 1;
					}
	
					if (date.getDate() <= 9) {
						cDay = '0' + date.getDate();
					} else {
						cDay = date.getDate();
					}
					var cFullDate = cYear + "-" + cMonth + "-" + cDay;
					
					form._setFieldValue(fieldName, cFullDate);
					form._setFieldValue(fieldName + '_hour', hour);
					form._setFieldValue(fieldName + '_min', minute);
				}
			}else{
				var lastIndex = eventField.config.name.lastIndexOf('_'); 
				var suffix = eventField.config.name.substring(lastIndex, eventField.config.name.length);
				form._setFieldValue(fieldName + suffix, eventField.config.value);
			}
			
			
		}else if (xtype == "textarea") { 
		
			var editValue = null;
			if (value) {
				editValue = replaceAll(value, '/n', "" + String.fromCharCode(10));
			}
			
			form._setFieldValue(fieldName, editValue);
		
		} else {
			form._setFieldValue(fieldName, value);
		}
	}
}

// 프로세스 상세보기 팝업
function showProcessDetailPop(srId) {
	window.open(getConstValue('CONTEXT') + "/itg/nbpm/provide/goDetailPage.do?sr_id=" + srId,
		"reqDetailPop", "width=1200,height=750,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

function showMultiTaskDetailPop(srId, taskId, eventName) {
	window.open(getConstValue('CONTEXT')+ "/itg/nbpm/provide/goMultiTaskPop.do?sr_id=" + srId + "&multi_task_id=" + taskId + "&type=MULTI_VIEW&event_name=" + eventName,
		"reqDetailMultiPop", "width=1100,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

function createReleaseOperGrid(compProperty){
	
	var that = {};
	for(var prop in compProperty){
		that[prop] = compProperty[prop];
	}
	var t_comp = null;
	
	t_comp = createGridComp({
		id : that['id'],
		keys: [],
		resizeColumn : true,
		pageable : true,
		pageSize : 10,
		checkbox : that['checkbox'],
		height : 150,
		url : that['url'],
		resource : that['resource'],
		params : {},
		header: {
			title: that['label']
		}
	});

	return {rows : t_comp} 
}

// 프로세스 그리드 필드 생성
function createProcessGridFieldComp_bak(compProperty){
	
	var that = {};
	for(var prop in compProperty){
		that[prop] = compProperty[prop];
	}
	var t_comp = null;
	
	var paramMap = {};
	paramMap = that['params'];
	paramMap['sr_id'] = that['sr_id'];
	paramMap['selectQuerykey'] = that['selectQuerykey'];
	paramMap['insertQuerykey'] = that['insertQuerykey'];
	paramMap['deleteQuerykey'] = that['deleteQuerykey'];
	
	var requiredColumns = [];
	if(that['requiredColumns']){
		requiredColumns = that['requiredColumns'].split(',');
	}
	
	var gridProps ={
		id : that['id'],
		keys: [],
		resizeColumn : true,
		pageable : that['pageable'],
		height : 150,
		url : that['url'],
		resource : that['resource'],
		params : paramMap,
		helper: that['helper'],
		helperWidth: that['helperWidth'],
		helperHeight: that['helperHeight'],
		header: {
			title: that['label']
		},
		required: that['required'],
		requiredColumns: requiredColumns
	};
	
	if(that['editorMode'] && (that['popUpType'] == null || that['popUpType'] == '')){
		// 추가/삭제 버튼 추가
		var items = {};
		var rowInsertBtn = createBtnComp({"id": that['id'] + "_rowAddBtn","label":"행추가", type:"form", click: function() { $$(that['id'])._addRow(); } });
		var removeRowBtn = createBtnComp({"id": that['id'] + "_rowRemoveBtn","label":"삭제", click: function() {$$(that['id'])._removeRow(); } });
		
		gridProps.checkbox = true;
		gridProps.header.buttons = {
			items: [	
			    rowInsertBtn, removeRowBtn
			]
		}	
	}else if(that['editorMode'] && (that['popUpType'] != null || that['popUpType'] != '')){
		// 추가/삭제 팝업
		var items = {};
		var insertBtn = createBtnComp({"id": this.id + "_add_btn","label":"추가", type:"form"});
		var deleteBtn = createBtnComp({"id": this.id + "_del_btn","label":"삭제"});
		
		gridProps.header.buttons = {
			items: [	
			    insertBtn, deleteBtn
			]
		}
		
		switch(that['popUpType']){
			case 'multiTask' :
				
				// 버튼 이벤트
				insertBtn.click = function(){
					var grid = $$(that['id']); 
					// 사용자 팝업
					var params = {};
					if (that['params'].roleId != "servicedesk") {
						params["auth_id"] = PROCESS_META.PROCESS_TYPE + "_" + that['params'].roleId;
					} else {
						params["auth_id"] = that['params'].roleId;
					}
					
					params["oper_type"] = that['params'].roleId;
					params["process_type"] = PROCESS_META.REQ_TYPE; 
					

					params["sr_id"] = PROCESS_META.SR_ID; 
					params["event_name"] = grid.config.params.event_name; 
					params["task_name"] = PROCESS_META.TASK_NAME;
					params["processid"] = PROCESS_META.PROCESSID; 
					params["nbpm_table_name"] = grid.config.params.table_name; 
					
					// CSR요청의 [변경계획]인 경우 [작업수행]에서 태워질 서비스 점검자로 저장
					if(PROCESS_META.TASK_NAME == 'REQUST_PLAN'){
						params["task_name"] = 'CHANGE_PROC';
						
					}else{
						params["task_name"] = PROCESS_META.TASK_NAME;

					}
					
					
					nkia.ui.utils.window({
						id: "userWindow",
						title: "담당자 조회",
						width : 1000,
						type: "multiTaskTemp",
						//url: "/itg/system/user/searchUserList.do",
						url: "/itg/nbpm/common/searchProcessOperUserList.do",
						params: params,
						reference: {
							grid: $$(that['id']),
							data: $$(that['id'])._getRows()
						},
						callFunc: createUserWindow
					});
				}
				
				// 버튼 이벤트
				deleteBtn.click = function(){
					var grid = $$(that['id']); 
					var items = grid._getSelectedRows();
					if(items.length > 0){
						if (confirm("선택한 작업자를 삭제합니다.\n계속 하시려면 확인을 눌러주세요")) {
							items.forEach(function(item){
								if(item.USER_ID && item.USER_ID.toString() != ""){
									var params = {
										sr_id: PROCESS_META.SR_ID,
										event_name: grid.config.params.event_name,
										oper_type: that['params'].roleId,
										task_name: PROCESS_META.TASK_NAME, 
										processid: PROCESS_META.PROCESSID, 
										nbpm_table_name: grid.config.params.table_name,
										worker_user_id: item.USER_ID
									};
									
									nkia.ui.utils.ajax({
								        url: "/itg/nbpm/deleteMultiTempInfo.do",
										params: params,
										isMask: false,
										async: false,
								        success: function(response){
								        	grid._removeRow(item.id);
								        }
									});	
								}else{
									grid._removeRow(item.id);
								}
							});
						}
					}else{
						nkia.ui.utils.notification({
			                type: 'error',
			                message: '선택된 정보가 없습니다.'
			            });
					}
					
				}
			break;
		
		}
	}
	
	t_comp = createGridComp(gridProps);
	
	return {rows : t_comp} 
}

// 프로세스 요청참조 Field
function createProcReferenceComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var procReferenceComp = Ext.create('nkia.custom.ProcReferenceField', {
				label : that['label'],
				name : that['name'],
				value : that['value'],
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				fieldWidth : that['fieldWidth'],
				popupId : that['popupId'],
				popupTitle : that['popupTitle'],
				popupWidth : (that['popupWidth']) ? that['popupWidth'] : 650,
				popupHeight : (that['popupHeight']) ? that['popupHeight'] : 370,
				targetFormId : that['targetFormId'],
				selectQuerykey : that['selectQuerykey'],
				url : that['url'],
				resource_prefix : that['resource_prefix'],
				switchCommand : that['switchCommand']
			});

	return procReferenceComp;
}

function isNotNullSubProcessGrid() {
	var panel = Ext.getCmp("requst_regist_grid");
	var gridDataList = panel.getGridData();
	if (gridDataList.length > 0) {
		return true;
	} else {
		return false;
	}
}

function checkMultiTaskGrid(gridId) {
	var isEndTask = true;	// 종료 Task 여부
	var notTicketCount = 0;	// 생성되지 않는 티켓 카운트
	//var panel = $$(gridId);
	//var gridDataList = panel._getRows();
	var gridDataList = $$(gridId)._getRows();
	
	for (var i = 0; gridDataList != null && i < gridDataList.length; i++) {
		if(gridDataList[i].MULTI_TASK_NAME){
			if(gridDataList[i].MULTI_TASK_NAME != "END") {
				isEndTask = false;
			}	
		}else{
			notTicketCount++;
		}
	}
	
	var message = ""; 
	if(notTicketCount > 0){
		message = "티켓 생성이 안된 처리자가 " + notTicketCount + " 명 존재";
		if(!isEndTask){
			message = message + "하며,\n";
		}else{
			message = message + "합니다.";
		}
	}
	if (!isEndTask) {
		message = message + "종료되지 않은 작업이 있습니다.";
	}
	if(message != ""){
		alert(message);
	}
	return isEndTask;
}

/**
 * 프로세스 저장 후에 후처리 핸들러
 * @param {} dataResult
 */
function afterProcessHandler(dataResult) {
	var data = dataResult;
	var isParentClose = false;
	if (data.success) {
		nkia.ui.utils.notification({
			message: data.resultMsg,
			exprie: 2000
		});
	} else {
		nkia.ui.utils.notification({
            message: data.resultMsg
        });
		nkia.ui.utils.hideProgress({id: 'app', isMask: true});
	}
	
	// 메시지 출력 실행.
	if (data.success) {
		setTimeout(
			function(){ 
				if (data.resultMap.selfWork) {
					var taskInfo = {
						'TASK_ID' : data.resultMap.nextTaskId,
						'SR_ID' : data.resultMap.sr_id,
						'TASK_NAME' : data.resultMap.nextNodeName
					};
					if (parent.reWorkTab != null) {
						parent.reWorkTab(taskInfo);
					} else {
						isParentClose = true;
					}
				} else {
					isParentClose = true;
				}
				if (isParentClose) {
					parent.closeTab();
				}
			}
		, 2000);
	}
}

function checkChangeResultType() {
	var isReturn = true;
	if (procGetFieldValue('change_opert_result_regist', 'codecombobox', 'change_result_type') == 'FAILR' 
			|| procGetFieldValue('change_opert_result_regist', 'codecombobox', 'change_result_type') == 'CANCL') {
		if (procGetFieldValue('change_opert_result_regist', 'textarea', 'failr_cancl_resn') == '') {
			alert('[실패 및 취소사유] 을(를) 입력해 주십시요.');
			isReturn = false;
			return isReturn;
		}
	}
	return isReturn;
}

// 변경 유형 체크
function checkChangeType() {
	if (procGetFieldValue('change_type_cl', 'codecombobox', 'change_type') == 'GNRL') {
		// 일반변경인 경우에 변경승인자를 필수로 만들어 준다.
		procSetRequriedField('change_plan_foundng', 'operselect', 'ROLE_OPETR_02');
	}
}

/**
 * 첨부 파일 체크 - 파일이 있으면 파일먼저 등록하고 후처리
 * @param {} fileId
 * @param {} paramMap
 * @param {} callbackSub
 * @return {}
 */
function checkFileSave(fileId, paramMap, callbackSub){
	var isFile = false;
	if($$(fileId)){
		isFile = true;
		$$(fileId)._send(callbackSub, null, paramMap);		
	}
	return isFile;
}

function checkFileSaveChild(fileId, paramMap, callbackSub){
	var isFile = false;
	if($$(fileId)){
		isFile = true;
		$$(fileId)._sendTest(callbackSub, null, paramMap);		
	}
	return isFile;
}

/**
 * 연계요청 유효검증
 * @param {} id
 * @return {Boolean}
 */
function checkLinkReqlValidate(){
	var isValidate = false;
	var gridDataList = $$("requst_regist_grid")._getRows();
	if(gridDataList.length > 0){
		isValidate = true;
	}
	if(!isValidate){
	 	alert("[연계요청] 목록을 확인바랍니다.");
	}
	return isValidate;
}

/**
 * 연계요청 데이터 검증
 * @param {} id
 * @return {}
 */
function checkLinkRelGrid() {
	var result = true;
	var panel = $$("requst_regist_grid");
	var gridDataList = panel._getRows();
	for (var i = 0; gridDataList != null && i < gridDataList.length; i++) {
		if (!(gridDataList[i].WORK_STATE == "WORKCOMP" || gridDataList[i].WORK_STATE == "NOTI" || gridDataList[i].WORK_STATE == "END")) {
			result = false;
			alert("[연계요청] 중 종료되지 않은 요청이 있습니다.");
			break;
		}
	}
	return result;
}

/**
 * 연계요청 요청 유형 데이터 검증
 * @param {} id
 * @return {}
 */
function checkProcessTypeLinkRelGrid(processType) {
	var resultFlag = false;
	var panel = $$("requst_regist_grid");
	var gridDataList = panel._getRows();
	
	if(gridDataList.length > 0){
		var processTypeCnt = 0;
		for (var i = 0; gridDataList != null && i < gridDataList.length; i++) {
			if(gridDataList[i].REQ_TYPE == processType){
				processTypeCnt++;
			}
		}
		if(processTypeCnt > 0){
			resultFlag = true;
		}
	}
	
	if(!resultFlag){
	 	alert("[연계요청] 목록을 확인바랍니다.");
	}
	
	return resultFlag;
}

function afterLinkRelHandler(gridId, data, isReload) {
	switch(gridId) {
		case "receipt_list":
			var param = {};
			param["req_id"] = data["up_sr_id"];
			param["rcept_sttus_cd"] = "CONFM";
			jq.ajax({
					"type" : "POST",
					"url" : "/itg/nbpm/provide/hanwha/updatePreReqStatus.do",
					"contentType" : "application/json",
					"dataType" : "json",
					"async" : false,
					"data" : getArrayToJson(param),
					"success" : function(data) {
					}
			});
			Ext.getCmp("poppreReqDetail").close();
		break;
	}
	if(isReload != undefined && isReload){
		$$(gridId)._reload();
	}
}

function afterLinkRelAddRow(gridId, data) {
	var valArr = [];
	valArr[0] = data;
	var gridFlag = false;
	for(var i = 0 ; i < $$(gridId)._getRows().length ; i++){
		if(data.SR_ID == $$(gridId)._getRows()[i].SR_ID){
			gridFlag = true;
			//updateItem - grid			
		}		
	}
	if(gridFlag){		
		var selRowId = $$(gridId)._getSelectedRows()[0].id;
		$$(gridId).updateItem(selRowId, data);
	}else{
		$$(gridId)._addRow(valArr);
	}
	/*var thisRow = this.getItem(gridId);
	thisRow[editor.column] = "";*/
	/*var selRowId = $$('requst_regist_grid')._getSelectedRows()[0].id;
	$$(gridId).updateItem(selRowId, data);*/
	
	//$$(gridId)._addRow(valArr);	
}

function checkSelfWorker(data) {
	if (data.resultMap.selfWork != null && data.resultMap.selfWork) {
		return true;
	} else {
		return false;
	}
}

function getNextTaskUrl(data) {
	return getConstValue('CONTEXT') + "/itg/nbpm/goTaskPage.do?nbpm_task_id=" + data.resultMap.nextTaskId;
}

function showAssetDetailPop(param){
	//var url = "/itg/itam/opms/opmsDetailInfo.do" + param;
	var url = "/itg/itam/automation/goAssetDetailViewer.do" + param;
	window.open(getConstValue('CONTEXT') +url, "assetPop", "width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

// 요청분류 선택시 담당자 매핑
function setReqTypeCharger(roleId, codeGrpId){
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	
	//roleId가 없는 경우, 기본 세팅(ROLE_OPETR_01)
	if(roleId == undefined || roleId == ''){
		roleId = "ROLE_OPETR_01";
	}
	
	var codeValue = $$(panelId)._getFieldValue((codeGrpId.toLowerCase()));
	var params = {};
	params.req_type = PROCESS_META.PROCESS_TYPE;
	params.process_oper_type = roleId;
	params.grp_code_id = codeGrpId;
	params.code_id = codeValue;
	
	nkia.ui.utils.ajax({
        url: '/itg/workflow/charger/searchReqTypeChargerGrpId.do',
		params: params,
		isMask: false,
        success: function(response){
        	var data = response.resultMap;
        	var grpId = "";
        	if(data){
        		var grpId = data.GRP_ID;
        	}
        	if(grpId != undefined && grpId != ''){
        		var grpNm = data.GRP_NM;
            	var grpSelectYn = data.GRP_SELECT_YN;
            	
        		var values = {};
				values[roleId] = grpNm; //userNm;
				values[roleId+"_user_id"] = grpId; //userId; 
				values[roleId+"_grp_select_yn"] = grpSelectYn;
				$$(panelId)._setValues(values);
				
        	}else{
        		//매핑되어있는 세부유형에 따른 담당자 그룹이 없는 경우
        		var values = {};
				values[roleId] = ""; //userNm;
				values[roleId+"_user_id"] = ""; //userId; 
				values[roleId+"_grp_select_yn"] = "";
				$$(panelId)._setValues(values);
        	}
		}
	});
	
}

//서식파일 다운도르 매핑
function setProcessTemplate(){
	var reqType = PROCESS_META.PROCESS_TYPE;
	//var serviceSubTypeValue = procGetFieldValue('requst_regist', 'codecombobox', 'service_sub_type');
	// 첨부파일 목록 세팅
	var templateDownloader = $$('template_downloader');
	templateDownloader.config.reqType = reqType;
	//templateDownloader.config.temCode = serviceSubTypeValue;
	templateDownloader._getFileList();
}

//전자결재 커스텀
//2018.09.11 molee
function SG_registerTempSrData(param){
	var paramMap = getProcessData(panelList);
	//opener로 등록함수를 호출하지 않는 경우에 한해 활성화
	paramMap["workType"] = param["workType"];
	paramMap["queryKey"] = param["queryKey"];
	
	if (isValidFormList(panelList)) {
		if( !procSubInfoTabValid(panelList, '$!{dataMap.nbpm_node_name}', 'NRMLT_PROC') ){
			return;
		}
		
		var callbackSub = function(atch_file_id) {
			var dataResult;
			if (atch_file_id != '') {
				paramMap['atch_file_id'] = atch_file_id;
			}
			nkia.ui.utils.ajax({
		        url: '/itg/nbpm/registerTemporarily.do',
				params: paramMap,
		        success: function(response){
		        	var data = response;
		        	
		        	//루프문으로 값이 팝업창에서 넘어왔는지 체크
		        	//SG_checkResponse(paramMap.sr_id);
		        	
		        	window.open(
		    			"/itg/nbpm/sample/goSamplePopup.do?sr_id="+paramMap.sr_id,
		    			"sample_pop",
		    			"width=550,height=350,history=no,resizable=yes,status=no,scrollbars=no,member=no"
		    		);
		        	
				}
			});
		};
		var isFile = checkFileSave('atch_file_id',paramMap,callbackSub);
		
	} else {
		nkia.ui.utils.hideProgress({id: 'app', isMask: true});
		return;
	}
}


//전자결재 커스텀
//루프문으로 값이 팝업창에서 넘어왔는지 체크
//2018.09.11 molee
function SG_checkResponse(sr_id){
	var paramMap = {};
	paramMap["sr_id"] = sr_id;
	
	nkia.ui.utils.ajax({
        url: '/itg/nbpm/sample/searchGetSampleSrApprData.do',
		params: paramMap,
        success: function(response){
//        	alert(response.resultString);
        	if (response.resultString == "FAIL") {
        		alert("응답 시간이 경과하였습니다.");
        		nkia.ui.utils.hideProgress({id: 'app', isMask: true});
        		return;
        	}
		}
	});
}


//전자결재 커스텀
//2018.09.11 molee
function SG_registerSrData(param){
	var paramMap = getProcessData(panelList);
	paramMap["sg_appr_no"] = param["sg_appr_no"];
	paramMap["sg_appr_status"] = param["sg_appr_status"];
	paramMap["workType"] = "NRMLT_PROC"; 
	paramMap["loginId"] = "$!{dataMap.nbpm_user_id}"; 
	paramMap["queryKey"] = "updatePROC_SERVICE,insertPROC_SERVICE,countPROC_SERVICE"; 
	var callbackSub = function(atch_file_id){
		var dataResult;
		if (atch_file_id != '') {
			paramMap['atch_file_id'] = atch_file_id;
		}
		
		jq.ajax({
			"type":"POST",
			"url":"/itg/nbpm/processRequest.do",
			"contentType":"application/json",
			"dataType":"json",
				"async":false,
				"data":getArrayToJson(paramMap),
				"success" : function(data){ dataResult = data; }
		});
		afterProcessHandler(dataResult);
	};
	var isFile = checkFileSave('atch_file_id', paramMap, callbackSub);
}


//전자결재 커스텀 - 프로세스 로딩 해제
//2018.09.11 molee
function SG_close(){
	nkia.ui.utils.hideProgress({id: 'app', isMask: true});
	return;
}

//전자결재 커스텀 - 요청절차 처리 화면 탭 닫기
//2018.09.19 molee
function SG_tabClose(){
	parent.closeTab();
}


//대한항공 - 2019-03-21 [작업요청] - [요청등록]
//1.선택된 업무구분에 따라 프로세스 흐름 변수의 값 세팅 (다중선택 - Unix, Windows, Linux, DB, MW, NW, HIST, DML)
//업무구분(JOB_CD) : 01(Unix),02(NT),03(Linux),04(DB),05(MW),06(NW),07(HIST),08(DML)
//	(1) Unix, NT, Linux, DB, MW 선택시		- etc_yn "Y" 
//	(2) NW 선택시								- nw_yn "Y" 
//	(3) HIST 선택시							- hist_yn "Y" 
//
//2.[DCO승인]-업무구분(job_dco_cd), [NOS승인]-업무구분(job_nos_cd) 기본값 세팅
//  (1) Unix, Windows, Linux, DB, MW, DML 선택시	- job_dco_cd에 선택되어진 값 세팅
//	(2) NW, HIST							    - job_nos_cd에 선택되어진 값 세팅
function setCustomCodeValue(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	
	var jobCd = $$(panelId)._getFieldValue('job_cd');
	if(jobCd.indexOf("01") > -1 || jobCd.indexOf("02") > -1 || jobCd.indexOf("03") > -1 || jobCd.indexOf("04") > -1 || jobCd.indexOf("05") > -1 || jobCd.indexOf("08") > -1 || jobCd.indexOf("09") > -1 || jobCd.indexOf("10")> -1 ){
		//01(Unix),02(Windows),03(Linux),04(DB),05(MW) 선택시 
		$$(panelId)._setFieldValue('etc_yn', "Y");
		
	}else{
		$$(panelId)._setFieldValue('etc_yn', "N");
	}
	
	if(jobCd.indexOf("06") > -1){
		//06(NW) 선택시 
		$$(panelId)._setFieldValue('nw_yn', "Y");
	}else{
		$$(panelId)._setFieldValue('nw_yn', "N");
	}
	
	if(jobCd.indexOf("07") > -1){
		//07(HIST) 선택시 
		$$(panelId)._setFieldValue('hist_yn', "Y");
	}else{
		$$(panelId)._setFieldValue('hist_yn', "N");
	}
	
	var jobCdStrArr = jobCd.split(",");
	var jobDcoCdArr = new Array;
	var jobNosCdArr = new Array;
	var jobDcoCd 	= "";
	var jobNosCd 	= "";
	for(var i=0; i<jobCdStrArr.length; i++){
		if(jobCdStrArr[i].indexOf("01") > -1 || jobCdStrArr[i].indexOf("02") > -1 || jobCdStrArr[i].indexOf("03") > -1 || jobCdStrArr[i].indexOf("04") > -1 || jobCdStrArr[i].indexOf("05") > -1 || jobCd.indexOf("08") > -1 || jobCd.indexOf("09") > -1 || jobCd.indexOf("10") > -1){
			jobDcoCdArr.push(jobCdStrArr[i]);
		}
		
		if(jobCdStrArr[i].indexOf("06") > -1 || jobCdStrArr[i].indexOf("07") > -1){
			jobNosCdArr.push(jobCdStrArr[i]);
		}
	}
	
	if(jobDcoCdArr.length > 0){
		jobDcoCd = jobDcoCdArr.join(',');
		$$(panelId)._setFieldValue('job_dco_cd', jobDcoCd);
	}
	
	if(jobNosCdArr.length > 0){
		jobNosCd = jobNosCdArr.join(',');
		$$(panelId)._setFieldValue('job_nos_cd', jobNosCd);
	}
	
}

//대한항공 - 2019-03-27  [작업요청] - [NOS 승인]
//1.선택된 [NOS승인]-업무구분(job_nos_cd)에 따라 프로세스 흐름 변수의 값 세팅 (다중선택 - NW, HIST)
//업무구분(JOB_NOS_CD) : 06(NW),07(HIST)
//	(1) NW 선택시							- nw_yn "Y" 
//	(2) HIST 선택시						- hist_yn "Y" 
//
//2.[NOS승인]-업무구분(job_nos_cd)에 따라 업무구분(job_cd)값 변경
//(1) NW, HIST							- JOB_CD에 값 중 NW, HIST 값 추가 혹은 제거
function changeJobCdValue(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}

	var jobNosCd = $$(panelId)._getFieldValue('job_nos_cd');
	var jobNosCdStrArr = jobNosCd.split(",");

	if(jobNosCd.indexOf("06") > -1){
		//06(NW) 선택시 
		$$(panelId)._setFieldValue('nw_yn', "Y");
	}else{
		$$(panelId)._setFieldValue('nw_yn', "N");
	}
	
	if(jobNosCd.indexOf("07") > -1){
		//07(HIST) 선택시 
		$$(panelId)._setFieldValue('hist_yn', "Y");
	}else{
		$$(panelId)._setFieldValue('hist_yn', "N");
	}
	
	var jobCd = $$(panelId)._getFieldValue('job_cd');
	var jobCdStrArr = jobCd.split(",");
	for(var i=0; i<jobCdStrArr.length; i++){
		if(jobCdStrArr[i].indexOf("06") > -1){
			jobCdStrArr.splice(i, 1);
		}
	}
	for(var i=0; i<jobCdStrArr.length; i++){
		if(jobCdStrArr[i].indexOf("07") > -1){
			jobCdStrArr.splice(i, 1);
		}
	}
	
	for(var i=0; i<jobNosCdStrArr.length; i++){
		jobCdStrArr.push(jobNosCdStrArr[i]);
	}
	
	var newJobcd = "";
	if(jobCdStrArr.length > 0){
		newJobcd = jobCdStrArr.join(',');
		$$(panelId)._setFieldValue('job_cd', newJobcd);
	}
}

//대한항공 - 2019-04-17  [작업요청] - [NOS 승인]
//1.선택된 [NOS승인]-업무구분(network_cd)에 따라 프로세스 흐름 변수의 값 세팅 
//네트워크 요청구분(network_cd) : 01(LG CNS),02(HIST), 03(LG CNS + HIST)
//	(1) LG CNS, LG CNS + HIST 선택시		- nw_yn "Y" 
//	(2) HIST 선택시						- hist_yn "Y" 
function changeHistYnNwYnValue(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}

	var networkCd = $$(panelId)._getFieldValue('network_cd');
	
	if(networkCd != '02'){
		//01(LG CNS), 03(LG CNS + HIST) 선택시 
		$$(panelId)._setFieldValue('nw_yn', "Y");
	}else{
		$$(panelId)._setFieldValue('nw_yn', "N");
	}
	
	if(networkCd != '01'){
		//02(HIST), 03(LG CNS + HIST) 선택시 
		$$(panelId)._setFieldValue('hist_yn', "Y");
	}else{
		$$(panelId)._setFieldValue('hist_yn', "N");
	}
}

//대한항공 - 2019-03-27  [작업요청] - [DCO 승인], [NOS 승인]
//[DCO 승인] - 보안검토(scrty_dco_yn), [NOS 승인] - 보안검토(scrty_nos_yn)
//둘 중의 하나라도(or) 보안검토 'Y'인 경우 -> 보안검토(scrty_yn) = 'Y' 
function setScrtyYnValue(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	var scrtyYn = $$(panelId)._getFieldValue('scrty_yn');
	var scrtyDcoYn = $$(panelId)._getFieldValue('scrty_dco_yn');
	if(scrtyDcoYn){
		if(scrtyDcoYn == 'Y'){
			$$(panelId)._setFieldValue('scrty_yn', 'Y');
		}else{
			if(scrtyYn != 'Y'){
				$$(panelId)._setFieldValue('scrty_yn', 'N');
			}
		}
	}
	
	var scrtyNosYn = $$(panelId)._getFieldValue('scrty_nos_yn');
	if(scrtyNosYn){
		if(scrtyNosYn == 'Y'){
			$$(panelId)._setFieldValue('scrty_yn', 'Y');
		}else{
			if(scrtyYn != 'Y'){
				$$(panelId)._setFieldValue('scrty_yn', 'N');
			}
		}
	}
}

//대한항공 - 2019-03-27  [작업요청] - [작업접수]
//작업유형이 '변경'인 경우,
//업무구분(job_cd)
function checkProcdCdValue(){
	var returnFlag = true;
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	
	//작업유형  : 01(작업), 02(변경)
	var procCd = $$(panelId)._getFieldValue('proc_cd');
	
	//업무구분 : 01(Unix),02(NT),03(Linux),04(DB),05(MW),06(NW)
	var jobCd = $$(panelId)._getFieldValue('job_cd');
	var jobCdStrArr = jobCd.split(",");
	for(var i=0; i<jobCdStrArr.length; i++){
		//07(HIST) 제외
		if(jobCdStrArr[i].indexOf("07") > -1){
			jobCdStrArr.splice(i, 1);
		}
	}
	
	var linkRelLengthFlag  = false;
	var subReqTypeFlag     = false;
	var sameValueFlag      = false;
	if(procCd == '02'){
		//연계요청 필수체크
		var linkRelList = $$("requst_regist_grid")._getRows();
		if(linkRelList.length > 0){
			linkRelLengthFlag = true;
		}
		
		var checkCnt = 0;
		if(linkRelLengthFlag == true){
			var subWorkArr = new Array();
			for(var i=0; i<jobCdStrArr.length; i++){
				for(var j=0; j<linkRelList.length; j++){
					//업무구분별 체크
					if(linkRelList[j].SUB_WORK == jobCdStrArr[i]){
						checkCnt++;
						subWorkArr.push(linkRelList[j].SUB_WORK);
						break;
					}
				}
			}
			
			//동일한 업무구분이 있는지 확인
			for(var i=0; i<subWorkArr.length; i++){
				for(var j=i+1; j<subWorkArr.length; j++){
					if(subWorkArr[i] == subWorkArr[j]){
						sameValueFlag = true;
						break;
					}
				}
			}
			
		}
		if(checkCnt == jobCdStrArr.length){
			subReqTypeFlag = true;
		}else{
			subReqTypeFlag = false;
		}
		
		if(linkRelLengthFlag == true && subReqTypeFlag == true && sameValueFlag == false){
			returnFlag = true;
		}else{
			returnFlag = false;
		}
		
		if(returnFlag == false){
			nkia.ui.utils.notification({
				type: 'error',
				message: '[연계요청] 목록을 확인바랍니다.'
			});
		}
	}
	
	return returnFlag;
}


//대한항공 - 2019-04-02  [서버생성/폐기-요청등록]
//업무구분에 따른 HIST작업여부 값 부여
//HIST 선택시- hist_yn "Y" 
function checkHistYN(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	var jobCd = $$(panelId)._getFieldValue('job_cd');	
	if(jobCd.indexOf("07") > -1){
		//07(HIST) 선택시 
		$$(panelId)._setFieldValue('hist_yn', "Y");
	}else{
		$$(panelId)._setFieldValue('hist_yn', "N");
	}
}
//대한항공 - 2019-04-02  [서버생성/폐기-요청등록]
// HIST선택시 다른 업무구분이 선택됬는지에 따라 true false반환
//HIST + alpha = true 
//HIST만 선택시 false 업무구분을 선택해달라는 메세지 출력
function checkHistAlpha(){
	var returnFlag = true;
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	
	var jobCd = $$(panelId)._getFieldValue('job_cd');
	//HIST가 선택되었을때
	if(jobCd.indexOf("07") > -1){
		//jobCd배열
		var jobCdStrArr = jobCd.split(",");
		//1보다클경우 HIST+alpha가 선택됨 1일경우 HIST만 선택된경우
		if(jobCdStrArr.length>1){
			returnFlag = true;
		}else{
			returnFlag = false;
		}
	}
	if(returnFlag == false){
		nkia.ui.utils.notification({
			type: 'error',
			message: '[업무구분]을 확인바랍니다.'
		});
	}
	return returnFlag;
}
//대한항공 - 2019-04-02  [서버생성/폐기-작업접수]
function compareJobCD(){
   var returnFlag = true;
   
   var panelId = PROCESS_META.TASK_NAME.toLowerCase();
   if(panelId == "start"){
      panelId = "requst_regist";
   }
   var disuseValue = $$(panelId)._getFieldValue('disuse_cd');
   //업무구분 : 01(Unix),02(NT),03(Linux),04(DB),05(MW),06(NW)
   var jobCd = $$(panelId)._getFieldValue('job_cd');
   var jobCdStrArr = jobCd.split(",");
   for(var i=0; i<jobCdStrArr.length; i++){
      //07(HIST) 제외
      if(jobCdStrArr[i].indexOf("07") > -1){
         jobCdStrArr.splice(i, 1);
      }
   }
   
   var linkRelLengthFlag  = false;
   var subReqTypeFlag     = false;
   var sameValueFlag      = false;
   //연계요청 필수체크
   var linkRelList = $$("requst_regist_grid")._getRows();
   if(linkRelList.length > 0){
      linkRelLengthFlag = true;
   }
   //요청유형이 서버 생성일 떄 
   if(disuseValue=="01"){
      var checkCnt = 0;
      if(linkRelLengthFlag == true){
         var subWorkArr = new Array();
         for(var i=0; i<jobCdStrArr.length; i++){
            for(var j=0; j<linkRelList.length; j++){
               //업무구분별 체크
               if(linkRelList[j].SUB_WORK == jobCdStrArr[i]){
                  checkCnt++;
                  subWorkArr.push(linkRelList[j].SUB_WORK);
                  break;
               }
            }
         }
         
         //동일한 업무구분이 있는지 확인
         for(var i=0; i<subWorkArr.length; i++){
            for(var j=i+1; j<subWorkArr.length; j++){
               if(subWorkArr[i] == subWorkArr[j]){
                  sameValueFlag = true;
                  break;
               }
            }
         }
         
      }
      if(checkCnt == jobCdStrArr.length){
         subReqTypeFlag = true;
      }else{
         subReqTypeFlag = false;
      }
      
      if(linkRelLengthFlag == true && subReqTypeFlag == true && sameValueFlag == false){
         returnFlag = true;
      }else{
         returnFlag = false;
      }
      

   }else{
      //요청유형이 서버생성이 아닐 때
      returnFlag = linkRelLengthFlag;
   }
   if(returnFlag == false){
      nkia.ui.utils.notification({
         type: 'error',
         message: '[연계요청] 목록을 확인바랍니다.'
      });
   }
   return returnFlag;
}

//대한항공 - 2019-04-05  [장애요청-관리자승인]
//담당자의 값이 있을 경우 담당자 확인 ,값이 없을 경우 결과확인
//담당자 선택시- charger_yn "Y" 
function checkChargerYN(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	var charger_Value = $$(panelId)._getFieldValue('ROLE_OPETR_03');	
	if(charger_Value.trim()==""||charger_Value==null){
		//07(HIST) 선택시 
		$$(panelId)._setFieldValue('charger_yn', "N");
	}else{
		$$(panelId)._setFieldValue('charger_yn', "Y");
	}
}

//대한항공 - 2019-04-08  [서버생성폐기-요청유형]
//요청자의 값에 따라 요청자의 그룹장 혹은 팀장을 default로 부서승인자에 셋팅해줌
//첫로드시, 요청자 변경시 마다 action
function getDeptConfmer(){
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	//view에서는 요청자에 따른 셋팅이 아닌 저장된 값으로 나와야함
	if(panelId!="requst_regist" && panelId!="sec_requst_regist"){
		return false;
	}
	var req_id = $$(panelId)._getFieldValue("req_user_id");
	//부서승인 view에서 그룹장을 부서승인자로 셋팅안한 경우 break
	//if($$("requst_regist"))

	var param = {};
	param["req_id"] = req_id;

	nkia.ui.utils.ajax({
        url: "/itg/customize/searchDeptConfmer.do",
		params: param,
		async: false,
		isMask: false,
        success: function(response){
			if(response.resultMap!=null){
				var dataMap = {};
				dataMap['ROLE_OPETR_01'] = response.resultMap.USER_NM;
				dataMap['ROLE_OPETR_01_grp_select_yn'] = "N";
				dataMap['ROLE_OPETR_01_user_id'] = response.resultMap.USER_ID;
				
				$$(panelId)._setValues(dataMap);
				}
		}
	});

}

//대한항공 - 2019-04-10 [HIST 작업상태 확인 - 팝업호출]
//[작업요청] - 결과확인
function histCheck(paramMap){
	
	//해당 요청의 SR_ID를 통해서 HIST_ID를 조회
	nkia.ui.utils.ajax({
        url: "/itg/customize/selectHistId.do",
		params: paramMap,
		async: false,
		isMask: false,
        success: function(response){
        	if(response.resultMap!=null && response.resultMap.HIST_URL !=null){
				var histId  = response.resultMap.HIST_ID;
				var histUrl = response.resultMap.HIST_URL;
				nkia.ui.utils.hideProgress({id: 'app', isMask: true});
				//HIST 시스템 화면 팝업
				winobject = window.open(
						histUrl,
						"pop",
						"width=1050,height=700,history=no,resizable=yes,status=no,scrollbars=no,member=no"
				);
				winobject.focus();
			}else{
				nkia.ui.utils.notification({
					type: 'error',
					message: 'HIST작업 상태를 확인할 수 없습니다.'
				});
				nkia.ui.utils.hideProgress({id: 'app', isMask: true});
			}
			
		}
	});
}
/*
//대한항공 - 2019-04-11 
//서버 생성/폐기 요청 
//요청유형에 따라 구성자원 상태 벨리데이션
//서버생성 -> 구성자원 
//서비스 종료 -> 구성자원 상태 "ACTIVE"
//서버폐기 -> 구성자원 상태 "ACTIVE"
function disuseCdForCiValidation(){
	var checkFlag = true;
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	
	var disuseCd = $$(panelId).getFieldValue("disuse_cd");
	if(disuseCd != null){
		var assetState = "";
		
		if(disuseCd == '01'){
			//요청유형이 '서버생성'인 경우
			assetState = "";
			
		}else if(disuseCd == '03'){
			//요청유형이 '서비스 종료'인 경우
			assetState = "ACTIVE";
			
		}else{
			//요청유형이 '서버폐기'인 경우 (02)
			assetState = "ACTIVE";
			
		}
		
		//구성자원조회
		if($$("setSubTabCiAssetGrid")){
			var ciAssetGridList = $$("setSubTabCiAssetGrid")._getRows();
			for(var i=0; i<ciAssetGridList.length;i++){
				if(ciAssetGridList[i].ASSET_STATE != assetState){
					checkFlag = false;
					break;
				}
			}
			
			if(checkFlag == false){
				nkia.ui.utils.notification({
					type: 'error',
					message: '추가할 수 없는 구성자원입니다. 요청유형과 일치하는 구성자원을 선택하세요.'
				});
				return false;
			}
		}
	}
}
*/

//대한항공 - 2019-04-11  [서버생성폐기-요청등록,요청등록(반려시),작업접수 공통 ]
//요청유형(disuse_cd)에 따른 job_cd 필수여부 
//서버생성일 때 job_cd required
//나머지 job_cd required clear 
function changeJobRequire(){
 var panelId = PROCESS_META.TASK_NAME.toLowerCase();
 if(panelId == "start"){
    panelId = "requst_regist";
 }
 var disuseValue = $$(panelId)._getFieldValue('disuse_cd');
 if(disuseValue=='01'){
    procRequiredRegainField(panelId, 'codemultiselect', 'job_cd');
 }else{
    procRequiredClearField(panelId, 'codemultiselect', 'job_cd');
 }
}
//대한항공 - 2019-04-15  [문제요청-요청등록 ]
//장애에서 상위연계요청이 있을 때 장애요청의 장애유형 우선순위 값을 가져옴
function getIncidentInfo(){
	
	var param = {};
	var srId = $$("requst_regist")._getFieldValue("up_sr_id");
	param["sr_id"]=srId;
	param["selectQuerykey"]="selectPROC_INCIDENT";
	nkia.ui.utils.ajax({
        url: "/itg/nbpm/searchProcessSubGrid.do",
		params: param,
		async: false,
		isMask: false,
        success: function(response){
        		var incidentInfo = response.gridVO.rows[0];
        		var dataMap = {};
				dataMap['incdnt_grad_cd'] = incidentInfo.INCDNT_GRAD_CD;
				dataMap['probm_cause_ty_cd'] = incidentInfo.PROBM_CAUSE_TY_CD;
				dataMap['probm_cause_ty_cd_nm'] = incidentInfo.PROBM_CAUSE_TY_CD_NM;
				$$("requst_regist")._setValues(dataMap);
        		
		}
	});
}
//대한항공 - 2019-04-16  [서버생성/폐기-NOS승인]
//업무구분에 따른 HIST작업여부 값 부여
//HIST or LG CNS + HIST 선택시- hist_yn "Y" 
function networkHistYN(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
	var networkCD = $$(panelId)._getFieldValue('network_cd');	
	if(networkCD=='01'){
		//07(HIST) 선택시 
		$$(panelId)._setFieldValue('hist_yn', "N");
	}else{
		$$(panelId)._setFieldValue('hist_yn', "Y");
	}
}


//대한항공 - 2019-04-22 [작업요청-요청등록, 요청등록(반려시)]
//업무구분시 'DML'이면 다른 업무구분 선택 못함 
//그리고 업무 구분을 DML 선택하면 DML 요청 유형 속성이 보임
function checkChangeJobCdForDml(){
	
	var panelId = PROCESS_META.TASK_NAME.toLowerCase();
	if(panelId == "start"){
		panelId = "requst_regist";
	}
		
	var jobCd = $$(panelId)._getFieldValue('job_cd');
	if(jobCd.indexOf("08") > -1){
		//업무구분(job_cd)가 DML(08)인 경우,
		//DML만 선택해야하고, DML요청 유형 속성보임
		$$(panelId)._setFieldValue('job_cd', '08');
		$$(panelId)._showField('dml_cd');
		procSetRequriedField('requst_regist', 'popbuttonfield', 'dml_cd');
	}else{
		$$(panelId)._hideField('dml_cd');
		procRequiredClearField('requst_regist', 'popbuttonfield', 'dml_cd');
	}
}

function requestCheck(){
	var pType = PROCESS_META.PTYPE;
	if(pType == "REQUST_REGIST_EDIT"){
		//var panelId = "requst_regist";
		//$$(panelId)._setControl({editorMode: false, exceptions: ['title','content']});
		//$$("processSubTab").hide();
	}
	
}

//임시조치 처리결과 컬럼사이즈 체크 
function checkMaxSize(){
	var fieldValue = $$('work_proc')._getFieldValue('proc_cn');
	var valueBytes = getByteSize(fieldValue);
	var maxSize = 4000;
	var validate = true;

	if(valueBytes > maxSize) {
		// invalid CSS 추가
		webix.html.addCss($$('work_proc')._getField('proc_cn').getNode(), 'webix_invalid');
		$$('work_proc')._getField('proc_cn').focus();
		nkia.ui.utils.notification({
			type : 'error',
			message : '값의 길이가 너무 깁니다. </br> ' + valueBytes + ' / ' + maxSize + ' bytes'
		});
		validate = false;
	}
	return validate;
}

//서비스요청 기본값 셋팅
function setSubTycdDefault(){
	//내부직원 일경우 요청자 수정 가능, 외부직원 일 경우 요청자 수정X
	//insideYn값은 요청자값 셋팅될때 히든으로 셋팅
	var insideYn = $$('requst_regist')._getFieldValue('inside_yn');	
	
	//요청접수 전 수정 팝업일경우
	if(PROCESS_META.TASK_NAME == "REQUST_RCEPT" || PROCESS_META.TASK_NAME == "TEMP_LINK_SERVICE"){
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm','center_id','service_trget_customer_id_nm','OPER_APPR_01','OPER_APPR_02','requst_type']});
			//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
		}else{
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['center_id','service_trget_customer_id_nm','OPER_APPR_01','OPER_APPR_02','requst_type']});
		}
		procSetVisibleComp('requst_result', 'processbutton', 'NRMLT_PROC_btn', false);
		procSetVisibleComp('requst_result', 'processbutton', 'tempRegister_btn', false);
		procSetVisibleComp('requst_result', 'processbutton', 'RETURN_END_PROC_btn', false);
	}else{
		// 기본정보 디폴트값 세팅
		// 등록자
		var regUserNm = $$('requst_regist')._getFieldValue('reg_user_info');
		if(regUserNm == null || regUserNm == ''){
			var reqUserNm = $$('requst_regist')._getFieldValue('req_user_nm');
			$$('requst_regist')._setFieldValue('reg_user_info', reqUserNm);
		}
		var params = {};
		params['req_doc_id'] = PROCESS_META.SUB_TY_CD; // 요청양식ID
		params['customer_id'] = PROCESS_META.CUSTOMER_ID; // 고객사ID
		
		params['req_user_id'] = $$('requst_regist')._getFieldValue('req_user_id'); // 요청자
		
		// 요청등록 디폴트값 세팅
		var foreignYn = false;
		nkia.ui.utils.ajax({
			url: '/itg/workflow/workReqDoc/selectRequstRegistDefault.do',
			params: params,
			isMask: false,
			async: false,
			success: function(result){
				
				var requstData = result.resultMap;
				if(requstData){
/*					if(requstData.foreign_customer_yn == "Y"){
						foreignYn = true;
						//$$("requst_regist")._setControl({editorMode: true, exceptions: ['service_trget_customer_id_nm']});
					}*/
					//PROCESSINSTANCEID 가 0인경우 (임시저장이나 반려되어 오지 않은 순수히 첫등록화면 일경우)에만 값 셋팅  
					if(PROCESS_META.PROCESSINSTANCEID == "0"){
						requstData["OPER_APPM_01"] = requstData.oper_appm_01;
  						requstData["OPER_APPM_01_grp_select_yn"] = requstData.oper_appm_01_grp_select_yn;
						requstData["OPER_APPM_01_user_id"] = requstData.oper_appm_01_user_id;
						requstData["OPER_APPR_01"] = requstData.oper_appr_01;
						requstData["OPER_APPR_01_grp_select_yn"] = requstData.oper_appr_01_grp_select_yn;
						requstData["OPER_APPR_01_user_id"] = requstData.oper_appr_01_user_id;
						$$("requst_regist")._setValues(requstData);
					}
				}
				
			}
		});
		
		if(foreignYn){
			if(insideYn == 'N'){ //대표고객사가 대외 이면서 외부직원 - 서비스대상고객사, 요청자 필드 readonly
				$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm','service_trget_customer_id_nm']});
			}else if(insideYn == 'Y'){//대표고객사가 대외 이면서 내부직원 - 서비스대상고객사 필드 readonly
				$$("requst_regist")._setControl({editorMode: true, exceptions: ['service_trget_customer_id_nm']});
			}			
		}else{
			if(insideYn == 'N'){ //대표고객사가 대외가 아니고 내부직원 - 서비스대상고객사 필드 readonly
				//$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm']});
			}
		}
		
		//신청유형 초기값 일반
		if(!($$('requst_regist').getValues().requst_type)){
			$$('requst_regist')._setFieldValue('requst_type', '01');
		}
		
		//반려되어 돌아온 요청등록인경우 취소종료 버튼 보여짐
		nkia.ui.utils.ajax({
			url: '/itg/customize/ubit/selectReturnYN.do',
			params: { srId : $$('requst_regist')._getValues().sr_id},
			isMask: false,
			async: false,
			success: function(result){
				var requstData = result.resultMap;
			
				if(requstData.CNT == 0){
					procSetVisibleComp('requst_result', 'processbutton', 'RETURN_END_PROC_btn', false);			
				}else{
					procSetVisibleComp('requst_result', 'processbutton', 'RETURN_END_PROC_btn', true);
				}
				
			}
		});
			
		procSetVisibleComp('requst_result', 'processbutton', 'update_btn', false);
	}
	
	//마감일시 처리 - 처음에 셋팅된 마감일시 이전으로 선택할 경우 선택 X
	//처음에 셋팅되는 마감일시는 gui디자이너에서 만든 select 쿼리에 FC_GET_WORKING_DAY 함수사용하여 양식에 지정된 마감일시에 주말, 공휴일 제외한 값으로 설정
	 var dateValidation= procGetFieldValue('requst_regist', 'datefield', 'end_due_dt');
 	nkia.ui.utils.attach.customEvent('onChange',$$("requst_regist")._getField('end_due_dt'),function(){

	if( procGetFieldValue('requst_regist', 'datefield', 'end_due_dt') < dateValidation){ 
		alert('[마감일] 이전의 날짜는 선택할 수 없습니다.'); 
     	$$("requst_regist")._setFieldValue('end_due_dt', dateValidation);
		isReturn = false; 
		return false; 
	}

});
}

//센터출입요청 초기값 셋팅
function setSubTycdCenterDefault(){
	//내부직원 일경우 요청자 수정 가능, 외부직원 일 경우 요청자 수정X
	var insideYn = $$('requst_regist')._getFieldValue('inside_yn');
	
	//요청접수 전 수정 팝업일경우
	if(PROCESS_META.TASK_NAME == "REQUST_RCEPT" || PROCESS_META.TASK_NAME == "TEMP_LINK_SERVICE"){
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm','center_id','OPER_APPR_01','OPER_APPR_02','requst_type']});
			//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
		}else{
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['center_id','OPER_APPR_01','OPER_APPR_02','requst_type']});
		}
		procSetVisibleComp('requst_result', 'processbutton', 'NRMLT_PROC_btn', false);
		procSetVisibleComp('requst_result', 'processbutton', 'tempRegister_btn', false);
	}else{
		// 기본정보 디폴트값 세팅
		// 등록자
		var regUserNm = $$('requst_regist')._getFieldValue('reg_user_info');
		if(regUserNm == null || regUserNm == ''){
			var reqUserNm = $$('requst_regist')._getFieldValue('req_user_nm');
			$$('requst_regist')._setFieldValue('reg_user_info', reqUserNm);
		}
		
		var params = {};
		params['req_doc_id'] = 'CD0000001643';
	
		params['req_user_id'] = $$('requst_regist')._getFieldValue('req_user_id'); // 요청자
		
		
		// 요청등록 디폴트값 세팅
		
		nkia.ui.utils.ajax({
			url: '/itg/customize/selectRequstRegistDefault.do',
			params: params,
			isMask: false,
			async: false,
			success: function(result){
				
				var requstData = result.resultMap;
				if(requstData){
					
					var insertData = {};
					insertData.entrance_purpose = requstData.content;
					insertData.end_due_cnt = requstData.end_due_cnt;
					insertData.customer_id = requstData.customer_id;
					insertData.customer_nm = requstData.customer_nm;
					insertData.end_due_dt = addDays(requstData.end_due_cnt);
					/*insertData.service_trget_customer_id = requstData.customer_id;
					insertData.service_trget_customer_id_nm = requstData.customer_nm;*/
					
					$$("requst_regist")._setValues(insertData);       
				}
				
			}
		});
		
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm']});
			//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
		}		
		
		procSetVisibleComp('requst_result', 'processbutton', 'update_btn', false);
	}
	
}

//장애요청 초기값 셋팅
function setIncidentDefault(){
	//내부직원 일경우 요청자 수정 가능, 외부직원 일 경우 요청자 수정X
	var insideYn = $$('requst_regist')._getFieldValue('inside_yn');
	
	//요청접수 전 수정 팝업일경우
	if(PROCESS_META.TASK_NAME == "REQUST_RCEPT"){
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm','center_id','customer_id_nm','OPER_ACTY_01']});
			//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
		}else{
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['center_id','customer_id_nm','OPER_ACTY_01']});
		}
		procSetVisibleComp('requst_result', 'processbutton', 'NRMLT_PROC_btn', false);
		procSetVisibleComp('requst_result', 'processbutton', 'tempRegister_btn', false);
	}else{
		// 기본정보 디폴트값 세팅
		// 등록자
		var regUserNm = $$('requst_regist')._getFieldValue('reg_user_info');
		if(regUserNm == null || regUserNm == ''){
			var reqUserNm = $$('requst_regist')._getFieldValue('req_user_nm');
			$$('requst_regist')._setFieldValue('reg_user_info', reqUserNm);
		}
		
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm']});
			//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
		}		
		
		procSetVisibleComp('requst_result', 'processbutton', 'update_btn', false);
	}
	
}

//문제요청 초기값 셋팅
function setProblemDefault(){
	//내부직원 일경우 요청자 수정 가능, 외부직원 일 경우 요청자 수정X
	var insideYn = $$('requst_regist')._getFieldValue('inside_yn');
	
	//요청접수 전 수정 팝업일경우
	if(PROCESS_META.TASK_NAME == "REQUST_RCEPT"){
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm','center_id','customer_id_nm','OPER_ACTY_01']});
			//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
		}else{
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['center_id','customer_id_nm','OPER_ACTY_01']});
		}
		procSetVisibleComp('requst_result', 'processbutton', 'NRMLT_PROC_btn', false);
		procSetVisibleComp('requst_result', 'processbutton', 'tempRegister_btn', false);
	}else{
		// 기본정보 디폴트값 세팅
		// 등록자
		var regUserNm = $$('requst_regist')._getFieldValue('reg_user_info');
		if(regUserNm == null || regUserNm == ''){
			var reqUserNm = $$('requst_regist')._getFieldValue('req_user_nm');
			$$('requst_regist')._setFieldValue('reg_user_info', reqUserNm);
		}
		
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm']});
			//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
		}		
		
		procSetVisibleComp('requst_result', 'processbutton', 'update_btn', false);
	}
	
}

//마감시한변경요청 기본값 셋팅
function setDeadlineDefault(){
		//내부직원 일경우 요청자 수정 가능, 외부직원 일 경우 요청자 수정X
		var insideYn = $$('requst_regist')._getFieldValue('inside_yn');
		
		//요청접수 전 수정 팝업일경우
		if(PROCESS_META.TASK_NAME == "REQUST_RCEPT"){
			if(insideYn == 'N'){
				$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm','center_id','OPER_APPR_01','requst_type']});
				//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
			}else{
				$$("requst_regist")._setControl({editorMode: true, exceptions: ['center_id','OPER_APPR_01','requst_type']});
			}
	//		procSetVisibleComp('requst_result', 'processbutton', 'NRMLT_PROC_btn', false);
	//		procSetVisibleComp('requst_result', 'processbutton', 'tempRegister_btn', false);
		}else{
			// 기본정보 디폴트값 세팅
			// 등록자
			var regUserNm = $$('requst_regist')._getFieldValue('reg_user_info');
			if(regUserNm == null || regUserNm == ''){
				var reqUserNm = $$('requst_regist')._getFieldValue('req_user_nm');
				$$('requst_regist')._setFieldValue('reg_user_info', reqUserNm);
			}
			
			var params = {};
			params['req_doc_id'] = 'CD0000001643';
		
			params['req_user_id'] = $$('requst_regist')._getFieldValue('req_user_id'); // 요청자
			
			
			// 요청등록 디폴트값 세팅
			
			nkia.ui.utils.ajax({
				url: '/itg/customize/selectRequstRegistDefault.do',
				params: params,
				isMask: false,
				async: false,
				success: function(result){
					
					var requstData = result.resultMap;
					if(requstData){
						
						var insertData = {};
						insertData.entrance_purpose = requstData.content;
						insertData.end_due_cnt = requstData.end_due_cnt;
						insertData.customer_id = requstData.customer_id;
						insertData.customer_nm = requstData.customer_nm;						
						
						
						$$("requst_regist")._setValues(insertData);       
					}
					
				}
			});
			
			if(insideYn == 'N'){
				$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm']});
				//procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
			}		
			
			procSetVisibleComp('requst_result', 'processbutton', 'update_btn', false);
			
			nkia.ui.utils.ajax({
				url: '/itg/customize/ubit/selectReturnYN.do',
				params: { srId : $$('requst_regist')._getValues().sr_id},
				isMask: false,
				async: false,
				success: function(result){
					var requstData = result.resultMap;
				
					if(requstData.CNT == 0){
						procSetVisibleComp('requst_result', 'processbutton', 'RETURN_END_PROC_btn', false);			
					}else{
						procSetVisibleComp('requst_result', 'processbutton', 'RETURN_END_PROC_btn', true);
					}
					
				}
			});
			
		}
		
	}


//상위요청이 있거나, 서비스연계여부가 N인경우 연계요청그리드 안보이게 
function hideLinkRelGrid(){
	if($$("requst_regist")._getFieldValue("up_sr_id")){
		$$("requst_regist_grid")._hide();
		$$("requst_regist_grid").config.helper = "DEL";
	}else if($$("requst_regist")._getFieldValue("linkrel_service_yn")=="N"){
		$$("requst_regist_grid")._hide();
		$$("requst_regist_grid").config.helper = "DEL";
	}
}

//변경연계선택 값에 따른 분기 
function hideLinkRelGrid2(){
	if($$("requst_regist")._getFieldValue("linkrel_change_cd")=="03"){
		$$("requst_rcept_grid")._hide();
		$$("requst_rcept_grid").config.helper = "DEL";
	}
}

//변경요청연계선택 값에 따른 분기 설정
function procLinkrelChangeCd(){
	var linkrel_change_cd = $$('requst_regist')._getValues().linkrel_change_cd;
	var checkFlag = false;
	
	if(linkrel_change_cd=="01"){   //변경연계 필수
		$$('requst_rcept')._setFieldValue('linkrel_change_yn', 'Y');		
		
		var param = {};
		param["sr_id"] = PROCESS_META.SR_ID;
		
		nkia.ui.utils.ajax({
			url: "/itg/customize/ubit/countLinkChangeEnd.do",
			params: param,
			async: false,
			isMask: false,
			success: function(result){        		
				var requstData = result.resultMap;
				if(requstData.CNT == 0){
					requstData.linkrel_end_yn = "Y";
				}else{
					requstData.linkrel_end_yn = "N";
				}
				$$("requst_rcept")._setValues(requstData);        				
			}
		});	
	}else if(linkrel_change_cd=="02"){   //변경요청 X - 그리드 hide
		if($$("requst_rcept_grid")._getRows().length == 0 ){
			$$('requst_rcept')._setFieldValue('linkrel_change_yn', 'N');
		}else{
			for(var i = 0; i < $$("requst_rcept_grid")._getRows().length; i++){
				if($$("requst_rcept_grid")._getRows()[i].REQ_TYPE == "CHANGE"){
					checkFlag = true;					
					
					var param = {};					
					param["sr_id"] = PROCESS_META.SR_ID;
					
					nkia.ui.utils.ajax({
						url: "/itg/customize/ubit/countLinkChangeEnd.do",
						params: param,
						async: false,
						isMask: false,
						success: function(result){        		
							var requstData = result.resultMap;
							if(requstData.CNT == 0){
								requstData.linkrel_end_yn = "Y";
							}else{
								requstData.linkrel_end_yn = "N";
							}
							$$("requst_rcept")._setValues(requstData);        				
						}
					});	
				}		
			}
			if(checkFlag){
				$$('requst_rcept')._setFieldValue('linkrel_change_yn', 'Y');
				procRequiredClearField('requst_rcept', 'operselect', 'OPER_ACTY_01');
			}else{
				$$('requst_rcept')._setFieldValue('linkrel_change_yn', 'N');
			}
		}
	}else if(linkrel_change_cd=="03"){   //변경요청 X - 그리드 hide
		$$('requst_rcept')._setFieldValue('linkrel_change_yn', 'N');
	}
		
}

//요청서 템플릿 다운로드 리스트 가져오기
function setTemplateDownloaderList(){
	var temCode = "";
	/*switch(type) {
		case "01" : // 요청서 템플릿 = 01
			temCode = '_01';
		break;
		case "02" : // 작업계획서 템플릿 = 02
			temCode = '_02';
		break;
	}*/

	var subTyCd = PROCESS_META.SUB_TY_CD;
	if(subTyCd){
	 	$$('template_downloader').config.temCode = subTyCd; //PROCESS_META.SUB_TY_CD;
	 	$$('template_downloader')._getFileList('', PROCESS_META.SR_ID);
	}
}

//요청서 템플릿 다운로드 리스트 가져오기
function setTemplateDownloaderList2(sub_ty_cd){
	var temCode = "";

	var subTyCd = sub_ty_cd;
	if(subTyCd){
	 	$$('template_downloader').config.temCode = subTyCd; //PROCESS_META.SUB_TY_CD;
	 	$$('template_downloader')._getFileList('', PROCESS_META.SR_ID);
	}
}

//필드 show, hide
//기존 show, hide 사용시 TextAreaField x2, x3 클릭시 폼의 resize가 안됨
function setFieldVisible(formId, fieldId, visibleFlag, resizeFlag){
	var changeHeight = 0;
	var form = $$(formId);
	var formHeight = form.$height;

	var field = $$(formId)._getField(fieldId);
	if(field){
		var fieldHeight = field.$height;
		
		if(visibleFlag === true){
			//show
			changeHeight = formHeight + fieldHeight;
			field.$view.style.display = 'inline-block';

		}else{
			//hide	
			changeHeight = formHeight - fieldHeight;
			field.$view.style.display = 'none';
		}
		
		if(resizeFlag === true){
			form.define('height', changeHeight);
			form.resize();
			form.refresh();
		}
	}
}

//UBIT 접수그룹 셋팅  - 서비스요청, 센터출입요청
function setRceptGrp(){
	var param = {};	
	
	param["req_id"] = $$('requst_regist')._getFieldValue('req_doc_id');
	param["center_id"] = $$("requst_regist")._getFieldValue("center_id");
	param["customer_id"] = $$("requst_regist")._getFieldValue("service_trget_customer_id");

	//센터출입요청의 경우는 고객사로 셋팅
	if(PROCESS_META.REQ_TYPE == 'CENTER'){
		param['req_id'] = 'CD0000001643';
		param["customer_id"] = $$("requst_regist")._getFieldValue("customer_id");
	}	
	
	//고객사(서비스대상고객사)와 센터값으로 접수그룹 셋팅
	nkia.ui.utils.ajax({
        url: "/itg/customize/setRceptGrp.do",
		params: param,
		async: false,
		isMask: false,
        success: function(result){        		
        		var requstData = result.resultMap;
        		if(requstData != null && requstData != ""){
        			
        			//센터출입요청의 경우 와 서비스요청의 경우 담당자 Role 분기
        			if(PROCESS_META.REQ_TYPE == 'CENTER'){
        				requstData.OPER_ACTY_01_user_id = requstData.OPER_APPM_01_ID;
            			requstData.OPER_ACTY_01_grp_select_yn = requstData.GRP_SELECT_YN;
        			}else{
        				requstData.OPER_APPM_01_user_id = requstData.OPER_APPM_01_ID;
            			requstData.OPER_APPM_01_grp_select_yn = requstData.GRP_SELECT_YN;
        			}
        			$$("requst_regist")._setValues(requstData);        			
        		}else{
        			var initData = {};
        			initData.OPER_APPM_01_user_id = "";
        			initData.OPER_APPM_01_grp_select_yn = "";
        			$$("requst_regist")._setValues(initData);
        		}
        		
		}
	});
}

//UBIT 관리자그룹 셋팅 - 서비스요청
function setMngGrp(){
	var param = {};	
	
	param["req_id"] = $$("requst_regist")._getFieldValue("req_doc_id");
	param["center_id"] = $$("requst_rcept")._getFieldValue("center_id");
	
	nkia.ui.utils.ajax({
        url: "/itg/customize/setMngGrp.do",
		params: param,
		async: false,
		isMask: false,
        success: function(result){        		
        		var requstData = result.resultMap;
        		if(requstData != null && requstData != ""){
        			requstData.OPER_MNGE_01_user_id = requstData.OPER_MNGE_01_ID;
        			requstData.OPER_MNGE_01_grp_select_yn = requstData.GRP_SELECT_YN;
        			$$("requst_rcept")._setValues(requstData);        			
        		}else{
        			var initData = {};
        			initData.OPER_MNGE_01_user_id = "";
        			initData.OPER_MNGE_01_grp_select_yn = "";
        			$$("requst_rcept")._setValues(initData);
        		}
        		
		}
	});
}

function formHideShow(formid){
	if($$(formid).isVisible()) {
		$$(formid).hide();
		$$(formid+"_btn").setValue("열기");
		if($$(formid+"_grid") != undefined){
			if($$(formid+"_grid").config.helper != "DEL"){
				$$(formid+"_grid_header").hide();
				$$(formid+"_grid").hide();
			}
		}
	} else {
		$$(formid).show();
		$$(formid+"_btn").setValue("접기");
		if($$(formid+"_grid") != undefined){
			if($$(formid+"_grid").config.helper != "DEL"){
				$$(formid+"_grid_header").show();
				$$(formid+"_grid").show();
			}
		}
	}
}

function formHide(){
	var i =0;
	var status = false;
	if(PROCESS_META.PTYPE != "VIEW" && PROCESS_META.TASK_NAME != "END_VIEW"){
		for(panelList[i];i < panelList.length;i++){
			if(panelList[i].view == "form"){
				if(!panelList[i].editorMode){
					if(status){
						if($$(panelList[i].id+"_btn") != undefined){
							$$(panelList[i].id).hide();
							$$(panelList[i].id+"_btn").setValue("열기");
							if($$(panelList[i].id+"_grid") != undefined){
								if($$(panelList[i].id+"_grid").config.helper != "DEL"){
									$$(panelList[i].id+"_grid").hide();
									$$(panelList[i].id+"_grid_header").hide();
								}
							}
						}
						/*if($$(panelList[i].id).config.id != "change_plan"){
							$$(panelList[i].id).hide();
							if($$(panelList[i].id+"_grid") != undefined){
								if($$(panelList[i].id+"_grid").config.helper != "DEL"){
									$$(panelList[i].id+"_grid").hide();
									$$(panelList[i].id+"_grid_header").hide();
								}
							}
						}*/
					}else{						
						status = true;
					}	
				}
			}
		}
	}
}

function serviceReqHideField(){
	//요청부서승인여부 값에 따른 요청부서결재자 hide,show
	if($$("requst_regist")._getFieldValue("req_cust_cnfirm_yn") == "N"){
	          procSetVisibleComp('requst_regist', 'textfield', 'OPER_APPR_01', false);
	          procRequiredClearField('requst_regist', 'textfield', 'OPER_APPR_01');
	}else if($$("requst_regist")._getFieldValue("req_cust_cnfirm_yn") == "Y"){
	          procSetVisibleComp('requst_regist', 'textfield', 'OPER_APPR_01', true);
	          procRequiredRegainField('requst_regist', 'textfield', 'OPER_APPR_01');
	}

	//서식파일 첨부유무에따라 서식파일다운로드컴포넌트 show, hide
	if($$("template_downloader_list_header")){
		if(PROCESS_META.PTYPE == "HIST"){
			$$("template_downloader_list_header").hide();
			$$("template_downloader_list").hide();
		}else{
			if($$("requst_regist")._getFieldValue("template_downloader_yn")=="Y"){
				$$("template_downloader_list_header").show();
				$$("template_downloader_list").show();
			}else if($$("requst_regist")._getFieldValue("template_downloader_yn")=="N"){
				$$("template_downloader_list_header").hide();
				$$("template_downloader_list").hide();
			}		
		}
	}

	if($$("requst_regist")._getFieldValue("requst_type_yn")=="Y"){
		procSetVisibleComp('requst_regist', 'codecombobox', 'requst_type', true);
	}else if($$("requst_regist")._getFieldValue("requst_type_yn")=="N"){
		procSetVisibleComp('requst_regist', 'codecombobox', 'requst_type', false);
	}	
	
	//서식파일다운로드 버튼 숨김
	procSetVisibleComp('requst_regist', 'processbutton', 'template_downloader', false);
	
	procSetVisibleComp('requst_regist', 'textfield', 'req_cust_cnfirm_yn', false);
	procSetVisibleComp('requst_regist', 'textfield', 'linkrel_service_yn', false);
	procSetVisibleComp('requst_regist', 'textfield', 'linkrel_change_yn', false);
	//procSetVisibleComp('requst_regist', 'textfield', 'OPER_APPM_01', false);
	procSetVisibleComp('requst_regist', 'textfield', 'req_doc_id', false);
	procSetVisibleComp('requst_regist', 'textfield', 'end_due_cnt', false);
	procSetVisibleComp('requst_regist', 'textfield', 'customer_id', false);
	procSetVisibleComp('requst_regist', 'textfield', 'requst_type_yn', false);
	procSetVisibleComp('requst_regist', 'textfield', 'linkrel_change_cd', false);
	procSetVisibleComp('requst_regist', 'textfield', 'template_downloader_yn', false, true);
}

function changeReqGlobalHideField(){
	procSetVisibleComp('requst_regist', 'textfield', 'OPER_MNGE_01', false);
	procSetVisibleComp('requst_regist', 'textfield', 'req_doc_id', false, true);
	
}

function changeReqLocalHideField(){
	//요청자 내부,외부에 따라 readonly처리
	var insideYn = $$('requst_regist')._getFieldValue('inside_yn');
	
	if(PROCESS_META.TASK_NAME == 'CHANGE_PLAN'){ //접수 전 수정일 경우
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm','center_id','customer_id_nm','OPER_APPM_01','change_category_cd']});
		}
		
		procSetVisibleComp('requst_result', 'processbutton', 'NRMLT_PROC_btn', false);
		procSetVisibleComp('requst_result', 'processbutton', 'tempRegister_btn', false);
	}else{//요청등록일경우
		//등록자 셋팅
		var regUserNm = $$('requst_regist')._getFieldValue('reg_user_info');
		if(regUserNm == null || regUserNm == ''){
			var reqUserNm = $$('requst_regist')._getFieldValue('req_user_nm');
			$$('requst_regist')._setFieldValue('reg_user_info', reqUserNm);
		}
			
		if(insideYn == 'N'){
			$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm']});
		}
		
		//변경관리자 셋팅(상위연계가 서비스일경우 - afterrender 이벤트)
		if($$('requst_regist')._getValues().up_sr_id != "" ){
			//MINOR 셋팅
			var setCategory = ["CD0000001144","CD0000001195","CD0000001193","CD0000001143","CD0000001196","CD0000001192","CD0000001140","CD0000001145","CD0000001155"];
			for(var i = 0 ; i < setCategory.length ; i++){
				if($$('requst_regist')._getValues().req_doc_id == setCategory[i]){
					var setData ={};
					setData.change_category_cd = "02";
					setData.change_ty_cd = "02";
					$$("requst_regist")._setValues(setData);       
				}
			}
			nkia.ui.utils.ajax({
				url: "/itg/customize/ubit/setChangeManager.do",
				params: { upSrId : $$('requst_regist')._getValues().up_sr_id},
				async: false,
				isMask: false,
				success: function(result){        		
					var requstData = result.resultMap;
					if(requstData != null && requstData != ""){
						if(requstData.UP_SR_REQ_TYPE == "SERVICE" && insideYn == 'N'){
							$$("requst_regist")._setControl({editorMode: true, exceptions: ['end_due_dt','req_user_nm']});
						}else if(requstData.UP_SR_REQ_TYPE == "SERVICE" && insideYn == 'Y'){
							$$("requst_regist")._setControl({editorMode: true, exceptions: ['end_due_dt']});
						}
						requstData.OPER_MNGE_01_user_id = requstData.OPER_MNGE_01_ID;
						requstData.OPER_MNGE_01_grp_select_yn = requstData.GRP_SELECT_YN;
						requstData.customer_id = requstData.CUSTOMER_ID;
						requstData.customer_id_nm = requstData.CUSTOMER_NM;
						
						var todayDt = createDateTimeStamp('DHM',{'year':0,'month':0,'day':0,'hour':0,'min':0});
						requstData.req_dt = todayDt.day+todayDt.hour+todayDt.min;
						requstData.req_dt_hour = todayDt.hour;
						requstData.req_dt_min = todayDt.min;
						$$("requst_regist")._setValues(requstData);        			
					}
					
				}
			});
		}		
		//변경관리자셋팅(상위연계가 서비스일경우 - change 이벤트)
		nkia.ui.utils.attach.customEvent('onChange',$$("requst_regist")._getField('up_sr_id'),function(){
			if($$('requst_regist')._getValues().up_sr_id != "" ){
				nkia.ui.utils.ajax({
					url: "/itg/customize/ubit/setChangeManager.do",
					params: { upSrId : $$('requst_regist')._getValues().up_sr_id},
					async: false,
					isMask: false,
					success: function(result){        		
						var requstData = result.resultMap;
						if(requstData != null && requstData != ""){
/*							if(insideYn == 'N'){
								$$("requst_regist")._setControl({editorMode: true, exceptions: ['req_user_nm']});
							}*/
							requstData.OPER_MNGE_01_user_id = requstData.OPER_MNGE_01_ID;
							requstData.OPER_MNGE_01_grp_select_yn = requstData.GRP_SELECT_YN;
							
							var todayDt = createDateTimeStamp('DHM',{'year':0,'month':0,'day':0,'hour':0,'min':0});
							requstData.req_dt = todayDt.day+todayDt.hour+todayDt.min;
							requstData.req_dt_hour = todayDt.hour;
							requstData.req_dt_min = todayDt.min;
							$$("requst_regist")._setValues(requstData);        			
						}
						
					}
				});
			}
		});
		procSetVisibleComp('requst_result', 'processbutton', 'update_btn', false);
	}

}

//센터출입요청
function centerReqGlobalHideField(){
	procSetVisibleComp('requst_regist', 'textfield', 'end_due_cnt', false);
	procSetVisibleComp('requst_regist', 'textfield', 'customer_id', false);
	procSetVisibleComp('requst_regist', 'textfield', 'OPER_ACTY_01', false, true);
	
	//대여장비 노트북일경우 sw설치 표시
	if($$('requst_regist')._getValues().rent_object_cd){
		if(($$('requst_regist')._getValues().rent_object_cd).substring(0,2)=='01'){
			$$('requst_regist')._showField('install_sw_nm');
		}else{
			$$('requst_regist')._hideField('install_sw_nm');
		}
	}
}

/*//UBIT 접수그룹 셋팅
function setCenterProcGrp(){
	var param = {};	
	
	param["req_id"] = PROCESS_META.SUB_TY_CD;
	param["center_id"] = $$("requst_regist")._getFieldValue("center_id");
	param["customer_id"] = $$("requst_regist")._getFieldValue("service_trget_customer_id");
	
	nkia.ui.utils.ajax({
        url: "/itg/customize/setRceptGrp.do",
		params: param,
		async: false,
		isMask: false,
        success: function(result){        		
        		var requstData = result.resultMap;
        		if(requstData != null && requstData != ""){
        			requstData.OPER_APPM_01_user_id = requstData.OPER_APPM_01_ID;
        			requstData.OPER_APPM_01_grp_select_yn = 'Y';
        			$$("requst_regist")._setValues(requstData);        			
        		}
        		
		}
	});
}*/

//존 중복 확인.
function findExistZone(mgmt_place,zone_id_nm){
	var param = {};		
	//var valid_grid =  $$("requst_regist")._getFieldValue("zone_grid")._getRows();
	param["mgmt_place"] = mgmt_place;
	param["zone_id_nm"] = zone_id_nm;
	var reesult = 0;
	
	nkia.ui.utils.ajax({
        url: "/itg/itam/dns/searchExistZoneListCount.do",
		params: param,
		async: false,
		isMask: false,
        success: function(ret){            
        	result = ret.resultInt;                	        
		}

	});
	return result;
}

function emrgncyDtValid(){
	var todayDt = createDateTimeStamp('DHM',{'year':0,'month':0,'day':0,'hour':0,'min':0}).day;
	
	if($$('requst_regist')._getFieldValue('emrgncy_dt')<todayDt){
		nkia.ui.utils.notification({
			type : 'error',
			message : '긴급요청 처리시간이 적합하지 않습니다.'
		});
	}
	if($$('requst_regist')._getFieldValue('requst_type')=='02' && $$('requst_regist')._getFieldValue('emrgncy_dt')<todayDt){
		nkia.ui.utils.ajax({
	        url: "/itg/customize/ubit/emrgncyDtValid.do",
			params: null,
			async: false,
			isMask: false,
	        success: function(result){            
	        	
	        	if(result.resultMap.VALID_DATE >= 14){
	        		nkia.ui.utils.notification({
	        			type : 'error',
	        			message : '긴급요청 처리시간이 적합하지 않습니다.'
	        		});
	        		return false;
	        	}
			}
		});
	}
	
}

//하위요청, 메인요청 동시등록 
function childBatchRegist(paramData){
	var taskName = PROCESS_META.TASK_NAME;
	var url = "/itg/nbpm/processRequest.do";
	
	if(PROCESS_META.SUB_REQ_TYPE == "MONITORING_INSERT" && (PROCESS_META.TASK_NAME == "start" || PROCESS_META.TASK_NAME == "REQUST_REGIST")){ //관제등록
		url = "/itg/nbpm/processMonitoringServiceRequest.do";
	}else if(PROCESS_META.SUB_REQ_TYPE == "AM_CARRY" && (PROCESS_META.TASK_NAME == "start" || PROCESS_META.TASK_NAME == "REQUST_REGIST")){ //반입
		url = "/itg/nbpm/processServiceRequest.do";
	}else if(PROCESS_META.SUB_REQ_TYPE == "AM_CARRYOUT" && (PROCESS_META.TASK_NAME == "start" || PROCESS_META.TASK_NAME == "REQUST_REGIST")){ //반출 접수에서만
		url = "/itg/nbpm/processCarryOutServiceRequest.do";
	}else if(PROCESS_META.SUB_REQ_TYPE == "POC_MONITOR_INST" && (PROCESS_META.TASK_NAME == "start" || PROCESS_META.TASK_NAME == "REQUST_REGIST")){ //POC관제등록
		url = "/itg/nbpm/processMonitoringPocServiceRequest.do";
	}else if(PROCESS_META.SUB_REQ_TYPE == "CLEANDAY" && PROCESS_META.TASK_NAME == "REQUST_RCEPT"){ //클린데이
		url = "/itg/nbpm/processCarryOutServiceRequest.do";
	}else if(PROCESS_META.SUB_REQ_TYPE == "MONITORING_DELETE" && (PROCESS_META.TASK_NAME == "start" || PROCESS_META.TASK_NAME == "REQUST_REGIST")){ //관제등록
		url = "/itg/nbpm/processMonitoringDeleteRequest.do";
	}
	
	taskName = taskName.toLowerCase();
	if(taskName == 'start'){
		taskName = 'requst_regist';
	}	
	if( isProcessValidation(taskName + '_NRMLT_PROC') ){
		if(isValidFormList(panelList)){
			if( !procSubInfoTabValid(panelList, '$!{dataMap.nbpm_node_name}', 'NRMLT_PROC') ){ return; }
			if(!confirm('[등록] 하시겠습니까?')){
				nkia.ui.utils.hideProgress({id: 'app', isMask: true});
				procRequiredRegain('requst_regist_NRMLT_PROC');
				return;
			}
		 	nkia.ui.utils.showProgress({id: 'app', isMask: true});
		 	
		 	/*if($$(panelList[3].id)._getRows().length > 0){
		 		for(var i=0; i < $$(panelList[3].id)._getRows().length; i++){
		 			//childData = $$(panelList[3].id)._getRows()[i].CHILD_DATA;		 			
		 			if($$(panelList[3].id)._getRows()[i].CHILD_DATA !== undefined){
		 				var childData = $$(panelList[3].id)._getRows()[i].CHILD_DATA;
		 				childData = childData.split("\\\\r\\\\n").join("\\n");
		 				jq.ajax({
		 					"type":"POST",
		 					"url":"/itg/nbpm/registerChildProcess.do",
		 					"contentType":"application/json",
		 					"dataType":"json",
		 					"async":false,
		 					"data":childData,
		 					"success" : function(data){
		 						dataResult = data; 
		 					}
		 				});		 				
		 			}
		 		}
		 	}*/
			var paramMap = getProcessData(panelList);
			paramMap["workType"] = "NRMLT_PROC"; 
			paramMap["loginId"] = "$!{dataMap.nbpm_user_id}"; 
			paramMap["queryKey"] = paramData.queryKey;
			var callbackSub = function(atch_file_id){var dataResult; if(atch_file_id != ''){ paramMap['atch_file_id'] = atch_file_id; } jq.ajax({"type":"POST","url":url,"contentType":"application/json","dataType":"json","async":false,"data":getArrayToJson(paramMap),"success":function(data){ dataResult = data; }}); afterProcessHandler(dataResult); } ;
			var isFile = checkFileSave('atch_file_id',paramMap,callbackSub);
		}else{ 
			procRequiredRegain('requst_regist_NRMLT_PROC');
			return;
		}
		}
}

function managerSelectControl(){
	var params = {};	
	params["req_doc_id"] = $$("requst_regist")._getValues().req_doc_id;
	nkia.ui.utils.ajax({
        url: '/itg/workflow/workReqDoc/selectWorkReqDoc.do',
		params: params,
		isMask: false,
		async: false,
        success: function(response){
        	var params = {};
        	if(response.resultMap.MANAGER_CNFIRM_CD==undefined){
        		params["cnfirm_yn"] = "Y";
        	}else{
        		params["cnfirm_yn"] = response.resultMap.MANAGER_CNFIRM_CD;
        	}
        	$$("proc_complete")._setValues(params);
			/*var manageCnfirmCd = response.resultMap.MANAGER_CNFIRM_CD;
			if(manageCnfirmCd == '01'){
				$$("proc_complete")._setControl({editorMode: true, exceptions: ['cnfirm_yn']});
			}else if(manageCnfirmCd == '03'){
				var setData = {};
				setData["cnfirm_yn"] = "N";
				$$("proc_complete")._setValues(setData);
				$$("proc_complete")._setControl({editorMode: true, exceptions: ['cnfirm_yn']});
			}*/
			
        }
	});	
	
}

//회수시 이전태스크 제외한 화면들 editormode false 처리
function editCurTask(){
	var recallTaskId = "";
	var taskList = [];
	var taskCnt = 0;
	for (var i = 0; i < panelList.length; i++) {
		var panel = panelList[i];
		if(panel instanceof nkia.ui.form.Form){
			taskList[taskCnt] = panel.id;
			taskCnt++;
		}
		/*var templateDownloader = $$('template_downloader');
		if(templateDownloader) {
			templateDownloader.disable();
		}*/
	}  
	if(taskList.length > 1){
		for(var i = 0; i<taskList.length-1; i++){
			$$(taskList[i])._setControl({editorMode: false});
		}
	}
}

//RECALL_EDIT.nvf 수정버튼 이벤트
function updateRecallSrData(paramMap) {
	nkia.ui.utils.ajax({
		url: "/itg/nbpm/edit/updateSrData.do",
		params: paramMap,
		isMask: true,
		notification: true,
		success: function(data){
			if (data.success) {
				alert(data.resultMsg);
					var pType = PROCESS_META.PTYPE;
					if (pType == "RECALL_EDIT"  ) {
						var data = opener.$$("searchForm")._getValues();
						// 검색폼의 데이터를 인자값으로 그리드 리로드
						data["list_type"] = 'ING';
						opener.$$("MyList")._reload(data);
						self.close();
					} else {
						nkia.ui.utils.notification({
							type: 'error',
							message: '잘못된 요청입니다.'
						});
						return false;
					}
			} else {
				alert(data.resultMsg);
			}
		}
	});
}


/**
 * 점검결과등록 task> [점검결과등록] 버튼
 **/
function openWorkChkRegist(){
	var req_dt = globalPageMap.REQ_DT;
	
	if(globalPageMap.REQ_DT == null || globalPageMap.REQ_DT == ""){
		var today = new Date();
		var year = String(today.getFullYear());
		var month = String(today.getMonth()+1);
		
		if(month.length < 2) month = "0" + month;
		
		req_dt = year + "-" + month;		
	}
	
	var yyyy = req_dt.substring(0,4);
	var mm = req_dt.substring(5,7);
	
	//자가점검 관리 > 점검결과등록 task> [점검결과등록] 버튼
	if(PROCESS_META.REQ_TYPE == "WORK_CHK"){
		if(globalPageMap.TARGET_CUST_ID != null && globalPageMap.TARGET_CUST_ID != ""){
			window.open(
					"/itg/workflow/workCheckResult/goWorkCheckResult.do?pop_view_type=regist&yyyy=" + yyyy + "&mm=" + mm + "&&target_cust_id=" + globalPageMap.TARGET_CUST_ID,
					"pop",
					"width=1050,height=850,history=no,resizable=yes,status=no,scrollbars=no,member=no"
				);
		}else{
			window.open(
					"/itg/workflow/workCheckResult/goWorkCheckResult.do?pop_view_type=regist&yyyy=" + yyyy + "&mm=" + mm,
					"pop",
					"width=1050,height=850,history=no,resizable=yes,status=no,scrollbars=no,member=no"
				);
		}		
	} 
}

/**
 * 자가점검 관리 > [자가점검 조회] 버튼
 **/
function openWorkChkSearch(){
	var req_dt = globalPageMap.REQ_DT;
	
	var yyyy = req_dt.substring(0,4);
	var mm = req_dt.substring(5,7);
	
	//자가점검 관리 > 점검결과등록 task> [점검결과등록] 버튼
	if(PROCESS_META.REQ_TYPE == "WORK_CHK"){
		if(globalPageMap.TARGET_CUST_ID != null && globalPageMap.TARGET_CUST_ID != ""){
			window.open(
					"/itg/workflow/workCheckResult/goWorkCheckResult.do?pop_view_type=search&yyyy=" + yyyy + "&mm=" + mm + "&&target_cust_id=" + globalPageMap.TARGET_CUST_ID,
					"pop",
					"width=1050,height=850,history=no,resizable=yes,status=no,scrollbars=no,member=no"
				);
		}else{
			window.open(
					"/itg/workflow/workCheckResult/goWorkCheckResult.do?pop_view_type=search&yyyy=" + yyyy + "&mm=" + mm,
					"pop",
					"width=1050,height=850,history=no,resizable=yes,status=no,scrollbars=no,member=no"
				);
		}
	}
}