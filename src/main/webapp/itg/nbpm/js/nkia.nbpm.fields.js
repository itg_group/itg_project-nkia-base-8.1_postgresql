/********************************************
 * ReqZoneCombo
 * Date: 2016-11-17
 * Version:
 *	1.0 - Text Field 지원
 ********************************************/
nkia.nbpm.fields.ReqZoneCombo = function() {
	// Custom Properties
	this.rows = [];
	this.url = "/itg/base/searchReqZoneDataList.do";
	this.displayField = "CUST_NM";
	this.valueField = "CUST_ID";
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.reqzone(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		this.rows.push(createCodeComboBoxComp(this));
	};
};

//프라퍼티 검증함수
nkia.nbpm.validator.reqzone = function(props) {
	if(props.name === undefined || props.name.length === 0){
		webix.message({
			type:"error",
			text: "[" + props.label + "] text 필드에 name property가 없습니다.",
			expire:-1
		});
		return;
	}
};


/**
 * @method {} createReqZoneComp
 * 요청지역 필드
 * @property {string} id			
 */
function createReqZoneComp(reqZoneProps) {
	var props = reqZoneProps||{};
	var reqZone = {};
	try {
		switch(props.selectType){
			case "combo":
				reqZone = new nkia.nbpm.fields.ReqZoneCombo();
			break;
			case "matrix":
				comp = createReqZoneMatrixComp(compProperty);
			break;
			case "popup":
				comp = createReqZonePopupComp(compProperty);
			break;
		}
		reqZone.setProps(props);
		reqZone.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return reqZone;
}

/**
 * @method {} createSrSelectComp
 * @property {string} id			
 */
/*
function createSrSelectComp(srSelectProps) {
	var props = srSelectProps||{};
	var srSelect = {};
	try {
		srSelect = new nkia.nbpm.fields.SrSelect();
		srSelect.setProps(props);
		srSelect.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return srSelect;
}
*/
/********************************************
 * User: 요청자 필드
 * Date: 2016-12-21
 ********************************************/
nkia.nbpm.fields.User = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.lastSelectedUser = "";
	this.isPopSelect = false;
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.user(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		
		var _this = this;
		
		// 사용자 명 일치 여부 체크 함수
		var userInfoCheckFunction = function(params) {
			// 마지막 요청자 입력 값 = 현재 요청자 필드의 값
			_this.lastSelectedUser = params["search_user_nm"];
			
			// 입력한 요청자명과 동일한 사용자가 있는지 체크
			nkia.ui.utils.ajax({
				url: "/itg/nbpm/common/searchProcessOperUserList.do",
				params: params,
				async: false,
				success: function(response){
					
					var result = response.gridVO.rows;
					var matchedCnt = result.length;
					
					if(matchedCnt == 1) {
						// 입력한 요청자명과 동일한 사용자명이 단일 값으로 나왔을 때 요청자 필드에 자동 세팅
						var userInfo = result[0];
						$$(_this.formId)._setValues({
							req_user_id: userInfo["USER_ID"],
							req_user_nm: userInfo["USER_NM"],
							req_user_detail: ( userInfo["CUST_NM"] + "(tel:" + userInfo["TEL_NO"] + ")" ),
						});
						
						// 마지막 요청자 입력 값 세팅
						_this.lastSelectedUser = userInfo["USER_NM"];
						
					} else if(matchedCnt > 1) {
						
						if(!_this.isPopSelect) {
							// 요청자 선택 팝업 호출
							if(params["search_user_nm"] == "") {
								alert("요청자를 선택해주세요.");
							} else {
								alert("동일한 이름의 요청자가 있습니다.\n한 명의 요청자를 선택해주세요.");
							}
							
							nkia.ui.utils.window({
								id: "userWindowForEnter",
								title: "사용자 조회",
								width : 1000,
								height : 800,
								url: "/itg/nbpm/common/searchProcessOperUserList.do",
								defaultSearchUser: params["search_user_nm"],
								reference: {
									form: $$(_this.formId),
									obj: _this,
									fields: {
										id: "req_user_id",
										name: "req_user_nm",
										user_detail: "req_user_detail"
									}
								},
								callFunc: createUserWindow
							});
							
							// 팝업 오픈 후 최초 포커스 세팅
							_this.initPopFocus("userWindowForEnter");
							
							return false;
						}						
					} else {
						
						// 입력한 요청자명과 동일한 사용자명이 없으면 선택 팝업 호출
						alert("요청자를 선택해주세요.");
						
						nkia.ui.utils.window({
							id: "userWindowForEnter",
							title: "사용자 조회",
							width : 1000,
							height : 800,
							url: "/itg/nbpm/common/searchProcessOperUserList.do",
							reference: {
								form: $$(_this.formId),
								obj: _this,
								fields: {
									id: "req_user_id",
									name: "req_user_nm",
									user_detail: "req_user_detail"
								}
							},
							callFunc: createUserWindow
						});
						
						// 팝업 오픈 후 최초 포커스 세팅
						_this.initPopFocus("userWindowForEnter");
						
						return false;
					}
				}
			});
		};
		
		// Webix Global Event 선언
		// 포커스 이동 시, 요청자 입력값 체크
		webix.attachEvent("onFocusChange", function(toObj, fromObj){
			
			if(toObj != null) {
				var topParent = toObj.getTopParentView();
				
				if(fromObj != null && fromObj.config.name === "req_user_nm" && toObj.config.view !== "window" && topParent.config.view !== "window") {
					// 다른 윈도우 팝업 호출 시에도 팝업에 포커스가 가게 되면 alert이 무한 호출 되므로 예외 처리
					var innerParams = {};
					var paramUserNm = $$(_this.formId)._getValues()[fromObj.config.name];
					innerParams["search_user_nm"] = paramUserNm;
					innerParams["page"] = 1;
					innerParams["start"] = 0;
					innerParams["limit"] = 10;
					
					userInfoCheckFunction(innerParams);
				}
			}
		});
		
		var render = createUnionFieldComp({
			// 1. 팝업 필드 형식
//			items: [
//				createSearchFieldComp({
//					label: props.label,
//					name: "req_user_nm",
//					value: props.userNm,
//					required: props.required,
//					helper: props.helper,
//					helperWidth: props.helperWidth,
//					helperHeight: props.helperHeight,
//					click: function(){
//						nkia.ui.utils.window({
//							id: "userWindow",
//							title: "사용자 조회",
//							width : 1000,
//							//height : 800,
//							url: "/itg/nbpm/common/searchProcessOperUserList.do",
//							reference: {
//								form: $$(_this.formId),
//								obj: _this,
//								fields: {
//									id: "req_user_id",
//									name: "req_user_nm",
//									user_detail: "req_user_detail"
//								}
//							},
//							callFunc: createUserWindow
//						});
//						// 팝업 오픈 후 최초 포커스 세팅
//						_this.initPopFocus("userWindow");
//					}
//				}),
//				createTextFieldComp({
//					label: "　",
//					name: "req_user_detail",
//					readonly: false,
//					isProcAfterHide: props.isProcAfterHide
//				}),
//				createBtnComp({ 
//					label:getConstText({isArgs : true, m_key : 'btn.system.00003'}),
//					isProcAfterHide: props.isProcAfterHide,
//					click: function(){
//						$$(_this.formId)._setValues({
//							req_user_id: '',
//							req_user_nm: '',
//							req_user_detail: ''
//						});
//					}
//				})
//			]
			// 2. 텍스트 필드 형식
			items: [
				createTextFieldComp({
					label: props.label,
					name: "req_user_nm",
					value: props.userNm,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					on: {
						onKeyPress: function(keyCode) {
							// Press 한 Key 가 입력 Key 라면 팝업 선택여부 값 false로. (값이 변경되었다고 판단)
							// 영어 || 숫자 || Delete || Space || Back-Space
							if(keyCode){
								if((keyCode>=65 && keyCode<=90) || (keyCode>=48 && keyCode<=57)
										|| keyCode==46 || keyCode==32 || keyCode==8) {
									_this.isPopSelect = false;
								} else if(keyCode == "13"){ //enter key
									var innerParams = {};
									var paramUserNm = $$(_this.formId)._getValues()[this.config.name];
									innerParams["search_user_nm"] = paramUserNm;
									innerParams["page"] = 1;
									innerParams["start"] = 0;
									innerParams["limit"] = 10;
									userInfoCheckFunction(innerParams);
								}	
							}
						}
					}
				}),
				createTextFieldComp({
					label: "　",
					name: "req_user_detail",
					readonly: true,
					isProcAfterHide: props.isProcAfterHide
				}),
				createBtnComp({ 
					label:"검 색",
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id: "userWindow",
							title: "사용자 조회",
							width : 1000,
							height : 800,
							url: "/itg/nbpm/common/searchProcessOperUserList.do",
							reference: {
								form: $$(_this.formId),
								obj: _this,
								fields: {
									id: "req_user_id",
									name: "req_user_nm",
									user_detail: "req_user_detail"
								}
							},
							callFunc: createUserWindow
						});
						
						// 팝업 오픈 후 최초 포커스 세팅
						_this.initPopFocus("userWindow");
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						$$(_this.formId)._setValues({
							req_user_id: '',
							req_user_nm: '',
							req_user_detail: ''
						});
					}
				})
			]
		});
		
		this.hiddens = {
			req_user_id: props.userId,
			inside_yn: '' //UBIT 내부직원, 외부직원 구분값 추가 191113 ysju
		};
		this.rows = render.rows
		
	};
	
	// 팝업창 최초 포커스
	this.initPopFocus = function(windowId){
		var searchForm = $$(windowId + '_searchForm');
		var focusField = searchForm._getField("search_value");
		if(focusField!=null) {
			focusField.focus();
		}
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.user = function(props) {
	
};


/**
 * @method {} createUserFieldComp
 * @property {string} id			
 */
function createUserFieldComp(userProps) {
	var props = userProps||{};
	var user = {};
	try {
		user = new nkia.nbpm.fields.User();
		user.setProps(props);
		user.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return user;
}

/********************************************
 * OperSelect: 담당자 필드
 * Date: 2016-12-21
 ********************************************/
nkia.nbpm.fields.OperSelect = function() {
	// Custom UI Properties
	this.rows = [];
	this.hiddens = {};
	
	// Custom Value Properties
	this.reassign = false;
	this.processType = "";
	this.operListData = [];
	this.operUserId = "";
	this.operUserName = "";
	this.isProcAfterHide = false;
	this.value = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.operselect(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
		
		if (this.operListData != null && this.operListData.length > 0) {
			this.operUserId = this.operListData[0].OPER_USER_ID;
			this.operUserName = this.operListData[0].OPER_USER_NM;
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		if(props.readonly == true){
			var render = createUnionFieldComp({
				items: [
				        createSearchFieldComp({
				        	label: props.label,
				        	name: props.name,
				        	value: this.operUserName,
				        	required: props.required,
				        	helper: props.helper,
				        	helperWidth: props.helperWidth,
				        	helperHeight: props.helperHeight,
				        	isProcAfterHide: props.isProcAfterHide,			
				        	readonly:props.readonly
				        })
				        ]
			});			
		}else{
			var render = createUnionFieldComp({
				items: [
				        createSearchFieldComp({
				        	label: props.label,
				        	name: props.name,
				        	value: this.operUserName,
				        	required: props.required,
				        	helper: props.helper,
				        	helperWidth: props.helperWidth,
				        	helperHeight: props.helperHeight,
				        	isProcAfterHide: props.isProcAfterHide,
				        	click: function(){
				        		var params = {};
				        		params.oper_type = _this.name;
				        		if (_this.name != "CNSLR") {
				        			params.process_type = _this.processType;
				        		}
				        		if (_this.value != ""){
				        			params.display_type = _this.value;
				        		}  
				        		nkia.ui.utils.window({
				        			id: _this.name + "_window",
				        			title: "담당자 조회",
				        			width : 1000,
				        			//height : 800,
				        			url: "/itg/nbpm/common/searchProcessOperUserList.do",
				        			params : params,
				        			type: "process",
				        			reference: {
				        				form: $$(_this.formId),
				        				fields: {
				        					id: _this.name + '_user_id',
				        					name: _this.name,
				        					grp_select_yn: _this.name + '_grp_select_yn'
				        				}
				        			},
				        			callFunc: createUserWindow
				        		});
				        	}
				        }),
				        createBtnComp({ 
				        	label:"본인지정", 
				        	isProcAfterHide: props.isProcAfterHide,
				        	click: function(){
				        		var params = {};
				        		params.oper_type = _this.name;
				        		if (_this.name != "CNSLR") {
				        			params.process_type = _this.processType;
				        		}
				        		
				        		nkia.ui.utils.ajax({
				        			url: '/itg/nbpm/process/selectSelfProcessAuth.do',
				        			params: params,
				        			isMask: false,
				        			success: function(response){
				        				var data = response.resultMap;
				        				if(data){
				        					var values = {};
				        					values[_this.name] = data.userNm;
				        					values[_this.name + '_user_id'] = data.userId;
				        					values[_this.name + '_grp_select_yn'] = "";
				        					$$(_this.formId)._setValues(values);
				        				}else{
				        					nkia.ui.utils.notification({
				        						message: '권한이 없습니다.'
				        					});
				        				}
				        			}
				        		});
				        	}
				        }),
				        createBtnComp({ 
				        	label:"초기화", 
				        	isProcAfterHide: props.isProcAfterHide,
				        	click: function(){
				        		var values = {};
				        		values[_this.name] = "";
				        		values[_this.name + '_user_id'] = "";
				        		values[_this.name + '_grp_select_yn'] = "";
				        		$$(_this.formId)._setValues(values);
				        	}
				        })
				        ]
			});			
		}
		
		this.hiddens[this.name + '_user_id'] = '';
		this.hiddens[this.name + '_grp_select_yn'] = '';
		
		this.rows = render.rows
	};
	
	this._getValue = function() {
		return {
			select_type: "SINGLE",  
			oper_type: (this.reassign) ? "re_" + this.name : this.name,
			oper_user_id : $$(this.formId)._getFieldValue(this.name + '_user_id'),
			oper_user_nm : $$(this.formId)._getFieldValue(this.name),
			grp_slct_yn  : $$(this.formId)._getFieldValue(this.name + '_grp_select_yn')
		};
	}
	
	this._setValue = function(values) {
		$$(this.formId)._setFieldValue(this.name + '_user_id', values.oper_user_id);
		$$(this.formId)._setFieldValue(this.name, values.oper_user_nm);
		$$(this.formId)._setFieldValue(this.name + '_grp_select_yn', values.oper_grp_select_yn);
	}
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.operselect = function(props) {
	
};


/**
 * 담당자 지정 필드
 * @method {} createOperSelectComp
 * @property {string} id			
 */
function createOperSelectComp(operSelectProps) {
	var props = operSelectProps||{};
	var user = {};
	try {
		user = new nkia.nbpm.fields.OperSelect();
		user.setProps(props);
		user.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return user;
}

/**
 * 담당자 재지정 필드
 * @method {} createReOperSelectComp
 * @property {string} id			
 */
function createReOperSelectComp(operSelectProps) {
	operSelectProps.reassign = true;
	var t_comp = createOperSelectComp(operSelectProps)
	return t_comp;
}

/********************************************
 * SrSelect: 요청ID
 * Date: 2016-12-21
 ********************************************/
nkia.nbpm.fields.SrSelect = function() {
	// Custom UI Properties
	this.rows = [];
	this.hiddens = {};
	
	// Custom Value Properties
	this.name = "";
	this.label = "";
	this.required = false;
	this.srId = "";
	this.processTypeFilter = "";
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.srselect(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: _this.label,
					name: _this.name,
					required: _this.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var params = {};
						params.cur_sr_id = _this.srId;
						
						if (_this.processTypeFilter != null) {
							params.req_type = _this.processTypeFilter;
						}
						
						nkia.ui.utils.window({
							id: _this.name + "_window",
							title: getConstText({isArgs : true, m_key : 'res.label.nbpm.00044'}),
							width : 1000,
							height : 600,
							url: "/itg/nbpm/common/searchProcessList.do",
							params : params,
							type: PROCESS_META.REQ_TYPE,
							reference: {
								form: $$(_this.formId),
								fields: {
									name: _this.name
								}
							},
							callFunc: createProcessWindow
						});
					}
				}),
				createBtnComp({ 
					label:getConstText({isArgs : true, m_key : 'btn.system.00003'}), 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var values = {};
						values[_this.name] = "";
						$$(_this.formId)._setValues(values);
					}
				})
			]
		});
		
		this.rows = render.rows
	};
};

//프라퍼티 검증함수
nkia.nbpm.validator.srselect = function(props) {
	
};

function createSrSelectComp(srSelectProps) {
	var props = srSelectProps||{};
	var srSelect = {};
	try {
		srSelect = new nkia.nbpm.fields.SrSelect();
		srSelect.setProps(props);
		srSelect.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return srSelect;
}

/**
 * @method {} createMultiCodeSelectPopComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.multiCode= function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.multiCode(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.targetName;
		var targetHiddenName = props.targetHiddenName;
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.targetLabel,
					name: tagetName,
					required: props.required,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id : tagetName,
							title: props.popUpTitle+" 조회",
							width : 1000,
							height : nkia.ui.utils.getWidePopupHeight(),
							params : {
								grpCodeId : props.grpCodeId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									id: props.targetHiddenName,
									name: tagetName
								}
							},
							callFunc: createMultiCodeWindow
						});
					}
				}),
				createBtnComp({ 
					label:getConstText({isArgs : true, m_key : 'btn.system.00003'}), 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var values = {};
						values[targetHiddenName] = "";
						values[tagetName] = "";
						$$(_this.formId)._setValues(values);
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.multiCode = function(props) {
	
};


/**
 * @method {} createMultiCodeSelectPopComp
 * @property {string} id			
 */
function createMultiCodeSelectPopComp(popupProps) {
	var props = popupProps||{};
	var popup = {};
	try {
		popup = new nkia.nbpm.fields.multiCode();
		popup.setProps(props);
		popup.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return popup;
}

/**
 * @method {} UBIT 고객사(customer) 선택 
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.customerPopup = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	this.targetHiddenName = "";
	this.panelId = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.customerPopup(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var panelIdVal =  PROCESS_META.TASK_NAME.toLowerCase();
		if(panelIdVal == "start"){
			panelIdVal = "requst_regist";
		}
		_this.panelId = panelIdVal;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var grpCodeId = props.name.toUpperCase();
		if(props.readonly == true){
			var render = createUnionFieldComp({
				items: [
					createSearchFieldComp({
						label: props.label,
						name: tagetName,
						required: props.required,
						helper: props.helper,
						helperWidth: props.helperWidth,
						helperHeight: props.helperHeight,
						isProcAfterHide: props.isProcAfterHide,
						readonly: props.readonly
					})
				]
			});
		}else{
			var render = createUnionFieldComp({
				items: [
					createSearchFieldComp({
						label: props.label,
						name: tagetName,
						required: props.required,
						helper: props.helper,
						helperWidth: props.helperWidth,
						helperHeight: props.helperHeight,
						isProcAfterHide: props.isProcAfterHide,
						click: function(){
							nkia.ui.utils.window({
								id : tagetName,
								title: props.label,
								width : nkia.ui.utils.getWidePopupWidth(),
								height : nkia.ui.utils.getWidePopupHeight(),
								popUpType : props.popUpType,
								params : {
									srId : props.srId,
								    grpCodeId : grpCodeId
								},
								reference: {
									form: $$(_this.formId),
									fields: {
										id: targetHiddenName,
										name: tagetName
									}
								},
								callFunc: createPopupFieldWindow
							});
						}
					}),
					createBtnComp({ 
						label:"내 고객사", 
						isProcAfterHide: props.isProcAfterHide,
						id: _this.panelId+'DefaultBtn',
						click: function(){
							nkia.ui.utils.ajax({
    							url: "/itg/customize/ubit/selectCustomerInfo.do", 
    							params:{req_user_id:$$('requst_regist')._getValues().req_user_id},
    							isMask:true,
    							success:function(data){
    								if(data.success){
    									var values = {};
    									values[targetHiddenName] = data.resultMap.customer_id;
    									values[tagetName] = data.resultMap.customer_nm;
    									$$(_this.panelId)._setValues(values);	    									
    								}else{
    									nkia.ui.utils.notification({type:'error', message:data.resultMsg});
    								}
    							}
    						});
						}
					}),
					createBtnComp({ 
						label:getConstText({isArgs : true, m_key : 'btn.system.00003'}), 
						isProcAfterHide: props.isProcAfterHide,
						id: _this.panelId+'resetBtn',
						click: function(){
							var values = {};
							values[targetHiddenName] = "";
							values[tagetName] = "";
							$$(_this.panelId)._setValues(values);
						}
					})
				]
			});
			/*if(PROCESS_META.REQ_TYPE == "SERVICE"){
				var render = createUnionFieldComp({
					items: [
						createSearchFieldComp({
							label: props.label,
							name: tagetName,
							required: props.required,
							helper: props.helper,
							helperWidth: props.helperWidth,
							helperHeight: props.helperHeight,
							isProcAfterHide: props.isProcAfterHide,
							click: function(){
								nkia.ui.utils.window({
									id : tagetName,
									title: props.label,
									width : nkia.ui.utils.getWidePopupWidth(),
									height : nkia.ui.utils.getWidePopupHeight(),
									popUpType : props.popUpType,
									params : {
										srId : props.srId,
									    grpCodeId : grpCodeId
									},
									reference: {
										form: $$(_this.formId),
										fields: {
											id: targetHiddenName,
											name: tagetName
										}
									},
									callFunc: createPopupFieldWindow
								});
							}
						}),
						createBtnComp({ 
							label:"내 고객사", 
							isProcAfterHide: props.isProcAfterHide,
							id: _this.panelId+'DefaultBtn',
							click: function(){
								var values = {};
								values[targetHiddenName] = $$('requst_regist')._getValues().customer_id;
								values[tagetName] = $$('requst_regist')._getValues().customer_nm;
								$$(_this.panelId)._setValues(values);
							}
						}),
						createBtnComp({ 
							label:getConstText({isArgs : true, m_key : 'btn.system.00003'}), 
							isProcAfterHide: props.isProcAfterHide,
							id: _this.panelId+'resetBtn',
							click: function(){
								var values = {};
								values[targetHiddenName] = "";
								values[tagetName] = "";
								$$(_this.panelId)._setValues(values);
							}
						})
					]
				});
			}else{
				var render = createUnionFieldComp({
					items: [
						createSearchFieldComp({
							label: props.label,
							name: tagetName,
							required: props.required,
							helper: props.helper,
							helperWidth: props.helperWidth,
							helperHeight: props.helperHeight,
							isProcAfterHide: props.isProcAfterHide,
							click: function(){
								nkia.ui.utils.window({
									id : tagetName,
									title: props.label,
									width : nkia.ui.utils.getWidePopupWidth(),
									height : nkia.ui.utils.getWidePopupHeight(),
									popUpType : props.popUpType,
									params : {
										srId : props.srId,
									    grpCodeId : grpCodeId
									},
									reference: {
										form: $$(_this.formId),
										fields: {
											id: targetHiddenName,
											name: tagetName
										}
									},
									callFunc: createPopupFieldWindow
								});
							}
						}),
						createBtnComp({ 
							label:"내 고객사", 
							isProcAfterHide: props.isProcAfterHide,
							id: _this.panelId+'DefaultBtn',
							click: function(){
								nkia.ui.utils.ajax({
	    							url: "/itg/customize/ubit/selectCustomerInfo.do", 
	    							params:{req_user_id:$$('requst_regist')._getValues().req_user_id},
	    							isMask:true,
	    							success:function(data){
	    								if(data.success){
	    									var values = {};
	    									values[targetHiddenName] = data.resultMap.customer_id;
	    									values[tagetName] = data.resultMap.customer_nm;
	    									$$(_this.panelId)._setValues(values);	    									
	    								}else{
	    									nkia.ui.utils.notification({type:'error', message:data.resultMsg});
	    								}
	    							}
	    						});
							}
						}),
						createBtnComp({ 
							label:getConstText({isArgs : true, m_key : 'btn.system.00003'}), 
							isProcAfterHide: props.isProcAfterHide,
							id: _this.panelId+'resetBtn',
							click: function(){
								var values = {};
								values[targetHiddenName] = "";
								values[tagetName] = "";
								$$(_this.panelId)._setValues(values);
							}
						})
					]
				});
			}*/		
		}
		
		//this.hiddens[this.targetHiddenName] = '';
		this.hiddens[targetHiddenName] = '';
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.customerPopup = function(props) {
	
};

/**
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.popBtnField = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	this.targetHiddenName = "";
	this.panelId = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.popBtnField(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var panelIdVal =  PROCESS_META.TASK_NAME.toLowerCase();
		if(panelIdVal == "start"){
			panelIdVal = "requst_regist";
		}
		_this.panelId = panelIdVal;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var grpCodeId = props.name.toUpperCase();
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: tagetName,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id : tagetName,
							title: props.label+" 조회",
							width : 1000,
							height : 800,
							popUpType : props.popUpType,
							params : {
								srId : props.srId,
							    grpCodeId : grpCodeId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									id: targetHiddenName,
									name: tagetName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					id: _this.panelId+'resetBtn',
					click: function(){
						var values = {};
						values[targetHiddenName] = "";
						values[tagetName] = "";
						$$(_this.panelId)._setValues(values);
					}
				})
			]
		});
		
		//this.hiddens[this.targetHiddenName] = '';
		this.hiddens[targetHiddenName] = '';
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.popBtnField = function(props) {
	
};

/**
 * @method {} pmCausePopup popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.pmCausePopup= function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.pmCausePopup(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: targetHiddenName,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id : tagetName,
							title: props.label+" 조회",
							width : 1000,
							height : 800,
							popUpType : props.popUpType,
							params : {
								srId : props.srId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									//id: targetHiddenName,
									name: targetHiddenName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var values = {};
						values[targetHiddenName] = "";
						values['pm_emrgncy_ty_cd'] = "";
						values['pm_affcte_ty_cd']  = "";
						
						$$(_this.formId)._setValues(values);
					}
				}),
				createCodeComboBoxComp({
					label: "긴급도",
					name: "pm_emrgncy_ty_cd",
					isProcAfterHide: props.isProcAfterHide,
					code_grp_id: 'PM_EMRGNCY_TY_CD',
					attachNone: true,
					readonly: true
				}),
				createCodeComboBoxComp({
					label: "영향도",
					name: "pm_affcte_ty_cd",
					isProcAfterHide: props.isProcAfterHide,
					code_grp_id: 'PM_AFFCTE_TY_CD',
					attachNone: true,
					readonly: true
				})
			]
		});
		
		//this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.pmCausePopup = function(props) {
	
};

/**
 * @method {} cmRiskPopup popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.cmRiskPopup = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.cmRiskPopup(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: targetHiddenName,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id : tagetName,
							title: props.label+" 조회",
							width : 1000,
							height : nkia.ui.utils.getWidePopupHeight(),
							popUpType : props.popUpType,
							params : {
								srId : props.srId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									//id: targetHiddenName,
									name: targetHiddenName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var values = {};
						values[targetHiddenName] = "";
						values['cm_emrgncy_ty_cd'] = "";
						values['cm_affcte_ty_cd']  = "";
						
						$$(_this.formId)._setValues(values);
					}
				}),
				createCodeComboBoxComp({
					label: "긴급도",
					name: "cm_emrgncy_ty_cd",
					isProcAfterHide: props.isProcAfterHide,
					code_grp_id: 'CM_EMRGNCY_TY_CD',
					attachNone: true,
					readonly: true
				}),
				createCodeComboBoxComp({
					label: "영향도",
					name: "cm_affcte_ty_cd",
					isProcAfterHide: props.isProcAfterHide,
					code_grp_id: 'CM_AFFCTE_TY_CD',
					attachNone: true,
					readonly: true
				})
			]
		});
		
		//this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.cmRiskPopup = function(props) {
	
};

/**
 * @method {} incdntGradPopup popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.incdntGradPopup = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.incdntGradPopup(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: targetHiddenName,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id : tagetName,
							title: props.label+" 조회",
							width : 1000,
							height : nkia.ui.utils.getWidePopupHeight(),
							popUpType : props.popUpType,
							params : {
								srId : props.srId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									//id: targetHiddenName,
									name: targetHiddenName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var values = {};
						values[targetHiddenName] = "";
						values['emrgncy_ty_cd'] = "";
						values['priort_ty_cd']  = "";
						
						$$(_this.formId)._setValues(values);
					}
				}),
				createCodeComboBoxComp({
					label: "긴급도",
					name: "emrgncy_ty_cd",
					isProcAfterHide: props.isProcAfterHide,
					code_grp_id: 'EMRGNCY_TY_CD',
					attachNone: true,
					readonly: true
				}),
				createCodeComboBoxComp({
					label: "우선순위",
					name: "priort_ty_cd",
					isProcAfterHide: props.isProcAfterHide,
					code_grp_id: 'PRIORT_TY_CD',
					attachNone: true,
					readonly: true
				})
			]
		});
		
		//this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.incdntGradPopup = function(props) {
	
};


/**
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.createDmlComp = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.dmlComp(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var codeGrpId = props.name.toUpperCase();
		var targetHiddenName = props.name; 
		var btnVisibleFlag = true;
		if(props.readonly === true){
			btnVisibleFlag = false;
		}
		var render = createUnionFieldComp({
			items: [
				createUnionFieldComp({
			    	items: [
							createCodeComboBoxComp({
							label: props.label,
							name: props.name,
							isProcAfterHide: props.isProcAfterHide,
							code_grp_id: codeGrpId,
							attachChoice: true,
							required: props.required,
							readonly : props.readonly
						}),
				        createBtnComp({
				        	label:"상세보기",
				        	isProcAfterHide: props.isProcAfterHide,
				        	visible:btnVisibleFlag,
				            click: function(){
				            	//showDmlCompWindow();
				            	
				            	nkia.ui.utils.window({
									id : targetHiddenName+'_pop',
									title:"DML유형",
//									width : 1000,
//									height : 400,
									popUpType : props.popUpType,
									params : {
										srId : props.srId
									},
									reference: {
										form: $$(_this.formId),
										fields: {
											//id: targetHiddenName,
											name: targetHiddenName
										}
									},
									callFunc: createPopupFieldWindow
								});
				            }
				        })
			    	]
			    })
			]
		});
		
		//this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.dmlComp = function(props) {
	
};

/**
 * 체크리스트
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.checkList = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '작업체크리스트 작성전입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"checkListBtn",
					click: function(){
						
						var subTyCd = $$('requst_regist')._getFieldValue('req_doc_id');
						
						if("" == nullToSpace(subTyCd)){
							return false;
						}
			         	
						var popupId = "workCheckListEditPop"; // 팝업아이디
			         	var paramMap = {};
						paramMap["packageName"] = "workCheckList"; // 패키지명
						paramMap["areaName"] = PROCESS_META.TASK_NAME.toLowerCase(); // 영역명
						paramMap["fieldName"] = "check_pop_btn"; // 필드명
						paramMap["sr_id"] = nullToSpace(PROCESS_META.SR_ID);
						paramMap["sub_ty_cd"] = nullToSpace(subTyCd);
						
						if('service_managt' == PROCESS_META.TASK_NAME.toLowerCase()){
							paramMap["mode"] = "save";
						}else{
							paramMap["mode"] = "view";
						}
						
						var optMap = {};
						openNbpmWindowPopup(popupId, paramMap, optMap); // nkia.ui.popup.nbpm.js 
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

/**
 * 변경관리 - 스냅샷설정
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.snapshot = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
//		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '스냅샷 설정전입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"요청", 
					   id:"snapshotBtn",
					click: function(){
						var griddata = $$("setSubTabCiAssetGrid")._getRows();
						nkia.ui.utils.window({
							id : 'snapshotPop',
							title:"스냅샷설정유형",
							popUpType : "snapshot",
							params : {
								srId : props.srId,
								gridData : griddata 
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									//id: targetHiddenName,
									name: targetHiddenName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

/**
 * 변경관리 - 스냅샷설정_보이기 [결과등록]
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.snapshot_disp = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
//		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '스냅샷 설정전입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"snapshotDispBtn",
					click: function(){
						var griddata = $$("setSubTabCiAssetGrid")._getRows();
						nkia.ui.utils.window({
							id : 'snapshotPop',
							title:"스냅샷설정유형",
//							width : 1000,
//							height : 400,
							popUpType : "snapshot_disp",
							params : {
								srId : props.srId,
								gridData : griddata
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									//id: targetHiddenName,
									name: targetHiddenName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

/**
 * 변경관리 - 체크리스트 변경전 
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.checklist_before = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
//		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '체크리스트 작성전입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"작성", 
					   id:"checklistBeforeBtn",
					click: function(){
						
						nkia.ui.utils.window({
							id : 'checklistBeforePop',
							title:"체크리스트 작성",
//							width : 1000,
//							height : 400,
							popUpType : "checklist_before",
							params : {
								srId : props.srId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									//id: targetHiddenName,
									name: targetHiddenName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				})
			]
		});
		
		this.hiddens ={
				checklist_before_version : ''
		}
		this.rows = render.rows
	};
};

/**
 * 변경관리 - 체크리스트 변경후 
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.checklist_after = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
//		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '체크리스트 작성전입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"작성", 
					   id:"checklistAfterBtn",
					click: function(){
						
						nkia.ui.utils.window({
							id : 'checklistAfterPop',
							title:"체크리스트작성",
//							width : 1000,
//							height : 400,
							popUpType : "checklist_after",
							params : {
								srId : props.srId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									//id: targetHiddenName,
									name: targetHiddenName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				})
			]
		});
		
		this.hiddens ={
				checklist_after_version : ''
		}
		this.rows = render.rows
	};
};

/**
 * 롯데UBIT - 변경요청 심의항목
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.reviewPop = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.reviewPop(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '작성대기',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"reviewPopBtn",
					click: function(){
						
						var grid = createGridComp({
							id : 'changeReviewGrid',
							keys: ["RNUM"],
							resizeColumn : true,
							height : 320,
							autoLoad : true, // 메모리 그리드로 사용
							resource : "grid.process.change.review",
							params : {sr_id:PROCESS_META.SR_ID},
							url: "/itg/customize/ubit/changeReview.do",
							checkbox : false,
							header: {
								title: "심의항목"								
								},
							editprops: [{
								column: "REVIEW_RESULT", 
								//attachChoice: true,
								params: {code_grp_id : "CHANGE_REVIEW_CD"} 
							}],
							// 필수 입력 표시를 해줄 컬럼명 배열을 적습니다.
							// (필수입력 표시만 해주고, 실제 벨리데이션 체크는 코드상에서 구현해야 함)
							requiredColumns: ["REVIEW_RESULT"]
						});
						
						var form = createFormComp({
							id: "form",
							header: {
								title: "참석자"								
								},
							fields: {
								colSize: 2, // 열 수
								items: [
									{ colspan: 2, item: createTextAreaFieldComp({name: "attendees_list", height: 100 }) }
								]
							}
						});
						
						var popupWindow = createWindow({
					    	id: "popupWindow",
					    	width: 800,
					    	height: nkia.ui.utils.getWidePopupHeight(),
					    	header: {
					    	},
					    	body: {
					    		rows: new Array().concat(grid,form)
					    	},
					    	footer: {
					    		buttons: {
					    			align: "center",
					    			items: [
					    				createBtnComp({label: '저 장', id: "saveBtn", type: "form", click: function() {
					    					var isEditFinish = $$("changeReviewGrid")._checkCellEditorState();
					    					if(!isEditFinish) {
					    						alert("입력 활성화 상태에서는 실행할 수 없습니다.");
					    						return false;
					    					}

					    						var rows = $$("changeReviewGrid")._getRows();
					    						//var saveList = [];
					    						for(var i in rows) {
					    							//console.log(rows[i].CHECK_YN);
					    							if(rows[i].REVIEW_RESULT == null || rows[i].REVIEW_RESULT == ""){
					    								alert("체크되지 않은 항목이 존재합니다.");
					    								return;
					    							}
					    						}
					    						if(!confirm('저장 하시겠습니까?')){ // 저장하시겠습니까? 
				    								return false;
				    							}
					    						
					    						var paramMap = {};
					    						paramMap["INPUT_LIST"] = rows;
					    						paramMap["sr_id"] = PROCESS_META.SR_ID;
					    						paramMap["attendees_list"] = $$("form")._getFieldValue("attendees_list");
					    						nkia.ui.utils.ajax({
					    							url: "/itg/customize/ubit/changeReviewInsert.do", 
					    							params:paramMap,
					    							isMask:true,
					    							success:function(data){
					    								if(data.success){
					    									var saveSuccess = {review_item:"작성완료"};
					    									$$("cab_result")._setValues(saveSuccess);
					    									$$("popupWindow").close();
					    									
					    								}else{
					    									nkia.ui.utils.notification({type:'error', message:data.resultMsg});
					    								}
					    							}
					    						});
					    				}})
					    			]
					    		}
					    	},
					    	closable: true
					    });
						
						// 팝업창 오픈
						$$("popupWindow").show();
						if(PROCESS_META.TASK_NAME=="CAB_RESULT"){
							nkia.ui.utils.shows(['saveBtn']);
						}else{
							nkia.ui.utils.hides(['saveBtn']);
						}
						var valArr = [];
						var defaultList = {};
						
						if($$("changeReviewGrid")._getRows().length == 0){							
						
							defaultList["REVIEW_LIST"] = "작업 대상";
							defaultList["CONCAT_KEY_STRING"] = "1";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "작업 목적";
							defaultList["CONCAT_KEY_STRING"] = "2";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "작업 시간";
							defaultList["CONCAT_KEY_STRING"] = "3";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "상세 작업계획";
							defaultList["CONCAT_KEY_STRING"] = "4";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "연관 서비스 영향도";
							defaultList["CONCAT_KEY_STRING"] = "5";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "인증 관련";
							defaultList["CONCAT_KEY_STRING"] = "6";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "테스트 방법";
							defaultList["CONCAT_KEY_STRING"] = "7";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "백업 및 복구 계획";
							defaultList["CONCAT_KEY_STRING"] = "8";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "보안성 승인 여부";
							defaultList["CONCAT_KEY_STRING"] = "9";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "유지보수 여부";
							defaultList["CONCAT_KEY_STRING"] = "10";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "구성항목 등록 및 변경";
							defaultList["CONCAT_KEY_STRING"] = "11";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
							defaultList["REVIEW_LIST"] = "사업 이익 및 재무 영향";
							defaultList["CONCAT_KEY_STRING"] = "12";
							valArr[0] = defaultList;
							$$("changeReviewGrid")._addRow(valArr);
							
						}
						
						nkia.ui.utils.ajax({
							url: "/itg/customize/ubit/changeReview.do", 
							params : {sr_id:PROCESS_META.SR_ID},
							isMask:true,
							success:function(data){
								if(data.success){
									if(data.gridVO.rows.length != 0){
										$$("form")._setFieldValue("attendees_list",data.gridVO.rows[0].ATTENDEES_LIST);
									}else{
										//CAB공지의 참석자그리드에서 값 가져옴
										var formData = $$("attendees_grid")._getRows();
										var attendeesList = "";
										for(var i=0; i<formData.length; i++){
											attendeesList += formData[i].USER_NM + " / "
										}
										$$("form")._setFieldValue("attendees_list",attendeesList);
									}
								}else{
									nkia.ui.utils.notification({type:'error', message:data.resultMsg});
								}
							}
						});
						/*//CAB공지의 참석자그리드에서 값 가져옴
						var formData = $$("attendees_grid")._getRows();
						var attendeesList = "";
						for(var i=0; i<formData.length; i++){
							attendeesList += formData[i].USER_NM + " / "
						}
						$$("form")._setFieldValue("attendees_list",attendeesList);*/
					}
				})
			]
		});
		
		this.rows = render.rows
	};
};

//프라퍼티 검증함수
nkia.nbpm.validator.reviewPop = function(props) {
	
};

/**
 * 롯데UBIT - 센터출입요청 이전항목불러오기
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.preReqLoadPop = function() {
	// Custom UI Properties
	this.rows = [];
	this.hiddens = {};
	
	// Custom Value Properties
	this.name = "";
	this.label = "";
	this.required = false;
	this.srId = "";
	this.processTypeFilter = "";
	this.isProcAfterHide = false;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.srselect(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var render = createUnionFieldComp({
			items: [
			   createTextFieldComp({
				label: props.label,
				name: props.name,
				required: props.required,
				helper: props.helper,
				helperWidth: props.helperWidth,
				helperHeight: props.helperHeight,
				value: getConstText({isArgs : true, m_key : 'res.label.nbpm.00043'}),
				isProcAfterHide: props.isProcAfterHide,
				readonly: true
			}),
			createBtnComp({ 
				label:getConstText({isArgs : true, m_key : 'btn.common.inquirever'}), 
				   id:"preReqLoadPopBtn",
				click: function(){
						var params = {};
						params.cur_sr_id = _this.srId;
						
						if (_this.processTypeFilter != null) {
							params.req_type = _this.processTypeFilter;
						}
						
						nkia.ui.utils.window({
							id: _this.name + "_window",
							title: getConstText({isArgs : true, m_key : 'res.label.nbpm.00042'}),
							width : nkia.ui.utils.getWidePopupWidth(),
							height : nkia.ui.utils.getWidePopupHeight(),
							url: "/itg/customize/ubit/selectPreReqLoad.do",
							params : params,
							type: "centerPreReqLoad",
							reference: {
								form: $$(_this.formId),
								fields: {
									name: _this.name
								}
							},
							callFunc: createProcessWindow
						});
					}
				})
			]
		});
		
		this.rows = render.rows
	};
};

//프라퍼티 검증함수
nkia.nbpm.validator.preReqLoadPop = function(props) {
	
};

/**
 * 롯데UBIT - 센터출입요청 이전항목불러오기
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.myReqLoad = function() {
	// Custom UI Properties
	this.rows = [];
	this.hiddens = {};
	
	// Custom Value Properties
	this.name = "";
	this.label = "";
	this.required = false;
	this.srId = "";
	this.processTypeFilter = "";
	this.isProcAfterHide = false;
	this.multiCodeId = "";
	this.multiCodeNm = "";	
	this.targetHiddenName = "";
	this.panelId = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.srselect(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var panelIdVal =  PROCESS_META.TASK_NAME.toLowerCase();
		if(panelIdVal == "start"){
			panelIdVal = "requst_regist";
		}
		_this.panelId = panelIdVal;
		var targetHiddenName = props.name+"_nm";
		var  tagetName = props.name;
		var grpCodeId = props.name.toUpperCase();
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: tagetName,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id : tagetName,
							title: props.label+" 조회",
							width : nkia.ui.utils.getWidePopupWidth(),
							height : nkia.ui.utils.getWidePopupHeight(),
							type : "myReqLoad",
							popUpType : props.popUpType,
							params : {
								srId : props.srId,
							    grpCodeId : grpCodeId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									id: targetHiddenName,
									name: tagetName
								}
							},
							callFunc: createProcessWindow
						});
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					id: _this.panelId+'resetBtn',
					click: function(){
						var values = {};
						values[targetHiddenName] = "";
						values[tagetName] = "";
						$$(_this.panelId)._setValues(values);
					}
				})
			]
		
		});
		
		this.rows = render.rows
	};
};

/**
 * 작업요청 팝업선택 테스트
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.testPopUp = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '작업양식 선택 팝업입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"checkListBtn",
					click: function(){
						
						var subTyCd = $$('requst_regist')._getFieldValue('req_doc_id');
						var reqType = PROCESS_META.PROCESS_TYPE;
						
						if("" == nullToSpace(subTyCd)){
							return false;
						}
			         	
						var popupId = "workReqDocPop"; // 팝업아이디
			         	var paramMap = {};
						paramMap["packageName"] = "testPopUp"; // 패키지명
						paramMap["areaName"] = PROCESS_META.TASK_NAME.toLowerCase(); // 영역명
						paramMap["fieldName"] = "check_pop_btn"; // 필드명
						paramMap["sr_id"] = nullToSpace(PROCESS_META.SR_ID);
						paramMap["sub_ty_cd"] = nullToSpace(subTyCd);
						
						/*if('service_managt' == PROCESS_META.TASK_NAME.toLowerCase()){
							paramMap["mode"] = "save";
						}else{
							paramMap["mode"] = "view";
						}*/
						
						var optMap = {};
						openNbpmWindowPopup(popupId, paramMap, optMap); // nkia.ui.popup.nbpm.js 
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

/**
 * 그리드팝업 테스트
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.gridTestPopUp1 = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '그리드 팝업 테스트입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"checkListBtn_1",
					click: function(){
						
						var subTyCd = $$('requst_regist')._getFieldValue('req_doc_id');
						var reqType = PROCESS_META.PROCESS_TYPE;
						
						if("" == nullToSpace(subTyCd)){
							return false;
						}
			         	
						var popupId = "gridTest1"; // 팝업아이디
			         	var paramMap = {};
						paramMap["packageName"] = "gridTest1"; // 패키지명
						paramMap["areaName"] = PROCESS_META.TASK_NAME.toLowerCase(); // 영역명
						paramMap["fieldName"] = "check_pop_btn"; // 필드명
						paramMap["sr_id"] = nullToSpace(PROCESS_META.SR_ID);
						paramMap["sub_ty_cd"] = nullToSpace(subTyCd);
						
						/*if('service_managt' == PROCESS_META.TASK_NAME.toLowerCase()){
							paramMap["mode"] = "save";
						}else{
							paramMap["mode"] = "view";
						}*/
						
						var optMap = {};
						openNbpmWindowPopup(popupId, paramMap, optMap); // nkia.ui.popup.nbpm.js 
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

/**
 * 그리드팝업 테스트
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.gridTestPopUp2 = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '그리드 팝업 테스트입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"checkListBtn_2",
					click: function(){
						
						var subTyCd = $$('requst_regist')._getFieldValue('req_doc_id');
						var reqType = PROCESS_META.PROCESS_TYPE;
						
						if("" == nullToSpace(subTyCd)){
							return false;
						}
			         	
						var popupId = "gridTest2"; // 팝업아이디
			         	var paramMap = {};
						paramMap["packageName"] = "gridTest2"; // 패키지명
						paramMap["areaName"] = PROCESS_META.TASK_NAME.toLowerCase(); // 영역명
						paramMap["fieldName"] = "check_pop_btn"; // 필드명
						paramMap["sr_id"] = nullToSpace(PROCESS_META.SR_ID);
						paramMap["sub_ty_cd"] = nullToSpace(subTyCd);
						
						/*if('service_managt' == PROCESS_META.TASK_NAME.toLowerCase()){
							paramMap["mode"] = "save";
						}else{
							paramMap["mode"] = "view";
						}*/
						
						var optMap = {};
						openNbpmWindowPopup(popupId, paramMap, optMap); // nkia.ui.popup.nbpm.js 
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

/**
 * 그리드팝업 테스트
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.gridTestPopUp3 = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '그리드 팝업 테스트입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"checkListBtn_3",
					click: function(){
						
						var subTyCd = $$('requst_regist')._getFieldValue('req_doc_id');
						var reqType = PROCESS_META.PROCESS_TYPE;
						
						if("" == nullToSpace(subTyCd)){
							return false;
						}
			         	
						var popupId = "gridTest3"; // 팝업아이디
			         	var paramMap = {};
						paramMap["packageName"] = "gridTest3"; // 패키지명
						paramMap["areaName"] = PROCESS_META.TASK_NAME.toLowerCase(); // 영역명
						paramMap["fieldName"] = "check_pop_btn"; // 필드명
						paramMap["sr_id"] = nullToSpace(PROCESS_META.SR_ID);
						paramMap["sub_ty_cd"] = nullToSpace(subTyCd);
						
						/*if('service_managt' == PROCESS_META.TASK_NAME.toLowerCase()){
							paramMap["mode"] = "save";
						}else{
							paramMap["mode"] = "view";
						}*/
						
						var optMap = {};
						openNbpmWindowPopup(popupId, paramMap, optMap); // nkia.ui.popup.nbpm.js 
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

/**
 * 그리드팝업 테스트
 * @method {} popBtnFieldComp
 * 팝업 필드
 * @property {string} id			
 */
nkia.nbpm.fields.gridTestPopUp4 = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.checkList(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					value: '그리드 팝업 테스트입니다.',
					readonly: true
				}),
				createBtnComp({ 
					label:"조회", 
					   id:"checkListBtn_4",
					click: function(){
						
						var subTyCd = $$('requst_regist')._getFieldValue('req_doc_id');
						var reqType = PROCESS_META.PROCESS_TYPE;
						
						if("" == nullToSpace(subTyCd)){
							return false;
						}
			         	
						var popupId = "gridTest4"; // 팝업아이디
			         	var paramMap = {};
						paramMap["packageName"] = "gridTest4"; // 패키지명
						paramMap["areaName"] = PROCESS_META.TASK_NAME.toLowerCase(); // 영역명
						paramMap["fieldName"] = "check_pop_btn"; // 필드명
						paramMap["sr_id"] = nullToSpace(PROCESS_META.SR_ID);
						paramMap["sub_ty_cd"] = nullToSpace(subTyCd);
						
						/*if('service_managt' == PROCESS_META.TASK_NAME.toLowerCase()){
							paramMap["mode"] = "save";
						}else{
							paramMap["mode"] = "view";
						}*/
						
						var optMap = {};
						openNbpmWindowPopup(popupId, paramMap, optMap); // nkia.ui.popup.nbpm.js 
					}
				})
			]
		});
		
		this.hiddens[this.targetHiddenName] = '';
		this.rows = render.rows
	};
};

//프라퍼티 검증함수
nkia.nbpm.validator.checkList = function(props) {
	
};

/**
 * @method {} popBtnFieldComp
 * 초기화 제외 필드
 * @property {string} id			
 */
nkia.nbpm.fields.poponBtnField = function(keep) {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.multiCodeId = "";
	this.multiCodeNm = "";
	this.isProcAfterHide = false;
	this.targetHiddenName = "";
	this.panelId = "";
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.popBtnField(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		var panelIdVal =  PROCESS_META.TASK_NAME.toLowerCase();
		if(panelIdVal == "start"){
			panelIdVal = "requst_regist";
		}
		_this.panelId = panelIdVal;
		var tagetName = props.name+"_nm";
		var targetHiddenName = props.name;
		var grpCodeId = props.name.toUpperCase();
		var render = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: tagetName,
					keepEvent: keep,
					readonly : keep,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						nkia.ui.utils.window({
							id : tagetName,
							title: props.label+" 조회",
							width : 1000,
							height : nkia.ui.utils.getWidePopupHeight(),
							
							popUpType : props.popUpType,
							params : {
								srId : props.srId,
							    grpCodeId : grpCodeId
							},
							reference: {
								form: $$(_this.formId),
								fields: {
									id: targetHiddenName,
									name: tagetName
								}
							},
							callFunc: createPopupFieldWindow
						});
					}
				})
			]
		});
		
		//this.hiddens[this.targetHiddenName] = '';
		this.hiddens[targetHiddenName] = '';
		this.rows = render.rows
	};
};

//프로세스 팝업 필드 생성
function createPopBtnFieldComp(compProperty) {
	
	var props = compProperty||{};
	var comp = {};
	try {
		
		switch(props.popUpType){
		case 'CreatePmCausePopup' : comp = new nkia.nbpm.fields.pmCausePopup();
			break;
		case 'CreateCmRiskPopup' : comp = new nkia.nbpm.fields.cmRiskPopup();
			break;	
		case 'CreateIncdntGradPopup' : comp = new nkia.nbpm.fields.incdntGradPopup();
			break;		
		case 'createDmlComp' : comp = new nkia.nbpm.fields.createDmlComp();
			break;
		case 'checkList' : comp = new nkia.nbpm.fields.checkList();
			break;	
		case 'reviewPop' : comp = new nkia.nbpm.fields.reviewPop();
			break;
		case 'testPopUp' : comp = new nkia.nbpm.fields.testPopUp();
			break;
		case 'gridTest1' : comp = new nkia.nbpm.fields.gridTestPopUp1();
			break;
		case 'gridTest2' : comp = new nkia.nbpm.fields.gridTestPopUp2();
			break;
		case 'gridTest3' : comp = new nkia.nbpm.fields.gridTestPopUp3();
			break;
		case 'gridTest4' : comp = new nkia.nbpm.fields.gridTestPopUp4();
			break;
		case 'customerPopup' : comp = new nkia.nbpm.fields.customerPopup();
			break;
		case 'dnsSelectZone' : comp = new nkia.nbpm.fields.poponBtnField(false);
			break;
		case 'preReqLoad' : comp = new nkia.nbpm.fields.preReqLoadPop();
			break;
		case 'monitoringsms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'monitoringnms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'monitoringdbms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'monitoringwas' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'monitoringvmm' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'monitoringtms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;
		case 'monitoringweburl' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;				
		case 'serverCarry' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'networkCarry' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'securityCarry' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'storageCarry' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'etcCarry' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'rackCarry' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'facilityCarry' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'customerCarry' : comp = new nkia.nbpm.fields.poponBtnField(false);
			break;
		case 'snapshot_set' : comp = new nkia.nbpm.fields.snapshot();
			break;
		case 'snapshot_display' : comp = new nkia.nbpm.fields.snapshot_disp();
			break;
		case 'checklist_before' : comp = new nkia.nbpm.fields.checklist_before();
			break;
		case 'checklist_after' : comp = new nkia.nbpm.fields.checklist_after();
			break;	
		case 'gethostpopup' : comp = new nkia.nbpm.fields.poponBtnField();
			break;			
		case 'locationPopup' : comp = new nkia.nbpm.fields.poponBtnField();
			break;			
		case 'delmonitoringsms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'delmonitoringnms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'delmonitoringdbms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'delmonitoringwas' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'delmonitoringvmm' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;	
		case 'delmonitoringtms' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;
		case 'delmonitoringweburl' : comp = new nkia.nbpm.fields.poponBtnField(true);
			break;				
		case 'monitoringcustomer' : comp = new nkia.nbpm.fields.poponBtnField();
			break;			
		case 'amdbPopup' : comp = new nkia.nbpm.fields.poponBtnField();
			break;		
		case 'myReqLoad' : comp = new nkia.nbpm.fields.myReqLoad();
			break;				
			
		default : comp = new nkia.nbpm.fields.popBtnField();
			break;
		}
		comp.setProps(props);
		comp.setRender(props);
		
	} catch(e) {
		alert(e.message);
	}
	return comp;

}

/**
 * 팝업콜백함수
 */
function fn_popupCallback(popupId, reqMap, resData)
{
	//fn_popupCallback__LOG_PRINT(popupId, reqMap, resData); // 로그기록
	if(!resData){
		return false;
	}
	//체크리스트
	if("workCheckListEditPop" == popupId){/* // 팝업ID
		if("requst_plan^check_pop_btn" == reqMap["areaName"]+"^"+reqMap["fieldName"]){
			//nkia.ui.utils.notification({type:'info', message: resData["resultMsg"], expire:2000});
			$$("requst_plan")._setValues({check_pop_btn: resData["resultMsg"]});
		}else if("check_plan^check_pop_btn" == reqMap["areaName"]+"^"+reqMap["fieldName"]){
			//nkia.ui.utils.notification({type:'info', message: resData["resultMsg"], expire:2000});
			$$("check_plan")._setValues({check_pop_btn: resData["resultMsg"]});
		}
	*/
		if("service_managt^check_pop_btn" == reqMap["areaName"]+"^"+reqMap["fieldName"]){
			$$("service_managt")._setValues({check_pop_btn: resData["resultMsg"]});
		}		
	}
}


/**
 * 팝업콜백함수 테스트
 */
function fn_popupCallback2(popupId,sub_ty_cd)
{
	//fn_popupCallback__LOG_PRINT(popupId, reqMap, resData); // 로그기록
/*	if(!resData){
		return false;
	}*/
	//체크리스트
	if("workReqDocPop" == popupId){/* // 팝업ID
		if("requst_plan^check_pop_btn" == reqMap["areaName"]+"^"+reqMap["fieldName"]){
			//nkia.ui.utils.notification({type:'info', message: resData["resultMsg"], expire:2000});
			$$("requst_plan")._setValues({check_pop_btn: resData["resultMsg"]});
		}else if("check_plan^check_pop_btn" == reqMap["areaName"]+"^"+reqMap["fieldName"]){
			//nkia.ui.utils.notification({type:'info', message: resData["resultMsg"], expire:2000});
			$$("check_plan")._setValues({check_pop_btn: resData["resultMsg"]});
		}
	*/
		alert("양식 화면 셋팅");
		setWorkReqDocGridShow2(sub_ty_cd);
		setTemplateDownloaderList2(sub_ty_cd);
		/*if("requst_regist^check_pop_btn" == reqMap["areaName"]+"^"+reqMap["fieldName"]){
		}*/		
	}
}

/********************************************
 * 공백용 필드(화면에 공백을 배치하기 위한 필드) 
 * Date: 2018-07-18
 ********************************************/
nkia.nbpm.fields.blank = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.label = "공백";
	this.name = "blank_field";
	this.isProcAfterHide = true;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.blank(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		
		var render = createUnionFieldComp({
			items: [
				createTextFieldComp({
					label: '공백'
				  , name: _this.name 
				  , isProcAfterHide: _this.isProcAfterHide
				})
			]
		});
		
		this.hiddens = {
			
		};
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.blank = function(props) {
	
};

/**
 * 공백용 필드(화면에 공백을 배치하기 위한 필드) 
 * @method {} createComp
 * @property {string} id			
 */
function createBlankComp(compProperty){
	var props = compProperty||{};
	var comp = {};
	try {
		comp = new nkia.nbpm.fields.blank();
		comp.setProps(props);
		comp.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return comp;
}

/********************************************
 * 프로세스 그리드 필드(폼안에 그리드를 배치하기 위한 필드) 
 * 종류 
 *  - 읽기전용 그리드
 *  - 선택저장 그리드
 *  - 직접입력 그리드
 *  - 팝업 그리드
 * Date: 2018-07-25
 ********************************************/
nkia.nbpm.fields.processGridField = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	
	this.id = "";
	this.name = "";
	this.label = "";
	this.params = {}
	this.sr_id = "";
	this.selectQuerykey = "";
	this.insertQuerykey = "";
	this.deleteQuerykey = "";
	this.requiredColumns = {};
	this.pageable = false;
	this.url = "";
	this.resource = "";
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.required = false;
	this.editorMode = false;
	this.popUpType = "";
	this.processGridField = true;
	this.editprops = [];
	this.visible = "show";
	this.headerMerges = [];

	this.propsValidate = function(props) {
		nkia.nbpm.validator.processGridField(props);
	};
	
	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		var cols = [];
		var rowInsertBtn = null;
		var removeRowBtn = null;
		var insertBtn = null;
		var deleteBtn = null;
		
		
		var paramMap = {};
		if(_this.params){
			paramMap = _this.params;
		}
		paramMap['sr_id'] = _this.sr_id;
		paramMap['selectQuerykey'] = _this.selectQuerykey;
		paramMap['insertQuerykey'] = _this.insertQuerykey;
		paramMap['deleteQuerykey'] = _this.deleteQuerykey;
		
		//직접입력 그리드, 팝업 그리드
		if(_this.editorMode === true){
			paramMap['editorMode'] = true;
		
		}else{
			//읽기전용 그리드, 선택저장 그리드
			paramMap['editorMode'] = false;
		}
		
		var requiredColumns = [];
		if(this.requiredColumns){
			//requiredColumns = _this.requiredColumns.split(',');
			requiredColumns = this.requiredColumns;
		}
		
		var gridProps = {
			id : _this.id,
			keys: [],
			resizeColumn : true,
			pageable : _this.pageable,
			height : 150,
			url : _this.url,
			resource : _this.resource,
			params : paramMap,
			helper: _this.helper,
			helperWidth: _this.helperWidth,
			helperHeight: _this.helperHeight,
			required: _this.required,
			requiredColumns: requiredColumns,
			processGridField: true,
			editprops : _this.editprops,
			headerMerges : _this.headerMerges,
			visible : _this.visible
		};
		
		
		//선택저장 그리드인 경우
		if(_this.editorMode === false && _this.insertQuerykey){
			gridProps.checkbox = true;
		}
		
		//직접입력 그리드, 팝업 그리드
		if(_this.editorMode === true){
			var popUpType = _this.popUpType;
			
			if(typeof gridProps.header === 'undefined'){
			
				gridProps.header = {
					title: _this.label
				   ,headerNoCnt : true
				}
			}
			
			switch(popUpType){
				case 'multiTask' 	:   // 멀티태스크
					
				case 'zoneSelect': // 멀티태스크
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label,
							_this.required, _this.id, popUpType);

					gridProps.checkbox = true;
					gridProps.header = {
						template : labelTemplate,
						height : 40
					};

					break;
					
				case 'operUserGrid' :   //(협조)작업자	
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				
				case 'attendees' :   //참석자
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				
				case 'changeAlarm' :   //변경요청 알림대상자
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = false;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				
				case 'centerGuestInfo' :   //센터출입요청 출입자정보(출/퇴실처리)
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				
				case 'incidentServiceList' :   //서비스영향목록
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				
				case 'incidentEmsList' :   //관련장애이벤트
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				
				case 'checklistGrid' :  
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = false;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				case 'grid_test' :  
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
								
				case 'zoneInsert' :  
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;

				case 'recordSerch' :  
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				case 'recordSerchDelete' :  
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				case 'recordInsert' :  
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;

				case 'zoneInsert_pop' :  
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;
				
				case 'lowformatlist': // 멀티태스크
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label,
							_this.required, _this.id, popUpType);

					gridProps.checkbox = true;
					gridProps.header = {
						template : labelTemplate,
						height : 40
					};

					break;				
					
				case 'centerWork' :   //센터출입요청 - 작업절차
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id, popUpType);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				break;					

				case 'centermove': // 멀티태스크
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label,
							_this.required, _this.id, popUpType);

					gridProps.checkbox = true;
					gridProps.header = {
						template : labelTemplate,
						height : 40
					};

					break;					
				
				
				
				
				
				
				default: // 기본 - 행추가/삭제 버튼 추가
					
					// 프로세스 그리드 필드 헤더 템플릿 세팅
					var labelTemplate = "";
					labelTemplate = nkia.ui.html.processGridField(_this.label, _this.required, _this.id);
					
					gridProps.checkbox = true;
					gridProps.header = {
						template: labelTemplate
					  , height: 40
					};
					
				 break;
			
				}
		}
		
		//그리드 on 이벤트
		gridProps.on = {
			onAfterEditStop:function(state, editor, ignoreUpdate){
				if(editor.column == "INFLUENCE_RANGE"){
					if(state.value<0 || state.value>100){
						nkia.ui.utils.notification({type:'error', message:"0~100까지만 입력가능합니다."});
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = state.old;
						this.updateItem(editor.row, thisRow);
					}
				}
			}
		}
		//멀티
      if(this.id == 'multi_all_grid'|| this.id == 'multi_order_grid') {
          gridProps.on.onItemDblClick = function(id){
             var item = this.getItem(id);
             var taskId = item.TASK_ID;
             if (taskId != null && taskId != "") {
                showMultiTaskDetailPop(PROCESS_META.SR_ID, taskId, item.EVENT_NAME);
             } else {
                nkia.ui.utils.notification({
                       type: 'error',
                       message: '티켓생성 후 조회 가능합니다.'
                   });
             }
          }
       }						
		//UBIT 액션컬럼
		//2019-11-18 : 프로세스그리드에서 사용될 모든 액션컬럼 정의 후 grids-itg_ko_KR.properties에서 필요한컬럼 사용
		gridProps.actionColumns = [{
			column: "ACTION_COLUMN",
			template: function(data){
				return nkia.ui.html.icon("search", "showMultiPop"); 
			},
			onClick: {
				"showMultiPop": function(e, row, html){
					var item = this.getItem(row);
				}
			}
		},
		{
			column: "ACTION_COLUMN2",
			template: function(data){
				return nkia.ui.html.icon("circle", "showMultiPop"); 
			},
			onClick: {
				"showMultiPop": function(e, row, html){
					var item = this.getItem(row);

					var editform = createFormComp({
						id: "editform",
						
						fields: {
							colSize: 2, // 열 수
							items: [
								{ colspan: 1, item: createTextFieldComp({label: "구분", name: "work_gubun", resizable: true, height: 100, value: item.WORK_GUBUN }) }
								,{ colspan: 1, item: createCodeComboBoxComp({label:"관리구분",name:"mgmt_place", code_grp_id:"MGMT_PLACE", attachChoice:true, value: item.MGMT_PLACE})}
								,{ colspan: 2, item: createTextFieldComp({label: "도메인영역", name: "zone_id_nm", resizable: true, height: 100, value: item.ZONE_ID_NM }) }
								,{ colspan: 1, item: createTextFieldComp({label: "구매사이트", name: "buy_site_cn", resizable: true, height: 100, value: item.BUY_SITE_CN }) }
								,{ colspan: 1, item: createTextFieldComp({label: "구매사이트웹주", name: "buy_site_url_cn", resizable: true, height: 100, value: item.BUY_SITE_URL_CN }) }
								,{ colspan :1, item: createDateFieldComp({label: "구매일", name :"buy_dt", dateType:"D", value: item.BUY_DT/*, dateValue:createDateTimeStamp('D',{'year':0,'month':0,'day':0,'hour':0,'min':0})*/})}
								,{ colspan :1, item: createDateFieldComp({label: "만료일", name :"expire_dt", dateType:"D", value: item.EXPIRE_DT})}
								,{ colspan: 1, item: createTextFieldComp({label: "구매담당자", name: "buy_user", resizable: true, height: 100, value: item.BUY_USER }) }
								,{ colspan: 1, item: createTextFieldComp({label: "관리담당자", name: "mgmt_user", resizable: true, height: 100, value: item.MGMT_USER }) }
							]
						}
					});
					
					var popupWindow = createWindow({
				    	id: "popupWindow",
				    	header: {
				    		title: "도메인목록"
				    	},
				    	width: 800,
				    	height: 650,
				    	body: {
				    		rows: new Array().concat(editform)
				    	},
				    	footer: {
				    		buttons: {
				    			align: "center",
				    			items: [
				    				createBtnComp({label: '수 정', type: "form", click: function() {
//				    					var clickRecord = $$("popupGrid")._getSelectedRows();
//				    					// _getSelectedRows()는 그리드의 선택된 값을 리턴하는 함수로, 항상 배열로 리턴됩니다.
				    					var tempCust = {};
				    					var valArr = [];
				    					
				    					item.WORK_GUBUN = $$("editform")._getFieldValue("work_gubun");
				    					item.MGMT_PLACE = $$("editform")._getFieldValue("mgmt_place");
				    					item.ZONE_ID_NM = $$("editform")._getFieldValue("zone_id_nm");
				    					item.BUY_SITE_CN = $$("editform")._getFieldValue("buy_site_cn");
				    					item.BUY_SITE_URL_CN = $$("editform")._getFieldValue("buy_site_url_cn");
				    					item.BUY_DT = $$("editform")._getFieldValue("buy_dt");
				    					item.EXPIRE_DT = $$("editform")._getFieldValue("expire_dt");
				    					item.BUY_USER = $$("editform")._getFieldValue("buy_user");
				    					item.MGMT_USER = $$("editform")._getFieldValue("mgmt_user");
				    					
				    					valArr[0] = tempCust;
				    					/*$$("zone_grid")._removeRow(item.id);
				    					$$("zone_grid")._addRow(valArr);*/
//				    					$$("editform")._setValues(values);
				    					$$("zone_grid").adjustRowHeight();
				    					popupWindow.close();
				    				}})
				    			]
				    		}
				    	},
				    	closable: true
				    });
					
					// 팝업창 오픈
					$$("popupWindow").show();
				}
			}
		},
		{
			column: "ACTION_REVIEW_ITEM",
			template: function(data){
				return nkia.ui.html.icon("search", "showReviewHisPop"); 
			},
			onClick: {
				"showReviewHisPop": function(e, row, html){

					var his_no = this.getItem(row).HIS_NO;
					var grid = createGridComp({
						id : 'changeReviewHisGrid',
						keys: ["RNUM"],
						resizeColumn : true,
						height : 320,
						autoLoad : true, // 메모리 그리드로 사용
						resource : "grid.process.change.review.his",
						params : {
									sr_id:PROCESS_META.SR_ID,
									his_no:his_no
								},
						url: "/itg/customize/ubit/changeReviewHis.do",
						checkbox : false,
						header: {
							title: "심의항목"								
							},
						editprops: [{
							column: "REVIEW_RESULT", 
							//attachChoice: true,
							params: {code_grp_id : "CHANGE_REVIEW_CD"} 
						}]
					});
					
					var form = createFormComp({
						id: "form",
						header: {
							title: "참석자"								
							},
						fields: {
							colSize: 2, // 열 수
							items: [
								{ colspan: 2, item: createTextAreaFieldComp({name: "attendees_list", height: 100, readonly:true }) }
							]
						}
					});
					
					var popupWindow = createWindow({
				    	id: "popupWindow",
				    	width: nkia.ui.utils.getWidePopupWidth(),
				    	height: nkia.ui.utils.getWidePopupHeight(),
				    	header: {
				    	},
				    	body: {
				    		rows: new Array().concat(grid,form)
				    	},
				    	closable: true
				    });
					
					// 팝업창 오픈
					$$("popupWindow").show();
										
					nkia.ui.utils.ajax({
						url: "/itg/customize/ubit/changeReviewHis.do", 
						params : {
									sr_id:PROCESS_META.SR_ID,
									his_no:his_no
								},
						isMask:true,
						success:function(data){
							if(data.success){
								$$("form")._setFieldValue("attendees_list",data.gridVO.rows[0].ATTENDEES_LIST);								
							}else{
								nkia.ui.utils.notification({type:'error', message:data.resultMsg});
							}
						}
					});
					
				}
			}
		},
		{
			column: "CEN_MOVE_ACTION",
			template: function(data){
				return nkia.ui.html.icon("circle", "showMultiPop"); 
			},
			onClick: {
				"showMultiPop": function(e, row, html){
					var loc_info = $$('requst_regist')._getFieldValue('cenmove_center');
					var loc_valid_info = $$('requst_regist')._getFieldValue('center_id');
					if(loc_info == ""){
						nkia.ui.utils.notification({
							type: 'error',
				            message: '이전센터를 선택해주세요.'
				        });
					}else if(loc_info == loc_valid_info){
						nkia.ui.utils.notification({
							type: 'error',
				            message: '센터와 이전센터의 값이 같습니다.'
				        });
				        
					}
					else{
					var _this = this;					
					
				
					//		var customerId = $$('requst_regist')._getFieldValue('customer_id');
					var searchForm = createFormComp({
						id: _this.popup_object_id + "_searchForm",
						elementsConfig: {
				    		labelPosition: "left"
				    	},
						fields: {
							colSize: 1,
							items: [
								{colspan: 1, item: createCodeComboBoxComp({label: '센터위치', name: 'locationTopCode', value: loc_info, readonly : true 
								//	, on: { onChange: this.searchTreeList }
								})}
							]
						}
					});
					
					var tree = createTreeComponent({
						id: _this.popup_object_id + "_tree",
				        url: '/itg/itam/location/searchLocation.do',
				        filterable: true,
				        expColable: true,
				        params: {use_yn: 'Y', locationTopCode: loc_info},
				        expandLevel: 2,
				        on: {
				    //    	onAfterSelect: this.afterSelect(_this)
				        }
				    });
					
					var locationForm = createFormComp({
						id: _this.popup_object_id + "_form",
						elementsConfig: {
				    		labelPosition: "left"
				    	},
						fields: {
							colSize: 1,
							items: [
								{colspan: 1, item: createTextFieldComp({label: '기타위치', name: 'POSITION_DETAIL_POP' })}
							]
						}
					});
					
					var bodyScrollView = {
				        view: "scrollview",
				        body: {
							cols: [{
				                rows: new Array().concat(searchForm, tree, locationForm)
				            }]
						}
					};
					
					
					var popupWindow = createWindow({
				    	id: "popupWindow",
				    	width : nkia.ui.utils.getWidePopupWidth(),
				    	height : nkia.ui.utils.getWidePopupHeight()	,
				    	header: {
				    		title: "위치 선택"
				    	},
				    	body: {
				    		rows: [bodyScrollView]
				    	},
				    	footer: {
				    		buttons: {
				    			align: "center",
				    			items: [
			    		            createBtnComp({label: '선 택', type : "form" ,
			    		            	click: choice
			    		            	})
			    		        ]
				    		}
				    	},
				    	closable: true
				    });
					
					// 팝업창 오픈
					
					$$("popupWindow").show();
					
					
					this.searchTreeList = function() {
						var _this = this;
						
						$$(_this.popup_object_id + '_tree')._reload($$(_this.popup_object_id + '_searchForm')._getValues());
					};
					
					this.afterSelect = function(id) {
						var _this = this;
						var treeItem = $$(_this.popup_object_id + "_tree").getSelectedItem();
						var form = $$(_this.popup_object_id + "_form");
						
						if(!id.match(/NOTCODE/gi)) {
							form._setValues({POSITION_DETAIL_POP: ''});
						}
					};
					
					// 선택 이벤트
					function choice(){
						
						var _this = this;
						// 보여지는 name필드 뒤에 붙는 suffix(자바 원장자동화에서 정의)
						var _NAME_FIELD_SUFFIX = this.name_field_suffix;
						
						var treeItem = $$(this.popup_object_id + "_tree").getSelectedItem();
						var form = $$(_this.popup_object_id + "_form");
						var formData = form._getValues();
						
						if(treeItem){
							
							
							var values = {};
							
							var locationDetail = getLocationDetail(treeItem.node_id);
							var locType = locationDetail.LOC_TYPE;
							var locLeaf = treeItem.leaf;
							
							if(locType == null) {
								nkia.ui.utils.notification({
					                type: 'error',
					                message: '최상위 계층은 선택하실 수 없습니다.'
					            });
								return false;
							}
							
							if(locLeaf == false) {
								nkia.ui.utils.notification({
					                type: 'error',
					                message: '마지막 노드만 선택할 수 있습니다.'
					            });
								return false;
							}
							
							
							if(locationDetail.LOC_CODE.match(/NOTCODE/gi)) {
								if(!formData['POSITION_DETAIL_POP']) {
									nkia.ui.utils.notification({
						                type: 'error',
						                message: '기타위치을 입력해 주세요.'
						            });
									return false;
								}
							} else {
								form._setValues({POSITION_DETAIL_POP: ''});
							}
							
							if(_this.reference && _this.reference.grid) {
								_this.reference.selectedItem['LOC_CD'] = locationDetail.LOC_CODE;
								_this.reference.selectedItem['TO_CENTER_POSITION_CD'] = locationDetail.LOC_NM;
								_this.reference.selectedItem['TO_POSITION_DETAIL'] = formData['POSITION_DETAIL_POP'];
								_this.reference.grid._addRow(_this.reference.selectedItem);
							} else {
								if(locType == "AREA") {								
									values["TO_LOC_CODE"] = locationDetail.LOC_CODE;
									values["TO_CENTER_POSITION_CD"] = locationDetail.LOC_NM;							
									values["TO_LOCATION_CD"] = locationDetail.LOCATION_CD;									
									if(locationDetail.LOC_CODE.match(/NOTCODE/gi)) {
										values["TO_POSITION_DETAIL"] = formData['POSITION_DETAIL_POP'];
									}
									else{
										values["TO_POSITION_DETAIL"] = null;
									}
								} else if(locType == "FLOOR") {								
									values["TO_LOC_CODE"] = locationDetail.LOC_CODE;
									values["TO_CENTER_POSITION_CD"] = locationDetail.LOC_NM;							
									values["TO_LOCATION_CD"] = locationDetail.LOCATION_CD;
								}
								
							
								var ret_grid = $$('requst_regist')._getField('center_move');
							
								$$('center_move').updateItem(row, values);		
								
								$$('center_move').refresh(row);   
								
							}
							$$("popupWindow").close();				          
						}
					};
					
					// 선택한 트리의 값의 상세정보를 구하는 함수
					 function getLocationDetail(locCode) {
						
						var locationInfo = {};
						
						nkia.ui.utils.ajax({
							viewId: 'assetDetailViewer',
							url: '/itg/itam/location/selectLocation.do',
							params: { loc_code : locCode },
							async: false,
							isMask: false,
							success: function(response){
								locationInfo = response.resultMap;
							}
						});
						
						return locationInfo;
					}
					
					
					
				}
					
					
				}
			}
		}
	]
		
		this.rows = createGridComp(gridProps);
		
		if(_this.editorMode === true){
			
		}else{
			// 프로세스 그리드 필드 라벨 템플릿 세팅
			var labelTemplate = "";
			labelTemplate = nkia.ui.html.processGridField(this.label, _this.required);

			cols = [
			    { view: "label", label: this.label, template: labelTemplate, height: 40 }
			];
			
			this.rows.unshift({
				cols: cols
			});	
		}
	};
}		
     
//프라퍼티 검증함수
nkia.nbpm.validator.processGridField = function(props) {
	
};

// 프로세스 그리드 필드 추가 버튼 이벤트
function clickProcessGridInsertBtn(gridId, popUpType){
	
	switch(popUpType){
	case 'multiTask' :
		// // 멀티태스크 사용자 추가 버튼 이벤트
		
		var grid = $$(gridId);
		var params = grid.config.params;
		var roleId = "";
		if(params.roleId){
			roleId = params.roleId;
		}else{
			roleId = params.oper_type;
		}
		
		//권한 추가(임시)
		// 'roleId': 'ROLE_OPETR_06'
		if(PROCESS_META.REQ_TYPE == 'WORK'){
			roleId = 'ROLE_OPETR_06';
		}else if(PROCESS_META.REQ_TYPE == 'DISUSE'){
			roleId = 'ROLE_OPETR_07';
		}
		
		var params = {};
		if (roleId != "servicedesk") {
			params["auth_id"] = PROCESS_META.PROCESS_TYPE + "_" + roleId;
		} else {
			params["auth_id"] = roleId;
		}
		
		var taskName = PROCESS_META.TASK_NAME.toUpperCase();
		if(taskName == "START"){
			taskName = "REQUST_REGIST";
		}
		
		params["task_name"]       = taskName;			
		params["oper_type"]       = roleId;
		params["process_type"]    = PROCESS_META.REQ_TYPE; 
		params["sr_id"]           = PROCESS_META.SR_ID; 
		params["event_name"]      = grid.config.params.event_name; 
		params["processid"]       = PROCESS_META.PROCESSID; 
		params["table_name"]      = grid.config.params.table_name;
		params["nbpm_table_name"] = grid.config.params.table_name;
		
		nkia.ui.utils.window({
			id: "userWindow",
			title: "담당자 조회",
			width : 1000,
			type: "multiTaskTemp",
			//url: "/itg/system/user/searchUserList.do",
			url: "/itg/nbpm/common/searchProcessOperUserList.do",
			params: params,
			reference: {
				grid: $$(gridId),
				data: $$(gridId)._getRows()
			},
			callFunc: createUserWindow
		});
	break;
	case 'zoneSelect':
		// // 멀티태스크 사용자 추가 버튼 이벤트

		var grid = $$(gridId);
		var params = grid.config.params;
		var roleId = "";
		if (params.roleId) {
			roleId = params.roleId;
		} else {
			roleId = params.oper_type;
		}

		// 권한 추가(임시)
		// 'roleId': 'ROLE_OPETR_06'
		if (PROCESS_META.REQ_TYPE == 'WORK') {
			roleId = 'ROLE_OPETR_06';
		} else if (PROCESS_META.REQ_TYPE == 'DISUSE') {
			roleId = 'ROLE_OPETR_07';
		}

		var params = {};
		if (roleId != "servicedesk") {
			params["auth_id"] = PROCESS_META.PROCESS_TYPE + "_" + roleId;
		} else {
			params["auth_id"] = roleId;
		}

		var taskName = PROCESS_META.TASK_NAME.toUpperCase();
		if (taskName == "START") {
			taskName = "REQUST_REGIST";
		}

 
		
		params["task_name"] = taskName;
		params["oper_type"] = roleId;
		params["process_type"] = PROCESS_META.REQ_TYPE;
		params["sr_id"] = PROCESS_META.SR_ID;
		params["event_name"] = grid.config.params.event_name;
		params["processid"] = PROCESS_META.PROCESSID;
		params["table_name"] = grid.config.params.table_name;
		params["nbpm_table_name"] = grid.config.params.table_name;

		nkia.ui.utils.window({
			id : "userWindow",
			title : "담당자 조회",
			width : 1000,
			type : "zoneSelect",
			// url: "/itg/system/user/searchUserList.do",
			url : "/itg/itam/dns/searchDnsZoneList.do",
			params : params,
			reference : {
				grid : $$(gridId),
				data : $$(gridId)._getRows()
			},
			callFunc : createUserWindow
		});
		break;
	case 'operUserGrid' :   //(협조)작업자	
		// 사용자 추가 버튼 이벤트
		var srId   = PROCESS_META.SR_ID;
		var taskName = PROCESS_META.TASK_NAME;
		
		var param = {
			sr_id : srId
		  , task_name : taskName
		  , grid_id : gridId
		};
			
		nkia.ui.utils.window({
			id: "userSelectPop",
			type: 'dependencyUser',
			subType: 'cab',
			subUrl: '/itg/customize/lgup/searchProcUserList.do',
			acceptUrl: '/itg/customize/lgup/insertProcUser.do',
			popupParamData: param,
			title: '사용자 선택 팝업',
			callFunc: createUserWindow
		});
	break;
	case 'attendees' :   //(협조)작업자	
		var searchUserForm = createFormComp({
			id: 'searchUserForm',
			elementsConfig:{
				labelPosition: "left"
			},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.common.search'})
			  , icon: "search"		
			},
			footer: {
				buttons: {
					align: "center",
					items:[
					       createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.search'}), id: 'serachUserBtn', type: 'form', click: function() {
					    		var data = $$("searchUserForm")._getValues();
					    		$$("smsUserGrid")._reload(data);
		    				}})
					     , createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.reset'}), id: 'searchUserInitBtn', click: function() {
					    		$$("searchUserForm")._reset();
		    				}}) 
					],
					css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: [
				      {colspan: 2, item: createUnionFieldComp({
	                  items: [
	                          	createCodeComboBoxComp({label: '조회구분', name: 'search_type', code_grp_id: 'SEARCH_SMS_USER', attachChoice: true, width: 250})
	                            , createTextFieldComp({name: "search_value", placeholder: '검색어를 입력해주세요.', on: {onKeyPress:"userPressEnterKey"}})
	                      ]
	                  })
					}
				]
			}
		});
		
		var userGridId = 'smsUserGrid';
		var userGrid = createGridComp({
			id: userGridId,
			keys: ["USER_ID"],
			resizeColumn: true,
			pageable: true,
			pageSize: 10,
			badgeId: userGridId+"_badge",
			url: '/itg/workflow/smsGrp/searchSmsUserList.do',
			resource: 'grid.workflow.smsUser',
			params: {},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.title.smsUserList'})
			},
			on:{
				onItemDblClick: userGridDbclick
			},
			dependency : { // 의존관계 그리드 사용
				targetGridId: "smsSendGrid" // 대상 그리드 id
			}

		});
				
		
		// 그룹목록
		var searchGrpForm = createFormComp({
			id: 'searchGrpForm',
			elementsConfig:{
				labelPosition: "left"
			},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.common.search'})
			  , icon: "search"		
			},
			footer: {
				buttons: {
					align: "center",
					items:[
					       createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.search'}), id: 'serachGrpBtn', type: 'form', click: function() {
					    		var data = $$("searchUserForm")._getValues();
					    		$$("smsUserGrid")._reload(data);
		    				}})
					     , createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.reset'}), id: 'searchGrpInitBtn', click: function() {
					    		$$("searchGrpForm")._reset();
		    				}}) 
					],
					css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: [
				      {colspan: 2, item: createUnionFieldComp({
	                  items: [
	                          	createCodeComboBoxComp({label: '조회구분', name: 'search_type', code_grp_id: 'SEARCH_SMS_GRP', attachChoice: true, width: 250})
	                            , createTextFieldComp({name: "search_value", placeholder: '검색어를 입력해주세요.', on: {onKeyPress:"grpPressEnterKey"}})
	                      ]
	                  })
					}
				]
			}
		});

		var grpGridId = 'smsGrpGrid';
		var grpGrid = createGridComp({
			id: grpGridId,
			keys: ['USER_ID'],
			resizeColumn: true,
			pageable: true,
			pageSize: 10,			
			badgeId: grpGridId+"_badge",
			url: '/itg/workflow/smsGrp/searchSmsGrpList.do',
			resource: 'grid.workflow.smsGrp',
			params: {upSmsGrpId:'0000012'},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.title.smsGroupList'})

			},
			on:{
				onItemDblClick: grpGridDbclick
			}
		});
		
		// 탭 bar 를 구성합니다. (탭 상단 구성)
		var tabBar = [{
			// 첫번째 탭 정보
			id: grpGridId + "_tab", // 개별 탭의 id
			value: "그룹목록" + "<span id='" + (grpGridId + "_badge") +"' class='webix_badge'>0</span>" // 탭 헤더명 (그리드에서 구성한 badge_id로 badge 구성)
			
		},{
			// 두번째 탭 정보
			id: userGridId + "_tab", // 개별 탭의 id
			value: "사용자목록" + "<span id='" + (userGridId + "_badge") +"' class='webix_badge'>0</span>" // 탭 헤더명 (그리드에서 구성한 badge_id로 badge 구성)
		}];
		
		// 탭의 body 를 구성합니다.
		var tabItem = [{
			// 첫번째 탭 body
			id: grpGridId + "_tab", // 개별 탭의 id, tabBar에서 부여한 id와 동일하게 세팅하여야 합니다.
			rows: new Array().concat(searchGrpForm, grpGrid) // 실제 배치할 객체를 넣어줍니다.
		},{
			// 두번째 탭 body
			id: userGridId + "_tab", // 개별 탭의 id, tabBar에서 부여한 id와 동일하게 세팅하여야 합니다.
			rows: new Array().concat(searchUserForm, userGrid) // 실제 배치할 객체를 넣어줍니다.

		}];
		
		// 실제 탭을 구성합니다.
		var tabGrid = {
			view:"tabview", // view 형식 : tabview로 입력
			id:"smsTab", // 탭의 ID
			animate:false,
			tabbar:{
				options: tabBar // tabbar 배치
			},
			cells:new Array().concat(tabItem) // tab body 배치
		};

		var smsSendGrid = createGridComp({
			id: 'smsSendGrid',
			keys: ['HP_NO'],
			resizeColumn: true,
			pageable: false,
			height : 200,
			checkbox: true,
			autoLoad: false,
			sortConfig : 'string',
			url: '/itg/workflow/smsGrp/searchSmsSendList.do',
			resource: 'grid.workflow.smsSend',
			params: {},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.title.smsToNumList'}),
	        	buttons: {
	       		 items: [
	       		        createBtnComp({label: '전체삭제', id:'userGrpAllDeleteBtn',  click:function(){
	       		        	ALL_GRP_NM = "";
	       		        	$$("smsSendGrid")._removeAllRow();
	       		        }})
	   	            ]
	        	}
			},
			// 액션 컬럼에 대한 정보를 세팅합니다. 액션 컬럼은 특정한 Action을 일으킬 수 있는 역할을 하는 컬럼을 의미합니다.
			// 행을 삭제하는 Action을 수행할 버튼을 배치합니다.
			actionColumns: [{
				column: "DEL_ACTION", // 액션 컬럼의 헤더명 (그리드 리소스)
				template: nkia.ui.html.icon("remove", "remove_row"), // 버튼 아이콘("아이콘 명", "생성할 CSS명(onClick 이벤트 명과 매칭되어야 함)")
				onClick: { // 이벤트 연결(실행 할 이벤트명 매칭)
					"remove_row": removeSmsSendData
				}
			}]
		});
		
		var popupWindow1 = createWindow({
	    	id: "popupWindow1",
	    	width: nkia.ui.utils.getWidePopupWidth(),
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: "알림대상자설정"
	    	},
			body: {
				rows: [{
					cols:[{
						rows: new Array().concat(tabGrid, smsSendGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: function() {
	    					$$(gridId)._removeAllRow();
	    					$$(gridId)._addRow($$("smsSendGrid")._getRows());
	    					
	    					popupWindow1.close();
	    				}})
	    			]
	    		}
	    	},
	    	closable: true
	    });
		
		// 팝업창 오픈
		$$("popupWindow1").show();
		var selData = $$(gridId)._getRows();
		$$("smsSendGrid")._addRow(selData);
		/*// 사용자 추가 버튼 이벤트
		var srId   = PROCESS_META.SR_ID;
		var taskName = PROCESS_META.TASK_NAME;
		
		var param = {
			sr_id : srId
		  , task_name : taskName
		  , grid_id : gridId
		};
			
		nkia.ui.utils.window({
			id: "userSelectPop",
			type: 'attendees',
			url: '/itg/nbpm/common/searchProcessOperUserList.do',
			popupParamData: param,
			title: '사용자 선택 팝업',
			callFunc: createUserWindow
		});
		var selData = $$("attendees_grid")._getRows();
		$$("userSelectPop_subGrid")._addRow(selData);*/
	break;	
	case 'incidentServiceList' :   //서비스영향목록	
/*		var customerId = $$('requst_regist')._getFieldValue('customer_id');
		var customerNm = $$('requst_regist')._getFieldValue('customer_id_nm');*/
		
		/***************************************************************************
		 * 트리 영역 : [Tree]
		 **************************************************************************/	
		
		// 고객사정보 트리 영역
		var selCustTree = createTreeComponent({
			id : 'selCustTree',
			url : '/itg/itam/statistics/searchCustTree.do',
			select: true,
			width: 300,
			expColable: true, // 전체펼침/접기 표시 여부
			filterable: true, // 검색필드 표시 여부
			expandLevel: 2,
			header: {
				title: '고객사정보' //고객사정보
			},
			on: {
	        	onItemClick: custTreeNodeClick
	        }
		});
		
		/***************************************************************************
		 * 서비스 관리정보 영역 : [Tree]
		 **************************************************************************/			
		
		// 서비스관리정보 트리 영역
		var amdbServiceTree = createTreeComponent({
			id: 'amdbServiceTree',
			select: true,
			drag: true,
			width: 300,
			url: '/itg/itam/amdb/searchAmdbServiceAllTree.do',
			expandLevel: 2,
			checkbox: true, // 체크박스 트리 사용
			on: {
				onItemDblClick: treeNodeRequst
			},
			params: {
				search_cust_id: ' ' //첫 로드시 select 0개
			},
			expColable: true, // 전체펼침/접기 표시 여부
			header: {
				title: '서비스 선택', //서비스관리정보				
			}
		});
		
		var popupWindow = createWindow({
	    	id: "popupWindow",
	    	width: 800,
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: '서비스 영향목록', 
	    	},
	    	body: {
	    		rows: [{
					cols: [{
						rows: selCustTree
					},{
						rows: amdbServiceTree
					}]
	    		}]	    		
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: function() {
	    					treeNodeRequst();
	    				}})
	    			]
	    		}
	    	},
	    	closable: true
	    });
		
		// 팝업창 오픈
		$$("popupWindow").show();
		
		// 부서정보 트리 클릭 이벤트
		function custTreeNodeClick(id, e, node) {
			
			// 선택된 아이템의 node_id 가져오기
			var treeItem = this.getItem(id);
			var nodeId = treeItem.node_id;
			// 클릭된 트리 하이라이트
			$$('selCustTree').select(id);

			paramMap = {};
			paramMap['search_cust_id'] =nodeId;
			paramMap['expandLevel'] = '2';
			
			$$('amdbServiceTree')._reload(paramMap,true);
						
			return false;
		}
		
		// 서비스관리정보 트리 클릭 이벤트
		function treeNodeRequst() {
			///////////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////
			var customerTreeItem = $$('selCustTree')._getSelectedItem();
			var treeItem = $$("amdbServiceTree")._getCheckedItems();
			var valArray = [];
			
			if(!treeItem || treeItem.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '서비스를 선택해 주시기 바랍니다.'
				});
				return false;
			}
			
			for (var i in treeItem) {
				var treeCheck = true;
				
				treeItem[i].SERVICE_ID = treeItem[i].id;
				treeItem[i].SERVICE_NM = treeItem[i].text;
				treeItem[i].UP_NODE_NM = treeItem[i].up_node_nm;
				treeItem[i].CUSTOMER_ID = customerTreeItem.node_id;
				treeItem[i].CUSTOMER_NM = customerTreeItem.text;
				
				for(var j = 0 ; j < $$('incident_service_grid')._getRows().length ; j++){
					if(treeItem[i].id == $$('incident_service_grid')._getRows()[j].SERVICE_ID){
						treeCheck = false;
					}
				}
				
				if(treeCheck && treeItem[i].leaf){
					valArray.push(treeItem[i]);
				}
			}
			
			
			$$("incident_service_grid")._addRow(valArray);
			$$("incident_service_grid").adjustRowHeight();
			popupWindow.close();
					
		}
	break;
	case 'incidentEmsList' :   //관련장애이벤트
		var searchform = createFormComp({
			id: "searchform",
			header: {
				title: "검색(Critical인 경우만 조회 가능)",
				
			},
			
			fields: {
				colSize: 2, // 열 수
				items: [  //ALARM_SEVERITY,NODE_NAME,HOSTNAME,IPADDRESS
					 /*{ colspan: 1, item: createTextFieldComp({label: "알람등급", name: "alarmSeverity", resizable: true }) }
					,{ colspan: 1, item: createTextFieldComp({label: "장비명", name: "nodeName", resizable: true }) }*/
					 { colspan: 1, item: createTextFieldComp({label: "호스트명", name: "hostname", resizable: true }) }			
					,{ colspan: 1, item: createTextFieldComp({label: "IP주소", name: "ipaddress", resizable: true }) }			
				]
			},
			footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: '검색', id:'searchBtn', type:"form", click: function() {
		                	// 검색폼의 값들 추출
		        			var data = $$("searchform")._getValues();
		        			
		        			// 검색폼의 데이터를 인자값으로 그리드 리로드
		        			$$("emsList")._reload(data);
	    				}})
		            ]
		        }
		    }
		});
		
		var gridProps = {
				id: "emsList",
				keys: ['ID'],
				resizeColumn: true,
				checkbox: true,
				pageable: true,
				pageSize: 30,
				url: "/itg/customize/ubit/searchIncidentEmsList.do",
				resource: "grid.process.incident.ems",
				params: {check_enable: 'Y'},
				header: {
					title: "장애목록"
				},
				dependency : { // 의존관계 그리드 사용
					targetGridId: "incident_ems_grid" // 대상 그리드 id
				}
			};
			
			var grid = createGridComp(gridProps);
			
		var popupWindow = createWindow({
	    	id: "popupWindow",
	    	width: 800,
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: "관련장애 이벤트"
	    	},
	    	body: {
	    		rows: new Array().concat(searchform,grid)
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: function() {
	    					if($$('emsList')._getSelectedRows().length == 0){
	    						nkia.ui.utils.notification({
	    					        type: 'error',
	    					        message: '관련장애를 선택해주세요.'
	    					    });
	    						return false;
	    					}
	    					$$('incident_ems_grid')._addRow($$('emsList')._getSelectedRows());
	    					popupWindow.close();
	    				}})
	    			]
	    		}
	    	},
	    	closable: true
	    });
		
		// 팝업창 오픈
		$$("popupWindow").show();
	break;	
	case 'changeAlarm' :   //알림대상자설정			
		var searchUserForm = createFormComp({
			id: 'searchUserForm',
			elementsConfig:{
				labelPosition: "left"
			},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.common.search'})
			  , icon: "search"		
			},
			footer: {
				buttons: {
					align: "center",
					items:[
					       createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.search'}), id: 'serachUserBtn', type: 'form', click: function() {
					    		var data = $$("searchUserForm")._getValues();
					    		$$("smsUserGrid")._reload(data);
		    				}})
					     , createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.reset'}), id: 'searchUserInitBtn', click: function() {
					    		$$("searchUserForm")._reset();
		    				}}) 
					],
					css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: [
				      {colspan: 2, item: createUnionFieldComp({
	                  items: [
	                          	createCodeComboBoxComp({label: '조회구분', name: 'search_type', code_grp_id: 'SEARCH_SMS_USER', attachChoice: true, width: 250})
	                            , createTextFieldComp({name: "search_value", placeholder: '검색어를 입력해주세요.', on: {onKeyPress:"userPressEnterKey"}})
	                      ]
	                  })
					}
				]
			}
		});
		
		var userGridId = 'smsUserGrid';
		var userGrid = createGridComp({
			id: userGridId,
			keys: ["USER_ID"],
			resizeColumn: true,
			pageable: true,
			pageSize: 10,
			badgeId: userGridId+"_badge",
			url: '/itg/workflow/smsGrp/searchSmsUserList.do',
			resource: 'grid.workflow.smsUser',
			params: {},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.title.smsUserList'})
			},
			on:{
				onItemDblClick: userGridDbclick
			},
			dependency : { // 의존관계 그리드 사용
				targetGridId: "smsSendGrid" // 대상 그리드 id
			}

		});
				
		
		// 그룹목록
		var searchGrpForm = createFormComp({
			id: 'searchGrpForm',
			elementsConfig:{
				labelPosition: "left"
			},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.common.search'})
			  , icon: "search"		
			},
			footer: {
				buttons: {
					align: "center",
					items:[
					       createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.search'}), id: 'serachGrpBtn', type: 'form', click: function() {
					    		var data = $$("searchUserForm")._getValues();
					    		$$("smsUserGrid")._reload(data);
		    				}})
					     , createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.reset'}), id: 'searchGrpInitBtn', click: function() {
					    		$$("searchGrpForm")._reset();
		    				}}) 
					],
					css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: [
				      {colspan: 2, item: createUnionFieldComp({
	                  items: [
	                          	createCodeComboBoxComp({label: '조회구분', name: 'search_type', code_grp_id: 'SEARCH_SMS_GRP', attachChoice: true, width: 250})
	                            , createTextFieldComp({name: "search_value", placeholder: '검색어를 입력해주세요.', on: {onKeyPress:"grpPressEnterKey"}})
	                      ]
	                  })
					}
				]
			}
		});

	var grpGridId = 'smsGrpGrid';
	var grpGrid = createGridComp({
		id: grpGridId,
		keys: ['USER_ID'],
		resizeColumn: true,
		pageable: true,
		pageSize: 10,		
		badgeId: grpGridId+"_badge",
		url: '/itg/workflow/smsGrp/searchSmsGrpList.do',
		resource: 'grid.workflow.smsGrp',
		params: {upSmsGrpId:'0000013'},
		header: {
			title: getConstText({isArgs : true, m_key : 'res.title.smsGroupList'})

		},
		on:{
			onItemDblClick: grpGridDbclick
		}
	});
		
		// 탭 bar 를 구성합니다. (탭 상단 구성)
		var tabBar = [{
			// 첫번째 탭 정보
			id: grpGridId + "_tab", // 개별 탭의 id
			value: "그룹목록" + "<span id='" + (grpGridId + "_badge") +"' class='webix_badge'>0</span>" // 탭 헤더명 (그리드에서 구성한 badge_id로 badge 구성)
			
		},{
			// 두번째 탭 정보
			id: userGridId + "_tab", // 개별 탭의 id
			value: "사용자목록" + "<span id='" + (userGridId + "_badge") +"' class='webix_badge'>0</span>" // 탭 헤더명 (그리드에서 구성한 badge_id로 badge 구성)
		}];
		
		// 탭의 body 를 구성합니다.
		var tabItem = [{
			// 첫번째 탭 body
			id: grpGridId + "_tab", // 개별 탭의 id, tabBar에서 부여한 id와 동일하게 세팅하여야 합니다.
			rows: new Array().concat(searchGrpForm, grpGrid) // 실제 배치할 객체를 넣어줍니다.
		},{
			// 두번째 탭 body
			id: userGridId + "_tab", // 개별 탭의 id, tabBar에서 부여한 id와 동일하게 세팅하여야 합니다.
			rows: new Array().concat(searchUserForm, userGrid) // 실제 배치할 객체를 넣어줍니다.

		}];
		
		// 실제 탭을 구성합니다.
		var tabGrid = {
			view:"tabview", // view 형식 : tabview로 입력
			id:"smsTab", // 탭의 ID
			animate:false,
			tabbar:{
				options: tabBar // tabbar 배치
			},
			cells:new Array().concat(tabItem) // tab body 배치
		};

		var smsSendGrid = createGridComp({
			id: 'smsSendGrid',
			keys: ['HP_NO'],
			resizeColumn: true,
			pageable: false,
			height : 200,
			checkbox: true,
			autoLoad: false,
			sortConfig : 'string',
			url: '/itg/workflow/smsGrp/searchSmsSendList.do',
			resource: 'grid.workflow.smsSend',
			params: {},
			header: {
				title: getConstText({isArgs : true, m_key : 'res.title.smsToNumList'}),
	        	buttons: {
	       		 items: [
	       		        createBtnComp({label: '전체삭제', id:'userGrpAllDeleteBtn',  click:function(){
	       		        	ALL_GRP_NM = "";
	       		        	$$("smsSendGrid")._removeAllRow();
	       		        }})
	   	            ]
	        	}
			},
			// 액션 컬럼에 대한 정보를 세팅합니다. 액션 컬럼은 특정한 Action을 일으킬 수 있는 역할을 하는 컬럼을 의미합니다.
			// 행을 삭제하는 Action을 수행할 버튼을 배치합니다.
			actionColumns: [{
				column: "DEL_ACTION", // 액션 컬럼의 헤더명 (그리드 리소스)
				template: nkia.ui.html.icon("remove", "remove_row"), // 버튼 아이콘("아이콘 명", "생성할 CSS명(onClick 이벤트 명과 매칭되어야 함)")
				onClick: { // 이벤트 연결(실행 할 이벤트명 매칭)
					"remove_row": removeSmsSendData
				}
			}]
		});
		
		var popupWindow1 = createWindow({
	    	id: "popupWindow1",
	    	width: nkia.ui.utils.getWidePopupWidth(),
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: "알림대상자설정"
	    	},
			body: {
				rows: [{
					cols:[{
						rows: new Array().concat(tabGrid, smsSendGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: function() {
	    					$$(gridId)._removeAllRow();
	    					$$(gridId)._addRow($$("smsSendGrid")._getRows());
	    					
	    					popupWindow1.close();
	    				}})
	    			]
	    		}
	    	},
	    	closable: true
	    });
		
		// 팝업창 오픈
		$$("popupWindow1").show();
		var selData = $$(gridId)._getRows();
		$$("smsSendGrid")._addRow(selData);
	break;	
	case "ci":
		ciAddBtnFunc("setSubTabCiAssetGrid");
	break;	
	case "service":
		serviceAddBtnFunc("setSubTabServGrid");
	case "grid_test":
		var editform = createFormComp({
			id: "editform",
			header: {
				title: "입력폼",
				
			},
			
			fields: {
				colSize: 1, // 열 수
				items: [
					{ colspan: 1, item: createTextAreaFieldComp({label: "텍스트박스1", name: "textarea1", resizable: true, height: 100 }) }
					,{ colspan: 1, item: createTextAreaFieldComp({label: "텍스트박스2", name: "textarea2", resizable: true, height: 100 }) }
					,{ colspan: 1, item: createTextAreaFieldComp({label: "텍스트박스3", name: "textarea3", resizable: true, height: 100 }) }
					,{ colspan: 1, item: createTextAreaFieldComp({label: "텍스트박스4", name: "textarea4", resizable: true, height: 100 }) }
				]
			}
		});
		
		var popupWindow = createWindow({
	    	id: "popupWindow",
	    	width: 800,
	    	height: 650,
	    	header: {
	    		title: "팝업 - 사용자선택"
	    	},
	    	body: {
	    		rows: new Array().concat(editform)
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: function() {
//	    					var clickRecord = $$("popupGrid")._getSelectedRows();
//	    					// _getSelectedRows()는 그리드의 선택된 값을 리턴하는 함수로, 항상 배열로 리턴됩니다.
	    					var tempCust = {};
	    					var valArr = [];
	    					tempCust["WORK_CONTENT"] = $$("editform")._getFieldValue("textarea1");
	    					tempCust["REMARKS"] = $$("editform")._getFieldValue("textarea2");
	    					
	    					valArr[0] = tempCust;
	    					$$("work_step")._addRow(valArr);
//	    					$$("editform")._setValues(values);
	    					$$("work_step").adjustRowHeight();
	    					popupWindow.close();
	    				}})
	    			]
	    		}
	    	},
	    	closable: true
	    });
		
		// 팝업창 오픈
		$$("popupWindow").show();
	break;	

	
	case "zoneInsert":
	  	var gridArray = new Array();
	  	var tempMap = {};
		tempMap['WORK_GUBUN'] = "INSERT";
		//tempMap['MGMT_PLACE'] = "ZONE_EXTERNAL";	  

		gridArray.push(tempMap);
		$$(gridId)._addRow(gridArray);
			
		


		break;
		
	case 'recordSerch' :
		//레코드 조회
		
		var grid = $$(gridId);
		var params = grid.config.params;
		var roleId = "";
		if(params.roleId){
			roleId = params.roleId;
		}else{
			roleId = params.oper_type;
		}
		
		var params = {};
		if (roleId != "servicedesk") {
			params["auth_id"] = PROCESS_META.PROCESS_TYPE + "_" + roleId;
		} else {
			params["auth_id"] = roleId;
		}
		
		var taskName = PROCESS_META.TASK_NAME.toUpperCase();
		if(taskName == "START"){
			taskName = "REQUST_REGIST";
		}
		
		params["task_name"]       = taskName;			
		params["oper_type"]       = roleId;
		params["process_type"]    = PROCESS_META.REQ_TYPE; 
		params["sr_id"]           = PROCESS_META.SR_ID; 
		params["event_name"]      = grid.config.params.event_name; 
		params["processid"]       = PROCESS_META.PROCESSID; 
		params["table_name"]      = grid.config.params.table_name;
		params["nbpm_table_name"] = grid.config.params.table_name;
		
		nkia.ui.utils.window({
			id: "recordWindow",
			title: "레코드 조회",
			width : 1000,
			type: "recordSerch",
			//url: "/itg/system/user/searchUserList.do",
			url: "/itg/itam/dns/searchDnsRecordList.do",
			params: params,
			reference: {
				grid: $$(gridId),
				data: $$(gridId)._getRows()
			},
			callFunc: createUserWindow
		});
	break;
	case 'recordSerchDelete' :
		//레코드 조회
		
		var grid = $$(gridId);
		var params = grid.config.params;
		var roleId = "";
		if(params.roleId){
			roleId = params.roleId;
		}else{
			roleId = params.oper_type;
		}
		
		var params = {};
		if (roleId != "servicedesk") {
			params["auth_id"] = PROCESS_META.PROCESS_TYPE + "_" + roleId;
		} else {
			params["auth_id"] = roleId;
		}
		
		var taskName = PROCESS_META.TASK_NAME.toUpperCase();
		if(taskName == "START"){
			taskName = "REQUST_REGIST";
		}
		
		params["task_name"]       = taskName;			
		params["oper_type"]       = roleId;
		params["process_type"]    = PROCESS_META.REQ_TYPE; 
		params["sr_id"]           = PROCESS_META.SR_ID; 
		params["event_name"]      = grid.config.params.event_name; 
		params["processid"]       = PROCESS_META.PROCESSID; 
		params["table_name"]      = grid.config.params.table_name;
		params["nbpm_table_name"] = grid.config.params.table_name;
		
		nkia.ui.utils.window({
			id: "recordWindow",
			title: "레코드 조회",
			width : 1000,
			type: "recordSerchDelete",
			//url: "/itg/system/user/searchUserList.do",
			url: "/itg/itam/dns/searchDnsRecordList.do",
			params: params,
			reference: {
				grid: $$(gridId),
				data: $$(gridId)._getRows()
			},
			callFunc: createUserWindow
		});
	break;
	case 'recordInsert' :
		//레코드 등록 그리드
		if($$("requst_regist")._getFieldValue("am_zone_id") != ""){
			
  		  var gridArray = new Array();
		  var tempMap = {};
		  tempMap['GUBUN_NM'] = "INSERT";
		  tempMap['RECORD_TYPE_CD'] = "A_RECORD";
		  tempMap['AM_ZONE_ID'] = $$("requst_regist")._getFieldValue("am_zone_id");

		  gridArray.push(tempMap);
		  $$(gridId)._addRow(gridArray);
		}else{
			nkia.ui.utils.notification({
		        type: 'error',
		        message: 'Zone을 선택해 주세요'
		    });
		}
		
	break;
	case "zoneInsert_pop":
		
		var editform = createFormComp({
			id: "editform",
			
			fields: {
				colSize: 2, // 열 수
				items: [
					{ colspan: 1, item: createTextFieldComp({label: "구분", name: "work_gubun", resizable: true, height: 100 }) }
					,{ colspan: 1, item: createCodeComboBoxComp({label:"관리구분",name:"mgmt_place", code_grp_id:"MGMT_PLACE", attachChoice:true})}
					,{ colspan: 2, item: createTextFieldComp({label: "도메인영역", name: "zone_id_nm", resizable: true, height: 100 }) }
					,{ colspan: 1, item: createTextFieldComp({label: "구매사이트", name: "buy_site_cn", resizable: true, height: 100 }) }
					,{ colspan: 1, item: createTextFieldComp({label: "구매사이트웹주", name: "buy_site_url_cn", resizable: true, height: 100 }) }
					,{ colspan :1, item: createDateFieldComp({label: "구매일", name :"buy_dt", dateType:"D"/*, dateValue:createDateTimeStamp('D',{'year':0,'month':0,'day':0,'hour':0,'min':0})*/})}
					,{ colspan :1, item: createDateFieldComp({label: "만료일", name :"expire_dt", dateType:"D"})}
					,{ colspan: 1, item: createTextFieldComp({label: "구매담당자", name: "buy_user", resizable: true, height: 100 }) }
					,{ colspan: 1, item: createTextFieldComp({label: "관리담당자", name: "mgmt_user", resizable: true, height: 100 }) }
				]
			}
		});
		
		var popupWindow = createWindow({
	    	id: "popupWindow",
	    	width: 800,
	    	height: 650,
	    	header: {
	    		title: "도메인목록"
	    	},
	    	body: {
	    		rows: new Array().concat(editform)
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: function() {
//	    					var clickRecord = $$("popupGrid")._getSelectedRows();
//	    					// _getSelectedRows()는 그리드의 선택된 값을 리턴하는 함수로, 항상 배열로 리턴됩니다.
	    					var formData = {};
	    					var valArr = [];
	    					formData["WORK_GUBUN"] = $$("editform")._getFieldValue("work_gubun");
	    					formData["MGMT_PLACE"] = $$("editform")._getFieldValue("mgmt_place");
	    					formData["ZONE_ID_NM"] = $$("editform")._getFieldValue("zone_id_nm");
	    					formData["BUY_SITE_CN"] = $$("editform")._getFieldValue("buy_site_cn");
	    					formData["BUY_SITE_URL_CN"] = $$("editform")._getFieldValue("buy_site_url_cn");
	    					formData["BUY_DT"] = $$("editform")._getFieldValue("buy_dt");
	    					formData["EXPIRE_DT"] = $$("editform")._getFieldValue("expire_dt");
	    					formData["BUY_USER"] = $$("editform")._getFieldValue("buy_user");
	    					formData["MGMT_USER"] = $$("editform")._getFieldValue("mgmt_user");
	    					
	    					valArr[0] = formData;
	    					$$("zone_grid")._addRow(valArr);
//	    					$$("editform")._setValues(values);
	    					$$("zone_grid").adjustRowHeight();
	    					$$("editform")._reset();
	    					//popupWindow.close();
	    				}})
	    			]
	    		}
	    	},
	    	closable: true
	    });
		
		// 팝업창 오픈
		$$("popupWindow").show();
		break;
	case 'lowformatlist':
		// // 멀티태스크 사용자 추가 버튼 이벤트

		var grid = $$(gridId);
		var params = grid.config.params;
		var roleId = "";
		if (params.roleId) {
			roleId = params.roleId;
		} else {
			roleId = params.oper_type;
		}

		// 권한 추가(임시)
		// 'roleId': 'ROLE_OPETR_06'
		if (PROCESS_META.REQ_TYPE == 'WORK') {
			roleId = 'ROLE_OPETR_06';
		} else if (PROCESS_META.REQ_TYPE == 'DISUSE') {
			roleId = 'ROLE_OPETR_07';
		}

		var params = {};
		if (roleId != "servicedesk") {
			params["auth_id"] = PROCESS_META.PROCESS_TYPE + "_" + roleId;
		} else {
			params["auth_id"] = roleId;
		}

		var taskName = PROCESS_META.TASK_NAME.toUpperCase();
		if (taskName == "START") {
			taskName = "REQUST_REGIST";
		}

 
		
		params["task_name"] = taskName;
		params["oper_type"] = roleId;
		params["process_type"] = PROCESS_META.REQ_TYPE;
		params["sr_id"] = PROCESS_META.SR_ID;
		params["event_name"] = grid.config.params.event_name;
		params["processid"] = PROCESS_META.PROCESSID;
		params["table_name"] = grid.config.params.table_name;
		params["nbpm_table_name"] = grid.config.params.table_name;

		nkia.ui.utils.window({
			id : "userWindow",
			title : "담당자 조회",
			width : 1000,
			type : "lowformatlist",
			// url: "/itg/system/user/searchUserList.do",
			url : "/itg/itam/lowformat/searchHostInfo.do",
			params : params,
			reference : {
				grid : $$(gridId),
				data : $$(gridId)._getRows()
			},
			callFunc : createUserWindow
		});
		break;
	case 'centerWork':
		var gridArray = new Array();	
		
		var setGridDataStrDt = $$('requst_regist')._getFieldValue('work_str_dt').substring(0,10);
		var setGridDataStrTm = $$('requst_regist')._getFieldValue('work_str_dt').substring(10,12) + ":" + $$('requst_regist')._getFieldValue('work_str_dt').substring(12,14);
		var setGridDataEndDt = $$('requst_regist')._getFieldValue('work_end_dt').substring(0,10);
		var setGridDataEndTm = $$('requst_regist')._getFieldValue('work_end_dt').substring(10,12) + ":" + $$('requst_regist')._getFieldValue('work_end_dt').substring(12,14);
		var param = {};
		param['WORK_STR_DT_DAY'] = setGridDataStrDt;
		param['WORK_STR_TIME'] = setGridDataStrTm;
		param['WORK_END_DT_DAY'] = setGridDataEndDt;
		param['WORK_END_TIME'] = setGridDataEndTm;
		gridArray.push(param);
		
		$$('center_work_grid')._addRow(gridArray);
	break;
	case 'centermove':
		// // 멀티태스크 사용자 추가 버튼 이벤트
		//var loc_info = $$('requst_regist')._getFieldValue('cenmove_center');
			
		var loc_info = $$('requst_regist')._getFieldValue('center_id');
		if(loc_info != ""){			
			var grid = $$(gridId);
			var params = grid.config.params;
			var roleId = "";
			if (params.roleId) {
				roleId = params.roleId;
			} else {
				roleId = params.oper_type;
			}
	
			// 권한 추가(임시)
			// 'roleId': 'ROLE_OPETR_06'
			if (PROCESS_META.REQ_TYPE == 'WORK') {
				roleId = 'ROLE_OPETR_06';
			} else if (PROCESS_META.REQ_TYPE == 'DISUSE') {
				roleId = 'ROLE_OPETR_07';
			}
	
			var params = {};
			if (roleId != "servicedesk") {
				params["auth_id"] = PROCESS_META.PROCESS_TYPE + "_" + roleId;
			} else {
				params["auth_id"] = roleId;
			}
	
			var taskName = PROCESS_META.TASK_NAME.toUpperCase();
			if (taskName == "START") {
				taskName = "REQUST_REGIST";
			}
	
	 
			
			params["task_name"] = taskName;
			params["oper_type"] = roleId;
			params["process_type"] = PROCESS_META.REQ_TYPE;
			params["sr_id"] = PROCESS_META.SR_ID;
			params["event_name"] = grid.config.params.event_name;
			params["processid"] = PROCESS_META.PROCESSID;
			params["table_name"] = grid.config.params.table_name;
			params["nbpm_table_name"] = grid.config.params.table_name;
			
			nkia.ui.utils.window({
				id : "userWindow",
				title : "담당자 조회",
				width : 1000,
				type : "centermove",
				url : "/itg/itam/serviceCenterMove/serviceCenterMoveList.do",
				params : params,
				reference : {
					grid : $$(gridId),
					data : $$(gridId)._getRows()
				},
				callFunc : createUserWindow
			});			
		}else{
			nkia.ui.utils.notification({
		        type: 'error',
		        message: '센터의 값을 선택해주세요.'
		    });
		}
		break;	
	}
}
// 프로세스 그리드 필드 삭제 버튼 이벤트
function clickProcessGridDeleteBtn(gridId, popUpType){

	switch(popUpType){
	case 'multiTask' :
		// 멀티태스크 사용자 삭제 버튼 이벤트
		var grid = $$(gridId);
		var params = grid.config.params;
		
		var roleId = "";
		if(params.roleId){
			roleId = params.roleId;
		}else{
			roleId = params.oper_type;
		}
	
		var items = grid._getSelectedRows();
		if(items.length > 0){
			if (confirm("선택한 승인자를 삭제합니다.\n계속 하시려면 확인을 눌러주세요")) {
				items.forEach(function(item){
				if(item.USER_ID && item.USER_ID.toString() != ""){
					
					var taskName = PROCESS_META.TASK_NAME.toUpperCase();
					if(taskName == "START"){
						taskName = "REQUST_REGIST";
					}
					
					var params = {
						sr_id: PROCESS_META.SR_ID,
						event_name: grid.config.params.event_name,
						oper_type: roleId,
						task_name: taskName, 
						processid: PROCESS_META.PROCESSID, 
						nbpm_table_name: grid.config.params.table_name,
						worker_user_id: item.USER_ID
					};
								
					nkia.ui.utils.ajax({
				        url: "/itg/nbpm/deleteMultiTempInfo.do",
						params: params,
						isMask: false,
						async: false,
				        success: function(response){
				        	grid._removeRow(item.id);
				        }
					});	
				}else{
					grid._removeRow(item.id);
				}
				});
			}
		}else{
			nkia.ui.utils.notification({
		        type: 'error',
		        message: '선택된 정보가 없습니다.'
		    });
		}
	break;
	case "noti_users":   // 사전공지대상(그룹)
	case "requst_users": // 이슈발생시 공지대상
	case "recept_users": // 수신대상(그룹)
	case "access_users": // 접근통제해제 작업자
	case "cab_users":    // CAB소집대상	
	case 'operUserGrid' :   //(협조)작업자	
		// 사용자 삭제 버튼 이벤트
		var srId   = PROCESS_META.SR_ID;
		var grid = $$(gridId); 
		
		var items = $$(gridId)._getSelectedRows();
		if(items.length > 0){
			if (confirm("선택한 작업자를 삭제합니다.\n계속 하시려면 확인을 눌러주세요")) {
				items.forEach(function(item){
					if(item.USER_ID && item.USER_ID.toString() != ""){
						var param = {
							sr_id : srId
						  , grid_id : gridId
						  , delete_user_id : item.USER_ID
						};
									
						nkia.ui.utils.ajax({
					        url: "/itg/customize/lgup/deleteProcUser.do",
							params: param,
							isMask: false,
							async: false,
					        success: function(response){
					        	var searchMap = {};
								searchMap['sr_id'] = srId;
								searchMap['selectQuerykey'] = "selectPROC_USER_GRID";
								searchMap['grid_id'] = gridId;
								
								$$(gridId)._reload(searchMap);
					        }
						});	
					}else{
						$$(gridId)._removeRow(item.id);
					}
				});
			}
		}else{
			nkia.ui.utils.notification({
		        type: 'error',
		        message: '선택된 정보가 없습니다.'
		    });
		}
	break;
	case "attendees" :
		$$("attendees_grid")._removeRow();
	break;	
	case "incidentEmsList" :
		$$("incident_ems_grid")._removeRow();
	break;	
	case "incidentServiceList" :
		$$("incident_service_grid")._removeRow();
	break;	
	case "ci" :
		$$("setSubTabCiAssetGrid")._removeRow();
	break;	
	case "service":
		$$("setSubTabServGrid")._removeRow();
	break;	

	case 'recordSerch' :
		var grid = $$(gridId);
		var params = grid.config.params;
		
		var roleId = "";
		if(params.roleId){
			roleId = params.roleId;
		}else{
			roleId = params.oper_type;
		}
	
		var items = grid._getSelectedRows();
		if(items.length > 0){
				items.forEach(function(item){
					grid._removeRow(item.id);
				});
		}else{
			nkia.ui.utils.notification({
		        type: 'error',
		        message: '선택된 정보가 없습니다.'
		    });
		}
	break;
	case 'recordSerchDelete' :
		var grid = $$(gridId);
		var params = grid.config.params;
		
		var roleId = "";
		if(params.roleId){
			roleId = params.roleId;
		}else{
			roleId = params.oper_type;
		}
	
		var items = grid._getSelectedRows();
		if(items.length > 0){
				items.forEach(function(item){
					grid._removeRow(item.id);
				});
		}else{
			nkia.ui.utils.notification({
		        type: 'error',
		        message: '선택된 정보가 없습니다.'
		    });
		}
	break;
	case 'recordInsert' :
		$$(gridId)._removeRow();
	break;
	
	case "zoneInsert":

		$$(gridId)._removeRow();
			

		break;
		
	case "zoneSelect":
		$$(gridId)._removeRow();

		break;	

	case "lowformatlist":
		$$(gridId)._removeRow();

		break;			
	case "centerWork":
		$$("center_work_grid")._removeRow();
	break;	

	case "centermove":
		$$(gridId)._removeRow();

		break;	
		
	case "changeAlarm":
		$$(gridId)._removeRow();

	break;	
	}
}

// UBIT 커스텀 - 변경요청 알림
function clickProcessGridChangeAlarmBtn(gridId, popUpType){
	if($$(gridId)._getRows().length == 0){
		nkia.ui.utils.notification({type:'error', message:"알림대상자를 설정해 주세요."});
		return false;
	}
	if(!confirm("대상자에게 알림을 보내시겠습니까?")){
		return false;
	}
	
	//var userList = $$(gridId)._getSelectedRows();
	var userList = $$(gridId)._getRows();
	
	switch(popUpType){	
	case 'workStart' :   //알림대상자설정 UBIT 커스텀
	case 'workEnd' :   //알림대상자설정 UBIT 커스텀
	case 'workCancel' :   //알림대상자설정 UBIT 커스텀
		nkia.ui.utils.ajax({
	        url: "/itg/customize/ubit/workStartAlarm.do",
	        params : {
	        			sr_id:PROCESS_META.SR_ID,
	        			user_list:userList,
	        			popUpType:popUpType,
	        			workStrDt:$$('change_result')._getValues().work_str_dt,
	        			workEndDt:$$('change_result')._getValues().work_end_dt
	        		},
			isMask: false,
			async: false,
	        success: function(response){
	        	nkia.ui.utils.notification({
					message: response.resultMap.RESULT_MSG
				});
	        }
		});	
	break;	

	}
}

//UBIT 커스텀 - 센터출입요청 출/퇴실처리
function clickProcessGridCenterInOutBtn(gridId, popUpType){
	var rows =  $$(gridId)._getSelectedRows();
	var params = {};
	
	if(rows.length == 0){
		nkia.ui.utils.notification({type:'error', message:"출입자정보를  선택해 주세요."});
		return false;
	}	
			
	switch(popUpType){	
	case 'in' :   //알림대상자설정 UBIT 커스텀
		for(var i=0; i<rows.length; i++){
			if($$('requst_regist')._getValues().center_id=='D1' && rows[i].CARD_NUMBER == ""){
				nkia.ui.utils.notification({type:'error', message:"교환카드번호를 확인해주세요."});
				return false;
			}
			
			if(rows[i].IN_TIME != ""){
				nkia.ui.utils.notification({type:'error', message:"이미 출입신청된 사용자가 존재합니다."});
				return false;
			}		
		}
		
		if(!confirm("출입 정보를 저장하시겠습니까?")){
			return false;
		}
		
		params["ROWS"] = rows;
		
		nkia.ui.utils.ajax({
	        url: "/itg/customize/ubit/centerGuestInUpdate.do",
	        params : params,
			isMask: false,
			async: false,
	        success: function(response){
	        	nkia.ui.utils.notification({
					message: '출입정보가 저장되었습니다.'
				});
	        	$$(gridId)._reload();
	        }
		});	
	break;
	
	case 'out' :   //알림대상자설정 UBIT 커스텀
		for(var i=0; i<rows.length; i++){
			if(rows[i].OUT_TIME != ""){
				nkia.ui.utils.notification({type:'error', message:"이미 퇴실신청된 사용자가 존재합니다."});
				return false;
			}		
			
			if(rows[i].IN_TIME == ""){
				nkia.ui.utils.notification({type:'error', message:"출입하지 않은 사용자입니다."});
				return false;
			}
		}
		
		if(!confirm("퇴실 정보를 저장하시겠습니까?")){
			return false;
		}
		params["ROWS"] = rows;
		
		nkia.ui.utils.ajax({
	        url: "/itg/customize/ubit/centerGuestOutUpdate.do",
	        params : params,
			isMask: false,
			async: false,
	        success: function(response){
	        	nkia.ui.utils.notification({
	        		message: '퇴실정보가 저장되었습니다.'
				});
	        	$$(gridId)._reload();
	        }
		});	
	break;	

	}
}

/**
 * 프로세스 그리드 필드(폼안에 그리드를 배치하기 위한 필드) 
 * @method {} createProcessGridFieldComp
 * @property {string} id			
 */
function createProcessGridFieldComp(compProperty){
	var props = compProperty||{};
	var comp = {};
	try {
		comp = new nkia.nbpm.fields.processGridField();
		
		comp.setProps(props);
		comp.setRender(props);
		
	} catch(e) {
		alert(e.message);
	}
	return {rows : comp.rows};
}


//인시던트관리 - KEDB 조회 필드 추가
function createKedbSearchField(props){
	var params = {};
	var _this = this;

  var object = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						window.open("/itg/workflow/kedb/kedbSearchManager.do?view_type=popView", "KedbPopup",
								"width=1300,height=700,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var values = {};
						values[_this.name] = "";
						$$(_this.formId)._setValues(values);
					}
				})
			]
		});
  
	return object;
}


//인시던트관리 - KEDB 편집 필드 추가
function createKedbEditPopupField(props){
	var params = {};
	var _this = this;

var object = createUnionFieldComp({
			items: [
				createSearchFieldComp({
					label: props.label,
					name: props.name,
					required: props.required,
					helper: props.helper,
					helperWidth: props.helperWidth,
					helperHeight: props.helperHeight,
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						window.open("/itg/workflow/kedb/kedbManager.do?view_type=popView", "KedbPopup",
								"width=1100,height=700,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
					}
				}),
				createBtnComp({ 
					label:"초기화", 
					isProcAfterHide: props.isProcAfterHide,
					click: function(){
						var values = {};
						values[_this.name] = "";
						$$(_this.formId)._setValues(values);
					}
				})
			]
		});

	return object;
}

//이미지 필드 추가
nkia.nbpm.fields.Image = function() {
	this.rows = [];
	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	}
	this.setRender = function(props) {

		var fontawesomeIcon = "";
		//var mousePointer = " style='cursor:pointer'"; // 나중에 이미지에 link 기능을 넣을
		var mousePointer = "";
		var icon = props.value;// gui에서 입력한 value
		var imgTmp = new Image();
		var imgUrl = "";
		var width;
		var height;
		switch (icon) {
		case "baseInfo_icon":
			mousePointer = "";
			fontawesomeIcon = "custom_base_icon";
			imgUrl = '/itg/base/css/webix/custom_icon/processBaseInfo_new_b.png';
			width = 2000;
			height = 40;
			break;
		case "detailInfo_icon":
			mousePointer = "";
			fontawesomeIcon = "custom_detail_icon";
			imgUrl = '/itg/base/css/webix/custom_icon/processDetailInfo_new_b.png';
			width = 2000;
			height = 40;
			break;
		default:
		}
		imgTmp.src = imgUrl;
		var imageHtml = "<div style='width:100%; height:100%; display:table;'>"
				+ "<div style='width:100%; height:100%; display:table-cell; vertical-align:left; text-align:left;'>"
				+ "<span style = 'width:100%'" + mousePointer + " class='label_info_comp "
				+ fontawesomeIcon + "'>"
				+ "<img src= '"+imgUrl+"' name='"+fontawesomeIcon+"' width='"+width+"px' height='"+height+"px' border=0 />" 
				+ "</span>"
				+ "</div>"
				+ "</div>";

		var cols = [ {
			view : "label",
			css : "label_template",
			label : imageHtml,
			height : height,
		} ];
		this.rows.push({
			cols : cols
		});
	}
};

/**
 * @method {} createImageComp : Form Function
 * @property {string} id: Form Id
 * @property {object} header : Form Header
 * @property {object} fields : Form Fields
 * @property {object} footer : Form Footer
 * @returns {object} form
 */

function createImageComp(imageProps) {
	var props = imageProps || {};
	var image;
	try {
		image = new nkia.nbpm.fields.Image();
		image.setProps(props);
		image.setRender(props);
	} catch (e) {
		alert(e.message);
	}
	return image;
}

function userGridDbclick(id, e, node){
	// 체크 된 데이터 추출
	var data = $$("smsUserGrid")._getSelectedRows();
	var sendGridItems = $$('smsSendGrid')._getRows();
	var userId = data[0].USER_ID;
	var hpNo = data[0].HP_NO; 
	
	// 핸드폰 번호가 없는 사용자를 추가할지 질문 합니다.
	if(typeof hpNo == 'undefined' || "" == hpNo){
		//if(!confirm("선택한 사용자는 핸드폰 번호가 없습니다. 진행하시겠습니까?")){
		if(!confirm(getConstText({isArgs: true, m_key: 'msg.sms.confirm.emptyPhone'}))){
			return false;
		}
		data[0].HP_NO = "-";
	}
	
	if(sendGridItems.length != 0 && typeof sendGridItems[0].USER_ID != 'undefined'){
		for ( var j = 0; j < sendGridItems.length; j++) {
			var sendUserId = sendGridItems[j].USER_ID;
			if(userId == sendUserId){
				//showMessage("발송대상자에 이미 포함되어있습니다.");
				nkia.ui.utils.notification({
					message: getConstText({isArgs: true, m_key: 'msg.sms.userDuplicate'})
				});
				return false;
			}
		}
	}
	// 체크된 데이터를 대상 그리드에 add 합니다.
	$$("smsSendGrid")._addRow(data);
	
	// add 된 데이터는 선택하지 못하도록 하기 위해 그리드를 다시 로드합니다.
	// 단, 현재 포커스 되어 있는 페이지를 유지합니다.
	$$("smsUserGrid")._reload({}, true); // 두 번째 인자값 : 페이지 유지 여부
}

function grpGridDbclick(id, e, node){
	
	var paramMap = {};
	var data = $$("smsGrpGrid").getItem(id.row);
	
	var smsGrpId = data.SMS_GRP_ID;
	var msg = data.DEFAULT_MSG;
	
	paramMap["SMS_GRP_ID"] = smsGrpId;
	
	nkia.ui.utils.ajax({
		url: '/itg/workflow/smsGrp/searchSmsGrpUserList.do'
	  , params: paramMap
	  , success: function(response){
		  var resultList = response.gridVO.rows;
		  
		  // 발송 대상자에 포함되어있는지 확인 합니다.
		  var sendGridItems = $$('smsSendGrid')._getRows();
		  var smsGridLength = sendGridItems.length;	
		  var gridArray = new Array();	
		  
		  // 그룹안의 사용자 정보 등록 
		  for ( var i = 0; i < resultList.length; i++) {
			// 등록여부 확인 파라미터
			var insertYn = true;
			var tempMap = {};
			
			// 선택한 사용자 정보 
			var userId = resultList[i].USER_ID; 
			var userNm = resultList[i].USER_NM; 
			var hpNo = resultList[i].HP_NO;
			var smsGrpNm = resultList[i].SMS_GRP_NM;
			var smsGrpId = resultList[i].SMS_GRP_ID;
			var custId = resultList[i].CUST_ID;
			var custNm = resultList[i].CUST_NM;
			var displayHpNo = resultList[i].DISPLAY_HP_NO;
			
			if(sendGridItems.length == 0 || typeof sendGridItems[0].USER_ID == 'undefined'){
				tempMap['SMS_GRP_NM'] = smsGrpNm;
				tempMap['SMS_GRP_ID'] = smsGrpId;
				tempMap['USER_ID'] = userId;
				tempMap['USER_NM'] = userNm;
				tempMap['HP_NO'] = hpNo;
				tempMap['CUST_ID'] = custId;
				tempMap['CUST_NM'] = custNm;
				tempMap['DISPLAY_HP_NO'] = displayHpNo;
				
				gridArray.push(tempMap);
		
			}else{
				var gridCnt = sendGridItems.length;
				for ( var j = 0; j < sendGridItems.length; j++) {
					var sendUserId = sendGridItems[j].USER_ID;
					if(userId == sendUserId){
						// 발송 대상자에 같은 userId가 있으면 등록 거부
						insertYn = false;
					}
				}
				
				// 등록여부 확인 후 등록 
				if(insertYn){
					tempMap['SMS_GRP_NM'] = smsGrpNm;
					tempMap['SMS_GRP_ID'] = smsGrpId;
					tempMap['USER_ID'] = userId;
					tempMap['USER_NM'] = userNm;
					tempMap['HP_NO'] = hpNo;
					tempMap['DISPLAY_HP_NO'] = displayHpNo;
					
					gridArray.push(tempMap);
				}
			}
			
		  }
		  $$('smsSendGrid')._addRow(gridArray);
		  $$("smsUserGrid")._reload({}, true);  
	  } 
	});
}

function removeSmsSendData(e, row, html) {
	$$("smsSendGrid")._removeRow(row);
	
	//$$("smsUserGrid")._reload({}, true); 
	//$$("smsGrpGrid")._reload({}, true);
}

function userPressEnterKey(keyCode){
	if(keyCode){
		if(keyCode == "13"){
			userSearch();
		}	
	}
}

function grpPressEnterKey(keyCode){
	if(keyCode){
		if(keyCode == "13"){
			grpSearch();
		}	
	}
}

function userSearch(){
	var data = $$("searchUserForm")._getValues();
	$$("smsUserGrid")._reload(data);
}
function grpSearch(){
	var data = $$("searchGrpForm")._getValues();
	$$("smsGrpGrid")._reload(data);
}

nkia.nbpm.fields.webEditor = function() {
	// Custom Properties
	this.rows = [];
	this.hiddens = {};
	this.label = "";
	this.id = "";
	this.name = "";
	this.isReadOnly = "";
	this.required = false;
	
	this.isProcAfterHide = true;
	
	this.propsValidate = function(props) {
		nkia.nbpm.validator.webEditor(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			this[i] = props[i];
		}
	};
	    
	this.setRender = function(props) {
		var _this = this;
		
		var render = createUnionFieldComp({
			labelConfig : "top",
			items: [
			    createLabelComp({
			    	  label : _this.label
			    	, width : 0
			    	, required : _this.required
			    }),
			    createWebEditorFieldComp({
					id : _this.id
				  , name: _this.name
				  , required : _this.required
				})
			]
		});
		
		this.hiddens = {
			
		};
		this.rows = render.rows
	};
};
     
//프라퍼티 검증함수
nkia.nbpm.validator.webEditor = function(props) {
	
};

/**
 * 공백용 필드(화면에 공백을 배치하기 위한 필드) 
 * @method {} createComp
 * @property {string} id			
 */
function createNbpmWebEditorComp(compProperty){
	var props = compProperty||{};
	var comp = {};
	try {
		comp = new nkia.nbpm.fields.webEditor();
		comp.setProps(props);
		comp.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return comp;
}
