/********************************************
 * ProcessSubGrid
 * Date: 2016-12-21
 ********************************************/
nkia.nbpm.components.ProcessSubGrid = function() {
	// UI Properties;
	this.component = null;
	
	// Custom Value Properties
	this.header = {};
	this.url = "";
	this.resource = "";
	this.required = false;
	this.isProcessGrid = true;
	this.editorMode = false;
	this.reqType = PROCESS_META.REQ_TYPE;
	this.processType = "";
	this.params = {};
	this.on = {};
	this.insertBtn = null;
	this.deleteBtn = null;
	this.insertQuerykey = "";
	this.deleteQuerykey = "";
	this.isBtn = true;
	this.height = CONSTRAINT.UBIT_SHORT_GRID_HEIGHT;
	
	this.setProps = function(props) {
		for(var i in props) {
			this[i] = props[i];
		}
		
		// 추가/삭제 버튼 추가
		this.insertBtn = createBtnComp({"id": this.id + "_add_btn","label":getConstText({isArgs : true, m_key : 'btn.common.append'}), type:"form"});
		this.deleteBtn = createBtnComp({"id": this.id + "_del_btn","label":getConstText({isArgs : true, m_key : 'btn.common.remove'})});
		
		if(this.editorMode){
			this.header.buttons = {
				items: [
					this.insertBtn, this.deleteBtn
				]
			}	
		}
		
		if(!this.isBtn) {
			delete this.header.buttons;
		}
		
		// 파라미터 추가
		this.params.req_type = this.processType;
		this.params.type = "worker";
	};
	    
	this.setRender = function(extendProps) {
		if(extendProps != null){
			nkia.ui.utils.extend(this, extendProps);
		}
		this.component = createGridComp(this);
	};
};

/********************************************
 * 확장 - 연계요청 Grid
 * Date: 2017-01-16
 ********************************************/
nkia.nbpm.components.extend.LinkRelRequest = function(){
	
	this.appendProps = function(gridProps) {
		// 추가버튼 이벤트
		if(gridProps.insertBtn != null){
			gridProps.insertBtn.click = function(){
				var srId = gridProps.params.sr_id;
				var processType = gridProps.processType;
				var reqType = gridProps.reqType;
				//var subReqType = "";
				//UBIT 연계요청시 subReqType 필요
				var subReqType = PROCESS_META.SUB_REQ_TYPE;
				var subTyCd = PROCESS_META.SUB_TY_CD;
				if(processType == null || processType == "") {
					processType = "all";
				}
				window.open("/itg/nbpm/common/goRelationRequestPop.do?up_sr_id=" + srId + "&reqType="+reqType+"&submitType=linkRel&processType=" + processType + "&reloadGridId=" + gridProps.id + "&subReqType=" + subReqType  /*+ "&subTyCd=" + subTyCd*/,
						"pop", "width=1300,height=700,history=no,resizable=yes,status=no,scrollbars=no,member=no");
			};
		}
		
		// 삭제버튼 이벤트
		if(gridProps.deleteBtn != null){
			gridProps.deleteBtn.click = function(){
				if(confirm("해당 요청을 종료합니다.\n계속 하시려면 확인을 눌러주세요")) {
					var grid = $$(gridProps.id);
					var selRows = grid._getSelectedRows();
					if( selRows.length > 0 ){
						for( var i = 0; i < selRows.length; i++ ){
							var record = selRows[i];
							nkia.ui.utils.ajax({
						        url: '/itg/nbpm/common/deleteLinkRelSrData.do',
								params: { sr_id: record.SR_ID, process_type: record.REQ_TYPE},
								async: false,
								isMask: false,
						        success: function(response){
						        	grid._removeRow(record.id);
								}
							});
						}
					}
				}
				/*if(confirm("해당 요청을 취소합니다.\n계속 하시려면 확인을 눌러주세요")) {
					var grid = $$(gridProps.id);
					var selRows = grid._getSelectedRows();
					if( selRows.length > 0 ){
						for( var i = 0; i < selRows.length; i++ ){
							var record = selRows[i];
							grid._removeRow(record.id);
						}
					}
				}*/
			};
		}
		
		// Grid 더블클릭 이벤트
		gridProps.on.onItemDblClick = function(id){
			var item = this.getItem(id);
			showProcessDetailPop(item.SR_ID);
			/*var childData = item.CHILD_DATA;
			
		    childData = childData.split(/&/g).join("%26");
			childData = childData.split(/\+/g).join("%2B");
			childData = childData.split(/%/g).join("%25");
			childData = childData.split(/#/g).join("%23");
			childData = childData.split(/\?/g).join("%3F");
			childData = childData.split(/=/g).join("%3D");*/

			/*nkia.ui.utils.ajax({
		        url: '/itg/customize/ubit/checkProcMasterCount.do',
				params: { srId: item.SR_ID},				
		        success: function(response){
		        	if(response.resultMap.CNT == 0){
		        		window.open("/itg/nbpm/common/goRelationRequestPopEdit.do?up_sr_id=" + PROCESS_META.SR_ID + "&reqType="+item.req_type+"&submitType=linkRel&processType=" + item.req_type + "&reloadGridId=" + gridProps.id + "&subReqType=" + item.sub_req_type + "&setData=" + childData,
								"pop", "width=1300,height=700,history=no,resizable=yes,status=no,scrollbars=no,member=no");
		        	}else{
		        		showProcessDetailPop(item.SR_ID);
		        	}
		        }
			});*/
			
			
			//if(PROCESS_META.TASK_NAME == "start" || PROCESS_META.TASK_NAME == "requst_regist"){
				/*var item = this.getItem(id)
				window.open("/itg/nbpm/common/goRelationRequestPopEdit.do?up_sr_id=" + PROCESS_META.SR_ID + "&reqType="+item.req_type+"&submitType=linkRel&processType=" + item.req_type + "&reloadGridId=" + gridProps.id + "&subReqType=" + item.sub_req_type + "&setData=" + item.CHILD_DATA,
						"pop", "width=1300,height=700,history=no,resizable=yes,status=no,scrollbars=no,member=no");
				
				open_pop(item);*/
				
				
			/*}else if(PROCESS_META.TASK_NAME != "start" && PROCESS_META.TASK_NAME != "requst_regist"){
				var item = this.getItem(id);
				showProcessDetailPop(item.SR_ID);
			}*/
		}
	};
};

/*function open_pop(item){
	var frmPop = document.forms[0];
	var url = "/itg/nbpm/common/goRelationRequestPopEdit.do";
	window.open("", "pop", "width=1300,height=700,history=no,resizable=yes,status=no,scrollbars=no,member=no");
	frmPop.action = url;
	frmPop.target = "pop";
	frmPop.up_sr_id = item.srId;
	frmPop.reqType = itme.req_type;
	frmPop.submitType = "linkRel";
	frmPop.processType = item.req_type;
	frmPop.reloadGridId = gridProps.id ;
	frmPop.subReqType = item.sub_req_type;
	frmPop.insertData = item.CHILD_DATA;
	
	frmPop.submit();
}*/
/********************************************
 * 확장 - 이메일 발송 Grid
 * Date: 2017-01-16
 ********************************************/
nkia.nbpm.components.extend.EmailSend = function(){
	
	this.appendProps = function(gridProps) {
		// 추가버튼 이벤트
		if(gridProps.insertBtn != null){
			gridProps.insertBtn.click = function(){
				var setPopUpProp = {	
					targetGrid : gridComp.id,
					url: getConstValue('CONTEXT') + '/itg/system/user/searchUserList.do',
					buttonAlign : 'center',
					bodyPadding: '5 5 5 5 ',
					bodyStyle: 'background-color: white; ',
					params: gridProp.params,
					isMulti: true
				}
				var popUp = createEmailUserSelectPop(setPopUpProp);
				popUp.show();
			};
		}
		
		// 삭제버튼 이벤트
		if(gridProps.deleteBtn != null){
			gridProps.deleteBtn.click = function(){
				selectedRowDel(gridId);
			};
		}
		
		// 이력버튼 생성
		var histotyBtn = createBtnComp({id: gridProps.id + "_his_btn", label: "이력보기", type: "form"});
		
		if(gridProps.header.buttons && gridProps.header.buttons.items){
			gridProps.header.buttons.items.push(histotyBtn);
		}else{
			gridProps.header.buttons = {
				items: [
					histotyBtn
				]
			}	
		}
		
		// 이력버튼 이벤트
		histotyBtn.click = function(){
			var closeBtn = createBtnComp({"id": gridId + "_close_btn","label":"닫기","visible":true, scale: "medium"});
			var storeParam = {};
			var that = {};
			var userSelectPop = null;		
			
			var gridProp = {
				id : that['id'] + '_grid',
				title : '이메일 참조자',
				gridHeight : 385,
				remoteSort : true,
				resource_prefix : 'grid.process.emailSend.sub',
				url : getConstValue('CONTEXT') + "/itg/nbpm/provide/searchSendMailHistoryList.do",
				params : {sr_id : srId}
			};
			var userGrid = createGridComp(gridProp);
	
			var userSelectPop = Ext.create('nkia.custom.windowPop', {
				title : '이메일 참조자 내역',
				height : 450,
				width : 750,
				autoDestroy : false,
				resizable : false,
				buttonAlign : 'center',
				buttons : [ closeBtn ],
				items : [ userGrid ],
				listeners : {
					destroy : function(p) {
	
					},
					hide : function(p) {
					}
				}
			});
			
			closeBtn.on("click", function() {
				userSelectPop.close();
			});
			userSelectPop.show();
		};
	};
};

/********************************************
 * 확장 - 담당 구성 장비
 * Date: 2017-01-16
 ********************************************/
nkia.nbpm.components.extend.AssignCiList = function(){
	
	this.appendProps = function(gridProps) {
		
	};
};

/********************************************
 * 확장 - 접수 목록
 * Date: 2017-01-16
 ********************************************/
nkia.nbpm.components.extend.ReceiptList = function(){
	
	this.appendProps = function(gridProps) {
		
	};
};

/********************************************
 * 확장 - 사용자 스토리
 * Date: 2017-01-16
 ********************************************/
nkia.nbpm.components.extend.UsrStoryRegist = function(){
	
	this.appendProps = function(gridProps) {
		
	};
};

/********************************************
 * 확장 - 작업 내용 상세(NKIA사내 솔루션 사업팀_유지보수요청)
 * Date: 2017-01-16
 ********************************************/
nkia.nbpm.components.extend.OperDetailRequest = function(){
	
	this.appendProps = function(gridProps) {
		
	};
};

/********************************************
 * 확장 - NICE BMT - IT기술감리 사업수행예산 Grid
 * Date: 2017-01-16
 ********************************************/
nkia.nbpm.components.extend.InspectionGrid = function(){
	
	this.appendProps = function(gridProps) {
		var srId   = gridProps.params.sr_id;
		var gridId = gridProps.id;
		
		// 추가버튼 이벤트
		if(gridProps.insertBtn != null){
			gridProps.insertBtn.click = function(){
				var srId = gridProps.params.sr_id;
				
				var grid = createGridComp({
						id: "InspectionGrid",
						keys: [],
						resizeColumn: true,
						pageable: true,
						pageSize: 30,
						url: getConstValue('CONTEXT') + "/itg/customize/nice/searchInspectionPopList.do",
						resource: 'grid.nice.inspection.budge.pop',
						params: {},
						header: {
							title: "사업수행예산"
						},
						on: {
					        onItemDblClick: InspectionGridDbClick
					    }
					});

				
				var popupWindow = createWindow({
			    	id: "popupWindow",
			    	width: 800,
			    	height: 600,
			    	header: {
			    		title: "사업수행예산"
			    	},
			    	body: {
			    		rows: new Array().concat(grid)
			    	},
			    	footer: {
			    		buttons: {
			    			align: "center",
			    			items: [
			    				createBtnComp({label: '적 용', type: "form", click: function() {
			    					
			    					InspectionGridDbClick();
			    					
			    				}})
			    			]
			    		}
			    	},
			    	closable: true
			    });
				
				// 팝업창 오픈
				$$("popupWindow").show();
				
				
			};
		}
		
		// 삭제버튼 이벤트
		if(gridProps.deleteBtn != null){
			gridProps.deleteBtn.click = function(){
				
				var selRows = $$(gridId)._getSelectedRows();
				
				// 선택된 데이터가 없으면 메시지를 띄워줍니다.
				if(!selRows || selRows.length < 1) {
					nkia.ui.utils.notification({
						type: 'error',
						message: '선택된 정보가 없습니다.'
					});
					return false;
				}
				
				if(confirm("선택한 사업 수행 예상을 삭제하시겠습니까?")) {
					if( selRows.length > 0 ){
						for( var i = 0; i < selRows.length; i++ ){
							var record = selRows[i];
							nkia.ui.utils.ajax({
						        url: '/itg/customize/nice/deleteInspectionBudge.do',
								params: { sr_id: record.SR_ID, budge_id: record.BUDGE_ID},
						        success: function(response){
						        }
							});
						}
						$$(gridId)._reload();
					}
				}
			};
		}
		
		function InspectionGridDbClick(){

			var selRows = $$("InspectionGrid")._getSelectedRows();
			
			// 선택된 데이터가 없으면 메시지를 띄워줍니다.
			if(!selRows || selRows.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '선택된 정보가 없습니다.'
				});
				return false;
			}
			if(selRows.length > 0){
				for(var i = 0; i<selRows.length; i++){
					var record = selRows[i];
					nkia.ui.utils.ajax({
				        url: '/itg/customize/nice/insertInspectionBudge.do',
						params: { sr_id: srId, budge_id: record.BUDGE_ID},
				        success: function(response){
						}
					});
				}
			}
			$$(gridId)._reload();
			$$("popupWindow").close();		
			}
	};
};

/********************************************
 * 확장 - 구성자원조회
 * Date: 2018-01-09
 ********************************************/
nkia.nbpm.components.extend.AssetSearchList = function(setGridId){
	this.appendProps = function(gridProps) {
		var _CLASS_ID = "0";
		var _CLASS_MNG_TYPE = "AM";
		var _CLASS_TYPE = "ROOT";
		var _ASSET_STATE = "ACTIVE";
		var _CENTER_ASSET_YN = "Y";
		var _TANGIBLE_ASSET_YN = "Y";
		this.checkbox = true;
		this.select = false;
		this.insertQuerykey = gridProps.insertQuerykey;
		this.deleteQuerykey = gridProps.deleteQuerykey;
        		
		// 추가버튼 이벤트
		if(gridProps.insertBtn != null){
			gridProps.insertBtn.click = function(){
				createSearchAssetSelectPop();
			};
		}
		
		// 삭제버튼 이벤트
		if(gridProps.deleteBtn != null){
			gridProps.deleteBtn.click = function(){
				$$(setGridId)._removeRow();
			};
		}
		
		function createSearchAssetSelectPop(){
			
			var width = nkia.ui.utils.getWidePopupWidth();
			var height = nkia.ui.utils.getWidePopupHeight();
				
			var classTree = createTreeComponent({
				id : 'classTree',
				url : '/itg/itam/amdb/searchAmClassAllTree.do',
				width: 200,
				expColable: true,
				filterable: true,
				params: {
					expandLevel: 3,
					center_asset_yn : _CENTER_ASSET_YN
				},
				expandLevel: 3,
				header: {
					title: "분류체계"
				},
				on: {
		        	onItemClick: treeNodeClick
		        }
			});
			
			var searchForm = createFormComp({
				id: "searchForm",
				elementsConfig: {
		    		labelPosition: "left"
		    	},
				header: {
					title: "검색",
					icon: "search"
				},
				footer: {
					buttons: {
						align: "center",
						items: [
							createBtnComp({ label:"검 색", type: "form", click: searchList }),
							createBtnComp({ label:"초기화", click: clearSearchForm })
						]
					}
				},
				fields: {
					colSize: 2,
					items: [
						{colspan: 2, item: createUnionFieldComp({
		                    items: [
		                            createCodeComboBoxComp({
		                				label: '조회구분',
		                				name: 'search_type',
		                				code_grp_id: 'SEARCH_'+_CLASS_TYPE+'_STATISTICS',
		                				attachChoice: true,
		                				width: 250
		                			}),
		                			createTextFieldComp({
		                				name: "search_value",
		                				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
		                				on: {
		                					onKeyPress:searchListByEnterKey
		                				}
		                			})
		                        ]
		                    })
						}
					]
				}
			});
			
			var setAssetGrid = createGridComp({
				id: 'setAssetGrid',
				header: {
		        	title: '자산 목록'
			    },
				height : 300,
			    resource: 'grid.itam.oper.asset',
			    params: this.gridParams,
				url: '/itg/itam/oper/searchAssetList.do',
				select: false,
				pageable: true,
				checkbox: true,
				keys: ["CONF_ID"],
				dependency: {
					targetGridId: 'selectedAssetGrid'
				}
			});
			
			var selectedAssetGrid = createGridComp({
				id: 'selectedAssetGrid',
				header: {
					title: "선택된 자산"
				},
			    resource: 'grid.nbpm.asset.serviceexp.sel',
				select: false,
				pageable: false,
				autoLoad: false,
				keys: ["CONF_ID"],
				actionColumns: [{
					column: "DEL_ACTION",
					template: function(data){
						return nkia.ui.html.icon("remove", "delete_row");
					},
					onClick: {
						"delete_row": function(e, row, html){
							this._removeRow(row);
							$$('setAssetGrid')._reload();
						}
					}
				}]
			});
			
			var popupFooter = createButtons({
				align: "center",
				items: [
					createBtnComp({label: '적 용', type: "form", click: choice})
				]
			});
			
			var body = {
					rows: [{
		    			view:"accordion",
		    			multi:true,
		    			cols: [{
		                    body: {
		                    	rows: classTree
		                    }
		                },{
		                    rows: new Array().concat(searchForm, setAssetGrid)
		                },{
		                    view: "template",
					 		template: nkia.ui.html.icon("arrow_right", "add_asset"),
					 		width: 20,
					 		onClick: {
					 			"add_asset": function(e, row, html){
					 				var data = $$('setAssetGrid')._getSelectedRows();
					 				if(data.length > 0){
					 					$$('selectedAssetGrid')._addRow(data);
					 					$$('setAssetGrid')._reload();
					 				}
					 			}
					 		}
		                },{
		                	width: 340,
		                    rows: new Array().concat(selectedAssetGrid,popupFooter)
		                }]
		    		}]
			};
					
			var popupWindow = createWindow({
		    	id: "popupWindow",
		    	width: width,
		    	height: height,
		    	header: {
		    		title: "구성자원조회"
		    	},
				body: body,
		    	closable: true
		    });
			
			bindData();
			
			// 팝업창 오픈
			$$("popupWindow").show();
			
			// Tree 클릭 이벤트 - 검색폼 초기화
			function treeNodeClick(id, e){
				var treeItem = this.getItem(id);
				clearSearchForm();
				
				_CLASS_ID = treeItem.node_id;
				_CLASS_TYPE = treeItem.class_type;
				
				var treeParams = {
					class_id : treeItem.node_id,
					class_type : treeItem.class_type,
					class_mng_type : _CLASS_MNG_TYPE,
					asset_state : _ASSET_STATE,
					tangible_asset_yn : _TANGIBLE_ASSET_YN,
					center_asset_yn : _CENTER_ASSET_YN
				}
				
				$$("setAssetGrid")._reload(treeParams);
  
			}
			
			
			// 검색 키프레스 이벤트
			function searchListByEnterKey(keyCode){
				if(keyCode && keyCode == "13"){
					searchList();
				}
			}
			
			// 검색폼 초기화
			function clearSearchForm(){
				$$("searchForm")._reset();
				setDefaultValueToSearchForm();
			}
			
			/**
			 * 검색폼 기본값 세팅
			 */
			function setDefaultValueToSearchForm() {
				
				var initValues = {
					asset_state : _ASSET_STATE
				};
				
				$$("searchForm")._setValues(initValues);
			}
			
			/**
			 * 검색
			 */
			function searchList() {
				var treeParams = $$("classTree").getSelectedItem();

				if(typeof treeParams != "undefined") {
					_CLASS_ID = treeParams.node_id;
					_CLASS_TYPE = treeParams.class_type;
				}
				
				var searchParams = $$("searchForm")._getValues();
				// 자산 리스트 검색에 필수로 필요한 조건 세팅
				searchParams["class_id"] = _CLASS_ID;
				searchParams["class_type"] = _CLASS_TYPE;
				searchParams["class_mng_type"] = _CLASS_MNG_TYPE;
				searchParams["asset_state"] = _ASSET_STATE;
				searchParams["tangible_asset_yn"] = _TANGIBLE_ASSET_YN;
				searchParams["center_asset_yn"] = _CENTER_ASSET_YN;
				
				$$("setAssetGrid")._reload(searchParams); // 페이지 유지
			}
			
			// 적용(선택) 이벤트
			function choice(id, e){
				var assetRows = $$("selectedAssetGrid")._getRows();
				if(assetRows.length > 0){
					var reference = $$(setGridId);
					reference._removeAllRow();
					reference._addRow(assetRows);
					
					$$("popupWindow").close();
		            
				}else{
		            nkia.ui.utils.notification({
		                type: 'error',
		                message: '선택된 자산이 없습니다.'
		            });
		        }
			};
			
			function bindData(){
				// 선택된 구성 목록
				var parentGridData = $$(setGridId)._getRows();
			    if(parentGridData.length > 0){
			    	$$('selectedAssetGrid')._addRow(parentGridData);
			    }
				
			}
		};	
	};
};


function createProcessSubGrid(command, gridProp){	
	var props = gridProp||{};
	var grid = {};
	var extendGrid = null;
	try {
		grid = new nkia.nbpm.components.ProcessSubGrid();
		grid.setProps(props);
		switch(command) {
			case "nw_request":	// 연계요청
			case "requst_regist_grid":	// 연계요청
			case "requst_rcept_grid":	// 연계요청
			case "requst_result_grid":	// 연계요청
			case "proc_complete_grid":	// 연계요청
				extendGrid = new nkia.nbpm.components.extend.LinkRelRequest();
				extendGrid.appendProps(grid)
				break;
			case "email_send":	// 이메일
				extendGrid = new nkia.nbpm.components.extend.EmailSend();
				extendGrid.appendProps(grid)
				break;
			case "assign_ci_list":	// 담당구성장비
				extendGrid = new nkia.nbpm.components.extend.AssignCiList();
				extendGrid.appendProps(grid)
				break;
			case "receipt_list":	// 접수목록
				extendGrid = new nkia.nbpm.components.extend.ReceiptList();
				extendGrid.appendProps(grid)
				break;
			case "usr_story_regist":	// 사용자 스토리
				extendGrid = new nkia.nbpm.components.extend.UsrStoryRegist();
				extendGrid.appendProps(grid)
				break;
			case "operDetail_request":	// 작업 내용 상세
				extendGrid = new nkia.nbpm.components.extend.OperDetailRequest();
				extendGrid.appendProps(grid)
				break;
			case "setInspectionGrid":
				extendGrid = new nkia.nbpm.components.extend.InspectionGrid();
				extendGrid.appendProps(grid)
				break;
			case "asset_search": // 구성자원조회 
				var setGridId = "asset_search";
				extendGrid = new nkia.nbpm.components.extend.AssetSearchList(setGridId);
				extendGrid.appendProps(grid)
				break;
			case "asset_search_sub": // 구성자원조회 (수정화면)
				var setGridId = "asset_search_sub";
				extendGrid = new nkia.nbpm.components.extend.AssetSearchList(setGridId);
				extendGrid.appendProps(grid)
				break;
		}
		grid.setRender(extendGrid);
	} catch(e) {
		alert(e.message);
	}
	return grid.component;
}

/********************************************
 * MultiTaskGrid
 * Date: 2016-12-21
 ********************************************/
nkia.nbpm.components.MultiTaskGrid = function() {
	// UI Properties;
	this.component = null;
	
	// Custom Value Properties
	this.id = "";
	this.header = {};
	this.actions = [];
	this.url = "";
	this.resource = "";
	this.required = false;
	this.isProcessGrid = true;
	this.editorMode = false;
	this.reqType = PROCESS_META.REQ_TYPE;
	this.checkbox = true;
	this.processType = "";
	this.params = {};
	this.on = {};
	this.srId = "";
	this.roleId = "";
	this.insertBtn = null;
	this.deleteBtn = null;
	this.actionColumns = [];
	this.keys = ["USER_ID"];
	
	this.setProps = function(props) {
		var _this = this;
		for(var i in props) {
			this[i] = props[i];
		}
		
		// 추가/삭제 버튼 추가
		this.allInsertBtn = createBtnComp({"id": this.id + "_all_add_btn","label":"일괄발행", type:"form"});
		this.insertBtn = createBtnComp({"id": this.id + "_add_btn","label":"추가", type:"form"});
		this.deleteBtn = createBtnComp({"id": this.id + "_del_btn","label":"삭제"});
		
		if(this.editorMode){
			this.header.buttons = {
				items: [
					this.allInsertBtn, this.insertBtn, this.deleteBtn
				]
			}	
		}
		
		// SR ID 셋팅
		this.srId = this.params.sr_id;
		
		// 파라미터 추가
		this.params.req_type = this.processType;
		this.params.type = "worker";
		// 버튼 이벤트
		this.allInsertBtn.click = function(){
			// 사용자 팝업
			var grid = $$(_this.id); 
			var items = grid._getSelectedRows();
			
			if(items.length > 0){
				if (confirm("이미 생성된 티켓은 발행되지 않습니다.\n티켓을 생성하시겠습니까?")) {
					items.forEach(function(item){
						if(item.NODENAME == null || item.NODENAME == ""){
							_this.params.oper_type = _this.roleId;
							_this.params.worker_user_id = item.USER_ID;
							_this.params.nbpm_processInstanceId = PROCESS_META.PROCESSINSTANCEID;
							nkia.ui.utils.ajax({
						        url: '/itg/nbpm/createMultiTask.do',
								params: _this.params,
								async: false,
								isMask: false,
						        success: function(response){
						        	item.TASK_ID = response.resultMap.task_id;
						        	item.NODENAME = response.resultMap.nodename;
						        	item.TASK_GROUP_ID = response.resultMap.task_group_id;
						        	item.MULTI_TASK_NAME = response.resultMap.task_name;
								}
							});	
							
						}
					});
					$$(_this.id)._reload();
				}
			}else{
				nkia.ui.utils.notification({
	                type: 'error',
	                message: '선택된 정보가 없습니다.'
	            });
			}
		}				
		// 버튼 이벤트
		this.insertBtn.click = function(){
			// 사용자 팝업
			var params = {};
			if (_this.roleId != "servicedesk") {
				params["auth_id"] = _this.processType + "_" + _this.roleId;
			} else {
				params["auth_id"] = _this.roleId;
			}
			
			params["oper_type"] = _this.roleId;
			params["process_type"] = _this.reqType; 
			
			nkia.ui.utils.window({
				id: "userWindow",
				title: "담당자 조회",
				width : 1000,
				type: "multitask",
				//url: "/itg/system/user/searchUserList.do",
				url: "/itg/nbpm/common/searchProcessOperUserList.do",
				params: params,
				reference: {
					grid: $$(_this.id),
					data: $$(_this.id)._getRows()
				},
				callFunc: createUserWindow
			});
		}
		
		// 버튼 이벤트
		this.deleteBtn.click = function(){
			var grid = $$(_this.id); 
			var items = grid._getSelectedRows();
			if(items.length > 0){
				if (confirm("해당 업무를 삭제합니다.\n계속 하시려면 확인을 눌러주세요")) {
					items.forEach(function(item){
						if(item.TASK_GROUP_ID && item.TASK_GROUP_ID.toString() != ""){
							var params = {
								sr_id: item.SR_ID,
								process_type: item.REQ_TYPE,
								nbpm_task_id: item.TASK_GROUP_ID.toString(),
								task_group_id: item.TASK_ID.toString()
							};
							
							nkia.ui.utils.ajax({
						        url: "/itg/nbpm/cancelMultiTask.do",
								params: params,
								isMask: false,
								async: false,
						        success: function(response){
						        	grid._removeRow(item.id);
						        }
							});	
						}else{
							grid._removeRow(item.id);
						}
					});
				}
			}else{
				nkia.ui.utils.notification({
	                type: 'error',
	                message: '선택된 정보가 없습니다.'
	            });
			}
			
		}
		
		// Grid 더블클릭 이벤트
		this.on.onItemDblClick = function(id){
			var item = this.getItem(id);
			var taskId = item.TASK_ID;
			var event_name = nkia.ui.utils.check.nullValue(item.EVENT_NAME);
				if(event_name ==""){
					event_name = this.config.params.event_name;
				}
			if (taskId != null && taskId != "") {
				showMultiTaskDetailPop(PROCESS_META.SR_ID, taskId, event_name);
			} else {
				nkia.ui.utils.notification({
	                type: 'error',
	                message: '티켓생성 후 조회 가능합니다.'
	            });
			}
		}
		
		// 편집모드에서 활성화
		if(this.editorMode){
			this.actionColumns = [{
				column: "CREATE",
				template: function(data){
					if(data.NODENAME != null && data.NODENAME != ""){
						return nkia.ui.html.icon("check", "");
					}else{
						return nkia.ui.html.icon("ticket", "create_ticket"); 
					}
				},
				onClick: {
					"create_ticket": function(e, row, html){
						var grid = this;
						if (confirm("티켓을 생성하시겠습니까?")) {
							var item = grid.getItem(row);
							_this.params.oper_type = _this.roleId;
							_this.params.worker_user_id = item.USER_ID;
							_this.params.nbpm_processInstanceId = PROCESS_META.PROCESSINSTANCEID;
							
							nkia.ui.utils.ajax({
						        url: '/itg/nbpm/createMultiTask.do',
								params: _this.params,
								async: false,
								isMask: false,
						        success: function(response){
						        	item.TASK_ID = response.resultMap.task_id;
						        	item.NODENAME = response.resultMap.nodename;
						        	item.TASK_GROUP_ID = response.resultMap.task_group_id;
						        	item.MULTI_TASK_NAME = response.resultMap.task_name;
						        	grid.updateItem(row, item);
								}
							});	
						}
					}
				}
			}]
		}
		
		
	};
	    
	this.setRender = function() {
		this.component = createGridComp(this);
	};
};

function createMultiTaskGrid(id, gridProp){
	var props = gridProp||{};
	var grid = {};
	try {
		props.id = id;
		grid = new nkia.nbpm.components.MultiTaskGrid();
		grid.setProps(props);
		grid.setRender();
	} catch(e) {
		alert(e.message);
	}
	return grid.component;
}

//프로세스 Template Grid 생성
function createProcessTemplate(compProperty) {
	
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;
	
	var req_type = that['req_type'];
	var tem_code = that['tem_code'];
	var req_doc_id = PROCESS_META.SUB_TY_CD;
	
	t_comp = createAtchFileComp({
		id: "template_downloader",
		height:150,
		readonly: 'true',
		isTemplate: 'true',
		reqType: req_type,
		temCode: tem_code,
		reqDocId: req_doc_id,
		helper: that['helper'],
		helperWidth: that['helperWidth'],
		helperHeight: that['helperHeight']
	});//, temCode: tem_code});
	
	return t_comp;
		
};