/********************************************
 * Process - 프로세스 조회 팝업
 * Date: 2017-01-13
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.Process = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/nbpm/common/searchProcessList.do";
	this.resource = "grid.pop.process";
	this.params = {};
	this.reference = {};
	this.type = "default";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 2,
		        items: [
		            { item: createTextFieldComp({ label : getConstText({isArgs : true, m_key : 'res.common.label.req.user'}), name : 'req_user_nm', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createTextFieldComp({ label : getConstText({isArgs : true, m_key : 'res.common.label.req.no'}), name : 'sr_id', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createSearchDateFieldComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00025'}), name : 'end_time'}) },
		            { item: createSearchDateFieldComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00040'}), name : 'req_time'}) },
		            { item: createCodeComboBoxComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00003'}), name : 'req_type', code_grp_id: 'REQ_TYPE', value: (_this.params.req_type) ? _this.params.req_type : "", readonly: (_this.params.req_type) ? true : false, attachAll: true }) },
		            { item: createCodeComboBoxComp({ label : getConstText({isArgs : true, m_key : 'res.label.itam.maint.req.00012'}), name : 'work_state', code_grp_id: 'WORK_STATE_SEARCH', attachAll:true}) },
		            { colspan: 2, item: createTextFieldComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00041'}), name : 'title', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) }		           
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.search'}), type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.system.00003'}), click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom_top"
		        }
		    }
		});
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: getConstText({isArgs : true, m_key : 'res.label.nbpm.00012'})
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			select: true,
			pageable: true,
			pagerCss: "webix_pager_no_margin",
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
	};
	
	 
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.searchForm, this.grid)
			            }]
					}
			};
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.choice'}), type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e){
		var treeItem = $$(this.id + '_deptTree').getItem(id);
		
    	var form = $$(this.id + '_searchForm');
    	var values = form._getValues();
    	
    	if( getConstValue("CENTER_CUST_ID") == treeItem.up_node_id ){
			values.dept_nm = treeItem.text;
		}else{
			values.cust_id = treeItem.node_id;
		}
		$$(this.id + '_grid')._reload(values);
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		$$(this.id + '_grid')._reload(form._getValues());
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(){
		var item = $$(this.id + "_grid").getSelectedItem();
		if(item){
			var reference = this.reference;
			var fields = reference.fields;
			
			var values = {};
			values[fields["name"]] = item.SR_ID; 
			
            reference.form._setValues(values)
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: getConstText({isArgs : true, m_key : 'res.ubit.center.000005'})
            });
        }
	};
};

/********************************************
 * Process - 프로세스 조회 (변경요청에서 상위연계요청 필드 선택 시)
 * Date: 2019-11-14
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.ProcessChange = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/nbpm/common/searchProcessList.do";
	this.resource = "grid.pop.process";
	this.params = {};
	this.reference = {};
	this.type = "";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 2,
		        items: [
		            { item: createTextFieldComp({ label : '요청자', name : 'req_user_nm', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createTextFieldComp({ label : '요청번호', name : 'sr_id', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createSearchDateFieldComp({ label : '완료일시', name : 'end_time'}) },
		            { item: createSearchDateFieldComp({ label : '요청일시', name : 'req_time'}) },
		            { item: createCodeComboBoxComp({ label : '요청유형', name : 'req_type', code_grp_id: 'CHANGE_UP_REQ_TYPE', value: (_this.params.req_type) ? _this.params.req_type : "", readonly: (_this.params.req_type) ? true : false, attachAll: true }) },
		            { item: createCodeComboBoxComp({ label : '요청상태', name : 'work_state', code_grp_id: 'WORK_STATE_SEARCH', attachAll:true}) },
		            { colspan: 2, item: createTextFieldComp({ label : '제목', name : 'title', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) }
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom_top"
		        }
		    }
		});
		
		this.params["type"]="CHANGE"
			
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: "요청 목록"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			select: true,
			pageable: true,
			pagerCss: "webix_pager_no_margin",
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
	};
	
	 
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.searchForm, this.grid)
			            }]
					}
			};
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e){
		var treeItem = $$(this.id + '_deptTree').getItem(id);
		
    	var form = $$(this.id + '_searchForm');
    	var values = form._getValues();
    	
    	if( getConstValue("CENTER_CUST_ID") == treeItem.up_node_id ){
			values.dept_nm = treeItem.text;
		}else{
			values.cust_id = treeItem.node_id;
		}
		$$(this.id + '_grid')._reload(values);
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		$$(this.id + '_grid')._reload(form._getValues());
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(){
		var item = $$(this.id + "_grid").getSelectedItem();
		if(item){
			var reference = this.reference;
			var fields = reference.fields;
			
			var values = {};
			values[fields["name"]] = item.SR_ID;
			values["center_id"] = item.CENTER_ID;
			values["service_trget_customer_id"] = item.SERVICE_TRGET_CUSTOMER_ID;
			values["service_trget_customer_id_nm"] = item.SERVICE_TRGET_CUSTOMER_NM;
			values["req_doc_id"] = item.REQ_DOC_ID;
			values["req_doc_nm"] = item.REQ_DOC_NM;
			//values["end_due_dt"] = item.END_DUE_DT;
			
            reference.form._setValues(values)
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 요청 정보가 없습니다.'
            });
        }
	};
};

/********************************************
 * Process - 센터출입요청 이전요청 불러오기
 * Date: 2019-11-29
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.CenterPreReqLoad = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/customize/ubit/selectPreReqLoad.do";
	this.resource = "grid.pop.process";
	this.params = {};
	this.reference = {};
	this.type = "";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 2,
		        items: [
		            { item: createTextFieldComp({ label : getConstText({isArgs : true, m_key : 'res.common.label.req.user'}), name : 'req_user_nm', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createTextFieldComp({ label : getConstText({isArgs : true, m_key : 'res.common.label.req.no'}), name : 'sr_id', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createSearchDateFieldComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00025'}), name : 'end_time'}) },
		            { item: createSearchDateFieldComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00040'}), name : 'req_time'}) },
		            { item: createCodeComboBoxComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00003'}), name : 'req_type', code_grp_id: 'REQ_TYPE', value: 'CENTER', readonly: true}) },
		            { item: createCodeComboBoxComp({ label : getConstText({isArgs : true, m_key : 'res.label.itam.maint.req.00012'}), name : 'work_state', code_grp_id: 'WORK_STATE_SEARCH', attachAll:true}) },
		            { colspan: 2, item: createTextFieldComp({ label : getConstText({isArgs : true, m_key : 'res.label.nbpm.00041'}), name : 'title', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) }
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.search'}), type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.system.00003'}), click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom_top"
		        }
		    }
		});
		
		this.params["type"]="CENTER"
		this.params["req_type"]="CENTER"
			
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: getConstText({isArgs : true, m_key : 'res.label.nbpm.00012'})
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			select: true,
			pageable: true,
			pagerCss: "webix_pager_no_margin",
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
	};
	
	 
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.searchForm, this.grid)
			            }]
					}
			};
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
			height : this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.choice'}), type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
		
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		$$(this.id + '_grid')._reload(form._getValues());
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(){
		var item = $$(this.id + "_grid").getSelectedItem();
		if(item){
			var reference = this.reference;
			var fields = reference.fields;
			
			var values = {};
			//values[fields["name"]] = item.SR_ID;
			values["title"] = item.TITLE;
			values["center_id"] = item.CENTER_ID;
			values["service_trget_customer_id"] = item.SERVICE_TRGET_CUSTOMER_ID;
			values["service_trget_customer_id_nm"] = item.SERVICE_TRGET_CUSTOMER_NM;
			//values["end_due_dt"] = item.END_DUE_DT;
			values["entrance_purpose"] = item.ENTRANCE_PURPOSE;
			values["supervisor_yn"] = item.SUPERVISOR_YN;
			values["entrance_cd"] = item.ENTRANCE_CD;
			values["purpose_cd"] = item.PURPOSE_CD;
			values["entrance_object_yn"] = item.ENTRANCE_OBJECT_YN;
			values["cable_work_yn"] = item.CABLE_WORK_YN;
			//values["work_str_dt"] = item.WORK_STR_DT;
			//values["work_end_dt"] = item.WORK_END_DT;
			values["work_help"] = item.WORK_HELP;
			values["rent_object_cd"] = item.RENT_OBJECT_CD;
			values["install_sw_nm"] = item.INSTALL_SW_NM;
			
            reference.form._setValues(values);
            
            nkia.ui.utils.ajax({
    			url: "/itg/customize/ubit/selectPreReqLoadGuestGrid.do",
    			params: item,
    			success: function(data) {
    				if (data.success) {
    					$$('regist_guest_grid')._removeAllRow();
    					$$('regist_guest_grid')._addRow(data.gridVO.rows)
    					nkia.ui.utils.ajax({
    		    			url: "/itg/customize/ubit/selectPreReqLoadWorkGrid.do",
    		    			params: item,
    		    			success: function(data) {
    		    				if (data.success) {
    		    					$$('center_work_grid')._removeAllRow();
    		    					$$('center_work_grid')._addRow(data.gridVO.rows)
    		    				} else {
    		    					//alert(data.resultMsg);
    		    				}
    		    			}			
    		    		});
    				} else {
    					//alert(data.resultMsg);
    				}
    			}			
    		});
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: getConstText({isArgs : true, m_key : 'res.ubit.center.000005'})
            });
        }
	};
};

/********************************************
 * Process - 마감시한변경요청 내요청 불러오기
 * Date: 2020-02-13
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.myReqLoad = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/customize/ubit/selectMyReqLoad.do";
	this.resource = "grid.pop.process";
	this.params = {};
	this.reference = {};
	this.type = "";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 2,
		        items: [
		            { item: createTextFieldComp({ label : '요청자', name : 'req_user_nm', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createTextFieldComp({ label : '요청번호', name : 'sr_id', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) },
		            { item: createSearchDateFieldComp({ label : '완료일시', name : 'end_time'}) },
		            { item: createSearchDateFieldComp({ label : '요청일시', name : 'req_time'}) },
		            { item: createCodeComboBoxComp({ label : '요청유형', name : 'req_type', code_grp_id: 'REQ_TYPE', value: (_this.params.req_type) ? _this.params.req_type : "", readonly: (_this.params.req_type) ? true : false, attachAll: true , filteredData:['DEADLINE']}) },
		            { item: createCodeComboBoxComp({ label : '요청상태', name : 'work_state', code_grp_id: 'WORK_STATE_SEARCH', attachAll:true, filteredData:['END']}) },
		            { colspan: 2, item: createTextFieldComp({ label : '제목', name : 'title', on:{ onKeyPress: this.doKeyPress.bind(_this) }}) }
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom_top"
		        }
		    }
		});
		
		//this.params["type"]="CENTER"
		//this.params["req_type"]="CENTER"
			
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: "요청 목록"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			select: true,
			pageable: true,
			pagerCss: "webix_pager_no_margin",
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
	};
	
	 
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.searchForm, this.grid)
			            }]
					}
			};
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
			height : this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
		
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		$$(this.id + '_grid')._reload(form._getValues());
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(){
		var item = $$(this.id + "_grid").getSelectedItem();
		if(item){
			var reference = this.reference;
			var fields = reference.fields;
			
			var values = {};
			values[fields.name] = item.SR_ID;
			values["end_due_dt"] = item.END_DUE_DT;
				
            reference.form._setValues(values);
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 요청 정보가 없습니다.'
            });
        }
	};
};

/********************************************
 * Process - 프로세스 삭제 사유 입력 팝업
 * Date: 2017-04-11
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Footer 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.processDelReason = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 800;
	this.params = {};
	this.reference = {};
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		this.edit = createFormComp({
			id: this.id + '_editForm',
			header: {
				title: this.title
		    },
		    fields: {
		    	colSize: 1,
		        items: [
		            { colspan: 1, item: createTextAreaFieldComp({ label : '삭제사유', name : 'delete_reason', required: true}) }
		        ]
		    }
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: new Array().concat(this.edit)
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
		
	// 적용 이벤트
	this.choice = function(){
		var deleteReason = $$(this.id + '_editForm')._getFieldValue('delete_reason');
		
		if(deleteReason == undefined || deleteReason == ""){
			nkia.ui.utils.notification({
				type: 'error',
				message: "삭제사유를 입력해주시기 바랍니다."
			});
			return false;
		}
		
		var paramMap = this["params"];
		paramMap["delete_reason"] = deleteReason;
		
		nkia.ui.utils.ajax({
			url: "/itg/nbpm/edit/deleteSrData.do",
			params: paramMap,
			success: function(data) {
				if (data.success) {
					alert(data.resultMsg);
					parent.closeTab();
					
				} else {
					alert(data.resultMsg);
				}
			}			
		});
	};
};


/********************************************
 * Process - 결재자라인 팝업
 * Date: 2017-04-11
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Footer 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.sanctionLine = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 800;
	this.height = 600;
	this.params = {};
	this.reference = {};
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_tree',
			url: '/itg/workflow/sanction/searchAllSanctionLineTree.do',
			expColable: true,
			filterable: true,
			width: 250,
			height: 400,
			params: this.params,
			expandLevel: 3,
			header: {
				title: "결재라인 목록"
			},
			on: {
				onItemDblClick: this.treeNodeClick.bind(_this)
			}
		});
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			keys: [],
			resizeColumn: true,
			pageable: true,
			paseSize: 10,
			width: 300,
			url: '/itg/workflow/sanction/searchSanctionLineProcAuth.do',
			resource: 'grid.workflow.sanction.auth.pop',
			params: this.params,
			on:{
				onItemDbClick: this.choice.bind(_this)
			},
			header: {
				title: "결재라인 프로세스 권한목록"
			}
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows:[{
	    			cols: [{
	    				rows: this.tree
	    			},{
	    				rows: new Array().concat(this.grid)
	    			}]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	this.treeNodeClick = function(id, e){
		var clickNode = $$(this.id+'_tree')._getSelectedItem();
		var param = {};
		for(var i in this.params) {
			if(this.params[i] !== undefined) {
				param[i] = this.params[i];
			}
		}
		
		param["line_id"] = clickNode.node_id;
		
		$$(this.id+'_grid')._reload(param);
	}

	// 적용 이벤트
	this.choice = function(){
		var _this = this;
		var lineGridMap = $$(this.id+'_grid')._getRows();
		var editFormId = this.params.editFormId;
		
		// 선택된 데이터가 없으면 메시지를 띄워줍니다.
		if(lineGridMap.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		
		var operUserList = [];
		for (var i = 0; i < lineGridMap.length; i++) {
			var dataMap = lineGridMap[i];
			operUserList.push(dataMap);
		}
		
		var operUserInfoParam = {};
		operUserInfoParam["operUserList"] = operUserList;
		operUserInfoParam["srId"] = PROCESS_META.SR_ID;
		
		if (operUserList != null && operUserList.length > 0) {
			
			nkia.ui.utils.ajax({
				url: "/itg/nbpm/registerSanctionLineInfo.do",
				params: operUserInfoParam,
				success: function(data) {
					if (data.success) {
						
						var editObj = $$(editFormId).elements;
						var editItem = "";
						var editName = {};
						var editLength = Object.keys(editObj).length;
						var editArray = Object.keys(editObj);
						
						if(typeof editArray != 'undefined'){
							for(var i=0; i<editLength; i++){
								editItem = editArray[i];
								editName = editItem.split('_');
								
								// 담당자 선택 컴포넌트의 네임 규칙 -> name : "ROLE_OPETR_XX"
								// 담당자 선택 컴포넌트 있는지 확인, 담당자명 셋팅
								if(editName[0] == 'ROLE' && editName[1] == 'OPETR'){
									if(editName.length < 4){
										
										for(var j=0; j<operUserList.length; j++){
											if(operUserList[j].SANCTION_NM != undefined){
												var tmpData = operUserList[j].PROC_AUTH_ID.split('_');
												
												if(parseInt(editName[2]) == parseInt(tmpData[tmpData.length-1])){
													var operUserNm = operUserList[j].SANCTION_NM;
													$$(editFormId)._setFieldValue(editItem, operUserNm);
													
													}
											}else{
												
											}
										}										
									}else{
										
									}
								}
							}
						}
						alert("등록되었습니다.");
						
						$$(_this.id).close();
						
					} else {
						alert(data.resultMsg);
					}
				}			
			});
		}
	};
};




/**
 * @method {} createDeptWindow : Form Function
 * @property {string} id			: Form Id
 * @property {object} header 		: Form Header
 * @property {object} fields 		: Form Fields
 * @property {object} footer 		: Form Footer
 * @returns {object} form
 */
function createProcessWindow(popupProps) {
	var props = popupProps||{};
	var process;
	try {
		
		switch(props.type){
			case 'processDelReason' : process = new nkia.ui.popup.nbpm.processDelReason();
				break;
			case 'sanctionLine' : process = new nkia.ui.popup.nbpm.sanctionLine(); 
				break;
			case 'CHANGE' : process = new nkia.ui.popup.nbpm.ProcessChange(); 
				break;
			case 'centerPreReqLoad' : process = new nkia.ui.popup.nbpm.CenterPreReqLoad(); 
			    break;	
			case 'myReqLoad' : process = new nkia.ui.popup.nbpm.myReqLoad(); 
				break;	    
			    
			default : process = new nkia.ui.popup.nbpm.Process();
				break;
		}
		process.setProps(props);
		process.setRender(props);
		process.setWindow();
	} catch(e) {
		alert(e.message);
	}
}

/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.MultiCode = function() {
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/base/searchMutilCodeDataListForTree.do',
	        height: 550,
	        filterable: true,
	        params: params,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if(treeItem){
			
			if(treeItem.leaf == true){
				var reference = this.props.reference;
				var fields = reference.fields;
				
				var values = {};
	
				values[fields.id] = treeItem.node_id;	// 멀티코드 ID
				values[fields.name] = treeItem.text;		// 멀티코드 명 
				
	            reference.form._setValues(values)
	            this.window.close();
	
			}else{
				nkia.ui.utils.notification({
	                type: 'info',
	                message: '마지막 노드만 선택가능합니다.'
	            });
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 코드정보가 없습니다.'
            });
        }
	};
};

/********************************************
 * Date: 2019-10-24
 * Version:
 *	UBIT 고객사 팝업
 ********************************************/
nkia.ui.popup.nbpm.customerPopup = function() {
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		params['use_yn'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/system/customer/searchStaticAllCustomerTree.do',
	        filterable: true,
	        params: params,
	        //height:550,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		var bodyScrollView = {
				view : "scrollview",
				body : {
					cols : [ {
						rows : new Array().concat(tree)
					} ]
				}
			};
		
		_this.window = createWindow({
	    	id: props.id,
	    	height : nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.choice'}), type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if(treeItem){
			
			if(treeItem.leaf == true){
				var reference = this.props.reference;
				var fields = reference.fields;
				
				var values = {};
				
				var sum = 0;

				sum = Number($$("requst_regist")._getFieldValue("servercarry_nm"))
				+Number($$("requst_regist")._getFieldValue("networkcarry_nm"))+Number($$("requst_regist")._getFieldValue("securitycarry_nm"))+Number($$("requst_regist")._getFieldValue("storagecarry_nm"))+Number($$("requst_regist")._getFieldValue("etccarry_nm"))
				+Number($$("requst_regist")._getFieldValue("rackcarry_nm"))
				+Number($$("requst_regist")._getFieldValue("facilitycarry_nm"));
				
				if(sum>0){
					if($$("requst_regist")._getFieldValue("service_trget_customer_id_nm")!=""){
						
						var after = 0;
						var before = 0;
						
						nkia.ui.utils.ajax({
					        url: "/itg/itam/serviceAsset/assetCustomerCount.do",
							params: { CUSTOMER_ID : $$("requst_regist")._getFieldValue("service_trget_customer_id") },
							async: false,
							isMask: false,
					        success: function(result){      
				        		var requstData = result.resultMap;
					        	after = requstData.totalCount;
							}
						});
						
						nkia.ui.utils.ajax({
					        url: "/itg/itam/serviceAsset/assetCustomerCount.do",
							params: { CUSTOMER_ID : treeItem.node_id },
							async: false,
							isMask: false,
					        success: function(result){      
				        		var requstData = result.resultMap;
				        		before = requstData.totalCount;
							}
						});
											
						if(after > before){
							if(!confirm('해당 고객사는 유지보수 시스템을 이용하지 않는 고객사 입니다. 유지보수 등록 장비를 삭제 합니다.')){
								nkia.ui.utils.hideProgress({id: 'app', isMask: true});
								return;
							}
							$$("requst_regist")._setFieldValue("servercarry_nm",0);
							$$("requst_regist")._setFieldValue("servercarry",null);
							$$("requst_regist")._setFieldValue("networkcarry_nm",0);
							$$("requst_regist")._setFieldValue("networkcarry",null);
							$$("requst_regist")._setFieldValue("securitycarry_nm",0);
							$$("requst_regist")._setFieldValue("securitycarry",null);
							$$("requst_regist")._setFieldValue("storagecarry_nm",0);
							$$("requst_regist")._setFieldValue("storagecarry",null);
							$$("requst_regist")._setFieldValue("etccarry_nm",0);
							$$("requst_regist")._setFieldValue("etccarry",null);
							$$("requst_regist")._setFieldValue("rackcarry_nm",0);
							$$("requst_regist")._setFieldValue("rackcarry",null);
							$$("requst_regist")._setFieldValue("facilitycarry_nm",0);
							$$("requst_regist")._setFieldValue("facilitycarry",null);
						}else if (after < before){
							if(!confirm('해당 고객사는 유지보수 시스템을 이용하는 고객사 입니다. 유지보수 등록을 하지 않은 장비를 삭제 합니다.')){
								nkia.ui.utils.hideProgress({id: 'app', isMask: true});
								return;
							}
							$$("requst_regist")._setFieldValue("servercarry_nm",0);
							$$("requst_regist")._setFieldValue("servercarry",null);
							$$("requst_regist")._setFieldValue("networkcarry_nm",0);
							$$("requst_regist")._setFieldValue("networkcarry",null);
							$$("requst_regist")._setFieldValue("securitycarry_nm",0);
							$$("requst_regist")._setFieldValue("securitycarry",null);
							$$("requst_regist")._setFieldValue("storagecarry_nm",0);
							$$("requst_regist")._setFieldValue("storagecarry",null);
							$$("requst_regist")._setFieldValue("etccarry_nm",0);
							$$("requst_regist")._setFieldValue("etccarry",null);
							$$("requst_regist")._setFieldValue("rackcarry_nm",0);
							$$("requst_regist")._setFieldValue("rackcarry",null);
							$$("requst_regist")._setFieldValue("facilitycarry_nm",0);
							$$("requst_regist")._setFieldValue("facilitycarry",null);
						}
					}
				}

				values[fields.id] = treeItem.node_id;	// 멀티코드 ID
				values[fields.name] = treeItem.text;		// 멀티코드 명 
				
	            reference.form._setValues(values)
	            this.window.close();
	
			}else{
				nkia.ui.utils.notification({
	                type: 'info',
	                message: '마지막 노드만 선택가능합니다.'
	            });
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 코드정보가 없습니다.'
            });
        }
	};
};

/**
 * @method {} createMultiCodeWindow : Form Function
 * @returns {object} form
 */
function createMultiCodeWindow(popupProps) {
	var props = popupProps||{};
	var multiCode;
	try {
		multiCode = new nkia.ui.popup.nbpm.MultiCode();
		multiCode.setRender(props);
	} catch(e) {
		alert(e.message);
	}
}

/**
 * NBPM 윈도우 팝업을 오픈한다.
 */
//체크리스트
function openNbpmWindowPopup(popupId, paramMap, optMap) {
	if("" == nullToSpace(popupId)){
		alert("Nbpm팝업오픈 popupId 은 필수 입력입니다.");
	}else if("" == nullToSpace(paramMap["fieldName"])){
		alert("Nbpm팝업오픈 fieldName 은 필수 입력입니다.");
		return false;
	}else if("" == nullToSpace(paramMap["areaName"])){
		alert("Nbpm팝업오픈 areaName 은 필수 입력입니다.");
		return false;
	}
	
	var url = "/itg/customize/ubit/openWindowPopup.do?";
	if("workCheckList" == nullToSpace(paramMap["packageName"])){
		url = "/itg/workflow/workCheckList/openWindowPopup.do?";
	}
	var paramStr = "&popupId=" + popupId;
	for(var key in paramMap){
		paramStr += "&" + key + "=" + paramMap[key]; 
	}
	url += paramStr;

	var width = "1050";
	var height = "570";
	if("" != nullToSpace(optMap["width"])){
		width = optMap["width"];
	}
	if("" != nullToSpace(optMap["height"])){
		height = optMap["height"];
	}
	
	var optStr = "width="+width+",height="+height+",history=no,resizable=yes,status=no,scrollbars=no,member=no";
	var popup = window.open(url, popupId, optStr);
}

/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.pmCausePopup = function() {
	this.reference = {};
	this.pmAffcteOptions = {};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		
		var editform = createFormComp({
			id: "pmCauseEditForm",
			header: {
				title: "긴급도/영향도 선택"
			},
			fields: {
				colSize: 1, // 열 수
				items: [
					{ colspan: 1, item: createCodeComboBoxComp({label: "긴급도", name: "pm_emrgncy_ty_cd", code_grp_id: 'PM_EMRGNCY_TY_CD', attachChoice: true, required: true }) }
				  , { colspan: 1, item: createCodeComboBoxComp({label: "영향도", name: "pm_affcte_ty_cd", code_grp_id: 'PM_AFFCTE_TY_CD', attachChoice: true, required: true }) }
				  , { colspan: 1, item: createTextFieldComp({label: "문제 등급", name: "pm_level", required: true, readonly: true }) }
				]
			}
		});
	
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: editform
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
		
		var form = $$('pmCauseEditForm');
		var pmEmrgncyTyCd = form._getField("pm_emrgncy_ty_cd"); // 긴급도  (A/B/C, 상/중/하)
		var pmAffcteTyCd = form._getField("pm_affcte_ty_cd"); // 영향도 (1/2/3/4, 1등급/../4등급)
		
		// 긴급도 값 변경시
		pmEmrgncyTyCd.attachEvent('onChange',function(newVal, oldVal){
			
			form._setFieldValue('pm_affcte_ty_cd', '');
			
//			var pmEmrgncyTyCdValue = $$('pmCauseEditForm')._getFieldValue('pm_emrgncy_ty_cd');  
//			var pmAffcteTyCdValue = $$('pmCauseEditForm')._getFieldValue('pm_affcte_ty_cd');
//			var pmAffcteCodeCombo = $$('pmCauseEditForm')._getField("pm_affcte_ty_cd");
//			
//			pmAffcteCodeCombo._reload({code_grp_id : 'PM_AFFCTE_TY_CD'});
//			
//			if(pmEmrgncyTyCdValue == 'A'){
//				$$('pmCauseEditForm')._getField("pm_affcte_ty_cd").config.options.splice(4, 1);
//				pmAffcteCodeCombo.refresh();
//			
//			}else if(pmEmrgncyTyCdValue == 'B'){
//				$$('pmCauseEditForm')._getField("pm_affcte_ty_cd").config.options.splice(4, 1);
//				pmAffcteCodeCombo.refresh();
//				
//			}else if(pmEmrgncyTyCdValue == 'C'){
//				$$('pmCauseEditForm')._getField("pm_affcte_ty_cd").config.options.splice(1, 1);
//				pmAffcteCodeCombo.refresh();
//				
//			}
		});
		
		pmAffcteTyCd.attachEvent('onItemClick', function(id, e){
			var pmEmrgncyTyCdValue = $$('pmCauseEditForm')._getFieldValue('pm_emrgncy_ty_cd');  
			var pmAffcteTyCdValue = $$('pmCauseEditForm')._getFieldValue('pm_affcte_ty_cd');
			
			if(pmEmrgncyTyCdValue == ''){
				form._setFieldValue('pm_affcte_ty_cd', '');
				
				if(pmAffcteTyCdValue == ''){
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '긴급도를 선택하세요.'
		            });
				}
				
				return false;
			}
			
		});
			
		// 영향도 값 변경시
		pmAffcteTyCd.attachEvent('onChange',function(newVal, oldVal){
			var pmEmrgncyTyCdValue = $$('pmCauseEditForm')._getFieldValue('pm_emrgncy_ty_cd');  
			var pmAffcteTyCdValue = $$('pmCauseEditForm')._getFieldValue('pm_affcte_ty_cd');
			
			if(pmEmrgncyTyCdValue != '' && pmAffcteTyCdValue != ''){
				
				var paramMap = {};
				// paramMap['pm_emrgncy_ty_cd'] = pmEmrgncyTyCdValue;
				// paramMap['pm_affcte_ty_cd'] = pmAffcteTyCdValue;
				paramMap['code_grp_id'] = 'PM_LEVEL';
				paramMap['code_key'] = pmEmrgncyTyCdValue + pmAffcteTyCdValue;
				
				nkia.ui.utils.ajax({
					url: "/itg/system/code/selectCompoundCodeValue.do",
					params: paramMap,
					success: function(data) {
						var resultData = data.resultMap;
						form._setFieldValue('pm_level', resultData.CODE_VALUE);
					}
				})
			}
		});
	};
	
	this.choice = function(){

		var form = $$('pmCauseEditForm');
		if(form._validate()){
			var reference = this.props.reference;
			
			var values = {};
			values['pm_level'] = form._getFieldValue('pm_level');
			values['pm_level_nm'] = form._getFieldValue('pm_level');
			values['pm_affcte_ty_cd'] = form._getFieldValue('pm_affcte_ty_cd');
			values['pm_emrgncy_ty_cd'] = form._getFieldValue('pm_emrgncy_ty_cd');
			
			reference.form._setValues(values);
			this.window.close();
		}
	};
};

/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.cmRiskPopup = function() {
	this.reference = {};
	this.pmAffcteOptions = {};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		
		var editform = createFormComp({
			id: "cmCauseEditForm",
			header: {
				title: "긴급도/영향도 선택"
			},
			fields: {
				colSize: 1, // 열 수
				items: [
					{ colspan: 1, item: createCodeComboBoxComp({label: "긴급도", name: "cm_emrgncy_ty_cd", code_grp_id: 'CM_EMRGNCY_TY_CD', attachChoice: true, required: true }) }
				  , { colspan: 1, item: createCodeComboBoxComp({label: "영향도", name: "cm_affcte_ty_cd", code_grp_id: 'CM_AFFCTE_TY_CD', attachChoice: true, required: true }) }
				  , { colspan: 1, item: createTextFieldComp({label: "변경영향도", name: "incdntGradEditForm", required: true, readonly: true }) }
				]
			}
		});
	
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: editform
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
		
		var form = $$('cmCauseEditForm');
		var pmEmrgncyTyCd = form._getField("cm_emrgncy_ty_cd"); // 긴급도  (A/B/C, 상/중/하)
		var pmAffcteTyCd = form._getField("cm_affcte_ty_cd"); // 영향도 (1/2/3/4, 1등급/../4등급)
		
		// 긴급도 값 변경시
		pmEmrgncyTyCd.attachEvent('onChange',function(newVal, oldVal){

			form._setFieldValue('cm_affcte_ty_cd', '');
	
		});
		
		pmAffcteTyCd.attachEvent('onItemClick', function(id, e){
			var pmEmrgncyTyCdValue = $$('cmCauseEditForm')._getFieldValue('cm_emrgncy_ty_cd');  
			var pmAffcteTyCdValue = $$('cmCauseEditForm')._getFieldValue('cm_affcte_ty_cd');
			
			if(pmEmrgncyTyCdValue == ''){
				form._setFieldValue('cm_affcte_ty_cd', '');
				
				if(pmAffcteTyCdValue == ''){
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '긴급도를 선택하세요.'
		            });
				}
				
				return false;
			}
			
		});
			
		// 영향도 값 변경시
		pmAffcteTyCd.attachEvent('onChange',function(newVal, oldVal){
			var pmEmrgncyTyCdValue = $$('cmCauseEditForm')._getFieldValue('cm_emrgncy_ty_cd');  
			var pmAffcteTyCdValue = $$('cmCauseEditForm')._getFieldValue('cm_affcte_ty_cd');
			
			if(pmEmrgncyTyCdValue != '' && pmAffcteTyCdValue != ''){
				
				var paramMap = {};
				// paramMap['pm_emrgncy_ty_cd'] = pmEmrgncyTyCdValue;
				// paramMap['pm_affcte_ty_cd'] = pmAffcteTyCdValue;
				paramMap['code_grp_id'] = 'CM_RISK';
				paramMap['code_key'] = pmEmrgncyTyCdValue + pmAffcteTyCdValue;
				
				nkia.ui.utils.ajax({
					url: "/itg/system/code/selectCompoundCodeValue.do",
					params: paramMap,
					success: function(data) {
						var resultData = data.resultMap;
						form._setFieldValue('cm_risk', resultData.CODE_VALUE);
					}
				})
			}
		});
	};
	
	this.choice = function(){

		var form = $$('cmCauseEditForm');
		if(form._validate()){
			var reference = this.props.reference;
			
			var values = {};
			values['cm_risk'] = form._getFieldValue('cm_risk');
			values['cm_level_nm'] = form._getFieldValue('cm_level');
			values['cm_affcte_ty_cd'] = form._getFieldValue('cm_affcte_ty_cd');
			values['cm_emrgncy_ty_cd'] = form._getFieldValue('cm_emrgncy_ty_cd');
			
			reference.form._setValues(values);
			this.window.close();
		}
	};
};

/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.incdntGradPopup = function() {
	this.reference = {};
	this.pmAffcteOptions = {};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		
		var editform = createFormComp({
			id: "incdntGradEditForm",
			header: {
				title: "긴급도/우선순위 선택"
			},
			fields: {
				colSize: 1, // 열 수
				items: [
					{ colspan: 1, item: createCodeComboBoxComp({label: "긴급도", name: "emrgncy_ty_cd", code_grp_id: 'EMRGNCY_TY_CD', attachChoice: true, required: true }) }
				  , { colspan: 1, item: createCodeComboBoxComp({label: "우선순위", name: "priort_ty_cd", code_grp_id: 'PRIORT_TY_CD', attachChoice: true, required: true }) }
				  , { colspan: 1, item: createTextFieldComp({label: "장애등급", name: "incdnt_grad", required: true, readonly: true }) }
				]
			}
		});
	
		_this.window = createWindow({
	    	id: props.id,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: editform
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
		
		var form = $$('incdntGradEditForm');
		var emrgncyTyCd = form._getField("emrgncy_ty_cd"); // 긴급도  (A/B/C, 상/중/하)
		var priortTyCd = form._getField("priort_ty_cd"); // 영향도 (1/2/3/4, 1등급/../4등급)
		
		// 긴급도 값 변경시
		emrgncyTyCd.attachEvent('onChange',function(newVal, oldVal){

			form._setFieldValue('priort_ty_cd', '');
	
		});
		
		priortTyCd.attachEvent('onItemClick', function(id, e){
			var emrgncyTyCdValue = $$('incdntGradEditForm')._getFieldValue('emrgncy_ty_cd');  
			var priortTyCdValue = $$('incdntGradEditForm')._getFieldValue('priort_ty_cd');
			
			if(emrgncyTyCdValue == ''){
				form._setFieldValue('priort_ty_cd', '');
				
				if(priortTyCdValue == ''){
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '긴급도를 선택하세요.'
		            });
				}
				
				return false;
			}
			
		});
			
		// 영향도 값 변경시
		priortTyCd.attachEvent('onChange',function(newVal, oldVal){
			var emrgncyTyCdValue = $$('incdntGradEditForm')._getFieldValue('emrgncy_ty_cd');  
			var priortTyCdValue = $$('incdntGradEditForm')._getFieldValue('priort_ty_cd');
			
			if(emrgncyTyCdValue != '' && priortTyCdValue != ''){
				
				var paramMap = {};
				// paramMap['pm_emrgncy_ty_cd'] = emrgncyTyCdValue;
				// paramMap['pm_affcte_ty_cd'] = priortTyCdValue;
				paramMap['code_grp_id'] = 'INCDNT_GRAD';
				paramMap['code_key'] = emrgncyTyCdValue + priortTyCdValue;
				
				nkia.ui.utils.ajax({
					url: "/itg/system/code/selectCompoundCodeValue.do",
					params: paramMap,
					success: function(data) {
						var resultData = data.resultMap;
						form._setFieldValue('incdnt_grad', resultData.CODE_VALUE);
					}
				})
			}
		});
	};
	
	this.choice = function(){

		var form = $$('incdntGradEditForm');
		if(form._validate()){
			var reference = this.props.reference;
			
			var values = {};
			values['incdnt_grad'] = form._getFieldValue('incdnt_grad');
			//values['cm_level_nm'] = form._getFieldValue('cm_level');
			values['priort_ty_cd'] = form._getFieldValue('priort_ty_cd');
			values['emrgncy_ty_cd'] = form._getFieldValue('emrgncy_ty_cd');
			
			reference.form._setValues(values);
			this.window.close();
		}
	};
};

/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.createDmlComp = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
//	this.width = 1000;
//	this.height = 400;
	this.url = "/itg/base/searchCodeDataList.do";
	this.resource = "grid.process.work.code";
	this.params = {code_grp_id : "DML_CD"};
	this.reference = {};
	this.type = "default";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.grid = [];
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: props.title
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			select: true,
			pageable: false,
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
		
		_this.window = createWindow({
	    	id: props.id,
	    	height: 350,
	    	width: 600,
	    	body: {
	    		rows: this.grid
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var selectedRow = $$(this.id + '_grid')._getSelectedRows();
		if(selectedRow){
			var reference = this.props.reference;
			var fields = reference.fields;
			
			var values = {};
			values[fields.name] = selectedRow[0].CODE_ID;  
			
            reference.form._setValues(values);
            this.window.close();
			
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 코드정보가 없습니다.'
            });
      }
	};
};

/********************************************
 * Date: 2019-11-13
 * Version:
 *	UBIT dnsSelect 팝업
 ********************************************/
nkia.ui.popup.nbpm.dnsSelectZone = function() {
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var searchForm = createFormComp({
			id: props.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item : createUnionFieldComp({
	                            items: [
	                    			createTextFieldComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.dns.record.000003'}),
	                    				name: "zone_id_nm",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									//onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	},
					{ item : createUnionFieldComp({
						items: [
							createCodeComboBoxComp({ //관리구분
								label: getConstText({isArgs : true, m_key : 'res.dns.record.000001'}),
								name: 'mgmt_place',
								code_grp_id: 'MGMT_PLACE',
								attachChoice: true
							})
							]
						})
					}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});

		var grid = createGridComp({
			id: props.id + '_grid',
			header: {
	        	title: "Zone 목록"
		    },
			checkbox: false,
			//minHeight: 100,
		    resource: "grid.process.service.zoneSerch",
		    params: {},
			url: "/itg/itam/dns/searchDnsZoneList.do",
			pageable: true,
			pageSize: 20,
			//keys: ["AM_RECORD_ID"],
			//dependency: this.dependency
		});
		
		var bodyScrollView = {
				view : "scrollview",
				body : {
					cols : [ {
						rows : new Array().concat(searchForm,grid)
					} ]
				}
			};
		
		_this.window = createWindow({
	    	id: props.id,
	    	width : nkia.ui.utils.getWidePopupWidth(),
	    	height : nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: [ bodyScrollView ]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.props.id + '_searchForm');
		var data = form._getValues();

		$$(this.props.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	this.choice = function(){
		var gridItem = $$(this.props.id + "_grid").getSelectedItem();
		if(gridItem){
			
			if(gridItem){
				var reference = this.props.reference;
				var fields = reference.fields;
				
				var values = {};
	
				values[fields.id] = gridItem.ZONE_ID;	// 멀티코드 ID
				values[fields.name] = gridItem.ZONE_ID_NM;		// 멀티코드 명 
				
	            reference.form._setValues(values)
	            
	            var gridRows = $$("record_insert")._getRows();
                            
	            if(gridRows.length >0){
	            	
                    for (var r=0; r<gridRows.length; r++) {
                        gridRows[r]["AM_ZONE_ID"] = gridItem.ZONE_ID;
                    }
                    $$("record_insert").refresh();
                }
	   
	            this.window.close();
	
			}else{
				nkia.ui.utils.notification({
	                type: 'info',
	                message: '마지막 노드만 선택가능합니다.'
	            });
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 코드정보가 없습니다.'
            });
        }
	};
};


/********************************************
 * Date: 2019-11-27
 * Version:
 *	UBIT 관제 팝업.
 ********************************************/
nkia.ui.popup.nbpm.monitorPopup = function() {
	var global;
	var global_props;
	var paramMap;	
	var monitor_popup_type;
	var selectRow = null;
	var selecturl = null;
	this.window = {};
	var grid_key = null;

	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		global = _this;
		global_props = _this.props;
		monitor_popup_type = _this.props.popUpType;
		 
		if($$("requst_regist")._getFieldValue("req_doc_id") == "CD0000002234"){	//CD0000002234 관제서비스 등록
			selecturl = '/itg/itam/monitormanagement/searchMonitorManagementList.do';
		}			
		else if($$("requst_regist")._getFieldValue("req_doc_id") == "CD0000000186"){  //CD0000002236 관제 POC 등록
			selecturl = '/itg/itam/monitormanagement/searchMonitorPocManagementList.do';			
		}
		
		if(monitor_popup_type != "monitoringweburl"){
			grid_key = "CONF_ID";
		}else{
			grid_key = "WEBURL_PAGE_NM";
		}
		
		paramMap = ( typeof props.params === undefined ) ? {} : props.params;		
		
		var fieldItems = [];
		fieldItems.push({
			colspan : 1,
			item : createPopBtnFieldComp({
				label : '호스트명',
				name:'HOST_NM',
				required : true,
				readonly : true,
				popUpType : "gethostpopup",
				srId : "$!{dataMap.sr_id}"
				
			})
		});
		fieldItems.push(/*IP*/{colspan: 1, item: createTextFieldComp({ label: 'IP', name: "MAIN_IP", readonly: true, maxlength : 300 }) });
		fieldItems.push(/*분류체계*/{colspan: 1, item: createTextFieldComp({ label: '분류체계', name: "CLASS_ID_NM",  readonly: true, maxlength : 300 }) });
		fieldItems.push(/*OS버전*/{colspan: 1, item: createTextFieldComp({ label: 'OS버전', name: "VERSION",  readonly: true, maxlength : 300 }) });
		fieldItems.push(/*업무명*/{colspan: 1, item: createTextFieldComp({ label: '업무명', name: "CONF_NM", readonly: true, maxlength : 300 }) });
		fieldItems.push(/*담당자*/{colspan: 1, item: createTextFieldComp({ label: '담당자', name: "MAIN_USER_ID_NM",  readonly: true, maxlength : 300 }) });
		fieldItems.push(/*모델명*/{colspan: 1, item: createTextFieldComp({ label: '모델명', name: "MODEL", readonly: true, maxlength : 300 }) });			
		fieldItems.push({colspan: 1, item: createTextFieldComp({ label: '벤더명', name: "VENDOR_CD_NM",readonly: true, maxlength : 300 }) });
		/*getConstText({ isArgs : true, m_key : 'res.common.label.maker'})*/

		//공통영역 끝		
		switch(monitor_popup_type){	
		case  'monitoringnms': //OK		
			fieldItems.push({
				colspan: 1, item :  createCodeComboBoxComp({ 
					label: 'SNMP Version', name: 'NMS_SNMP_VER', code_grp_id: 'NMS_SNMP_VER', attachChoice: true, required: true,
					on:{
						onChange : function(newVal, oldVal){
							var nms_column_array = ["NMS_SM_USER_NM","NMS_SM_SECU_INFO", "NMS_SM_AUTH_ALGO", "NMS_SM_AUTH_PASS", "NMS_SM_PRI_ALGO", "NMS_SM_PRI_PASS"];
							
							if(newVal == "SNMPV3"){
								$$(props.id + '_form').elements["NMS_COM_NM_CN"].define("required", false);
								$$(props.id + '_form')._setFieldValue("NMS_COM_NM_CN",null);
								$$(props.id + '_form').elements["NMS_COM_NM_CN"].refresh();
								$$(props.id + '_form')._hideField('NMS_COM_NM_CN',true);
								
								$$(props.id + '_form').elements["NMS_SM_SECU_INFO"].define("required", true);		
								$$(props.id + '_form')._setFieldValue("NMS_SM_SECU_INFO",null);
								$$(props.id + '_form').elements["NMS_SM_SECU_INFO"].refresh();
								$$(props.id + '_form')._showField("NMS_SM_SECU_INFO",true);		
							}
							else if(newVal == ""){								
								for(var i=0; i < nms_column_array.length; i++){		
									$$(props.id + '_form').elements[nms_column_array[i]].define("required", false);
									$$(props.id + '_form')._setFieldValue(nms_column_array[i],null);
									$$(props.id + '_form').elements[nms_column_array[i]].refresh();
									$$(props.id + '_form')._hideField(nms_column_array[i],true);
								}													
								$$(props.id + '_form').elements["NMS_COM_NM_CN"].define("required", false);
								$$(props.id + '_form')._setFieldValue("NMS_COM_NM_CN",null);
								$$(props.id + '_form').elements["NMS_COM_NM_CN"].refresh();
						    	$$(props.id + '_form')._hideField('NMS_COM_NM_CN',true);
							}
							else{								
								for(var i=0; i < nms_column_array.length; i++){
									
									$$(props.id + '_form').elements[nms_column_array[i]].define("required", false);
									$$(props.id + '_form')._setFieldValue(nms_column_array[i],null);
									$$(props.id + '_form').elements[nms_column_array[i]].refresh();
									$$(props.id + '_form')._hideField(nms_column_array[i],true);
								
								}
											
								$$(props.id + '_form').elements["NMS_COM_NM_CN"].define("required", true);		
								$$(props.id + '_form')._setFieldValue("NMS_COM_NM_CN",null);
								$$(props.id + '_form').elements["NMS_COM_NM_CN"].refresh();
								$$(props.id + '_form')._showField('NMS_COM_NM_CN',true);	
							
						
						    			
							}

						}
					}
					})
				
			});	
			fieldItems.push({
				colspan: 1, item :  createCodeComboBoxComp({ 
					label: '보안연결 정보', name: 'NMS_SM_SECU_INFO', code_grp_id: 'NMS_SM_SECU_INFO', attachChoice: true, required: false, 
					on:{
						onChange : function(newVal, oldVal){
							var nms_column_array = ["NMS_SM_USER_NM", "NMS_SM_AUTH_ALGO", "NMS_SM_AUTH_PASS", "NMS_SM_PRI_ALGO", "NMS_SM_PRI_PASS"];
							
							if(newVal == "AUTH_NOPRIVACY"){		
								for(var i=3; i<5; i++){
									$$(props.id + '_form').elements[nms_column_array[i]].define("required", false);
									$$(props.id + '_form')._setFieldValue(nms_column_array[i],null);
									$$(props.id + '_form').elements[nms_column_array[i]].refresh();
									$$(props.id + '_form')._hideField(nms_column_array[i],true);	
								}				
								for(var i=0; i < 3; i++){
									$$(props.id + '_form').elements[nms_column_array[i]].define("required", true);
									$$(props.id + '_form')._setFieldValue(nms_column_array[i],null);
									$$(props.id + '_form').elements[nms_column_array[i]].refresh();
									$$(props.id + '_form')._showField(nms_column_array[i],true);		
								}						
							}							
							else if(newVal == "AUTH_PRIVACY"){								
								for(var i=0; i < nms_column_array.length; i++){									
									$$(props.id + '_form').elements[nms_column_array[i]].define("required", true);
									$$(props.id + '_form')._setFieldValue(nms_column_array[i],null);
									$$(props.id + '_form').elements[nms_column_array[i]].refresh();
									$$(props.id + '_form')._showField(nms_column_array[i],true);							
								}    			
							}
							else if(newVal == "NOAUTH_NOPRIVACY"){
								for(var i=1; i<5; i++){
									$$(props.id + '_form').elements[nms_column_array[i]].define("required", false);
									$$(props.id + '_form')._setFieldValue(nms_column_array[i],null);
									$$(props.id + '_form').elements[nms_column_array[i]].refresh();
									$$(props.id + '_form')._hideField(nms_column_array[i],true);	
								}		
								$$(props.id + '_form').elements["NMS_SM_USER_NM"].define("required", true);
								$$(props.id + '_form')._setFieldValue("NMS_SM_USER_NM",null);
								$$(props.id + '_form').elements["NMS_SM_USER_NM"].refresh();
								$$(props.id + '_form')._showField("NMS_SM_USER_NM",true);										
								
							}
							else {								
								for(var i=0; i < nms_column_array.length; i++){		
									$$(props.id + '_form').elements[nms_column_array[i]].define("required", false);
									$$(props.id + '_form')._setFieldValue(nms_column_array[i],null);
									$$(props.id + '_form').elements[nms_column_array[i]].refresh();
									$$(props.id + '_form')._hideField(nms_column_array[i],true);
								}																					
							}

						}
					}
					})
				
			});	
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'Community Name', name: "NMS_COM_NM_CN", required: false, maxlength : 100 }) });			
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'SNMP3_유저이름', name: "NMS_SM_USER_NM", required: false, maxlength : 100 }) });			
			fieldItems.push({colspan: 1, item :  createCodeComboBoxComp({ label: 'SNMP3_Auth알고리즘', name: 'NMS_SM_AUTH_ALGO', code_grp_id: 'NMS_SM_AUTH_ALGO', attachChoice: true, required: false })});	
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'SNMP3_Auth패스워드', name: "NMS_SM_AUTH_PASS", required: false, maxlength : 100 }) });			
			fieldItems.push({colspan: 1, item :  createCodeComboBoxComp({ label: 'SNMP3_Privacy알고리즘', name: 'NMS_SM_PRI_ALGO', code_grp_id: 'NMS_SM_PRI_ALGO', attachChoice: true, required: false })});			
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'SNMP3_Privacy비밀번호', name: "NMS_SM_PRI_PASS", required: false, maxlength : 100 }) });												
			break;	
		case  'monitoringdbms': 
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'DB벤더/DB버전', name: "DBMS_VENDOR_NM", required: true, maxlength : 100 }) });
			//fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'DB Version', name: "DBMS_VERSION", required: true, maxlength : 100 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'SID', name: "DBMS_SID", required: true, maxlength : 50 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'Port', name: "DBMS_PORT", required: true, maxlength : 10 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: '접속 계정', name: "DBMS_ACCOUNT", required: true, maxlength : 50 }) });

			break;	
		case  'monitoringwas':  //OK
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'Was 종류/Version', name: "WAS_VERSION", required: true, maxlength : 100 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'JDK버전 (5.0 미만 설치 불가)', name: "WAS_JDK_VERSION", required: true, maxlength : 100 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'JVM옵션 설정파일', name: "WAS_CONF_FILE_CN", required: true, maxlength : 100 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'WPM설치 경로', name: "WAS_WPM_PATH_CN", required: true, maxlength : 255 }) });									
			break;		
		case  'monitoringvmm': //OK
			fieldItems.push({colspan: 1, item :  createCodeComboBoxComp({ label: '하이퍼바이저', name: 'VMM_HYPER', code_grp_id: 'VMM_HYPER', filteredData: ['HYPERV', 'OPENSTACK'], attachChoice: true, required: true })});			
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: '버전', name: "VMM_VERSION", required: true, maxlength : 100 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'API서버 접속URL', name: "VMM_API_URL_CN", required: true, maxlength : 255 }) });
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy1'})});
			break;	
		case  'monitoringtms': //OK
			fieldItems.push({colspan: 1, item :  createCodeComboBoxComp({ label: 'Flow Type', name: 'TMS_FLOW_TYPE', code_grp_id: 'TMS_FLOW_TYPE', attachChoice: true, required: true })});			
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'Port', name: "TMS_PORT", required: true, maxlength : 100 }) });
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy1'})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy2'})});
			break;	
		case 'monitoringweburl': //OK
			fieldItems = [];
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: '홈페이지명', name: "WEBURL_PAGE_NM", required: true, maxlength : 200 }) });
			fieldItems.push({colspan: 1, item: createTextFieldComp({ label: 'URL', name: "WEBURL_PAGE_URL_CN", required: true, maxlength : 300 }) });	
			break;			
		}			

		//폼
		var Form = createFormComp({
			id: props.id + '_form',	
			header: {
	        	title: getConstText({isArgs : true, m_key : 'res.label.itam.entity.00016'})
		    },
			fields: {
				colSize: 3,
				items: fieldItems,
			    hiddens: {
			    	CUSTOMER_ID : "", 		    	
		        	MONITOR_ID : "",
		       		MONITOR_TYPE : "",
		       		id : "",
		       		ASSET_ID : "",
		       		CONF_ID : ""				
			    }
			},			
			footer: {
	        	buttons : {
	        	 	align: "center",
		            items: [
		                createBtnComp({
		                	id:"saveGrid",
		                	label: '저장',
		                	type:"form",
		                	visible: false,
		                	click: this.doInsert.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
			}
		});

		var Grid = createGridComp({
			id : props.id + '_grid',
			header : {
				title: getConstText({isArgs : true, m_key : 'res.title.common.result'}),
				buttons : {
					align : "right",
					items : [
								createBtnComp({
									id:"btn1",
									label:getConstText({ isArgs : true, m_key : 'res.common.insert'}) /*등록*/,
									type: "form",
									click: function() {
										
										if(monitor_popup_type == "monitoringnms"){
											$$(props.id + '_form')._setFieldValue('NMS_SNMP_VER',null);
											$$(props.id + '_form')._setFieldValue('NMS_SM_SECU_INFO',null);
											$$(props.id + '_form')._setFieldValue('NMS_SM_AUTH_ALGO',null);
											$$(props.id + '_form')._setFieldValue('NMS_SM_PRI_ALGO',null);											
										}
										if(monitor_popup_type == "monitoringvmm"){
											$$(props.id + '_form')._setFieldValue('VMM_HYPER',null);											
										}
										if(monitor_popup_type == "monitoringtms"){
											$$(props.id + '_form')._setFieldValue('TMS_FLOW_TYPE',null);											
										}
										$$(props.id + '_form')._reset();
										$$(props.id + '_form')._setControl({editorMode: true});
										
										$$('saveGrid').define("label", "추가");
										$$('saveGrid').refresh();
										
										nkia.ui.utils.shows(['saveGrid']);
										
										
										selectRow = null;
										
									}
								}),
						
								createBtnComp({
									id:"btn2",
									label: getConstText({ isArgs : true, m_key : 'res.common.update'})  /*수정*/,
									type: "form",
									click: function() {
										if(selectRow){
											$$(props.id + '_form')._setControl({editorMode: true});												
											$$(props.id + '_form')._setValues(selectRow);
											
											if(monitor_popup_type != "monitoringweburl")
												$$(props.id + '_form')._setFieldValue($$(props.id + '_form')._getField("HOST_NM"),selectRow["HOST_NM"]);
											$$('saveGrid').define("label", "수정");
											$$('saveGrid').refresh();
											nkia.ui.utils.shows(['saveGrid']);
										}else {
											alert("로우을 선택해 주세요");
											return false;
										}
									}
								}),
								createBtnComp({
									id:"btn3",
									label: getConstText({ isArgs : true, m_key : 'res.common.delete'}) /*삭제*/,
									type: "form",
									click: function() {
									
										if(selectRow){
											$$(props.id + '_form')._setControl({editorMode: false});
											var work_row = $$(props.id + '_grid').getSelectedItem();																									
											$$(props.id + '_grid')._removeRow($$(props.id + '_grid')._getSelectedRows().id);
											
									//		$$(props.id + '_form')._setControl({editorMode: false});
											nkia.ui.utils.hides(['saveGrid']);
											$$(props.id + '_form')._reset();
										}else {
											alert("로우을 선택해 주세요");
											return false;
										}
					
									
									}
								}),
								createBtnComp({
									id:"btn5",
									label: getConstText({ isArgs : true, m_key : 'res.billing.cntrct.00009'}) /*삭제*/,
									type: "form",
									click: function() {
								
										
										var reference = _this.props.reference;
										var fields = reference.fields;
										
										var param = {
												custoemr_id : $$("requst_regist")._getFieldValue("customer_id"),
												sr_id: $$("requst_regist")._getFieldValue("sr_id"),
												monitor_type : fields.id
										};
										
										var excelAttrs = {
												sheet: [
													{ 
													  sheetName		: getConstText({isArgs : true, m_key : 'res.monitoring.label.00002'}), 
													  titleName		: getConstText({isArgs : true, m_key : 'res.monitoring.label.00002'}),
													  headerNames	: getConstText({isArgs : true, m_key : 'grid.itam.monitor.'+monitor_popup_type+'.text'}),
													  includeColumns: getConstText({isArgs : true, m_key : 'grid.itam.monitor.'+monitor_popup_type+'.header'}),
													}
												]
											};
											var excelParam = {};
											excelParam['fileName'] = getConstText({isArgs : true, m_key : 'res.monitoring.label.00002'}),
											excelParam['excelAttrs'] = excelAttrs;
											goExcelDownLoad("/itg/itam/monitormanagement/searchMonitorListExcelDown.do", param, excelParam);
											
									
									}
									
								})								
								
							]
				}
			},
			checkbox : false,
			MaxHeight : 100,
			resource : 'grid.itam.monitor.'+monitor_popup_type,					
			url : selecturl,			
			params : {			
				class_mng_type : "",
				sr_id: $$("requst_regist")._getFieldValue("sr_id"),
				monitor_type : monitor_popup_type				
			},
			pageable : false,
			autoLoad : false,	
			keys : [ grid_key ],			
			on:{
				onItemClick: this.gridCellClick.bind(_this)
			}
		});
			
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
			        	rows: new Array().concat(Form, Grid)
			        }
			};
		 
		_this.window = createWindow({
	    	id: props.id,	  
	    	//width: 1000,		    	
	    	//height : 800,
	    	width : nkia.ui.utils.getWidePopupWidth(),	    	
	    	height : nkia.ui.utils.getWidePopupHeight()	,
	    	header: {
	    		title: props.title
	    	},
	        body: {
	        	rows: [bodyScrollView]
	        },	    	
	        footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저장', id : 'btn4', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable
	    });
		
		var reference = this.props.reference;
		var fields = reference.fields;
		
		
		switch(monitor_popup_type){
		case 'monitoringvmm' :
			$$(props.id + '_form')._hideField('dummy1');
				
			break;
		case 'monitoringtms' :
			$$(props.id + '_form')._hideField('dummy1');
			$$(props.id + '_form')._hideField('dummy2');					
			break;
		}
			
		
		
		
		
		
		if(PROCESS_META.TASK_NAME != "start" && PROCESS_META.TASK_NAME != "REQUST_RCEPT"){
			nkia.ui.utils.hides(['saveGrid']);
			nkia.ui.utils.hides(['btn1']);
			nkia.ui.utils.hides(['btn2']);
			nkia.ui.utils.hides(['btn3']);
			nkia.ui.utils.hides(['btn4']);
			nkia.ui.utils.shows(['btn5']);
			
			nkia.ui.utils.hides([props.id + '_form']);
			
						
			if($$("requst_regist")._getFieldValue(fields.id) ){	
				$$(props.id + '_grid')._addRow($$("requst_regist")._getFieldValue(fields.id));				
			}
			else{
					$$(props.id + '_grid')._reload();										
			}
		}else{
			
		    if(monitor_popup_type == 'monitoringnms'){ //OK		monitor_popup_type

		    	$$(props.id + '_form')._hideField('NMS_COM_NM_CN',false);
		    	$$(props.id + '_form')._hideField('NMS_SM_USER_NM',false);
		    	$$(props.id + '_form')._hideField('NMS_SM_AUTH_ALGO',false);
		    	$$(props.id + '_form')._hideField('NMS_SM_AUTH_PASS',false);
		    	$$(props.id + '_form')._hideField('NMS_SM_PRI_ALGO',false);
		    	$$(props.id + '_form')._hideField('NMS_SM_PRI_PASS',false);
		    	$$(props.id + '_form')._hideField('NMS_SM_SECU_INFO',false);
		    	
			
		 
		    }
			nkia.ui.utils.hides(['btn5']);
			if($$("requst_regist")._getFieldValue(fields.id) ){	
				$$(props.id + '_grid')._addRow($$("requst_regist")._getFieldValue(fields.id));				
			}
			else{
					$$(props.id + '_grid')._reload();										
			}
		}
		$$(props.id + '_form')._setControl({editorMode: false});		
	};	

	this.choice = function(){

		var extract_grid = $$(global.props.id + '_grid')._getRows();		
		var reference = this.props.reference;
		var fields = reference.fields;
		var values = {};
				
		values[fields.id] = extract_grid; // 그리드 row 값들.
		values[fields.name] =  extract_grid.length ;
		
		reference.form._setValues(values);
		
		this.window.close();
	};

	// 그리드 더블클릭 버튼 이벤트
	this.gridCellClick = function(){	
		
		selectRow = $$(global.props.id + '_grid').getSelectedItem();

	};
	
	this.doInsert = function(){
		if($$(global.props.id + '_form')._validate()){
			
			var value = $$(global.props.id + '_form')._getValues();
			
			if(monitor_popup_type == "monitoringsms"){
				value["NMS_SNMP_VER"] = $$(global.props.id + '_form')._getField('NMS_SNMP_VER')._getText();
				value["NMS_SM_SECU_INFO"] = $$(global.props.id + '_form')._getField('NMS_SM_SECU_INFO')._getText();
				value["NMS_SM_AUTH_ALGO"] = $$(global.props.id + '_form')._getField('NMS_SM_AUTH_ALGO')._getText();
				value["NMS_SM_PRI_ALGO"] = $$(global.props.id + '_form')._getField('NMS_SM_PRI_ALGO')._getText();
			}
			if(monitor_popup_type == "monitoringvmm"){
				value["VMM_HYPER"] = $$(global.props.id + '_form')._getField('VMM_HYPER')._getText();
			}
			if(monitor_popup_type == "monitoringtms"){
				value["TMS_FLOW_TYPE"] = $$(global.props.id + '_form')._getField('TMS_FLOW_TYPE')._getText();
			}
			var resultStr = '[' + JSON.stringify(value) + ']';	
			
			
			if(selectRow){				
					$$(global.props.id + '_grid').updateItem(selectRow['id'], value);
					nkia.ui.utils.hides(['saveGrid']);				
				
			}else{											
				if(monitor_popup_type != "monitoringweburl"){
					if(value.CONF_ID)
						$$(global.props.id + '_grid')._addRow(JSON.parse(resultStr));
				}else{					
					if(value.WEBURL_PAGE_NM)				
						$$(global.props.id + '_grid')._addRow(JSON.parse(resultStr));
				
				}
				nkia.ui.utils.hides(['saveGrid']);
			}
			$$(global.props.id + '_form')._reset();	
			$$(global.props.id + '_form')._setControl({editorMode: false});
		}
	};
};


/********************************************
 * Date: 2020-02-04
 * Version:
 *	UBIT 관제 팝업 ( SMS 등록)
 ********************************************/
nkia.ui.popup.nbpm.smsmonitorPopup = function() {
	var global;
	var global_props;
	var paramMap;	
	var monitor_popup_type;
	var selectRow = null;
	this.window = {};
	this.bindEvent = null;
	var selecturl = null;
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		global = _this;
		global_props = _this.props;
		monitor_popup_type = _this.props.popUpType;
		
		if($$("requst_regist")._getFieldValue("req_doc_id") == "CD0000002234"){	//CD0000002234 관제서비스 등록
			selecturl = '/itg/itam/monitormanagement/searchMonitorManagementList.do';
		}			
		else if($$("requst_regist")._getFieldValue("req_doc_id") == "CD0000000186"){  //CD0000002236 관제 POC 등록
			selecturl = '/itg/itam/monitormanagement/searchMonitorPocManagementList.do';			
		}
		
		paramMap = ( typeof props.params === undefined ) ? {} : props.params;		
		
		var fieldItems = [];
		fieldItems.push({colspan : 1, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.monitoring.label.itam.opms.00001'}), name : "class_id", popUpType : "amdbPopup", srId : "$!{dataMap.sr_id}"})});
		fieldItems.push({colspan : 1, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.center.move.label.customer.0001'}), name : "customer_id", popUpType : "monitoringcustomer", srId : "$!{dataMap.sr_id}"})});
		fieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.common.label.hostnm'}), name : "host_nm"})});
		fieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.label.itam.oper.00006'}), name : "main_ip", required : false})});
		fieldItems.push({colspan : 1, item : createCodeComboBoxComp({ // 센터구분
				label : getConstText({ isArgs : true, m_key : 'res.label.itam.opms.00020'}),
				name : 'location_cd',
				code_grp_id : 'CENTER',
				attachChoice : true
			}) });
		fieldItems.push({colspan : 1, item : createCodeComboBoxComp({// 고객사
			label : getConstText({ isArgs : true, m_key : 'res.common.status'}),
			name : 'asset_state',
			code_grp_id : 'ASSET_STATE',
			attachChoice : true
		}) });		
		
		//폼
		var Form = createFormComp({
			id: props.id + '_form',				
			header: {
	        	title: getConstText({isArgs : true, m_key : 'res.label.itam.entity.00016'})
		    },
			fields: {
				colSize: 3,
				items: fieldItems,
			    hiddens: {
		       		MONITOR_TYPE : monitor_popup_type,			
			    }
			},			
			footer: {
	        	buttons : {
	        	 	align: "center",
					items: [
							createBtnComp({ //검색
								id:"btn1",
								label:"검색", 
								type: "form", 
								click: this.searchList.bind(_this)
								
							}),
							createBtnComp({ //초기화
								id:"btn2",
								label:"초기화", 
								click: this.resetFormData.bind(_this)
									
								
							
							})
						],
		            css: "webix_layout_form_bottom"
		        }
			}
		});

		var Smsgrid = createGridComp({
			id : props.id + '_emsgrid',
			header : {
				title: getConstText({isArgs : true, m_key : 'res.title.common.result'}),
			},
			
			keys : [ "CONF_ID" ],			
			params : {							
				MONITOR_TYPE : monitor_popup_type				
			},
			url : '/itg/itam/monitormanagement/searchHostInfo.do',
			resource : 'grid.itam.gethostnm' ,				
			//autoLoad : false,
			autoLoad : false,
			pageable : true,
			pageSize : 10,
			resizeColumn : true,
			on : {
				onItemDblClick: this.addUser.bind(_this)
			},
			
			dependency : { // 의존관계 그리드 사용
				targetGridId: props.id + '_grid' // 대상 그리드 id
			},
			minHeight : 200
		});
		
		var Grid = createGridComp({
			id : props.id + '_grid',
			checkbox : true,
			header : {
				title: getConstText({isArgs : true, m_key : 'res.title.common.result'}),
				buttons : {
					align : "right",
					items : [						
								createBtnComp({
									id:"btn3",
									label: getConstText({ isArgs : true, m_key : 'res.common.delete'}) /*삭제*/,
									type: "form",
									click: function() {
									
									
											$$(props.id + '_grid')._removeRow();

											// 사용자 목록 새로고침
											//$$(props.id + '_grid')._reload(null);
										}	
								}),
								createBtnComp({
									id:"btn5",
									label: getConstText({ isArgs : true, m_key : 'res.billing.cntrct.00009'}) /*삭제*/,
									type: "form",
									click: function() {
								
										
										var reference = _this.props.reference;
										var fields = reference.fields;
										
										var param = {
												custoemr_id : $$("requst_regist")._getFieldValue("customer_id"),
												sr_id: $$("requst_regist")._getFieldValue("sr_id"),
												monitor_type : fields.id
										};
										
										var excelAttrs = {
												sheet: [
													{ 
													  sheetName		: getConstText({isArgs : true, m_key : 'res.delmonitoring.label.00001'}), 
													  titleName		: getConstText({isArgs : true, m_key : 'res.delmonitoring.label.00001'}),
													  headerNames	: getConstText({isArgs : true, m_key : 'grid.itam.monitor.'+monitor_popup_type+'.text'}),
													  includeColumns: getConstText({isArgs : true, m_key : 'grid.itam.monitor.'+monitor_popup_type+'.header'}),
													}
												]
											};
											var excelParam = {};
											excelParam['fileName'] = getConstText({isArgs : true, m_key : 'res.delmonitoring.label.00001'}),
											excelParam['excelAttrs'] = excelAttrs;
											goExcelDownLoad("/itg/itam/monitordelmanagement/searchMonitorListExcelDown.do", param, excelParam);
											
									
									}
									
								})								
								
							]
				}
			},
			
			minHeight : 200,
			resource : 'grid.itam.monitor.'+monitor_popup_type,		
			url : selecturl,
			params : {			
				class_mng_type : "",
				sr_id: $$("requst_regist")._getFieldValue("sr_id"),
				monitor_type : monitor_popup_type				
			},
			pageable : false,			
			autoLoad : false,
			keys : [ "CONF_ID" ],			
			on:{
				onItemClick: this.gridCellClick.bind(_this)
			}
		});
			
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
			        	rows: new Array().concat(Form, Smsgrid, Grid)
			        }
			};
		 
		_this.window = createWindow({
	    	id: props.id,	  
	    	//width: 800,
	    	width : nkia.ui.utils.getWidePopupWidth(),
	    	//height : 800,
	    	height : nkia.ui.utils.getWidePopupHeight()	,
	    	header: {
	    		title: props.title
	    	},
	        body: {
	        	rows: [bodyScrollView]
	        },	    	
	        footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저장', id : 'btn4', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable
	    });
		
		var reference = this.props.reference;
		var fields = reference.fields;
		
		
		
		if(PROCESS_META.TASK_NAME != "start" && PROCESS_META.TASK_NAME != "REQUST_RCEPT"){
			nkia.ui.utils.hides(['btn1']);
			nkia.ui.utils.hides(['btn2']);
			nkia.ui.utils.hides(['btn3']);
			nkia.ui.utils.shows(['btn5']);			
			
			nkia.ui.utils.hides([props.id + '_form']);
			nkia.ui.utils.hides([props.id + '_emsgrid']);
			
						
			if($$("requst_regist")._getFieldValue(fields.id)){
				$$(props.id + '_grid')._addRow($$("requst_regist")._getFieldValue(fields.id));
			}else{
				$$(props.id + '_grid')._reload();
			}
		}else{
			
			nkia.ui.utils.hides(['btn5']);
			
			if($$("requst_regist")._getFieldValue(fields.id) ){	
				$$(props.id + '_grid')._addRow($$("requst_regist")._getFieldValue(fields.id));				
			}
			else{
					$$(props.id + '_grid')._reload();										
			}
		}		
	};	

	this.choice = function(){

		var extract_grid = $$(global.props.id + '_grid')._getRows();		
		var reference = this.props.reference;
		var fields = reference.fields;
		var values = {};
				
		values[fields.id] = extract_grid; // 그리드 row 값들.
		values[fields.name] =  extract_grid.length ;
		
		reference.form._setValues(values);
		
		this.window.close();
	};

	this.addUser = function() {
		var data = $$(global.props.id + '_emsgrid')._getSelectedRows();		
		$$(global.props.id + '_grid')._addRow(data);
		// 사용자 목록 새로고침	
		$$(global.props.id + '_emsgrid')._reload({}, true);
	}
	
	// 그리드 더블클릭 버튼 이벤트
	this.gridCellClick = function(){	
		
		selectRow = $$(global.props.id + '_grid').getSelectedItem();

	};
	
	this.pressEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){ //enter key
				searchList();
			}	
		}
	};


	/**
	 * 검색
	 */
	this.searchList = function() {			
		// 검색폼의 값들 추출		
		if($$(global.props.id + '_form')._validate()){
			var data = $$(global.props.id + '_form')._getValues();	
		// 검색폼의 데이터를 인자값으로 그리드 리로드
			$$(global.props.id + '_emsgrid')._reload(data);
		}
	};

	/**
	 * 검색 폼 초기화
	 */
	this.resetFormData = function() {			
		$$(global.props.id + '_form')._reset();	
		$$(global.props.id + '_form')._setValues({
			MONITOR_TYPE : monitor_popup_type				
		});		
	};
};


nkia.ui.popup.nbpm.getHostPopup = function(){
	this.setRender = function(props) {

		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;		
		var popupfieldItems = [];
		
		popupfieldItems.push({colspan : 1, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.monitoring.label.itam.opms.00001'}), name : "class_id", popUpType : "amdbPopup", srId : "$!{dataMap.sr_id}"})});
		popupfieldItems.push({colspan : 1, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.center.move.label.customer.0001'}), name : "customer_id", popUpType : "monitoringcustomer", srId : "$!{dataMap.sr_id}"})});
		popupfieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.common.label.hostnm'}), name : "host_nm"})});
		popupfieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.label.itam.oper.00006'}), name : "main_ip", required : false})});
		popupfieldItems.push({colspan : 1, item : createCodeComboBoxComp({ // 센터구분
				label : getConstText({ isArgs : true, m_key : 'res.label.itam.opms.00020'}),
				name : 'location_cd',
				code_grp_id : 'CENTER',
				attachChoice : true
			}) });
		popupfieldItems.push({colspan : 1, item : createCodeComboBoxComp({// 고객사
			label : getConstText({ isArgs : true, m_key : 'res.common.status'}),
			name : 'asset_state',
			code_grp_id : 'ASSET_STATE',
			attachChoice : true
		}) });		
		
		
		var popupform = createFormComp({
			id: "popupform",
			elementsConfig: {
	    		labelPosition: "left" // 라벨 좌측위치를 위해 입력, 검색폼은 왼쪽 정렬로 사용합니다.
	    	},
			header: {
				title: getConstText({ isArgs : true, m_key : 'res.common.status'}),
				icon: "search", // 검색폼은 헤더에 아이콘을 넣어줍니다.
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ //검색							
							label : getConstText({
								isArgs : true,
								m_key : 'res.common.search'
							}),
							type: "form", 
							width : 300,
							click: function() {
								// 검색폼의 값들 추출searchform
								var data = $$("popupform")._getValues();
								
								// 검색폼의 데이터를 인자값으로 그리드 리로드
								$$("popupGrid")._reload(data);		
		    				}
							
						}),
						createBtnComp({ //초기화
							label : getConstText({
								isArgs : true,
								m_key : 'res.common.reset'
							}),						 
							width : 300,
							click:  function() {
								$$("popupform")._reset();
		    				}
							
						})
					],
			        css: "webix_layout_form_bottom" // 검색폼의 Footer CSS 문제 때문에 좌측 CSS를 입력해주셔야 합니다.
				}
			},
			fields : {
				colSize : 2, // 열 수
				items : popupfieldItems
			}
		});
		
		var popupGrid = createGridComp({
			id : "popupGrid",
			pageable : true,
			pageSize : 10,
			resizeColumn : true,
			url : '/itg/itam/monitormanagement/searchHostInfo.do',
			resource : 'grid.itam.gethostnm',
			params : {
				class_mng_type : ""
			},			
			header : {
				title : getConstText({	isArgs : true, m_key : 'res.label.itam.lifecycle.req.00012'})
			},
			on : {
				onItemDblClick:  this.choice.bind(_this)
			},
			minHeight : 200,
		});
		
		var bodyScrollView = {
		        view: "scrollview",
		        body: {
					cols: [{
						rows: new Array().concat(popupform, popupGrid)
		            }]
				}
			};
		
		
					
		_this.popupWindow = createWindow({
	    	id: props.id,
	    	width : nkia.ui.utils.getWidePopupWidth(),
	    	height : nkia.ui.utils.getWidePopupHeight()*3/4	,
	    	header: {
	    		title: getConstText({	isArgs : true, m_key : 'res.title.common.ciasset'})
	    	},
	    	body: {
	    		rows: [bodyScrollView]	    		
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({
	    					label : getConstText({
								isArgs : true,
								m_key : 'res.monitoring.label.applyhost.00003'
							}),
	    					type: "form", click:	this.choice.bind(_this)
	    			
	    				})
	    			]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	
	};
	
	this.choice = function(){
		var clickRecord = $$("popupGrid").getSelectedItem(); //webix API Method
		var reference = this.props.reference;
		var fields = reference.fields;
		reference.form._setValues(clickRecord);
		reference.form._setFieldValue(fields.name,clickRecord["HOST_NM"]);    			
		this.popupWindow.close();
	}
};

nkia.ui.popup.nbpm.amdbPopup = function() {
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: '/itg/itam/amdb/searchAmClassAllTree.do',
	        filterable: true,
		    params: {use_yn:'ALL'},
		    expandLevel: 3,	      
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		var bodyScrollView = {
		        view: "scrollview",
		        body: {
					cols: [{
		                rows: new Array().concat(tree)
		            }]
				}
			};
		
		_this.window = createWindow({
	    	id: props.id,
	    	width : nkia.ui.utils.getWidePopupWidth()/2,
	    	height : nkia.ui.utils.getWidePopupHeight()*3/5	,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({
    		            	label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if (treeItem) {

			var reference = this.props.reference;
			var fields = reference.fields;

			var values = {};

			values[fields.id] = treeItem.node_id; // 멀티코드 ID
			values[fields.name] = treeItem.text; // 멀티코드 명

			reference.form._setValues(values)
			this.window.close();

		} else {
			nkia.ui.utils.notification({
				type : 'error',
                message: '선택된 코드정보가 없습니다.'
            });
        }
	};
};

nkia.ui.popup.nbpm.monitorCustomerPopup = function() {
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        //url: '/itg/system/customer/searchStaticAllCustomerTree.do',
			url: '/itg/itam/statistics/searchCustTree.do',
	        filterable: true,
	        params: params,
	        height:550,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		var bodyScrollView = {
		        view: "scrollview",
		        body: {
					cols: [{
		                rows: new Array().concat(tree)
		            }]
				}
			};
		
		
		_this.window = createWindow({
	    	id: props.id,
	    	width : nkia.ui.utils.getWidePopupWidth()/2,
	    	height : nkia.ui.utils.getWidePopupHeight()*3/4	,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if(treeItem){
			
			if(treeItem.leaf == true){
				var reference = this.props.reference;
				var fields = reference.fields;
				
				var values = {};
				
				values[fields.id] = treeItem.node_id;	// 멀티코드 ID
				values[fields.name] = treeItem.text;		// 멀티코드 명 
				
	            reference.form._setValues(values)
	            this.window.close();
	
			}else{
				nkia.ui.utils.notification({
	                type: 'info',
	                message: '마지막 노드만 선택가능합니다.'
	            });
			}
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 코드정보가 없습니다.'
            });
        }
	};
};

/********************************************
 * Date: 2019-11-27
 * Version:
 *	UBIT 서버 반입 팝업
 ********************************************/
nkia.ui.popup.nbpm.carry = function(popupName) {
	
	var selectRow = null;
	this.setRender = function(props) {
		
		if($$("requst_regist")._getFieldValue("service_trget_customer_id_nm")==""){
			nkia.ui.utils.notification({
	            type: 'error',
	            message: '반입고객사를 입력해 주세요.'
	        });
			return false;
		}
		
		if($$("requst_regist")._getFieldValue("insert_date")==""){
			nkia.ui.utils.notification({
	            type: 'error',
	            message: '반입일자를 입력해 주세요.'
	        });
			return false;
		}


		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
			read = false;
			
		fieldItems = [];
		
		fieldItems.push({colspan:1, item : createTextFieldComp({
			label:'호스트명', 
			name:'HOST_NM',
			maxsize : 50,
			required: true
			})});

		
		switch(popupName){
			case 'serverCarry' : 
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label: '분류체계',
					name: 'CLASS_ID',
					url : '/itg/itam/newAsset/searchAmClassName.do',
					params: {class_id: '2'},
					required: true,
					attachChoice: true
				})});

			requireds = [ "HOST_NM", "CLASS_ID_NM", "CONF_NM",
					"BUSINESS_TYPE_CD_NM", "ASSET_OWN_TYPE_CD_NM",
					"OFFER_DATE", "OFFER_PRICE", "LOCATION_CD_NM",
					"CENTER_POSITION_CD_NM", "RACK_ORDER", "U_SIZE",
					"DUAL_POWER_CD_NM", "POTENTIAL_QUANTITY", "VENDOR_CD_NM",
					"MODEL", "CONFIDENTIALITY_CD_NM", "INTEGRITY_CD_NM",
					"AVAILABILITY_CD_NM", "MAINT_CONTRACT_YN", "MAIN_IP",
					"HCI_YN", "VIRTUAL_YN", "VERSION", "CPU_TYPE", "CPU_CNT",
					"TOTAL_MEMORY", "DISK_INTERNAL","SERIAL_NUMBER","DUAL_YN" ];
				break;	
			case 'networkCarry' : 
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label: '분류체계',
					name: 'CLASS_ID',
					url : '/itg/itam/newAsset/searchAmClassName.do',
					params: {class_id: '6',
						class_id2: '8'},
					filteredData: ["20","64"],
					required: true,
					attachChoice: true
				})});
				
			requireds = [ "HOST_NM", "CLASS_ID_NM", "CONF_NM",
					"BUSINESS_TYPE_CD_NM", "ASSET_OWN_TYPE_CD_NM",
					"OFFER_DATE", "OFFER_PRICE", "LOCATION_CD_NM",
					"CENTER_POSITION_CD_NM", "RACK_ORDER", "U_SIZE",
					"DUAL_POWER_CD_NM", "POTENTIAL_QUANTITY", "VENDOR_CD_NM",
					"MODEL", "CONFIDENTIALITY_CD_NM", "INTEGRITY_CD_NM",
					"AVAILABILITY_CD_NM", "MAINT_CONTRACT_YN", "MAIN_IP",
					"VERSION", "VIRTUAL_YN", "PORT_CNT", "SLOT_CNT", "DUAL_YN","SERIAL_NUMBER" ];

				break;	
			case 'securityCarry' : 
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label: '분류체계',
					name: 'CLASS_ID',
					url : '/itg/itam/newAsset/searchAmClassName.do',
					params: {class_id: '7'},
					required: true,
					attachChoice: true
				})});
				
			requireds = [ "HOST_NM", "CLASS_ID_NM", "CONF_NM",
					"BUSINESS_TYPE_CD_NM", "ASSET_OWN_TYPE_CD_NM",
					"OFFER_DATE", "OFFER_PRICE", "LOCATION_CD_NM",
					"CENTER_POSITION_CD_NM", "RACK_ORDER", "U_SIZE",
					"DUAL_POWER_CD_NM", "POTENTIAL_QUANTITY", "VENDOR_CD_NM",
					"MODEL", "CONFIDENTIALITY_CD_NM", "INTEGRITY_CD_NM",
					"AVAILABILITY_CD_NM", "MAINT_CONTRACT_YN", "MAIN_IP",
					"VERSION", "CONFIG_BACKUP_YN", "DUAL_YN","SERIAL_NUMBER" ];

				break;	
			case 'storageCarry' : 
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label: '분류체계',
					name: 'CLASS_ID',
					url : '/itg/itam/newAsset/searchAmClassName.do',
					params: {class_id: '8'},
					filteredData: ["46"],
					required: true,
					attachChoice: true
				})});
				
			requireds = [ "HOST_NM", "CLASS_ID_NM", "CONF_NM",
					"BUSINESS_TYPE_CD_NM", "ASSET_OWN_TYPE_CD_NM",
					"OFFER_DATE", "OFFER_PRICE", "LOCATION_CD_NM",
					"CENTER_POSITION_CD_NM", "RACK_ORDER", "U_SIZE",
					"DUAL_POWER_CD_NM", "POTENTIAL_QUANTITY", "VENDOR_CD_NM",
					"MODEL", "CONFIDENTIALITY_CD_NM", "INTEGRITY_CD_NM",
					"AVAILABILITY_CD_NM", "MAINT_CONTRACT_YN", "MAIN_IP",
					"CONNECT_TYPE_CD_NM","SERIAL_NUMBER" ];

				break;	
			case 'etcCarry' : 
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label:'기밀성', 
					name:'CONFIDENTIALITY_CD', 
					code_grp_id:"SECURITY_LEVEL_CD",
					required: true,
					attachChoice: true
					})});
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label:'무결성', 
					name:'INTEGRITY_CD', 
					code_grp_id:"SECURITY_LEVEL_CD",
					required: true,
					attachChoice: true
					})});
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label:'가용성', 
					name:'AVAILABILITY_CD', 
					code_grp_id:"SECURITY_LEVEL_CD",
					required: true,
					attachChoice: true
					})});
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label: '분류체계',
					name: 'CLASS_ID',
					url : '/itg/itam/newAsset/searchAmClassName.do',
					params: {class_id: '9'},
					required: true,
					attachChoice: true
				})});
				
			requireds = [ "HOST_NM", "CLASS_ID_NM", "ASSET_OWN_TYPE_CD_NM",
					"OFFER_DATE", "OFFER_PRICE", "LOCATION_CD_NM",
					"CENTER_POSITION_CD_NM", "RACK_ORDER", "U_SIZE",
					"DUAL_POWER_CD_NM", "POTENTIAL_QUANTITY",
					"CONFIDENTIALITY_CD_NM", "INTEGRITY_CD_NM",
					"AVAILABILITY_CD_NM" ];

				break;	
			case 'rackCarry' : 
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label: '분류체계',
					name: 'CLASS_ID',
					url : '/itg/itam/newAsset/searchAmClassName.do',
					params: {class_id: '68'},
					required: true,
					attachChoice: true
				})});
				
			requireds = [ "HOST_NM", "CLASS_ID_NM", "U_SIZE",
					"DUAL_POWER_CD_NM" ];

				break;	
			case 'facilityCarry' : 
				fieldItems.push({colspan:1, item : createCodeComboBoxComp({
					label: '분류체계',
					name: 'CLASS_ID',
					url : '/itg/itam/newAsset/searchAmClassName.do',
					params: {class_id: '67'},
					required: true,
					attachChoice: true
				})});

		requireds = [ "HOST_NM", "CLASS_ID_NM", "DUAL_POWER_CD_NM",
				"POTENTIAL_QUANTITY", "VENDOR_CD_NM", "MODEL",
				"CONFIDENTIALITY_CD_NM", "INTEGRITY_CD_NM",
				"AVAILABILITY_CD_NM", "MAINT_CONTRACT_YN" ];
				break;	

		}

	
		if(popupName == "serverCarry" || popupName == "networkCarry" || popupName == "securityCarry" || popupName == "storageCarry"){
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'단위업무명', 
				name:'CONF_NM', 
				required: true,
				maxsize : 300,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'모델명', 
				name:'MODEL', 
				required: true,
				maxsize : 50,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'제조사', 
				name:'VENDOR_CD',
				code_grp_id:"VENDOR_CD",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='NOTCODE'){
							webix.html.removeCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["VENDOR_DETAIL"].define("readonly", false);
							$$(popupName).elements["VENDOR_DETAIL"].define("required", true);
	
							$$("VENDOR_DETAIL").refresh();
						}else{
							webix.html.removeCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["VENDOR_DETAIL"].define("readonly", true);
							$$(popupName).elements["VENDOR_DETAIL"].define("required", false);
							
						    $$(popupName)._setFieldValue('VENDOR_DETAIL','');
	
							$$("VENDOR_DETAIL").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'제조사명', 
				id : 'VENDOR_DETAIL',
				name:'VENDOR_DETAIL', 
				readonly: true,
				required: false,
				maxsize : 200,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'소유형태', 
				name:'ASSET_OWN_TYPE_CD',
				code_grp_id:"ASSET_OWN_TYPE_CD",
				attachChoice: true,
				required: true,
				on:{
					onChange:function(val){
						if(val=='RENT'){
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["OWNER"].define("readonly", false);
							$$(popupName).elements["OWNER"].define("required", true);
	
							$$("OWNER").refresh();
						}else{
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["OWNER"].define("readonly", true);
							$$(popupName).elements["OWNER"].define("required", false);
							
						    $$(popupName)._setFieldValue('OWNER','');
	
							$$("OWNER").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'소유주체', 
				id : 'OWNER',
				name:'OWNER', 
				readonly: true,
				required: false,
				maxsize : 100,
				})});
			fieldItems.push({colspan:1, item : createDateFieldComp({
				label:'취득일', 
				name:'OFFER_DATE',
				dateType:'D',
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'취득가액 <font color=#0077ee>(단위 : 원)</font>', 
				name:'OFFER_PRICE', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'유지보수여부', 
				name:'MAINT_CONTRACT_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='Y'){
							webix.html.removeCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["MAINT_VENDOR"].define("readonly", false);
							$$(popupName).elements["MAINT_VENDOR"].define("required", true);
	
							$$("MAINT_VENDOR").refresh();
						}else{
							webix.html.removeCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["MAINT_VENDOR"].define("readonly", true);
							$$(popupName).elements["MAINT_VENDOR"].define("required", false);
							
						    $$(popupName)._setFieldValue('MAINT_VENDOR','');
	
							$$("MAINT_VENDOR").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'유지보수 업체명', 
				id : 'MAINT_VENDOR',
				name:'MAINT_VENDOR', 
				readonly: true,
				required: false,
				maxsize : 200
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'용도', 
				name:'BUSINESS_TYPE_CD',
				code_grp_id:"BUSINESS_TYPE_CD",
				required: true,
				attachChoice: true
				})});
			
			
			
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'센터위치', 
				name:'LOCATION_CD', 
				code_grp_id:"CENTER",
				readonly: true,
				required: true,
				attachChoice: true
				})});
			

			fieldItems.push({colspan:1, item : createPopBtnFieldComp({ 
				label: "상면코드",
				name: 'CENTER_POSITION_CD_NM',
				required: true,
				popUpType : "locationPopup"
				/**
				keepEvent: true,
				click: function() {
					locationPopup(popupName);
				}
				**/
			})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'기타위치', 
				name:'POSITION_DETAIL', 
				maxsize : 200,
				readonly: true,
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'랙U번호', 
				name:'RACK_ORDER', 
				maxsize : 50,
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'U사이즈', 
				name:'U_SIZE', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'전원이중화여부', 
				name:'DUAL_POWER_CD', 
				code_grp_id:"DUAL_POWER_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'정격전력량 <font color=#0077ee>(단위 : W)</font>', 
				name:'POTENTIAL_QUANTITY', 
				inputType:"price",
				required: true,
				})});
			
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'기밀성', 
				name:'CONFIDENTIALITY_CD', 
				code_grp_id:"SECURITY_LEVEL_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'무결성', 
				name:'INTEGRITY_CD', 
				code_grp_id:"SECURITY_LEVEL_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'가용성', 
				name:'AVAILABILITY_CD', 
				code_grp_id:"SECURITY_LEVEL_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'시리얼넘버', 
				name:'SERIAL_NUMBER', 
				required: true,
				maxsize : 50,
				})});

			
			
		}
		
		switch(popupName){
		case 'serverCarry' : 
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'관리IP', 
				name:'MAIN_IP', 
				inputType:"ip",
				required: true,
				maxsize : 100,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'버전(OS) 정보', 
				name:'VERSION', 
				required: true,
				maxsize : 100,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'HCI 여부', 
				name:'HCI_YN', 
				code_grp_id:"YN",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'MEMORY 총용량 <font color=#0077ee>(단위 : GB)</font>', 
				name:'TOTAL_MEMORY', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'CPU모델', 
				name:'CPU_TYPE', 
				required: true,
				maxsize : 50,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'총 물리 코어 수', 
				name:'CPU_CNT', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'CPU 소켓 수량', 
				name:'CPU_SOCKET_CNT', 
				inputType:"price",
				//required: true,
				})});
			
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'HDD 총용량 <font color=#0077ee>(단위 : GB)</font>', 
				name:'DISK_INTERNAL', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'가상화 HOST 여부', 
				name:'VIRTUAL_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='Y'){
							$$(popupName).elements["VIRTUAL_TYPE_CD"].enable();
							$$(popupName).elements["VIRTUAL_TYPE_CD"].define("required", true);

							$$("VIRTUAL_TYPE_CD").refresh();
						}else{
							$$(popupName).elements["VIRTUAL_TYPE_CD"].disable();
							$$(popupName).elements["VIRTUAL_TYPE_CD"].define("required", false);
							
						    $$(popupName)._setFieldValue('VIRTUAL_TYPE_CD','');

							$$("VIRTUAL_TYPE_CD").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'가상화방식', 
				id : 'VIRTUAL_TYPE_CD',
				name:'VIRTUAL_TYPE_CD', 
				code_grp_id:"VIRTUAL_TYPE_CD",
				readonly: true,
				required: false,
				attachChoice: true,
				})});
			
			
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'이중화 여부', 
				name:'DUAL_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='Y'){
							$$(popupName).elements["DUAL_TYPE_CD"].enable();
							$$(popupName).elements["DUAL_TYPE_CD"].define("required", true);

							$$("DUAL_TYPE_CD").refresh();
						}else{
							$$(popupName).elements["DUAL_TYPE_CD"].disable();
							$$(popupName).elements["DUAL_TYPE_CD"].define("required", false);
							
						    $$(popupName)._setFieldValue('DUAL_TYPE_CD','');

							$$("DUAL_TYPE_CD").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'이중화유형', 
				id : 'DUAL_TYPE_CD',
				name:'DUAL_TYPE_CD', 
				code_grp_id:"DUAL_TYPE_CD",
				readonly: true,
				required: false,
				attachChoice: true,
				})});
			break;
		
		case 'networkCarry' : 
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'관리IP', 
				name:'MAIN_IP', 
				inputType:"ip",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'버전 정보', 
				name:'VERSION', 
				required: true,
				maxsize : 100
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'전체 포트수', 
				name:'PORT_CNT', 
				inputType:"price",
				required: true
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'전체 슬롯수', 
				name:'SLOT_CNT', 
				inputType:"price",
				required: true
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'가상화 HOST 여부', 
				name:'VIRTUAL_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'이중화 여부', 
				name:'DUAL_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='Y'){
							$$(popupName).elements["DUAL_TYPE_CD"].enable();
							$$(popupName).elements["DUAL_TYPE_CD"].define("required", true);

							$$("DUAL_TYPE_CD").refresh();
						}else{
							$$(popupName).elements["DUAL_TYPE_CD"].disable();
							$$(popupName).elements["DUAL_TYPE_CD"].define("required", false);
							
						    $$(popupName)._setFieldValue('DUAL_TYPE_CD','');

							$$("DUAL_TYPE_CD").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'이중화유형', 
				id : 'DUAL_TYPE_CD',
				name:'DUAL_TYPE_CD', 
				code_grp_id:"DUAL_TYPE_CD",
				readonly: true,
				required: false,
				attachChoice: true,
				})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy'})});
			
			break;	
		case 'securityCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'관리IP', 
				name:'MAIN_IP', 
				inputType:"ip",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'버전 정보', 
				name:'VERSION', 
				required: true,
				maxsize : 100
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'Config 백업 여부', 
				name:'CONFIG_BACKUP_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy'})});

			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'이중화 여부', 
				name:'DUAL_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='Y'){
							$$(popupName).elements["DUAL_TYPE_CD"].enable();
							$$(popupName).elements["DUAL_TYPE_CD"].define("required", true);

							$$("DUAL_TYPE_CD").refresh();
						}else{
							$$(popupName).elements["DUAL_TYPE_CD"].disable();
							$$(popupName).elements["DUAL_TYPE_CD"].define("required", false);
							
						    $$(popupName)._setFieldValue('DUAL_TYPE_CD','');

							$$("DUAL_TYPE_CD").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'이중화유형', 
				id : 'DUAL_TYPE_CD',
				name:'DUAL_TYPE_CD', 
				code_grp_id:"DUAL_TYPE_CD",
				readonly: true,
				required: false,
				attachChoice: true,
				})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy1'})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy2'})});

			break;	

		case 'storageCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'관리IP', 
				name:'MAIN_IP', 
				inputType:"ip",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'연결방식', 
				name:'CONNECT_TYPE_CD',
				code_grp_id:"CONNECT_TYPE_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy'})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy1'})});

			break;	
		case 'etcCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'센터위치', 
				name:'LOCATION_CD', 
				code_grp_id:"CENTER",
				readonly: true,
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createPopBtnFieldComp({ 
				label: "상면코드",
				name: 'CENTER_POSITION_CD_NM',
				required: true,
				popUpType : "locationPopup"
				/**
				keepEvent: true,
				click: function() {
					locationPopup(popupName);
				}
				**/
			})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'기타위치', 
				name:'POSITION_DETAIL', 
				maxsize : 200,
				readonly: true,
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'랙U번호', 
				name:'RACK_ORDER', 
				maxsize : 50,
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'U사이즈', 
				name:'U_SIZE', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'전원이중화여부', 
				name:'DUAL_POWER_CD', 
				code_grp_id:"DUAL_POWER_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'정격전력량 <font color=#0077ee>(단위 : W)</font>', 
				name:'POTENTIAL_QUANTITY', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createDateFieldComp({
				label:'취득일', 
				name:'OFFER_DATE',
				dateType:'D',
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'취득가액 <font color=#0077ee>(단위 : 원)</font>', 
				name:'OFFER_PRICE', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'소유형태', 
				name:'ASSET_OWN_TYPE_CD',
				code_grp_id:"ASSET_OWN_TYPE_CD",
				attachChoice: true,
				required: true,
				on:{
					onChange:function(val){
						if(val=='RENT'){
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["OWNER"].define("readonly", false);
							$$(popupName).elements["OWNER"].define("required", true);
	
							$$("OWNER").refresh();
						}else{
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["OWNER"].define("readonly", true);
							$$(popupName).elements["OWNER"].define("required", false);
							
						    $$(popupName)._setFieldValue('OWNER','');
	
							$$("OWNER").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'소유주체', 
				id : 'OWNER',
				name:'OWNER', 
				readonly: true,
				required: false,
				maxsize : 100,
				})});
			break;	
		case 'rackCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'전원이중화여부', 
				name:'DUAL_POWER_CD', 
				code_grp_id:"DUAL_POWER_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy'})});
			fieldItems.push({colspan:1, item : createDateFieldComp({
				label:'취득일', 
				name:'OFFER_DATE',
				dateType:'D',
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'취득가액 <font color=#0077ee>(단위 : 원)</font>', 
				name:'OFFER_PRICE', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'소유형태', 
				name:'ASSET_OWN_TYPE_CD',
				code_grp_id:"ASSET_OWN_TYPE_CD",
				attachChoice: true,
				required: true,
				on:{
					onChange:function(val){
						if(val=='RENT'){
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["OWNER"].define("readonly", false);
							$$(popupName).elements["OWNER"].define("required", true);
	
							$$("OWNER").refresh();
						}else{
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["OWNER"].define("readonly", true);
							$$(popupName).elements["OWNER"].define("required", false);
							
						    $$(popupName)._setFieldValue('OWNER','');
	
							$$("OWNER").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'소유주체', 
				id : 'OWNER',
				name:'OWNER', 
				readonly: true,
				required: false,
				maxsize : 100,
				})});

			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'센터위치', 
				name:'LOCATION_CD', 
				code_grp_id:"CENTER",
				readonly: true,
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createPopBtnFieldComp({ 
				label: "상면코드",
				name: 'CENTER_POSITION_CD_NM',
				required: true,
				popUpType : "locationPopup"
				/**
				keepEvent: true,
				click: function() {
					locationPopup(popupName);
				}
				**/
			})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'기타위치', 
				name:'POSITION_DETAIL', 
				maxsize : 200,
				readonly: true,
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'U사이즈', 
				name:'U_SIZE', 
				inputType:"price",
				required: true,
				})});
			break;	
		case 'facilityCarry' : 
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'모델명', 
				name:'MODEL', 
				required: true,
				maxsize : 50,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'정격전력량 <font color=#0077ee>(단위 : W)</font>', 
				name:'POTENTIAL_QUANTITY', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'제조사', 
				name:'VENDOR_CD',
				code_grp_id:"VENDOR_CD",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='NOTCODE'){
							webix.html.removeCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["VENDOR_DETAIL"].define("readonly", false);
							$$(popupName).elements["VENDOR_DETAIL"].define("required", true);
	
							$$("VENDOR_DETAIL").refresh();
						}else{
							webix.html.removeCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("VENDOR_DETAIL").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["VENDOR_DETAIL"].define("readonly", true);
							$$(popupName).elements["VENDOR_DETAIL"].define("required", false);
							
						    $$(popupName)._setFieldValue('VENDOR_DETAIL','');
	
							$$("VENDOR_DETAIL").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'제조사명', 
				id : 'VENDOR_DETAIL',
				name:'VENDOR_DETAIL', 
				readonly: true,
				required: false,
				maxsize : 200,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'소유형태', 
				name:'ASSET_OWN_TYPE_CD',
				code_grp_id:"ASSET_OWN_TYPE_CD",
				attachChoice: true,
				required: true,
				on:{
					onChange:function(val){
						if(val=='RENT'){
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["OWNER"].define("readonly", false);
							$$(popupName).elements["OWNER"].define("required", true);
	
							$$("OWNER").refresh();
						}else{
							webix.html.removeCss($$("OWNER").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("OWNER").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["OWNER"].define("readonly", true);
							$$(popupName).elements["OWNER"].define("required", false);
							
						    $$(popupName)._setFieldValue('OWNER','');
	
							$$("OWNER").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'소유주체', 
				id : 'OWNER',
				name:'OWNER', 
				readonly: true,
				required: false,
				maxsize : 100,
				})});

			fieldItems.push({colspan:1, item : createDateFieldComp({
				label:'취득일', 
				name:'OFFER_DATE',
				dateType:'D',
				required: true,
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'취득가액 <font color=#0077ee>(단위 : 원)</font>', 
				name:'OFFER_PRICE', 
				inputType:"price",
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'유지보수여부', 
				name:'MAINT_CONTRACT_YN',
				code_grp_id:"YN",
				required: true,
				attachChoice: true,
				on:{
					onChange:function(val){
						if(val=='Y'){
							webix.html.removeCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly_false');
							webix.html.removeCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly');
							webix.html.addCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly_false');
							
							$$(popupName).elements["MAINT_VENDOR"].define("readonly", false);
							$$(popupName).elements["MAINT_VENDOR"].define("required", true);
	
							$$("MAINT_VENDOR").refresh();
						}else{
							webix.html.removeCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly_false');
							webix.html.addCss($$("MAINT_VENDOR").getNode(), 'nkia_field_readonly');
							
							$$(popupName).elements["MAINT_VENDOR"].define("readonly", true);
							$$(popupName).elements["MAINT_VENDOR"].define("required", false);
							
						    $$(popupName)._setFieldValue('MAINT_VENDOR','');
	
							$$("MAINT_VENDOR").refresh();
						}
					}
				}
				})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'유지보수 업체명', 
				id : 'MAINT_VENDOR',
				name:'MAINT_VENDOR', 
				readonly: true,
				required: false,
				maxsize : 200
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'센터위치', 
				name:'LOCATION_CD', 
				code_grp_id:"CENTER",
				readonly: true,
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createPopBtnFieldComp({ 
				label: "상면코드",
				name: 'CENTER_POSITION_CD_NM',
				required: true,
				popUpType : "locationPopup"
				/**
				keepEvent: true,
				click: function() {
					locationPopup(popupName);
				}
				**/
			})});
			fieldItems.push({colspan:1, item : createTextFieldComp({
				label:'기타위치', 
				name:'POSITION_DETAIL', 
				maxsize : 200,
				readonly: true,
				required: true,
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'전원이중화여부', 
				name:'DUAL_POWER_CD', 
				code_grp_id:"DUAL_POWER_CD",
				required: true,
				attachChoice: true
				})});
			
			
			
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'기밀성', 
				name:'CONFIDENTIALITY_CD', 
				code_grp_id:"SECURITY_LEVEL_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'무결성', 
				name:'INTEGRITY_CD', 
				code_grp_id:"SECURITY_LEVEL_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item : createCodeComboBoxComp({
				label:'가용성', 
				name:'AVAILABILITY_CD', 
				code_grp_id:"SECURITY_LEVEL_CD",
				required: true,
				attachChoice: true
				})});
			fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy'})});


			break;	
		default :  
			break;
		}	

		
		

		buttonItems = [];
		buttonItems.push(createBtnComp({label:'#springMessage("btn.common.insert")',type:'form',click: function() { actionLink("insert") } }) );
		
		

		var carry = createFormComp({
			id: popupName,
			header: {
				title: "자산정보"
			},
		    fields: {
		    	colSize: 4,
				items: fieldItems,
			    hiddens: {
			    	CENTER_POSITION_CD : "LOC",
					ITSM_ASSET_ID : "",
					ITSM_DIV_ID : "",
					ITSM_ASSET_CICT_CODE : ""
			    }

		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({
		                	id:"saveGrid",
		                	label: '▼',
		                	type:"form",
		                	visible: false,
		                	click: this.doInsert.bind(_this)})
		                	,
			                createBtnComp({
			                	id:"cpGrid",
			                	label: '붙여넣기',
			                	type:"form",
			                	visible: false,
			                	click: this.cpInsert.bind(_this)})
		                //,createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		

		var grid = createGridComp({
			id: 'grid',
			header: {
	        	title: "자산 반입 목록",
				buttons: {
					items: [

						createBtnComp({
							id:"btn1",
							label:"등록",
							type: "form",
							click: function() {
								$$(popupName)._reset();
								$$(popupName)._setControl({editorMode: true});
								nkia.ui.utils.shows(['saveGrid']);
								nkia.ui.utils.hides(['cpGrid']);

								selectRow = null;
							}
						}),
						createBtnComp({
							id:"btn6",
							label:"붙여넣기",
							type: "form",
							click: function() {
								selectRow = $$('grid')._getSelectedRows();

								if (selectRow.length != 0) {

											$$(popupName)._setControl({editorMode: true});				
											$$('grid')._addRow(selectRow);
											var row = $$('grid')._getUnSelectedRows();

											$$(popupName)._setValues(selectRow[0]);
											$$('grid')._removeAllRow();
											$$('grid')._addRow(row);

											nkia.ui.utils.shows(['cpGrid']);
										}else {
											alert("자산을 선택해 주세요");
											return false;
										}
							}
						}),
						createBtnComp({
							id:"btn5",
							label:"유지보수등록장비선택",
							type: "form",
							click: function() {
								assetPopup(popupName);
							}
						}),
						createBtnComp({
							id:"btn2",
							label:"수정",
							type: "form",
							click: function() {
						var selectRow = $$("grid")._getSelectedRows();

						if (selectRow.length != 0) {
									$$(popupName)._setControl({editorMode: true});		
									selectRow[0].CENTER_POSITION_CD_NM_nm = selectRow[0].CENTER_POSITION_CD_NM;
									$$(popupName)._setValues(selectRow[0]);
									
									nkia.ui.utils.shows(['saveGrid']);
									nkia.ui.utils.hides(['cpGrid']);

								}else {
									alert("자산을 선택해 주세요");
									return false;
								}
							}
						}),
						createBtnComp({
							id:"btn3",
							label:"삭제",
							type: "form",
							click: function() {
								var selectRow = $$('grid')._getSelectedRows();

								if (selectRow.length != 0) {
									var row = $$('grid')._getUnSelectedRows();
									$$('grid')._removeAllRow();
									$$('grid')._addRow(row);
									
									$$(popupName)._setControl({editorMode: false});
									nkia.ui.utils.hides(['saveGrid']);
									nkia.ui.utils.hides(['cpGrid']);
									$$(popupName)._reset();
								}else {
									alert("자산을 선택해 주세요");
									return false;
								}
							}
						})
					]
				}
		    },
			checkbox: false,
			minHeight: 100,
		    resource: "grid.itam.process."+popupName,
		    url : '/itg/itam/serviceAsset/assetCarrySelect.do',
			params: {
				type: popupName,
				sr_id: $$("requst_regist")._getFieldValue("sr_id")
			},
			pageable: false,
			autoLoad : false,
			resizeColumn : true,
			on:{
				onItemClick: this.gridCellClick.bind(_this)
			},
			requiredColumns : requireds
			//keys: ["AM_RECORD_ID"],
			//dependency: this.dependency
		});
		

		var bodyScrollView = {
				view : "scrollview",
				body : {
					cols : [ {
						rows : new Array().concat(carry,grid)
					} ]
				}
			};
		
		_this.window = createWindow({
	    	id: props.id,
	    	width : nkia.ui.utils.getWidePopupWidth(),
	    	height : nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: '반입자산'
	    	},
	    	body: {
	    		rows: [ bodyScrollView ]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저장', id:'btn4', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });

		var reference = this.props.reference;
		var fields = reference.fields;
		
		switch(popupName){
		case 'serverCarry' : 
			break;	
		case 'networkCarry' : 
			$$(popupName)._hideField('dummy');

			break;	
		case 'securityCarry' : 
			$$(popupName)._hideField('dummy');
			$$(popupName)._hideField('dummy1');
			$$(popupName)._hideField('dummy2');

			break;	
		case 'storageCarry' : 
			$$(popupName)._hideField('dummy');
			$$(popupName)._hideField('dummy1');

			break;	
		case 'etcCarry' : 
			break;	
		case 'rackCarry' : 
			$$(popupName)._hideField('dummy');

			break;	
		case 'facilityCarry' : 
			$$(popupName)._hideField('dummy');

			break;	
		}

		

		$$(popupName)._setControl({editorMode: false});
				
		if(PROCESS_META.TASK_NAME != "start" && PROCESS_META.TASK_NAME != "REQUST_RCEPT"){

			nkia.ui.utils.hides(['saveGrid']);
			nkia.ui.utils.hides(['btn1']);
			nkia.ui.utils.hides(['btn2']);
			nkia.ui.utils.hides(['btn3']);
			nkia.ui.utils.hides(['btn4']);
			nkia.ui.utils.hides(['btn5']);
			nkia.ui.utils.hides(['btn6']);

			if($$("requst_regist")._getFieldValue(fields.id)){
				$$("grid")._addRow($$("requst_regist")._getFieldValue(fields.id));
			}else{
				$$("grid")._reload();
			}
		}else{				
				
			nkia.ui.utils.ajax({
		        url: "/itg/itam/serviceAsset/assetCustomerCount.do",
				params: { CUSTOMER_ID : $$("requst_regist")._getFieldValue("service_trget_customer_id") },
				async: false,
				isMask: false,
		        success: function(result){      
	        		var requstData = result.resultMap;
		        	if(requstData.totalCount>0){
		    			nkia.ui.utils.hides(['btn1']);
		    			nkia.ui.utils.hides(['btn6']);
		    			nkia.ui.utils.shows(['btn5']);
		        	}else{
		    			nkia.ui.utils.hides(['btn5']);
		    			nkia.ui.utils.shows(['btn1']);
		    			nkia.ui.utils.shows(['btn6']);
		        	}
				}
			});

			if($$("requst_regist")._getFieldValue(fields.id)){
				$$("grid")._addRow($$("requst_regist")._getFieldValue(fields.id));
			}else{
				$$("grid")._reload();
			}
		}
		
		

		

	};
	

	// 그리드 더블클릭 버튼 이벤트
	this.gridCellClick = function(){	
		selectRow = $$("grid")._getSelectedRows();
	}

	
	// 등록 버튼 이벤트
	this.doInsert = function(){
		
		var data = $$(popupName)._getValues();

		if(popupName == "serverCarry" || popupName == "networkCarry" || popupName == "securityCarry" || popupName == "storageCarry"){
			data.BUSINESS_TYPE_CD_NM = ($$(popupName)._getField('BUSINESS_TYPE_CD'))._getText();
			data.VENDOR_CD_NM = ($$(popupName)._getField('VENDOR_CD'))._getText();
			data.MAINT_CONTRACT_YN_NM = ($$(popupName)._getField('MAINT_CONTRACT_YN'))._getText();
		}
		
		if(popupName != "rackCarry"){
			data.CONFIDENTIALITY_CD_NM = ($$(popupName)._getField('CONFIDENTIALITY_CD'))._getText();
			data.INTEGRITY_CD_NM = ($$(popupName)._getField('INTEGRITY_CD'))._getText();
			data.AVAILABILITY_CD_NM = ($$(popupName)._getField('AVAILABILITY_CD'))._getText();
		}
		
		data.DUAL_POWER_CD_NM = ($$(popupName)._getField('DUAL_POWER_CD'))._getText();
		data.CLASS_ID_NM = ($$(popupName)._getField('CLASS_ID'))._getText();
		data.ASSET_OWN_TYPE_CD_NM = ($$(popupName)._getField('ASSET_OWN_TYPE_CD'))._getText();
		data.LOCATION_CD_NM = ($$(popupName)._getField('LOCATION_CD'))._getText();

		
		switch(popupName){
		case 'serverCarry' : 
			data.HCI_YN_NM = ($$(popupName)._getField('HCI_YN'))._getText();
			data.VIRTUAL_YN_NM = ($$(popupName)._getField('VIRTUAL_YN'))._getText();
			data.VIRTUAL_TYPE_CD_NM = ($$(popupName)._getField('VIRTUAL_TYPE_CD'))._getText();
			data.DUAL_YN_NM = ($$(popupName)._getField('DUAL_YN'))._getText();
			data.DUAL_TYPE_CD_NM = ($$(popupName)._getField('DUAL_TYPE_CD'))._getText();
			break;
		case 'networkCarry' : 
			data.VIRTUAL_YN_NM = ($$(popupName)._getField('VIRTUAL_YN'))._getText();
			data.DUAL_YN_NM = ($$(popupName)._getField('DUAL_YN'))._getText();
			data.DUAL_TYPE_CD_NM = ($$(popupName)._getField('DUAL_TYPE_CD'))._getText();
			break;	
		case 'securityCarry' : 
			data.CONFIG_BACKUP_YN_NM = ($$(popupName)._getField('CONFIG_BACKUP_YN'))._getText();
			data.DUAL_YN_NM = ($$(popupName)._getField('DUAL_YN'))._getText();
			data.DUAL_TYPE_CD_NM = ($$(popupName)._getField('DUAL_TYPE_CD'))._getText();
			break;	
		case 'storageCarry' : 
			data.CONNECT_TYPE_CD_NM = ($$(popupName)._getField('CONNECT_TYPE_CD'))._getText();
			break;	
		case 'facilityCarry' : 
			data.VENDOR_CD_NM = ($$(popupName)._getField('VENDOR_CD'))._getText();
			data.MAINT_CONTRACT_YN_NM = ($$(popupName)._getField('MAINT_CONTRACT_YN'))._getText();
			break;	
		default :  
			break;
		}	

		
		var resultStr = '['+JSON.stringify(data)+']';

		if(selectRow){
			if( $$(popupName)._validate() ) {
			var row = $$('grid')._getUnSelectedRows();
			$$('grid')._removeAllRow();
			$$('grid')._addRow(row);
			$$('grid')._addRow(JSON.parse(resultStr));
			
			$$(popupName)._setControl({editorMode: false});
			nkia.ui.utils.hides(['saveGrid']);

			$$(popupName)._reset();

			}
		}else{
			if( $$(popupName)._validate() ) {
			$$('grid')._addRow(JSON.parse(resultStr));
			$$(popupName)._setControl({editorMode: false});
			nkia.ui.utils.hides(['saveGrid']);

			$$(popupName)._reset();
			}
		}

		
	}
	
	this.cpInsert = function(){
		
		var data = $$(popupName)._getValues();

		if(popupName == "serverCarry" || popupName == "networkCarry" || popupName == "securityCarry" || popupName == "storageCarry"){
			data.BUSINESS_TYPE_CD_NM = ($$(popupName)._getField('BUSINESS_TYPE_CD'))._getText();
			data.VENDOR_CD_NM = ($$(popupName)._getField('VENDOR_CD'))._getText();
			data.MAINT_CONTRACT_YN_NM = ($$(popupName)._getField('MAINT_CONTRACT_YN'))._getText();
		}
		
		if(popupName != "rackCarry"){
			data.CONFIDENTIALITY_CD_NM = ($$(popupName)._getField('CONFIDENTIALITY_CD'))._getText();
			data.INTEGRITY_CD_NM = ($$(popupName)._getField('INTEGRITY_CD'))._getText();
			data.AVAILABILITY_CD_NM = ($$(popupName)._getField('AVAILABILITY_CD'))._getText();
		}
		
		data.DUAL_POWER_CD_NM = ($$(popupName)._getField('DUAL_POWER_CD'))._getText();
		data.CLASS_ID_NM = ($$(popupName)._getField('CLASS_ID'))._getText();
		data.ASSET_OWN_TYPE_CD_NM = ($$(popupName)._getField('ASSET_OWN_TYPE_CD'))._getText();
		data.LOCATION_CD_NM = ($$(popupName)._getField('LOCATION_CD'))._getText();

		
		switch(popupName){
		case 'serverCarry' : 
			data.HCI_YN_NM = ($$(popupName)._getField('HCI_YN'))._getText();
			data.VIRTUAL_YN_NM = ($$(popupName)._getField('VIRTUAL_YN'))._getText();
			data.VIRTUAL_TYPE_CD_NM = ($$(popupName)._getField('VIRTUAL_TYPE_CD'))._getText();
			data.DUAL_YN_NM = ($$(popupName)._getField('DUAL_YN'))._getText();
			data.DUAL_TYPE_CD_NM = ($$(popupName)._getField('DUAL_TYPE_CD'))._getText();
			break;
		case 'networkCarry' : 
			data.VIRTUAL_YN_NM = ($$(popupName)._getField('VIRTUAL_YN'))._getText();
			data.DUAL_YN_NM = ($$(popupName)._getField('DUAL_YN'))._getText();
			data.DUAL_TYPE_CD_NM = ($$(popupName)._getField('DUAL_TYPE_CD'))._getText();
			break;	
		case 'securityCarry' : 
			data.CONFIG_BACKUP_YN_NM = ($$(popupName)._getField('CONFIG_BACKUP_YN'))._getText();
			data.DUAL_YN_NM = ($$(popupName)._getField('DUAL_YN'))._getText();
			data.DUAL_TYPE_CD_NM = ($$(popupName)._getField('DUAL_TYPE_CD'))._getText();
			break;	
		case 'storageCarry' : 
			data.CONNECT_TYPE_CD_NM = ($$(popupName)._getField('CONNECT_TYPE_CD'))._getText();
			break;	
		case 'facilityCarry' : 
			data.VENDOR_CD_NM = ($$(popupName)._getField('VENDOR_CD'))._getText();
			data.MAINT_CONTRACT_YN_NM = ($$(popupName)._getField('MAINT_CONTRACT_YN'))._getText();
			break;	
		default :  
			break;
		}	

		
		var resultStr = '['+JSON.stringify(data)+']';

			if( $$(popupName)._validate() ) {	
				//selectRow[0];
				//$$("grid")._addRow();
				//var row = $$('grid')._getUnSelectedRows();
				//var rows = $$('grid')._getSelectedRows();
				//$$('grid')._removeAllRow();
				//$$('grid')._addRow(row);
				//$$('grid')._addRow(JSON.parse(resultStr));
				$$('grid')._addRow(JSON.parse(resultStr));
			}

		
	}

	
	this.choice = function(){
		var reference = this.props.reference;
		var fields = reference.fields;
		
		var values = {};

		values[fields.id] = $$('grid')._getRows();	// 멀티코드 ID
		values[fields.name] = $$('grid')._getRows().length;		// 멀티코드 명 

		var clickRow = $$("grid")._getRows();

		var requiredColumns = $$("grid").config.requiredColumns;
		
		if(clickRow.length == 0){
			nkia.ui.utils.notification({
				 type: 'error', message: '반입자산 목록이 0건입니다.' }); 
				return false; 
		}
				
		if(clickRow.length != 0){
			for(var j=0; j<clickRow.length; j++) { 
				var rowData = clickRow[j]; 
				for(var c in requiredColumns) {
					if(rowData[requiredColumns[c]] !== undefined && (rowData[requiredColumns[c]] == null || rowData[requiredColumns[c]] == "")) {
						if(typeof(rowData[requiredColumns[c]])!= "number"){
							nkia.ui.utils.notification({
							 type: 'error', message: '필수 입력 항목(그리드)을 확인바랍니다.' }); 
							return false; 
						} 
					}
				}
				reference.form._setValues(values);
			}
		}else{
			reference.form._setValues(values);
		}

		this.window.close();
	};
	
	

};

/**
 * 유지보수시스템 장비 선택 팝업
 */
function assetPopup(popupname){	
	var classid = null;
	var classid2 = null;
	var classid3 = null;
	var resour = null;

	switch (popupname) {
	case 'serverCarry':
		classid = '2';
		resour = "grid.itam.process.serverCarry";
		break;
	case 'networkCarry':
		classid = '6';
		classid2 = '46';
		resour = "grid.itam.process.networkCarry";
		break;
	case 'securityCarry':
		classid = '7';
		resour = "grid.itam.process.securityCarry";
		break;
	case 'storageCarry':
		classid = '8';
		classid3 = '46';
		resour = "grid.itam.process.storageCarry";
		break;
	case 'etcCarry':
		classid = '9';
		resour = "grid.itam.process.etcCarry";
		break;
	case 'rackCarry':
		classid = '68';
		resour = "grid.itam.process.rackCarry";
		break;
	case 'facilityCarry':
		classid = '67'
		resour = "grid.itam.process.facilityCarry";
		break;
	}

	var assetSearchform = createFormComp({
		id: "assetSearchform",
		elementsConfig: {
    		labelPosition: "left" // 라벨 좌측위치를 위해 입력, 검색폼은 왼쪽 정렬로 사용합니다.
    	},
		header: {
			title: "검색조건",
			icon: "search", // 검색폼은 헤더에 아이콘을 넣어줍니다.
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ //검색
						label:"검색", 
						type: "form", 
						click: function() {
							assetSearchList(classid,classid2,classid3);
						}
					}),
					createBtnComp({ //초기화
						label:"초기화", 
						click: function() {
							assetResetFormData();
						}
					})
				],
		        css: "webix_layout_form_bottom" // 검색폼의 Footer CSS 문제 때문에 좌측 CSS를 입력해주셔야 합니다.
			}
		},
		fields: {
			colSize: 2, // 열 수
			items: [
				{ colspan: 2, item : createUnionFieldComp({
					items: [
						createTextFieldComp({
							label: "호스트명",
							name: "search_value",
							placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
							on:{
								onKeyPress: function(keycode) {
									if(keyCode == "13"){ //enter key
										assetSearchList(classid,classid2,classid3);
									}
								}
							}
						})
						]
					})
				}
			],
		    hiddens: {
		    }

		}
	});

	var popupGrid = createGridComp({
		id : "popupGrid2",
		pageable : true,
		checkbox : true,
		keys : [ "ITSM_ASSET_ID" ],
		pageSize : 10,
		url : "/itg/itam/serviceAsset/searchAssetList.do",
		params : {
			class_id : classid,
			class_id2 : classid2,
			class_id3 : classid3
		},
		resource : resour,
		header : {
			title : "자산목록"
		},
		on:{
			onItemDblClick: function(rowId, event, node) {
				var clickRecord = $$("popupGrid2")._getSelectedRows(); //webix API Method
				var values = {
						host_nm : clickRecord[0].HOST_NM,
						asset_id : clickRecord[0].ASSET_ID,
						model_nm : clickRecord[0].MODEL,
						am_location : clickRecord[0].CENTER_POSITION_CD_NM,
						customer : clickRecord[0].CUSTOMER_ID_NM,
						mgmt_user : clickRecord[0].MAIN_USER_NM
				};
				$$("ledForm")._setValues(values);
				popupWindow.close();
			}
		},
		dependency : { // 의존관계 그리드 사용
			targetGridId : "subgrid" // 대상 그리드 id
		}
	});

	var subgrid = createGridComp({
		id : 'subgrid',
		keys : [ "ITSM_ASSET_ID" ],
		resizeColumn : true,
		checkbox : true,
		autoLoad : false, // 메모리 그리드 사용 (데이터 로드 없음)
		resource : resour,
		header : {
			title : "선택된 자산"
		},
	/**
	 * // 액션 컬럼에 대한 정보를 세팅합니다. 액션 컬럼은 특정한 Action을 일으킬 수 있는 역할을 하는 컬럼을 의미합니다. //
	 * 행을 삭제하는 Action을 수행할 버튼을 배치합니다. actionColumns: [{ column: "DEL_ACTION", //
	 * 액션 컬럼의 헤더명 (그리드 리소스) template: nkia.ui.html.icon("remove", "remove_row"), //
	 * 버튼 아이콘("아이콘 명", "생성할 CSS명(onClick 이벤트 명과 매칭되어야 함)") onClick: { // 이벤트
	 * 연결(실행 할 이벤트명 매칭) "remove_row": removeData } }]
	 */
	});

	var arrow = {
		height : 30,
		cols : [ {
			view : "template",
			template : nkia.ui.html.icon("arrow_down", "add_row"),
			onClick : {
				"add_row" : function() {
					addData(classid, classid2, classid3);
				}
			}
		}, {
			view : "template",
			template : nkia.ui.html.icon("arrow_up", "del_row"),
			onClick : {
				"del_row" : function() {
					removeData(classid, classid2, classid3);
				}
			}
		} ]
	}

	var popupWindow = createWindow({
		id : "popupWindow2",
		width : 800,
		height : 650,
		header : {
			title : "유지보수연계자산목록"
		},

		body : {

			rows : new Array().concat(assetSearchform, popupGrid, arrow,subgrid)
		},
		footer : {
			buttons : {
				align : "center",
				items : [ createBtnComp({
					label : '적 용',
					type : "form",
					click : function() {
						var srow = $$("subgrid")._getRows();

						if (srow[0].ITSM_ASSET_ID) {
							$$('grid')._removeAllRow();

							$$('grid')._addRow(srow);
						}

						popupWindow.close();
					}
				}) ]
			}
		},
		closable : true
	});

	var grow = $$("grid")._getRows();

	//if (grow[0].ITSM_ASSET_ID) {
		$$('subgrid')._addRow(grow);
	//}

	// 팝업창 오픈
	$$("popupWindow2").show();

}

/**
 * 검색
 */
function assetSearchList(classid,classid2,classid3){
	
	// 검색폼의 값들 추출
	var data = {
			class_id : classid,
			class_id2 : classid2,
			class_id3 : classid3,
			host_nm : $$("assetSearchform")._getFieldValue("search_value")

	};
	
	// 검색폼의 데이터를 인자값으로 그리드 리로드
	$$("popupGrid2")._reload(data);
	
}

function removeData(classid, classid2, classid3) {

	$$("subgrid")._removeRow();

	// 선택하지 못하는 데이터를 다시 선택가능으로 되돌리기 위해 그리드를 다시 로드합니다.
	// 단, 현재 포커스 되어 있는 페이지를 유지합니다.
	$$("popupGrid2")._reload({
		class_id : classid,
		class_id2 : classid2,
		class_id3 : classid3
	}, true); // 두 번째 인자값 : 페이지 유지 여부
}

function addData(classid, classid2, classid3) {

	// 체크 된 데이터 추출
	var data = $$("popupGrid2")._getSelectedRows();

	// 선택 한 데이터가 없을 시, 메시지를 띄워줍니다.
	if (data.length < 1) {
		nkia.ui.utils.notification({
			type : 'error',
			message : '선택된 정보가 없습니다.'
		});
		return false;
	}

	// 체크된 데이터를 대상 그리드에 add 합니다.
	$$("subgrid")._addRow(data);

	// add 된 데이터는 선택하지 못하도록 하기 위해 그리드를 다시 로드합니다.
	// 단, 현재 포커스 되어 있는 페이지를 유지합니다.
	$$("popupGrid2")._reload({
		class_id : classid,
		class_id2 : classid2,
		class_id3 : classid3
	}, true); // 두 번째 인자값 : 페이지 유지 여부
}

/**
 * 상면코드 선택 팝업
 */
nkia.ui.popup.nbpm.locationPopup = function(){
	this.setRender = function(props) {

		var _this = this;
		_this.props = props;
		var params = ( typeof props.params === undefined ) ? {} : props.params;		


	// 선택한 트리의 값의 상세정보를 구하는 함수
	this.getLocationDetail = function(locCode) {
		
		var locationInfo = {};
		
		nkia.ui.utils.ajax({
			viewId: 'assetDetailViewer',
			url: '/itg/itam/location/selectLocation.do',
			params: { loc_code : locCode },
			async: false,
			isMask: false,
			success: function(response){
				locationInfo = response.resultMap;
			}
		});
		
		return locationInfo;
	}

	
	this.searchTreeList = function() {
		var param = $$('locationSearchForm')._getValues();
		param.use_yn = 'Y';
		
		$$('locationSelectTree')._reload(param);
	}
	
	var _this = this;

	var locationSearchForm = createFormComp({
		id: "locationSearchForm",
		elementsConfig: {
    		labelPosition: "left"
    	},
		fields: {
			colSize: 1,
			items: [
				{colspan: 1, item: createCodeComboBoxComp({
					label: '센터위치',
					name: 'locationTopCode',
					code_grp_id: 'CENTER',
					value: 'D1',
					attachChoice: true,
					on: {
						onChange: this.searchTreeList.bind(_this) 
						}
				})
				}
			]
		}
	});

	
	var locationSelectTree = createTreeComponent({
		id : 'locationSelectTree',
		url : '/itg/itam/location/searchLocation.do',
		expColable: true, // 전체펼침/접기 표시 여부
		filterable: true, // 검색필드 표시 여부
        params: {use_yn: 'Y', locationTopCode: 'D1'},
		expandLevel: 2,
		header: {
			title: "상면코드"
		}
	});
	
	var locationForm = createFormComp({
		id: "locationForm",
		elementsConfig: {
    		labelPosition: "left"
    	},
		fields: {
			colSize: 1,
			items: [
				{colspan: 1, item: createTextFieldComp({label: '기타위치2222', name: 'POSITION_DETAIL_POP' })}
			]
		}
	});

	
	var bodyScrollView = {
        view: "scrollview",
        body: {
			cols: [{
                rows: new Array().concat(locationSearchForm,locationSelectTree, locationForm)
            }]
		}
	};
	
	_this.popupWindow = createWindow({
    	id: props.id,
    	width: 500,
    	height: 500,
    	header: {
    		title: "상면코드"
    	},
    	body: {
    		rows: [bodyScrollView]
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
		            createBtnComp({label: '선 택', type : "form", click: this.choice.bind(_this)})
		        ]
    		}
    	},
    	closable: ( props.closable === undefined ) ? true : props.closable 
    });

	};
	
	//선택 이벤트
	this.choice = function(){
		
		// 보여지는 name필드 뒤에 붙는 suffix(자바 원장자동화에서 정의)
		var _NAME_FIELD_SUFFIX = this.name_field_suffix;
		var treeItem = $$("locationSelectTree").getSelectedItem();
		var form = $$("locationForm");
		var formData = form._getValues();
		var _this = this;

		if(treeItem){
			var values = {};

			var locationDetail = this.getLocationDetail(treeItem.node_id);
			var locType = locationDetail.LOC_TYPE;
			var locLeaf = treeItem.leaf;

			if(locType == null) {
				nkia.ui.utils.notification({
	                type: 'error',
	                message: '최상위 계층은 선택하실 수 없습니다.'
	            });
				return false;
			}

			if(locLeaf == false) {
				nkia.ui.utils.notification({
	                type: 'error',
	                message: '마지막 노드만 선택할 수 있습니다.'
	            });
				return false;
			}
			
			if(locationDetail.LOC_CODE.match(/NOTCODE/gi)) {
				if(!formData['POSITION_DETAIL_POP']) {
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '기타위치을 입력해 주세요.'
		            });
					return false;
				}
			} else {
				form._setValues({POSITION_DETAIL_POP: ''});
			}

			var reference = this.props.reference;
			var fields = reference.fields;

			reference.form._setFieldValue('LOCATION_CD',locationDetail.LOCATION_CD);
			reference.form._setFieldValue('LOCATION_CD_NM',locationDetail.LOCATION_CD_NM);
			reference.form._setFieldValue(fields.name,locationDetail.LOC_NM);    			
			reference.form._setFieldValue('CENTER_POSITION_CD_NM',locationDetail.LOC_NM);
			reference.form._setFieldValue('CENTER_POSITION_CD',locationDetail.LOC_CODE);
			reference.form._setFieldValue('POSITION_DETAIL',formData['POSITION_DETAIL_POP']);

	    	var values = {
	    			CENTER_POSITION_CD : locationDetail.LOC_CODE,
	    			CENTER_POSITION_CD_NM : locationDetail.LOC_NM

	    	};

	    	reference.form._setValues(values);
			this.popupWindow.close();
		}else{
	        nkia.ui.utils.notification({
	            type: 'error',
	            message: '선택된 정보가 없습니다.'
	        });
	    }

	}

	
	

};

/********************************************
 * 스냅샷설정기능
 * Date: 2019-12-10
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.snapshot = function() {
	// 속성 Properties
	this.id = "snapshotPop";
	this.title = "스냅샷 설정";
	this.closable = true;
	this.width = 1200;
	this.height = 600;
	this.url = "/itg/customize/ubit/searchSnapshot.do";
	this.resource = "grid.nbpm.change.snapshot";
	this.params = {};
	this.reference = {};
	this.type = "";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: "스냅샷 설정",
	        	buttons: {
					items: [
						createBtnComp({
							id:"helpBtn",
							label:"도움말",
							type: "form",
							click: this.helpPop.bind(_this)
						})
					]
				}
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.grid)
			            }]
					}
			};
		_this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height : nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저 장', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
		
	};
	this.helpPop = function(){
		var paramMap ={};
		var CUSTOM_COLUMNS = {
				gridHeader: "GUBUN,SNAPSHOT,CONFIG",
				gridText: "구분,스냅샷,설정정보",
				gridWidth: "200,200,200",
				gridFlex: "na,1,1",
				gridXtype: "na,na,na",
				gridSortable: "na,na,na",
				gridAlign: "left,left,left"
			}
		var COMMAND_COLUMNS = {
				gridHeader: "SNAPSHOT,CATEGORY,COMMAND",
				gridText: "스냅샷대상,범주,명령어",
				gridWidth: "200,200,200",
				gridFlex: "na,1,1",
				gridXtype: "na,na,na",
				gridSortable: "na,na,na",
				gridAlign: "left,left,left"
			}
		var helpGrid = createGridComp({
			id: 'helpGrid',
			header: {
	        	title: '도움말',
		    },
			autoLoad : false,
			columns : CUSTOM_COLUMNS,
			cellMerges: ["GUBUN"]
		});
		
		var commandGrid = createGridComp({
			id: 'commandGrid',
			header: {
	        	title: '스냅샷 설정정보',
		    },
			autoLoad : false,
			columns : COMMAND_COLUMNS,
			cellMerges: ["SNAPSHOT"]
		});
		
		 var bodyScrollView = {
		    view: "scrollview",
		    body: {
				cols: [{
					rows: new Array().concat(helpGrid,commandGrid)
		        }]
			}
		 };
		var helpPopWindow = createWindow({
	    	id: "helpPopWindow",
	    	width: 1000,
	    	header: {
	    		title: "도움말"
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	closable: true
	    });
		var tempCust = {};
		var valArr = [];
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●CPU 사용률";
		tempCust["CONFIG"] = "●HOST목록";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●메모리 사용량/메모리 총량/메모리 사용률";
		tempCust["CONFIG"] = "●Default Gw 정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●파일시스템 사용량";
		tempCust["CONFIG"] = "●마운트 가능한 파일시스템, 파티션 정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●DB 저장공간 할당량/사용량/사용률";
		tempCust["CONFIG"] = "●파일시스템 마운트 상황";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "●사용자정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "●그룹정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "●스케줄작업";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "네트워크";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "show running-config";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "DB";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "공통정보";
		valArr[0] = tempCust;//CONCAT_KEY_STRING
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "DB";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "리스너 정보(ex: initsid.ora,listener.ora)";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "WAS";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "WAS 설정 파일(exP: server.xml)";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr); ///// 도움말창
		///////////////////////////////////////////
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "/etc/hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "/etc/sysconfig/network";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "/etc/fstab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/mtab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "/etc/passwd";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/group";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "crontab -l";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "/etc/hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "/etc/rc.config.d/netconf";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "/etc/fstab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/mnttab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "/etc/passwd";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/group";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "crontab -l";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "/etc/hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "netstat -nr";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "/etc/filesystems";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "/etc/passwd";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/group";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "crontab -l";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "C:\Windows\System32\drivers\etc\hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "ipconfig /all";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "list disk";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "wmic path win32_volume get DriveLetter, label, FileSystem, Capacity";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "net user";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "net localgroup";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "schtasks";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "CISCO";
		tempCust["CATEGORY"] = "네트워크 설정";
		tempCust["COMMAND"] = "show running-config";
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "ORACLE";
		tempCust["CATEGORY"] = "리스너";
		tempCust["COMMAND"] = "listener.ora";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "ORACLE";
		tempCust["CATEGORY"] = "DB설정정보";
		tempCust["COMMAND"] = "initsid.ora";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "MS SQL";
		tempCust["CATEGORY"] = "DB설정정보";
		tempCust["COMMAND"] = "";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "TIBERO";
		tempCust["CATEGORY"] = "DB설정정보";
		tempCust["COMMAND"] = "";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "JEUS";
		tempCust["CATEGORY"] = "설정정보";
		tempCust["COMMAND"] = "$JEUS_HOME/config/hostname/JEUSMain.xml(jeus5,6)";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "JEUS";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "$JEUS_HOME/domains/jeus_domain/config/domain.xml(jeus 7,8)";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "TOMCAT";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "$TOMCAT_HOME/conf/server.xml";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		////스냅샷 설정정보 창
		// 팝업창 오픈
		$$("helpPopWindow").show();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(){
		var item = $$(this.id + "_grid")._getRows();
		if(item.length < 1){
			alert("스냅샷 건 이 없습니다.");
			return false;
		}
		var paramMap ={};
		paramMap["SNAPSHOT_LIST"] = item;
		paramMap["SR_ID"] = item[0].SR_ID;
		if(item){
            nkia.ui.utils.ajax({
    			url: "/itg/customize/ubit/insertSnapshot.do",
    			params: paramMap,
    			success: function(data) {
    				if (data.success) {
    					alert(data.resultMsg);
    					$$('change_plan')._setValues({snapshot_set:"작성완료"});
    				}
    			}			
    		});
            this.window.close();
		}
	};

};

/********************************************
 * 스냅샷설정기능
 * Date: 2019-12-10
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.snapshot_disp = function() {
	// 속성 Properties
	this.id = "snapshotPop";
	this.title = "스냅샷 설정";
	this.closable = true;
	this.width = 1200;
	this.height = 600;
	this.url = "/itg/customize/ubit/searchSnapshot.do";
	this.resource = "grid.nbpm.change.snapshotdisp";
	this.params = {};
	this.reference = {};
	this.type = "";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: "스냅샷 설정",
	        	buttons : {
		            items : [
		                createBtnComp({
		                	label : '작업 후 스냅샷 요청', 
		                	id : 'snapshot_disp_btn',
		                	type : "form",
		                	click : function() {
		                		snap_restful();
		                	}
		                }),
		                createBtnComp({
							id:"helpBtn",
							label:"도움말",
							type: "form",
							click: this.helpPop.bind(_this)
						})
		            ]
		        }
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			actionColumns : [{
				column: "SERVER_SNAPSHOT",
				template: function(value) {
					if(value["SERVER_SNAPSHOT"] == '1') {
						return nkia.ui.html.icon("check_on", "editEvent");
					} else {
						return "";
					}
				}
			},
			{
				column: "SERVER_CONFIG",
				template: function(value) {
					if(value["SERVER_CONFIG"] == '1') {
						return nkia.ui.html.icon("check_on", "editEvent");
					} else {
						return "";
					}
				}
			},
			{
				column: "NETWORK_CONFIG",
				template: function(value) {
					if(value["NETWORK_CONFIG"] == '1') {
						return nkia.ui.html.icon("check_on", "editEvent");
					} else {
						return "";
					}
				}
			},
			{
				column: "DB_CONFIG",
				template: function(value) {
					if(value["DB_CONFIG"] == '1') {
						return nkia.ui.html.icon("check_on", "editEvent");
					} else {
						return "";
					}
				}
			},
			{
				column: "WAS_CONFIG",
				template: function(value) {
					if(value["WAS_CONFIG"] == '1') {
						return nkia.ui.html.icon("check_on", "editEvent");
					} else {
						return "";
					}
				},
				onClick: {
					"editEvent": function(e, row, el){
						var clickRecord = this.getItem(row.row);
						//this.sss.bind(_this);
						snapDetail(clickRecord,row);
					}
				}
			}
			]
		});
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.grid)
			            }]
					}
			};
		_this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height : nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	closable: this.closable 
	    });
		
	};
	this.helpPop = function(){
		var paramMap ={};
		var CUSTOM_COLUMNS = {
				gridHeader: "GUBUN,SNAPSHOT,CONFIG",
				gridText: "구분,스냅샷,설정정보",
				gridWidth: "200,200,200",
				gridFlex: "na,1,1",
				gridXtype: "na,na,na",
				gridSortable: "na,na,na",
				gridAlign: "left,left,left"
			}
		var COMMAND_COLUMNS = {
				gridHeader: "SNAPSHOT,CATEGORY,COMMAND",
				gridText: "스냅샷대상,범주,명령어",
				gridWidth: "200,200,200",
				gridFlex: "na,1,1",
				gridXtype: "na,na,na",
				gridSortable: "na,na,na",
				gridAlign: "left,left,left"
			}
		var helpGrid = createGridComp({
			id: 'helpGrid',
			header: {
	        	title: '도움말',
		    },
			autoLoad : false,
			columns : CUSTOM_COLUMNS,
			cellMerges: ["GUBUN"]
		});
		
		var commandGrid = createGridComp({
			id: 'commandGrid',
			header: {
	        	title: '스냅샷 설정정보',
		    },
			autoLoad : false,
			columns : COMMAND_COLUMNS,
			cellMerges: ["SNAPSHOT"]
		});
		
		 var bodyScrollView = {
		    view: "scrollview",
		    body: {
				cols: [{
					rows: new Array().concat(helpGrid,commandGrid)
		        }]
			}
		 };
		var helpPopWindow = createWindow({
	    	id: "helpPopWindow",
	    	width: 1000,
	    	header: {
	    		title: "도움말"
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	closable: true
	    });
		var tempCust = {};
		var valArr = [];
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●CPU 사용률";
		tempCust["CONFIG"] = "●HOST목록";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●메모리 사용량/메모리 총량/메모리 사용률";
		tempCust["CONFIG"] = "●Default Gw 정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●파일시스템 사용량";
		tempCust["CONFIG"] = "●마운트 가능한 파일시스템, 파티션 정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "●DB 저장공간 할당량/사용량/사용률";
		tempCust["CONFIG"] = "●파일시스템 마운트 상황";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "●사용자정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "●그룹정보";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "서버";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "●스케줄작업";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "네트워크";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "show running-config";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "DB";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "공통정보";
		valArr[0] = tempCust;//CONCAT_KEY_STRING
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "DB";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "리스너 정보(ex: initsid.ora,listener.ora)";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr);
		tempCust["GUBUN"] = "WAS";
		tempCust["SNAPSHOT"] = "";
		tempCust["CONFIG"] = "WAS 설정 파일(exP: server.xml)";
		valArr[0] = tempCust;
		$$("helpGrid")._addRow(valArr); ///// 도움말창
		///////////////////////////////////////////
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "/etc/hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "/etc/sysconfig/network";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "/etc/fstab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/mtab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "/etc/passwd";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/group";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "리눅스";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "crontab -l";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "/etc/hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "/etc/rc.config.d/netconf";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "/etc/fstab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/mnttab";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "/etc/passwd";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/group";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "유닉스";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "crontab -l";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "/etc/hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "netstat -nr";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "/etc/filesystems";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "/etc/passwd";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "/etc/group";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "AIX";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "crontab -l";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "IP";
		tempCust["COMMAND"] = "C:\Windows\System32\drivers\etc\hosts";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "NW";
		tempCust["COMMAND"] = "ipconfig /all";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "파일시스템";
		tempCust["COMMAND"] = "list disk";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "wmic path win32_volume get DriveLetter, label, FileSystem, Capacity";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "계정";
		tempCust["COMMAND"] = "net user";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "net localgroup";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "WINDOWS";
		tempCust["CATEGORY"] = "예약된 작업";
		tempCust["COMMAND"] = "schtasks";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "CISCO";
		tempCust["CATEGORY"] = "네트워크 설정";
		tempCust["COMMAND"] = "show running-config";
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "ORACLE";
		tempCust["CATEGORY"] = "리스너";
		tempCust["COMMAND"] = "listener.ora";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "ORACLE";
		tempCust["CATEGORY"] = "DB설정정보";
		tempCust["COMMAND"] = "initsid.ora";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "MS SQL";
		tempCust["CATEGORY"] = "DB설정정보";
		tempCust["COMMAND"] = "";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "TIBERO";
		tempCust["CATEGORY"] = "DB설정정보";
		tempCust["COMMAND"] = "";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "JEUS";
		tempCust["CATEGORY"] = "설정정보";
		tempCust["COMMAND"] = "$JEUS_HOME/config/hostname/JEUSMain.xml(jeus5,6)";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "JEUS";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "$JEUS_HOME/domains/jeus_domain/config/domain.xml(jeus 7,8)";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		tempCust["SNAPSHOT"] = "TOMCAT";
		tempCust["CATEGORY"] = "";
		tempCust["COMMAND"] = "$TOMCAT_HOME/conf/server.xml";
		valArr[0] = tempCust;
		$$("commandGrid")._addRow(valArr);
		////스냅샷 설정정보 창
		// 팝업창 오픈
		$$("helpPopWindow").show();
	}
};
function snap_restful(){
	var params = {};
	var grid = $$("snapshotPop_grid")._getRows();
	if(grid.length < 1){
		alert("요청 할 자원이 없습니다.");
	}else{
		params["sr_id"] = $$("snapshotPop_grid").config.params.srId;
 	    nkia.ui.utils.ajax({
		url: "/itg/customize/ubit/snapshotRestful.do",
		params: params,
		success: function(data) {
			if (data.success) {
				alert("요청 하였습니다.");
			}
		}
	   });

	}
	
	
}
function snapDetail(record,row){
	var param = {};
	var type = row.column;
	var resource = "";
	var cell =[];
	param["sr_id"] = record.SR_ID;
	param["ems_id"] = record.EMS_ID;
	if(type == "SERVER_SNAPSHOT"){
		rtype = "1";
		param["request_type"] = "1";
	}else if(type == "SERVER_CONFIG"){
		rtype = "2";
		resource = "grid.nbpm.change.snapshotdetail";
		cell = ["REQUEST_NM"];
		param["request_type"] = "2";
	}else if(type == "NETWORK_CONFIG"){
		rtype = "3";
		resource = "grid.nbpm.change.snapshotdetail";
		cell = ["REQUEST_NM"]
		param["request_type"] = "3";
	}else if(type == "DB_CONFIG"){
		rtype = "4";
		resource = "grid.nbpm.change.snapshotdetailB";
		cell = ["RESPONSE_INSTANCE","REQUEST_NM"];
		param["request_type"] = "4";
	}else if(type == "WAS_CONFIG"){
		rtype = "5";
		resource = "grid.nbpm.change.snapshotdetailB";
		cell = ["RESPONSE_INSTANCE","REQUEST_NM"];
		param["request_type"] = "5";	
	}
	if(rtype =="1"){
		var memoryGrid = createGridComp({
			id : "memoryGrid",
			url : "/itg/customize/ubit/searchSnapshotDetailMemory.do",
			params : param,
			resource : "grid.nbpm.change.snapshotdetailMemory",
			header : {
				title : "서버"
			}
		});
		var diskGrid = createGridComp({
			id : "diskGrid",
			url : "/itg/customize/ubit/searchSnapshotDetailDisk.do",
			params : param,
			resource : "grid.nbpm.change.snapshotdetailDisk",
			header : {
				title : "파일시스템"
			}
		});
		var DBGrid = createGridComp({
			id : "DBGrid",
			url : "/itg/customize/ubit/searchSnapshotDetailDB.do",
			params : param,
			resource : "grid.nbpm.change.snapshotdetailDB",
			header : {
				title : "DB"
			}
		});
		var popupWindow = createWindow({
	    	id: "snapDetailWindow",
	    	width: 1000,
	    	height: 600,
	    	header: {
	    		title: "스냅샷상세정보"
	    	},
	    	body: {
	    		rows: new Array().concat(memoryGrid,diskGrid,DBGrid)
	    	},
	    	closable: true
	    });
	}else{
		var popupGrid = createGridComp({
			id : "snapDetailGrid",
			url : "/itg/customize/ubit/searchSnapshotDetail.do",
			params : param,
			resource : resource,
			header : {
				title : "속성"
			},
			on: {
				onItemDblClick: function(record,row) {
	        						diff(record,row);
	        					}
			},
			cellMerges: cell
		});
		
		var popupWindow = createWindow({
	    	id: "snapDetailWindow",
	    	width: 1000,
	    	height: 600,
	    	header: {
	    		title: "스냅샷상세정보"
	    	},
	    	body: {
	    		rows: new Array().concat(popupGrid)
	    	},
	    	closable: true
	    });
	}
	
	
	// 팝업창 오픈
	$$("snapDetailWindow").show();
	
}

function diff(record,row){
	//alert("aaa");
	var srId = $$("snapDetailGrid").getItem(record.row).SR_ID;
	var attr = $$("snapDetailGrid").getItem(record.row).RESPONSE_ATTR;
	var rtype = $$("snapDetailGrid").getItem(record.row).REQUEST_TYPE;
	var instance = $$("snapDetailGrid").getItem(record.row).RESPONSE_INSTANCE;
	var ems_id = $$("snapDetailGrid").getItem(record.row).EMS_ID;
	var url ="/itg/customize/ubit/snapshotDiff.do?srId="+srId+"&rtype="+rtype+"&attr="+attr+"&instance="+instance+"&ems_id="+ems_id;
	var popupId = "diff";
	var width = 1000;
	var height = 600;
	var optStr = "width="+width+",height="+height+",history=no,resizable=yes,status=no,scrollbars=no,member=no";
	var popup = window.open(url, popupId, optStr);
}

/********************************************
 * 체크리스트 작성기능 - 변경전
 * Date: 2019-12-10
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.checklist_before = function() {
	// 속성 Properties
	this.id = "checklistPop";
	this.title = "체크리스트 작성";
	this.closable = true;
	this.width = 1000;
	this.height = 800;
	this.url = "/itg/customize/ubit/selectChecklistDetail.do";
	this.resource = "grid.nbpm.change.checklistbefore";
	this.params = {};
	this.reference = {};
	this.type = "";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		var id = this.id;
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
		    fields: {
		        colSize: 4,
		        items: [
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "서버", name: "CHECK_SERVER_BEFORE", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		        			onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_server_grid_header").show();
		        					$$(id+"_server_grid").show();
		        				}else{
		        					$$(id+"_server_grid_header").hide();
		        					$$(id+"_server_grid").hide();
		        				}
		        				var data = $$(id+"_server_grid")._getRows();
        						for(var i=0; i<data.length; i++) {
									var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_server_grid").updateItem(rowId, row);
								}

		        			}
		        		}})},
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "네트워크", name: "CHECK_NETWORK_BEFORE", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		            		onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_network_grid").show();
		        					$$(id+"_network_grid_header").show();
		        				}else{
		        					$$(id+"_network_grid").hide();
		        					$$(id+"_network_grid_header").hide();
		        					
		        				}
		        				var data = $$(id+"_network_grid")._getRows();
		        				for(var i=0; i<data.length; i++) {
									var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_network_grid").updateItem(rowId, row);
								}
		        			}
		        		}})},
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "DB", name: "CHECK_DB_BEFORE", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		            		onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_db_grid").show();
		        					$$(id+"_db_grid_header").show();
		        				}else{
		        					$$(id+"_db_grid").hide();
		        					$$(id+"_db_grid_header").hide();
		        				}
		        				var data = $$(id+"_db_grid")._getRows();
								for(var i=0; i<data.length; i++) {
        							var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_db_grid").updateItem(rowId, row);
								}
		        			}
		        		}})},
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "WAS", name: "CHECK_WAS_BEFORE", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		            		onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_was_grid").show();
		        					$$(id+"_was_grid_header").show();
		        				}else{
		        					$$(id+"_was_grid").hide();
		        					$$(id+"_was_grid_header").hide();
		        				}
		        				var data = $$(id+"_was_grid")._getRows();
								for(var i=0; i<data.length; i++) {
        							var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_was_grid").updateItem(rowId, row);
								}
		        			}
		        		}})}	
		        ]
		    }
		});
		this.params["TYPE"] = "CHECK_SERVER_BEFORE";
		this.serverGrid = createGridComp({
			id: this.id + '_server_grid',
			autoheight : true,
			header: {
	        	title: "서버"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		this.params["TYPE"] = "CHECK_NETWORK_BEFORE";
		this.networkGrid = createGridComp({
			id: this.id + '_network_grid',
			autoheight : true,
			header: {
	        	title: "네트워크"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		this.params["TYPE"] = "CHECK_DB_BEFORE";
		this.dbGrid = createGridComp({
			id: this.id + '_db_grid',
			autoheight : true,
			header: {
	        	title: "DB"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		this.params["TYPE"] = "CHECK_WAS_BEFORE";
		this.wasGrid = createGridComp({
			id: this.id + '_was_grid',
			autoheight : true,
			header: {
	        	title: "WAS"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		})
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.searchForm,this.serverGrid,this.networkGrid,this.dbGrid,this.wasGrid)
			            }]
					}
			};
		_this.window = createWindow({
	    	id: this.id,
	    	width : this.width,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저 장', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
		$$(this.id + '_db_grid_header').hide();
		$$(this.id + '_server_grid_header').hide();
		$$(this.id + '_network_grid_header').hide();
		$$(this.id + '_was_grid_header').hide()
		$$(this.id + '_db_grid').hide();
		$$(this.id + '_server_grid').hide();
		$$(this.id + '_network_grid').hide();
		$$(this.id + '_was_grid').hide();
		var checkMap  ={};
		checkMap["SR_ID"] = this.params.srId;
		nkia.ui.utils.ajax({
			url: "/itg/customize/ubit/selectChecklist.do",
			params: checkMap,
			success: function(data) {
				if (data.success) {
					for(var z=0; z<data.gridVO.rows.length;z++){
						$$(id + '_searchForm')._setFieldValue(data.gridVO.rows[z].CHECKLIST_TYPE,"Y");
					}
				}
			}
	   });
		
	};
	// 적용(선택) 이벤트
	this.choice = function(){
		var chksw = true;
		var param = {};
		var checkInfo = {};
		if($$(this.id+'_searchForm')._getValues().CHECK_SERVER_BEFORE == "Y"){
			var data = $$(this.id+"_server_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N" || row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				 checkInfo['TYPE'] = 'CHECK_SERVER_BEFORE';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['SERVERINFO'] = checkInfo;
				 checkInfo = {};
			}
		}
		if($$(this.id+'_searchForm')._getValues().CHECK_NETWORK_BEFORE == "Y"){
			var data = $$(this.id+"_network_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N"||row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				checkInfo['TYPE'] = 'CHECK_NETWORK_BEFORE';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['NETWORKINFO'] = checkInfo;
				 checkInfo = {};
			}
		}
		if($$(this.id+'_searchForm')._getValues().CHECK_DB_BEFORE == "Y"){
			var data = $$(this.id+"_db_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N"||row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				 checkInfo['TYPE'] = 'CHECK_DB_BEFORE';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['DBINFO'] = checkInfo;
				 checkInfo = {};
			}
		}
		if($$(this.id+'_searchForm')._getValues().CHECK_WAS_BEFORE == "Y"){
			var data = $$(this.id+"_was_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N"||row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				 checkInfo['TYPE'] = 'CHECK_WAS_BEFORE';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['WASINFO'] = checkInfo;
				 checkInfo = {};
			}
		}
		if(chksw){
			var form = $$(this.id+'_searchForm')._getValues();
			param["SR_ID"] = this.params.srId;
			   nkia.ui.utils.ajax({
	    			url: "/itg/customize/ubit/insertChecklist.do",
	    			params: param,
	    			success: function(data) {
	    				if (data.success) {
	    					alert("등록 되었습니다.");
	    				}
	    			}
			   });
			$$('change_plan')._setValues({checklist_before:"작성완료"});
			
			this.window.close();
		}else{
			alert("미체크된 리스트를 확인 하십시오.");
		}
	};
};

/********************************************
 * 체크리스트 작성기능 - 변경후
 * Date: 2019-12-10
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.nbpm.checklist_after = function() {
	// 속성 Properties
	this.id = "checklistPop";
	this.title = "체크리스트 작성";
	this.closable = true;
	this.width = 1000;
	this.height = 800;
	this.url = "/itg/customize/ubit/selectChecklistDetail.do";
	this.resource = "grid.nbpm.change.checklistafter";
	this.params = {};
	this.reference = {};
	this.type = "";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		var id = this.id;
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
		    fields: {
		        colSize: 4,
		        items: [
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "서버", name: "CHECK_SERVER_AFTER", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		        			onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_server_grid_header").show();
		        					$$(id+"_server_grid").show();
		        				}else{
		        					$$(id+"_server_grid_header").hide();
		        					$$(id+"_server_grid").hide();
		        				}
		        				var data = $$(id+"_server_grid")._getRows();
        						for(var i=0; i<data.length; i++) {
									var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_server_grid").updateItem(rowId, row);
								}

		        			}
		        		}})},
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "네트워크", name: "CHECK_NETWORK_AFTER", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		            		onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_network_grid").show();
		        					$$(id+"_network_grid_header").show();
		        				}else{
		        					$$(id+"_network_grid").hide();
		        					$$(id+"_network_grid_header").hide();
		        					
		        				}
		        				var data = $$(id+"_network_grid")._getRows();
		        				for(var i=0; i<data.length; i++) {
									var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_network_grid").updateItem(rowId, row);
								}
		        			}
		        		}})},
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "DB", name: "CHECK_DB_AFTER", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		            		onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_db_grid").show();
		        					$$(id+"_db_grid_header").show();
		        				}else{
		        					$$(id+"_db_grid").hide();
		        					$$(id+"_db_grid_header").hide();
		        				}
		        				var data = $$(id+"_db_grid")._getRows();
								for(var i=0; i<data.length; i++) {
        							var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_db_grid").updateItem(rowId, row);
								}
		        			}
		        		}})},
		            { colspan: 1, item: createSingleCheckBoxFieldComp({labelRight: "WAS", name: "CHECK_WAS_AFTER", value:"N", checkValue:"Y", uncheckValue:"N",
		            	on:{
		            		onChange : function(newv,oldv) {
		        				if(newv == "Y"){
		        					$$(id+"_was_grid").show();
		        					$$(id+"_was_grid_header").show();
		        				}else{
		        					$$(id+"_was_grid").hide();
		        					$$(id+"_was_grid_header").hide();
		        				}
		        				var data = $$(id+"_was_grid")._getRows();
								for(var i=0; i<data.length; i++) {
        							var row = data[i];
									var rowId = row.id;
									row["CHECK_YN"] = "N";
									$$(id+"_was_grid").updateItem(rowId, row);
								}
		        			}
		        		}})}	
		        ]
		    }
		});
		this.params["TYPE"] = "CHECK_SERVER_AFTER";
		this.serverGrid = createGridComp({
			id: this.id + '_server_grid',
			header: {
	        	title: "서버"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		this.params["TYPE"] = "CHECK_NETWORK_AFTER";
		this.networkGrid = createGridComp({
			id: this.id + '_network_grid',
			header: {
	        	title: "네트워크"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		this.params["TYPE"] = "CHECK_DB_AFTER";
		this.dbGrid = createGridComp({
			id: this.id + '_db_grid',
			header: {
	        	title: "DB"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		this.params["TYPE"] = "CHECK_WAS_AFTER";
		this.wasGrid = createGridComp({
			id: this.id + '_was_grid',
			header: {
	        	title: "WAS"
		    },
		    resource: this.resource,
		    params: this.params,
			url: this.url
		});
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.searchForm,this.serverGrid,this.networkGrid,this.dbGrid,this.wasGrid)
			            }]
					}
			};
		_this.window = createWindow({
	    	id: this.id,
	    	width : this.width,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저 장', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
		$$(this.id + '_db_grid_header').hide();
		$$(this.id + '_server_grid_header').hide();
		$$(this.id + '_network_grid_header').hide();
		$$(this.id + '_was_grid_header').hide();
		$$(this.id + '_db_grid').hide();
		$$(this.id + '_server_grid').hide();
		$$(this.id + '_network_grid').hide();
		$$(this.id + '_was_grid').hide();
		var checkMap  ={};
		checkMap["SR_ID"] = this.params.srId;
		nkia.ui.utils.ajax({
			url: "/itg/customize/ubit/selectChecklist.do",
			params: checkMap,
			success: function(data) {
				if (data.success) {
					for(var z=0; z<data.gridVO.rows.length;z++){
						$$(id + '_searchForm')._setFieldValue(data.gridVO.rows[z].CHECKLIST_TYPE,"Y");
					}
				}
			}
	   });
		
	};
	// 적용(선택) 이벤트
	this.choice = function(){
		var chksw = true;
		var param = {};
		var checkInfo = {};
		if($$(this.id+'_searchForm')._getValues().CHECK_SERVER_AFTER == "Y"){
			var data = $$(this.id+"_server_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N"||row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				 checkInfo['TYPE'] = 'CHECK_SERVER_AFTER';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['SERVERINFO'] = checkInfo;
				 checkInfo = {};
			}
		}
		if($$(this.id+'_searchForm')._getValues().CHECK_NETWORK_AFTER == "Y"){
			var data = $$(this.id+"_network_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N"||row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				 checkInfo['TYPE'] = 'CHECK_NETWORK_AFTER';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['NETWORKINFO'] = checkInfo;
				 checkInfo = {};
			}
		}
		if($$(this.id+'_searchForm')._getValues().CHECk_DB_AFTER == "Y"){
			var data = $$(this.id+"_db_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N"||row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				 checkInfo['TYPE'] = 'CHECK_DB_AFTER';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['DBINFO'] = checkInfo;
				 checkInfo = {};
			}
		}
		if($$(this.id+'_searchForm')._getValues().CHECk_WAS_AFTER == "Y"){
			var data = $$(this.id+"_was_grid")._getRows();
			for(var i=0; i<data.length; i++) {
				var row = data[i];
				if(row.CHECK_YN == "N"||row.CHECK_YN == "0"){
					chksw = false;
					break;
				}
			}
			if(chksw){
				 checkInfo['TYPE'] = 'CHECK_WAS_AFTER';
				 checkInfo['CHECK_GROUP_CD'] = data[0].CHECK_GROUP_CD;
				 checkInfo['VERSION'] = data[0].VERSION;
				 param['WASINFO'] = checkInfo;
				 checkInfo = {};
			}
		}		
		if(chksw){
			var form = $$(this.id+'_searchForm')._getValues();
			param["SR_ID"] = this.params.srId;
			nkia.ui.utils.ajax({
    			url: "/itg/customize/ubit/insertChecklist.do",
    			params: param,
    			success: function(data) {
    				if (data.success) {
    					alert("등록 되었습니다.");
    				}
    			}
            });
			$$('change_result')._setValues({checklist_after:"작성완료"});
			this.window.close();
		}else{
			alert("미체크된 리스트를 확인 하십시오.");
		}
        
	};
};

/********************************************
 * Date: 2020-01-21
 * Version:
 *	UBIT 관제 삭제 팝업.
 ********************************************/
nkia.ui.popup.nbpm.delmonitorPopup = function() {
	var global;
	var global_props;
	var paramMap;	
	var monitor_popup_type;
	var selectRow = null;
	var selecturl = null;
	this.window = {};
	this.bindEvent = null;

	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		global = _this;
		global_props = _this.props;
		monitor_popup_type = _this.props.popUpType;
		

		selecturl = '/itg/itam/monitordelmanagement/searchEmsList.do';
		
		paramMap = ( typeof props.params === undefined ) ? {} : props.params;		
		
		var fieldItems = [];		
		if(monitor_popup_type == 'delmonitoringdbms'){
			fieldItems.push({colspan : 1, item : createCodeComboBoxComp({ label: 'DBMS', name: 'MONITOR_DEL_DBMS', code_grp_id: 'MONITOR_DEL_DBMS', attachChoice: true, required: true })});
		}
		fieldItems.push({colspan : 1, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.center.move.label.customer.0001'}), name : "customer_id", popUpType : "monitoringcustomer", srId : "$!{dataMap.sr_id}"})});
		fieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.common.label.hostnm'}), name : "HOST_NM" , on : {	onKeyPress : this.pressEnterKey.bind(_this)}})});
		fieldItems.push({colspan : 1, item : createTextFieldComp({label : "IP", name : "MAIN_IP" , on : {	onKeyPress : this.pressEnterKey.bind(_this)}})});
		
		if(monitor_popup_type == 'delmonitoringweburl'){
			fieldItems = [];
			fieldItems.push({colspan : 1, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.center.move.label.customer.0001'}), name : "customer_id", popUpType : "monitoringcustomer", srId : "$!{dataMap.sr_id}"})});
			fieldItems.push({colspan : 1, item : createTextFieldComp({label : '홈페이지명', name : "NAME" , on : {	onKeyPress : this.pressEnterKey.bind(_this)}})});
			fieldItems.push({colspan : 1, item : createTextFieldComp({label : 'URL', name : "URL" , on : {	onKeyPress : this.pressEnterKey.bind(_this)}})});
		}
		//폼
		var Form = createFormComp({
			id: props.id + '_form',				
			header: {
	        	title: getConstText({isArgs : true, m_key : 'res.label.itam.entity.00016'})
		    },
			fields: {
				colSize: 3,
				items: fieldItems,
			    hiddens: {
		       		MONITOR_TYPE : monitor_popup_type,			
			    }
			},			
			footer: {
	        	buttons : {
	        	 	align: "center",
					items: [
							createBtnComp({ //검색
								id:"btn1",
								label:"검색", 
								type: "form", 
								click: this.searchList.bind(_this)
								
							}),
							createBtnComp({ //초기화
								id:"btn2",
								label:"초기화", 
								click: this.resetFormData.bind(_this)
									
								
							
							})
						],
		            css: "webix_layout_form_bottom"
		        }
			}
		});

		var Emsgrid = createGridComp({
			id : props.id + '_emsgrid',
			header : {
				title : "EMS 목록"
			},
			
			keys : [ "EMS_ID" ],			
			params : {							
				MONITOR_TYPE : monitor_popup_type				
			},
			url : selecturl,
			resource : 'grid.itam.monitor.'+monitor_popup_type,				
			//autoLoad : false,
			autoLoad : false,
			on : {
				onItemDblClick: this.addUser.bind(_this)
			},
			minHeight : 200
		});
		
		var Grid = createGridComp({
			id : props.id + '_grid',
			checkbox : true,
			header : {
				title: getConstText({isArgs : true, m_key : 'res.title.common.result'}),
				buttons : {
					align : "right",
					items : [						
								createBtnComp({
									id:"btn3",
									label: getConstText({ isArgs : true, m_key : 'res.common.delete'}) /*삭제*/,
									type: "form",
									click: function() {
									
									
											$$(props.id + '_grid')._removeRow();

											// 사용자 목록 새로고침
											//$$(props.id + '_grid')._reload(null);
										}	
								}),
								createBtnComp({
									id:"btn5",
									label: getConstText({ isArgs : true, m_key : 'res.billing.cntrct.00009'}) /*삭제*/,
									type: "form",
									click: function() {
								
										
										var reference = _this.props.reference;
										var fields = reference.fields;
										
										var param = {
												custoemr_id : $$("requst_regist")._getFieldValue("customer_id"),
												sr_id: $$("requst_regist")._getFieldValue("sr_id"),
												monitor_type : fields.id
										};
										
										var excelAttrs = {
												sheet: [
													{ 
													  sheetName		: getConstText({isArgs : true, m_key : 'res.delmonitoring.label.00001'}), 
													  titleName		: getConstText({isArgs : true, m_key : 'res.delmonitoring.label.00001'}),
													  headerNames	: getConstText({isArgs : true, m_key : 'grid.itam.monitor.'+monitor_popup_type+'.text'}),
													  includeColumns: getConstText({isArgs : true, m_key : 'grid.itam.monitor.'+monitor_popup_type+'.header'}),
													}
												]
											};
											var excelParam = {};
											excelParam['fileName'] = getConstText({isArgs : true, m_key : 'res.delmonitoring.label.00001'}),
											excelParam['excelAttrs'] = excelAttrs;
											goExcelDownLoad("/itg/itam/monitordelmanagement/searchMonitorListExcelDown.do", param, excelParam);
											
									
									}
									
								})								
								
							]
				}
			},
			minHeight : 200,
			resource : 'grid.itam.monitor.'+monitor_popup_type,					
			url : "/itg/itam/monitordelmanagement/searchDelMonitorManagementList.do",			
			params : {			
				class_mng_type : "",
				sr_id: $$("requst_regist")._getFieldValue("sr_id"),
				monitor_type : monitor_popup_type				
			},
			pageable : false,			
			autoLoad : false,
			keys : [ "EMS_ID" ],			
			on:{
				onItemClick: this.gridCellClick.bind(_this)
			}
		});
			
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
			        	rows: new Array().concat(Form, Emsgrid, Grid)
			        }
			};
		 
		_this.window = createWindow({
	    	id: props.id,	  
	    	width : nkia.ui.utils.getWidePopupWidth(),
	    	height : nkia.ui.utils.getWidePopupHeight()	,
	    	header: {
	    		title: props.title
	    	},
	        body: {
	        	rows: [bodyScrollView]
	        },	    	
	        footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저장', id : 'btn4', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable
	    });
		
		var reference = this.props.reference;
		var fields = reference.fields;
		
		
		
		if(PROCESS_META.TASK_NAME != "start" && PROCESS_META.TASK_NAME != "REQUST_RCEPT"){
			nkia.ui.utils.hides(['btn1']);
			nkia.ui.utils.hides(['btn2']);
			nkia.ui.utils.hides(['btn3']);
			nkia.ui.utils.shows(['btn5']);			
			
			nkia.ui.utils.hides([props.id + '_form']);
			nkia.ui.utils.hides([props.id + '_emsgrid']);
			
						
			if($$("requst_regist")._getFieldValue(fields.id)){
				$$(props.id + '_grid')._addRow($$("requst_regist")._getFieldValue(fields.id));
			}else{
				$$(props.id + '_grid')._reload();
			}
		}else{
			
			nkia.ui.utils.hides(['btn5']);
			
			if($$("requst_regist")._getFieldValue(fields.id) ){	
				$$(props.id + '_grid')._addRow($$("requst_regist")._getFieldValue(fields.id));				
			}
			else{
					$$(props.id + '_grid')._reload();										
			}
		}		
	};	

	this.choice = function(){

		var extract_grid = $$(global.props.id + '_grid')._getRows();		
		var reference = this.props.reference;
		var fields = reference.fields;
		var values = {};
				
		values[fields.id] = extract_grid; // 그리드 row 값들.
		values[fields.name] =  extract_grid.length ;
		
		reference.form._setValues(values);
		
		this.window.close();
	};

	this.addUser = function() {
		var data = $$(global.props.id + '_emsgrid')._getSelectedRows();		
		$$(global.props.id + '_grid')._addRow(data);
		// 사용자 목록 새로고침
		$$(global.props.id + '_emsgrid')._reload(null);
	}
	
	// 그리드 더블클릭 버튼 이벤트
	this.gridCellClick = function(){	
		
		selectRow = $$(global.props.id + '_grid').getSelectedItem();

	};
	
	this.pressEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){ //enter key
				searchList();
			}	
		}
	};


	/**
	 * 검색
	 */
	this.searchList = function() {			
		// 검색폼의 값들 추출		
		if($$(global.props.id + '_form')._validate()){
			var data = $$(global.props.id + '_form')._getValues();	
		// 검색폼의 데이터를 인자값으로 그리드 리로드
			$$(global.props.id + '_emsgrid')._reload(data);
		}
	};

	/**
	 * 검색 폼 초기화
	 */
	this.resetFormData = function() {			
		$$(global.props.id + '_form')._reset();	
		$$(global.props.id + '_form')._setValues({
			MONITOR_TYPE : monitor_popup_type				
		});		
	};
};


/**
 * @method {} createMultiCodeWindow : Form Function
 * @returns {object} form
 */
function createPopupFieldWindow(popupProps) {
	var props = popupProps||{};
	var popup;
	try {
		switch(props.popUpType){
		case 'CreatePmCausePopup' : popup = new nkia.ui.popup.nbpm.pmCausePopup();
			break;
		
		case 'CreateCmRiskPopup' : popup = new nkia.ui.popup.nbpm.cmRiskPopup();
			break;	
		
		case 'CreateIncdntGradPopup' : popup = new nkia.ui.popup.nbpm.incdntGradPopup();
			break;	
			
		case 'createDmlComp' : popup = new nkia.ui.popup.nbpm.createDmlComp();
			break;		
			
		case 'customerPopup' : popup = new nkia.ui.popup.nbpm.customerPopup();
			break;				
			
		case 'dnsSelectZone' : popup = new nkia.ui.popup.nbpm.dnsSelectZone();
			break;
			
		case 'monitoringsms' : popup =   new nkia.ui.popup.nbpm.smsmonitorPopup();
			break;		
			
		case 'monitoringnms' : popup =   new nkia.ui.popup.nbpm.monitorPopup();
			break;				
			
		case 'monitoringdbms' : popup =   new nkia.ui.popup.nbpm.monitorPopup();
			break;			
			
		case 'monitoringwas' : popup =   new nkia.ui.popup.nbpm.monitorPopup();
			break;	

		case 'monitoringvmm' : popup =   new nkia.ui.popup.nbpm.monitorPopup();
			break;	

		case 'monitoringtms' : popup =   new nkia.ui.popup.nbpm.monitorPopup();
			break;	
		
		case 'monitoringweburl' : popup =   new nkia.ui.popup.nbpm.monitorPopup();
			break;			
		case 'amdbPopup' : popup = new nkia.ui.popup.nbpm.amdbPopup();
			break;			
		case 'serverCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			break;	
		case 'networkCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			break;	
		case 'securityCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			break;	
		case 'storageCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			break;	
		case 'etcCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			break;	
		case 'rackCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			break;	
		case 'facilityCarry' : popup = new nkia.ui.popup.nbpm.carry(props.popUpType);
			break;	
		case 'customerCarry' : popup = new nkia.ui.popup.nbpm.customerPopup();
			break;
		case 'snapshot' : popup = new nkia.ui.popup.nbpm.snapshot();
						  popup.setProps(props);
			break;
		case 'snapshot_disp' : popup = new nkia.ui.popup.nbpm.snapshot_disp();
		  				 popup.setProps(props);
		    break;
		case 'checklist_before' : popup = new nkia.ui.popup.nbpm.checklist_before();
						  popup.setProps(props);
			break;
		case 'checklist_after' : popup = new nkia.ui.popup.nbpm.checklist_after();
							   popup.setProps(props);
			break;		    
		case 'gethostpopup' :  popup = new nkia.ui.popup.nbpm.getHostPopup();
			break;			
		case 'locationPopup' :
			popup = new nkia.ui.popup.nbpm.locationPopup();
			break;			
		case 'delmonitoringsms' : popup =   new nkia.ui.popup.nbpm.delmonitorPopup();
			break;					
		case 'delmonitoringnms' : popup = new nkia.ui.popup.nbpm.delmonitorPopup();
			break;
		case 'delmonitoringdbms' :popup = new nkia.ui.popup.nbpm.delmonitorPopup();
			break;
		case 'delmonitoringwas' : popup = new nkia.ui.popup.nbpm.delmonitorPopup();
			break;
		case 'delmonitoringvmm' : popup = new nkia.ui.popup.nbpm.delmonitorPopup();
			break;
		case 'delmonitoringtms' : popup = new nkia.ui.popup.nbpm.delmonitorPopup();
			break;
		case 'delmonitoringweburl' : popup = new nkia.ui.popup.nbpm.delmonitorPopup();
			break;			
		case 'monitoringcustomer' : popup = new nkia.ui.popup.nbpm.monitorCustomerPopup();
			break;
			
		default : popup = new nkia.ui.popup.nbpm.MultiCode();
			break;
	}	
		popup.setRender(props);
	
	} catch(e) {
		alert(e.message);
	}
}
