function createProcessSubGrid(command, gridProp){
	var popupLongHeight = "750";
	var popupHeight = "680";
	var popupShortHeight = "510";
	var insertBtn = createBtnComp({"id": gridId + "_add_btn","label":"추가", type:"form"});;
	var deleteBtn = createBtnComp({"id": gridId + "_del_btn","label":"삭제"});;
	var gridId = gridProp.id;
	
	//그리드 생성
	var gridComp;
	var reqType = PROCESS_META.REQ_TYPE;
	var processType  =  gridProp.processType;
	var subReqType = "";

	gridProp.isProcessGrid = true;
	gridProp.reqType = reqType;
	
	var params = gridProp.params;
	params["type"] = "worker";
	params["req_type"] = gridProp.processType;
	gridProp.params = params;
	gridProp.popupHeight = popupHeight;
	
	if(gridProp.editorMode){
		gridProp.header.buttons = {
			items: [
				insertBtn, deleteBtn
			]
		}	
	}
	
	gridProp.on = {};
	
	switch(command) {
		case "nw_request":	// 연계요청
		case "requst_regist_grid":	// 연계요청
			var paramMap = {};
			var srId = gridProp.params.sr_id;
			
			

			if( insertBtn != null ){
				insertBtn.click = function(){
					var processType = gridProp["processType"];
					if(processType == null || processType == "") {
						processType = "all";
					}
					window.open("/itg/nbpm/common/goRelationRequestPop.do?up_sr_id=" + srId + "&reqType="+reqType+"&submitType=linkRel&processType=" + processType + "&reloadGridId=" + gridProp.id + "&subReqType=" + subReqType,
							"pop", "width=1300,height=800,history=no,resizable=yes,status=no,scrollbars=no,member=no");
				};
			}
			
			if( deleteBtn != null ){
				deleteBtn.click = function(){
					if(confirm("해당 요청을 종료합니다.\n계속 하시려면 확인을 눌러주세요")) {
						var sourceGrid = $$(gridId);
						var sourceSelRows = sourceGrid._getSelectedRows();
						if( sourceSelRows.length > 0 ){
							for( var i = 0; i < sourceSelRows.length; i++ ){
								var record = sourceSelRows[i];
								paramMap["sr_id"] = record.SR_ID;
								paramMap["process_type"] = record.REQ_TYPE;
								jq.ajax({
									type : "POST",
									url : getConstValue('CONTEXT') + "/itg/nbpm/common/deleteLinkRelSrData.do",
									contentType : "application/json",
									dataType : "json",
									data : getArrayToJson(paramMap),
									success : function(data) {
										if (data.success) {
											sourceGridStore.remove(record);
											alert(data.resultMsg);
										} else {
										alert(data.resultMsg);
										}
									}
								});
							}
						}
					}
					
				};
			}
			
			var attachEvent = {
				onItemDblClick: function(id){
					var item = this.getItem(id);
					showProcessDetailPop(item.SR_ID);
				}
			};
			
			nkia.ui.utils.extend(gridProp.on, attachEvent);
			
			gridComp = createGridComp(gridProp);
		break;
		
		case "email_send":	// 이메일
			var histotyBtn = createBtnComp({"id": gridId + "_his_btn","label":"이력보기","visible":true, "ui":""});
			var srId = gridProp.params.sr_id;
			
			if(gridProp.header.buttons && gridProp.header.buttons.items){
				gridProp.header.buttons.items.push(histotyBtn);
			}else{
				gridProp.header.buttons = {
					items: [
						histotyBtn
					]
				}	
			}
			
			var srId = gridProp.params.sr_id;
						
			if( insertBtn != null ){
				insertBtn.click = function(){
					
				};
			}
			
			if( deleteBtn != null ){
				deleteBtn.click = function(){
					selectedRowDel(gridId);
				};
			}
			
			histotyBtn.click = function(){
				var closeBtn = createBtnComp({"id": gridId + "_close_btn","label":"닫기","visible":true, scale: "medium"});
				var storeParam = {};
				var that = {};
				var userSelectPop = null;		
				
				var gridProp = {
					id : that['id'] + '_grid',
					title : '이메일 참조자',
					gridHeight : 385,
					remoteSort : true,
					resource_prefix : 'grid.process.emailSend.sub',
					url : getConstValue('CONTEXT') + "/itg/nbpm/provide/searchSendMailHistoryList.do",
					params : {sr_id : srId}
				};
				var userGrid = createGridComp(gridProp);
		
				var userSelectPop = Ext.create('nkia.custom.windowPop', {
					title : '이메일 참조자 내역',
					height : 450,
					width : 750,
					autoDestroy : false,
					resizable : false,
					buttonAlign : 'center',
					buttons : [ closeBtn ],
					items : [ userGrid ],
					listeners : {
						destroy : function(p) {
		
						},
						hide : function(p) {
						}
					}
				});
				
				closeBtn.on("click", function() {
					userSelectPop.close();
				});
				userSelectPop.show();
			};
			
			gridComp = createGridComp(gridProp);
		break;
		
		case "assign_ci_list" : // 담당구성장비
			
			var paramMap = {};
			gridProp["dockLoc"] = null;
			gridProp["dockedType"] = null;
			//gridProp["dockComp"] = null;
			gridProp["multiSelect"] = false;
			gridProp["selModel"] = false;
			
			function createChckInputPop(gridView, rowIndex, colIndex) {
				var record = gridView.getStore().getAt(rowIndex);
				var chckCnValue = record.raw.CHCK_CN;
				var chckCnTextArea = createTextAreaFieldComp({id:"chckCnTextArea", "name":"CHCK_CN","value":chckCnValue,"width":'100%',height:240})
				var popCloseBtn = Ext.create('Ext.Button', {
							text : '적용',
							scale : "medium",
							margin : getConstValue('WITH_BUTTON_MARGIN'),
							handler : function() {
								if(record.raw.CHCK_CN == null) {
									record.set("CHCK_CN", chckCnTextArea.getValue());
									gridView.refreshNode(rowIndex);
									Ext.getCmp('chckCnInputPop').close();
								}
							}
				});
						
				var chckCnInputPop = Ext.create('nkia.custom.windowPop', {
							id: 'chckCnInputPop',
							title : '점검내용',
							height : 250,
							width : 550,
							autoDestroy : false,
							resizable : false,
							layout : 'fit',
							// bodyPadding: '5 5 5 5',
							buttonAlign : 'center',
							buttons : [popCloseBtn],
							bodyStyle : 'background-color: white; ',
							items : [chckCnTextArea],
							listeners : {
								destroy : function(p) {
									p = null;
								},
								hide : function(p) {
									p = null;
								}
							}
						});
				chckCnInputPop.show();
			}
			
			gridProp["actionBtnEvent"] = createChckInputPop;
			gridProp["actionIcon"] = getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/menu_system.gif';
						
			gridComp = createGridComp(gridProp);
			
			for(var i = 0 ; i < gridComp.columns.length ; i++) {
				if(gridComp.columns[i].dataIndex == "ACTION") {
					gridComp.columns[i].renderer = function(value, meta, record) {
						return "<img alt=\"\" src=\"/itg/base/images/ext-js/common/icons/menu_system.gif\" class=\"x-action-col-icon x-action-col-0\">";
					}
				}
			}
			
			gridComp.on('beforeitemdblclick', function(grid, record, item, rowIndex){
				var rowData = record.raw;
	        	var tangible_asset_yn	= rowData["TANGIBLE_ASSET_YN"];
	        	var logical_yn			= rowData["LOGICAL_YN"];
	        	var class_id			= rowData["CLASS_ID"];
	        	
	        	var entity_class_id = "";
	        	if(tangible_asset_yn != "Y" && logical_yn == "Y"){
	        		entity_class_id = class_id + "-0";		
	        	}else{
	        		entity_class_id = class_id;
	        	}
				var param = "?asset_id="+rowData.ASSET_ID+"&conf_id="+rowData.CONF_ID+"&class_id="+class_id+"&view_type=pop_view&entity_class_id="+entity_class_id;
		        showAssetDetailPop(param);
			});
			
			gridComp["insertQuerykey"] = gridProp["insertQuerykey"];
			gridComp["deleteQuerykey"] = gridProp["deleteQuerykey"];		
		break;
		
		case "receipt_list" :
			var srId = gridProp.params.sr_id;
			gridProp["url"] = getConstValue('CONTEXT') + "/itg/nbpm/provide/hanwha/searchReceiptList.do";
			
			gridProp["dockComp"] = null;
			//gridProp["dockLoc"] = "bottom";
			//gridProp["dockedType"] = "toolbar";
			gridProp["title"] = gridProp["title"] + " (더블 클릭 시 팝업 표시됩니다.)" 
			var srId = gridProp.params.sr_id;
			gridComp = createGridComp(gridProp);
			
			gridComp.on('beforeitemdblclick', function(grid, record, item, rowIndex){
				var rowData = record.raw;
				preReqDetailPop(rowData);
			});
			
			if( insertBtn != null ){
				insertBtn.on("click", function(){
					preReqDetailPop();
				});
			}
			
			if( deleteBtn != null ){
				deleteBtn.on("click", function(){
					selectedRowDel(gridId);
				});
			}
			
			gridComp["selectQuerykey"] = "";
			gridComp["insertQuerykey"] = "";
			gridComp["deleteQuerykey"] = "";
		
		break;
		
		case "usr_story_regist":	// User Story
			var actionBtnEvent = function(grid, rowIndex, colIndex) {
				//window.open("/itg/base/images/test/image002.png", "pop", "width=1441,height=691,resizable=no,scrollbars=no");
				var imgTmp = new Image();
		        imgTmp.src = "/itg/base/images/test/image002.png";
		        
		        var imgWin;
		        
		        if (navigator.userAgent.indexOf('Chrome')>-1 || navigator.userAgent.indexOf('Safari')>-1) {
		        	imgWin = window.open("","gImgWin","width="+imgTmp.width+",height="+imgTmp.height+",status=no,toolbar=no,scrollbars=no,resizable=no"); 
		        } else {
		        	imgWin = window.open("","gImgWin","width=15,height=15,status=no,toolbar=no,scrollbars=no,resizable=no"); 
		        }
		        
		        imgWin.document.write("<html><title>User Story 진행상태</title>" 
			        +"<body topmargin=0 leftmargin=0 marginheight=0 marginwidth=0>" 
			        +"<img src='/itg/base/images/test/image002.png' name='winImg' width='"+imgTmp.width+"px' height='"+imgTmp.height+"px' border=0 />" 
			        +"</body></html>");
			    
			    if (!(navigator.userAgent.indexOf('Chrome')>-1 || navigator.userAgent.indexOf('Safari')>-1)) {
			    	imgWin.resizeTo(imgTmp.width+10, imgTmp.height+29);
			    }
			};
			
			gridProp["actionIcon"] = '/itg/base/images/ext-js/common/icons/bullet_search.gif';
			gridProp["actionBtnEvent"] = actionBtnEvent;
			
			gridComp = createGridComp(gridProp);
			
			if( insertBtn != null ){
				insertBtn.on("click", function(){
					var targetGrid = getGrid(gridId);
					var targetGridStore = targetGrid.getStore();
					var targetGridSelModel = targetGrid.view.selModel;
					
			   	if( targetGridSelModel.hasSelection() ){
			   		targetGridSelModel.deselectAll();
			   	}
					
			   	usr_story_regist();
				});
			}
			
			if( deleteBtn != null ){
				deleteBtn.on("click", function(){
					selectedRowDel(gridId);
				});
			}
			
			// 팝업생성
			function usr_story_regist(){
				var insertPopProp = {
					title: gridComp.title.split("[")[0],
					formTitle: gridComp.title.split("[")[0],
			       	id: gridComp.id + "_pop",
			       	targetGridId: gridComp.id,
			       	buttonAlign : 'center',
			       	bodyStyle: 'background-color: white; ',
			       	border: false,
			       	editorMode: gridProp["editorMode"]
			  	}
				//화면처리
				var insertPop = createUsrStory(insertPopProp, gridId);
				insertPop.show();
				return insertPop;
			}
			
			gridComp.on('beforeitemdblclick', function(grid, record, item, rowIndex){
				var insertPop = usr_story_regist();
				var form = insertPop.down("form");
				var recordRaw = record.raw;
				form.setDataForMap(recordRaw);
				
				if( gridProp["editorMode"] ){
					formEditable({id : form.id , editorFlag : true , excepArray : null});
				}else{
					formEditable({id : form.id , editorFlag : false , excepArray : null});
				}
			});
			
			gridComp["insertQuerykey"] = gridProp["insertQuerykey"];
			gridComp["deleteQuerykey"] = gridProp["deleteQuerykey"];
			
		break;
		
		case "operDetail_request":	// 작업내용 상세 (NKIA사내 솔루션 사업팀_유지보수요청)
			var actionBtnEvent = function(grid, rowIndex, colIndex) {
			
			var insertPop = operDetail_request();
			var form = insertPop.down("form");
			var recordRaw = grid.store.getAt(rowIndex).raw; //record.raw;
			form.setDataForMap(recordRaw);
			form.setHiddenValue('rowIndex', rowIndex);
			
			if( gridProp["editorMode"] ){
				formEditable({id : form.id , editorFlag : true , excepArray : null});
			}else{
				formEditable({id : form.id , editorFlag : false , excepArray : null});
			}
		};
			
			gridProp["actionIcon"] = '/itg/base/images/ext-js/common/icons/bullet_search.gif';
			gridProp["actionBtnEvent"] = actionBtnEvent;
			
			gridComp = createGridComp(gridProp);
			
			if( insertBtn != null ){
				insertBtn.on("click", function(){
					var targetGrid = getGrid(gridId);
					var targetGridStore = targetGrid.getStore();
					var targetGridSelModel = targetGrid.view.selModel;
					
			   	if( targetGridSelModel.hasSelection() ){
			   		targetGridSelModel.deselectAll();
			   	}
					
			   	operDetail_request();
				});
			}
			
			if( deleteBtn != null ){
				deleteBtn.on("click", function(){
					selectedRowDel(gridId);
				});
			}
			
			// 팝업생성
			function operDetail_request(){
				var insertPopProp = {
					title: gridComp.title.split("[")[0],
					formTitle: gridComp.title.split("[")[0],
			       	id: gridComp.id + "_pop",
			       	targetGridId: gridComp.id,
			       	buttonAlign : 'center',
			       	bodyStyle: 'background-color: white; ',
			       	border: false,
			       	editorMode: gridProp["editorMode"]
			  	}
				//화면처리
				var insertPop = createOperDetail(insertPopProp, gridId);
				insertPop.show();
				return insertPop;
			}
			
			gridComp.on('beforeitemdblclick', function(grid, record, item, rowIndex){
				var insertPop = operDetail_request();
				var form = insertPop.down("form");
				var recordRaw = record.raw;
				form.setDataForMap(recordRaw);
				
				if( gridProp["editorMode"] ){
					formEditable({id : form.id , editorFlag : true , excepArray : null});
				}else{
					formEditable({id : form.id , editorFlag : false , excepArray : null});
				}
			});
			
			gridComp["insertQuerykey"] = gridProp["insertQuerykey"];
			gridComp["deleteQuerykey"] = gridProp["deleteQuerykey"];
			
		break;
		
	}
	return gridComp;
}

function createUsrStory(setProp, selectRow){
	var storeParam = {};
	var that = setProp;
	var setPop = null; 
	
	if(popUp_comp[that['id']] == null) {
		var setEditorFormProp = {
			id: that['id']+"Form",
			title: that['formTitle'],
			editOnly: true,
			border: true,
			columnSize: 2,
			tableProps : [
			    {colspan:2, item : createTextFieldComp({label: '제목', name:'TITLE', maxLength: 200, notNull:true, widthFactor:0.5})},
			    {colspan:2, item : createTextAreaFieldComp({label: '내용', name:'CONTENT', labelWidth: 55, notNull:true, readOnly: false, widthFactor:0.5})},
			    {colspan:1, item : createTextFieldComp({label: 'Story Point', name:'STORY_PNT', maxLength: 20, widthFactor:0.5})},
			    {colspan:1, item : createTextFieldComp({label: 'Business Value', name:'BSINS_VAL', maxLength: 20, widthFactor:0.5})},
			    {colspan:1, item : createTextFieldComp({label: 'MM', name:'MM', maxLength: 10, widthFactor:0.5})},
			    {colspan:1, item : createTextFieldComp({label: '금액', name:'COST', maxLength: 10, widthFactor:0.5})}
         	]
		}
		
		var editorFormPanel = createEditorFormComp(setEditorFormProp);
		var formId = editorFormPanel['id'];
		
		var theForm = Ext.getCmp(formId);
		
		if( that["editorMode"] ){
			formEditable({id : formId , editorFlag : true , excepArray : null});
		}else{
			formEditable({id : formId , editorFlag : false , excepArray : null});
		}
		
		// 적용버튼 생성
		var insertBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
	       handler: function() {
				if(theForm.isValid()){
					var close = true;
					var targetGrid = getGrid(that["targetGridId"]);
					var targetGridStore = targetGrid.getStore();
					var targetGridRows = targetGrid.view.selModel.getSelection();
					
					var rowData = {};
					var inputData = editorFormPanel.getInputData();
					for( key in inputData ){
						/*if(key=='ALT_SUB_WORK_START_DT' || key=='ALT_SUB_WORK_END_DT'){
							rowData[key.toUpperCase()] = inputData[key].substr(0,10) + ' ' + inputData[key].substr(10,2) + ':' + inputData[key].substr(12,2);
						}
						else {*/
							rowData[key.toUpperCase()] = nullToSpace(inputData[key]);
						//}
						/*
						if(key=='ALT_SUB_WORK_END_DT'){
							var now_date = new Date();
							var insert_date = new Date(inputData[key].substr(0,10)+'T'+ inputData[key].substr(10,2) + ':' + inputData[key].substr(12,2)+':00');
							if(that['formTitle']=='작업 내역'){
								if(now_date<insert_date){
									alert('작업 내역의 종료시간은 현재보다 과거가 되어야 합니다.');
									close = false;
								}
							}
						}*/
					}
					if(close == true){
						if( targetGridRows.length > 0 ){
							var record = targetGridRows[0];
							var rowIndex = targetGridStore.indexOf(record);
							targetGridStore.remove(record);
							targetGridStore.insert(rowIndex, rowData);
						}else{
							targetGridStore.add(rowData);
						}
					
						setPop.close();
					}
				}else{
					alert(getConstText({ isArgs: true, m_key: 'msg.common.00012' }));
				}
	       }
		});
		
		// 닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
	    			setPop.close();
			}
		});
		
		setPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],
			width: 700,
			autoDestroy: false,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			buttons: ( that["editorMode"] ) ? [insertBtn, closeBtn] : null,
			bodyStyle: that['bodyStyle'],
			items: [editorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setPop;
		
	} else {
		setPop = popUp_comp[that['id']];
		setPop.close();
	}
	
	return setPop;
}

//작업내용 상세 (NKIA사내 솔루션 사업팀_유지보수요청)
function createOperDetail(setProp, selectRow){
	var storeParam = {};
	var that = setProp;
	var setPop = null; 
	
	if(popUp_comp[that['id']] == null) {
		var setEditorFormProp = {
			id: that['id']+"Form",
			title: that['formTitle'],
			editOnly: true,
			border: true,
			columnSize: 2,
			tableProps : [
			    {colspan:2, item : createTextAreaFieldComp({label: '조치내용', name:'OPER_DETAIL', labelWidth: 55, notNull:true, readOnly: false, widthFactor:0.5})},
			    {colspan:1, item : createCodeComboBoxComp({label : '처리단계', name:'OPER_LEVEL', code_grp_id:'OPER_LEVEL', attachAll : true})}
			],
			hiddenFields: [{name:'rowIndex'}]
		}
		
		var editorFormPanel = createEditorFormComp(setEditorFormProp);
		var formId = editorFormPanel['id'];
		
		var theForm = Ext.getCmp(formId);
		
		if( that["editorMode"] ){
			formEditable({id : formId , editorFlag : true , excepArray : null});
		}else{
			formEditable({id : formId , editorFlag : false , excepArray : null});
		}
		
		
		// 적용버튼 생성
		var insertBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
	       handler: function() {
	    	   setViewPortMaskTrue();
	    	   if (!confirm('등록 하시겠습니까?')) {
					setViewPortMaskFalse();
					return;
				}
	    	   
				if(theForm.isValid()){
					var close = true;
					var targetGrid = getGrid(that["targetGridId"]);
					var targetGridStore = targetGrid.getStore();
					var targetGridRows = targetGrid.view.selModel.getSelection();
					
					var rowData = {};
					var inputData = editorFormPanel.getInputData();
					for( key in inputData ){
						
							rowData[key.toUpperCase()] = nullToSpace(inputData[key]);
					}
					
					var operLevelNm = editorFormPanel.getFormField('OPER_LEVEL').rawValue;
					rowData['OPER_LEVEL_NM'] = operLevelNm;
					
					var actionBtnRow = theForm.getHiddenValue('rowIndex');
					
					if(close == true){
						
						if( targetGridRows.length > 0 ){
							var record = targetGridRows[0];
							var rowIndex = targetGridStore.indexOf(record);
							targetGridStore.remove(record);
							targetGridStore.insert(rowIndex, rowData);
						}else if( actionBtnRow != undefined &&  actionBtnRow >= 0){
							targetGridRows = targetGrid.store.getAt(actionBtnRow);
							var record = targetGridRows;
							var rowIndex = targetGridStore.indexOf(record);
							targetGridStore.remove(record);
							targetGridStore.insert(rowIndex, rowData);
						}else {
							targetGridStore.add(rowData);
						}
					
						setPop.close();
					}
				}else{
					alert(getConstText({ isArgs: true, m_key: 'msg.common.00012' }));
				}
				setViewPortMaskFalse();
	       }
		});
		
		// 닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
	    			setPop.close();
			}
		});
		
		setPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],
			width: 700,
			autoDestroy: false,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			buttons: ( that["editorMode"] ) ? [insertBtn, closeBtn] : null,
			bodyStyle: that['bodyStyle'],
			items: [editorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setPop;
		
	} else {
		setPop = popUp_comp[that['id']];
		setPop.close();
	}
	
	return setPop;
}

function checkLinkReqlValidate(id){
	var gridSizeZero = true;
	var panel = $$(id);
	var gridDataList = panel._getRows();
	if(gridDataList.length < 1){
		gridSizeZero = true;
	}else{
		gridSizeZero = false;
	}

	if(gridSizeZero){
	 	alert("[연계요청] 목록을 확인바랍니다.");
	 	return false;
	}
}

function checkLinkRelGrid(id) {
	var result = true;
	var panel = $$(id);
	var gridDataList = panel._getRows();
	for (var i = 0; gridDataList != null && i < gridDataList.length; i++) {
		if (!(gridDataList[i].work_state == "WORKCOMP" || gridDataList[i].work_state == "NOTI" || gridDataList[i].work_state == "END")) {
			result = false;
			alert("[연계요청] 중 종료되지 않은 요청이 있습니다.");
			break;
		}
	}
	return result;
}

function reloadGrid(gridId) {
	$$(gridId)._reload();
}

function preReqDetailPop (rowData) {
	var selectedRecord = rowData;
	var propertyPop = null;
	var actionLinkPop = function (command) {
		setViewPortMaskTrue();
		switch (command) {
			case "insert":
				if (!confirm('등록 하시겠습니까?')) {
					setViewPortMaskFalse();
					return;
				}
				var reqId = selectedRecord.REQ_ID;
				window.open("/itg/nbpm/common/goRelationRequestPop.do?" +
						"up_sr_id=" + reqId + "&submitType=linkRel&processType=CHANGE" +
						"&reloadGridId=receipt_list&ref_url=/itg/nbpm/provide/hanwha/selectReceiptDetail.do",
						"preChangeRequestPop", "width=1150,height=650,history=no,resizable=yes,status=no,scrollbars=no,member=no");
				break;
	
			case "reject":
				if (!confirm('부결 하시겠습니까?')) {
					setViewPortMaskFalse();
					return;
				}
				var paramMap = {};
				paramMap["req_id"] = selectedRecord.REQ_ID;
				paramMap["rcept_sttus_cd"] = "REJECT";
				var dataResult;
				jq.ajax({
						"type" : "POST",
						"url" : "/itg/nbpm/provide/hanwha/updatePreReqStatus.do",
						"contentType" : "application/json",
						"dataType" : "json",
						"async" : false,
						"data" : getArrayToJson(paramMap),
						"success" : function(data) {
							alert("처리 되었습니다.");
							$$("receipt_list")._reload();
							propertyPop.close();
						}
				});
				setViewPortMaskFalse();
				break;
		}
	
	}
	
	var popInsertBtn = createBtnComp({visible: true, label: '요청등록', id:'popInsertBtn', ui:'correct', scale:'medium'});
	attachBtnEvent(popInsertBtn, actionLinkPop,  'insert');
	var popRejectBtn = createBtnComp({visible: true, label: '부 결', id:'popRejectBtn', scale:'medium'});
	attachBtnEvent(popRejectBtn, actionLinkPop,  'reject');

	var editorFormProp = {
		id: 'editorForm',
		title: '요청상세',
		columnSize: 2,
		editOnly: true,
		tableProps :
		[
			 {colspan:1, item : createTextFieldComp({label:'요청자', name:'req_user_id_nm', readOnly: true})}
			,{colspan:1, item : createTextFieldComp({label:'요청일', name:'req_dt', readOnly: true})}
			,{colspan:2, item : createTextFieldComp({label:'제목', name:'title', readOnly: true })}
			,{colspan:2, item : createTextAreaFieldComp({label:'내용', name:'content', readOnly: true})}
			,{colspan:2, item : createAtchFileFieldComp({label:'첨부파일', name:'atch_file_id', id : 'atch_file_id', width: '100%', editorMode: false})}
		],
		hiddenFields: [{name:'req_user_id'}, {name:'req_id'}]
	}
	var editorForm = createInnerEditorFormComp(editorFormProp);
	
	var windowProp = {
		id: 'poppreReqDetail',	
		title : '상세 팝업',
		width : 840,
		height : 420,
		buttons : [popInsertBtn, popRejectBtn],
		items : [ editorForm ],
		layout : {
			type : 'border'
		},
		closeBtn : true
	}
	propertyPop = createWindowComp(windowProp);
	propertyPop.show();
	editorForm.setDataForMap(rowData);
	getAtchFileList("atch_file_id", rowData.ATCH_FILE_ID);
	
	if(rowData["RCEPT_STTUS_CD"] != "EXMNT") {
		hideIds = ['popInsertBtn','popRejectBtn'];
		hideBtns(hideIds);
	} else {
		showIds = ['popInsertBtn','popRejectBtn'];
		showBtns(showIds);
	}
}


// 2016-06-18 최양규추가 템플릿보기 popup
function createTemplatPopup(){
	
	var height = 523;
	var params = {"cur_sr_id" : "SR160618_11783"};
	var agt = navigator.userAgent;
	if(agt.indexOf("Chrome") != -1) {
		height = height - 7;
	}
	
	var setPopUpProp = {
		height: height,
		width: 870,
		id: 'processGridPop',
		url: getConstValue('CONTEXT') + '/itg/workflow/template/searchTemplateList.do',
		buttonAlign : 'center',
		popUpText: '기본 템플릿',
		params: params
	}
	
	var popup = createTemplateSelectPop(setPopUpProp);
	popup.show();
}

//2016-06-18 최양규추가 템플릿보기 popup
function createTemplateSelectPop(setPopUpProp){
	var storeParam = {};
	var that = {};
	var processSelectPop = null;
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {

		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : '', // Grid Title
			gridHeight : 305,
			resource_prefix : 'grid.pop.template', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			forceFit : true,
			pagingBar : true,
			pageSize : 10,
			autoLoad : false
		};

		var processGrid = createGridComp(setGridProp);
		
		/////////삭제예정 데이터//////////
		Ext.getCmp('processGridPop_grid').getStore().add(
				{ REQ_TYPE_NM: '변경요청',  TITLE:"홈페이지 로그인 이미지 변경요청", REQ_REASON:"사규에 의거하여 회사 로고가 변경되었으니 해당 내역 반영 바랍니다.", CONTENT:"그룹 메인 홈페이지 중앙 로고 변경 요청 입니다." },
				{ REQ_TYPE_NM: '릴리즈요청', TITLE:"릴리즈 테스트1", REQ_REASON:"", CONTENT:"TEST3" },
			    { REQ_TYPE_NM: '변경요청',  TITLE:"SMR 접수 변경 요청",  CONTENT:"SMR 접수 변경 요청 하겠습니다." },
			    { REQ_TYPE_NM: '변경요청',  TITLE:"UAT 수행", REQ_REASON:"", CONTENT:"요청등록" },
			    { REQ_TYPE_NM: '변경요청',  TITLE:"접수변경테스트2", REQ_REASON:"", CONTENT:"TEST2" },
			    { REQ_TYPE_NM: '변경요청',  TITLE:"접수변경테스트2", REQ_REASON:"", CONTENT:"TEST2" },
			    { REQ_TYPE_NM: '서비스요청',  TITLE:"홈페이지 서비스 이슈사항", REQ_REASON:"", CONTENT:"홈페이지에 이슈사항 입니다." },
			    { REQ_TYPE_NM: '서비스요청',  TITLE:"", REQ_REASON:"", CONTENT:"TEST2" },
			    { REQ_TYPE_NM: '변경요청', TITLE:"접수변경 테스트3", REQ_REASON:"", CONTENT:"TEST4" });
		/////////삭제예정 데이터//////////
		
		var searchBtn = new Ext.button.Button({
			id : that['id'] + '_btn',
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			handler : function(thisBtn, event) {
				var param = Ext.getCmp('searchForm').getInputData();
				//Ext.getCmp(that['id'] + '_grid').searchList(param);
			}
		});
	
 		var setSearchFormProp = {
			id : 'searchForm',
			formBtns : [ searchBtn ],
			columnSize : 2,
			//@@정중훈20140217 엔터펑션 추가 시작
			enterFunction : function(){
				var param = Ext.getCmp('searchForm').getInputData();
				//Ext.getCmp(that['id'] + '_grid').searchList(param);
			},
			//@@정중훈20140217 엔터펑션 추가 종료
			tableProps : [
			// row1
			{
				colspan : 2,
				tdHeight : 28,
				item : createCodeComboBoxComp({
					label : '템플릿 유형',
					name : 'req_type',
					code_grp_id:'REQ_TYPE',
					defaultValue: (that['params']['req_type'] != null) && (that['params']['req_type'] != '') ? that['params']['req_type'] : '',
					readOnly: (that['params']['req_type'] != null) && (that['params']['req_type'] != '') ? true : false,
					attachAll:true
				})
			}, {
				colspan : 2,
				tdHeight : 28,
				item : createTextFieldComp({
					label : '템플릿 제목',
					name : 'title',
					width : 660
				})
			} ]
		}

		var searchFormPanel = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ searchFormPanel, processGrid ]
		})

		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct'
		});
		if( that["acceptBtnVisible"] == false ){
			acceptBtn = null;
		}
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				processSelectPop.close();
			}
		});
		
		processSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height']+28,
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : (acceptBtn != null) ? [acceptBtn, closeBtn] : [closeBtn],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = processSelectPop;
		
		if( acceptBtn != null ){
			function gridSelectEvent(grid, td, cellIndex, record) {
				var selectedRecords = processGrid.view.selModel.getSelection();
				if (selectedRecords.length < 1) {
					showMessage("템플릿 유형을 선택하지 않았습니다.");
				} else {
					//that['tagetUpSrIdField'].setValue(selectedRecords[0].raw.TITLE);
					var formdata = {
							'title' : selectedRecords[0].raw.TITLE ,
							'req_reason' : selectedRecords[0].raw.REQ_REASON,
							'content' : selectedRecords[0].raw.CONTENT
					}
					Ext.getCmp('requst_regist').setDataForMap(formdata);
					processSelectPop.close();
				}	
			}			
		}

		attachCustomEvent('celldblclick', processGrid, gridSelectEvent);
		if( acceptBtn != null ){
			attachBtnEvent(acceptBtn, gridSelectEvent)	
		}
		
	} else {
		processSelectPop = popUp_comp[that['id']];
	}
	
	return processSelectPop;
}