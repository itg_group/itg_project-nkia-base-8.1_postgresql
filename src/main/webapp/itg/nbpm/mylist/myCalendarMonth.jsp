                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         <%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<html>
<head>
<title>calendar</title>      
<%!
public String dateFormat(Calendar cal, String format){ // 날짜 폼멧데이타
	SimpleDateFormat sdf = new SimpleDateFormat(format);
	Calendar c1 = Calendar.getInstance();
	return sdf.format(cal.getTime());
}
%>
<%
Calendar calC = Calendar.getInstance(); // 현재일
Calendar calS = (Calendar)calC.clone(); // 시작일
Calendar calE = (Calendar)calC.clone(); // 종료일

String strYearS  = request.getParameter("yearS");
String strMonthS = request.getParameter("monthS");
String strDateS = request.getParameter("dateS");
String strWeekS = request.getParameter("weekS");
String centerType = request.getParameter("center_type");

int startDay = calS.getMinimum(Calendar.DATE); // 월 시작일
int endDay = calS.getActualMaximum(Calendar.DAY_OF_MONTH); // 월 마지막일
int start = calS.get(Calendar.DAY_OF_WEEK); // 월첫주 요일(1~7, 1:일요일)

String uiType = "CURRENT"; // YEAR / MONTH / WEEK / DATE / CURRENT(MONOTH)
if(strYearS != null && strWeekS != null) uiType = "WEEK";
else if(strYearS != null && strMonthS != null && strDateS != null) uiType = "DATE";
else if(strYearS != null && strMonthS != null) uiType = "MONTH";
else if(strYearS != null) uiType = "YEAR";
else uiType = "CURRENT";

if("YEAR".equals(uiType)){
	calS.set(Integer.parseInt(strYearS), 0, 1);
	calE = (Calendar)calS.clone(); 
	
	startDay = calS.getMinimum(Calendar.DATE); // 월 시작일
	endDay = calS.getActualMaximum(Calendar.DAY_OF_MONTH); // 월 마지막일
	start = calS.get(Calendar.DAY_OF_WEEK); // 월첫주 요일(1~7, 1:일요일)

}else if("MONTH".equals(uiType)){
	calS.set(Integer.parseInt(strYearS), Integer.parseInt(strMonthS)-1, 1);
	calE = (Calendar)calS.clone(); // 년월
	startDay = calS.getMinimum(Calendar.DATE); // 월 시작일
	endDay = calS.getActualMaximum(Calendar.DAY_OF_MONTH); // 월 마지막일
	start = calS.get(Calendar.DAY_OF_WEEK); // 월첫주 요일(1~7, 1:일요일)

}else if("CURRENT".equals(uiType)){
	calS.set(calC.get(Calendar.YEAR), calC.get(Calendar.MONTH), 1);
	calE = (Calendar)calS.clone(); // 년월
	startDay = calS.getMinimum(Calendar.DATE); // 월 시작일
	endDay = calS.getActualMaximum(Calendar.DAY_OF_MONTH); // 월 마지막일
	start = calS.get(Calendar.DAY_OF_WEEK); // 월첫주 요일(1~7, 1:일요일)
	
}else if("WEEK".equals(uiType)){
	calS = Calendar.getInstance();
	calS.setMinimalDaysInFirstWeek(4);
	calS.setFirstDayOfWeek(Calendar.SATURDAY);
	calS.set(Calendar.YEAR, Integer.parseInt(strYearS));
	calS.set(Calendar.WEEK_OF_YEAR, Integer.parseInt(strWeekS));
	
	int dayOfWeek = calS.get(Calendar.DAY_OF_WEEK); // 월 시작일
	calS.add(Calendar.DATE, (dayOfWeek-1)*-1);
	calS.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
	
	calE = (Calendar)calS.clone();
	dayOfWeek = calS.get(Calendar.DAY_OF_WEEK);
	calE.add(Calendar.DATE, 8-dayOfWeek);
	
}else if("DATE".equals(uiType)){
	;
}

String startYYYY = dateFormat(calS, "yyyy");
String startMM = dateFormat(calS, "MM");

%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
<meta http-equiv='Pragma' content='No-Cache' charset="utf-8">
<link rel="stylesheet" type="text/css" href="/itg/nbpm/mylist/css/myCalendar.css"><!-- calendar -->
<link rel="stylesheet" type="text/css" href="/itg/nbpm/mylist/css/jquery.monthpicker.css"><!-- datePicker month -->
<link rel="stylesheet" type="text/css" href="/itg/nbpm/mylist/css/font-awesome.min.css"><!-- datePicker month -->
<link rel="stylesheet" type="text/css" href="/itg/nbpm/mylist/css/notosanskr.css"><!-- notosanskr -->
<script type="text/javascript" src="/itg/base/component/jquery/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="/itg/nbpm/mylist/js/jquery.yearpicker.js"></script><!-- datePicker year -->
<script type="text/javascript" src="/itg/nbpm/mylist/js/jquery.monthpicker.js"></script><!-- datePicker month -->
<script type="text/javascript" src="/itg/nbpm/mylist/js/jquery.weekpicker.js"></script><!-- datePicker week -->
</head>
<body id="body" >
    <!-- 달력구분:<%=uiType%>, 년도:<%=strYearS%>, 월:<%=strMonthS%>, 주:<%=strWeekS%>, 일:,<%=strDateS%> -->
	<!-- <div class="button01" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calC.get(Calendar.YEAR)%>'" style='background:<%=("YEAR".equals(uiType)?"#555555;":"#f5f5f5;")%>;color:<%=("YEAR".equals(uiType)?"#ffffff;":"#111111;")%>;cursor:pointer;' >년</div> -->
	<div class="button01" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do'" style='background:<%=("MONTH".equals(uiType) || "CURRENT".equals(uiType)?"#555555;":"#f5f5f5;")%>;color:<%=("MONTH".equals(uiType) || "CURRENT".equals(uiType)?"#ffffff;":"#111111;")%>;cursor:pointer;' >월</div>
	<!-- <div class="button01" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calC.get(Calendar.YEAR)%>&weekS=<%=calC.get(Calendar.WEEK_OF_YEAR)%>'" style='background:<%=("WEEK".equals(uiType)?"#555555;":"#f5f5f5;")%>;color:<%=("WEEK".equals(uiType)?"#ffffff;":"#111111;")%>;cursor:pointer;' >주</div> -->
	<div class="button01" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do'" style='background:#f5f5f5;cursor:pointer;' >오늘:<%=dateFormat(calC, "yyyy-MM-dd")%></div>
	<div class="calendar" style="height:550px;">
<% if("YEAR".equals(uiType)){ %>
	<!-- 	<div class="head" style="display:inline;" >
			<%
			Calendar calPrev = (Calendar)calS.clone();
			Calendar calNext = (Calendar)calS.clone();
			calPrev.add(Calendar.YEAR, -1);
			calNext.add(Calendar.YEAR, +1);
			%>
			<div id="calPrev" class="title arrowL" style="cursor:pointer;" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calPrev.get(Calendar.YEAR)%>'" ><i class=" fa fa-angle-left"></i></div>
			<div id="calYearSelect" class="title" style="cursor:pointer;" ><%=startYYYY%></div>
			<div id="calNext" class="title arrowR" style="cursor:pointer;" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calNext.get(Calendar.YEAR)%>'"><i class=" fa fa-angle-right"></i></div>
		</div>
		<table class="body">
			<tr>
		<%
		Calendar calendar = (Calendar)calS.clone();
		for(int index = 0; index < 12; index++){
			String color = "black"; // black
			String tdClass = "weekDiv";
			if(calendar.get(Calendar.YEAR) == calC.get(calC.YEAR) && calendar.get(Calendar.MONTH) ==  calC.get(Calendar.MONTH)){
				tdClass = "class='active'";
			}
			out.print("\n  <td "+tdClass+" >");
			out.print("<div class='weekDay'><font color="+color+">"+(index+1)+"월</font></div>"); // 일 head
			out.print("<div id='month"+dateFormat(calendar, "yyyyMM")+"' class='task scroll_box03' ></div>"); // 일 body
			out.print("</td>"); // 일 body
			
			calendar.add(Calendar.MONTH, 1);
		}
		calendar.add(Calendar.DATE, -1);
		calE = (Calendar)calendar.clone();
		%>
			</tr>
		</table>  -->
<% }else if("MONTH".equals(uiType) || "CURRENT".equals(uiType)){ %>
		<div class="head" style="display:inline;" >
			<%
			Calendar calPrev = (Calendar)calS.clone();
			Calendar calNext = (Calendar)calS.clone();
			calPrev.add(Calendar.MONTH, -1);
			calNext.add(Calendar.MONTH, +1);
			%>
			<div id="calPrev" class="title" style="cursor:pointer;" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calPrev.get(Calendar.YEAR)%>&monthS=<%=calPrev.get(Calendar.MONTH) + 1%>&center_type=<%=centerType%>'" ><i class=" fa fa-angle-left"></i></div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div id="calMonthSelect" class="title" style="cursor:pointer;" ><%=startYYYY+"년 "+Integer.parseInt(startMM)%>월</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div id="calNext" class="title" style="cursor:pointer;" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calNext.get(Calendar.YEAR)%>&monthS=<%=calNext.get(Calendar.MONTH) + 1%>&center_type=<%=centerType%>'"><i class=" fa fa-angle-right"></i></div>
		</div>
		<p>
		<!-- <div id="type3" style="float:left;">센터구분 : <select id="selcenter" name="selcenter" onchange="changeCenterSelect()"></select></div> -->		
		<!-- <div id="type2" style="float:right;text-align:center; background:#ffa463;cursor:pointer;color:#000000;width: 120px;">장비반출입</div>-->
		<div id="type1" style="float:right;text-align:center; background:#57c9af;cursor:pointer;color:#ffffff;width: 120px;" >Major변경</div> 
		<table class="monthDay body">
			<tr><th><font color="#ef355e">일</th><th>월</th><th>화</th><th>수</th><th>목</th><th>금</th><th><font color="#6464ef">토</font></th></tr>
			<tr>
		<%
		int newLine = 0;
		calS.add(Calendar.DATE, 1-start);
		Calendar calendar = (Calendar)calS.clone();
		for(int index = 1; index < start ; index++ ){
			//out.print("<td class='none'>&nbsp;"+calendar.get(Calendar.DATE)+"</td>");
			out.print("\n  <td class='none' >");
			out.print("<div>"+calendar.get(Calendar.DATE)+"</div>"); // 일 head
			out.print("<div id='day"+dateFormat(calendar, "yyyyMMdd")+"' class='task scroll_box01' ></div>");
			out.print("</td>");
			newLine++;
			calendar.add(Calendar.DATE, 1);
		}
		for(int index = 1; index <= endDay; index++){
			String color = "black"; // black
			if(newLine == 0){
				color = "#ef355e"; // red
			}else if(newLine == 6){
				color = "#6464ef"; // blue
			}
			String tdClass = "";
			
			if(calendar.get(Calendar.YEAR) == calC.get(calC.YEAR) && calendar.get(Calendar.MONTH) ==  calC.get(Calendar.MONTH) && calendar.get(Calendar.DATE) == calC.get(Calendar.DATE)){
				tdClass = "class='active'";
			}
			out.print("\n  <td "+tdClass+" >");
			out.print("<div><font color='"+color+"'>"+calendar.get(Calendar.DATE)+"</font></div>"); // 일 head
			out.print("<div id='day"+dateFormat(calendar, "yyyyMMdd")+"' class='task scroll_box01' ></div>"); // 일 body
			out.print("</td>"); // 일 body
			newLine++;
			if(newLine == 7){
				out.print("\n</tr>");
				if(index <= endDay){
					out.print("\n<tr>");
				}
				newLine=0;
			}
			calendar.add(Calendar.DATE, 1);
		}
		
		int iiCnt = 1;             
		while(newLine > 0 && newLine < 7){
			out.print("\n  <td class='none' >");
			out.print("<div>"+calendar.get(Calendar.DATE)+"</div>"); // 일 head
			out.print("<div id='day"+dateFormat(calendar, "yyyyMMdd")+"' class='task scroll_box01' ></div>");
			out.print("</td>");
			
			newLine++;
			calendar.add(Calendar.DATE, 1);
		}
		calendar.add(Calendar.DATE, -1);
		calE = (Calendar)calendar.clone();
		%>
			</tr>
		</table>
<% }else if("WEEK".equals(uiType)){ %>
	<!-- 	<div class="head" style="display:inline;" >
			<%
			Calendar calPrev = (Calendar)calS.clone();
			Calendar calNext = (Calendar)calS.clone();
			calPrev.add(Calendar.DATE, -7);
			calNext.add(Calendar.DATE, +7);
			%>
			<div id="calPrev" class="title" style="cursor:pointer;" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calPrev.get(Calendar.YEAR)%>&weekS=<%=calPrev.get(Calendar.WEEK_OF_YEAR)%>&center_type=<%=centerType%>'" ><i class=" fa fa-angle-left"></i></div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div id="calWeekSelect" class="title" style="cursor:pointer;" ><%=startYYYY+"/"+calS.get(Calendar.WEEK_OF_YEAR)%>주</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div id="calNext" class="title" style="cursor:pointer;" onClick="location.href='/itg/nbpm/goMyCalendarMonth.do?yearS=<%=calNext.get(Calendar.YEAR)%>&weekS=<%=calNext.get(Calendar.WEEK_OF_YEAR)%>&center_type=<%=centerType%>'"><i class="	fa fa-angle-right"></i></div>
		</div>
		<table class="body weekCon">
			<tr><th>월</th><th>화</th><th>수</th><th>목</th><th>금</th><th><font color="#6464ef">토</font></th><th><font color="#ef355e">일</th></tr>
			<tr class="weekDiv">
		<%
		Calendar calendar = (Calendar)calS.clone();
		for(int index = 0; index < 7; index++){
			String color = "black"; // black
			if(index == 6){
				color = "#ef355e"; // red
			}else if(index == 5){
				color = "#6464ef"; // blue
			}
			String tdClass = "weekDay";

			if(calendar.get(Calendar.YEAR) == calC.get(calC.YEAR) && calendar.get(Calendar.MONTH) ==  calC.get(Calendar.MONTH) && calendar.get(Calendar.DATE) == calC.get(Calendar.DATE)){
				tdClass = "class='active'";
			}
			out.print("\n  <td "+tdClass+" >");
			out.print("<div style='text-align:center;'><font color='"+color+"'>"+dateFormat(calendar, "MM-dd")+"</font></div>"); // 일 head
			out.print("<div id='day"+dateFormat(calendar, "yyyyMMdd")+"' class='task scroll_box02' ></div>"); // 일 body
			out.print("</td>"); // 일 body
			
			calendar.add(Calendar.DATE, 1);
		}
		calendar.add(Calendar.DATE, -1);
		%>
			</tr>
		</table> -->
<% }else { %>
		<font color='red'>잘못된 UI유형입니다.</font>
<% } %>
	</div>
</body>

<script>
var v_uiType = "<%=uiType%>";
var v_centerType = "<%=centerType%>";
$(document).ready(function(){
	selcenter();
});
function changeCenterSelect(){
	//header.parentNode.removeChild(header);
	var sel = document.getElementById("selcenter");
	//var val = sel.options[sel.selectedIndex].value;
	v_centerType = sel.options[sel.selectedIndex].value;
	//selcenter();
	location.href="/itg/nbpm/goMyCalendarMonth.do?center_type="+v_centerType;
	//fn_searchCalendarData("<%=dateFormat(calS, "yyyyMMdd")%>", "<%=dateFormat(calE, "yyyyMMdd")%>");
}
function selcenter(){
// 	var params = {};
// 	params.code_grp_id = "CENTER";
// 	$.ajax({
// 		type:"POST",
// 		contentType: "application/json",
// 		async:true,
// 		dataType: "json",
// 		url:"/itg/base/searchCodeDataList.do",
// 		data: JSON.stringify(params),
// 		success: function(data){
// 			if(data != null)    {
// 				if(data.success == true){
// 					var objSel = document.getElementById("selcenter");
// 				    var objOption = document.createElement("option");       
// 				    objOption.text = "전체";
// 				    objOption.value = "ALL";
// 				    objSel.options.add(objOption);
// 					for(var i=0; i<data.gridVO.rows.length;i++){
// 						var objOption = document.createElement("option");  
// 						objOption.text = data.gridVO.rows[i].CODE_TEXT;
// 					    objOption.value = data.gridVO.rows[i].CODE_ID;
// 					    objSel.options.add(objOption);
// 					}
// 					if(v_centerType != "null"){
// 						var len = objSel.options.length;
//  						for (var z=0; z<len; z++){
//  							if(objSel.options[z].value == v_centerType){
//  								objSel.options[z].selected = true;
//  								break;
//  							}
//  						}
// 					}
<%-- 					fn_searchCalendarData("<%=dateFormat(calS, "yyyyMMdd")%>", "<%=dateFormat(calE, "yyyyMMdd")%>"); --%>
// 				}else{
// 					alert('send response data fail'); 
// 				}
// 			}
// 		},
// 		failure: function (data) { 
// 			alert('send fail'); 
// 		}
// 	});
	fn_searchCalendarData("<%=dateFormat(calS, "yyyyMMdd")%>", "<%=dateFormat(calE, "yyyyMMdd")%>");
}
// 데이타 조회 
function fn_searchCalendarData(startDate, endDate){
	
	
		var params = {};
		params.calendar_startDate = startDate;
		params.calendar_endDate = endDate;
		//var sel = document.getElementById("selcenter");
		//var val = sel.options[sel.selectedIndex].value;
		if(v_centerType != "null"){
			params.center_type = v_centerType;
		}
	$.ajax({
		type:"POST",
		contentType: "application/json",
		async:true,
		dataType: "json",
		url:"/itg/nbpm/searchMyCalendarList.do",
		data: JSON.stringify(params),
		success: function(data){
			if(data != null)    {
				if(data.success == true){
					fn_makeTask(data.resultMap.outList1);
				}else{
					alert('send response data fail'); 
				}
			}
		},
		failure: function (data) { 
			alert('send fail'); 
		}
	});
}

// 데이타 설정 
function fn_makeTask(list){
	var planData = "";
    for (var r=0; r<list.length; r++) {
    	var row = list[r]; 
		var strYYYY = row.CALENDAR_DT.substring(0, 4);
		var strYYYYMM = row.CALENDAR_DT.substring(0, 6);
		var strYYYYMMDD = row.CALENDAR_DT.substring(0, 8);
		if(document.getElementById("day"+strYYYYMMDD)){ // MONTH / CURRENT/ WEEK 방식
			var sYYYY = row.CALENDAR_DT.substring(0, 4); 
			var sMM = row.CALENDAR_DT.substring(4, 6);
			var sDD = row.CALENDAR_DT.substring(6, 8);
			//planData = (null != row.WORK_STATE_NM && 0 < row.WORK_STATE_NM.length? "["+row.WORK_STATE_NM+"] ":"") + row.TITLE;
			planData = row.TITLE;
			var dataInfo = "";
			var background = "#57c9af"; // 파랑
			var color = "";
			if("SERVICE" == row.REQ_TYPE){ // 임시저장
				background = "#ffa463"; // 노랑
				color = "#000000";
			}else if("CHANGE" == row.REQ_TYPE){ // 종료
				background ="#57c9af"; //
				color = "#ffffff";
			}
			dataInfo += "<div style='background:"+background+";cursor:pointer;color:"+color+";' title='"+planData+"' ";
			dataInfo += " onClick=\"fn_opnePlan('"+sYYYY+"', '"+sMM+"', '"+sDD+"', '"+row.SR_ID+"', '"+row.TASK_ID+"', '"+row.TASK_NAME+"', '"+row.GRP_SLCT_YN+"', '"+row.ACTUALOWNER_ID+"'); return false; \" >";
			dataInfo += planData+"</div>";
			document.getElementById("day"+strYYYYMMDD).innerHTML += dataInfo;
			
		}else if(document.getElementById("month"+strYYYYMM)){ // YEAR 방식 
			var sYYYY = row.CALENDAR_DT.substring(0, 4); 
			var sMM = row.CALENDAR_DT.substring(4, 6);
			var sDD = row.CALENDAR_DT.substring(6, 8);
			//planData = sDD+":"+(null != row.WORK_STATE_NM && 0 < row.WORK_STATE_NM.length? "["+row.WORK_STATE_NM+"] ":"") + row.TITLE;
			planData = sDD+":"+ row.TITLE;
			var dataInfo = "";
			var background = "#57c9af"; // 파랑
			var color = "";
			if("SERVICE" == row.REQ_TYPE){ // 임시저장
				background = "#ffa463"; // 노랑
				color = "#000000";
			}else if("CHANGE" == row.REQ_TYPE){ // 종료
				background ="#57c9af"; // 
				color = "#ffffff";
			}
			dataInfo += "<div style='background:"+background+";cursor:pointer;color:"+color+";' title='"+planData+"' ";
			dataInfo += " onClick=\"fn_opnePlan('"+sYYYY+"', '"+sMM+"', '"+sDD+"', '"+row.SR_ID+"', '"+row.TASK_ID+"', '"+row.TASK_NAME+"', '"+row.GRP_SLCT_YN+"', '"+row.ACTUALOWNER_ID+"'); return false; \" >";
			dataInfo += planData+"</div>";
			document.getElementById("month"+strYYYYMM).innerHTML += dataInfo;
		}
    }
}

// 계획 오픈 
function fn_opnePlan(pYYYY, pMM, pDD, pSR_ID, pTASK_ID, pTASK_NAME, pGRP_SLCT_YN, pACTUALOWNER_ID){
	//alert(">>> fn_opnePlan("+pYYYY+", "+pMM+", "+pDD+", "+pSR_ID+", "+pTASK_ID+", "+pTASK_NAME+", "+pGRP_SLCT_YN+", "+pACTUALOWNER_ID+")"); 
	var clickRecord = {};
	clickRecord.SR_ID = pSR_ID;
	clickRecord.TASK_ID = pTASK_ID;
	clickRecord.TASK_NAME = pTASK_NAME;
	clickRecord.GRP_SLCT_YN = pGRP_SLCT_YN;
	clickRecord.ACTUALOWNER_ID = pACTUALOWNER_ID;
	var closable = true;
	parent.addTab('/itg/nbpm/goTaskPage.do', clickRecord, closable);
	//parent.addTab('/itg/nbpm/provide/goDetailPage.do', clickRecord, closable);
	
}

//datePicker year 선택
$(function() {
	var yearList = [];
	for(var y=<%=calC.get(Calendar.YEAR)+1%>; y>1999; y--){
		yearList.push(y);
	}
	$('#calYearSelect').yearpicker({
		yyyy:<%=startYYYY%>,
		years:yearList,
		topOffset:6,
		onOKSelect: function(y, m) {
			//console.log(', year: ' + y + ', Month: ' + m);
			location.href = "/itg/nbpm/goMyCalendarMonth.do?yearS="+y;
		}
	});
});

//datePicker month 선택
$(function() {
	var yearList = [];
	for(var y=<%=calC.get(Calendar.YEAR)+1%>; y>1999; y--){
		yearList.push(y);
	}
	$('#calMonthSelect').monthpicker({
		yyyy:<%=startYYYY%>,
		mm:<%=startMM%>,
		years:yearList,
		topOffset:6,
		onMonthSelect: function(y, m) {
			//console.log(', year: ' + y + ', Month: ' + m);
			location.href = "/itg/nbpm/goMyCalendarMonth.do?yearS="+y+"&monthS="+m;
		}
	});
});

//datePicker week 선택
$(function() {
	var yearList = [];
	for(var y=<%=calC.get(Calendar.YEAR)+1%>; y>1999; y--){
		yearList.push(y);
	}
	$('#calWeekSelect').weekpicker({
		yyyy:<%=startYYYY%>,
		week:<%=calS.get(Calendar.WEEK_OF_YEAR)%>,
		years:yearList,
		topOffset:6,
		onWeekSelect: function(y, w) {
			//console.log(', year: ' + y + ', Week: ' + w);
			location.href = "/itg/nbpm/goMyCalendarMonth.do?yearS="+y+"&weekS="+w;
		}
	});
});

</script>
</html>
