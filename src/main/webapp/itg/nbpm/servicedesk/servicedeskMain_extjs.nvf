<!doctype html>
<html lang="ko">
<!--
*//**
 * 시스템권한관리 페이지
 * @author <a href="mailto:jyt@nkia.co.kr"> YoungTai Jung
 * 
 *//*
-->

<head>
#parse("/itg/base/include/page_meta.nvf")
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>
<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<script>
var centerTabPanel = null;
var index = 0;
var DO_NOT_WORK_STATE = 0; 	//작업 할 수 없는 상태
var REGISTER = 1;				//상담원 등록
var ALREADY_REGISTER = 2;		//상담원 정보 이미 등록

Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);

	//검색버튼을 생성
	var searchBtn = createBtnComp({visible: true, label: "#springMessage('btn.common.search')", id:'searchBtn', "ui":"correct"});
	
	//초기화버튼을 생성
	var initBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.reset")', id:'initBtn'});
	
	
	//actionList 측에 분기문을 추가함
	attachBtnEvent(searchBtn, actionLink,  'search');
	//초기화 버튼 이벤트
	attachBtnEvent(initBtn, actionLink,  'searchFormInit');
	
	var setSearchFormProp = {
			id: 'search_form',
			formBtns: [searchBtn, initBtn],
			columnSize: 3,
			tableProps : [
						 {colspan:1, item : createCodeComboBoxComp({label:"#springMessage('res.label.nbpm.00003')", name:'req_type', code_grp_id:'REQ_TYPE', attachAll: true})}
						,{colspan:1, item : createTextFieldComp({label:"#springMessage('res.label.nbpm.00001')", name:'req_user_nm'})}
						,{colspan:1, item : createFromToDateContainer({label:"#springMessage('res.label.nbpm.00002')", name:'req_dt', dateType:'D'})}
			            ,{colspan:1, item : createTextFieldComp({label:"#springMessage('res.label.nbpm.00004')", name:'sr_id'})}
						,{colspan:1, item : createTextFieldComp({label:"#springMessage('res.label.nbpm.00005')", name:'title'})}
						,{colspan:1,
							item : createTreeSelectPopComp({popId:'serviceSelectPop', 
														popUpTitle: "서비스조회", 
														targetName:'service_nm', 
														targetHiddenName:'service_id', 
														targetForm:'search_form', 
														treeId:'serviceSelectTree', 
														url: '/itg/nbpm/common/searchServiceTree.do',
														targetLabel:"서비스조회", 
														btnId:'service_idBtn', params: null, 
														rnode_text: "서비스조회",
														expandLevel : 2,
														leafSelect: false, rootSelect : true})}
			             ]
	}
	
	var searchFormPanel = createSeachFormComp(setSearchFormProp);
	
	var sdListType = '$!{sd_list_type}';
	var gridProps = {	
			context: '${context}',												// Context
			id: 'serviceDeskList',												// Grid id
			title: "#springMessage('res.label.nbpm.sdesk.00005')", 
			width: '100%',
			resource_prefix: 'grid.servicelist.' + sdListType.toLowerCase(),	// Resource Prefix
			url: '$!{context}/itg/nbpm/servicedesk/list/searchServicedeskList.do',				// Data Url
			params: {sd_list_type: sdListType},
			pagingBar : true,
			border : true,
			pageSize : 10
		};

	var grid = null;
	if("READY" == sdListType){
		setAssignAndReleaseBtnConfig(gridProps);
		grid = createGridComp(gridProps);
		setGridActionColumn(grid);	
	} else {
		grid = createGridComp(gridProps);
	}
		
	// Viewport 설정
	var viewportProperty = {
		center: { 
			items : [searchFormPanel, grid] 
		}
	}
	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	attachCustomEvent('celldblclick', grid, gridCellClick);
});

// Layout Resise Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["search_form"], "serviceDeskList");
}

function gridCellClick( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {
	var clickRecord = record.raw;
	var closable = true;
	var paramMap = {};
	paramMap["sr_id"] = record.raw.SR_ID;
	paramMap["task_id"] = record.raw.TASK_ID;
	paramMap["cnslr_user_id"] = parent.getCnslrUserInfo();
	
	parent.addTab('${context}/itg/nbpm/goTaskPage.do', clickRecord, closable);
}

function actionLink(command) {
	var theForm = Ext.getCmp('editor_form');
	
	switch(command) {
		case 'search' :
			// 마스크를 띄웁니다.
			setViewPortMaskTrue();
			
			var searchForm = Ext.getCmp('search_form');
			var paramMap = searchForm.getInputData();
			paramMap["sd_list_type"] = '$!{sd_list_type}';
			Ext.getCmp('serviceDeskList').searchList(paramMap);
			// 마스크를 숨깁니다.
			setViewPortMaskFalse();
			break;
		case 'searchFormInit' :
			Ext.getCmp('search_form').initData();
		break;	
	}
}

// 검색폼에 엔터이벤트 추가
function searchBtnEvent(){
	actionLink('search');
}	

function reloadServiceDeskGrid() {
	Ext.getCmp('serviceDeskList').getStore().reload();
}

function getParentTabId(){
	return parent.Ext.getCmp("serviceDeskTab").getActiveTab().id;
}

function setAssignAndReleaseBtnConfig(props) {
	props["actionBtnEvent"] = function(grid, rowIdx, colIdx) {
		var store = grid.getStore();
		var record = store.getAt(rowIdx);
		var url;
		var questionText;
		var resultText;
		if(record.raw["RECEIPT_USER_ID"] == '$!{login_user_id}') {
			url = getConstValue('CONTEXT') + "/itg/nbpm/process/releaseGroupAssignTask.do";
			questionText = "[접수취소]하시겠습니까?";
			resultText = "[접수취소]되었습니다.";
		} else if(record.raw["RECEIPT_USER_ID"] != '$!{login_user_id}'){
			url = getConstValue('CONTEXT') + "/itg/nbpm/process/acceptGroupAssignTask.do";
			questionText = "[접수]하시겠습니까?";
			resultText = "[접수]되었습니다.";
		}
		if(confirm(questionText)) {
			jq.ajax({
				"type" : "POST",
				"url" : url,
				"contentType" : "application/json",
				"dataType" : "json",
				"async" : false,
				"data" : getArrayToJson({task_id : String(record.raw.TASK_ID)}),
				"success" : function(data) {
					if (data.success) {
						alert(resultText);
						store.load();
					} else {
						alert(data.resultMsg);
					}
				}			
			});
			
		}
	};
	props["actionIcon"] = getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/menu_system.gif';
}

function setGridActionColumn(grid) {
	for(var i = 0 ; i < grid.columns.length ; i++) {
		if(grid.columns[i].dataIndex == "ACTION") {
			grid.columns[i].renderer = function(value, meta, record) {
				if(record.raw["RECEIPT_USER_ID"] == '$!{login_user_id}') {
					return "<img alt='' src='/itg/base/images/ext-js/simple/btn-icons/reformat.gif' class='x-action-col-icon x-action-col-0'>";
				} else if(record.raw["RECEIPT_USER_ID"] != '$!{login_user_id}'){
					return "<img alt='' src='/itg/base/images/ext-js/common/icons/menu_process.gif' class='x-action-col-icon x-action-col-0'>";
				} else {
					return "";
				}
			}
		}
	}	
}
</script>
</head>
<body>
<div id="loading-mask"></div>
<div id="loading">
  <div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>
</body>
</html>
