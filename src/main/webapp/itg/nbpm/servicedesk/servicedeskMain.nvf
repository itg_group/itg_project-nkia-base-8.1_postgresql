<!DOCTYPE HTML>
<html>
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
<script type="text/javascript" charset="utf-8">

webix.ready(function() {
	var sdListType = '$!{sd_list_type}';
	
	var searchForm = createFormComp({
		id: "searchForm",
		elementsConfig: {
			labelPosition: "left"
		},
		header: {
			title: "검색",
			icon: "search",
		},
		footer: {
			buttons: {
				align: "center",
				items: [
				        createBtnComp({
				        	label: "검 색",
				        	type: "form",
				        	click: function() {
				        		searchList();
				        	}
				        }),
				        createBtnComp({
				        	label: "초기화",
				        	click: function() {
				        		resetFormData();
				        	}
				        })
				 ],
				 css: "webix_layout_form_bottom"
			}
		},
		fields: {
			colSize: 2,
			items: [
			         {colspan:1, item : createCodeComboBoxComp({label:"#springMessage('res.label.nbpm.00003')", name:'req_type', code_grp_id:'REQ_TYPE', attachChoice: true})}
					,{colspan:1, item : createTextFieldComp({label:"#springMessage('res.label.nbpm.00001')", name:'req_user_nm', on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);} }})}
					,{colspan:1, item : createSearchDateFieldComp({label:"#springMessage('res.label.nbpm.00002')", name:'reg_dt'})}
					,{colspan:1, item : createTextFieldComp({label:"#springMessage('res.label.nbpm.00004')", name:'sr_id', on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);} }})}
					,{colspan:1, item : createTextFieldComp({label:"#springMessage('res.label.nbpm.00005')", name:'title', on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);} }})}
					,{colspan: 1, item: createUnionFieldComp({
					    items: [
					            createSearchFieldComp({
									label: "서비스조회",
									name: 'service_nm',
									click: function() {
										showServicePopup();
									}
								}),
								createBtnComp({
									label: "초기화",
									click: function() {
										initPopupField();
									}
								})
					        ]
					    })
					}
			],
			// 히든 필드 - 팝업 필드 사용 시, 코드 값을 저장하는 용도로 사용합니다.
		    hiddens: {
		    	service_id : ""
		    }
		}	
	});
	
	var gridProps = {
		id: "serviceDeskList",
		keys: [],
		resizeColumn: true,
		pageable: true,
		pageSize: 30,
		url: "/itg/nbpm/servicedesk/list/searchServicedeskList.do",
		resource: 'grid.servicelist.' + sdListType.toLowerCase(),
		params: {sd_list_type: sdListType},
		header: {
			title: "#springMessage('res.label.nbpm.sdesk.00005')"
		},
		on: {
	        onItemDblClick: gridCellDblClick
	    }
	};
	
	var grid = null;
	if("READY" == sdListType){
		gridProps["actionColumns"] = [{
			column: "ACTION",
			template: function(value) {
				if(value["RECEIPT_USER_ID"] == '$!{login_user_id}') {
					return nkia.ui.html.icon("circle", "grpSlctEvent");
				} else if(value["RECEIPT_USER_ID"] != '$!{login_user_id}'){
					return nkia.ui.html.icon("circle_empty", "grpSlctEvent");
				} else{
					return "";
				}
			},
			onClick: {
				"grpSlctEvent": function(e, row, el){
					var clickRecord = this.getItem(row.row);
					setAssignAndReleaseBtnConfig(clickRecord);
				}
			}
		}];
		grid = createGridComp(gridProps);
	} else {
		grid = createGridComp(gridProps);
	}
	
	var view = {
		view: "scrollview",
		id: "app",
		body: {
			rows: [{
				cols: [{
					rows: new Array().concat(searchForm, grid)
				}]
			}]
		}
	};
	
	nkia.ui.render(view);
	
});

function reloadServiceDeskGrid() {
	searchList();
}

/**
 * 검색 폼 초기화
 */
function resetFormData(){
	$$("searchForm")._reset();
	initPopupField();
}

/**
 * 검색
 */
function searchList(){
	// 검색폼의 값들 추출
	var data = $$("searchForm")._getValues();
	data["sd_list_type"] = '$!{sd_list_type}';
	
	// 검색폼의 데이터를 인자값으로 그리드 리로드
	$$("serviceDeskList")._reload(data);
}

/**
 * 엔터키 이벤트(Enter Key = 13)
 */
function pressEnterKey(keyCode){
	if(keyCode){
		if(keyCode == "13"){ //enter key
			searchList();
		}	
	}
}

/**
 * 프로세스 상세 페이지 탭 열기(그리드 더블클릭 이벤트)
 */
function gridCellDblClick(id, e, node){
	var clickRecord = $$("serviceDeskList").getItem(id);
	var closable = true;
	var paramMap = {};
	paramMap["sr_id"] = clickRecord.SR_ID;
	paramMap["task_id"] = clickRecord.TASK_ID;
	paramMap["cnslr_user_id"] = parent.getCnslrUserInfo();
	parent.addTab('/itg/nbpm/goTaskPage.do', clickRecord, closable);
}


/**
 * 접수/해제 이벤트
 */
function setAssignAndReleaseBtnConfig(clickRecord) {
		var url;
		var questionText;
		var resultText;
		
		if(clickRecord["RECEIPT_USER_ID"] == '$!{login_user_id}') {
			url = getConstValue('CONTEXT') + "/itg/nbpm/process/releaseGroupAssignTask.do";
			questionText = "[접수취소]하시겠습니까?";
			resultText = "[접수취소]되었습니다.";
		} else if(clickRecord["RECEIPT_USER_ID"] != '$!{login_user_id}'){
			url = getConstValue('CONTEXT') + "/itg/nbpm/process/acceptGroupAssignTask.do";
			questionText = "[접수]하시겠습니까?";
			resultText = "[접수]되었습니다.";
		}
		if(confirm(questionText)) {
			nkia.ui.utils.ajax({
				url: url,
				params: {task_id : String(clickRecord.TASK_ID)},
				success: function(data) {
					if (data.success) {
						nkia.ui.utils.notification({
							message: resultText
						});
						// 검색폼의 값들 추출
						var data = {};
						data["sd_list_type"] = '$!{sd_list_type}';
						
						$$("serviceDeskList")._reload(data);
					} else {
						nkia.ui.utils.notification({
							message: data.resultMsg
						});
					}
				}			
			});
		}
}

/**
 * 서비스 조회 팝업 필드 초기화
 */
function initPopupField() {
	
	var values = {
		service_nm : "",
		service_id : ""
	};
	
	$$("searchForm")._setValues(values); // or $$("editform")._reset(); <- 전체 폼 데이터 초기화
	
}

/**
 * 서비스 조회 팝업창 오픈
 */
function showServicePopup() {
	
	var tree = createTreeComponent({
		id : 'serviceTree',
		url : '/itg/nbpm/common/searchServiceTree.do',
		width: 400,
		height: 350,
		expColable: true, // 전체펼침/접기 표시 여부
		filterable: true, // 검색필드 표시 여부
		params: {},
		expandLevel: 2,
		header: {
			title: "서비스 조회"
		},
		on: {
			onItemDblClick: treeNodeDbClick
        }
	});

	
	var popupWindow = createWindow({
    	id: "popupWindow",
    	width: 400,
    	height: 600,
    	header: {
    		title: "서비스 조회- 팝"
    	},
    	body: {
    		rows: new Array().concat(tree)
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
    				createBtnComp({label: '적 용', type: "form", click: function() {
    					var data = $$("serviceTree")._getSelectedItem();
    					
    					// 선택된 데이터가 없으면 메시지를 띄워줍니다.
    					if(!data || data.length < 1) {
    						nkia.ui.utils.notification({
    							type: 'error',
    							message: '선택된 정보가 없습니다.'
    						});
    						return false;
    					}

    					var values = {
    							service_nm : data.text,
    							service_id : data.node_id
    					};
    					$$("searchForm")._setValues(values);
    					popupWindow.close();

    				}})
    			]
    		}
    	},
    	closable: true
    });
	
	// 팝업창 오픈
	$$("popupWindow").show();
	
	/**
	 * 서비스 조회 팝업 트리 클릭
	 */
	function treeNodeDbClick(nodeId, e, element){
		
		var clickNode = this.getItem(nodeId);
		var values = {
				service_nm : clickNode.text,
				service_id : clickNode.node_id
		};
		
		$$("searchForm")._setValues(values);
		popupWindow.close();
		
	}
}

</script>
</body>
</html>