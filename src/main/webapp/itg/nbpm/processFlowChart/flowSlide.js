
function choice(num){
    var carouselContent = Ext.getElementById("carouselContent"+num)
    for(var i=1; i<=size; i++){
        if(i == num){        	
        	Ext.getElementById("carouselContent"+i).style.border = "2px solid orange";
        }else{
        	if(Ext.getElementById("carouselContent"+i) != null)
        		Ext.getElementById("carouselContent"+i).style.border = "2px solid red";
        }
    }
    document.location.href = "#formPanel2"; 
}

var priorCenterItem = 1;
var lastRan = -1;

/**
 * Custom inital load handler. Called when the carousel loads the initial
 * set of data items. Specified to the carousel as the configuration
 * parameter: loadInitHandler
 **/
var loadInitialItems = function(type, args) {
	var start = args[0];
	var last = args[1]; 
	init(this, start, last);
};

/**
 * Custom load next handler. Called when the carousel loads the next
 * set of data items. Specified to the carousel as the configuration
 * parameter: loadNextHandler
 **/
var loadNextItems = function(type, args) {
	
	// get the last middle item and turn off spotlight
	var li = this.getItem(priorCenterItem);
	
	var start = args[0];
	var last = args[1]; 
	var alreadyCached = args[2];

	if(!alreadyCached) {
		init(this, start, last);
	}
};

/**
 * Custom load previous handler. Called when the carousel loads the previous
 * set of data items. Specified to the carousel as the configuration
 * parameter: loadPrevHandler
 **/
var loadPrevItems = function(type, args) {
	// get the last middle item and turn off spotlight
	var li = this.getItem(priorCenterItem);

	var start = args[0];
	var last = args[1]; 
	var alreadyCached = args[2];
	
	if(!alreadyCached) {
		init(this, start, last);
	}
};

/**
 * Custom button state handler for enabling/disabling button state. 
 * Called when the carousel has determined that the previous button
 * state should be changed.
 * Specified to the carousel as the configuration
 * parameter: prevButtonStateHandler
 **/
var handlePrevButtonState = function(type, args) {
	var enabling = args[0];
	var leftImage = args[1];
	if(enabling) {
		leftImage.src = "/itg/base/images/ext-js/simple/process/btn_process_left.gif";	
	} else {
		leftImage.src = "/itg/base/images/ext-js/simple/process/btn_process_left_disabled.gif";
	}
	
};

/**
 * Custom button state handler for enabling/disabling button state. 
 * Called when the carousel has determined that the next button
 * state should be changed.
 * Specified to the carousel as the configuration
 * parameter: nextButtonStateHandler
 **/
var handleNextButtonState = function(type, args) {
	var enabling = args[0];
	var rightImage = args[1];
	if(enabling) {
		rightImage.src = "/itg/base/images/ext-js/simple/process/btn_process_right.gif";	
	} else {
		rightImage.src = "/itg/base/images/ext-js/simple/process/btn_process_right_disabled.gif";
	}
	
};

/**
 * You must create the carousel after the page is loaded since it is
 * dependent on an HTML element (in this case 'dhtml-carousel'.) See the
 * HTML code below.
 **/