/********************************************
 * Asset
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회
 ********************************************/
nkia.ui.popup.itam.selectAppConfSelectAsset = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 400;
	this.height = 500;
	this.url = ""; // 자산 목록
	this.resource = '';
	this.treeParams = {}; // this.treeParams = {classType: "SW"};
	this.checkbox = true;
	this.expandLevel = 1;
	this.reference = {};
	this.keys = ["CONF_ID"];
	this.dependency = {};
	this.type = "itam";
	this.bindEvent = null;
	this.callbackFunc = null;
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	this.treeGrid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		
		// 참조 : ciView.nvf
		this.treeGrid = createTreeGridComp({
			id : this.id + '_treeGrid',
			resizeColumn : true,
			checkgrid : true,
			url: '/itg/workflow/fp/searchClassTree.do', // /itg/system/amBookmark/searchBookmarkAppCiTreeGridList.do
			resource: 'grid.workflow.fp.classTreePop',
			params: {
				rel_type: "APP",
				expandLevel: 1,
				bookmark_type : "", // MY_LIST
				user_id: "" 
			},
			expandLevel: 1,
			expColable: true, // 전체펼침/접기 표시 여부
			filterable: true, // 검색여부
			header: {
				title: "어플리케이션 구성목록"
			}
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: this.treeGrid
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var _this = this;
		var treeGridItem = $$(_this.id + '_treeGrid')._getCheckedItem();
		if(treeGridItem.length < 1) {
			nkia.ui.utils.notification({type: 'error', message: '선택된 정보가 없습니다.'});
			return false;
		}else{
			var gridRows = $$(_this.id + '_treeGrid')._getRows();
			var checkArray = new Array();
			var checkBoxId = $$(_this.id + '_treeGrid').config.id + __TREE_GRID_CHECK_HEADER_SUFFIX; // 체크박스 컬럼
			for(i=0; i<gridRows.length; i++){
				var item = gridRows[i];
				if(typeof item[checkBoxId] != "undefined" && item[checkBoxId] === true ){ // 행이 체크되었다면
					var map = {};
					var conf_id_arr = item["id"].split("^");
					for(var r=0; r<conf_id_arr.length; r++){
						map["CONF_ID_PATH" + (r+1)] = conf_id_arr[r];
						if(0 == r){
							map["MODULE_ID"] = conf_id_arr[r]; // 모듈ID
							for(var x=0; x<gridRows.length; x++){
								if(conf_id_arr[r] == gridRows[x].id){
									map["MODULE_NM"] = gridRows[x].text; // 모듈명
									break;
								}
							}
						}
					}
					map["CONF_ID"] = item["node_id"]; // 구성ID
					map["CONF_NM"] = item["text"]; // 구성명
					checkArray.push(map);
				}
			}
            this.window.close();
			if(this.callbackFunc){
				this.callbackFunc(checkArray);
			}
        }
	};
};

/**
 * App구성정보 팝업
 * @param {} popupProps
 */
function createAppConfWindow(popupProps) {
	var props = popupProps||{};
	//console.log(">>> createApplicationConfWindow > props=", props);
	var asset;
	try {
		switch (props.type) {
			case "ConfMulti":
				asset = new nkia.ui.popup.itam.selectAppConfSelectAsset();
				break;
			default:
				alert("화면유형이 잘못되었습니다. ");
		}
		asset.setProps(props);
		asset.setRender(props);
		asset.setWindow();
	} catch(e) {
		alert(e.message);
	}
}



