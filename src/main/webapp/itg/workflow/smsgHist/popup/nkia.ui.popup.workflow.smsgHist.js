/********************************************
 * Date: 2017-02-16
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  엑셀다운로드 팝업
 ********************************************/
// 일반 엑셀 다운로드
nkia.ui.popup.workflow.ExcelByDynamic = function() {
	// 속성 Properties
	this.id = "";
	this.param = {};	// 조회조건
	this.excelParam = {};	// excelParam 필수 항목 :  fileName, sheetName, titleName, includeColumns, headerNames, defaultColumns, url 
	this.title = "엑셀다운로드";
	this.closable = true;
	this.width = 450;
	this.height = 500;
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var propSelectPop = null;
		var propNmArray = [];
		var propIdArray = [];
		var colNm;
		var colId;
		var addData = {};
		
		var tempExcelParam = this.excelParam;
		var param = this.param;
		
		this.excelAttrGrid = createGridComp({
			id : this.id+'_grid',
			resizeColumn : true,
			checkbox : true,
			resource : "grid.popup.excel",
			autoLoad : false,
			header : {
				title : "속성 선택",
			}
		});
		
	} // end this.setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.excelAttrGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.downExcelFile.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
		
	} // end setWindow
	
	this.setGridAttrData = function() {
		var popGrid = $$(this.id+'_grid');
		var gridData = new Array();
		var colNm = this.excelParam['headerNames'].split(",");
		var colId = this.excelParam['includeColumns'].split(",");
		
//		popGrid._addRow(gridData);
		var rowDataArr = new Array();
		for ( var i = 0; i < colNm.length; i++) {
			var rowDataMap = {
					'RNUM' : i,
					'COL_NM' : colNm[i],
					'COL_ID' : colId[i],
			}
			rowDataArr.push(rowDataMap);
		}
		popGrid._addRow(rowDataArr);
		
	} // end setGridArrtData
	
	
	this.downExcelFile = function(){
		var _this = this;
		var propNmArray = [];
		var propIdArray = [];
		var selectedRecords = $$(this.id+'_grid')._getSelectedRows();
		if (selectedRecords.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택한 속성이 없습니다.'
			});
		} else {
			for ( var i = 0; i < selectedRecords.length; i++) {
				var col_nm = nullToSpace(selectedRecords[i].COL_NM);
				var col_id = nullToSpace(selectedRecords[i].COL_ID);
				propNmArray.push(col_nm);
				propIdArray.push(col_id);
			}
			
			var headerNm = propNmArray.join();
			var headerId = propIdArray.join();
			
			var excelAttrs = {
					sheet : [ {
						sheetName : this.excelParam['sheetName'],
						titleName : this.excelParam['titleName'],
						headerWidths : this.excelParam['headerWidths'],
						headerNames : headerNm,
						includeColumns : headerId,
						groupHeaders : this.excelParam['groupHeaders'],
						align : this.excelParam['align']
					} ]
				};
			
			var excelParam = {};
			excelParam['fileName'] = this.excelParam["fileName"];
			excelParam['excelAttrs'] = excelAttrs;

			goExcelDownLoad(this.excelParam['url'], this.param, excelParam);
			_this.window.close();
		}
		
	} // downExcelFile
	
} // end nkia.ui.popup.itam.excel.ExcelByDynamic 

/**
 * 엑셀 다운로드 속성선택 팝업
 * @param {}
 */
function creatSmsgHistPop(popupProps) {
	var props = popupProps||{};
	
	var popupObject;
	try {
		switch (props.type) { // dynamic 일반 엑셀 다운로드
			case 'Dynamic' :
				popupObject = new nkia.ui.popup.workflow.ExcelByDynamic();
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
				popupObject.setGridAttrData();
			break;
			default:
		}
	} catch(e) {
		alert(e.message);
	}
}


