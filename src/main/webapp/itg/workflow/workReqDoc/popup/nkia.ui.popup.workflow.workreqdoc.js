/********************************************
 * Vendor
 * Date: 2017-11-16
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.workflow.workreqdoc = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = "";
	this.height = "";
	this.url = "";
	this.resource = "";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.bindEvent = null;
	this.dependency = {};
	this.header = "";
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.insertForm = [];
	this.grid = [];
	this.mode = "";
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.insertForm = createFormComp({
			id: this.id + "_searchForm",
			header: {
	        	title: this.title
		    },
		    fields: {		    	
		        colSize: 2,
		        items: [
					{ colspan:1, item: createTextFieldComp({label: '요청양식명', name: 'req_doc_nm', readonly: true})},
					{ colspan:1, item: createTextFieldComp({label: '고객사명', name: 'customer_nm', readonly: true})},
					{ colspan:1, item: createCodeComboBoxComp({label: '요청서브타입', name: 'sub_req_type', code_grp_id: 'SUB_REQ_TYPE', attachChoice: true, required: false })},
					{ colspan:1, item: createCodeComboBoxComp({label: '관리자승인 여부', name: 'manager_cnfirm_cd', code_grp_id: 'MANAGER_CNFIRM_CD', attachChoice: true, required: false })},
					//{ colspan:1, item: createTextFieldComp({label: '더미', name: 'dummy_02', required: false, readonly: true, isProcAfterHide: true})},
					{ colspan:2, item: createTextAreaFieldComp({label: '공지사항', name: 'req_notice', resizable: true})},
					{ colspan:2, item: createTextAreaFieldComp({label: '요청내용', name: 'req_format', required: false, resizable: true})},
					{ colspan:1, item: createCodeComboBoxComp({label: '요청부서 승인여부', name: 'req_cust_cnfirm_yn', code_grp_id: 'YN', attachChoice: true, required: false })},
					{ colspan:1, item: createCodeComboBoxComp({label: '신청유형선택여부', name: 'requst_type_yn', code_grp_id: 'YN', attachChoice: true, required: false })},
					{ colspan:1, item: createCodeComboBoxComp({label: '서비스연계 신청여부', name: 'linkrel_service_yn', code_grp_id: 'YN', attachChoice: true, required: false })},
					{ colspan:1, item: createCodeComboBoxComp({label: '변경연계 신청여부', name: 'linkrel_change_cd', code_grp_id: 'LINKREL_CHANGE_CD', attachChoice: true, required: false })},
					{ colspan:1, item: createCodeComboBoxComp({label: '서식파일 첨부여부', name: 'template_downloader_yn', code_grp_id: 'YN', attachChoice: true, required: true ,on:{onchange: function(){hideTemplate()}}})},
					{ colspan:1, item: createTextFieldComp({label: '처리마감일', name: 'end_due_cnt', required: false })},					
					//{ colspan:1, item: createTextFieldComp({label: '더미', name: 'dummy_01', required: false, readonly: true, isProcAfterHide: true})},
					{ colspan:1,item:createOperSelectComp({label:"요청부서관리자", name:"OPER_APPR_01", processType:"SERVICE",required: false})},
					{ colspan:1,item:createOperSelectComp({label:"접수그룹", name:"OPER_APPM_01", processType:"SERVICE",required: false})},
					//{ colspan:1, item: createTextFieldComp({label: '접수그룹id지정', name: 'role_opetr_id'})},
					{ colspan:2, item: createAtchFileComp({label: '추가', id: 'atch_file_id', name:'atch_file_id', register: "$!{UserSession.getSesssionUserNm()}", height:100, allowFileExt: CONSTRAINT.DEFAULT_FILE_ALLOW_EXT})} 
		        ],
			    hiddens: {
			    	req_doc_id: '',
			    	customer_id: ''
			    }
		    }/*,
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }*/
		});					
	};
	//var view = {view:'scrollview',id:'app',body:{cols:[{rows:new Array().concat(flowList, panelList)},{view:'template',width:30,template:"<div id='sidebar'><div id='scroller-anchor'></div><div id='scroller' style='margin-top:5px;' onclick='doScrollTop();'><button type='button' style='height:100%;width:100%;' class='webix_icon_button'><span class='webix_icon fa-angle-double-up '></span></button></div></div>"}]}};	
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: new Array().concat(this.insertForm)
			            }]
					}
			};
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,	    	
	    	header: {
	    		title: this.header
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: [bodyScrollView]
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저 장', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
			return false;
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		$$(this.id + '_grid')._reload(form._getValues());
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 저장 이벤트
	this.choice = function(id, e){
		var isValid = $$(this.id + '_searchForm')._validate();
		if(isValid != true){
			return;
		}
		
		var saveParam = $$(this.id + '_searchForm')._getValues();

		var saveUrl = "";
		if(this.mode == "insert"){
			saveUrl = "/itg/workflow/workReqDoc/insertWorkReqDocCustomer.do";
		}else{
			saveUrl = "/itg/workflow/workReqDoc/updateWorkReqDocCustomer.do";
		}
		
		var callbackSub = function(atch_file_id) {
			if("" != nullToSpace(atch_file_id)){
				saveParam['atch_file_id'] = atch_file_id;
			}
			var callbackSub2 = function() { // 파일(atch_file2_id) 저장결과
				nkia.ui.utils.ajax({
					url: saveUrl, // 저장 [insert/update/delete] 
					params:saveParam, 
					isMask:true,
					async: false,
					success:function(data){
						if(data.success){
							//console.log(data);
							var resultMap = data.resultMap;
							nkia.ui.utils.notification({type:'info', message:data.resultMsg, expire:3000});
							$$('reqDocToCustomerGrid')._reload();
							if($$("customerUpdatePop")!==undefined){
								$$("customerUpdatePop").close();
							}else{
								$$("customerInsertPop").close();
							}
						}else{
							nkia.ui.utils.notification({type:'error', message:data.resultMsg});
						}
					}
				});
			}
			callbackSub2("");
		}
		if($$("atch_file_id")){
			$$("atch_file_id")._send(callbackSub, $$(this.id + '_searchForm'));
		}else{
			callbackSub("");
		}
	};
};

/**
 * 업체 팝업
 * @param {} popupProps
 */
function createCustomerInsertWindow(popupProps) {
	var props = popupProps||{};
	var comp;
	try {
		switch (props.type) {
			default:
				comp = new nkia.ui.popup.workflow.workreqdoc();
		}
		comp.setProps(props);
		comp.setRender(props);
		comp.setWindow();
	} catch(e) {
		alert(e.message);
	}
}

function hideTemplate(){
	if($$("customerUpdatePop")!==undefined){
		if($$("customerUpdatePop_searchForm")._getFieldValue("template_downloader_yn")=="Y"){
			$$("atch_file_id_list_header").show();
			$$("atch_file_id_list").show();
		}else{
			$$("atch_file_id_list_header").hide();
			$$("atch_file_id_list").hide();
		}
	}
	
	if($$("customerInsertPop")!==undefined){
		if($$("customerInsertPop_searchForm")._getFieldValue("template_downloader_yn")=="Y"){
			$$("atch_file_id_list_header").show();
			$$("atch_file_id_list").show();
		}else{
			$$("atch_file_id_list_header").hide();
			$$("atch_file_id_list").hide();
		}
	}
}