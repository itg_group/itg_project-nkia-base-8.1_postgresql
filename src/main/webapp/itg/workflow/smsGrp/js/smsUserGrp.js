function createSmsUserGrp(){
	
	var searchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct'});
	
	var insertSmsUserGrpBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.insert'}), id:'insertSmsUserGrpBtn', ui : 'correct'}); 
	var deleteSmsUserGrpBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.delete'}), id:'deleteSmsUserGrpBtn'}); 
	
	// createDeptSelectPopComp({label:'부서', name:'dept_nm', id:'dept_nm_pop', notNull : true, maxLength: 100})
	
	var deptSelectPop = createTreeSelectPopComp
	({
		popId:'deptSelectPop'
		, popUpTitle: getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00001' })
		, targetName:'cust_nm'
		, targetHiddenName:'DEPT_ID'
		, targetForm:'searchDeptUser'
		, treeId:'deptSelectTree'
		, url: '/itg/base/searchDeptTree.do'
		, targetLabel:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00002' })
		, btnId:'custSelectBtn'
		, params: {cust_id : "0"}
		, rnode_text: getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00003' })
		//, notNull:true
		, leafSelect: false
		, expandLevel: 2
		, width: 245
	});
	
	// 부서별 사용자 그리드 검색
	var setSearchFormProp = {
		id: 'searchDeptUser',
		title: getConstText({isArgs: true, m_key: 'res.common.search'}),
		formBtns: [searchBtn],
		columnSize: 1,
		tableProps : [{
						colspan:1, tdHeight: 30, 
						//item: deptComboBox()
						//item: createSelectPopComp({label:'부서', name:'DEPT_ID', id:'dept_nm_pop', notNull : true, maxLength: 100})
						item: deptSelectPop
					/*
					},{
						colspan:1, tdHeight: 30, 
						item: createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.smsPosition'}), labelWidth : 30, name:'POSITION', width : 150})
					*/
					},{
						colspan:1, tdHeight: 30, 
						item : createUnionCodeComboBoxTextContainer({label:getConstText({isArgs: true, m_key: 'res.label.smsUser'}), comboboxName:'USER_GUBN', code_grp_id:'SEARCH_USER', textFieldName: 'SEARCH_VALUE', widthFactor: 0.25})
					}]
	}
	var searchDeptUserPanel = createSeachFormComp(setSearchFormProp);
	
	//부서별 사용자 그리드
	var setDeptUserGridProp = {	
		id: 'deptUserGrid',
		//title: '부서별사용자' ,
		title: getConstText({isArgs: true, m_key: 'res.title.deptUserGrid'}),
		resource_prefix: 'grid.workflow.deptGrpUser',
		url: '/itg/workflow/smsGrp/searchDeptUserGrid.do',
		params: null,//{SMS_GRP_ID : '0000009'},
		autoLoad: false,
		border : false,
		//scrollPaging: true,
		pagingBar : true,
		pageSize: 20,
		selModel : true,
		gridHeight : 450,
		emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
	};
	var deptUserGrid = createGridComp(setDeptUserGridProp);
	
	//deptUserGrid.getView().selectedItemCls = "";
	//deptUserGrid.getView().focusedItemCls = "";
	
	var deptUserProp = {
		id : 'deptUserPanel',
		height : 580,
		panelItems : [{
			width : '100%',
			items : searchDeptUserPanel
		},{
			width : '100%',
			items : deptUserGrid
		}]
	};
	var deptUserPanel = createVerticalPanel(deptUserProp);
	
	var setSmsGrpUserGridProp = {	
			id: 'smsGrpUserGrid',
			//title: 'SMS그룹 사용자' ,
			title: getConstText({isArgs: true, m_key: 'res.title.smsGroupUser'}),
			resource_prefix: 'grid.workflow.smsGrpUser',
			url: '/itg/workflow/smsGrp/searchSmsGrpUserList.do',
			params: null,
			autoLoad: false,
			border : false,
			//isMemoryStore : true,
			selModel : true,
			gridHeight :580,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var smsGrpUserGrid = createGridComp(setSmsGrpUserGridProp);
	/*
	// 더하기 버튼 
	var move_btn_right = new Ext.button.Button({
        icon:'/itg/base/images/ext-js/simple/btn-icons/moveBtn.gif',
        handler: function(thisBtn,event) {
        	
        	moveSmsGrpUserGridToGrid(deptUserGrid, smsGrpUserGrid, "RIGHT");
        	
        } // handler 종료
	}); //버튼생성종료
	
	// 빼기버튼
	var move_btn_left = new Ext.button.Button({
        icon:'/itg/base/images/ext-js/simple/btn-icons/moveBtn2.gif',
        handler: function(grid, rowIndex, colIndex) {
        	
        	moveSmsGrpUserGridToGrid(smsGrpUserGrid, deptUserGrid, "LEFT");
        	
        } //handler종료
	}); //버튼 종료
	*/
	var move_btn_right = createIconBtnComp({id:'moveRightBtn', icon :'/itg/base/images/ext-js/simple/btn-icons/moveBtn.gif'});
	var move_btn_left = createIconBtnComp({id:'moveLeftBtn'	, icon :'/itg/base/images/ext-js/simple/btn-icons/moveBtn2.gif'});
	
	attachBtnEvent(move_btn_right ,actionLink ,"moveRight");
	attachBtnEvent(move_btn_left ,actionLink ,"moveLeft");
	
	var outerPanelProp = {
			id: 'outerPanel',
	        //title: 'SMS 그룹 - 사용자',
			title: getConstText({isArgs: true, m_key: 'res.title.smsGroup-User'}),
	        align : 'middle',
	        height : 600,
	        panelBtns: [insertSmsUserGrpBtn],
	        panelItems: [{
				flex: 1,
	            style: 'border: solid #A3B7CC 1px',
	            margin: '10 10 10 10',
	            vline : false,
				items: deptUserPanel
			  },{
			  	width : 37,
			  	vline : false,
			  	margin: '0 0 0 0',
			  	items : createUnionFieldContainer({layout : 'vbox', fixedLayout: true, fixedWidth : 37, margin : '10 0 10 0', items : [move_btn_right, move_btn_left], isNotColspan: true})
			  },{
	    		flex: 1,
	            style: 'border: solid #A3B7CC 1px',
	            margin: '10 10 10 10',
	            vline : false,
			  	items: smsGrpUserGrid
			}]
		}
		
	var outerPanel = createHorizonPanel(outerPanelProp);
	
	// 부서별 사용자 트리 선택시
	//attachCustomEvent('select', deptUserTree, deptUserTreeClick);
	// 부서별 사용자 트리 검색 버튼 클릭시 
	attachBtnEvent(searchBtn, actionLink,  'searchAttr');
	attachBtnEvent(insertSmsUserGrpBtn, actionLink,  'insertSmsUserGrp');
	
	insertSmsUserGrpBtn.setVisible(false);
	
	return outerPanel;
}

//부서별 사용자 트리 클릭시 (사용방법 체크)
function deptUserTreeClick(tree, record, index, eOpts){
	
	var checked = record.data.checked;
	var value ;
	
	// 기존에 True이면 false로 변경 
	if(checked){
		value = false
	}else{
		value = true
	}
	
	// nkia_common.js 참고 
	setTreeCheckAll(record,value);
	//트리 선택 취소
	tree.view.selModel.deselectAll();
	
}

/**
 *  부서 콤보박스 생성 
 */
function deptComboBox(){
	
	var param = {};
	
	var deptCombo_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/workflow/smsGrp/searchDeptComboBox.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}, listeners :{
			changeData : function(p) {
			}
		}
	});
 
	var deptCombo_val = "";
		deptCombo_val = {label:'부서', labelWidth : 50, id: 'DEPT_ID', name:'DEPT_ID', width: 200, attachAll : true};
		deptCombo_val['proxy'] = deptCombo_proxy;
		deptCombo_val['valueField'] = 'CUST_ID';  
		deptCombo_val['displayField'] = 'CUST_NM';
	 
	var deptComboBox = createComboBoxComp(deptCombo_val);
	
	return deptComboBox;
}

