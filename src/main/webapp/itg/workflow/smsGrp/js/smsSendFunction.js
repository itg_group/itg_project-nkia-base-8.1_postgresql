function createTextButton(){
	// 추가 버튼
	var addReceiveHpNoBtn 
		= createBtnComp({
			  //label: '추가'
			label: getConstText({isArgs: true, m_key: 'btn.common.append.trim'}) 
			, id:'addReceiveHpNoBtn'
		});
	// 입력필드
	var textField 
		= createTextFieldComp({
			//label:'수신번호'
			label:getConstText({isArgs: true, m_key: 'btn.common.toNum'})
			, name:'receiveHpNo'
			, id:'receiveHpNo'
			, vtype : 'cellPhoneNum'
			//, widthFactor : 0.8
		});
	
	// 하나로 합쳐서 보여줍니다. 
	var textButton = {
		xtype: 'fieldcontainer',
		width: 500,
	    layout:'hbox',
		items: [textField, addReceiveHpNoBtn]
	} 
	
	// 버튼 액션 
	attachBtnEvent(addReceiveHpNoBtn, actionLink,  'addReceiveHpNoBtn');
	
	return textButton;
}


function createUserForm(){
	
	// 사용자 검색버튼 
	var searchUserBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchUserBtn', ui : 'correct'});
	var searchUserInitBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.reset'}), id:'searchUserInitBtn'});
	
	function clickSearchBtn() {
		actionLink('userSearch');
	}
	
	// 사용자 검색 폼
	var setSearchUserFormProp = {
			id: 'searchUserForm',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			formBtns: [searchUserBtn, searchUserInitBtn],
			enterFunction : clickSearchBtn,
			tableProps : [{colspan:2, tdHeight: 30, 
							item : createUnionCodeComboBoxTextContainer({label:getConstText({isArgs: true, m_key: 'res.common.search'}), comboboxName:'code_id', code_grp_id:'SEARCH_SMS_USER', textFieldName: 'search_value', widthFactor: 0.25})
						}]
			}
			
	var searchUserForm = createSeachFormComp(setSearchUserFormProp);
	
	// 버튼 액션 부분 
	attachBtnEvent(searchUserBtn, actionLink,  'userSearch');
	attachBtnEvent(searchUserInitBtn, actionLink,  'searchUserInit');
	
	// 사용자 리스트
	var setSmsUserGridProp = {	
			id: 'smsUserGrid',
			title: getConstText({isArgs: true, m_key: 'res.title.smsUserList'}),
			resource_prefix: 'grid.workflow.smsUser',
			url: '/itg/workflow/smsGrp/searchSmsUserList.do',
			params: null,
			autoLoad: true,
			border : false,
			pagingBar : true,
			pageSize : 10,
			forceFit : true,
			gridHeight :300,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	var smsUserGrid = createGridComp(setSmsUserGridProp);
	
	// 사용자검색 폼 + 사용자 리스트 패널 
	var userPanel = Ext.create('nkia.custom.Panel',{
		id : 'userPanel',
		//title : '사용자목록',
		title : getConstText({isArgs: true, m_key: 'res.title.smsUserList'}),
		items: [searchUserForm, smsUserGrid] 
	});
	
	// 그리드 셀 클릭시
	attachCustomEvent('cellclick', smsUserGrid, smsUserGridCellClick);
	
	return userPanel;
}


function createGrpPanel(){
	
	// 그룹검색 버튼
	var searchGrpBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchGrpBtn', ui : 'correct'});
	var searchGrpInitBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.reset'}), id:'searchGrpInitBtn'});
	
	function clickSearchBtn() {
		actionLink('grpSearch');
	}
	
	// 사용자 검색 폼
	var setSearchGrpFormProp = {
			id: 'searchGrpForm',
			title : getConstText({isArgs: true, m_key: 'res.common.search'}),
			columnSize: 2,
			enterFunction : clickSearchBtn,
			formBtns: [searchGrpBtn, searchGrpInitBtn],
			tableProps : [{colspan:2, tdHeight: 30, 
							item : createUnionCodeComboBoxTextContainer({label:getConstText({isArgs: true, m_key: 'res.common.search'}), comboboxName:'code_id', code_grp_id:'SEARCH_SMS_GRP', textFieldName: 'search_value', widthFactor: 0.25})
						}]
			}
			
	var searchGrpForm = createSeachFormComp(setSearchGrpFormProp);
	
	// 버튼 액션 
	attachBtnEvent(searchGrpBtn, actionLink,  'grpSearch');
	attachBtnEvent(searchGrpInitBtn, actionLink,  'searchGrpInit');
	
	var setSmsGrpGridProp = {	
		id: 'smsGrpGrid',
		//title: '그룹목록' ,
		title: getConstText({isArgs: true, m_key: 'res.title.smsGroupList'}),
		resource_prefix: 'grid.workflow.smsGrp',
		url: '/itg/workflow/smsGrp/searchSmsGrpList.do',
		params: null,
		autoLoad: true,
		border : false,
		pagingBar : true,
		pageSize : 10,
		forceFit : true,
		gridHeight :300,
		emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
	};
	var smsGrpGrid = createGridComp(setSmsGrpGridProp);
	
	// 사용자검색 폼 + 사용자 리스트 패널 
	var grpPanel = Ext.create('nkia.custom.Panel',{
		id : 'grpPanel',
		//title : '그룹목록',
		title : getConstText({isArgs: true, m_key: 'res.title.smsGroupList'}),
		items: [searchGrpForm, smsGrpGrid] 
	});
	
	// 그리드 셀 클릭시 
	attachCustomEvent('cellclick', smsGrpGrid, smsGrpGridCellClick);
	
	return grpPanel;
	
}

// 사용자 그리드 셀 클릭시 
function smsUserGridCellClick(gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e){
	
	var sendGrid = getGrid("smsSendGrid").getStore();
	var gridCnt = sendGrid.data.length;
	var gridData = sendGrid.data;
	
	if(30 < gridCnt+1){
		//showMessage("최대 발송 인원은 30명까지 입니다. ");
		showMessage(getConstText({isArgs: true, m_key: 'msg.sms.smsMaxSending'}));
		return false;
	}	
	
	// 선택한 사용자 정보 
	var userId = record.raw.USER_ID; 
	var userNm = record.raw.USER_NM; 
	var hpNo = record.raw.HP_NO;
	
	// 발송 대상자에 포함되어있는지 확인 합니다.
	for ( var i = 0; i < gridData.length; i++) {
		var sendUserId = gridData.items[i].raw.USER_ID;
		if(userId == sendUserId){
			//showMessage("발송대상자에 이미 포함되어있습니다.");
			showMessage(getConstText({isArgs: true, m_key: 'msg.sms.userDuplicate'}));
			return false;
		}
	}
	// 핸드폰 번호가 없는 사용자를 추가할지 질문 합니다.
	if("" == hpNo
		|| null == hpNo){
		//if(!confirm("선택한 사용자는 핸드폰 번호가 없습니다. 진행하시겠습니까?")){
		if(!confirm(getConstText({isArgs: true, m_key: 'msg.sms.confirm.emptyPhone'}))){
			return false;
		}
		hpNo = "-";
	}
	
	// Store에 Item 추가 
	sendGrid.add({"SMS_GRP_NM":"-" , "USER_ID":userId, "USER_NM":userNm, "HP_NO":hpNo});
	//sendGrid.add({"USER_ID":userId,"USER_NM":userNm,"HP_NO":hpNo});
} // smsUserGridCellClick 종료

// 그룹 그리드 셀 클릭시  
function smsGrpGridCellClick(gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e){
	
	var paramMap = {};
	var smsGrpId =  record.raw.SMS_GRP_ID;
	var msg =  record.raw.DEFAULT_MSG;
	paramMap["SMS_GRP_ID"] = smsGrpId;
	Ext.getCmp('DEFAULT_MSG').setValue(msg);
	
	// 데이터 조회
	jq.ajax({ type:"POST"
		, url:'/itg/workflow/smsGrp/searchSmsGrpUserList.do'
		, contentType: "application/json"
		, dataType: "json"
		, async: true
		, data : getArrayToJson(paramMap)
		, success:function(data){
			
			var sendGrid = getGrid("smsSendGrid").getStore();
			var resultList = data.gridVO.rows;
			
			// 그룹안의 사용자 정보 등록 
			for ( var i = 0; i < resultList.length; i++) {
				// 등록여부 확인 파라미터
				var insertYn = true;
				
				// 선택한 사용자 정보 
				var userId = resultList[i].USER_ID; 
				var userNm = resultList[i].USER_NM; 
				var hpNo = resultList[i].HP_NO;
				var smsGrpNm = resultList[i].SMS_GRP_NM;
				
				// 발송 대상자에 포함되어있는지 확인 합니다.
				var gridCnt = sendGrid.data.length;
				var gridData = sendGrid.data;
				for ( var j = 0; j < gridData.length; j++) {
					var sendUserId = gridData.items[j].raw.USER_ID;
					if(userId == sendUserId){
						// 발송 대상자에 같은 userId가 있으면 등록 거부
						insertYn = false;
					}
				}
				// 등록여부 확인 후 등록 
				if(insertYn){
					// Store에 Item 추가 
					sendGrid.add({"SMS_GRP_NM":smsGrpNm , "USER_ID":userId, "USER_NM":userNm, "HP_NO":hpNo});
				}
			} //for 종료
		}//success function 종료 
	});
} // smsGrpGridCellClick 종료
