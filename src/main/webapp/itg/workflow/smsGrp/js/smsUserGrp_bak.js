function createSmsUserGrp(){
	
	var searchBtn = createBtnComp({label: getConstText({isArgs: true, m_key: 'btn.common.search'}), id:'searchBtn', ui : 'correct', scale : 'medium'});
	
	// 부서별 사용자 그리드 검색
	var setSearchFormProp = {
		id: 'searchDeptUser',
		title: getConstText({isArgs: true, m_key: 'res.common.search'}),
		formBtns: [searchBtn],
		columnSize: 2,
		tableProps : [{
						colspan:1, tdHeight: 30, 
						item: deptComboBox()
					/*
					},{
						colspan:1, tdHeight: 30, 
						item: createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.smsPosition'}), labelWidth : 30, name:'POSITION', width : 150})
					*/
					},{
						colspan:1, tdHeight: 30, 
						item: createCodeComboBoxComp({label: getConstText({isArgs: true, m_key: 'res.label.smsUser'}), labelWidth : 50, name:'USER_GUBN', code_grp_id:'SEARCH_USER', width: 200, padding : '0 5 0 0'})
					},{
						colspan:1, tdHeight: 30, 
						item: createTextFieldComp({name:'SEARCH_VALUE', width : 150})
					}]
	}
	var searchDeptUserPanel = createSeachFormComp(setSearchFormProp);
	
	
	//부서별 사용자 그리드
	var setDeptUserGridProp = {	
		id: 'deptUserGrid',
		//title: '부서별사용자' ,
		title: getConstText({isArgs: true, m_key: 'res.title.deptUserGrid'}),
		resource_prefix: 'grid.workflow.deptGrpUser',
		url: '/itg/workflow/smsGrp/searchDeptUserGrid.do',
		params: null,
		autoLoad: false,
		border : false,
		scrollPaging: true,
		gridHeight : 400,
		emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
	};
	var deptUserGrid = createGridComp(setDeptUserGridProp);
	
	deptUserGrid.getView().selectedItemCls = "";
	deptUserGrid.getView().focusedItemCls = "";
	
	var deptUserProp = {
		id : 'deptUserPanel',
		panelItems : [{
			width : '100%',
			items : searchDeptUserPanel
		},{
			width : '100%',
			items : deptUserGrid
		}]
	};
	var deptUserPanel = createVerticalPanel(deptUserProp);
	
	var setSmsGrpUserGridProp = {	
			id: 'smsGrpUserGrid',
			//title: 'SMS그룹 사용자' ,
			title: getConstText({isArgs: true, m_key: 'res.title.smsGroupUser'}),
			resource_prefix: 'grid.workflow.smsGrpUser',
			url: '/itg/workflow/smsGrp/searchSmsGrpUserList.do',
			params: null,
			autoLoad: true,
			border : false,
			selModel : true,
			gridHeight :530,
			emptyMsg: getConstText({isArgs: true, m_key: 'msg.common.00001'})
		};
	
	var smsGrpUserGrid = createGridComp(setSmsGrpUserGridProp);
	
	// 더하기 버튼 
	var move_btn_right = new Ext.button.Button({
        icon:'/itg/base/images/ext-js/simple/btn-icons/moveBtn.gif',
        handler: function(thisBtn,event) {
        	
        	var paramMap = Ext.getCmp("smsGrpEditForm").getInputData();
        	//var theForm = Ext.getCmp("deptUserTree");
        	var theForm = Ext.getCmp("deptUserGrid");
        	var arrayList = new Array();
        	// 좌측 트리를 선택안했을 경우 
        	if(!paramMap.SMS_GRP_ID){
        		//showMessage("문자메시지 그룹을 먼저 선택하시기 바랍니다.");
        		showMessage(getConstText({isArgs: true, m_key: 'msg.sms.clickMessageGroup'}));
        		return false;
        	}else{
        		
        		// 부서별 사용자그리드 
        		var selData = theForm.view.selModel.store.data;
        		var count = theForm.view.selModel.store.data.length;
        		
        		// 사용자를 체크안했을 경우
        		if(0 == count){
        			//showMessage("그룹에 등록할 사용자 먼저 선택하시기 바랍니다.");
        			showMessage(getConstText({isArgs: true, m_key: 'msg.sms.clickInsertUser'}));
            		return false;
        		}else{
        			
        			var theGrid = Ext.getCmp("smsGrpUserGrid");
        			var theGridStore = theGrid.getStore();
        			
        			// 그리드에 이미 사용자가 있는경우 한번더 체크 
        			if(0 < theGridStore.data.length){
        				//if(!confirm('사용자가 등록되어있습니다. 새로운 정보로 등록 하시겠습니까?')){
        				if(!confirm(getConstText({isArgs: true, m_key: 'msg.sms.confirm.checkUser'}))){
        					return false;
        				}
        			}
        			
        			// 체크된 사용자의 정보를 arrayList 담습니다.
            		for ( var i = 0; i < count; i++) {
            			if(true == selData.items[i].data.CHECKED
            					|| "true" == selData.items[i].data.CHECKED){
            				var tempArray = Ext.getCmp("smsGrpEditForm").getInputData();
            				tempArray["USER_ID"] = selData.items[i].data.USER_ID;
            				tempArray["USER_NM"] = selData.items[i].data.USER_NM;
            				arrayList.push(tempArray);
            			}
    				}
        			
        			// ajax로 보내기 위해 Map으로 감싸줍니다.
                	paramMap["arrayList"] = arrayList;
        			// 등록 ajax 시작
        			jq.ajax({ type:"POST"
	        			, url: '/itg/workflow/smsGrp/insertSmsGrpUser.do'
	        			, contentType: "application/json"
	        			, dataType: "json"
	        			, async: false
	        			, data : getArrayToJson(paramMap)
	        			, success:function(data){
	        				
	        				showMessage(data.resultMsg);
	        				// 그리드 리로드
	        				var theGrid = Ext.getCmp('smsGrpUserGrid');
	        				var store = theGrid.getStore();
	        				store.getProxy().jsonData = paramMap;
	        				store.reload();
	        				
	        				// 부서별 사용자 그리드 리로드
	        				theGrid = Ext.getCmp('deptUserGrid'); 
	        				store = theGrid.getStore();
	        				store.getProxy().jsonData = paramMap;
	        				store.load();
	        			}
	        		}); //ajax 종료
        		}// else종료
        	}// else종료
        } // handler 종료
	}); //버튼생성종료
	
	// 빼기버튼
	var move_btn_left = new Ext.button.Button({
        icon:'/itg/base/images/ext-js/simple/btn-icons/moveBtn2.gif',
        handler: function(grid, rowIndex, colIndex) {
        	
        	var paramMap = {};
        	//체크된 데이터 가져오기
        	var selected = smsGrpUserGrid.view.selModel.getSelection();                       
        	var param = Ext.getCmp("smsGrpEditForm").getInputData();
        	// 사용자가 선택이 안되었을 때
        	if(0 >= selected.length){
        		//showMessage("삭제하실 사용자를 선택하여 주십시요");
        		showMessage(getConstText({isArgs: true, m_key: 'msg.sms.clickDeleteUser'}));
        		return false;
        	}else{
        		
        		if(!confirm(getConstText({isArgs: true, m_key: "msg.common.confirm.00003"}))){
    				return false;
    			}
        		
        		var arrayList = new Array();
        		for ( var i = 0; i < selected.length; i++) {
					var tempArray = new Array();
					tempArray = selected[i].raw;
					arrayList.push(tempArray);
				}
        		// ajax로 보내기 위해 Map으로 감싸줍니다.
        		paramMap["arrayList"] = arrayList;
        		
        		// 삭제 ajax 시작
    			jq.ajax({ type:"POST"
        			, url: '/itg/workflow/smsGrp/deleteSmsGrpUser.do'
        			, contentType: "application/json"
        			, dataType: "json"
        			, async: false
        			, data : getArrayToJson(paramMap)
        			, success:function(data){
        				
        				showMessage(data.resultMsg);
        				// 그리드 리로드
        				var theGrid = Ext.getCmp('smsGrpUserGrid');
        				var store = theGrid.getStore();
        				store.getProxy().jsonData = param;
        				store.reload();
        				
        				// 부서별 사용자 그리드 리로드
        				theGrid = Ext.getCmp('deptUserGrid');  
        				store = theGrid.getStore();
        				store.getProxy().jsonData = param;
        				store.load();
        			}
        		}); //ajax 종료
        	}//else 종료
        } //handler종료
	}); //버튼 종료
	
	var outerPanelProp = {
			id: 'outerPanel',
	        //title: 'SMS 그룹 - 사용자',
			title: getConstText({isArgs: true, m_key: 'res.title.smsGroup-User'}),
	        align : 'middle',
	        panelItems: [{
				flex: 1,
	            style: 'border: solid grey 1px',
	            vline : false,
				items: deptUserPanel
			  },{
				width : 25,
	    		items: move_btn_left,
	    		vline : false,
	    		syncSkip: true
	    	  },{
	    		width : 25,
	    		items: move_btn_right,
	    		vline : false,
	    		syncSkip: true
	    	  },{
	    		flex: 1,
	            style: 'border: solid grey 1px',
	            vline : false,
			  	items: smsGrpUserGrid
			}]
		}
		
	var outerPanel = createHorizonPanel(outerPanelProp);
	
	// 부서별 사용자 트리 선택시
	//attachCustomEvent('select', deptUserTree, deptUserTreeClick);
	// 부서별 사용자 트리 검색 버튼 클릭시 
	attachBtnEvent(searchBtn, actionLink,  'searchAttr');
	
	return outerPanel;
}

//부서별 사용자 트리 클릭시 (사용방법 체크)
function deptUserTreeClick(tree, record, index, eOpts){
	
	var checked = record.data.checked;
	var value ;
	
	// 기존에 True이면 false로 변경 
	if(checked){
		value = false
	}else{
		value = true
	}
	
	// nkia_common.js 참고 
	setTreeCheckAll(record,value);
	//트리 선택 취소
	tree.view.selModel.deselectAll();
	
}

/**
 *  부서 콤보박스 생성 
 */
function deptComboBox(){
	
	var param = {};
	
	var deptCombo_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/workflow/smsGrp/searchDeptComboBox.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}, listeners :{
			changeData : function(p) {
			}
		}
	});
 
	var deptCombo_val = "";
		deptCombo_val = {label:'부서', labelWidth : 50, id: 'DEPT_ID', name:'DEPT_ID', width: 200, attachAll : true};
		deptCombo_val['proxy'] = deptCombo_proxy;
		deptCombo_val['valueField'] = 'CUST_ID';  
		deptCombo_val['displayField'] = 'CUST_NM';
	 
	var deptComboBox = createComboBoxComp(deptCombo_val);
	
	return deptComboBox;
}

