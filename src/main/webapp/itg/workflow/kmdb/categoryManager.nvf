<!doctype html>
<html lang="ko">
<!--
*//**
 * Category관리 페이지
 * @author <a href="mailto:minsoo@nkia.co.kr"> Minsoo Jeon
 * 
 *//*
-->
<head>
#parse("/itg/base/include/page_meta.nvf")
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>

<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<script>
var upCategoryID = null;
var comboCodeID = null;
var categoryType = 'KMDB';
Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	var categoryInsertModeBtn = createBtnComp({label: "#springMessage('btn.system.00006')", id:'categoryInsertModeBtn'});
	var categoryInsertBtn = createBtnComp({label: "#springMessage('btn.common.regist')", id:'categoryInsertBtn', ui:'correct', scale:'medium'});
	var categoryUpdateBtn = createBtnComp({label: "#springMessage('btn.common.modify')", id:'categoryUpdateBtn', ui:'correct', scale:'medium'});
	var categoryDeleteBtn = createBtnComp({label: "#springMessage('btn.common.delete')", id:'categoryDeleteBtn', scale:'medium'});
	var categoryInsertBackBtn = createBtnComp({label: "#springMessage('btn.common.cancel')", id:'categoryInsertBackBtn', scale:'medium'});
	
	attachBtnEvent(categoryInsertModeBtn, actionLink,  'm_modeChange');	
	attachBtnEvent(categoryInsertBtn, actionLink,  'm_insert');	
	attachBtnEvent(categoryUpdateBtn, actionLink,  'm_update');	
	attachBtnEvent(categoryDeleteBtn, actionLink,  'm_delete');	
	attachBtnEvent(categoryInsertBackBtn, actionLink,  'm_back');
	
	var multiRootModeChange = createBtnComp({visible: true, label: "#springMessage('btn.common.add')", id:'multiRootModeChange', margin: getConstValue('WITH_BUTTON_MARGIN')});
	attachBtnEvent(multiRootModeChange, actionLink,  'multiRootModeChange');
	var multiRootAddBtn = createBtnComp({visible: false, label: "#springMessage('btn.common.regist')", id:'multiRootAddBtn', ui:'correct', scale:'medium'});	
	attachBtnEvent(multiRootAddBtn, actionLink,  'multiRootAdd');
	
	var multiCombo = createCodeComboBoxComp({id: 'categoryCombo', name:'categoryCombo', category_id: '0', hideLabel: true, width: 130, category_code: 'Y', url:'/itg/workflow/kmdb/searchCodeDataList.do'});	
					 
	var multiTreeComboPanel = Ext.create('nkia.custom.Panel', {
        id: 'multiTreeComboPanel',
        border: false,
        padding: '2 0 1 5',
		layout: 'hbox',
		items: [multiCombo, multiRootModeChange]
	})
	
	categoryTree = createTabTreeComponent({
    	url : '/itg/workflow/kmdb/searchMultiTree.do',
    	id : 'categoryTree',
        params: null,
    	title : "#springMessage('res.label.workflow.kmdb.00001')",
    	tree_height : 500,
    	tree_widh : 300,
    	dynamic_flag : false,
    	searchable : false,
    	expandAllBtn : true,
    	collapseAllBtn : true,
    	extraToolbar : multiTreeComboPanel,
    	rnode_text : "#springMessage('res.00003')",
    	expand_level: 3
    });
    
    var setCodeOrderGridProp = {	
		context: '${context}',						
		id: 'codeOrderGrid',	
		title : "#springMessage('res.label.workflow.kmdb.00019')",
		resource_prefix: 'grid.workflow.category',				
		url: '$!{context}/itg/workflow/kmdb/searchCodeDataForChild.do',	
		params: null,								
		pagingBar : false,
		autoLoad: true,
		dragPlug: true,
		border: false
	}
	var codeOrderGrid = createGridComp(setCodeOrderGridProp);
   	
   	var setEditCodeProp = {
		id: 'setEditCodeProp',
		title: "#springMessage('res.label.workflow.kmdb.00018')",
		columnSize: 1,
		editOnly: true,
		border: true,
		formBtns: [categoryInsertModeBtn],
		tableProps :[
			 		 {colspan:1, tdHeight: 30, item : createTextFieldComp({label:"#springMessage('res.label.workflow.kmdb.00013')", name:'category_type', width:300})}
					,{colspan:1, tdHeight: 30, item : checkOverlapComp({text:"#springMessage('res.label.workflow.kmdb.00014')", id: 'category_id', btnId: 'codeCheckBtn', name:'category_id', btnText:"#springMessage('res.common.validation')", targetForm:'setEditCodeProp', vtype: '' , chkUrl:'/itg/workflow/kmdb/selectCodeIdComp.do', notNull:true, category_check:'Y', successMsg: "#springMessage('msg.system.result.00035')", failMsg: "#springMessage('msg.system.result.00034')", disabled: true, width: 260, maxLength: 10})}						
			        ,{colspan:1, tdHeight: 30, item : createTextFieldComp({label:"#springMessage('res.label.workflow.kmdb.00015')", name:'category_nm', width:300, notNull:true, maxLength: 200})}
					,{colspan:1, tdHeight: 30, item : createCodeRadioComp({label:"#springMessage('res.00008')", name:'use_yn', code_grp_id: 'YN', columns:2})}				        
					//,{colspan:1, tdHeight: 30, item : createTextFieldComp({label:"#springMessage('res.common.validationYn')", name:'overLapYNcode_id', id:'overLapYNcode_id', value: 'N', width: 200, notNull:false})}	
		],
		hiddenFields:[{name:'code_grp_id'},{name:'up_code_id'}]
	}
   	var systemCodeForm = createEditorFormComp(setEditCodeProp);
	
	// Viewport 설정
	var viewportProperty = {
		west: { 
			minWidth: 210, 
			maxWidth: 350, 
			items : [categoryTree] 
		},
		center: { 
			items : [systemCodeForm, codeOrderGrid], 
			buttons: [categoryUpdateBtn, categoryDeleteBtn, categoryInsertBtn, multiRootAddBtn, categoryInsertBackBtn]  
		}
	}

	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	
	formEditable({id : 'setEditCodeProp', editorFlag : false, excepArray : ['categoryInsertModeBtn'] });
	
	categoryBtnInit();
	
	attachCustomEvent('select', multiCombo, comboHandler);
	attachCustomEvent('afterrender', multiCombo, 
			function(combo, eOpts) {
				var firstRecord = combo.getStore().getAt(0);
				combo.select(firstRecord);
	});
	attachCustomEvent('load', multiCombo, 
			function(combo, eOpts) {
				var firstRecord = combo.getStore().getAt(0);
				combo.select(firstRecord);
	});
	
	attachCustomEvent('itemclick', categoryTree, multiTreeNodeClick);
	
	var comboBox = Ext.getCmp('categoryCombo').getStore();
	var firstRecord = comboBox.getAt(0).data.CODE_ID;

	var tree_store = getGrid("categoryTree").getStore();
	tree_store.proxy.jsonData={category_id: firstRecord, expandLevel:2};
	
});

//Layout Resise Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["setEditCodeProp"], "codeOrderGrid");
}


function comboHandler(combo, selectedRecord, eOpts){
	var code_id = selectedRecord[0].raw.CODE_ID;
	var code_text = selectedRecord[0].raw.CODE_TEXT;
	comboCodeID = code_text;
	categoryType = code_text;
	
	var tree_store = getGrid("categoryTree").getStore();
	tree_store.proxy.jsonData={category_id: code_id, expandLevel:2};
	tree_store.load();
}

function multiTreeNodeClick( tree, record, index, eOpts ) {

	categoryBtnInit();
	
	var code_id = record.raw.node_id;
	var up_code_id = record.raw.up_node_id;
	upCategoryID = up_code_id;
	var grid_store = getGrid("codeOrderGrid").getStore();
	grid_store.proxy.jsonData={up_code_id: code_id};
	grid_store.load();

	var varData = {};
	varData["up_code_id"] = up_code_id;
	varData["code_id"] = code_id;
	
	jq.ajax({ type:"POST"
			, url: "$!{context}/itg/workflow/kmdb/selectCategoryDetailData.do"
			, contentType: "application/json"
			, dataType: "json"
			, data : getArrayToJson(varData)
			, success:function(data){
				if( data.success ){
					var clickRecord = data.resultMap;
					var dataForm = Ext.getCmp('setEditCodeProp');
					dataForm.initData();
					dataForm.setDataForMap(clickRecord);
					
					formEditable({id : 'setEditCodeProp', editorFlag : true, excepArray : ['category_type','category_id','codeCheckBtn'] });
					
					var hideIds = ['categoryInsertBtn'];	
					hideBtns(hideIds);
					var showIds = ['categoryUpdateBtn', 'categoryDeleteBtn', 'categoryInsertBackBtn'];	
					showBtns(showIds);
					
					Ext.getCmp('codeOrderGrid').show();
				}else{
					alert(data.resultMsg);
				}
	        }
	});
}

function categoryBtnInit(){
	var hideIds = ['categoryInsertBtn','categoryInsertBackBtn', 'categoryUpdateBtn', 'categoryDeleteBtn','multiRootAddBtn'];	
	hideBtns(hideIds);	
	//var showIds = ['categoryInsertModeBtn'];	
	//showBtns(showIds);
}

function actionLink(command) {
	var theForm = Ext.getCmp('setEditCodeProp');

	switch(command) {
		case 'm_modeChange' :
			var seletedObject = Ext.getCmp('categoryTree').view.selModel.getSelection();
			if(seletedObject == "" ){
				alert("#springMessage('msg.system.result.00020')");
				return;
			} 
			
			//폼초기화
			theForm.initData();
			formEditable({id : 'setEditCodeProp', editorFlag : true, excepArray : ['category_type', 'categoryInsertModeBtn'] });
			
			//버튼컨트롤
			var hideIds = ['categoryInsertModeBtn', 'categoryUpdateBtn', 'categoryDeleteBtn'];	
			hideBtns(hideIds);	//버튼을 숨긴다.
			var showIds = ['categoryInsertBtn','categoryInsertBackBtn'];	
			showBtns(showIds);
			
			//필요한 데이터값 setValue()
			var up_code_nm = seletedObject[0].raw.text;
			var cust_id = seletedObject[0].raw.node_id;
			
			var comboBox = Ext.getCmp('categoryCombo').getStore();
			var codeText = comboBox.getAt(0).data.CODE_TEXT;
			theForm.setFieldValue('category_type', codeText);
			theForm.setHiddenValue('up_code_id', cust_id);

			Ext.getCmp('codeOrderGrid').searchList();
			
			//Ext.getCmp('code_id').clearInvalid();//동적 readOnly속성
			Ext.getCmp('category_id').setReadOnly(false);
			//Ext.getCmp('codeCheckBtn').setDisabled(false);
		break;
		case 'm_insert' :
			if(theForm.isValid()){
				var editForm = Ext.getCmp('setEditCodeProp');
				var tree = Ext.getCmp('categoryTree');
				var orderGrid = Ext.getCmp('codeOrderGrid');
				
				var idCheckYN = editForm.getFieldValue('overLapYNcode_id');
				if(idCheckYN == "N"  || idCheckYN == ""){
					alert("#springMessage('msg.system.result.00022')");
					return;
				}
				var code_nm = editForm.getFieldValue('code_nm');
				if(code_nm == ""){
					alert("#springMessage('msg.system.result.00023')");
					return;
				}
				
				// 마스크를 띄웁니다.
				setViewPortMaskTrue();
				
				if(!confirm("#springMessage('msg.common.confirm.00001')")){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return false;
				}
				
				theForm.setHiddenValue('code_grp_id', categoryType);
				//폼데이터 가져오기
				var paramMap = theForm.getInputData();
				jq.ajax({ 
					type:"POST"
					, url: '/itg/workflow/kmdb/insertCategoryInfo.do'
					, contentType: "application/json"
					, dataType: "json" 
					, data : getArrayToJson(paramMap)
					, success:function(data){
						if( data.success ){
							theForm.initData();
							categoryAfterAction(data.resultMsg);
						}else{
							alert(data.resultMsg);
						}
						// 마스크를 숨깁니다.
						setViewPortMaskFalse();
					}
				});
			}else{
				alert("#springMessage('msg.common.00011')");
				return false;
			}
		break;
		case 'm_update' :
			if(theForm.isValid()){
				var editForm = Ext.getCmp('setEditCodeProp');
				var tree = Ext.getCmp('categoryTree');
				var orderGrid = Ext.getCmp('codeOrderGrid');
					
				var seletedObject = tree.view.selModel.getSelection();
					
				if(seletedObject == "" ){
					alert("#springMessage('msg.system.result.00020')");
					return;
				} 
					
				var category_nm = editForm.getFieldValue('category_nm');
				if(category_nm == ""){
					alert("#springMessage('msg.system.result.00023')");
					return;
				}
				
				
				// 마스크를 띄웁니다.
				setViewPortMaskTrue();
				
				if(!confirm("#springMessage('msg.common.confirm.00002')")){
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
					return false;
				}
				
				theForm.setHiddenValue('code_grp_id', categoryType);
				var paramMap = theForm.getInputData();
				paramMap['up_category_id'] = upCategoryID;

				editForm.setFieldValue('overLapYNcode_id', 'Y');
				jq.ajax({ type:"POST"
						, url: '/itg/workflow/kmdb/updateCategoryInfo.do'
						, contentType: "application/json"
						, dataType: "json" 
						, async: false
						, data : getArrayToJson(paramMap)
						, success:function(data){
							if( data.success ){
								//순서변경
						        var orderGrid = Ext.getCmp('codeOrderGrid').getStore();
								var orderInsertArray = [];
								var orderInsertMap = {};
		
								for(var i=0; i<orderGrid.data.items.length;i++){
									orderInsertMap['up_category_id'] = orderGrid.data.items[i].data.UP_CATEGORY_ID;
									
									orderInsertMap['display_no'] = i+1;
									
									jq.ajax({ type:"POST"
								    		, url:'$!{context}/itg/workflow/kmdb/updateCodeOrder.do'
								    		, contentType: "application/json"
								    		, dataType: "json"
								    		, async: false
								    		, data : getArrayToJson(orderInsertMap)
								    		, success:function(){
								    			if( data.success ){
								    				//alert(data.resultMsg);
								    			}
								    		}
									});
								}
							}else{
								alert(data.resultMsg);
			   				}
				   			var dataForm = Ext.getCmp('setEditCodeProp');
							dataForm.initData();
							getGrid("codeOrderGrid").getStore().removeAll();
				   			// 마스크를 숨깁니다.
							setViewPortMaskFalse();
						}
				});
				alert("#springMessage('msg.common.result.00002')");
					
				var comboBox = Ext.getCmp('categoryCombo').getStore();
				comboBox.proxy.jsonData={category_id : '0', category_code: 'Y'};
				comboBox.load();
										
				var firstRecord = comboBox.getAt(0).data.CODE_ID;
					
				var path = tree.getSelectionModel().getSelection()[0].getPath();
					
				var tree_store = tree.getStore();
				tree_store.proxy.jsonData={category_id: firstRecord, expandLevel:2};
				tree_store.load();
				tree.expandPath(path);
					
				tree.view.selModel.deselectAll();//selectTree초기화			
				editForm.setFieldValue('overLapYNcode_id', 'N');
				
				formEditable({id : 'setEditCodeProp', editorFlag : false, excepArray : [] });
				categoryBtnInit();
				//Ext.getCmp('codeCheckBtn').disable();
			}else{
				alert("#springMessage('msg.common.00011')");
				return false;
			}
		break;
		case 'm_delete' :
				
			var seletedObject = Ext.getCmp('categoryTree').view.selModel.getSelection();
				
			if(seletedObject == "" ){
				alert("#springMessage('msg.system.result.00020')");
				return;
			} 
			// 마스크를 띄웁니다.
			setViewPortMaskTrue();
			
			if(!confirm("#springMessage('msg.common.confirm.00003')")){
				// 마스크를 숨깁니다.
				setViewPortMaskFalse();
				return false;
			}
			paramMap = {};
			paramMap['category_id'] = theForm.getFieldValue('category_id');
			paramMap['up_category_id'] = theForm.getHiddenValue('up_code_id');
				
			jq.ajax({ 
				type:"POST"
				, url: '/itg/workflow/kmdb/deleteCategoryInfo.do'
				, contentType: "application/json"
				, dataType: "json" 
				, data : getArrayToJson(paramMap)
				, success:function(data){
					if( data.success ){	
						theForm.initData();
						theForm.getForm().reset();
						for(var i = 0; i < theForm.hiddenFields.length; i++){
							theForm.hiddenFields[i].value = "";
						}
						categoryAfterAction(data.resultMsg);
						
					}else{
						alert(data.resultMsg);
					}
					
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
				}
			});
		break;
		case 'm_back' :
			Ext.getCmp('codeOrderGrid').show();
			theForm.initData();
			
			var code_grp_id = Ext.getCmp('setEditCodeProp').getHiddenValue('code_grp_id');

			var getOrderGrid = getGrid("codeOrderGrid").getStore();
			getOrderGrid.proxy.jsonData={code_grp_id: code_grp_id};
			getOrderGrid.load();
			
			//Ext.getCmp('codeCheckBtn').disable();
			
			Ext.getCmp('categoryTree').view.selModel.deselectAll();//selectTree초기화
			//Ext.getCmp('codeCheckBtn').disable();
			formEditable({id : 'setEditCodeProp', editorFlag : false, excepArray : ['categoryInsertModeBtn'] });
			categoryBtnInit();
		break;
		case 'multiRootModeChange' :
		
			//폼초기화
			theForm.initData();
			//필요한 데이터값 setValue()
			theForm.setFieldValue('category_type', 'Category');
			theForm.setHiddenValue('up_code_id', '0');
			formEditable({id : 'setEditCodeProp', editorFlag : true, excepArray : ['category_type'] });
			
			categoryBtnInit();
			//버튼컨트롤
			var showIds = ['multiRootAddBtn', 'categoryInsertBackBtn'];	
			showBtns(showIds);
			
			Ext.getCmp('codeOrderGrid').hide();
			
			//Ext.getCmp('code_id').clearInvalid();
			Ext.getCmp('category_id').setReadOnly(false);
			//Ext.getCmp('codeCheckBtn').setDisabled(false);
			
		break;
		case 'multiRootAdd' :
			var idCheckYN = Ext.getCmp('setEditCodeProp').getFieldValue('overLapYNcode_id');
			if(idCheckYN == "N"){
				alert("#springMessage('msg.system.result.00022')");
				return;
			}
			var code_nm = Ext.getCmp('setEditCodeProp').getFieldValue('code_nm');
			if(code_nm == ""){
				alert("#springMessage('msg.system.result.00023')");
				return;
			}
			
			// 마스크를 띄웁니다.
			setViewPortMaskTrue();
			
			if(!confirm("#springMessage('msg.common.confirm.00001')")){
				// 마스크를 숨깁니다.
				setViewPortMaskFalse();
				return false;
			}
			theForm.setHiddenValue('code_grp_id', theForm.getFieldValue('category_nm'));
			
			//폼데이터 가져오기
			var paramMap = theForm.getInputData();
			jq.ajax({ 
				type:"POST"
				, url: '/itg/workflow/kmdb/insertCategoryInfo.do'
				, contentType: "application/json"
				, dataType: "json" 
				, data : getArrayToJson(paramMap)
				, success:function(data){
					if( data.success ){
						theForm.initData();
	
						var comboBox = Ext.getCmp('categoryCombo').getStore();
						comboBox.proxy.jsonData={code_grp_id : '0', category_code: 'Y'};
						comboBox.load();
									
						var firstRecord = comboBox.getAt(0);
						Ext.getCmp('categoryCombo').select(firstRecord);
						
						var firstRecord = comboBox.getAt(0).data.CODE_ID;
	
						var tree_store = getGrid("categoryTree").getStore();
						tree_store.proxy.jsonData={category_id: firstRecord, expandLevel:2};
						tree_store.load();
											
						Ext.getCmp('codeOrderGrid').show();
				        
				        var getOrderGrid = getGrid("codeOrderGrid").getStore();
						getOrderGrid.proxy.jsonData={};
						getOrderGrid.load();
							
						Ext.getCmp('categoryTree').view.selModel.deselectAll();//selectTree초기화
				       
						alert(data.resultMsg);
						
						formEditable({id : 'setEditCodeProp', editorFlag : false, excepArray : [] });
				        categoryBtnInit();
					}else{
						alert(data.resultMsg);
					}
					
					// 마스크를 숨깁니다.
					setViewPortMaskFalse();
				}
		});
		break;
	}
}

function categoryAfterAction(msg) {
	var comboBox = Ext.getCmp('categoryCombo').getStore();
	comboBox.proxy.jsonData={code_grp_id : comboCodeID, category_code: 'Y'};
	comboBox.load();
						
	var firstRecord = comboBox.getAt(0).data.CODE_ID;
	
	var tree_store = getGrid("categoryTree").getStore();
	tree_store.proxy.jsonData={category_id: firstRecord, expandLevel:2};
	tree_store.load();
	var getOrderGrid = getGrid("codeOrderGrid").getStore();
	getOrderGrid.proxy.jsonData={};
	getOrderGrid.load();
					
	Ext.getCmp('categoryTree').view.selModel.deselectAll();//selectTree초기화
	//alert("#springMessage('msg.common.result.00003')");
	alert(msg);
	formEditable({id : 'setEditCodeProp', editorFlag : false, excepArray : [] });
	categoryBtnInit();
	//Ext.getCmp('codeCheckBtn').disable();
}
</script>
</head>
<body>
<div id="loading-mask"></div>
<div id="loading">
  <div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>
</body>
</html>
