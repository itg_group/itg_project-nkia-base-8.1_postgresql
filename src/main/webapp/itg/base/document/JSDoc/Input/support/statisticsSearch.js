/**
 * 요청부대 콤보박스 생성하기 
 * @param id - 콤보박스의 ID
 * @param name - 콤보박스의 NAME
 * @param addMap - 전체 또는 선택을 콤보박스에 추가로 넣어줍니다. (attachAll : true OR attachChoice : true) 
 * @returns
 */
function createContractCombo(id, name, addMap){
	
	var param = {};
	var combobox;
	
	var that = {};
	var i = 0;
	for( var prop in addMap){
		that[prop] = addMap[prop];
		i++;
	}
	
	// 콤보박스에 추가될 내용 선택 (전체 / 선택)
	var attachAll = that['attachAll'] ? that['attachAll'] : false;
	var attachChoice = that['attachChoice'] ? that['attachChoice'] : false;
	
	jq.ajax({ type:"POST"
		, url: '/itg/slms/common/searchCustCombo.do' 
		, contentType: "application/json"
		, dataType: "json"
		, async: false
		, data : getArrayToJson(param)
		, success:function(data){
			
			var rows = null;
			var length = data.gridVO.rows.length;
			var addData = {};
			
			// Rows 널값 추가 
			if(0 < length){
				rows = data.gridVO.rows;
			}else{
				rows = [];
			}
			
			// 콤보박스 스토어 생성 
			var store = Ext.create('Ext.data.Store', {
			    fields: ['CODE_ID', 'CODE_TEXT'],
			    data : rows
			});
			
			// 콤보박스에 추가될 내용을 정해줍니다.
			if(1 < i){
				
				alert("선택 / 전체 둘중에 하나만 넣어주세요! statisticsSearch.js 체크 !");
				
			}else{
				if(attachAll){
					addData["CODE_ID"] = "";
					addData["CODE_TEXT"] = getConstText({m_key:'COMBO_ALL'});
					store.insert(0, addData);
				}else if(attachChoice){
					addData["CODE_ID"] = "";
					addData["CODE_TEXT"] = getConstText({m_key:'COMBO_CHOICE'});
					store.insert(0, addData);
				}
			}
			
			// 실제 콤보박스 생성 부분 
			combobox = Ext.create('Ext.form.field.ComboBox', {
			    fieldLabel: getConstText({isArgs: true, m_key: 'res.00032'})
			    , store: store
			    , queryMode: 'local'
			    , displayField: 'CODE_TEXT'
			    , valueField: 'CODE_ID'
			    , width : 300
			    , id: id
			    , name: name
			    , labelAlign: 'right'
		    	, editable : false
		    	, labelSeparator: ''
	    		, emptyText : getConstText({isArgs: true, m_key: 'msg.common.00001'})
			});
			
			// 콤보박스 첫번째 로우 셀렉트
			var record = combobox.getStore().getAt(0);
			combobox.select(record);
		}
	});

	return combobox;
}

function vaildCheck(param, dateType){
	
	var checkFlag = "Y";
	
	if(dateType == 'singleDate'){
		
		if(param['search_day'] == "" || param['search_day'] == null){
			showMessage(getConstText({ isArgs: true, m_key: 'msg.support.statistics.00003' }));
			checkFlag = "N";
		}
		
	}else{
		//@@정중훈 2014.02.04  1년 체크 로직 변경 		
		var startDate = param['search_startDate']; 
		var endDate = param['search_endDate']; 
		var endYear = endDate.substring(0,4);
		var endMonth = endDate.substring(4,6);
		var maxStartDate =(endYear-1) + endMonth;
		
		if(param['search_startDate'] == "" || param['search_endDate'] == "" || 
		   param['search_startDate'] == null || param['search_endDate'] == null){
			showMessage(getConstText({ isArgs: true, m_key: 'msg.support.statistics.00003' }));
			checkFlag = "N";
		}
	
//		if((param['search_endDate'] - param['search_startDate']) > 12){
		if(maxStartDate >= startDate){
			
			showMessage(getConstText({ isArgs: true, m_key: 'msg.support.statistics.00001' }));
			checkFlag = "N";
		}
		
		if(param['search_endDate'] < param['search_startDate']){
			showMessage(getConstText({ isArgs: true, m_key: 'msg.slms.monitoring.endDateCheck' }));
			checkFlag = "N";
		}
	}
	return checkFlag;
}

