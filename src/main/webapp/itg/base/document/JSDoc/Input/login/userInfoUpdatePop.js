var popUp_comp={};
var result = null;
var isFirstOpen = false;
//@@20130711 전민수 START
//createOutUserSelectPopComp 추가 : 대무자 
//createSearchDateFieldComp  추가 : 대무기간
//userInfoUpdateDetail       변경 : 부서명(cust_nm) colspan :1 -> colspan: 2 로 변경

//개인정보 수정 팝업 생성
function createUserInfoUpdatePopup(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var url = that["url"];
	var paramMap = that["paramMap"];
	
	if(null == popUp_comp[that['id']]){
		
		isFirstOpen = true;
		
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				result = data.resultMap;
			}
		});
		
		// 버튼 묶음
		var userInfoUpdateBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.modify' }), id:'userInfoUpdateBtn', ui:'correct', scale:'medium'});
		var userInfoCancelBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.cancel' }), id:'userInfoCancelBtn', scale:'medium'});
		
		// 비밀번호 초기화 버튼
		var userPwInitBtn = createBtnComp({id:'userPwInitBtn', width:30, icon:getConstValue('CONTEXT')+'/itg/base/images/ext-js/common/icons/time.png'});
		
		//패스워드 입력방법 텍스트
		var passInputText = {
				xtype: 'component',
				padding : '0 0 10 105',
				html: '<span style="color: blue; font-weight: bold;" id="passInputTxt"><b>'+getConstText({ isArgs: true, m_key: 'msg.user.reg.00006'})+'</b></span>',
				listeners: {
					afterrender : function() {
						jq("#passInputTxt").hide();
					}
				}
		};
		
		//패스워드 관련 오브젝트는 핸들링 편하게 변수 따로 빼놓음 - 2014.08.02.정정윤
		//비밀번호 수정 체크박스
		var check_chg_pw = createSingleCheckBoxFieldComp({fieldLabel:'비밀번호 변경', id:'check_chg_pw', name:'check_chg_pw', checked : false, inputValue : 'Y', defaultValue: 'N'});
		
		//현재 비밀번호
		var cur_pw = createTextFieldComp({label:'현재 비밀번호', name:'cur_pw', id :'cur_pw' , inputType: 'password', notNull: false, vtype: 'password', maxLength: 30});
		
		//현재 비밀번호 확인용
		//var cur_pw_check = createTextFieldComp({label:'현재 비밀번호 확인', name:'cur_pw_check', id :'cur_pw_check' , inputType: 'password', notNull: false, vtype: 'password', maxLength: 30});
		
		//변경 비밀번호
		var pw = createTextFieldComp({label:'변경 비밀번호', name:'pw', id :'pw' , inputType: 'password', notNull: false, vtype: 'password', maxLength: 30});
		
		//변경 비밀번호 확인
		var pwCheck = createTextFieldComp({label:'비밀번호 확인', name:'pwCheck', id:'pwCheck' , inputType: 'password', notNull: false, vtype: 'password', maxLength: 30});
		
		//군전화 구분 콤보박스 - 2014.11.17.정정윤
		var combo_tel_type = 
			createCodeComboBoxComp({label : getConstText({ isArgs: true, m_key: 'res.common.label.tel' }), id:"combo_tel_type", name : "combo_tel_type", width : 190, "notNull" : true, "attachChoice" : false, code_grp_id : "TEL_TYPE", padding:"0 15 0 0"});
		
		var tel_no = 
			createPhoneFieldComp({name:'tel_no', id:'tel_no', notNull: true, firstNumberNotNull: false});
		
		var tel_field = 
			{
				xtype: 'fieldcontainer',
				width: 500,
			    combineErrors: true,
			    layout:'hbox',
			    items: [
			    	combo_tel_type,tel_no
			    ]
			} 
		
		// 수정 에디터 폼
		var userInfoUpdateDetail = {
				id: 'userInfoUpdateDetail',
				title : getConstText({ isArgs: true, m_key: 'res.title.login.userInfoUpdateDetail' }),
				columnSize : 2,
				editOnly : true,
				border: true,
				tableProps :[{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00001'}), name:'cust_nm', id:'cust_nm', width: 320})
				},{
					colspan : 1, tdHeight : 30,
					//item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00009'}), name:'dept_nm', id:'dept_nm', notNull : true, maxLength: 100, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00001'})})
					item : createDeptSelectPopComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00009'}), name:'dept_nm', id:'dept_nm_pop', notNull : true, maxLength: 100, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00001'})})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00002'}), name:'user_nm', id:'user_nm', maxLength: 100})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00003'}), name:'user_id', id:'user_id', maxLength: 40})
				},{
					colspan : 2, tdHeight : 30,
					//item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00006'}), name:'position' , id:'position', maxLength: 100, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00003'})})
					item : createClassPopupComp({label: getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00006'}), targetName:'position_nm', targetHiddenName:'position', notNull:true })
				},{
					colspan : 2, tdHeight : 30,
					item : tel_field
				}
				/*
				,{
					colspan : 1, tdHeight : 30,
					item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00007'}), name:'tel_no', id:'tel_no', notNull: true, firstNumberNotNull: false})
				}
				*/
				,{
					colspan : 1, tdHeight : 30,
					item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00008'}), name:'hp_no', id:'hp_no', notNull: true, firstNumberNotNull: true, firstFieldLength:3})
				},{
					colspan : 1, tdHeight : 30,
					item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00009'}), name:'fax_no', id:'fax_no'})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00010'}), name:'email', id:'email', notNull: true, maxLength: 40, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00004'})})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00011'}), name:'use_yn', id:'use_yn'})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00012'}), name:'cust_id', id:'cust_id'})
				},{
					colspan:1, tdHeight: 30, 
					item : createOutUserSelectPopComp({id:'userSelectPop',  popUpText: getConstText({ isArgs: true, m_key: 'res.title.system.user.tempuser'}), label:getConstText({ isArgs: true, m_key: 'res.common.label.tempusernm'}), name:'absent_nm', hiddenName:'absent_id', targetForm:'setEditorFormProp', gridId:'userSelectGrid', userId:'user_id', btnId: 'absent_nmBtn', isCustId:true, thatEditFormId:'userInfoUpdateDetail'})
				},{ 
					colspan:2, tdHeight: 30, 
					item : createSearchDateFieldComp({label:getConstText({ isArgs: true, m_key: 'res.common.label.tempuserdate'}), id : 'absent' , name:'absent', dateType : 'D', format: 'Y-m-d'})
				},{
					colspan:2, tdHeight: 100, 
					item : check_chg_pw
				},{
					colspan : 1, tdHeight : 30,
					item : cur_pw
				//},{
				//	colspan : 1, tdHeight : 30,
				//	item : cur_pw_check
				},{
					colspan:1, tdHeight: 30, 
					item : createUnionFieldContainer({label : "비밀번호 초기화" ,id:"userPwInitBtn_container" ,items : [userPwInitBtn]})
				},{
					colspan : 1, tdHeight : 30,
					item : pw
				},{
					colspan : 1, tdHeight : 30,
					item : pwCheck
				},{
					colspan:2, tdHeight: 30, 
					item : passInputText
				}]
			};
		
		var userInfoUpdateForm = createEditorFormComp(userInfoUpdateDetail);
		
		// 숨길 필드들 
		Ext.getCmp('use_yn').hide();
		Ext.getCmp('cust_id').hide();
		//Ext.getCmp('cur_pw_check').hide();
		
		//필드값 setValue
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		theForm.setDataForMap(result);
		//필드값 setValue
		
		// 취소버튼을 눌렀을 경우
		attachBtnEvent(userInfoCancelBtn, actionLink,  'cancel');
		// 수정버튼을 눌렀을 경우
		attachBtnEvent(userInfoUpdateBtn, actionLink,  'update');
		// 비밀번호 초기화 버튼을 눌렀을 경우
		attachBtnEvent(userPwInitBtn, actionLink,  'pwInit');
		
		userInfoUpdatePopup = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: getConstText({ isArgs: true, m_key: 'res.title.login.userInfoUpdatePopup' }),  
			width: 750,  
			autoDestroy: false,
			resizable:false, 
			//bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [userInfoUpdateBtn, userInfoCancelBtn],
			bodyStyle: that['bodyStyle'],
			items: [userInfoUpdateForm, userInfoCancelBtn],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null;
	 			},
	 			hide:function(p) {
	 			},
	 			afterrender: function(){
	 				// ReadOnly 컬럼
	 				Ext.getCmp('cust_nm').setReadOnly(true);
	 				Ext.getCmp('user_nm').setReadOnly(true);
	 				Ext.getCmp('user_id').setReadOnly(true);
	 				
	 				formEditable({id : 'userInfoUpdateDetail', editorFlag : true, excepArray :['']});
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = userInfoUpdatePopup;
		
	}else {
		
		isFirstOpen = false;
		
		// 데이터 새로 조회합니다.
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				theForm.initData();
				var newResult = data.resultMap;
				theForm.setDataForMap(newResult);
			}
		});
		
		userInfoUpdatePopup = popUp_comp[that['id']];
	}
	
	// 히든 필드에 이전 비밀번호 세팅(확인용) - 2014.08.02. 정정윤
	//Ext.getCmp('cur_pw_check').setValue(Ext.getCmp('pw').getValue());
	
	//첫 로드시 비밀번호 관련 컴포넌트들 사용불가로.. - 2014.08.02. 정정윤
	setPwObjsDisabled(true);
	
	if(isFirstOpen) {
		//비밀번호 변경 체크박스 클릭 이벤트 - 2014.08.02. 정정윤
		check_chg_pw.on('change', function() {
			var checked = check_chg_pw.getValue();
			if(checked) {
				setPwObjsDisabled(false);
				jq("#passInputTxt").show();
			} else {
				setPwObjsDisabled(true);
				jq("#passInputTxt").hide();
			}
		});
	}
	
	// 전화번호필드가 2개로 구분된다면 군전화로 인식함
	var telCombo = Ext.getCmp("combo_tel_type");
	var firstHomeTelNo = Ext.getCmp("tel_no");
	var formData = Ext.getCmp('userInfoUpdateDetail').getInputData();
	var telNoArr = formData['tel_no'].split("-");
	
	if(telNoArr.length < 3) {
		telCombo.setValue("MP");
		firstHomeTelNo.setReadOnly(true);
	} else {
		if(telNoArr[0] == "" || telNoArr[0] == null) {
			telCombo.setValue("MP");
			firstHomeTelNo.setReadOnly(true);
		} else {
			telCombo.setValue("HP");
			firstHomeTelNo.setReadOnly(false);
		}
	}
	
	// 전화구분 콤보박스 체인지 이벤트
	telCombo.on('change', function() {
		var val = telCombo.getValue();
		if(val == "MP") {
			firstHomeTelNo.setValue("");
			firstHomeTelNo.setReadOnly(true);
			if( firstHomeTelNo.inputEl && firstHomeTelNo.inputEl.dom ){
				addClass(firstHomeTelNo.inputEl.dom, "field-readonly");
			}
		} else {
			firstHomeTelNo.setReadOnly(false);
			if( firstHomeTelNo.inputEl && firstHomeTelNo.inputEl.dom ){
				removeClass(firstHomeTelNo.inputEl.dom, "field-readonly");
			}
		}
	});
	
	return userInfoUpdatePopup;
}
//@@20130711 전민수 
//END

function actionLink(command) {
	var theForm = Ext.getCmp('userInfoUpdateDetail');
	
	switch(command) {
		
		case 'update' :
			
			var paramMap = theForm.getInputData();
			paramMap['userId'] = paramMap['user_id'];
			
			var prePwIsNotEq = "true";
			
			jq.ajax({ type:"POST"
				, url:'/itg/certification/searchPreUserPW.do'
				, contentType: "application/json"
				, dataType: "json"
				, async: false
				, data : getArrayToJson(paramMap)
				, success:function(data){
					prePwIsNotEq = data.resultMap.PWFLAG;
				}
			});
			
			if(paramMap['check_chg_pw'] == "Y") {
				
				if(paramMap['cur_pw'] == "") {
					alert("현재 비밀번호를 입력하세요");
					Ext.getCmp("cur_pw").focus(true,10);
					return false;
					
				} else if(prePwIsNotEq=="true") {
					alert("현재 비밀번호가 일치하지 않습니다. 다시 입력해주세요.");
					Ext.getCmp("cur_pw").focus(true,10);
					return false;
					
				} else if(paramMap['pw'] == "") {
					alert("변경하실 비밀번호를 입력하세요");
					Ext.getCmp("pw").focus(true,10);
					return false;
					
				} else if(paramMap['pwCheck'] == "") {
					alert("변경하실 비밀번호 확인을 입력하세요");
					Ext.getCmp("pwCheck").focus(true,10);
					return false;
				}
			
				var pwResult = checkPwResult(paramMap['cur_pw'], paramMap['pw'], paramMap['pwCheck']);
				if(!pwResult) {
					alert("암호는 9~12자리 이내로 영문과 숫자 그리고 특수문자를 조합하여 입력해 주세요");
					return false;
				}
				
				if(paramMap['pw'] != paramMap['pwCheck']) {
					alert("변경 비밀번호가 일치하지 않습니다");
	            	Ext.getCmp("pw").focus(true,10);
					return false;
				}
			}
			
			if(theForm.isValid()){
				
				var insertNameValue = theForm.getInputData();
				
				//비밀번호 변경 체크박스를 눌렀을 때만 비밀번호 검사 로직 수행 - 2014.08.02. 정정윤
				if(paramMap['check_chg_pw'] != "Y") {
					
					paramMap['pw'] = "";
					// 서버단에서 pw가 비어있으면 패스워드를 업데이트 하지 않는 로직 추가함 - 2014.09.22. 정정윤
				}
				
				// 전화번호 제일 앞에 대쉬(-)가 붙엇을 경우 제거하여 parameter 세팅 - 2014.10.05. 정정윤
				if(insertNameValue['tel_no'].substr(0,1) == "-") {
					
					insertNameValue['tel_no'] = insertNameValue['tel_no'].replace("-","");
					
					if(paramMap['tel_no'].substr(0,1) == "-") {
						paramMap['tel_no'] = paramMap['tel_no'].replace("-","");
					}
				}
	            
				paramMap["insertNameValue"] = insertNameValue;
				
				if(!confirm("수정하시겠습니까?")) {
					return false;
				}
				
				jq.ajax({ type:"POST"
					, url:'/itg/certification/updateUserInfo.do'
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(paramMap)
					, success:function(data){
						if(data.success){
							theForm.initData();
							result = data.resultMap;
							//theForm.setDataForMap(result);
							alert(data.resultMsg);
							userInfoUpdatePopup.hide();
						}else{
							alert(data.resultMsg);
						}
					}
				});
				
			}else{
				alert(getConstText({ isArgs: true, m_key: 'msg.common.00011' }));
			}
			
		break;
		
		case 'cancel' :
			
			theForm.initData();
			theForm.setDataForMap(result);
			userInfoUpdatePopup.hide();
			
		break;
		
		case 'pwInit' :
			
			if(!confirm("확인버튼을 누르면 지금 즉시 비밀번호가 초기화 됩니다.\n계속 하시겠습니까?")) {
				return false;
			}
			
			var inputMap = theForm.getInputData();
			var paramMap = {};
			
			var user_id_sub_str = inputMap['user_id'].substr(0,4);
			var hp_no_sub_str = inputMap['hp_no_third'];
			
			paramMap['user_id'] = inputMap['user_id'];
			paramMap['initPw'] = ( user_id_sub_str + hp_no_sub_str + "!" );
			paramMap['email'] = inputMap['email'];
			
			jq.ajax({ type:"POST"
				, url:'/itg/certification/updatePwInit.do'
				, contentType: "application/json"
				, dataType: "json"
				, async: false
				, data : getArrayToJson(paramMap)
				, success:function(data){
					var alertString = "비밀번호가 초기화 되었습니다."
						+ "\n"
						+ "\n초기화 된 비밀번호는"
						+ "\n[ ID 앞4자리 + 핸드폰 뒤4자리 + !(느낌표) ] 입니다."
						//+ "\n"
						//+ "\nex) nkia7274!"
						;
					alert(alertString);
				}
			});
			
		break;
	}
	
}

// 비밀번호 변경 관련 오브젝트 사용가능/불가능 핸들링 - 2014.08.02. 정정윤
function setPwObjsDisabled(flag) {
	
	//Ext.getCmp('check_chg_pw').setValue('N');
	
	Ext.getCmp('cur_pw').setValue('');
	Ext.getCmp('pw').setValue('');
	Ext.getCmp('pwCheck').setValue('');
	
	Ext.getCmp('cur_pw').setDisabled(flag);
	Ext.getCmp('pw').setDisabled(flag);
	Ext.getCmp('pwCheck').setDisabled(flag);
	//Ext.getCmp('userPwInitBtn').setDisabled(flag);
	Ext.getCmp('userPwInitBtn_container').setDisabled(flag);
}

function checkPwResult(cur_pw, chg_pw, chg_pw_chk) {
	
	var valid = false;
	var pw_arr = new Array();
	pw_arr.push(cur_pw);
	pw_arr.push(chg_pw);
	pw_arr.push(chg_pw_chk);
	
	var passWordCheckVal = /^(?=.*[a-zA-Z])(?=.*[~!@#$%^&*()+=-])(?=.*[0-9]).*$/;
	
	for(var i=0; i<3; i++) {
		if(passWordCheckVal.test(pw_arr[i])){
			// 문자열 길이 체크 로직
			var str = pw_arr[i];
			var strLength = str.length;
			if(9 <= strLength){
				if(strLength <= 12){
					valid = true;
				} else {
					valid = false;
					break;
				}
			} else {
				valid = false;
				break;
			}
		} else {
			valid = false;
			break;
		}
	}
	
	return valid;
}
