var popUp_comp={};


/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */
function createAmMngInfo(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputAmMngPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var theForm = Ext.getCmp('editor_form');
	
	var seletedObject = Ext.getCmp('amClassTree').view.selModel.getSelection();
	var class_id = seletedObject[0].raw.node_id; 
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
				id: 'editor_form',
				title: getConstText({ isArgs: true, m_key: 'res.label.tableMng.00015' }),
				editOnly: true,
				border: true,
				columnSize: 2,
				//formBtns: [insertBtn],	//Form 상단에 들어가는 버튼을 세팅
				tableProps : [
				             //row1
							 {colspan:1, tdHeight: 25, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00016' }), name:'entity_nm', notNull:true})}
							,{colspan:1, tdHeight: 25, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00011' }), name:'table_nm', class_id:class_id, url:'/itg/itam/amdb/selectTableList.do', notNull:true})}
							//row2
							,{colspan:1, tdHeight: 25, item : createCodeRadioComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00017' }), name:'use_yn', code_grp_id: 'YN', columns:2})}
							,{colspan:1, tdHeight: 25, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00018' }), name:'sort_basis', code_grp_id: 'SORT_BASIS', columns:2})}
							//row3
							,{colspan:1, tdHeight: 25, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00019' }), name:'tab_type', code_grp_id: 'TAB_TYPE', columns:2})}
							,{colspan:1, tdHeight: 25, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00020' }), name:'tab_group'})}
							//row4
							,{colspan:1, tdHeight: 25, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00021' }), name:'window_size'})}
							,{colspan:1, tdHeight: 25, item : createCodeRadioComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00022' }), name:'readonly_yn', code_grp_id: 'YN', columns:2})}
							//row5
							,{colspan:2, tdHeight: 40, item : createTextAreaFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00023' }), name:'action', width: 600})}
				             ]
				
		}
		
		var eidtorFormPanel = createEditorFormComp(setEditorFormProp);
		var insertBtn = new Ext.button.Button({
	        text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
	        handler: function() {
	        	
	    		var seletedObject = Ext.getCmp('amClassTree').view.selModel.getSelection();
	    		var class_id = seletedObject[0].raw.node_id;
	    		
	        	var entity_nm = Ext.getCmp('editor_form').getFieldValue('entity_nm');
	        	var table_nm = Ext.getCmp('editor_form').getFieldValue('table_nm');
	        	var use_yn = Ext.getCmp('editor_form').getInputData().use_yn;
	        	var sort_basis = Ext.getCmp('editor_form').getInputData().sort_basis;
	        	var tab_type = Ext.getCmp('editor_form').getInputData().tab_type;
	        	var tab_group = Ext.getCmp('editor_form').getInputData().tab_group;
	        	var window_size = Ext.getCmp('editor_form').getInputData().window_size;
	        	var action = Ext.getCmp('editor_form').getInputData().action;
	        	var readonly_yn = Ext.getCmp('editor_form').getInputData().readonly_yn;
	        	
	        	if(entity_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00018' }));
	        		return;
	        	}else if(table_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00019' }));
	        		return;
	        	}else{
	        		
	        		getGrid("entityGrid").getStore().add({"RNUM":'NEW'
	        			                                  ,"ENTITY_ID":'NEW'
	        			                                  ,"ENTITY_NM":entity_nm
	        			                                  ,"TABLE_NM":table_nm
	        			                                  ,"USE_YN":use_yn
	        			                                  ,"SORT_BASIS":sort_basis
	        			                                  ,"TAB_TYPE":tab_type
	        			                                  ,"TAB_GROUP":tab_group
	        			                                  ,"WINDOW_SIZE":window_size
	        			                                  ,"READONLY_YN":readonly_yn
	        			                                  ,"ACTION":action
	        			                                  ,"SORT_ORDER":'NEW'
	        			                                  ,"CLASS_ID":class_id});
	    			//버튼처리
	        		var hideIds = ['mngUpdateBtn', 'mngDeleteBtn','mngInsPopBtn','mngUpdPopBtn'];	 
	    			hideBtns(hideIds);	//버튼을 숨긴다.
	    			var showIds = ['mngInsertBtn','mngCnacleBtn'];	
	    			showBtns(showIds);
	    			//관리항목 리스트 셋팅
	    			var notGrid = getGrid("notContainGrid").getStore();
	    			notGrid.proxy.jsonData={table_nm: table_nm};
	    			notGrid.load();
	    			
	    			var inGrid = getGrid("inContainGrid").getStore();
	    			inGrid.proxy.jsonData={table_nm: table_nm};
	    			inGrid.load();
	    			
	        		inputAmMngPop.hide();
	        	}
	        }
		});
		
		inputAmMngPop = new Ext.Window({

			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn],
			bodyStyle: that['bodyStyle'],
			items: [eidtorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = inputAmMngPop;
		
	} else {
		inputAmMngPop = popUp_comp[that['id']];
		theForm.initData();
	}
	
	return inputAmMngPop;
}

function updateAmMngInfo(setPopUpProp){
	var storeParam = {};
	var that = this;
	var updateAmMngPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var theForm = Ext.getCmp('update_form');
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
				id: 'update_form',
				title: getConstText({ isArgs: true, m_key: 'res.label.tableMng.00015' }),
				editOnly: true,
				border: true,
				columnSize: 2,
				tableProps : [
				             //row1
							 {colspan:1, tdHeight: 25, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00016' }), name:'entity_nm', notNull:true})}
							,{colspan:1, tdHeight: 25, item : createDisplayTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00001' }), name:'table_nm'})}
							//row2
							,{colspan:1, tdHeight: 25, item : createCodeRadioComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00015' }), name:'use_yn', code_grp_id: 'YN', columns:2})}
							,{colspan:1, tdHeight: 25, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00018' }), name:'sort_basis', code_grp_id: 'SORT_BASIS', columns:2})}
							//row3
							,{colspan:1, tdHeight: 25, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00019' }), name:'tab_type', code_grp_id: 'TAB_TYPE', columns:2})}
							,{colspan:1, tdHeight: 25, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00020' }), name:'tab_group'})}
							//row4
							,{colspan:1, tdHeight: 25, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00021' }), name:'window_size'})}
							,{colspan:1, tdHeight: 25, item : createCodeRadioComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00022' }), name:'readonly_yn', code_grp_id: 'YN', columns:2})}
							//row5
							,{colspan:2, tdHeight: 40, item : createTextAreaFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00023' }), name:'action', width: 600})}
				             ],
				hiddenFields:[ {name:'rnum'},{name:'entity_id'},{name:'sort_order'}]
				
		}
		
		var eidtorFormPanel = createEditorFormComp(setEditorFormProp);
		
		var seletedObject = Ext.getCmp('entityGrid').view.selModel.getSelection();
		var updateForm = Ext.getCmp('update_form');
		
		var clickRecord = seletedObject[0].raw;
		updateForm.setDataForMap(clickRecord);
		
		var insertBtn = new Ext.button.Button({
	        text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
	        handler: function() {
	        	
	        	var seletedObject = Ext.getCmp('amClassTree').view.selModel.getSelection();
	    		var class_id = seletedObject[0].raw.node_id;
	    		
	    		var entity_id = Ext.getCmp('update_form').getInputData().entity_id;
	        	var entity_nm = Ext.getCmp('update_form').getFieldValue('entity_nm');
	        	var table_nm = Ext.getCmp('update_form').getFieldValue('table_nm');
	        	var use_yn = Ext.getCmp('update_form').getInputData().use_yn;
	        	var sort_basis = Ext.getCmp('update_form').getInputData().sort_basis;
	        	var tab_type = Ext.getCmp('update_form').getInputData().tab_type;
	        	var tab_group = Ext.getCmp('update_form').getInputData().tab_group;
	        	var window_size = Ext.getCmp('update_form').getInputData().window_size;
	        	var action = Ext.getCmp('update_form').getInputData().action;
	        	var readonly_yn = Ext.getCmp('update_form').getInputData().readonly_yn;
	        	var sort_order = Ext.getCmp('update_form').getInputData().sort_order;
	        	
	        	if(entity_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00018' }));
	        		return;
	        	}else if(table_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00019' }));
	        		return;
	        	}else{
	        		
	        		var seletedObject = Ext.getCmp('entityGrid').view.selModel.getSelection();
	        		getGrid("entityGrid").getStore().remove(seletedObject[0]);
	        		
	        		getGrid("entityGrid").getStore().add({"RNUM":'UPDATE'
	        			                                  ,"ENTITY_ID":entity_id
	        			                                  ,"ENTITY_NM":entity_nm
	        			                                  ,"TABLE_NM":table_nm
	        			                                  ,"USE_YN":use_yn
	        			                                  ,"SORT_BASIS":sort_basis
	        			                                  ,"TAB_TYPE":tab_type
	        			                                  ,"TAB_GROUP":tab_group
	        			                                  ,"WINDOW_SIZE":window_size
	        			                                  ,"READONLY_YN":readonly_yn
	        			                                  ,"ACTION":action
	        			                                  ,"SORT_ORDER":sort_order
	        			                                  ,"CLASS_ID":class_id});
	        		/*
	    			//버튼처리
	        		var hideIds = ['mngUpdateBtn', 'mngDeleteBtn','mngInsPopBtn','mngUpdPopBtn'];	 
	    			hideBtns(hideIds);	//버튼을 숨긴다.
	    			var showIds = ['mngInsertBtn','mngCnacleBtn'];	
	    			showBtns(showIds);
	    			//관리항목 리스트 셋팅
	    			var notGrid = getGrid("notContainGrid").getStore();
	    			notGrid.proxy.jsonData={table_nm: table_nm};
	    			notGrid.load();
	    			
	    			var inGrid = getGrid("inContainGrid").getStore();
	    			inGrid.proxy.jsonData={table_nm: table_nm};
	    			inGrid.load();
	    			*/
	        		updateAmMngPop.hide();
	        	}
	        }
		});
		
		updateAmMngPop = new Ext.Window({

			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn],
			bodyStyle: that['bodyStyle'],
			items: [eidtorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = updateAmMngPop;
		
	} else {
		updateAmMngPop = popUp_comp[that['id']];
		theForm.initData();
		
	}
	
	return updateAmMngPop;
}

function capyAmMngInfo(setPopUpProp){
	var storeParam = {};
	var that = this;
	var capyAmMngPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	//var theForm = Ext.getCmp('update_form');
	
	if(popUp_comp[that['id']] == null) {

		/*
		분류체계 트리구조는 기존 amdbClassController사용함.
		*/
		var setTreeCapyProp = {
			context: '${context}',						
			id : 'amClassCapyTree',
			url : '/itg/itam/amdb/searchAmClassCheckTree.do',
			title : getConstText({ isArgs: true, m_key: 'res.00016' }),
			height : 450,
			dynamicFlag : false,
			searchable : true,
			searchableTextWidth : 60,
			expandAllBtn : false,
			collapseAllBtn : false,
			params: null,
			rnode_text : '#springMessage("res.00003")',
			expandLevel: 4
		}
		var classCheckTree = createTreeComponent(setTreeCapyProp);
		
		var setMngGridPropPop = {	
				context: '${context}',						
				id: 'popEntityGrid',	
				title : getConstText({ isArgs: true, m_key: 'res.label.tableMng.00010' }),
				resource_prefix: 'grid.itam.amentity',				// Resource Prefix
				url: '/itg/itam/amdb/searchAmEntityGridList.do',	// Data Url
				params: null,								// Data Parameters
				pagingBar : true,
				pageSize : 5,
				autoLoad: true,
				gridHeight: getConstValue('TALL_GRID_HEIGHT'),
				//toolBarComp: [mngInsPopBtn,mngUpdPopBtn,mngCapyBtn],
				emptyMsg: getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00010' })
		}
		var popEntityGrid = createGridComp(setMngGridPropPop);
		
		var temp = Ext.create('Ext.form.Panel', {
			  id: 'temp'
			, title: 'TEST'
	        , autoScroll : true
	        //, width : '100%'
	        //, height: '100%'
	        , layout: { 
	      	            type : 'hbox'
	      	            //pack : 'center',
	                   // align : 'middle'
	                   }
		    , items: [{
		    			flex: 1,
		    	        //border: true,
		                //style: 'border: solid grey 1px',
		                //margin: '10 10 10 10',
						items: classCheckTree
					  },getTbSpacer({width: 10}),{
			    		flex: 2,
				        //border: true,
			            //style: 'border: solid grey 1px',
		                //margin: '10 10 10 10',
					  	items: popEntityGrid
					  }]
		
		})
		
		//var seletedObject = Ext.getCmp('entityGrid').view.selModel.getSelection();
		//var updateForm = Ext.getCmp('update_form');
		
		//var clickRecord = seletedObject[0].raw;
		//updateForm.setDataForMap(clickRecord);
		
		
		var insertCapyBtn = new Ext.button.Button({
	        text: getConstText({ isArgs: true, m_key: 'btn.itam.00026' }),
	        handler: function() {
	        	
	        	var seletedObject = Ext.getCmp('amClassTree').view.selModel.getSelection();
	    		var class_id = seletedObject[0].raw.node_id;
	    		
	    		var entity_id = Ext.getCmp('update_form').getInputData().entity_id;
	        	var entity_nm = Ext.getCmp('update_form').getFieldValue('entity_nm');
	        	var table_nm = Ext.getCmp('update_form').getFieldValue('table_nm');
	        	var use_yn = Ext.getCmp('update_form').getInputData().use_yn;
	        	var sort_basis = Ext.getCmp('update_form').getInputData().sort_basis;
	        	var tab_type = Ext.getCmp('update_form').getInputData().tab_type;
	        	var tab_group = Ext.getCmp('update_form').getInputData().tab_group;
	        	var window_size = Ext.getCmp('update_form').getInputData().window_size;
	        	var action = Ext.getCmp('update_form').getInputData().action;
	        	var readonly_yn = Ext.getCmp('update_form').getInputData().readonly_yn;
	        	var sort_order = Ext.getCmp('update_form').getInputData().sort_order;
	        	
	        	if(entity_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00018' }));
	        		return;
	        	}else if(table_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00019' }));
	        		return;
	        	}else{
	        		
	        		var seletedObject = Ext.getCmp('entityGrid').view.selModel.getSelection();
	        		getGrid("entityGrid").getStore().remove(seletedObject[0]);
	        		
	        		getGrid("entityGrid").getStore().add({"RNUM":'UPDATE'
	        			                                  ,"ENTITY_ID":entity_id
	        			                                  ,"ENTITY_NM":entity_nm
	        			                                  ,"TABLE_NM":table_nm
	        			                                  ,"USE_YN":use_yn
	        			                                  ,"SORT_BASIS":sort_basis
	        			                                  ,"TAB_TYPE":tab_type
	        			                                  ,"TAB_GROUP":tab_group
	        			                                  ,"WINDOW_SIZE":window_size
	        			                                  ,"READONLY_YN":readonly_yn
	        			                                  ,"ACTION":action
	        			                                  ,"SORT_ORDER":sort_order
	        			                                  ,"CLASS_ID":class_id});
	        		capyAmMngPop.hide();
	        	}
	        }
		});
		
		capyAmMngPop = new Ext.Window({

			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertCapyBtn],
			layout : 'fit',
			bodyStyle: that['bodyStyle'],
			items: [temp],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = capyAmMngPop;
		
	} else {
		capyAmMngPop = popUp_comp[that['id']];
		theForm.initData();
		
	}
	
	return capyAmMngPop;
}
