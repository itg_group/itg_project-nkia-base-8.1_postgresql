
/**
 * 운영자 컴포넌트
 * 
 * @param compProperty
 * @returns {String}
 */
function createManagerList(compProperty) {

	var that = convertDomToArray(compProperty);
	var mngGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];
	var selModel = false;
	var btns = [];
	var addMngBtn = createBtnComp({
				visible : true,
				label : '추가',
				id : 'addMngBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});
	var delMngBtn = createBtnComp({
				visible : true,
				label : '삭제',
				id : 'delMngBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});
	var cellModel = false;

	// 삭제 버튼 핸들러
	var removeMngGridRow = function() {
		var selRows = gridSelectedRows(id);
		if (isSelected(id)) {
			Ext.suspendLayouts();
			for (var i = 0; i < selRows.length; i++) {
				mngGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						title : "경고",
						msg : "선택된 정보가 없습니다.",
						type : "ALERT"
					});
		}
	}

	// 추가 버튼 핸들러
	var mngListSelectPop = function() {
		createMainUserSelectPop(compProperty);
	}

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [addMngBtn, delMngBtn];
		cellModel = true;
	} else {
		btns = null;
		cellModel = false;
	}

	var gridProps = {
		id : id,
		title : title
		// ,resource_prefix: 'grid.itam.managerList'
		,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsSelMngUserList.do',
		gridHeight : 150,
		params : {
			conf_id : conf_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : false,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns
	}
	// 그리드 생성
	mngGrid = createGridComp(gridProps);
	// 버튼 이벤트
	attachBtnEvent(delMngBtn, removeMngGridRow);
	attachBtnEvent(addMngBtn, mngListSelectPop);

	return mngGrid;
}

/**
 * 사용자 컴포넌트
 * 
 * @param compProperty
 * @returns {String}
 */
function createUserList(compProperty) {

	var that = convertDomToArray(compProperty);
	var userGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];
	var selModel = false;
	var btns = [];
	var addUserBtn = createBtnComp({
				visible : true,
				label : '추가',
				id : 'addUserBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});
	var delUserBtn = createBtnComp({
				visible : true,
				label : '삭제',
				id : 'delUserBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});
	var cellModel = false;

	// 삭제 버튼 핸들러
	var removeUserGridRow = function() {
		var selRows = gridSelectedRows(id);
		if (isSelected(id)) {
			Ext.suspendLayouts();
			for (var i = 0; i < selRows.length; i++) {
				userGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						title : "경고",
						msg : "선택된 정보가 없습니다.",
						type : "ALERT"
					});
		}
	}

	// 추가 버튼 핸들러
	var userListSelectPop = function() {
		createUserListPop(compProperty); // TODO 해당 팝업 호출 메소드로 변경
	}

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [addUserBtn, delUserBtn];
		cellModel = true;
	} else {
		btns = null;
		cellModel = false;
	}

	var gridProps = {
		id : id,
		title : title

		,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsSelUserList.do' // TODO 해당 서비스로 붙일 것.
		,
		gridHeight : 150,
		params : {
			conf_id : conf_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : false,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns,
		cellEditing : true
		// , actionIcon:
		// '${context}/itg/base/images/ext-js/common/icons/bullet_search.gif'
		// , actionBtnEvent: actionBtnEvent
	}
	// 그리드 생성
	userGrid = createGridComp(gridProps);
	// 버튼 이벤트
	attachBtnEvent(delUserBtn, removeUserGridRow);
	attachBtnEvent(addUserBtn, userListSelectPop);

	return userGrid;
}

function actionBtnEvent(grid, rowIndex, colIndex) {
	// 그리드의 서비스 값 가져와서 해당 사용자 컬럼 조회
	var record = grid.getStore().getAt(rowIndex);
	var rowData = record.data;
	if (rowData["ASSIGNED_LICENCE_CNT"] > 0) {
		openSelSwAssetLicenceAssignedPop(rowData);
	} else {
		alert("할당된 라이선스 정보가 없습니다.");
	}
}
/**
 * 업체관리자정보 리스트 컴포넌트
 * 
 * @param compProperty
 * @returns {String}
 */
function createComMngInfoList(compProperty) {

	var that = convertDomToArray(compProperty);
	var commngGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];
	var selModel = false;
	var btns = [];
	var addComMngBtn = createBtnComp({
				visible : true,
				label : '추가',
				id : 'addMngBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});
	var delComMngBtn = createBtnComp({
				visible : true,
				label : '삭제',
				id : 'delMngBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});
	var cellModel = false;

	// 삭제 버튼 핸들러
	var removeComMngGridRow = function() {
		var selRows = gridSelectedRows(id);
		if (isSelected(id)) {
			Ext.suspendLayouts();
			for (var i = 0; i < selRows.length; i++) {
				userGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						title : "경고",
						msg : "선택된 정보가 없습니다.",
						type : "ALERT"
					});
		}
	}

	// 추가 버튼 핸들러
	var commngListSelectPop = function() {
		createUserListPop(compProperty); // TODO 팝업창 메소드로 변경
	}

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [addComMngBtn, delComMngBtn];
		cellModel = true;
	} else {
		btns = null;
		cellModel = false;
	}

	var gridProps = {
		id : id,
		title : title
		// ,resource_prefix: 'grid.itam.commngList'
		,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsSelUserList.do' // TODO 서비스 붙일 것.
		,
		gridHeight : 150,
		params : {
			conf_id : conf_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : false,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns
	}
	// 그리드 생성
	commngGrid = createGridComp(gridProps);
	// 버튼 이벤트
	attachBtnEvent(delComMngBtn, removeComMngGridRow);
	attachBtnEvent(addComMngBtn, commngListSelectPop);

	return commngGrid;
}

/**
 * 자자산 리스트 컴포넌트
 * 
 * @param compProperty
 * @returns {String}
 */
function createChildList(compProperty) {

	var that = convertDomToArray(compProperty);
	var childGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var ori_asset_id = that["ori_asset_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];
	var selModel = true;
	var btns = [];
	var sendChildBtn = createBtnComp({
				visible : true,
				label : '모자산 변경',
				id : 'sendChildBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});

	var cellModel = cellModel;

	// 모자산 변경 핸들러
	var motherListSelectPop = function() {
		createMotherListPop(compProperty);
	}

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [sendChildBtn];
		cellModel = true;
	} else {
		btns = null;
		cellModel = false;
	}

	var gridProps = {
		id : id,
		title : title
		// ,resource_prefix: 'grid.itam.childList'
		,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsSelChildList.do',
		gridHeight : 150,
		params : {
			conf_id : conf_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : false,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns
	}
	// 그리드 생성
	childGrid = createGridComp(gridProps);
	// 버튼 이벤트
	// attachBtnEvent(delChildBtn ,removeChildGridRow);
	attachBtnEvent(sendChildBtn, motherListSelectPop);

	return childGrid;
}
/**
 * 구매정보 리스트 컴포넌트
 * 
 * @param compProperty
 * @returns {String}
 */
function createPurInfoList(compProperty) {

	var that = convertDomToArray(compProperty);
	var purinfoGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];
	var selModel = false;
	var btns = [];
	// var addChildBtn = createBtnComp({visible: true,label:'추가', id:'addMngBtn'
	// , icon :'/itg/base/images/ext-js/common/icons/add.png' ,margin : '0 2 0
	// 0'});
	// var delChildBtn = createBtnComp({visible: true,label:'삭제', id:'delMngBtn'
	// , icon :'/itg/base/images/ext-js/common/icons/delete.png' ,margin : '0 0
	// 0 0'});
	var cellModel = false;

	// //삭제 버튼 핸들러
	// var removeChildGridRow = function(){
	// var selRows = gridSelectedRows(id);
	// if(isSelected(id)){
	// Ext.suspendLayouts();
	// for(var i=0;i<selRows.length;i++){
	// childGrid.store.remove(selRows[i]);
	// }
	// Ext.resumeLayouts(true);
	// }else{
	// showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
	// }
	// }
	//	
	// //추가 버튼 핸들러
	// var childListSelectPop = function(){
	// createMngUserListPop(compProperty);
	// }

	// //사용자가 그리드 수정권한이 있을경우
	// if(authType == "U"){
	// btns = [addChildBtn,delChildBtn];
	// cellModel = true;
	// }else{
	// btns = null;
	// cellModel = false;
	// }

	var gridProps = {
		id : id,
		title : title
		// ,resource_prefix: 'grid.itam.purinfoList'
		,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsSelPurInfoList.do' // 해당서비스 붙일 것
		,
		gridHeight : 150,
		params : {
			asset_id : asset_id
		} // TODO 쿼리에 따라 변수 바꿀 것.
		,
		autoLoad : true,
		multiSelect : true,
		cellEditing : false,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns,
		selfScroll : true,
		forceFit : true
	}
	// 그리드 생성
	purinfoGrid = createGridComp(gridProps);
	// 버튼 이벤트
	// attachBtnEvent(delChildBtn ,removeChildGridRow);
	// attachBtnEvent(addChildBtn,childListSelectPop);

	return purinfoGrid;
}

/**
 * 감가상각 정보 리스트 컴포넌트
 * 
 * @param compProperty
 * @returns {String}
 */
function createDepreciationInfoList(compProperty) {

	var that = convertDomToArray(compProperty);
	var purinfoGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];
	var selModel = false;
	var btns = [];
	// var addChildBtn = createBtnComp({visible: true,label:'추가', id:'addMngBtn'
	// , icon :'/itg/base/images/ext-js/common/icons/add.png' ,margin : '0 2 0
	// 0'});
	// var delChildBtn = createBtnComp({visible: true,label:'삭제', id:'delMngBtn'
	// , icon :'/itg/base/images/ext-js/common/icons/delete.png' ,margin : '0 0
	// 0 0'});
	var cellModel = false;

	// //삭제 버튼 핸들러
	// var removeChildGridRow = function(){
	// var selRows = gridSelectedRows(id);
	// if(isSelected(id)){
	// Ext.suspendLayouts();
	// for(var i=0;i<selRows.length;i++){
	// childGrid.store.remove(selRows[i]);
	// }
	// Ext.resumeLayouts(true);
	// }else{
	// showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
	// }
	// }
	//	
	// //추가 버튼 핸들러
	// var childListSelectPop = function(){
	// createMngUserListPop(compProperty);
	// }

	// //사용자가 그리드 수정권한이 있을경우
	// if(authType == "U"){
	// btns = [addChildBtn,delChildBtn];
	// cellModel = true;
	// }else{
	// btns = null;
	// cellModel = false;
	// }

	var gridProps = {
		id : id,
		title : title
		// ,resource_prefix: 'grid.itam.DepreciationList'
		,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsDepreciationInfoList.do' // TODO
																	// 해당서비스 붙일
																	// 것
		,
		gridHeight : 150,
		params : {
			asset_id : asset_id
		} // TODO 쿼리에 따라 변수 바꿀 것.
		,
		autoLoad : true,
		multiSelect : true,
		cellEditing : false,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns,
		selfScroll : true,
		forceFit : true
	}
	// 그리드 생성
	depreciationGrid = createGridComp(gridProps);
	// 버튼 이벤트
	// attachBtnEvent(delChildBtn ,removeChildGridRow);
	// attachBtnEvent(addChildBtn,childListSelectPop);

	return depreciationGrid;
}
/**
 * 파일 컴포넌트
 */
function createFileForm(compProperty) {

	var that = convertDomToArray(compProperty);
	var mngGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var atch_file_id = that["atch_file_id"];

	var setFileFormProp = {
		id : id,
		title : title,
		editOnly : true,
		columnSize : 1,
		border : false,
		tableProps : [{
					colspan : 1,
					item : createAtchFileFieldComp({
								name : "atch_file_id",
								fileHeight : 100
							})
				}],
		hiddenFields : [createHiddenFieldComp({
							name : 'conf_id'
						}), createHiddenFieldComp({
							name : 'asset_id'
						})]
	}

	var fileForm = createEditorFormComp(setFileFormProp);

	fileForm.on({
				afterrender : function() {
					if (authType == "U") {
						setVisibleFileButton("atch_file_id", true);
					} else {
						setVisibleFileButton("atch_file_id", false);
					}
					getAtchFileList("atch_file_id", atch_file_id);
				}
			});

	return fileForm;
}

/**
 * 연관 장비 탭 그룹
 * 
 * @param compProperty
 */
function createRelInfra(compProperty) {

	var that = convertDomToArray(compProperty);
	var mngGrid = "";

	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var itemArray = that["itemArray"];
	var gridCompArr = [];
	var btns = [];
	var addRelBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.append'
						}),
				id : 'add111RelBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});
	var delRelBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.remove'
						}),
				id : 'delRelBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});

	/*
	 * var raidTypeCombo =
	 * createCodeComboBoxComp({name:'RAID_TYPE',code_grp_id:'RAID_TYPE' ,width :
	 * 100,hideLabel : true,attachChoice:true }); var ifTypeCombo =
	 * createCodeComboBoxComp({name:'IF_TYPE' ,code_grp_id:'IF_TYPE' ,width :
	 * 100 ,hideLabel : true,attachChoice:true });
	 */

	var tabComp = "";

	var clickTab = function(tabComp, clickTab, prevTab) {
		var classType = clickTab.id;
		if (classType == "LOGSV" || classType == "HA") {
			addRelBtn.hide();
			delRelBtn.hide();
		} else {
			addRelBtn.show();
			delRelBtn.show();
		}
	}

	// var actionBtnEvent = function actionBtnEvent(grid, rowIndex, colIndex) {
	// var gridData = grid.getStore().data.items[rowIndex].data;
	// var tabId = grid.getStore().data.items[rowIndex].data.CONF_ID;
	// gridData["TAB_ID"] = tabId;
	// var closable = true;
	// parent.addRelConfTab(gridData, closable);
	// };

	var removeConfGridRow = function() {

		var selTabComp = tabComp.getActiveTab();
		var gridId = selTabComp.id;
		var selRows = gridSelectedRows(gridId);
		var title = selTabComp.title;
		var relTitle = title.substr(0, title.indexOf("["));

		if (isSelected(gridId)) {
			Ext.suspendLayouts();
			for (var i = 0; i < selRows.length; i++) {
				selTabComp.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						msg : getConstText({
									isArgs : true,
									m_key : 'msg.itam.opms.00010'
								}),
						type : "ALERT"
					});
			return;
		}
		selTabComp.setTitle(relTitle + "[" + selTabComp.store.count() + "]");
	}

	var relConfSelectPop = function() {
		var selTabComp = tabComp.getActiveTab();
		var gridId = selTabComp.id;
		var title = selTabComp.title;
		var relTile = title.substr(0, title.indexOf("["));
		createRelConfPop({
					class_type : gridId,
					title : relTile,
					target_id : gridId,
					conf_id : conf_id
				});
	}

	// ///////////////// function area end

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [addRelBtn, delRelBtn];
		cellModel = true;
	} else {
		btns = null;
		cellModel = false;
	}

	var view_type = that['view_type'];
	var rel_conf_url = '/itg/itam/opms/selectOpmsRelConfList.do'

	if (view_type == 'infmodify') {
		rel_conf_url = '/itg/itam/opms/selectOpmsInfoRelConfList.do'
	}

	// 해당 컴포넌트 그리드 생성
	for (i = 0; i < itemArray.length; i++) {

		var itemCodeId = itemArray[i].CODE_ID;
		var itemCodeNm = itemArray[i].CODE_NM;
		var storeOnFn = "";
		setSelGridProp = {
			id : itemCodeId,
			title : itemCodeNm,
			gridFormItem : that["gridFormItem" + itemCodeId]
			// ,resource_prefix: 'grid.itam.relConf'+itemCodeId
			,
			url : rel_conf_url,
			gridHeight : 138,
			params : {
				conf_id : conf_id,
				asset_id : asset_id,
				class_type : itemCodeId
			},
			autoLoad : true,
			multiSelect : true,
			cellEditing : true,
			selModel : cellModel,
			pagingBar : false,
			editorMode : true,
			border : true,
			comboComp : getCompOptionComp(itemCodeId, authType)
			// ,actionIcon:
			// '/itg/base/images/ext-js/common/icons/menu_process.gif'
			// ,actionBtnEvent: actionBtnEvent
		};
		eval("var gridComp" + [i] + " = createGridComp(setSelGridProp)");

		storeOnFn += 'gridComp' + [i] + '.store.on("load",';
		storeOnFn += 'function(gridCompStore){';
		storeOnFn += 'var itemsArray= tabComp.items.items;';
		storeOnFn += 'for(var i=0;i<itemsArray.length;i++){';
		storeOnFn += 'var tabItem = itemsArray[i];';
		storeOnFn += 'if(tabItem.id == gridComp' + [i] + '.id){';
		storeOnFn += 'var title = tabItem.title;';
		storeOnFn += 'tabItem.setTitle(title + " ["+gridCompStore.count()+"]");';
		storeOnFn += '}}});';

		eval(storeOnFn);
		eval('gridCompArr.push(gridComp' + [i] + ')');
	}

	// 탭생성
	var tabBasicProperty = {
		tab_id : id + "Tab",
		width : '100%',
		// title : title,
		layout : 'center',
		item : gridCompArr,
		tools : btns
	}
	
	tabComp = createTabComponent(tabBasicProperty);

	attachCustomEvent('tabchange', tabComp, clickTab);

	attachBtnEvent(delRelBtn, removeConfGridRow);
	attachBtnEvent(addRelBtn, relConfSelectPop);

	var outerPanel = Ext.create('Ext.form.Panel', {
				id : id + 'Panel',
				width : '100%',
				title : title,
				defaults : {
					layout : 'fit',
					flex : 1
				},
				items : [{
							flex : 1,
							items : tabComp
						}]
			});

	return outerPanel;
}

/**
 * 연관 서비스
 * 
 * @param compProperty
 */
function createRelService(compProperty) {

	var that = convertDomToArray(compProperty);
	var serGrid = "";

	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var btns = [];
	var addRelSerBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.append'
						}),
				id : 'addRelSerBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});
	var delRelSerBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.remove'
						}),
				id : 'delRelSerBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});

	var relServiceSelectPop = function(compProperty) {
		createRelServicePop({
					target_id : id,
					conf_id : conf_id
				});
	}

	var removeServiceGridRow = function() {
		var gridId = serGrid.id;
		var selRows = gridSelectedRows(gridId);

		if (isSelected(gridId)) {
			Ext.suspendLayouts();
			for (var i = 0; i < selRows.length; i++) {
				serGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						msg : getConstText({
									isArgs : true,
									m_key : 'msg.itam.opms.00010'
								}),
						type : "ALERT"
					});
			return;
		}
	}

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [addRelSerBtn, delRelSerBtn];
		cellModel = true;
	} else {
		btns = null;
		cellModel = false;
	}

	var view_type = that['view_type'];
	var service_url = '/itg/itam/opms/selectOpmsRelServiceList.do'

	if (view_type == 'infmodify') {
		service_url = '/itg/itam/opms/selectOpmsInfoServiceList.do'
	}

	var setSerGridProp = {
		id : id,
		title : title
		// ,resource_prefix : 'grid.itam.relService'
		,
		gridFormItem : that["gridFormItem"],
		url : service_url,
		gridHeight : 138,
		params : {
			conf_id : conf_id,
			asset_id : asset_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : true,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns
	};

	serGrid = createGridComp(setSerGridProp);
	attachBtnEvent(delRelSerBtn, removeServiceGridRow);
	attachBtnEvent(addRelSerBtn, relServiceSelectPop);

	return serGrid;
}

/**
 * 서비스 부서 컴포넌트를 가져온다.
 * 
 * @param compProperty
 * @returns
 */
function createManualComboBoxComp(compProperty) {
	var param = convertDomToArray(compProperty);
	var combo_proxy = Ext.create('nkia.custom.JsonProxy', {
				defaultPostHeader : "application/json",
				noCache : false,
				type : 'ajax',
				url : param["url"],
				jsonData : param,
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST'
				},
				reader : {
					type : 'json',
					root : 'gridVO.rows'
				}
			});

	compProperty['proxy'] = combo_proxy;
	compProperty['valueField'] = param["valueField"];
	compProperty['displayField'] = param["displayField"];
	var combo_comp = createComboBoxComp(compProperty);
	return combo_comp;
}

/**
 * 
 * @param compProperty
 */
function createInstallInfo(compProperty) {

	var that = convertDomToArray(compProperty);
	var mngGrid = "";

	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var itemArray = that["itemArray"];
	var gridCompArr = [];
	var btns = [];
	var addInsBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.append'
						}),
				id : 'addInsBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});
	var delInsBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.remove'
						}),
				id : 'delInsBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});
	var tabComp = "";

	// ///////////////// function area start

	var addInstallInfo = function() {

		var selTabComp = tabComp.getActiveTab();
		var tabCompId = selTabComp.id;
		var selTabCompStore = selTabComp.store;
		var columns = selTabComp.columns;
		var idxCol = that["idx_col_" + tabCompId];
		var param = {};
		var title = selTabComp.title;
		var relTile = title.substr(0, title.indexOf("["));
		var idxId = "";

		if (idxCol) {
			for (var i = 0; i < columns.length; i++) {
				if (columns[i].dataIndex != ""
						&& columns[i].dataIndex == idxCol) {
					var idxCnt = 0;
					var prevIdx = 0;
					var curIdx = 0;
					for (var j = 0; j < selTabCompStore.count(); j++) {
						var insIdNm = selTabCompStore.data.items[j].raw[idxCol];
						if (insIdNm.indexOf("M") == 0) {
							curIdx = parseInt(insIdNm.substr(1, insIdNm.length));
							if (curIdx > prevIdx) {
								prevIdx = curIdx;
							}
						}
					}
					param[columns[i].dataIndex] = "M" + (prevIdx + 1);
				} else {
					param[columns[i].dataIndex] = "";
				}
			}
		} else {
			for (var i = 0; i < columns.length; i++) {
				if (columns[i].dataIndex != "") {
					param[columns[i].dataIndex] = "";
				}
			}
		}

		selTabCompStore.insert(selTabCompStore.count(), param);
		selTabComp.setTitle(relTile + "[" + selTabCompStore.count() + "]");
	}

	var delInstallInfo = function() {

		var selTabComp = tabComp.getActiveTab();
		var gridId = selTabComp.id;
		var selRows = gridSelectedRows(gridId);
		var title = selTabComp.title;
		var relTitle = title.substr(0, title.indexOf("["));

		if (isSelected(gridId)) {
			Ext.suspendLayouts();
			for (var i = 0; i < selRows.length; i++) {
				selTabComp.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						msg : getConstText({
									isArgs : true,
									m_key : 'msg.itam.opms.00010'
								}),
						type : "ALERT"
					});
			return;
		}
		selTabComp.setTitle(relTitle + "[" + selTabComp.store.count() + "]");
	}
	// ///////////////// function area end

	var cellModel = false;
	var cellEditing = false;
	var editorMode = false;
	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [addInsBtn, delInsBtn];
		cellModel = true;
		cellEditing = true;
		editorMode = true;
	} else {
		btns = null;
		cellModel = false;
		cellEditing = false;
		editorMode = false;
	}

	// 해당 컴포넌트 그리드 생성
	for (i = 0; i < itemArray.length; i++) {

		var itemCodeId = itemArray[i].CODE_ID;
		var itemCodeNm = itemArray[i].CODE_NM;
		var storeOnFn = "";

		setSelGridProp = {
			id : itemCodeId,
			title : itemCodeNm,
			gridFormItem : that["gridFormItem" + itemCodeId]
			// ,resource_prefix: 'grid.itam.installInfo'+itemCodeId
			,
			url : '/itg/itam/opms/selectOpmsInstallInfoList.do',
			gridHeight : 138,
			params : {
				conf_id : conf_id,
				asset_id : asset_id,
				ins_type : itemCodeId,
				view_type : that['view_type']
			} // @@ 고은규 수정
			,
			autoLoad : true,
			multiSelect : true,
			cellEditing : cellEditing,
			selModel : cellModel,
			pagingBar : false,
			editorMode : true,
			border : true,
			comboComp : getCompOptionComp(itemCodeId, authType)
		};

		eval("var gridComp" + [i] + " = createGridComp(setSelGridProp)");

		storeOnFn += 'gridComp' + [i] + '.store.on("load",';
		storeOnFn += 'function(gridCompStore){';
		storeOnFn += 'var itemsArray= tabComp.items.items;';
		storeOnFn += 'for(var i=0;i<itemsArray.length;i++){';
		storeOnFn += 'var tabItem = itemsArray[i];';
		storeOnFn += 'if(tabItem.id == gridComp' + [i] + '.id){';
		storeOnFn += 'var title = tabItem.title;';
		storeOnFn += 'tabItem.setTitle(title + " ["+gridCompStore.count()+"]");';
		storeOnFn += '}}});';

		eval(storeOnFn);
		eval('gridCompArr.push(gridComp' + [i] + ')');
	}

	// 탭생성
	var tabBasicProperty = {
		tab_id : id + "Tab",
		width : '100%',
		// title : title,
		layout : 'center',
		item : gridCompArr,
		tools : btns
	}

	tabComp = createTabComponent(tabBasicProperty);
	// attachCustomEvent('tabchange', tabComp, clickTab);
	attachBtnEvent(addInsBtn, addInstallInfo);
	attachBtnEvent(delInsBtn, delInstallInfo);

	var outerPanel = Ext.create('Ext.form.Panel', {
				id : id + 'Panel',
				width : '100%',
				title : title,
				defaults : {
					layout : 'fit',
					flex : 1
				},
				items : [{
							flex : 1,
							items : tabComp
						}]
			});
	return outerPanel;
}

function createConfNmCode(compProperty) {

	var that = convertDomToArray(compProperty);
	var mngGrid = "";

	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var notNull = false;
	var confCodeForm = "";
	var btn = [];
	var item = [];
	var editMode = false;

	var popOpBtn = createPopBtnComp({
				id : id + "OpPopBtn"
			});
			
	var popInfoBtn = createBtnComp({
				id : id + "InfoPopBtn",
				icon : getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/bullet_detail.gif',
				tooltip : "상세보기"
			});

	if (authType == "U") {
		notNull = false;
		btn = [popOpBtn]
		editMode = true;
	} else {
		notNull = false;
		btn = [popInfoBtn]
		editMode = false;
	}

	var confNmCodeOpPop = function() {
		confNmCodePop(compProperty);
	}

	// 장비명명 뷰화면
	item.push(createTextFieldComp({
				name : 'CENTER_LOC_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 25,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));
	item.push(createLabelComp({
				width : 10,
				text : '-'
			}));
	item.push(createTextFieldComp({
				name : 'PHYSI_LOC_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 100,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));
	item.push(createLabelComp({
				width : 10,
				text : '-'
			}));
	item.push(createTextFieldComp({
				name : 'NW_LINE_TYPE_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 20,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));
	item.push(createLabelComp({
				width : 10,
				text : '-'
			}));
	item.push(createTextFieldComp({
				name : 'INSTALL_PERIOD_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 20,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));
	item.push(createLabelComp({
				width : 10,
				text : '-'
			}));
	item.push(createTextFieldComp({
				name : 'CONF_TYPE_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 20,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));
	item.push(createLabelComp({
				width : 10,
				text : '-'
			}));
	item.push(createTextFieldComp({
				name : 'CONF_FUNC_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 50,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));
	item.push(createLabelComp({
				width : 10,
				text : '-'
			}));
	item.push(createTextFieldComp({
				name : 'SERIAL_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 22,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));
	item.push(createLabelComp({
				width : 10,
				text : '-'
			}));
	item.push(createTextFieldComp({
				name : 'HA_NM',
				editMode : editMode,
				readOnly : true,
				notNull : notNull,
				width : 20,
				hideLabel : true,
				fieldStyle : 'text-align: center;'
			}));

	item.push(createHiddenFieldComp({
				name : 'CENTER_TYPE'
			}));
	item.push(createHiddenFieldComp({
				name : 'BUILDING'
			}));
	item.push(createHiddenFieldComp({
				name : 'FLOOR'
			}));
	item.push(createHiddenFieldComp({
				name : 'RACK_PERIOD'
			}));
	item.push(createHiddenFieldComp({
				name : 'RACK_ORDER'
			}));
	item.push(createHiddenFieldComp({
				name : 'CHASSIS'
			}));
	item.push(createHiddenFieldComp({
				name : 'MODULE'
			}));
	item.push(createHiddenFieldComp({
				name : 'NW_LINE_TYPE'
			}));
	item.push(createHiddenFieldComp({
				name : 'INSTALL_PERIOD'
			}));
	item.push(createHiddenFieldComp({
				name : 'CONF_TYPE'
			}));
	item.push(createHiddenFieldComp({
				name : 'CONF_FUNC'
			}));
	item.push(createHiddenFieldComp({
				name : 'SERIAL'
			}));
	item.push(createHiddenFieldComp({
				name : 'HA'
			}));
	item.push(createHiddenFieldComp({
				name : 'CONF_NM_CODE'
			}));

	// 이력 관리 데이터만 라벨을 넣어줍니다.....
	// (매뉴얼로 만든거라서.. 이력을 넣을때 라벨 관리가 어렵습니다.
	// 그래서 히든으로 넘겨줍니다.. 한곳에서 관리 되는게 좋을것 같아서..)

	// 버튼 추가 될수도 있어서 추가 합니다.
	for (var i = 0; i < btn.length; i++) {
		item.push(btn[i]);
	}

	var colIdContainer = createUnionFieldContainer({
				label : getConstText({
							isArgs : true,
							m_key : 'res.label.itam.opms.00001'
						}),
				id : id + "Container",
				items : item,
				notNull : notNull
			});

	var setEditorFormProp = {
		id : id,
		title : title,
		editOnly : true,
		border : true,
		columnSize : 1,
		tableProps : [{
					colspan : 1,
					tdHeight : 25,
					item : colIdContainer
				}]
	}

	attachBtnEvent(popOpBtn, confNmCodeOpPop);
	attachBtnEvent(popInfoBtn, confNmCodeOpPop);

	confCodeForm = createEditorFormComp(setEditorFormProp);

	return confCodeForm;

}

/**
 * 수정이력을 만든다.
 * 
 * @param compProperty
 * @returns
 */
function createUpdateHistory(compProperty) {

	var that = convertDomToArray(compProperty);
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var itemArray = that["itemArray"];

	var startDateObj = "";
	var endDateObj = "";

	var panelItem = [];
	var btn = [];
	var item = [];
	var compItems = [];
	var searchForm = [];

	var searchCompList = function() {

		var paramData = searchForm.getInputData();
		var schCompItems = [];

		for (var i = 0; i < itemArray.length; i++) {

			var itemData = itemArray[i];
			var entity_type = itemData.ENTITY_TYPE;
			var entity_id = itemData.ENTITY_ID;
			var entity_nm = itemData.ENTITY_NM;
			var comp_type = itemData.COMP_TYPE;

			var obj = Ext.getCmp(entity_id + "modList");

			paramData["conf_id"] = conf_id;
			paramData["asset_id"] = asset_id;
			paramData["entity_id"] = entity_id;
			paramData["comp_type"] = comp_type;

			// 사용자 지정 폼인경우...
			if (entity_type == "MANUAL") {
				obj.searchList(paramData);
			} else if (entity_type == "COMPONENT") {
				if (comp_type == "CONFNMCODE") {
					obj.searchList(paramData);
				} else {
					schCompItems.push(itemData);
				}
			}
		}

		if (compItems.length > 0) {
			var compTotModList = Ext.getCmp("compTotModList");
			paramData["compItems"] = schCompItems;
			compTotModList.searchList(paramData);
		}
	}

	// 검색 버튼
	var searchCompModBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.search'
						}),
				id : 'searchCompModBtn',
				ui : 'correct'
			});
	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {
				year : 0,
				month : 0,
				day : -7,
				hour : 0,
				min : 0
			});
	var endDateObj = createDateTimeStamp('DHM', {
				year : 0,
				month : 0,
				day : 0,
				hour : 0,
				min : 0
			});

	// 검색값
	var searchDateContainer = createSearchDateFieldComp({
				label : getConstText({
							isArgs : true,
							m_key : 'res.label.itam.opms.00002'
						}),
				id : 'searchDate',
				name : 'updDt',
				dateType : 'D',
				format : 'Y-m-d',
				start_dateValue : startDateObj,
				end_dateValue : endDateObj
			})

	/*
	var searchKey = createCodeComboBoxComp({
				name : 'modSearchKey',
				code_grp_id : 'MOD_SEARCH_KEY',
				hideLabel : true,
				width : 100,
				attachAll : true
			});
	// 검색값
	var searchValue = createTextFieldComp({
				name : 'modSearchValue',
				padding : '0 0 0 5',
				width : 200,
				hideLabel : true
			});
	// 컨테이너
	var searchContainer = createUnionFieldContainer({
				label : getConstText({
							isArgs : true,
							m_key : 'res.common.search'
						}),
				width : 500,
				items : [searchKey, searchValue],
				id : "modSearchContainer"
			});
	*/
	
	var searchContainer	= createUnionCodeComboBoxTextContainer({
		label : getConstText({
			isArgs : true,
			m_key : 'res.common.search'
		})
		, comboboxName: 'modSearchKey'
		, textFieldName: 'modSearchValue'
		, code_grp_id: 'MOD_SEARCH_KEY'
		, widthFactor:0.5
		, fixedLayout: true
		, fixedWidth: 120
	});
	
	
	// 검색 폼 생성
	var setSearchFormProp = {
		id : 'modSearchForm',
		columnSize : 3,
		formBtns : [searchCompModBtn]// 검색버튼을 생성 formBtns라는 속성을 추가하여 생성한
										// 조회버튼을 추가함
		,
		tableProps : [{
					colspan : 1,
					tdHeight : 22,
					item : searchDateContainer
				}, {
					colspan : 2,
					tdHeight : 22,
					item : searchContainer
				}],
		enterFunction : searchCompList
	}
	// 검색폼
	searchForm = createSeachFormComp(setSearchFormProp);

	panelItem.push(searchForm);

	// 컴포넌트 유형별 그리드 생성
	for (var i = 0; i < itemArray.length; i++) {

		var itemData = itemArray[i];

		var entity_type = itemData.ENTITY_TYPE;
		var entity_id = itemData.ENTITY_ID;
		var entity_nm = itemData.ENTITY_NM;
		var comp_type = itemData.COMP_TYPE;
		var result = "";

		var paramData = {
			conf_id : conf_id,
			asset_id : asset_id,
			entity_id : entity_id,
			entity_nm : entity_nm,
			updDt_startDate : startDateObj.day,
			updDt_endDate : endDateObj.day,
			comp_type : comp_type
		};

		// 사용자 지정 폼인경우...
		if (entity_type == "MANUAL") {
			result = createFormModGrid(paramData);
		} else if (entity_type == "COMPONENT") {
			if (comp_type == "CONFNMCODE") {
				// 장비 명명정보
				result = createFormModGrid(paramData);
			} else if (comp_type == "CHILD") {
				// 원장에 자품목 리스트가 있으면 자품목 변경 이력도 생성
				paramData["a_asset_id_hi"] = asset_id;
				result = createDivHierachyHisGrid(paramData);
			} else {
				compItems.push(itemData);
			}
		}
		if (result != "") {
			panelItem.push(result);
		}
	}

	// 나머지 컴포넌트는 모아서 한 리스트로 출력
	if (compItems.length > 0) {
		paramData["compItems"] = compItems;
		result = createCompModGrid(paramData);
		panelItem.push(result);
	}

	// 검색 버튼 이벤트
	attachBtnEvent(searchCompModBtn, searchCompList);

	// 검색 패널 생성
	var modPanelcompProperty = {
		id : 'modPanelcompPanel'
		,title : getConstText({isArgs : true, m_key : 'res.label.itam.opms.00003'})
		,panelItems : {
			width:'100%'
			,items:panelItem
		}
	};

	return createVerticalPanel(modPanelcompProperty);
}

/**
 * 자품 수정이력 그리드 생성
 * 
 * @param compProperty
 */
function createDivHierachyHisGrid(compProperty) {

	var that = convertDomToArray(compProperty);
	var modGrid = "";
	var entity_id = that["entity_id"];
	var entity_nm = that["entity_nm"];

	var gridProps = {
		id : entity_id + "modList",
		title : entity_nm + " " + getConstText({
					isArgs : true,
					m_key : 'res.label.itam.opms.00003'
				}),
		resource_prefix : 'grid.itam.childHisList',
		url : '/itg/itam/opms/selectChildChgHisList.do',
		gridHeight : 150,
		params : compProperty,
		autoLoad : true,
		multiSelect : false,
		cellEditing : false,
		pagingBar : false,
		border : true
	}

	modGrid = createGridComp(gridProps);

	attachCustomEvent('cellclick', modGrid, createDivHierachyHisPop);

	return modGrid;

}

/**
 * 자품수정이력 상세 팝업
 * 
 * @param grid
 * @param td
 * @param cellIndex
 * @param record
 * @param tr
 * @param rowIndex
 * @param e
 * @param eOpts
 */
function createDivHierachyHisPop(grid, td, cellIndex, record, tr, rowIndex, e,
		eOpts) {

	var searchForm = Ext.getCmp("modSearchForm");
	var paramData = searchForm.getInputData();
	var modAttrGrid = "";

	for (var prop in record.raw) {
		paramData[prop] = record.raw[prop];
	}

	paramData["asset_id"] = paramData["ASSET_ID"];

	var gridProps = {
		id : "compAttrTotModList",
		title : "[자품목리스트] " + getConstText({
					isArgs : true,
					m_key : 'res.label.itam.opms.00004'
				}),
		resource_prefix : 'grid.itam.attrModHisList',
		url : '/itg/itam/opms/selectChildChgHisDetail.do',
		gridHeight : 265,
		params : paramData,
		autoLoad : true,
		multiSelect : false,
		cellEditing : false,
		pagingBar : false,
		border : true
	};

	modAttrGrid = createGridComp(gridProps);

	var complexPanel = createFlexPanel({
				items : [modAttrGrid]
			});
	// Viewport 생성

	var codePopupProp = {
		id : "compAttrModListPop",
		title : getConstText({
					isArgs : true,
					m_key : 'res.label.itam.opms.00005'
				}),
		width : 400,
		height : 315,
		modal : true,
		layout : 'fit',
		buttonAlign : 'center',
		bodyStyle : 'background-color: white; ',
		closeBtn : true,
		items : complexPanel,
		resizable : true
	}

	windowComp = createWindowComp(codePopupProp);
	windowComp.show();
}

/**
 * 사용자 지정 폼 속성 이력
 * 
 * @param compProperty
 * @returns {String}
 */
function createFormModGrid(compProperty) {

	var that = convertDomToArray(compProperty);
	var modGrid = "";
	var entity_id = that["entity_id"];
	var entity_nm = that["entity_nm"];

	var gridProps = {
		id : entity_id + "modList",
		title : entity_nm + " " + getConstText({
					isArgs : true,
					m_key : 'res.label.itam.opms.00003'
				}),
		resource_prefix : 'grid.itam.modList',
		url : '/itg/itam/opms/selectOpmsFormModList.do',
		gridHeight : 150,
		params : compProperty,
		autoLoad : true,
		multiSelect : false,
		cellEditing : false,
		pagingBar : false,
		border : true
	}

	modGrid = createGridComp(gridProps);

	attachCustomEvent('cellclick', modGrid, createFormModListPop);

	return modGrid;
}

/**
 * 컴포넌트 탭그리드 나 싱글 그리드를 모두 모아서 데이터를 보여준다..
 * 
 * @param compProperty
 */
function createCompModGrid(compProperty) {

	var modCompGrid = "";

	// var addMngBtn = createBtnComp({visible: true,label:'추가',
	// id:'compTotModListBtn' , icon
	// :'/itg/base/images/ext-js/common/icons/add.png' ,margin : '0 2 0 0'});

	var gridProps = {
		id : "compTotModList",
		title : getConstText({
					isArgs : true,
					m_key : 'res.label.itam.opms.00004'
				}),
		resource_prefix : 'grid.itam.compModList',
		url : '/itg/itam/opms/selectOpmsCompModList.do',
		gridHeight : 200,
		params : compProperty,
		autoLoad : true,
		multiSelect : false,
		cellEditing : false,
		pagingBar : false,
		border : true
		// ,toolBarComp : [addMngBtn]
	}

	modCompGrid = createGridComp(gridProps);

	attachCustomEvent('cellclick', modCompGrid, createCompModListPop);
	// attachBtnEvent(addMngBtn ,extentionModHisList ,modCompGrid);

	return modCompGrid;
}

/**
 * 속성이력 관리 속성 팝업
 * 
 * @param grid
 * @param td
 * @param cellIndex
 * @param record
 * @param tr
 * @param rowIndex
 * @param e
 * @param eOpts
 */
function createFormModListPop(grid, td, cellIndex, record, tr, rowIndex, e,
		eOpts) {

	var searchForm = Ext.getCmp("modSearchForm");
	var paramData = searchForm.getInputData();
	var modAttrGrid = "";

	for (var prop in record.raw) {
		paramData[prop] = record.raw[prop];
	}

	var gridProps = {
		id : "compAttrTotModList",
		title : "[" + paramData["CHG_COLUMN_NM"] + "] " + getConstText({
					isArgs : true,
					m_key : 'res.label.itam.opms.00004'
				}),
		resource_prefix : 'grid.itam.attrModHisList',
		url : '/itg/itam/opms/selectFormAttrModList.do',
		gridHeight : 265,
		params : paramData,
		autoLoad : true,
		multiSelect : false,
		cellEditing : false,
		pagingBar : false,
		border : true
	};

	modAttrGrid = createGridComp(gridProps);

	var complexPanel = createFlexPanel({
				items : [modAttrGrid]
			});
	// Viewport 생성

	var codePopupProp = {
		id : "compAttrModListPop",
		title : getConstText({
					isArgs : true,
					m_key : 'res.label.itam.opms.00005'
				}),
		width : 400,
		height : 315,
		modal : true,
		layout : 'fit',
		buttonAlign : 'center',
		bodyStyle : 'background-color: white; ',
		closeBtn : true,
		items : complexPanel,
		resizable : true
	}

	windowComp = createWindowComp(codePopupProp);
	windowComp.show();
}

/**
 * 속성이력 관리 속성 팝업
 * 
 * @param grid
 * @param td
 * @param cellIndex
 * @param record
 * @param tr
 * @param rowIndex
 * @param e
 * @param eOpts
 */
function createCompModListPop(grid, td, cellIndex, record, tr, rowIndex, e,
		eOpts) {

	var searchForm = Ext.getCmp("modSearchForm");
	var paramData = searchForm.getInputData();
	var modCompGrid = "";

	for (var prop in record.raw) {
		paramData[prop] = record.raw[prop];
	}

	data = getArrayToJson(paramData);
	url = '/itg/itam/amdb/selectCompDataColumn.do';
	callback = function(data) {
		if (data.success) {
			var gridFormItem = eval(data.resultString);
			var comp_name = paramData["COMP_NAME"];
			var tab_type_name = paramData["TAB_TYPE_NM"];
			var tab_type = paramData["TAB_TYPE"];
			var comp_type = paramData["COMP_TYPE"];
			var title = "";

			if (tab_type_name) {
				title = "[" + comp_name + " : " + tab_type_name + "] "
						+ getConstText({
									isArgs : true,
									m_key : 'res.label.itam.opms.00003'
								});
			} else {
				title = "[" + comp_name + "] " + getConstText({
							isArgs : true,
							m_key : 'res.label.itam.opms.00003'
						});
			}

			var gridProps = {
				id : "compDetailModList",
				title : title,
				gridFormItem : gridFormItem,
				url : '/itg/itam/opms/selectCompDetailModList.do',
				gridHeight : 315,
				params : paramData,
				autoLoad : true,
				multiSelect : false,
				cellEditing : false,
				pagingBar : false,
				border : true,
				comboComp : getCompOptionComp(tab_type, "R")
			};

			modCompGrid = createGridComp(gridProps);

			var complexPanel = createFlexPanel({
						items : [modCompGrid]
					});

			var compPopupProp = {
				id : "compDetailModListPop",
				title : getConstText({
							isArgs : true,
							m_key : 'res.label.itam.opms.00006'
						}),
				width : getCompHistoryWidth(comp_type),
				height : 365,
				modal : true,
				layout : 'fit',
				buttonAlign : 'center',
				bodyStyle : 'background-color: white; ',
				closeBtn : true,
				items : complexPanel,
				resizable : true
			}

			// 그리드 머지
			var loadFunction = function() {

				var gridTrs = modCompGrid.view.el.query('tr.x-grid-row');

				for (var i = 0; i < gridTrs.length; i++) {
					var rowTr = Ext.get(gridTrs[i]);
					var tds1 = rowTr.query('td');
					var textContent = tds1[0].innerText;
					var count = getDupTextCount(gridTrs, textContent, i, 0);
					for (j = 1; j < count; j++) {
						var rowTr2 = Ext.get(gridTrs[i + j]);
						var tds2 = rowTr2.query('td');
						Ext.get(tds2[0]).set({
									style : 'display: none'
								});
					}
					Ext.get(tds1[0]).set({
								rowspan : count
							});
					Ext.get(tds1[0]).addCls({
								'class' : 'titleBack'
							});
					var divText = Ext.get(tds1[0]).query('div');
					i = i + count - 1;
				}

				modCompGrid.store.un("load", loadFunction);
			}
			modCompGrid.store.on("load", loadFunction);

			windowComp = createWindowComp(compPopupProp);
			windowComp.show();
		} else {
			showMessageBox({
						msg : data.resultMsg,
						type : "ERROR"
					});
		}
	};
	callAjax(url, data, callback);
}

/**
 * 타입별 팝업 넓이
 * 
 * @param key
 * @returns {Number}
 */
function getCompHistoryWidth(key) {
	var width = 500;
	switch (key) {
		case "MANAGER" :
		case "USER" :
			widhth = 800;
		case "REL_SERVICE" :
			widhth = 800;
		case "REL_INFRA" :
		case "INSTALL" :
			width = 800;
			break;
	}
	return width;
}

/**
 * 그리드 유형별 컴포넌트 추가..
 * 
 * @param tab_type
 * @param auth_type
 * @returns {___anonymous28565_28566}
 */
function getCompOptionComp(tab_type, auth_type) {

	var component = {};
	var readOnly = true;
	switch (tab_type) {
		case "ST" :
			var raidTypeCombo = createCodeComboBoxComp({
						name : 'RAID_TYPE',
						code_grp_id : 'RAID_TYPE',
						width : 100,
						hideLabel : true,
						attachChoice : true
					});
			var ifTypeCombo = createCodeComboBoxComp({
						name : 'IF_TYPE',
						code_grp_id : 'IF_TYPE',
						width : 100,
						hideLabel : true,
						attachChoice : true
					});

			if (auth_type == "U") {
				raidTypeCombo.readOnly = false;
				ifTypeCombo.readOnly = false;
			}
			component = {
				RAID_TYPE : raidTypeCombo,
				IF_TYPE : ifTypeCombo
			}
			break;
	}
	return component;
}

/**
 * 폼에 추가될 버튼을 생성한다.
 * 
 * @param tab_type
 * @param auth_type
 * @returns {___anonymous28565_28566}
 */
function createPanelBtnComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var btn_nm = that['btn_nm'];
	var command = that['command'];
	var ui = (that['ui']) ? (that['ui']) : 'default';
	var scale = (that['scale']) ? (that['scale']) : 'small';

	// 이벤트 정의 주요 키목록 정의
	var conf_id = that['conf_id'];
	var class_type = that['class_type'];
	var tangible_asset_yn = that['tangible_asset_yn'];

	var btnEventHandler = function() {
		var param = "conf_id=" + conf_id + "&class_type=" + class_type
				+ "&tangible_asset_yn=" + tangible_asset_yn;
		if (command == "TOPOLOGY") { // 토폴로지맵

			var url = "/itg/itam/topology/goTopology.do?" + param;
			window
					.open(
							getConstValue('CONTEXT') + url,
							"topologyPop",
							"width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
		}
	}

	var btnObject = Ext.create('Ext.button.Button', {
				text : btn_nm,
				ui : ui,
				scale : scale,
				icon : that['icon'] ? that['icon'] : '',
				tooltip : that['tooltip'] ? that['tooltip'] : null,
				margin : that['margin']
						? that['margin']
						: getConstValue('BUTTON_MARGIN')
			});

	attachBtnEvent(btnObject, btnEventHandler);

	return btnObject;
}

/**
 * 연관 장비(상하위팝업선택) 탭 그룹
 * 
 * @param compProperty
 */
function createSelUpDownRelInfra(compProperty) {

	var that = convertDomToArray(compProperty);
	var mngGrid = "";

	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var itemArray = that["itemArray"];
	var gridCompArr = [];
	var btns = [];
	var addRelBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.append'
						}),
				id : 'addRelBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 2 0 0'
			});
	var delRelBtn = createBtnComp({
				visible : true,
				label : getConstText({
							isArgs : true,
							m_key : 'btn.common.remove'
						}),
				id : 'delRelBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});

	/*
	 * var raidTypeCombo =
	 * createCodeComboBoxComp({name:'RAID_TYPE',code_grp_id:'RAID_TYPE' ,width :
	 * 100,hideLabel : true,attachChoice:true }); var ifTypeCombo =
	 * createCodeComboBoxComp({name:'IF_TYPE' ,code_grp_id:'IF_TYPE' ,width :
	 * 100 ,hideLabel : true,attachChoice:true });
	 */

	var tabComp = "";

	var clickTab = function(tabComp, clickTab, prevTab) {
		var classType = clickTab.id;
		if (classType == "LOGSV" || classType == "HA") {
			addRelBtn.hide();
			delRelBtn.hide();
		} else {
			addRelBtn.show();
			delRelBtn.show();
		}
	}

	var removeConfGridRow = function() {

		var selTabComp = tabComp.getActiveTab();
		var gridId = selTabComp.id;
		var selRows = gridSelectedRows(gridId);
		var title = selTabComp.title;
		var relTitle = title.substr(0, title.indexOf("["));

		if (isSelected(gridId)) {
			Ext.suspendLayouts();
			for (var i = 0; i < selRows.length; i++) {
				selTabComp.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		} else {
			showMessageBox({
						title : getConstText({
									isArgs : true,
									m_key : 'res.label.itam.opms.00013'
								}),
						msg : getConstText({
									isArgs : true,
									m_key : 'msg.itam.opms.00010'
								}),
						type : "ALERT"
					});
			return;
		}
		selTabComp.setTitle(relTitle + "[" + selTabComp.store.count() + "]");
	}

	var relConfSelectPop = function() {
		var selTabComp = tabComp.getActiveTab();
		var gridId = selTabComp.id;
		var title = selTabComp.title;
		var relTile = title.substr(0, title.indexOf("["));
		createRelUpDownPop({
					class_type : gridId,
					title : relTile,
					target_id : gridId,
					conf_id : conf_id
				});
	}

	// ///////////////// function area end

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [addRelBtn, delRelBtn];
		cellModel = true;
	} else {
		btns = null;
		cellModel = false;
	}

	var view_type = that['view_type'];
	var rel_conf_url = '/itg/itam/opms/selectOpmsRelConfList.do'

	if (view_type == 'infmodify') {
		rel_conf_url = '/itg/itam/opms/selectOpmsInfoRelConfList.do'
	}

	// 해당 컴포넌트 그리드 생성
	for (i = 0; i < itemArray.length; i++) {
		var itemCodeId = itemArray[i].CODE_ID;
		var itemCodeNm = itemArray[i].CODE_NM;
		var storeOnFn = "";

		var actionIcon = null;
		var actionBtnEvent = null;
		if (itemCodeId == "LOGSV" || itemCodeId == "SV" || itemCodeId == "ST") {
			actionIcon = getConstValue('CONTEXT')
					+ '/itg/base/images/ext-js/common/icons/bullet_search.gif';
			actionBtnEvent = showAssetDetailPopBtnEvent;
		}

		setSelGridProp = {
			id : itemCodeId,
			title : itemCodeNm,
			gridFormItem : that["gridFormItem" + itemCodeId]
			// ,resource_prefix: 'grid.itam.relConf'+itemCodeId
			,
			url : rel_conf_url,
			gridHeight : getConstValue('SHORT_GRID_HEIGHT'),
			params : {
				conf_id : conf_id,
				asset_id : asset_id,
				class_type : itemCodeId
			},
			autoLoad : true,
			multiSelect : true,
			cellEditing : true,
			selModel : cellModel,
			pagingBar : false,
			editorMode : true,
			border : true,
			comboComp : getCompOptionComp(itemCodeId, authType),
			actionIcon : actionIcon,
			actionBtnEvent : actionBtnEvent
		};

		eval("gridComp" + [i] + " = createGridComp(setSelGridProp)");

		storeOnFn += 'gridComp' + [i] + '.store.on("load",';
		storeOnFn += 'function(gridCompStore){';
		storeOnFn += 'var itemsArray= tabComp.items.items;';
		storeOnFn += 'for(var i=0;i<itemsArray.length;i++){';
		storeOnFn += 'var tabItem = itemsArray[i];';
		storeOnFn += 'if(tabItem.id == gridComp' + [i] + '.id){';
		storeOnFn += 'var title = tabItem.title;';
		storeOnFn += 'logsvGrid = gridCompStore;';
		storeOnFn += 'tabItem.setTitle(title + " ["+gridCompStore.count()+"]");';
		storeOnFn += '}}});';
		if (itemCodeId == "LOGSV") {
			storeOnFn += 'gridComp' + [i] + '.store.on("update",';
			storeOnFn += 'function(gridCompStore){';
			storeOnFn += 'gridCompStore.commitChanges();';
			storeOnFn += 'gridCompStore.update();';
			storeOnFn += 'logsvGrid = gridCompStore;'; // 전역변수
			storeOnFn += '});';
		}

		eval(storeOnFn);
		eval('gridCompArr.push(gridComp' + [i] + ')');
	}

	// 자산상세정보 보기
	function showAssetDetailPopBtnEvent(grid, rowIndex, colIndex) {
		var record = grid.getStore().getAt(rowIndex);
		var rowData = record.raw;

		var asset_id = rowData["ASSET_ID"];
		var conf_id = rowData["CONF_ID"];
		var class_id = rowData["CLASS_ID"];
		var tangible_asset_yn = rowData["TANGIBLE_ASSET_YN"];
		var view_type = "pop_view";
		var entity_class_id = "";
		if (tangible_asset_yn == "N") {
			entity_class_id = class_id + "-0";
		} else {
			entity_class_id = class_id;
		}
		var param = "?asset_id=" + asset_id + "&conf_id=" + conf_id
				+ "&class_id=" + class_id + "&view_type=" + view_type
				+ "&entity_class_id=" + entity_class_id;
		showAssetDetailPop(param);
	}

	// 탭생성
	var tabBasicProperty = {
		tab_id : id + "Tab",
		width : '100%',
		// title : title,
		layout : 'center',
		item : gridCompArr,
		tools : btns
	}

	tabComp = createTabComponent(tabBasicProperty);

	attachCustomEvent('tabchange', tabComp, clickTab);

	attachBtnEvent(delRelBtn, removeConfGridRow);
	attachBtnEvent(addRelBtn, relConfSelectPop);

	var outerPanel = Ext.create('Ext.form.Panel', {
				id : id + 'Panel',
				width : '100%',
				title : title,
				defaults : {
					layout : 'fit',
					flex : 1
				},
				items : [{
							flex : 1,
							items : tabComp
						}]
			});

	return outerPanel;
}

/**
 * 설치 소프트웨어 컴포넌트 -20140225 정정윤 추가
 * 
 * @param compProperty
 * @returns {String}
 */
function createInstalledSwList(compProperty) {

	var that = convertDomToArray(compProperty);
	var installSwGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];
	var selModel = false;
	var btns = [];
	var addSwBtn = createBtnComp({
				visible : true,
				label : '추가',
				id : 'addSwBtn',
				icon : '/itg/base/images/ext-js/common/icons/add.png',
				margin : '0 5 0 0'
			});
	var delSwBtn = createBtnComp({
				visible : true,
				label : '삭제',
				id : 'delSwBtn',
				icon : '/itg/base/images/ext-js/common/icons/delete.png',
				margin : '0 0 0 0'
			});
	var detailSwBtn = createBtnComp({
				visible : true,
				label : '상세보기',
				id : 'detailSwBtn',
				icon : '/itg/base/images/ext-js/common/icons/bullet_detail.gif',
				margin : '0 5 0 0'
			});
	var cellModel = false;

	// 삭제 버튼 핸들러
	var removeSwGridRow = function() {
		var selRows = gridSelectedRows(id);
		var adYnCnt = 0;
		for (var i = 0; i < selRows.length; i++) {
			if (selRows[i].data.SW_AD_YN == 'Y') {
				adYnCnt++;
			}
		}
		if (adYnCnt > 0) {
			alert("자동수집 소프트웨어는 삭제할 수 없습니다.");
			return;
		} else {
			if (isSelected(id)) {
				Ext.suspendLayouts();
				for (var i = 0; i < selRows.length; i++) {
					installSwGrid.store.remove(selRows[i]);
				}
				Ext.resumeLayouts(true);
			} else {
				showMessageBox({
							title : "경고",
							msg : "선택된 정보가 없습니다.",
							type : "ALERT"
						});
			}
		}
	}

	// 추가 버튼 핸들러
	var installSwListSelectPop = function() {
		createInstallSwListPop(compProperty);
	}

	// 상세보기 버튼 핸들러
	var detailSwListPop = function() {
		var selRows = gridSelectedRows(id);
		if (selRows.length == 1) {
			createInstallSwDetailPop(compProperty, selRows[0].data, authType);
		} else if (selRows.length > 1) {
			showMessageBox({
						title : "경고",
						msg : "상세보기는 하나만 선택하실 수 있습니다.",
						type : "ALERT"
					});
		} else {
			showMessageBox({
						title : "경고",
						msg : "선택된 정보가 없습니다.",
						type : "ALERT"
					});
		}
	}

	// 사용자가 그리드 수정권한이 있을경우
	if (authType == "U") {
		btns = [detailSwBtn, addSwBtn, delSwBtn];
		cellModel = true;
	} else {
		btns = [detailSwBtn];
		cellModel = false;
	}

	var gridProps = {
		id : id,
		title : title,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsInsSwList.do' // TODO 해당 서비스로 붙일 것.
		,
		gridHeight : 200,
		params : {
			conf_id : conf_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : false,
		selModel : cellModel,
		pagingBar : false,
		editorMode : true,
		border : true,
		toolBarComp : btns
	}
	// 그리드 생성
	installSwGrid = createGridComp(gridProps);

	// 버튼 이벤트
	attachBtnEvent(delSwBtn, removeSwGridRow);
	attachBtnEvent(addSwBtn, installSwListSelectPop);
	attachBtnEvent(detailSwBtn, detailSwListPop);

	return installSwGrid;
}
/**
 * 유지보수 입력 폼
 * 
 * @param compProperty
 */
function createMaintForm(compProperty) {

	var that = convertDomToArray(compProperty);
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var asset_id = that["asset_id"];
	var authType = that["authType"];
	var get_dt = that["itemArray"][0]["GET_DT"];
	var maint_yn = that["itemArray"][0]["MAINT_YN"];
	var warranty = that["itemArray"][0]["WARRANTY"];
	var free_as_start_date = that["itemArray"][0]["FREE_AS_START_DATE"];
	var free_as_end_date = that["itemArray"][0]["FREE_AS_END_DATE"];
	var as_start_date = that["itemArray"][0]["AS_START_DATE"];
	var as_end_date = that["itemArray"][0]["AS_END_DATE"];
	var maint_date = new Date();
	var maint_year = maint_date.getFullYear();

	// var free_as_start_date = createDateTimeStamp('DHM', {year:0, month:+1,
	// day:0, hour:0, min:0});
	// var free_as_end_date = createDateTimeStamp('DHM', {year:0, month:0,
	// day:0, hour:0, min:0});
	// var as_start_date = createDateTimeStamp('DHM', {year:0, month:0, day:0,
	// hour:0, min:0});
	// var as_end_date = createDateTimeStamp('DHM', {year:0, month:0, day:0,
	// hour:0, min:0});

	// 사용자가 그리드 수정권한이 있을경우
	// if(authType == "U"){
	// btns = [addMngBtn,delMngBtn];
	// / cellModel = true;
	// }else{
	// / btns = null;
	// / cellModel = false;
	// }
	//		
	//		
	var dd = '2015-02-04';

	var setEditorFormProp = {
		id : id,
		title : title,
		editOnly : true,
		columnSize : 3,
		tableProps : [
				// row1
				{
			colspan : 1,
			item : createTextFieldComp({
						label : "품목관리번호",
						id : 'item_code',
						name : 'item_code',
						maxLength : 100,
						readOnly : true,
						width : 300
					})
		}, {
			colspan : 1,
			item : createNumberFieldComp({
						label : "계약년도",
						id : 'asset_ma_year',
						name : 'asset_ma_year',
						vtype : 'number',
						minValue : maint_year,
						value : maint_year,
						maxLength : 4,
						notNull : false,
						width : 215,
						notNull : true
					})
		}, {
			colspan : 1,
			item : createDateFieldComp({
						label : "계약체결일",
						id : 'as_date',
						name : 'as_date',
						dateType : 'D',
						format : 'Y-m-d',
						width : 300,
						readOnly : true
					})
		}		// HMMS
				// row2
				, {
					colspan : 1,
					item : createTextFieldComp({
								label : "용도분류",
								id : 'ma_prop13',
								name : 'ma_prop13',
								tdType : 'wide',
								maxLength : 200,
								readOnly : true,
								width : 300
							})
				}, {
					colspan : 1,
					item : createDateFieldComp({
								label : "무상유지보수시작일",
								id : 'free_as_start_date',
								name : 'free_as_start_date',
								dateType : 'D',
								format : 'Y-m-d',
								dateValue : free_as_start_date,
								width : 300,
								notNull : true
							})
				} // 설치일의 그 다음달의 1일부터
				, {
					colspan : 1,
					item : createDateFieldComp({
								label : "무상유지보수종료일",
								id : 'free_as_end_date',
								name : 'free_as_end_date',
								dateType : 'D',
								format : 'Y-m-d',
								dateValue : free_as_end_date,
								width : 300,
								notNull : true
							})
				} // 무상유지보수 시작일 + warranty
				// row3
				, {
					colspan : 1,
					item : createTextFieldComp({
								label : "계약수량",
								id : 'as_item_qty',
								name : 'as_item_qty',
								tdType : 'wide',
								maxLength : 100,
								width : 300
							})
				}// 수량은 마스터 수량 자동 입력
				, {
					colspan : 1,
					item : createDateFieldComp({
								label : "계약시작일",
								id : 'as_start_date',
								name : 'as_start_date',
								dateType : 'D',
								format : 'Y-m-d',
								setValue : dd,
								width : 300,
								notNull : true
							})
				} // 자동으로 무상유지보수 종료일의 그 다음달 1일
				, {
					colspan : 1,
					item : createDateFieldComp({
								label : "계약종료일",
								id : 'as_end_date',
								name : 'as_end_date',
								dateType : 'D',
								format : 'Y-m-d',
								dateValue : as_end_date,
								width : 300,
								notNull : true
							})
				} // 자동으로 무상유지보수 종료일의 그 해 마지막 날(12/31)
				// row4
				, {
					colspan : 1,
					item : createOutVendorSelectPopComp({
								label : "계약업체코드",
								id : 'pt_com_code',
								name : 'pt_com_code',
								hiddenName : 'pt_com_code',
								tdType : 'wide',
								maxLength : 100,
								popUpText : '업체선택',
								notNull : true,
								width : 335
							})
				} // 업체 팝업 콤포넌트 TODO
				, {
					colspan : 1,
					item : createOutVendorSelectPopComp({
								label : "공급업체코드",
								id : 'sale_com_code',
								name : 'sale_com_code',
								hiddenName : 'sale_com_code',
								tdType : 'wide',
								maxLength : 100,
								popUpText : '업체선택',
								notNull : true,
								width : 335
							})
				} // 업체 팝업 콤포넌트
				, {
					colspan : 1,
					item : createOutVendorSelectPopComp({
								label : "서비스업체코드",
								id : 'as_com_code',
								name : 'as_com_code',
								hiddenName : 'as_com_code',
								tdType : 'wide',
								maxLength : 20,
								popUpText : '업체선택',
								width : 335
							})
				} // 업체 팝업 콤포넌트
				// row5
				, {
					colspan : 1,
					item : createTextFieldComp({
								label : "MA요율",
								id : 'pt_ma_rate',
								name : 'pt_ma_rate',
								tdType : 'wide',
								maxLength : 100,
								readOnly : true,
								width : 300
							})
				}, {
					colspan : 1,
					item : createTextFieldComp({
								label : "서비스금액",
								id : 'pt_as_price',
								name : 'pt_as_price',
								tdType : 'wide',
								maxLength : 100,
								readOnly : true,
								width : 300
							})
				}, {
					colspan : 1,
					item : createTextFieldComp({
								label : "총MA금액",
								id : 'pt_contract_price',
								name : 'pt_contract_price',
								tdType : 'wide',
								maxLength : 20,
								readOnly : true,
								width : 300
							})
				}
		// row6
		// row7
		// row8
		]
	}

	var eidtorFormPanel = createEditorFormComp(setEditorFormProp);

	var free_as_end_date_comp = Ext.getCmp('free_as_end_date');
	attachCustomEvent('change', free_as_end_date_comp, as_set_date);

	// var paramMap = ();
	// jq.ajax({ type:"POST"
	// , url: "/itg/itam/opms/updateAssetMaintInfo.do"
	// , contentType: "application/json"
	// , dataType: "json"
	// , async: false
	// , data : getArrayToJson(paramMap)
	// , success:function(data){
	// if( data.success ){
	// //폼에 데이터 init
	// var editForm = Ext.getCmp(id);
	// editForm.initData();
	// editForm.setDataForMap(data);
	// alert('SUCCESS');
	// }else{
	// alert('FAIL');
	// }
	// }
	// });

	Ext.getCmp('as_start_date').setValue(as_start_date);
	Ext.getCmp('as_end_date').setValue(as_end_date);
	Ext.getCmp('free_as_start_date').setValue(free_as_start_date);
	Ext.getCmp('free_as_end_date').setValue(free_as_end_date);

	if (maint_yn = "Y") {
		if (get_dt != "" && get_dt != null) {
			formEditable({
						id : id,
						editorFlag : true
/* excepArray :['up_model_id','up_model_nm','model_id'] */});

		} else {
			formEditable({
						id : id,
						editorFlag : false
/* excepArray :['up_model_id','up_model_nm','model_id'] */});
		}
	}
	return eidtorFormPanel;

}
function as_set_date() {

	var free_as_end_date_t = Ext.getCmp('free_as_end_date').getValue;
	var as_start_date_cmp = Ext.getCmp('as_start_date');
	var as_end_date_cmp = Ext.getCmp('as_end_date');
}

/**
 * 할당 라이선스 정보 그리드 컴포넌트 -20140303 정정윤 추가
 * 
 * @param compProperty
 * @returns {String}
 */
function createSwLicenceList(compProperty) {

	var that = convertDomToArray(compProperty);
	var swAssetLicenceAssignedGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];

	var gridProps = {
		id : id,
		title : title
		// ,resource_prefix: 'grid.itam.sam.assignedSw'
		,
		gridFormItem : gridFormItem,
		url : '/itg/itam/sam/selectLicenceAssignedSwList.do',
		gridHeight : 500,
		params : {
			conf_id : conf_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : true
		// ,selModel : false
		// ,pagingBar : false
		,
		editorMode : true,
		border : true
	}

	swAssetLicenceAssignedGrid = createGridComp(gridProps);

	ss = Ext.getCmp(id);

	// 2014.04.02 정정윤 추가 시작

	// 할당된 라이선스가 있으면 소프트웨어 그룹 선택(변경) 불가
	// 할당된 라이선스가 있으면 소프트웨어 그룹 팝업 버튼에 disabled 걸어줌

	// 그리드 스토어 로드가 완료되면
	swAssetLicenceAssignedGrid.getStore().on('load', function(store, records) {
		// 상위 아이템을 탐색
		var specMenualForm = swAssetLicenceAssignedGrid.up().items.items[0];
		// 타이틀이 일반정보 일 때
		if (specMenualForm.title == "일반정보") {
			// 소프트웨어 그룹 ID 선택 팝업이 존재한다면
			if (specMenualForm.getForm().findField("SW_GROUP_ID_POP_NM")) {
				// 할당 라이선스 목록이 있다면
				if (records.length > 0) {
					// 소프트웨어 그룹은 변경할 수 없음
					var swGroupCompId = specMenualForm.getForm()
							.findField("SW_GROUP_ID_POP_NM").up().id;
					var startCompId = swGroupCompId.replace("_Container", "");
					var popClrBtnId = startCompId + "_PopClrBtn";
					var popOpenBtnId = startCompId + "_PopOpBtn";

					Ext.getCmp(popClrBtnId).setDisabled(true);
					Ext.getCmp(popOpenBtnId).setDisabled(true);
				}
			}
		}
			// 2014.04.02 정정윤 추가 끝
	});
	return swAssetLicenceAssignedGrid;
}

/**
 * 논리 이력 그리드 컴포넌트 -20140513 정정윤 추가
 * 
 * @param compProperty
 * @returns {String}
 */
function createLogicHisList(compProperty) {
	var that = convertDomToArray(compProperty);
	var logicHisGrid = "";
	var id = that["id"];
	var title = that["title"];
	var conf_id = that["conf_id"];
	var authType = that["authType"];
	var gridFormItem = that["gridFormItem"];

	var gridProps = {
		id : id,
		title : title,
		gridFormItem : gridFormItem,
		url : '/itg/itam/opms/selectOpmsLogicHisList.do',
		gridHeight : 500,
		params : {
			conf_id : conf_id
		},
		autoLoad : true,
		multiSelect : true,
		cellEditing : true,
		editorMode : true,
		border : true
	}

	logicHisGrid = createGridComp(gridProps);

	return logicHisGrid;
}