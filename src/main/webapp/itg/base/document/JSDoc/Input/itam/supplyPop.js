var popUp_comp={};
var removeRow="";
//INFRA 데이터추가 팝업생성
function createInfraPopUp(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
								
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	var theForm = Ext.getCmp('infraInsert_form');
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
			id: 'infraInsert_form',
			title: getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00001'}), 
			editOnly: true,
			border: true,
			columnSize: 2,
			tableProps: [
				{colspan:1, item : createTreeSelectPopComp({popId:'classSelectPop', readOnly:true, popUpTitle:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00004' }), targetName:'class_nm', targetHiddenName:'class_id', treeId:'classSelectTree', url: '/itg/itam/amdb/searchAmClassAllTree.do', targetLabel:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00005' }), btnId:'classbtnId', params: {classType:'HW'}, rnode_text: getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00005' }), expandLevel: 3, notNull:true, width: 270})},
				{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00006' }), name:'conf_nm', maxLength:300, notNull:true, width: 300})},
				{colspan:1, item : createOutVendorSelectPopComp({id:'vendorInfraSelectPop', popUpText: getConstText({ isArgs: true, m_key: 'res.common.label.maker' }), label:getConstText({ isArgs: true, m_key: 'res.common.label.maker' }), name:'vendor_nm', hiddenName:'vendor_id', targetForm:'infraInsert_form', gridId:'vendorInfraSelectGrid', notNull:true, width: 242})},
				{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00008' }), name:'model_nm', maxLength:200, notNull:true, width: 300})},
				{colspan:1, item : createNumberFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.00022' }), name:'purchs_qy', width: 300, value: 1, minValue: 1})},
				{colspan:1, item : createTextMoneyFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00013' }), name:'cost', vtype: 'number',maxLength:15, width: 300})}
			], 
			hiddenFields:[{name:'supply_id'},{name:'rnum'}]
		}
		
		var editorFormPanel = createEditorFormComp(setEditorFormProp);
		
		formEditable({id : 'infraInsert_form' , editorFlag : true , excepArray :['class_nm','vendor_nm','user_nm']});
		
		var insertBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
	        handler: function() {
	        	var theForm = Ext.getCmp('infraInsert_form');
				if(theForm.isValid()){	    	        			        	
		        	var class_id = theForm.getInputData().class_id;
		        	var class_nm = theForm.getFieldValue('class_nm');
		        	var conf_nm = theForm.getFieldValue('conf_nm');
		        	var vendor_id = theForm.getFieldValue('vendor_id');
		        	var vendor_nm = theForm.getFieldValue('vendor_nm');
		        	var model_nm = theForm.getFieldValue('model_nm');
		        	var purchs_qy = theForm.getFieldValue('purchs_qy');
		        	var cost = theForm.getFieldValue('cost');
		        	var rnum = theForm.getInputData().rnum;	 	        	
		        	if(rnum == null || rnum == ""){
		        		rnum = Ext.getCmp('supplyAssetInfraGrid').getStore().data.length + 1
			     		getGrid("supplyAssetInfraGrid").getStore().add({"RNUM":rnum,"CLASS_ID":class_id,"CLASS_NM":class_nm,"CONF_NM":conf_nm,"VENDOR_ID":vendor_id,"VENDOR_NM":vendor_nm,"MODEL_NM":model_nm,"COST":cost,"PURCHS_QY":purchs_qy});
		        	}else{
		        		getGrid('supplyAssetInfraGrid').getStore().removeAt(removeRow);
			     		getGrid("supplyAssetInfraGrid").getStore().insert(removeRow,{"RNUM":rnum,"CLASS_ID":class_id,"CLASS_NM":class_nm,"CONF_NM":conf_nm,"VENDOR_ID":vendor_id,"MODEL_NM":model_nm,"COST":cost,"PURCHS_QY":purchs_qy});
		        	}
		     		inputTablePop.close();
				}else{
					showMessage(getConstText({isArgs: true, m_key: 'msg.common.00012'}));
				}		     		
	        }
		});
		
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				inputTablePop.close();
			}
		});
		
		inputTablePop = Ext.create('nkia.custom.windowPop',{

			id: that['id'],
			title: getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00001'}), 
			width: 700,
			autoDestroy: false,
			resizable:false, 
			//bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn, closeBtn],
			bodyStyle: that['bodyStyle'],
			items: [editorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = inputTablePop;
		
	} else {
		inputTablePop = popUp_comp[that['id']];
		Ext.getCmp('infraInsert_form').initData();
	}
	
	return inputTablePop;
}


//SW 데이터추가 팝업생성
function createSWPopUp(setPopUpProp){

	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var theForm = Ext.getCmp('swInsert_form');
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
				id: 'swInsert_form',
				title: getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00001'}),
				editOnly: true,
				border: true,
				columnSize: 2,
				tableProps : [
					            //row1
								//{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00002' }), name:'supply_id', width: 300})}
					           //,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00003' }), name:'rnum', width: 300})}
					            //row2
					           {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00006' }), name:'conf_nm', maxLength:300, notNull:true, width: 300, maxLength:200})}
					           ,{colspan:1, item : createTreeSelectPopComp({popId:'classSelectPop_SW', popUpTitle: getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00004' }), targetName:'class_nm', targetHiddenName:'class_id', treeId:'classSelectTree_SW', url: '/itg/itam/amdb/searchAmClassAllTree.do', targetLabel:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00005' }), btnId:'classSWbtnId', params: {classType:'SW'}, rnode_text: getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00005' }), expandLevel: 3, notNull:true, width: 270})}
					            //row3
					           ,{colspan:1, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00006' }), id:'licence_type', name:'licence_type', code_grp_id:'LICE_TYPE', attachAll:true, notNull: true, width: 300})}
					           ,{colspan:1, item : createTextMoneyFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00008' }),id:'licence_cnt', name:'licence_cnt', vtype: 'number', maxLength:10, notNull:true, width: 300, value: '0'})}
					            //row4
					           ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.common.label.maker' }), name:'vendor_id', maxLength:20, notNull:true, width: 300})}
					           ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.infra.00008' }), name:'model_nm', maxLength:200, notNull:true, width: 300})}
					            //row5
					           ,{colspan:1, item : createDateFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00009' }), name:'licence_start_dt', dateType:'D'})}
					           ,{colspan:1, item : createDateFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00010' }), name:'licence_end_dt', dateType:'D'})}
					            //row6
					           ,{colspan:2, item : createTextMoneyFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00011' }), name:'tot_cost', vtype: 'number', maxLength:15, width: 300})}
				            ], hiddenFields:[{name:'supply_id'},{name:'rnum'}]
		}
		
		var editorFormPanel = createEditorFormComp(setEditorFormProp);
		Ext.getCmp('licence_type').on('select', popComboLink);
		
		formEditable({id : 'swInsert_form' , editorFlag : true , excepArray :['class_nm','vendor_nm']});
		
		var insertBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
	        handler: function() {
	        	var theForm = Ext.getCmp('swInsert_form');
				if(theForm.isValid()){	    	        	
					var checkFlag = swVaildCheck();
					if(checkFlag == "N"){
						return;
					}    
	
		        	var class_id = Ext.getCmp('swInsert_form').getInputData().class_id;
		        	var class_nm = Ext.getCmp('swInsert_form').getFieldValue('class_nm');
		        	var conf_nm = Ext.getCmp('swInsert_form').getFieldValue('conf_nm');
		        	var licence_type = Ext.getCmp('swInsert_form').getFieldValue('licence_type');
		        	var licence_cnt = Ext.getCmp('swInsert_form').getFieldValue('licence_cnt');
		        	var vendor_id = Ext.getCmp('swInsert_form').getFieldValue('vendor_id');
		        	var model_nm = Ext.getCmp('swInsert_form').getFieldValue('model_nm');	        	
		        	var licence_start_dt = Ext.getCmp('swInsert_form').getInputData().licence_start_dt;
		        	var licence_end_dt = Ext.getCmp('swInsert_form').getInputData().licence_end_dt;
		        	var licence_start_dt_day = Ext.getCmp('swInsert_form').getInputData().licence_start_dt;
		        	var licence_end_dt_day = Ext.getCmp('swInsert_form').getInputData().licence_end_dt;
		        	var tot_cost = Ext.getCmp('swInsert_form').getFieldValue('tot_cost');

		        	//var totalCost = Number(cost) * Number(licence_cnt);
		        	//var totalCost = Number(cost);
		        	//var tot_cost = totalCost;
		        	//var tot_cost = cost;
		        	var rnum = Ext.getCmp('swInsert_form').getInputData().rnum;
		        	
		        	if(rnum == null || rnum == ""){
		        		rnum = Ext.getCmp('supplyAssetSWGrid').getStore().data.length + 1;
		        		getGrid("supplyAssetSWGrid").getStore().add({"RNUM":rnum,"CLASS_ID":class_id,"CLASS_NM":class_nm,"LICENCE_TYPE":licence_type,"LICENCE_CNT":licence_cnt,"LICENCE_START_DT":licence_start_dt,"LICENCE_END_DT":licence_end_dt,/*"COST":cost,*/"TOT_COST":tot_cost,"CONF_NM":conf_nm,"VENDOR_ID":vendor_id,"MODEL_NM":model_nm,"LICENCE_START_DT_DAY":licence_start_dt_day,"LICENCE_END_DT_DAY":licence_end_dt_day});
		        	}else{
		        		getGrid('supplyAssetSWGrid').getStore().removeAt(removeRow);
		        		getGrid("supplyAssetSWGrid").getStore().insert(removeRow,{"RNUM":rnum,"CLASS_ID":class_id,"CLASS_NM":class_nm,"LICENCE_TYPE":licence_type,"LICENCE_CNT":licence_cnt,"LICENCE_START_DT":licence_start_dt,"LICENCE_END_DT":licence_end_dt,/*"COST":cost,*/"TOT_COST":tot_cost,"CONF_NM":conf_nm,"VENDOR_ID":vendor_id,"MODEL_NM":model_nm,"LICENCE_START_DT_DAY":licence_start_dt_day,"LICENCE_END_DT_DAY":licence_end_dt_day});
		        	}
			  		inputTablePop.close();
			  		
				}else{
					showMessage(getConstText({isArgs: true, m_key: 'msg.common.00012'}));
				}		     		
	        }
		});
 		
 		var closeBtn = new Ext.button.Button({
 			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
 			handler: function() {
 				inputTablePop.close();
 			}
 		});
 		
		inputTablePop = Ext.create('nkia.custom.windowPop',{

			id: that['id'],
			title: getConstText({ isArgs: true, m_key: 'res.label.itam.supply.sw.00001'}),  
			width: 700,  
			autoDestroy: false,
			resizable:false, 
			//bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn, closeBtn],
			bodyStyle: that['bodyStyle'],
			items: [editorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = inputTablePop;
		
	} else {
		inputTablePop = popUp_comp[that['id']];
		theForm.initData();
	}
	
	return inputTablePop;
}

function assetCheck(record, rowIndex) {
	var clickRecord = record.raw;
	removeRow = rowIndex;
}

function swVaildCheck(){
	
	var theForm = Ext.getCmp('swInsert_form');
	var licence_start_dt = theForm.getFieldValue('licence_start_dt_day');
	var licence_end_dt = theForm.getFieldValue('licence_end_dt_day');
	
	var checkFlag = "Y";
	if(licence_start_dt > licence_end_dt){
		showMessage(getConstText({isArgs: true, m_key: 'msg.itam.supply.00001'}));
		checkFlag = "N";
	}
	
	return checkFlag;
	
}

function createSupplySelectPopUp(setPopUpProp){
	var storeParam = {};
	var that = this;
	var supplySelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
		
	if(popUp_comp[that['id']] == null) {
		
		var supplySearchBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.search' }), margin: getConstValue('WITH_BUTTON_MARGIN')});
		
		//공급계약리스트 검색 폼
		var setSearchUserFormProp = {
			id: that['id']+'searchForm',
			title: getConstText({ isArgs: true, m_key: 'res.common.search' }),
			target: that['id']+'_grid',
			columnSize: 2,
			tableProps : [{
							colspan:1, item: createTextFieldComp({name:'supply_nm', padding: getConstValue('WITH_FIELD_PADDING'), label:getConstText({ isArgs: true, m_key: 'res.label.itam.supply.00003' })})
						},{
							colspan:1, item: supplySearchBtn
						}]
		}
		var supplySearchForm = createSeachFormComp(setSearchUserFormProp);
		
		var setGridProp = {	
			id: that['id']+'_grid',							
			gridHeight: 265,
			resource_prefix: 'grid.itam.supply',				
			url: getConstValue('CONTEXT') + '/itg/itam/supply/searchSupply.do',
			params: that['params'],
			pagingBar : true
		};
		var supplyListGrid = createGridComp(setGridProp);
		
		// 패널 병합
		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			layout: 'vbox',
			items: [supplySearchForm, supplyListGrid]
		});
		
		var tagetUserIdField = that['tagetUserIdField'];
		var tagetUserNameField = that['tagetUserNameField'];
		var tagetEtcField = that['tagetEtcField'];
		
		var acceptBtn = createBtnComp({
			label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct', 
			scale:'medium'
		});
				
		function clickSearchBtn() {
			var searchForm = Ext.getCmp(that['id']+'searchForm').getInputData();
			var supplyListGridStore = supplyListGrid.getStore();
			supplyListGridStore.proxy.jsonData = searchForm;
			supplyListGridStore.load();
		}

		function dbSelectEvent(){
			var selectedRecords = supplyListGrid.view.selModel.getSelection();
			if(selectedRecords.length <1){
				showMessage(getConstText({ isArgs: true, m_key: 'msg.system.result.00031' }));
			}else{
				if(that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
				} else {
					var supply_id = nullToSpace(selectedRecords[0].raw.SUPPLY_ID);
					var supply_nm = nullToSpace(selectedRecords[0].raw.SUPPLY_NM);
					
					if(that['targetFormId'] != null){
						Ext.getCmp(that['targetFormId']).setFieldValue('supply_id', supply_id);
						Ext.getCmp(that['targetFormId']).setFieldValue('supply_nm', supply_nm);
					}
					supplySelectPop.close();						
				}
			}
		}

		attachBtnEvent(acceptBtn, dbSelectEvent);
		attachBtnEvent(supplySearchBtn, clickSearchBtn);
		attachCustomEvent('beforeitemdblclick', supplyListGrid, dbSelectEvent);
		
		supplySelectPop = Ext.create('nkia.custom.windowPop',{
			id: that['id']+'_Pop',
			title: that['popUpText'],  
			height: 412, 
			width: 600,  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = supplySelectPop;
		
	} else {
		supplySelectPop = popUp_comp[that['id']];
	}
	
	return supplySelectPop;
}


//,{colspan:1, item : createBasePopComp({id:'basePop_1', popUpText: '팝업텍스트', label:'라벨', name:'pgm_nm', hiddenName:'pgm_cd', pop_type: 'BZ'})}
function createBasePopComp(compProperty) {
	var that = {};
	for( var prop in compProperty ){
		that[prop] = compProperty[prop];
	}
	
	var labelWidth = getConstValue('LABEL_WIDTH');
	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	if(that['width'] != null) {
		fieldWidth = that['width'];
	}
	
	var nameField = createTextFieldComp({
			label: that['label'],
			name:that['name'],
			notNull:( that['notNull'] ) ? that['notNull'] : false,
			width: fieldWidth,
			readOnly: true});
	
	var idField =  Ext.create('Ext.form.field.Hidden', {
		xtype : 'hidden',
		name : that['hiddenName']
	});
	
	var t_comp = null;
	t_comp = {
		xtype: 'fieldcontainer',
		height: ( that['height'] ) ? that['height'] : "",
		width: getConstValue('DEFAULT_FIELD_WIDTH'),
		combineErrors: true,
		layout:'hbox',
		items: [
			nameField, idField,
			createPopBtnComp({
				handler: function(){
					var setPopUpProp = {	
						id: that['id'],
						title : that['title'],
						popUpText: that['popUpText'],
						tagetIdField: idField,
						tagetNameField: nameField,
						height: 504,								
						width: 590,
						buttonAlign : 'center',
						bodyStyle: 'background-color: white;',
						pop_type: that['pop_type']
					}
					var popUp = createBasePop(setPopUpProp);
					popUp.show();
				}
			}),
			createClearBtnComp({
            	handler: function(){
					nameField.setValue('');
					idField.setValue('');
				}
			})
        ]
	} 
	return t_comp;
}

function createBasePop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var selectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		
		//조회버튼
		var searchBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			})
			, ui : 'correct'
			, handler : function() {
				popSearchBtnEvent();
			}
		});
		
		//초기화버튼
		var initBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.reset'
			})
			, handler : function() {
				Ext.getCmp(that['id'] + '__searchForm').initData();
			}
		});
		
		function popSearchBtnEvent(){
			var searchForm = Ext.getCmp(that['id'] + '__searchForm');
			var paramMap = searchForm.getInputData();
			paramMap['pop_type'] = that['pop_type'];
			Ext.getCmp(that['id'] + '__Grid').searchList(paramMap);
		}		
		
		var setSearchFormProp = {
				id: that['id'] + '__searchForm',
				target: grid,
				enterFunction: popSearchBtnEvent,
				formBtns: [searchBtn, initBtn],			
				columnSize: 1,
				tableProps : [{colspan:1, item : createTextFieldComp({label:'사업명', name:'data', width: 350})}
				             ]
		}
		var searchFormPanel = createSeachFormComp(setSearchFormProp);	
		
		var setGridProp = {
			context : getConstValue('CONTEXT'),
			id : that['id'] + '__Grid',
			title : that['title'],
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.requirement.bzPop',
			url : getConstValue('CONTEXT') + '/itg/itam/requirementIns/searchPopData.do',
			params : {pop_type: that['pop_type']},
			pagingBar : true,
			pageSize : 20
		};
		var grid = createGridComp(setGridProp);

		var tagetIdField = that['tagetIdField'];
		var tagetNameField = that['tagetNameField'];
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				selectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
			, ui : 'correct'
			, handler : function() {
				dbSelectEvent();
			}
		});

		/** * Private Function Area Start ******************** */

		function dbSelectEvent() {
			var selectedRecords = grid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage("#springMessage('msg.common.00002')");
			} else {

				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var data_id = selectedRecords[0].raw.ID;
					var data_nm = selectedRecords[0].raw.DATA;
					tagetIdField.setValue(data_id);
					tagetNameField.setValue(data_nm);
					selectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', grid, dbSelectEvent);

		selectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ searchFormPanel, grid ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = selectPop;

	} else {
		selectPop = popUp_comp[that['id']];
	}

	return selectPop;
}


function popComboLink(command){

	switch(command.id) {
		case 'licence_type':
			var value = Ext.getCmp('swInsert_form').getFieldValue('licence_type');
			if(value == '00004'){
				Ext.getCmp('licence_cnt').setDisabled(true);
				Ext.getCmp('licence_cnt').setValue(0);
			}else{
				Ext.getCmp('licence_cnt').setDisabled(false);
			}
		break;
	}
}


var searchGrid;
function createSelectDrimsPop(setPopUpProp){
	var storeParam = {};
	var that = this;
	var setDrimsPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		
		searchGrid = that['id']+'Grid';
		
		// 조회 그리드
		var setSearchGridProp = {	
				id: that['id']+'Grid',
				title : 'DRIMS 연계 데이터',
				gridHeight: 210,
				resource_prefix: that['sPrefix'],				
				url: that['url'],
				params: ( that['params'] ) ? that['params'] : null,
				selModel: true,
				pagingBar : true,
				pageSize : 10,
				autoLoad: true
		};
		var drimsSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
				id: that['tGridId'],
				title : '선택된 DRIMS 연계 데이터',
				gridHeight: 187,
				resource_prefix: that['tPrefix'],				
				params: null,		
				pagingBar : false,
				autoLoad: false,
				dragPlug: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
				actionBtnEvent: actionBtnEvent,
				isMemoryStore: true
		};
		var drimsSaveListGrid = createGridComp(setSaveGridProp);

		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.choice' }),
			handler: function() {
				checkGridRowCopyToGrid(that['id']+'Grid', that['tGridId'], { primaryKeys: ['CI_CONVDSC'] });
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var operDrimsSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				var searchForm = Ext.getCmp('drimsPopSearchForm');
				var paramMap = searchForm.getInputData();
				Ext.getCmp(that['id']+'Grid').searchList(paramMap);	
			}
		});
		
		var operDrimsSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp('drimsPopSearchForm').initData();
			}
		});
		
		var setDrimsSearchFormProp = {
			id: 'drimsPopSearchForm',
			formBtns: [operDrimsSearchBtn, operDrimsSearchInitBtn],
			columnSize: 3,
			enterFunction : clickPopSearchBtn,
			tableProps : [{colspan:1, item : createTextFieldComp({label:'공급계약ID', name:'ci_convdsc', width: 250, maxLength:200})}
						 ,{colspan:1, item : createTextFieldComp({label:'공급계약명', name:'ci_convcnm', width: 250, maxLength:200})}
						 ,{colspan:1, item : createTextFieldComp({label:'계약업체명', name:'ci_convend', width: 250, maxLength:200})}
						 ]
		}
		var searchDrimsForm = createSeachFormComp(setDrimsSearchFormProp);

		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var formPanel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			flex:1 ,
			items: [searchDrimsForm, drimsSearchListGrid, choiceBtnpanel, drimsSaveListGrid]
		});
		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			ui:'correct',
			handler: function() {
				
				if(!confirm(getConstText({ isArgs: true, m_key: 'msg.common.confirm.00001' }))){
					return false;
				}
				
				setViewPortMaskTrue();
				if(getGrid(that['tGridId']).getStore().data.items.length <1){
					alert('선택된 데이터가 없습니다.');
					setViewPortMaskFalse();
				}else{

					var paramMap = {};
					var items = getGrid(that['tGridId']).getStore().data.items;
					for(var i=0; i < items.length; i++){
						paramMap[i] = items[i].data.CI_CONVDSC;
					}
					
					jq.ajax({ type:"POST", url: '/itg/itam/supply/insertDrimsSupplyData.do', 
						contentType: "application/json", dataType: "json", async: false , data : getArrayToJson(paramMap), 
						success:function(data){
							if(data.success){
								showMessage(data.resultMsg);
				    			setViewPortMaskFalse();
				    			setDrimsPop.close();
				    			
				    			var searchForm = Ext.getCmp('supplySearchForm');
				    			var paramMap = searchForm.getInputData();
				    			Ext.getCmp('supply_grid').searchList(paramMap);
				    			
							}else{
								alert("getConstText({ isArgs: true, m_key: 'msg.common.00007' })");
								setViewPortMaskFalse();
							}
						}
					});					
				}
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				setDrimsPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		
		function actionBtnEvent(grid, rowIndex, colIndex){
			grid.getStore().removeAt(rowIndex); 
		};
		
		
		function clickPopSearchBtn() {
			var searchForm = Ext.getCmp('drimsPopSearchForm');
			var paramMap = searchForm.getInputData();
			Ext.getCmp(searchGrid).searchList(paramMap);	
		}
		
		/*********************** Private Function Area End ***/
		
		setDrimsPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [formPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setDrimsPop;
		
	} else {
		setDrimsPop = popUp_comp[that['id']];
	}
	
	return setDrimsPop;

}