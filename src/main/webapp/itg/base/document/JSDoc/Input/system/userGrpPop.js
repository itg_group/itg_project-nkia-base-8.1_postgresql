var popUp_comp={};

/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */
function createAuthAddPop(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var systemAuthSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var auth_map = {};
		var auth_id_array = that['auth_id_map'];
		auth_map['auth_id'] = auth_id_array;
		
		var setAuthGridProp = {	
				context: that['context'],						
				id: that['gridId'],	
				height: '100%',
				resource_prefix: that['resource_prefix'],				
				url: that['url'],	
				params: auth_map,		
				selModel: true,
				pagingBar : false,
				gridWidth: '100%',
				autoLoad: true,
				border: true,
				emptyMsg: that['emptyMsg']
		}
		var authGrid = createGridComp(setAuthGridProp);		
		var insertBtn = new Ext.button.Button({
	        text: '적용',
	        handler: function() {
	        	var selectedRecords = Ext.getCmp(that['gridId']).view.selModel.getSelection();
				if(selectedRecords.length <1){
					showMessage("권한을 선택하지 않았습니다.");
				}else{	
					checkGridRowCopyToGrid(that['gridId'], 'setSystemAuthGridProp', { primaryKeys: ['AUTH_ID'] });
					systemAuthSelectPop.close();
				}
	        }
		});
		
		systemAuthSelectPop = new Ext.Window({
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn],
			bodyStyle: that['bodyStyle'],
			items: [authGrid],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = systemAuthSelectPop;
		
	} else {
		systemAuthSelectPop = popUp_comp[that['id']];
		
		var auth_map = {};
		var auth_id_array = that['auth_id_map'];
		auth_map['auth_id'] = auth_id_array;
		var proGrid = getGrid(that['gridId']).getStore();
		proGrid.proxy.jsonData = auth_map;
		proGrid.load();
	}
	
	return systemAuthSelectPop;
}

/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */
function createProAuthAddPop(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var processAuthSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		// @@20130726 전민수  START 
		// 프로세스 권한 선택팝업 오픈시에 파람명이 틀려서 오류있어  pro_auth_id->proc_auth_id 수정하였음
		var proc_auth_map = {};
		var proc_auth_id_array = that['proc_auth_id_map'];
		proc_auth_map['proc_auth_id'] = proc_auth_id_array;

		var setProAuthGridProp = {	
				context: that['context'],						
				id: that['gridId'],	
				height: '100%',
				resource_prefix: that['resource_prefix'],				
				url: that['url'],	
				params: proc_auth_map,		
				selModel: true,
				pagingBar : false,
				gridWidth: '100%',
				autoLoad: true,
				border: true,
				emptyMsg: that['emptyMsg']
		}
		var proAuthGrid = createGridComp(setProAuthGridProp);
		// @@20130726 전민수  END 
		var insertBtn = new Ext.button.Button({
	        text: '적용',
	        handler: function() {
	        	var selectedRecords = Ext.getCmp(that['gridId']).view.selModel.getSelection();
				if(selectedRecords.length <1){
					showMessage("권한을 선택하지 않았습니다.");
				}else{
					checkGridRowCopyToGrid(that['gridId'], 'setProcessAuthGridProp', { primaryKeys: ['PROC_AUTH_ID'] });
					processAuthSelectPop.close();
				}
	        }
		});
		
		processAuthSelectPop = new Ext.Window({
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn],
			bodyStyle: that['bodyStyle'],
			items: [proAuthGrid],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = processAuthSelectPop;
		
	} else {
		processAuthSelectPop = popUp_comp[that['id']];
		
		var pro_auth_map = {};
		var pro_auth_id_array = that['pro_auth_id_map'];
		pro_auth_map['pro_auth_id'] = pro_auth_id_array;
		var proGrid = getGrid(that['gridId']).getStore();
		proGrid.proxy.jsonData = pro_auth_map;
		proGrid.load();
	}
	
	return processAuthSelectPop;
}

/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */
function createMenuViewPop(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var menuViewPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var pro_auth_map = {};
		var auth_id = that['auth_id'];
		pro_auth_map['auth_id'] = auth_id;
		
		var setTreeProp = {
				context: '${context}',						
				id : that['treeId'],
				url : that['url'],
				dynamicFlag : false,
				searchable : true,
				expandAllBtn : false,
				collapseAllBtn : false,
				params: pro_auth_map,
				expandLevel: 4,
				rootVisible: false
		}
		var menuTree = createTreeComponent(setTreeProp);
		
		menuViewPop = new Ext.Window({
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			bodyStyle: that['bodyStyle'],
			items: [menuTree],
			autoScroll: false,
			layout: 'fit',
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = menuViewPop;
		
	} else {
		menuViewPop = popUp_comp[that['id']];
	}
	
	return menuViewPop;
}