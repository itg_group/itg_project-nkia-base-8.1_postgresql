/**
 * 일반적인 자바스크립트 공통 
 * 
 */


/**
 * value값이 "na"인지를 비교하여 true|false 리턴
 * @param {} value
 * @return {}
 */
function isNa(value){
	var isNa = false;
	if(value != "na"){
		isNa = true;
	}
	return isNa;
}
//@@20131025 고한조 - 엑셀 이력용 Page Navigation 추출 START
function getIndexPage(){
	var getParent = function( target ){
		if( target.parent == null ){
			return false;
		}else{
			return true;
		}
	}
	
	var index = this;
	var prevPage;
	while( getParent(index) ){
		if( index.parent == index ){
			break;
		}
		
		prevPage= index;
		index = index.parent;
	}
	
	return {indexPage: index, prevPage: prevPage};
}
//@@20131025 고한조 - 엑셀 이력용 Page Navigation 추출 END

//@@20130711 전민수 START
//신규추가
/**
 * 엑셀파일 다운로드 Submit
 * @param form
 * @param fileId
 * @param naviInfo - index.nvf 를 통해 들어온 페이지가 아닌경우 반드시 입력해야 
 */
function excelFileDown(form, fileId, naviInfo){
	//@@ 20131025 고한조 - 엑셀 저장시 페이지 네비게이션 기록을 위한 수정 START
	var naviStr = "";
	if( naviInfo == null ){
		var page = getIndexPage();
		var navigation;
		if( page["indexPage"].getNavigation != null ){
			navigation = page["indexPage"].getNavigation(page["prevPage"]);
		}
		
		if( navigation != null ){
			// Object의 키를 가져와서 키 순서별로 입력한다.
			// Obejct의 키가 일정하지 않을 경우가 있기에 이와같이 사용
			for( var str in navigation ){
				// ID 항목 제외
				if( str.indexOf("Id") > -1 ){ continue; }
				if( naviStr != "" ){
					naviStr = naviStr + " > ";
				}
				
				naviStr = naviStr + navigation[str];
			}
		} else {
			//@@ 고한조 20131209 예외상황 추가 - 서비스 포탈에서 단일창 Popup이 될 시 현재 창의 navi가 제대로 표기되지 않는 문제
			if( page["indexPage"].name.indexOf("portal_pop") > -1 ){
				naviStr = "서비스 포탈 > 팝업 리스트";
			}
		}
	}else{
		naviStr = naviInfo;
	}
	//@@ 20131025 고한조 - 엑셀 저장시 페이지 네비게이션 기록을 위한 수정 END
	//console.log(form.parentElement);
	/*
	var bodyElement = document.body;
	var downForm = document.getElementById("excelDownForm");
	if( downForm == null ){
		downForm = document.createElement("form");
		bodyElement.appendChild(downForm);
	}
	*/
	var hiddenNavi;
	var hiddenFileId;
	if( form.naviInfo == null ){
		hiddenNavi = document.createElement("input");
		hiddenNavi.type = "hidden";
		hiddenNavi.name = "naviInfo";
		form.appendChild( hiddenNavi );
	}else{
		hiddenNavi = form.naviInfo;
	}
	if( form.fileId == null ){
		hiddenFileId = document.createElement("input");
		hiddenFileId.type = "hidden";
		hiddenFileId.name = "fileId";
		form.appendChild( hiddenFileId );
	}else{
		hiddenFileId = form.fileId;
	}
	
	hiddenFileId.value = fileId;
	hiddenNavi.value= naviStr;
	
	form.method = "post";
	form.target = "_self";
	form.action = "/itg/base/downloadExcelFile.do";
	form.submit();
	
	form.target = "";
	form.action = "";
}
//@@20130711 전민수 
//END

//@@20130711 전민수 START
//신규추가
/**
 * 엑셀다운로드 리스트 가져오기
 * @param url  		: 엑셀리스트 가져오기 위한 URL	
 * @param paramMap	: 쿼리문에 사용할 param
 */
function goExcelDownLoad(url, param, excelParam){
	var paramMap 			= {};
	paramMap["param"] 		= param;
	paramMap["excelParam"] 	= excelParam;
	
	var bodyElement = document.body;
	var form = document.getElementById("excelDownForm");
	if( form == null ){
		form = document.createElement("form");
		bodyElement.appendChild(form);
	}
	jq.ajax({ type:"POST"
		, url: url
		, contentType: "application/json"
		, dataType: "json"
		, async: true
		, data : getArrayToJson(paramMap)
		, success:function(data){
			excelFileDown(form, data.resultMap.file_id);
		}
	});
	
}
//@@20130711 전민수 
//END

//@@20130711 전민수 START
//신규추가
/**
 * 동적 엑셀다운로드 리스트 가져오기
 * @param url  		: 엑셀리스트 가져오기 위한 URL	
 * @param paramMap	: 쿼리문에 사용할 param
 * @param prefix	: 속성선택팝업에 출력할 속성데이터목록
 */
function goDynamicExcelDownLoad(paramMap, excelParam){
	var setPopUpProp = {	
		id: 'selectUserExcelDownPropPop',
		height: 450,								
		width: 500,
		param: paramMap,
		excelParam: excelParam
	}
	createExcelDownPropSelectPop(setPopUpProp);
	
}
//@@20130711 전민수 
//END

/**
 * 오즈 리포트 뷰어 생성용 팝업
 * 
 * @param reportName	: String - 가져올 리포트 명칭
 * @param param			: Object - 리포트에서 사용할 변수 리스트
 * 
 */
//@@20140113 고한조 추가
function createReportPopup(reportName, param){

	var reportParam = "";
	for( var p in param ){
		var str = "&" + p + "=" + param[p];
		
		reportParam = reportParam + str;
	}
	
	window.open('/itg/support/report/ozRemort.jsp?report_name=' + reportName + reportParam,'report','resizable=yes, scrollbars=yes, width=1000, height=1150');
	
	// 오즈 리포트 이력 저장
	param["name"] = reportName;
	
	// 현재 페이지 정보 호출
	var naviStr = "";
	var page = getIndexPage();
	var navigation;
	if( page["indexPage"].getNavigation != null ){
		navigation = page["indexPage"].getNavigation(page["prevPage"]);
	}
	
	if( navigation != null ){
		// Object의 키를 가져와서 키 순서별로 입력한다.
		// Obejct의 키가 일정하지 않을 경우가 있기에 이와같이 사용
		for( var str in navigation ){
			// id 항목 제외
			if( str.indexOf("Id") > -1 ){
				continue;
			}
			
			if( naviStr != "" ){
				naviStr = naviStr + " > ";
			}
			
			naviStr = naviStr + navigation[str];
		}
		
		param["navi"] = naviStr;
		param["title"] = navigation[str];
	}
	
	jq.ajax({ type:"POST"
		, url: "/itg/system/reportHistory/insertReportHistory.do"
		, contentType: "application/json"
		, dataType: "json"
		, async: true
		, data : getArrayToJson(param)
		, success:function(data){
			//console.log(data);
		}
	});
}
//@@20140113 고한조 추가
/**
 * 0, 1을 비교하여 true|false 리턴
 * @param {} value
 * @return {}
 */
function compareValueToBoolean(value){
	var isBoolean = false;
	if(value == "1"){
		isBoolean = true;
	}
	return isBoolean;
}


function getCodeDataList(code_grp_id, multi_code) {
	var codeList;
	var ajaxParam = {};
	
	ajaxParam["code_grp_id"] = code_grp_id;
	if(multi_code != null) {
		ajaxParam["multi_code"] = multi_code;		
	}

	codeList = getGenericDataList(getConstValue('CONTEXT') + "/itg/base/searchCodeDataList.do", ajaxParam)
	
	return codeList;
}

function getGenericDataList(url, listParam) {

	var dataList;

	if(listParam == null){
		listParam = {};
	}

	jq.ajax({
		type:"POST"
		, url: url
		, contentType: "application/json"
		, dataType: "json"
		, async: false
		, data: getArrayToJson(listParam)
		, success:function(data){
			if( data.success ){
				dataList = data.gridVO.rows;	
			}else{
				showMessage(data.resultMsg);
			}
			
		}

	})
	return dataList;
}

// Json Empty 검사
function isEmptyJson(obj) {
	var isEmpty = true;
	for( key in obj ){
		isEmpty = false;
		break;
	}
    return isEmpty;
}


/**
 * List에 데이터 일치 여부판단
 * @param {} dataArray
 * @param {} matchName
 * @return {}
 */
function isExistDataForArray(dataArray, matchName){
	var isExist = false;
	if( typeof dataArray != "undefined" && dataArray != null ){
		for( var i = 0, len = dataArray.length; i < len; i++ ){
			if( dataArray[i] == matchName ){
				isExist = true;
				break;
			}
		}
	}
	return isExist;
}


function createDateTimeStamp(dateType, param, changeDay) {
	var dateStore = {
		day : "",
		month : "",
		hour : "",
		min : "",
		date : ""
	}
	/* 수정전 
	var d = new Date();
	var cTimestamp = 0;
	*/
	var d = "";
	var cTimestamp = 0;
	
	if(changeDay != null){
		d = changeDay;
	}else {
		d = new Date();
	}
	
	if(dateType.indexOf('D') > -1 && param.year != null && param.year != 0 ) {
		d = addYear(d, param.year);
	}
	if(dateType.indexOf('D') > -1 && param.month != null && param.month != 0 ) {
		d = addMonth(d, param.month);
	}
	if(dateType.indexOf('D') > -1 && param.day != null && param.day != 0 ) {
		cTimestamp = param.day * 1000 * 60 * 60 * 24;
	}
	if(dateType.indexOf('H') > -1 && param.hour != null && param.hour != 0 ) {
		cTimestamp = cTimestamp + param.hour * 1000 * 60 * 60;
	}
	if(dateType.indexOf('M') > -1 && param.min != null && param.min != 0 ) {
		cTimestamp = cTimestamp + param.min * 1000 * 60;
	}
	
	cTimestamp = d.getTime() + cTimestamp;
	
	var curDate= new Date(cTimestamp);
	//@@ 정중훈 2013.08.08 MonthPicker로 인하여 추가 ㄴ
	dateStore.month =
		leadingZeros(curDate.getFullYear(), 4) + '-' +
		leadingZeros(curDate.getMonth() + 1, 2);
	dateStore.day =
		leadingZeros(curDate.getFullYear(), 4) + '-' +
		leadingZeros(curDate.getMonth() + 1, 2) + '-' +
		leadingZeros(curDate.getDate(), 2);
	dateStore.hour =
		leadingZeros(curDate.getHours(), 2);
	dateStore.min =
		leadingZeros(curDate.getMinutes(), 2);
	dateStore.date = dateStore.day + " " + dateStore.hour + ":" + dateStore.min
	return dateStore;
}

function addMonth(dateObj, pMonth) {
	var yyyy = dateObj.getFullYear();
	var mm = dateObj.getMonth();
	var dd = dateObj.getDate();
	
	var cDate; // 계산에 사용할 날짜 객체 선언
	var oDate; // 리턴할 날짜 객체 선언
	var cYear, cMonth, cDay // 계산된 날짜값이 할당될 변수
	mm = mm*1 + ((pMonth*1) + 1); // 월은 0~11 이므로 하나 빼준다
	cDate = new Date(yyyy, mm, dd) // 계산된 날짜 객체 생성 (객체에서 자동 계산)
	cYear = cDate.getFullYear(); // 계산된 년도 할당
	cMonth = cDate.getMonth(); // 계산된 월 할당
	cDay = cDate.getDate(); // 계산된 일자 할당
	oDate = (dd == cDay) ? cDate : new Date(cYear, cMonth, 0); // 넘어간 월의 첫쨋날 에서 하루를 뺀 날짜 객체를 생성한다.
	return oDate;
}

function addYear(dateObj, pYear) {
	dateObj.setYear(dateObj.getFullYear() + parseInt(pYear));
	return dateObj;
}


function leadingZeros(n, digits) {
	var zero = '';
	n = n.toString();
	
	if (n.length < digits) {
		for (i = 0; i < digits - n.length; i++)
			zero += '0';
	}
	return zero + n;
}


function procActionLink(command) {
	switch(command) {
	case "request":
		var theForm = Ext.getCmp("request");
		if(theForm.isValid()){
			var paramMap = getProcessData(panelList);
			paramMap["workType"] = "request"; 
			paramMap["loginId"] = "$!{dataMap.nbpm_user_id}"; 
			paramMap["queryKey"] = "insertProcService"; 
			var callbackSub = function(){
				jq.ajax({
					"type":"POST",
					"url":"$!{context}/itg/nbpm/processRequest.do",
					"contentType":"application/json",
					"dataType":"json",
					"data":getArrayToJson(paramMap),
					"success":function(data){ 
						showMessage('성공'); 
					}
				});
			};
			var isFile = checkFileSave('atch_file_id',paramMap,callbackSub);;
			if( !isFile ){ eval(callbackSub)(); }
		}else{ showMessage('데이터 확인') }
		
		break;
		
		
	}
}

// TabSize
function getActiveTabPanelSize(tabId){
	var panelSize = {};
	var tab = Ext.getCmp(tabId);
	var activeTab = tab.getActiveTab();
	panelSize.clientWidth = activeTab.body.dom.clientWidth;
	panelSize.clientHeight = activeTab.body.dom.clientHeight;
	panelSize.tabTitleHeight = getTabTitleHeight(tab);
	return panelSize;
}

// Tab Resize
function setResizeActiveTabPanel(tabId){
	var tab = Ext.getCmp(tabId);
	var activeTab = tab.getActiveTab();
	tab.setWidth(activeTab.body.dom.clientWidth);
	tab.setHeight(activeTab.body.dom.clientHeight);
	
	activeTab.setWidth(activeTab.body.dom.clientWidth);
	activeTab.setHeight(activeTab.body.dom.clientHeight);
}

// 내부 Tab Panel을 부모 탭 Panel Size로 변경
function setResizeInnerTabPanel(tabId, parentPanelSize){
	var tab = Ext.getCmp(tabId);
	tab.setWidth(parentPanelSize.clientWidth);
	tab.setHeight(parentPanelSize.clientHeight);
	
	var activeTab = tab.getActiveTab();
	activeTab.setWidth(tab.getWidth());
	activeTab.setHeight(tab.getHeight());
}

// Setting 리사이즈 패널
function setResizePanel(panelId, panelSize){
	var panel = Ext.getCmp(panelId);
	panel.setWidth(panelSize.clientWidth);
	panel.setHeight(panelSize.clientHeight);
}

// Setting 리사이즈 Width 패널
function setResizePanelWidth(panelId, width){
	var panel = Ext.getCmp(panelId);
	panel.setWidth(width);
}

// 원래 Height값
function setOrignalHeightPanel(panel, height){
	panel.originalHeight = height;
}

// 원래 Width값
function setOrignalWidthPanel(panel, width){
	panel.originalWidth = width;
}

// overflow 제어 함수
function setOverflow(targetElem, value){
	targetElem.style.overflow = value;
}

// Tab Title Height
function getTabTitleHeight(tab){
	var tabTitleHeight = 0;
	if( tab.ownerCt.body ){
		tabTitleHeight = tab.ownerCt.body.dom.childNodes[0].childNodes[0].clientHeight;
	}
	return tabTitleHeight;
}

// Panel Height
function getPanelHeight(panel){
	var panelHeight = 0;
	var agt = navigator.userAgent;
	if(agt.indexOf("MSIE 6") != -1 || agt.indexOf("MSIE 7") != -1 || agt.indexOf("MSIE 8") != -1 ){
		panelHeight = panel.getHeight();
	}else{
		panelHeight = Math.max(panel.body.dom.parentNode.clientHeight, panel.getHeight());
	}
	return panelHeight;
}

// Panel Width
function getPanelWidth(panel){
	var panelWidth = 0;
	var agt = navigator.userAgent;
	if(agt.indexOf("MSIE 6") != -1 || agt.indexOf("MSIE 7") != -1 || agt.indexOf("MSIE 8") != -1 ){
		panelWidth = panel.getWidth();
	}else{
		panelWidth = Math.max(panel.body.dom.parentNode.clientWidth, panel.getWidth());
	}
	return panelWidth;
}

// Document Width 값을 가져온다
function getDocWidth(){
	return document.body.clientWidth;
}

// Document Height Max값을 가져온다.
function getDocHeight(){
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

function resizePopupLayout(viewPortId, borderId, calculatePanels, adjustPanelId, popupWidth) {
	resizeLayout(viewPortId, borderId, calculatePanels, adjustPanelId, popupWidth, "popup");
}

// Resize Layout
function resizeBorderLayout(viewPortId, borderId, calculatePanels, adjustPanelId){
	resizeLayout(viewPortId, borderId, calculatePanels, adjustPanelId, 
			CONSTRAINT.APP_RESOLUTION_REALSIZE_WIDTH - CONSTRAINT.APP_LEFT_MENU_WIDTH, "common");
}

function resizeLayout(viewPortId, borderId, calculatePanels, adjustPanelId, resizeWidth, winType) {
	var viewPort = Ext.getCmp(viewPortId);
	if( viewPort ){
		Ext.suspendLayouts();
		
		var borderPanel = Ext.getCmp(borderId);
		
		var agt = navigator.userAgent;
		
		// 표준 프레임 해상도 Width
		var defaultContentResultionWidth = resizeWidth;
		
		var frameInnerWidth = 0;
		if(agt.indexOf("MSIE 6") != -1 || agt.indexOf("MSIE 7") != -1 || agt.indexOf("MSIE 8") != -1 ){
			frameInnerWidth = document.documentElement.offsetWidth;
		}else{
			frameInnerWidth = window.innerWidth;
		}
		
		var documentClientWidth = 0;
		if( winType == "popup" ){
			documentClientWidth = resizeWidth;			
		} else {
			documentClientWidth = document.body.clientWidth;
		}
		
		var rightScrollWidth = 0;
		if( parent ){
			rightScrollWidth = frameInnerWidth - document.body.clientWidth;	
		}
		
		// 해상도에서 스크롤값을 빼준다.
		var calculateFrameBodyWidth = defaultContentResultionWidth - rightScrollWidth;
		
		
		// viewport width 값 조정 
		if( document.body.clientWidth > defaultContentResultionWidth ){
			// 해상도 보다 클 경우
			borderPanel.setWidth(documentClientWidth);
			viewPort.setWidth(documentClientWidth);
			
			// viewport width값 변경전이여서 이쪽에서 원래 width값을 셋팅
			if( viewPort.originalWidth ){
				if( viewPort.originalWidth > documentClientWidth ){
					borderPanel.setWidth(viewPort.originalWidth);
					viewPort.setWidth(viewPort.originalWidth);	
				}
			}else{
				setOrignalWidthPanel(viewPort, documentClientWidth);
			}
		}else{ 
			borderPanel.setWidth(calculateFrameBodyWidth - 1);
			viewPort.setWidth(calculateFrameBodyWidth - 1);	// body와 viewport의 차이가 -1이 발생
			
			// viewport width값 변경전이여서 이쪽에서 원래 width값을 셋팅
			if( viewPort.originalWidth ){
				if( viewPort.originalWidth > calculateFrameBodyWidth - 1 ){
					borderPanel.setWidth(viewPort.originalWidth);
					viewPort.setWidth(viewPort.originalWidth);	
				}
			}else{
				setOrignalWidthPanel(viewPort, calculateFrameBodyWidth - 1);
			}
		}
		
//		var widths = {
//			defaultContentResultionWidth: defaultContentResultionWidth,
//			frameInnerWidth: frameInnerWidth,
//			rightScrollWidth: rightScrollWidth,
//			calculateFrameBodyWidth: calculateFrameBodyWidth,
//			"document.body.clientWidth": document.body.clientWidth,
//			window: window,
//			"document.body": document.body,
//			borderWidth: getPanelWidth(Ext.getCmp(borderId)),
//			viewportWidth: getPanelWidth(Ext.getCmp(viewPortId))
//		}
//		
//		console.info(widths);
		
		// 표준 해상도 Height		
		
		var parentOuterHeight = 0;
		var parentInnerHeight = 0;
		var frameOuterHeight = 0;
		var frameInnerHeight = 0;
		
		if(agt.indexOf("MSIE 6") != -1 || agt.indexOf("MSIE 7") != -1 || agt.indexOf("MSIE 8") != -1 ){
			parentOuterHeight = parent.document.documentElement.offsetHeight;
			parentInnerHeight = parent.document.documentElement.offsetHeight;
			frameOuterHeight = document.documentElement.offsetHeight;
			frameInnerHeight = document.documentElement.offsetHeight;
		}else{
			parentOuterHeight = parent.window.outerHeight;
			parentInnerHeight = parent.window.innerHeight;
			frameOuterHeight = window.outerHeight;
			frameInnerHeight = window.innerHeight;
		}
		var windowOtherHeight = parentOuterHeight - parentInnerHeight;
		var gapParentFrameHeight = parentInnerHeight - frameInnerHeight;
		var defaultContentResultionHeight = CONSTRAINT.APP_RESOLUTION_HEIGHT - ( windowOtherHeight + gapParentFrameHeight );
		var calculateFrameBodyHeight = frameInnerHeight;
		
		var calculateHeight = 0;
		if ( calculatePanels != null && calculatePanels.length > 0 ){
			for( var i = 0 ; i < calculatePanels.length; i++ ){
				calculateHeight = calculateHeight + getPanelHeight(Ext.getCmp(calculatePanels[i]));
			}
		}
		var adjustPanel = Ext.getCmp(adjustPanelId);
		var adjustHeight = 0;
		if( adjustPanel ){
			if( adjustPanel.originalHeight ){
				adjustHeight = adjustPanel.originalHeight;				
			}else{
				adjustHeight = getPanelHeight(adjustPanel);
				setOrignalHeightPanel(adjustPanel, adjustHeight);
			}
		}

		var centerPanel = null;
		var northHeight = 0;
		var southHeight = 0;
		var centerButtonHeight = 0;
		if( borderPanel.items.items.length > 0 ){
			var borderItemsLength = borderPanel.items.items.length;
			for( var i = 0; i < borderItemsLength; i++ ){
				if( borderPanel.items.items[i].region == "north" ){
					northHeight = borderPanel.items.items[i].getHeight();
				}
				
				if( borderPanel.items.items[i].region == "center" ){
					centerPanel = borderPanel.items.items[i];
					if( borderPanel.items.items[i].dockedItems.items.length > 0 ){
						centerButtonHeight = CONSTRAINT.DEFAULT_BUTTON_HEIGHT;
					}
				}
				
				if( borderPanel.items.items[i].region == "south" ){
					southHeight = borderPanel.items.items[i].getHeight();
				}
			}
		}
		calculateHeight = calculateHeight + northHeight + southHeight + centerButtonHeight;
		
		var renderHeight = calculateHeight + adjustHeight;
		
		var heights = {
			defaultContentResultionHeight: defaultContentResultionHeight,	// 998
			parentOuterHeight: parentOuterHeight,								// 684
			parentInnerHeight: parentInnerHeight,									// 684
			frameOuterHeight: window.outerHeight,								// undefined
			frameInnerHeight: window.innerHeight,								// undefined
			windowOtherHeight: windowOtherHeight,							// 0
			centerButtonHeight: centerButtonHeight,								// 36
			renderHeight: renderHeight,												// 972
			calculateHeight: calculateHeight,										// 672
			calculateFrameBodyHeight: calculateFrameBodyHeight,				// 658
			"defaultContentResultionHeight - calculateHeight": defaultContentResultionHeight - calculateHeight,	// 326
			"calculateFrameBodyHeight - calculateHeight": calculateFrameBodyHeight - calculateHeight				// -14
			 
		}
		
//		console.info(heights);
		if( defaultContentResultionHeight <= calculateFrameBodyHeight ){
			if( calculateFrameBodyHeight < renderHeight ){
				borderPanel.setHeight(renderHeight);
				if( centerPanel != null ){
					centerPanel.setHeight(renderHeight);
				}
				viewPort.setHeight(renderHeight);
			}else{
				borderPanel.setHeight(calculateFrameBodyHeight);
				
				if( centerPanel != null ){
					centerPanel.setHeight(calculateFrameBodyHeight);
				}
				
				// 여백이 남았을 경우
				if( adjustPanel ){
					adjustPanel.setHeight(calculateFrameBodyHeight - calculateHeight );	
				}
				
				viewPort.setHeight(calculateFrameBodyHeight);
			}
			
		}else{
			if( renderHeight > calculateFrameBodyHeight ){
				if(agt.indexOf("MSIE 6") != -1 || agt.indexOf("MSIE 7") != -1 || agt.indexOf("MSIE 8") != -1 ){
					renderHeight = renderHeight + 20;
				}
				
				// Border Panel Height 변경
				borderPanel.setHeight(renderHeight);
				if( centerPanel != null ){
					centerPanel.setHeight(renderHeight);
				}
				
				// 여백이 남았을 경우
				if( adjustPanel ){
					adjustPanel.setHeight(renderHeight - calculateHeight );	
				}
				viewPort.setHeight(renderHeight);	
			}else{
				borderPanel.setHeight(calculateFrameBodyHeight);
				
				if( centerPanel != null ){
					centerPanel.setHeight(calculateFrameBodyHeight);
				}
				
				// 여백이 남았을 경우
				if( adjustPanel ){
					adjustPanel.setHeight(calculateFrameBodyHeight - calculateHeight);	
				}
				
				viewPort.setHeight(calculateFrameBodyHeight);	
			}
		}
		Ext.resumeLayouts(true);
	}
}

// Resize Layout
function resizeWidthByAdjustPanel(viewPortId, borderId, adjustPanelIds){
	var viewPort = Ext.getCmp(viewPortId);
	if( viewPort ){
		Ext.suspendLayouts();
		var adjustPanelsWidth = 0;
		for( var i = 0; i < adjustPanelIds.length; i++ ){
			adjustPanelsWidth = adjustPanelsWidth + getPanelWidth(Ext.getCmp(adjustPanelIds[i]));
		}
		var borderPanel = Ext.getCmp(borderId);
		
//		// viewport width 값 조정 
		if( viewPort.getWidth() < adjustPanelsWidth ){
			// 해상도 보다 클 경우
			borderPanel.setWidth(adjustPanelsWidth);
			viewPort.setWidth(adjustPanelsWidth);
		}
		Ext.resumeLayouts(this);
	}
}
 
/**
 * 하위 Panel들의 높이를 같게 해주는 Fucntion
 * @param {} panel
 * @param {} syncHeight
 */
/*
function syncPanelHeight(syncPanelId, syncHeight){
	var syncPanel = Ext.getCmp(syncPanelId);
	var childPanel = syncPanel.query(".panel");
	console.info(syncPanel.child("panel"));
	console.info(syncPanel.query("> panel"));
	var itemPanelCount = childPanel.length;
	if( itemPanelCount > 0 ){
		for( var i = 0; i < itemPanelCount; i++ ){
			var itemPanel = childPanel[i];
			if( !itemPanel.syncSkip ){
				var marginHeight = 0;
				if( itemPanel.up("panel") ){
					var upPanel = itemPanel.up("panel");
					// 상위 패널이 존재할 경우 Margin 체크
					if( upPanel.margin ){
						marginHeight = upPanel.margin$.height * 2;
					}
				}
				          
				if( itemPanel.margin ){
					marginHeight = ( marginHeight + itemPanel.margin$.height ) * 2;
				}
				
				if( typeof syncHeight != "undefined" ){
					itemPanel.setHeight(syncHeight - marginHeight);	
				}else{
					itemPanel.setHeight(syncPanel.getHeight() - marginHeight );
				}
			}
		}
	}
}
*/

function syncPanelHeight(syncPanelId, syncHeight){
	Ext.suspendLayouts();
	var syncPanel = Ext.getCmp(syncPanelId);
	var childPanel = syncPanel.query("> panel");
	var itemPanelCount = childPanel.length;
	
	if( typeof syncHeight == "undefined" ){
		syncHeight = syncPanel.getHeight();
	}
	
	if( itemPanelCount > 0 ){
		for( var i = 0; i < itemPanelCount; i++ ){
			var itemPanel = childPanel[i];

			if( !itemPanel.syncSkip ){
				var marginHeight = 0;
				var titleHeight = 0;
				if( itemPanel.up("panel") ){   
					var upPanel = itemPanel.up("panel");
					// 상위 패널이 존재할 경우 Margin 체크
					if( upPanel.margin ){
						marginHeight = upPanel.margin$.height * 2;
					}
					if( upPanel.xtype == "tabpanel" ){
						titleHeight = getTabTitleHeight(upPanel);
					}
				}
				//@@ 고한조 - margin만 했을때는 margin$가 없는 경우도 있어서 에러가 발생 
				if( itemPanel.margin$ ){
					marginHeight = ( marginHeight + itemPanel.margin$.height ) * 2;
				}
				
				itemPanel.setHeight(syncHeight - (marginHeight + titleHeight) );
				syncPanelHeight(itemPanel.id, syncHeight);
			}
		}
	}
	Ext.resumeLayouts(this);
}

/**
 * 
 * @param {} value
 * @return {}
 */
function nullToSpace(value){
	if(value == null || value == 'null' || value == 'undefined') {
		value = "";
	}
	return value;
}

function isNullOrSpace(value) {
	var result = false;
	if(value == "" || value == null) {
		result = true;
	}
	
	return result;
}

/**
 * Has Css
 * @param {} ele
 * @param {} cls
 * @return {}
 */
function hasClass(ele,cls) {
	var arry = ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
	if( arry != null && arry != "null" && arry.length > 0 ){
		return true;
	}else{
		return false;
	}
	//return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}
 
/**
 * Add Css
 * @param {} ele
 * @param {} cls
 */
function addClass(ele,cls) {
	if (!this.hasClass(ele,cls)) ele.className += " "+cls;
}  
      
/**
 * Remove Css
 * @param {} ele
 * @param {} cls
 */
function removeClass(ele,cls) {
	if (hasClass(ele,cls)) {
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className =ele.className.replace(reg,' ');
	}
}

//@@ 20130806 고은규 summaryGrid 사용시 null값의 length가 0이 아닌값이 출력되어 데이터 trim 기능사용함.
function trim(str){
	return str.replace(/(^\s*)|(\s*$)/gi, "");
}

//replaceALL 기능 추가
function replaceAll(temp, org, rep){
	return temp.split(org).join(rep);
}

// 선택된 RadioGroup의 Text값을 가져온다.
function getRadioGroupText(radioGroup, value){
	var text = "";
	var radioItems = radioGroup.items;
	for( var i = 0; i < radioItems.length; i++ ){
		var radioItem = radioItems.items[i]; 
		if( radioItem.inputValue == value ){
			text = radioItem.boxLabel;
			break;
		}
	}
	return text;
}

// Data의 Key값을 lowercase로 변경하여 반환
function jsonDataToLowercase(data){
	var toLowerCaseData = {};
	for( var key in data ){
		toLowerCaseData[key.toLowerCase()] = data[key];
	}
	return toLowerCaseData;
}

// 콤보박스의 Value값에 따라서 Text값을 가져오는 Function
function getComboTextByValue(comboField, key, text, value){
	var returnText = "";
	var comboStore = comboField.getStore();
	var comboDataItems = comboStore.data.items;
	for( var i = 0; i < comboDataItems.length; i++ ){
		var comboData = comboDataItems[i].data;
		if( comboData[key] == value ){
			returnText = comboData[text];
			break;
		}
	}
	
	return returnText;
}

/**
 * 2014.02.25 정중훈
 * 날짜를 format 형식에 맞게 변환하여 줍니다.
 * @param date 변경할 날짜를 넣어줍니다. 
 * @param format 날짜 형식을 넣어줍니다. (ex> 'YYYY-MM-DD', 'YYYYMMDD') 
 * @returns
 */
function converDateString(date, format){
	
	var result
	if("YYYY-MM-DD" == format){
		result = date.getFullYear() + "-"
			+ addZero(date.getMonth()+1) + "-"
			+ addZero(date.getDate());
	}else if("YYYYMMDD" == format){
		result = date.getFullYear()
		+ addZero(date.getMonth()+1)
		+ addZero(date.getDate());
	}else{
		alert("날짜 형식이 잘못되었습니다.");
	}
	
	return result;
}

/**
 * 2014.02.25 정중훈
 * 한자리 숫자에 0을 붙이는 방법
 * @param i
 * @returns
 */
function addZero(i){
	var rtn = i + 100;
	// 10진수로 변경하고 난 후 문자열을 자릅니다. 
	var cnt = rtn.toString(10).substring(1,3);
	return cnt;
}

/**
 * 2014.02.25 정중훈 장애요청에서 옮김
 * 겟방식의 URl을 만들어 줍니다.
 * @param paramMap 겟방식에 넣어줄 파라미터들 입니다. 
 * @param startString 시작할 구분자를 넣어줍니다. (ex> ? 또는 & )
 * @returns
 */
function createAddUrl(paramMap, startString){
	var addUrl = startString;
	addUrl += Ext.Object.toQueryString(paramMap);
	return addUrl;
}

/**
 * 선택한 달의 마지막날을 들고 옵니다. 
 * @param date YYYYMMDD 형식으로 가져옵니다. 
 * @param format 등록한 날짜형식으로 가져옵니다.
 */
function searchLastDate(date, format){
	
	var checkDate;
	// 혹시나 "2014-01-01" 이렇게 넣었을 경우 구분함.
	var cnt = date.indexOf("-");
	
	if(0 < cnt){
		checkDate = replaceAll(date, "-", "");
	}else{
		checkDate = date;	
	}
	
	// 이번달의 말일을 가져옵니다.  
	var endYear = checkDate.substring(0,4);
	var endMonth = checkDate.substring(4,6);
	// 일자를 "01"로 안주면 익스플로어 에러남 
	var endDate = new Date(endYear + "-" + endMonth + "-" +"01");
	endDate.setMonth(endDate.getMonth()+1);
	endDate.setDate(0);
	var newEndDate = converDateString(endDate, format);
	
	return newEndDate;
}

// 보안 XSS Filter - Grid, Tooltip 적용
function securityXssFilter(value){
	var xssFilterList = CONSTRAINT["XSS_FILTER_LIST"];
	for( var i = 0; i < xssFilterList.length; i++ ){
		if( typeof value == "string" && value.indexOf(xssFilterList[i]) != -1 ){
			value = Ext.util.Format.htmlEncode(value);
			break;
		}
	}
	return value;
}

function cloneDataJsonToJson(json){
	var cloneData = {};
	for( var key in json ){
		cloneData[key] = json[key];
	}
	return cloneData;
}

function setFieldExpression(str, type){
	if( type == "WON" ){
		var reg = /(^[+-]?\d+)(\d{3})/;
		str += '';
		while (reg.test(str)) str = str.replace(reg, '$1' + ',' + '$2');
		return str;
	}
}

function removeFieldExpression(str, type) { 
	if( type == "WON" ){
		if(typeof str == 'string'){
			return parseInt(str.replace(/,/g, ""));
		}else if( typeof str == 'number' ){
			return str;
		}
	}   
	return str;
}   

function removeHtmlTagAll(text){
	if( typeof text == "string"){
		text = text.replace(/<br>/ig, "\n"); // <br>을 엔터로 변경
		text = text.replace(/&nbsp;/ig, " "); // 공백      
		// HTML 태그제거
		text = text.replace(/<(\/)?([a-zA-Z]*)(\s[a-zA-Z]*=[^>]*)?(\s)*(\/)?>/ig, "");
	 
		// shkim.add.
		text = text.replace(/<(no)?script[^>]*>.*?<\/(no)?script>/ig, "");
		text = text.replace(/<style[^>]*>.*<\/style>/ig, "");
		text = text.replace(/<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>/ig, "");
		text = text.replace(/<\\w+\\s+[^<]*\\s*>/ig, "");
		text = text.replace(/&[^;]+;/ig, "");
		text = text.replace(/\\s\\s+/ig, "");
	}
	return text;
}

// 특정값으로 Array Sort
function arrayPredicateBy(prop){
   return function(a,b){
      if( a[prop] > b[prop]){
          return 1;
      }else if( a[prop] < b[prop] ){
          return -1;
      }
      return 0;
   }
}