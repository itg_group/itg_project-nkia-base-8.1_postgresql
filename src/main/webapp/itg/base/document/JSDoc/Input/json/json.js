function getFormParam(formId) {
	var fields = jq('#'+formId).serializeArray();
	//파라메터
	var param = {};
	for(var i = 0 ; i < fields.length ; i++) {
		var field = fields[i];
		var inputObj = jq("input[name="+field.name+"]");
		if(inputObj.length > 1) {
			var paramAraay = new Array(inputObj.length);
			for(var j = 0 ; j < inputObj.length ; j++) {
				paramAraay[j] = inputObj[j].value;
			}
			param[field.name] = paramAraay;
			i = i + inputObj.length - 1;
		} else {
			param[field.name] = field.value;
		}
    }
	return param;
}


function getArrayToJson(param) {
	return jq.toJSON(param);
}

//@@20130808 김도원 START
//getArrayToJson 반대기능을 추가
function getJsonToArray(param) {
	return jq.evalJSON(param);
}
//@@20130808 김도원 END