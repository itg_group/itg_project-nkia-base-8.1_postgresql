/**
 * 
 * @file nkia_tree
 * @description NKIA Tree 컴포넌트
 * @author ITG
 * @copyright NKIA 2013
 */

/**
 * Tree 컴퍼넌트를 가져온다
 * @param {} tree_id
 * @return {} Tree Component
 */
function getTree(tree_id) {
	return Ext.getCmp(tree_id);
}

/**
 * Tree의 Store를 가져온다.
 * @param {} tree_id
 * @return {} Tree Store
 */
function getTreeStore(tree_id) {
	return Ext.getCmp(tree_id).getStore();
}


/**
 * @method {} createTreeComponent : 트리 생성 Function
 * @param {json} setTreeProp : Tree Propertis
 * @property {string} id			: Tree Id
 * @property {string} title 			: Tree Title
 * @property {string} url			: Tree Data URL
 * @property {string} height 		: Tree Height
 * @property {string} rootText 		: root text
 * @property {string} expandLevel	: 트리 초기 확장레벨
 * @property {string} dynamicFlag	: 트리 다이나믹(트리 클릭시마다 로드) 로딩( true | false )
 * @property {string} expandAllBtn	: 트리 전체 펼치기( true | false )
 * @property {string} collapseAllBtn	: 트리 전체 접기( true | false )
 * @property {string} searchable	: 검색여부( true | false )
 * @property {string} extraToolbar	: Toolbar확장
 * @property {string} params		: 데이터 초기 Loading시 검색조건 Parameter
 * 
 * @returns {object} tree
 */
function createTreeComponent(setTreeProp) {
	
	var that = {};
	var tree_comp;
	for( var prop in setTreeProp ){
		that[prop] = setTreeProp[prop];
	}
	var param = that['params'] ? that['params'] : {};
	var expandLevel = that['expandLevel'] ? that['expandLevel'] : 0;
	var id = that['id'];
	var rootText = that['rootText'] ? that['rootText'] : '전체';
	var dynamicFlag = that['dynamicFlag'] ? that['dynamicFlag'] : false;
	var expandAllBtn = that['expandAllBtn'] ? that['expandAllBtn'] : false;
	var collapseAllBtn = that['collapseAllBtn'] ? that['collapseAllBtn'] : false;
	var searchable = that['searchable'] ? that['searchable'] : false;
	var extraToolbar = that['extraToolbar'] ? that['extraToolbar'] : "";
	param['expandLevel'] = expandLevel;
	var tree_proxy = createTreeProxyComp(id, that['url'], param);
	
	var icon = that['icon'] ? that['icon'] : getConstValue('CONTEXT')+'/itg/base/images/ext-js/common/icons/bullet_tree.gif';
	var checkBoxShow = ( typeof that['checkBoxShow'] ) ? that['checkBoxShow'] : true;
	
	var treeStore = Ext.create('Ext.data.TreeStore', {
		id: id + "_store",
		proxy: tree_proxy,
		autoLoad: false,
		root: {
			text: rootText,
			id: 'root',
			node_id: 'root'
		},
		listeners: {
			beforeexpand:function(node) {
				if(dynamicFlag) {
					var arrayParam = {};
					var node_id = node.raw.id;
					arrayParam["up_node_id"] = node_id;
					arrayParam["expandLevel"] = 0;
					tree_proxy.jsonData = arrayParam;
				}
			}
		}
	});
	
	var tree_dockedItem;
	var tree_expandBtn = null;
	var tree_collapseBtn = null;
	
	var resultPanel = null;
	
	var toolbarBtnArray = new Array();
	var toolbarArray = new Array();

	var tabHeight = ( that['height'] ) ? that['height'] : '100%';
	var fullSize = true;
	
	var tree_comp = Ext.create('nkia.custom.TreePanel', {
    	id : id,
    	itemId: that['itemId'] ? that['itemId'] : '',
    	title: that['title'],
    	animate: false,	// Tree expand/collapse 애니메이션 효과 제거
        store: treeStore,
        width: that['width'] ? that['width'] : '100%',
        cls: that['cls'] ? that['cls'] : null,
        minHeight: 100,
        useArrows: true,
        rootVisible: that['rootVisible'] ? that['rootVisible'] : false,
        autoScroll: true,
        forceFit: true,
        height: that['height'] ? that['height'] : '100%',
        maxHeight: that['height'] ? that['height'] : '100%',
        multiSelect: that['multiSelect'] ? that['multiSelect'] : false,
        tools : that['tools'],
        expandAllBtn : expandAllBtn,
        collapseAllBtn : collapseAllBtn,
        searchable: searchable,
        searchableTextWidth: that['searchableTextWidth'] ? that['searchableTextWidth'] : getConstValue('DEFAULT_SEARCH_TREE_WIDTH_FIELD'),
        dynamicFlag: dynamicFlag,
        extraToolbar: extraToolbar,
        icon : (icon) ? (icon) : getConstValue('CONTEXT')+'/itg/base/images/ext-js/common/icons/bullet_all.gif',
        viewConfig: { 
        	plugins: ( that['dragPlug'] ) ? {ptype: 'treeviewdragdrop', appendOnly: true} : null,
			emptyText:( that['emptyMsg'] ) ? that['emptyMsg'] : getConstText({ isArgs: true, m_key: 'msg.common.00001' }),
			deferEmptyText : false,		
			loadMask: ( that['loadMask'] ) ? that['loadMask'] : true,
			loadingText:( that['loadMsg'] ) ? that['loadMsg'] : getConstText({ isArgs: true, m_key: 'msg.mask.common' }),
			listeners: {
				drop: function(node, data, overModel, dropPosition, dropHandler){
					var params = that['params'] ? that['params'] : {}; 
					params['expandLevel'] = expandLevel;
					
					if(!confirm( getConstText({ isArgs: true, m_key: 'msg.system.result.00043' }) )){
						var treeReLoad = getGrid(that['id']).getStore();
						treeReLoad.proxy.jsonData=params;
						treeReLoad.load();							
						return false;
					}
					var paramMap = {};
					paramMap['node_id'] = data.records[0].raw.node_id;
					paramMap['up_node_id'] = overModel.raw.node_id;
					if(that['updateUrl'] != null){
						jq.ajax({ type:"POST", url: that['updateUrl'], 
							contentType: "application/json", dataType: "json" , data : getArrayToJson(paramMap), 
							success:function(data){
								alert(data.resultMsg);
							}
						});
					}else{
						alert("Error");
					}
				}
			}			
        },
		listeners: {
        	afterrender:function(tree) {
        		tree.readyState = true;
        		Ext.suspendLayouts();
        		//@@ 고한조 20131127 추가 - Tree가 생성될때 로드 되지 않도록 autoLoad 추가
        		if( that["autoLoad"] == null || that["autoLoad"] == true ){
        			tree.expandNode(tree.getRootNode());
        		}
        		// 확장 Level이 1보다 클 때 // cascadeBy를 사용하여 확장
        		if( expandLevel > 1 ){
        			var treeStore = tree.getStore();
	        		var interval_expand = setInterval(function () {
			  			if (!treeStore.isLoading()) {
			  				var rootNode = treeStore.getRootNode();
							if( rootNode.firstChild ){	// Root의 ChildNode가 있을 경우
								rootNode.firstChild.cascadeBy(function (node) {
									if( node.getDepth() < expandLevel ) { 
										node.expand();
									}
									if( node.getDepth() == expandLevel ) { return false; }
								});
							}
					    	clearInterval(interval_expand);
			 		 	}
		  				//rest of function
					}, 100); // refresh every 100 milliseconds
        		}
        		Ext.resumeLayouts(true);
        	},
        	beforeitemappend:function(treePanel, node, eOpts) {
        		// checkBoxShow : true로 넣어주면 전체 체크박스 등장
        		if(!checkBoxShow){
        			// 체크박스 폴더형일때 숨기기
	        		if(!node.data.leaf) {
	        			node.data.checked = null;
	        		}
        		}
        	}
		}
    });
    
    return tree_comp;
}

/**
 * @function
 * @summary 탭 트리 컴포넌트
 * @param {json} tree_property
 * @return {object} tab_tree 트리객체
 */
function createTabTreeComponent(tree_property) {
	tree_property["enableTab"] = true;
	var tab_tree = createTreeComponent(tree_property);
	return tab_tree;
}

/**
 * @function
 * @summary 트리 프록시 객체
 * @param {string} id Proxy Id
 * @param {string} url 요청 URL
 * @param {json} params 요청 파라미터
 * @return {object} tree_proxy
 */
function createTreeProxyComp(id, url, params) {
	var tree_proxy = Ext.create('nkia.custom.JsonProxy', {
		id: id+ '_proxy',
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: url,
		jsonData : ( params ) ? params : {},
		actionMethods: {
			create	: 'POST',
			read	: 'POST',
			update	: 'POST'
		},
		reader: {
			type	: 'json'
		}
	});	
	return tree_proxy;
}

/**
 * @function
 * @summary 스트링 비교 indexOf로 리턴
 * @param {string} searchStr 검색문자열
 * @param {string} targetStr 대상문자열
 * @return {int} indexOf 값
 */
function checkStr(searchStr, targetStr) {
	searchStr = searchStr.toUpperCase();
	targetStr = targetStr.toUpperCase();
	return targetStr.indexOf(searchStr);
}