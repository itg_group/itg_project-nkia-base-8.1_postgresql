
var searchGrid = null; // 검색폼에 엔터 이벤트줄때 검색될 그리드의 id 값을 들고 있는다.
function createSelectAssetPop(setPopUpProp){	
	var storeParam = {};
	var that = this;
	var setAssetPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		
		searchGrid = that['id']+'Grid';
		
		// 조회 그리드
		var setSearchGridProp = {	
				id: that['id']+'Grid',
				title : getConstText({ isArgs: true, m_key: 'res.title.itam.maint.assetlist' }),
				gridHeight: 210,
				resource_prefix: that['sPrefix'],				
				url: that['url'],
				params: ( that['params'] ) ? that['params'] : null,
				selModel: true,
				pagingBar : true,
				pageSize : 10,
				autoLoad: true
		};
		var assetSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
				id: that['tGridId'],
				title : getConstText({ isArgs: true, m_key: 'res.label.itam.maint.req.00014' }),
				gridHeight: 187,
				resource_prefix: that['tPrefix'],				
				params: null,		
				pagingBar : false,
				autoLoad: false,
				dragPlug: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
				actionBtnEvent: actionBtnEvent,
				isMemoryStore: true
		};
		var assetSaveListGrid = createGridComp(setSaveGridProp);
		
		var setTreeProp = {				
				id : that['id']+'Tree',
				url : '/itg/itam/amdb/searchAmClassAllTree.do',
				title : getConstText({ isArgs: true, m_key: 'res.00016' }),
				dynamicFlag : false,
				searchable : true,
				expandAllBtn : false,
				collapseAllBtn : false,
				params: ( that['tree_params'] ) ? that['tree_params'] : null,
				rnode_text :  getConstText({ isArgs: true, m_key: 'res.00003' }),
				expandLevel: 3
		}
		var staticClassTree = createTreeComponent(setTreeProp);	
		// 높이 길이 체크 무시
		staticClassTree['syncSkip'] = true;
		attachCustomEvent('select', staticClassTree, treeNodeClick);

		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.choice' }),
			handler: function() {
				checkGridRowCopyToGrid(that['id']+'Grid', that['tGridId'], { primaryKeys: ['CONF_ID'] });
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var operAssetSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				var searchForm = Ext.getCmp('assetPopSearchForm');
				var paramMap = searchForm.getInputData();
				Ext.getCmp(that['id']+'Grid').searchList(paramMap);	
			}
		});
		
		var operAssetSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp('assetPopSearchForm').initData();
			}
		});
		
		var setAssetSearchFormProp = {
			id: 'assetPopSearchForm',
			formBtns: [operAssetSearchBtn, operAssetSearchInitBtn],
			columnSize: 3,
			enterFunction : clickPopSearchBtn,
			tableProps : [{
							colspan:1, tdHeight: 30, item: createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_ASSET', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})
						},{
							colspan:1, tdHeight: 30, item: createTextFieldComp({name:'search_value', padding: getConstValue('WITH_FIELD_PADDING')})
						}]
		}
		var searchAssetForm = createSeachFormComp(setAssetSearchFormProp);
		
		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: 515,
			autoScroll: true,
			items: [staticClassTree]
		});
		
		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			flex:1 ,
			items: [searchAssetForm, assetSearchListGrid, choiceBtnpanel, assetSaveListGrid]
		});
		
		var complexProp = {
				height : '100%',
				panelItems : [{
					flex: 1,
					items : left_panel
				},{
					flex: 3,
					items : right_panel
				}]
		};
		var complexPanel = createHorizonPanel(complexProp);		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			ui:'correct',
			handler: function() {
				if(getGrid(that['tGridId']).getStore().data.items <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.maint.req.00037' }));
				}else{
					getGrid(that['sGridId']).getStore().removeAll();
					var appendable = false;
					gridAllRowCopyToGrid( that['tGridId'],  that['sGridId'], appendable, null, { primaryKeys: ['CONF_ID'] });
					setAssetPop.close();
				}
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				setAssetPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		
		function actionBtnEvent(grid, rowIndex, colIndex){
			grid.getStore().removeAt(rowIndex); 
		};
		
		
		function treeNodeClick( tree, record, index, eOpts ) {
			var params = ( that['params'] ) ? that['params'] : {};
			params['class_id'] = record.raw.node_id;
			var grid_store = getGrid(that['id']+'Grid').getStore();
			grid_store.proxy.jsonData = params;
			grid_store.reload();
		}
		
		function clickPopSearchBtn() {
			var searchForm = Ext.getCmp('assetPopSearchForm');
			var paramMap = searchForm.getInputData();
			Ext.getCmp(searchGrid).searchList(paramMap);	
		}
		
		/*********************** Private Function Area End ***/
		
		setAssetPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setAssetPop;
		
	} else {
		setAssetPop = popUp_comp[that['id']];
	}
	
	return setAssetPop;

}

// 유지보수 계약 대상 자산조회 팝업
function createSelectMaintTartgetAssetPop(setPopUpProp){	
	var storeParam = {};
	var that = this;
	var setAssetPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		
		searchGrid = that['id']+'Grid';
		
		// 조회 그리드
		var setSearchGridProp = {	
				id: that['id']+'Grid',
				title : getConstText({ isArgs: true, m_key: 'res.title.itam.maint.assetlist' }),
				gridHeight: 200,
				resource_prefix: that['sPrefix'],				
				url: that['url'],
				params: ( that['params'] ) ? that['params'] : null,
				selModel: true,
				pagingBar : true,
				pageSize : 10,
				autoLoad: true
		};
		var assetSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
				id: that['tGridId'],
				title : getConstText({ isArgs: true, m_key: 'res.label.itam.maint.req.00014' }),
				gridHeight: 200,
				resource_prefix: that['tPrefix'],				
				params: null,		
				pagingBar : false,
				autoLoad: false,
				dragPlug: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
				actionBtnEvent: actionBtnEvent,
				isMemoryStore: true
		};
		var assetSaveListGrid = createGridComp(setSaveGridProp);
		
		var setTreeProp = {				
				id : that['id']+'Tree',
				url : '/itg/itam/amdb/searchAmClassAllTree.do',
				title : getConstText({ isArgs: true, m_key: 'res.00016' }),
				dynamicFlag : false,
				searchable : true,
				searchableTextWidth : 100,
				width:250,
				height:640,
				expandAllBtn : true,
				collapseAllBtn : true,
				params: ( that['tree_params'] ) ? that['tree_params'] : null,
				rnode_text :  getConstText({ isArgs: true, m_key: 'res.00003' }),
				expandLevel: 3				
				
		}
		var staticClassTree = createTreeComponent(setTreeProp);	
		attachCustomEvent('select', staticClassTree, treeNodeClick);
		
		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.choice' }),
			handler: function() {
				checkGridRowCopyToGrid(that['id']+'Grid', that['tGridId'], { primaryKeys: ['CONF_ID'] });
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var operAssetSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				var searchForm = Ext.getCmp('assetPopSearchForm');
				var paramMap = searchForm.getInputData();
				
				Ext.getCmp(that['id']+'Grid').searchList(paramMap);	
			}
		});
		
		var operAssetSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp('assetPopSearchForm').initData();
			}
		});
		
		// 공급계약 SEARCH
		var supplyNm			= createTextFieldComp({name:'supply_nm', notNull:true, width:300, hideLabel:true, readOnly : true});
		var supplyId			= createHiddenFieldComp({id:'supply_id', name:'supply_id'});
		var supplyPopBtn		= createPopBtnComp({id:"supplyPopBtn"});
		var supplyClrBtn		= createClearBtnComp({id:"supplyClrBtn"});
		
		attachBtnEvent(supplyPopBtn, openPopType, "SUPPLY");
		attachBtnEvent(supplyClrBtn, openPopType, "CLRSUPPLY");
		
		// 유지보수 계약 SEARCH
		var maintNm			= createTextFieldComp({name:'maint_nm', notNull:true, width:300, hideLabel:true, readOnly : true});
		var maintId			= createHiddenFieldComp({id:'maint_id', name:'maint_id'});
		var maintPopBtn		= createPopBtnComp({id:"maintPopBtn"});
		var maintClrBtn		= createClearBtnComp({id:"maintClrBtn"});
		
		attachBtnEvent(maintPopBtn, openPopType, "MAINT");
		attachBtnEvent(maintClrBtn, openPopType, "CLRMAINT");
		
		function openPopType(command){
			switch(command) {
				case "SUPPLY" :
					openAmPopup(supplyPopBtn,"SUPPLY",supplyId,supplyNm,"statistics");
				break;
				case "CLRSUPPLY" :
					supplyNm.setValue('');
					supplyId.setValue('');
				break;
				case "MAINT" :
					openAmPopup(maintPopBtn,"MAINT",maintId,maintNm,"statistics");
				break;
				case "CLRMAINT" :
					maintNm.setValue('');
					maintId.setValue('');
				break;
			}
		}		
		
		var searchCombo = createCodeComboBoxComp({name:'search_type', code_grp_id:'SEARCH_ASSET', width: 120, attachChoice:true});
		var searchValue = createTextFieldComp({name:'search_value', padding: getConstValue('WITH_FIELD_PADDING'), width: 230})
		
		var searchDateCombo = createCodeComboBoxComp({name:'search_date_type', code_grp_id:'MAINT_SEARCH_DATE', width: 120, attachChoice:true});
		var searchDateValue = createSearchDateFieldComp({id : 'search_date_value' , margin: '1 2 0 5', name:'search_date_value', dateType : 'D', format: 'Y-m-d', width : 300});
		//var searchValue = createTextFieldComp({name:'search_value', padding: getConstValue('WITH_FIELD_PADDING'), width: 250})

		var setAssetSearchFormProp = {
			id: 'assetPopSearchForm',
			formBtns: [operAssetSearchBtn, operAssetSearchInitBtn],
			columnSize: 3,
			enterFunction : clickPopSearchBtn,
			tableProps : [{colspan:3, item: createUnionFieldContainer({label : "공급계약"	,id:"supplySelectField" ,items : [supplyNm,supplyId,supplyPopBtn,supplyClrBtn]})}
						 ,{colspan:3, item: createUnionFieldContainer({label : "유지보수계약"	,id:"maintSelectField" ,items : [maintNm,maintId,maintPopBtn,maintClrBtn]})}
						 ,{colspan:3, item: createUnionFieldContainer({label: "일자구분",id:"searchDateField" ,items : [searchDateCombo,searchDateValue]})}
						 ,{colspan:3, item: createUnionFieldContainer({label: getConstText({ isArgs: true, m_key: 'res.label.system.00012' }),id:"searchField" ,items : [searchCombo,searchValue]})}
						 ,{colspan:3, item: createCodeComboBoxComp({label: '자산상태', name:'asset_state', code_grp_id:'ASSET_STATE', width: 225, attachAll:true})}
						 ]
		}
		var searchAssetForm = createSeachFormComp(setAssetSearchFormProp);
		

		var left_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			items: [staticClassTree]
		});
		
		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			items: [searchAssetForm, assetSearchListGrid, choiceBtnpanel, assetSaveListGrid]
		});
		
		var complexProp = {
				panelItems : [{
					width: 250,
					items : left_panel
				},{
					width: 590,
					items : right_panel
				}]
			};
		var complexPanel = createHorizonPanel(complexProp);		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			ui:'correct',
			handler: function() {
				if(getGrid(that['tGridId']).getStore().data.items <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.maint.req.00037' }));
				}else{
					getGrid(that['sGridId']).getStore().removeAll();
					var appendable = false;
					gridAllRowCopyToGrid( that['tGridId'],  that['sGridId'], appendable, null, { primaryKeys: ['CONF_ID'] });
					setAssetPop.close();
				}
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				setAssetPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		
		function actionBtnEvent(grid, rowIndex, colIndex){
			grid.getStore().removeAt(rowIndex); 
		};
		
		
		function treeNodeClick( tree, record, index, eOpts ) {
			var params = ( that['params'] ) ? that['params'] : {};
			params['class_id'] = record.raw.node_id;
			var grid_store = getGrid(that['id']+'Grid').getStore();
			grid_store.proxy.jsonData = params;
			grid_store.reload();
		}
		
		function clickPopSearchBtn() {
			var searchForm = Ext.getCmp('assetPopSearchForm');
			var paramMap = searchForm.getInputData();
			Ext.getCmp(searchGrid).searchList(paramMap);	
		}
		
		/*********************** Private Function Area End ***/
		
		setAssetPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setAssetPop;
		
	} else {
		setAssetPop = popUp_comp[that['id']];
	}
	
	return setAssetPop;

}