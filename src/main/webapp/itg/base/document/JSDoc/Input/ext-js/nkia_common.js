/**
 * 
 * @file nkia_common
 * @description ext-js 공통 Function
 * @author ITG
 * @copyright NKIA 2013
 */
 
 /**
  * @global
  * @summary 팝업 객체 컨테이너(생성된 팝업관리)
  * @type {object}
  * @author ITG
  * @since 1.0 
  */
var popUp_comp = {};

/**
 * 
 * @function
 * @summary 팝업 생성 컴포넌트
 * @param {json} setWindowProp 생성프로퍼티
 * @property {string} id 생성ID
 * @property {string} title 팝업 타이틀
 * @property {string} items 팝업 Items(구성객체)
 * @property {string} width 팝업 width
 * @property {string} height 팝업 height
 * @property {string} buttons 팝업 버튼들
 * @property {boolean} closeBtn Close(닫기)버튼 생성여부
 * @author ITG
 * @since 1.0
 * 
 * @returns {object} window_comp  
 */
function createWindowComp(setWindowProp) {
	// 내부Property 바인딩
	var that = {};
	var window_comp = null;
	for (var prop in setWindowProp) {
		that[prop] = setWindowProp[prop];
	}

	if (!that['buttons']) {
		that['buttons'] = new Array();
	}

	var closeBtn = createBtnComp({
		label : getConstText({m_key : 'CLOSE'}),
		scale : 'medium'
	});

	// Close 버튼 추가
	if (that['closeBtn']) {
		that['buttons'].push(closeBtn);
	}

	closeBtn.on('click', function() {
		Ext.getCmp(that['id']).close();
	});

	if (popUp_comp[that['id']] == null) {
		window_comp = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['title'],
			width : that['width'],
			height : that['height'],
			modal : true,
			layout : 'fit',
			buttonAlign : 'center',
			bodyStyle : 'background-color: white; ',
			buttons : (that['buttons']) ? that['buttons'] : null,
			items : that['items'],
			resizable : false,
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {

				}
			}
		});
		popUp_comp[that['id']] = window_comp;

	} else {
		window_comp = popUp_comp[that['id']];
	}

	return window_comp;
}

/**
 * @function
 * @summary Icon CSS 가져오기
 * @param {string} 아이콘명
 * @return {string} css명
 * @author ITG
 * @since 1.0 
 */
function getIconCls(iconName) {
	var iconCls;
	switch (iconName) { // key값에 따른 버튼 생성
		case "search" :
			iconCls = 'icon-table-refresh';
			break;
		case "add" :
			iconCls = 'icon-add';
			break;
		case "user-add" :
			iconCls = 'icon-user-add';
			break;
		case "reset" :
			iconCls = 'icon-reset';
			break;
		case "update" :
			break;
		case "delete" :
			break;
		case "save" :
			break;
		case "cancel" :
			break;
		case "confirm" :
			break;
		case "request" :
			break;
		case "approval" :
			break;
		case "change" :
			break;
		case "void" :
			break;
		case "endservice" :
			break;
		case "startservice" :
			break;
		case "downexcel" :
			break;
		case "copy" :
			break;
		case "duplication" :
			break;
		case "move" :
			break;
		default :
	}
	return iconCls;
}

/**
 * @function
 * @summary 엔터키 이벤트 붙이기
 * @param {object} comp 대상컴포넌트
 * @param {function} callFunc 실행Function
 * @author ITG
 * @since 1.0 
 */
function attachEnterKeyEvent(comp, callFunc) {
	comp.on("render", function() {
		Ext.create('Ext.util.KeyNav', comp.getEl(), {
			"enter" : function(e) {
				var el = Ext.Element.getActiveElement();
				if (el.type == 'text') { // text field에 만 엔터키
					eval(callFunc); 	// Event적용
				}
			},
			scope : this
		});
	});
}

/**
 * @function
 * @summary 커스텀 이벤트 붙이기
 * @param {string} eventName 이벤트명
 * @param {object} comp 대상컴포넌트
 * @param {function} callFunc 실행 Function
 * @param {string[]} param 실행Function 파라미터
 * @author ITG
 * @since 1.0 
 */
function attachCustomEvent(eventName, comp, callFunc, param) {
	if (typeof param != "undefined") {
		comp.on(eventName, function() {
			callFunc(param);
		});
	} else {
		comp.on(eventName, callFunc);
	}
}

/**
 * @function
 * @summary 버튼 이벤트 붙이기
 * @param {object} comp 대상컴포넌트
 * @param {function} callFunc 실행 Function
 * @param {string[]} param 실행Function 파라미터
 * @author ITG
 * @since 1.0 
 */
function attachBtnEvent(comp, callFunc, param) {
	comp.on('click', function() {
		callFunc(param);
	});
}

/**
 * @function
 * @summary 버튼생성 컴포넌트
 * @param {json} compProperty
 * @property {boolean} visible 표시여부
 * @property {string} label 버튼 Text
 * @property {string} id 객체고유ID
 * @property {string} ui correct-파란색표시(화면의 Main이벤트)/ undefined-회색표시(default)
 * @property {string} scale medium-두껍게표시 / undefined-얇게표시(default)  
 * @return {object} btnObject
 * @author ITG
 * @since 1.0 
 */
function createBtnComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var label = that['label'];
	var id = that['id'];
	var handler = (that['handler']) ? { click : that['handler'] } : {};

	var ui = (that['ui']) ? (that['ui']) : 'default';
	var scale = (that['scale']) ? (that['scale']) : 'small';

	var btnObject = Ext.create('Ext.button.Button', {
		id : id,
		ui : ui,      
		scale : scale,
		icon : that['icon'] ? that['icon'] : '',
		tooltip : that['tooltip'] ? that['tooltip'] : label,
		enableState : that['enableState'],
		margin : that['margin'] ? that['margin'] : getConstValue('BUTTON_MARGIN'),
		text : label,
		listeners : handler,
		width : that['width'],
		multiYn : that['multiYn'], // 버튼제어용 변수(프로세스 구성/서비스 멀티조회에서 사용됨)
		ignore : false
	});
	var visibleF = that['visible'];
	if (visibleF == null) {
		visibleF = true;
	}
	btnObject.setVisible(visibleF);
	return btnObject;
}

/**
 * @function
 * @summary 초기화 버튼 컴포넌트
 * @param {json} compProperty
 * @property {string} id 객체고유ID
 * @return {object} btnObject
 * @author ITG
 * @since 1.0
 */
function createClearBtnComp(compProperty) {
	compProperty["icon"] = getConstValue('CONTEXT') + '/itg/base/images/ext-js/simple/btn-icons/reformat.gif';
	compProperty["tooltip"] = getConstText({ isArgs : true, m_key : 'btn.common.reset' });
	return createIconBtnComp(compProperty);
}

/**
 * @function
 * @summary 팝업 버튼 컴포넌트
 * @param {json} compProperty
 * @property {string} id 객체고유ID
 * @return {object} btnObject
 * @author ITG
 * @since 1.0
 */
function createPopBtnComp(compProperty) {
	if (compProperty == null) {
		compProperty = {};
	}
	compProperty["icon"] = getConstValue('CONTEXT') + '/itg/base/images/ext-js/simple/btn-icons/search.gif';
	compProperty["tooltip"] = getConstText({isArgs : true, m_key : 'btn.common.search'});
	return createIconBtnComp(compProperty);
}

/**
 * @function
 * @summary Icon 버튼 컴포넌트
 * @param {json} compProperty
 * @property {string} id 객체고유ID 
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createIconBtnComp(compProperty) {
	var that = convertDomToArray(compProperty);
	var handler = (that['handler']) ? {
		click : that['handler']
	} : {};
	var btnObject = Ext.create('Ext.button.Button', {
		id : that['id'],
		icon : that['icon'],
		width : that['width'] ? that['width'] : getConstValue('DEFAULT_BUTTON_WIDTH'),
		tooltip : that['tooltip'] ? that['tooltip'] : '',
		margin : that['margin'] ? that['margin'] : getConstValue('WITH_BUTTON_MARGIN'),
		listeners : handler,
		disabled : that['disabled'] ? that['disabled'] : false,
		hidden : that['hidden'] ? that['hidden'] : false,
		ignore : false
	});
	return btnObject;
}

/**
 * @function
 * @summary 코드 콤보박스 컴포넌트
 * @param {json} compProperty
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name
 * @property {string} code_grp_id 코드그룹ID
 * @property {boolean} attachAll 콤보박스에 "---전체---" 표시여부
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createCodeComboBoxComp(compProperty) {
	var param = {};
	for (var prop in compProperty) {
		param[prop] = compProperty[prop];
	}

	var combo_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type : 'ajax',
		url : (compProperty['url']) ? compProperty['url'] : '/itg/base/searchCodeDataList.do',
		jsonData : param,
		actionMethods : {
			create : 'POST',
			read : 'POST',
			update : 'POST'
		},
		reader : {
			type : 'json',
			root : 'gridVO.rows'
		}
	});

	compProperty['proxy'] = combo_proxy;
	compProperty['valueField'] = (compProperty['valueField']) ? compProperty['valueField'] : 'CODE_ID';
	compProperty['displayField'] = (compProperty['displayField']) ? compProperty['displayField'] : 'CODE_TEXT';
	compProperty['valueDesc'] = (compProperty['valueDesc']) ? compProperty['valueDesc'] : 'CODE_DESC';
	var combo_comp = createComboBoxComp(compProperty);

	return combo_comp;
}

/**
 * @function
 * @summary 요청지역 콤보박스 컴포넌트
 * @param {json} compProperty
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name
 * @property {string} id 객체고유ID 
 * @property {boolean} attachAll 콤보박스에 "---전체---" 표시여부
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createReqZoneComboBoxComp(compProperty) {
	var param = {};
	for (var prop in compProperty) {
		param[prop] = compProperty[prop];
	}
	var combo_proxy = Ext.create('nkia.custom.JsonProxy', {
				defaultPostHeader : "application/json",
				noCache : false,
				type : 'ajax',
				url : '/itg/base/searchReqZoneDataList.do',
				jsonData : param,
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST'
				},
				reader : {
					type : 'json',
					root : 'gridVO.rows'
				}
			});

	compProperty['proxy'] = combo_proxy;
	compProperty['valueField'] = 'CUST_ID';
	compProperty['displayField'] = 'CUST_NM';
	var combo_comp = createComboBoxComp(compProperty);
	return combo_comp;
}

/**
 * @function
 * @summary 요청지역 팝업 컴포넌트
 * @param {json} compProperty
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name 
 * @property {boolean} leafSelect 최하위노드만 선택가능 여부 
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createReqZonePopupComp(compProperty) {
	var t_comp = null;
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var name = that["name"];
	var targetName = that["name"] + "_nm";
	var targetHiddenName = that["name"];
	var popUpTitle = getConstText({isArgs : true, m_key : 'res.label.system.userReq.00001'});
	var targetLabel = that["label"] ? that["label"] : getConstText({isArgs : true, m_key : 'res.label.system.userReq.00002'});
	var rnode_text = getConstText({isArgs : true, m_key : 'res.label.system.userReq.00003'});

	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var labelWidth = that['labelWidth'] ? that['labelWidth'] : null;

	var textField;
	if (labelWidth == null) {
		textField = createTextFieldComp({label : targetLabel, notNull : notNull, name : targetName, width : fieldWidth, readOnly : true});
	} else {
		textField = createTextFieldComp({labelWidth : labelWidth, label : targetLabel, notNull : notNull, name : targetName, width : fieldWidth, readOnly : true});
	}

	textField.on("afterrender", function() {
		addClass(textField.inputEl.dom, "field-readonly");
	});

	var hiddenField = createHiddenFieldComp({
		name : targetHiddenName,
		value : that["userReqZoneId"]
	});
	var textBtn = createPopBtnComp({
		id : name + "_btn"
	});
	var textInitBtn = createClearBtnComp({
		id : name + "_btn" + 'init',
		handler : function() {
			textField.setValue('');
			hiddenField.setValue('');
		}
	});
	var fieldItems = [textField, textBtn, textInitBtn, hiddenField];

	var leafSelect = that['leafSelect'];
	if (leafSelect == null) {
		leafSelect = false;
	}

	var rootSelect = that["rootSelect"];
	if (rootSelect == null) {
		rootSelect = true;
	}

	textBtn.on('click', function() {
		var setPopUpProp = {
			popId : name + "_comp_popup",
			treeId : name + "_comp_tree",
			popUpTitle : popUpTitle,
			context : '${context}',
			url : '/itg/system/reqzone/searchReqZoneTree.do',
			title : that['title'],
			targetField : textField,
			targetHiddenField : hiddenField,
			leafSelect : leafSelect,
			rootSelect : rootSelect,
			// expandAllBtn : that['expandAllBtn'],
			// collapseAllBtn : that['collapseAllBtn'],
			expandLevel : 2,
			params : that['params'] ? that['params'] : {},
			rnode_text : rnode_text
		};
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	});

	var t_comp = null;
	t_comp = {
		xtype : 'nkiafieldcontainer',
		name : that["name"] + "_container",
		labelAlign : 'right',
		beforeLabelTextTpl : requier,
		allowBlank : allowBlank,
		width : getConstValue('DEFAULT_FIELD_WIDTH'),
		layout : 'hbox',
		items : fieldItems,
		listeners : {
			afterrender : function() {
				textField.setValue(that["userReqZoneNm"]);
				hiddenField.setValue(that["userReqZoneId"]);
			}
		}
	}

	return t_comp;
}

/**
 * @function
 * @summary 콤보박스 콤포넌트
 * @param {json} compProperty
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name
 * @property {string} id 객체고유ID
 * @property {boolean} notNull 필수여부
 * @property {boolean} attachAll 콤보박스에 "---전체---" 표시여부 
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createComboBoxComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var defaultValue = null;
	if (that['defaultValue'] != null && that['defaultValue'] != '') {
		defaultValue = that['defaultValue'];
	} else {
		defaultValue = '';
	}
	var notNull = that['notNull'];

	var allowBlank = true;

	var labelTxtTpl = "";
	if (notNull) {
		labelTxtTpl += getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}
	if(that['emsYn']){
		var emsValue = "EMS : " + that['emsValue'];
		if( that['amValue'] == that['emsValue'] ){
			labelTxtTpl += replaceAll(getConstValue('EMS_EQUAL_SYMBOL'), "#{value}", "title=" + that['emsValue']);
		}else{
			labelTxtTpl += replaceAll(getConstValue('EMS_UNEQUAL_SYMBOL'), "#{value}", "title=" + that['emsValue']);
		}
	}
	if(that['dcaYn']){
		var dcaValue = "DCA : " + that['dcaValue'];
		if( that['amValue'] == that['dcaValue'] ){
			labelTxtTpl += replaceAll(getConstValue('DCA_EQUAL_SYMBOL'), "#{value}", "title=" + that['dcaValue']);
		}else{
			labelTxtTpl += replaceAll(getConstValue('DCA_UNEQUAL_SYMBOL'), "#{value}", "title=" + that['dcaValue']);
		}
	}
	var hideLabel = (that['hideLabel']) ? (that['hideLabel']) : false;

	var label = that['label'];
	if (label != null && label != "") {
		hideLabel = false;
	} else {
		hideLabel = true;
	}
	var name = that['name'];

	var combo_comp = null;

	var valueField = that['valueField'];
	var displayField = that['displayField'];
	var valueDesc = (that['valueDesc']) ? (that['valueDesc']) : null;

	var fields = [];
	if (null != valueDesc) {
		fields = [valueField, displayField, valueDesc]
	} else {
		fields = [valueField, displayField]
	}

	Ext.define('SelectModel', {
		extend : 'Ext.data.Model',
		fields : fields
	});

	var storeData = [];
	if (that['storeData'] != null) {
		storeData = [];
		if (that['emptyText'] != null && that['emptyText'] != '') {
			var emptyRow = {};
			var text = '';
			emptyRow[valueField] = '';
			emptyRow[displayField] = that['emptyText'];
			storeData.push(emptyRow);
		}
		for (var i = 0; i < that['storeData'].length; i++) {
			storeData.push(that['storeData'][i]);
		}
	}

	var proxy = that['proxy'];
	var autoLoad = null;

	var comboStore = null;
	if (proxy != null) {
		comboStore = Ext.create('Ext.data.Store', {
			model : 'SelectModel',
			filterOnLoad : true,
			proxy : proxy,
			autoLoad : true,
			filters : [function(item) {
				var result = true;
				if (that['filteredData'] != null) {
					for (var i = 0; i < that['filteredData'].length; i++) {
						if (item.raw.CODE_ID == that['filteredData'][i]) {
							result = false;
							break;
						}
					}
				}
				return result;
			}]
		});
	} else {
		comboStore = Ext.create('Ext.data.Store', {
			model : 'SelectModel',
			noChange : false,
			data : storeData
		});
	}

	combo_comp = Ext.create('nkia.custom.field.ComboBox', {
		id : (that['id']) ? that['id'] : "",
		displayField : that['displayField'],
		valueField : that['valueField'],
		fieldLabel : label,
		queryMode : 'local',
		labelSeparator : '',
		labelWidth : (that['labelWidth']) ? that['labelWidth'] : getConstValue('LABEL_WIDTH'),
		hideLabel : hideLabel,
		allowBlank : allowBlank,
		store : comboStore,
		noChange : (that['noChange']) ? that['noChange'] : false,
		labelAlign : 'right',
		beforeLabelTextTpl : labelTxtTpl,
		defaultValue : defaultValue,
		listConfig : {
			emptyText : '조회 결과 항목이 없습니다.'
		},
		matchFieldWidth : true,
		attachChoice : that['attachChoice'] ? that['attachChoice'] : false,
		attachAll : that['attachAll'] ? that['attachAll'] : false,
		attachCustom : that['attachCustom'] ? that['attachCustom'] : false,
		attachCustomData : that['attachCustomData'] ? that['attachCustomData'] : null,
		padding : that['padding'] ? that['padding'] : '0 0 0 0',
		xtype : 'combobox',
		name : name,
		validator : function(v) {
			var idx = this.getSelectedIndex();
			var val = "";
			if (idx > -1) {
				val = this.getValue();
			}
			return commValidator(this, val, that);
		},
		validateOnBlur : true,
		getSelectedIndex : function() {
			var v = this.getValue();
			var r = this.findRecord(this.valueField || this.displayField, v);
			return (this.store.indexOf(r));
		},
		editable : false,
		editMode : that["editMode"] ? that["editMode"] : false,
		setEditMode : function(flag) {
			this.editMode = flag;
		},
		getEditMode : function() {
			return this.editMode;
		},
		readOnly : that["readOnly"] ? that["readOnly"] : false,
		isNotColspan : that["isNotColspan"],
		widthFactor: that['widthFactor']	// Width Factor(콤보박스의 Width조절)
	});

	combo_comp.store.on('load', function(store) {
		var comboData = {};
		if (combo_comp.attachCustom) {
			comboData[combo_comp.valueField] = "";
			var customDataArray = that['attachCustomData'];
			if( typeof customDataArray != "undefined" ){
				for (var i = 0; i < customDataArray.length; i++) {
					var customData = customDataArray[i];
					comboData[combo_comp.displayField] = customData.display;
					comboData[combo_comp.valueField] = customData.value;
					store.insert(combo_comp.store.length + i, comboData);
				}
			}
		}

		if (combo_comp.attachChoice) {
			comboData[combo_comp.valueField] = "";
			comboData[combo_comp.displayField] = getConstText({m_key : 'COMBO_CHOICE'});
			store.insert(0, comboData);
		}

		if (combo_comp.attachAll) {
			comboData[combo_comp.valueField] = "";
			comboData[combo_comp.displayField] = getConstText({m_key : 'COMBO_ALL'});
			store.insert(0, comboData);
		}

		// Store load 이벤트 예외처리
		if (combo_comp.noChange == true || this.noChange == true) {
			this.noChange = false;
			return combo_comp;
		}

		var comboValue = combo_comp.getValue();
		if (combo_comp.defaultValue != "") {
			combo_comp.select(combo_comp.defaultValue);
		} else {
			if (nullToSpace(comboValue) == "") {
				if (proxy != null) {
					if (store.count() > 0) {
						if (!(isNullOrSpace(store.getAt(0).raw[combo_comp.valueField]) && isNullOrSpace(store.getAt(0).raw[combo_comp.displayField]))) {
							combo_comp.select(store.getAt(0).raw[combo_comp.valueField]);
						}
					}
				} else {
					if (storeData[0][valueField] != null && storeData[0][valueField] != "") {
						combo_comp.select(storeData[0][valueField]);
					}
				}
			}
		}
		combo_comp.clearInvalid();
	});
	return combo_comp;
}

/**
 * @deprecated
 * @author ITG
 * @since 1.0 
 */
function createSearchDateFieldComp(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var t_comp = null;

	var label = that['label'];
	var name = that['name'];
	var format = that['format'];

	var allowBlank = true;
	if (typeof that['notNull'] != "undefined" && that['notNull'] == true) {
		allowBlank = false;
	}

	var disabled = false;
	if (typeof that['disabled'] != "undefined" && that['disabled'] == true) {
		disabled = true
	}

	var nowDate = Ext.Date.format(new Date(), 'Y-m-d');

	var startInput = Ext.create('nkia.custom.Date', {
		margin : that['margin'] ? that['margin'] : '0 2 0 0',
		name : (that['start_dateName']) ? that['start_dateName'] : name + '_startDate',
		dateType : 'D',
		allowBlank : allowBlank,
		dateValue : that['start_dateValue'],
		format : (format) ? format : 'Y-m-d',
		limitNowDate : that['limitNowDate'],
		disabled : disabled
	});
	
	var endInput = Ext.create('nkia.custom.Date', {
		margin : that['margin'] ? that['margin'] : '0 0 0 0',
		allowBlank : allowBlank,
		name : (that['end_dateName']) ? that['end_dateName'] : name + '_endDate',
		dateType : 'D',
		dateValue : that['end_dateValue'],
		format : (format) ? format : 'Y-m-d',
		disabled : disabled
	});
	t_comp = {
		xtype : 'fieldcontainer',
		id : that['id'] ? that['id'] : "",
		fieldLabel : label,
		labelSeparator : '',
		labelAlign : 'right',
		// labelStyle : getConstValue('LABEL_STYLE'),
		labelWidth : (that['labelWidth']) ? that['labelWidth'] : getConstValue('LABEL_WIDTH'),
		width : (that['labelWidth']) ? 260 + that['labelWidth'] : 260 + getConstValue('LABEL_WIDTH'),
		// minWidth: 100 + 20,
		combineErrors : true,
		layout : 'hbox',
		defaults : {
			hideLabel : true
		},
		items : [startInput, endInput/*
										 * , createClearBtnComp({margin:
										 * getConstValue('SEARCH_DATE_BTN_MARGIN'),
										 * handler: function(){
										 * startInput.setDayValue('');
										 * endInput.setDayValue(''); }})
										 */
		]
	}

	startInput.dateField.onChange = function() {

		var sDate = startInput.getValue();
		var eDate = endInput.getValue();

		if (startInput.limitNowDate) {
			if (startInput.getValue() < nowDate) {
				alert("시작일은 현재일 이전일 수 없습니다.");
				startInput.setValue("");
				return;
			}
		}

		// 시작 날짜를 바꿨는데 종료날짜가 비어 있으면 종료날짜를 -> 바꾼 시작날짜와 같게 세팅
		if (eDate == "" || eDate == null) {
			endInput.setValue(sDate);
			endInput.dateField.clearInvalid();
		}
		// 시작 날짜를 바꿨는데 시작날짜가 종료날짜보다 높은 날짜라면 시작날짜를 종료날짜와 같게 세팅
		else {
			if (sDate > eDate) {
				alert('시작일은 종료일 이후로 선택하실 수 없습니다.');
				startInput.setValue(eDate);
			}
		}

		startInput.dateField.clearInvalid();
	}

	endInput.dateField.onChange = function() {

		var sDate = startInput.getValue();
		var eDate = endInput.getValue();

		// 종료 날짜를 바꿨는데 시작날짜가 비어 있으면 시작날짜를 -> 바꾼 종료날짜와 같게 세팅
		if (sDate == "" || sDate == null) {
			startInput.setValue(eDate);
			startInput.dateField.clearInvalid();
		}
		// 종료 날짜를 바꿨는데 종료날짜가 시작날짜보다 낮은 날짜라면 종료날짜를 시작날짜와 같게 세팅
		else {
			if (sDate > eDate) {
				alert('종료일은 시작일 이전으로 선택하실 수 없습니다.');
				endInput.setValue(sDate);
			}
		}
		endInput.dateField.clearInvalid();
	}

	return t_comp;
}

/**
 * @deprecated
 * @author ITG
 * @since 1.0 
 */
function createSearchDateFieldClearComp(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var t_comp = null;

	var label = that['label'];
	var name = that['name'];
	var format = that['format'];

	var allowBlank = true;
	if (typeof that['notNull'] != "undefined" && that['notNull'] == true) {
		allowBlank = false;
	}

	var disabled = false;
	if (typeof that['disabled'] != "undefined" && that['disabled'] == true) {
		disabled = true
	}

	var nowDate = Ext.Date.format(new Date(), 'Y-m-d');

	var startInput = Ext.create('nkia.custom.Date', {
				width : 95,
				minWidth : 95,
				margin : that['margin'] ? that['margin'] : '0 2 0 0',
				name : (that['start_dateName']) ? that['start_dateName'] : name
						+ '_startDate',
				dateType : 'D',
				allowBlank : allowBlank,
				dateValue : that['start_dateValue'],
				format : (format) ? format : 'Y-m-d',
				limitNowDate : that['limitNowDate'],
				disabled : disabled
			});

	var endInput = Ext.create('nkia.custom.Date', {
				width : 95,
				minWidth : 95,
				margin : that['margin'] ? that['margin'] : '0 0 0 0',
				allowBlank : allowBlank,
				name : (that['end_dateName']) ? that['end_dateName'] : name
						+ '_endDate',
				dateType : 'D',
				dateValue : that['end_dateValue'],
				format : (format) ? format : 'Y-m-d',
				disabled : disabled
			});

	t_comp = {
		xtype : 'fieldcontainer',
		id : that['id'] ? that['id'] : "",
		fieldLabel : label,
		labelSeparator : '',
		labelAlign : 'right',
		// labelStyle : getConstValue('LABEL_STYLE'),
		labelWidth : (that['labelWidth'])
				? that['labelWidth']
				: getConstValue('LABEL_WIDTH'),
		width : (that['labelWidth']) ? 260 + that['labelWidth'] : 260
				+ getConstValue('LABEL_WIDTH'),
		// minWidth: 100 + 20,
		combineErrors : true,
		layout : 'hbox',
		defaults : {
			hideLabel : true
		},
		items : [startInput, endInput, createClearBtnComp({
							margin : getConstValue('SEARCH_DATE_BTN_MARGIN'),
							handler : function() {
								startInput.setDayValue('');
								endInput.setDayValue('');
							}
						})]
	}

	startInput.dateField.onChange = function() {

		var sDate = startInput.getValue();
		var eDate = endInput.getValue();

		if (startInput.limitNowDate) {
			if (startInput.getValue() < nowDate) {
				alert("시작일은 현재일 이전일 수 없습니다.");
				startInput.setValue("");
				return;
			}
		}

		// 시작 날짜를 바꿨는데 종료날짜가 비어 있으면 종료날짜를 -> 바꾼 시작날짜와 같게 세팅
		if (eDate == "" || eDate == null) {
			endInput.setValue(sDate);
			endInput.dateField.clearInvalid();
		}
		// 시작 날짜를 바꿨는데 시작날짜가 종료날짜보다 높은 날짜라면 시작날짜를 종료날짜와 같게 세팅
		else {
			if (sDate > eDate) {
				alert('시작일은 종료일 이후로 선택하실 수 없습니다.');
				startInput.setValue(eDate);
			}
		}

		startInput.dateField.clearInvalid();
	}

	endInput.dateField.onChange = function() {

		var sDate = startInput.getValue();
		var eDate = endInput.getValue();

		// 종료 날짜를 바꿨는데 시작날짜가 비어 있으면 시작날짜를 -> 바꾼 종료날짜와 같게 세팅
		if (sDate == "" || sDate == null) {
			startInput.setValue(eDate);
			startInput.dateField.clearInvalid();
		}
		// 종료 날짜를 바꿨는데 종료날짜가 시작날짜보다 낮은 날짜라면 종료날짜를 시작날짜와 같게 세팅
		else {
			if (sDate > eDate) {
				alert('종료일은 시작일 이전으로 선택하실 수 없습니다.');
				endInput.setValue(sDate);
			}
		}
		endInput.dateField.clearInvalid();
	}

	return t_comp;
}

/**
 * @function
 * @summary 날짜 필드 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createDateFieldComp(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var t_comp = Ext.create('nkia.custom.Date', {
		id : that['id'],
		label : that['label'],
		name : that['name'],
		dateType : that['dateType'],
		dateValue : that['dateValue'],
		format : that['format'],
		notNull : that['notNull'] ? that['notNull'] : false,
		readOnly : that['readOnly'] ? that['readOnly'] : false,
		compProperty : compProperty
	});
	return t_comp;
}

/**
 * @function
 * @summary TextArea 필드 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createTextAreaFieldComp(compProperty) {
	var that = convertDomToArray(compProperty);
	var compnent = null;

	var emptyText = null;
	if (that["emptyText"]) {
		// Browser에 따라 Placeholder이 달라서 아래와 같이 처리함.
		if (Ext.isGecko || Ext.isWebKit) { emptyText = replaceAll(that["emptyText"], '/n', "" + String.fromCharCode(10));
		} else {
			emptyText = replaceAll(that["emptyText"], '/n', "" + String.fromCharCode(13) + String.fromCharCode(10));
		}
	}
	
	// 2015.07.07.정정윤추가 : TextArea 안에서 Tab키 입력이 가능하기 위한 파라메터 추가
	var allowTabKey = ( that["allowTabKey"] ) ? that["allowTabKey"] : false;
	
	compnent = Ext.create('nkia.custom.field.TextArea', {
		xtype : 'textareafield',
		name : that['name'] ? that['name'] : '',
		fieldLabel : that['label'] ? that['label'] : '',
		value : that['value'] ? that['value'] : '',
		height : that['height'] ? that['height'] : getConstValue('DEFAULT_TEXTAREA_HEIGHT'),
		readOnly : that['readOnly'] ? that['readOnly'] : false,
		beforeLabelTextTpl : that['notNull'] ? getConstValue('NOT_NULL_SYMBOL') : '',
		vtype : that['vtype'] ? that['vtype'] : '',
		editMode : that["editMode"] ? that["editMode"] : false,
		beforeSubTpl : that['beforeSubTpl'],
		afterSubTpl : that['afterSubTpl'],
		emptyText : that['emptyText'] ? emptyText : null,
		allowOnlyWhitespace : false,
		allowBlank : ( typeof that["notNull"] != "undefined" ) ? !that["notNull"] : true,
		enableKeyEvents : allowTabKey,	// 2015.07.07.정정윤추가 : TextArea 안에서 Tab키 입력이 가능하기 위한 속성 추가
		validator : function(v) {
			return commValidator(this, v, that);
		},
		listeners : {
			// 포커스로 인해 버튼 두번 클릭을 해야될 상황이 발생될 경우 처리로직 ( 문제발생시 주석처리해주세요. )
			focus : function(field) {
				field.onFocus = true;

				var agt = navigator.userAgent;
				if (agt.indexOf("MSIE") != -1) {
					if (this.emptyText && this.valueContainsPlaceholder) {
						this.setRawValue('');
						this.isEmpty = true;
						this.inputEl.removeCls(this.emptyCls);
						this.valueContainsPlaceholder = false;
					}
				}
			}
		},
		widthFactor: that['widthFactor']
	});

	/*
	 * compnent.on("boxready", function( field ){ // 포커스로 인해 버튼 두번 클릭을 해야될 상황이
	 * 발생될 경우 처리로직 ( 문제발생시 주석처리해주세요. )
	 * 
	 * field.el.on("mouseout", function(){ if( field.onFocus ){ field.blur();
	 * field.onFocus = false; } });
	 * 
	 * 
	 * if( typeof that['width'] == "string" ){ var dom = field.getEl().dom;
	 * dom.style.width = that['width'];
	 * dom.parentNode.parentNode.parentNode.parentNode.style.width =
	 * that['width']; } });
	 */
	
	// 2015.07.07.정정윤추가 : 생성시, allowTabKey 파라메터를 'true'로 주면 TextArea 안에서 Tab키 입력이 가능함
	if(allowTabKey) {
		compnent.on("specialkey", function(fd, e) {
			if (e.getKey() == e.TAB) {
				e.stopEvent();
				var el = fd.inputEl.dom;
				if (el.setSelectionRange) {
					var withIns = el.value.substring(0, el.selectionStart) + '\t';
					var pos = withIns.length;
					el.value = withIns + el.value.substring(el.selectionEnd, el.value.length);
					el.setSelectionRange(pos, pos);
				} else if (document.selection) {
					document.selection.createRange().text = '\t';
				}
			}
		});
	}

	return compnent;
}

/**
 * @function
 * @summary json 객체 복사
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function convertDomToArray(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	return that;
}

/**
 * @function
 * @summary 코드 라디오 버튼 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createCodeRadioComp(compProperty) {
	var codeGrpId = compProperty['code_grp_id'];
	var multiCode = compProperty['multiCode'];
	compProperty['storeData'] = getCodeDataList(codeGrpId, multiCode);
	compProperty['valueField'] = 'CODE_ID';
	compProperty['displayField'] = 'CODE_TEXT';

	var combo_comp = createRadioComp(compProperty);

	return combo_comp;
}

/**
 * @function
 * @summary 라디오 버튼 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createRadioComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;

	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var label = that['label'];
	var name = that['name'];

	var itemsInGroup = [];

	var c_flag = true;

	if (that['storeData']) {
		for (var i = 0; i < that['storeData'].length; i++) {
			if (i == 0) {
				c_flag = true;
			} else {
				c_flag = false;
			}
			if (typeof that['defaultChecked'] != "undefined") {
				c_flag = that['defaultChecked'];
			}

			var rowData = that['storeData'][i];

			var boxLabel = rowData[that['displayField']];
			var inputValue = rowData[that['valueField']];

			var radioField = Ext.create('Ext.form.field.Radio', {
				boxLabel : boxLabel,
				name : name,
				inputValue : inputValue,
				checked : c_flag
			})
			itemsInGroup.push(radioField);
		}
	}

	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH');
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var line = (that['line']) ? that['line'] : 1;
	var columns = Math.round(itemsInGroup.length / line);
	var columnSize = (that['columnSize'] != null) ? that['columnSize'] : 80;
	var height = getConstValue('DEFAULT_FIELD_HEIGHT') * parseInt(line);
	
	t_comp = Ext.create('nkia.custom.RadioGroup', {
		xtype : 'radiogroup',
		beforeLabelTextTpl : requier,
		name : name,
		fieldLabel : (label) ? label : "",
		columns : columns,
		items : itemsInGroup,
		isNotColspan : that["isNotColspan"]
	});
	return t_comp;
}

/**
 * @function
 * @summary 텍스트 필드 컴포넌트
 * @param {json} compProperty
 * @property {string} label 필드 Text
 * @property {string} id 객체 고유ID
 * @property {string} name 객체 Input Name
 * @property {boolean} notNull 필수 여부
 * @property {boolean} readOnly 읽기 전용 여부
 * @property {int} widthFactor 넓이 비율(0.1 ~ 1.0)
 * @property {string} emptyText 공백일 경우 표시할 문구
 * @property {string} value Input 객체의 초기 표시 값  
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createTextFieldComp(compProperty) {

	var that = convertDomToArray(compProperty);
	var compnent = null;
	var labelTxtTpl = "";

	if (that['notNull']) {
		labelTxtTpl += getConstValue('NOT_NULL_SYMBOL');
	}
	if (that['emsYn']) {
		labelTxtTpl += getConstValue('EMS_SYMBOL');
	}
	
	compnent = Ext.create('nkia.custom.field.Text', {
		xtype : 'textfield',
		id : that['id'] ? that['id'] : "",
		fieldLabel : that['label'] ? that['label'] : '',
		name : that['name'] ? that['name'] : '',
		value: that['value'] ? that['value'] : '',
		allowBlank : ( typeof that["notNull"] != "undefined" ) ? !that["notNull"] : true,
		hideLabel : that['hideLabel'] ? that['hideLabel'] : false,
		inputType : that['inputType'] ? that['inputType'] : "text",
		labelAlign : that['labelAlign'] ? that['labelAlign'] : getConstValue('DEFAULT_LABEL_ALIGN'),
		beforeLabelTextTpl : labelTxtTpl,
		enableKeyEvents : that['enableKeyEvents'] ? that['enableKeyEvents'] : false,
		afterLabelTextTpl : that['afterLabelTextTpl'] ? that['afterLabelTextTpl'] : '',
		readOnly : that['readOnly'] ? that['readOnly'] : false,
		vtype : that['vtype'] ? that['vtype'] : '',
		padding : that['padding'] ? that['padding'] : '0 0 0 0',
		hidden : (typeof that['hidden']) ? that['hidden'] : false,
		editMode : that["editMode"] ? that["editMode"] : false,
		fieldStyle : that["fieldStyle"],
		emptyText : that['emptyText'] ? that['emptyText'] : null,
		isNotColspan: that["isNotColspan"],
		validator : function(v) {
			return commValidator(this, v, that);
		},
		widthFactor: that['widthFactor']
	});
	return compnent;

}

/**
 * @function
 * @summary 숫자단위 필드 컴포넌트
 * @param {json} compProperty
 * @property {string} label 필드 Text
 * @property {string} id 객체 고유ID
 * @property {string} name 객체 Input Name
 * @property {string} value Input 객체의 초기 표시 값 
 * @property {boolean} notNull 필수 여부
 * @property {boolean} readOnly 읽기 전용 여부
 * @property {string} emptyText 공백일 경우 표시할 문구
 * @property {string} vtype 입력값의 데이터 타입
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createNumberUnitFieldComp(compProperty) {
	var that = convertDomToArray(compProperty);
	var compnent = null;
	compnent = Ext.create('nkia.custom.field.NumberUnit', {
		labelWidth : that['labelWidth'] ? that['labelWidth'] : getConstValue('LABEL_WIDTH'),
		labelSeparator : that['labelSeparator'] ? that['labelSeparator'] : '',
		hideLabel : that['hideLabel'] ? that['hideLabel'] : false,
		inputType : that['inputType'] ? that['inputType'] : "text",
		height : getConstValue('DEFAULT_FIELD_HEIGHT'),          
		fieldLabel : that['label'] ? that['label'] : '',
		labelAlign : that['labelAlign'] ? that['labelAlign'] : getConstValue('DEFAULT_LABEL_ALIGN'),
		beforeLabelTextTpl : that['notNull'] ? getConstValue('NOT_NULL_SYMBOL') : '',
		afterLabelTextTpl : that['afterLabelTextTpl'] ? that['afterLabelTextTpl'] : '',
		readOnly : that['readOnly'] ? that['readOnly'] : false,
		hidden : that['hidden'] ? that['hidden'] : false,
		name : that['name'] ? that['name'] : '',
		id : that['id'] ? that['id'] : "",
		allowBlank : ( typeof that["notNull"] != "undefined" ) ? !that["notNull"] : true,
		value : that['value'] ? that['value'] : 0,
		vtype : that['vtype'] ? that['vtype'] : 'number',
		padding : that['padding'] ? that['padding'] : '0 0 0 0',
		fieldStyle : 'text-align: right;',
		isNotColspan: that["isNotColspan"],
		validator : function(v) {
			return commValidator(this, v, that);
		},
		// @@ 20140226 고은규 : 금액입력시 자동 콤마생성
		listeners : {
			el : {
				keyup : function() {
					var comp = Ext.getCmp(this.id);
					var value = comp.getValue();
					nkia.custom.field.NumberUnit.superclass.setValue.call(comp, value != null ? value.toString().replace('.',comp.decimalSeparator) : value);
					comp.setRawValue(comp.getFormattedValue(comp.getValue()));
				}
			}
		}
	});

	compnent.on("keyup", function(field) {
		var value = component.getValue();
		Ext.ux.form.NumericField.superclass.setValue.call(component, value != null ? value.toString().replace('.', component.decimalSeparator) : value);
		component.setRawValue(component.getFormattedValue(component.getValue()));
	});
	return compnent;

}

/**
 * @function
 * @summary Hidden 필드 컴포넌트
 * @param {json} compProperty
 * @property {string} id 객체 고유ID
 * @property {string} name 객체 Input Name
 * @property {string} value Input 객체의 초기 표시 값 
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createHiddenFieldComp(compProperty) {
	var that = convertDomToArray(compProperty);
	var t_comp = Ext.create('Ext.form.field.Hidden', {
		xtype : 'hiddenfield',
		name : that['name'] ? that['name'] : "",
		id : that['id'] ? that['id'] : "",
		value : that['value'] ? that['value'] : "",
		width : 0
	});
	return t_comp;
}

/**
 * @function
 * @summary Number 필드 컴포넌트
 * @param {json} compProperty 
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createNumberFieldComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;

	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var labelWidth = getConstValue('LABEL_WIDTH');

	var label = that['label'];
	var name = that['name'];
	var updateable = true;
	if (that['updateable'] != null) {
		updateable = that['updateable'];
	}

	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH');
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	t_comp = {
		xtype : 'numberfield',
		labelWidth : labelWidth,
		labelSeparator : '',
		updateable : updateable,
		inputType : (that['inputType']) ? that['inputType'] : "text",
		height : (that['height']) ? that['height'] : "",
		fieldLabel : (label) ? label : "",
		labelAlign : that['labelAlign'] ? that['labelAlign'] : getConstValue('DEFAULT_LABEL_ALIGN'),
		beforeLabelTextTpl : requier,
		allowBlank : allowBlank,
		name : (name) ? name : "",
		id : (that['id']) ? that['id'] : "",
		width : fieldWidth,
		value : (that['value']) ? that['value'] : "",
		minWidth : fieldWidth,
		// @@ 20130808 고은규 minValue 추가
		minValue : (that['minValue']) ? that['minValue'] : 0
	}
	return t_comp;
}

/**
 * @function
 * @summary 싱글 체크 박스 필드 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createSingleCheckBoxFieldComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;

	var labelWidth = getConstValue('LABEL_WIDTH');

	var label = that['label'];
	var name = that['name'];

	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH');
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}
	if (that['labelWidth'] != null) {
		labelWidth = that['labelWidth'];
	}

	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	t_comp = Ext.create('Ext.form.field.Checkbox', {
		labelWidth : labelWidth,
		labelSeparator : '',
		fieldLabel : (that['fieldLabel']) ? that['fieldLabel'] : "",
		labelAlign : 'right',
		height : (that['height']) ? that['height'] : "",
		hideLabel : that['hideLabel'] ? that['hideLabel'] : false,
		boxLabel : (label) ? label : "",
		name : (name) ? name : "",
		inputValue : (that['inputValue']) ? that['inputValue'] : "",
		checked : (that['checked']) ? that['checked'] : false,
		defaultValue : (that['defaultValue']) ? that['defaultValue'] : false,
		padding : (that['padding']) ? that['padding'] : '0 0 0 0',
		beforeLabelTextTpl : requier,
		allowBlank : allowBlank,
		width : fieldWidth,
		minWidth : fieldWidth
	});

	return t_comp;
}

/**
 * @function
 * @summary 코드 체크 박스 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createCodeCheckComp(compProperty) {
	var check_comp;
	var items = compProperty['items'];

	if (items == "" || items == null || items == "undefined") {// 체크박스 item
																// 동적생성
		var codeGrpId = compProperty['code_grp_id'];
		var multiCode = compProperty['multiCode'];
		compProperty['storeData'] = getCodeDataList(codeGrpId, multiCode);
		compProperty['itemName'] = 'CODE_ID';
		compProperty['boxLabel'] = 'CODE_TEXT';
		check_comp = createMultiCheckBoxFieldComp(compProperty);
	} else {// 체크박스 item 수동생성
		check_comp = createMultiCheckBoxFieldComp(compProperty);
	}
	return check_comp;
}

/**
 * @function
 * @summary 멀티 체크박스 필드 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createMultiCheckBoxFieldComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;

	var labelWidth = getConstValue('LABEL_WIDTH');

	var label = that['label'];
	var name = that['name'];

	var items;
	var itemsArray = [];
	
	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	if (that['items'] != null) {
		items = that['items'];
	} else {
		for (var i = 0; i < that['storeData'].length; i++) {
			var rowData = that['storeData'][i];

			var boxLabel = rowData[that['boxLabel']];
			var inputValue = rowData[that['itemName']];

			var checkItem = Ext.create('Ext.form.field.Checkbox', {
				xtype : "checkboxfield",
				boxLabel : boxLabel,
				name : name,
				inputValue : inputValue,
				defaultValue : (that['defaultValue']) ? that['defaultValue'] : false,
				readOnly : that["readOnly"] ? that["readOnly"] : false
			})
			itemsArray.push(checkItem);
		}
		items = itemsArray;
	}
	
	var line = (that['line']) ? that['line'] : 1;
	var columns = Math.round(itemsArray.length / line);
	var columnSize = (that['columnSize'] != null) ? that['columnSize'] : 80;
	var height = getConstValue('DEFAULT_FIELD_HEIGHT') * parseInt(line);
	
	t_comp = Ext.create('nkia.custom.CheckboxGroup', {
		xtype : 'checkboxgroup',
		beforeLabelTextTpl : requier,
		fieldLabel : (that['label']) ? that['label'] : "",
		name : name,
		columns : columns,
		items : items,
		isNotColspan : that["isNotColspan"]
	});

	return t_comp;
}

/**
 * @function
 * @summary 트리선택 팝업 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createTreeSelectPopComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var t_comp = null;

	var textField = createTextFieldComp({label : that['targetLabel'], notNull : notNull, name : that['targetName'], width: ( that["width"] ) ? that["width"] : getConstValue('DEFAULT_FIELD_WIDTH'), readOnly : true});
	var hiddenField = createHiddenFieldComp({name : that['targetHiddenName']});
	var textBtn = createPopBtnComp({id : that['btnId']});
	var textInitBtn = createClearBtnComp({
		id : that['btnId'] + 'init', 
		handler : function() { 
			textField.setValue('');
			hiddenField.setValue('');
		}
	});
	
	var fieldItems = [textField, textBtn, textInitBtn, hiddenField];

	// @@ 20141022 정정윤 추가 커스텀 readOnly 속성시 팝업선택버튼 숨김
	var customReadOnly = that['customReadOnly'] ? that['customReadOnly'] : false;
	if (customReadOnly) {
		fieldItems = [textField, hiddenField];
	}

	var leafSelect = that['leafSelect'];
	if (leafSelect == null) {
		leafSelect = true;
	}

	var rootSelect = that["rootSelect"];
	if (rootSelect == null) {
		rootSelect = true;
	}
	var expandLevel = that["expandLevel"];
	if (expandLevel == null) {
		expandLevel = 1;
	}

	if (that['targetHiddenName'] != null) {
		if (that['targetHiddenName'] == 'cust_id') {
			expandLevel = 2;
		}
	}

	textBtn.on('click', function() {
		var setPopUpProp = {
			popId : that['popId'],
			treeId : that['treeId'],
			popUpTitle : that['popUpTitle'] ? that['popUpTitle'] : '',
			context : '${context}',
			url : that['url'],
			title : that['title'],
			targetField : textField,
			targetHiddenField : hiddenField,
			leafSelect : leafSelect,
			rootSelect : rootSelect,
			expandAllBtn : that['expandAllBtn'],
			collapseAllBtn : that['collapseAllBtn'],
			expandLevel : expandLevel,
			params : that['params'] ? that['params'] : {},
			rnode_text : that['rnode_text'] ? that['rnode_text'] : '부서'
		};
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	});

	var t_comp = null;
	t_comp = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});
	return t_comp;
}

/**
 * @function
 * @summary 콤보박스 + 텍스트 입력 - 컨테이너
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createUnionCodeComboBoxTextContainer(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	
	// ComboBox Field
	var comboBoxField = createCodeComboBoxComp({label: that['label'], name: that['comboboxName'], code_grp_id: that['code_grp_id'],padding : CONSTRAINT.DEFAULT_FIELD_GAP, attachAll:true, widthFactor: that['widthFactor'], notNull:that['notNull']});
	// Text Field
	var textField = createTextFieldComp({name : that['textFieldName']});
	// Union Items
	var fieldItems = [comboBoxField, textField];

	var container = createUnionFieldContainer({
		items:fieldItems, 
		isNotColspan: that["isNotColspan"],
		getComboBoxField: function(){
			return comboBoxField;
		},
		getTextField: function(){
			return textField;
		}
	});
	return container;
}


/**
 * @function
 * @summary 외부사용자 단일 선택 팝업 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createOutUserSelectPopComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var labelWidth = getConstValue('LABEL_WIDTH');
	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	var notNull = (that['notNull']) ? that['notNull'] : false;
	var btnId = (that['btnId']) ? that['btnId'] : 'outUserSelectBtn';
	var isCustId = (that['isCustId']) ? that['isCustId'] : false;
	var thatEditFormId = (that['thatEditFormId']) ? that['thatEditFormId'] : "";
	var textLabel = (that['label']) ? that['label'] : getConstText({isArgs : true,m_key : 'res.label.itam.maint.00025'});
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var userNmField = createTextFieldComp({label :  textLabel, notNull : notNull, name : that['name'], width: ( that["width"] ) ? that["width"] : getConstValue('DEFAULT_FIELD_WIDTH'), readOnly : true});
	var userIdField = createHiddenFieldComp({name : that['hiddenName']});
	var textBtn = createPopBtnComp({id : btnId});
	var textInitBtn = createClearBtnComp({
		id : btnId + 'init', 
		handler : function() { 
			userNmField.setValue('');
			userIdField.setValue('');
		}
	});
	
	var fieldItems = [userNmField, textBtn, textInitBtn, userIdField];
	
	textBtn.on('click', function() {
		var user_id = null;
		var cust_nm = null;
		var targetFormId = null;
		var params = {};
		if (isCustId && thatEditFormId != "") {
			var c_id = Ext.getCmp(thatEditFormId).getFormField('cust_id').getValue();
			params["cust_id"] = c_id;
		}
		if (that['userId'] != "" && that['userId'] != null) {
			user_id = Ext.getCmp(that['userId']).getValue();
			params["user_id"] = user_id;
		}
		if (that["custId"] != "" && that['custId'] != null) {
			cust_id = that["custId"];
			params["cust_id"] = cust_id;
		}
		if (that['custNm'] != null
				&& that['targetFormId'] != null) {
			cust_nm = that['custNm'];
			targetFormId = that['targetFormId'];
		}
		if (that['sysAuth'] != null) {
			params["SYSAUTH"] = that['sysAuth'];
		}
		var setPopUpProp = {
			id : that['id'],
			popUpText : that['popUpText'],
			tagetUserIdField : userIdField,
			tagetUserNameField : userNmField,
			tagetEtcField : null,
			height : 433,
			width : 800,
			userId : that['userId'],
			custNm : cust_nm,
			targetFormId : targetFormId,
			url : '/itg/system/user/searchUserList.do',
			buttonAlign : 'center',
			bodyStyle : 'background-color: white;',
			params : params
		}
		var popUp = createUserSelectPop(setPopUpProp);
		popUp.show();
	});

	var t_comp = null;
	t_comp = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});
	
	return t_comp;
}

/**
 * @function
 * @summary 외부사용자 복수 선택 팝업 컴포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createOutUserMultiSelectPopComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var labelWidth = getConstValue('LABEL_WIDTH');

	var userNmField = createTextFieldComp({
				hideLabel : true,
				name : that['name'],
				width : that['width'] ? that['width'] : 120,
				readOnly : true
			});

	var userIdField = Ext.create('Ext.form.field.Hidden', {
				xtype : 'hidden',
				name : that['hiddenName']
			});

	var t_comp = null;
	t_comp = {
		xtype : 'fieldcontainer',
		fieldLabel : (that['label']) ? that['label'] : "",
		height : (that['height']) ? that['height'] : "",
		labelWidth : labelWidth,
		labelSeparator : '',
		labelAlign : 'right',
		width : getConstValue('DEFAULT_FIELD_WIDTH') + 60,
		combineErrors : true,
		layout : 'hbox',
		items : [userNmField, userIdField, createPopBtnComp({
			id : that['btnId'],
			handler : function() {
				var user_id = null;
				var cust_nm = null;
				var targetFormId = null;
				if (that['userId'] != "" && that['userId'] != null) {
					user_id = Ext.getCmp(that['userId']).getValue();
				}
				if (that['custNm'] != null
						&& that['targetFormId'] != null) {
					cust_nm = that['custNm'];
					targetFormId = that['targetFormId'];
				}
				var setPopUpProp = {
					id : that['id'],
					popUpText : that['popUpText'],
					tagetUserIdField : userIdField,
					tagetUserNameField : userNmField,
					tagetEtcField : null,
					height : 600,
					width : 600,
					userId : that['userId'],
					custNm : cust_nm,
					targetFormId : targetFormId,
					url : '/itg/system/user/searchUserList.do',
					buttonAlign : 'center',
					bodyPadding : '5 5 5 5 ',
					bodyStyle : 'background-color: white;',
					params : {
						user_id : user_id
					}
				}
				var popUp = createUserMultiSelectPop(setPopUpProp);
				popUp.show();
			}
		}), createClearBtnComp({
			id : that['btnId'] + 'Init',
			handler : function() {
				userNmField.setValue('');
				userIdField.setValue('');
				if (that['custNm'] != null
						&& that['targetFormId'] != null) {
					var theForm = Ext.getCmp(targetFormId);
					theForm.setFieldValue(that['custNm'], '');
				}
			}
		})]
	}
	return t_comp;
}

/**
 * @function
 * @summary 전화번호 필드 콤포넌트
 * @param {json} compProperty'
 * @property {string} label 필드 Text
 * @property {string} name 객체 Input Name 
 * @property {boolean} notNull 필수 여부
 * @property {int} firstFieldLength 선 입력 값 길이
 * @property {boolean} firstNumberNotNull 선 입력 값 필수 여부
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createPhoneFieldComp(compProperty) {
	var that = convertDomToArray(compProperty);

	var component = Ext.create('nkia.custom.phone', {
		label : that['label'],
		id : that['id'],
		name : that['name'],
		notNull : that['notNull'],
		firstFieldLength : (that['firstFieldLength']) ? that['firstFieldLength'] : 4,
		firstNumberNotNull : that['firstNumberNotNull']
	});

	return component;
}

/**
 * @deprecated
 * @summary 검증 필드 콤포넌트
 * @author ITG
 * @since 1.0 
 */
function checkOverlapComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;

	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var labelWidth = getConstValue('LABEL_WIDTH');

	var label = that['label'];
	var name = that['name'];

	if (name == null) {
		return;
	}

	var updateable = true;
	if (that['updateable'] != null) {
		updateable = that['updateable'];
	}

	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var successMsg = (that['successMsg']) ? that['successMsg'] : getConstText({isArgs : true, m_key : 'msg.common.00013'});
	var failMsg = (that['failMsg']) ? that['failMsg'] : getConstText({
				isArgs : true,
				m_key : 'msg.common.00014'
			});

	var chkUrl = that['chkUrl'];
	if (chkUrl == null) {
		return;
	}

	var multi_check = that['multi_check'];

	var t_comp = null;

	// @@정중훈 20131125 validation 추가 시작
	var maxLength;
	if (null != that['maxLength']) {
		maxLength = that['maxLength'];
	}
	// @@정중훈 20131125 validation 추가 종료

	var textField = createTextFieldComp({
		label : (that['text']) ? that['text'] : "",
		id : (that['id']) ? that['id'] : "",
		name : name,
		value : that['value'] ? that['value'] : "",
		vtype : (that['vtype']) ? that['vtype'] : '',
		width : fieldWidth,
		readOnly : that['readOnly'] ? that['readOnly'] : false,
		notNull : that['notNull']
		// @@정중훈 20131125 validation 추가 시작
		,
		maxLength : maxLength
			// @@정중훈 20131125 validation 추가 종료
		});

	var chekBtn = createBtnComp({
				margin : getConstValue('WITH_BUTTON_MARGIN'),
				label : that['btnText'],
				"id" : that['btnId']
			});
	attachBtnEvent(chekBtn, function() {
		var checkParam;
		var theForm = Ext.getCmp(that['targetForm']);
		if (theForm.getFieldValue(name) == null
				|| theForm.getFieldValue(name) == '') {
			alert(getConstText({
						isArgs : true,
						m_key : 'msg.common.00010'
					}));
			Ext.getCmp(that['id']).focus();
			return;
		}

		if ("Y" == multi_check) {
			checkParam = theForm.getInputData();
		} else {
			checkParam = {};
			checkParam[name] = theForm.getFieldValue(name);
		}

		// @@정중훈 20131125 검증버튼을 눌렀을 경우 먼저 validation 체크를 먼저 한후 검증 합니다.
		// if(theForm.isValid()){
		jq.ajax({
					type : "POST",
					url : chkUrl,
					contentType : "application/json",
					dataType : "json",
					async : false,
					data : getArrayToJson(checkParam),
					success : function(data) {
						if (data.success) {
							if (data.resultString == "0") {
								alert(successMsg);
								theForm.setFieldValue('overLapYN' + name, 'Y');
							} else {
								alert(failMsg);
								theForm.setFieldValue('overLapYN' + name, '');
							}
						} else {
							alert(data.resultMsg);
						}
					}
				})

			// }else{
			// alert(getConstText({ isArgs: true, m_key: 'msg.common.00011' }));
			// }
			// @@정중훈 20131125 검증버튼을 눌렀을 경우 먼저 validation 체크를 먼저 한후 검증 합니다.
		});

	t_comp = {
		xtype : 'fieldcontainer',
		// margin: getConstValue('MARING_ZERO'),
		width : getConstValue('DEFAULT_FIELD_WIDTH'),
		layout : 'hbox',
		items : [textField, chekBtn]
	}
	return t_comp;
}

/**
 * @function
 * @summary 디스플레이 필드 콤포넌트
 * @param {json} compProperty
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createDisplayTextFieldComp(compProperty) {
	var that = convertDomToArray(compProperty);
	var t_comp = Ext.create('Ext.form.field.Display', {
		xtype : 'displayfield',
		labelWidth : that['labelWidth'] ? that['labelWidth'] : getConstValue('LABEL_WIDTH'),
		labelAlign : that['labelAlign'] ? that['labelAlign'] : 'right',
		width : that['width'] ? that['width'] : getConstValue('DEFAULT_FIELD_WIDTH'),
		minWidth : that['width'] ? that['width'] : getConstValue('DEFAULT_FIELD_WIDTH'),
		height : that['height'] ? that['height'] : "",
		labelSeparator : that['labelSeparator'] ? that['labelSeparator'] : '',
		fieldLabel : that['label'] ? that['label'] : "",
		value : that['value'] ? that['value'] : "",
		name : that['name'] ? that['name'] : ""
	});
	return t_comp;
}

/**
 * @function
 * @summary 라벨 콤포넌트
 * @param {json} compProperty
 * @property {int} width 문구 넓이
 * @property {string} text 표시문구
 * @property {string} id 고유식별 ID
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createLabelComp(compProperty) {
	var that = convertDomToArray(compProperty);
	var t_comp = Ext.create("Ext.form.Label", {
		id : that["id"],
		xtype : "label",
		forId : that["forId"] ? that["forId"] : null,
		width : that["width"] ? that["width"] : ( that["text"].length * 10 ),
		text : that["text"],
		margin : that["margin"] ? that["margin"] : "0 0 0 5",
		style : that["style"] ? that["style"] : ""
	});
	return t_comp;
}

/**
 * @function
 * @summary 검색 폼 콤포넌트
 * @param {json} setSearchFormProp
 * @property {json[]} tableProps 폼에 배치될 Item 배열
 * @property {json[]} detailProps 폼의 상세 검색 정보에 배치될 Item 배열 
 * @property {json[]} formBottomBtns 폼에 배치될 버튼 객체 배열
 * @property {string} id 고유식별 ID
 * @property {string} icon Form객체의 타이틀에 표시되는 아이콘
 * @property {string} title Form객체의 타이틀 문구
 * @property {string} buttonAlign 버튼정렬
 * @property {int} columnSize 컬럼사이즈 Item이 표시되는 열수
 * @property {function} enterFunction 엔터키를 눌렀을 때 실행되는 함수    
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createSeachFormComp(setSearchFormProp) {
	var that = {};
	var grid_comp;
	for (var prop in setSearchFormProp) {
		that[prop] = setSearchFormProp[prop];
	}

	var required = '<span style="color:red;font-weight:600" data-qtip="Required">*</span>';

	// 일반검색조건
	var td_items = parseTdItem(that['tableProps']);

	// 상세검색조건
	var td_detailItems = parseTdItem(that['detailProps']);
	if (td_detailItems != null && td_detailItems.length > 0) {
		td_items.push({
			id : 'detailSearch',
			colspan : that['columnSize'],
			xtype : 'fieldset',
			collapsed : true,// 숨김(hide)기본
			padding : getConstValue('MARING_ZERO'),
			fieldDefaults : {
				labelAlign : 'top',
				msgTarget : 'left'
			},
			layout : {
				type : 'table',
				columns : that['columnSize']
			},
			items : td_detailItems
		});
	}

	var enterFunction = ""
	if (that["enterFunction"]) {
		enterFunction = that["enterFunction"];
	} else {
		enterFunction = function(e) {
			if (window["searchBtnEvent"] != null) {
				searchBtnEvent();
			}
		}
	}

	var btnArray = new Array();

	var detailSearchBtn = createBtnComp({visible : true, label : '상세조회 열기'});

	if (that['formBtns'] != null) {
		if (td_detailItems != null && td_detailItems.length > 0) {
			btnArray.push(detailSearchBtn);
			attachBtnEvent(detailSearchBtn, function() {
				if (Ext.getCmp('detailSearch').collapsed) {
					Ext.getCmp('detailSearch').expand();
					detailSearchBtn.setText('상세조회 닫기');
					callResizeLayout();
				} else {
					Ext.getCmp('detailSearch').collapse();
					detailSearchBtn.setText('상세조회 열기');
					callResizeLayout();
				}
			}, '');
		}

		for (var i = 0; i < that['formBtns'].length; i++) {
			btnArray.push(that['formBtns'][i]);
		}
	};

	var new_form = Ext.create('nkia.custom.EditorForm', {
		id : that['id'],
		// Editor Form의 경우 사이즈를 줄경우 길이가 제대로 늘어나지 않는 경우가 발생한다.
		// width: that['width'] ? that['width'] : '100%',
		title : that['title'] ? that['title'] : getConstText({
					isArgs : true,
					m_key : 'res.common.search'
				}),
		itemId : that['itemId'] ? that['itemId'] : "",
		icon : (that['icon'])
				? that['icon']
				: getConstValue('CONTEXT')
						+ '/itg/base/images/ext-js/common/icons/bullet_search.gif',
		buttonAlign : 'center',
		formBtns: that['toolBarComp'],
		margin : getConstValue('MARING_ZERO'),
		items : [{
					id : that['id'] + "fieldset",
					xtype : 'fieldset',
					defaults : {

		}			,
					fieldDefaults : {

		}			,
					layout : {
						type : 'table',
						columns : (that['columnSize'])
								? that['columnSize']
								: 3
					},
					items : td_items
				}],
		listeners : {
			render : function(comp) {
				Ext.create('Ext.util.KeyNav', comp.getEl(), {
							"enter" : enterFunction,
							scope : this
						});
			}
		},
		buttons : btnArray
	});
	return new_form;
}

/**
 * @function
 * @summary 이너 에디트 폼 콤포넌트
 * @param {json} setEditorFormProp
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createInnerEditorFormComp(setEditorFormProp) {
	setEditorFormProp['innerForm'] = true;
	return createEditorFormComp(setEditorFormProp);
}

/**
 * @function
 * @summary 에디트폼 콤포넌트
 * @param {json} setEditorFormProp
 * @property {json[]} tableProps 폼에 배치될 Item 배열
 * @property {json[]} formBottomBtns 폼에 배치될 버튼 객체 배열
 * @property {boolean} editorMode 수정모드 여부
 * @property {string} id 고유식별 ID
 * @property {string} icon Form객체의 타이틀에 표시되는 아이콘
 * @property {string} title Form객체의 타이틀 문구
 * @property {string} height Form객체의 높이
 * @property {boolean} innerForm 패널안에 들어가는 내부 폼 여부
 * @property {boolean} editOnly 수정전용 여부
 * @property {string} buttonAlign 버튼정렬
 * @property {json[]} hiddenFields Hidden필드 배열
 * @property {int} columnSize 컬럼사이즈 Item이 표시되는 열수 
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createEditorFormComp(setEditorFormProp) {
	var that = {};
	var grid_comp;
	for (var prop in setEditorFormProp) {
		that[prop] = setEditorFormProp[prop];
	}

	var required = '<span style="color:red;font-weight:600" data-qtip="Required">*</span>';
	var td_items = parseTdItem(that['tableProps']);

	var btnArray = new Array();
	if (that['formBottomBtns'] != null) {
		for (var i = 0; i < that['formBottomBtns'].length; i++) {
			btnArray.push(that['formBottomBtns'][i]);
		}
	};

	var isEditorMode = true;
	if (!that['editorMode']) {
		isEditorMode = that['editorMode'];
	}
	// 김건범 본사
	var dockedItems = null;
	if (btnArray.length > 0) {
		dockedItems = [{
			xtype : 'toolbar',
			padding : '2 0 2 0',
			dock : 'bottom',
			ui : 'footer',
			items : btnArray
		}]
	}
	var editor_form = Ext.create('nkia.custom.EditorForm', {
		id : that['id'],
		icon : that['icon'] ? that['icon'] : getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/bullet_detail.gif',
		title : that['title'],
		height : that['height'] ? that['height'] : '100%',
		innerForm : (that['innerForm']) ? that['innerForm'] : false,
		editOnly : that['editOnly'],
		formBtns : that['formBtns'],
		buttonAlign : that['buttonAlign'],
		hiddenFields : that['hiddenFields'],
		taskName : that['taskName'],	// 엔진처리를 위해 taskName을 추가
		autoScroll : (typeof that['autoScroll']) ? that['autoScroll'] : false,
		refreshCmp : (that['refreshCmp']) ? that['refreshCmp'] : null,
		items : [{
			id : that['id'] + "fieldset",
			xtype : 'fieldset',
			layout : {
				type : 'table',
				columns : (that['columnSize'])
						? that['columnSize']
						: 2
			},
			// applied to child components
			items : td_items
		}],
		editorMode : isEditorMode,
		buttons : that['alignBottomBtns'], // @@ 고한조 - Bottom 정렬되는 버튼
		dockedItems : dockedItems
	});

	if (that['editOnly']) {
		editor_form.setUpdateMode();
	} else {
		editor_form.hideForm();
	}
	return editor_form;
}

/**
 * @function
 * @summary 테이블 Layout TD Item Parser
 * @param {json} setEditorFormProp
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function parseTdItem(tableProps) {
	var tdItems = null;
	if (tableProps != null && tableProps.length != null) {
		var tdItems_length = tableProps.length + 1;

		tdItems = new Array(tdItems_length);
		for (var i = 0; i < tableProps.length; i++) {
			var td_props = tableProps[i];
			var tdColspanStr = td_props.colspan;
			var tdColspanValue = 0;
			if (tdColspanStr != null && tdColspanStr != "0") {
				tdColspanValue = parseInt(tdColspanStr);
			} else {
				tdColspanValue = 1;
			}
			var fieldWidth = tdColspanValue
					* (getConstValue('DEFAULT_FIELD_WIDTH'));
			fieldWidth = fieldWidth + (25 * (tdColspanValue - 1));
			td_props.item.colspan = tdColspanValue;
			tdItems[i] = td_props.item;
		}
	}
	return tdItems;
}

/**
 * @function
 * @summary 뷰 포트 컴포넌트
 * @param {json} viewportProperty
 * @param {function} viewportHandler
 * @property {string} viewportId 고유식별 ID ( default: 'viewportPanel' )
 * @property {string} borderId Boarde 패널 ID ( default: 'borderPanel' )
 * @property {json} north North 영역
 * @property {Array} items North Items
 * @property {boolean} split 분할 여부( default: true )
 * @property {boolean} collapsed collapsed ( default: false )
 * @property {int} minHeight North 영역 최소 Height
 * @property {int} maxHeight North 영역 최대 Height
 * @property {Array} buttons North 영역 버튼
 * @property {string} buttonAlign North 영역 위치 ( defalut: 'center' )
 * @property {json} west West 영역
 * @property {Array} items West Items
 * @property {int} width width ( default: CONSTRAINT.BORDER_WEST_DEFAULT_WIDTH[200px] )
 * @property {boolean} split split ( default: true )
 * @property {boolean} collapsed collapsed ( default: false )
 * @property {int} minWidth West 영역 최소 Height
 * @property {int} maxWidth West 영역 최대 Height
 * @property {Array} buttons West 영역 버튼
 * @property {string} buttonAlign West 영역 위치 ( defalut: 'center' )
 * @property {json} center Center 영역
 * @property {Array} items Center Items
 * @property {boolean} collapsed collapsed ( default: false )
 * @property {Array} buttons West 영역 버튼
 * @property {string} buttonAlign West 영역 위치 ( defalut: 'center' )
 * @property {json} easet East 영역
 * @property {Array} items East Items
 * @property {int} width Viewport 넓이 ( default: CONSTRAINT.BORDER_WEST_DEFAULT_WIDTH[200px] )
 * @property {boolean} split split ( default: true )
 * @property {boolean} collapsed collapsed ( default: false )
 * @property {int} minWidth East 영역 최소 Height
 * @property {int} maxWidth East 영역 최대 Height
 * @property {Array} buttons East 영역 버튼
 * @property {string} buttonAlign East 영역 위치 ( defalut: 'center' )
 * @property {json} south South 영역
 * @property {Array} items South Items
 * @property {int} minHeight South 영역 최소 Height
 * @property {int} maxHeight South 영역 최대 Height
 * @property {Array} buttons South 영역 버튼
 * @property {string} buttonAlign South 영역 위치 ( defalut: 'center' )
 * @return {object}
 * @author ITG
 * @since 1.0 
 */
function createBorderViewPortComp(viewportProperty, viewportHandler) {
	var that = {};
	for (var prop in viewportProperty) {
		that[prop] = viewportProperty[prop];
	}

	var borderItems = [];

	var northContent, westContent, centerContent, eastContent, southContent = null;
	if (that["north"] && that["north"]["items"]) {
		northContent = {
			xtype : 'panel',
			region : 'north',
			// layout: 'vbox',
			layout : 'fit',
			autoScroll : false,
			items : that["north"]["items"],
			split : (that["north"]["split"]) ? that["north"]["split"] : false,
			collapsed : (that["north"]["collapsed"]) ? that["north"]["collapsed"] : false,
			minHeight : (that["north"]["minHeight"]) ? that["north"]["minHeight"] : 80,
			maxHeight : (that["north"]["maxHeight"]) ? that["north"]["maxHeight"] : 100
			// buttons: ( that["north"]["buttons"] ) ? that["north"]["buttons"]
			// : null,
			// buttonAlign : ( that["north"]["buttonAlign"] ) ?
			// that["north"]["buttonAlign"] : 'center'
		}
		borderItems.push(northContent);
	}

	if (that["west"] && that["west"]["items"]) {
		westContent = {
			xtype : 'panel',
			region : 'west',
			layout : 'fit',
			autoScroll : false,
			width : (that["west"]["width"]) ? that["west"]["width"] : CONSTRAINT.BORDER_WEST_DEFAULT_WIDTH,
			items : that["west"]["items"],
			split : (that["west"]["split"]) ? that["west"]["split"] : true,
			collapsed : (that["west"]["collapsed"]) ? that["west"]["collapsed"] : false,
			minWidth : (that["west"]["minWidth"]) ? that["west"]["minWidth"] : 100,
			maxWidth : (that["west"]["maxWidth"]) ? that["west"]["maxWidth"] : 200,
			buttons : (that["west"]["buttons"]) ? that["west"]["buttons"] : null,
			buttonAlign : (that["west"]["buttonAlign"]) ? that["west"]["buttonAlign"] : 'center'
		}
		borderItems.push(westContent);
	}

	if (that["center"]) {
		centerContent = {
			xtype : 'panel',
			region : 'center',
			autoScroll : false,
			items : that["center"]["items"],
			collapsed : (that["center"]["collapsed"]) ? that["center"]["collapsed"] : false,
			buttons : (that["center"]["buttons"]) ? that["center"]["buttons"] : null,
			buttonAlign : (that["center"]["buttonAlign"]) ? that["center"]["buttonAlign"] : 'center'
		}
		borderItems.push(centerContent);
	}

	if (that["east"] && that["east"]["items"]) {
		eastContent = {
			xtype : 'panel',
			region : 'east',
			layout : 'fit',
			autoScroll : false,
			items : that["east"]["items"],
			width : (that["east"]["width"]) ? that["east"]["width"] : CONSTRAINT.BORDER_EAST_DEFAULT_WIDTH,
			split : (that["east"]["split"]) ? that["east"]["split"] : true,
			collapsed : (that["east"]["collapsed"]) ? that["east"]["collapsed"] : false,
			minWidth : (that["east"]["minWidth"]) ? that["east"]["minWidth"] : null,
			maxWidth : (that["east"]["maxWidth"]) ? that["east"]["maxWidth"] : null,
			buttons : (that["east"]["buttons"]) ? that["east"]["buttons"] : null,
			buttonAlign : (that["east"]["buttonAlign"]) ? that["east"]["buttonAlign"] : 'center'
		}
		borderItems.push(eastContent);
	}

	if (that["south"] && that["south"]["items"]) {
		southContent = {
			xtype : 'panel',
			region : 'south',
			layout : 'vbox',
			autoScroll : false,
			items : that["south"]["items"],
			split : (that["south"]["split"]) ? that["south"]["split"] : true,
			collapsed : (that["south"]["collapsed"]) ? that["south"]["collapsed"] : false,
			minHeight : (that["south"]["minHeight"]) ? that["south"]["minHeight"] : null,
			maxHeight : (that["south"]["maxHeight"]) ? that["south"]["maxHeight"] : null,
			buttons : (that["south"]["buttons"]) ? that["south"]["buttons"] : null,
			buttonAlign : (that["south"]["buttonAlign"]) ? that["south"]["buttonAlign"] : 'center'
		}
		borderItems.push(southContent);
	}

	var borderPanel = Ext.create('Ext.panel.Panel', {
		id : (that["borderId"]) ? that["borderId"] : 'borderPanel',
		width : (that["width"]) ? that["width"] : CONSTRAINT.APP_RESOLUTION_REALSIZE_WIDTH - CONSTRAINT.APP_LEFT_MENU_WIDTH,
		height : CONSTRAINT.APP_RESOLUTION_HEIGHT,
		layout : 'border',
		gapWidth : 0,
		items : borderItems,
		listeners : {
			afterrender : function(borderPanel) {
				var itemPanelCount = borderPanel.items.items.length;
				if (itemPanelCount > 0) {
					for (var i = 0; len = itemPanelCount, i < len; i++) {
						var itemPanel = borderPanel.items.items[i];
						if (itemPanel.split) { // split width 조정
							itemPanel.splitter.setWidth(1);
							itemPanel.splitter.setUI('inside');
						}
					}
				}
			}
		}
	});

	var viewPort = Ext.create('Ext.panel.Panel', {
		id : (that["viewportId"]) ? that["viewportId"] : 'viewportPanel',
		width : (that["width"]) ? that["width"] : CONSTRAINT.APP_RESOLUTION_REALSIZE_WIDTH - CONSTRAINT.APP_LEFT_MENU_WIDTH,
		height : CONSTRAINT.APP_RESOLUTION_HEIGHT,
		border : false,
		items : [borderPanel],
		renderTo : Ext.getBody()
	});

	var targetMaskId = (that["viewportId"]) ? that["viewportId"] : 'viewportPanel';

	var myMask = new Ext.LoadMask(targetMaskId, {
		id : (that["maskId"]) ? that["maskId"] : 'viewportMask',
		msg : getConstText({isArgs : true, m_key : 'msg.mask.common'})
	});

	// viewport handler
	if (viewportHandler) {
		if (viewportHandler.isResize) {
			eval(viewportHandler.resizeFunc)();
		}

		if (viewportHandler.isLoading) {
			if (Ext.get('loading') && Ext.get('loading-mask')) {
				setTimeout(function() {
					Ext.get('loading').remove();
					Ext.get('loading-mask').fadeOut({remove : true});
				}, 350);
			}
		}
	}

	return viewPort;
}

/**
 * @function
 * @summary 뷰 포트 마스크 활성화
 * @param {string} maskId
 * @author ITG
 * @since 1.0 
 */
function setViewPortMaskTrue(maskId) {
	var viewportMask = getViewPortMask(maskId);

	if (viewportMask != null && viewportMask.show != null) {

		if (viewportMask.showCount != null && viewportMask.showCount > 0) {
			viewportMask.showCount++;
		} else {
			viewportMask.show();
			viewportMask.showCount = 1;
		}
	}
}

/**
 * @function
 * @summary 뷰 포트 마스크 비활성화
 * @param {string} maskId
 * @author ITG
 * @since 1.0 
 */
function setViewPortMaskFalse(maskId) {
	var viewportMask = getViewPortMask(maskId);

	if (viewportMask != null && viewportMask.hide != null) {

		viewportMask.showCount--;
		if (viewportMask.showCount == null) {
			viewportMask.hide();
		} else if (viewportMask.showCount == 0) {
			viewportMask.hide();
		}
	}
}

/**
 * @function
 * @summary 뷰 포트 마스크 객체 가져오기
 * @param {string} maskId
 * @returns {object} viewportMask
 * @author ITG
 * @since 1.0 
 */
function getViewPortMask(maskId) {
	var viewportMask = null;

	/*
	 * if( this.parent == this.top ){ if(null == maskId) { viewportMask =
	 * Ext.getCmp("viewportMask"); } else { viewportMask = Ext.getCmp(maskId); }
	 * }else{ viewportMask =
	 * this.top.centerTabPanel.activeTab.getDocument().defaultView.getViewPortMask(maskId); }
	 */

	if (null == maskId) {
		viewportMask = Ext.getCmp("viewportMask");
	} else {
		viewportMask = Ext.getCmp(maskId);
	}
	return viewportMask;
}

/**
 * @function
 * @summary  탭 콤포넌트
 * @param {string} compProperty
 * @returns {object} 
 * @author ITG
 * @since 1.0 
 */
function createTabComponent(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var btnArray = new Array();
	if (that['tools'] != null) {
		btnArray.push({
					xtype : 'tbfill'
				});
		for (var i = 0; i < that['tools'].length; i++) {
			that['tools'][i].margin = getConstValue('TAB_BUTTON_MARGIN');
			btnArray.push(that['tools'][i]);
		}
	};

	var tabHeight = (that['height']) ? that['height'] : '100%';
	var tabCls = (that['cls']) ? that['cls'] : '';
	var fullSize = false;
	if (tabHeight == '100%') {
		fullSize = true;
	}

	var extTab = Ext.create('Ext.tab.Panel', {
		id : that['tab_id'],
		plain : false,
		title : (that['title']) ? that['title'] : null,
		isTab : true,
		autoScroll : (that['autoScroll'] != null)
				? that['autoScroll']
				: true,
		height : (that['height']) ? that['height'] : '100%',
		activeTab : 0,
		defaults : {
			layout : 'fit',
			flex : 1
		},
		cls : tabCls,
		items : that['item'],
		listeners : {
			resize : function(tabpanel, width, height, oldWidth,
					oldHeight, eOpts) {
				if (tabpanel.fullSize) {
				}
			}
		}
	});

	extTab.tabBar.addTool(btnArray);
	return extTab;
}

/**
 * @function
 * @summary  테이블 사이 간격(폭)
 * @param {string} compProperty
 * @returns {object} 
 * @author ITG
 * @since 1.0 
 */
function getTbSpacer(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var tbSpacer = Ext.widget('tbspacer', {
		region : 'center',
		width : (that['width']) ? that['width'] : 50,
		height : (that['height']) ? that['height'] : 10,
		cls : (that['cls']) ? that['cls'] : ''
	});
	return tbSpacer;
}

/**
 * @function
 * @summary  컴포넌트 Height 가져오기
 * @param {obejct} ext_comp
 * @returns {int} result_height
 * @author ITG
 * @since 1.0 
 */
function getComponentHeight(ext_comp) {
	var pagingBar_f = false;
	var result_height = 0;
	if (ext_comp.xtype == "gridpanel") {
		for (var i = 0; i < ext_comp.dockedItems.keys.length; i++) {
			var indexVal = ext_comp.dockedItems.keys[i].indexOf("_pagingBar");
			if (indexVal != -1) {
				pagingBar_f = true;
				break;
			}
		}
		if (pagingBar_f) {
			result_height = result_height + 30 + ext_comp.minHeight;
		} else {
			result_height = result_height + ext_comp.minHeight;
		}
	} else {
		result_height = result_height + 30 + ext_comp.getHeight();
	}
	return result_height;
}

/**
 * @function
 * @summary  컴포넌트 Width 가져오기
 * @param {obejct} ext_comp
 * @returns {int} component width
 * @author ITG
 * @since 1.0 
 */
function getComponentWidth(ext_comp) {
	return ext_comp.getWidth();
}

/**
 * @function
 * @summary  대상 폼 객체 가져오기
 * @param {string} formId
 * @returns {object} form
 * @author ITG
 * @since 1.0 
 */
function getTargetForm(formId) {
	return Ext.getCmp(formId).getForm();
}

/**
 * @function
 * @summary  모든 버튼 객체 숨기기
 * @param {string} ids
 * @author ITG
 * @since 1.0 
 */
function hideBtns(ids) {
	Ext.suspendLayouts();
	for (var i = 0; i < ids.length; i++) {
		Ext.getCmp(ids[i]).hide();
	}
	Ext.resumeLayouts(true);
}

/**
 * @function
 * @summary  모든 버튼 객체 보이기
 * @param {string} ids
 * @author ITG
 * @since 1.0 
 */
function showBtns(ids) {
	Ext.suspendLayouts();
	for (var i = 0; i < ids.length; i++) {
		Ext.getCmp(ids[i]).show();
	}
	Ext.resumeLayouts(true);
}

/**
 * @function
 * @summary 텍스트 필드 값 설정하기
 * @param {object} targetForm
 * @param {string} elemName
 * @param {string} value
 * @author ITG
 * @since 1.0 
 */
function setInputTextValue(targetForm, elemName, value) {
	targetForm.findField(elemName).setValue(value);
}

/**
 * @function
 * @summary 텍스트 필드 값 가져오기
 * @param {object} targetForm
 * @param {string} elemName
 * @param {string} value
 * @author ITG
 * @since 1.0 
 */
function getInputTextValue(targetForm, elemName) {
	return targetForm.findField(elemName).getValue();
}

/**
 * @function
 * @summary 멀티코드 선택 팝업 콤포넌트
 * @param {json} compProperty
 * @author ITG
 * @since 1.0 
 */
function createMultiCodeSelectPopComp(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var multiCodeSelComp = Ext.create('nkia.custom.multiCodeSelector', {
		targetName : that['targetName'],
		targetHiddenName : that['targetHiddenName'],
		notNull : that['notNull'],
		targetLabel : that['targetLabel'],
		leafSelect : that['leafSelect'],
		popUpTitle : that['popUpTitle'],
		grpCodeId : that['grpCodeId'],
		params : that['params'],
		rnode_text : that['rnode_text'],
		width : that['width'],
		labelAlign : that['labelAlign'] ? that['labelAlign'] : getConstValue('DEFAULT_LABEL_ALIGN')
	});
	return multiCodeSelComp;
}

/**
 * @function
 * @summary 세부사업조회 팝업 컴포넌트
 * @param {json} compProperty
 * @author ITG
 * @since 1.0 
 */
function createOutProjectSelectPopComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var labelWidth = getConstValue('LABEL_WIDTH');
	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	var notNull = (that['notNull']) ? that['notNull'] : false;
	var btnId = (that['btnId']) ? that['btnId'] : 'projectPopBtn';
	
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var textField = createTextFieldComp({label :  getConstText({isArgs : true,m_key : 'res.label.itam.supply.00016'})
		, notNull : notNull, name : that['name'], width: ( that["width"] ) ? that["width"] : getConstValue('DEFAULT_FIELD_WIDTH'), readOnly : true});
	var hiddenField_id = createHiddenFieldComp({name : that['hiddenName']});
	var hiddenField_seq = createHiddenFieldComp({name : that['seqName']});
	var textBtn = createPopBtnComp({id : btnId});
	var textInitBtn = createClearBtnComp({
		id : btnId + 'init', 
		handler : function() { 
			hiddenField_id.setValue('');
			hiddenField_seq.setValue('');
			textField.setValue('');
		}
	});
	
	var fieldItems = [textField, textBtn, textInitBtn, hiddenField_id, hiddenField_seq];
	
	textBtn.on('click', function() {
		var project_id = null;

		var setPopUpProp = {
			id : that['id'],
			popUpText : that['popUpText'],
			tagetProjectIdField : hiddenField_id,
			tagetProjectSeqField : hiddenField_seq,
			tagetProjectNameField : textField,
			tagetEtcField : null,
			height : 517,
			width : 650,
			projectId : that['projectId'],
			url : '/itg/itam/project/searchProject.do',
			buttonAlign : 'center',
			bodyStyle : 'background-color: white;',
			params : {
				project_id : project_id
			}
		}
		var projectPopUp = createProjectSelectPop(setPopUpProp);
		projectPopUp.show();
	});

	var t_comp = null;
	t_comp = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});
	
	return t_comp;
}

/**
 * @function
 * @summary 업체조회 팝업 컴포넌트
 * @param {json} compProperty
 * @author ITG
 * @since 1.0 
 */
function createOutVendorSelectPopComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var labelWidth = getConstValue('LABEL_WIDTH');
	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	var notNull = (that['notNull']) ? that['notNull'] : false;
	var btnId = (that['btnId']) ? that['btnId'] : 'vendorSelectBtn';
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var vendorNmField = createTextFieldComp({label :  getConstText({isArgs : true,m_key : 'res.title.system.vendor.search'})
		, notNull : notNull, name : that['name'], width: ( that["width"] ) ? that["width"] : getConstValue('DEFAULT_FIELD_WIDTH'), readOnly : true});
	var vendorIdField = createHiddenFieldComp({name : that['hiddenName']});
	var textBtn = createPopBtnComp({id : btnId});
	var textInitBtn = createClearBtnComp({
		id : btnId + 'init', 
		handler : function() { 
			vendorNmField.setValue('');
			vendorIdField.setValue('');
		}
	});
	
	var fieldItems = [vendorNmField, textBtn, textInitBtn, vendorIdField];
	
	textBtn.on('click', function() {
		var vendor_id = null;
		var setPopUpProp = {
			id : that['id'],
			popUpText : that['popUpText'],
			tagetVendorIdField : vendorIdField,
			tagetVendorNameField : vendorNmField,
			vendorSalesNameField : that['sales_user_nm'],
			vendorSalesTelField : that['sales_user_tel'],
			targetFormId : that['targetFormId'],
			height : 515,
			width : 590,
			url : '/itg/system/vendor/searchVendor.do',
			buttonAlign : 'center',
			bodyStyle : 'background-color: white;',
			params : {
				use_yn : 'Y'
			}
		}
		var vendorPopUp = createVendorSelectPop(setPopUpProp);
		vendorPopUp.show();
	});

	var t_comp = null;
	t_comp = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});
	
	return t_comp;
}

/**
 * @function
 * @summary KEDB조회 팝업 컴포넌트
 * @param {json} compProperty
 * @author ITG
 * @since 1.0 
 */
function createOutKedbSelectPopComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = (that['width']) ? that['width'] : getConstValue('DEFAULT_FIELD_WIDTH') - 38;
	var containerWidth = (that['width']) ? that['width'] : getConstValue('DEFAULT_FIELD_WIDTH') + 55;

	var kedbIdField = createTextFieldComp({
		label : getConstText({ isArgs : true, m_key : 'res.title.kedb.popup' }),
		name : that['name'],
		width : fieldWidth - 40,
		readOnly : true
	});

	var newRegBtn = createBtnComp({
		label : getConstText({isArgs : true, m_key : 'btn.common.append.trim'}),
		margin : getConstValue('WITH_BUTTON_MARGIN'),
		handler : function() {
			var kedb_id = null;
			var setPopUpProp = {
				id : that['id'],
				tagetKedbIdField : kedbIdField,
				popUpText : 'KEDB등록',
				tagetEtcField : null,
				kedbId : that['kedbId'],
				buttonAlign : 'center',
				bodyStyle : 'background-color: white;'
			}
			var kedbRegPopUp = createKedbRegisterPop(setPopUpProp);
			kedbRegPopUp.show();
		}
	});
	var t_comp = null;
	t_comp = {
		xtype : 'fieldcontainer',
		height : (that['height']) ? that['height'] : "",
		width : containerWidth,
		combineErrors : true,
		layout : 'hbox',
		items : [kedbIdField, newRegBtn, createPopBtnComp({
			handler : function() {
				var kedb_id = null;
				var setPopUpProp = {
					id : that['id'],
					tagetKedbIdField : kedbIdField,
					tagetEtcField : null,
					width : 900,
					height : 506,
					kedbId : that['kedbId'],
					buttonAlign : 'center',
					bodyStyle : 'background-color: white;'
				}
				var kedbPopUp = createKedbSelectPop(setPopUpProp);
				kedbPopUp.show();
			}
		}), createClearBtnComp({
			handler : function() {
				kedbIdField.setValue('');
			}
		})]
	}
	return t_comp;
}

/**
 * @function
 * @summary Document Width 가져오기
 * @returns {int} offsetWidth
 * @author ITG
 * @since 1.0 
 */
function getDocumentWidth() {
	var screenWidth = document.body.offsetWidth;
	return screenWidth;
}

/**
 * @function
 * @summary Document Height 가져오기
 * @returns {int} offsetHeight
 * @author ITG
 * @since 1.0 
 */
function getDocumentHeight() {
	var screenHeight = 0;
	var agt = navigator.userAgent;
	if (agt.indexOf("Chrome") != -1) {
		screenHeight = document.documentElement.clientHeight;
	} else if (agt.indexOf("MSIE 8") != -1) {
		screenHeight = window.document.documentElement.offsetHeight;
	} else if (agt.indexOf("MSIE 9") != -1) {
		screenHeight = window.document.documentElement.offsetHeight;
	}
	return screenHeight;
}

/**
 * @function
 * @summary 폼 Height 그리기
 * @param {object} extForm
 * @author ITG
 * @since 1.0 
 */
function getRenderFormHeight(extForm) {
	var formHeight = 0;
	var agt = navigator.userAgent;
	if (agt.indexOf("Chrome") != -1) {
		formHeight = extForm.getHeight() + 15;
	} else if (agt.indexOf("MSIE 8") != -1) {
		if (!extForm.hiddenFlag) {
			var fieldSet = Ext.getCmp(extForm.id + "fieldset");
			if (fieldSet != null) {
				formHeight = Ext.getCmp(extForm.id + "fieldset").getHeight();
				fieldSet.setHeight(formHeight + 25);
			}
		}
		formHeight = formHeight + 50;
	} else if (agt.indexOf("MSIE 9") != -1) {
		if (!extForm.hiddenFlag) {
			var fieldSet = Ext.getCmp(extForm.id + "fieldset");
			if (fieldSet != null) {
				formHeight = Ext.getCmp(extForm.id + "fieldset").getHeight();
				// extForm.setHeight(formHeight + 20);
				fieldSet.setHeight(formHeight + 25);
			}
		}
		formHeight = formHeight + 40;
	}
	return formHeight;
}

/**
 * @function
 * @summary 파일첨부 필드 컴포넌트
 * @param {json} compProperty
 * @property {string} name Field Name(hidden)
 * @property {string} label Field Label
 * @property (string) fileCmpId File Component Id
 * @property {int} width Field Width
 * @property {boolean} notNull Field Null 여부 ( true | false )
 * @property {int} fileHeight File Grid Height
 * @property {int} maxFileCount 첨부 파일 최대 개수 ex)$!{NkiaAppPropMap.MAX_FILE_COUNT}
 * @property {string[]} allowFileExt 허용 파일 확장자 ex) $!{NkiaAppPropMap.File_Allow_Ext_Atype}
 * @property {int} fileAllMaxSize 전체 파일 첨부 최대 사이즈 ex) $!{NkiaAppPropMap.FILE_ALL_MAX_SIZE_DEFAULT}
 * @property {int} fileUnitMaxSize 개별 파일 첨부 최대 사이즈 ex) $!{NkiaAppPropMap.FILE_UNIT_MAX_SIZE_DEFAULT}
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createAtchFileFieldComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;

	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var labelWidth = that['labelWidth'] ? that['labelWidth'] : getConstValue('LABEL_WIDTH'); // @@ Hanjo - labelWidth 속성 추가

	var label = that['label'];
	var name = that['name'];

	var fieldWidth = 780;
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var editorMode = true;
	if (typeof that["editorMode"] != "undefined") {
		editorMode = that["editorMode"];
	}

	// File Hidden Field 생성
	var hiddenAtchFileField = createHiddenFieldComp({
		name : name
	})

	var fileGrid = Ext.create('nkia.custom.DefaultFileupload', {
		id : that['name'] + "_grid", // (*)파일ID
		listFilesHeight : (that['fileHeight']) ? that['fileHeight'] : 200, // 파일Grid Height : Default 200
		saveButton : false, // 저장버튼여부
		maxFileCount : (that['maxFileCount']) ? that['maxFileCount'] : CONSTRAINT.DEFAULT_FILE_MAX_COUNT, // 최대파일 개수
		allowFileExt : (that['allowFileExt']) ? that['allowFileExt'] : CONSTRAINT.DEFAULT_FILE_ALLOW_EXT, // 파일 확장자
		fileUnitMaxSize : (that['fileUnitMaxSize']) ? that['fileUnitMaxSize'] : CONSTRAINT.DEFAULT_FILE_UNIT_MAX_SIZE, // 파일 개별
		fileAllMaxSize : (that['fileAllMaxSize']) ? that['fileAllMaxSize'] : CONSTRAINT.DEFAULT_FILE_ALL_MAX_SIZE, // 파일 전체 Size
		fileUnitMaxSize : (that['fileUnitMaxSize']) ? that['fileUnitMaxSize'] : CONSTRAINT.DEFAULT_FILE_UNIT_MAX_SIZE, // 파일 개별 Size
		downloadFrameId : 'download_' + name + '_frame',
		editorMode : editorMode
	});

	t_comp = {
		id : that['name'] + "_container",
		xtype : 'fieldcontainer',
		fieldLabel : label,
		labelSeparator : '',
		labelAlign : 'right',
		labelWidth : labelWidth, // @@ Hanjo - labelWidth 속성 추가
		width : fieldWidth,
		combineErrors : true,
		layout : 'hbox',
		defaults : {
			width : '100%',
			hideLabel : true
		},
		items : [fileGrid, hiddenAtchFileField]
	}
	return t_comp;
}

/**
 * @function
 * @summary 프로세스 파일첨부 필드 컴포넌트
 * @param {json} compProperty
 * @property {string} name Field Name(hidden)
 * @property {string} label Field Label
 * @property (string) fileCmpId File Component Id
 * @property {int} width Field Width
 * @property {boolean} notNull Field Null 여부 ( true | false )
 * @property {int} fileHeight File Grid Height
 * @property {int} maxFileCount 첨부 파일 최대 개수 ex)$!{NkiaAppPropMap.MAX_FILE_COUNT}
 * @property {string[]} allowFileExt 허용 파일 확장자 ex) $!{NkiaAppPropMap.File_Allow_Ext_Atype}
 * @property {int} fileAllMaxSize 전체 파일 첨부 최대 사이즈 ex) $!{NkiaAppPropMap.FILE_ALL_MAX_SIZE_DEFAULT}
 * @property {int} fileUnitMaxSize 개별 파일 첨부 최대 사이즈 ex) $!{NkiaAppPropMap.FILE_UNIT_MAX_SIZE_DEFAULT}
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createAtchFileGirdCompByProcess(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;
	that['name'] = "atch_file_id";
	var name = that['name'];

	var requiredTxt = "";
	if (that['required']) {
		requiredTxt = getConstValue('NOT_NULL_SYMBOL');
	}

	var t_comp = Ext.create('nkia.custom.ProcessFileupload', {
		id : that['name'] + "_grid", // (*)파일ID
		title : (requiredTxt != "") ? requiredTxt + that['title'] : that['title'],
		originTitle : that['title'],
		layout : 'fit',
		nodeName : (that['nodeName']) ? that['nodeName'] : null,
		editorMode : (that['editorMode']) ? that['editorMode'] : false,
		required : (that['required']) ? that['required'] : false,
		listFilesHeight : (that['fileHeight']) ? that['fileHeight'] : 200, // 파일Grid Height : Default 200
		saveButton : false, // 저장버튼여부
		params : that['params'],
		maxFileCount : (that['maxFileCount']) ? that['maxFileCount'] : CONSTRAINT.DEFAULT_FILE_MAX_COUNT, // 최대파일 개수
		allowFileExt : (that['allowFileExt']) ? that['allowFileExt'] : CONSTRAINT.DEFAULT_FILE_ALLOW_EXT, // 파일 확장자
		fileUnitMaxSize : (that['fileUnitMaxSize']) ? that['fileUnitMaxSize'] : CONSTRAINT.DEFAULT_FILE_UNIT_MAX_SIZE, // 파일 개별
		fileAllMaxSize : (that['fileAllMaxSize']) ? that['fileAllMaxSize'] : CONSTRAINT.DEFAULT_FILE_ALL_MAX_SIZE, // 파일 전체 Size
		downloadFrameId : 'download_' + name + '_frame'
	});

	return t_comp;
}

/**
 * @function
 * @summary 첨부 파일이 있는지 체크
 * @param {string} fileId
 * @param {json} paramMap
 * @param {function} callbackFunc
 * @returns {boolean}
 * @author ITG
 * @since 1.0 
 */
function checkFileSave(fileId, paramMap, callbackFunc) {
	var isFile = false;
	var fileGrid = Ext.getCmp(fileId + "_grid");
	// 김건범 본사
	if (fileGrid && fileGrid.readyState) {
		isFile = fileGrid.fileSaveHandler(fileId, paramMap, callbackFunc);
	}

	return isFile;
}

/**
 * @function
 * @summary 첨부 파일 목록
 * @param {string} fileId
 * @param {string} atchFileId
 * @author ITG
 * @since 1.0 
 */
function getAtchFileList(fileId, atchFileId) {
	var fileGrid = Ext.getCmp(fileId + "_grid");
	if (atchFileId != null && atchFileId != "") {
		fileGrid.getFileList(atchFileId);
	} else {
		fileGrid.initDefaultFileupload();
	}

	// @@20130718 김도원 START
	// 자산원장에서 파일업로드가 탭구조로 되어 있어
	// 패널생성이전에 파일목록을 로드하여 발생하는 문제를 해결
	if (fileGrid.listFilesPanel != null) {
		fileGrid.clearToolbarAndField();
	}
	// @@20130718 김도원 END
}

/**
 * @function
 * @summary 파일업로드 초기화
 * @param {string} fileId
 * @author ITG
 * @since 1.0 
 */
function initDefaultFileupload(fileId) {
	var fileGrid = Ext.getCmp(fileId + "_grid");
	fileGrid.initDefaultFileupload();
	fileGrid.clearToolbarAndField();
}

/**
 * @function
 * @summary 파일 버튼 보이기/숨기기
 * @param {string} fileId
 * @param {boolean} isVisible
 * @author ITG
 * @since 1.0 
 */
function setVisibleFileButton(fileId, isVisible) {
	var fileGrid = Ext.getCmp(fileId + "_grid");
	// @@20130814 김도원 START
	// 파일컴포넌트 버튼 관련 기능 보완
	var fileField = fileGrid.getForm().findField('temp_file_0');
	fileField.setVisible(isVisible);
	// @@20130814 김도원 END
}

/**
 * @function
 * @summary 수평 패널
 * @param {json} compProperty
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createHorizonPanel(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var align = (that["align"]) ? that["align"] : "top";
	var pack = (that["pack"]) ? that["pack"] : "center";
	var height = (that["height"]) ? that["height"] : "100%";
	// 김건범 본사
	var width = (that["width"]) ? that["width"] : "100%";

	var itemArray = new Array();
	var panelItems = that['panelItems'];

	for (var i = 0; panelItems != null && i < panelItems.length; i++) {
		itemArray.push(panelItems[i]);
		if (i < panelItems.length - 1) {
			if (panelItems[i].vline == true || panelItems[i].vline == null) {
				itemArray.push(getTbSpacer({
							width : 1,
							height : height,
							cls : 'x-hbox-vline'
						}));
			}
		}
	}

	var t_comp = Ext.create('Ext.panel.Panel', {
		id : (that['id']) ? that['id'] : null,
		title : (that['title']) ? that['title'] : null,
		border : false,
		// 김건범 본사
		width : width,
		height : height,
		cls : (that['cls']) ? that['cls'] : null,
		buttonAlign : 'center',
		padding : (that['padding']) ? that['padding'] : '',
		margin : (that['margin']) ? that['margin'] : '',
		layout : {
			type : 'hbox',
			align : align,
			pack : pack
		},
		items : itemArray,
		buttons : (that['panelBtns']) ? that['panelBtns'] : null,
		tools : (that['panelTools']) ? that['panelTools'] : null
	});

	return t_comp;
}

/**
 * @function
 * @summary 수직 패널
 * @param {json} compProperty
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createVerticalPanel(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var height = (that["height"]) ? that["height"] : "100%";
	var align = (that["align"]) ? that["align"] : "center";
	var pack = (that["pack"]) ? that["pack"] : "center";

	var t_comp = Ext.create('Ext.panel.Panel', {
		id : (that['id']) ? that['id'] : null,
		title : (that['title']) ? that['title'] : null,
		border : false,
		width : '100%',
		height : height,
		cls : (that['cls']) ? that['cls'] : null,
		padding : (that['padding']) ? that['padding'] : '',
		margin : (that['margin']) ? that['margin'] : '',
		buttonAlign : 'center',
		layout : {
			type : 'vbox',
			pack : pack,
			align : align
		},
		items : (that['panelItems']) ? that['panelItems'] : null,
		buttons : (that['panelBtns']) ? that['panelBtns'] : null,
		tools : (that['panelTools']) ? that['panelTools'] : null
	});

	return t_comp;
}

/**
 * @function
 * @summary 선택된 Grid Row에 대한 중복 체크
 * @param {string} targetGridId
 * @param {Array} primaryKeys
 * @param {json} rowRecord
 * @returns {boolean} isExist
 * @author ITG
 * @since 1.0 
 */
function gridOverlapCheck(targetGridId, primaryKeys, rowRecord) {
	var targetGridStore = getGridStore(targetGridId);
	var isExist = false;

	targetGridStore.each(function(record, index, count) {
		var matchCount = 0;
		for (var j = 0; j < primaryKeys.length; j++) {
			var primaryKey = primaryKeys[j];
			if (rowRecord[primaryKey]) {
				if (rowRecord[primaryKey] == record.raw[primaryKey]) {
					matchCount++;
				}
			}
		}
		// matchCount가 일치할 경우
		if (matchCount == primaryKeys.length) {
			isExist = true;
		}
	});
	return isExist;
}
/**
 * 선택된 Grid Row를 다른 Grid에 Copy
 * 
 * @param {}
 *            sourceGridId : Source Grid Id
 * @param {}
 *            targetGridId : Taget Grid Id
 * @param {}
 *            primaryJson : Primary key를 이용하여 중복데이터 삽입을 방지
 */

/**
 * @function
 * @summary 선택된 Grid Row를 다른 Grid에 Copy
 * @param {string} sourceGridId
 * @param {string} targetGridId
 * @param {Array} primaryJson
 * @author ITG
 * @since 1.0 
 */
function checkGridRowCopyToGrid(sourceGridId, targetGridId, primaryJson) {
	var targetGridStore = getGridStore(targetGridId);

	var sourceSelRows = Ext.getCmp(sourceGridId).view.selModel.getSelection();

	for (var i = 0; i < sourceSelRows.length; i++) {
		var sendData = {};
		var rowRecord = sourceSelRows[i];
		if (!isEmptyJson(rowRecord.raw)) {
			var isExist = false;
			if (typeof primaryJson != "undefined") {
				var primaryKeys = primaryJson["primaryKeys"]; // 중복데이터 체크

				isExist = gridOverlapCheck(targetGridId, primaryKeys,
						rowRecord.raw);

				/*
				 * targetGridStore.each(function(record, index, count){ var
				 * matchCount = 0; for( var j = 0; j < primaryKeys.length; j++ ){
				 * var primaryKey = primaryKeys[j]; if(
				 * rowRecord.raw[primaryKey] ){ if( rowRecord.raw[primaryKey] ==
				 * record.raw[primaryKey] ){ matchCount++; } } } // matchCount가
				 * 일치할 경우 if( matchCount == primaryKeys.length ){ isExist =
				 * true; } });
				 */
			}
			// 중복데이터가 없을 경우 Add
			if (!isExist) {
				for (key in rowRecord.raw) {
					if (key == "RNUM") {
						sendData[key] = targetGridStore.count() + 1;
					} else {
						sendData[key] = rowRecord.raw[key];
					}
				}
				targetGridStore.add(sendData);
			}
		}
	}
}

/**
 * @function
 * @summary Grid Data 모두 Copy
 * @param {string} sourceGridId
 * @param {string} targetGridId
 * @param {boolean} appendable 기존 데이터에 추가
 * @param {string} noDataAlertMsg 카피될 데이터가 없을 경우 Alert Messgae를 보여준다.
 * @param {json} primaryJson
 * @param {Array} primaryJson
 * @author ITG
 * @since 1.0 
 */
function gridAllRowCopyToGrid(sourceGridId, targetGridId, appendable, noDataAlertMsg, primaryJson) {
	var sourceGridStore = getGridStore(sourceGridId);
	storeAllRowCopyToGrid(sourceGridStore, targetGridId, appendable, noDataAlertMsg, primaryJson)
}

/**
 * @function
 * @summary Grid Data 모두 Copy
 * @param {store} sourceStore
 * @param {string} targetGridId
 * @param {boolean} appendable 기존 데이터에 추가
 * @param {string} noDataAlertMsg 카피될 데이터가 없을 경우 Alert Messgae를 보여준다.
 * @param {json} primaryJson
 * @param {Array} primaryJson
 * @author ITG
 * @since 1.0 
 */
function storeAllRowCopyToGrid(sourceStore, targetGridId, appendable, noDataAlertMsg, primaryJson) {
	var sourceGridStore = sourceStore;
	var targetGridStore = getGridStore(targetGridId);
	if (sourceGridStore.count() > 0) {
		if (!appendable) {
			targetGridStore.removeAll();
		}
		var sourceGridStoreSize = sourceGridStore.data.items;

		for (var i = 0; i < sourceGridStoreSize.length; i++) {
			var sendData = {};
			var rowRecord = sourceGridStoreSize[i];
			if (!isEmptyJson(rowRecord.raw)) {
				var isExist = false;
				if (typeof primaryJson != "undefined") {
					var primaryKeys = primaryJson["primaryKeys"]; // 중복데이터 체크

					isExist = gridOverlapCheck(targetGridId, primaryKeys,
							rowRecord.raw);
				}
				// 중복데이터가 없을 경우 Add
				if (!isExist) {
					for (key in rowRecord.raw) {
						if (key == "RNUM") {
							sendData[key] = targetGridStore.count() + 1;
						} else {
							sendData[key] = rowRecord.raw[key];
						}
					}
					var count = targetGridStore.getCount();
					targetGridStore.add(sendData);
				}
			}
		}
	} else {
		if (typeof noDataAlertMsg != "undefined" && noDataAlertMsg != null
				&& noDataAlertMsg != "") {
			alert(noDataAlertMsg);
		}
	}
}

/**
 * @function
 * @summary 선택된 Grid Row삭제
 * @param {string} sourceGridId
 * @author ITG
 * @since 1.0 
 */
function selectedRowDel(sourceGridId) {
	var sourceGrid = getGrid(sourceGridId);
	var sourceGridStore = sourceGrid.getStore();
	var sourceSelRows = sourceGrid.view.selModel.getSelection();
	if (sourceSelRows.length > 0) {
		for (var i = 0; i < sourceSelRows.length; i++) {
			sourceGridStore.remove(sourceSelRows[i])
		}
	}
}

/**
 * @function
 * @summary Tree의 Check된 노드를 Target Grid에 복사
 * @param {string} sourceTreeId
 * @param {string} targetGridId
 * @param {boolean} appendable 기존 데이터에 추가
 * @param {string} noDataAlertMsg 카피될 데이터가 없을 경우 Alert Messgae를 보여준다.
 * @param {json} primaryJson
 * @author ITG
 * @since 1.0 
 */
function treeCheckNodeCopyToGrid(sourceTreeId, targetGridId, appendable, noDataAlertMsg, primaryJson) {
	var sourceTree = getTree(sourceTreeId);
	var sourceTreeStore = getTreeStore(sourceTreeId);
	var targetGridStore = getGridStore(targetGridId);
	var count = targetGridStore.count() + 1;
	var isChecked = false;
	var dupCount = 0;
	sourceTree.getRootNode().cascadeBy(function(n) {
		if (n.raw.id != "root") {
			if (n.get('checked')) {
				var sendData = {};
				var addFlag = true;
				if (!isEmptyJson(n.raw.otherData)) {
					var isExist = false;
					if (typeof primaryJson != "undefined") {
						var primaryKeys = primaryJson["primaryKeys"];
						isExist = gridOverlapCheck(targetGridId, primaryKeys,
								n.raw.otherData);
					}

					if (!isExist) {
						sendData["RNUM"] = count++;
						for (key in n.raw.otherData) {
							sendData[key] = n.raw.otherData[key];
						}
						targetGridStore.add(sendData);
					}
				}
				isChecked = true;
			}
		}
	});
	if (!isChecked && typeof noDataAlertMsg != "undefined"
			&& noDataAlertMsg != null && noDataAlertMsg != "") {
		alert(noDataAlertMsg);
	}
}

/**
 * @function
 * @summary Tree Node를 Grid에 복사
 * @param {string} sourceTreeId
 * @param {string} targetGridId
 * @param {boolean} appendable 기존 데이터에 추가
 * @param {string} noDataAlertMsg 카피될 데이터가 없을 경우 Alert Messgae를 보여준다.
 * @param {json} primaryJson
 * @author ITG
 * @since 1.0 
 */
function selTreeNodeCopyToGridOnlyOne(sourceTreeId, targetGridId, appendable, noDataAlertMsg, primaryJson) {
	var sourceTree = getTree(sourceTreeId);
	var sourceTreeStore = getTreeStore(sourceTreeId);
	var targetGridStore = getGridStore(targetGridId);

	if (targetGridStore.count() > 0) {
		targetGridStore.removeAll();
	}

	var count = 1;
	var sourceSelNode = sourceTree.view.selModel.getSelection();
	if (sourceSelNode.length > 0) {
		for (var i = 0; i < sourceSelNode.length; i++) {
			var n = sourceSelNode[i];
			var sendData = {};
			var addFlag = true;
			sendData["RNUM"] = count++;
			if (!isEmptyJson(n.raw.otherData)) {
				var isExist = false;
				if (typeof primaryJson != "undefined") {
					var primaryKeys = primaryJson["primaryKeys"];
					isExist = gridOverlapCheck(targetGridId, primaryKeys,
							n.raw.otherData);
				}

				if (!isExist) {
					for (key in n.raw.otherData) {
						sendData[key] = n.raw.otherData[key];
					}
					targetGridStore.add(sendData);
				}
			}
		}
	} else {
		alert(noDataAlertMsg);
	}
}

/**
 * @function
 * @summary 타이틀 바 가져오기(?)
 * @param {string} title
 * @param {string} formBtn
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function getTitleBar(title, formBtn) {

	var btnArray = new Array();

	var titleText = {
		xtype : 'header',
		cls : 'titleBack',
		title : '&nbsp;&nbsp;' + title,
		icon : '/itg/base/images/ext-js/common/icons/menu_left02.gif',
		padding : '5 5 5 5',
		// width: '300px',
		frame : false
		// height: 25
	};

	var formBtns = formBtn;
	for (var i = 0; formBtns != null && i < formBtns.length; i++) {
		btnArray.push(formBtns[i]);
	}

	var titlePanel = Ext.create('Ext.panel.Panel', {
				// autoScroll : true,
				width : '100%',
				height : 28,
				// autoHeight: true,
				layout : {
					type : 'hbox',
					pack : 'center',
					align : 'middle'
				},
				items : [{
							xtype : 'tbspacer',
							width : 10
						}, {
							items : titleText
						}, {
							xtype : 'tbspacer',
							flex : 1
						}, {
							items : btnArray
						}]

			})

	return titlePanel;

	// return btnArray;
}

/**
 * @function
 * @summary 조합 필드 컨테이너
 * @param {json} compProperty
 * @param {string} formBtn
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createUnionFieldContainer(compProperty) {

	var that = convertDomToArray(compProperty);
	var items = that["items"];
	var labelTxtTpl = "";
	if (that['notNull']) {
		labelTxtTpl += getConstValue('NOT_NULL_SYMBOL')
	}
	if(that['emsYn']){
		var emsValue = "EMS : " + that['emsValue'];
		if( that['amValue'] == that['emsValue'] ){
			labelTxtTpl += replaceAll(getConstValue('EMS_EQUAL_SYMBOL'), "#{value}", "title=" + that['emsValue']);
		}else{
			labelTxtTpl += replaceAll(getConstValue('EMS_UNEQUAL_SYMBOL'), "#{value}", "title=" + that['emsValue']);
		}
	}
	if(that['dcaYn']){
		var dcaValue = "DCA : " + that['dcaValue'];
		if( that['amValue'] == that['dcaValue'] ){
			labelTxtTpl += replaceAll(getConstValue('DCA_EQUAL_SYMBOL'), "#{value}", "title=" + that['dcaValue']);
		}else{
			labelTxtTpl += replaceAll(getConstValue('DCA_UNEQUAL_SYMBOL'), "#{value}", "title=" + that['dcaValue']);
		}
	}
	var container = {
		xtype : 'nkiafieldcontainer',
		id : that["id"] ? that["id"] : null,
		hideLabel : that['hideLabel'] ? that['hideLabel'] : false,
		fieldLabel : that['label'] ? that['label'] : '',
		labelSeparator : that['labelSeparator'] ? that['labelSeparator'] : '',
		labelAlign : that['labelAlign'] ? that['labelAlign'] : 'right',
		labelWidth : that['labelWidth'] ? that['labelWidth'] : getConstValue('LABEL_WIDTH'),
		combineErrors : that['combineErrors'] ? that['combineErrors'] : true,
		layout : {
			type : that['layout'] ? that['layout'] : 'hbox',
			align : 'middle'
		},
		width : that['width'] ? that['width'] : getConstValue('BORDER_VIEWPORT_MINWIDTH'),
		margin : that['margin'] ? that['margin'] : '0 0 4 0',
		beforeLabelTextTpl : labelTxtTpl,
		items : items,
		fixedLayout : that["fixedLayout"],
		fixedWidth : that["fixedWidth"],
		isNotColspan: that["isNotColspan"],
		getComboBoxField: that["getComboBoxField"],
		getTextField: that["getTextField"]
	}
	return container;
}

/**
 * @function
 * @summary 폼 Edit/Display 모드 설정
 * @param {json} commPropeties
 * @author ITG
 * @since 1.0 
 */
function formEditable(commPropeties) {
	var that = convertDomToArray(commPropeties);
	var panelObj = Ext.getCmp(that['id']);

	var editorFlag = that['editorFlag'];
	var excepArray = that['excepArray'];

	var readFlag = false;
	var visibleFlag = true;

	if (!editorFlag) {
		readFlag = true;
		visibleFlag = false;
	}
	
	var readOnlyStyle = CONSTRAINT.APP_READONLY_STYLE;
	var highlightStyle = "field-readonly-highlight";
	if(readOnlyStyle ){
		switch(readOnlyStyle){
			case "block" :
				readOnlyStyle = "field-readonly-block";
				break;
			case "underline" :
				readOnlyStyle = "field-readonly-underline";
				break;
			case "highlight" :
				readOnlyStyle = "field-readonly-underline";
				break;
		}
	}
	
	var fieldArr = panelObj.query('.field');

	var existFlag = true;
	Ext.suspendLayouts();
	for( var i = 0, len = fieldArr.length; i < len; i++ ){
		var field = fieldArr[i];
		if( field.xtype != "displayfield" ){
			// 원본 Readonly 상태 저장
			if (typeof field.originReadOnly == "undefined") {
				field.originReadOnly = field.readOnly ? field.readOnly : false;
			}
			
			// ReadOnly 설정
			if( excepArray ){
				// 예외처리가 있는 경우
				if (isExistDataForArray(excepArray, field.getName())) {
					field.setReadOnly(!readFlag);
				} else {
					field.setReadOnly(readFlag);
				}
			} else {
				// 예외처리가 없는 경우
				field.setReadOnly(readFlag);
			}
			
			// CSS 변경
			if (field.inputEl && field.inputEl.dom && field.xtype != 'checkboxfield' && field.xtype != 'radiofield') {
				var isApplyCss = false;
				if( excepArray ){       
					if (isExistDataForArray(excepArray, field.getName())) {
						if( !readFlag ){
							addClass(field.inputEl.dom, ( !field.allowBlank && CONSTRAINT.APP_READONLY_STYLE ) ? highlightStyle :  readOnlyStyle);	
						}else{
							removeClass(field.inputEl.dom, ( !field.allowBlank && CONSTRAINT.APP_READONLY_STYLE ) ? highlightStyle :  readOnlyStyle);
						}
						isApplyCss = true;
					}
				}
				if( field.originReadOnly ){
					addClass(field.inputEl.dom, ( !field.allowBlank && CONSTRAINT.APP_READONLY_STYLE ) ? highlightStyle :  readOnlyStyle);
					isApplyCss = true;
				}
				
				if( !isApplyCss ){
					if( editorFlag ){
						removeClass(field.inputEl.dom, ( !field.allowBlank && CONSTRAINT.APP_READONLY_STYLE ) ? highlightStyle :  readOnlyStyle);
					}else{
						addClass(field.inputEl.dom, ( !field.allowBlank && CONSTRAINT.APP_READONLY_STYLE ) ? highlightStyle :  readOnlyStyle);
					}
				}
			}
			
			// Field EditMode 설정
			if ( typeof field.editMode != "undefined" ) {
				field.setEditMode(editorFlag);
			}
			// Validate clear
			field.clearInvalid();
		}
	}

	var buttonArr = panelObj.query('.button');
	// 버튼 Dsiable
	for (var i = 0, len = buttonArr.length; i < len; i++) {
		var button = buttonArr[i];
		if (excepArray) {
			if (isExistDataForArray(excepArray, button.getId())) {
				button.setDisabled(visibleFlag);
			} else {
				if (button.ignore) {
					button.setDisabled(false);
				} else {
					button.setDisabled(!visibleFlag);
				}
			}
		} else {
			button.setDisabled(!visibleFlag);  
		} 
	}
	Ext.resumeLayouts(true);
}

/**
 * @function
 * @summary Flex 패널 
 * @param {json} compProperty
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createFlexPanel(commPropeties) {
	var that = convertDomToArray(commPropeties);
	var comp = Ext.create('nkia.custom.Panel', {
		border : that["border"] ? that["border"] : false,
		items : that["items"]
	})
	return comp;
}

/**
 * @function
 * @summary field array 에 있는 field나 label button 을 숨겨준다.
 * @param {object} form
 * @param {Array} fieldArray
 * @author ITG
 * @since 1.0 
 */
function hideField(form, fieldArray) {

	var field = "";

	Ext.suspendLayouts();

	for (var i = 0; i < fieldArray.length; i++) {
		field = form.getForm().findField(fieldArray[i]);
		if (field) {// field일경우
			field.setVisible(false);
			field.clearInvalid();
			if( !field.allowBlank ){
				field.originalAllowBlank = field.allowBlank; 
			}
			field.allowBlank = true;
		} else {// button 이나 label 등등
			field = Ext.getCmp(fieldArray[i]);
			if (field) {
				if (field.xtype == "nkiafieldcontainer"
						|| field.xtype == "fieldcontainer") {
					var fieldArr = field.query('.field');
					for (var j = 0; j < fieldArr.length; j++) {
						fieldArr[j].setVisible(false);
						// @@2013-07-18 김건범fieldContainer 인경우 vaildation 초기화
						// ID : 김건범-공통-0009
						// START
						fieldArr[j].clearInvalid();
						if( !fieldArr[j].allowBlank ){
							fieldArr[j].originalAllowBlank = fieldArr[j].allowBlank; 
						}
						fieldArr[j].allowBlank = true;
						// END 김건범-공통-0009
					}
					field.setVisible(false);
				} else {
					field.setVisible(false);
				}
			}
		}
	}
	Ext.resumeLayouts(true);
}

/**
 * @function
 * @summary field array 에 있는 field나 label button 을 보여준다.
 * @param {object} form
 * @param {Array} fieldArray
 * @author ITG
 * @since 1.0 
 */
function showField(form, fieldArray) {

	var field = "";
	Ext.suspendLayouts();

	for (var i = 0; i < fieldArray.length; i++) {

		field = form.getForm().findField(fieldArray[i]);
		if (field) {
			field.setVisible(true);
			field.clearInvalid();
			if( typeof field.originalAllowBlank != "undefined" ) {
				field.allowBlank = field.originalAllowBlank;
			}
		} else {
			field = Ext.getCmp(fieldArray[i]);
			if (field) {
				if (field.xtype == "nkiafieldcontainer"
						|| field.xtype == "fieldcontainer") {
					var fieldArr = field.query('.field');
					for (var j = 0; j < fieldArr.length; j++) {
						fieldArr[j].setVisible(true);
						// @@2013-07-18 김건범 fieldContainer 인경우 vaildation 초기화
						// ID : 김건범-공통-0010
						// START
						fieldArr[j].clearInvalid();
						if( typeof fieldArr[j].originalAllowBlank != "undefined" ) {
							fieldArr[j].allowBlank = fieldArr[j].originalAllowBlank;
						}
						// END
					}
					field.setVisible(true);
				} else {
					field.setVisible(true);
				}
			}
		}
	}
	Ext.resumeLayouts(true);
}

/**
 * @function
 * @summary targetArr 에 있는 field 값을 초기화 시켜준다. radio 는 첫번째 check 하고 checkbox 는 모두 check 해제
 * @param {object} targetForm
 * @param {Array} targetArr
 * @author ITG
 * @since 1.0 
 */
function targetInitValue(targetForm, targetArr) {
	var targetField = "";

	for (var i = 0; i < targetArr.length; i++) {
		targetField = targetForm.getForm().findField(targetArr[i]);
		if (targetField) {
			var xtype = targetField.xtype;
			switch (xtype) {
				case "textfield" :
				case "textareafield" :
					targetField.setValue("");
					break;
				case "combobox" :
					if (!targetField.readOnly) {
						var record = targetField.getStore().getAt(0);
						targetField.select(record);
					}
					break;
				case "checkboxgroup" :
					Ext.iterate(targetField.items.items, function(c) {
								c.setValue(false)
							});
					break;
				case "radiogroup" :
					targetField.items.items[0].setValue(true)
					break;
			}
			if (targetField.clearInvalid) {
				targetField.clearInvalid();
			}
		}
	}
}

/**
 * @function
 * @summary 해당 컴포넌트에 해당 값을 세팅한다.
 * @param {object} targetForm
 * @param {string} fieldNm
 * @param {string} value
 * @author ITG
 * @since 1.0 
 */
function fieldSetValue(targetForm, fieldNm, value) {

	var targetField = targetForm.getForm().findField(fieldNm);
	var itemsArray = "";

	if (targetField) {
		var xtype = targetField.xtype;

		switch (xtype) {
			case "textfield" :
			case "textareafield" :
			case "combobox" :
			case "hiddenfield" :
				targetField.setValue(value[0]);
				break;
			case "checkboxgroup" :
			case "radiogroup" :
				itemsArray = targetField.items.items;
				for (var i = 0; i < itemsArray.length; i++) {
					if (isExistDataForArray(value, itemsArray[i].inputValue)) {
						itemsArray[i].setValue(true);
					} else {
						itemsArray[i].setValue(false);
					}
				}
				break;
		}
	}
}

/**
 * 해당 componet 비활성화 id : 김건범-공통-0011
 * 
 * @param compIds :
 *            component id
 * @param flag :
 *            true or false
 */

/**
 * @function
 * @summary 해당 componet 비활성화
 * @param {Array} compIds
 * @param {boolean} flag
 * @author ITG
 * @since 1.0 
 */
function compDisabled(compIds, flag) {
	var comp = "";
	if (compIds.length > 0) {
		for (var i = 0; i < compIds.length; i++) {
			comp = Ext.getCmp(compIds[i]);
			comp.setDisabled(flag);
		}
	}
}

/**
 * @function
 * @summary 해당 componet 읽기속성 부여
 * @param {object} targetForm
 * @param {Array} compNms
 * @param {boolean} flag
 * @author ITG
 * @since 1.0 
 */
function formCompReadOnly(targetForm, compNms, flag) {
	var field = "";
	if (compNms.length > 0) {
		for (var i = 0; i < compNms.length; i++) {
			field = targetForm.getForm().findField(compNms[i]);
			field.setReadOnly(flag);
		}
	}
}

/**
 * @function
 * @summary 디스플레이 필드
 * @param {json} compProperty
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createDisplayField(compProperty) {
	var that = convertDomToArray(compProperty);
	var compnent = {
		xtype : 'displayfield',
		name : that['name'],
		fieldStyle : {
			"overflow" : "auto",
			"height" : that['height']
		},
		hideLabel : that['hideLabel'] ? that['hideLabel'] : false,
		fieldLabel : that['label'] ? that['label'] : '',
		labelSeparator : that['labelSeparator'] ? that['labelSeparator'] : '',
		labelAlign : that['labelAlign'] ? that['labelAlign'] : 'right',
		labelWidth : that['labelWidth']
				? that['labelWidth']
				: getConstValue('LABEL_WIDTH'),
		width : that['width']
				? that['width']
				: getConstValue('BORDER_VIEWPORT_MINWIDTH'),
		padding : that['padding'] ? that['padding'] : '0 0 0 0',
		margin : that['margin'] ? that['margin'] : '0 0 0 0',
		beforeLabelTextTpl : that['notNull']
				? getConstValue('NOT_NULL_SYMBOL')
				: ''
	}
	return compnent;
}

/**
 * @function
 * @summary 메시지
 * @param {string} msg
 * @author ITG
 * @since 1.0 
 */
function showMessage(msg) {
	alert(msg);
	/*
	 * var compProperty = { msg : msg, type : 'INFO' }
	 * showMessageBox(compProperty); compProperty = null;
	 */
}

/**
 * @function
 * @summary 컨펌메시지
 * @param {string} msg
 * @param {function} fn
 * @author ITG
 * @since 1.0 
 */
function showConfirm(msg, fn) {
	var compProperty = {
		msg : msg,
		type : 'CONFIRM',
		fn : fn
	}
	alert(showMessageBox(compProperty));
	compProperty = null;
}

/**
 * @function
 * @summary 메시지 박스
 * @param {json} compProperty
 * @author ITG
 * @since 1.0 
 */
function showMessageBox(compProperty) {

	var that = convertDomToArray(compProperty);
	var buttons = "";
	var title = "";
	var msg = that["msg"];
	var type = that["type"];
	var fn = that["fn"];
	var buttonText = that["buttonText"] ? that["buttonText"] : {};
	var icons = "";

	switch (type) {
		case 'ALERT' :
			title = that["title"] ? that["title"] : "경고";
			buttons = Ext.MessageBox.OK;
			buttonText = {
				ok : '확인'
			};
			icons = Ext.MessageBox.WARNING;
			break;
		case 'INFO' :
			title = that["title"] ? that["title"] : "정보";
			buttons = Ext.MessageBox.OK;
			buttonText = {
				ok : '확인'
			};
			icons = Ext.MessageBox.INFO;
			break;
		case 'ERROR' :
			title = that["title"] ? that["title"] : "오류";
			buttons = Ext.MessageBox.OK;
			buttonText = {
				ok : '확인'
			};
			icons = Ext.MessageBox.ERROR;
			break;
		case 'CONFIRM' :
			title = that["title"] ? that["title"] : "확인";
			buttons = Ext.MessageBox.OKCANCEL;
			buttonText = {
				ok : '확인',
				cancel : '취소'
			};
			icons = Ext.MessageBox.QUESTION;
			break;
		case 'CHOICE' :
			title = that["title"] ? that["title"] : "선택";
			buttons = Ext.MessageBox.YESNOCANCEL;
			buttonText = {
				yes : '예',
				no : '아니오',
				cancel : '취소'
			};
			icons = Ext.MessageBox.QUESTION;
			break;
		// @@정중훈20130909 사용자정의추가 시작
		case 'CUSTOM' :
			title = that["title"] ? that["title"] : "선택";
			buttons = Ext.MessageBox.YESNOCANCEL; // 요넘이 아직 확인이 안됨.
			buttonText = {
				yes : buttonText.text1,
				no : buttonText.text2,
				cancel : buttonText.text3
			};
			icons = Ext.MessageBox.INFO;
			break;
		// @@정중훈20130909 사용자정의추가종료
	}

	var message = Ext.MessageBox.show({
		title : title,
		msg : msg,
		buttons : buttons,
		buttonText : buttonText,
		fn : fn,
		icon : icons
	});

	message.bottomTb.removeCls('x-toolbar-footer');
	message.bottomTb.removeCls('x-toolbar');
}

/**
 * @function
 * @summary 퓨전차트 생성
 * @param {json} fusionChartProp
 * @author ITG
 * @since 1.0 
 */
function createFusionChart(fusionChartProp) {
	var that = {};
	for (var prop in fusionChartProp) {
		that[prop] = fusionChartProp[prop];
	}
	var autoLoad = false;
	var xml;
	if (that["autoLoad"] == null || that["autoLoad"] == true) {
		xml = createFusionChartXml(fusionChartProp);
		autoLoad = true;
	}

	var location = that["location"];
	var chartType = that["chartType"];
	var chart = "";

	var chartDiv = Ext.get(location);
	if (that["width"]) {
		chartDiv.setWidth(that["width"]);
	} else {
		chartDiv.setWidth("100%");
	}
	if (that["height"]) {
		chartDiv.setHeight(that["height"]);
	} else {
		chartDiv.setHeight("100%");
	}

	var cWidth = chartDiv.getWidth();
	var cHeight = chartDiv.getHeight();
	// var cWidth = "100%";
	// var cHeight = "100%";

	switch (chartType) {
		case "barLine" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/MSCombiDY2D.swf",
					location, cWidth, cHeight);
			break;

		case "yBar" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/Bar2D.swf", location,
					cWidth, cHeight);
			break;

		case "multiYbar" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/MSBar2D.swf", location,
					cWidth, cHeight);
			break;

		case "pie" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/Pie2D.swf", location,
					cWidth, cHeight);
			break;

		case "2dBar" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/Column2D.swf", location,
					cWidth, cHeight);
			break;

		case "multi2D" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/MSColumn2D.swf",
					location, cWidth, cHeight);
			break;

		case "stack" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/MSStackedColumn2DLineDY.swf",
					location, cWidth, cHeight);
			break;

		case "angularGauge" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/AngularGauge.swf",
					location, cWidth, cHeight);
			break;

		case "lineGauge" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/HLinearGauge.swf",
					location, cWidth, cHeight);
			break;

		case "StackedColumn2D" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/StackedColumn2D.swf",
					location, cWidth, cHeight);
			break;

		case "multi2DBar" :
			chart = new FusionCharts(
					"/itg/base/js/fusionCharts/images/MSBar2D.swf", location,
					cWidth, cHeight);
			break;
	}

	if (autoLoad) {
		chart.setTransparent(true);
		chart.setDataXML(xml);
		chart.render(location);
	} else {
		chart.setTransparent(true);
	}

	return chart;
}


/**
 * @function
 * @summary 그리드 페이징 없는 경우 셀렉트 + 텍스트 서버사이드 없이 검색 그리드 필터 (select + text)
 * @param {json} compProperty
 * @author 김건범
 * @since 1.0 
 */
function gridFilterFn(compProperty) {

	var paramMap = convertDomToArray(compProperty);
	var gridObj = paramMap["gridObj"];
	var keyObj = paramMap["keyObj"];
	var valObj = paramMap["valObj"];
	var colIds = paramMap["colIds"];
	var sourceData = "";
	gridObj.store.clearFilter();

	if (gridObj && keyObj && valObj) {

		var val = valObj.getValue();
		var key = keyObj.getValue();
		var keyVal = "";

		var cnt = 0;

		if (val != "") {
			if (key == "") {
				for (var i = 0; i < keyObj.store.count(); i++) {
					keyVal = keyObj.store.getAt(i).raw[keyObj.valueField]
					if (keyVal != "") {
						if (cnt == 0) {
							sourceData += "tester.test(rec.get('" + keyVal
									+ "'))";
							cnt++;
						} else {
							sourceData += " || tester.test(rec.get('" + keyVal
									+ "'))";
							cnt++;
						}
					}
				}
			} else {
				sourceData = "tester.test(rec.get('" + key + "'))";
			}
			var tester = new RegExp(Ext.String.escapeRegex(val), "i");
			gridObj.store.filter({
						filterFn : function(rec) {
							return eval(sourceData);
						}
					});
		}
	} else if (gridObj && colIds && valObj) {
		var val = valObj.getValue();

		if (colIds.length > 0) {
			for (var i = 0; i < colIds.length; i++) {
				if (i == 0) {
					sourceData += "tester.test(rec.get('" + colIds[i] + "'))";
				} else {
					sourceData += " || tester.test(rec.get('" + colIds[i]
							+ "'))";
				}
			}

			var tester = new RegExp(Ext.String.escapeRegex(val), "i");
			gridObj.store.filter({
						filterFn : function(rec) {
							return eval(sourceData);
						}
					});
		}
	}
}

/**
 * @function
 * @summary 그리드 죄우로 옮기기
 * @param {string} mainGridId
 * @param {string} targetGridId
 * @author 김건범
 * @since 1.0 
 */
function moveGridRow(mainGridId, targetGridId) {

	var mainGrid = getGrid(mainGridId);
	var targetGrid = getGrid(targetGridId);
	var selRows = gridSelectedRows(mainGridId);

	if (isSelected(mainGridId)) {
		Ext.suspendLayouts();

		for (var i = 0; i < selRows.length; i++) {
			targetGrid.store.add(selRows[i]);
			mainGrid.store.remove(selRows[i]);
		}

		Ext.resumeLayouts(true);
	} else {
		// showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
		alert("선택된 정보가 없습니다.");

		// 김건범 본사
		return;
	}
}

/**
 * @function
 * @summary 그리드 순서 변경 (위로)
 * @param {string} gridId
 * @author 김건범
 * @since 1.0 
 */
function upToTblOrder(gridId) {
	if (isSelected(gridId)) {

		var gridObj = Ext.getCmp(gridId);
		var record = gridSelectedRows(gridId)
		var rowIndex = 0;

		if (gridObj.multiSelect) {
			if (record.length > 1) {
				// showMessageBox({title : "경고",msg : "순서를 바꿀 데이터를 하나만 선택해주세요."
				// ,type : "ALERT"});
				alert("순서를 바꿀 데이터를 하나만 선택해주세요.");
				setGridAllSelection(gridId, false);
				return;
			}
			gridObj.multiSelect = false;
			rowIndex = gridObj.getStore().indexOf(record[0]);
			if (rowIndex != 0) {
				gridObj.store.removeAt(rowIndex);
				gridObj.store.insert(rowIndex - 1, record[0]);
				gridObj.getSelectionModel().select(rowIndex - 1, true)
			}
			gridObj.multiSelect = true;

		} else {
			rowIndex = gridObj.getStore().indexOf(record);
			if (rowIndex != 0) {
				gridObj.store.removeAt(rowIndex);
				gridObj.store.insert(rowIndex - 1, record);
				gridObj.getSelectionModel().select(rowIndex - 1, true)
			}
		}
	} else {
		// showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
		alert("선택된 정보가 없습니다.");
	}
}

/**
 * @function
 * @summary 그리드 순서 변경 (아래로)
 * @param {string} gridId
 * @author 김건범
 * @since 1.0 
 */
function downToTblOrder(gridId) {
	if (isSelected(gridId)) {
		var gridObj = Ext.getCmp(gridId);
		var record = gridSelectedRows(gridId);
		var rowIndex = 0;

		if (gridObj.multiSelect) {
			if (record.length > 1) {
				// showMessageBox({title : "경고",msg : "순서를 바꿀 데이터를 하나만 선택해주세요."
				// ,type : "ALERT"});
				alert("순서를 바꿀 데이터를 하나만 선택해주세요.");
				setGridAllSelection(gridId, false);
				return;
			}
			gridObj.multiSelect = false;
			rowIndex = gridObj.getStore().indexOf(record[0]);
			if (rowIndex != gridObj.store.count() - 1) {
				gridObj.store.removeAt(rowIndex);
				gridObj.store.insert(rowIndex + 1, record[0]);
				gridObj.getSelectionModel().select(rowIndex + 1, true)
			}
			gridObj.multiSelect = true;
		} else {
			rowIndex = gridObj.getStore().indexOf(record);
			if (rowIndex != gridObj.store.count() - 1) {
				gridObj.store.removeAt(rowIndex);
				gridObj.store.insert(rowIndex + 1, record);
				gridObj.getSelectionModel().select(rowIndex + 1, true)
			}
		}
	} else {
		// showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
		alert("선택된 정보가 없습니다.");
	}
}

/**
 * @function
 * @summary 그리드 모두 선택
 * @param {string} gridId
 * @param {boolean} selected
 * @author ITG
 * @since 1.0 
 */
function setGridAllSelection(gridIds, selected) {
	var targetGridObj = Ext.getCmp(gridIds);
	var targetGridStore = targetGridObj.store;
	if (targetGridStore.count() > 0) {
		if (selected) {
			if (targetGridObj.multiSelect) {
				targetGridObj.getSelectionModel().selectAll(true);
			}
		} else {
			targetGridObj.getSelectionModel().deselectAll(true);
		}
	}
}

/**
 * @function
 * @summary 년월 필드 컴포넌트
 * @param {json} compProperty
 * @author ITG
 * @since 1.0 
 */
function createMonthFieldComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var dateValue = that['dateValue'];
	var submitFormat = (that['submitFormat']) ? that['submitFormat'] : 'Y-m'
	var format = (that['format']) ? that['format'] : 'Y-m';
	var notNull = that['notNull'];

	var labelTxtTpl = "";
	var allowBlank = true;
	if (notNull) {
		labelTxtTpl += getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	
	var t_comp = Ext.create('nkia.custom.Month.Picker', {
		xtype : 'customMonthfield',
		editable : false,
		submitFormat : submitFormat,
		id : that['id'],
		name : that['name'] + '_day',
		fullDateName : that['name'],
		fieldLabel : that['label'],
		beforeLabelTextTpl : labelTxtTpl,
		labelWidth : getConstValue('LABEL_WIDTH'),
		format : format,
		value : dateValue ? dateValue.month : '',
		rawValue : dateValue ? dateValue.month : ''
	});
	
	/*
	var t_comp = Ext.widget('form', {
		items : [{
			xtype : 'customMonthfield',
			editable : false,
			submitFormat : submitFormat,
			id : that['id'],
			name : that['name'] + '_day',
			fullDateName : that['name'],
			fieldLabel : that['label'],
			beforeLabelTextTpl : labelTxtTpl,
			labelWidth : getConstValue('LABEL_WIDTH'),
			format : format
			// @@ 고한조 - 기본값이 없는 경우 추가
			,
			value : dateValue ? dateValue.month : '',
			rawValue : dateValue ? dateValue.month : ''
		}]
	});
	*/
	return t_comp;
}

/**
 * @function
 * @summary 월별달력기간조회폼
 * @param {json} compProperty
 * @author 정중훈
 * @since 1.0 
 */
function createSearchMonthPicker(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var monthPicker_comp = null;
	var items = [];
	var margin;
	var padding;

	var startDateValue = that['start_dateValue'];
	var endDateValue = that['end_dateValue'];
	var submitFormat = (that['submitFormat']) ? that['submitFormat'] : 'Y-m';
	var format = (that['format']) ? that['format'] : 'Y-m';

	var label = (that["label"]) ? that["label"] : "";

	// @@정중훈 20140210 익스 구분
	if (agt.indexOf("MSIE") != -1) {
		margin = "2 3 0 0";
	} else {
		margin = "0 3 0 0";
	}

	var startInput = Ext.create('nkia.custom.Month.Picker', {
		submitFormat : submitFormat,
		editable : false,
		name : that['name'] + '_startDate',
		fullDateName : that['name'] + '_startDate_day',
		format : format,
		value : startDateValue.month,
		rawValue : startDateValue.month,
		width : getConstValue('DEFAULT_MONTH_PICKER_WIDTH'),
		margin : margin
	});

	var endInput = Ext.create('nkia.custom.Month.Picker', {
		submitFormat : submitFormat,
		editable : false,
		name : that['name'] + '_endDate',
		fullDateName : that['name'] + '_endDate_day',
		format : format,
		value : endDateValue.month,
		rawValue : endDateValue.month,
		width : getConstValue('DEFAULT_MONTH_PICKER_WIDTH'),
		margin : margin
	});

	var resetBtn = createClearBtnComp({
				margin : margin,
				handler : function() {
					startInput.setValue('');
					endInput.setValue('');
				}
			});
	
	items = [startInput, endInput, resetBtn]

	monthPicker_comp = {
		xtype : 'fieldcontainer',
		id : that['id'] ? that['id'] : "",
		fieldLabel : label,
		labelSeparator : '',
		labelAlign : 'right',
		labelWidth : getConstValue('LABEL_WIDTH'),
		width : 360 + getConstValue('LABEL_WIDTH'),
		combineErrors : true,
		layout : 'hbox',
		defaults : {
			hideLabel : true
		},
		items : items
	}

	return monthPicker_comp;
}

/**
 * @function
 * @summary openClickTreeNode
 * @param {string} cmp_id
 * @param {string} id
 * @author ITG
 * @since 1.0 
 */
function openClickTreeNode(cmp_id, id) {
	var tree = Ext.getCmp(cmp_id);
	var node = tree.store.getNodeById(id);
	tree.expandPath(node.getPath());
	tree.getSelectionModel().select(node, true);
}

/**
 * @function
 * @summary 트리 체크박스 모두 선택
 * @param {node} node
 * @param {boolean} checked
 * @author ITG
 * @since 1.0 
 */
function setTreeCheckAll(node, checked) {
	if (node.get('leaf')) {
		node = node.parentNode;
		var siblingStateEqual = false;
		node.cascadeBy(function(n) {
			if (n != node) {
				if (n.get('checked') == true) {
					siblingStateEqual = true;
				}
			}
		});
		node.set('checked', siblingStateEqual);
	} else {
		node.cascadeBy(function(n) {
			n.set('checked', checked);
		});
	}
}

/**
 * @function
 * @summary 그룹코드 체크박스 컴포넌트
 * @param {json} compProperty
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createGroupCodeCheckComp(compProperty) {
	var check_comp;
	var items = compProperty['items'];

	if (items == "" || items == null || items == undefined) {// 체크박스 item
																// 동적생성
		var codeGrpId = compProperty['code_grp_id'];
		var multiCode = compProperty['multiCode'];
		compProperty['storeData'] = getCodeDataList(codeGrpId, multiCode);
		compProperty['itemName'] = 'CODE_ID';
		compProperty['boxLabel'] = 'CODE_TEXT';
		check_comp = createMgroupCheckBoxFieldComp(compProperty);
	} else {// 체크박스 item 수동생성
		check_comp = createMgroupCheckBoxFieldComp(compProperty);
	}
	return check_comp;
}

/**
 * @function
 * @summary createMgroupCheckBoxFieldComp
 * @param {json} compProperty
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createMgroupCheckBoxFieldComp(compProperty) {

	var that = convertDomToArray(compProperty);

	var fieldWidth = 0;
	var fieldMinWidth = 0;
	var t_comp = null;

	var labelWidth = getConstValue('LABEL_WIDTH');

	// if(that['fieldLabel'] == null){
	// labelWidth = 0;
	// }

	var label = that['label'];
	var name = that['name'];

	var fieldWidth = getConstValue('DEFAULT_CHECK_FILED_WIDTH');
	if (that['width'] != null) {
		fieldWidth = that['fieldWidth'];
	}

	var items;
	var itemsArray = [];

	if (that['items'] != null) {
		items = that['items'];
	} else {
		for (var i = 0; i < that['storeData'].length; i++) {

			var rowData = that['storeData'][i];
			var boxLabel = rowData[that['boxLabel']];
			var inputValue = rowData[that['itemName']];
			var defaultCheck = that["defaultCheck"];
			var allCheck = that["allCheck"] ? that["allCheck"] : false;
			var checked = false;

			if (defaultCheck != null) {
				for (var j = 0; j < defaultCheck.length; j++) {
					if (defaultCheck[j] == inputValue) {
						checked = true;
					}
				}
			}
			if (allCheck) {
				checked = true;
			}

			var checkItem = Ext.create('Ext.form.field.Checkbox', {
						xtype : "checkboxfield",
						boxLabel : boxLabel,
						name : name,
						inputValue : inputValue,
						width : that["fieldWidth"]
								? that["fieldWidth"]
								: getConstValue('DEFAULT_CHECK_FILED_WIDTH'),
						readOnly : that["readOnly"] ? that["readOnly"] : false,
						// allowBlank : that['notNull'] ? false : true,
						checked : checked
					})
			itemsArray.push(checkItem);
		}
		items = itemsArray;
	}

	t_comp = Ext.create('Ext.form.CheckboxGroup', {
				xtype : 'checkboxgroup',
				id : that['id'] ? that['id'] : '',
				name : that['name'] ? that['name'] : '',
				fieldLabel : that['label'] ? that['label'] : '',
				labelSeparator : that['labelSeparator']
						? that['labelSeparator']
						: '',
				readOnly : that['readOnly'] ? that['readOnly'] : false,
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				beforeLabelTextTpl : that['notNull']
						? getConstValue('NOT_NULL_SYMBOL')
						: '',
				labelWidth : that['labelWidth']
						? that['labelWidth']
						: getConstValue('LABEL_WIDTH'),
				labelSeparator : that['labelSeparator']
						? that['labelSeparator']
						: '',
				labelAlign : that['labelAlign']
						? that['labelAlign']
						: getConstValue('DEFAULT_LABEL_ALIGN'),
				beforeLabelTextTpl : that['notNull']
						? getConstValue('NOT_NULL_SYMBOL')
						: '',
				allowBlank : that['notNull'] ? false : true,
				columns : (that['columns']) ? that['columns'] : 3,
				height : (that['height']) ? that['height'] : "",
				items : items
			});

	return t_comp;
}

/**
 * @function
 * @summary 분류체계 팝업 콤포넌트
 * @param {json} compProperty
 * @returns {object}
 * @author ITG
 * @since 1.0 
 */
function createClassPopupComp(compProperty) {
	var t_comp = null;
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}
	var notNull = that['notNull'];
	var requier = '';
	var allowBlank = true;
	if (notNull) {
		requier = getConstValue('NOT_NULL_SYMBOL');
		allowBlank = false;
	}

	var targetName = that["targetName"];
	var targetHiddenName = that["targetHiddenName"];
	var label = that["label"];

	t_comp = createTreeSelectPopComp({
				popId : 'classSelectPop',
				popUpTitle : label,
				notNull : notNull,
				targetName : targetName,
				targetHiddenName : targetHiddenName,
				treeId : 'classSelectTree',
				url : '/itg/system/code/searchMultiTree.do',
				targetLabel : label,
				btnId : 'classSelectBtnId',
				params : {
					multi_id : 'MILITARY_CLASS',
					expandLevel : 2,
					use_yn : 'Y'
				},
				rnode_text : '#springMessage("res.label.board.00005")'
			});

	return t_comp;
}

/**
 * @function
 * @summary selectAfterTree
 * @param {string} cmpId
 * @param {string} delYn
 * @author ITG
 * @since 1.0 
 */
function selectAfterTree(cmpId, delYn) {
	var tree = Ext.getCmp(cmpId);
	var nodeMap = tree.getSelectionModel().getSelection();
	var id = nodeMap[0].raw.id;
	if (delYn == "Y") {
		id = nodeMap[0].raw.up_node_id;
	}
	tree.getStore().load();
	var afterTree = function(treeStore, records, successful, operlation) {
		var node = tree.store.getNodeById(id);
		if (node) {
			tree.expandPath(node.getPath());
			tree.getSelectionModel().select(node, true);
		}
		tree.un("load", afterTree);
	}
	tree.on("load", afterTree);
}

/**
 * @function
 * @summary 부서선택 팝업 호출 컴포넌트
 * @param {json} compProperty
 * @returns {obejct}
 * @author ITG
 * @since 1.0 
 */
function createDeptSelectPopComp(compProperty) {
	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var labelWidth = getConstValue('LABEL_WIDTH');
	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	if (that['width'] != null) {
		fieldWidth = that['width'];
	}

	var userNmField = createTextFieldComp({
		id : that['id'],
		name : that['name'],
		label : (that['label']) ? that['label'] : "",
		notNull : (that['notNull']) ? that['notNull'] : false,
		readOnly : that['readOnly'] ? that['readOnly'] : true
	});

	var t_comp = null;
	t_comp = {
		xtype : 'fieldcontainer',
		height : (that['height']) ? that['height'] : "",
		width : getConstValue('DEFAULT_FIELD_WIDTH'),
		// margin: getConstValue('MARING_ZERO'),
		combineErrors : true,
		layout : 'hbox',
		items : [userNmField, createPopBtnComp({
					id : that['id'] + "_btn",
					handler : function() {

						var nowCenterId = getConstText({
									isArgs : true,
									m_key : 'Globals.CenterCustId'
								});
						var firstCenterId = getConstText({
									isArgs : true,
									m_key : 'Globals.CenterCustId.first'
								});
						var secondCenterId = getConstText({
									isArgs : true,
									m_key : 'Globals.CenterCustId.second'
								});

						var params = {};
						params['cust_id'] = nowCenterId;
						params['customTree'] = "Y";

						var isCenter = true;
						if (Ext.getCmp('userReqForm')) {
							var reqCustId = Ext.getCmp('userReqForm')
									.getFieldValue('cust_id');
							if (reqCustId == "") {
								alert("부대를 먼저 선택해주세요");
								return;
							} else if (reqCustId == firstCenterId
									|| reqCustId == secondCenterId) {
								params['cust_id'] = reqCustId;
								isCenter = true;
							} else {
								params['cust_id'] = "9999999999";
								isCenter = false;
							}
						} else if (Ext.getCmp('userInfoUpdateDetail')) {
							var infoCustId = Ext.getCmp('userInfoUpdateDetail')
									.getFieldValue('cust_id');
							if (infoCustId == firstCenterId
									|| infoCustId == secondCenterId) {
								params['cust_id'] = infoCustId;
								isCenter = true;
							} else {
								params['cust_id'] = "9999999999";
								isCenter = false;
							}
						} else if (Ext.getCmp('setEditorFormProp')) {
							var editorCustId = Ext.getCmp('setEditorFormProp')
									.getFieldValue('cust_id');
							if (editorCustId == "") {
								alert("부대를 먼저 선택해주세요");
								return;
							} else if (editorCustId == firstCenterId
									|| editorCustId == secondCenterId) {
								params['cust_id'] = editorCustId;
								isCenter = true;
							} else {
								params['cust_id'] = "9999999999";
								isCenter = false;
							}
						} else if (Ext.getCmp('userReqInfo')) {
							var editorCustId = Ext.getCmp('userReqInfo')
									.getFieldValue('cust_id');
							if (editorCustId == "") {
								alert("부대를 먼저 선택해주세요");
								return;
							} else if (editorCustId == firstCenterId
									|| editorCustId == secondCenterId) {
								params['cust_id'] = editorCustId;
								isCenter = true;
							} else {
								params['cust_id'] = "9999999999";
								isCenter = false;
							}
						}

						var setPopUpProp = {
							id : that['id'],
							popUpText : '부서 선택',
							tagetUserNameField : userNmField,
							height : 423,
							width : 300,
							url : '/itg/system/dept/searchStaticAllDeptTree.do',
							buttonAlign : 'center',
							bodyStyle : 'background-color: white;',
							isCenter : isCenter,
							params : params
						}
						var popUp = createDeptSelectPop(setPopUpProp);
						popUp.show();
						popUp.changeCenter(isCenter);
					}
				}), createClearBtnComp({
					id : that['id'] + "_clr_btn",
					handler : function() {
						userNmField.setValue('');
					}
				})]

	}
	return t_comp;
}

/**
 * @function
 * @summary 부서선택 팝업 호출 컴포넌트
 * @param {json} compProperty
 * @property {json} params 부서정보 검색 시 사용할 파라메터
 * @property {string} tagetUserNameField 값을 세팅할 필드 객체
 * @property {int} width 팝업 넓이
 * @returns {obejct}
 * @author ITG
 * @since 1.0 
 */
function createDeptSelectPop(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var deptSelectPop = null;
	var isCenter = (that['isCenter']) ? that['isCenter'] : true;

	if (popUp_comp[that['id']] == null) {

		var param = (that['params']) ? that['params'] : {};

		var setTreeProp = {
			id : that['id'] + '_Tree',
			url : that['url'],
			height : 350,
			title : '부서 목록',
			dynamicFlag : false,
			searchable : true,
			searchableTextWidth : 120,
			expandAllBtn : true,
			collapseAllBtn : true,
			// extraToolbar : multiTreeComboPanel,
			params : that['params'], // '0000001'
			rnode_text : '부서 목록',
			expandLevel : 3
		}
		var selectDeptTree = createTreeComponent(setTreeProp);

		attachCustomEvent('beforeitemdblclick', selectDeptTree, function() {
					var treeSelItem = selectDeptTree.selModel.selected.items;
					if (treeSelItem[0].raw.leaf) {
						that['tagetUserNameField'].setValue(treeSelItem[0].raw.text);
						deptSelectPop.close();
					} else {
						alert("최하위 부서를 선택해주시기 바랍니다.");
						return;
					}
				});

		var check_input_yn_a = createSingleCheckBoxFieldComp({
					fieldLabel : '선택',
					id : 'check_input_yn_a',
					name : 'check_input_yn_a',
					checked : false,
					inputValue : 'Y',
					defaultValue : 'N',
					labelWidth : 50,
					width : 90
				});

		var treeContainer = {
			xtype : 'fieldcontainer',
			height : 100,
			width : that['width'],
			// width: getConstValue('DEFAULT_FIELD_WIDTH'),
			// margin: getConstValue('MARING_ZERO'),
			combineErrors : true,
			layout : 'hbox',
			items : [check_input_yn_a, selectDeptTree]
		}

		var check_input_yn = createSingleCheckBoxFieldComp({
					fieldLabel : '직접입력',
					id : 'check_input_yn',
					name : 'check_input_yn',
					checked : false,
					inputValue : 'Y',
					defaultValue : 'N',
					labelWidth : 50,
					width : 90
				});

		var dept_input_box = createTextFieldComp({
			label : '',
			name : 'dept_input_box',
			id : 'dept_input_box',
			maxLength : 100
		});

		var inputContainer = {
			xtype : 'fieldcontainer',
			height : 50,
			width : that['width'] - 13,
			border : 2,
			style : {
				borderColor : 'skyblue',
				borderStyle : 'solid'
			},
			combineErrors : true,
			layout : 'hbox',
			items : [check_input_yn, dept_input_box]
		}

		var vPanelProp = {
			id : 'vPanel',
			pack : "top",
			panelItems : [{
						height : 350,
						width : that['width'],
						items : selectDeptTree
					}, {
						width : that['width'],
						items : inputContainer
					}]
		};
		var vPanel = createVerticalPanel(vPanelProp);

		Ext.getCmp('dept_input_box').setDisabled(true);

		check_input_yn.on('change', function() {
					if (check_input_yn.getValue()) {
						Ext.getCmp(that['id'] + '_Tree').setDisabled(true);
						Ext.getCmp('dept_input_box').setDisabled(false);
					} else {
						Ext.getCmp('dept_input_box').setValue('');
						Ext.getCmp(that['id'] + '_Tree').setDisabled(false);
						Ext.getCmp('dept_input_box').setDisabled(true);
					}
				});

		var acceptBtn = new Ext.button.Button({
			text : '적 용',
			ui : 'correct',
			handler : function() {
				var tagetUserNameField = that['tagetUserNameField'];
				if (check_input_yn.getValue()) {
					var deptInputVal = Ext.getCmp('dept_input_box').getValue();
					if (deptInputVal == "") {
						alert("부서를 입력해주시기 바랍니다.");
						return;
					} else {
						tagetUserNameField.setValue(deptInputVal);
					}
				} else {
					var treeSelItem = Ext.getCmp(that['id'] + '_Tree').selModel.selected.items;
					if (treeSelItem.length < 1) {
						alert("부서를 선택해주시기 바랍니다.");
						return;
					} else {
						if (treeSelItem[0].raw.leaf) {
							tagetUserNameField
									.setValue(treeSelItem[0].raw.text);
						} else {
							alert("최하위 부서를 선택해주시기 바랍니다.");
							return;
						}
					}
				}
				deptSelectPop.close();
			}
		});

		deptSelectPop = Ext.create('nkia.custom.windowPop', {
					id : that['id'] + "_inner_pop",
					title : '부서 선택',
					height : 480,
					width : that['width'],
					autoDestroy : false,
					resizable : false,
					buttonAlign : 'center',
					buttons : [acceptBtn],
					bodyStyle : that['bodyStyle'],
					items : [vPanel],
					listeners : {
						destroy : function(p) {
							popUp_comp[that['id']] = null
						},
						hide : function(p) {
						}
					}
				});

		popUp_comp[that['id']] = deptSelectPop;

	} else {
		deptSelectPop = popUp_comp[that['id']];
	}

	var changeCenter = function(center) {
		if (center) {
			Ext.getCmp('dept_input_box').setValue('');
			Ext.getCmp(that['id'] + '_Tree').setDisabled(false);
			Ext.getCmp('dept_input_box').setDisabled(true);
			check_input_yn.setReadOnly(false);
			check_input_yn.setDisabled(true);
		} else {
			Ext.getCmp(that['id'] + '_Tree').setDisabled(true);
			Ext.getCmp('dept_input_box').setDisabled(false);
			check_input_yn.setValue("Y");
			check_input_yn.setDisabled(false);
			check_input_yn.setReadOnly(true);
		}
	};

	deptSelectPop.changeCenter = changeCenter;

	return deptSelectPop;

}

function checkSystemDate(date, dateType) {

	var checkDt = date;

	var systemDt = createDateTimeStamp('DHM', {
				year : 0,
				month : 0,
				day : 0,
				hour : 0,
				min : 0
			});

	var day = systemDt.day;
	var hour = systemDt.hour;
	var min = systemDt.min;

	var returnValue = true;

	if (dateType == 'DHM') {
		systemDt = day + hour + min;

		if (checkDt > systemDt) {
			returnValue = false;
		}
	} else if (dateType == 'D') {
		systemDt = day;
		if (checkDt > systemDt) {
			returnValue = false;
		}
	}

	return returnValue;
}

/**
 * @function
 * @summary Form-To 날짜 선택 컨테이너
 * @param {json} compProperty
 * @property {string} format 날짜 표기 형식(예)Y-m-d
 * @property {string} dateType 날짜 표시 형태 (DHM : 년월일시분, DH : 년월일시, D:년월일)
 * @property {string} start_dateValue 시작일시 초기값
 * @property {string} end_dateValue 종료일시 초기값
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name
 * @property {boolean} limitNowDate 현재일시 이전으로 시작일을 설정할 수 없도록 함 
 * @property {boolean} notNull 필수 여부
 * @returns {obejct}
 * @author ITG
 * @since 1.0 
 */
function createFromToDateContainer(compProperty) {

	var that = {};
	for (var prop in compProperty) {
		that[prop] = compProperty[prop];
	}

	var container = null;

	var label = that['label'];
	var name = that['name'];
	var format = that['format'];

	var allowBlank = true;
	if (typeof that['notNull'] != "undefined" && that['notNull'] == true) {
		allowBlank = false;
	}

	var disabled = false;
	if (typeof that['disabled'] != "undefined" && that['disabled'] == true) {
		disabled = true
	}

	var nowDate = Ext.Date.format(new Date(), 'Y-m-d');

	var startDate = Ext.create('nkia.custom.Date', {
		label: that['label'],
		margin : that['margin'] ? that['margin'] : CONSTRAINT.DEFAULT_FIELD_GAP,
		name : name + '_startDate',
		dateType : that["dateType"],
		allowBlank : allowBlank,
		dateValue : that['start_dateValue'],
		format : (format) ? format : 'Y-m-d',
		limitNowDate : that['limitNowDate'],
		disabled : disabled,
		containerFit : true
	});
	
	var endDate = Ext.create('nkia.custom.Date', {
		margin : that['margin'] ? that['margin'] : '0 0 0 0',
		allowBlank : allowBlank,
		name : (that['end_dateName']) ? that['end_dateName'] : name + '_endDate',
		dateType : that["dateType"],
		dateValue : that['end_dateValue'],
		format : (format) ? format : 'Y-m-d',
		disabled : disabled,
		containerFit : true
	});
	
	startDate.dateField.onChange = function() {
		var sDate = startDate.getValue();
		var eDate = endDate.getValue();

		if (startDate.limitNowDate) {
			if (startDate.getValue() < nowDate) {
				alert("시작일은 현재일 이전일 수 없습니다.");
				startDate.setValue("");
				return;
			}
		}

		// 시작 날짜를 바꿨는데 종료날짜가 비어 있으면 종료날짜를 -> 바꾼 시작날짜와 같게 세팅
		if (eDate == "" || eDate == null) {
			endDate.setValue(sDate);
			endDate.dateField.clearInvalid();
		}
		// 시작 날짜를 바꿨는데 시작날짜가 종료날짜보다 높은 날짜라면 시작날짜를 종료날짜와 같게 세팅
		else {
			if (sDate > eDate) {
				alert('시작일은 종료일 이후로 선택하실 수 없습니다.');
				startDate.setValue(eDate);
			}
		}

		startDate.dateField.clearInvalid();
	}

	endDate.dateField.onChange = function() {

		var sDate = startDate.getValue();
		var eDate = endDate.getValue();

		// 종료 날짜를 바꿨는데 시작날짜가 비어 있으면 시작날짜를 -> 바꾼 종료날짜와 같게 세팅
		if (sDate == "" || sDate == null) {
			startDate.setValue(eDate);
			startDate.dateField.clearInvalid();
		}
		// 종료 날짜를 바꿨는데 종료날짜가 시작날짜보다 낮은 날짜라면 종료날짜를 시작날짜와 같게 세팅
		else {
			if (sDate > eDate) {
				alert('종료일은 시작일 이전으로 선택하실 수 없습니다.');
				endDate.setValue(sDate);
			}
		}
		endDate.dateField.clearInvalid();
	}
	
	var fieldItems = [startDate, endDate];
	if( typeof that['isReturnItems'] != "undefined" && that['isReturnItems'] ){
		// field Items만 Return 한다. ComboBox + FromToDate Container에서 사용.
		return fieldItems; 
	}
	container = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});

	return container;
}

/**
 * @function
 * @summary Highlight 텍스트 필드 콤포넌트
 * @param {json} compProperty
 * @property {string} value 표시 값
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name 
 * @returns {obejct}
 * @author ITG
 * @since 1.0 
 */
function createHighlightTextFieldComp(compProperty) {
	var that = convertDomToArray(compProperty);
	var compnent = createTextFieldComp({label : that["label"], name : that['name'], value : that['value'], notNull : false, readOnly : true });
	compnent.on("afterrender", function() {
		addClass(compnent.inputEl.dom, "field-highlight");
	});
	return compnent;
}

/**
 * @function
 * @summary 콤보박스 + 텍스트 입력 - 컨테이너
 * @param {json} compProperty
 * @property {string} comboboxName 코드콤보박스 Input Name
 * @property {string} code_grp_id 코드콤보박스의 코드 그룹 ID
 * @property {string} widthFactor 콤보박스의 넓이(0.1 ~ 1.0)
 * @property {string} dateFieldName 날짜 필드의 Input Name
 * @property {string} dateType 날짜 표시 형태 (DHM : 년월일시분, DH : 년월일시, D:년월일)
 * @property {string} label 필드라벨명
 * @property {string} labelText 단위로 사용할 문자열  
 * @property {boolean} isFromToDate 날짜가 범위 지정 일 경우
 * @property {boolean} notNull 필수 여부
 * @returns {obejct}
 * @author ITG
 * @since 1.0 
 */
function createUnionCodeComboBoxDateContainer(compProperty) {
	var that = convertDomToArray(compProperty);
	
	var fieldItems = [];
	
	// ComboBox Field
	var comboBoxField = createCodeComboBoxComp({label: that['label'], name: that['comboboxName'], code_grp_id: that['code_grp_id'],padding : CONSTRAINT.DEFAULT_FIELD_GAP, attachAll:true, widthFactor: that['widthFactor'], notNull:that['notNull']});
	fieldItems.push(comboBoxField);
	
	var dateField;
	
	if( that['isFromToDate'] ){
		// margin을 0 0 0 0로 셋팅해야지 Gap이 맞게 나온다.
		dateField = createFromToDateContainer({name: that['dateFieldName'], dateType: that['dateType'], format: 'Y-m-d', margin: '0 0 0 0', isReturnItems: true});
		for( var i = 0; i < dateField.length; i++ ){
			fieldItems.push(dateField[i]);
		}
	}else{
		dateField = createDateFieldComp({name: that['dateFieldName'], dateType: that['dateType']});
		fieldItems.push(dateField);
	}

	var container = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});
	return container;
}

/**
 * @function
 * @summary 텍스트 검증 컨테이너(ID 중복 체크 등)
 * @param {json} compProperty
 * @property {string} successMsg 검증 성공 시 표시할 메세지
 * @property {string} failMsg 검증 실패 시 표시할 메세지
 * @property {string} checkUrl 검증 처리 시에 연결할 URL
 * @property {string} multi_check 검증 시 여러 값들이 필요할 경우에 Y로 설정(Y / N (default))
 * @property {string} targetForm multi_check 체크가 Y 일 경우 데이터를 수집할 대상 form의 ID
 * @property {string} btnLabel 검증 버튼의 문구
 * @property {string} btnId 검증 버튼의 ID 
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name
 * @property {string} id 객체고유ID
 * @property {string} vtype 입력값의 데이터 타입
 * @property {boolean} notNull 필수 여부
 * @property {boolean} readOnly 읽기전용 여부
 * @property {int} maxLength 최대길이 
 * @returns {obejct}
 * @author ITG
 * @since 1.0 
 */
function createUnionCheckTextContainer(compProperty) {
	var that = convertDomToArray(compProperty);

	var successMsg = (that['successMsg']) ? that['successMsg'] : getConstText({ isArgs : true, m_key : 'msg.common.00013' });
	var failMsg = (that['failMsg']) ? that['failMsg'] : getConstText({ isArgs : true, m_key : 'msg.common.00014' });

	var checkUrl = that['checkUrl'];
	if( checkUrl == null ){
		return;
	}

	var multi_check = that['multi_check'];

	var textField = createTextFieldComp({label : that['label'], id : that['id'], name : that['name'], vtype : that['vtype'], readOnly : that['readOnly'], notNull : that['notNull'], maxLength : that['maxLength'], editMode:that['editMode'] });

	var checkBtn = createBtnComp({id : that['btnId'], label : that['btnLabel'], width: 50, margin : getConstValue('WITH_BUTTON_MARGIN')});
	
	attachBtnEvent(checkBtn, function() {
		var checkParam;
		var theForm = Ext.getCmp(that['targetForm']);
		if (theForm.getFieldValue(that['name']) == null || theForm.getFieldValue(that['name']) == '') {
			alert(getConstText({ isArgs : true, m_key : 'msg.common.00010'}));
			Ext.getCmp(that['id']).focus();
			return;
		}

		if ("Y" == multi_check) {
			checkParam = theForm.getInputData();
		} else {
			checkParam = {};
			checkParam[that['name']] = theForm.getFieldValue(that['name']);
		}

		jq.ajax({
			type : "POST",
			url : checkUrl,
			contentType : "application/json",
			dataType : "json",
			async : false,
			data : getArrayToJson(checkParam),
			success : function(data) {
				if (data.success) {
					if (data.resultString == "0") {
						alert(successMsg);
						theForm.setFieldValue('overLapYN' + that['name'], 'Y');
					} else {
						alert(failMsg);
						theForm.setFieldValue('overLapYN' + that['name'], '');
					}
				} else {
					alert(data.resultMsg);
				}
			}
		})
	});
	var fieldItems = [textField, checkBtn];
	var container = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});
	return container;
}

/**
 * @function
 * @summary 텍스트 + 라벨 컨테이너(텍스트필드 뒤에 단위 등이 나올때 사용함)
 * @param {json} compProperty
 * @property {string} label 필드라벨명
 * @property {string} name 필드의 Input Name
 * @property {string} id 객체고유ID
 * @property {string} vtype 입력값의 데이터 타입
 * @property {boolean} notNull 필수 여부
 * @property {boolean} readOnly 읽기전용 여부
 * @property {int} maxLength 최대길이 
 * @property {string} labelText 단위로 사용할 문자열
 * @returns {obejct}
 * @author ITG
 * @since 1.0 
 */
function createUnionTextLabelContainer(compProperty) {
	var that = convertDomToArray(compProperty);
	// TextField	
	var textField = createTextFieldComp({label : that['label'], name : that['name'], vtype : that['vtype'], readOnly : that['readOnly'], notNull : that['notNull'], maxLength : that['maxLength'] });
	// Label
	var label = createLabelComp({text: that['labelText']});
	
	var fieldItems = [textField, label];
	
	var container = createUnionFieldContainer({items:fieldItems, isNotColspan: that["isNotColspan"]});
	return container;
}

/**
 * @function
 * @summary 사용자 필드 컴포넌트 - 프로세스에서 사용중
 * @param {json} setProp
 * @author ITG
 * @since 1.0 
 */
function createUserFieldComp(setProp) {
	var that = {};
	for (var prop in setProp) {
		that[prop] = setProp[prop];
	}

	var userComp = Ext.create('nkia.custom.RequestUserField', {
		label : that['label'],
		userNm : that['userNm'],
		userId : that['userId'],
		userDetail : that['userDetail'],
		userSel : that['userSel'],
		btnAction : that['btnAction'],
		labelAlign : that['labelAlign'] ? that['labelAlign'] : getConstValue('DEFAULT_LABEL_ALIGN'),
		width : that['width']
	});

	return userComp;
}

/**
 * Component에 마스크
 * @param targetMaskId
 * @param isMask
 */
function setComponentMask(targetMaskId, isMask){
	if( Ext.getCmp(targetMaskId) ){
		if( isMask ){
			Ext.getCmp(targetMaskId).getEl().mask(getConstText({ isArgs: true, m_key: 'msg.mask.common' }), 'x-mask-loading');
		}else{
			Ext.getCmp(targetMaskId).getEl().unmask();
		}
	}
}