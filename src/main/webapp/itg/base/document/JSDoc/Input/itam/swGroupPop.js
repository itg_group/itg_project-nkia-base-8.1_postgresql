// 2014.02.06 정정윤 추가
// SW그룹관리 SW마스터 테이블에서 오브젝트 아이디 선택할 수 있는 팝업

function openSelectObjectIdPop() {
	var setPopUpProp = {
			id: 'selObjectIdPop',
			popUpText: "SW 그룹관리",
			title: "SW 그룹관리",
			prefix: 'grid.itam.sam.swGroupMngPop',
			height: 500,
			width: 650,
			url: '/itg/itam/sam/selectSwGroupObjIdPop.do',
			buttonAlign: 'center',
			bodyStyle: 'background-color: white;'
	}
	var popUp = createSelectObjectIdPop(setPopUpProp);
	popUp.show();
}

function createSelectObjectIdPop(setPopUpProp){	
	
	var that 		= convertDomToArray(setPopUpProp);
	var setObjIdPop = null;
	
	// 조회 그리드
	var selObjectIdPopGridProp = {	
			id: 'selObjectIdPopGrid',
			title : "수집 소프트웨어 목록",
			gridHeight: 300,
			resource_prefix: that['prefix'],				
			url: that['url'],
			params: ( that['params'] ) ? that['params'] : null,
			pagingBar : true,
			pageSize : 10,
			autoLoad: true
	};
	var selObjectIdPopGrid = createGridComp(selObjectIdPopGridProp);
	
	// 검색 버튼
	var objIdSearchBtn = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
		ui:'correct',
		handler: function() {
			var searchForm = Ext.getCmp('objIdPopSearchForm');
			var paramMap = searchForm.getInputData();
			Ext.getCmp('selObjectIdPopGrid').searchList(paramMap);	
		}
	});
	
	// 초기화 버튼
	var objIdSearchInitBtn = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
		handler: function() {
			Ext.getCmp('search_value').setValue("");
		}
	});
	
	// 검색 폼
	var setObjIdPopSearchFormProp = {
		id: 'objIdPopSearchForm',
		formBtns: [objIdSearchBtn, objIdSearchInitBtn],
		columnSize: 3,
		tableProps : [{
						colspan:1, tdHeight: 30, item: createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_SWGROUP_OBJ', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})
					},{
						colspan:1, tdHeight: 30, item: createTextFieldComp({name:'search_value', id:'search_value', padding: getConstValue('WITH_FIELD_PADDING')})
					}]
	}
	var searchObjForm = createSeachFormComp(setObjIdPopSearchFormProp);

	// 등록버튼 생성
	var acceptBtn = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
		ui:'correct',
		scale:'medium',
		handler: function() {
			var gridStore = Ext.getCmp('selObjectIdPopGrid').selModel.selected.items;
			if(gridStore < 1){
				alert("값을 선택해주세요.");
			}else{
				Ext.getCmp('sw_master_object_id').setValue(gridStore[0].data.OBJECT_ID);
				Ext.getCmp('object_name').setValue(gridStore[0].data.OBJECT_NAME);
				
				setParentFormComp(gridStore[0].data.OBJECT_ID, setObjIdPop);
			}
		}
	});
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		scale:'medium',
		handler: function() {
			setObjIdPop.close();
		}
	});
	
	setObjIdPop = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		title: that['title'],  
		height: that['height'], 
		width: that['width'],  
		autoDestroy: false,
		resizable:false,
		buttonAlign: that['buttonAlign'],
		buttons: [acceptBtn, closeButton],
		bodyStyle: that['bodyStyle'],
		items: [searchObjForm, selObjectIdPopGrid],
	});
	
	attachCustomEvent('itemdblclick', selObjectIdPopGrid, function() {
		var gridStore = Ext.getCmp('selObjectIdPopGrid').selModel.selected.items;
		Ext.getCmp('sw_master_object_id').setValue(gridStore[0].data.OBJECT_ID);
		Ext.getCmp('object_name').setValue(gridStore[0].data.OBJECT_NAME);
		
		setParentFormComp(gridStore[0].data.OBJECT_ID, setObjIdPop);
	});
	
	return setObjIdPop;

}


function searchBtnEvent(){
	var searchForm = Ext.getCmp('objIdPopSearchForm');
	var paramMap = searchForm.getInputData();
	Ext.getCmp('selObjectIdPopGrid').searchList(paramMap);
}


// 벨리데이션
function setParentFormComp(id, that) {
	
	var objMap = {};
	objMap['object_id'] = id;
	
	jq.ajax({ type:"POST"
		, url: "/itg/itam/sam/selectMappingObjectIdSwGroup.do"
		, contentType: "application/json"
		, dataType: "json"
		, data : getArrayToJson(objMap)
		, success:function(data){
			if( data.success ){
				if(data.resultMap.isExistSwGroup) {
					Ext.getCmp('mngt_flag_combo').setValue("INPUT");
					Ext.getCmp('mngt_flag_combo').setDisabled(true);
				} else {
					Ext.getCmp('mngt_flag_combo').setDisabled(false);
				}
				that.close();
			}else{
				alert(data.resultMsg);
			}
        }
	});
	
}



// 옵션ID 선택용 콤보박스
function createSwOptionComboBox(codeType) {
	
	var param = {};
	param['code_type'] = codeType;
	
	var swOption_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/itam/sam/searchSwGroupOptionId.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var swOptionProp_val = {label: '옵션선택', id: 'option_id', name: 'option_id', width: 300, attachAll:true};
		swOptionProp_val['proxy'] = swOption_proxy;
		swOptionProp_val['valueField'] = 'OPTION_ID';
	    swOptionProp_val['displayField'] = 'OPTION_NM';
    
   var swOptComboBox =  createComboBoxComp(swOptionProp_val);
   
   return swOptComboBox;
}
