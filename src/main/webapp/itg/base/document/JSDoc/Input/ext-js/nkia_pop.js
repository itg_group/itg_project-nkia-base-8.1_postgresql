/**
 * 
 * @file nkia_pop
 * @description NKIA Popup 생성
 * @author ITG
 * @copyright NKIA 2013
 */


var popUp_comp = {};
var popUp_props = {};

/**
 * @function
 * @summary 팝업 프로퍼티 가져오기
 * @param {string} id 팝업ID
 * @return {json} popup properties
 */
function getPopupProps(id) {
	return popUp_props[id];
}

/**
 * @function
 * @summary 사용자선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createUserSelectPop(setPopUpProp) {
	var storeParam = {};
	var that = {};
	var userSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {

		var comboProps = {
			// id: that['id']+"_combo",
			labelWidth : getConstValue('POP_LABEL_WIDTH'),
			displayField : "searchNm",
			valueField : "searchCol",
			padding : getConstValue('WITH_FIELD_PADDING'),
			label : getConstText({
				isArgs : true,
				m_key : 'res.common.search'
			}),
			name : "searchCondition",
			storeData : [ {
				searchCol : 'USER_NM',
				searchNm : getConstText({
					isArgs : true,
					m_key : 'res.label.system.00015'
				})
			}, {
				searchCol : 'USER_ID',
				searchNm : getConstText({
					isArgs : true,
					m_key : 'res.label.system.00014'
				})
			} ],
			widthFactor : 0.51
		};

		var searchConditionComp = createComboBoxComp(comboProps);
		searchConditionComp.select('USER_NM');
		
		var searchText = createTextFieldComp({
			hideLabel : true,
			name : 'user_nm',
			padding : getConstValue('WITH_FIELD_PADDING'),
			widthFactor : 0.5,
			label : getConstText({
				isArgs : true,
				m_key : 'res.label.system.00015'
			})
		})
		
		var searchContainer = createUnionFieldContainer({items:[searchConditionComp, searchText], fixedLayout: true, fixedWidth: 450});

		var userSearchBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			ui:'correct',
			margin : getConstValue('WITH_BUTTON_MARGIN')
		});
		
		var userSearchInitBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.reset'
			}),
			margin : getConstValue('WITH_BUTTON_MARGIN')
		});
		
		//@@고은규 2013.09.04 익스 구분 
		var agt = navigator.userAgent;
		if(agt.indexOf("MS") != -1){
			userSearchBtn.margin = '0 0 5 5';
		}
		
		// 사용자 조회 폼
		var setSearchUserFormProp = {
			id : that['id'] + 'searchUserForm',
			title : getConstText({
				isArgs : true,
				m_key : 'res.common.search'
			}),
			target : that['id'] + '_grid',
			columnSize : 1,
			enterFunction : clickUserSearchBtn,
			formBtns : [userSearchBtn, userSearchInitBtn],
			tableProps : [ {
				colspan : 1,
				tdHeight : 30,
				item : searchContainer
			} ]
		}
		var userSearchForm = createSeachFormComp(setSearchUserFormProp);

		var setTreeProp = {
			id : that['id'] + 'Tree',
			url : '/itg/system/dept/searchStaticAllDeptTree.do',
			title : getConstText({
				isArgs : true,
				m_key : 'res.00034'
			}),
			dynamicFlag : false,
			searchable : true,
			height : 363,
			expandAllBtn : false,
			collapseAllBtn : false,
			params : that['params'],
			rnode_text : getConstText({
				isArgs : true,
				m_key : 'res.00034'
			}),
			expandLevel : 3
		}
		var deptTree = createTreeComponent(setTreeProp);
		// 높이 길이 체크 무시
		deptTree['syncSkip'] = true;
		attachCustomEvent('select', deptTree, treeNodeClick);

		var setGridProp = {
			id : that['id'] + '_grid',
			title : getConstText({
				isArgs : true,
				m_key : 'res.00012'
			}),
			gridHeight : 265,
			resource_prefix : (that['resource_prefix']) ? that['resource_prefix'] : 'grid.system.user.pop',
			url : that['url'],
			params : that['params'],
			pagingBar : true,
			pageSize : 10,
			autoLoad : true
		};
		var userGrid = createGridComp(setGridProp);

		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel', {
			region : 'west',
			height : '100%',
			autoScroll : true,
			items : [ deptTree ]
		});

		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel', {
			border : false,
			region : 'center',
			flex : 1,
			items : [ userSearchForm, userGrid ]
		});

		var complexProp = {
			height : '100%',
			panelItems : [ {
				flex : 1.1,
				items : left_panel
			}, {
				flex : 3.0,
				items : right_panel
			} ]
		};
		var complexPanel = createHorizonPanel(complexProp);

		var tagetUserIdField = that['tagetUserIdField'];
		var tagetUserNameField = that['tagetUserNameField'];
		var tagetEtcField = that['tagetEtcField'];

		var acceptBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
			, scale: 'medium'
			, ui : 'correct'
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale: 'medium',
			handler: function() {
				userSelectPop.close();
			}
		});
		
		/** * Private Function Area Start ******************** */

		function treeNodeClick(tree, record, index, eOpts) {
			var cust_id = record.raw.node_id;

			var grid_store = getGrid(that['id'] + '_grid').getStore();
			var param = {};
			if (that['params'] != null) {
				for ( var key in that['params']) {
					param[key] = that['params'][key];
				}
			}
			
			if( getConstValue("CENTER_CUST_ID") == record.raw.up_node_id ){
				param["dept_nm"] = record.raw.text;
			}else{
				param["cust_id"] = cust_id;
			}
			grid_store.proxy.jsonData = param;
			//grid_store.reload();
			grid_store.loadPage(1);
		}

		function clickUserSearchBtn() {
			var conditionVal = searchConditionComp.getValue();
			var textVal = searchText.getValue();
			var userGridStore = userGrid.getStore();
			var param = {};
			if (that['params'] != null) {
				for ( var key in that['params']) {
					param[key] = that['params'][key];
				}
			}
			param["search_type"] = conditionVal;
			param["search_value"] = textVal;
			userGrid.searchList(param);
			//userGridStore.proxy.jsonData = param;
			//userGridStore.load();
			//grid_store.loadPage(1);
		}
		
		function clickUserSearchInitBtn() {
			searchConditionComp.setValue('USER_NM');
			searchText.setValue('');
		}

		function dbSelectEvent() {
			var selectedRecords = userGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00031'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var user_id = nullToSpace(selectedRecords[0].raw.USER_ID);
					var user_nm = nullToSpace(selectedRecords[0].raw.USER_NM);
					var absent_yn = nullToSpace(selectedRecords[0].raw.ABSENT_YN);
					if(absent_yn != null && absent_yn == "Y") {
						user_id = nullToSpace(selectedRecords[0].raw.ABSENT_ID);
						user_nm = nullToSpace(selectedRecords[0].raw.ABSENT_NM);
					}
					var cust_nm = nullToSpace(selectedRecords[0].raw.CUST_NM);
					var detailInfo = nullToSpace(selectedRecords[0].raw.CUST_NM)
							+ "(tel:"
							+ nullToSpace(selectedRecords[0].raw.TEL_NO)
							+ "/email:"
							+ nullToSpace(selectedRecords[0].raw.EMAIL) + ")";
					tagetUserIdField.setValue(user_id);
					tagetUserNameField.setValue(user_nm);
					if (tagetEtcField != null) {
						tagetEtcField.setValue(detailInfo);
					}
					if (that['targetFormId'] != null) {
						cust_nm = Ext.getCmp(that['targetFormId']).setFieldValue(that['custNm'], cust_nm);
					}
					userSelectPop.destroy();
				}
			}
		}

		/** ********************* Private Function Area End ** */

		attachBtnEvent(acceptBtn, dbSelectEvent);
		attachBtnEvent(userSearchBtn, clickUserSearchBtn);
		attachBtnEvent(userSearchInitBtn, clickUserSearchInitBtn);
		attachCustomEvent('itemdblclick', userGrid, dbSelectEvent);

		userSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'] + '_Pop',
			title : that['popUpText'],
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeButton ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				},
				cellclick : function(grid, td, cellIndex, record, tr, rowIndex,
						e, eOpts) {
				}
			}
		});

		popUp_comp[that['id']] = userSelectPop;

	} else {
		userSelectPop = popUp_comp[that['id']];
	}

	return userSelectPop;
}

/**
 * @function
 * @summary 사용자 다중 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createUserMultiSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var userSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {

		var searchFormId = 'searchUserForm' + that['popupId']; // searchFormId
		var userSearchForm = getAssetSearchForm(searchFormId,
				'searchMultiUser', 'searchMultiUser', 2, goSearch);

		var setGridProp = {
			id : that['id'] + '_grid',
			title : '사용자 목록',
			gridHeight : 200,
			resource_prefix : 'grid.popup.user',
			url : getConstValue('CONTEXT') + that['url'],
			enterFunction : goSearch,
			params : that['params'],
			selModel : true,
			multiSelect : true,
			pagingBar : true,
			pageSize : 5
		};
		var userGrid = createGridComp(setGridProp);

		var setUserGridProps = getGridByMemoryVariabilityProps(that['id']
				+ '_setGrid', 'grid.popup.seluser', null, getConstText({
			isArgs : true,
			m_key : 'res.00012'
		}));
		var setUserGrid = createGridComp(setUserGridProps);

		var choiceBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.choice'
			})
		});

		// 버튼 패널
		var btnPanel = Ext.create('Ext.panel.Panel', {
			border : false,
			layout : {
				type : 'vbox',
				align : 'center'
			},
			margin : '5 0 5 0',
			items : [ choiceBtn ]
		});

		// 패널 병합
		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			layout : {
				type : 'vbox',
				align : 'center'
			},
			items : [ userSearchForm, userGrid, btnPanel, setUserGrid ]
		});

		var acceptBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct',
			scale : 'medium'
		});

		userSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : 597,
			width : 600,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				},
				cellclick : function(grid, td, cellIndex, record, tr, rowIndex,
						e, eOpts) {
				}
			}
		});

		popUp_comp[that['id']] = userSelectPop;

		function goSearch() {
			var searchForm = Ext.getCmp(searchFormId);
			var paramMap = searchForm.getInputData();
			if(that['params'] != null) {
				for(var key in that['params']) {
					paramMap[key] = that['params'][key];
				}
			}
			Ext.getCmp(that['id'] + '_grid').searchList(paramMap);
		}

		function acceptEvent() {
			gridAllRowCopyToGrid(that['id'] + '_setGrid', that['targetGrid'],
					true, null, {
						primaryKeys : [ 'USER_ID' ]
					});
			userSelectPop.close();
		}

		function choiceEvent() {
			checkGridRowCopyToGrid(that['id'] + '_grid', that['id']
					+ '_setGrid', {
				primaryKeys : [ 'USER_ID' ]
			});
		}

		attachBtnEvent(acceptBtn, acceptEvent);
		attachBtnEvent(choiceBtn, choiceEvent);

	} else {
		userSelectPop = popUp_comp[that['id']];
	}

	return userSelectPop;
}

/**
 * @function
 * @summary 공통 엑셀업로드 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createExcelUploadPop(paramMap){
	var that = {};
	for( var prop in paramMap ){
		that[prop] = paramMap[prop];
	}
	var excelType = that['excelType'];
	var popObj = null;
	var validFlag = false;
	var fileUploadBtn	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.file.upload' }), id: 'fileUploadBtn', ui:'correct'});	
	var templateDownBtn	= createBtnComp({label: 'Template', id: 'templateDownBtn', icon :'/itg/base/images/ext-js/common/btn-icons/excel-icon.png'});	
	var gridValidBtn	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.validation' }), id: 'gridValidBtn', margin: '10 0 10 400', ui:'correct', scale:'medium'});	
	var gridInsertBtn	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.insert' }), id: 'insertBtn', ui:'correct'});	
	
	var fileAttachCmp = createAtchFileFieldComp({ 
				label: ''
			  , id: 'atch_file_name'
			  , name: 'atch_file_name'
			  , width : 865
			  , maxFileCount: 1
			  , notNull: true
			  , fileHeight: 80
	});
	
	if(popUp_comp[that['id']] == null) {
		var fileUploadPanelProp = {
				  id: 'fileUploadPanel'
				, editOnly		: true
				, formBtns		: [fileUploadBtn, templateDownBtn]
				, border		: true
				, title			: getConstText({ isArgs: true, m_key: 'res.label.itam.maint.00016' })
				, columnSize	: 1
				, tableProps	: [{	colspan:1, tdHeight: 105, item: fileAttachCmp }]
		};
		var fileUploadPanel = createSeachFormComp(fileUploadPanelProp);	
		
		var filePanel = createHorizonPanel({ panelItems: [ { flex: 2, items: fileUploadPanel }]});
		
		var getExcelUploadGridProp = {
				  id					: "excelTempUploadGrid"
				, title				: "업로드 목록" + "<font color='#B2CCFF' style='float:right'>&nbsp;■ 실패&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color='#CEF279' style='float:right'>&nbsp;■ 중복&nbsp;</font>"
				, gridHeight			: 340
				, gridWidth			: 887
				, resource_prefix	: that['resource_prefix']
				, isMemoryStore : true
				, params			: null
				, pagingBar 		: false
				, selfScroll 		: true
				, cellEditing		: true
				, forceFit			: false
				, autoScroll 		: true
				, emptyMsg			: getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00010'})
		};
		var tempGrid = createGridComp(getExcelUploadGridProp);
		
		var validResultGridProp = {
				  id				: "validResultGrid"
				, title				: getConstText({ isArgs: true, m_key: 'res.label.code.00014' })
				, gridHeight		: 97
				, gridWidth			: 887
				, resource_prefix	: 'grid.system.excelUpload.validResult'
				, isMemoryStore 	: true
				, params			: null
				, pagingBar 		: false
				, selfScroll 		: true
				, forceFit			: true
				, autoScroll 		: true
				, emptyMsg			: getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00010'})
		};
		var validResultGrid = createGridComp(validResultGridProp);
		
		
		var callbackSub = function(data){
			var fileCompId = Ext.getCmp("atch_file_name_grid").atch_file_id;
				
			if(fileCompId != ""){
				jq.ajax({ type:"POST"
						, url: that['parseUrl']
						, contentType: "application/json"
						, dataType: "json"
						, async: false
						, data : getArrayToJson( { atch_file_id: fileCompId, hearders : that['hearders'] } )
						, success:function(data){
							if( data.success || data.resultMap.storeList != undefined){
								var storeList 		= data.resultMap.storeList;
								var gridObj  		= Ext.getCmp("excelTempUploadGrid");
								var gridStore	= gridObj.getStore();
							    for(var i = 0; i < storeList.length; i++){
							  	  gridStore.insert(i, storeList[i]);
							    }
							}else{
								alert(data.resultMsg);
							}
						}
				});
			}
		}
		
		var gridInsertBtnEvent = function (){
			if(validFlag){
				var store 			= Ext.getCmp('excelTempUploadGrid').getStore();
				var paramList 	= [];
				var paramMap	 = {};
				for(var k = 0; k < store.data.items.length; k++){
					paramList.push(store.getAt(k).data);
				}
				paramMap['dataList'] = paramList;
				
				jq.ajax({ type:"POST"
						, url: that['insertUrl']
						, contentType: "application/json"
						, dataType: "json"
						, async: false
						, data : getArrayToJson(paramMap )
						, success:function(data){
							if( data.success ){
								alert(data.resultMsg);
								windowComp.close();
							}else{
								alert(data.resultMsg);
							}
						}
					});
			}else{
				showMessage(getConstText({ isArgs: true, m_key: 'msg.code.result.00007' }));
			}
		}
		
		var gridValidBtnEvent = function (validRulesMap){
			var grid 					= jq('#excelTempUploadGrid-body').find('.x-grid-row');
			var gridObj 			= Ext.getCmp('excelTempUploadGrid');
			var store 				= gridObj.getStore();
			var dataLength 	= store.data.items.length;
			var allCnt 				= dataLength;
			var successCnt 	= dataLength;
			var failCnt 			= 0;
			var failFlag 			= 0;
			
			for(var k = 0; k < dataLength; k++){
					var dataMap	= store.getAt(k).data;
					var colIndex 		= 0;
					for ( var dataKey in dataMap) {
						for ( var validKey in validRulesMap) {
							if(dataKey == validKey){
								var ruleKey 	= validRulesMap[validKey];
								var rule 			= ruleKey.split(",");
								for(var x = 0; x < rule.length; x++){
									
									if( rule[x] == "NULL" ){ //제약조건들 추가해나가면 됩니다.
										if(dataMap[dataKey] == null || dataMap[dataKey] == "" || dataMap[dataKey] == "-"){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										}
									}else if( rule[x] == "NUMBER" ){										
										if( isNaN(dataMap[dataKey]) == true ){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										}
									}else if( rule[x] == "STRING" ){
										if( isNaN(dataMap[dataKey]) ){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										}
									}else if( rule[x] == "BIGALPHA"){
										var regExp = /^([A-Z0-9|_^]+)$/;
										if(!regExp.test(dataMap[dataKey])){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										};
									}else if(rule[x] == "PK"){
										
										for(var p = k+1; p < dataLength; p++){
											var compareMap = store.getAt(p).data[dataKey];
											var nowVal = dataMap[dataKey];
											
											if(compareMap == nowVal){
												failFlag = failFlag+1;
												grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #CEF279");
												grid.eq(p).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #CEF279");
											}
										}
										var resultFunc = function( data ){
										if( data.success ){
											var cnt = data.resultString;
											if(cnt  > 0){
												failFlag = failFlag+1;
												grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #CEF279");
											}
										}else{
												alert(data.resultMsg);
											}
										}
										dataControls( that['validUrl'], dataMap, resultFunc );
									}else{
										
									}
								}
							}
						}
						colIndex = colIndex +1;
					}
					
					if(failFlag > 0){
						var successCnt = successCnt-1;
						var failCnt = failCnt+1;
					}
			}
			var validGridStore = Ext.getCmp('validResultGrid').getStore();
			validGridStore.removeAll();
			validGridStore.insert(0, {ALL_CNT : allCnt, SUCCESS_CNT : successCnt, FAIL_CNT : failCnt});
			
			if(failCnt == 0){
				validFlag = true;
				showMessage(getConstText({isArgs : true,m_key : 'msg.common.00013'}));
			}
		}
		
		var fileUploadBtnEvent = function (){
				Ext.getCmp('excelTempUploadGrid').getStore().removeAll();

				if(jQuery('#atch_file_name_grid-body').find(".x-grid-cell-inner").text() != ""){
					var isFile = checkFileSave("atch_file_name", {}, callbackSub);
					if( !isFile ){
						alert("파일등록 실패");
					}
				}else{
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.inventory.00003' }));
				}
		}
		
		var templateDownBtnEvent = function (){
				var downloader = Ext.create('nkia.custom.FileDownloadFrame', {
					id:  "TEMPLATE_DOWNLOAD_FRAME",
					renderTo: Ext.getBody()
				});
				
				var atch_file_id = '';
				
				if(excelType == "CODE"){
					atch_file_id = 9999;
					downloader.load({
						url: getConstValue('CONTEXT') + '/itg/base/downloadTemplate.do?atch_file_id=' + atch_file_id
					});	
				}else if(excelType == "TEST"){ //excelType에 따라 분기처리하세요.
				
				}
		}
			
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				windowComp.close();
			}
		});
		
		attachBtnEvent(fileUploadBtn				, fileUploadBtnEvent);
		attachBtnEvent(gridValidBtn					, gridValidBtnEvent, that['validRules']);
		attachBtnEvent(gridInsertBtn					, gridInsertBtnEvent);
		attachBtnEvent(templateDownBtn		, templateDownBtnEvent);
				
		var viewportProperty = {
				  borderId	: 'popupBorderId'
				, width		: 938
				, viewportId : 'popupViewId' 
				, center: { items : [filePanel, tempGrid, gridValidBtn, validResultGrid] }
		}
				
		var codePopupProp		= {
				  id:   that['id']+'Pop'
				, title: getConstText({ isArgs: true, m_key: 'res.label.itam.excelUpload.'+excelType+'.00001'})
				, width: 900
				, height: 675
				, modal: true
				, layout: 'fit'
				, buttons: [gridInsertBtn, closeBtn]
				, items: createBorderViewPortComp(viewportProperty, { isLoading: false, isResize : false} )
		  }

		windowComp = createWindowComp(codePopupProp);
		windowComp.show();
		
	} else {
		popObj = popUp_comp[that['id']];
	}
	
	return popObj;
}

/**
 * @function
 * @summary 엑셀다운로드 속성선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 * @author 전민수
 * @since 1.0
 */
function createExcelDownPropSelectPop(setPopUpProp) {
	var that = {};
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	var storeParam = {};
	var propSelectPop = null;
	var propNmArray = [];
	var propIdArray = [];
	var colNm;
	var colId;
	var addData = {};

	var tempExcelParam = that["excelParam"];
	var param = that["param"];

	if (popUp_comp[that['id']] == null) {
		// @@20130724 전민수 START
		// selModelDefault : true 함으로써 최초 속성선택 그리드 로드시 전체 체크되어진체로 로드되어짐
		var setGridProp = {
			id : that['id'] + '_grid',
			gridHeight : 380,
			resource_prefix : 'grid.popup.excel',
			selModel : true,
			selModelDefault : true,
			isMemoryStore : true,
			autoLoad : false,
			forceFit : false,
			remoteSort: false
		};
		// @@20130724 전민수 END
		var propGrid = createGridComp(setGridProp);
		colNm = tempExcelParam['headerNames'].split(",");
		colId = tempExcelParam['includeColumns'].split(",");

		var excelGridStore = getGridStore(that['id'] + '_grid');

		for ( var i = 0; i < colNm.length; i++) {
			addData['RNUM'] = i;
			addData['COL_NM'] = colNm[i];
			addData['COL_ID'] = colId[i];
			excelGridStore.add(addData);
		}

		var acceptBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct',
			scale : 'medium'
		});

		function selectEvent() {
			var selectedRecords = propGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.common.00015'
				}));
			} else {
				for ( var i = 0; i < selectedRecords.length; i++) {
					var col_nm = nullToSpace(selectedRecords[i].data.COL_NM);
					var col_id = nullToSpace(selectedRecords[i].data.COL_ID);
					propNmArray.push(col_nm);
					propIdArray.push(col_id);
				}

				var headerNm = propNmArray.join();
				var headerId = propIdArray.join();

				// @@ 20130903 고은규 엑셀 Align 추가
				var excelAttrs = {
					sheet : [ {
						sheetName : tempExcelParam['sheetName'],
						titleName : tempExcelParam['titleName'],
						headerNames : headerNm,
						includeColumns : headerId,
						groupHeaders : tempExcelParam['groupHeaders'],
						align : tempExcelParam['align']
					} ]
				};

				var excelParam = {};
				excelParam['fileName'] = tempExcelParam["fileName"];
				excelParam['excelAttrs'] = excelAttrs;

				goExcelDownLoad(tempExcelParam['url'], param, excelParam);
				propSelectPop.close();
			}
		}

		attachBtnEvent(acceptBtn, selectEvent);

		propSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'] + '_Pop',
			title : getConstText({
				isArgs : true,
				m_key : 'res.00029'
			}),
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : getConstValue('DEFAULT_POP_PADDING'),
			buttonAlign : getConstValue('DEFAULT_POP_BTN_ALIGN'),
			buttons : [ acceptBtn ],
			bodyStyle : getConstValue('DEFAULT_POP_CSS'),
			items : [ propGrid ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = propSelectPop;

	} else {
		propSelectPop = popUp_comp[that['id']];
	}

	propSelectPop.show();

	return propSelectPop;
}
 
/**
 * @function
 * @summary 멀티코드 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createMultiCodeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var multiCodePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {

		var popUpBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct',
			scale : 'medium'
		});
		var setTreeProp = {
			id : that['treeId'],
			url : getConstValue('CONTEXT')
					+ '/itg/base/searchMutilCodeDataListForTree.do',
			title : that['popUpTitle'],
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : false,
			collapseAllBtn : false,
			params : {
				grpCodeId : that['grpCodeId']
			},
			rnode_text : that['rnode_text'],
			expandLevel : 2
		}

		var staticTreeforPop = createTreeComponent(setTreeProp);

		attachCustomEvent('beforeitemdblclick', staticTreeforPop, dbSelectEvent);

		multiCodePop = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'] + " " + getConstText({
				m_key : "SELECT_POPUP"
			}),
			height : 400,
			width : 300,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : [ popUpBtn ],
			bodyStyle : 'background-color: white; ',
			items : [ staticTreeforPop ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUpBtn.on('click', dbSelectEvent);

		function dbSelectEvent() {
			var node = Ext.getCmp(that['treeId']).view.selModel.getSelection();
			if (node.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00033'
				}));
			} else {

				var node_leaf = node[0].raw.leaf;
				if (that['leafSelect'] == true) {
					if (node_leaf == false) {
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00032'
						}));
						/*
						Ext.getCmp(that['treeId']).expandAll(function() {
							//Ext.getCmp(that['treeId']).getEl().unmask();
							//toolbar.enable();
						});
						*/
						return;
					}
				}
				node_id = node[0].raw.node_id;
				node_nm = node[0].raw.text;

				that['targetHiddenField'].setValue(node_id);
				that['targetField'].setValue(node_nm);

				multiCodePop.close();
			}
		}

		popUp_comp[that['popId']] = multiCodePop;

	} else {
		multiCodePop = popUp_comp[that['popId']];
	}

	return multiCodePop;
}

/**
 * @function
 * @summary 트리선택 선택 팝업(공통)
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createTreeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var treeSelectPopBody = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {

		var popUpBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			id : that['treeId'] + "btn",
			ui : 'correct',
			scale : 'medium'
		});

		var setTreeProp = {
			context : '${context}',
			id : that['treeId'],
			title : that['title'] ? that['title'] : '',
			url : that['url'],
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : that['expandAllBtn'] ? that['expandAllBtn'] : true,
			collapseAllBtn : that['collapseAllBtn'] ? that['collapseAllBtn']
					: true,
			params : that['params'] ? that['params'] : {},
			rnode_text : that['rnode_text'] ? that['rnode_text'] : '프로세스유형',
			expandLevel : that['expandLevel'] ? that['expandLevel'] : 1
		}

		var staticTreeforPop = createTreeComponent(setTreeProp);

		attachCustomEvent('beforeitemdblclick', staticTreeforPop, dbSelectEvent);
		
		// btnHide 값을 true 로 주면 적용 버튼을 숨긴다.
		var btnControll = [popUpBtn];
		if(that['btnHide'] == true){	btnControll = [];	}

		treeSelectPopBody = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			height : 400,
			width : 300,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : btnControll,
			bodyStyle : 'background-color: white; ',
			items : [ staticTreeforPop ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUpBtn.on('click', dbSelectEvent);

		function dbSelectEvent() {
			// accept btnHide 
			if(that['btnHide'] == true){	return false;	}
			
			var node = Ext.getCmp(that['treeId']).view.selModel.getSelection();
			if (node.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00033'
				}));
			} else {	
				
				//@@정중훈20131105 시작
				//Root는 선택이 안되게 만듭니다.
				// true : 선택이 됨
				// false : 선택이 안됨
				if(false == that["rootSelect"]){
					var node_level = node[0].raw.node_level;
					if(1 == node_level){
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00044'
						}));
						return;
					}
				}
				//@@정중훈20131105 종료

				var node_leaf = node[0].raw.leaf;
				if (that['leafSelect'] == true) {
					if (node_leaf == false) {
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00032'
						}));
						/*
						Ext.getCmp(that['treeId']).expandAll(function() {
							Ext.getCmp(this.id).getEl().unmask();
							//toolbar.enable();
						});
						*/
						return;
					}
				}
				node_id = node[0].raw.node_id;
				node_nm = node[0].raw.text;
				
				var otherData = node[0].raw.otherData;

				if(typeof that['targetVersionField'] != "undefined") {
					var versionText = "";
					if(otherData.SW_MNGT_VERSION != "ALL") {
						versionText = otherData.SW_MNGT_VERSION;
					}
					that['targetVersionField'].setValue(versionText);
				}
				if(typeof that['targetEditionField'] != "undefined") {
					that['targetEditionField'].setValue(otherData.SW_MNGT_EDITION);
				}
				
				that['targetHiddenField'].setValue(node_id);
				that['targetField'].setValue(Ext.util.Format.htmlDecode(node_nm));

				treeSelectPopBody.close();
			}
		}

		popUp_comp[that['popId']] = treeSelectPopBody;

	} else {
		treeSelectPopBody = popUp_comp[that['popId']];
	}

	return treeSelectPopBody;
}

/**
 * @function
 * @summary 구성 서비스 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createCiServicePop(setPopUpProp) {
	var that = {};
	var ciServiceSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var popType = that["popType"];
	if(popType == "basic") {
		ciServiceSelectPop = createBasicCiSercivePop(setPopUpProp);
	} else if(popType == "ciAndService") {
		popUp_props[that['popupId']] = setPopUpProp;
		window.open(getConstValue('CONTEXT') + "/itg/nbpm/common/goCiServiceSelectPop.do?popId=" + that['popupId'], "service_sel_pop", "width="+ getConstValue("PROCESS_CIPOP_WIDTH") +",height=650,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
		//ciAssetSelectPop = createCiAssetAndServicePop(setPopUpProp);
	}
	
	return ciServiceSelectPop;
}

/**
 * @function
 * @summary 일반 프로세스 구성, 서비스 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createBasicCiSercivePop(setPopUpProp) {

	var that = {};
	var servicePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var param = (that['param']) ? that['param'] : {};
	var selectedList = that['selectedList'];
	var selectedServiceIds = new Array();
	for(var i = 0 ; selectedList != null && i < selectedList.length ; i++) {
		selectedServiceIds.push(selectedList[i].service_id);
	}
	param["selectedList"] = selectedServiceIds;
	
	var checkServiceTreeId = 'checkServiceTree'; // sourceGridId
	var setServiceTreeProp = {
		id : checkServiceTreeId,
		url : getConstValue('CONTEXT') + '/itg/nbpm/common/searchServiceList.do',
		title : getConstText({
			isArgs : true,
			m_key : 'res.label.itam.service.00001'
		}),
		// height: (height) ? height : null,
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		autoLoad : false,
		collapseAllBtn : true,
		params : param,
		rnode_text : '#springMessage("res.00003")',
		expandLevel : 2
	}
	
	var checkServiceTree = createTreeComponent(setServiceTreeProp);
	
	if( !that["multiYn"] ){
		checkServiceTree.on("load", function(){
			checkServiceTree.getRootNode().cascadeBy(function (n) {
				delete n.set("checked", null);
			});
		});	
	}
	
	
	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn",
		ui : 'correct',
		scale : 'medium'
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.service'
		}),
		width : that['popupWidth'],
		height : that['popupHeight'],
		buttons : [ applyBtn ],
		items : [ checkServiceTree ],
		closeBtn : true
	}
	servicePop = createWindowComp(windowProp);
	
	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'treeLoad':
			var treeView = checkServiceTree.getView();
			var treeStore = checkServiceTree.getStore();
			break;
		case 'applyBtn': // 적용버튼 Click
			// 선택된 Tree 데이터를 타켓Grid에 복사 ( 소스Tree, 타켓Grid, append속성, 노데이타 메시지,
			// 검증키 )
			clearGridData(that['openerTargetGridId']);
			if( !that["multiYn"] ){
				selTreeNodeCopyToGridOnlyOne(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});	
			}else{
				treeCheckNodeCopyToGrid(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});
			}
			servicePop.close();
			break;
		}
	}
	/** ********************* Private Function Area End ** */
	return servicePop;
}

/**
 * @function
 * @summary 프로세스 구성 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createCiAssetPop(setPopUpProp) {
	var that = {};
	var ciAssetSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var req_zone = that['param']['req_zone'];
	
	var popType = that["popType"];
	if(popType == "basic") {
		ciAssetSelectPop = createBasicCiAssetPop(setPopUpProp);
	} else if(popType == "ciAndService") {
		popUp_props[that['popupId']] = setPopUpProp;
		window.open(getConstValue('CONTEXT') + "/itg/nbpm/common/goCiAssetSelectPop.do?popId=" + that['popupId'] + "&req_zone=" + req_zone, "ci_sel_pop", "width="+ getConstValue("PROCESS_CIPOP_WIDTH") +",height=650,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
	}
	return ciAssetSelectPop;
}

/**
 * @function
 * @summary 기본 구성 자산 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createBasicCiAssetPop(setPopUpProp) {
	var that = {};
	var ciAssetSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	// itam.js 참조 : ITAM분류체계트리(전부)
	var assetClassTree = getAssetClassAllTree('amClassAllTree', 3,
			that['params']);
	attachCustomEvent('select', assetClassTree, treeNodeClick);

	var assetBookMarkCateTree = {
		id : 'assetBookMarkCateTree',
		url : getConstValue('CONTEXT')
				+ '/itg/itam/amdb/searchAmClassAllTree.do',
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.bookMark'
		}),
		dynamicFlag : false,
		searchable : false,
		searchableTextWidth : 100,
		expandAllBtn : false,
		collapseAllBtn : false,
		params : null,
		rnode_text : getConstText({
			isArgs : true,
			m_key : 'res.00015'
		}),
		expandLevel : 2
	}
	var ciAssetBookMarkCateTree = createTreeComponent(assetBookMarkCateTree);
	attachCustomEvent('select', ciAssetBookMarkCateTree, treeNodeClick);

	var tabProperty = {
		tab_id : 'ciAssetTreeTab',
		width : '100%',
		layout : 'center',
		item : [ assetClassTree, ciAssetBookMarkCateTree ]
	// item: [assetClassTree]
	}
	var treeTabs = createTabComponent(tabProperty);

	// itam.js 참조 : 자산검색조건 / formId, searchBtnId, type, columnSize, callFuncKey
	var searchFormId = 'searchAssetForm' + that['popupId']; // searchFormId
	var searchAssetForm = getAssetSearchForm(searchFormId, 'searchBtn',
			'Atype', 2, searchBtnEvent);

	/**
	 * 검색 폼에서 text에서 엔터키 칠때 호출
	 */
	function searchBtnEvent() {
		actionLink('search');
	}

	// itam.js 참조 : 자산리스트 Grid(체크박스형태) / gridId
	var sourceGridId = 'checkAssetGrid'; // sourceGridId

	// itam.js 참조 : 선택자산목록에 사용되는 자산목록 Grid(MemoryStore)
	var targetGridId = 'selAssetGrid'; // targetGridId

	// 구성자원관리 조회 팝업의 GRID Prefix 및 소스 URL 경로를 변경 시켜줄 경우 사용
	if (that['targetPrefix'] != null && that['sourceUrl'] != null
			&& that['sourcePrefix'] != null) {
		var targetPrefix = that['targetPrefix']; // targetGridPrefix
		var selAssetGrid = getAssetGridByMemoryVariability(targetGridId,
				targetPrefix, {selectedList : that['selectedList'], height:170});

		var sourceUrl = that['sourceUrl']; // sourceGridUrl
		var sourcePrefix = that['sourcePrefix']; // sourceGridPrefix
		var checkAssetGrid;
		if( !that['multiYn'] ){
			checkAssetGrid = getRadioCheckAssetGridVariability(sourceGridId, sourceUrl, sourcePrefix, {height : 200});
		}else{
			checkAssetGrid = getCheckAssetGridVariability(sourceGridId, sourceUrl, sourcePrefix, {height : 200});
		}
		

	} else {
		var checkAssetGrid = getCheckAssetGrid(sourceGridId, {
			pageSize : 5,
			gridHeight : 200
		});
		
		var selAssetGrid = getAssetGridByMemory(targetGridId, {
			pageSize : 5,
			gridHeight : 170
		});
	}

	// 선택버튼
	var choiceBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.choice'
		}),
		id : "choiceBtn"
	});
	attachBtnEvent(choiceBtn, actionLink, 'choiceBtn');
	var choiceBtnpanel = createHorizonPanel({
		panelBtns : [ choiceBtn ]
	});

	// 좌측 패널 : Tree
	var left_panel = Ext.create('Ext.panel.Panel', {
		height : '100%',
		width : 200,
		region : 'west',
		autoScroll : false,
		split : true,
		maxWidth : 200,
		minWidth : 200,
		items : [ treeTabs ]
	});

	// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
	var right_panel = Ext.create('Ext.panel.Panel',
			{
				border : false,
				region : 'center',
				items : [ searchAssetForm, checkAssetGrid, choiceBtnpanel,
						selAssetGrid ]
			});

	// 패널 병합
	var complexPanel = Ext.create('nkia.custom.Panel', {
		border : false,
		layout : {
			type : 'border'
		},
		items : [ left_panel, right_panel ],
		listeners : {
			afterrender : function() {
				left_panel.splitter.setWidth(1);
				this.doLayout();
			}
		}
	});

	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn",
		ui : 'correct',
		scale : 'medium'
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.00019'
		}),
		width : that['popupWidth'],
		height : that['popupHeight'],
		buttons : [ applyBtn ],
		items : [ complexPanel ],
		layout : {
			type : 'border'
		},
		closeBtn : true
	}
	ciAssetSelectPop = createWindowComp(windowProp);
	// Opener Grid의 데이터를 타겟 Grid에 복사 : 구성자원 데이터( 소스Grid, 타켓Grid, append속성, 노데이타
	// 메시지 )
	// gridAllRowCopyToGrid( that['openerTargetGridId'], targetGridId, false,
	// null);

	/** * Private Function Area Start ******************** */
	function treeNodeClick(tree, record, index, eOpts) {
		var am_class_id = record.raw.node_id;
		getForm(searchFormId).setFieldValue("class_id", am_class_id); // 분류체계ID
		// hidden
		goSearch();
	};

	var appendable;
	if (that['appendable'] != null) {
		appendable = that['appendable'];
	} else {
		appendable = false;
	}
	
	//기존에 선택된 리스트가 있다면 세팅한다.
	actionLink('gridLoad');
	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'gridLoad' :
			var changeSelectedList = new Array();
			for(var i = 0 ; that['selectedList'] != null && i < that['selectedList'].length ; i++) {
				var row = {};
				for(var key in that['selectedList'][i]) {
					row[key.toUpperCase()] = that['selectedList'][i][key] 
				}
				changeSelectedList.push(row);
			}
			addDataListToGrid(targetGridId, changeSelectedList, {
				primaryKeys : that['primaryKeys']
			})
			break;
		case 'choiceBtn': // 선택버튼 Click
			// 선택된 정보 타켓Grid에 복사 ( 소스Gird, 타켓Grid, 중복체크 keys )
			if( !that['multiYn'] ){
				checkGridRowCopyToGridOnlyOne(sourceGridId, targetGridId, {
					primaryKeys : that['primaryKeys']
				});
			}else{
				checkGridRowCopyToGrid(sourceGridId, targetGridId, {
					primaryKeys : that['primaryKeys']
				});	
			}
			break;
		case 'applyBtn': // 적용버튼 Click
			clearGridData(that['openerTargetGridId']);
			// Grid 데이터 전체를 타켓Grid에 복사 ( 소스Grid, 타켓Grid, append속성, 노데이타 메시지 )
			gridAllRowCopyToGrid(targetGridId, that['openerTargetGridId'],
					appendable, null, {
						primaryKeys : that['primaryKeys']
					});
			ciAssetSelectPop.close();
			break;
		case 'search': // 검색버튼 Click
			goSearch();
			break;
		}
	}

	// 검색
	function goSearch() {
		var theForm = getForm(searchFormId);
		var paramMap = theForm.getInputData();
		if (that['params'] != null && that['params'].assetId) {
			paramMap['asset_id'] = that['params'].assetId;
		}
		getGrid(sourceGridId).searchList(paramMap);
	}
	/** ********************* Private Function Area End ** */
	return ciAssetSelectPop;
}

/**
 * @function
 * @summary 서비스연계조회 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createServiceRelPop(setPopUpProp) {
	var that = {};
	var servicePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	var ciCustTreeIdProp = {
		context : '${context}',
		id : 'ciCustTree',
		url : getConstValue('CONTEXT') + '/itg/itam/oper/searchSystemList.do',
		title : getConstText({
			isArgs : true,
			m_key : 'res.00013'
		}),
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		collapseAllBtn : true,
		height : 334,
		params : null,
		autoLoad : true,
		rnode_text : getConstText({
			isArgs : true,
			m_key : 'res.00003'
		}),
		expandLevel : 3
	}
	var ciCustTree = createTreeComponent(ciCustTreeIdProp);

	var checkServiceTreeId = 'checkServiceTree'; // sourceGridId
	var checkServiceTree = getCheckServiceTree(checkServiceTreeId, 1,
			that['param'], 334);

	var complexItem = [];
	if (that['flag'] == true || that['flag'] == undefined) {
		complexItem.push({
			items : checkServiceTree
		});
	} else {
		complexItem.push({
			flex : 1,
			items : ciCustTree
		});
		complexItem.push({
			flex : 1,
			items : checkServiceTree
		});
	}

	var complexPanelProp = {
		border : true,
		panelItems : complexItem
	}
	var complexPanel = createHorizonPanel(complexPanelProp);

	attachCustomEvent('itemclick', ciCustTree, ciCustTreeClick);

	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn"
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.service'
		}),
		width : 600,
		height : 400,
		buttons : [ applyBtn ],
		items : [ complexPanel ],
		closeBtn : true
	}
	servicePop = createWindowComp(windowProp);

	/** * Private Function Area Start ******************** */

	function ciCustTreeClick(tree, record, index, eOpts) {

		var cust_id = record.raw.node_id;

		var checkServiceTreeStore = getGrid(checkServiceTreeId).getStore();
		checkServiceTreeStore.proxy.jsonData = {
			cust_id : cust_id,
			expandLevel : 1
		};
		checkServiceTreeStore.reload();
	}

	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'applyBtn': // 적용버튼 Click
			// 선택된 Tree 데이터를 타켓Grid에 복사 ( 소스Tree, 타켓Grid, append속성, 노데이타 메시지,
			// 검증키 )
			treeCheckNodeCopyToGrid(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});
			servicePop.close();
			break;
		}
	}
	/** ********************* Private Function Area End ** */
	return servicePop;
}

/**
 * @function
 * @summary 제조사 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createVendorSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var vendorSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.title.system.vendor.list'
			}), // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.vendor.popup', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			pagingBar : true,
			pageSize : 10
		};

		var vendorGrid = createGridComp(setGridProp);

		var vendorSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'SearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(vendorSearchBtn, actionLink, 'searchAttr');
		
		var vendorSearchInitBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.reset'
			}),
			id : that['id'] + 'SearchInitBtn'
		});
		attachBtnEvent(vendorSearchInitBtn, actionLink, 'searchInit');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ vendorSearchBtn, vendorSearchInitBtn ],
			enterFunction : clickVendorSearchBtn,
			columnSize : 1,
			tableProps : [ {
				colspan : 1,
				item : createTextFieldComp({
					label : getConstText({
						isArgs : true,
						m_key : 'res.label.system.vendor.00002'
					}),
					name : 'search_vendor_nm'
				})
			} ]
		}

		var vendor_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ vendor_search_form, vendorGrid ]
		})

		var tagetVendorIdField = that['tagetVendorIdField'];
		var tagetVendorNameField = that['tagetVendorNameField'];
		var vendorSalesTelField = that['vendorSalesTelField'];
		var vendorSalesNameField = that['vendorSalesNameField'];
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale : 'medium',
			handler: function() {
				vendorSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
			, ui : 'correct'
			, scale : 'medium'
			, handler : function() {
				var selectedRecords = vendorGrid.view.selModel.getSelection();
				if (selectedRecords.length < 1) {
					showMessage(getConstText({
						isArgs : true,
						m_key : 'msg.itam.vendor.00016'
					}));
				} else {

					if (that['afterFunction'] != null) {
						that['afterFunction'](selectedRecords[0].raw,
								that['mappingObj']);
					} else {
						dbSelectEvent();
					}
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();

				vendorGrid.searchList(paramMap);
				break;
			case 'searchInit':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				searchForm.initData();
				break;
			}
		}

		function clickVendorSearchBtn() {
			actionLink('searchAttr');
		}
		
		function dbSelectEvent() {
			var selectedRecords = vendorGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.itam.vendor.00016'
				}));
			} else {

				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var vendor_id = selectedRecords[0].raw.VENDOR_ID;
					var vendor_nm = selectedRecords[0].raw.VENDOR_NM;
					var sales_user_tel = selectedRecords[0].raw.SALES_USER_TEL;
					var sales_user_nm = selectedRecords[0].raw.SALES_USER_NM;
					tagetVendorIdField.setValue(vendor_id);
					tagetVendorNameField.setValue(vendor_nm);
					if(vendorSalesTelField != null && vendorSalesNameField != null){
						Ext.getCmp(that['targetFormId']).setFieldValue(vendorSalesTelField, sales_user_tel);
						Ext.getCmp(that['targetFormId']).setFieldValue(vendorSalesNameField, sales_user_nm);
					}
					vendorSelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', vendorGrid, dbSelectEvent);

		vendorSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = vendorSelectPop;

	} else {
		vendorSelectPop = popUp_comp[that['id']];
	}

	return vendorSelectPop;
}

/**
 * @function
 * @summary 사업 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProjectSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var projectSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.title.itam.project'
			}), // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.project', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			pagingBar : true,
			pageSize : 10
		};

		var projectGrid = createGridComp(setGridProp);

		var projectSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'ProjectBtn',
			ui : 'correct'
		});
		attachBtnEvent(projectSearchBtn, actionLink, 'searchAttr');
		
		var projectSearchInitBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.reset'
			}),
			id : that['id'] + 'ProjectInitBtn'
		});
		attachBtnEvent(projectSearchInitBtn, actionLink, 'searchInit');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ projectSearchBtn, projectSearchInitBtn ],
			columnSize : 2,
			enterFunction : clickProjectSearchBtn,
			tableProps : [{
				colspan : 1,
				item : createTextFieldComp({
					label : getConstText({
						isArgs : true,
						m_key : 'res.label.itam.project.00002'
					}),
					name : 'search_project_nm',
					width : 250
				})
			}]
		}

		var project_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ project_search_form, projectGrid ]
		})

		var tagetProjectIdField = that['tagetProjectIdField'];
		var tagetProjectNameField = that['tagetProjectNameField'];
		var tagetProjectSeqField = that['tagetProjectSeqField'];
		var tagetEtcField = that['tagetEtcField'];

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale: 'medium',
			handler: function() {
				projectSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
			, ui : 'correct'
			, scale: 'medium'
			, handler : function() {
				dbSelectEvent();
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();

				projectGrid.searchList(paramMap);
				break;
			case 'searchInit':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				searchForm.initData();
				break;
			}
		}
		
		function clickProjectSearchBtn() {
			actionLink('searchAttr');
		}
		
		function dbSelectEvent() {
			var selectedRecords = projectGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.itam.project.00013'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var project_id = selectedRecords[0].raw.PROJECT_ID;
					var project_nm = selectedRecords[0].raw.PROJECT_NM;
					tagetProjectIdField.setValue(project_id);
					tagetProjectNameField.setValue(project_nm);
					projectSelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', projectGrid, dbSelectEvent);

		projectSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 800,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = projectSelectPop;

	} else {
		projectSelectPop = popUp_comp[that['id']];
	}

	return projectSelectPop;
}

/**
 * @function
 * @summary 서비스 조회 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createServicePop(setPopUpProp) {
	var that = {};
	var servicePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	var checkServiceTreeId = 'checkServiceTree'; // sourceGridId
	var checkServiceTree = getCheckServiceTree(checkServiceTreeId, 1,
			that['param']);

	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn"
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.service'
		}),
		width : that['popupWidth'],
		height : that['popupHeight'],
		buttons : [ applyBtn ],
		items : [ checkServiceTree ],
		closeBtn : true
	}
	servicePop = createWindowComp(windowProp);

	/** * Private Function Area Start ******************** */
	function serviceCustClick(gridCmp, tdEl, cellIndex, record, trEl, rowIndex,
			e) {
		var cust_id = record.raw.CUST_ID;
		var tree_store = getTreeStore(checkServiceTreeId);
		var rootNode = tree_store.getRootNode();
		while (rootNode.firstChild) { // Root의 ChildNode가 있을 경우
			rootNode.removeChild(rootNode.firstChild);
		}
		tree_store.proxy.jsonData = {
			cust_id : cust_id,
			expandLevel : 1
		};
		tree_store.load();
	}

	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'applyBtn': // 적용버튼 Click
			// 선택된 Tree 데이터를 타켓Grid에 복사 ( 소스Tree, 타켓Grid, append속성, 노데이타 메시지,
			// 검증키 )
			treeCheckNodeCopyToGrid(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});
			servicePop.close();
			break;
		}
	}
	/** ********************* Private Function Area End ** */
	return servicePop;
}

/**
 * @function
 * @summary 프로세스 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcessSelectPop(setPopUpProp) {
	var storeParam = {};
	var that = {};
	var processSelectPop = null;
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {

		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : '', // Grid Title
			gridHeight : 305,
			resource_prefix : 'grid.pop.process', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			forceFit : true,
			pagingBar : true,
			pageSize : 10
		};

		var processGrid = createGridComp(setGridProp);
		
		// 상세보기 컬럼 추가
		var procViewColumn = Ext.create('Ext.grid.column.Action', {
			text: getConstText({ isArgs: true, m_key: 'res.common.label.detailview' }),
			width: 100,
			align: 'center',
			items: [{
		        icon: '/itg/base/images/ext-js/common/icons/bullet_search.gif',
		        tooltip: getConstText({ isArgs: true, m_key: 'res.common.label.detailview' }),
		        handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex);
		            var recordRaw = rec.raw;
		            showProcessDetailPop(recordRaw["SR_ID"]);
		        }
		    }]
		});
		
		processGrid.headerCt.add(procViewColumn);

		var searchBtn = new Ext.button.Button({
			id : that['id'] + '_btn',
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			handler : function(thisBtn, event) {
				/* 기존에 사용하던 부분
				var conditionVal = searchFormPanel.getValue();
				var textVal = searchText.getValue();
				var processGridStore = processGrid.getStore();
				var param = {};
				param[conditionVal] = textVal
				processGridStore.proxy.jsonData = param;
				processGridStore.load();
				*/
				//@@정중훈20140217 검색이 안되는 부분 수정 시작
				var param = Ext.getCmp('searchForm').getInputData();
				if(param['end_time_startDate_day'] > param['end_time_endDate_day']){
					if(param['end_time_endDate_day'] == ""){
						alert("완료종료일을 입력해주세요.");
						return false;
					}
					alert("완료일시 입력이 잘못되었습니다.");
					return false;
				}
				
				if(param['req_time_startDate_day'] == "" && param['end_time_endDate_day'] != ""){
					alert("완료시작일을 입력해주세요.");
					return false;
				}
				
				if(param['req_time_startDate_day'] > param['req_time_endDate_day']){
					if(param['req_time_endDate_day'] == ""){
						alert("요청종료일을 입력해주세요.");
						return false;
					}
					alert("요청일시 입력이 잘못되었습니다.");
					return false;
				}

				if(param['req_time_startDate_day'] == "" && param['req_time_endDate_day'] != ""){
					alert("요청시작일을 입력해주세요.");
					return false;
				}
				
				Ext.getCmp(that['id'] + '_grid').searchList(param);
				//@@정중훈20140217 검색이 안되는 부분 수정 종료
			}
		});

		var setSearchFormProp = {
			id : 'searchForm',
			url : '$!{context}/itg/nbpm/common/searchProcessList.do',
			formBtns : [ searchBtn ],
			columnSize : 2,
			//@@정중훈20140217 엔터펑션 추가 시작
			enterFunction : function(){
				var param = Ext.getCmp('searchForm').getInputData();
				Ext.getCmp(that['id'] + '_grid').searchList(param);
			},
			//@@정중훈20140217 엔터펑션 추가 종료
			tableProps : [
			// row1
			{
				colspan : 1,
				tdHeight : 28,
				item : createTextFieldComp({
					width : 300,
					label : '요청자',
					name : 'req_user_nm'
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createTextFieldComp({
					width : 300,
					label : '요청번호',
					name : 'sr_id'
				})
			}
			// row3
			, {
				colspan : 1,
				tdHeight : 28,
				item : createSearchDateFieldComp({
					label : '완료일시',
					name : 'end_time'
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createSearchDateFieldComp({
					label : '요청일시',
					name : 'req_time'
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createCodeComboBoxComp({
					label : '요청유형',
					name : 'req_type',
					code_grp_id:'REQ_TYPE',
					attachAll:true
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createCodeComboBoxComp({
					label : '요청상태',
					name : 'work_state',
					code_grp_id:'WORK_STATE_SEARCH',
					attachAll:true
				})
			}
			// row2
			, {
				colspan : 2,
				tdHeight : 28,
				item : createTextFieldComp({
					label : '제목',
					name : 'title',
					width : 660
				})
			} ]
		}

		var searchFormPanel = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ searchFormPanel, processGrid ]
		})

		// var tagetUserIdField = that['tagetUserIdField'];
		// var tagetUserNameField = that['tagetUserNameField'];
		// var tagetEtcField = that['tagetEtcField'];
		
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct'
		});
		if( that["acceptBtnVisible"] == false ){
			acceptBtn = null;	
		}
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				processSelectPop.close();
			}
		});
		
		
		processSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height']+28,
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : (acceptBtn != null) ? [acceptBtn, closeBtn] : [closeBtn],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = processSelectPop;
		
		if( acceptBtn != null ){
			function gridSelectEvent(grid, td, cellIndex, record) {
				if( cellIndex != 6 ){	// 상세보기 클릭시는 동작하지 않는다.
					var selectedRecords = processGrid.view.selModel.getSelection();
					if (selectedRecords.length < 1) {
						showMessage("요청을 선택하지 않았습니다.");
					} else {
						that['tagetUpSrIdField'].setValue(selectedRecords[0].raw.SR_ID);
						processSelectPop.close();
					}	
				}
			}			
		}

		attachCustomEvent('celldblclick', processGrid, gridSelectEvent);
		if( acceptBtn != null ){
			attachBtnEvent(acceptBtn, gridSelectEvent)	
		}
		
	} else {
		processSelectPop = popUp_comp[that['id']];
	}
	return processSelectPop;
}

/**
 * @function
 * @summary 코드 검색 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createSelSysCodePopup(commPropeties){
	var that = convertDomToArray(commPropeties);
	
	var targetForm	= that["targetForm"];
	var targetObjId	= that["targetObjId"];
	var targetObjNm	= that["targetObjNm"];
	
	var popId				= targetObjId+'PopId';
	var upCodeId			= "GRP_ID";
	
	
	
	//cell click 시에 호출되는 function
	var getSysCode = function (){
		var paramMap = searchFormPanel.getInputData();
		paramMap["code_grp_id"] 	= upCodeId;
		paramMap["use_yn"] 		= "Y";
		paramMap["paging"] 			= true;
		Ext.getCmp('sysCodeGrid').searchList(paramMap);
	}
	
	//Search Form 초기화
	var setSearchInit = function (){
		searchFormPanel.initData();
	}
	
	//cell click 시에 호출되는 function
	var getSysCodeDetail = function ( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e){
		var paramMap = searchFormPanel.getInputData();
		paramMap["code_grp_id"] 	= record.raw.CODE_ID;
		paramMap["searchValue"]	= "";
		paramMap["searchKey"]	= "";
		paramMap["paging"] 		= false;
		paramMap["use_yn"] 	= "Y";
		Ext.getCmp('sysCodeDetailGrid').searchList(paramMap);
	}
	
	
	//적용버튼 클릭시에 호출되는 function
	var confirmHandler = function(){
			if (isSelected("sysCodeGrid")) {
				var selRow = gridSelectedRows("sysCodeGrid");
				var setTargetForm	= Ext.getCmp(targetForm).getForm();
				if(setTargetForm){
					
					if(setTargetForm.findField(targetObjId)){
						setTargetForm.findField(targetObjId).setValue(selRow.get('CODE_ID'));
					}
					
					if(setTargetForm.findField(targetObjNm)){
						setTargetForm.findField(targetObjNm).setValue(selRow.get('CODE_NM'));
					}
					Ext.getCmp(popId).close();
				}
			}else{
				showMessage("선택된 코드가 없습니다.");
				return;
			}
		}
	//검색버튼
	var searchCodeBtn		= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchCodeBtn' ,ui:'correct' ,handler : getSysCode });
	
	var searchPopInitBtn 	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.reset' }), name:'searchPopInitBtn' ,id:'searchPopInitBtn', handler : setSearchInit });
	
	var attrSearchContainer	= createUnionCodeComboBoxTextContainer({label : '검 색', comboboxName: 'searchKey', textFieldName: 'searchValue', code_grp_id: 'CODE_POP_SEARCH_KEY', widthFactor: 0.25 });
	
	var selectSearchKey = attrSearchContainer.getComboBoxField();
	var selectSearchValue = attrSearchContainer.getTextField();
	
	
	var setSearchFormProp 	= {
								 id			: 'codeSearchForm'
								,columnSize	: 1
								,url		: '/itg/system/code/searchCodeDataForChlid.do'
								,formBtns	: [searchCodeBtn, searchPopInitBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [{colspan:1, tdHeight: 30, item : attrSearchContainer}]
								,enterFunction : getSysCode
							  }
	
	var searchFormPanel 	= createSeachFormComp(setSearchFormProp);
	
	
	var setCodeGridProp 	= {	 id			: 'sysCodeGrid'
								,title		: '코드'
								,resource_prefix: 'grid.syscode'
								,url		: '/itg/system/code/searchCodePopup.do'
								,params		: {code_grp_id : upCodeId,paging : true	,use_yn : "Y"}
								,pagingBar 	: true
								,border 	: true
								,gridHeight	: 175
								,pageSize 	: 10
							};
	
	var gridPanel			= createGridComp(setCodeGridProp);
	
	var gridComp1 = Ext.getCmp("sysCodeGrid");
	
	var setCodeDetailGridProp		= {	 id			: 'sysCodeDetailGrid'
										,title		: '코드상세'
										,height		: getConstValue('TALL_GRID_HEIGHT')
										,resource_prefix: 'grid.syscode'
										,url		: '/itg/system/code/searchCodePopup.do'
										,params		: {code_grp_id : "",paging : false	,use_yn : "Y" }
										,pagingBar 	: false
										,border 	: true
										,gridHeight	: 175
										,pageSize 	: 10
									};
	
	var gridDetailPanel		= createGridComp(setCodeDetailGridProp);
	
	var complexPanel = createFlexPanel({items : [searchFormPanel, gridPanel,gridDetailPanel]});
	
	var conFirmBtn		=	createBtnComp({ id : "codePopConfirmBtn" , label:getConstText({ isArgs: true, m_key: 'btn.common.apply' })	,ui   : "correct"	,scale: "medium"	,handler : confirmHandler });
	
	var codePopupProp		= {
								id:   popId
								,title: '공통코드선택'
								,width: 700
								,height: 500
								,modal: true
								,layout: 'fit'
								,buttonAlign: 'center'
								,bodyStyle: 'background-color: white; '
								,buttons: [conFirmBtn]
								,closeBtn : true
								,items: complexPanel
								,resizable:true
							  }
	
	attachCustomEvent('cellclick', gridPanel, getSysCodeDetail);
	
	var callback = function(){
		Ext.getCmp('sysCodeDetailGrid').searchList(null);
	};
	gridPanel.store.on("load",callback);
	
	window_comp = createWindowComp(codePopupProp);
	window_comp.show();
}

/**
 * @function
 * @summary KEDB 등록 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createKedbRegisterPop(setPopUpProp){
	var storeParam = {};
	var that = {};
	var kedbRegPop = null;
	var paramMap = null;
	for(var i = 0 ; i < panelList.length ; i++) {
		if(panelList[i].isForm && panelList[i].editorMode) {
			paramMap = panelList[i].getInputData();
			
			originSrData = panelList[i].getSelectedRecord();
		}
	}
	for(var key in originSrData) {
		if(originSrData[key] != "") {
			paramMap[key.toLowerCase()] = originSrData[key];
		}
	}
	
	var inputData = {};
	inputData["title"] = nullToSpace(paramMap.title);
	inputData["content"] = nullToSpace(paramMap.content);
	inputData["cause_type_nm"] = nullToSpace(paramMap.pro_cause_type_nm);
	inputData["cause_type"] = nullToSpace(paramMap.pro_cause_type);
	inputData["root_cause"] = nullToSpace(paramMap.pro_root_cause);
	inputData["resolution"] = nullToSpace(paramMap.pro_solve_alternative);
	inputData["work_content"] = nullToSpace(paramMap.pro_solve_content);
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	var kedbInfoId = that['id'] + 'kedbInfoForm';
	var causeInfoId = that['id'] + 'causeInfoForm';
	var fileInfoId = that['id'] + 'fileInfoForm';
	if(popUp_comp[that['id']] == null) {
		var kedbCategoryCodeId = getConstValue('KEDB_CATEGORY_CODE_GRP')
		var causeInfo = {
				id: causeInfoId,
				title: getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00003'}),
				columnSize: 3,
				editOnly: true,
				border: true,
				tableProps :[{colspan:3, item : createMultiCodeSelectPopComp({ popId : 'incCauseTypePop', popUpTitle : getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00001'}), targetName : 'cause_type_nm', targetHiddenName : 'cause_type', btnId : 'incCauseTypeBtn', treeId : 'inccategoryTree', grpCodeId : kedbCategoryCodeId, targetLabel : getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00001'}), params : null,notNull:true})}
							,{colspan:3, item : createTextFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00004'}), name:'title', notNull:true, width: 900, maxLength:200})}
							,{colspan:3, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00005'}), name:'content', width: 900, maxLength:2000})}],
				hiddenFields:[{name : 'cause_type'}]
		}
		var causeInfoForm = createEditorFormComp(causeInfo);
		causeInfoForm.setDataForMap(inputData);
		var kedbInfo = {
				id: kedbInfoId,
				title: getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00010'}),
				columnSize: 2,
				editOnly: true,
				border: true,
				tableProps :[{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00006'}), name:'temp_resolution', width: 450, maxLength:2000})}
							,{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00007'}), name:'root_cause', width: 450, maxLength:2000})}
							,{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00008'}), name:'resolution', width: 450, maxLength:2000})}
							,{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00009'}), name:'work_content', width: 450, maxLength:2000, notNull:true})}]
		}
		var kedbInfoForm = createEditorFormComp(kedbInfo);
		kedbInfoForm.setDataForMap(inputData);
		var fileInfo = {
				id: fileInfoId,
				title: getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00011'}),
				columnSize: 1,
				editOnly: true,
				border: true,
				tableProps :[{colspan:1, item : createAtchFileFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00011'}), name:'atch_file_id', width: 847, fileHeight: 100, anchor: '94%'})}]
		}
		
		var fileInfoForm = createEditorFormComp(fileInfo);
		var complexPanel = Ext.create('Ext.form.Panel', {
	        id: 'complexPanel',
			items: [causeInfoForm,kedbInfoForm,fileInfoForm]
		});
			
		var tagetKedbIdField = that['tagetKedbIdField'];
		
		var insertBtn = createBtnComp({
			visible : true,
			label : getConstText({isArgs : true, m_key : 'btn.common.regist'}),
			id : that['id'] + 'insertBtn',
			ui : 'correct',
			scale : 'medium'
		});
		
		var closeBtn = createBtnComp({
			visible : true,
			label : getConstText({isArgs : true, m_key : 'btn.common.close'}),
			id : that['id'] + 'closeBtn',
			scale : 'medium'
		});
		
		attachBtnEvent(insertBtn, actionLink, 'insert');
		attachBtnEvent(closeBtn, actionLink, 'close');
		
		function actionLink(command) {
			switch (command) {
				case 'insert':
					if(Ext.getCmp(causeInfoId).isValid() && Ext.getCmp(kedbInfoId).isValid()){
						// 마스크를 띄웁니다.
						setViewPortMaskTrue();
								
						var paramMap = Ext.getCmp(fileInfoId).getInputData();
						
						var title = Ext.getCmp(causeInfoId).getFieldValue('title');
						var content = Ext.getCmp(causeInfoId).getFieldValue('content');
						var cause_type = Ext.getCmp(causeInfoId).getFieldValue('cause_type');
						
						var temp_resolution = Ext.getCmp(kedbInfoId).getFieldValue('temp_resolution');
						var root_cause = Ext.getCmp(kedbInfoId).getFieldValue('root_cause');
						var resolution = Ext.getCmp(kedbInfoId).getFieldValue('resolution');
						var work_content = Ext.getCmp(kedbInfoId).getFieldValue('work_content');
						
						paramMap['title'] = title;
						paramMap['content'] = content;
						paramMap['cause_type'] = cause_type;
						paramMap['temp_resolution'] = temp_resolution;
						paramMap['root_cause'] = root_cause;
						paramMap['resolution'] = resolution;
						paramMap['work_content'] = work_content;
						var callbackSub = function(){
				 			jq.ajax({ type:"POST", url: getConstValue('CONTEXT') + '/itg/workflow/kedb/insertKedb.do', 
								contentType: "application/json", dataType: "json" , data : getArrayToJson(paramMap), 
								success:function(data){
									if( data.success ){
										var kedbId = data.resultMap.kedbId;
										tagetKedbIdField.setValue(kedbId);
										kedbRegPop.close();
									}else{
										showMessage(data.resultMsg);
					   				}
					   			
						   			// 마스크를 숨깁니다.
									setViewPortMaskFalse();
								}
							});
						}
				 		var isFile = checkFileSave("atch_file_id", paramMap, callbackSub);
						if( !isFile ){
							eval(callbackSub)();			
						}
					} else {
						showMessage(getConstText({ isArgs: true, m_key: 'msg.common.00012' }));
					}
				break;
			case 'close':
				kedbRegPop.close();
				break;
			}
		}
		
		var windowProp = {
			id: that['id'],
			title: that['popUpText'],  
			height: 620,
			width: 970,
			layout:{
				type: 'border'
			},
			buttons: [insertBtn, closeBtn],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		};
		kedbRegPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = kedbRegPop;
		
	} else {
		kedbRegPop = popUp_comp[that['id']];
	}
	
	return kedbRegPop;	
}

/**
 * @function
 * @summary KEDB 조회 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createKedbSelectPop(setPopUpProp){
	
	var storeParam = {};
	var that = {};
	var kedbSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {

		var grpId = "GRP_ID";
		var kedbCategoryCodeId = getConstValue('KEDB_CATEGORY_CODE_GRP');
		var param = {};
		param["categoryCodeId"] = kedbCategoryCodeId;
		
		var setTreeProp = {
			context: getConstValue('CONTEXT'),						
			id: that['id']+'_tree',	
			url: getConstValue('CONTEXT') + '/itg/workflow/kedb/searchCategory.do',
			title: getConstText({ isArgs: true, m_key: 'res.label.workflow.kedb.00001' }),
			dynamicFlag : false,
			searchable : true,
			searchableTextWidth : 100,
			expandAllBtn : true,
			collapseAllBtn : true,
			params: param,
			rnode_text : getConstText({ isArgs: true, m_key: 'res.label.workflow.kedb.00001' }),
			expandLevel: 0
		}
		var causeTypeTree = createTreeComponent(setTreeProp);

		var setGridProp = {	
				context: getConstValue('CONTEXT'),						// Context
				id: that['id']+'_grid',								// Grid id
				title: getConstText({ isArgs: true, m_key: 'res.title.system.kedb.search' }),		// Grid Title
				height: getConstValue('SHORT_GRID_HEIGHT'),
				resource_prefix: 'grid.workflow.kedb',
				url: getConstValue('CONTEXT') +'/itg/workflow/kedb/searchKedbList.do',
				params: {codeGrpId:grpId, codeId: kedbCategoryCodeId, categoryCodeId: kedbCategoryCodeId},
				pagingBar : true,
				pageSize : 10
		};
		
		var kedbGrid = createGridComp(setGridProp);
		
		var kedbSearchBtn   = createBtnComp({visible: true, label:getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:that['id']+'SearchBtn', ui:'correct'});
		attachBtnEvent(kedbSearchBtn, actionLink,  'searchAttr');
		
		var setSearchFormProp = {
				id: that['id']+'SearchForm',
				formBtns: [kedbSearchBtn],			
				columnSize: 2,
				tableProps : [
					            {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.workflow.kedb.00004' }), name:'searchTitle'})}
					          , {colspan:1, item : createTextFieldComp({label:'KEDB ID', name:'searchKedbID'})}
				             ]
		}

		var kedb_search_form = createSeachFormComp(setSearchFormProp);

		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: '100%',
			width: 200,
			autoScroll: false,
			split: true,
			maxWidth: 200,
			minWidth: 200,
			items: [causeTypeTree]
		});


		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			//flex:1 ,
			items: [kedb_search_form,kedbGrid]
		});

		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			layout:{
				type: 'border'
			},
			items: [left_panel,right_panel],
			listeners: {
				afterrender: function(){
					left_panel.splitter.setWidth(1);
					this.doLayout();
				}
			}
		});
			
		var tagetKedbIdField = that['tagetKedbIdField'];
		
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			handler: function() {
				var selectedRecords = kedbGrid.view.selModel.getSelection();
				if(selectedRecords.length <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.workflow.kedb.00001' }));
				}else{
					
					if(that['afterFunction'] != null) {
						that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
					} else {
						var kedb_id = selectedRecords[0].raw.KEDB_ID;
						tagetKedbIdField.setValue(kedb_id);
						kedbSelectPop.close();
					}
				}	
			}
		});
		
		/*** Private Function Area Start *********************/		
		
		function actionLink(command) {

			switch(command) {		
				case 'searchAttr' :
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
			
					kedbGrid.searchList(paramMap);
				break;
			
			}
		}
		
		function dbSelectEvent(){
			var selectedRecords = kedbGrid.view.selModel.getSelection();
			if(selectedRecords.length <1){
				showMessage(getConstText({ isArgs: true, m_key: 'msg.workflow.kedb.00001' }));
			}else{
				
				if(that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
				} else {
					var kedb_id = selectedRecords[0].raw.KEDB_ID;
					tagetKedbIdField.setValue(kedb_id);
					kedbSelectPop.close();
				}
			}
		}
		
		function causeTypeTreeClick(tree, record, index, eOpts) {
			
			var code_id = record.raw.node_id;
			
			if(code_id == KEDB_CATEGORY_CODE_ID){
				var getSelect = getGrid(that['id']+'_grid').getStore();
				getSelect.proxy.jsonData={codeGrpId: grpId, codeId: kedbCategoryCodeId, categoryCodeId: kedbCategoryCodeId};
				getSelect.load();	
			}else{
				var getSelect = getGrid(that['id']+'_grid').getStore();
				getSelect.proxy.jsonData={codeGrpId: kedbCategoryCodeId, codeId: code_id, categoryCodeId: kedbCategoryCodeId};
				getSelect.load();
			}

		}		
		
		/*********************** Private Function Area End ***/
		
		attachCustomEvent('select', causeTypeTree, causeTypeTreeClick);
		attachCustomEvent('beforeitemdblclick', kedbGrid, dbSelectEvent);		
		
		var windowProp = {
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'],
			width: that['width'],
			layout:{
				type: 'border'
			},
			buttons: [acceptBtn],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
					
				},
				hide:function(p) {
				}
			}
		};
		kedbSelectPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = kedbSelectPop;
		
	} else {
		kedbSelectPop = popUp_comp[that['id']];
	}
	
	return kedbSelectPop;
}

/**
 * @function
 * @summary 장애레벨 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createIncLevelSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var incLevelSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {
		var maxLevel = that['maxLevel'];
		var renderIncLevelMetrix = function(view, eOpts) {
			var gridTrs = view.el.query('tr.x-grid-row');
			if (gridTrs != null && gridTrs.length > 0) {
				for ( var i = 0; i < gridTrs.length; i++) {
					var rowTr = Ext.get(gridTrs[i]);
					var tds = rowTr.query('td');
					for ( var j = 0; j < tds.length; j++) {
						if (j == 0) {
							Ext.get(tds[j]).set({
								'class' : 'incPopHeader'
							});
						} else {

							var rawData = view.store.data.items[i].raw;
							var incLevelTime = rawData["INC_LEVEL_TIME_" + j];
							var incLevel = rawData["INC_LEVEL_" + j];
							var incLevelNm = rawData["INC_LEVEL_NM_" + j];
							var effectLevel = rawData["EFFECT_NM"];
							var urgencyNm = rawData["URGENCY_NM_" + j];
							// j + 1 은 긴급도
							// i + 1 은 영향도
							if (maxLevel != null && i + 1 == parseInt(maxLevel)) {
								Ext.get(tds[j]).set({
									'class' : 'incSelecedPopTd'
								});
							} else {
								Ext.get(tds[j]).set({
									'class' : 'incPopTd'
								});
							}

							Ext.get(tds[j]).set(
									{
										'onclick' : "Ext.getCmp('"
												+ that['targetFormId']
												+ "').getFormFieldContainer('"
												+ that['targetName']
												+ "').selectedCell('"
												+ incLevel + "','"
												+ incLevelTime + "','"
												+ incLevelNm + "','" // 장애레벨,장애시간,장애레벨명
												+ effectLevel + "','"
												+ urgencyNm + "');" // 영향도, 긴급도
												+ "Ext.getCmp('"
												+ that['popId'] + "').close();"
									});
						}
					}
				}
			}
		}

		var setGridProp = {
			title : "등급선택", // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.nbpm.inclevelmatrix',
			url : '/itg/nbpm/common/searchIncLevelMatrix.do',
			params : {},
			viewRenderer : renderIncLevelMetrix,
			pagingBar : false,
			autoLoad : true
		};

		var gridComp = createGridComp(setGridProp);
		// attachCustomEvent('cellclick', gridComp, dbSelectEvent);
		// popUpBtn.on('click', dbSelectEvent);

		incLevelSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			height : 340,
			width : 500,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : [],
			bodyStyle : 'background-color: white; ',
			items : [ gridComp ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['popId']] = incLevelSelectPop;

	} else {
		incLevelSelectPop = popUp_comp[that['popId']];
	}

	return incLevelSelectPop;
}

/**
 * @function
 * @summary 변경등급 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createChaLevelSelectPop(setPopUpProp) {
	
	var storeParam = {};
	var that = {};
	var incLevelSelectPop = null;
	
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	if (popUp_comp[that['popId']] == null) {
		
		var fieldContainer = Ext.getCmp(that["targetFormId"]).getFormFieldContainer(that["targetName"]);
		var values = fieldContainer.getValues();
		
		var btnHandler = function(){
			if( effectField.getRawValue() == "-- 선 택 --" ||
				dangerField.getRawValue() == "-- 선 택 --" ||
				gradeField.getRawValue() == "-- 선 택 --"){
				alert( "모든항목을 선택하여 주십시오." );
			}else{
				//Ext.getCmp(that["targetFormId"]).getFormFieldContainer(that["targetName"]).selectedCell((resultIndex+1), 0, resultFieldValue, effectField.getValue(), dangerField.getValue(), gradeField.getValue());
				Ext.getCmp(that["targetFormId"]).getFormFieldContainer(that["targetName"]).selectedCell((resultIndex+1), 0, resultFieldValue, effectField.getRawValue(), dangerField.getRawValue(), gradeField.getRawValue());
				incLevelSelectPop.close();
			}
		};
		
		var popUpBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.apply' }), ui:'correct',scale:'medium'});
		attachBtnEvent(popUpBtn, btnHandler, "" );
		var maxLevel = that['maxLevel'];
		
		var setEditProp = {
			id: that['popId'] + "_editForm",
			title: "등급선택",
			columnSize: 1,
			editOnly: true,
			border: true,
			buttons: [],
			buttonAlign: 'center',
			tableProps :[
				{colspan:1, tdHeight: 30, item : createCodeComboBoxComp({label: "체계 등급별", name: "CHA_GRADE_LVL", code_grp_id: 'CHA_GRADE_LVL', attachChoice: true, width : 300, notNull: true})},
				{colspan:1, tdHeight: 30, item : createCodeComboBoxComp({label: "변경 영향도별", name: "CHA_EFFECT_LVL", code_grp_id: 'CHA_EFFECT_LVL', attachChoice: true, width : 300, notNull: true})},
				{colspan:1, tdHeight: 30, item : createCodeComboBoxComp({label: "변경 위험도별", name: "CHA_DANGER_LVL", code_grp_id: 'CHA_DANGER_LVL', attachChoice: true, width : 300, notNull: true})}
			]
		};
		
		var editComp = createEditorFormComp(setEditProp);
		
		var gradeField = editComp.getFormField("CHA_GRADE_LVL");
		var effectField = editComp.getFormField("CHA_EFFECT_LVL");
		var dangerField = editComp.getFormField("CHA_DANGER_LVL");
		// 데이터가 있을시 입력
		var gradeImValue;
		var effectImValue;
		var dangerImValue;
		var isValues = false;
		if( values["conf_level"] != null ){
			//gradeField.setValue(values["conf_level"]);
			gradeImValue = values["conf_level"];
			isValues = true;
		}
		if( values["effect"] != null ){
			//effectField.setValue(values["effect"]);
			effectImValue = values["effect"];
			isValues = true;
		}
		if( values["risk"] != null ){
			//dangerField.setValue(values["risk"]);
			dangerImValue = values["risk"];
			isValues = true;
		}
		
		var resultValue;
		var resultFieldValue;
		var resultIndex;
		
		var changeHandler = function(){
			var gradeValue = gradeField.getValue();
			var effectValue = effectField.getValue();
			var dangerValue = dangerField.getValue();
			
			resultValue = 0;
			if( gradeValue != null ){ resultValue = resultValue + (Number(gradeValue) / 100 * 50); }
			if( effectValue != null ){ resultValue = resultValue + (Number(effectValue) / 100 * 30); }
			if( dangerValue != null ){ resultValue = resultValue + (Number(dangerValue) / 100 * 20); }
			
			var dataIdx;
			for( dataIdx=0; dataIdx < dataList.length; dataIdx++ ){
				
				if( dataIdx == 0 ){
					if( Number(dataList[dataIdx]["CODE_ID"]) < resultValue ){
						break;
					}
				}else{
					if( Number(dataList[dataIdx-1]["CODE_ID"]) >= resultValue && Number(dataList[dataIdx]["CODE_ID"]) < resultValue ){
						break;
					}
				}
			}
			
			if( dataIdx >= dataList.length ){
				dataIdx = dataList.length-1;
			}
			resultIndex = dataIdx;
			resultFieldValue = dataList[dataIdx]["CODE_TEXT"];
			designPanel.selectedRow(dataIdx);
		};
		
		gradeField.on("change", changeHandler);
		effectField.on("change", changeHandler);
		dangerField.on("change", changeHandler);
		
		var getComboValue = function(){
			var me = this;
			if( isValues == true ){
				var loofDataStore = function(value, targetField){
					var imList = me.data.items;
					for( var imIdx=0; imIdx < imList.length; imIdx++ ){
						var imData = imList[imIdx].data;
						if( imData["CODE_TEXT"] == value ){
							break;
						}else{
							imData = null;
						}
					}
					
					if( imData != null ){
						targetField.setValue( imData["CODE_ID"] );
					}
				};
				
				if( this == gradeField.getStore() ){
					loofDataStore(gradeImValue, gradeField);
				}
				if( this == effectField.getStore() ){
					loofDataStore(effectImValue, effectField);
				}
				if( this == dangerField.getStore() ){
					loofDataStore(dangerImValue, dangerField);
				}
			}
		};
		
		gradeField.getStore().on("load", getComboValue);
		effectField.getStore().on("load", getComboValue);
		dangerField.getStore().on("load", getComboValue);
		
		var dataList;
		jq.ajax({ 
			type: "POST"
			, url: "/itg/base/searchCodeDataList.do"
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson({code_grp_id: 'CHA_WORK_LVL', start: '0', limit: '10', page: '1'})
			, success:function(data){
				if( data.success ){
					dataList = data.gridVO.rows;
				}
			}
		});
		
		var templateHtml = new Ext.XTemplate(
		  	'<table id="{id}-table" class="x-grid-table x-grid-easy-table<tpl if="bodyCls"> {bodyCls}</tpl>" style="width: 100%;">',
	  			'<tbody>',
	  				'<tr>',
			  			'<tpl for="headers">',
			  				'<td class="x-grid-header-ct x-column-header x-column-easy-header" style="width: {width}; height: 30px; background-image: none; background-color: transparent;"><div class="x-column-header-inner" style="text-align: center;">{text}</div></th>',
			  			'</tpl>',
		  			'</tr>',
	  				'<tpl for="rows">',
		  				'<tr class="x-grid-row x-grid-with-row-lines x-grid-row-alt">',
			  				'<tpl for=".">',
			  						'<td class="x-grid-cell x-grid-cell-inner" colspan="1" rowspan="1" style="height: 25px;"><div style="text-align: center;">{text}</div></td>',
			  				'</tpl>',
		  				'</tr>',
		  			'</tpl>',
	  			'</tbody>',
	  		'</table>'
	  	);
		var gridColumns = [
			{text: "변경 작업 위험점수", width: "150px"},
			{text: "변경 작업 위험도", width: "150px"}
		];
		var gridRows = [];
		
		for( var idx=0; idx < dataList.length; idx++ ){
			var rowData = dataList[idx];
			var row1 = {};
			var row2 = {};
			
			if( idx == 0 ){ // 첫번째 Row
				row1["text"] = rowData["CODE_ID"] + "점 초과";
			}else if( idx == dataList.length - 1 ){ // 마지막 Row
				row1["text"] = dataList[idx-1]["CODE_ID"] + "점 이하";
			}else{
				row1["text"] = dataList[idx-1]["CODE_ID"] + "이하 ~ " + rowData["CODE_ID"] + "초과";
			}
			
			row2["text"] = rowData["CODE_TEXT"];
			
			gridRows[idx] = [row1, row2];
		}
		
		var gridHTML = templateHtml.applyTemplate({
			headers: gridColumns,
			rows: gridRows
		});
		
		var designPanel = Ext.create("Ext.Component", {
			html: gridHTML,
			selectedRow: function(idx){
				var rows = this.el.query("tr.x-grid-row");
				
				for( var rowIdx in rows ){
					Ext.get(rows[rowIdx]).removeCls("x-grid-row-selected");
				}
				Ext.get(rows[idx]).addCls("x-grid-row-selected");
			}
		});
		
		incLevelSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			height : 340,
			width : 500,
			autoDestroy : false,
			resizable : false,
			//layout : 'fit',
			buttonAlign : 'center',
			buttons : [popUpBtn],
			bodyStyle : 'background-color: white; ',
			items : [ editComp, designPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});
		
		popUp_comp[that['popId']] = incLevelSelectPop;
		
	} else {
		incLevelSelectPop = popUp_comp[that['popId']];
	}
	
	return incLevelSelectPop;
}

/**
 * @function
 * @summary 프로세스 삭제 사유 입력 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcessDelReasonPop(setPopUpProp) {

	var that = {};
	var delReasonPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var delReasonTextArea = createTextAreaFieldComp({
			"name" : "delete_reason",
			"notNull" : true,
			"width" : 478,
			"height" : 230
		});
		var paramMap = that["param"];
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			handler : function() {
				paramMap["delete_reason"] = delReasonTextArea.getValue();
				if(paramMap["delete_reason"] == null || paramMap["delete_reason"] == "") {
					alert("삭제사유를 입력해주시기 바랍니다.");
					return false;
				}
				var closeFlag = false;
				jq.ajax({
					"type" : "POST",
					"url" : getConstValue('CONTEXT')
							+ "/itg/nbpm/edit/checkDeleteSrData.do",
					"contentType" : "application/json",
					"dataType" : "json",
					"async": false,
					"data" : getArrayToJson(paramMap),
					"success" : function(data) {
						if (data.success) {
							var runFlag = false;
							// 묶여있는 프로세스가 있는지 판단함
							if (data.resultMap.isExist) {
								if (confirm(data.resultMsg)) {
									runFlag = true;
								} else {
									delReasonPop.close();
								}
							} else {
								runFlag = true;
							}
							if (runFlag) {
								jq.ajax({
									type : "POST",
									url : getConstValue('CONTEXT')
											+ "/itg/nbpm/edit/deleteSrData.do",
									contentType : "application/json",
									dataType : "json",
									async: false,
									data : getArrayToJson(paramMap),
									success : function(data) {
										if (data.success) {
											alert(data.resultMsg);
											delReasonPop.close();
											closeFlag = true;
										} else {
											alert(data.resultMsg);
											setViewPortMaskFalse();
										}
									}
								});
							}
						} else {
							alert(data.resultMsg);
							setViewPortMaskFalse();
						}
					}
				});
				if(closeFlag) {
					window.parent.closeTab();
				}
			}
		});

		delReasonPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpTitle'],
			height : 300,
			width : 500,
			autoDestroy : false,
			resizable : false,
			bodyPadding : '5 5 5 5',
			buttonAlign : 'center',
			buttons : [ acceptBtn ],
			bodyStyle : 'background-color: white; ',
			items : [ delReasonTextArea ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = delReasonPop;

	} else {
		delReasonPop = popUp_comp[that['id']];
	}

	return delReasonPop;
}

/**
 * @function
 * @summary 프로세스 담당자 변경 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createUserOperChangePop(setPopUpProp){
	
	var storeParam = {};
	var that = {};
	var userSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var param = ( that['params'] ) ? that['params'] : {};
		
		var comboProps = {
			//id: that['id']+"_combo",
			labelWidth : getConstValue('POP_LABEL_WIDTH'),
			displayField: "searchNm",
			valueField: "searchCol",
			padding: getConstValue('WITH_FIELD_PADDING'), 
			label: getConstText({ isArgs: true, m_key: 'res.common.search' }),
			name: "searchCondition",
			storeData : [
			             {searchCol : 'search_user_nm', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00015' }) },
			             {searchCol : 'search_user_id', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00014' }) },
			             {searchCol : 'cust_nm', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00023' }) },
			             {searchCol : 'emp_id', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00024' }) }
			            ],
			width: 200
		};
		
		var searchConditionComp = createComboBoxComp(comboProps);
		searchConditionComp.select('search_user_nm');
		
		var searchText = createTextFieldComp({hideLabel: true, name:'user_nm', padding: getConstValue('WITH_FIELD_PADDING'), width: 150, label:getConstText({ isArgs: true, m_key: 'res.label.system.00015' })})
		
		var userSearchBtn = createBtnComp({
		    label: getConstText({ isArgs: true, m_key: 'btn.common.search' })
		  , margin: getConstValue('WITH_BUTTON_MARGIN')
		});
		
		//사용자 조회 폼
		var setSearchUserFormProp = {
			id: that['id']+'searchUserForm',
			title: '업무목록',
			target: that['id']+'_grid',
			columnSize: 3,
			enterFunction : clickUserSearchBtn,
			tableProps : [{
							colspan:1, tdHeight: 30, item: searchConditionComp
						},{
							colspan:1, tdHeight: 30, item: searchText
						},{
							colspan:1, tdHeight: 30, item: userSearchBtn
						}]
		}
		var userSearchForm = createSeachFormComp(setSearchUserFormProp);		
		var setGridProp = {	
			id: that['id']+'_grid',							
			gridHeight: 250,
			resource_prefix: 'grid.popup.user',				
			url: getConstValue('CONTEXT') + that['url'],
			params: that['params'],
			pagingBar : true,
			pageSize : 15
		};
		
		var userGrid = createGridComp(setGridProp);
		
		var userInfoFormProp = {
			"id" : "userInfoForm",
			"title" : "변경정보",
			"columnSize" : 1,
			"editOnly" : true,
			"formBtns" : [],
			"tableProps" : [{
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createTextFieldComp({
									"label" : "변경 전 담당자",
									"name" : "worker_nm",
									"value" : '',
									"readOnly" : true
								})
					}, {
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createTextFieldComp({
									"label" : "변경후  담당자",
									"name" : "after_worker_nm",
									"value" : "",
									"readOnly" : true
								})
					}],
			"hiddenFields" : [{
						"name" : "after_worker_id",
						"value" : ""
					},{
						"name" : "worker_id",
						"value" : ''
					}]
		};
		var userInfoFormPanel = createEditorFormComp(userInfoFormProp);		
		// 패널 병합
		
		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			layout: 'vbox',
			items: [userSearchForm, userGrid, {xtype:'tbspacer', height:7 },userInfoFormPanel]
		});
		
		var tagetUserIdField = that['tagetUserIdField'];
		var tagetUserNameField = that['tagetUserNameField'];
		var tagetEtcField = that['tagetEtcField'];
		
		var acceptBtn = createBtnComp({
			label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct', 
			scale:'medium'
		});
		
		function clickUserSearchBtn() {
			var conditionVal = searchConditionComp.getValue();
        	var textVal = searchText.getValue();
			var userGridStore = userGrid.getStore();
			param[conditionVal] = textVal
			userGridStore.proxy.jsonData = param;
			userGridStore.load();
		}

		function clickGrid(grid, td, cellIndex, record, tr, rowIndex){
			userInfoFormPanel.setHiddenValue('after_worker_id', record.raw.USER_ID);
			userInfoFormPanel.setFieldValue('after_worker_nm', record.raw.USER_NM);
		}
		
		function changeWorker() {
			var varData = userInfoFormPanel.getInputData();
			varData["sr_id"] = param["SR_ID"];
			varData["id"] = param["ID"];
			varData["auth_id"] = param["AUTH_ID"];
			varData["role_id"] = param["ROLE_ID"];
			jq.ajax({ type:"POST"
					, url: getConstValue('CONTEXT') + "/itg/nbpm/edit/updateWorkerData.do"
					, contentType: "application/json"
					, dataType: "json"
					, data : getArrayToJson(varData)
					, success:function(data){
						if( data.success ){
							showMessage(data.resultMsg);
							if(userSelectPop != null) {
								userSelectPop.close();
								if(that["afterFunction"] != null) {
									that["afterFunction"]();
								}
							}
						}else{
							showMessage(data.resultMsg);
						}
			        }
			});
		}		

		attachBtnEvent(acceptBtn, changeWorker);
		attachBtnEvent(userSearchBtn, clickUserSearchBtn);
		attachCustomEvent('cellclick', userGrid, clickGrid);
		userSelectPop = Ext.create('nkia.custom.windowPop',{
			id: that['id']+'_Pop',
			title: '사용자조회',  
			height: 500, 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		userInfoFormPanel.setDataForMap(that['params']);
		
		popUp_comp[that['id']] = userSelectPop;
		
	} else {
		userSelectPop = popUp_comp[that['id']];
	}
	
	return userSelectPop;
}

/**
 * @function
 * @summary VM 목록 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createVmSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var vmSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var nwType = "";
	if(that['clo_nw_type'] != null) {
		nwType = that['clo_nw_type'];
	}
	
	var nwTypeNm = "";
	if(that['clo_nw_type_nm'] != null) {
		nwTypeNm = that['clo_nw_type_nm'];
	}
	
	if ( popUp_comp[that['id']] == null ){
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : '가상서버 요청내역'
			}), // Grid Title
			gridHeight : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.vmList', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],  
			selfScroll: true
		};

		var vmAssetGrid = createGridComp(setGridProp);

		var vmSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'vmSearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(vmSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ vmSearchBtn ],
			columnSize : 2,
			tableProps : [
				{ colspan : 1, item : createTextFieldComp({label : '가상서버 ID', name : 'search_vm_id', width : 250 }) },
				{ colspan : 1, item : createTextFieldComp({label : '가상서버명', name : 'search_vm_nm', width : 250 }) }
			],
			enterFunction: function(){
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();
					paramMap['search_nw_type'] = nwType;
				vmAssetGrid.searchList(paramMap);
			}
		}

		var vm_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ vm_search_form, vmAssetGrid ]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				vmSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				var selModel = vmAssetGrid.view.selModel;
				if( selModel.hasSelection() ){
					var selectedRecords = selModel.getSelection();				// 선택된 Tree_Node
					var selData = selectedRecords[0].data;
					var targetForm = Ext.getCmp(that['targetFormId']);
					
					if( targetForm.getFormField("cha_storage_size") ){
						targetForm.getFormField("cha_storage_size").reset();
					}
					
					targetForm.setHiddenValue("clo_vm_id", selData["VM_ID"]);
					targetForm.setFieldValue("clo_os_code", selData["OS_CODE"]);
					targetForm.setFieldValue("clo_vm_nm", selData["VM_NM"]);
					targetForm.setFieldValue("clo_host_nm", selData["HOST_NM"]);
					targetForm.setFieldValue("clo_os_nm", selData["OS_NM"]);
					targetForm.setFieldValue("clo_cpu", selData["CPU"]);
					targetForm.setFieldValue("clo_memory", selData["MEMORY"]);
					targetForm.setFieldValue("clo_add_storage", selData["STORAGE"]);
					targetForm.setFieldValue("clo_storage", selData["STORAGE"]);
					targetForm.setFieldValue("clo_storage_prev", selData["STORAGE"]);
					targetForm.setFieldValue("clo_network_code", nwType);
					targetForm.setFieldValue("clo_network_nm", nwTypeNm);
					
					vmSelectPop.close();
				}else{
					showMessage("선택된 내역이 없습니다.");
					return;
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {
			switch (command) {
				case 'searchAttr':
					var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
					var paramMap = searchForm.getInputData();
						paramMap['search_nw_type'] = nwType;
					vmAssetGrid.searchList(paramMap);
				break;
			}
		}

		function dbSelectEvent() {
			var selModel = vmAssetGrid.view.selModel;
			if( selModel.hasSelection() ){
				var selectedRecords = selModel.getSelection();				// 선택된 Tree_Node
				var selData = selectedRecords[0].data;
				var targetForm = Ext.getCmp(that['targetFormId']);
				
				if( targetForm.getFormField("cha_storage_size") ){
					targetForm.getFormField("cha_storage_size").reset();
				}
				
				targetForm.setHiddenValue("clo_vm_id", selData["VM_ID"]);
				targetForm.setFieldValue("clo_os_code", selData["OS_CODE"]);
				targetForm.setFieldValue("clo_vm_nm", selData["VM_NM"]);
				targetForm.setFieldValue("clo_host_nm", selData["HOST_NM"]);
				targetForm.setFieldValue("clo_os_nm", selData["OS_NM"]);
				targetForm.setFieldValue("clo_cpu", selData["CPU"]);
				targetForm.setFieldValue("clo_memory", selData["MEMORY"]);
				targetForm.setFieldValue("clo_add_storage", selData["STORAGE"]);
				targetForm.setFieldValue("clo_storage", selData["STORAGE"]);
				targetForm.setFieldValue("clo_storage_prev", selData["STORAGE"]);
				
				vmSelectPop.close();							
			}else{
				showMessage("선택된 내역이 없습니다.");
				return;
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', vmAssetGrid, dbSelectEvent);

		vmSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 744,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = vmSelectPop;

	} else {
		vmSelectPop = popUp_comp[that['id']];
	}

	return vmSelectPop;
}

/**
 * @function
 * @summary 작업요청 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcJobSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var procJobSelectPop = null;
	
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	// 최근일로부터 6달간의 데이터만 출력되도록 - 2014.09.30 정정윤
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-7, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})
	
	that['params']['search_req_dt_startDate'] = startDateObj['day'];
	that['params']['search_req_dt_endDate'] = endDateObj['day'];
	
	if ( popUp_comp[that['id']] == null ){
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : '가상서버 요청내역'
			}), // Grid Title
			gridHeight : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.process.jobSub2', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params']
		};

		var procJobGrid = createGridComp(setGridProp);

		var procJobSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'procJobSearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(procJobSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ procJobSearchBtn ],
			columnSize : 2,
			tableProps : [
				{ colspan : 1, item : createTextFieldComp({label : '장비명', labelWidth:50, name : 'search_conf_nm', width : 200 }) },
				{ colspan : 1, item : createSearchDateFieldComp({label:'요청일', labelWidth:50, name:'search_req_dt', start_dateValue : startDateObj, end_dateValue: endDateObj}) }
			],
			enterFunction: function(){
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();
				procJobGrid.searchList(paramMap);
			}
		}

		var procJobSearchForm = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ procJobSearchForm, procJobGrid ]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				procJobSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				var selModel = procJobGrid.view.selModel;
				if( selModel.hasSelection() ){
					var selectedRecords = selModel.getSelection();	// 선택된 Row
					var selData = selectedRecords[0].data;
					var targetForm = Ext.getCmp(that['targetFormId']);
					
					targetForm.setFieldValue("job_sr_id", selData["SR_ID"]);
					targetForm.setFieldValue("job_type", selData["JOB_TYPE"]);
					targetForm.setFieldValue("job_cycle", selData["JOB_CYCLE"]);
					targetForm.setFieldValue("job_exec_file_name", selData["JOB_EXEC_FILE_NAME"]);
					targetForm.setFieldValue("job_exec_file_path", selData["JOB_EXEC_FILE_PATH"]);
					targetForm.getFormField("job_st_dt").setValue(selData["JOB_ST_DT"]);
					//targetForm.getFormField("job_end_dt").setValue(selData["JOB_END_DT"]);
					//targetForm.setFieldValue("job_end_dt", selData["JOB_END_DT"]);
					targetForm.setFieldValue("job_work_terms", selData["JOB_WORK_TERMS"]);
					targetForm.setFieldValue("job_monitoring_yn", selData["JOB_MONITORING_YN"]);
					procJobSelectPop.close();
				}else{
					showMessage("선택된 내역이 없습니다.");
					return;
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {
			switch (command) {
				case 'searchAttr':
					var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
					var paramMap = searchForm.getInputData();
					paramMap['selectQuerykey'] = that['selectQuerykey'];
					procJobGrid.searchList(paramMap);
				break;
			}
		}

		function dbSelectEvent() {
			var selModel = procJobGrid.view.selModel;
			if( selModel.hasSelection() ){
				var selectedRecords = selModel.getSelection();	// 선택된 Row
				var selData = selectedRecords[0].data;
				var targetForm = Ext.getCmp(that['targetFormId']);
				
				targetForm.setFieldValue("job_sr_id", selData["SR_ID"]);
				targetForm.setFieldValue("job_type", selData["JOB_TYPE"]);
				targetForm.setFieldValue("job_cycle", selData["JOB_CYCLE"]);
				targetForm.setFieldValue("job_exec_file_name", selData["JOB_EXEC_FILE_NAME"]);
				targetForm.setFieldValue("job_exec_file_path", selData["JOB_EXEC_FILE_PATH"]);
				//targetForm.getFormField("job_st_dt").setValue(selData["JOB_ST_DT"]);
				//targetForm.getFormField("job_end_dt").setValue(selData["JOB_END_DT"]);
				targetForm.setFieldValue("job_end_dt", selData["JOB_END_DT"]);
				targetForm.setFieldValue("job_work_terms", selData["JOB_WORK_TERMS"]);
				targetForm.setFieldValue("job_monitoring_yn", selData["JOB_MONITORING_YN"]);
				procJobSelectPop.close();	
			}else{
				showMessage("선택된 내역이 없습니다.");
				return;
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', procJobGrid, dbSelectEvent);

		procJobSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 570,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = procJobSelectPop;

	} else {
		procJobSelectPop = popUp_comp[that['id']];
	}

	return procJobSelectPop;
}

/**
 * @function
 * @summary 프로세스 참조 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcReferencePop(setPopupProp) {
	var that = {};
	for ( var prop in setPopupProp) {
		that[prop] = setPopupProp[prop];
	}
	
	// 최근일로부터 6달간의 데이터만 출력되도록 - 2014.09.30 정정윤
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-7, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})
	
	var params = {};
	
	params['selectQuerykey'] = that['selectQuerykey'];
	params['search_req_dt_startDate'] = startDateObj['day'];
	params['search_req_dt_endDate'] = endDateObj['day'];
	
	var procReferencePop = null;
	if ( popUp_comp[that['id']] == null ){
		var setGridProp = {
			id : that['id'] + '_grid', // Grid id
			title : that['popupTitle'],
			gridHeight : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : that['resource_prefix'],
			url : getConstValue('CONTEXT') + "/itg/nbpm/searchProcessReferenceList.do", // Data Url
			params : params
		};

		var procReferenceGrid = createGridComp(setGridProp);

		var procReferenceSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'procJobSearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(procReferenceSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ procReferenceSearchBtn ],
			columnSize : 2,
			tableProps : [
				{ colspan : 1, item : createTextFieldComp({label : '장비명', labelWidth:50, name : 'search_conf_nm', width : 200 }) },
				{ colspan : 1, item : createSearchDateFieldComp({label:'요청일', labelWidth:50, name:'search_req_dt', start_dateValue : startDateObj, end_dateValue: endDateObj}) }
			],
			enterFunction: function(){
				actionLink('searchAttr');
			}
		}

		var procReferenceSearchForm = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ procReferenceSearchForm, procReferenceGrid ]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				procReferencePop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				var selModel = procReferenceGrid.view.selModel;
				if( selModel.hasSelection() ){
					var selectedRecords = selModel.getSelection();	// 선택된 Row
					var selData = selectedRecords[0].data;
					// Data Set
					setProcReferenceData(selData);
					procReferencePop.close();
				}else{
					showMessage("선택된 내역이 없습니다.");
					return;
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {
			switch (command) {
				case 'searchAttr':
					var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
					var paramMap = searchForm.getInputData();
					paramMap['selectQuerykey'] = that['selectQuerykey'];
					procReferenceGrid.searchList(paramMap);
				break;
			}
		}

		function dbSelectEvent() {
			var selModel = procReferenceGrid.view.selModel;
			if( selModel.hasSelection() ){
				var selectedRecords = selModel.getSelection();	// 선택된 Row
				var selData = selectedRecords[0].data;
				// Data Set
				setProcReferenceData(selData);
				
				procReferencePop.close();	
			}else{
				showMessage("선택된 내역이 없습니다.");
				return;
			}
		}
		
		function setProcReferenceData(selData){
			var targetForm = Ext.getCmp(that['targetFormId']);
			switch(that['switchCommand']) {
				case "bac_policy_id":
					targetForm.setFieldValue("rec_bac_policy_id", selData["BAC_POLICY_ID"]);
				break;
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', procReferenceGrid, dbSelectEvent);

		procReferencePop = Ext.create('nkia.custom.windowPop', {
			id : that['popupId'],
			title : that['popupTitle'],
			width : that['popupWidth'],
			height : that['popupHeight'],
			autoDestroy : false,
			resizable : false,
			buttons : [ acceptBtn, closeBtn ],
			buttonAlign : 'center',
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : 'background-color: white;',
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = procReferencePop;

	} else {
		procReferencePop = popUp_comp[that['id']];
	}

	return procReferencePop;
}

/**
 * @function
 * @summary Matrix 형태의 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createMatrixPop(setPopupProp) {
	var that = {};
	for ( var prop in setPopupProp) {
		that[prop] = setPopupProp[prop];
	}
	
	var procReferencePop = null;
	if ( popUp_comp[that['id']] == null ){
		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			html: that["matrixHtml"]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				procReferencePop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				setMatrixData();
				procReferencePop.close();	
			}
		});

		/** * Private Function Area Start ******************** */
		function setMatrixData(selData){
			var targetForm = Ext.getCmp(that['targetFormId']);
			switch(that['switchCommand']) {
				case "req_zone":
					if( popUp_comp[that['id']].custId && popUp_comp[that['id']].custNm ){
						targetForm.setFieldValue(that["reqZoneIdField"], popUp_comp[that['id']].custId);
						targetForm.setFieldValue(that["reqZoneNmField"], popUp_comp[that['id']].custNm);	
					}
				break;
			}
		}
		/** ********************* Private Function Area End ** */

		procReferencePop = Ext.create('nkia.custom.windowPop', {
			id : that['popupId'],
			title : that['popupTitle'],
			width : that['popupWidth'],
			autoDestroy : false,
			resizable : false,
			buttons : [ acceptBtn, closeBtn ],
			buttonAlign : 'center',
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : 'background-color: white;',
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = procReferencePop;

	} else {
		procReferencePop = popUp_comp[that['id']];
	}

	return procReferencePop;
}

/**
 * @function
 * @summary Combo & Tree 선택 팝업(공통)
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createComboTreeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var treeSelectPopBody = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {

		var popUpBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			id : that['treeId'] + "btn",
			ui : 'correct',
			scale : 'medium'
		});
		
		var comboHandler =  function(combo, selectedRecord, eOpts){
			var serviceId = selectedRecord[0].raw.SERVICE_ID;
			var codeText = selectedRecord[0].raw.CODE_TEXT;
			
			var treeGrid = staticTreeforPop.getStore();
			treeGrid.proxy.jsonData={service_id: serviceId, expandLevel:2};
			treeGrid.load();
		}
		
		var comboLoadHandler =  function(combo, selectedRecord, eOpts){
			var custId = selectedRecord[0].raw.CODE_ID;
			var serviceId = selectedRecord[0].raw.SERVICE_ID;
			var codeText = selectedRecord[0].raw.CODE_TEXT;
			
			var treeGrid = staticTreeforPop.getStore();
			
			if( that['params'] == null ){
				treeGrid.proxy.jsonData={service_id: serviceId, expandLevel:2};
			} else {
				if( that['params']['cust_id'] ){
					var custId = that['params']['cust_id'];
					var comboStore = multiCombo.getStore();
					var len = comboStore.getTotalCount();
					for( var i=0; i < len; i++ ){
						var comboData = comboStore.getAt(i);
						
						if( comboData.raw["CODE_ID"] == custId ){
							multiCombo.setValue( custId, false );
							serviceId = comboData.raw["SERVICE_ID"];
							break;
						}
					}
					
					treeGrid.proxy.jsonData={service_id: serviceId, expandLevel:2};
				}
			}
			
			treeGrid.load();
		}

		var multiCombo = createCodeComboBoxComp({name:'multiCombo', hideLabel: true, width: 130, url:'/itg/itam/amdb/searchCodeDataList.do'});	
		
		attachCustomEvent('select', multiCombo, comboHandler);
		attachCustomEvent('load', multiCombo.getStore(), comboLoadHandler);
		
		var multiTreeComboPanel = Ext.create('nkia.custom.Panel', {
	        id: 'multiTreeComboPanel',
	        border: false,
	        padding: '2 0 1 5',
			layout: 'hbox',
			items: [multiCombo]
		})

		var setTreeProp = {
			context : '${context}',
			id : that['treeId'],
			title : that['title'] ? that['title'] : '',
			url : that['url'],
			extraToolbar : multiTreeComboPanel,
			dynamicFlag : false,
			autoLoad: false,
			searchable : true,
			expandAllBtn : that['expandAllBtn'] ? that['expandAllBtn'] : true,
			collapseAllBtn : that['collapseAllBtn'] ? that['collapseAllBtn'] : true,
			params : that['params'] ? that['params'] : {},
			rnode_text : that['rnode_text'] ? that['rnode_text'] : '코드',
			expandLevel : that['expandLevel'] ? that['expandLevel'] : 1
		}

		var staticTreeforPop = createTreeComponent(setTreeProp);
		
		attachCustomEvent('load', staticTreeforPop, function(){
			staticTreeforPop.expandNode(staticTreeforPop.getRootNode());
		});
		attachCustomEvent('beforeitemdblclick', staticTreeforPop, dbSelectEvent);
		
		// btnHide 값을 true 로 주면 적용 버튼을 숨긴다.
		var btnControll = [popUpBtn];
		if(that['btnHide'] == true){	btnControll = [];	}

		treeSelectPopBody = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			height : 400,
			width : 300,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : btnControll,
			bodyStyle : 'background-color: white; ',
			items : [ staticTreeforPop ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUpBtn.on('click', dbSelectEvent);

		function dbSelectEvent() {
			// accept btnHide 
			if(that['btnHide'] == true){	return false;	}
			
			var node = Ext.getCmp(that['treeId']).view.selModel.getSelection();
			if (node.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00033'
				}));
			} else {	
				
				//@@정중훈20131105 시작
				//Root는 선택이 안되게 만듭니다.
				// true : 선택이 됨
				// false : 선택이 안됨
				if(false == that["rootSelect"]){
					var node_level = node[0].raw.node_level;
					if(1 == node_level){
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00044'
						}));
						return;
					}
				}
				//@@정중훈20131105 종료

				var node_leaf = node[0].raw.leaf;
				if (that['leafSelect'] == true) {
					if (node_leaf == false) {
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00032'
						}));
						Ext.getCmp(that['treeId']).expandAll(function() {
							Ext.getCmp(me.id).getEl().unmask();
							toolbar.enable();
						});
						return;
					}
				}
				node_id = node[0].raw.node_id;
				node_nm = node[0].raw.text;

				that['targetHiddenField'].setValue(node_id);
				that['targetField'].setValue(node_nm);

				treeSelectPopBody.close();
			}
		}

		popUp_comp[that['popId']] = treeSelectPopBody;

	} else {
		treeSelectPopBody = popUp_comp[that['popId']];
	}

	return treeSelectPopBody;
}

/**
 * @function
 * @summary 자산상세 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function showAssetDetailPop(param){
	//var url = "/itg/itam/opms/opmsDetailInfo.do" + param;
	var url = "/itg/itam/automation/goAssetDetailViewer.do" + param;
	window.open(getConstValue('CONTEXT') +url, "", "width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}
