var popUp_comp	= {};
var removeRow	= "";
var grd			= "";
var gridCount	= 0;
var assetSeq	= "";
var MSFlag		= "";
var assetId		= "";


var xAssetSeq	= 0;
var xMSFlag		= 1;
var xAssetName	= 2;
var xSerial		= 7;
var xIntrDate	= 9;	//SW인경우 +11

var xAssetId	= 37;
var xCarryId	= 38;
var xCarrySeq	= 39;

var xCounts		= 13;	//SW인경우 +10

var xTpIntrCost	= 14;	//SW인경우 +11
var xIntrCost	= 14;	//SW인경우 +11
var xWarranty	= 17;	//SW인경우 +11
var xMaintRate	= 18;	//SW인경우 +11
var xRegion		= 19;	//SW인경우 +11

//PO USER, VENDOR 검색콤보
function carryComboBox(id, codeType, label, name, po){
	var param				= {};
	param['code_type']		= codeType;
	param['code_group']		= name;
	param['PO_NO']			= po;

	var contract_proxy	= Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader	: "application/json",
		noCache				: false,
		type				: 'ajax',
		url					: '/itg/itam/newAsset/searchCodeDataList.do',
		jsonData			: param,
		actionMethods		: {
								create	: 'POST',
								read	: 'POST',
								update	: 'POST'
		},
		reader				: {
								type	: 'json',
								root	: 'gridVO.rows'
		}
	});
	
	var contractProp_val 				= {label: label, id: id, name: name, width: 250, attachAll:true, editable:true, editMode: true};
	contractProp_val['proxy']			= contract_proxy;
	contractProp_val['valueField']		= 'CODE_ID';
	contractProp_val['displayField']	= 'CODE_TEXT';
	    
	var contractComboBox 				=  createComboBoxComp(contractProp_val);
	   
	return contractComboBox;
}

function callVendorPop(cellNo, rowNo, straightId, seq){
	var grd				= jq('#straightList-body').find('.x-grid-row'); 
	var xgc				= ".x-grid-cell";
	var gci 			= ".x-grid-cell-inner";
	var preCellNo 		= parseInt(String(cellNo))-1;
	
	actionLinkEvent(  cellNo
			, rowNo
			, ''
			, grd.eq(rowNo).find(xgc).eq(preCellNo).find(gci)
			, grd.eq(rowNo).find(xgc).eq(cellNo).find(gci)
			, '제조사'
			, 'all'
			, straightId
			, seq
			);
}

function callUserPop(cellNo, rowNo, poNo, straightId, seq){
	var grd				= jq('#straightList-body').find('.x-grid-row'); 
	var xgc				= ".x-grid-cell";
	var gci 			= ".x-grid-cell-inner";
	var preCellNo 		= parseInt(String(cellNo))-1;

	var setPopupProp = {
			cellNo				: cellNo
			, rowNo				: rowNo
			, poNo				: poNo
			, userIdField		: grd.eq(rowNo).find(xgc).eq(preCellNo).find(gci)
			, userNmField		: grd.eq(rowNo).find(xgc).eq(cellNo).find(gci)
			, popUpTitle		: '운영 담당'
			, straightId 		: straightId
			, seq				: seq
			, targetFormId		: 'operUserList'	/* 숨겨져 있는 그리드 ID */
			, targetType 		: 'grid'			/* 현재 Text Box에 넣는 경우는 text, 그리드의 한 필드에 넣는 경우는 grid 로 표기. */
			, destinationId 	: 'straightList'		/* 선택된 사용자가 들어갈 리스트 IS */
			, filter			: 'OPER'
		};
	
	userLinkEvent(setPopupProp);
}

//Excel Upload 팝업생성
function createExcelUploadPopUp(setPopUpProp){
	garbageDelete();
	
	var paramMap = {};
	var that = this;
	var inputTablePop = null;
	var windowComp	= "";
	var carryGrid = "";
	var callinit   = 0;
	var classId		= class_id;
	var classType	= type;
	
	var xgci, xgc, carryId, assetName, carrySeq, serial, introDate;
	
	xgci = ".x-grid-cell-inner";
	xgc  = ".x-grid-cell";
	
	var exlMsg = "";

	//파일 삭제를 위한 부분 시작
	var poNo = "";
	var vdNo = "";
	//파일 삭제를 위한 부분 끝
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
//	var ctFlag = that['carryType'];	//CE:Capital Expenditure, GN:General
//	
//	if(ctFlag == "GS" || ctFlag == "CS"){
//		xCounts		= 23;
//		xTpIntrCost	= 24;
//		xIntrCost	= 24;
//		xIntrDate	= 26;
//		xWarranty	= 27;
//		xMaintRate	= 28;
//		xRegion		= 29;
//	}
	
	//하드웨어인지, 소프트웨어인지 구분
	var resPrefixByType	= "";

	resPrefixByType	= 'grid.itam.straightInput.'+type;
	
	//등록버튼
	var fileUploadBtn	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.file.upload' }), margin: '0 0 0 380', id: 'fileUploadBtn'});	//margin: getConstValue('WITH_BUTTON_MARGIN')
	
	//업무분류
	//var divWorkBtn		= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.itam.StraightInput.00001' }), id: 'divWorkBtn'});
	//모델
	//var modelBtn		= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.itam.StraightInput.00002' }), id: 'modelBtn'});
	//업무 분류와 모델도 일괄 수정에서 수정!
	//제조사 <- 모델을 선택한면 제조사는 자동!
	//var vendorBtn		= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.itam.StraightInput.00003' }), id: 'vendorBtn'});
	
	var fileAttachCmp = createAtchFileFieldComp({ 
				label: ''
	  			, id: 'atch_file_id'
	  			, name: 'atch_file_name'
	  			, width: 815
	  			, maxFileCount: 1
	  			, notNull: true
	  			, allowFileExt: 'xlsx'
	  			, fileHeight: 80
	});
	
	
	if(popUp_comp[that['id']] == null) {
		var setExcelInsertFormProp = {
				id: 'excelInsert_form'
				, name : 'excelInsert_form'
				, width : 835
				, editOnly		: true
				, border		: true
				, title			: getConstText({ isArgs: true, m_key: 'res.label.itam.excelUpload.00002'})
				, columnSize	: 1
				, tableProps	: [
								  	{	colspan:1
								  		, tdHeight: 105
								  		, item: fileAttachCmp	//fileAttachCmp, filePanelComponent
								  	}
								  	,{	colspan: 1
								  		, item: fileUploadBtn
								  	}
								  ]
//				, hiddenFields	: [{name:'search_po_no'}, {name:'search_vd_no'}]
		};

		var excelInsertPanel = createEditorFormComp(setExcelInsertFormProp);	//filePanelComponent

		var filePanelComponent;
		filePanelComponent = Ext.create('nkia.custom.Panel', {
			id : "fileCompPanel"
			, title : ''
			, height: 85
			, border: false
			, align: 'center'
			, layout: 'fit'
//			, icon: that['icon']
			, collapsible: that['collapsible'] ? that['collapsible'] : false
			, items : [excelInsertPanel]	//fileAttachCmp
//			, tools: [expandChartViewPopBtn]
		});
		
//		alert('class_id:' + class_id + '\ntype:' + type + '\n\nresPrefixByType:' + resPrefixByType);

		var getExcelUploadGridProp = {
								 id					: "straightList"
								,title				: "자산일괄등록"
								,gridHeight			: 480	//476
								,gridWidth			: 835
								,resource_prefix	: resPrefixByType
								,url				: '/itg/itam/newAsset/searchStraightInputList.do'
								,params				: null
//								,toolBarComp		: [divWorkBtn, modelBtn]
								,autoLoad			: false
								,pagingBar 			: false
//								,multiSelect		: true
//								,selModel			: true
								,border 			: false
								,selfScroll 		: true
								,cellEditing		: true
								,forceFit			: false
								,autoScroll 		: true
								,emptyMsg			: getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00010'})
							};
		
		carryGrid = createGridComp(getExcelUploadGridProp);

		
		var callback = function() {
			
			callinit		= 1;
			grd				= jq('#straightList-body').find('.x-grid-row'); 
			gridCount		= grd.length;
			assetSeq		= "";
			MSFlag			= "";
			
			var selectList = jq('#straightList-body').find('.x-grid-row');
			
//			for( var i=0; i < gridCount; i++ ){
//				paramMap[i] = selectList[i].raw;
				
				//var storeIndex = selectList[i].raw["RNUM"] - 1;
				//var param_store = grid.getStore().data.items[storeIndex].data;
//			}
			if(type == "SW"){
				xIntrDate	= xIntrDate-1;
			}
			
			
			var tempCarry = "";
			var q = 0;
			for(var s = 0 ; s <= (gridCount-1) ; s++){
				assetSeq	= grd.eq(s).find(xgc).eq(xAssetSeq).find(xgci).html();
				carryId		= grd.eq(s).find(xgc).eq(xCarryId).find(xgci).html();
				carrySeq	= grd.eq(s).find(xgc).eq(xCarrySeq).find(xgci).html();
				MSFlag		= grd.eq(s).find(xgc).eq(xMSFlag).find(xgci).html();
				assetName	= grd.eq(s).find(xgc).eq(xAssetName).find(xgci).html().replace(/&nbsp;/g,"");
				serial		= grd.eq(s).find(xgc).eq(xSerial).find(xgci).html().replace(/&nbsp;/g,"");
				introDate	= grd.eq(s).find(xgc).eq(xIntrDate).find(xgci).html().replace(/&nbsp;/g,"");
				
				assetId		= grd.eq(s).find(xgc).eq(xAssetId).find(xgci).html().replace(/&nbsp;/g,"");
				
				if(assetSeq != ""){
					
					if(MSFlag != "m" && MSFlag != "M" && MSFlag != "모" && MSFlag != "s" && MSFlag != "S" && MSFlag != "자"){
						grd.eq(s).find(xgc).find(xgci).addClass("alertRow");
						tempCarry = carryId;
					}else{
//						alert(xIntrDate+'['+introDate+']');
						if(assetId != ""){
							if(MSFlag == "자") {
								grd.eq(s).find(xgc).eq(xAssetId).find(xgci).html(assetId);
//								alert(s);
							}
						}
						if(MSFlag == "모" && assetId == "" ){
							var item = {
									"CLASS_ID"			: classId
									,"CLASS_TYPE"		: classType
									,"CARRY_ID"			: carryId
									,"CARRY_SEQ"		: carrySeq
									,"ASSET_NM"			: assetName
									,"INPUT_SOURCE"		: "Straight"	//Straight:일괄입력, Carry(빈값):반입->도입입력
//									,"SERIAL_NO"		: grd.eq(s).find(xgc).eq(xSerial).find(xgci).html().replace(/&nbsp;/g,"")
//									,"ASSET_AMOUNT"		: grd.eq(s).find(xgc).eq(8).find(xgci).html().replace(/&nbsp;/g,"")
									,"INTRO_DT"			: grd.eq(s).find(xgc).eq(xIntrDate).find(xgci).html().replace(/&nbsp;/g,"")
//									,"EQUIP_INST_DT"	: grd.eq(s).find(xgc).eq(10).find(xgci).html().replace(/&nbsp;/g,"")
//									,"GET_DT"			: grd.eq(s).find(xgc).eq(11).find(xgci).html().replace(/&nbsp;/g,"")
//									,"EOS_DT"			: grd.eq(s).find(xgc).eq(12).find(xgci).html().replace(/&nbsp;/g,"")
//									,"EOL_DT"			: grd.eq(s).find(xgc).eq(13).find(xgci).html().replace(/&nbsp;/g,"")
//									,"WARRANTY_LIFE"	: grd.eq(s).find(xgc).eq(14).find(xgci).html().replace(/&nbsp;/g,"")
//									,"PO_NO"			: grd.eq(s).find(xgc).eq(15).find(xgci).html().replace(/&nbsp;/g,"")
//									,"ERP_NO"			: grd.eq(s).find(xgc).eq(16).find(xgci).html().replace(/&nbsp;/g,"")
//									,"OLD_ERP_NO"		: grd.eq(s).find(xgc).eq(17).find(xgci).html().replace(/&nbsp;/g,"")
//									,"U_ID"				: grd.eq(s).find(xgc).eq(18).find(xgci).html().replace(/&nbsp;/g,"")
//									,"ASSET_DESC"		: grd.eq(s).find(xgc).eq(20).find(xgci).html().replace(/&nbsp;/g,"")
//									,"ASSET_STATE"		: grd.eq(s).find(xgc).eq(35).find(xgci).html().replace(/&nbsp;/g,"")
//									,"ASSET_TYPE"		: grd.eq(s).find(xgc).eq(36).find(xgci).html().replace(/&nbsp;/g,"")
//									,"ASSET_USE"		: grd.eq(s).find(xgc).eq(37).find(xgci).html().replace(/&nbsp;/g,"")
//									,"USE_TYPE"			: grd.eq(s).find(xgc).eq(38).find(xgci).html().replace(/&nbsp;/g,"")
//									,"REGION"			: grd.eq(s).find(xgc).eq(39).find(xgci).html().replace(/&nbsp;/g,"")
							};
//							alert(q + '.' + MSFlag + '\nCLASS_ID:' + classId + '\nCLASS_TYPE:' + classType + '\nCARRY_ID:' + carryId + '\nCARRY_SEQ:' + carrySeq + '\nASSET_NM:' + assetName);
							paramMap[q] = item;
							q += 1;
						}
					}

					if(MSFlag == ""){
						grd.eq(s).find(xgc).eq(xMSFlag).find(xgci).addClass("blankCell");
					}
					
					if(assetName == ""){
						//grd.eq(s).find(xgc).eq(xAssetName).find(xgci).attr("style","text-align: center; background-color:#FFCC33;");
						grd.eq(s).find(xgc).eq(xAssetName).find(xgci).addClass("blankCell");
					}else{
						if(assetName.indexOf("<span") != -1){
							exlMsg = "＊ 중복된 자산이 있습니다.";
						}
					}

					if(MSFlag == "모"){
						if(type != "SW"){
							if(serial == ""){
								grd.eq(s).find(xgc).eq(xSerial).find(xgci).addClass("blankCell");
							}
						}
						if(introDate == ""){
							grd.eq(s).find(xgc).eq(xMSFlag).find(xgci).addClass("blankCell");
						}
					}

				}

			}
//			alert('배열수:'+q);
			
			paramMap = JSON.parse(JSON.stringify(paramMap));

			if(exlMsg.indexOf("*") != -1 || exlMsg.indexOf("＊") != -1){
				if(callinit == 1){
					alert(exlMsg);
					callinit = 2;
				}
			}
			
		}
		
		carryGrid.store.on("load",callback);

		
		var tempOperUserGridProp = {
				  id				: "operUserList"
				, title				: '선택된 운영자 리스트'
				, gridHeight		: 0	//0
				, gridWidth			: 935
				, resource_prefix	: 'grid.popup.operUser'
				, url				: ''
				, params			: null
				, pagingBar 		: false
				, multiSelect		: true
				, selModel			: true
				, border 			: false
				, selfScroll 		: true
				, forceFit			: false
				, autoScroll 		: true
				, autoLoad			: false
				, emptyMsg			: getConstText({ isArgs: true, m_key: 'msg.itam.carry.00023'})
			};

		tempOperUserGrid = createGridComp(tempOperUserGridProp);

		
		//등록
		var fileUpload = function (){
			if(jQuery('#atch_file_name_grid-body').find(".x-grid-cell").length >= 2){
				alert('한번에 한개의 엑셀파일만 업로드 할 수 있습니다.');
			}else{
				if(jQuery('#atch_file_name_grid-body').find(".x-grid-cell-inner").text() != ""){
					var isFile = checkFileSave("atch_file_name", fileMap, callbackSub);
					if( !isFile ){
						if(fileMap["atch_file_name"] != ""){
							alert(fileMap["atch_file_name"]);
							eval(callbackSub)();
						}
					}
				}else{
					alert('파일을 첨부하여주세요');
				}
			}
			
		}
		
		attachBtnEvent(fileUploadBtn	,fileUpload);
		
		//setVisibleFileButton("atch_file_name", false);
		
		//initDefaultFileupload("atch_file_name");
		//setVisibleFileButton("atch_file_name", true);
		
		var callbackSub = function(){
			var fileId = fileMap["atch_file_name"];

			if(fileId != ""){
				jq.ajax({ type:"POST"
					, url: "/itg/itam/newAsset/selectCarryExcelFileName.do"
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson( { atch_file_id: fileId } )
					, success:function(data){
						if( data.success ){
							excelMap = data.resultMap;

							var excelFile 		= excelMap["SERVEREXCELFILENAME"];
							var excelType 		= excelMap["FILEEXTSN"];
							var StoreFileName 	= excelMap["STOREFILENAME"];
							var loadProgram 	= "";

							if (excelFile != "" && excelFile != null && excelFile.length != 0){
								if(type == "NW" || type == "NS"){
									loadProgram = "/itg/itam/newAsset/straightNetworkInput.do";
								}else if(type == "DC" || type == "MC"){
									loadProgram = "/itg/itam/newAsset/straightDCMCInput.do";
								}else{
									loadProgram = "/itg/itam/newAsset/straight"+type+"Input.do";
								}

								jq.ajax({ type:"POST"
									, url: loadProgram
									, contentType: "application/json"
									, dataType: "json"
									, async: false
									, data : getArrayToJson({fileName: excelFile, classType: type, classId: class_id, fileId: StoreFileName})
									, success:function(data){
										if( data.success ){
											if(data.resultMsg == null){
												exlMsg = '';
											}else{
												exlMsg = '* '+data.resultMsg+'\n엑셀파일을 확인 후 다시 등록하여주세요.';
											}
											
											callinit   = 0;
											carryGrid = getGrid("straightList").getStore();
											carryGrid.proxy.jsonData={CLASS_TYPE: type, CLASS_ID: class_id};
											carryGrid.load();
											
										}else{
											alert(data.resultMsg);
										}
									}
								});
								
//								alert(loadProgram);
							}
						}else{
							alert(data.resultMsg);
						}
					}
				});
			}
		}
		
		//저장 버튼
		var insertBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.insert' }),
			ui:'correct',
			handler: function() {
				//alert('적용버튼 클릭');
				if(exlMsg.indexOf("*") != -1){
					alert('엑셀양식을 수정하셔야 합니다.');
				}else{
					
					if(exlMsg.indexOf("＊") != -1){
						alert('중복된 자산을 제외하고 등록합니다.');
					}
					
//					jq.ajax({ type:"POST"
//						, url: "/itg/itam/newAsset/moveStraightToAsset.do"	//moveStraightToAsset.do
//						, contentType: "application/json"
//						, dataType: "json"
//						, async: false
//						, data : getArrayToJson({classType: type})
//						, success:function(data){
//							if( data.success ){

								jq.ajax({ type:"POST"
									, url: "/itg/itam/newAsset/insertAssetInfo.do"
									, contentType: "application/json"
									, dataType: "json"
									, async: true
									, data : getArrayToJson(paramMap) 
									, success:function(data){
										if( data.success ){
											alert('저장되었습니다.');
											actionLink('searchAttr');
										}else{
											alert('저장 실패하였습니다.');
										}
										
									}
								});
								windowComp.close();

//							}else{
//								//showMessageBox({msg : data.resultMsg ,type : "ERROR"});
//								alert(data.resultMsg);
//
//							}
//						}
//					});
				}
				
			}
		});
		
		//닫기버튼
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				windowComp.close();
			}
		});
		
		var fileMap = {};
		var excelMap = {};
		
		//뷰포트 생성: 바로 아래 POPUP생성에서 viewport를 사용할 경우
		var viewportProperty = {
				borderId	: 'popupBorderId'
				,width		: 838
				,viewportId : 'popupViewId' 
//				,north: {
//					maxHeight: 200
//					,items : [excelInsertPanel] 
//				}
				,center: { 
						items : [excelInsertPanel, carryGrid, tempOperUserGrid]	// 
				}
		}
		
		var popTitle			= getConstText({ isArgs: true, m_key: 'res.label.itam.excelUpload.'+type+'.00001'});
		
		var codePopupProp		= {
				id:   that['id']+type
				,title: popTitle
				,width: 847
				,height: 700
				,modal: true
				,layout: 'fit'
				,buttonAlign: 'center'
				,bodyStyle: 'background-color: white; '
				,buttons: [insertBtn, closeBtn]
				,items: createBorderViewPortComp(viewportProperty, { isLoading: false, isResize : false} )
				,resizable: true
				,resizeFnName: 'callResizePopupLayout'
		  }

		windowComp = createWindowComp(codePopupProp);
		windowComp.show();
		
	} else {
		inputTablePop = popUp_comp[that['id']];
		Ext.getCmp('excelInsert_form').initData();
	}
	
	jq("#zipButton").removeClass('x-btn-default-toolbar-small-disabled');
	jq("#zipButton").removeClass('x-btn-disabled');

	var wa = 0;
	var tridentVal = window.navigator.userAgent.search("Trident");
//	alert(window.navigator.userAgent.search("Trident"));
	if(tridentVal==59 || tridentVal==36){
		//alert('호환성보기 아님:'+window.navigator.userAgent.search("Trident"));	//59
		wa = -86;
	} else {
		//크롬에서는 기본 설정에서 -1 반환하여 이대로 사용하면됨.
		//alert('호환성보기임:'+window.navigator.userAgent.search("Trident"));		//58
		wa = getBrowserByWidth(-86);
	}
	
	
	var newHtml = '';
		newHtml += '<img src="/itg/sample/ui_template/images/icons/gray-component.gif" alt="양식다운로드" style="cursor:pointer;" id="btnDown" />';
		newHtml += '<font style="margin-bottom:5px; cursor:pointer;">양식다운로드</font>';
		newHtml += '';	//나중에 이미지 오면 대체. 아래 버튼은 어울리지 않는 것으로 판단됨.
		
	var newHtml = '<div class="x-btn x-box-item x-toolbar-item x-btn-default-small x-noicon x-btn-noicon x-btn-default-small-noicon" style="border-width: 1px; left: '+wa+'px; margin: 0px; top: -8px; width: 88px;" id="button-1999">';
		newHtml += '<em id="button-1999-btnWrap">';
		newHtml += '<button id="button-1999-btnEl" type="button" class="x-btn-center" hidefocus="true" role="button" autocomplete="off" style="height: 15px;">';
		newHtml += '<span id="button-1999-btnInnerEl" class="x-btn-inner" style="margin-top:-2px !important; padding-left:3px; color:#222 !important;">양식&nbsp;다운로드</span>';
		newHtml += '<span id="button-1999-btnIconEl" class="x-btn-icon "></span>';
		newHtml += '</button>';
		newHtml += '</em>';
		newHtml += '</div>';
		
	jq("#zipButton").html(newHtml);
	
	jq('#zipButton').click( function(e) {
		if(type == "SV"){
			downloadFile(1225, 0);
		}else if(type == "NW"){
			downloadFile(1226, 0);
		}else if(type == "NS"){
			downloadFile(1226, 0);
		}else if(type == "BK"){
			downloadFile(1227, 0);
		}else if(type == "SS"){
			downloadFile(1228, 0);
		}else if(type == "ST"){
			downloadFile(1229, 0);
		}else if(type == "PC"){
			downloadFile(1230, 0);
		}else if(type == "ME"){
			downloadFile(1231, 0);
		}else if(type == "AF"){
			downloadFile(1232, 0);
		}else if(type == "SW"){
			downloadFile(1233, 0);
		}else if(type == "DC"){
			downloadFile(1234, 0);
		}else if(type == "MC"){
			downloadFile(1235, 0);
		}
	});

	return inputTablePop;
}

function downloadFile(atchFileId, fileIdx){
	var downloader = Ext.getCmp("newAssetCarrayDownloadFrame");
	downloader.load({
		url: getConstValue('CONTEXT') + '/itg/base/downloadAtchFile.do?atch_file_id=' + atchFileId + '&file_idx=' + fileIdx
	});
	
}
//Layout Resise Event Handler	callResizeLayout,	callResizePopupLayout
function callResizePopupLayout(wValue, hValue){
	var gridPanel = Ext.getCmp("straightList");
	var fileProp = Ext.getCmp("excelInsert_form");	//excelInsert_form, excelInsert_formfieldset
	var fileComp = Ext.getCmp("atch_file_name");
	var reWidth = 0, reHeight = 210;
	
	var gridBaseHeight = gridPanel.getHeight();
	
	resizeBorderLayout("popupViewId", "popupBorderId", ["straightList"], "straightList");

	if(hValue == '700'){
		if(gridBaseHeight == '490'){
			gridPanel.setHeight(gridBaseHeight);
		}else{
			gridPanel.setHeight(gridBaseHeight+10);
		}
	}else{
		var reHValue = 0, reHValue = parseInt(hValue);
		var reReHeight = reHValue-reHeight;

		gridPanel.setHeight(reReHeight);
	}
	
	gridPanel.setWidth(wValue-12);
	
//	fileProp.setWidth(wValue-12);
//
//	reWidth = parseInt(wValue)-12;
//
//	fileProp.setWidth(reWidth);
//
//	if(!jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(0).hasClass("blackLabel")){
//		jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(0).attr("name","blackLabel");
//		jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(0).addClass("blackLabel");
//		jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(0).attr("style","border-width: 1px; width: "+(reWidth-22)+"px; left: 0px; top: 0px; color: black;");
//	}else{
//		jq(".blackLabel").attr("style", "border-width: 1px; width: "+(reWidth-22)+"px; left: 0px; top: 0px; color: black;")
//	}
//
//
//		
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(0).attr("style","border-width: 1px; width: "+(reWidth-22)+"px; left: 0px; top: 0px; color: black;");
//	
//	
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(1).attr("style","width: "+(reWidth-22)+"px; left: 0px; height: 25px; top: 26px; color: red;");
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(1).attr("name","redLabel");
//	
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(2).attr("style","width: "+(reWidth-22)+"px; left: 0px; top: 51px; color: green;");
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(2).attr("name","greenLabel");
//	
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(0).find("div").eq(0).find("div").eq(0).find("div").eq(0).attr("style","border-width: 1px; height: auto; left: 0px; margin: 0px; width: "+(reWidth-22)+"px; top: 0px;");
//
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(6).attr("style","width: "+(reWidth-22)+"px; left: 0px; height: 25px; top: 26px; color: gold;");
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(7).attr("style","overflow: auto; margin: 0px; width: "+(reWidth-22)+"px; height: 25px; color: orange;");
//	
//	jq("#atch_file_name_grid-body").find("div").eq(7).find("table").attr("style","width: "+(reWidth-22)+"px; color: blue;");
//	jq("#atch_file_name_grid-body").find("div").eq(7).find("th").attr("style","width: "+(reWidth-22)+"px; height: 0px; color: yellow;");
//	
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(8).attr("style","width: "+(reWidth-22)+"px; left: 0px; top: 51px;");
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(9).attr("style","width: "+(reWidth-24)+"px; height: 22px;");
//	jq("#atch_file_name_grid-body").find("div").eq(0).find("div").eq(10).attr("style","position: absolute; width: "+(reWidth-24)+"px; left: 0px; top: 0px; height: 1px;");
//
//	jq("#zipButton").attr("style", "border-width: 1px; left: "+(reWidth-40)+"px; top: 8px; margin: 0px;");
//
//	jq("#fileUploadBtn").attr("style","margin:0px 0px 0px "+((reWidth-80)/2)+"px;border-width:1px 1px 1px 1px;");
//	
//	jq("#atch_file_name_container").attr("style", "width: "+(reWidth-20)+"px; table-layout: fixed;");
//	jq("#atch_file_name_grid-body").find("div").eq(0).attr("style", "border-top-width: 1px; border-top-color: rgb(184, 200, 217); border-top-style: solid; border-left-width: 1px; border-left-color: rgb(184, 200, 217); border-left-style: solid; border-right-width: 1px; border-right-color: rgb(184, 200, 217); border-right-style: solid; padding: 0px; margin: 0px; height: 80px; width: "+(reWidth-20)+"px;");
//	jq("#atch_file_name_container-innerCt").attr("style", "width: "+(reWidth-20)+"px; height: 80px;");
//	jq("#atch_file_name_container-targetEl").attr("style", "position: absolute; width: "+(reWidth-20)+"px; left: 0px; top: 0px; height: 1px;");
//	jq("#atch_file_name_grid").attr("style", "width: "+(reWidth-20)+"px; left: 0px; margin: 0px; top: 0px; height: 80px;");
//	jq("#atch_file_name_grid-body").attr("style", "width: "+(reWidth-20)+"px; left: 0px; top: 0px; height: 80px;");
//	jq("#atch_file_name_grid_toolbar-targetEl").attr("style", "position: absolute; width: "+(reWidth-20)+"px; left: 0px; top: 0px; height: 1px;");

}

function getBrowserByWidth(ws) {
	var widthSize = 0;
	var agt = navigator.userAgent;
	if(agt.indexOf("Chrome") != -1) {
		widthSize = ws;
	} else if(agt.indexOf("MSIE 7") != -1) {
		widthSize = parseInt(ws)+42;
	} else if(agt.indexOf("MSIE 8") != -1) {
		widthSize = parseInt(ws)+42;
	} else if(agt.indexOf("MSIE 9") != -1) {
		widthSize = parseInt(ws)+42;
	} else if(agt.indexOf("MSIE 10") != -1) {
		widthSize = parseInt(ws)+42;
	} else if(agt.indexOf("MSIE 11") != -1) {
		widthSize = parseInt(ws)+42;
	} else {
		widthSize = parseInt(ws)+42;
	}
	//alert(agt + '\n' + heightSize);
	return widthSize;
}

function commify(n) {
	var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
	n += '';                          // 숫자를 문자열로 변환

	while (reg.test(n)){
		n = n.replace(reg, '$1' + ',' + '$2');
	}

	return n;
}

function garbageDelete(){
	var paramMap 			= {};
	
	jq.ajax({ type:"POST"
		, url: "/itg/itam/newAsset/deleteStraight.do"
		, contentType: "application/json"
		, dataType: "json"
		, async: false
		, data : getArrayToJson(paramMap)
		, success:function(data){
		}
	});
}