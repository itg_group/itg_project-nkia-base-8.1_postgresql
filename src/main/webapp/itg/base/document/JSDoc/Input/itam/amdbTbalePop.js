var popUp_comp={};


/**
 * @param {
 * 			colspan	: table의 colspan 갯수 Null일 경우 1
 * 			label	: 필드의 Label
 * 			name	: 필드의 파라메터명
 * 		} td_props
 * @return {}
 */
function createAmTableInfo(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var theForm = Ext.getCmp('editor_form');
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
				id: 'editor_form',
				title: getConstText({ isArgs: true, m_key: 'res.label.tableMng.00007' }),
				editOnly: true,
				border: true,
				columnSize: 1,
				//formBtns: [insertBtn],	//Form 상단에 들어가는 버튼을 세팅
				tableProps : [
				             //row1
							{colspan:1, tdHeight: 25, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00001' }), name:'gen_table_nm', notNull:true})}
							//row2
							,{colspan:1, tdHeight: 25, item : createCodeRadioComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00008' }), name:'use_yn', code_grp_id: 'YN', columns:2})}
							//row3
							,{colspan:1, tdHeight: 50, item : createTextAreaFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00009' }), name:'gen_table_desc', width: 500})}
				             ]
				
		}
		
		var eidtorFormPanel = createEditorFormComp(setEditorFormProp);
		
		var insertBtn = new Ext.button.Button({
	        text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
	        handler: function() {
	        	
	        	var use_yn = Ext.getCmp('editor_form').getInputData().use_yn;
	        	var gen_table_nm = Ext.getCmp('editor_form').getFieldValue('gen_table_nm');
	        	var gen_table_desc = Ext.getCmp('editor_form').getFieldValue('gen_table_desc');
	        	if(use_yn == "Y"){
	        		use_yn_kr = getConstText({ isArgs: true, m_key: 'res.common.selectbox.yes' });
	        	}else{
	        		use_yn_kr = getConstText({ isArgs: true, m_key: 'res.common.selectbox.no' });
	        	}
	        	
	        	if(gen_table_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00009' }));
	        		return;
	        	}else{
	        		
	        		getGrid("tblListGrid").getStore().add({"RNUM":'NEW',"GEN_TABLE_NM":gen_table_nm,"GEN_TABLE_DESC":gen_table_desc,"USE_YN":use_yn,"USE_YN_KR":use_yn_kr,"UPD_DT":'NEW'});
	    			//버튼처리
	        		var hideIds = ['mngUpdateBtn', 'mngDeleteBtn','mngInsPopBtn','mngUpdPopBtn'];	 
	    			hideBtns(hideIds);	//버튼을 숨긴다.
	    			var showIds = ['mngInsertBtn','mngCnacleBtn'];	
	    			showBtns(showIds);
	    			//관리항목 리스트 셋팅
	    			var notGrid = getGrid("notContainGrid").getStore();
	    			notGrid.proxy.jsonData={gen_table_nm: gen_table_nm};
	    			notGrid.load();
	    			
	    			var inGrid = getGrid("inContainGrid").getStore();
	    			inGrid.proxy.jsonData={gen_table_nm: gen_table_nm};
	    			inGrid.load();
	    			
	        		inputTablePop.hide();
	        	}
	        }
		});
		
		inputTablePop = new Ext.Window({

			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn],
			bodyStyle: that['bodyStyle'],
			items: [eidtorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = inputTablePop;
		
	} else {
		inputTablePop = popUp_comp[that['id']];
		theForm.initData();
		
	}
	
	return inputTablePop;
}

function updateAmTableInfo(setPopUpProp){
	var storeParam = {};
	var that = this;
	var updateTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var theForm = Ext.getCmp('update_form');
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
				id: 'update_form',
				title: getConstText({ isArgs: true, m_key: 'res.label.tableMng.00007' }),
				editOnly: true,
				border: true,
				columnSize: 1,
				//formBtns: [insertBtn],	//Form 상단에 들어가는 버튼을 세팅
				tableProps : [
				             //row1
							{colspan:1, tdHeight: 25, item : createDisplayTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00001' }), name:'gen_table_nm', notNull:true})}
							//row2
							,{colspan:1, tdHeight: 25, item : createCodeRadioComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00008' }), name:'use_yn', code_grp_id: 'YN', columns:2})}
							//row3
							,{colspan:1, tdHeight: 50, item : createTextAreaFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.tableMng.00009' }), name:'gen_table_desc', width: 500})}
				             ],
				hiddenFields:[ {name:'rnum'}]
				
		}
		
		var eidtorFormPanel = createEditorFormComp(setEditorFormProp);
		
		var seletedObject = Ext.getCmp('tblListGrid').view.selModel.getSelection();
		var updateForm = Ext.getCmp('update_form');
		
		var clickRecord = seletedObject[0].raw;
		updateForm.setDataForMap(clickRecord);
		
		var insertBtn = new Ext.button.Button({
	        text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
	        handler: function() {
	        	
	        	
	        	var use_yn = Ext.getCmp('update_form').getInputData().use_yn;
	        	var gen_table_nm = Ext.getCmp('update_form').getFieldValue('gen_table_nm');
	        	var gen_table_desc = Ext.getCmp('update_form').getFieldValue('gen_table_desc');
	        	if(use_yn == "Y"){
	        		use_yn_kr = getConstText({ isArgs: true, m_key: 'res.common.selectbox.yes' });
	        	}else{
	        		use_yn_kr = getConstText({ isArgs: true, m_key: 'res.common.selectbox.no' });
	        	}
	        	
	        	if(gen_table_nm == ""){
	        		showMessage(getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00009' }));
	        		return;
	        	}else{
	        		
	        		var seletedObject = Ext.getCmp('tblListGrid').view.selModel.getSelection();
	        		getGrid("tblListGrid").getStore().remove(seletedObject[0]);
	        		
	        		getGrid("tblListGrid").getStore().add({"RNUM":'UPDATE',"GEN_TABLE_NM":gen_table_nm,"GEN_TABLE_DESC":gen_table_desc,"USE_YN":use_yn,"USE_YN_KR":use_yn_kr,"UPD_DT":'NEW'});
	    			//버튼처리
	        		var hideIds = ['mngInsertBtn','mngUpdateBtn', 'mngDeleteBtn','mngInsPopBtn','mngUpdPopBtn'];	 
	    			hideBtns(hideIds);	//버튼을 숨긴다.
	    			var showIds = ['mngUpdateBtn','mngCnacleBtn'];	
	    			showBtns(showIds);
	    			//관리항목 리스트 셋팅
	    			var notGrid = getGrid("notContainGrid").getStore();
	    			notGrid.proxy.jsonData={gen_table_nm: gen_table_nm};
	    			notGrid.load();
	    			
	    			var inGrid = getGrid("inContainGrid").getStore();
	    			inGrid.proxy.jsonData={gen_table_nm: gen_table_nm};
	    			inGrid.load();
	    			
	        		updateTablePop.hide();
	        	}
	        }
		});
		
		updateTablePop = new Ext.Window({

			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn],
			bodyStyle: that['bodyStyle'],
			items: [eidtorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = updateTablePop;
		
	} else {
		updateTablePop = popUp_comp[that['id']];
		theForm.initData();
		
	}
	
	return updateTablePop;
}
