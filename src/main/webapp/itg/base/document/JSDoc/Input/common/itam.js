/**
 * @method {} getAssetSearchForm : 자산검색조건 Form
 * @param {} formId : formId
 * @param {} searchBtnId : searchBtnId
 * @param {} type 	: 검색조건 Type
 * @param {} columnSize : columnSize
 * @param {} callFuncKey : 검색버튼 Click시 호출되는 Key
 * @type Atype : 조회분류(Combo) + Search Keyword(Text)
 * @type Btype : 조회분류(Combo) + Search Keyword(Text)
 * 
 * @return {}
 */
function getAssetSearchForm(formId, searchBtnId, type, columnSize, callFuncKey){
	// 조회검색버튼
	var searchBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.search' }), id: searchBtnId, ui:'correct',"scale":"medium"});
	attachBtnEvent(searchBtn, callFuncKey);		

	var searchProp;
	switch(type){
		case 'Atype':
			searchProp = [
			  	{colspan:1, tdHeight: 28, item : createCodeComboBoxComp({label: getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_OPER_ASSET', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})},
			  	{colspan:1, tdHeight: 28, item : createTextFieldComp({name:'search_value', padding: getConstValue('WITH_FIELD_PADDING')})},
			  	{colspan:1, tdHeight: 28, item : createHiddenFieldComp({name:'class_id'})}
			]
			break;
		case 'Btype':
			searchProp = [
			  	{colspan:1, tdHeight: 28, item : createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.label.itam.common.00001' }), name:'asset_id'})},
			  	{colspan:1, tdHeight: 28, item : createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.label.itam.common.00002' }), name:'asset_name'})}
			]
			break;
		case 'searchHaGroup':
			searchProp = [
			  	{colspan:1, tdHeight: 28, item : createCodeComboBoxComp({label: getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_ha_type', code_grp_id:'SEARCH_HA_GROUP', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})},
			  	{colspan:1, tdHeight: 28, item : createTextFieldComp({name:'search_value', padding: getConstValue('WITH_FIELD_PADDING')})}
			]
			break;
		case 'searchMultiUser':
			searchProp = [
			  	{colspan:1, tdHeight: 28, item : createCodeComboBoxComp({label: getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', id: formId+"_combo", code_grp_id:'SEARCH_USER', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})},
			  	{colspan:1, tdHeight: 28, item : createTextFieldComp({hideLabel: true, name:'search_value', padding: getConstValue('WITH_FIELD_PADDING'), width: 250, label:getConstText({ isArgs: true, m_key: 'res.label.system.00015' })})}
			]
		case 'JJYtype':
			searchProp = [
			    {colspan:2, tdHeight: 28, item : createReqZonePopupComp({label:"사용부대", id:"use_cust_id", name:"use_cust_id", width:245, leafSelect:false})},
			  	{colspan:1, tdHeight: 28, item : createCodeComboBoxComp({label: getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_OPER_ASSET', width: 250})},
			  	{colspan:1, tdHeight: 28, item : createTextFieldComp({name:'search_value', padding: getConstValue('WITH_FIELD_PADDING')})},
			  	{colspan:1, tdHeight: 28, item : createHiddenFieldComp({name:'class_id'})}
			]
			break;
			
			//Ext.getCmp(formId+"_combo").select('user_nm');
		break;
	}
	
    var setSearchFormProp = {
		id: formId,
		formBtns: [searchBtn],
		columnSize: columnSize,
		tableProps : searchProp,
		enterFunction : callFuncKey
	}
	
	var searchAssetForm = createSeachFormComp(setSearchFormProp);
	
	return searchAssetForm;
}

// 
/**
 * 자산 목록 Grid + 체크박스(선택용)
 * @param {} treeId		 : Tree Id
 * @param {} expandLevel : 트리 확장 레벨
 * @return {}
 */
function getAssetClassAllTree(treeId, expandLevel, param){
	var assetClassTreeProp = {
		context: getConstValue('CONTEXT'),						
		id : treeId,
		url : getConstValue('CONTEXT') + '/itg/itam/amdb/searchAmClassAllTree.do',
		title : getConstText({ isArgs: true, m_key: 'res.title.itam.class' }),
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		collapseAllBtn : true,
		params: param,
		rnode_text : getConstText({ isArgs: true, m_key: 'res.00015' }),
		expandLevel: expandLevel
	}
	var assetClassTree = createTreeComponent(assetClassTreeProp);
	return assetClassTree;
}

/**
 * 자산 목록 Grid + 체크박스(선택용)
 * @param {} gridId
 * @param {} setExtProp : 확장파라메터
 * {
 * 		@property 1{} pageSize: 페이지 Size
 * 	} 
 *  
 * @return {}
 */
function getCheckAssetGrid(gridId, setExtProp){
	var that= {};
	for( var prop in setExtProp ){
		that[prop] = setExtProp[prop];
	}
	var checkAssetGridProp = {	
		context: getConstValue('CONTEXT'),		
		id: gridId,	  
		title : getConstText({ isArgs: true, m_key: 'res.title.itam.maint.assetlist' }),
		gridWidth: (that['gridWidth']) ? that['gridWidth'] : null,
		gridHeight: (that['gridHeight']) ? that['gridHeight'] : 250,
		resource_prefix: 'grid.itam.oper.asset',				
		url: getConstValue('CONTEXT') + '/itg/itam/oper/searchAssetList.do',			
		params: null,		
		selModel: true,
		pagingBar : true,  
		pageSize : 10,  
		autoLoad: false,
		dragPlug: false,
		border: true,
		pageSize : (that['pageSize']) ? that['pageSize']			: null,	
		emptyMsg: getConstText({ isArgs: true, m_key: 'MSG_NO_DATA' })
	}
	var checkAssetGrid = createGridComp(checkAssetGridProp);
	return checkAssetGrid;
}

/**
 * 자산 목록 Grid + 체크박스(선택용), Url, prefix가 가변적일 경우
 * @param {} gridId
 * @param {} url
 * @param {} resource_prefix
 * @param {} setExtProp : 확장파라메터
 * {
 * 		@property 1{} pageSize: 페이지 Size
 * 	} 
 * 
 * @return {}
 */
function getCheckAssetGridVariability(gridId, url, resource_prefix, setExpProps){
	var checkAssetGridProp = {	
		context: getConstValue('CONTEXT'),		
		id: gridId,	  
		title : getConstText({ isArgs: true, m_key: 'res.title.itam.maint.assetlist' }),
		gridHeight: (setExpProps["height"]) ? setExpProps["height"] : 200,
		gridWidth: (setExpProps["width"]) ? setExpProps["width"] : null,
		resource_prefix: resource_prefix,
		url: getConstValue('CONTEXT') + url,					
		params: null,		
		selModel: true,
		pagingBar : true,
		pageSize : 10,
		autoLoad: false,
		dragPlug: false,
		border: true,
		emptyMsg: getConstText({ isArgs: true, m_key: 'MSG_NO_DATA' })
	}
	var checkAssetGrid = createGridComp(checkAssetGridProp);
	
	return checkAssetGrid;
}

function getRadioCheckAssetGridVariability(gridId, url, resource_prefix, setExpProps){
	var checkAssetGridProp = {	
		context: getConstValue('CONTEXT'),		
		id: gridId,	  
		title : getConstText({ isArgs: true, m_key: 'res.title.itam.maint.assetlist' }),
		gridHeight: (setExpProps["height"]) ? setExpProps["height"] : 200,
		gridWidth: (setExpProps["width"]) ? setExpProps["width"] : null,
		resource_prefix: resource_prefix,
		url: getConstValue('CONTEXT') + url,					
		params: null,		
		selModel: false,
		pagingBar : true,  
		pageSize : 5,  
		autoLoad: false,
		dragPlug: false,
		border: true,
		emptyMsg: getConstText({ isArgs: true, m_key: 'MSG_NO_DATA' })
	}
	var checkAssetGrid = createGridComp(checkAssetGridProp);
	
	var radioColumn = Ext.create('Ext.grid.column.Column', {
		text: '선택',
		width: 50,
		align: 'center',
		hideable:true,
		renderer: function(){
			var radio = '<input type= "radio" name="radiogroup" style="margin-left:10px;"/>';
   			return radio; 
		}, 
		editor: { xtype:'radio' }, 
		width: 50, 
		align: 'center'
	});
	
	checkAssetGrid.headerCt.insert(0, radioColumn);
	checkAssetGrid.getView().refresh();
	
	checkAssetGrid.on('itemclick', function(view, rec, item, index, e, eOpt){
   		var nodeEl = view.getSelectedNodes()[0];
   		var radioEl = nodeEl.childNodes[0].childNodes[0].childNodes[0];
   		radioEl.checked = true;
  	});
	
	return checkAssetGrid;
}

/**
 * 선택자산목록에 사용되는 자산목록 Grid + Memory Proxy
 * @param {} gridId
 * @param {} setExtProp : 확장파라메터
 * {
 * 		@property 1{} pageSize: 페이지 Size
 * 	} 
 *  
 * @return {}
 */
function getAssetGridByMemory(gridId, setExtProp){
	//@@20130731 김도원 START
	//getGridByMemoryVariabilityProps()를 호출하여 가져오도록 수정
	var memoryAssetGridProp = getGridByMemoryVariabilityProps(gridId, 'grid.itam.oper.asset.sel', setExtProp);
	//@@20130731 김도원 END
	var memoryAssetGrid = createGridComp(memoryAssetGridProp);
	return memoryAssetGrid;
}
/**
 * 선택자산목록에 사용되는 자산목록 Grid + Memory Proxy   Resource_prefix 를 가변적으로 선언해줘야 하는경우 사용.
 * @param {} gridId
 * @return {}
 */
function getAssetGridByMemoryVariability(gridId, resource_prefix, propeties, title){
	//@@20130731 김도원 START
	//기존의 Props 설정이 재사용가능성이 높아 공통 쪽으로 옮김(nkia_grid.js)
	//getGridByMemoryVariabilityProps()를 호출하여 가져온다.
	var memoryAssetGridProp = getGridByMemoryVariabilityProps(gridId, resource_prefix, propeties, title);
	//@@20130731 김도원 END
	var memoryAssetGrid = createGridComp(memoryAssetGridProp);
	return memoryAssetGrid;
}

/**
 * 서비스 운영부서 Grid
 * @param {} gridId
 * @return {}
 */
function getServiceCustGrid(gridId){
	var setServiceCustGridProp = {	
		id: gridId,	
		title : '서비스 관련 부서선택',
		height: 300,
		resource_prefix: 'grid.itam.service.cust',				
		url: getConstValue('CONTEXT') + '/itg/itam/amdb/searchCustList.do',	
		params: null,								
		pagingBar : false,
		pageSize : 10,
		autoLoad: true,
		dragPlug: false,
		border: true,
		emptyMsg: getConstText({ isArgs: true, m_key: 'MSG_NO_DATA' })
	}
	var serviceCustGrid = createGridComp(setServiceCustGridProp);
	
	return serviceCustGrid;
}

// @@20130726 전민수 START
// 팝업내 사용할 경우 AUTOSCROLL 문제로 인하여 height 높이값을 파라미터로 받게 하였음
// height 값을 주지않을 경우는 수정전과 동일하게 사용 되어짐. 
/**
 * 서비스 Tree
 * @param {} treeId
 * @param {} expandLevel
 */
function getCheckServiceTree(treeId, expandLevel, param, height){
	// itam.js 참조 : ITAM 서비스 트리 조회 
	var setServiceTreeProp = {
		id : treeId,
		url : getConstValue('CONTEXT') + '/itg/itam/amdb/searchAmdbServiceCheckTree.do',
		title : getConstText({ isArgs: true, m_key: 'res.label.itam.service.00001' }),
		height: (height) ? height : null,
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		autoLoad: false,
		collapseAllBtn : true,
		params: (param) ? param : null,
		rnode_text : '#springMessage("res.00003")',
		expandLevel: expandLevel
	}
	var checkServiceTree = createTreeComponent(setServiceTreeProp);
	return checkServiceTree;
}
// @@20130726 전민수 END

//@@20130731 김도원 START
//getAssetGridByMemoryVariability와 비슷한 맥락으로 gridActionRemoveBtnEvent을 공통으로 옮김(nkia_grid.js)
//@@20130731 김도원 END