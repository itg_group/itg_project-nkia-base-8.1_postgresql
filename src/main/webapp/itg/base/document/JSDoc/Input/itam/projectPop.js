var popUp_comp={};
var removeRow="";
//INFRA 데이터추가 팝업생성
function createSubPopUp(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var theForm = Ext.getCmp('subInsert_form');
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
				id: 'subInsert_form',
				title: getConstText({ isArgs: true, m_key: 'res.title.itam.project.tab2'}),
				editOnly: true,
				border: true,
				columnSize: 2,
				//formBtns: [insertBtn],	//Form 상단에 들어가는 버튼을 세팅
				tableProps : [
					            //row1
					            //{colspan:2, tdHeight: 30, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.project.00001' }), name:'project_id', readOnly:true})}
								{colspan:2, tdHeight: 30, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.project.00011' }), name:'sub_project_nm', notNull:true, width:600, maxLength:200})}
								//row2				           
								,{colspan:2, tdHeight: 30, item : createTextAreaFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.project.00012' }), name:'sub_project_content', width: 600, maxLength:2000})}
								//row3
					            ,{colspan:1, tdHeight: 30, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.project.00013' }), name:'cust_nm', readOnly:true, width: 250})}
					            ,{colspan:1, tdHeight: 30, item : createOutUserSelectPopComp({id:'contractUserSubPop', width: 220, popUpText: getConstText({ isArgs: true, m_key: 'res.label.itam.project.00014' }), label: getConstText({ isArgs: true, m_key: 'res.label.itam.project.00014' }), name:'user_nm', hiddenName:'sub_user_id', targetFormId:'subInsert_form', custNm:'cust_nm', gridId:'contractUserSubGrid', notNull:true})}
					            //row4
					            ,{colspan:1, tdHeight: 30, item : createDateFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.project.00015' }), name:'sub_start_dt', dateType:'D'})}
					            ,{colspan:1, tdHeight: 30, item : createDateFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.project.00016' }), name:'sub_end_dt', dateType:'D'})}
					            ,{colspan:1, tdHeight: 30, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.project.00020' }), name:'sub_project_state', code_grp_id:'PROJECT_STATE', width: 250})}
				             ],hiddenFields:[{name:'rnum'}]
		}
		
		var editorFormPanel = createEditorFormComp(setEditorFormProp);
		
		formEditable({id : 'subInsert_form' , editorFlag : true , excepArray :['project_id','cust_nm','user_nm']});
		
		var insertBtn = new Ext.button.Button({
			ui:'correct',		
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
	        handler: function() {
	        	var theForm = Ext.getCmp('subInsert_form');
				if(theForm.isValid()){	    	        	
					var checkFlag = subVaildCheck();
					if(checkFlag == "N"){
						return;
					}
		        	var project_id = Ext.getCmp('subInsert_form').getInputData().project_id;
		        	var sub_project_nm = Ext.getCmp('subInsert_form').getFieldValue('sub_project_nm');
		        	var sub_project_content = Ext.getCmp('subInsert_form').getFieldValue('sub_project_content');
		        	var sub_start_dt = Ext.getCmp('subInsert_form').getInputData().sub_start_dt;
		        	var sub_end_dt = Ext.getCmp('subInsert_form').getInputData().sub_end_dt;
		        	var sub_start_dt_day = Ext.getCmp('subInsert_form').getInputData().sub_start_dt;
		        	var sub_end_dt_day = Ext.getCmp('subInsert_form').getInputData().sub_end_dt;
		        	var sub_project_state = Ext.getCmp('subInsert_form').getFieldValue('sub_project_state');
		        	var cust_nm = Ext.getCmp('subInsert_form').getFieldValue('cust_nm');
		        	var user_nm = Ext.getCmp('subInsert_form').getFieldValue('user_nm');
		        	var sub_user_id = Ext.getCmp('subInsert_form').getFieldValue('sub_user_id');
		        	var sub_cust_id = Ext.getCmp('subInsert_form').getInputData().sub_cust_id;
		        	var rnum = Ext.getCmp('subInsert_form').getInputData().rnum;	      
		        	if(rnum == null || rnum == ""){
		        		var rnum = Ext.getCmp('projectSubGrid').getStore().data.length + 1;
			     		getGrid("projectSubGrid").getStore().add({"RNUM":rnum,"PROJECT_ID":project_id,"SUB_PROJECT_NM":sub_project_nm,"SUB_PROJECT_CONTENT":sub_project_content,"SUB_START_DT":sub_start_dt,"SUB_END_DT":sub_end_dt,"SUB_PROJECT_STATE":sub_project_state,"CUST_NM":cust_nm,"USER_NM":user_nm,"SUB_USER_ID":sub_user_id,"SUB_CUST_ID":sub_cust_id,"SUB_START_DT_DAY":sub_start_dt_day,"SUB_END_DT_DAY":sub_end_dt_day});
		        	}else{
		        		getGrid('projectSubGrid').getStore().removeAt(removeRow);
			     		getGrid("projectSubGrid").getStore().insert(removeRow,{"RNUM":rnum,"PROJECT_ID":project_id,"SUB_PROJECT_NM":sub_project_nm,"SUB_PROJECT_CONTENT":sub_project_content,"SUB_START_DT":sub_start_dt,"SUB_END_DT":sub_end_dt,"SUB_PROJECT_STATE":sub_project_state,"CUST_NM":cust_nm,"USER_NM":user_nm,"SUB_USER_ID":sub_user_id,"SUB_CUST_ID":sub_cust_id,"SUB_START_DT_DAY":sub_start_dt_day,"SUB_END_DT_DAY":sub_end_dt_day});
		        	}
		     		inputTablePop.close();
				}else{
					showMessage(getConstText({isArgs: true, m_key: 'msg.common.00012'}));
				}		     		
	        }
		});
		
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				inputTablePop.close();
			}
		});

		
		inputTablePop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['popUpText'],  
			width: 660,  
			autoDestroy: false,
			resizable:false, 
			//bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn, closeBtn],
			bodyStyle: that['bodyStyle'],
			items: [editorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = inputTablePop;
		
	} else {
		inputTablePop = popUp_comp[that['id']];
		theForm.initData();
	}
	
	return inputTablePop;
}


function subCheck(record, rowIndex) {
	var clickRecord = record.raw;
	removeRow = rowIndex;
}



function subVaildCheck(){
	
	var theForm = Ext.getCmp('subInsert_form');
	var sub_start_dt = theForm.getFieldValue('sub_start_dt_day');
	var sub_end_dt = theForm.getFieldValue('sub_end_dt_day');
	
	var checkFlag = "Y";
	if(sub_start_dt > sub_end_dt){
		showMessage(getConstText({isArgs: true, m_key: 'msg.itam.project.00002'}));
		checkFlag = "N";
	}
	return checkFlag;
	
}

//,{colspan:1, item : createBasePopComp({id:'basePop_1', popUpText: '팝업텍스트', label:'라벨', name:'pgm_nm', hiddenName:'pgm_cd', pop_type: 'BZ'})}
function createBasePopComp(compProperty) {
	var that = {};
	for( var prop in compProperty ){
		that[prop] = compProperty[prop];
	}
	
	var labelWidth = getConstValue('LABEL_WIDTH');
	var fieldWidth = getConstValue('DEFAULT_FIELD_WIDTH') - 55;
	if(that['width'] != null) {
		fieldWidth = that['width'];
	}
	
	var nameField = createTextFieldComp({
			label: that['label'],
			name:that['name'],
			notNull:( that['notNull'] ) ? that['notNull'] : false,
			width: fieldWidth,
			readOnly: true});
	
	var idField =  Ext.create('Ext.form.field.Hidden', {
		xtype : 'hidden',
		name : that['hiddenName']
	});
	
	var t_comp = null;
	t_comp = {
		xtype: 'fieldcontainer',
		height: ( that['height'] ) ? that['height'] : "",
		width: getConstValue('DEFAULT_FIELD_WIDTH'),
		combineErrors: true,
		layout:'hbox',
		items: [
			nameField, idField,
			createPopBtnComp({
				handler: function(){
					var setPopUpProp = {	
						id: that['id'],
						title : that['title'],
						popUpText: that['popUpText'],
						tagetIdField: idField,
						tagetNameField: nameField,
						height: 504,								
						width: 590,
						buttonAlign : 'center',
						bodyStyle: 'background-color: white;',
						pop_type: that['pop_type']
					}
					var popUp = createBasePop(setPopUpProp);
					popUp.show();
				}
			}),
			createClearBtnComp({
            	handler: function(){
					nameField.setValue('');
					idField.setValue('');
				}
			})
        ]
	} 
	return t_comp;
}

function createBasePop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var selectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	if (popUp_comp[that['id']] == null) {
	
		//조회버튼
		var searchBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			})
			, ui : 'correct'
			, handler : function() {
				popSearchBtnEvent();
			}
		});
		
		//초기화버튼
		var initBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.reset'
			})
			, handler : function() {
				Ext.getCmp(that['id'] + '__searchForm').initData();
			}
		});
		
		function popSearchBtnEvent(){
			var searchForm = Ext.getCmp(that['id'] + '__searchForm');
			var paramMap = searchForm.getInputData();
			paramMap['pop_type'] = that['pop_type'];
			Ext.getCmp(that['id'] + '__Grid').searchList(paramMap);
		}
		
		var setSearchFormProp = {
				id: that['id'] + '__searchForm',
				target: grid,
				enterFunction: popSearchBtnEvent,
				formBtns: [searchBtn, initBtn],			
				columnSize: 1,
				tableProps : [{colspan:1, item : createTextFieldComp({label:'사업명', name:'data', width: 350})}
				             ]
		}
		var searchFormPanel = createSeachFormComp(setSearchFormProp);
		
		var setGridProp = {
			context : getConstValue('CONTEXT'),
			id : that['id'] + '__Grid',
			title : that['title'],
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.requirement.bzPop',
			url : getConstValue('CONTEXT') + '/itg/itam/requirementIns/searchPopData.do',
			params : {pop_type: that['pop_type']},
			pagingBar : true,
			pageSize : 20
		};
		var grid = createGridComp(setGridProp);

		var tagetIdField = that['tagetIdField'];
		var tagetNameField = that['tagetNameField'];
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				selectPop.close();
			}
		});
		
		//적용버튼
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
			, ui : 'correct'
			, handler : function() {
				dbSelectEvent();
			}
		});

		/** * Private Function Area Start ******************** */

		function dbSelectEvent() {
			var selectedRecords = grid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage("#springMessage('msg.common.00002')");
			} else {

				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var data_id = selectedRecords[0].raw.ID;
					var data_nm = selectedRecords[0].raw.DATA;
					tagetIdField.setValue(data_id);
					tagetNameField.setValue(data_nm);
					selectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', grid, dbSelectEvent);

		selectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ searchFormPanel, grid ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = selectPop;

	} else {
		selectPop = popUp_comp[that['id']];
	}

	return selectPop;
}

var searchGrid;
function createSelectDrimsPop(setPopUpProp){
	var storeParam = {};
	var that = this;
	var setDrimsPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		
		searchGrid = that['id']+'Grid';
		
		// 조회 그리드
		var setSearchGridProp = {	
				id: that['id']+'Grid',
				title : 'DRIMS 연계 데이터',
				gridHeight: 210,
				resource_prefix: that['sPrefix'],				
				url: that['url'],
				params: ( that['params'] ) ? that['params'] : null,
				selModel: true,
				pagingBar : true,
				pageSize : 10,
				autoLoad: true
		};
		var drimsSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
				id: that['tGridId'],
				title : '선택된 DRIMS 연계 데이터',
				gridHeight: 187,
				resource_prefix: that['tPrefix'],				
				params: null,		
				pagingBar : false,
				autoLoad: false,
				dragPlug: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
				actionBtnEvent: actionBtnEvent,
				isMemoryStore: true
		};
		var drimsSaveListGrid = createGridComp(setSaveGridProp);

		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.choice' }),
			handler: function() {
				checkGridRowCopyToGrid(that['id']+'Grid', that['tGridId'], { primaryKeys: ['IB_BIZID'] });
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var operDrimsSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				var searchForm = Ext.getCmp('drimsPopSearchForm');
				var paramMap = searchForm.getInputData();
				Ext.getCmp(that['id']+'Grid').searchList(paramMap);	
			}
		});
		
		var operDrimsSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp('drimsPopSearchForm').initData();
			}
		});
		
		var setDrimsSearchFormProp = {
			id: 'drimsPopSearchForm',
			formBtns: [operDrimsSearchBtn, operDrimsSearchInitBtn],
			columnSize: 3,
			enterFunction : clickPopSearchBtn,
			tableProps : [{colspan:1, item : createTextFieldComp({label:'사업ID', name:'ib_bizid', width: 275, maxLength:200})}
						 ,{colspan:1, item : createTextFieldComp({label:'사업명', name:'ib_bizcnm', width: 275, maxLength:200})}
						 ,{colspan:1, item : contractComboBox('project_year', "사업년도", 'search_project_yyyy')}]
		}
		var searchDrimsForm = createSeachFormComp(setDrimsSearchFormProp);

		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var formPanel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			flex:1 ,
			items: [searchDrimsForm, drimsSearchListGrid, choiceBtnpanel, drimsSaveListGrid]
		});
		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			ui:'correct',
			handler: function() {
				
				if(!confirm(getConstText({ isArgs: true, m_key: 'msg.common.confirm.00001' }))){
					return false;
				}
				
				setViewPortMaskTrue();
				if(getGrid(that['tGridId']).getStore().data.items.length <1){
					alert('선택된 데이터가 없습니다.');
					setViewPortMaskFalse();
				}else{

					var paramMap = {};
					var items = getGrid(that['tGridId']).getStore().data.items;
					for(var i=0; i < items.length; i++){
						paramMap[i] = items[i].data.IB_BIZID;
					}
					
					jq.ajax({ type:"POST", url: '/itg/itam/project/insertDrimsProjectData.do', 
						contentType: "application/json", dataType: "json", async: false , data : getArrayToJson(paramMap), 
						success:function(data){
							if(data.success){
								showMessage(data.resultMsg);
				    			setViewPortMaskFalse();
				    			setDrimsPop.close();
				    			
				    			var searchForm = Ext.getCmp('projectSearchForm');
				    			var paramMap = searchForm.getInputData();
				    			Ext.getCmp('project_grid').searchList(paramMap);
				    			
							}else{
								alert("getConstText({ isArgs: true, m_key: 'msg.common.00007' })");
								setViewPortMaskFalse();
							}
						}
					});					
				}
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				setDrimsPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		
		function actionBtnEvent(grid, rowIndex, colIndex){
			grid.getStore().removeAt(rowIndex); 
		};
		
		
		function clickPopSearchBtn() {
			var searchForm = Ext.getCmp('drimsPopSearchForm');
			var paramMap = searchForm.getInputData();
			Ext.getCmp(searchGrid).searchList(paramMap);	
		}
		
		/*********************** Private Function Area End ***/
		
		setDrimsPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [formPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setDrimsPop;
		
	} else {
		setDrimsPop = popUp_comp[that['id']];
	}
	
	return setDrimsPop;

}