var popUp_comp={};

/**
 * 맵이들어있는 배열형태에서 맵속성들을 하나의 통합맵객체로 합치는 기능
 *  
 * @method {} createOperAssetPop : 자산수정기능 팝업 
 * @param {} setPopUpProp   
 * @property 1{*} popupId	    : 팝업 ID 	
 *    	
 */
function mapAddAction(mapArray){
	var totalMap = {};
	var singleMap;
	
	for(var i=0; i<mapArray.length; i++){
		singleMap = mapArray[i];
	
		for( var prop in singleMap ){
			totalMap[prop] = singleMap[prop];
		}
	}
	return totalMap;
}

/**
 *  자산등록기능 팝업 
 *  
 * @method {} createNewAssetPop : 자산등록기능 팝업 
 * @param {} setPopUpProp   
 * @property 1{*} popupId	    : 팝업 ID 	
 *    	
 */
function createNewAssetPop(setPopUpProp){
	
	var that = {};
	var operAssetPopBody = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
			
	if(popUp_comp[that['popId']] == null) {
		var assetId = that['assetId'];
		var confId = that['confId'];
		
		var assetNote = createBasicAssetTab(assetId, that['class_type'], confId, true, 'NEW');
		
		var basicInfoUpdateBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.insert' }), ui:'correct', scale:'medium'});
		attachBtnEvent(basicInfoUpdateBtn, basicActionLink,  'basicInfoUpdateBtn');
		
		var operAssetPopBody = Ext.create('nkia.custom.windowPop',{
			id: that['popId'],
			title: getConstText({ isArgs: true, m_key: 'res.label.itam.oper.00058' }),  
			height: 618, 
			width: 900,  
			autoDestroy: true,
			resizable:false, 
			layout: 'fit',
			buttonAlign: 'center',
			buttons: [basicInfoUpdateBtn],
			bodyStyle: 'background-color: white; ',
			items: [assetNote],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['popId']] = null
	 			},
	 			hide:function(p) {
	 				
	 			}
	 		}
		});
		
		popUp_comp[that['popId']] = operAssetPopBody;
		
		/**
		 * 자산의 기본정보부분(일반정보, 스펙정보, 유지보수정보, 이력정보, 첨부파일정보) 버튼 이벤트 정의
		 */
		function basicActionLink(command) {
			var theForm = Ext.getCmp('setGeneralInfoProp');
			var theSpecForm = Ext.getCmp('setSpecInfoProp');
			
			switch(command) {
				case 'basicInfoUpdateBtn' :
					if(theForm.isValid() == true && theSpecForm.isValid() == true){
						
						var idMap = createAssetConfId(that['class_type']);
						var url;
						
						if(assetId == null || assetId == ""){
							assetId = idMap.assetId;
							url = '/itg/itam/newAsset/insertAssetBasicInfo.do';
						}else{
							url = '/itg/itam/oper/updateAssetBasicInfo.do';
						}
						
						if(confId == null || confId == ""){
							confId = idMap.confId;
						}
						
						var amInfraTotalMap = [];
						
						amInfraTotalMap.push(Ext.getCmp('setGeneralInfoProp').getInputData());
						amInfraTotalMap.push(Ext.getCmp('setSpecInfoProp').getInputData());
						amInfraTotalMap.push(Ext.getCmp('supplyAssetInfoForm').getInputData());
						amInfraTotalMap.push(Ext.getCmp('setAssetInfoProp').getInputData());
						amInfraTotalMap.push(Ext.getCmp('setMaintInfoProp').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwBasicForm').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwHostIpForm').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwPowerForm').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwDiscardForm').getInputData());
						var infraTotalMap = mapAddAction(amInfraTotalMap);
						
						//일반정보에 분류체계, 분류타입, 자산ID, 구성ID 저장
						infraTotalMap['class_id'] = that['class_id'];
						infraTotalMap['class_type'] = that['class_type'];
						infraTotalMap['asset_id'] = assetId;
						infraTotalMap['conf_id'] = confId;
						
						//@@20130719 전민수 START
						//1.첨부파일기능 신규추가
						var fileForm = Ext.getCmp('ci_atch_file_id_grid');
						//@@20130719 전민수 END
												
						//@@20130719 전민수 START
						//2.첨부파일기능 신규추가
						var fileMap = fileForm.getInputData();
						//@@20130719 전민수 END

						var operUserMap = dynamicGridUpdateEvent('createOperUserGrid', 'conf_id', confId, 'operUserStore', 'asset_id', assetId);
				    	var cpuInfoMap = dynamicGridUpdateEvent('operAssetCpuProp', 'conf_id', confId, 'cpuStore', 'asset_id', assetId);
						var memInfoMap = dynamicGridUpdateEvent('operAssetMemProp', 'conf_id', confId, 'memStore', 'asset_id', assetId);
						var ipInfoMap = dynamicGridUpdateEvent('operAssetIpProp', 'conf_id', confId, 'ipStore', 'asset_id', assetId);
						var innerDiskMap = dynamicGridUpdateEvent('operAssetInnerDiskProp', 'conf_id', confId, 'innerDiskGridStore', 'asset_id', assetId);

						// @@20130726 전민수 START
						// 5.서비스연계에 대한 로직이 없었으므로 신규 추가함
						var relServiceAsset = dynamicRelGridEditEvent('operAssetServicePropGrid','conf_id', that['confId'], 'serviceRelStore','/itg/itam/oper/insertserviceRelInfo.do', 'SE');
						// @@20130726 전민수 END

						var relSvAsset = dynamicRelGridEditEvent('operAssetSvProp','rel_conf_id', confId, 'svStore', '/itg/itam/oper/insertRelInfo.do', 'SV');
						var relNwAsset = dynamicRelGridEditEvent('operAssetNwProp','rel_conf_id', confId, 'nwStore', '/itg/itam/oper/insertRelInfo.do', 'NW');
						var relScAsset = dynamicRelGridEditEvent('operAssetScProp','rel_conf_id', confId, 'scStore', '/itg/itam/oper/insertRelInfo.do', 'SC');
						var relStAsset = dynamicRelGridEditEvent('operAssetStProp','rel_conf_id', confId, 'stStore', '/itg/itam/oper/insertRelInfo.do', 'ST');
						var relBkAsset = dynamicRelGridEditEvent('operAssetBackUpProp', 'rel_conf_id', confId, 'bkStore', '/itg/itam/oper/insertRelInfo.do', 'BK');
						var relSwAsset = dynamicRelGridEditEvent('operAssetSoftWareStProp','rel_conf_id', confId, 'swStore','/itg/itam/oper/insertRelInfo.do', 'SW');
						var relHaAsset = dynamicRelGridEditEvent('operAssetHAProp','conf_id', confId, 'haStore','/itg/itam/oper/insertHaInfo.do', 'HA');

						var UpdateDataMap = {};
						UpdateDataMap['infraTotalMap'] = infraTotalMap;
						
						//@@20130719 전민수 START
						//3.첨부파일기능 신규추가
						UpdateDataMap['fileMap'] = fileMap;
						
						//@@20130719 전민수 END
						UpdateDataMap['operUserMap'] = operUserMap;
						UpdateDataMap['cpuMap'] = cpuInfoMap;
						UpdateDataMap['memMap'] = memInfoMap;
						UpdateDataMap['ipMap'] = ipInfoMap;
						UpdateDataMap['innerDiskMap'] = innerDiskMap;
						// @@20130726 전민수 START
						// 5.서비스연계에 대한 로직이 없었으므로 신규 추가함
						UpdateDataMap['relServiceAssetMap'] = relServiceAsset;
						// @@20130726 전민수 END
						UpdateDataMap['relSvAssetMap'] = relSvAsset;
						UpdateDataMap['relNwAssetMap'] = relNwAsset;
						UpdateDataMap['relScAssetMap'] = relScAsset;
						UpdateDataMap['relStAssetMap'] = relStAsset;
						UpdateDataMap['relBkAssetMap'] = relBkAsset;
						UpdateDataMap['relSwAssetMap'] = relSwAsset;
						UpdateDataMap['relHaAssetMap'] = relHaAsset;
						
						//@@20130719 전민수 START
						//4.첨부파일기능 신규추가로인한 선처리로 checkFileSave내부함수 호출과 후처리 콜백함수 호출
						var callbackSub = function(){ 
							jq.ajax({ type:"POST"
								, url: url
								, contentType: "application/json"
								, dataType: "json"
								, async: false
								, data : getArrayToJson(UpdateDataMap)
								, success:function(data){
									if( data.success ){
										getGrid('newAssetList').searchList();
										operAssetPopBody.close();
					    				showMessage(data.resultMsg);
					    			}else{
					    				showMessage(data.resultMsg);
					    			}
								}
							});
						}
						
						var isFile = checkFileSave("ci_atch_file_id", fileMap, callbackSub);
						if( !isFile ){
							eval(callbackSub)();			
						}
						//@@20130719 전민수 END
					}else{
						showMessage(getConstText({ isArgs: true, m_key: 'msg.common.00011' }));
						return false;
					}
				break;
			}
		}
		
	} else {
		operAssetPopBody = popUp_comp[that['popId']];
	}
	
	return operAssetPopBody;
}

/**
 * 자산ID, 구성ID로 생성(SEQ생성)
 */
function createAssetConfId(classType){
	var resultValues;
	
	jq.ajax({ type:"POST"
			, url: "/itg/itam/newAsset/createAssetConfId.do"
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson({class_type : classType})
			, success:function(data){
				if( data.success ){
					resultValues = data.resultMap;
				}else{
				    showMessage(data.resultMsg);
				}
			}
	});
	
	return resultValues;
}

/**
 * 공통 연계정보들 수정후 테이블의 DISPLAY 필드 데이터 변경 
 */
function dynamicRelTableChange(assetId, confId){
	var count = getRelCount(assetId, confId);
	
	var dynamicField = Ext.getCmp('relAssetTableForm').query('.field');
	
	for(var i=0; i<dynamicField.length; i++){
		if(dynamicField[i].assetType != undefined){
			if(dynamicField[i].assetType == 'SV'){
				dynamicField[i].setValue(count[0]);
			}else if(dynamicField[i].assetType == 'NW'){
				dynamicField[i].setValue(count[1]);
			}else if(dynamicField[i].assetType == 'SC'){
				dynamicField[i].setValue(count[2]);
			}else if(dynamicField[i].assetType == 'ST'){
				dynamicField[i].setValue(count[4]);
			}else if(dynamicField[i].assetType == 'BK'){
				dynamicField[i].setValue(count[5]);
			}else if(dynamicField[i].assetType == 'SW'){
				dynamicField[i].setValue(count[6]);
			}else if(dynamicField[i].assetType == 'HA'){
				dynamicField[i].setValue(count[7]);
			}
		}
	}
}