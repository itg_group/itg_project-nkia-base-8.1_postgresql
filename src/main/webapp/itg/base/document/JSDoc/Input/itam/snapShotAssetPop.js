/**
 *  자산수정기능 팝업 
 *  
 * @method {} createOperAssetPop : 자산수정기능 팝업 
 * @param {} setPopUpProp   
 * @property 1{*} popupId	    : 팝업 ID 	
 *    	
 */
function createSnapShotAssetPop(setPopUpProp){
	
	var that = {};
	var operAssetPopBody = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['popId']] == null) {
		var assetNote = createBasicHistAssetTab(that['assetId'], that['assetType'], that['confId'], that['seq']);
				
		var operAssetPopBody = Ext.create('nkia.custom.windowPop',{
			id: that['popId'],
			title: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00038' }),  
			height: 600, 
			width: 900,  
			autoDestroy: true,
			resizable:false, 
			layout: 'fit',
			buttonAlign: 'center',
			bodyStyle: 'background-color: white; ',
			items: [assetNote],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['popId']] = null
	 			},
	 			hide:function(p) {
	 				popUp_comp[that['popId']] = null
	 				operAssetPopBody.close();
	 			}
	 		}
		});
		
		popUp_comp[that['popId']] = operAssetPopBody;
		
	} else {
		operAssetPopBody = popUp_comp[that['popId']];
	}
	
	return operAssetPopBody;
}