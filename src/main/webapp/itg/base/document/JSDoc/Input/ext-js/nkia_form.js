/**
 * 
 * @file nkia_form
 * @description 폼관련 함수
 * @author ITG
 * @copyright NKIA 2013
 */

/**
 * Form Items Resource
 * @param {} resourcePrefix
 * @return {}
 */
function getFormMsgSource(context, resourcePrefix){
	// 내부Property 바인딩
	var that = this;
	
	jq.ajax({ type:"POST"
		, url: context + "/itg/base/getFormMsgSource.do"
		, contentType: "application/json"
		, dataType: "json"
		, async: false
		, data : getArrayToJson({ resource_prefix : resourcePrefix  })
		, success:function(data){
			that['data'] = data;
		}
	});
	return that['data'];
}

/**
 * createColumnForm
 * @param {} setFormProp
 * @return {}
 */
function createColumnForm(setFormProp){
	// 내부Property 바인딩
	var that = this;
	for( var prop in setFormProp ){
		that[prop] = setFormProp[prop];
	}
	// ColumnItems 생성
	var columnItems = createColumnItems(setFormProp);
	
	that['form_object'] = Ext.create('Ext.form.Panel', {
		id: that["form_id"],
	    border: false,
	    bodyBorder: false,
	    fieldDefaults: ( that['fieldDefaults'] ) ? that['fieldDefaults'] : { labelAlign: 'right', msgTarget: 'side' },
	    items: [{
	        xtype: 'fieldset',
	        title: that['fieldset_title'],
	        autoHeight: true,
	        collapsed: ( typeof that['fieldset_collapsed'] != "undefined" ) ? that['fieldset_collapsed'] : false,
	        collapsible: ( typeof that['fieldset_collapsible'] != "undefined" ) ? that['fieldset_collapsible'] : true,
	        items: [{
                layout: 'column',
                border: false,
                items: 
                	columnItems
			}]
	    }],
	    
	    dockedItems: ( that['dockedItems'] ) ? that['dockedItems'] : null
	});
	return that['form_object'];
}

/**
 * createColumnItems
 * @param {} setFormProp
 * @return {}
 */
function createColumnItems(setFormProp){
	var formFieldsProp = setFormProp['formFieldsProp'];
	var columnCount = setFormProp['columnCount'];
	var columnWidthP = setFormProp['columnWidthP'];
		
	// Form Items 속성
	var formId = formFieldsProp["id"].split(",");
	var formLabel = formFieldsProp["label"].split(",");
	var formXtype = formFieldsProp["xtype"].split(",");
	var formAllowBlank = ( formFieldsProp["allowBlank"] ) ? formFieldsProp["allowBlank"].split(",") : "";
	var formEmptyText = ( formFieldsProp["emptyText"] ) ? formFieldsProp["emptyText"].split(",") : ""; 
	
	// 최대 items count (verical)
	var maxItemsCount = Math.ceil(formId.length / columnCount);
	
	var formArray = new Array(columnCount);	// 1차원 배열선언
	for( var i = 0; i < formArray.length; i++ ){
		formArray[i] = new Array(maxItemsCount);	// 2차원 배열선언
	}
	
	for( var i = 0; i < formId.length; i++ ){
		var itemsArray = {};
		itemsArray["name"] = formId[i];					// field name
		itemsArray["fieldLabel"] = formLabel[i];		// field Label
		itemsArray["xtype"] = formXtype[i];				// field xtype
		if( formAllowBlank.length > 0 ){				// field allowBlank
			itemsArray["allowBlank"] = compareValueToBoolean(formAllowBlank[i]);
		}
		if( formEmptyText.length > 0 ){					// field emptyText
			if( isNa(formEmptyText[i]) ){ itemsArray["emptyText"] = formEmptyText[i]; }
		}
		var firstLoc = i%columnCount;					// 2차원 배열의 첫번째 위치
		var secondLoc = Math.floor(i/columnCount);		// 2차원 배열의 두번째 위치				
		formArray[firstLoc][secondLoc] = itemsArray;
	}
	
	// 컬럼 Items Field 생성
	var columnItems = new Array();
	for( var i = 0; i < formArray.length; i++){
		var columnProp = {};
		columnProp['columnWidth'] = "."+columnWidthP[i];
		columnProp['border'] = false;
		columnProp['layout'] = 'form';
		var formItems = new Array();
		for( var j = 0; j < maxItemsCount; j++){
			formItems[j] = formArray[i][j];
		}  
		columnProp['items'] = formItems;
		columnItems[i] = columnProp; 
	}
	
	return columnItems;
}

function getForm(form_id) {
	return Ext.getCmp(form_id);
}