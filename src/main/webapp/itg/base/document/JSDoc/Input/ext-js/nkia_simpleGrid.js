/**
 * 
 * @file nkia_simpleGrid
 * @description NKIA Simple Grid(??) 컴포넌트
 * @author ITG
 * @copyright NKIA 2013
 */

/**
 * SimpleGrid 생성
 * 
 * @param compProperty
 * @returns {object} grid
 */
function createSimpleGrid( compProperty ){
	var grid;
	
	var that = {
		resource_url: getConstValue('CONTEXT') + "/itg/base/getGridMsgSource.do"
	};
	
	Ext.apply(that, compProperty);
	
	var searchData = function( url, param, func ){
		var r_value;
		
		jq.ajax({ type:"POST"
			, url: url
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(param)
			, success:function(data){
				if( func == null ){
					r_value = data;
				}else{
					r_value = func(data);
				}
			}
		});
		
		return r_value;
	}
	
	var data = searchData( that["resource_url"], { resource_prefix: that["resource_prefix"] } );
	
	var gridText = '';
	var gridHeader = '';
	var gridWidth = '';
	var gridFlex = '';
	var gridAlign = '';
	var gridSortable = '';
	var gridXtype = '';
	var gridTpl = '';
	var gridFormat = '';
	var gridHidden = '';
	var summaryheader = '';
	var summarycoltype = '';
	var summarycolspan = '';
	
	if(data.gridText != null) {
		gridText = data.gridText.split(",");
		gridHeader = data.gridHeader.split(",");
		gridWidth = data.gridWidth.split(",");
		gridAlign = data.gridAlign.split(",");
		gridFlex = ( data.gridFlex ) ? data.gridFlex.split(",") : "";
		gridSortable = ( data.gridSortable ) ? data.gridSortable.split(",") : "";
		gridXtype = ( data.gridXtype ) ? data.gridXtype.split(",") : "";
		gridTpl = ( data.gridTpl ) ? data.gridTpl.split(",") : "";
		gridFormat = ( data.gridFormat ) ? data.gridFormat.split(",") : "";
		summaryheader = ( data.summaryheader ) ? data.summaryheader.split(",") : "";
		summarycoltype = ( data.summarycoltype ) ? data.summarycoltype.split(",") : "";
		summarycolspan = ( data.summarycolspan ) ? data.summarycolspan.split(",") : "";
	}
	
	var gridColumns = [];
	for(var i=0 ; i < gridHeader.length ; i++) {
		var header = {};
		
		header["id"] = "id_" + i;
		header["tag"] = gridXtype[i];
		header["dataIndex"] = gridHeader[i];
		header["text"] = gridText[i];
		header["renderTpl"] = "<td>{text}</td>";
		header["width"] = gridWidth[i];
		header["spanColumn"] = summarycolspan[i];
		
		gridColumns[i] = header;
		//gridColumns[i] = Ext.create('Ext.Component', header);
	}
	
	//Proxy 객체를 생성함
	var id = that['id'];
	
	var grid_proxy = createGridProxyComp(id, that['url'], that['params']);	
	
	var grid_store = Ext.create('Ext.data.Store', {
		id: id + '_store',
		fields: gridHeader,
		remoteSort:  ( typeof that['remoteSort'] != "undefined" ) ? that['remoteSort'] : true,
		proxy: grid_proxy,
		pageSize: ( that['pageSize'] ) ? that['pageSize'] : getConstValue('DEFAULT_PAGE_SIZE'),
		autoLoad: ( typeof that['autoLoad'] != "undefined" ) ? that['autoLoad'] : true
	});
	
	var typeCode = {};
	var gridRows = new Array();
	grid_store.on("load", function(){
		var len = grid_store.getTotalCount();
		gridRows.length = 0;
		
		var mergeTemp = {};
		var sumData = {};
		for( var i=0; i < len; i++ ){
			var storeData = grid_store.getAt(i).raw;
			var codeGrpId = storeData.TYPE_CODE;
			var checkType = storeData.CHECK_TYPE;
			
			// 공통코드 콤보 생성용
			if( typeCode[codeGrpId] == null ){
				var codeCombo = searchData( '/itg/base/searchCodeDataList.do', {code_grp_id: codeGrpId} ).gridVO.rows;
				typeCode[codeGrpId] = codeCombo;
				
			}
			
			var row = new Array();
			for( var j=0; j < gridColumns.length; j++ ){
				var dataCol = {};
				var dataIndex = gridColumns[j]["dataIndex"];
				var dataValue = storeData[dataIndex] == "null" ? "" : storeData[dataIndex];
				var dataTag = gridColumns[j]["tag"];
				var editable = that["editable"];
				
				if( dataTag == "active" ){
					dataTag = checkType.toLowerCase();
				}
				
				switch( dataTag ){
					case "textarea":
						dataCol["text"] = Ext.String.format(
								'<textarea name="{0}" rows="5" cols="20" maxLength="500" onblur="f_chk_byte(this,500)" class="x-form-field x-form-text" style="width:100%;" autocomplete="off" aria-invalid="false" data-errorqtip="" {2}>{1}</textarea>',
								dataIndex,
								dataValue != null? dataValue : "",
								editable == false ? "readonly" : ""
						);
					break;
					case "combo":
						var codeCombo = typeCode[codeGrpId];
						var options = "";
						
						for( var k=0; k < codeCombo.length; k++ ){
							if( codeCombo[k].CODE_ID == dataValue ){
								options += "<option value='" + codeCombo[k].CODE_ID + "'  selected>" + codeCombo[k].CODE_TEXT + "</option>";
							}else{
								options += "<option value='" + codeCombo[k].CODE_ID + "'  >" + codeCombo[k].CODE_TEXT + "</option>";
							}
						}
						
						dataCol["text"] = Ext.String.format(
								'<select name="{0}" class="x-form-field x-form-text" value="{2}" {3}>{1}</select>',
								dataIndex,
								options,
								dataValue,
								editable == false ? "disabled" : ""
						);
					break;
					case "text":
						dataCol["text"] = Ext.String.format(
							'<input name="{0}" type="text" class="x-form-field x-form-text" {2}>{1}</input>',
							dataIndex,
							dataValue != null? dataValue : "",
							editable == false ? "readonly" : ""
						);
					break;
					default:
						dataCol["text"] = dataValue;
				}
				
				// 기본 데이터 셋팅
				dataCol["value"] = dataValue;
				
				// 셀 병합 Start
				dataCol["colspan"] = 1;
				dataCol["rowspan"] = 1;
				if( gridColumns[j]["spanColumn"] != "na" ){ // 병합용 셀 설정
					var spanColumn = gridColumns[j]["spanColumn"];
					if( mergeTemp[dataIndex] == null ){
						mergeTemp[dataIndex] = dataCol;
					}else{
						if( dataCol["value"] == mergeTemp[dataIndex]["value"] ){
							mergeTemp[dataIndex]["rowspan"] = mergeTemp[dataIndex]["rowspan"] + 1;
							continue;
						}else{
							mergeTemp[dataIndex] = dataCol;
						}
					}
				}
				// 조건
				
				// 셀 병합 End
				
				row.push(dataCol);
				
				if( that["summeryRow"] == true ){
					var numValue = Number(dataValue);
					// 합계 초기화
					if( sumData[dataIndex] == null && numValue > -1 ){
						sumData[dataIndex] = 0;
					}
					
					// 숫자형일때만 합계
					if( numValue > -1 ){
						sumData[dataIndex] += numValue;
					}
				}
			}
			
			gridRows.push(row);
		}
		
		// 총 합계 Row 입력
		if( that["summeryRow"] == true ){
			var row = new Array();
			for( var j=0; j < gridColumns.length; j++ ){
				var sumCol = {};
				sumCol["text"] = "";
				sumCol["colspan"] = 1;
				sumCol["rowspan"] = 1;
				
				if( j == 0 ){
					sumCol["text"] = "합계";
				}
				
				if( sumData[gridColumns[j]["dataIndex"]] ){
					sumCol["text"] = sumData[gridColumns[j]["dataIndex"]];
				}
				row.push( sumCol );
			}
			gridRows.push(row);
		}
		
		if( grid.rendered ){
			var gridHTML = grid.tableTpl.applyTemplate({
				headers: gridColumns,
				rows: gridRows
			});
			
			grid.body.dom.innerHTML = gridHTML;
		}
	});
	
	grid = Ext.create("nkia.custom.SimpleGrid", {
		id: that["id"],
		title: that["title"],
		formBtns: that['formBtns'],
		headers: gridColumns,
		rows: gridRows,
		store: grid_store,
		items: [],
		listeners: {
			afterrender: function(){
				var gridHTML = grid.tableTpl.applyTemplate({
					headers: gridColumns,
					rows: gridRows
				});
				
				grid.body.dom.innerHTML = gridHTML;
			}
		}
	});
	
	if( that["autoLoad"] ){
		grid_store.load();
	}
	
	return grid;
}

Ext.define('nkia.custom.HTMLTag', {
	extend: 'Ext.Component',
	childEls:[
		'el'
	],
	renderTpl: [
		'<td id="{id}-el">',
			'{text}',
		'</td>'
	],
	initComponent: function(){
		var me = this;
		
		me.callParent();
	}
});

Ext.define('nkia.custom.SimpleGrid', {
	extend: 'nkia.custom.Panel',
	headers: [],
	rows: [],
	overflowX: "auto",
	overflowY: "auto",
  	tableTpl: new Ext.XTemplate(
  		'<table id="{id}-table" class="x-grid-table x-grid-easy-table<tpl if="bodyCls"> {bodyCls}</tpl>" style="width: 100%;">',
  			'<tbody>',
  				'<tr>',
		  			'<tpl for="headers">',
		  				'<td class="x-grid-header-ct x-column-header x-column-easy-header" style="width: {width}; height: 30px; background-image: none; background-color: transparent;"><div class="x-column-header-inner" style="text-align: center;">{text}</div></th>',
		  			'</tpl>',
	  			'</tr>',
  				'<tpl for="rows">',
	  				'<tr class="x-grid-row x-grid-with-row-lines x-grid-row-alt">',
		  				'<tpl for=".">',
		  						'<td class="x-grid-cell x-grid-easy-cell x-grid-cell-inner" colspan="{colspan}" rowspan="{rowspan}" style="height: 25px;"><div style="text-align: center;">{text}</div></td>',
		  				'</tpl>',
	  				'</tr>',
	  			'</tpl>',
  			'</tbody>',
  		'</table>'
  	),
	initComponent: function(){
		var me = this;
		
		me.callParent(arguments); // 필수 수행 명령
	},
	getInputData: function(){
		var me = this,
			i, storeData,
			fields = {},
			returnData = [];
		
		for( i=0; i < me.headers.length; i++ ){
			var dataIndex = me.headers[i].dataIndex;
			
			fields[dataIndex] = me.getEl().query("[name=" + dataIndex + "]");
			
		}
		
		storeData = me.store;
		for( i=0; i < storeData.getTotalCount(); i++ ){
			var storeItem = storeData.getAt(i).raw;
			
			for( var p in fields ){
				if( fields[p].length > 0 ){
					var fieldValue = fields[p][i].value;
					if( fieldValue == null || fieldValue == "" ){
						storeItem[p] = "&nbsp;";
					} else {
						storeItem[p] = fieldValue;
					}
				}
			}
			
			returnData[i] = storeItem;
		}
		
		return returnData;
	},
	listeners: {
		afterrender: function(){
		}
	}
});

/*


function createSimpleGrid( compProperty ){
	var grid;
	
	var that = {
		resource_url: getConstValue('CONTEXT') + "/itg/base/getGridMsgSource.do"
	};
	
	Ext.apply(that, compProperty);
	
	var searchData = function( url, param, func ){
		var r_value;
		
		jq.ajax({ type:"POST"
			, url: url
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(param)
			, success:function(data){
				if( func == null ){
					r_value = data;
				}else{
					r_value = func(data);
				}
			}
		});
		
		return r_value;
	}
	
	var data = searchData( that["resource_url"], { resource_prefix: that["resource_prefix"] } );
	
	var gridText = '';
	var gridHeader = '';
	var gridWidth = '';
	var gridFlex = '';
	var gridAlign = '';
	var gridSortable = '';
	var gridXtype = '';
	var gridTpl = '';
	var gridFormat = '';
	var gridHidden = '';
	
	if(data.gridText != null) {
		gridText = data.gridText.split(",");
		gridHeader = data.gridHeader.split(",");
		gridWidth = data.gridWidth.split(",");
		gridAlign = data.gridAlign.split(",");
		gridFlex = ( data.gridFlex ) ? data.gridFlex.split(",") : "";
		gridSortable = ( data.gridSortable ) ? data.gridSortable.split(",") : "";
		gridXtype = ( data.gridXtype ) ? data.gridXtype.split(",") : "";
		gridTpl = ( data.gridTpl ) ? data.gridTpl.split(",") : "";
		gridFormat = ( data.gridFormat ) ? data.gridFormat.split(",") : "";					
	}
	//var gridColumns = new Array(gridHeader.length);
	var gridColumns = [];
	
	for(var i=0 ; i < gridHeader.length ; i++) {
		var header = {};
		
		header["tag"] = "th";
		header["text"] = gridText[i];
		//header["html"] = gridText[i];
		
		//gridColumns.push( Ext.create('nkia.custom.HTMLTag', header) );
		//var headerCmp = ( Ext.create('Ext.Component', header) );
		var headerCmp = ( Ext.create('nkia.custom.HTMLTag', header) );
		
		gridColumns[i] = headerCmp;
	}
	
	//Proxy 객체를 생성함
	var id = that['id'];
	
	var grid_proxy = createGridProxyComp(id, that['url'], that['params']);	
	
	var grid_store = Ext.create('Ext.data.Store', {
		id: id + '_store',
		fields: gridHeader,
		remoteSort:  ( typeof that['remoteSort'] != "undefined" ) ? that['remoteSort'] : true,
		proxy: grid_proxy,
		pageSize: ( that['pageSize'] ) ? that['pageSize'] : getConstValue('DEFAULT_PAGE_SIZE'),
		autoLoad: ( typeof that['autoLoad'] != "undefined" ) ? that['autoLoad'] : true
	});
	
	grid_store.on("load", function(){
		var len = grid_store.getTotalCount();
		
		for( var i=0; i < len; i++ ){
			var storeData = grid_store.getAt(i);
		}
	});
	
	grid = Ext.create("nkia.custom.SimpleGrid", {
		id: that["id"],
		title: that["title"],
		headers: gridColumns,
		store: grid_store,
		items: gridColumns,
		listeners: {
			afterrender: function(){
//				var gridHTML = grid.tableTpl.applyTemplate({
//					headers: gridColumns
//				});
//				
//				grid.body.dom.innerHTML = gridHTML;
			}
		}
	});
	
	grid_store.load();
	
	return grid;
}

Ext.define('nkia.custom.HTMLTag', {
	extend: 'Ext.Component',
	childEls:[
		//'El'
	],
	renderTpl: [
		'<td>',
			'{text}',
		'</td>'
	],
	initComponent: function(){
		var me = this;
		
		me.callParent(arguments); // 필수 수행 명령
	}
});

Ext.define('nkia.custom.SimpleGrid', {
	extend: 'nkia.custom.Panel',
	childEls: ["body"],
	renderTpl: [
		'<table id="{id}-body" class="x-grid-table x-grid-easy-table<tpl if="bodyCls"> {bodyCls}</tpl>" style="width: 100%; height: 100%;">',
			'<tbody>',
				'<tr>',
					'<tpl for="headers">',
						'<th class="x-column-header-ct x-column-header x-column-easy-header" style="width: {width}px; height: 0px;"><div class="x-column-header-inner" style="text-align: center;">{text}</div></th>',
					'</tpl>',
				'</tr>',
				'<tr>',
					'{%this.renderContainer(out,values);%}',
				'</tr>',
			'</tbody>',
		'</table>'
	],
//  	tableTpl: new Ext.XTemplate(
//  		'<table id="{id}-table" class="x-grid-table x-grid-easy-table<tpl if="bodyCls"> {bodyCls}</tpl>" style="width: 100%; height: 100%;">',
//  			'<tbody>',
//  				'<tr>',
//		  			'<tpl for="headers">',
//		  				'<th class="x-column-header-ct x-column-header x-column-easy-header" style="width: {width}px; height: 0px;"><div class="x-column-header-inner" style="text-align: center;">{text}</div></th>',
//		  			'</tpl>',
//	  			'</tr>',
//  				'<tr>',
//	  				'<tpl for="rows">',
//		  				'<tr>',
//		  						'{text}',
//		  				'</tr>',
//		  			'</tpl>',
//  				'</tr>',
//  			'</tbody>',
//  		'</table>'
//  	),
	initComponent: function(){
		var me = this;
		
		me.callParent(arguments); // 필수 수행 명령
	},
	listeners: {
		afterrender: function(){
		}
	}
});




Ext.define('nkia.custom.SimpleGrid', {
	extend: 'nkia.custom.Panel',
	childEls:[
  		'body', 'head', 'row'
  	],
  	renderTpl: [
  		'<table id="{id}-body" class="{baseCls}-body<tpl if="bodyCls"> {bodyCls}</tpl>">',
	  		'<thead>',
  				'<tr id="{id}-head">',
	  			'</tr>',
  			'</thead>',
  			'<tbody id="{id}-row">',
  			'</tbody>',
  		'</table>'
  	],
	initComponent: function(){
		var me = this;
		
		me.items = [];
		
		me.callParent(arguments); // 필수 수행 명령
	},
	listeners: {
		afterrender: function(){
		}
	}
});













Ext.define('nkia.custom.MergeGrid', {
	extend: 'nkia.custom.Panel',
	resource_url: '/itg/slms/slo/getGridMsgSource.do',
	resource_prefix: '',
	scroll: true,
	//autoLoad: true, // Error is UrlAppend Method
	layout: 'vbox',
	headerHeight: 30,
	minHeight: 400,
	//height: getConstValue('MIDDLE_GRID_HEIGHT'),
	initComponent: function(){
		var me = this;
		
		me.items = me.load();
		
		me.dockedItems = [].concat(me.dockedItems || []); // 필수 수행 명령
		
		me.callParent(arguments); // 필수 수행 명령
	},
	reConfig: function(){
		
	},
	reload:function(){
		//var tableData = (searchData( me.url, me.params )).gridVO;
		this.removeAll();
		
		var items = this.load();
		
		this.add(items);
	},
	load: function(){
		var me = this;
		
		// Private Function
		var createItems = function( headerProp, tableData ){
			var items = [];
			
			var headers = createTableHeaders(headerProp);
			var columns = createTableColumns(headers, tableData.rows);
			
			// hide Column Header
			var temp = [];
			var index = 0;
			var i=0;
			for( ; i < headers.length; i++ ){
				if( headers[i]['item_xtype'] == "0" || headers[i]['show'] == "0" ){
					continue;
				}
				
				temp[index] = headers[i];
				
				index = index + 1;
			}
			
			var showHeaders = temp;
			
			var scrollWidth = 21;
			var scrollHeight = 26;
			
			items = [{
				id: me.id + '_headers',
				layout: { type: 'hbox' },
				//height: me.headerHeight,
				style: {
					backgroundColor: '#FFFFFF'
				},
				items: showHeaders
			}, {
				id: me.id + '_columns',
				layout: {
					type: 'table',
					columns: showHeaders.length,
					trAttrs: {class: 'x-grid-row'},
					tdAttrs: {style: 'border-bottom-width: 1px;backgroundColor: #FFFFFF'},
					tableCls: 'x-grid-table',
					cellCls: 'x-grid-with-row-lines x-grid-cell'
				},
				style: {
					backgroundColor: '#FFFFFF'
				},
				autoScroll: me.scroll,
				height: 400,
				items: columns,
				listeners: {
					afterrender: function(){
					}
				}
			}];
			
			//me.items = items;
			return items;
		}
		
		var createTableHeaders = function( headerProp, sIndex, eIndex ){
			var headers = [];
			
			var gridText = headerProp.gridText.split(",");
			var gridHeader = headerProp.gridHeader.split(",");
			var gridWidth = headerProp.gridWidth.split(",");
			var gridXtype = ( headerProp.gridXtype ) ? headerProp.gridXtype.split(",") : "";
			var gridRowSpan = ( headerProp.gridRowSpan ) ? headerProp.gridRowSpan.split(",") : "";
			var gridVisible = ( headerProp.gridVisible ) ? headerProp.gridVisible.split(",") : "";
			
			if( sIndex == null ){ sIndex = 0; }
			if( eIndex == null ){ eIndex = gridText.length; }
			for( var i=sIndex; i < eIndex; i++ ){
				var dataObj = {};
				
				dataObj['html'] =	'<div class="' + Ext.baseCSSPrefix + 'column-header-inner">' +
										'<span class="' + Ext.baseCSSPrefix + 'column-header-text">' +
											gridText[i] +
										'</span>' +
									'</div>';
				
				dataObj['xtype'] = "box";
				dataObj['dataIndex'] = gridHeader[i];
				dataObj['width'] = Number(gridWidth[i]);
				dataObj['minWidth'] = Number(gridWidth[i]);
				dataObj['height'] = this.headerHeight;
				dataObj['item_xtype'] = gridXtype[i];
				dataObj['rowspan'] = gridRowSpan[i];
				dataObj['baseCls'] = 'x-unselectable x-column-header-align-center x-box-item x-column-header x-unselectable-default x-column-header-sort-undefined x-column-header-sort-null';
				
				if( dataObj['item_xtype'] == "combo" ){
					prop[dataObj['dataIndex']] = getCodeStore('/itg/base/searchCodeDataList.do', {code_grp_id:dataObj['dataIndex']});
				}
				
				headers[i - sIndex] = dataObj;
			}
			
			return headers;
		};
		
		var createTableColumns = function(headers, tableData){
			var columns = [];
			var propColumns = [];
			
			var index = 0;
			for(var i=0; i < tableData.length; i++){
				var data = tableData[i];
				propColumns[i] = {};
				
				for(var j=0; j < headers.length; j++){
					var header = headers[j];
					var dataIndex = header['dataIndex'];
					var value = (data[dataIndex] != null ? data[dataIndex] : "");
					var column;
					var columnItem = createXTypeComponent(header['item_xtype'], dataIndex, i, header['minWidth'], header['height'], value);
					if( columnItem != null ){
						column = columnItem;
						column['listeners'] = {
								change: function(){
									var target = this;
									var columnData = target['columnData'];
									var rowIndex = columnData['rowIndex'];
									var dataIndex = columnData['dataIndex'];
									var propColumns = me["columns"];
									
									propColumns[rowIndex][dataIndex].value = target.value;
								}
						};
					}else{
						column = {};
						column['xtype'] = 'box';
						column['html'] = String(value);
						column['width'] = header['width'];
						column['minWidth'] = header['minWidth'];
						column['height'] = header['height'];
						column['value'] = value;
					}
					column['columnData'] = {rowIndex: i, colIndex: j, dataIndex: dataIndex};
					column['baseCls'] = 'x-grid-cell-inner';
					column['style'] = 'textAlign: center; verticalAlign: middle';
					
					propColumns[i][dataIndex] = column;
					
					if(header['rowspan'] == '1'){
						if( i > 0 ){
							if(data[dataIndex] != tableData[i-1][dataIndex]){
								rowMerge( i, data[dataIndex], tableData, dataIndex, column );
							}else{
								continue;
							}
						}else{
							rowMerge( i, data[dataIndex], tableData, dataIndex, column );
						}
					}
					
					if( header['item_xtype'] == "0" || header['show'] == "0" ){
						continue;
					}
					columns[index] = column;
					
					index = index + 1;
				}
			}
			
			me["columns"] = propColumns;
			
			return columns;
		};
		var createXTypeComponent = function( xtype, grpId, index, width, height, value ){
			var result = null;
			
			switch(xtype){
				case "combo":
					result ={
						layout: 'fit',
						width: width,
						items: {
							xtype:'combobox',
							value: value,
							displayField: 'CODE_TEXT',
							valueField: 'CODE_ID',
							store: prop[grpId]
						}
					};
					break;
				case "date":
					result = { layout: 'fit', width: width, items: { xtype:'datefield', format: 'Ym', disabledDays: [0, 6], value: value }}
					break;
				case "text":
					result = { layout: 'fit', width: width, items: { xtype:'textfield', value: value }}
					break;
			}
			
			return result;
		};
		
		var rowMerge = function ( rowIndex, value, tableData, dataIndex, column ){
			var spanCount = 1;
			for( var k=(rowIndex + 1); k < tableData.length; k++ ){
				if( tableData[k][dataIndex] != value ){
					break;
				}
				spanCount = spanCount + 1;
			}
			
			column['rowspan'] = spanCount;
			column['height'] = this.headerHeight * spanCount;
		};
		
		var searchData = function( url, param, func ){
			var r_value;
			
			jq.ajax({ type:"POST"
				, url: url
				, contentType: "application/json"
				, dataType: "json"
				, async: false
				, data : getArrayToJson(param)
				, success:function(data){
					if( func == null ){
						r_value = data;
					}else{
						r_value = func(data);
					}
				}
			});
			
			return r_value;
		};
		// Private Function End
		var headerProp = (searchData( me.resource_url, { resource_prefix: me.resource_prefix } )).resultMap;
		var tableData = (searchData( me.url, me.params )).gridVO;
		
		return createItems( headerProp, tableData );
	},
	prepareData: function(){
	},
	onDestroy: function(){
	},
	// Custom Method
	getInputData: function(){
		var me = this;
		var list = me["columns"];
		var param = {};
		for( var i=0; i < list.length; i++ ){
			var obj = list[i];
			var paramObj = {};
			for( var p in obj ){
				var value = obj[p]["value"];
				if( value == undefined ){
					paramObj[p] = "";
					continue;
				}
				
				if( typeof(value) == Date ){
					var mm = "0" + (value.getMonth()+1);
					paramObj[p] = value.getFullYear() + "" + mm.substr(0, 2);
				}else{
					paramObj[p] = value;
				}
			}
			param[i] = paramObj;
		}
		
		param["size"] = list.length;
		
		return param;
	},
	listeners: {
		resize: function( comp, width, height, oldWidth, oldHeight, eOpts ){
			Ext.suspendLayouts();
			
			var column = Ext.getCmp(comp.id + '_columns');
			var header = Ext.getCmp(comp.id + '_headers');
			
			if( column != null ){
				column.setWidth( width );
				//column.setHeight( height - comp.headerHeight );
				
				var columnItems = column.items.items;
				var headerItems = header.items.items;
				
				var resizeColumnWidth = {};
				var widthOffset = (this.getWidth() - column.getWidth());
				var headerLength = headerItems.length;
				var blankWidth = widthOffset / headerLength;
				var blankWidthPart = (widthOffset % headerLength);
				
				var i = 0;
				for( i=0; i < headerLength; i++ ){
					var hItem = headerItems[i];
					var changeWidth;
					if( i + 1 == headerLength ){
						changeWidth = hItem['width'] + blankWidth + blankWidthPart;
					} else {
						changeWidth = hItem['width'] + blankWidth;
					}
					hItem.setWidth(changeWidth);
					resizeColumnWidth[hItem['dataIndex']] = changeWidth;
				}
				
				for( i=0; i < columnItems.length; i++ ){
					var cItem = columnItems[i];
					cItem.setWidth(resizeColumnWidth[cItem['columnData']['dataIndex']]);
				}
			}
			
			Ext.resumeLayouts(true);
		},
		afterrender: function (){
			if( scroll ){
				var me = this;
				var header = Ext.getCmp(me.id + '_headers').getTargetEl();
				var eventTarget = Ext.getCmp(me.id + '_columns').getTargetEl();
				
				eventTarget.on('scroll', function(obj1, obj2, obj3){
					var left = eventTarget.dom.scrollLeft;
					var top = eventTarget.dom.scrollTop;
					
					header.setLeft(-left);
				});
			}
		}
	}
});
*/