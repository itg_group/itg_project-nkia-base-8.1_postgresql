var popUp_comp={};
var gridNm=null;

function treeNodeClick( tree, record, index, eOpts ) {
	
	var class_id = record.raw.node_id;
	
	var gridData = getGrid(gridNm).getStore();
	gridData.proxy.jsonData={class_id: class_id};
	gridData.load();
			
}

//미계약 유지보수 자산조회 팝업생성
function createNoneReqPopup(setPopUpProp){	
	var storeParam = {};
	var that = this;
	var selectPopUp = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var setGridProp = {	
				context: getConstValue('CONTEXT'),					// Context
				id: that['id']+'_grid',								// Grid id
				title: getConstText({ isArgs: true, m_key: 'res.label.itam.maint.nonereq.00017' }),
    			gridHeight: 288,	
				resource_prefix: 'grid.itam.maint.nonereq.pop',			// Resource Prefix
				url: getConstValue('CONTEXT') + that['url'],		// Data Url
				pagingBar : true,
				pageSize : 10,
				border : true
		};
		
		var popUpGrid = createGridComp(setGridProp);
		gridNm = that['id']+'_grid';
		
		var setTreeProp = {				
				id : that['id']+'_Tree',
				url : '/itg/itam/amdb/searchAmClassAllTree.do',
				title : getConstText({ isArgs: true, m_key: 'res.title.itam.class' }),
				dynamicFlag : false,
				searchable : true,
				searchableTextWidth : 60,
				expandAllBtn : true,
				collapseAllBtn : true,
				params: null,
				rnode_text : getConstText({ isArgs: true, m_key: 'res.00003' }),
				expandLevel: 1
			}
		var staticClassTree = createTreeComponent(setTreeProp);
		
		// 높이 길이 체크 무시
		staticClassTree['syncSkip'] = true;
		
		
		var assetSearchBtn   = createBtnComp({visible: true, label:getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:that['id']+'SearchBtn', ui:'correct'});
		attachBtnEvent(assetSearchBtn, actionLink,  'searchAttr');
		
		var setSearchFormProp = {
				id: that['id']+'SearchForm',
				formBtns: [assetSearchBtn],	
				columnSize: 2,
				tableProps : [
					            {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.common.00001' }), name:'search_asset_id', width:250})}
					          , {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.common.00002' }), name:'search_model_nm', width:250})}
				             ]
		}

		var asset_search_form = createSeachFormComp(setSearchFormProp);
		
		function actionLink(command) {

			switch(command) {		
				case 'searchAttr' :
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
			
					popUpGrid.searchList(paramMap);
				break;
			
			}
		}		
		
		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: 385,
			autoScroll: true,
			items: [staticClassTree]
		});
		
		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			flex:1 ,
			items: [asset_search_form, popUpGrid]
		});
		
		var complexProp = {
				height : '100%',
				//padding : '0 0 0 0',
				panelItems : [{
					flex: 1,
					items : left_panel
				},{
					flex: 3,
					items : right_panel
				}]
			};
		var complexPanel = createHorizonPanel(complexProp);		
		
		attachCustomEvent('select', staticClassTree, treeNodeClick);
		
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			scale:'medium',
			ui:'correct',
			handler: function() {
				var selectedRecords = popUpGrid.view.selModel.getSelection();
				if(selectedRecords.length <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.maint.req.00037' }));
				}else{
					
					if(that['afterFunction'] != null) {
						that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
						
					} else {

						var theForm = Ext.getCmp('second_editor_form');
						
						var user_nm = selectedRecords[0].raw.USER_NM;
						var cust_nm = selectedRecords[0].raw.CUST_NM;
						var asset_id = selectedRecords[0].raw.ASSET_ID;
						var model_nm = selectedRecords[0].raw.MODEL_NM;
						var class_nm = selectedRecords[0].raw.CLASS_NM; 
						var manufacturer = selectedRecords[0].raw.VENDOR_NM; 
						
						theForm.setFieldValue('asset_user_nm', user_nm);
						theForm.setFieldValue('asset_cust_nm', cust_nm);
						theForm.setFieldValue('asset_id', asset_id);
						theForm.setFieldValue('model_nm', model_nm);
						theForm.setFieldValue('class_nm', class_nm);
						theForm.setFieldValue('manufacturer', manufacturer);
						
						selectPopUp.hide();
					}
				}
			}
		});
		
		selectPopUp = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			modal: that['modal'],
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = selectPopUp;
		
	} else {
		selectPopUp = popUp_comp[that['id']];
	}
	
	return selectPopUp;

}

//유지보수업체 조회팝업
function createVendorSelectPop(setPopUpProp){		
	var storeParam = {};
	var that = this;
	var vendorSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var setGridProp = {	
				context: getConstValue('CONTEXT'),					// Context
				id: that['id']+'_grid',								// Grid id
				title : getConstText({ isArgs: true, m_key: 'res.label.itam.maint.nonereq.00018' }),
				height: getConstValue('SHORT_GRID_HEIGHT'),
				resource_prefix: 'grid.itam.vendor.popup',				// Resource Prefix
				url: getConstValue('CONTEXT') + that['url'],		// Data Url
				pagingBar : true,
				pageSize : 10
		};
		
		var vendorGrid = createGridComp(setGridProp);
		
		var vendorSearchBtn   = createBtnComp({visible: true, label:getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:that['id']+'SearchBtn', ui:'correct'});
		attachBtnEvent(vendorSearchBtn, actionLink,  'searchAttr');
		
		var setSearchFormProp = {
				id: that['id']+'SearchForm',
				formBtns: [vendorSearchBtn],			
				columnSize: 2,
				tableProps : [
					            {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.vendor.00002' }), name:'search_vendor_nm', width:250})}
					          , {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.vendor.00001' }), name:'search_vendor_id', width:250})}
				             ]
		}

		var vendor_search_form = createSeachFormComp(setSearchFormProp);
		
		function actionLink(command) {

			switch(command) {		
				case 'searchAttr' :
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
			
					vendorGrid.searchList(paramMap);
				break;
			
			}
		}
		
		var complexPanel = Ext.create('Ext.panel.Panel',{
			border: false,
			items: [vendor_search_form, vendorGrid]
		})
		
		
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			scale:'medium',
			handler: function() {
				var selectedRecords = vendorGrid.view.selModel.getSelection();
				if(selectedRecords.length <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.maint.req.00037' }));
				}else{
					
					if(that['afterFunction'] != null) {
						that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
						
					} else {
						
						var theForm = Ext.getCmp('second_editor_form');
						
						var vendor_nm = selectedRecords[0].raw.VENDOR_NM;
						var vendor_tel_no = selectedRecords[0].raw.VENDOR_TEL_NO;
						var tech_user_nm = selectedRecords[0].raw.TECH_USER_NM;
						var tech_user_email = selectedRecords[0].raw.TECH_USER_EMAIL;
						var vendor_id = selectedRecords[0].raw.VENDOR_ID;
						
						theForm.setFieldValue('vendor_nm', vendor_nm);
						theForm.setFieldValue('vendor_tel_no', vendor_tel_no);
						theForm.setFieldValue('tech_user_nm', tech_user_nm);
						theForm.setFieldValue('tech_user_email', tech_user_email);
						theForm.setFieldValue('vendor_id', vendor_id);
						
						vendorSelectPop.hide();
					}
				}
			}
		});
		
		vendorSelectPop = Ext.create('nkia.custom.windowPop',{			
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			modal: that['modal'],
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
					
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = vendorSelectPop;
		
	} else {
		vendorSelectPop = popUp_comp[that['id']];
	}
	
	return vendorSelectPop;
}	