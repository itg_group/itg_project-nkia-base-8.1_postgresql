/**
 * 팝업 초기화 버튼
 */
function containerInitField(obj){
	//자품모품에서 모품ID 선택을 초기화 할 경우 자산의 모/자 구분을 다시 모품으로 되돌린다.
	var objId = obj.id;
	var assetIdHiLength = objId.indexOf('ASSET_ID_HI');
	//만약 초기화를 누른 속성의 ID에 ASSET_ID_HI라는 문장이 포함되어 있다면
	//(다른 속성의 초기화 버튼 클릭시엔 objIdLength는 -1 임)
	if(assetIdHiLength > 0) {
		var theForm = obj.up('fieldcontainer').up("panel").getForm();
		var parentAssetFlagField = theForm.findField('DIV_HIERACHY');
		parentAssetFlagField.setValue("M");
	}
	var objArr = obj.up('fieldcontainer').query('.field');
	for(var i=0;i<objArr.length;i++){
		var c = objArr[i]
		if(c.xtype == 'textfield' || c.xtype == 'hiddenfield'){
			c.setValue('');
		}
	}
}

/**
 * 팝업 component 유형에 따라 보낸다.
 */
// 2014.03.27정정윤 인자값 추가 : expandEntityMap
// 현재 선택 자산의 정보를 가져오기 위한 맵 추가
// - 사용하려면 OpmsUtil.java 의 createPopupInfoComp(상세보기), createPopupBtnComp(팝업열기) 메서드에 
// Map을 인자값에 추가하여 openAmPopup 핸들러 생성할 것
function openAmPopup(btnObj,popupType,targetId,targetNm,view_type,class_id,class_type){

	targetForm = null;
	var targetIdObj = null;
	var targetNmObj = null;
	var popId = null;
	var assetId = null;
	var classId = null;
	
	// view_type이 statistics이면 자산운영관리(구성운영관리) 화면에서 조회시 사용함.
	if(view_type != "statistics"){
		targetForm 	= btnObj.up('fieldcontainer').up("panel");
		targetIdObj	= targetForm.getForm().findField(targetId);
		targetNmObj	= targetForm.getForm().findField(targetNm);
		///모델 팝업에서 모델 선택 시, 제조사 필드에도 데이터 세팅해라. -심희철.owner.body.dom.firstChild.firstChild.firstChild;
		targetIdObj2= targetForm.getForm().findField("VENDOR_ID");
		targetNmObj2= targetForm.getForm().findField("VENDOR_ID_POP_NM");
		
		locXYId= targetForm.getForm().findField("LOC_XY");
		locXYNm= targetForm.getForm().findField("LOC_XY_POP_NM");
		
		//SW그룹선택시 SW옵션도 같이 세팅 -20140326정정윤
		optionIdObj = targetForm.getForm().findField("SW_GROUP_OPTION_ID");
		optionNmObj = targetForm.getForm().findField("SW_GROUP_OPTION_ID_POP_NM");
		popId = targetIdObj.name+"__Popup";
		// (X)자산리스트 상에서 자산을 선택하여 원장화면을 열었으면 클릭한 해당자산의 ASSET_ID와 CLASS_ID를 가져오고
		// (O)자산리스트 상에서 자산을 선택하여 원장화면을 열었으면 클릭한 해당자산의 ASSET_ID를 세팅해준다
		if(targetForm.selectedRecord != undefined) {
			assetId = targetForm.selectedRecord.ASSET_ID;
			classId = class_id;
		// 아닐경우(신규등록) OpmsUtil.java에서 리턴한 class_id를 세팅한다 - 20140108_jyjeong
		} else {
			classId = class_id;
		}
	}else{
		targetIdObj = targetId;
		targetNmObj = targetNm;
		popId = targetIdObj+"__Popup";
	}

	if(popupType == "PROJECT"){
		var setPopUpProp = {
				id: popId,
				popUpText: "사업조회",
				targetProjectIdField: targetIdObj,
				targetProjectNameField: targetNmObj,
				height: 507,
				width: 650,
				url: '/itg/itam/project/searchProject.do',
				buttonAlign: 'center',
				bodyStyle: 'background-color: white;'
		}
		var popUp = createOpmsProjectSelectPop(setPopUpProp);
		popUp.show();
		
	}else if(popupType == "SUPPLY"){
		var setPopUpProp = {
				id: popId,
				popUpText: "공급계약조회",
				targetSupplyIdField: targetIdObj,
				targetSupplyNameField: targetNmObj,
				height: 507,
				width: 650,
				url: '/itg/itam/supply/searchSupply.do',
				buttonAlign: 'center',
				bodyStyle: 'background-color: white;'
		}
		var popUp = createOpmsSupplySelectPop(setPopUpProp);
		popUp.show();
		
	}else if(popupType == "CLASS"){
		var others;
		if(class_type != null){
			others = class_type;
		}
		var setPopUpProp = {
				popId: popId,
				treeId: popId+'__treeId',
				popUpTitle: "분류체계조회",
				url: '/itg/itam/amdb/searchAmClassAllTree.do',
				title:"분류체계조회",
				targetField: targetNmObj,
				targetHiddenField: targetIdObj,
				leafSelect: false,
				rootSelect: false,
//				btnHide: true,
				params: {classType : class_type, others : others},
				expandAllBtn : popId+"__expandAllBtn",
				collapseAllBtn : popId+"__collapseAllBtn",
				expandLevel: 3,
				type : popupType
		}
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	}else if(popupType == "CUST"){
		var setPopUpProp = {
				popId: popId,
				treeId: popId+'__treeId',
				popUpTitle: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00010' }),
				url: '/itg/system/dept/searchStaticAllDeptTree.do',
				title: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00010' }),
				targetField: targetNmObj,
				targetHiddenField: targetIdObj,
				leafSelect: true,
				rootSelect: true,
				expandAllBtn : popId+"__expandAllBtn",
				collapseAllBtn : popId+"__collapseAllBtn",
				expandLevel: 3
		}
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	}else if(popupType == 'RACK_CODE'){
		var vendorField = targetForm.getForm().findField("AM_ASSET__VENDOR_ID");
		var setPopUpProp = {	
				id: popId,
            	tagetIdField: targetIdObj,
            	tagetNameField: targetNmObj,
            	tagetVendorField: vendorField,
    			height: 507,
    			width: 650,
    			buttonAlign : 'center',
    			bodyStyle: 'background-color: white;'
		}
		var popUp = searchRackModelSelectPop(setPopUpProp);
		popUp.show();
	}else if(popupType == 'LOC_CODE'){
/*		locationTargetId = targetIdObj;
		locationTargetNm = targetNmObj;
		rackTargetId = locXYObj;
		if(view_type == 'V'){
			var fieldValue = targetIdObj.value; 

			if(fieldValue == null || fieldValue == ""){
				alert("설치위치를 등록해주세요.");
				return false;
			}
			var gubun = 'sel'; 
			if(fieldValue.length < 9){
				gubun = 'all';
			}
			
			window.open("/itg/itam/topView/topViewPop.do?loc_plan_code="+fieldValue+"&gubun="+gubun,
					"topViewPop", "width=1150,height=600,history=no,resizable=no,status=no,scrollbars=yes,member=no");			
		}else{		
			var setPopUpProp = {	
					id: popId,
	            	tagetIdField: targetIdObj,
	            	tagetNameField: targetNmObj,
	            	view_type: view_type,
	    			height: 507,								
	                width: 650,
	    			buttonAlign : 'center',
	    			bodyStyle: 'background-color: white;'
			}
			var popUp = locationSelectPop(setPopUpProp);
			popUp.show();
		}
	}else if(popupType == 'RACK_LOC'){*/
		rackTargetId = locXYId;
		rackTargetNm = locXYNm;
		locationTargetId = targetIdObj;
		locationTargetNm = targetNmObj;		
		if(view_type == 'V'){
			var gubun = 'rack';
			var fieldValue = targetIdObj.value;

			if(fieldValue == null || fieldValue == ""){
				alert("설치위치를 등록해주세요.");
				return false;
			}

			if(locXYId.value == "" || locXYId.value == null){
				gubun = 'all';
			}else{
				fieldValue = locXYId.value;
			}
			window.open("/itg/itam/topView/topViewPop.do?loc_plan_code="+fieldValue+"&gubun="+gubun,
					"topViewPop", "width=1150,height=600,history=no,resizable=no,status=no,scrollbars=yes,member=no");			
		}else{
			var setPopUpProp = {	
					id: popId,
					tagetIdField: targetIdObj,
					tagetNameField: targetNmObj,
	    			height: 507,								
	                width: 900,
					buttonAlign : 'center',
					bodyStyle: 'background-color: white;'
			}
			var popUp = createRackLocSelectPop(setPopUpProp);
			popUp.show();					
		}	
	}else if(popupType == "ASSETHIERACHY") {
		var targetFlagObj	= targetForm.getForm().findField("DIV_HIERACHY");
		var setPopUpProp = {
				id: popId,
				popUpText: getConstText({ isArgs: true, m_key: 'res.label.itam.oper.00087' }),
				targetParentAssetIdField: targetIdObj,
				targetParentAssetFlagField: targetFlagObj,
				height: 507,
				width: 650,
				url: '/itg/itam/statistics/searchAssetListPop.do',
				buttonAlign: 'center',
				params: {asset_id: assetId, class_id: classId},
				bodyStyle: 'background-color: white;'
		}
		var popUp = createOpmsParentSelectPop(setPopUpProp);
		popUp.show();
	}
	else if(popupType == "WORK") {
		var setPopUpProp = {
				popId: popId,
				treeId: popId+'__treeId',
				popUpTitle: getConstText({ isArgs: true, m_key: '업무분류 선택' }),
				url: '/itg/itam/opms/searchWorkTreeComp.do',
				title: getConstText({ isArgs: true, m_key: '업무분류 선택' }),
				targetField: targetNmObj,
				targetHiddenField: targetIdObj,
				leafSelect: true,
				rootSelect: true,
				expandAllBtn : popId+"__expandAllBtn",
				collapseAllBtn : popId+"__collapseAllBtn",
				expandLevel: 3
		}
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	}
	else if(popupType == "VENDOR") {
		var vendorId;
		var vendorNm;		
		var setPopUpProp = {
				id: popId,
				popUpText: "벤더 조회",//"res.label.itam.oper.00087",
				targetField: targetNmObj,
				targetHiddenField: targetIdObj,
				height: 507,
				width: 650,
				url: '/itg/system/vendor/searchVendor.do', //TODO 모델팝업 내 벤더 팝업 서비스 붙일 것.
				buttonAlign: 'center',
				params: {vendor_id: vendorId, vendor_nm: vendorNm},
				bodyStyle: 'background-color: white;'
		}
		var popUp = createOpmsVendorSelectPop(setPopUpProp);
		popUp.show();
	}else if(popupType == "MODEL") {
		var setPopUpProp = {
			popId: popId,
			treeId: popId+'__treeId',
			popUpTitle: getConstText({ isArgs: true, m_key: '모델선택' }),//모델선택
			url: '/itg/itam/opms/searchModelTreeComp.do',//모델관련 트리 뿌릴 것.
			title: getConstText({ isArgs: true, m_key: '모델선택' }),//모델선택
			targetField: targetNmObj,
			targetHiddenField: targetIdObj,
			targetField2: targetNmObj2,
			targetHiddenField2: targetIdObj2,
			leafSelect: true,
			rootSelect: true,
			expandAllBtn : popId+"__expandAllBtn",
			collapseAllBtn : popId+"__collapseAllBtn",
			expandLevel: 3,
			type : popupType
		}
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	// SWGROUP 팝업 20140326 정정윤수정
	// - 옵션까지 나오는트리로 URL 변경
	// - 옵션ID, 옵션명 필드 오브젝트 추가
	// - 타입 추가
	}else if(popupType == "SWGROUP") {
		var setPopUpProp = {
			popId: popId,
			treeId: popId+'__treeId',
			popUpTitle: '소프트웨어 그룹 선택',
			//url: '/itg/itam/sam/searchSwGroupTree.do',
			url: '/itg/itam/sam/searchSwGroupOptionTree.do',
			title: '소프트웨어 그룹 선택',
			targetField: targetNmObj,
			targetHiddenField: targetIdObj,
			optionField: optionIdObj,
			optionHiddenField: optionNmObj,
			leafSelect: true,
			rootSelect: true,
			expandAllBtn : popId+"__expandAllBtn",
			collapseAllBtn : popId+"__collapseAllBtn",
			expandLevel: 2,
			type: popupType
		}
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	} else if(popupType == "PHYSICONF") {
		
		// 기본정보 탭에 있는 자산ID, 분류체계아이디, 구성ID 등등을 불러오기 위해...
		// 기본정보 탭을 찾아가서 해당 필드들의 값을 가져옴
		var validFlag = false;
		var detailForm = targetForm.up().up();
		var detailFormItems = detailForm.items.items; // <- 스펙정보/변경이력정보/기본정보 등, 탭 목록
		var manualFormItems;
		
		for(var i=0; i<detailFormItems.length; i++) {
			if(detailFormItems[i].title == "기본정보") {
				manualFormItems = detailFormItems[i];
				validFlag = true;
				break;
			}
		}
		
		if(validFlag) {
			var manualPanelForm 	= manualFormItems.down("panel").getForm();
			var asset_id 			= manualPanelForm.findField("ASSET_ID").getValue();
			var class_id 			= manualPanelForm.findField("CLASS_ID").getValue();
			var conf_id 			= targetIdObj.getValue();
			var tangible_asset_yn	= "Y";
			var view_type			= "pop_view";
			var entity_class_id 	= class_id;
			
			var param = "?asset_id="+asset_id+"&conf_id="+conf_id+"&class_id="+class_id+"&view_type="+view_type+"&entity_class_id="+entity_class_id;
		    showAssetDetailPop(param);
		} else {
			alert("물리자산 정보가 없습니다.");
		}
		
	}
}

function createMainUserSelectPop(compProperty){
	var that 		= convertDomToArray(compProperty);
	var id			= that["id"];
	var targetGrid = Ext.getCmp(id);
	var confId		= that["conf_id"];
	var selParentData = targetGrid.getGridDataUpper(); 
	var serviceGrid		= "";
	var selServiceGrid	= "";
	var windowComp	= "";
	var searchForm	= "";
	
	var conFirmBtn	 	= createBtnComp({visible: true,label:"적용" ,id:"confConFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	
	var mainRadioComp = createCodeRadioComp({name:'main_yn', code_grp_id: 'MAIN_YN', columnSize: 15, width: 40});
	var complexPanel = createFlexPanel({items : [mainRadioComp]});
	//적용
	var confirmMng = function (){
		var checkedValue = mainRadioComp.getChecked();
		var mainValue = checkedValue[0].getGroupValue();
		var mainName = checkedValue[0].boxLabel;
		var mainMap ={};

		mainMap['mainValue'] = mainValue;
		mainMap['mainName'] = mainName;
				
		windowComp.close();
				
		createMngUserListPop({title : that["title"], target_id : that["id"] ,conf_id : that["conf_id"], mainValue: mainMap } );
	}
	var codePopupProp		= {
			id:   id+"Pop"
			,title: '정/부 선택'
			,width: 190
			,height: 100
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [conFirmBtn]
			,closeBtn : true
			,items: [mainRadioComp]
			,resizable:true
	  }
	
	windowComp = createWindowComp(codePopupProp);

	attachBtnEvent(conFirmBtn	,confirmMng);
	
	windowComp.show();
	
}

/**
 function openAmPopup(btnObj,popupType,targetId,targetNm){
	var targetForm 	= btnObj.up('fieldcontainer').up("panel");
	var targetIdObj	= targetForm.getForm().findField(targetId);
	var targetNmObj	= targetForm.getForm().findField(targetNm);
	var popId = targetIdObj.name+"__Popup";
	
	if(popupType == "PROJECT"){
		var setPopUpProp = {
				id: popId,
				popUpText: "사업조회",
				targetProjectIdField: targetIdObj,
				targetProjectNameField: targetNmObj,
				height: 507,
				width: 650,
				url: '/itg/itam/project/searchProject.do',
				buttonAlign: 'center',
				bodyStyle: 'background-color: white;'
		}
		var popUp = createOpmsProjectSelectPop(setPopUpProp);
		popUp.show();
		
	}else if(popupType == "SUPPLY"){
		var setPopUpProp = {
				id: popId,
				popUpText: "공급계약조회",
				targetSupplyIdField: targetIdObj,
				targetSupplyNameField: targetNmObj,
				height: 507,
				width: 650,
				url: '/itg/itam/supply/searchSupply.do',
				buttonAlign: 'center',
				bodyStyle: 'background-color: white;'
		}
		var popUp = createOpmsSupplySelectPop(setPopUpProp);
		popUp.show();
		
	}else if(popupType == "CLASS"){
		var setPopUpProp = {
				popId: popId,
				treeId: popId+'__treeId',
				popUpTitle: "분류체계조회",
				url: '/itg/itam/amdb/searchAmClassAllTree.do',
				title:"분류체계조회",
				targetField: targetNmObj,
				targetHiddenField: targetIdObj,
				leafSelect: false,
				rootSelect: false,
				btnHide: true,
				expandAllBtn : popId+"__expandAllBtn",
				collapseAllBtn : popId+"__collapseAllBtn",
				expandLevel: 3
		}
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	}else if(popupType == "CUST"){
		var setPopUpProp = {
				popId: popId,
				treeId: popId+'__treeId',
				popUpTitle: "소유부서조회",
				url: '/itg/system/dept/searchStaticAllDeptTree.do',
				title:"소유부서조회",
				targetField: targetNmObj,
				targetHiddenField: targetIdObj,
				leafSelect: true,
				rootSelect: true,
				expandAllBtn : popId+"__expandAllBtn",
				collapseAllBtn : popId+"__collapseAllBtn",
				expandLevel: 3
		}
		var popUp = createTreeSelectPop(setPopUpProp);
		popUp.show();
	}
}
 */

/**
 * 담당자 선택 팝업 (자동화 원장 전용...)
 * @param compProperty
 */
/*function createMngUserListPop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	var id			= that["id"];
	var conf_id		= that["conf_id"];
	var userGrid	= "";
	var selUserGrid= "";
	var window_comp= "";
	var parentGrid 	= Ext.getCmp(id);
	var selParentData= parentGrid.getGridDataUpper();
	
	var searchUserList = function (){
		
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//추가
	var addSeluser = function (){
		var paramMap = searchForm.getInputData();
		var selUserGridStore = selUserGrid.store;
		var selRows 	= gridSelectedRows(userGrid.id);
		var selectList = getGrid(userGrid.id).view.selModel.getSelection();
		if(isSelected(userGrid.id)){
			Ext.suspendLayouts();
			
			checkGridRowCopyToGrid(userGrid.id, selUserGrid.id, {
				primaryKeys : ["USER_ID"]
			});
			
			Ext.resumeLayouts(true);
			
			
			selUserGridStore.each(function(record, index, count){
				var rowData = record.data;
				if( rowData["MAIN_YN"] != "Y" ){
					record.set('MAIN_YN', "N");
				}
			});
			
		}else{
			showMessageBox({msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }),type : "ALERT"});
			return;
		}
		
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//삭제
	var deSeluser = function (){
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		//var selRows 	= gridSelectedRows(selUserGrid.id);
		var selRows = getGrid(selUserGrid.id).view.selModel.getSelection();
		
		if(isSelected(selUserGrid.id)){
			Ext.suspendLayouts();
			for( var i=0; i < selRows.length; i++ ){
				selUserGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }) ,type : "ALERT"});
			return;
		}
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//적용
	var confirmMng = function (){
		var selUserData = selUserGrid.getGridDataUpper();
		if( selUserData.length > 0 ){
			var userMainCount = 0;
			var isMainYn = false;
			var notSelUserMainYn = false;
			for( var i = 0; i < selUserData.length; i++ ){
				var mainYn = selUserData[i]["MAIN_YN"];
				if( mainYn != null && mainYn != "" ) {
					if( mainYn == "Y" ){
						userMainCount++;
						isMainYn = true;
					}
				}else{
					notSelUserMainYn = true;
					break;
					
				}
			}
			
			if( notSelUserMainYn ){
				alert("[정]/[부] 담당자 정보를 입력하여 주세요.");
				return;
			}
			if( !isMainYn ){
				alert("한명의 [정] 담당자를 지정해 주십시요.");
				return;
			}
			if( isMainYn && userMainCount > 1 ){
				alert("[정] 담당자는 반드시 1명 이어야 합니다.");
				return;
			}
		}
		showMessageBox({msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00011' }) ,type : "CONFIRM" ,fn : function(btns){
			if( btns == "ok" ){							
				var selUserData = selUserGrid.getGridDataUpper();
				if( selUserData.length > 0 ){
					var parentGridStore = parentGrid.store;
					Ext.suspendLayouts();
					parentGridStore.removeAll();
					for( var i = 0; i < selUserData.length; i++ ){
						var sourceData = selUserData[i];
						var mainYn = sourceData["MAIN_YN"];
						sourceData["FN_MAIN_YN_NM"] = ( mainYn == "Y" ) ? "정" : "부";
						
						// 운영부서 정보 Setting
						if( mainYn == "Y" ){
							var custId = sourceData["CUST_ID"];
							var custNm = sourceData["CUST_NM"];
							var gridItems = null;
							// 일괄수정 시에는 운영부서 필드에 '정'의 부서값 세팅하는 로직 필요없음... 에러남.. -2014.03.31정정윤-
							if( parentGrid.up().items.items[0].id != "allItemVPanel" ) {
								gridItems = parentGrid.up().items.items[0].getFormFields();
								for(var j=0; j<gridItems.length; j++){
									var gridItem = gridItems[j];
									if(gridItems[j].name == "ASSET_CUST_ID"){	//운영부서 ID 필드
										gridItem.setValue(custId);
									} else if(gridItems[j].name == "ASSET_CUST_ID_POP_NM") { //운영부서 NAME 필드
										gridItem.setValue(custNm);
									}
								}
							}
						}
						parentGridStore.add(sourceData);
					}
					Ext.resumeLayouts(true);
					window_comp.close();
				}else{
					window_comp.close();
				}
			}
		}});
	}
	
	//검색
	var searchUserBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchUserBtn' ,ui:'correct'});
	//적용
	var conFirmBtn	 	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }) ,id:"conFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	//사용자 추가
	var addSeluserBtn	= createIconBtnComp({id:'addSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif' 	,margin : '2 2 2 0'});
	//사용자 삭제
	var deSeluserBtn	= createIconBtnComp({id:'deSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif' ,margin : '2 0 2 2'});
	
	addSeluserBtn.vline 	= false;
	deSeluserBtn.vline 	= false;
	
	//검색조건
	var searchKey		= createCodeComboBoxComp({name:'searchKey'	,code_grp_id:'SEARCH_USER'	,hideLabel : true 	,width : 100 ,attachAll : true});
	//검색값
	var searchValue	= createTextFieldComp({name:'searchValue'	,padding 	: '0 0 0 5' ,width : 200 ,hideLabel : true });
	//컨테이너
	var searchContainer		= createUnionFieldContainer({label : getConstText({ isArgs: true, m_key: 'btn.common.search' }) ,items : [searchKey,searchValue] ,id : "searchContainer"});
	
	var setSearchFormProp 	= {
		id : id+'SearchForm',
		columnSize : 1,
		formBtns : [searchUserBtn],
		tableProps : [{colspan:1, tdHeight: 30, item : searchContainer}],
		enterFunction : searchUserList
	}
	//검색폼
	var searchForm	= createSeachFormComp(setSearchFormProp);
	//추가,삭제 버튼 컨테이너
	var btnContainer		= createHorizonPanel({align : 'center' ,panelItems : [addSeluserBtn,deSeluserBtn]})

	//담당자 리스트
	var setUserGridProp = {	 
		id	: id+"UserList",
		title : getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00011' }),
		gridHeight : 190,
		resource_prefix	: 'grid.itam.managerBaseList',
		url	: '/itg/itam/opms/selectOpmsMngUserList.do',
		params : {selUserList : selParentData},
		pagingBar : true,
		multiSelect	: true,
		selModel : true,
		border : true,
		pageSize : 5
	};
	
	userGrid = createGridComp(setUserGridProp);
	
	var mainCombo = createCodeComboBoxComp({name:'main_yn'	,code_grp_id:'MAIN_YN' ,width : 60	,hideLabel : true,attachChoice:true });
	
	var setSelGridProp = {						// Context
		id: id + 'SelUserList',							// Grid id
		title: "Infra 운영자 정보",		// Grid Title
		resource_prefix: 'grid.process.operate.sub.oper',	// Resource Prefix
		url: getConstValue('CONTEXT') + '/itg/base/selectedUserList.do',	// Data Url
		gridHeight : 160,
		border : true,
		autoLoad : true,
		params : { userIds : new Array() },
		multiSelect	: true,
		selModel: true,
		editorMode : true,
		cellEditing : true,
		comboComp  : {MAIN_YN : mainCombo}
	};
	
	selUserGrid = createGridComp(setSelGridProp);
	
	selUserGrid.getStore().on("load", function(store, records, successful, eOpts) {
		if( selParentData.length > 0 ){
			Ext.suspendLayouts();
			for( var i = 0; i < selParentData.length; i++ ){
				store.add(selParentData[i]);
			}
			Ext.resumeLayouts(true);
		}
	});
	
	
	var complexPanel = createFlexPanel({items : [searchForm,userGrid,btnContainer,selUserGrid]});
	var codePopupProp = {
		id:   id+"Pop",
		title: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00012' }),
		width: 700,
		height: 568,
		modal: true,
		layout: 'fit',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white; ',
		buttons: [conFirmBtn],
		closeBtn : true,
		items: complexPanel,
		resizable:true
	}
	
	window_comp = createWindowComp(codePopupProp);
	
	//버튼 이벤트
	attachBtnEvent(conFirmBtn		,confirmMng);
	attachBtnEvent(searchUserBtn	,searchUserList);
	attachBtnEvent(addSeluserBtn	,addSeluser);
	attachBtnEvent(deSeluserBtn		,deSeluser);
	
	window_comp.show();
	
}*/
function createMngUserListPop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	var id			= that["target_id"];
	var conf_id		= that["conf_id"];
	var userGrid	= "";
	var selUserGrid= "";
	var window_comp= "";
	var parentGrid 	= Ext.getCmp(id);
	var selParentData= parentGrid.getGridDataUpper();
	var mainValue= that["mainValue"];
	
	var searchUserList = function (){
		
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	var searchUserInit = function (){
		searchForm.initData();
	}
	
	//추가
	var addSeluser = function (){
		var paramMap = searchForm.getInputData();
		var selUserGridStore = selUserGrid.store;
		var selRows 	= gridSelectedRows(userGrid.id);
		
		if(isSelected(userGrid.id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				selRows[i].RNUM = i+1;
				if(mainValue != undefined){
					if(selRows[i].MAIN_YN == null || selRows[i].MAIN_YN == ""){
						selRows[i].data.MAIN_YN_VAL = mainValue.mainValue;
						selRows[i].data.MAIN_YN = mainValue.mainName;
					}
				}
				selUserGrid.store.add(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
			return;
		}
		
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//삭제
	var deSeluser = function (){
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		var selRows 	= gridSelectedRows(selUserGrid.id);
		
		if(isSelected(selUserGrid.id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				selUserGrid.store.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
			return;
		}
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//적용
	var confirmMng = function (){
		showMessageBox({msg : "적용하시겠습니까?" ,type : "CONFIRM" ,fn : function(btns){
					if(btns == "ok"){
							parentGrid.store.removeAll();
							var selUserData = selUserGrid.getGridDataUpper();
							if(selUserData.length>0){
								Ext.suspendLayouts();
								for(var i=0;selUserData.length>i;i++){
									selUserData[i].RNUM = i+1;
									if(mainValue != undefined){
										if(selUserData[i].MAIN_YN == null || selUserData[i].MAIN_YN == ""){
											selUserData[i].MAIN_YN_VAL = mainValue.mainValue;
											selUserData[i].MAIN_YN = mainValue.mainName;
										}
									}
									parentGrid.store.add(selUserData[i]);
								}
								Ext.resumeLayouts(true);
							}
							
							window_comp.close();
					}else{
						return;
					}
		}});
	}
	
	//검색
	var searchUserBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchUserBtn' ,ui:'correct'});
	//검색 초기화
	var searchUserInitBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.reset' })	,id:'searchUserInitBtn' });
	//적용
	var conFirmBtn	 	= createBtnComp({visible: true,label:"적용" ,id:"conFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	//사용자 추가
	var addSeluserBtn	= createIconBtnComp({id:'addSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif' 	,margin : '2 2 2 0'});
	//사용자 삭제
	var deSeluserBtn	= createIconBtnComp({id:'deSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif' ,margin : '2 0 2 2'});
	
	addSeluserBtn.vline 	= false;
	deSeluserBtn.vline 	= false;
	
	var searchContainer		= createUnionCodeComboBoxTextContainer({label : '검색', comboboxName: 'searchKey', textFieldName: 'searchValue', code_grp_id: 'SEARCH_USER', widthFactor: 0.25 });
	
	var searchKey = searchContainer.getComboBoxField();
	var searchValue = searchContainer.getTextField();
	
	var setSearchFormProp 	= {
								 id				: id+'SearchForm'
								,columnSize	: 1
								,formBtns	: [searchUserBtn, searchUserInitBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [{colspan:1, tdHeight: 30, item : searchContainer}]
								,enterFunction : searchUserList
							  }
	//검색폼
	var searchForm	= createSeachFormComp(setSearchFormProp);
	//추가,삭제 버튼 컨테이너
	var btnContainer		= createHorizonPanel({align : 'center' ,panelItems : [addSeluserBtn,deSeluserBtn]})
	
	//담당자 리스트
	var setUserGridProp 	= {	 
									 id						: id+"UserList"
									,title					: '담당자정보'
									,gridHeight			: 190
									,resource_prefix	: 'grid.itam.managerBaseList'
									,url					: '/itg/itam/opms/selectOpmsMngUserList.do'
									,params				: {selUserList : selParentData}
									,pagingBar 			: true
									,multiSelect			: true
									,selModel			: true
									,border 				: true
									,pageSize 			: 5
								};
	
	userGrid = createGridComp(setUserGridProp);
	
	//선택된 담당자 리스트
	var setSelGridProp 	= {	 
									 id						: id+"SelUserList"
									,title					: '담당자정보'
									,gridHeight			: 190
									,resource_prefix	: 'grid.itam.managerSelList'
									,isMemoryStore	: true
									,pagingBar 			: false
									,selModel			: true
									,multiSelect			: true
									,border 				: true
									};
	
	selUserGrid = createGridComp(setSelGridProp);
	
	
	//그리드 로드시.. 선택된 사용자 정보를 셋해준다.. 팝업이 열리고 최초 한번만..
	var selUserGirdLoadFn = function(){
										if(selParentData.length>0){
											for(var i=0;i<selParentData.length;i++){
												selUserGrid.store.add(selParentData[i]);
											}
										}
										selUserGrid.store.un("load",selUserGirdLoadFn);
									}
	
	selUserGrid.store.on("load",selUserGirdLoadFn);

	
	var complexPanel = createFlexPanel({items : [searchForm,userGrid,btnContainer,selUserGrid]});
	
	var codePopupProp		= {
									id:   id+"Pop"
									,title: '담당자선택'
									,width: 700
									,height: 568
									,modal: true
									,layout: 'fit'
									,buttonAlign: 'center'
									,bodyStyle: 'background-color: white; '
									,buttons: [conFirmBtn]
									,closeBtn : true
									,items: complexPanel
									,resizable:true
							  }
	
	window_comp = createWindowComp(codePopupProp);
	//버튼 이벤트
	
	
	attachBtnEvent(conFirmBtn	,confirmMng);
	attachBtnEvent(searchUserBtn	,searchUserList);
	attachBtnEvent(searchUserInitBtn	,searchUserInit);
	attachBtnEvent(addSeluserBtn	,addSeluser);
	attachBtnEvent(deSeluserBtn		,deSeluser);
	
	window_comp.show();
}

function gridComboChange( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {

	var selIndex = tdEl.rowIdx;
	var selValue = tdEl.value;
	var gridIndex = gridCmp.view.store.data.items.length;
	//gridCmp.view.store.data.items[0].data.MAIN_YN
	if(selValue == "Y"){
		for(var i=0;i<gridIndex;i++){
			gridCmp.view.store.data.items[i].data.MAIN_YN = "N";
		}
		gridCmp.view.store.data.items[selIndex].data.MAIN_YN = "Y";
		gridCmp.view.store.update();
	}else{
		for(var k=0;k<gridIndex;k++){
			if(gridCmp.view.store.data.items[k].data.MAIN_YN == "Y"){
				for(var j=k;j<gridIndex;j++){
					gridCmp.view.store.data.items[j].data.MAIN_YN == "N"
				}
				return;
			}
		}
		gridCmp.view.store.update();
	}

	//alert('0');
}
/**
 * 사용자 선택 팝업 (자동화 원장 전용...)
 * @param compProperty
 */
function createUserListPop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	var id			= that["id"];
	var conf_id		= that["conf_id"];
	var userGrid	= "";
	var selUserGrid= "";
	var window_comp= "";
	var parentGrid 	= Ext.getCmp(id);
	var selParentData= parentGrid.getGridDataUpper();
	
	var searchUserList = function (){
		
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//추가
	var addSeluser = function (){
		var paramMap = searchForm.getInputData();
		var selUserGridStore = selUserGrid.store;
		var selRows 	= gridSelectedRows(userGrid.id);
		
		if(isSelected(userGrid.id)){
			Ext.suspendLayouts();

			for(var i=0;i<selRows.length;i++){
				selUserGrid.store.add(selRows[i]);
			}
			var selUserData = selUserGrid.getGridDataUpper();
			var selUserAmount = selUserData.length;
			Ext.resumeLayouts(true);
			selUserGridStore.update();
		}else{
			showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
			return;
		}
		
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//삭제
	var deSeluser = function (){
		var selUserGridStore = selUserGrid.store;
		var paramMap = searchForm.getInputData();
		var selRows 	= gridSelectedRows(selUserGrid.id);
		
		if(isSelected(selUserGrid.id)){
			Ext.suspendLayouts();
			
			for(var i=0;i<selRows.length;i++){
				selUserGrid.store.remove(selRows[i]);
			}
			
			
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : "경고",msg : "선택된 정보가 없습니다." ,type : "ALERT"});
			return;
		}
		if(selUserGridStore.count()>0){
			paramMap["selUserList"] =  selUserGrid.getGridDataUpper();
		}
		userGrid.searchList(paramMap);
	}
	
	//적용
	var confirmMng = function (){
		showMessageBox({msg : "적용하시겠습니까?" ,type : "CONFIRM" ,fn : function(btns){
					if(btns == "ok"){
							var selUserData = selUserGrid.getGridDataUpper();
							var useRateTot = 0;
							for(var i=0;i<selUserData.length;i++){
								useRateTot += Number(selUserGrid.selModel.store.data.items[i].data.USE_RATE);
							}
							parentGrid.store.removeAll();
							if(selUserData.length>0){
								Ext.suspendLayouts();
								for(var i=0;selUserData.length>i;i++){
									parentGrid.store.add(selUserData[i]);
								}
								Ext.resumeLayouts(true);
							}
							window_comp.close();
					}else{
						return;
					}
		}});
	}
	
	//검색
	var searchUserBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchUserBtn' ,ui:'correct'});
	//적용
	var conFirmBtn	 	= createBtnComp({visible: true,label:"적용" ,id:"conFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	//사용자 추가
	var addSeluserBtn	= createIconBtnComp({id:'addSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif' 	,margin : '2 2 2 0'});
	//사용자 삭제
	var deSeluserBtn	= createIconBtnComp({id:'deSeluserBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif' ,margin : '2 0 2 2'});
	
	//분류 선택 콤보박스(id, codetype, label, name)
	var upcode =  contractComboBox('search_up_code', 'up_code', '대분류', 'UP_CODE');
	var subcode =  contractComboBox('search_sub_code', 'sub_code', '중분류', 'SUB_CODE');
	 
	
	addSeluserBtn.vline = false;
	deSeluserBtn.vline 	= false;
	
	//검색 ==
	//검색조건
	var searchKey		= createCodeComboBoxComp({name:'searchKey'	,code_grp_id:'SEARCH_USER'	,hideLabel : true 	,width : 100 ,attachAll : true});
	//검색값
	var searchValue	= createTextFieldComp({name:'searchValue'	,padding 	: '0 0 0 5' ,width : 200 ,hideLabel : true });
	//컨테이너
	var searchContainer		= createUnionFieldContainer({label : '검색' ,items : [searchKey,searchValue] ,id : "searchContainer"});
	
	var setSearchFormProp 	= {
								 id				: id+'SearchForm'
								,columnSize	: 2
								
								,formBtns	: [searchUserBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [{colspan:1,tdHeight: 30, item : upcode},{colspan:1,tdHeight: 30, item : subcode}]
								,enterFunction : searchUserList
							  };
	//검색폼
	var searchForm	= createSeachFormComp(setSearchFormProp);
	//추가,삭제 버튼 컨테이너
	var btnContainer		= createHorizonPanel({align : 'center' ,panelItems : [addSeluserBtn,deSeluserBtn]});
	
	attachCustomEvent('change',upcode,comboUpCodeSelect);
		
	
	//담당자 리스트
	var setUserGridProp 	= {	 
									 id						: id+"UserList"
									,title					: '사용자정보'
									,gridHeight			: 190
									,resource_prefix	: 'grid.itam.userBaseList'
									,url					: '/itg/itam/opms/selectOpmsUserList.do'//사용자정보 관련 서비스 붙일 것.
									,params				: {selUserList : selParentData}
									,pagingBar 			: true
									,multiSelect			: true
									,selModel			: true
									,border 				: true
									,pageSize 			: 5
									//, actionIcon: '${context}/itg/base/images/ext-js/common/icons/bullet_search.gif'
									//, actionBtnEvent: actionBtnEvent
								};
	
	userGrid = createGridComp(setUserGridProp);
	
	//선택된 담당자 리스트
	var setSelGridProp 	= {	 
									 id						: id+"SelUserList"
									,title					: '사용자정보'
									,gridHeight			: 190
									,resource_prefix	: 'grid.itam.userSelList'
									,isMemoryStore	: true
									,pagingBar 			: false
									,selModel			: true
									,multiSelect		: true
									,border 			: true
									,cellEditing		: true
									//, actionIcon: '${context}/itg/base/images/ext-js/common/icons/bullet_search.gif'
									//, actionBtnEvent: actionBtnEvent
									};
	
	selUserGrid = createGridComp(setSelGridProp);
	
	
	//그리드 로드시.. 선택된 사용자 정보를 셋해준다.. 팝업이 열리고 최초 한번만..
	var selUserGirdLoadFn = function(){
										if(selParentData.length>0){
											for(var y=0;y<selParentData.length;y++){
												selUserGrid.store.add(selParentData[y]);
											}
										}
										selUserGrid.store.un("load",selUserGirdLoadFn);
									};
	
	selUserGrid.store.on("load",selUserGirdLoadFn);

	
	var complexPanel = createFlexPanel({items : [searchForm,userGrid,btnContainer,selUserGrid]});
	
	var codePopupProp		= {
											id:   id+"Pop"
											,title: '사용자선택'
											,width: 700
											,height: 568
											,modal: true
											,layout: 'fit'
											,buttonAlign: 'center'
											,bodyStyle: 'background-color: white; '
											,buttons: [conFirmBtn]
											,closeBtn : true
											,items: complexPanel
											,resizable:true
									  };
	
	window_comp = createWindowComp(codePopupProp);
	//버튼 이벤트
	
	
	attachBtnEvent(conFirmBtn	,confirmMng);
	attachBtnEvent(searchUserBtn	,searchUserList);
	attachBtnEvent(addSeluserBtn	,addSeluser);
	attachBtnEvent(deSeluserBtn		,deSeluser);
	
	window_comp.show();
}
function actionBtnEvent(grid, rowIndex, colIndex){
	var record = grid.getStore().getAt(rowIndex);
	var rowData = record.data;
	if( rowData["ASSIGNED_LICENCE_CNT"] > 0 ){
		openSelSwAssetLicenceAssignedPop(rowData);
	}else{
		alert("할당된 라이선스 정보가 없습니다.");
	}
}
function contractComboBox(id, codeType, label, name){
	var param = {};
		param['code_type'] = codeType;
		
	var contract_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/itam/opms/searchCodeDataList.do', //코드 서비스 붙일 것.
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var contractProp_val = {label: label, id: id, name: name, width: 250, attachAll:true};
		contractProp_val['proxy'] = contract_proxy;
		contractProp_val['valueField'] = 'CODE_ID';
	    contractProp_val['displayField'] = 'CODE_TEXT';
	    
	   var contractComboBox =  createComboBoxComp(contractProp_val);
	   
	   return contractComboBox;
}


function comboUpCodeSelect(){
	
	var sel_up_code = Ext.getCmp('search_up_code').value;
	var sub_code_cmp = Ext.getCmp('search_sub_code');
	sub_code_cmp.store.proxy.jsonData['sel_up_code'] = sel_up_code;
	sub_code_cmp.store.load();
	sub_code_cmp.setValue("");
}
	
/**
 * 연관 서비스 팝업
 * @param compProperty
 */
function createRelServicePop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	var id			= that["target_id"];
	var targetGrid = Ext.getCmp(id);
	var confId		= that["conf_id"];
	var selParentData = targetGrid.getGridDataUpper(); 
	var serviceGrid		= "";
	var selServiceGrid	= "";
	var windowComp	= "";
	var searchForm	= "";
	
	var searchServiceList = function (){
		var selServiceGridStore = selServiceGrid.store;
		var paramMap = searchForm.getInputData();
		if(selServiceGridStore.count()>0){
			paramMap["selServiceList"] =  selServiceGrid.getGridDataUpper();
		}
		serviceGrid.searchList(paramMap);
	}
	
	var searchServiceInit = function (){
		searchForm.initData();
	}
	
	//추가
	var addSelService = function (){
		var paramMap = searchForm.getInputData();
		var selServiceGridStore = selServiceGrid.store;
		var selRows 	= gridSelectedRows(serviceGrid.id);
		
		if(isSelected(serviceGrid.id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				selServiceGridStore.add(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00013' })
							, msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }) ,type : "ALERT"});
			return;
		}
		
		if(selServiceGridStore.count()>0){
			paramMap["selServiceList"] =  selServiceGrid.getGridDataUpper();
		}
		serviceGrid.searchList(paramMap);
	}
	
	//삭제
	var deSelService = function (){
		var selServiceGridStore = selServiceGrid.store;
		var paramMap = searchForm.getInputData();
		var selRows 	= gridSelectedRows(selServiceGrid.id);
		
		if(isSelected(selServiceGrid.id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				selServiceGridStore.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00013' })
							, msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }) ,type : "ALERT"});
			return;
		}
		if(selServiceGridStore.count()>0){
			paramMap["selServiceList"] =  selServiceGrid.getGridDataUpper();
		}
		serviceGrid.searchList(paramMap);
	}
	
	//적용
	var confirmMng = function (){
		showMessageBox({msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00011' }) ,type : "CONFIRM" ,fn : function(btns){
					if(btns == "ok"){
							targetGrid.store.removeAll();
							var selServiceData = selServiceGrid.getGridDataUpper();
							if(selServiceData.length>0){
								Ext.suspendLayouts();
								for(var i=0;selServiceData.length>i;i++){
									selServiceData[i].RNUM = i+1;
									targetGrid.store.add(selServiceData[i]);
								}
								Ext.resumeLayouts(true);
							}
							windowComp.close();
					}else{
						return;
					}
		}});
	}
	
	//검색 버튼
	var searchServiceBtn= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchServiceBtn' ,ui:'correct'});
	//검색 초기화 버튼
	var searchServiceInitBtn= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.reset' })	,id:'searchServiceInitBtn' });
	//장비추가
	var addServiceBtn	= createIconBtnComp({id:'addServiceBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif' 	,margin : '2 2 2 0'});
	//장비삭제                 
	var delServiceBtn		= createIconBtnComp({id:'delServiceBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif' 	,margin : '2 0 2 2'});
	
	var conFirmBtn	 	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),id:"confConFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	
	addServiceBtn.vline 	= false;
	delServiceBtn.vline 	= false;
	
	//검색조건
	var custCombo	= createManualComboBoxComp({label : getConstText({ isArgs: true, m_key: 'res.00013' }) ,name:'cust_id'	,url : '/itg/itam/opms/selectServiceCustList.do ',valueField : 'CUST_ID',displayField : 'CUST_NM',width: 300 ,attachAll : true});
	
	var effectCombo	= createCodeComboBoxComp({label : getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00014' }),name:'service_effect',code_grp_id:'SERVICE_EFFECT'	 ,width : 205 ,attachAll : true});
	
	var searchContainer		= createUnionCodeComboBoxTextContainer({label : '검색', comboboxName: 'searchKey', textFieldName: 'searchValue', code_grp_id: 'SER_POP_SEARCH_TYPE', widthFactor: 0.5 });
	
	var searchKey = searchContainer.getComboBoxField();
	var searchValue = searchContainer.getTextField();
	
	var setSearchFormProp 	= {
								 id				: id+'SearchForm'
								,columnSize	: 1
								,formBtns	: [searchServiceBtn, searchServiceInitBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [	 //{colspan:1, tdHeight: 22, item : custCombo}
													//,{colspan:1, tdHeight: 22, item : effectCombo}
													//,
													{colspan:1, tdHeight: 22, item : searchContainer}
													]
								,enterFunction : searchServiceList
							  }
	//검색폼
	searchForm	= createSeachFormComp(setSearchFormProp);
	//추가,삭제 버튼 컨테이너
	var btnContainer		= createHorizonPanel({align : 'center',panelItems : [addServiceBtn,delServiceBtn]});
	
	//담당자 리스트
	var setServiceGridProp 	= {	 
									 id					: id+'ServiceList'
									,title				: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00015' })
									,gridHeight			: 195
									,resource_prefix	: 'grid.itam.serviceList'
									,url				: '/itg/itam/opms/selectOpmsServiceList.do'
									,params				: {selServiceList : selParentData}
									,pagingBar 			: true
									,multiSelect		: true
									,selModel			: true
									,border 			: true
									,pageSize 			: 10
								};
	
	serviceGrid = createGridComp(setServiceGridProp);
	
	//선택된 구성
	var setSelServiceGridProp 	= {	 
									 id					: id+'SelServiceList'
									,title				: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00015' })
									,gridHeight			: 195
									,resource_prefix	:  'grid.itam.selServiceList'
									,isMemoryStore		: true
									,pagingBar 			: false
									,selModel			: true
									,multiSelect		: true
									,border 			: true
									};
	
	selServiceGrid = createGridComp(setSelServiceGridProp);
	
	var complexPanel = createFlexPanel({items : [searchForm,serviceGrid,btnContainer,selServiceGrid]});
	// Viewport 생성
	
	var codePopupProp		= {
			id:   id+"Pop"
			,title: getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00016' })
			,width: 700
			,height: 600
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [conFirmBtn]
			,closeBtn : true
			,items: complexPanel
			,resizable:true
	  }

	
	windowComp = createWindowComp(codePopupProp);
//버튼 이벤트
//	버튼 이벤트
	
	//그리드 로드시.. 선택된 구성정보를 셋해준다.. 팝업이 열리고 최초 한번만..
	var selServiceGridLoadFn = function(selServiceGridStore){
										if(selParentData.length>0){
											Ext.suspendLayouts();
											for(var i=0;i<selParentData.length;i++){
												selServiceGridStore.add(selParentData[i]);
											}
											Ext.resumeLayouts(true);
										}
										selServiceGridStore.un("load",selServiceGridLoadFn);
									}
	
	selServiceGrid.store.on("load",selServiceGridLoadFn);
	
	attachBtnEvent(delServiceBtn	,deSelService);
	attachBtnEvent(addServiceBtn	,addSelService);
	attachBtnEvent(conFirmBtn	,confirmMng);
	attachBtnEvent(searchServiceBtn	,searchServiceList);
	attachBtnEvent(searchServiceInitBtn	,searchServiceInit);
	
	windowComp.show();
}

/** 연관 자산 상하위 팝업
* @param compProperty
**/
function createRelUpDownPop(compProperty){
	var that 		= convertDomToArray(compProperty);
	var id			= that["target_id"];
	var targetGrid = Ext.getCmp(id);
	var confId		= that["conf_id"];
	var selParentData = targetGrid.getGridDataUpper(); 
	var serviceGrid		= "";
	var selServiceGrid	= "";
	var windowComp	= "";
	var searchForm	= "";
	var class_type = that["class_type"];
	
	if(class_type == "HA"){
		
		var upDownMap ={};
		upDownMap['upDownValue'] = '동등';
		upDownMap['upDownName'] = 'M';
		createRelConfPop({class_type : that["class_type"] ,title : that["title"], target_id : that["target_id"] ,conf_id : that["conf_id"], upDownValue: upDownMap } );
	}else{
		var conFirmBtn	 	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }) ,id:"confConFirmBtn" 	,ui    : "correct"	,scale: "medium"});
		
		var upDownRadioComp = createCodeRadioComp({name:'UPDOWN', code_grp_id: 'UPDOWN', columnSize: 5});
		var complexPanel = createFlexPanel({items : [upDownRadioComp]});
		//적용
		var confirmMng = function (){
			var checkedValue = upDownRadioComp.getChecked();
			var upDownValue = checkedValue[0].getGroupValue();
			var upDownName = checkedValue[0].boxLabel;
			var upDownMap ={};
					
			upDownMap['upDownValue'] = upDownValue;
			upDownMap['upDownName'] = upDownName;
					
			windowComp.close();
					
			createRelConfPop({class_type : that["class_type"] ,title : that["title"], target_id : that["target_id"] ,conf_id : that["conf_id"], upDownValue: upDownMap } );
		}
		
		var codePopupProp		= {
				id:   id+"Pop"  
				,title: getConstText({ isArgs: true, m_key: 'res.label.attribute.00028' })
				,width: 280
				,height: 100
				,modal: true
				,layout: 'fit'
				,buttonAlign: 'center'
				,bodyStyle: 'background-color: white; '
				,buttons: [conFirmBtn]
				,closeBtn : true
				,items: [upDownRadioComp]
				,resizable:true
		  }
		
		windowComp = createWindowComp(codePopupProp);

		attachBtnEvent(conFirmBtn	,confirmMng);
		
		windowComp.show();
	}
}

/**
 * 연관 장비 팝업
 * @param compProperty
 */
function createRelConfPop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	var targetGrid = Ext.getCmp(that["target_id"]);
	var classType = that["class_type"];
	var confId		= that["conf_id"];
	var title			= that["title"];
	var upDownValue = that["upDownValue"];
	var selParentData = targetGrid.getGridDataUpper(); 
	var confGrid		= "";
	var selConfGrid	= "";
	var amClassTree	= "";
	var windowComp	= "";
	var searchForm	= "";
	
	var searchConfList = function (){
		
		var selConfGridStore = selConfGrid.store;
		var paramMap = searchForm.getInputData();
		var treeData = amClassTree.getSelectionModel().getSelection()[0];
		
		paramMap["conf_id"] = confId;
		if(treeData){
			paramMap["class_id"] = treeData.data.id;
		}else{
			paramMap["class_type"] = classType; 
		}
		
		if(selConfGridStore.count()>0){
			paramMap["selConfList"] =  selConfGrid.getGridDataUpper();
		}
		confGrid.searchList(paramMap);
	}
	
	var searchConfInit = function (){
		searchForm.initData();
	}
	
	//추가
	var addSelConf = function (){
		var paramMap 			= searchForm.getInputData();
		var selConfGridStore 	= selConfGrid.store;
		var selRows 				= gridSelectedRows(confGrid.id);
		var treeData 				= amClassTree.getSelectionModel().getSelection()[0];
		
		paramMap["conf_id"] = confId;
		paramMap["class_type"] = classType; 
		if(treeData){
			paramMap["class_id"] = treeData.data.id;
		}
		
		if(isSelected(confGrid.id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				selConfGridStore.add(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00013' })
							, msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }) ,type : "ALERT"});
			return;
		}
		
		if(selConfGridStore.count()>0){
			paramMap["selConfList"] =  selConfGrid.getGridDataUpper();
		}
		confGrid.searchList(paramMap);
	}
	
	//삭제
	var deSelConf = function (){
		var selConfGridStore = selConfGrid.store;
		var paramMap = searchForm.getInputData();
		var selRows 	= gridSelectedRows(selConfGrid.id);
		var treeData 				= amClassTree.getSelectionModel().getSelection()[0];
		
		paramMap["conf_id"] = confId;
		paramMap["class_type"] = classType; 
		if(treeData){
			paramMap["class_id"] = treeData.data.id;
		}
		
		if(isSelected(selConfGrid.id)){
			Ext.suspendLayouts();
			for(var i=0;i<selRows.length;i++){
				selConfGridStore.remove(selRows[i]);
			}
			Ext.resumeLayouts(true);
		}else{
			showMessageBox({title : getConstText({ isArgs: true, m_key: 'res.label.itam.opms.00013' })
							, msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00010' }) ,type : "ALERT"});
			return;
		}
		if(selConfGridStore.count()>0){
			paramMap["selConfList"] =  selConfGrid.getGridDataUpper();
		}
		confGrid.searchList(paramMap);
	}
	
	//적용
	var confirmMng = function (){
		showMessageBox({msg : getConstText({ isArgs: true, m_key: 'msg.itam.opms.00011' }) ,type : "CONFIRM" ,fn : function(btns){			
					if(btns == "ok"){
						targetGrid.store.removeAll();
						var selConfData = selConfGrid.getGridDataUpper();
						if(selConfData.length>0){
							Ext.suspendLayouts();
							for(var i=0;selConfData.length>i;i++){
								selConfData[i].RNUM = i+1;
								if(upDownValue != undefined){
									if(selConfData[i].UPDOWNVAL == null || selConfData[i].UPDOWNVAL == ""){
										selConfData[i].UPDOWNVAL = upDownValue.upDownValue;
										selConfData[i].UPDOWNNAME = upDownValue.upDownName;
									}
								}
								targetGrid.store.add(selConfData[i]);
							}
							Ext.resumeLayouts(true);
						}
						var gridTitle		= targetGrid.title;
						var relTitle			= gridTitle.substr(0,gridTitle.indexOf("["));
						targetGrid.setTitle(relTitle+"["+targetGrid.store.count()+"]");
						windowComp.close();
					}else{
						return;
					}
		}});
	}
	
	var amClassTreeNodeClick = function(obj,selData){
		
		searchForm.initData();
		var selConfGridStore = selConfGrid.store;
		
		var class_id = selData.data.id;
		var paramMap = [];
		
		paramMap["conf_id"] = confId;
		paramMap["class_id"] = class_id;
		
		if(selConfGridStore.count()>0){
			paramMap["selConfList"] =  selConfGrid.getGridDataUpper();
		}
		confGrid.searchList(paramMap);
	}
	
	//검색 버튼
	var searchConfBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchUserBtn' ,ui:'correct'});
	//검색초기화 버튼
	var searchConfInitBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.reset' })	,id:'searchConfInitBtn'});
	
	//장비추가
	var addConfBtn		= createIconBtnComp({id:'addConfBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveDownBtn.gif' 		,margin : '2 2 2 0'});
	//장비삭제                createIconBtnComp
	var delConfBtn		= createIconBtnComp({id:'delConfBtn'	,icon :'/itg/base/images/ext-js/simple/btn-icons/moveUpBtn.gif' 	,margin : '2 0 2 2'});
	
	var conFirmBtn	 	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),id:"confConFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	
	addConfBtn.vline 	= false;
	delConfBtn.vline 	= false;
	
	//검색 ==
	//검색조건
	var custCombo	= createReqZoneComboBoxComp({label: getConstText({isArgs: true, m_key: 'res.label.system.00023'}), id: 'cust_id', name: 'cust_id', width: 200, attachAll: true})
	var operSate		= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.oper.00068'}) ,name:'oper_state'	,code_grp_id:'OPER_STATE'		,width : 205 ,attachAll : true});
	var tangibleAssetYn = createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00017'}) ,name:'tangible_asset_yn'	,code_grp_id:'YN'		,width : 205 ,attachAll : true});
	
	var searchContainer		= createUnionCodeComboBoxTextContainer({label : '검색', comboboxName: 'searchKey', textFieldName: 'searchValue', code_grp_id: 'CIPOP_SEARCH_TYPE', widthFactor: 0.5 });
	
	var searchKey = searchContainer.getComboBoxField();
	var searchValue = searchContainer.getTextField();
	
	var setSearchFormProp 	= {
								 id				: classType+'SearchForm'
								,columnSize	: 1
								,width 		: 685
								,formBtns	: [searchConfBtn, searchConfInitBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [/*{colspan:1, tdHeight: 22, item : custCombo}
								               	  ,{colspan:1, tdHeight: 22, item : operSate}
								               	  ,{colspan:1, tdHeight: 22, item : tangibleAssetYn}*/
												  {colspan:1, tdHeight: 22, item : searchContainer}]
								,enterFunction : searchConfList
							  }
	//검색폼
	searchForm	= createSeachFormComp(setSearchFormProp);
	//추가,삭제 버튼 컨테이너
	var btnContainer		= createHorizonPanel({align : 'center', width : 685,panelItems : [addConfBtn,delConfBtn]});
	
	var setTreeProp = {
			 id 			: 'amClassTree'
			,url 			: '/itg/itam/opms/selectRelConfClassTree.do'
			,title 			: title + getConstText({isArgs: true, m_key: 'res.00015'})
			,dynamicFlag 	: false
			,height			: 550
			,searchable 	: true
			,expandAllBtn 	: true
			,collapseAllBtn : true
			,params			: {class_type : classType , conf_id : confId}
			,rnode_text 	: title + getConstText({isArgs: true, m_key: 'res.00015'})
			,expandLevel	: 2
			,searchableTextWidth : 60
	}
	amClassTree = createTreeComponent(setTreeProp);
	attachCustomEvent('select', amClassTree, amClassTreeNodeClick);
	
	//구성자원 리스트
	var setConfGridProp 	= {	 
									 id					: classType+"RelConfList"
									,title				: title + getConstText({isArgs: true, m_key: 'res.label.itam.opms.00018'})
									,gridHeight			: 195
									,gridWidth			: 685
									,resource_prefix	:  'grid.itam.conf'+classType
									,url				: '/itg/itam/opms/selectOpmsConfList.do'
									,params				: {selConfList : selParentData,class_type : classType , conf_id : confId, tangible_asset_yn : 'Y'}
									,pagingBar 			: true
									,multiSelect		: true
									,selModel			: true
									,border 			: true
									,pageSize 			: 10
								};
	
	confGrid = createGridComp(setConfGridProp);
	
	//선택된 구성
	var setSelConfGridProp 	= {	 
									 id					: classType+"SelConfList"
									,title				: title + getConstText({isArgs: true, m_key: 'res.label.itam.opms.00018'})
									,gridHeight			: 195
									,gridWidth			: 685
									,resource_prefix	:  'grid.itam.conf'+classType
									,isMemoryStore		: true
									,pagingBar 			: false
									,selModel			: true
									,multiSelect		: true
									,border 			: true
									};
	
	selConfGrid = createGridComp(setSelConfGridProp);
	
	//뷰포트 생성
	var viewportProperty = {
			borderId	: classType+'BorderId' 
			,viewportId : classType+'ViewId' 
			,west: {
				minWidth: 180 
				,maxWidth: 685 
				,items : [amClassTree] 
			},
			 center: { 
				 	minWidth: 180 
					,maxWidth: 685 
					,items : [searchForm,confGrid,btnContainer,selConfGrid] 
			}
	}
	// Viewport 생성
	
	var codePopupProp		= {
			id:   classType+"Pop"
			,title: getConstText({isArgs: true, m_key: 'res.label.itam.opms.00019'}) + ' '+ title+ ' ' + getConstText({isArgs: true, m_key: 'res.common.choice'})
			,width: 900
			,height: 600
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [conFirmBtn]
			,closeBtn : true
			,items: createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : false} )
			,resizable:true
	  }

	windowComp = createWindowComp(codePopupProp);
//	버튼 이벤트
	
	//그리드 로드시.. 선택된 구성정보를 셋해준다.. 팝업이 열리고 최초 한번만..
	var selConfGridLoadFn = function(selConfGridStore){
										if(selParentData.length>0){
											for(var i=0;i<selParentData.length;i++){
												selConfGridStore.add(selParentData[i]);
											}
										}
										selConfGridStore.un("load",selConfGridLoadFn);
									}
	
	selConfGrid.store.on("load",selConfGridLoadFn);
	
	attachBtnEvent(delConfBtn	,deSelConf);
	attachBtnEvent(addConfBtn	,addSelConf);
	attachBtnEvent(conFirmBtn	,confirmMng);
	attachBtnEvent(searchConfBtn	,searchConfList);
	attachBtnEvent(searchConfInitBtn	,searchConfInit);
	
	windowComp.show();
}


/** 분류체계 속성관리 버튼 선택 트리 팝업
* @param compProperty
**/
function selectGrpBtnPop(compProperty){
	var that 		= convertDomToArray(compProperty);
	var applyBtn = createBtnComp({ visible : true, label : getConstText({ isArgs : true, m_key : 'btn.common.apply'}), id : "applyBtn"});
		
	var confirmMng = function (){
		// 선택된 Tree 데이터를 타켓Grid에 복사 ( 소스Tree, 타켓Grid, append속성, 노데이타 메시지,
		// 검증키 )
		/*treeCheckNodeCopyToGrid('entitiyGrpBtnTree',
				that['openerGridId'], true, null, {
					primaryKeys : [ 'BTN_ID' ]
				});*/
		
		clearGridData(that['openerGridId']);
		selTreeNodeCopyToGridOnlyOne('entitiyGrpBtnTree',
			that['openerGridId'], false, null, {
			primaryKeys : [ 'BTN_ID' ]
		});	
		windowComp.close();
	}
	
	var setBtnTreeProp = {
			id : 'entitiyGrpBtnTree',
			url : '/itg/itam/opms/selectOpmsBtnTree.do',
			title : getConstText({ isArgs: true, m_key: 'res.label.itam.service.00001' }),
			//height: (height) ? height : null,
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : true,
			autoLoad: true,
			collapseAllBtn : true,
			//params: (param) ? param : null,
			rnode_text : '#springMessage("res.00003")',
			expandLevel: 3
	}
	var entitiyGrpBtnTree = createTreeComponent(setBtnTreeProp);
	
	var codePopupProp		= {
			id:   "entitiyGrpBtnTreePop"
			,width: 400
			,height: 400
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [applyBtn]
			,closeBtn : true
			,items: [entitiyGrpBtnTree]
			,resizable:true
	}
	
	windowComp = createWindowComp(codePopupProp);

	attachBtnEvent(applyBtn ,confirmMng);
		
	windowComp.show();
}

function confNmCodePop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	
	var id			= that["id"];
	var title			= that["title"];
	var conf_id		= that["conf_id"];
	var asset_id	= that["asset_id"];
	var authType	= that["authType"];
	var windowComp = "";
	var targetForm = Ext.getCmp(id);
	var targetFormData = targetForm.getInputData();
	var confCodeForm = "";
	var editorFlag = "";
	var btn = {};
	
	var conFirmBtn		= createBtnComp({visible: true,label: getConstText({isArgs: true, m_key: 'btn.common.apply'}) ,id: id+"ConFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	var initBtn			= createBtnComp({visible: true,label: getConstText({isArgs: true, m_key: 'btn.common.reset'}) ,id: id+"InitBtn" });
	
	if(authType == "U"){
		editorFlag = true;
		btn = { "conFirmBtn" : conFirmBtn	,"initBtn" 		: initBtn};
	}else{
		editorFlag = false;
	}
	
	var confirmFn	= function(){
		if(!confCodeForm.isValid()){
			showMessageBox({msg : getConstText({isArgs: true, m_key: 'msg.itam.opms.00008'}) ,type : "ALERT"});
			return;
		}else{
			showMessageBox({msg : getConstText({isArgs: true, m_key: 'msg.itam.opms.00011'}) ,type : "CONFIRM" ,fn : function(btns){
				if(btns == "ok"){
					var formData = confCodeForm.getInputData();
					
					formData["CENTER_LOC_NM"] = formData["CENTER_TYPE"];
					
					formData["PHYSI_LOC_NM"] =	 formData["BUILDING"]
														   	+formData["FLOOR"]
															+formData["RACK_PERIOD"]
															+formData["RACK_ORDER"]
															+formData["CHASSIS"]
															+formData["MODULE"];
					
					formData["NW_LINE_TYPE_NM"] = formData["NW_LINE_TYPE"];
					formData["INSTALL_PERIOD_NM"] = formData["INSTALL_PERIOD"];
					formData["CONF_TYPE_NM"] = formData["CONF_TYPE"];
					formData["CONF_FUNC_NM"] = formData["CONF_FUNC"];
					formData["SERIAL_NM"] = formData["SERIAL"];
					formData["HA_NM"] = formData["HA"];
					
					formData["CONF_NM_CODE"] = formData["CENTER_LOC_NM"]+"-"
					 										+formData["PHYSI_LOC_NM"]+"-"
					 										+formData["NW_LINE_TYPE_NM"]+"-"
					 										+formData["INSTALL_PERIOD_NM"]+"-"
					 										+formData["CONF_TYPE_NM"]+"-"
															+formData["CONF_FUNC_NM"]+"-"
															+formData["SERIAL_NM"]+"-"
															+formData["HA_NM"];
					
					targetForm.setDataForMap(formData);
					windowComp.close();
				}else{
					return;
				}
			}});
		}
	}
	
	var initFn	= function(){
		confCodeForm.initData();
	}
	
	var centerType 	= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00020'})	,name:'CENTER_TYPE'	,code_grp_id:'CENTER_TYPE'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var building 		= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00021'})			,name:'BUILDING'			,code_grp_id:'BUILDING'			,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var floor 			= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00022'})			,name:'FLOOR'				,code_grp_id:'FLOOR'			,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var rackPeriod 	= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00023'})	,name:'RACK_PERIOD'	,code_grp_id:'RACK_PERIOD'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var rackOrder		= createTextFieldComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00024'})	,name:'RACK_ORDER'	,notNull:true	,width:150	,labelWidth : 60 ,vtype : "number" 	,maxLength : 2 ,minLength : 2 });
	var chassis			= createTextFieldComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00025'})		,name:'CHASSIS'		,notNull:true	,width:150	,labelWidth : 60 ,vtype : "number"	,maxLength : 2 ,minLength : 2 });
	var module			= createTextFieldComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00026'})		,name:'MODULE'		,notNull:true	,width:150	,labelWidth : 60 ,vtype : "number"	,maxLength : 2 ,minLength : 2 });
	var nwLineType 	= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00027'})		,name:'NW_LINE_TYPE'	,code_grp_id:'NW_LINE_TYPE'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var installPeriod 	= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00028'})	,name:'INSTALL_PERIOD'	,code_grp_id:'INSTALL_PERIOD'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var confType 		= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00029'})	,name:'CONF_TYPE'	,code_grp_id:'LOC_CONF_TYPE'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	//var confFunc 		= createOutConfTechPopComp({label : "장비기능"	,name:'CONF_FUNC'	,code_grp_id:'CONF_FUNC'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var confFunc 		= createOutConfTechPopComp({id:'techSelectPop', popUpText: getConstText({isArgs: true, m_key: 'res.label.itam.opms.00030'}), label:getConstText({isArgs: true, m_key: 'res.label.itam.opms.00031'}), name:'CONF_FUNC', width: 150});
	var serial 			= createTextFieldComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00032'})	,name:'SERIAL'		,notNull:true	,width:150	,labelWidth : 60 ,vtype : "number"	,maxLength : 2 ,minLength : 2});
	//var serial 			= createCodeComboBoxComp({label : "일련번호"	,name:'SERIAL'			,code_grp_id:'SERIAL'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	var ha	 			= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00033'})	,name:'HA'				,code_grp_id:'HA'	,notNull:true 	,width:150	,attachChoice:true,labelWidth : 60});
	
	
	var setEditorFormProp 	= {
								 id			: id+"PopForm"
								,title		: title
								,editOnly	: true
								,border		: true
								,formBtns	: [btn["initBtn"]]
								,columnSize	: 4
								,tableProps : [
								               		 {colspan:1, tdHeight: 25, item : centerType}
								               		,{colspan:1, tdHeight: 25, item : building}
								               		,{colspan:1, tdHeight: 25, item : floor}
								               		,{colspan:1, tdHeight: 25, item : rackPeriod}
								               		
								               		,{colspan:1, tdHeight: 25, item : rackOrder}
								               		,{colspan:1, tdHeight: 25, item : chassis}
								               		,{colspan:2, tdHeight: 25, item : module}
								               		
								               		,{colspan:1, tdHeight: 25, item : nwLineType}
								               		,{colspan:1, tdHeight: 25, item : installPeriod}
								               		,{colspan:1, tdHeight: 25, item : confType}
								               		,{colspan:1, tdHeight: 25, item : confFunc}
								               		
								               		,{colspan:1, tdHeight: 25, item : serial}
								               		,{colspan:3, tdHeight: 25, item : ha}
								               	]
							  }
	confCodeForm = createEditorFormComp(setEditorFormProp);
	
	var complexPanel = createFlexPanel({items : [confCodeForm]});
	// Viewport 생성
	
	var codePopupProp		= {
			id:   id+"Pop"
			,title:title
			,width: 650
			,height: 215
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [btn["conFirmBtn"]]
			,closeBtn : true
			,items: complexPanel
			,resizable:true
	 }
	
	windowComp = createWindowComp(codePopupProp);
	attachBtnEvent(conFirmBtn	,confirmFn);
	attachBtnEvent(initBtn	,initFn);
	
	windowComp.show();
	
	confCodeForm.setDataForMap(targetFormData);
	
	formEditable({id : id+"PopForm" ,editorFlag : editorFlag});
}



/**
 * 원장 팝업 - 사업등록
 * 
 * @param {}
 *            setPopUpProp
 * @return {}
 */
function createOpmsProjectSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var projectSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.title.itam.project'
			}), // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.project.pop', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			pagingBar : true,
			pageSize : 10
		};

		var projectGrid = createGridComp(setGridProp);

		var projectSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'ProjectBtn',
			ui : 'correct'
		});
		attachBtnEvent(projectSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ projectSearchBtn ],
			enterFunction : clickProjectSearchBtn,
			columnSize : 2,
			tableProps : [ {
				colspan : 1,
				item : createTextFieldComp({
					label : getConstText({isArgs: true, m_key: 'res.label.itam.project.00002'}),
					name : 'search_project_nm',
					width : 250
				})
			}, {
				colspan : 1,
				item : createTextFieldComp({
					label :getConstText({isArgs: true, m_key: 'res.label.itam.project.00001'}),
					name : 'search_project_id',
					width : 250
				})
			} ]
		}

		var project_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ project_search_form, projectGrid ]
		})

		var targetProjectIdField = that['targetProjectIdField'];
		var targetProjectNameField = that['targetProjectNameField'];

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				projectSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button(
				{
					text : getConstText({
						isArgs : true,
						m_key : 'btn.common.apply'
					})
					, ui : 'correct'
					, handler : function() {
						var selectedRecords = projectGrid.view.selModel
								.getSelection();
						if (selectedRecords.length < 1) {
							showMessage(getConstText({
								isArgs : true,
								m_key : 'msg.itam.project.00013'
							}));
						} else {
							dbSelectEvent();
						}
					}
				});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();

				projectGrid.searchList(paramMap);
				break;

			}
		}
		
		function clickProjectSearchBtn() {
			actionLink('searchAttr');
		}
		
		function dbSelectEvent() {
			var selectedRecords = projectGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.itam.project.00013'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var project_id = selectedRecords[0].raw.PROJECT_ID;
					var project_nm = selectedRecords[0].raw.PROJECT_NM;
					targetProjectIdField.setValue(project_id);
					targetProjectNameField.setValue(project_nm);
					projectSelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', projectGrid, dbSelectEvent);

		projectSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 600,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = projectSelectPop;

	} else {
		projectSelectPop = popUp_comp[that['id']];
	}

	return projectSelectPop;
}


/**
 * 원장 팝업 - 공급계약조회
 * 
 * @param {}
 *            setPopUpProp
 * @return {}
 */
function createOpmsSupplySelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var supplySelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.label.itam.supply.00001'
			}), // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.supply.pop', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			pagingBar : true,
			pageSize : 10
		};

		var supplyGrid = createGridComp(setGridProp);

		var supplySearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'SupplyBtn',
			ui : 'correct'
		});
		attachBtnEvent(supplySearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ supplySearchBtn ],
			columnSize : 2,
			enterFunction : clickSupplySearchBtn,
			tableProps : [ {
				colspan : 1,
				item : createTextFieldComp({
					label : getConstText({
						isArgs : true,
						m_key : 'res.label.itam.supply.00003'
					}),
					name : 'supply_nm',
					width : 250
				})
			}, {
				colspan : 1,
				item : createTextFieldComp({
					label : getConstText({
						isArgs : true,
						m_key : 'res.label.itam.oper.00017'
					}),
					name : 'supply_id',
					width : 250
				})
			} ]
		}

		var supply_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ supply_search_form, supplyGrid ]
		})

		var targetSupplyIdField = that['targetSupplyIdField'];
		var targetSupplyNameField = that['targetSupplyNameField'];

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				supplySelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button(
				{
					text : getConstText({
						isArgs : true,
						m_key : 'btn.common.apply'
					})
					, ui : 'correct'
					, handler : function() {
						var selectedRecords = supplyGrid.view.selModel
								.getSelection();
						if (selectedRecords.length < 1) {
							showMessage(getConstText({
								isArgs : true,
								m_key : 'msg.common.00002'
							}));
						} else {
							dbSelectEvent();
						}
					}
				});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();

				supplyGrid.searchList(paramMap);
				break;

			}
		}
				
		function clickSupplySearchBtn() {
			actionLink('searchAttr');
		}		

		function dbSelectEvent() {
			var selectedRecords = supplyGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.common.00002'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var supply_id = selectedRecords[0].raw.SUPPLY_ID;
					var supply_nm = selectedRecords[0].raw.SUPPLY_NM;
					targetSupplyIdField.setValue(supply_id);
					targetSupplyNameField.setValue(supply_nm);
					supplySelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', supplyGrid, dbSelectEvent);

		supplySelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 600,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = supplySelectPop;

	} else {
		supplySelectPop = popUp_comp[that['id']];
	}

	return supplySelectPop;
}


/**
 * 랙모델 조회 팝업
 * 
 * @param {}
 *            setPopUpProp
 * @return {}
 */
function searchRackModelSelectPop(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var selectRackPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {

		var setGridProp = {	
				id: that['id'] + '_grid',
				title: getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00004' }),
				gridHeight: 350,
				resource_prefix: 'grid.itam.locationManage.rackInfo',
				url: '/itg/itam/locationManage/searchRackList.do',
				pagingBar : true,
				pageSize : 10
		};

		var rackSearchGrid = createGridComp(setGridProp);
		
		var rackSearchBtn   = createBtnComp({visible: true, label:getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:that['id']+'SearchBtn', ui:'correct'});
		attachBtnEvent(rackSearchBtn, actionLink,  'searchAttr');
		
		var setSearchFormProp = {
				id: that['id']+'SearchForm',
				formBtns: [rackSearchBtn],
				enterFunction : clickRackSearchBtn,
				columnSize: 2,
				tableProps : [
					             {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00001' }), width: 250, name:'model_nm'})}
					            ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00008' }), width: 250, name:'rack_type'})}
				             ]
		}

		var rackSearchForm = createSeachFormComp(setSearchFormProp);


		var panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			//flex:1 ,
			items: [rackSearchForm,rackSearchGrid]
		});

		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			items: [panel]
		});

		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
			handler: function() {
				dbSelectEvent();	
			}
		});

		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				selectRackPop.close();
			}
		});		
		
		/*** Private Function Area Start *********************/		
		
		function actionLink(command) {

			switch(command) {		
				case 'searchAttr' :
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
			
					rackSearchGrid.searchList(paramMap);
				break;
			
			}
		}
		
		function clickRackSearchBtn() {
			actionLink('searchAttr');
		}		
		
		var tagetIdField = that['tagetIdField'];
		var tagetNameField = that['tagetNameField'];
		var tagetVendorField = that['tagetVendorField'];
		
		function dbSelectEvent(){
			var selectedRecords = rackSearchGrid.view.selModel.getSelection();
			if(selectedRecords.length <1){
				showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.rack.00008' }));
			}else{
				
				if(that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
				} else {
					var selectRackCode = selectedRecords[0].raw.RACK_CODE;
					var selectRackName = selectedRecords[0].raw.MODEL_NM;
					var selectVendorNm = selectedRecords[0].raw.VENDOR_NM;

					tagetIdField.setValue(selectRackCode);
					tagetNameField.setValue(selectRackName);
					if(tagetVendorField != null){
						tagetVendorField.setValue(selectVendorNm);
					}
					selectRackPop.close();
				}
			}
		}		
		
		/*********************** Private Function Area End ***/
		attachCustomEvent('beforeitemdblclick', rackSearchGrid, dbSelectEvent);		
		
		var windowProp = {
			id: that['id'],
			title: getConstText({isArgs: true, m_key: 'res.label.itam.opms.00034'}),  
			height: that['height'],
			width: that['width'],
			layout:{
				type: 'border'
			},
			buttons: [acceptBtn, closeBtn],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		};
		selectRackPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = selectRackPop;
		
	} else {
		selectRackPop = popUp_comp[that['id']];
	}
	
	return selectRackPop;
}


//장비기능 팝업
function createOutConfTechPopComp(compProperty) {
	var that = {};
	for( var prop in compProperty ){
		that[prop] = compProperty[prop];
	}
	
	var techIdField = createTextFieldComp({
			label: getConstText({isArgs: true, m_key: 'res.label.itam.opms.00037'}),
			labelWidth: 60,
			name:that['name'],
			width: 120,
			readOnly: true,
			notNull:true
	});

	var t_comp = null;
	t_comp = {
			xtype: 'fieldcontainer',
			width: getConstValue('DEFAULT_FIELD_WIDTH'),
	        height: ( that['height'] ) ? that['height'] : "",
	        combineErrors: true,
	        layout:'hbox',
	        //layout: 'hbox',
	        items: [
	            techIdField,
	            createPopBtnComp({
					handler: function(){
						var setPopUpProp = {	
								id: that['id'],
								popUpText: that['popUpText'],
		                    	tagetTechIdField: techIdField,
	                			height: 347,								
	                			width: 660,
	                			techId: that['techId'],
	                			buttonAlign : 'center',
	                			bodyStyle: 'background-color: white;'
						}
						var techPopUp = createTechSelectPop(setPopUpProp);
						techPopUp.show();
					}
				})
	        ]
		
	} 
 return t_comp;
}

function createTechSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var techSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {

		var setTreeProp = {
			id : that['id'] + 'Tree',
			url : '/itg/itam/opms/selectConfFuncTree.do',
			title : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00037'}),
			dynamicFlag : false,
			searchable : false,
			expandAllBtn : false,
			collapseAllBtn : false,
			rnode_text : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00037'}),
			expandLevel : 3
		}
		var typeTree = createTreeComponent(setTreeProp);
		// 높이 길이 체크 무시
		typeTree['syncSkip'] = true;
		attachCustomEvent('select', typeTree, treeNodeClick);

		var setGridProp = {
			id : that['id'] + '_grid',
			title : getConstText({
				isArgs : true,
				m_key : 'res.00012'
			}),
			gridHeight : 289,
			resource_prefix : 'grid.itam.opms.confFunc',
			url : '/itg/itam/opms/searchConfFuncList.do',
			pagingBar : true,
			pageSize : 20,
			autoLoad : true
		};
		var techGrid = createGridComp(setGridProp);

		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel', {
			region : 'west',
			height : 515,
			autoScroll : true,
			items : [ typeTree ]
		});

		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel', {
			border : false,
			region : 'center',
			flex : 1,
			items : [ techGrid ]
		});

		var complexProp = {
			height : '100%',
			panelItems : [ {
				flex : 1,
				items : left_panel
			}, {
				flex : 3,
				items : right_panel
			} ]
		};
		var complexPanel = createHorizonPanel(complexProp);

		var tagetTechIdField = that['tagetTechIdField'];

		var acceptBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
		  , ui : 'correct'
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				techSelectPop.close();
			}
		});
		
		/** * Private Function Area Start ******************** */

		function treeNodeClick(tree, record, index, eOpts) {
			var param = {};
			param["func_type"] = record.raw.node_id;
			var grid_store = getGrid(that['id'] + '_grid').getStore();
			grid_store.proxy.jsonData = param;
			grid_store.reload();
		}


		function dbSelectEvent() {
			var selectedRecords = techGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.itam.opms.00014'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var tech_id = selectedRecords[0].raw.FUNC_CODE;
					tagetTechIdField.setValue(tech_id);					
					techSelectPop.destroy();
				}
			}
		}

		/** ********************* Private Function Area End ** */

		attachBtnEvent(acceptBtn, dbSelectEvent);
		attachCustomEvent('itemdblclick', techGrid, dbSelectEvent);

		techSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'] + '_Pop',
			title : that['popUpText'],
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeButton ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				},
				cellclick : function(grid, td, cellIndex, record, tr, rowIndex,
						e, eOpts) {
				}
			}
		});

		popUp_comp[that['id']] = techSelectPop;

	} else {
		techSelectPop = popUp_comp[that['id']];
	}

	return techSelectPop;
}


/**
 * 원장 팝업 - 모품등록
 * 
 * @param {}
 *            setPopUpProp
 * @return {}
 */
function createOpmsParentSelectPop(setPopUpProp) {
	
	var storeParam = {};
	var that = {};
	var parentAssetSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		
		storeParam = that['params'];
		
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.label.itam.oper.00087'
			}), // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.statistics.parent', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : storeParam,
			pagingBar : true,
			pageSize : 10
		};
		
		var parentAssetGrid = createGridComp(setGridProp);

		var parentAssetSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'parentAssetBtn',
			ui : 'correct'
		});
		attachBtnEvent(parentAssetSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ parentAssetSearchBtn ],
			columnSize : 2,
			tableProps : [ {
				colspan : 1,
				item : createTextFieldComp({
					label : "자산ID",
					name : 'search_asset_id',
					width : 250
				})
			}, {
				colspan : 1,
				item : createTextFieldComp({
					label : "구성명",
					name : 'search_conf_nm',
					width : 250
				})
			} ]
		}

		var parentAsset_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ parentAsset_search_form, parentAssetGrid ]
		})

		var targetParentAssetIdField = that['targetParentAssetIdField'];
		var targetParentAssetFlagField = that['targetParentAssetFlagField'];
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				parentAssetSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button(
				{
					text : getConstText({
						isArgs : true,
						m_key : 'btn.common.apply'
					})
					, ui : 'correct'
					, handler : function() {
						var selectedRecords = parentAssetGrid.view.selModel
								.getSelection();
						if (selectedRecords.length < 1) {
							alert(getConstText({
								isArgs : true,
								m_key : 'msg.itam.project.00013'
							}));
						} else {
							dbSelectEvent();
						}
					}
				});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();
				paramMap['asset_id'] = storeParam['asset_id'];
				paramMap['class_id'] = storeParam['class_id'];
				parentAssetGrid.searchList(paramMap);
				break;

			}
		}

		function dbSelectEvent() {
			var selectedRecords = parentAssetGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				alert(getConstText({
					isArgs : true,
					m_key : 'msg.itam.project.00013'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var parentAsset_id = selectedRecords[0].raw.ASSET_ID_HI;
					targetParentAssetIdField.setValue(parentAsset_id);
					targetParentAssetFlagField.setValue("S");
					parentAssetSelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', parentAssetGrid, dbSelectEvent);

		parentAssetSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 600,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = parentAssetSelectPop;

	} else {
		parentAssetSelectPop = popUp_comp[that['id']];
	}

	return parentAssetSelectPop;
}


/**
 * 원장 팝업 - 벤더선택
 * 
 * @param {}
 *            setPopUpProp
 * @return {}
 */

function createOpmsVendorSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var vendorSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		
		storeParam = that['params'];
		
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.title.system.vendor.list'//'res.label.itam.oper.00087'
			}), // Grid Title
			gridHeight : getConstValue('MIDDLE_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.vendor.popup', // Resource Prefix 모델팝업 벤더 그리드 프리픽스
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : storeParam,
			pagingBar : true,
			pageSize : 10
		};
		
		var vendorGrid = createGridComp(setGridProp);

		var vendorSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'vendorBtn',
			ui : 'correct'
		});
		attachBtnEvent(vendorSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ vendorSearchBtn ],
			columnSize : 2,
			tableProps : [ {
				colspan : 1,
				item : createTextFieldComp({
					label : "업체명",//'res.label.itam.oper.00009',
					name : 'search_vendor_nm',
					width : 250
				})
			}]
		}

		var vendor_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ vendor_search_form, vendorGrid ]
		})

		var targetVendorNmField = that['targetField'];
		var targetVendorIdField = that['targetHiddenField'];
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				vendorSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button(
				{
					text : getConstText({
						isArgs : true,
						m_key : 'btn.common.apply'
					})
					, ui : 'correct'
					, handler : function() {
						var selectedRecords = vendorGrid.view.selModel
								.getSelection();
						if (selectedRecords.length < 1) {
							alert(getConstText({
								isArgs : true,
								m_key : 'msg.itam.project.00013' //TODO 수정할 것
							}));
						} else {
							dbSelectEvent();
						}
					}
				});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();
				paramMap['vendor_id'] = storeParam['vendor_id'];
				paramMap['vendor_nm'] = storeParam['vendor_nm'];
				vendorGrid.searchList(paramMap);
				break;

			}
		}

		function dbSelectEvent() {
			var selectedRecords = vendorGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				alert(getConstText({
					isArgs : true,
					m_key : 'msg.itam.vendor.00016'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var vendor_id = selectedRecords[0].raw.VENDOR_ID;
					var vendor_nm = selectedRecords[0].raw.VENDOR_NM;
					targetVendorIdField.setValue(vendor_id);
					targetVendorNmField.setValue(vendor_nm); //벤더이름
					vendorSelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', vendorGrid, dbSelectEvent);

		vendorSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 600,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			layout: 'fit',
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = vendorSelectPop;

	} else {
		vendorSelectPop = popUp_comp[that['id']];
	}

	return vendorSelectPop;
}



/**
 * 설치 소프트웨어 컴포넌트 소프트웨어 선택 팝업 (자동화 원장 전용...)
 * @param compProperty
 */
function createInstallSwListPop(compProperty){
	
	var that 		= convertDomToArray(compProperty);
	var id			= that["id"];
	var conf_id		= that["conf_id"];
	var window_comp= "";
	
	// SW그룹 선택용 팝업
	var swGroupId = createUnionFieldContainer(
			{id:'sw_group_container'
			,label:"SW그룹ID"
			,notNull:true
			,width:400
			,items:[
			        createTextFieldComp({name:'sw_group_id'
			        					,id:'sw_group_id'
										,hideLabel:true
										,readOnly:true
										,widthFactor: 0.45
										,notNull:true})
										
					,createPopBtnComp({	id:'sw_group_PopOpBtn'
										,handler:function(){ 
																var setPopUpProp = {
																	popId: 'SwGroupPop',
																	treeId: 'SwGroupPop__treeId',
																	popUpTitle: '소프트웨어 그룹 선택',
																	url: '/itg/itam/sam/searchSwGroupTreeInsSwComp.do',
																	title: '소프트웨어 그룹 선택',
																	params: {isInstallSwPop : "true"},
																	targetField: Ext.getCmp('sw_nm'),
																	targetHiddenField: Ext.getCmp('sw_group_id'),
																	targetVersionField: Ext.getCmp('sw_version'),
																	targetEditionField: Ext.getCmp('sw_edition'),
																	leafSelect: true,
																	rootSelect: true,
																	isSwGroup: true,
																	expandAllBtn : "SwGroupPop__expandAllBtn",
																	collapseAllBtn : "SwGroupPop__collapseAllBtn",
																	expandLevel: 3
																}
																var popUp = createTreeSelectPop(setPopUpProp);
																popUp.show();
											}})
									
					,createClearBtnComp({id:'sw_group_PopClrBtn'
										,handler:function(){ Ext.getCmp('sw_group_id').setValue("");
															Ext.getCmp('sw_nm').setValue("");
															Ext.getCmp('sw_version').setValue("");
															}})
				]
				, fixedLayout: true
				, fixedWidth: 500
			}
		);
	// SW명 : 텍스트필드
	var swNm = createTextFieldComp({label:"소프트웨어명", name:'sw_nm', id:'sw_nm', notNull:true, readOnly:true});
	// SW버전 : 텍스트필드
	var swVersion = createTextFieldComp({label:"버전", name:'sw_version', id:'sw_version', notNull:true, vtype:"numberDot" });
	// SW에디션 : 텍스트필드
	var swEdition = createTextFieldComp({label:"Edition", name:'sw_edition', id:'sw_edition', notNull:true, readOnly:true });
	// 설치경로 : 텍스트필드
	var instPath = createTextFieldComp({label:"설치경로", name:'sw_inst_path', id:'sw_inst_path', notNull:true });
	// 설치일자 : 날짜필드
	var swInstDt = createDateFieldComp({label:'설치일자', name:'sw_inst_dt', id:'sw_inst_dt', dateType:'D'});
	// 제외 여부
	//var expYn = createCodeComboBoxComp({label : "소프트웨어 제외여부", name:'exp_yn_detail', id:'exp_yn_detail', code_grp_id:'YN', width : 300, attachAll:true, notNull:true});
	

	
	
	// 트리에서 선택된 SW그룹의 상세정보 및 신규등록을 위한 입력 폼
	var swEditForm = {
		id: 'swEditForm',
		title: "설치 소프트웨어 추가",
		columnSize: 1,
		editOnly: true,
		border: true,
		formBtns: [],
		tableProps :[
			 		{colspan:1, tdHeight: 60, item : swGroupId}
					,{colspan:1, tdHeight: 60, item : swNm}						
					,{colspan:1, tdHeight: 60, item : swEdition}
					,{colspan:1, tdHeight: 60, item : swVersion}
					,{colspan:1, tdHeight: 60, item : instPath }
					,{colspan:1, tdHeight: 60, item : swInstDt }
					//,{colspan:1, tdHeight: 60, item : expYn }
		],
		hiddenFields:[]
	}
   	var swGroupEditForm = createEditorFormComp(swEditForm);
	
	// 컴포넌트에 notNull 조건을 주고 isValid()를 사용하려면 formEditable을 한번 돌려줘야한다
	formEditable({id : 'swEditForm', editorFlag : true, excepArray : [] });
	
	// 등록버튼 생성
	var acceptButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
		ui:'correct',
		scale:'medium',
		handler: function() {
				
			if(Ext.getCmp('swEditForm').isValid()) {
						
				var sendData = {};
				
				sendData['CONF_ID'] = conf_id;
				sendData['SW_NM'] = swNm.getValue();
				sendData['SW_VERSION'] = swVersion.getValue();
				sendData['SW_EDITION'] = swEdition.getValue();
				sendData['SW_INST_PATH'] = instPath.getValue();
				sendData['SW_INST_DT'] = swInstDt.getValue();
				sendData['SW_GROUP_ID'] = Ext.getCmp('sw_group_id').getValue();
				sendData['SW_AD_YN'] = "N";
				sendData['SW_AD_YN_NM'] = "아니오";
				sendData['SW_LICENCE_EXP_YN'] = "N";
				
				if(sendData['SW_EDITION']=="") {
					sendData['SW_EDITION'] = "-";
				}
				
				if(sendData['SW_INST_PATH']=="") {
					sendData['SW_INST_PATH'] = "-";
				}
				
				if(sendData['SW_GROUP_ID']=="") {
					sendData['MNGT_YN'] = "N";
					sendData['MNGT_YN_NM'] = "아니오";
				} else {
					sendData['MNGT_YN'] = "Y";
					sendData['MNGT_YN_NM'] = "예";
				}
				
				var parentGrid = getGridStore(id);
				var parentGridItems = parentGrid.data.items;
				var existFlag = true;
				
				// 부모그리드에 중복된 데이터가 있는지
				if(parentGridItems.length > 0) {
					
					// 입력된 데이터중 DB상 키값들을 일련의 문자로 만들어 비교하여 중복체크
					for(var i=0; i<parentGridItems.length; i++) {
						
						var item = parentGridItems[i].data;
						var parentVal = "" + item.SW_NM + item.SW_VERSION + item.SW_EDITION + item.SW_INST_PATH + "";
						var inputVal = "" + swNm.getValue() + swVersion.getValue() + swEdition.getValue() + instPath.getValue() + "";
						
						if(parentVal == inputVal) {
							alert("동일한 소프트웨어가 존재합니다.");
							existFlag = true;
							return;
						} else {
							existFlag = false;
						}
					}
					// 부모 그리드에 아이템이 없으면 그냥 바로 입력시킴
				} else {
					existFlag = false;
				}
				
				if(!existFlag) {
					parentGrid.add(sendData);
				}
				
				window_comp.close();
				
			} else {
				
				alert("필수 입력값을 확인해주세요.");
				return;
				
			}
		}
	});
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		scale:'medium',
		handler: function() {
			window_comp.close();
		}
	});
	
	var codePopupProp		= {
								id:   id+"Pop"
								,title: "설치 소프트웨어 등록"
								,width: 550
								,height: 300
								,modal: true
								,layout: 'fit'
								,buttonAlign: 'center'
								,bodyStyle: 'background-color: white; '
								,buttons: [acceptButton,closeButton]
								,items: [swGroupEditForm]
								,resizable:true
						  }
	
	window_comp = createWindowComp(codePopupProp);
		
	window_comp.show();
}


// 설치 소프트웨어 상세보기
function createInstallSwDetailPop(compProperty, param, authType) {
	
	var that 		= convertDomToArray(compProperty);
	var id			= that["id"]+"_detail";
	var conf_id		= that["conf_id"];
	var window_comp= "";
	var btns;
	
	// SW그룹 선택용 팝업
	var swGroupId = createUnionFieldContainer(
			{id:'sw_group_container_detail'
			,label:"SW그룹ID"
			,notNull:true
			,items:[
			        createTextFieldComp({name:'sw_group_id_detail'
			        					,id:'sw_group_id_detail'
										,hideLabel:true
										,widthFactor: 0.45
										,readOnly:true
										,notNull:true})
										
					,createPopBtnComp({	id:'sw_group_PopOpBtn_detail'
										,handler:function(){ 
															var setPopUpProp = {
																popId: 'SwGroupPop_detail',
																treeId: 'SwGroupPop__treeId_detail',
																popUpTitle: '소프트웨어 그룹 선택',
																url: '/itg/itam/sam/searchSwGroupTreeInsSwComp.do',
																title: '소프트웨어 그룹 선택',
																targetField: Ext.getCmp('sw_nm_detail'),
																targetHiddenField: Ext.getCmp('sw_group_id_detail'),
																targetVersionField: Ext.getCmp('sw_version_detail'),
																targetEditionField: Ext.getCmp('sw_edition_detail'),
																targetGrid: Ext.getCmp('optionListGrid'),
																leafSelect: true,
																rootSelect: true,
																isSwGroup: true,
																expandAllBtn : "SwGroupPop__expandAllBtn_detail",
																collapseAllBtn : "SwGroupPop__collapseAllBtn_detail",
																expandLevel: 3
															}
															
															var popUp = createTreeSelectPop(setPopUpProp);
															popUp.show();
														}})
									
					,createClearBtnComp({id:'sw_group_PopClrBtn_detail'
										,handler:function(){ Ext.getCmp('sw_group_id_detail').setValue("");
																//Ext.getCmp('sw_nm_detail').setValue("");
																//Ext.getCmp('sw_version_detail').setValue("");
																Ext.getCmp('optionListGrid').store.removeAll();
																}})
				]
				, fixedLayout: true
				, fixedWidth: 500
			}
		);
	// SW명 : 텍스트필드
	var swNm = createTextFieldComp({label:"소프트웨어명", name:'sw_nm_detail', id:'sw_nm_detail', notNull:true, readOnly:true});
	// SW에디션 : 텍스트필드
	var swEdition = createTextFieldComp({label:"Edition", name:'sw_edition_detail', id:'sw_edition_detail', notNull:true, readOnly:true });
	// SW버전 : 텍스트필드
	var swVersion = createTextFieldComp({label:"버전", name:'sw_version_detail', id:'sw_version_detail', notNull:true, vtype:"numberDot" });
	// 설치경로 : 텍스트필드
	var instPath = createTextFieldComp({label:"설치경로", name:'sw_inst_path_detail', id:'sw_inst_path_detail', notNull:true });
	// 설치일자 : 날짜필드
	var swInstDt = createDateFieldComp({label:'설치일자', name:'sw_inst_dt_detail', id:'sw_inst_dt_detail', dateType:'D'});
	// 제외 여부
	var expYn = createCodeComboBoxComp({label : "소프트웨어 제외여부", name:'exp_yn_detail', id:'exp_yn_detail', code_grp_id:'YN', attachAll:true, notNull:true});
	
	// 트리에서 선택된 SW그룹의 상세정보 및 신규등록을 위한 입력 폼
	var swEditForm = {
		id: 'swEditForm_detail',
		title: "설치 소프트웨어 추가",
		columnSize: 1,
		editOnly: true,
		border: true,
		formBtns: [],
		tableProps :[
			 		{colspan:1, tdHeight: 60, item : swGroupId}
					,{colspan:1, tdHeight: 60, item : swNm}						
					,{colspan:1, tdHeight: 60, item : swEdition}
					,{colspan:1, tdHeight: 60, item : swVersion}
					,{colspan:1, tdHeight: 60, item : instPath }
					,{colspan:1, tdHeight: 60, item : swInstDt }
					,{colspan:1, tdHeight: 60, item : expYn}
		],
		hiddenFields:[]
	}
   	var swGroupEditForm = createEditorFormComp(swEditForm);
	
	// 컴포넌트에 notNull 조건을 주고 isValid()를 사용하려면 formEditable을 한번 돌려줘야한다
	formEditable({id : 'swEditForm_detail', editorFlag : true, excepArray : [] });
	
	/*
	// 옵션 불러오기 버튼 (나중에 필요하면... 쓰셈)
	var loadOptionButton = new Ext.button.Button({
		text: "옵션 불러오기",
		scale:'small',
		ui:'correct',
		handler: function() {
			optionListGrid.searchList({sw_group_id : Ext.getCmp('sw_group_id_detail').getValue("")});
		}
	});
	*/
	
	// 옵션목록 그리드 생성
	var setOptionListGrid = {
		id : 'optionListGrid'
		,title: '옵션 목록'
		,resource_prefix: 'grid.itam.oper.installedSw.option'
		,url: '/itg/itam/sam/selectUsingOptionList.do'
		//,isMemoryStore: true
		,params: param
		,pagingBar: false
		,gridHeight: 250
		//,selModel: true
		,autoLoad: true
		,allowDeselect: true
		//,toolBarComp: [loadOptionButton]
	}
	
	var optionListGrid = createGridComp(setOptionListGrid);
	
	// V패널 생성
	var swOptionPopPanelProp = {
			id: 'swOptionPopPanel',
			align: "center",
	        panelItems: [
	          {
	        	width : '100%',
				items: swGroupEditForm
			  }
			  ,{
				 width : '100%',
	    		items: optionListGrid
			  }
	    	]
	}
	
	var swOptionPopPanel = createVerticalPanel(swOptionPopPanelProp);	
	
	
	// 적용버튼 생성
	var acceptButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
		ui:'correct',
		scale:'medium',
		handler: function() {
			
			var optionIdList = new Array;
			var selOptions = optionListGrid.getSelectedGridData();
			
			for(var i=0; i<selOptions.length; i++) {
				optionIdList.push(selOptions[i].option_id);
			}
				
			if(Ext.getCmp('swEditForm_detail').isValid()) {
				
				var sendData = {};
				
				sendData['CONF_ID'] = conf_id;
				sendData['SW_NM'] = Ext.getCmp('sw_nm_detail').getValue();
				sendData['SW_VERSION'] = Ext.getCmp('sw_version_detail').getValue();
				sendData['SW_EDITION'] = Ext.getCmp('sw_edition_detail').getValue();
				sendData['SW_INST_PATH'] = Ext.getCmp('sw_inst_path_detail').getValue();
				sendData['SW_INST_DT'] = Ext.getCmp('sw_inst_dt_detail').getValue();
				sendData['SW_GROUP_ID'] = Ext.getCmp('sw_group_id_detail').getValue();
				sendData['SW_AD_YN'] = "N";
				sendData['SW_AD_YN_NM'] = "아니오";
				sendData['OPTION_ID_LIST'] = optionIdList;
				sendData['SW_LICENCE_EXP_YN'] = Ext.getCmp('exp_yn_detail').getValue();
				
				if(sendData['SW_EDITION']=="") {
					sendData['SW_EDITION'] = "-";
				}
				
				if(sendData['SW_INST_PATH']=="") {
					sendData['SW_INST_PATH'] = "-";
				}
				
				if(sendData['SW_GROUP_ID']=="") {
					sendData['MNGT_YN'] = "N";
					sendData['MNGT_YN_NM'] = "아니오";
				} else {
					sendData['MNGT_YN'] = "Y";
					sendData['MNGT_YN_NM'] = "예";
				}
				
				var parentGrid = getGrid(that["id"]);
				var parentGridStore = parentGrid.getStore();
				var parentDataModel = parentGrid.view.selModel.getLastSelected();
				var parentRowIndex = parentDataModel.index;
				
				parentGridStore.remove(parentDataModel);
				parentGridStore.insert(parentRowIndex, sendData);
				
				window_comp.close();
				
			} else {
				
				alert("필수 입력값을 확인해주세요.");
				return;
				
			}
		}
	});
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		scale:'medium',
		handler: function() {
			window_comp.close();
		}
	});
	
	// 사용자가 수정권한이 있을경우 수정버튼 표시
	if(authType == "U"){
		//btns = [acceptButton,closeButton];
		btns = [closeButton];
	}else{
		btns = [closeButton];
	}
	
	var codePopupProp		= {
								id:   id+"Pop_detail"
								,title: "설치 소프트웨어 상세보기"
								,width: 550
								,height: 550
								,modal: true
								,layout: 'fit'
								,buttonAlign: 'center'
								,bodyStyle: 'background-color: white; '
								,buttons: btns
								,items: [swOptionPopPanel]
								,resizable:true
						  }
	
	window_comp = createWindowComp(codePopupProp);
	
	/*
	// 그리드 로드완료 후 이벤트
	optionListGrid.getStore().on("load", function(store, records, successful, eOpts) {
		
		// 선택한 설치 소프트웨어 raw
		var parentGridSelection = Ext.getCmp(that["id"]).view.selModel.getSelection();
		// 선택한 설치 소프트웨어의 옵션ID 리스트
		var optionIdList = parentGridSelection[0].data.OPTION_ID_LIST;
		// 선택되게 할 레코드
		var selectRecords = new Array();
		
		// 모든 옵션들의 수 만큼 반복문을 수행하면서
		for(var i = 0 ; i < records.length ; i++) {
			
			// 선택한 설치 소프트웨어 옵션ID 리스트의 길이만큼 반복문 수행
			for(var j = 0 ; j < optionIdList.length ; j++) {
				
				// 만약 i번째 옵션ID가 j번째 옵션ID 리스트의 값과 같다면
				if(records[i].raw.OPTION_ID == optionIdList[j]) {
					
					// 선택되게 만들 리스트에 담아줌
					selectRecords.push(records[i]);
				}
			}
			// 선택되게 만들 리스트를 선택되게 만듬
			optionListGrid.getSelectionModel().select(selectRecords);
		}
	});
	*/
	
	Ext.getCmp('sw_group_id_detail').setValue(param['SW_GROUP_ID']);
	Ext.getCmp('sw_nm_detail').setValue(param['SW_NM']);
	Ext.getCmp('sw_version_detail').setValue(param['SW_VERSION']);
	Ext.getCmp('sw_edition_detail').setValue(param['SW_EDITION']);
	Ext.getCmp('sw_inst_path_detail').setValue(param['SW_INST_PATH']);
	Ext.getCmp('sw_inst_dt_detail').setValue(param['SW_INST_DT']);
	Ext.getCmp('exp_yn_detail').setValue(param['SW_LICENCE_EXP_YN']);
	
	if(param['SW_AD_YN'] == "Y" || param['LICENCE_YN'] == "Y") {
		Ext.getCmp('sw_group_id_detail').setReadOnly(true);
		Ext.getCmp('sw_group_PopOpBtn_detail').setDisabled(true);
		Ext.getCmp('sw_group_PopClrBtn_detail').setDisabled(true);
		Ext.getCmp('sw_nm_detail').setReadOnly(true);
		Ext.getCmp('sw_version_detail').setReadOnly(true);
		Ext.getCmp('sw_edition_detail').setReadOnly(true);
		Ext.getCmp('sw_inst_path_detail').setReadOnly(true);
		Ext.getCmp('sw_inst_dt_detail').setReadOnly(true);
		Ext.getCmp('exp_yn_detail').setReadOnly(true);
	}
	
	window_comp.show();
	
}

/**
 * 모 자산 변경 팝업
 * @param compProperty
 */
function createMotherListPop(compProperty){
	
	var paramMap = {};
	var that 			= convertDomToArray(compProperty);
	var title			= that["title"];
	var type 			= that["type"];
	var id				= that["id"];
	var conf_id			= that["conf_id"];
	var ori_asset_id	= that["ori_asset_id"];
	var confGrid		= "";
	var amClassTree		= "";
	var windowComp		= "";
	var searchForm		= "";
	
	var selectList = getGrid(id).view.selModel.getSelection();
	
	if (selectList.length == 0){
		alert('변경할 자 자산을 선택하지 않았습니다.');
		return;
	}
	for(var i=0;i<selectList.length;i++){
		paramMap[i] = selectList[i].raw;

	}
	
	//Searchform 엔터 액션
	var searchConfList = function (){
		var paramTreeMap = searchForm.getInputData();
		var treeData = amClassTree.getSelectionModel().getSelection()[0];
		paramTreeMap["type"] = type;
		if(treeData){
			paramTreeMap["class_id"] = treeData.data.id;
		}
		confGrid.searchList(paramTreeMap);
	}
	
	//적용
	var confirmMng = function (){  //적용 버튼
		var row 	= gridSelectedRows("confList"); //팝업 그리드에서 선택한 모자산
		
		if(row){
			showMessageBox({msg : "선택하신 모 자산의 자 자산으로 변경하시겠습니까?" ,type : "CHOICE" ,fn : function(btns){
					var conf_nm =  row.raw.CONF_NM;
					var class_id =  row.raw.CLASS_ID;
					var asset_id_hi =  row.raw.ASSET_ID;
					var class_type =  row.raw.CLASS_TYPE;
					var closable = true;
					
					if(btns == "yes"){
							for( var c=0; c < selectList.length; c++ ){
								paramMap[c]["ASSET_ID_HI"] = asset_id_hi;
								paramMap[c]["ORI_ASSET_ID"] = ori_asset_id;
							}
    	   					jq.ajax({ type:"POST"
				    	   		, url:'/itg/itam/opms/updateMotherInfo.do'//모자산 변경 서비스
				    	   		, contentType: "application/json"
				    	   		, dataType: "json"
				    	   		, async: false
				    	   		, data : getArrayToJson(paramMap)
				    	   		, success:function(data){
				    	   			if(data.success){//성공 팝업 띄우고, 자품목 리스트 컴포넌트 리프레쉬
				    	   				alert('모자산 변경 성공!');
				    	   				var getAsset_ChildGrid = getGrid(id).getStore();
										getAsset_ChildGrid.load();
										
				    	   			}else{
				    	   				alert('Mother Change Fail!\n\n'+data.resultMsg);
				    	   				
				    	   			}
					   			}
					   		});
    	   				windowComp.close();
						
					}else if(btns == "no"){
						alert(row.raw.ASSET_ID);
						windowComp.close();
						
					}else{	
						return;
						
					}
				}});

		}else{
			//showMessageBox({msg : "모 자산을 선택하십시요." ,type : "ALERT"});
			alert('모 자산을 선택하십시요.');
			
		}
		
	}

	//검색 버튼
	var searchConfBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:'searchUserBtn' , ui:'correct'});					//검색
	var conFirmBtn	 	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.itam.00031' })	, id:'confConFirmBtn', ui:'correct', scale: "medium"});	//적용
	
	//검색조건
	var userText		= createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.itam.motherAssetSearch.00001'}), name: 'amUser', notNull: false, width: 200, editMode: true});	//사용자명
	var operText		= createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.itam.motherAssetSearch.00002'}), name: 'amOper', notNull: false, width: 200, editMode: true});	//운영자명
	var srvcText		= createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.itam.motherAssetSearch.00003'}), name: 'amSrvc', notNull: false, width: 200, editMode: true});	//업무명
	var asetText		= createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.itam.common.00002'}),			 name: 'amAset', notNull: false, width: 200, editMode: true});	//자산명
	var mdelText		= createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.itam.invenPart.00017'}),		 name: 'amMdel', notNull: false, width: 200, editMode: true});	//모델명
	var locaText		= createTextFieldComp({label: getConstText({isArgs: true, m_key: 'res.label.itam.rack.00016'}),				 name: 'amLcal', notNull: false, width: 200, editMode: true});	//위치명
	//검색폼 설정
	var setSearchFormProp 	= {
								 id				: 'confSearchForm'
								,columnSize		: 3
								,width 			: 755
								,formBtns		: [searchConfBtn]								//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps 	: [
								               	   {colspan:1, tdHeight: 22, item : operText}
								            	  //,{colspan:1, tdHeight: 22, item : userText}
												  //,{colspan:1, tdHeight: 22, item : srvcText}
												  ,{colspan:1, tdHeight: 22, item : asetText}
												  ,{colspan:1, tdHeight: 22, item : mdelText}
								               	  //,{colspan:1, tdHeight: 22, item : locaText}
												  ]
								,enterFunction	: searchConfList
							  }
	//검색폼
	searchForm	= createSeachFormComp(setSearchFormProp);
	
	var treeHeight = getBrowserByHeight(453);
	
	var setOperClassTreeProp = {
			id : 'confClassTree',
			url : '/itg/itam/newAsset/selectCarryClassTree.do',
			title : getConstText({ isArgs: true, m_key: 'res.title.itam.class' }),
			height: treeHeight,
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : true,
			collapseAllBtn : true,
			params: {treeType : type},
			rnode_text : '#springMessage("res.00003")',
			expandLevel: 3
	}
	amClassTree = createTreeComponent(setOperClassTreeProp);
	//트리 클릭 이벤트
	var amClassTreeNodeClick = function(obj,selData){
		//searchForm.initData();
		var paramMap = searchForm.getInputData();
		var class_id = selData.data.id;
		paramMap["class_id"] = class_id;
		paramMap["type"] = type;
		confGrid.searchList(paramMap);
	}
	
	attachCustomEvent('select', amClassTree, amClassTreeNodeClick);
	
	//구성리스트
	var setConfGridProp 	= {	 
									 id					: "confList"
									,title				: title
									,gridHeight			: 335
									,gridWidth			: 755
									,resource_prefix	: 'grid.itam.opmsDetail.MotherChange'
									,url				: '/itg/itam/opms/selectMotherList.do'
									,params				: {type : type, tangible_asset_yn : "Y"}
									,pagingBar 			: true
									,multiSelect		: false
									,selModel			: false
									,border 			: true
									,pageSize 			: 10
							};
	
	confGrid = createGridComp(setConfGridProp);
	
	//뷰포트 생성
	var viewportProperty = {
			borderId	: 'borderId' 
			,viewportId : 'viewId' 
			,west: {
				minWidth: 170 
				,maxWidth: 755 
				,items : [amClassTree] 
			},
			 center: { 
				 	minWidth: 180 
					,maxWidth: 755 
					,items : [searchForm,confGrid] 
			}
	}
	// Viewport 생성
	
	var popupHeight = getBrowserByHeight(520);
	
	var codePopupProp	= {
			id:   "confPop"
			,title: title
			,width: 965
			,height: popupHeight
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [conFirmBtn]
			,closeBtn : true
			,items: createBorderViewPortComp(viewportProperty,{ isLoading: true, isResize : false} )
			,resizable:true
	  }

	windowComp = createWindowComp(codePopupProp);
	
	//버튼 이벤트
	attachBtnEvent(conFirmBtn	,confirmMng);
	attachBtnEvent(searchConfBtn	,searchConfList);
	
	windowComp.show();

	
}

function getBrowserByHeight(hs) {
	var heightSize = 0;
	var agt = navigator.userAgent;
	if(agt.indexOf("Chrome") != -1) {
		heightSize = hs;
	} else if(agt.indexOf("MSIE 7") != -1) {
		heightSize = parseInt(hs)+10;
	} else if(agt.indexOf("MSIE 8") != -1) {
		heightSize = parseInt(hs)+10;
	} else if(agt.indexOf("MSIE 9") != -1) {
		heightSize = parseInt(hs)+10;
	} else if(agt.indexOf("MSIE 10") != -1) {
		heightSize = parseInt(hs)+10;
	} else if(agt.indexOf("MSIE 11") != -1) {
		heightSize = parseInt(hs)+10;
	} else {
		heightSize = parseInt(hs)+10;
	}
	//alert(agt + '\n' + heightSize);
	return heightSize;
}


/**
 * 랙 위치 팝업
 * 
 * @param {}
 *            setPopUpProp
 * @return {}
 */
function createRackLocSelectPop(setPopUpProp) {
	
	var storeParam = {};
	var that = this;
	var locPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}

	if(popUp_comp[that['id']] == null) {
		
		var param = {};
		param["locationTopCode"] = '0';
		param["use_yn"] = 'Y';
		
		var setTreeProp = {
				context: '${context}',						
				id : 'locationTree',
				url : '/itg/system/location/searchLocation.do',
				title : '위치조회',
				dynamicFlag : false,
				searchable : true,
				searchableTextWidth : 100,
				height: 450,
				expandAllBtn : true,
				collapseAllBtn : true,
				params: param,
				rnode_text : '위치',
				expandLevel: 3
		}
		var locationTree = createTreeComponent(setTreeProp);
		attachCustomEvent('select', locationTree, treeNodeClick);
		
		
		var layOutPanel = Ext.create('Ext.panel.Panel', {
			id:'layOutPanel',
			title: '상면조회',
			width: '100%',
			height: 445,
			autoScroll: true,
			border: false,
		    bodyStyle: 'background-color:#C6C6C6',
			layout: {
				type: 'table',
				columns: 0
			}
		});		
		
		var leftPanel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			height: '100%',
			items: [locationTree]
		});
		
		var rightPanel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			height: '100%',
			items: [layOutPanel]
		});
		
		var panelProperty = {
				panelItems : [{
					flex: 0.3,
					items : leftPanel
				},{
					width: 700,
					items : rightPanel
				}]
		};
		var totalPanel = createHorizonPanel(panelProperty);
		
		
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
			handler: function() {
				var seletedObject = Ext.getCmp('locationTree').view.selModel.getSelection();
				var locData = {};
				locData['id'] = seletedObject[0].raw.id;
				rackLocClick(locData);
			}
		});
		
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				locPop.close();
			}
		});
		
		
		/*** Private Function Area Start *********************/		
		
		function treeNodeClick( tree, record, index, eOpts ) {
			
			var param = {};
			loc_code = record.raw.id;
			param["loc_code"] = loc_code;
			locationSelectPopUp = locPop;
			
			x_axis = null;
			y_axis = null;
			dataVO = null;
			
			jq.ajax({ type:"POST"
				, url:"/itg/itam/topView/searchTopViewData.do"
					, contentType: "application/json"
						, dataType: "json"
							, async: false
							, data : getArrayToJson(param)
							, success:function(data){
								if( data.success ){
									var clickRecord = data.gridVO.rows[0];
									if(data.gridVO.rows.length > 0){
										xAxis = clickRecord.LOC_WIDTH;
										yAxis = clickRecord.LOC_HEIGHT;
										dataVO = data.gridVO;
										makeLayOut(dataVO, xAxis, yAxis);
									}else{
										var panel = Ext.getCmp('layOutPanel');
										panel.removeAll();					
									}
								}else{
									showMessageBox({msg : data.resultMsg ,type : "ALERT"});
								}
							}		
			});
		}
		
		function makeLayOut(loc_plan_code, widthSize, heightSize){
			Ext.suspendLayouts();
			var panel = Ext.getCmp('layOutPanel');
			panel.removeAll();
			panel.layout.columns = widthSize+1;
			
			
			var topHeaderInfo = null;
			
			var param = {};
			param["loc_code"] = loc_code;
			jq.ajax({ type:"POST"
				, url:"/itg/itam/topView/searchTopHeader.do"
					, contentType: "application/json"
						, dataType: "json"
							, async: false
							, data : getArrayToJson(param)
							, success:function(data){
								if( data.success ){
									topHeaderInfo = data.gridVO.rows;
								}else{
									showMessageBox({msg : data.resultMsg ,type : "ALERT"});
								}
							}		
			});
			
			// 탑 헤더 생성
			for(var i = 0; i <= topHeaderInfo.length; i++){
				if(i == 0){	
					// 헤더부분 넘버 생성
					var html= '<p align=center style="border-collapse:collapse; line-height:31px;"></p>';
					var info = {frame:true,	html:html};
					info.height = 62;
					info.rowspan = 2;
					panel.add(info);
				}else{
					var html= '<p align=center style="border-collapse:collapse; line-height:31px;">'+topHeaderInfo[i-1].AREA_CODE+'</p>';
					var info = {frame:true,	html:html};
					info.height = 31;
					info.colspan = topHeaderInfo[i-1].CNT;
					panel.add(info);
				}
			}
			
			// 두번째 헤더 생성
			for(var i = 1; i <= widthSize; i++){
				var html= '<p align=center style="border-collapse:collapse; line-height:31px;">'+loc_plan_code.rows[i-1].LOC_PLAN_X+'</p>';
				var info = {frame:true,	html:html};
				info.height = 31;
				panel.add(info);
			}
			
			var dataRow = 0;
			for(var i = 1; i <= Number(heightSize); i++){
				var typeCnt = 0;
				//좌측 넘버 생성
				var html= '<p align=center style="border-collapse:collapse; line-height:31px;">'+i+'</p>';
				var info = {
						frame:true,
						html:html
				};
				info.width = 31;
				info.height = 31;
				panel.add(info);
				
				for(var j = 1; j <= Number(widthSize); j++){
					dataRow = dataRow + 1;
					var cellInfo = makeImage(loc_plan_code.rows[dataRow-1]);
					panel.add(cellInfo);
				}
			}
			
			// merge_yn 값이 Y 이면 하위 셀을 지운다.
			for(var i = 0; i < loc_plan_code.rows.length; i++){
				var merge_yn = loc_plan_code.rows[i].MERGE_YN;
				var rowCnt = Number(loc_plan_code.rows[i].LOC_WIDTH); 
				if(merge_yn == "Y"){
					panel.remove(loc_plan_code.rows[i + rowCnt].LOC_PLAN_CODE);
				} 
			}
			
			Ext.resumeLayouts(this);
		}
		
		function makeImage(data){
			var html= null;
			
			var id = data.LOC_PLAN_CODE;
			var type = data.ASSET_YN;
			var merge_yn = data.MERGE_YN;
			var cell_color = data.CELL_COLOR;
			
			if(type == "N"){
				if(merge_yn == "Y"){
					html = '<a href="javascript:rackLocClick('+id+')" style="color:red"><img src="/itg/base/images/ext-js/common/itg-icons/merge_icon_rack_disable.png" border="0" style="background-color:'+cell_color+'"></a>';					
				}else{
					html = '<a href="javascript:rackLocClick('+id+')" style="color:red"><img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_disable.png" border="0" style="background-color:'+cell_color+'"></a>';					
				}
			}else if(type == "Y"){
				if(merge_yn == "Y"){
					html = '<a href="javascript:rackLocClick('+id+')" style="color:red"><img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_32px_enable_2row.png" border="0" style="background-color:'+cell_color+'"></a>';					
				}else{
					html = '<a href="javascript:rackLocClick('+id+')" style="color:red"><img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_32px_enable.png" border="0" style="background-color:'+cell_color+'"></a>';					
				}
			}else if(type == "WAY"){
				html = '<img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_floor.png" border="0">';
			}
			
			var info = {
					id: id,
					frame:false,
					html:html
			};
			
			info.width = 31;
			info.height = 31;
			
			if(merge_yn == "Y"){
				info.height = 62;
				info.rowspan = 2;
			}
			return info;
		}
		
		/*********************** Private Function Area End ***/	
		
		var tagetIdField = that['tagetIdField'];
		var tagetNameField = that['tagetNameField'];
		
		var windowProp = {
				id: that['id'],
				title: '설치위치',  
				height: that['height'],
				width: that['width'],
				layout:{
					type: 'border'
				},
				buttons: [acceptBtn,closeBtn],
				items: [totalPanel],
				listeners: {
					destroy:function(p) {
						popUp_comp[that['id']] = null
					},
					hide:function(p) {
					}
				}
		};
		locPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = locPop;
		
	} else {
		locPop = popUp_comp[that['id']];
	}
	
	return locPop;
}

/**
 * 위치조회 팝업 
 * 
 * @param {}
 *            setPopUpProp
 * @return {}
 */
function locationSelectPop(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var locPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {

		var param = {};
		param["locationTopCode"] = '0';
		param["use_yn"] = 'Y';
		
		var setTreeProp = {
				context: '${context}',						
				id : 'locationTree',
				url : '/itg/system/location/searchLocation.do',
				title : '위치조회',
				dynamicFlag : false,
				searchable : true,
				searchableTextWidth : 100,
				expandAllBtn : true,
				collapseAllBtn : true,
				params: param,
				rnode_text : '위치',
				expandLevel: 3
		}
		var locationTree = createTreeComponent(setTreeProp);
		attachCustomEvent('select', locationTree, treeNodeClick);
		

		var layOutPanel = Ext.create('Ext.panel.Panel', {
		    id:'layOutPanel',
		    title: '상면조회',
		    width: '98%',
		    height: 445,
		    autoScroll: true,
		    border: false,
		    bodyStyle: 'background-color:#C6C6C6',
		    layout: {
		        type: 'table',
		        columns: 0
		    }
		});		

		var leftPanel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			items: [locationTree]
		});
		
		var rightPanel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			items: [layOutPanel]
		});
		
		var panelProperty = {
				panelItems : [{
					flex: 0.3,
					items : leftPanel
				},{
					width: 400,
					items : rightPanel
				}]
			};
		var totalPanel = createHorizonPanel(panelProperty);


		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
			handler: function() {
				var seletedObject = Ext.getCmp('locationTree').view.selModel.getSelection();
				var locData = {};
				locData['id'] = seletedObject[0].raw.id;
				locClick(locData, 'accept');
			}
		});
		
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				locPop.close();
			}
		});
				
		
		/*** Private Function Area Start *********************/		

		function treeNodeClick( tree, record, index, eOpts ) {

			var param = {};
			loc_code = record.raw.id;
			param["loc_code"] = loc_code;
			locationSelectPopUp = locPop;
			
			x_axis = null;
			y_axis = null;
			dataVO = null;
			
			jq.ajax({ type:"POST"
				, url:"/itg/itam/topView/searchTopViewData.do"
				, contentType: "application/json"
				, dataType: "json"
				, async: false
				, data : getArrayToJson(param)
				, success:function(data){
					if( data.success ){
						var clickRecord = data.gridVO.rows[0];
						if(data.gridVO.rows.length > 0){
							xAxis = clickRecord.LOC_WIDTH;
							yAxis = clickRecord.LOC_HEIGHT;
							dataVO = data.gridVO;
							makeLayOut(dataVO, xAxis, yAxis);
						}else{
							var panel = Ext.getCmp('layOutPanel');
							panel.removeAll();					
						}
					}else{
						showMessageBox({msg : data.resultMsg ,type : "ALERT"});
					}
				}		
			});
		}

		function makeLayOut(loc_plan_code, widthSize, heightSize){
			Ext.suspendLayouts();
			var panel = Ext.getCmp('layOutPanel');
			panel.removeAll();
			panel.layout.columns = widthSize+1;
			
			
			var topHeaderInfo = null;
			
			var param = {};
			param["loc_code"] = loc_code;
			jq.ajax({ type:"POST"
				, url:"/itg/itam/topView/searchTopHeader.do"
				, contentType: "application/json"
				, dataType: "json"
				, async: false
				, data : getArrayToJson(param)
				, success:function(data){
					if( data.success ){
						topHeaderInfo = data.gridVO.rows;
					}else{
						showMessageBox({msg : data.resultMsg ,type : "ALERT"});
					}
				}		
			});

			// 탑 헤더 생성
			for(var i = 0; i <= topHeaderInfo.length; i++){
				if(i == 0){	
					// 헤더부분 넘버 생성
					var html= '<p align=center style="border-collapse:collapse; line-height:31px;"></p>';
					var info = {frame:true,	html:html};
				    info.height = 62;
				    info.rowspan = 2;
					panel.add(info);
				}else{
					var html= '<p align=center style="border-collapse:collapse; line-height:31px;">'+topHeaderInfo[i-1].AREA_CODE+'</p>';
					var info = {frame:true,	html:html};
				    info.height = 31;
				    info.colspan = topHeaderInfo[i-1].CNT;
					panel.add(info);
				}
			}
			
			// 두번째 헤더 생성
			for(var i = 1; i <= widthSize; i++){
				var html= '<p align=center style="border-collapse:collapse; line-height:31px;">'+loc_plan_code.rows[i-1].LOC_PLAN_X+'</p>';
				var info = {frame:true,	html:html};
			    info.height = 31;
				panel.add(info);
			}
			
			var dataRow = 0;
			for(var i = 1; i <= Number(heightSize); i++){
				var typeCnt = 0;
				//좌측 넘버 생성
				var html= '<p align=center style="border-collapse:collapse; line-height:31px;">'+i+'</p>';
				var info = {
					frame:true,
					html:html
			    };
			    info.width = 31;
			    info.height = 31;
				panel.add(info);
				
				for(var j = 1; j <= Number(widthSize); j++){
					dataRow = dataRow + 1;
					var cellInfo = makeImage(loc_plan_code.rows[dataRow-1]);
					panel.add(cellInfo);
				}
			}

			// merge_yn 값이 Y 이면 하위 셀을 지운다.
			for(var i = 0; i < loc_plan_code.rows.length; i++){
				var merge_yn = loc_plan_code.rows[i].MERGE_YN;
				var rowCnt = Number(loc_plan_code.rows[i].LOC_WIDTH); 
		 		if(merge_yn == "Y"){
					panel.remove(loc_plan_code.rows[i + rowCnt].LOC_PLAN_CODE);
				} 
			}
			
			Ext.resumeLayouts(this);
		}

		function makeImage(data){
			var html= null;

			var id = data.LOC_PLAN_CODE;
			var type = data.ASSET_YN;
			var merge_yn = data.MERGE_YN;
			var cell_color = data.CELL_COLOR;
			
			if(type == "N"){
				if(merge_yn == "Y"){
					html = '<img src="/itg/base/images/ext-js/common/itg-icons/merge_icon_rack_disable.png" border="0" style="background-color:'+cell_color+'">';					
				}else{
					html = '<img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_disable.png" border="0" style="background-color:'+cell_color+'">';					
				}
			}else if(type == "Y"){
				if(merge_yn == "Y"){
					html = '<a href="javascript:locClick('+id+')" style="color:red"><img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_32px_enable_2row.png" border="0" style="background-color:'+cell_color+'"></a>';					
				}else{
					html = '<a href="javascript:locClick('+id+')" style="color:red"><img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_32px_enable.png" border="0" style="background-color:'+cell_color+'"></a>';					
				}
			}else if(type == "WAY"){
				html = '<img src="/itg/base/images/ext-js/common/itg-icons/icon_rack_floor.png" border="0">';
			}
			
			var info = {
				id: id,
				frame:false,
				html:html
		    };
			
			info.width = 31;
		    info.height = 31;
		    
			if(merge_yn == "Y"){
			    info.height = 62;
			    info.rowspan = 2;
			}
			return info;
		}

		/*********************** Private Function Area End ***/	
		
		var tagetIdField = that['tagetIdField'];
		var tagetNameField = that['tagetNameField'];
		
		var windowProp = {
			id: that['id'],
			title: '설치위치',  
			height: that['height'],
			width: that['width'],
			layout:{
				type: 'border'
			},
			buttons: [acceptBtn,closeBtn],
			items: [totalPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		};
		locPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = locPop;
		
	} else {
		locPop = popUp_comp[that['id']];
	}
	
	return locPop;
}

// 위치등록 팝업 클릭이벤트
 locationTargetId = null;
var locationTargetNm = null;
var locationSelectPopUp = null;
var rackTargetId = null;
var rackTargetNm = null;
function locClick(data, gubun){
	if(confirm("선택한 위치에 자산을 등록하시겠습니까?")){
		var id = data.id;
		var bId = null;
		var param = {};
		param["loc_plan_code"] = id;
		if(gubun == 'accept'){
			param['accept_yn'] = 'Y';			
		}
		jq.ajax({ type:"POST"
			, url:"/itg/system/location/selectLocationName.do"
			, contentType: "application/json"
			, dataType: "json"
			, async: true
			, data : getArrayToJson(param)
			, success:function(data){
				if( data.success ){
					if(data.resultMap.CONF_ID == null){
						locationTargetId.setValue(id);
						locationTargetNm.setValue(data.resultMap.LOCATION_NAME);
						rackTargetId.setValue("");
					}else{
						bId = id.split("_")[0];
						locationTargetId.setValue(bId);
						locationTargetNm.setValue("SKHDC>" + bId);
						rackTargetId.setValue(data.resultMap.CONF_ID);
					}
					locationSelectPopUp.close();						
				}else{
					alert(data.resultMsg);
					locationSelectPopUp.close();
				}
			}		
		});		
	}
}

function rackLocClick(data){
	if(confirm("선택한 위치에 자산을 등록하시겠습니까?")){
		var id = data.id;
		var bId = null;
		var param = {};
		param["loc_plan_code"] = id;
		jq.ajax({ type:"POST"
			, url:"/itg/system/location/selectRackLocationName.do"
			, contentType: "application/json"
			, dataType: "json"
			, async: true
			, data : getArrayToJson(param)
			, success:function(data){
				if( data.success ){
					bId = id.split("_")[0];
//					console.log("id = " + id);
//					console.log("bId = " + bId);
					locationTargetId.setValue(bId);
					locationTargetNm.setValue("SKHDC>" + bId);
					if(id.split("_").length > 1){
						var rawValue;
						var tmp;
						var rId = id.split("_")[1] + "_"  + id.split("_")[2] + "_" + id.split("_")[3];
						
						tmp = id.split("_");
						
						if(tmp[3].length == 1){
							rawValue = tmp[1] + tmp[2] + '0' + tmp[3];
						} else {
							rawValue = tmp[1] + tmp[2] + tmp[3];
						}
						
						rackTargetId.setValue(id);
						rackTargetNm.setValue(rawValue);
					}else{
						rackTargetId.setValue("");
						rackTargetNm.setValue("");
					}
					locationSelectPopUp.close();
				}else{
					alert(data.resultMsg);
					locationSelectPopUp.close();
					rackTargetId.setValue("");
					rackTargetNm.setValue("");
				}
			}		
		});		
	}
}
