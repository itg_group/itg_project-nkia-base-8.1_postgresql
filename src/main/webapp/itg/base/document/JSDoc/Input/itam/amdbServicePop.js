var searchGridId = null;
function createSetUserPop(setPopUpProp){	
	var storeParam = {};
	var that = this;
	var setUserPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		
		searchGridId = that['id']+'Grid';
		// 조회 그리드
		var setSearchGridProp = {	
				id: that['id']+'Grid',
				title : getConstText({ isArgs: true, m_key: 'res.00012' }),
				gridHeight: 210,
				resource_prefix: 'grid.system.user.pop',				
				url: '/itg/system/user/searchUserList.do',
				params: null,
				selModel: true,
				pagingBar : true,
				pageSize : 10,
				autoLoad: true
		};
		var userSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
				id: 'selUserGrid',
				title : getConstText({ isArgs: true, m_key: 'res.label.itam.service.00016' }),
				gridHeight: 187,
				resource_prefix: 'grid.itam.service.user',				
				params: null,		
				pagingBar : false,
				autoLoad: false,
				dragPlug: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
				actionBtnEvent: actionBtnEvent,
				isMemoryStore: true
		};
		var userSaveListGrid = createGridComp(setSaveGridProp);
		
		var setTreeProp = {				
				id : that['id']+'Tree',
				url : '/itg/system/dept/searchStaticAllDeptTree.do',
				title : getConstText({ isArgs: true, m_key: 'res.00013' }),
				dynamicFlag : false,
				searchable : true,
				expandAllBtn : false,
				collapseAllBtn : false,
				params: null,
				rnode_text :  getConstText({ isArgs: true, m_key: 'res.00003' }),
				expandLevel: 3
		}
		var staticClassTree = createTreeComponent(setTreeProp);	
		// 높이 길이 체크 무시
		staticClassTree['syncSkip'] = true;
		attachCustomEvent('select', staticClassTree, treeNodeClick);

		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.choice' }),
			handler: function() {
				checkGridRowCopyToGrid(that['id']+'Grid', 'selUserGrid', { primaryKeys: ['USER_ID'] });
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var operUserSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				var searchForm = Ext.getCmp('amdbServiceUserPop');
				var paramMap = searchForm.getInputData();
				Ext.getCmp(that['id']+'Grid').searchList(paramMap);	
			}
		});
		
		var operUserSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp('amdbServiceUserPop').initData();
			}
		});
		
		var setUserSearchFormProp = {
			id: 'amdbServiceUserPop',
			formBtns: [operUserSearchBtn, operUserSearchInitBtn],
			columnSize: 2,
			tableProps : [{
							colspan:1, tdHeight: 30, item: createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_USER', width: 230, padding: getConstValue('WITH_FIELD_PADDING')})
						},{
							colspan:1, tdHeight: 30, item: createTextFieldComp({name:'search_value', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})
						}]
		}
		var searchUserForm = createSeachFormComp(setUserSearchFormProp);
		
		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: 515,
			autoScroll: true,
			items: [staticClassTree]
		});
		
		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			flex:1 ,
			items: [searchUserForm, userSearchListGrid, choiceBtnpanel, userSaveListGrid]
		});
		
		var complexProp = {
				height : '100%',
				panelItems : [{
					flex: 1,
					items : left_panel
				},{
					flex: 3,
					items : right_panel
				}]
		};
		var complexPanel = createHorizonPanel(complexProp);		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			ui:'correct',
			handler: function() {
				if(getGrid('selUserGrid').getStore().data.items <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.service.00006' }));
				}else{
					getGrid('userListGrid').getStore().removeAll();
					var appendable = false;
					gridAllRowCopyToGrid('selUserGrid', 'userListGrid', appendable, null, { primaryKeys: ['USER_ID'] });
					setUserPop.close();
				}
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				setUserPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		
		function treeNodeClick( tree, record, index, eOpts ) {
			var cust_id = record.raw.node_id;
			
			var grid_store = getGrid(that['id']+'Grid').getStore();
			grid_store.proxy.jsonData={cust_id: cust_id};
			grid_store.reload();
		}	
		
		/*********************** Private Function Area End ***/
		
		setUserPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: getConstText({ isArgs: true, m_key: 'res.label.itam.service.00015' }),  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setUserPop;
		
	} else {
		setUserPop = popUp_comp[that['id']];
	}
	
	return setUserPop;

}

function searchBtnEvent(){
	var searchForm = Ext.getCmp('amdbServiceUserPop');
	var paramMap = searchForm.getInputData();
	Ext.getCmp(searchGridId).searchList(paramMap);
}	