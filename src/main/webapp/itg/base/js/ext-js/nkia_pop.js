/**
 * 
 * @file nkia_pop
 * @description NKIA Popup 생성
 * @author ITG
 * @copyright NKIA 2013
 */


var popUp_comp = {};
var popUp_props = {};

/**
 * @function
 * @summary 팝업 프로퍼티 가져오기
 * @param {string} id 팝업ID
 * @return {json} popup properties
 */
function getPopupProps(id) {
	return popUp_props[id];
}

/**
 * @function
 * @summary 사용자선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createUserSelectPop(popUpProps) {
	var storeParam = {};
	var userSelectPop = null;

	var id = popUpProps['id'];
	var tagetUserIdField = popUpProps['tagetUserIdField'];
	var tagetUserNameField = popUpProps['tagetUserNameField'];
	var tagetEtcField = popUpProps['tagetEtcField'];
	var popUpText = popUpProps['popUpText'];
	var height = popUpProps['height'];
	var width = popUpProps['width'];

	var afterFunction = popUpProps['afterFunction'];
	var mappingObj = popUpProps['mappingObj'];
	var targetFormId = popUpProps['targetFormId'];
	var custNm = popUpProps['custNm'];
	

	if (popUp_comp[id] == null) {

		var acceptBtn = createBtnComp({
			label : getConstText({ isArgs : true, m_key : 'btn.common.apply' })
			, scale: 'medium'
			, ui : 'correct'
		});
		
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale: 'medium',
			handler: function() {
				userSelectPop.close();
			}
		});
		
		var complexPanel = setUserSelectPopPanel(popUpProps);
		
		userSelectPop = Ext.create('nkia.custom.windowPop', {
			id : id + '_Pop',
			title : popUpText,
			height : height,
			width : width,
			autoDestroy : false,
			resizable : false,
			buttons : [ acceptBtn, closeButton ],
			items : [ complexPanel ],
			buttonAlign : 'center',
			bodyStyle : 'background-color: white;',
			listeners : {
				destroy : function(p) {
					popUp_comp[id] = null

				}
			}
		});

		function dbSelectEvent(grid, record, item, index, e, eOpts) {
			var selectedRecords = grid.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00031'
				}));
			} else {
				if (afterFunction != null) {
					afterFunction(selectedRecords[0].raw, mappingObj);
				} else {
					var user_id = nullToSpace(selectedRecords[0].raw.USER_ID);
					var user_nm = nullToSpace(selectedRecords[0].raw.USER_NM);
					var absent_yn = nullToSpace(selectedRecords[0].raw.ABSENT_YN);
					if(absent_yn != null && absent_yn == "Y") {
						user_id = nullToSpace(selectedRecords[0].raw.ABSENT_ID);
						user_nm = nullToSpace(selectedRecords[0].raw.ABSENT_NM);
					}
					var cust_nm = nullToSpace(selectedRecords[0].raw.CUST_NM);
					var detailInfo = nullToSpace(selectedRecords[0].raw.CUST_NM)
							+ "(tel:"
							+ nullToSpace(selectedRecords[0].raw.TEL_NO)
							+ "/email:"
							+ nullToSpace(selectedRecords[0].raw.EMAIL) + ")";
					tagetUserIdField.setValue(user_id);
					tagetUserNameField.setValue(user_nm);
					if (tagetEtcField != null) {
						tagetEtcField.setValue(detailInfo);
					}
					if (targetFormId != null) {
						cust_nm = Ext.getCmp(targetFormId).setFieldValue(custNm, cust_nm);
					}
					userSelectPop.destroy();
				}
			}
		}
		
		attachCustomEvent('itemdblclick', complexPanel.getUserGrid(), dbSelectEvent);
		attachBtnEvent(acceptBtn, function() {
			dbSelectEvent(complexPanel.getUserGrid());
		});
		
		popUp_comp[id] = userSelectPop;
	} else {
		userSelectPop = popUp_comp[id];
	}

	return userSelectPop;
}

function setUserSelectPopPanel(properties) {
	var id = properties["id"];
	var url = properties["url"];
	var params = properties["params"];
	var height = properties['height'];
	var resourcePrefix = properties["resource_prefix"];
	
	var comboProps = {
		labelWidth : getConstValue('POP_LABEL_WIDTH'),
		displayField : "searchNm",
		valueField : "searchCol",
		padding : getConstValue('WITH_FIELD_PADDING'),
		label : getConstText({
			isArgs : true,
			m_key : 'res.common.search'
		}),
		name : "searchCondition",
		storeData : [ {
			searchCol : 'USER_NM',
			searchNm : getConstText({
				isArgs : true,
				m_key : 'res.label.system.00015'
			})
		}, {
			searchCol : 'USER_ID',
			searchNm : getConstText({
				isArgs : true,
				m_key : 'res.label.system.00014'
			})
		} ],
		widthFactor : 0.51
	};

	var searchConditionComp = createComboBoxComp(comboProps);
	searchConditionComp.select('USER_NM');
	
	var searchText = createTextFieldComp({
		hideLabel : true,
		name : 'user_nm',
		padding : getConstValue('WITH_FIELD_PADDING'),
		widthFactor : 0.5,
		label : getConstText({
			isArgs : true,
			m_key : 'res.label.system.00015'
		})
	})
	
	var searchContainer = createUnionFieldContainer({items:[searchConditionComp, searchText], fixedLayout: true, fixedWidth: 450});

	var userSearchBtn = createBtnComp({
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.search'
		}),
		ui:'correct',
		margin : getConstValue('WITH_BUTTON_MARGIN')
	});
	
	var userSearchInitBtn = createBtnComp({
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.reset'
		}),
		margin : getConstValue('WITH_BUTTON_MARGIN')
	});
	
	userSearchBtn.margin = '0 0 5 5';
	
	// 사용자 조회 폼
	var setSearchUserFormProp = {
		id : id + 'searchUserForm',
		title : getConstText({
			isArgs : true,
			m_key : 'res.common.search'
		}),
		target : id + '_grid',
		columnSize : 1,
		enterFunction : clickUserSearchBtn,
		formBtns : [userSearchBtn, userSearchInitBtn],
		tableProps : [ {
			colspan : 1,
			tdHeight : 30,
			item : searchContainer
		} ]
	}
	var userSearchForm = createSeachFormComp(setSearchUserFormProp);

	var treeProps = {
		id : id + 'Tree',
		url : '/itg/base/searchDeptTree.do',
		title : getConstText({isArgs : true, m_key : 'res.00034'}),
		searchable : true,
		height : height - 98,
		expandAllBtn : true,
		collapseAllBtn : true,
		params : params,
		rnode_text : getConstText({
			isArgs : true,
			m_key : 'res.00034'
		}),
		expandLevel : 3
	}
	var deptTree = createTreeComponent(treeProps);
	// 높이 길이 체크 무시
	deptTree['syncSkip'] = true;
	attachCustomEvent('select', deptTree, treeNodeClick);

	var gridProps = {
		id : id + '_grid',
		title : getConstText({
			isArgs : true,
			m_key : 'res.00012'
		}),
		gridHeight : height - 224,
		resource_prefix : (resourcePrefix) ? resourcePrefix : 'grid.system.user.pop',
		url : url,
		params : params,
		pagingBar : true,
		pageSize : 20,
		autoLoad : true
	};
	var userGrid = createGridComp(gridProps);

	var left_panel = Ext.create('Ext.panel.Panel', {
		region : 'west',
		height : '100%',
		autoScroll : true,
		items : [ deptTree ]
	});

	var right_panel = Ext.create('Ext.panel.Panel', {
		border : false,
		region : 'center',
		flex : 1,
		items : [ userSearchForm, userGrid ]
	});

	var complexProp = {
		height : '100%',
		panelItems : [ {
			flex : 1.1,
			items : left_panel
		}, {
			flex : 3.0,
			items : right_panel
		} ]
	};
	var complexPanel = createHorizonPanel(complexProp);
	
	complexPanel.getUserGrid = function() {
		return userGrid;
	}
	
	function treeNodeClick(tree, record, index, eOpts) {
		var cust_id = record.raw.node_id;

		var grid_store = userGrid.getStore();
		var param = {};
		if (params != null) {
			for ( var key in params) {
				param[key] = params[key];
			}
		}
		
		if( getConstValue("CENTER_CUST_ID") == record.raw.up_node_id ){
			param["dept_nm"] = record.raw.text;
		}else{
			param["cust_id"] = cust_id;
		}
		grid_store.proxy.jsonData = param;
		grid_store.loadPage(1);
	}

	function clickUserSearchBtn() {
		var conditionVal = searchConditionComp.getValue();
		var textVal = searchText.getValue();
		var userGridStore = userGrid.getStore();
		var param = {};
		if (params != null) {
			for ( var key in params) {
				param[key] = params[key];
			}
		}
		param["search_type"] = conditionVal;
		param["search_value"] = textVal;
		userGrid.searchList(param);
	}
	
	function clickUserSearchInitBtn() {
		searchConditionComp.setValue('USER_NM');
		searchText.setValue('');
	}

	attachBtnEvent(userSearchBtn, clickUserSearchBtn);
	attachBtnEvent(userSearchInitBtn, clickUserSearchInitBtn);
	
	return complexPanel;
}

/**
 * @function
 * @summary 사용자 다중 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createUserMultiSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var userSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {

		var searchFormId = 'searchUserForm' + that['popupId']; // searchFormId
		var userSearchForm = getAssetSearchForm(searchFormId,
				'searchMultiUser', 'searchMultiUser', 2, goSearch);

		var setGridProp = {
			id : that['id'] + '_grid',
			title : '사용자 목록',
			gridHeight : 200,
			resource_prefix : 'grid.popup.user',
			url : getConstValue('CONTEXT') + that['url'],
			enterFunction : goSearch,
			params : that['params'],
			selModel : true,
			multiSelect : true,
			pagingBar : true,
			pageSize : 5
		};
		var userGrid = createGridComp(setGridProp);

		var setUserGridProps = getGridByMemoryVariabilityProps(that['id']
				+ '_setGrid', 'grid.popup.seluser', null, getConstText({
			isArgs : true,
			m_key : 'res.00012'
		}));
		var setUserGrid = createGridComp(setUserGridProps);

		var choiceBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.choice'
			})
		});

		// 버튼 패널
		var btnPanel = Ext.create('Ext.panel.Panel', {
			border : false,
			layout : {
				type : 'vbox',
				align : 'center'
			},
			margin : '5 0 5 0',
			items : [ choiceBtn ]
		});

		// 패널 병합
		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			layout : {
				type : 'vbox',
				align : 'center'
			},
			items : [ userSearchForm, userGrid, btnPanel, setUserGrid ]
		});

		var acceptBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct',
			scale : 'medium'
		});

		userSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : 597,
			width : 600,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				},
				cellclick : function(grid, td, cellIndex, record, tr, rowIndex,
						e, eOpts) {
				}
			}
		});

		popUp_comp[that['id']] = userSelectPop;

		function goSearch() {
			var searchForm = Ext.getCmp(searchFormId);
			var paramMap = searchForm.getInputData();
			if(that['params'] != null) {
				for(var key in that['params']) {
					paramMap[key] = that['params'][key];
				}
			}
			Ext.getCmp(that['id'] + '_grid').searchList(paramMap);
		}

		function acceptEvent() {
			gridAllRowCopyToGrid(that['id'] + '_setGrid', that['targetGrid'],
					true, null, {
						primaryKeys : [ 'USER_ID' ]
					});
			userSelectPop.close();
		}

		function choiceEvent() {
			checkGridRowCopyToGrid(that['id'] + '_grid', that['id']
					+ '_setGrid', {
				primaryKeys : [ 'USER_ID' ]
			});
		}

		attachBtnEvent(acceptBtn, acceptEvent);
		attachBtnEvent(choiceBtn, choiceEvent);

	} else {
		userSelectPop = popUp_comp[that['id']];
	}

	return userSelectPop;
}

/**
 * @function
 * @summary 공통 엑셀업로드 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createExcelUploadPop(paramMap){
	var that = {};
	for( var prop in paramMap ){
		that[prop] = paramMap[prop];
	}
	var excelType = that['excelType'];
	var popObj = null;
	var validFlag = false;
	var fileUploadBtn	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.file.upload' }), id: 'fileUploadBtn', ui:'correct'});	
	var templateDownBtn	= createBtnComp({label: 'Template', id: 'templateDownBtn', icon :'/itg/base/images/ext-js/common/btn-icons/excel-icon.png'});	
	var gridValidBtn	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.validation' }), id: 'gridValidBtn', margin: '10 0 10 400', ui:'correct', scale:'medium'});	
	var gridInsertBtn	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.insert' }), id: 'insertBtn', ui:'correct'});	
	
	var fileAttachCmp = createAtchFileFieldComp({ 
				label: ''
			  , id: 'atch_file_name'
			  , name: 'atch_file_name'
			  , width : 865
			  , maxFileCount: 1
			  , notNull: true
			  , fileHeight: 80
	});
	
	if(popUp_comp[that['id']] == null) {
		var fileUploadPanelProp = {
				  id: 'fileUploadPanel'
				, editOnly		: true
				, formBtns		: [fileUploadBtn, templateDownBtn]
				, border		: true
				, title			: getConstText({ isArgs: true, m_key: 'res.label.itam.maint.00016' })
				, columnSize	: 1
				, tableProps	: [{	colspan:1, tdHeight: 105, item: fileAttachCmp }]
		};
		var fileUploadPanel = createSeachFormComp(fileUploadPanelProp);	
		
		var filePanel = createHorizonPanel({ panelItems: [ { flex: 2, items: fileUploadPanel }]});
		
		var getExcelUploadGridProp = {
				  id					: "excelTempUploadGrid"
				, title				: "업로드 목록" + "<font color='#B2CCFF' style='float:right'>&nbsp;■ 실패&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color='#CEF279' style='float:right'>&nbsp;■ 중복&nbsp;</font>"
				, gridHeight			: 340
				, gridWidth			: 887
				, resource_prefix	: that['resource_prefix']
				, isMemoryStore : true
				, params			: null
				, pagingBar 		: false
				, selfScroll 		: true
				, cellEditing		: true
				, forceFit			: false
				, autoScroll 		: true
				, emptyMsg			: getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00010'})
		};
		var tempGrid = createGridComp(getExcelUploadGridProp);
		
		var validResultGridProp = {
				  id				: "validResultGrid"
				, title				: getConstText({ isArgs: true, m_key: 'res.label.code.00014' })
				, gridHeight		: 97
				, gridWidth			: 887
				, resource_prefix	: 'grid.system.excelUpload.validResult'
				, isMemoryStore 	: true
				, params			: null
				, pagingBar 		: false
				, selfScroll 		: true
				, forceFit			: true
				, autoScroll 		: true
				, emptyMsg			: getConstText({ isArgs: true, m_key: 'msg.asset.tableMng.00010'})
		};
		var validResultGrid = createGridComp(validResultGridProp);
		
		
		var callbackSub = function(data){
			var fileCompId = Ext.getCmp("atch_file_name_grid").atch_file_id;
				
			if(fileCompId != ""){
				jq.ajax({ type:"POST"
						, url: that['parseUrl']
						, contentType: "application/json"
						, dataType: "json"
						, async: false
						, data : getArrayToJson( { atch_file_id: fileCompId, hearders : that['hearders'] } )
						, success:function(data){
							if( data.success || data.resultMap.storeList != undefined){
								var storeList 		= data.resultMap.storeList;
								var gridObj  		= Ext.getCmp("excelTempUploadGrid");
								var gridStore	= gridObj.getStore();
							    for(var i = 0; i < storeList.length; i++){
							  	  gridStore.insert(i, storeList[i]);
							    }
							}else{
								alert(data.resultMsg);
							}
						}
				});
			}
		}
		
		var gridInsertBtnEvent = function (){
			if(validFlag){
				var store 			= Ext.getCmp('excelTempUploadGrid').getStore();
				var paramList 	= [];
				var paramMap	 = {};
				for(var k = 0; k < store.data.items.length; k++){
					paramList.push(store.getAt(k).data);
				}
				paramMap['dataList'] = paramList;
				
				jq.ajax({ type:"POST"
						, url: that['insertUrl']
						, contentType: "application/json"
						, dataType: "json"
						, async: false
						, data : getArrayToJson(paramMap )
						, success:function(data){
							if( data.success ){
								alert(data.resultMsg);
								windowComp.close();
							}else{
								alert(data.resultMsg);
							}
						}
					});
			}else{
				showMessage(getConstText({ isArgs: true, m_key: 'msg.code.result.00007' }));
			}
		}
		
		var gridValidBtnEvent = function (validRulesMap){
			var grid 					= jq('#excelTempUploadGrid-body').find('.x-grid-row');
			var gridObj 			= Ext.getCmp('excelTempUploadGrid');
			var store 				= gridObj.getStore();
			var dataLength 	= store.data.items.length;
			var allCnt 				= dataLength;
			var successCnt 	= dataLength;
			var failCnt 			= 0;
			var failFlag 			= 0;
			
			for(var k = 0; k < dataLength; k++){
					var dataMap	= store.getAt(k).data;
					var colIndex 		= 0;
					for ( var dataKey in dataMap) {
						for ( var validKey in validRulesMap) {
							if(dataKey == validKey){
								var ruleKey 	= validRulesMap[validKey];
								var rule 			= ruleKey.split(",");
								for(var x = 0; x < rule.length; x++){
									
									if( rule[x] == "NULL" ){ //제약조건들 추가해나가면 됩니다.
										if(dataMap[dataKey] == null || dataMap[dataKey] == "" || dataMap[dataKey] == "-"){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										}
									}else if( rule[x] == "NUMBER" ){										
										if( isNaN(dataMap[dataKey]) == true ){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										}
									}else if( rule[x] == "STRING" ){
										if( isNaN(dataMap[dataKey]) ){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										}
									}else if( rule[x] == "BIGALPHA"){
										var regExp = /^([A-Z0-9|_^]+)$/;
										if(!regExp.test(dataMap[dataKey])){
											failFlag = failFlag+1;
											grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #B2CCFF");
										};
									}else if(rule[x] == "PK"){
										
										for(var p = k+1; p < dataLength; p++){
											var compareMap = store.getAt(p).data[dataKey];
											var nowVal = dataMap[dataKey];
											
											if(compareMap == nowVal){
												failFlag = failFlag+1;
												grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #CEF279");
												grid.eq(p).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #CEF279");
											}
										}
										var resultFunc = function( data ){
										if( data.success ){
											var cnt = data.resultString;
											if(cnt  > 0){
												failFlag = failFlag+1;
												grid.eq(k).find('.x-grid-cell-inner').eq(colIndex).attr("style", "background-color: #CEF279");
											}
										}else{
												alert(data.resultMsg);
											}
										}
										dataControls( that['validUrl'], dataMap, resultFunc );
									}else{
										
									}
								}
							}
						}
						colIndex = colIndex +1;
					}
					
					if(failFlag > 0){
						var successCnt = successCnt-1;
						var failCnt = failCnt+1;
					}
			}
			var validGridStore = Ext.getCmp('validResultGrid').getStore();
			validGridStore.removeAll();
			validGridStore.insert(0, {ALL_CNT : allCnt, SUCCESS_CNT : successCnt, FAIL_CNT : failCnt});
			
			if(failCnt == 0){
				validFlag = true;
				showMessage(getConstText({isArgs : true,m_key : 'msg.common.00013'}));
			}
		}
		
		var fileUploadBtnEvent = function (){
				Ext.getCmp('excelTempUploadGrid').getStore().removeAll();

				if(jQuery('#atch_file_name_grid-body').find(".x-grid-cell-inner").text() != ""){
					var isFile = checkFileSave("atch_file_name", {}, callbackSub);
					if( !isFile ){
						alert("파일등록 실패");
					}
				}else{
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.inventory.00003' }));
				}
		}
		
		var templateDownBtnEvent = function (){
				var downloader = Ext.create('nkia.custom.FileDownloadFrame', {
					id:  "TEMPLATE_DOWNLOAD_FRAME",
					renderTo: Ext.getBody()
				});
				
				var atch_file_id = '';
				
				if(excelType == "CODE"){
					atch_file_id = 9999;
					downloader.load({
						url: getConstValue('CONTEXT') + '/itg/base/downloadTemplate.do?atch_file_id=' + atch_file_id
					});	
				}else if(excelType == "TEST"){ //excelType에 따라 분기처리하세요.
				
				}
		}
			
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				windowComp.close();
			}
		});
		
		attachBtnEvent(fileUploadBtn				, fileUploadBtnEvent);
		attachBtnEvent(gridValidBtn					, gridValidBtnEvent, that['validRules']);
		attachBtnEvent(gridInsertBtn					, gridInsertBtnEvent);
		attachBtnEvent(templateDownBtn		, templateDownBtnEvent);
				
		var viewportProperty = {
				  borderId	: 'popupBorderId'
				, width		: 938
				, viewportId : 'popupViewId' 
				, center: { items : [filePanel, tempGrid, gridValidBtn, validResultGrid] }
		}
				
		var codePopupProp		= {
				  id:   that['id']+'Pop'
				, title: getConstText({ isArgs: true, m_key: 'res.label.itam.excelUpload.'+excelType+'.00001'})
				, width: 900
				, height: 675
				, modal: true
				, layout: 'fit'
				, buttons: [gridInsertBtn, closeBtn]
				, items: createBorderViewPortComp(viewportProperty, { isLoading: false, isResize : false} )
		  }

		windowComp = createWindowComp(codePopupProp);
		windowComp.show();
		
	} else {
		popObj = popUp_comp[that['id']];
	}
	
	return popObj;
}

/**
 * @function
 * @summary 엑셀다운로드 속성선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 * @author 전민수
 * @since 1.0
 */
function createExcelDownPropSelectPop(setPopUpProp) {
	var that = {};
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	var storeParam = {};
	var propSelectPop = null;
	var propNmArray = [];
	var propIdArray = [];
	var colNm;
	var colId;
	var addData = {};

	var tempExcelParam = that["excelParam"];
	var param = that["param"];

	if (popUp_comp[that['id']] == null) {
		// @@20130724 전민수 START
		// selModelDefault : true 함으로써 최초 속성선택 그리드 로드시 전체 체크되어진체로 로드되어짐
		var setGridProp = {
			id : that['id'] + '_grid',
			gridHeight : 380,
			resource_prefix : 'grid.popup.excel',
			selModel : true,
			selModelDefault : true,
			isMemoryStore : true,
			autoLoad : false,
			forceFit : false,
			remoteSort: false
		};
		// @@20130724 전민수 END
		var propGrid = createGridComp(setGridProp);
		colNm = tempExcelParam['headerNames'].split(",");
		colId = tempExcelParam['includeColumns'].split(",");

		var excelGridStore = getGridStore(that['id'] + '_grid');

		for ( var i = 0; i < colNm.length; i++) {
			addData['RNUM'] = i;
			addData['COL_NM'] = colNm[i];
			addData['COL_ID'] = colId[i];
			excelGridStore.add(addData);
		}

		var acceptBtn = createBtnComp({
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct',
			scale : 'medium'
		});

		function selectEvent() {
			var selectedRecords = propGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.common.00015'
				}));
			} else {
				for ( var i = 0; i < selectedRecords.length; i++) {
					var col_nm = nullToSpace(selectedRecords[i].data.COL_NM);
					var col_id = nullToSpace(selectedRecords[i].data.COL_ID);
					propNmArray.push(col_nm);
					propIdArray.push(col_id);
				}

				var headerNm = propNmArray.join();
				var headerId = propIdArray.join();

				// @@ 20130903 고은규 엑셀 Align 추가
				var excelAttrs = {
					sheet : [ {
						sheetName : tempExcelParam['sheetName'],
						titleName : tempExcelParam['titleName'],
						headerWidths : tempExcelParam['headerWidths'],
						headerNames : headerNm,
						includeColumns : headerId,
						groupHeaders : tempExcelParam['groupHeaders'],
						align : tempExcelParam['align']
					} ]
				};

				var excelParam = {};
				excelParam['fileName'] = tempExcelParam["fileName"];
				excelParam['excelAttrs'] = excelAttrs;

				goExcelDownLoad(tempExcelParam['url'], param, excelParam);
				propSelectPop.close();
			}
		}

		attachBtnEvent(acceptBtn, selectEvent);

		propSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'] + '_Pop',
			title : getConstText({
				isArgs : true,
				m_key : 'res.00029'
			}),
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			buttonAlign : getConstValue('DEFAULT_POP_BTN_ALIGN'),
			buttons : [ acceptBtn ],
			bodyStyle : getConstValue('DEFAULT_POP_CSS'),
			items : [ propGrid ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = propSelectPop;

	} else {
		propSelectPop = popUp_comp[that['id']];
	}

	propSelectPop.show();

	return propSelectPop;
}
 
/**
 * @function
 * @summary 멀티코드 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createMultiCodeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var multiCodePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {

		var popUpBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct',
			scale : 'medium'
		});
		var setTreeProp = {
			id : that['treeId'],
			url : getConstValue('CONTEXT')
					+ '/itg/base/searchMutilCodeDataListForTree.do',
			title : that['popUpTitle'],
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : false,
			collapseAllBtn : false,
			params : {
				grpCodeId : that['grpCodeId']
			},
			rnode_text : that['rnode_text'],
			expandLevel : 2
		}

		var staticTreeforPop = createTreeComponent(setTreeProp);

		attachCustomEvent('beforeitemdblclick', staticTreeforPop, dbSelectEvent);

		multiCodePop = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'] + " " + getConstText({
				m_key : "SELECT_POPUP"
			}),
			height : 400,
			width : 300,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : [ popUpBtn ],
			bodyStyle : 'background-color: white; ',
			items : [ staticTreeforPop ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUpBtn.on('click', dbSelectEvent);

		function dbSelectEvent() {
			var node = Ext.getCmp(that['treeId']).view.selModel.getSelection();
			if (node.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00033'
				}));
			} else {

				var node_leaf = node[0].raw.leaf;
				if (that['leafSelect'] == true) {
					if (node_leaf == false) {
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00032'
						}));
						/*
						Ext.getCmp(that['treeId']).expandAll(function() {
							//Ext.getCmp(that['treeId']).getEl().unmask();
							//toolbar.enable();
						});
						*/
						return;
					}
				}
				node_id = node[0].raw.node_id;
				node_nm = node[0].raw.text;
				
				if (that['selNodeText'] != true){
					var selctedNode = node[0].raw;
					var text = selctedNode.text;
					var nodeStore = Ext.getCmp(that['treeId']).view.selModel.getStore().data;
					var nodeNmArray = new Array();
					
					var nodeUpNodeId = selctedNode.up_node_id;
					
					for(var i=0; i<nodeStore.length; i++){
						if( nodeStore.items[i].raw.node_id == nodeUpNodeId ){
								
							nodeNmArray.push(nodeStore.items[i].raw.text);
							nodeUpNodeId = nodeStore.items[i].raw.up_node_id;
							i = -1;
							
							if(nodeUpNodeId == "GRP_ID"){
						
							node_nm = "";
						
							for(var j=nodeNmArray.length-2; j>-1; j--){
								node_nm += nodeNmArray[j]+" > "
							}
							node_nm += text;
							break;
						}
					} 	
				}
			}	

				that['targetHiddenField'].setValue(node_id);
				that['targetField'].setValue(node_nm);

				multiCodePop.close();
			}
		}

		popUp_comp[that['popId']] = multiCodePop;

	} else {
		multiCodePop = popUp_comp[that['popId']];
	}

	return multiCodePop;
}

/**
 * @function
 * @summary 트리선택 선택 팝업(공통)
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createTreeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var treeSelectPopBody = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {

		var popUpBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			id : that['treeId'] + "btn",
			ui : 'correct',
			scale : 'medium'
		});

		var setTreeProp = {
			context : '${context}',
			id : that['treeId'],
			title : that['title'] ? that['title'] : '',
			url : that['url'],
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : that['expandAllBtn'] ? that['expandAllBtn'] : true,
			collapseAllBtn : that['collapseAllBtn'] ? that['collapseAllBtn']
					: true,
			params : that['params'] ? that['params'] : {},
			rnode_text : that['rnode_text'] ? that['rnode_text'] : '프로세스유형',
			expandLevel : that['expandLevel'] ? that['expandLevel'] : 1
		}

		var staticTreeforPop = createTreeComponent(setTreeProp);

		attachCustomEvent('beforeitemdblclick', staticTreeforPop, dbSelectEvent);
		
		// btnHide 값을 true 로 주면 적용 버튼을 숨긴다.
		var btnControll = [popUpBtn];
		if(that['btnHide'] == true){	btnControll = [];	}

		treeSelectPopBody = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			height : 400,
			width : 300,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : btnControll,
			bodyStyle : 'background-color: white; ',
			items : [ staticTreeforPop ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUpBtn.on('click', dbSelectEvent);

		function dbSelectEvent() {
			// accept btnHide 
			if(that['btnHide'] == true){	return false;	}
			
			var node = Ext.getCmp(that['treeId']).view.selModel.getSelection();
			if (node.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00033'
				}));
			} else {	
				
				//@@정중훈20131105 시작
				//Root는 선택이 안되게 만듭니다.
				// true : 선택이 됨
				// false : 선택이 안됨
				if(false == that["rootSelect"]){
					var node_level = node[0].raw.node_level;
					if(1 == node_level){
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00044'
						}));
						return;
					}
				}
				//@@정중훈20131105 종료

				var node_leaf = node[0].raw.leaf;
				if (that['leafSelect'] == true) {
					if (node_leaf == false) {
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00032'
						}));
						/*
						Ext.getCmp(that['treeId']).expandAll(function() {
							Ext.getCmp(this.id).getEl().unmask();
							//toolbar.enable();
						});
						*/
						return;
					}
				}
				
				// 20160112 정정윤 추가  [입력값]-[이상]의 레벨만 선택 가능 하도록
				if(that["aboveLevel"] > -1){
					var node_level = node[0].raw.node_level;
					if(node_level < that["aboveLevel"]){
						alert(
							getConstText({
								m_key : "msg.system.result.00050"
								, isArgs : true
								,args : String(that["aboveLevel"]) 
							})
						);
						return;
					}
				}
				
				// 20160112 정정윤 추가  [입력값]-[이하]의 레벨만 선택 가능 하도록
				if(that["belowLevel"] > -1){
					var node_level = node[0].raw.node_level;
					if(node_level > that["belowLevel"]){
						alert(
							getConstText({
								m_key : "msg.system.result.00051"
								, isArgs : true
								,args : String(that["aboveLevel"]) 
							})
						);
						return;
					}
				}
				
				node_id = node[0].raw.node_id;
				node_nm = node[0].raw.text;
				
				var otherData = node[0].raw.otherData;

				if(typeof that['targetVersionField'] != "undefined") {
					var versionText = "";
					if(otherData.SW_MNGT_VERSION != "ALL") {
						versionText = otherData.SW_MNGT_VERSION;
					}
					that['targetVersionField'].setValue(versionText);
				}
				if(typeof that['targetEditionField'] != "undefined") {
					that['targetEditionField'].setValue(otherData.SW_MNGT_EDITION);
				}
				
				that['targetHiddenField'].setValue(node_id);
				that['targetField'].setValue(Ext.util.Format.htmlDecode(node_nm));

				treeSelectPopBody.close();
			}
		}

		popUp_comp[that['popId']] = treeSelectPopBody;

	} else {
		treeSelectPopBody = popUp_comp[that['popId']];
	}

	return treeSelectPopBody;
}

/**
 * @function
 * @summary 구성 서비스 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createCiServicePop(setPopUpProp) {
	var that = {};
	var ciServiceSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var popType = that["popType"];
	if(popType == "basic") {
		ciServiceSelectPop = createBasicCiSercivePop(setPopUpProp);
	} else if(popType == "ciAndService") {
		popUp_props[that['popupId']] = setPopUpProp;
		window.open(getConstValue('CONTEXT') + "/itg/nbpm/common/goCiServiceSelectPop.do?popId=" + that['popupId'], "service_sel_pop", "width="+ getConstValue("PROCESS_CIPOP_WIDTH") +",height=650,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
		//ciAssetSelectPop = createCiAssetAndServicePop(setPopUpProp);
	}
	
	return ciServiceSelectPop;
}

/**
 * @function
 * @summary 일반 프로세스 구성, 서비스 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createBasicCiSercivePop(setPopUpProp) {

	var that = {};
	var servicePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var param = (that['param']) ? that['param'] : {};
	var selectedList = that['selectedList'];
	var selectedServiceIds = new Array();
	for(var i = 0 ; selectedList != null && i < selectedList.length ; i++) {
		selectedServiceIds.push(selectedList[i].service_id);
	}
	param["selectedList"] = selectedServiceIds;
	
	var checkServiceTreeId = 'checkServiceTree'; // sourceGridId
	var setServiceTreeProp = {
		id : checkServiceTreeId,
		url : getConstValue('CONTEXT') + '/itg/nbpm/common/searchServiceList.do',
		title : getConstText({
			isArgs : true,
			m_key : 'res.label.itam.service.00001'
		}),
		// height: (height) ? height : null,
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		autoLoad : false,
		collapseAllBtn : true,
		params : param,
		rnode_text : '#springMessage("res.00003")',
		expandLevel : 2
	}
	
	var checkServiceTree = createTreeComponent(setServiceTreeProp);
	
	if( !that["multiYn"] ){
		checkServiceTree.on("load", function(){
			checkServiceTree.getRootNode().cascadeBy(function (n) {
				delete n.set("checked", null);
			});
		});	
	}
	
	
	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn",
		ui : 'correct',
		scale : 'medium'
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.service'
		}),
		width : that['popupWidth'],
		height : that['popupHeight'],
		buttons : [ applyBtn ],
		items : [ checkServiceTree ],
		closeBtn : true
	}
	servicePop = createWindowComp(windowProp);
	
	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'treeLoad':
			var treeView = checkServiceTree.getView();
			var treeStore = checkServiceTree.getStore();
			break;
		case 'applyBtn': // 적용버튼 Click
			// 선택된 Tree 데이터를 타켓Grid에 복사 ( 소스Tree, 타켓Grid, append속성, 노데이타 메시지,
			// 검증키 )
			clearGridData(that['openerTargetGridId']);
			if( !that["multiYn"] ){
				selTreeNodeCopyToGridOnlyOne(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});	
			}else{
				treeCheckNodeCopyToGrid(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});
			}
			servicePop.close();
			break;
		}
	}
	/** ********************* Private Function Area End ** */
	return servicePop;
}

/**
 * @function
 * @summary 프로세스 구성 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createCiAssetPop(setPopUpProp) {
	var that = {};
	var ciAssetSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var req_zone = that['param']['req_zone'];
	
	var popType = that["popType"];
	if(popType == "basic") {
		ciAssetSelectPop = createBasicCiAssetPop(setPopUpProp);
	} else if(popType == "ciAndService") {
		popUp_props[that['popupId']] = setPopUpProp;
		window.open(getConstValue('CONTEXT') + "/itg/nbpm/common/goCiAssetSelectPop.do?popId=" + that['popupId'] + "&req_zone=" + req_zone, "ci_sel_pop", "width="+ getConstValue("PROCESS_CIPOP_WIDTH") +",height=650,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
	}
	return ciAssetSelectPop;
}

/**
 * @function
 * @summary 기본 구성 자산 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createBasicCiAssetPop(setPopUpProp) {
	var that = {};
	var ciAssetSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	// itam.js 참조 : ITAM분류체계트리(전부)
	var assetClassTree = getAssetClassAllTree('amClassAllTree', 3,
			that['params']);
	attachCustomEvent('select', assetClassTree, treeNodeClick);

	var assetBookMarkCateTree = {
		id : 'assetBookMarkCateTree',
		url : getConstValue('CONTEXT')
				+ '/itg/itam/amdb/searchAmClassAllTree.do',
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.bookMark'
		}),
		dynamicFlag : false,
		searchable : false,
		searchableTextWidth : 100,
		expandAllBtn : false,
		collapseAllBtn : false,
		params : null,
		rnode_text : getConstText({
			isArgs : true,
			m_key : 'res.00015'
		}),
		expandLevel : 2
	}
	var ciAssetBookMarkCateTree = createTreeComponent(assetBookMarkCateTree);
	attachCustomEvent('select', ciAssetBookMarkCateTree, treeNodeClick);

	var tabProperty = {
		tab_id : 'ciAssetTreeTab',
		width : '100%',
		layout : 'center',
		item : [ assetClassTree, ciAssetBookMarkCateTree ]
	// item: [assetClassTree]
	}
	var treeTabs = createTabComponent(tabProperty);

	// itam.js 참조 : 자산검색조건 / formId, searchBtnId, type, columnSize, callFuncKey
	var searchFormId = 'searchAssetForm' + that['popupId']; // searchFormId
	var searchAssetForm = getAssetSearchForm(searchFormId, 'searchBtn',
			'Atype', 2, searchBtnEvent);

	/**
	 * 검색 폼에서 text에서 엔터키 칠때 호출
	 */
	function searchBtnEvent() {
		actionLink('search');
	}

	// itam.js 참조 : 자산리스트 Grid(체크박스형태) / gridId
	var sourceGridId = 'checkAssetGrid'; // sourceGridId

	// itam.js 참조 : 선택자산목록에 사용되는 자산목록 Grid(MemoryStore)
	var targetGridId = 'selAssetGrid'; // targetGridId

	// 구성자원관리 조회 팝업의 GRID Prefix 및 소스 URL 경로를 변경 시켜줄 경우 사용
	if (that['targetPrefix'] != null && that['sourceUrl'] != null
			&& that['sourcePrefix'] != null) {
		var targetPrefix = that['targetPrefix']; // targetGridPrefix
		var selAssetGrid = getAssetGridByMemoryVariability(targetGridId,
				targetPrefix, {selectedList : that['selectedList'], height:170});

		var sourceUrl = that['sourceUrl']; // sourceGridUrl
		var sourcePrefix = that['sourcePrefix']; // sourceGridPrefix
		var checkAssetGrid;
		if( !that['multiYn'] ){
			checkAssetGrid = getRadioCheckAssetGridVariability(sourceGridId, sourceUrl, sourcePrefix, {height : 200});
		}else{
			checkAssetGrid = getCheckAssetGridVariability(sourceGridId, sourceUrl, sourcePrefix, {height : 200});
		}
		

	} else {
		var checkAssetGrid = getCheckAssetGrid(sourceGridId, {
			pageSize : 5,
			gridHeight : 200
		});
		
		var selAssetGrid = getAssetGridByMemory(targetGridId, {
			pageSize : 5,
			gridHeight : 170
		});
	}

	// 선택버튼
	var choiceBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.choice'
		}),
		id : "choiceBtn"
	});
	attachBtnEvent(choiceBtn, actionLink, 'choiceBtn');
	var choiceBtnpanel = createHorizonPanel({
		panelBtns : [ choiceBtn ]
	});

	// 좌측 패널 : Tree
	var left_panel = Ext.create('Ext.panel.Panel', {
		height : '100%',
		width : 200,
		region : 'west',
		autoScroll : false,
		split : true,
		maxWidth : 200,
		minWidth : 200,
		items : [ treeTabs ]
	});

	// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
	var right_panel = Ext.create('Ext.panel.Panel',
			{
				border : false,
				region : 'center',
				items : [ searchAssetForm, checkAssetGrid, choiceBtnpanel,
						selAssetGrid ]
			});

	// 패널 병합
	var complexPanel = Ext.create('nkia.custom.Panel', {
		border : false,
		layout : {
			type : 'border'
		},
		items : [ left_panel, right_panel ],
		listeners : {
			afterrender : function() {
				left_panel.splitter.setWidth(1);
				this.doLayout();
			}
		}
	});

	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn",
		ui : 'correct',
		scale : 'medium'
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.00019'
		}),
		width : that['popupWidth'],
		height : that['popupHeight'],
		buttons : [ applyBtn ],
		items : [ complexPanel ],
		layout : {
			type : 'border'
		},
		closeBtn : true
	}
	ciAssetSelectPop = createWindowComp(windowProp);
	// Opener Grid의 데이터를 타겟 Grid에 복사 : 구성자원 데이터( 소스Grid, 타켓Grid, append속성, 노데이타
	// 메시지 )
	// gridAllRowCopyToGrid( that['openerTargetGridId'], targetGridId, false,
	// null);

	/** * Private Function Area Start ******************** */
	function treeNodeClick(tree, record, index, eOpts) {
		var am_class_id = record.raw.node_id;
		getForm(searchFormId).setFieldValue("class_id", am_class_id); // 분류체계ID
		// hidden
		goSearch();
	};

	var appendable;
	if (that['appendable'] != null) {
		appendable = that['appendable'];
	} else {
		appendable = false;
	}
	
	//기존에 선택된 리스트가 있다면 세팅한다.
	actionLink('gridLoad');
	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'gridLoad' :
			var changeSelectedList = new Array();
			for(var i = 0 ; that['selectedList'] != null && i < that['selectedList'].length ; i++) {
				var row = {};
				for(var key in that['selectedList'][i]) {
					row[key.toUpperCase()] = that['selectedList'][i][key] 
				}
				changeSelectedList.push(row);
			}
			addDataListToGrid(targetGridId, changeSelectedList, {
				primaryKeys : that['primaryKeys']
			})
			break;
		case 'choiceBtn': // 선택버튼 Click
			// 선택된 정보 타켓Grid에 복사 ( 소스Gird, 타켓Grid, 중복체크 keys )
			if( !that['multiYn'] ){
				checkGridRowCopyToGridOnlyOne(sourceGridId, targetGridId, {
					primaryKeys : that['primaryKeys']
				});
			}else{
				checkGridRowCopyToGrid(sourceGridId, targetGridId, {
					primaryKeys : that['primaryKeys']
				});	
			}
			break;
		case 'applyBtn': // 적용버튼 Click
			clearGridData(that['openerTargetGridId']);
			// Grid 데이터 전체를 타켓Grid에 복사 ( 소스Grid, 타켓Grid, append속성, 노데이타 메시지 )
			gridAllRowCopyToGrid(targetGridId, that['openerTargetGridId'],
					appendable, null, {
						primaryKeys : that['primaryKeys']
					});
			ciAssetSelectPop.close();
			break;
		case 'search': // 검색버튼 Click
			goSearch();
			break;
		}
	}

	// 검색
	function goSearch() {
		var theForm = getForm(searchFormId);
		var paramMap = theForm.getInputData();
		if (that['params'] != null && that['params'].assetId) {
			paramMap['asset_id'] = that['params'].assetId;
		}
		getGrid(sourceGridId).searchList(paramMap);
	}
	/** ********************* Private Function Area End ** */
	return ciAssetSelectPop;
}

/**
 * @function
 * @summary 서비스연계조회 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createServiceRelPop(setPopUpProp) {
	var that = {};
	var servicePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	var ciCustTreeIdProp = {
		context : '${context}',
		id : 'ciCustTree',
		url : getConstValue('CONTEXT') + '/itg/itam/oper/searchSystemList.do',
		title : getConstText({
			isArgs : true,
			m_key : 'res.00013'
		}),
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		collapseAllBtn : true,
		height : 334,
		params : null,
		autoLoad : true,
		rnode_text : getConstText({
			isArgs : true,
			m_key : 'res.00003'
		}),
		expandLevel : 3
	}
	var ciCustTree = createTreeComponent(ciCustTreeIdProp);

	var checkServiceTreeId = 'checkServiceTree'; // sourceGridId
	var checkServiceTree = getCheckServiceTree(checkServiceTreeId, 1,
			that['param'], 334);

	var complexItem = [];
	if (that['flag'] == true || that['flag'] == undefined) {
		complexItem.push({
			items : checkServiceTree
		});
	} else {
		complexItem.push({
			flex : 1,
			items : ciCustTree
		});
		complexItem.push({
			flex : 1,
			items : checkServiceTree
		});
	}

	var complexPanelProp = {
		border : true,
		panelItems : complexItem
	}
	var complexPanel = createHorizonPanel(complexPanelProp);

	attachCustomEvent('itemclick', ciCustTree, ciCustTreeClick);

	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn"
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.service'
		}),
		width : 600,
		height : 400,
		buttons : [ applyBtn ],
		items : [ complexPanel ],
		closeBtn : true
	}
	servicePop = createWindowComp(windowProp);

	/** * Private Function Area Start ******************** */

	function ciCustTreeClick(tree, record, index, eOpts) {

		var cust_id = record.raw.node_id;

		var checkServiceTreeStore = getGrid(checkServiceTreeId).getStore();
		checkServiceTreeStore.proxy.jsonData = {
			cust_id : cust_id,
			expandLevel : 1
		};
		checkServiceTreeStore.reload();
	}

	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'applyBtn': // 적용버튼 Click
			// 선택된 Tree 데이터를 타켓Grid에 복사 ( 소스Tree, 타켓Grid, append속성, 노데이타 메시지,
			// 검증키 )
			treeCheckNodeCopyToGrid(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});
			servicePop.close();
			break;
		}
	}
	/** ********************* Private Function Area End ** */
	return servicePop;
}

/**
 * @function
 * @summary 제조사 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createVendorSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var vendorSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.title.system.vendor.list'
			}), // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.vendor.popup', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			pagingBar : true,
			pageSize : 10
		};

		var vendorGrid = createGridComp(setGridProp);

		var vendorSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'SearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(vendorSearchBtn, actionLink, 'searchAttr');
		
		var vendorSearchInitBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.reset'
			}),
			id : that['id'] + 'SearchInitBtn'
		});
		attachBtnEvent(vendorSearchInitBtn, actionLink, 'searchInit');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ vendorSearchBtn, vendorSearchInitBtn ],
			enterFunction : clickVendorSearchBtn,
			columnSize : 1,
			tableProps : [ {
				colspan : 1,
				item : createTextFieldComp({
					label : getConstText({
						isArgs : true,
						m_key : 'res.label.system.vendor.00002'
					}),
					name : 'search_vendor_nm'
				})
			} ]
		}

		var vendor_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ vendor_search_form, vendorGrid ]
		})

		var tagetVendorIdField = that['tagetVendorIdField'];
		var tagetVendorNameField = that['tagetVendorNameField'];
		var vendorSalesTelField = that['vendorSalesTelField'];
		var vendorSalesNameField = that['vendorSalesNameField'];
		
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale : 'medium',
			handler: function() {
				vendorSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
			, ui : 'correct'
			, scale : 'medium'
			, handler : function() {
				var selectedRecords = vendorGrid.view.selModel.getSelection();
				if (selectedRecords.length < 1) {
					showMessage(getConstText({
						isArgs : true,
						m_key : 'msg.itam.vendor.00016'
					}));
				} else {

					if (that['afterFunction'] != null) {
						that['afterFunction'](selectedRecords[0].raw,
								that['mappingObj']);
					} else {
						dbSelectEvent();
					}
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();

				vendorGrid.searchList(paramMap);
				break;
			case 'searchInit':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				searchForm.initData();
				break;
			}
		}

		function clickVendorSearchBtn() {
			actionLink('searchAttr');
		}
		
		function dbSelectEvent() {
			var selectedRecords = vendorGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.itam.vendor.00016'
				}));
			} else {

				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var vendor_id = selectedRecords[0].raw.VENDOR_ID;
					var vendor_nm = selectedRecords[0].raw.VENDOR_NM;
					var sales_user_tel = selectedRecords[0].raw.SALES_USER_TEL;
					var sales_user_nm = selectedRecords[0].raw.SALES_USER_NM;
					tagetVendorIdField.setValue(vendor_id);
					tagetVendorNameField.setValue(vendor_nm);
					if(vendorSalesTelField != null && vendorSalesNameField != null){
						Ext.getCmp(that['targetFormId']).setFieldValue(vendorSalesTelField, sales_user_tel);
						Ext.getCmp(that['targetFormId']).setFieldValue(vendorSalesNameField, sales_user_nm);
					}
					vendorSelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', vendorGrid, dbSelectEvent);

		vendorSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = vendorSelectPop;

	} else {
		vendorSelectPop = popUp_comp[that['id']];
	}

	return vendorSelectPop;
}

/**
 * @function
 * @summary 사업 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProjectSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var projectSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : 'res.title.itam.project'
			}), // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.project', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			pagingBar : true,
			pageSize : 10
		};

		var projectGrid = createGridComp(setGridProp);

		var projectSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'ProjectBtn',
			ui : 'correct'
		});
		attachBtnEvent(projectSearchBtn, actionLink, 'searchAttr');
		
		var projectSearchInitBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.reset'
			}),
			id : that['id'] + 'ProjectInitBtn'
		});
		attachBtnEvent(projectSearchInitBtn, actionLink, 'searchInit');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ projectSearchBtn, projectSearchInitBtn ],
			columnSize : 2,
			enterFunction : clickProjectSearchBtn,
			tableProps : [{
				colspan : 1,
				item : createTextFieldComp({
					label : getConstText({
						isArgs : true,
						m_key : 'res.label.itam.project.00002'
					}),
					name : 'search_project_nm',
					width : 250
				})
			}]
		}

		var project_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ project_search_form, projectGrid ]
		})

		var tagetProjectIdField = that['tagetProjectIdField'];
		var tagetProjectNameField = that['tagetProjectNameField'];
		var tagetProjectSeqField = that['tagetProjectSeqField'];
		var tagetEtcField = that['tagetEtcField'];

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale: 'medium',
			handler: function() {
				projectSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			})
			, ui : 'correct'
			, scale: 'medium'
			, handler : function() {
				dbSelectEvent();
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {

			switch (command) {
			case 'searchAttr':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();

				projectGrid.searchList(paramMap);
				break;
			case 'searchInit':
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				searchForm.initData();
				break;
			}
		}
		
		function clickProjectSearchBtn() {
			actionLink('searchAttr');
		}
		
		function dbSelectEvent() {
			var selectedRecords = projectGrid.view.selModel.getSelection();
			if (selectedRecords.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.itam.project.00013'
				}));
			} else {
				if (that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw,
							that['mappingObj']);
				} else {
					var project_id = selectedRecords[0].raw.PROJECT_ID;
					var project_nm = selectedRecords[0].raw.PROJECT_NM;
					tagetProjectIdField.setValue(project_id);
					tagetProjectNameField.setValue(project_nm);
					projectSelectPop.close();
				}
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', projectGrid, dbSelectEvent);

		projectSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 800,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = projectSelectPop;

	} else {
		projectSelectPop = popUp_comp[that['id']];
	}

	return projectSelectPop;
}

/**
 * @function
 * @summary 서비스 조회 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createServicePop(setPopUpProp) {
	var that = {};
	var servicePop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	var checkServiceTreeId = 'checkServiceTree'; // sourceGridId
	var checkServiceTree = getCheckServiceTree(checkServiceTreeId, 1,
			that['param']);

	// 적용버튼
	var applyBtn = createBtnComp({
		visible : true,
		label : getConstText({
			isArgs : true,
			m_key : 'btn.common.apply'
		}),
		id : "applyBtn"
	});
	attachBtnEvent(applyBtn, actionLink, 'applyBtn');

	// Window Prop
	var windowProp = {
		id : that['popupId'],
		title : getConstText({
			isArgs : true,
			m_key : 'res.title.common.service'
		}),
		width : that['popupWidth'],
		height : that['popupHeight'],
		buttons : [ applyBtn ],
		items : [ checkServiceTree ],
		closeBtn : true
	}
	servicePop = createWindowComp(windowProp);

	/** * Private Function Area Start ******************** */
	function serviceCustClick(gridCmp, tdEl, cellIndex, record, trEl, rowIndex,
			e) {
		var cust_id = record.raw.CUST_ID;
		var tree_store = getTreeStore(checkServiceTreeId);
		var rootNode = tree_store.getRootNode();
		while (rootNode.firstChild) { // Root의 ChildNode가 있을 경우
			rootNode.removeChild(rootNode.firstChild);
		}
		tree_store.proxy.jsonData = {
			cust_id : cust_id,
			expandLevel : 1
		};
		tree_store.load();
	}

	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch (command) {
		case 'applyBtn': // 적용버튼 Click
			// 선택된 Tree 데이터를 타켓Grid에 복사 ( 소스Tree, 타켓Grid, append속성, 노데이타 메시지,
			// 검증키 )
			treeCheckNodeCopyToGrid(checkServiceTreeId,
					that['openerTargetGridId'], false, null, {
						primaryKeys : [ 'SERVICE_ID' ]
					});
			servicePop.close();
			break;
		}
	}
	/** ********************* Private Function Area End ** */
	return servicePop;
}

/**
 * @function
 * @summary 프로세스 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcessSelectPop(setPopUpProp) {
	var storeParam = {};
	var that = {};
	var processSelectPop = null;
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
    
	if (popUp_comp[that['id']] == null) {
        
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : '', // Grid Title
			gridHeight : 305,
			resource_prefix : 'grid.pop.process', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],
			forceFit : true,
			pagingBar : true,
			pageSize : 20
		};

		var processGrid = createGridComp(setGridProp);
		
		// 상세보기 컬럼 추가
		var procViewColumn = Ext.create('Ext.grid.column.Action', {
			text: getConstText({ isArgs: true, m_key: 'res.common.label.detailview' }),
			width: 100,
			align: 'center',
			items: [{
		        icon: '/itg/base/images/ext-js/common/icons/bullet_search.gif',
		        tooltip: getConstText({ isArgs: true, m_key: 'res.common.label.detailview' }),
		        handler: function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex);
		            var recordRaw = rec.raw;
		            showProcessDetailPop(recordRaw["SR_ID"]);
		        }
		    }]
		});
		
		processGrid.headerCt.add(procViewColumn);
        
		var searchBtn = new Ext.button.Button({
			id : that['id'] + '_btn',
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			handler : function(thisBtn, event) {
				var param = Ext.getCmp('searchForm').getInputData();
				if(param['end_time_startDate_day'] > param['end_time_endDate_day']){
					if(param['end_time_endDate_day'] == ""){
						alert("완료종료일을 입력해주세요.");
						return false;
					}
					alert("완료일시 입력이 잘못되었습니다.");
					return false;
				}
				
				if(param['end_time_startDate_day'] == "" && param['end_time_endDate_day'] != ""){
					alert("완료시작일을 입력해주세요.");
					return false;
				}
				
				if(param['req_time_startDate_day'] > param['req_time_endDate_day']){
					if(param['req_time_endDate_day'] == ""){
						alert("요청종료일을 입력해주세요.");
						return false;
					}
					alert("요청일시 입력이 잘못되었습니다.");
					return false;
				}

				if(param['req_time_startDate_day'] == "" && param['req_time_endDate_day'] != ""){
					alert("요청시작일을 입력해주세요.");
					return false;
				}
				
				Ext.getCmp(that['id'] + '_grid').searchList(param);
				},
		});
		
		//신승호(shshin)20161210 초기화 생성
		var initBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp('searchForm').initData();
			}
		});

		//신승호(shshin)20161210 요청일시 기본세팅(시작시간:-7, 종료시간: 현재시간 )
		var req_time_startDate_day = createDateTimeStamp('DHM', {year:0, month:0, day:-7, hour:0, min:0});
		var req_time_endDate_day = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		that['params']['req_time_startDate'] = req_time_startDate_day['day'];
		that['params']['req_time_endDate'] = req_time_endDate_day['day'];
		
 		var setSearchFormProp = {
			id : 'searchForm',
			url : '$!{context}/itg/nbpm/common/searchProcessList.do',
			formBtns : [ searchBtn, initBtn ],
			columnSize : 2,
			//@@정중훈20140217 엔터펑션 추가 시작
			enterFunction : function(){
				var param = Ext.getCmp('searchForm').getInputData();
				Ext.getCmp(that['id'] + '_grid').searchList(param);
			},
			//@@정중훈20140217 엔터펑션 추가 종료
			tableProps : [
			// row1
			{
				colspan : 1,
				tdHeight : 28,
				item : createTextFieldComp({
					width : 300,
					label : '요청자',
					name : 'req_user_nm'
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createTextFieldComp({
					width : 300,
					label : '요청번호',
					name : 'sr_id'
				})
			}
			// row3
			 , {
				colspan : 1,
				tdHeight : 28,
				item : createFromToDateContainer({
					label : '완료일시',
					name : 'end_time',
					dateType:'D'
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createFromToDateContainer({
					label : '요청일시',
					name : 'req_time',
					dateType:'D',
					start_dateValue: req_time_startDate_day,
					end_dateValue: req_time_endDate_day
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createCodeComboBoxComp({
					label : '요청유형',
					name : 'req_type',
					code_grp_id:'REQ_TYPE',
					defaultValue: (that['params']['req_type'] != null) && (that['params']['req_type'] != '') ? that['params']['req_type'] : '',
					readOnly: (that['params']['req_type'] != null) && (that['params']['req_type'] != '') ? true : false,
					attachAll:true
				})
			}, {
				colspan : 1,
				tdHeight : 28,
				item : createCodeComboBoxComp({
					label : '요청상태',
					name : 'work_state',
					code_grp_id:'WORK_STATE_SEARCH',
					attachAll:true
				})
			}
			// row2
			, {
				colspan : 2,
				tdHeight : 28,
				item : createTextFieldComp({
					label : '제목',
					name : 'title',
					width : 660
				})
			} ]
		}
		

		var searchFormPanel = createSeachFormComp(setSearchFormProp);
		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ searchFormPanel, processGrid ]
		})
				
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			ui : 'correct'
		});
		if( that["acceptBtnVisible"] == false ){
			acceptBtn = null;	
		}
		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				processSelectPop.close();
			}
		});
		
		processSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height']+28,
			width : that['width'],
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : (acceptBtn != null) ? [acceptBtn, closeBtn] : [closeBtn],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = processSelectPop;
		
		if( acceptBtn != null ){
			function gridSelectEvent(grid, td, cellIndex, record) {
				if( cellIndex != 6 ){	// 상세보기 클릭시는 동작하지 않는다.
					var selectedRecords = processGrid.view.selModel.getSelection();
					if (selectedRecords.length < 1) {
						showMessage("요청을 선택하지 않았습니다.");
					} else {
						that['tagetUpSrIdField'].setValue(selectedRecords[0].raw.SR_ID);
						processSelectPop.close();
					}	
				}
			}			
		}

		attachCustomEvent('celldblclick', processGrid, gridSelectEvent);
		if( acceptBtn != null ){
			attachBtnEvent(acceptBtn, gridSelectEvent)	
		}
		
	} else {
		processSelectPop = popUp_comp[that['id']];
	}
	return processSelectPop;
}

/**
 * @function
 * @summary 코드 검색 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createSelSysCodePopup(commPropeties){
	var that = convertDomToArray(commPropeties);
	
	var targetForm	= that["targetForm"];
	var targetObjId	= that["targetObjId"];
	var targetObjNm	= that["targetObjNm"];
	
	var popId				= targetObjId+'PopId';
	var upCodeId			= "GRP_ID";
	
	
	
	//cell click 시에 호출되는 function
	var getSysCode = function (){
		var paramMap = searchFormPanel.getInputData();
		paramMap["code_grp_id"] 	= upCodeId;
		paramMap["use_yn"] 		= "Y";
		paramMap["paging"] 			= true;
		Ext.getCmp('sysCodeGrid').searchList(paramMap);
	}
	
	//Search Form 초기화
	var setSearchInit = function (){
		searchFormPanel.initData();
	}
	
	//cell click 시에 호출되는 function
	var getSysCodeDetail = function ( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e){
		var paramMap = searchFormPanel.getInputData();
		paramMap["code_grp_id"] 	= record.raw.CODE_ID;
		paramMap["searchValue"]	= "";
		paramMap["searchKey"]	= "";
		paramMap["paging"] 		= false;
		paramMap["use_yn"] 	= "Y";
		Ext.getCmp('sysCodeDetailGrid').searchList(paramMap);
	}
	
	
	//적용버튼 클릭시에 호출되는 function
	var confirmHandler = function(){
			if (isSelected("sysCodeGrid")) {
				var selRow = gridSelectedRows("sysCodeGrid");
				var setTargetForm	= Ext.getCmp(targetForm).getForm();
				if(setTargetForm){
					
					if(setTargetForm.findField(targetObjId)){
						setTargetForm.findField(targetObjId).setValue(selRow.get('CODE_ID'));
					}
					
					if(setTargetForm.findField(targetObjNm)){
						setTargetForm.findField(targetObjNm).setValue(selRow.get('CODE_NM'));
					}
					Ext.getCmp(popId).close();
				}
			}else{
				showMessage("선택된 코드가 없습니다.");
				return;
			}
		}
	//검색버튼
	var searchCodeBtn		= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchCodeBtn' ,ui:'correct' ,handler : getSysCode });
	
	var searchPopInitBtn 	= createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.reset' }), name:'searchPopInitBtn' ,id:'searchPopInitBtn', handler : setSearchInit });
	
	var attrSearchContainer	= createUnionCodeComboBoxTextContainer({label : '검 색', comboboxName: 'searchKey', textFieldName: 'searchValue', code_grp_id: 'CODE_POP_SEARCH_KEY', widthFactor: 0.25 });
	
	var selectSearchKey = attrSearchContainer.getComboBoxField();
	var selectSearchValue = attrSearchContainer.getTextField();
	
	
	var setSearchFormProp 	= {
								 id			: 'codeSearchForm'
								,columnSize	: 1
								,url		: '/itg/system/code/searchCodeDataForChlid.do'
								,formBtns	: [searchCodeBtn, searchPopInitBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [{colspan:1, tdHeight: 30, item : attrSearchContainer}]
								,enterFunction : getSysCode
							  }
	
	var searchFormPanel 	= createSeachFormComp(setSearchFormProp);
	
	
	var setCodeGridProp 	= {	 id			: 'sysCodeGrid'
								,title		: '코드'
								,resource_prefix: 'grid.syscode'
								,url		: '/itg/system/code/searchCodePopup.do'
								,params		: {code_grp_id : upCodeId,paging : true	,use_yn : "Y"}
								,pagingBar 	: true
								,border 	: true
								,gridHeight	: 175
								,pageSize 	: 10
							};
	
	var gridPanel			= createGridComp(setCodeGridProp);
	
	var gridComp1 = Ext.getCmp("sysCodeGrid");
	
	var setCodeDetailGridProp		= {	 id			: 'sysCodeDetailGrid'
										,title		: '코드상세'
										,height		: getConstValue('TALL_GRID_HEIGHT')
										,resource_prefix: 'grid.syscode'
										,url		: '/itg/system/code/searchCodePopup.do'
										,params		: {code_grp_id : "",paging : false	,use_yn : "Y" }
										,pagingBar 	: false
										,border 	: true
										,gridHeight	: 175
										,pageSize 	: 10
									};
	
	var gridDetailPanel		= createGridComp(setCodeDetailGridProp);
	
	var complexPanel = createFlexPanel({items : [searchFormPanel, gridPanel,gridDetailPanel]});
	
	var conFirmBtn		=	createBtnComp({ id : "codePopConfirmBtn" , label:getConstText({ isArgs: true, m_key: 'btn.common.apply' })	,ui   : "correct"	,scale: "medium"	,handler : confirmHandler });
	
	var codePopupProp		= {
								id:   popId
								,title: '공통코드선택'
								,width: 700
								,height: 500
								,modal: true
								,layout: 'fit'
								,buttonAlign: 'center'
								,bodyStyle: 'background-color: white; '
								,buttons: [conFirmBtn]
								,closeBtn : true
								,items: complexPanel
								,resizable:true
							  }
	
	attachCustomEvent('cellclick', gridPanel, getSysCodeDetail);
	
	var callback = function(){
		Ext.getCmp('sysCodeDetailGrid').searchList(null);
	};
	gridPanel.store.on("load",callback);
	
	window_comp = createWindowComp(codePopupProp);
	window_comp.show();
}

/**
 * @function
 * @summary KEDB 등록 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createKedbRegisterPop(setPopUpProp){
	var storeParam = {};
	var that = {};
	var kedbRegPop = null;
	var columnList = ["title", "content", "probm_cause_ty_cd_nm", "probm_cause_ty_cd", "probm_root_cause", "solut_mthd_cn", "opert_cn"];
	var inputData = {};
	for(var i = 0 ; i < panelList.length ; i++) {
		var paramMap = null;
		if(panelList[i].isForm) {
			paramMap = panelList[i].getInputData();
			for(var key in paramMap) {
				for(var j = 0 ; j < columnList.length ; j++) {
					if(key == columnList[j]) {
						var paramName = "";
						if(key == "title") {
							paramName = "kedb_titl";
						} else if(key == "content") {
							paramName = "kedb_cn"; 
						} else {
							paramName = key;
						}
						inputData[paramName] = paramMap[columnList[j]];
						continue;
					}
				}
			}
		}
	}
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	var kedbInfoId = that['id'] + 'kedbInfoForm';
	var causeInfoId = that['id'] + 'causeInfoForm';
	var fileInfoId = that['id'] + 'fileInfoForm';
	if(popUp_comp[that['id']] == null) {
		var kedbCategoryCodeId = 'PROBM_CAUSE_TY_CD';
		var causeInfo = {
				id: causeInfoId,
				title: getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00003'}),
				columnSize: 2,
				editOnly: true,
				border: true,
				tableProps :[{colspan:2, item : createMultiCodeSelectPopComp({ popId : 'probmCauseTypePop', popUpTitle : "문제원인유형", targetName : 'probm_cause_ty_cd_nm', targetHiddenName : 'probm_cause_ty_cd', btnId : 'probmCauseTypeBtn', treeId : 'probmCauseTypeTree', grpCodeId : kedbCategoryCodeId, targetLabel : "문제원인유형", params : null,notNull:true})}
							,{colspan:2, item : createTextFieldComp({label:"KEDB 제목", name:'kedb_titl', notNull:true, maxLength:200})}
							,{colspan:2, item : createTextAreaFieldComp({label:"KEDB 내용", name:'kedb_cn', maxLength:2000})}],
				hiddenFields:[{name : "probm_cause_ty_cd"}]
		}
		var causeInfoForm = createEditorFormComp(causeInfo);
		causeInfoForm.setDataForMap(inputData);
		var kedbInfo = {
				id: kedbInfoId,
				title: getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00010'}),
				columnSize: 2,
				editOnly: true,
				border: true,
				tableProps :[{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00006'}), name:'tmpr_solut_mthd_cn', width: 450, maxLength:2000})}
							,{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00007'}), name:'root_cause_cn', width: 450, maxLength:2000})}
							,{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00008'}), name:'solut_mthd_cn', width: 450, maxLength:2000})}
							,{colspan:1, item : createTextAreaFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00009'}), name:'opert_cn', width: 450, maxLength:2000, notNull:true})}]
		}
		var kedbInfoForm = createEditorFormComp(kedbInfo);
		kedbInfoForm.setDataForMap(inputData);
		var fileInfo = {
				id: fileInfoId,
				title: getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00011'}),
				columnSize: 1,
				editOnly: true,
				border: true,
				tableProps :[{colspan:1, item : createAtchFileFieldComp({label:getConstText({isArgs : true,m_key : 'res.label.workflow.kedb.00011'}), name:'atch_file_id', width: 847, fileHeight: 100, anchor: '94%'})}]
		}
		
		var fileInfoForm = createEditorFormComp(fileInfo);
		var complexPanel = Ext.create('Ext.form.Panel', {
	        id: 'complexPanel',
			items: [causeInfoForm,kedbInfoForm,fileInfoForm]
		});
			
		var tagetKedbIdField = that['tagetKedbIdField'];
		
		var insertBtn = createBtnComp({
			visible : true,
			label : getConstText({isArgs : true, m_key : 'btn.common.regist'}),
			id : that['id'] + 'insertBtn',
			ui : 'correct',
			scale : 'medium'
		});
		
		var closeBtn = createBtnComp({
			visible : true,
			label : getConstText({isArgs : true, m_key : 'btn.common.close'}),
			id : that['id'] + 'closeBtn',
			scale : 'medium'
		});
		
		attachBtnEvent(insertBtn, actionLink, 'insert');
		attachBtnEvent(closeBtn, actionLink, 'close');
		
		function actionLink(command) {
			switch (command) {
				case 'insert':
					if(confirm("등록하시겠습니까?")) {
						if(Ext.getCmp(causeInfoId).isValid() && Ext.getCmp(kedbInfoId).isValid()){
							// 마스크를 띄웁니다.
							setViewPortMaskTrue();
							
							var fileMap = Ext.getCmp(fileInfoId).getInputData();
							var causeMap = Ext.getCmp(causeInfoId).getInputData();
							var kedbMap = Ext.getCmp(kedbInfoId).getInputData();
							
							var paramMap = {};
							for(var key in fileMap) {
								paramMap[key] = fileMap[key];
							}
							for(var key in causeMap) {
								paramMap[key] = causeMap[key];
							}
							for(var key in kedbMap) {
								paramMap[key] = kedbMap[key];
							}
							
							var callbackSub = function(){
					 			jq.ajax({ type:"POST", url: getConstValue('CONTEXT') + '/itg/workflow/kedb/insertKedb.do', 
									contentType: "application/json", dataType: "json" , data : getArrayToJson(paramMap), 
									success:function(data){
										if( data.success ){
											showMessage(data.resultMsg);
											var kedbId = data.resultMap.kedbId;
											tagetKedbIdField.setValue(kedbId);
											kedbRegPop.close();
										}else{
						   				}
						   			
							   			// 마스크를 숨깁니다.
										setViewPortMaskFalse();
									}
								});
							}
					 		var isFile = checkFileSave("atch_file_id", paramMap, callbackSub);
							if( !isFile ){
								eval(callbackSub)();
							}
						} else {
							showMessage(getConstText({ isArgs: true, m_key: 'msg.common.00012' }));
						}
					}
				break;
			case 'close':
				kedbRegPop.close();
				break;
			}
		}
		
		var windowProp = {
			id: that['id'],
			title: that['popUpText'],  
			height: 620,
			width: 970,
			layout:{
				type: 'border'
			},
			buttons: [insertBtn, closeBtn],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		};
		kedbRegPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = kedbRegPop;
		
	} else {
		kedbRegPop = popUp_comp[that['id']];
	}
	
	return kedbRegPop;	
}

/**
 * @function
 * @summary KEDB 조회 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createKedbSelectPop(setPopUpProp){
	
	var storeParam = {};
	var that = {};
	var kedbSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {

		var grpId = "GRP_ID";
		var kedbCategoryCodeId = "PROBM_CAUSE_TY_CD";
		var param = {};
		param["grpCodeId"] = kedbCategoryCodeId;
		
		var setTreeProp = {
			context: getConstValue('CONTEXT'),						
			id: that['id']+'_tree',	
			url: getConstValue('CONTEXT') + '/itg/base/searchMutilCodeDataListForTree.do',
			title: getConstText({ isArgs: true, m_key: 'res.label.workflow.kedb.00001' }),
			dynamicFlag : false,
			searchable : true,
			searchableTextWidth : 100,
			expandAllBtn : true,
			collapseAllBtn : true,
			params: param,
			rnode_text : getConstText({ isArgs: true, m_key: 'res.label.workflow.kedb.00001' }),
			expandLevel: 0
		}
		var causeTypeTree = createTreeComponent(setTreeProp);

		var setGridProp = {	
				context: getConstValue('CONTEXT'),						// Context
				id: that['id']+'_grid',								// Grid id
				title: "KEDB 목록",		// Grid Title
				height: getConstValue('SHORT_GRID_HEIGHT'),
				resource_prefix: 'grid.workflow.kedb',
				url: getConstValue('CONTEXT') +'/itg/workflow/kedb/searchKedbList.do',
				params: {},
				pagingBar : true,
				pageSize : 10
		};
		
		var kedbGrid = createGridComp(setGridProp);
		
		var kedbSearchBtn   = createBtnComp({visible: true, label:getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:that['id']+'SearchBtn', ui:'correct'});
		attachBtnEvent(kedbSearchBtn, actionLink,  'searchAttr');
		
		var setSearchFormProp = {
				id: that['id']+'SearchForm',
				formBtns: [kedbSearchBtn],			
				columnSize: 2,
				tableProps : [
					            {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.workflow.kedb.00004' }), name:'kedb_titl', widthFactor : 0.95})}
					          , {colspan:1, item : createTextFieldComp({label:'KEDB ID', name:'kedb_id', widthFactor : 0.95})}
				             ]
		}

		var kedb_search_form = createSeachFormComp(setSearchFormProp);

		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: '100%',
			width: 200,
			autoScroll: false,
			split: true,
			maxWidth: 200,
			minWidth: 200,
			items: [causeTypeTree]
		});


		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			//flex:1 ,
			items: [kedb_search_form,kedbGrid]
		});

		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			layout:{
				type: 'border'
			},
			items: [left_panel,right_panel],
			listeners: {
				afterrender: function(){
					left_panel.splitter.setWidth(1);
					this.doLayout();
				}
			}
		});
			
		var tagetKedbIdField = that['tagetKedbIdField'];
		
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui : 'correct',
			scale: "medium",
			handler: function() {
				var selectedRecords = kedbGrid.view.selModel.getSelection();
				if(selectedRecords.length <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.workflow.kedb.00001' }));
				}else{
					
					if(that['afterFunction'] != null) {
						that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
					} else {
						var kedb_id = selectedRecords[0].raw.KEDB_ID;
						tagetKedbIdField.setValue(kedb_id);
						kedbSelectPop.close();
					}
				}	
			}
		});
		
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale: "medium",
			handler: function() {
				popUp_comp[that['id']] = null
				kedbSelectPop.close();	
			}
		});
		
		/*** Private Function Area Start *********************/		
		
		function actionLink(command) {

			switch(command) {		
				case 'searchAttr' :
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
					
					var selNode =Ext.getCmp(that['id']+'_tree').view.selModel.getSelection();
					if(selNode.length > 0){				
						paramMap['code_id'] = selNode[0].raw.node_id;
					}
					
					kedbGrid.searchList(paramMap);
				break;
			
			}
		}
		
		function dbSelectEvent(){
			var selectedRecords = kedbGrid.view.selModel.getSelection();
			if(selectedRecords.length <1){
				showMessage(getConstText({ isArgs: true, m_key: 'msg.workflow.kedb.00001' }));
			}else{
				
				if(that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
				} else {
					var kedb_id = selectedRecords[0].raw.KEDB_ID;
					tagetKedbIdField.setValue(kedb_id);
					kedbSelectPop.close();
				}
			}
		}
		
		function causeTypeTreeClick(tree, record, index, eOpts) {
			var code_id = record.raw.node_id;
			
			if(code_id == kedbCategoryCodeId){
				var getSelect = getGrid(that['id']+'_grid').getStore();
				getSelect.proxy.jsonData={};
				getSelect.load();	
			}else{
				var getSelect = getGrid(that['id']+'_grid').getStore();
				getSelect.proxy.jsonData={code_id: code_id};
				getSelect.load();
			}

		}		
		
		/*********************** Private Function Area End ***/
		
		attachCustomEvent('select', causeTypeTree, causeTypeTreeClick);
		attachCustomEvent('beforeitemdblclick', kedbGrid, dbSelectEvent);		
		
		var windowProp = {
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'],
			width: that['width'],
			layout:{
				type: 'border'
			},
			buttons: [acceptBtn, closeBtn],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
					
				},
				hide:function(p) {
				}
			}
		};
		kedbSelectPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = kedbSelectPop;
		
	} else {
		kedbSelectPop = popUp_comp[that['id']];
	}
	
	return kedbSelectPop;
}

/**
 * @function
 * @summary 장애레벨 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createIncidentGradeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var incidentGradeSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {
		var maxLevel = that['maxLevel'];
		var renderIncidentGradeMetrix = function(view, eOpts) {
			var gridTrs = view.el.query('tr.x-grid-row');
			if (gridTrs != null && gridTrs.length > 0) {
				for ( var i = 0; i < gridTrs.length; i++) {
					var rowTr = Ext.get(gridTrs[i]);
					var tds = rowTr.query('td');
					for ( var j = 0; j < tds.length; j++) {
						if (j == 0) {
							Ext.get(tds[j]).set({
								'class' : 'incPopHeader'
							});
						} else {

							var rawData = view.store.data.items[i].raw;
							var index = "";
							if(j < 10) {
								index = "0" + j;
							}
							var incLevelTime = rawData["INC_LEVEL_TIME_" + index];
							var incidentGradeCd = rawData["INCDNT_GRAD_CD_" + index];
							var incidentGradeCdNm = rawData["INCDNT_GRAD_CD_NM_" + index];
							var incidentAffcteCd = rawData["INCDNT_AFFCTE_CD"];
							var incidentEmrgncyCd = rawData["INCDNT_EMRGNCY_CD_" + index];
							// j + 1 은 긴급도
							// i + 1 은 영향도
							if (maxLevel != null && i + 1 == parseInt(maxLevel)) {
								Ext.get(tds[j]).set({
									'class' : 'incSelecedPopTd'
								});
							} else {
								Ext.get(tds[j]).set({
									'class' : 'incPopTd'
								});
							}

							Ext.get(tds[j]).set(
									{
										'onclick' : "Ext.getCmp('"
												+ that['targetFormId']
												+ "').getFormFieldContainer('"
												+ that['targetName']
												+ "').selectedCell('"
												+ incidentGradeCd + "','"
												+ incLevelTime + "','"
												+ incidentGradeCdNm + "','" // 장애레벨,장애시간,장애레벨명
												+ incidentAffcteCd + "','"
												+ incidentEmrgncyCd + "');" // 영향도, 긴급도
												+ "Ext.getCmp('"
												+ that['popId'] + "').close();"
									});
						}
					}
				}
			}
		}

		var setGridProp = {
			title : "등급선택", // Grid Title
			height : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.nbpm.inclevelmatrix',
			url : '/itg/nbpm/common/searchIncLevelMatrix.do',
			params : {},
			viewRenderer : renderIncidentGradeMetrix,
			pagingBar : false,
			autoLoad : true
		};

		var gridComp = createGridComp(setGridProp);
		// attachCustomEvent('cellclick', gridComp, dbSelectEvent);
		// popUpBtn.on('click', dbSelectEvent);
		
		incidentGradeSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			height : 340,
			width : 500,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : [],
			bodyStyle : 'background-color: white; ',
			items : [ gridComp ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['popId']] = incidentGradeSelectPop;

	} else {
		incidentGradeSelectPop = popUp_comp[that['popId']];
	}

	return incidentGradeSelectPop;
}

/**
 * @function
 * @summary 변경등급 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createChangeGradeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var changeGradeSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {
		var maxLevel = that['maxLevel'];
		var renderChangeGradeMetrix = function(view, eOpts) {
			var gridTrs = view.el.query('tr.x-grid-row');
			if (gridTrs != null && gridTrs.length > 0) {
				for ( var i = 0; i < gridTrs.length; i++) {
					var rowTr = Ext.get(gridTrs[i]);
					var tds = rowTr.query('td');
					for ( var j = 0; j < tds.length; j++) {
						if (j == 0) {
							Ext.get(tds[j]).set({
								'class' : 'incPopHeader'
							});
						} else {

							var rawData = view.store.data.items[i].raw;
							var index = "";
							if(j < 10) {
								index = "0" + j;
							}
							var changeLevelTime = rawData["INC_LEVEL_TIME_" + index];
							var changeGradeCd = rawData["INCDNT_GRAD_CD_" + index];
							var changeGradeCdNm = rawData["INCDNT_GRAD_CD_NM_" + index];
							var changeAffcteCd = rawData["INCDNT_AFFCTE_CD"];
							var changeEmrgncyCd = rawData["INCDNT_EMRGNCY_CD_" + index];
							// j + 1 은 긴급도
							// i + 1 은 영향도
							if (maxLevel != null && i + 1 == parseInt(maxLevel)) {
								Ext.get(tds[j]).set({
									'class' : 'incSelecedPopTd'
								});
							} else {
								Ext.get(tds[j]).set({
									'class' : 'incPopTd'
								});
							}

							Ext.get(tds[j]).set(
									{
										'onclick' : "Ext.getCmp('"
												+ that['targetFormId']
												+ "').getFormFieldContainer('"
												+ that['targetName']
												+ "').selectedCell('"
												+ changeGradeCd + "','"
												+ changeLevelTime + "','"
												+ changeGradeCdNm + "','" // 장애레벨,장애시간,장애레벨명
												+ changeAffcteCd + "','"
												+ changeEmrgncyCd + "');" // 영향도, 긴급도
												+ "Ext.getCmp('"
												+ that['popId'] + "').close();"
									});
						}
					}
				}
			}
		}
		//등급 변경시에 레이아웃 사이즈 조절 할것 setGridProp의 (height/gridHeight) t_comp의 height속성을 동일하게 맞춰준다.
		var setGridProp = {
			title : "등급선택", // Grid Title
			resource_prefix : 'grid.nbpm.inclevelmatrix',
			url : '/itg/nbpm/common/searchIncLevelMatrix.do',
			params : {},
			height: 250,
			gridHeight : 250,
			loadTitleCount : false,
			viewRenderer : renderChangeGradeMetrix,
			pagingBar : false,
			autoLoad : true
		};
		
		var gridComp = createGridComp(setGridProp);
		// attachCustomEvent('cellclick', gridComp, dbSelectEvent);
		// popUpBtn.on('click', dbSelectEvent);
		
		var gridContainer = {
			xtype : 'nkiafieldcontainer',
			width : 500,
			height : 202,
			layout : 'auto',
			items : [gridComp]
		}
		
		changeGradeSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			buttonAlign : 'center',
			buttons : [],
			bodyStyle : 'background-color: white; ',
			items : [ gridContainer ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});
		
		
		popUp_comp[that['popId']] = changeGradeSelectPop;

	} else {
		changeGradeSelectPop = popUp_comp[that['popId']];
	}

	return changeGradeSelectPop;
}

/**
 * @function
 * @summary 프로세스 삭제 사유 입력 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcessDelReasonPop(setPopUpProp) {

	var that = {};
	var delReasonPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['id']] == null) {
		var delReasonTextArea = createTextAreaFieldComp({
			"name" : "delete_reason",
			"label" : "삭제사유",
			"notNull" : true,
			//"width" : 478,
			widthFactor : 2,
			"height" : 230
		});
		var paramMap = that["param"];
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			handler : function() {
				paramMap["delete_reason"] = delReasonTextArea.getValue();
				if(paramMap["delete_reason"] == null || paramMap["delete_reason"] == "") {
					alert("삭제사유를 입력해주시기 바랍니다.");
					return false;
				}
				var closeFlag = false;
				var runFlag = false;
				var isExist = false;
				jq.ajax({
					type : "POST",
					url : getConstValue('CONTEXT') + "/itg/nbpm/edit/deleteSrData.do",
					contentType : "application/json",
					dataType : "json",
					async: false,
					data : getArrayToJson(paramMap),
					success : function(data) {
						if (data.success) {
							alert(data.resultMsg);
							closeFlag = true;
						} else {
							alert(data.resultMsg);
						}
					}
				});
				/*
				jq.ajax({
					"type" : "POST",
					"url" : getConstValue('CONTEXT')
							+ "/itg/nbpm/edit/checkDeleteSrData.do",
					"contentType" : "application/json",
					"dataType" : "json",
					"async": false,
					"data" : getArrayToJson(paramMap),
					"success" : function(data) {
						if (data.success) {
							// 묶여있는 프로세스가 있는지 판단함
							isExist = data.resultMap.isExist;
						} else {
							alert(data.resultMsg);
							setViewPortMaskFalse();
						}
					}
				});
				if (isExist) {
					if (confirm(data.resultMsg)) {
						runFlag = true;
					} else {
						delReasonPop.close();
					}
				} else {
					runFlag = true;
				}
				alert(runFlag);
				if (runFlag) {
					var delParam = { sr_id : paramMap["sr_id"]};
					jq.ajax({
						type : "POST",
						url : getConstValue('CONTEXT') + "/itg/nbpm/edit/deleteSrData.do",
						contentType : "application/json",
						dataType : "json",
						async: false,
						data : getArrayToJson(delParam),
						success : function(data) {
							if (data.success) {
								alert(data.resultMsg);
								closeFlag = true;
							} else {
								alert(data.resultMsg);
							}
						}
					});
				}
				 */
				
				if(closeFlag) {
					delReasonPop.close();
					window.parent.closeTab();
				}
				setViewPortMaskFalse();
				
			}
		});

		delReasonPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpTitle'],
			height : 300,
			width : 700,
			autoDestroy : false,
			resizable : false,
			bodyPadding : '5 5 5 5',
			buttonAlign : 'center',
			buttons : [ acceptBtn ],
			bodyStyle : 'background-color: white; ',
			items : [ delReasonTextArea ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null

				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = delReasonPop;

	} else {
		delReasonPop = popUp_comp[that['id']];
	}

	return delReasonPop;
}

/**
 * @function
 * @summary 프로세스 담당자 변경 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createUserOperChangePop(setPopUpProp){
	
	var storeParam = {};
	var that = {};
	var userSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var param = ( that['params'] ) ? that['params'] : {};
		
		var comboProps = {
			//id: that['id']+"_combo",
			labelWidth : getConstValue('POP_LABEL_WIDTH'),
			displayField: "searchNm",
			valueField: "searchCol",
			widthFactor : 0.5,
			padding: getConstValue('WITH_FIELD_PADDING'), 
			label: getConstText({ isArgs: true, m_key: 'res.common.search' }),
			name: "searchCondition",
			storeData : [
			             {searchCol : 'search_user_nm', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00015' }) },
			             {searchCol : 'search_user_id', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00014' }) },
			             {searchCol : 'cust_nm', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00023' }) },
			             {searchCol : 'emp_id', searchNm : getConstText({ isArgs: true, m_key: 'res.label.system.00024' }) }
			            ]
		};
		
		var searchConditionComp = createComboBoxComp(comboProps);
		searchConditionComp.select('search_user_nm');
		
		var searchText = createTextFieldComp({hideLabel: true, name:'user_nm', padding: getConstValue('WITH_FIELD_PADDING'), widthFactor : 0.5, label:getConstText({ isArgs: true, m_key: 'res.label.system.00015' })})
		
		var userSearchBtn = createBtnComp({
		    label: getConstText({ isArgs: true, m_key: 'btn.common.search' })
		  , margin: getConstValue('WITH_BUTTON_MARGIN')
		});
		
		//사용자 조회 폼
		var setSearchUserFormProp = {
			id: that['id']+'searchUserForm',
			title: '사용자 조회',
			target: that['id']+'_grid',
			columnSize: 3,
			enterFunction : clickUserSearchBtn,
			tableProps : [{
							colspan:1, item: searchConditionComp
						},{
							colspan:1, item: searchText
						},{
							colspan:1, item: userSearchBtn
						}]
		}
		var userSearchForm = createSeachFormComp(setSearchUserFormProp);		
		var setGridProp = {	
			id: that['id']+'_grid',							
			gridHeight: 230,
			title: '사용자 목록',
			resource_prefix: 'grid.popup.user',				
			url: getConstValue('CONTEXT') + that['url'],
			params: that['params'],
			pagingBar : true,
			pageSize : 15
		};
		
		var userGrid = createGridComp(setGridProp);
		
		var userInfoFormProp = {
			"id" : "userInfoForm",
			"title" : "변경정보",
			"columnSize" : 1,
			"editOnly" : true,
			"formBtns" : [],
			"tableProps" : [{
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createTextFieldComp({
									"label" : "변경 전 담당자",
									"name" : "worker_nm",
									"value" : '',
									"readOnly" : true
									
								})
					}, {
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createTextFieldComp({
									"label" : "변경후  담당자",
									"name" : "after_worker_nm",
									"value" : "",
									"readOnly" : true,
									"notNull":true
								})
					}, {
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createCodeComboBoxComp({
									"label" : "알림 이메일 발송여부",
									"name" : "email_send",
									"code_grp_id" : "YN",
									"defaultValue" : "N"
								})
					}],
			"hiddenFields" : [{
						"name" : "after_worker_id",
						"value" : ""
					},{
						"name" : "worker_id",
						"value" : ''
					}]
		};
		var userInfoFormPanel = createEditorFormComp(userInfoFormProp);		
		// 패널 병합
		
		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			layout: 'vbox',
			items: [userSearchForm, userGrid, {xtype:'tbspacer', height:7 },userInfoFormPanel]
		});
		
		var tagetUserIdField = that['tagetUserIdField'];
		var tagetUserNameField = that['tagetUserNameField'];
		var tagetEtcField = that['tagetEtcField'];
		
		var acceptBtn = createBtnComp({
			label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct', 
			scale:'medium'
		});
		
		function clickUserSearchBtn() {
			var conditionVal = searchConditionComp.getValue();
        	var textVal = searchText.getValue();
			var userGridStore = userGrid.getStore();
			param[conditionVal] = textVal
			userGridStore.proxy.jsonData = param;
			userGridStore.load();
		}

		function clickGrid(grid, td, cellIndex, record, tr, rowIndex){
			userInfoFormPanel.setHiddenValue('after_worker_id', record.raw.USER_ID);
			userInfoFormPanel.setFieldValue('after_worker_nm', record.raw.USER_NM);
		}
		
		function changeWorker() {
			if(Ext.getCmp("userInfoForm").isValid()){
				var varData = userInfoFormPanel.getInputData();
				varData["sr_id"] = param["SR_ID"];
				varData["id"] = param["ID"];
				varData["auth_id"] = param["AUTH_ID"];
				varData["role_id"] = param["ROLE_ID"];
				jq.ajax({ type:"POST"
						, url: getConstValue('CONTEXT') + "/itg/nbpm/edit/updateWorkerData.do"
						, contentType: "application/json"
						, dataType: "json"
						, data : getArrayToJson(varData)
						, success:function(data){
							if( data.success ){
								showMessage(data.resultMsg);
								if(userSelectPop != null) {
									userSelectPop.close();
									if(that["afterFunction"] != null) {
										that["afterFunction"]();
									}
								}
							}else{
								showMessage(data.resultMsg);
							}
				        }
				});
			}else {
				alert("입력 항목을 확인해주세요.");
				return;
			}
		}		

		attachBtnEvent(acceptBtn, changeWorker);
		attachBtnEvent(userSearchBtn, clickUserSearchBtn);
		attachCustomEvent('cellclick', userGrid, clickGrid);
		userSelectPop = Ext.create('nkia.custom.windowPop',{
			id: that['id']+'_Pop',
			title: '사용자조회',  
			height: 500, 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		userInfoFormPanel.setDataForMap(that['params']);
		
		popUp_comp[that['id']] = userSelectPop;
		
	} else {
		userSelectPop = popUp_comp[that['id']];
	}
	
	return userSelectPop;
}

/**
* @function
* @summary 프로세스 담당자 변경 팝업
* @param {json} setPopUpProp
* @return {object} popup
*/
function createUserOperGrpChangePop(setPopUpProp){
	
	var storeParam = {};
	var that = {};
	var userSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		var param = ( that['params'] ) ? that['params'] : {};
		
		var chargerGrpTreeProp = {
				id : chargerGrpTree,
				url : getConstValue('CONTEXT') + that['url'] ,
				title : "담당자 그룹",
				dynamicFlag : false,
				searchable : true,
				expandAllBtn : true,
				collapseAllBtn : true,
				params : param,
				height : 350,
				rnode_text : '#springMessage("res.00003")',
				expandLevel : 2
			}
			
		var chargerGrpTree = createTreeComponent(chargerGrpTreeProp);
		attachCustomEvent('itemdblclick', chargerGrpTree, treeNodeClick);
		
		var userInfoFormProp = {
			"id" : "userInfoForm",
			"title" : "변경정보",
			"columnSize" : 1,
			"editOnly" : true,
			"formBtns" : [],
			"tableProps" : [{
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createTextFieldComp({
									"label" : "변경 전 담당자(그룹)",
									"name" : "worker_nm",
									"value" : '',
									"readOnly" : true
								})
					}, {
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createTextFieldComp({
									"label" : "변경후  담당자(그룹)",
									"name" : "after_worker_nm",
									"value" : "",
									"readOnly" : true,
									"notNull":true
								})
					}, {
						"colspan" : "1",
						"rowspan" : "1",
						"item" : createCodeComboBoxComp({
									"label" : "알림 이메일 발송여부",
									"name" : "email_send",
									"code_grp_id" : "YN",
									"defaultValue" : "N"
								})
					}],
			"hiddenFields" : [{
						"name" : "after_worker_id",
						"value" : ""
					},{
						"name" : "worker_id",
						"value" : ''
					}]
		};
		var userInfoFormPanel = createEditorFormComp(userInfoFormProp);		
		// 패널 병합
		
		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			layout: 'vbox',
			items: [chargerGrpTree, {xtype:'tbspacer', height:7 },userInfoFormPanel]
		});
		
		var tagetUserIdField = that['tagetUserIdField'];
		var tagetUserNameField = that['tagetUserNameField'];
		var tagetEtcField = that['tagetEtcField'];
		
		var acceptBtn = createBtnComp({
			label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct', 
			scale:'medium'
		});
		
		function treeNodeClick(tree, record, index, eOpts) {
			
			if(record.raw.leaf == true){
				var rawText = record.raw.text;
				var afterWorkerNm = rawText.split('[');
				
				userInfoFormPanel.setHiddenValue('after_worker_id', record.raw.node_id);
				userInfoFormPanel.setFieldValue('after_worker_nm', afterWorkerNm[0]);
				
			}else{
				alert(
				getConstText({
					m_key : "msg.system.result.00032"
					, isArgs : true
				}));
				return;
			}
			
		}
		
		function changeWorker() {
			if(Ext.getCmp("userInfoForm").isValid()){var varData = userInfoFormPanel.getInputData();
			varData["sr_id"] = param["SR_ID"];
			varData["id"] = param["ID"];
			varData["auth_id"] = param["AUTH_ID"];
			varData["role_id"] = param["ROLE_ID"];
			varData["grp_slct_yn"] = "Y";
			
			jq.ajax({ type:"POST"
					, url: getConstValue('CONTEXT') + "/itg/nbpm/edit/updateOperData.do"
					, contentType: "application/json"
					, dataType: "json"
					, data : getArrayToJson(varData)
					, success:function(data){
						if( data.success ){
							showMessage(data.resultMsg);
							if(userSelectPop != null) {
								userSelectPop.close();
								if(that["afterFunction"] != null) {
									that["afterFunction"]();
								}
							}
						}else{
							showMessage(data.resultMsg);
						}
			        }
			});
			}else {
				alert("입력 항목을 확인해주세요.");
				return;
			}
		
		}		

		attachBtnEvent(acceptBtn, changeWorker);
		userSelectPop = Ext.create('nkia.custom.windowPop',{
			id: that['id']+'_Pop',
			title: '사용자조회',  
			height: 600, 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		userInfoFormPanel.setDataForMap(that['params']);
		
		popUp_comp[that['id']] = userSelectPop;
		
	} else {
		userSelectPop = popUp_comp[that['id']];
	}
	
	return userSelectPop;
}

/**
 * @function
 * @summary VM 목록 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createVmSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var vmSelectPop = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	var nwType = "";
	if(that['clo_nw_type'] != null) {
		nwType = that['clo_nw_type'];
	}
	
	var nwTypeNm = "";
	if(that['clo_nw_type_nm'] != null) {
		nwTypeNm = that['clo_nw_type_nm'];
	}
	
	if ( popUp_comp[that['id']] == null ){
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : '가상서버 요청내역'
			}), // Grid Title
			gridHeight : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.itam.vmList', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params'],  
			selfScroll: true
		};

		var vmAssetGrid = createGridComp(setGridProp);

		var vmSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'vmSearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(vmSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ vmSearchBtn ],
			columnSize : 2,
			tableProps : [
				{ colspan : 1, item : createTextFieldComp({label : '가상서버 ID', name : 'search_vm_id', width : 250 }) },
				{ colspan : 1, item : createTextFieldComp({label : '가상서버명', name : 'search_vm_nm', width : 250 }) }
			],
			enterFunction: function(){
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();
					paramMap['search_nw_type'] = nwType;
				vmAssetGrid.searchList(paramMap);
			}
		}

		var vm_search_form = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ vm_search_form, vmAssetGrid ]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				vmSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				var selModel = vmAssetGrid.view.selModel;
				if( selModel.hasSelection() ){
					var selectedRecords = selModel.getSelection();				// 선택된 Tree_Node
					var selData = selectedRecords[0].data;
					var targetForm = Ext.getCmp(that['targetFormId']);
					
					if( targetForm.getFormField("cha_storage_size") ){
						targetForm.getFormField("cha_storage_size").reset();
					}
					
					targetForm.setHiddenValue("clo_vm_id", selData["VM_ID"]);
					targetForm.setFieldValue("clo_os_code", selData["OS_CODE"]);
					targetForm.setFieldValue("clo_vm_nm", selData["VM_NM"]);
					targetForm.setFieldValue("clo_host_nm", selData["HOST_NM"]);
					targetForm.setFieldValue("clo_os_nm", selData["OS_NM"]);
					targetForm.setFieldValue("clo_cpu", selData["CPU"]);
					targetForm.setFieldValue("clo_memory", selData["MEMORY"]);
					targetForm.setFieldValue("clo_add_storage", selData["STORAGE"]);
					targetForm.setFieldValue("clo_storage", selData["STORAGE"]);
					targetForm.setFieldValue("clo_storage_prev", selData["STORAGE"]);
					targetForm.setFieldValue("clo_network_code", nwType);
					targetForm.setFieldValue("clo_network_nm", nwTypeNm);
					
					vmSelectPop.close();
				}else{
					showMessage("선택된 내역이 없습니다.");
					return;
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {
			switch (command) {
				case 'searchAttr':
					var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
					var paramMap = searchForm.getInputData();
						paramMap['search_nw_type'] = nwType;
					vmAssetGrid.searchList(paramMap);
				break;
			}
		}

		function dbSelectEvent() {
			var selModel = vmAssetGrid.view.selModel;
			if( selModel.hasSelection() ){
				var selectedRecords = selModel.getSelection();				// 선택된 Tree_Node
				var selData = selectedRecords[0].data;
				var targetForm = Ext.getCmp(that['targetFormId']);
				
				if( targetForm.getFormField("cha_storage_size") ){
					targetForm.getFormField("cha_storage_size").reset();
				}
				
				targetForm.setHiddenValue("clo_vm_id", selData["VM_ID"]);
				targetForm.setFieldValue("clo_os_code", selData["OS_CODE"]);
				targetForm.setFieldValue("clo_vm_nm", selData["VM_NM"]);
				targetForm.setFieldValue("clo_host_nm", selData["HOST_NM"]);
				targetForm.setFieldValue("clo_os_nm", selData["OS_NM"]);
				targetForm.setFieldValue("clo_cpu", selData["CPU"]);
				targetForm.setFieldValue("clo_memory", selData["MEMORY"]);
				targetForm.setFieldValue("clo_add_storage", selData["STORAGE"]);
				targetForm.setFieldValue("clo_storage", selData["STORAGE"]);
				targetForm.setFieldValue("clo_storage_prev", selData["STORAGE"]);
				
				vmSelectPop.close();							
			}else{
				showMessage("선택된 내역이 없습니다.");
				return;
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', vmAssetGrid, dbSelectEvent);

		vmSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 744,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = vmSelectPop;

	} else {
		vmSelectPop = popUp_comp[that['id']];
	}

	return vmSelectPop;
}

/**
 * @function
 * @summary 작업요청 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcJobSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var procJobSelectPop = null;
	
	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}
	
	// 최근일로부터 6달간의 데이터만 출력되도록 - 2014.09.30 정정윤
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-7, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})
	
	that['params']['search_req_dt_startDate'] = startDateObj['day'];
	that['params']['search_req_dt_endDate'] = endDateObj['day'];
	
	if ( popUp_comp[that['id']] == null ){
		var setGridProp = {
			context : getConstValue('CONTEXT'), // Context
			id : that['id'] + '_grid', // Grid id
			title : getConstText({
				isArgs : true,
				m_key : '가상서버 요청내역'
			}), // Grid Title
			gridHeight : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : 'grid.process.jobSub2', // Resource Prefix
			url : getConstValue('CONTEXT') + that['url'], // Data Url
			params : that['params']
		};

		var procJobGrid = createGridComp(setGridProp);

		var procJobSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'procJobSearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(procJobSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ procJobSearchBtn ],
			columnSize : 2,
			tableProps : [
				{ colspan : 1, item : createTextFieldComp({label : '장비명', labelWidth:50, name : 'search_conf_nm', width : 200 }) },
				{ colspan : 1, item : createSearchDateFieldComp({label:'요청일', labelWidth:50, name:'search_req_dt', start_dateValue : startDateObj, end_dateValue: endDateObj}) }
			],
			enterFunction: function(){
				var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
				var paramMap = searchForm.getInputData();
				procJobGrid.searchList(paramMap);
			}
		}

		var procJobSearchForm = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ procJobSearchForm, procJobGrid ]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				procJobSelectPop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				var selModel = procJobGrid.view.selModel;
				if( selModel.hasSelection() ){
					var selectedRecords = selModel.getSelection();	// 선택된 Row
					var selData = selectedRecords[0].data;
					var targetForm = Ext.getCmp(that['targetFormId']);
					
					targetForm.setFieldValue("job_sr_id", selData["SR_ID"]);
					targetForm.setFieldValue("job_type", selData["JOB_TYPE"]);
					targetForm.setFieldValue("job_cycle", selData["JOB_CYCLE"]);
					targetForm.setFieldValue("job_exec_file_name", selData["JOB_EXEC_FILE_NAME"]);
					targetForm.setFieldValue("job_exec_file_path", selData["JOB_EXEC_FILE_PATH"]);
					targetForm.getFormField("job_st_dt").setValue(selData["JOB_ST_DT"]);
					//targetForm.getFormField("job_end_dt").setValue(selData["JOB_END_DT"]);
					//targetForm.setFieldValue("job_end_dt", selData["JOB_END_DT"]);
					targetForm.setFieldValue("job_work_terms", selData["JOB_WORK_TERMS"]);
					targetForm.setFieldValue("job_monitoring_yn", selData["JOB_MONITORING_YN"]);
					procJobSelectPop.close();
				}else{
					showMessage("선택된 내역이 없습니다.");
					return;
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {
			switch (command) {
				case 'searchAttr':
					var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
					var paramMap = searchForm.getInputData();
					paramMap['selectQuerykey'] = that['selectQuerykey'];
					procJobGrid.searchList(paramMap);
				break;
			}
		}

		function dbSelectEvent() {
			var selModel = procJobGrid.view.selModel;
			if( selModel.hasSelection() ){
				var selectedRecords = selModel.getSelection();	// 선택된 Row
				var selData = selectedRecords[0].data;
				var targetForm = Ext.getCmp(that['targetFormId']);
				
				targetForm.setFieldValue("job_sr_id", selData["SR_ID"]);
				targetForm.setFieldValue("job_type", selData["JOB_TYPE"]);
				targetForm.setFieldValue("job_cycle", selData["JOB_CYCLE"]);
				targetForm.setFieldValue("job_exec_file_name", selData["JOB_EXEC_FILE_NAME"]);
				targetForm.setFieldValue("job_exec_file_path", selData["JOB_EXEC_FILE_PATH"]);
				//targetForm.getFormField("job_st_dt").setValue(selData["JOB_ST_DT"]);
				//targetForm.getFormField("job_end_dt").setValue(selData["JOB_END_DT"]);
				targetForm.setFieldValue("job_end_dt", selData["JOB_END_DT"]);
				targetForm.setFieldValue("job_work_terms", selData["JOB_WORK_TERMS"]);
				targetForm.setFieldValue("job_monitoring_yn", selData["JOB_MONITORING_YN"]);
				procJobSelectPop.close();	
			}else{
				showMessage("선택된 내역이 없습니다.");
				return;
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', procJobGrid, dbSelectEvent);

		procJobSelectPop = Ext.create('nkia.custom.windowPop', {
			id : that['id'],
			title : that['popUpText'],
			height : that['height'],
			width : 570,
			autoDestroy : false,
			resizable : false,
			bodyPadding : that['bodyPadding'],
			buttonAlign : that['buttonAlign'],
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : that['bodyStyle'],
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = procJobSelectPop;

	} else {
		procJobSelectPop = popUp_comp[that['id']];
	}

	return procJobSelectPop;
}

/**
 * @function
 * @summary 프로세스 참조 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createProcReferencePop(setPopupProp) {
	var that = {};
	for ( var prop in setPopupProp) {
		that[prop] = setPopupProp[prop];
	}
	
	// 최근일로부터 6달간의 데이터만 출력되도록 - 2014.09.30 정정윤
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:-7, day:0, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0})
	
	var params = {};
	
	params['selectQuerykey'] = that['selectQuerykey'];
	params['search_req_dt_startDate'] = startDateObj['day'];
	params['search_req_dt_endDate'] = endDateObj['day'];
	
	var procReferencePop = null;
	if ( popUp_comp[that['id']] == null ){
		var setGridProp = {
			id : that['id'] + '_grid', // Grid id
			title : that['popupTitle'],
			gridHeight : getConstValue('SHORT_GRID_HEIGHT'),
			resource_prefix : that['resource_prefix'],
			url : getConstValue('CONTEXT') + "/itg/nbpm/searchProcessReferenceList.do", // Data Url
			params : params
		};

		var procReferenceGrid = createGridComp(setGridProp);

		var procReferenceSearchBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.search'
			}),
			id : that['id'] + 'procJobSearchBtn',
			ui : 'correct'
		});
		attachBtnEvent(procReferenceSearchBtn, actionLink, 'searchAttr');

		var setSearchFormProp = {
			id : that['id'] + 'SearchForm',
			formBtns : [ procReferenceSearchBtn ],
			columnSize : 2,
			tableProps : [
				{ colspan : 1, item : createTextFieldComp({label : '장비명', labelWidth:50, name : 'search_conf_nm', width : 200 }) },
				{ colspan : 1, item : createSearchDateFieldComp({label:'요청일', labelWidth:50, name:'search_req_dt', start_dateValue : startDateObj, end_dateValue: endDateObj}) }
			],
			enterFunction: function(){
				actionLink('searchAttr');
			}
		}

		var procReferenceSearchForm = createSeachFormComp(setSearchFormProp);

		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			items : [ procReferenceSearchForm, procReferenceGrid ]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				procReferencePop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				var selModel = procReferenceGrid.view.selModel;
				if( selModel.hasSelection() ){
					var selectedRecords = selModel.getSelection();	// 선택된 Row
					var selData = selectedRecords[0].data;
					// Data Set
					setProcReferenceData(selData);
					procReferencePop.close();
				}else{
					showMessage("선택된 내역이 없습니다.");
					return;
				}
			}
		});

		/** * Private Function Area Start ******************** */
		function actionLink(command) {
			switch (command) {
				case 'searchAttr':
					var searchForm = Ext.getCmp(that['id'] + 'SearchForm');
					var paramMap = searchForm.getInputData();
					paramMap['selectQuerykey'] = that['selectQuerykey'];
					procReferenceGrid.searchList(paramMap);
				break;
			}
		}

		function dbSelectEvent() {
			var selModel = procReferenceGrid.view.selModel;
			if( selModel.hasSelection() ){
				var selectedRecords = selModel.getSelection();	// 선택된 Row
				var selData = selectedRecords[0].data;
				// Data Set
				setProcReferenceData(selData);
				
				procReferencePop.close();	
			}else{
				showMessage("선택된 내역이 없습니다.");
				return;
			}
		}
		
		function setProcReferenceData(selData){
			var targetForm = Ext.getCmp(that['targetFormId']);
			switch(that['switchCommand']) {
				case "bac_policy_id":
					targetForm.setFieldValue("rec_bac_policy_id", selData["BAC_POLICY_ID"]);
				break;
			}
		}
		/** ********************* Private Function Area End ** */

		attachCustomEvent('beforeitemdblclick', procReferenceGrid, dbSelectEvent);

		procReferencePop = Ext.create('nkia.custom.windowPop', {
			id : that['popupId'],
			title : that['popupTitle'],
			width : that['popupWidth'],
			height : that['popupHeight'],
			autoDestroy : false,
			resizable : false,
			buttons : [ acceptBtn, closeBtn ],
			buttonAlign : 'center',
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : 'background-color: white;',
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = procReferencePop;

	} else {
		procReferencePop = popUp_comp[that['id']];
	}

	return procReferencePop;
}

/**
 * @function
 * @summary Matrix 형태의 선택 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createMatrixPop(setPopupProp) {
	var that = {};
	for ( var prop in setPopupProp) {
		that[prop] = setPopupProp[prop];
	}
	
	var procReferencePop = null;
	if ( popUp_comp[that['id']] == null ){
		var complexPanel = Ext.create('nkia.custom.Panel', {
			border : false,
			html: that["matrixHtml"]
		})

		//닫기버튼 생성
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				procReferencePop.close();
			}
		});
		
		var acceptBtn = new Ext.button.Button({
			text : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}), 
			ui : 'correct', 
			handler : function() {
				setMatrixData();
				procReferencePop.close();	
			}
		});

		/** * Private Function Area Start ******************** */
		function setMatrixData(selData){
			var targetForm = Ext.getCmp(that['targetFormId']);
			switch(that['switchCommand']) {
				case "req_zone":
					if( popUp_comp[that['id']].custId && popUp_comp[that['id']].custNm ){
						targetForm.setFieldValue(that["reqZoneIdField"], popUp_comp[that['id']].custId);
						targetForm.setFieldValue(that["reqZoneNmField"], popUp_comp[that['id']].custNm);	
					}
				break;
			}
		}
		/** ********************* Private Function Area End ** */

		procReferencePop = Ext.create('nkia.custom.windowPop', {
			id : that['popupId'],
			title : that['popupTitle'],
			width : that['popupWidth'],
			autoDestroy : false,
			resizable : false,
			buttons : [ acceptBtn, closeBtn ],
			buttonAlign : 'center',
			buttons : [ acceptBtn, closeBtn ],
			bodyStyle : 'background-color: white;',
			items : [ complexPanel ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['id']] = null
				},
				hide : function(p) {
				}
			}
		});

		popUp_comp[that['id']] = procReferencePop;

	} else {
		procReferencePop = popUp_comp[that['id']];
	}

	return procReferencePop;
}

/**
 * @function
 * @summary Combo & Tree 선택 팝업(공통)
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createComboTreeSelectPop(setPopUpProp) {

	var storeParam = {};
	var that = {};
	var treeSelectPopBody = null;

	for ( var prop in setPopUpProp) {
		that[prop] = setPopUpProp[prop];
	}

	if (popUp_comp[that['popId']] == null) {

		var popUpBtn = createBtnComp({
			visible : true,
			label : getConstText({
				isArgs : true,
				m_key : 'btn.common.apply'
			}),
			id : that['treeId'] + "btn",
			ui : 'correct',
			scale : 'medium'
		});
		
		var comboHandler =  function(combo, selectedRecord, eOpts){
			var serviceId = selectedRecord[0].raw.SERVICE_ID;
			var codeText = selectedRecord[0].raw.CODE_TEXT;
			
			var treeGrid = staticTreeforPop.getStore();
			treeGrid.proxy.jsonData={service_id: serviceId, expandLevel:2};
			treeGrid.load();
		}
		
		var comboLoadHandler =  function(combo, selectedRecord, eOpts){
			var custId = selectedRecord[0].raw.CODE_ID;
			var serviceId = selectedRecord[0].raw.SERVICE_ID;
			var codeText = selectedRecord[0].raw.CODE_TEXT;
			
			var treeGrid = staticTreeforPop.getStore();
			
			if( that['params'] == null ){
				treeGrid.proxy.jsonData={service_id: serviceId, expandLevel:2};
			} else {
				if( that['params']['cust_id'] ){
					var custId = that['params']['cust_id'];
					var comboStore = multiCombo.getStore();
					var len = comboStore.getTotalCount();
					for( var i=0; i < len; i++ ){
						var comboData = comboStore.getAt(i);
						
						if( comboData.raw["CODE_ID"] == custId ){
							multiCombo.setValue( custId, false );
							serviceId = comboData.raw["SERVICE_ID"];
							break;
						}
					}
					
					treeGrid.proxy.jsonData={service_id: serviceId, expandLevel:2};
				}
			}
			
			treeGrid.load();
		}

		var multiCombo = createCodeComboBoxComp({name:'multiCombo', hideLabel: true, width: 130, url:'/itg/itam/amdb/searchCodeDataList.do'});	
		
		attachCustomEvent('select', multiCombo, comboHandler);
		attachCustomEvent('load', multiCombo.getStore(), comboLoadHandler);
		
		var multiTreeComboPanel = Ext.create('nkia.custom.Panel', {
	        id: 'multiTreeComboPanel',
	        border: false,
	        padding: '2 0 1 5',
			layout: 'hbox',
			items: [multiCombo]
		})

		var setTreeProp = {
			context : '${context}',
			id : that['treeId'],
			title : that['title'] ? that['title'] : '',
			url : that['url'],
			extraToolbar : multiTreeComboPanel,
			dynamicFlag : false,
			autoLoad: false,
			searchable : true,
			expandAllBtn : that['expandAllBtn'] ? that['expandAllBtn'] : true,
			collapseAllBtn : that['collapseAllBtn'] ? that['collapseAllBtn'] : true,
			params : that['params'] ? that['params'] : {},
			rnode_text : that['rnode_text'] ? that['rnode_text'] : '코드',
			expandLevel : that['expandLevel'] ? that['expandLevel'] : 1
		}

		var staticTreeforPop = createTreeComponent(setTreeProp);
		
		attachCustomEvent('load', staticTreeforPop, function(){
			staticTreeforPop.expandNode(staticTreeforPop.getRootNode());
		});
		attachCustomEvent('beforeitemdblclick', staticTreeforPop, dbSelectEvent);
		
		// btnHide 값을 true 로 주면 적용 버튼을 숨긴다.
		var btnControll = [popUpBtn];
		if(that['btnHide'] == true){	btnControll = [];	}

		treeSelectPopBody = Ext.create('nkia.custom.windowPop', {
			id : that['popId'],
			title : that['popUpTitle'],
			height : 400,
			width : 300,
			autoDestroy : false,
			resizable : false,
			layout : 'fit',
			// bodyPadding: '5 5 5 5',
			buttonAlign : 'center',
			buttons : btnControll,
			bodyStyle : 'background-color: white; ',
			items : [ staticTreeforPop ],
			listeners : {
				destroy : function(p) {
					popUp_comp[that['popId']] = null;
				},
				hide : function(p) {
				}
			}
		});

		popUpBtn.on('click', dbSelectEvent);

		function dbSelectEvent() {
			// accept btnHide 
			if(that['btnHide'] == true){	return false;	}
			
			var node = Ext.getCmp(that['treeId']).view.selModel.getSelection();
			if (node.length < 1) {
				showMessage(getConstText({
					isArgs : true,
					m_key : 'msg.system.result.00033'
				}));
			} else {	
				
				//@@정중훈20131105 시작
				//Root는 선택이 안되게 만듭니다.
				// true : 선택이 됨
				// false : 선택이 안됨
				if(false == that["rootSelect"]){
					var node_level = node[0].raw.node_level;
					if(1 == node_level){
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00044'
						}));
						return;
					}
				}
				//@@정중훈20131105 종료

				var node_leaf = node[0].raw.leaf;
				if (that['leafSelect'] == true) {
					if (node_leaf == false) {
						showMessage(getConstText({
							isArgs : true,
							m_key : 'msg.system.result.00032'
						}));
						Ext.getCmp(that['treeId']).expandAll(function() {
							Ext.getCmp(me.id).getEl().unmask();
							toolbar.enable();
						});
						return;
					}
				}
				node_id = node[0].raw.node_id;
				node_nm = node[0].raw.text;

				that['targetHiddenField'].setValue(node_id);
				that['targetField'].setValue(node_nm);

				treeSelectPopBody.close();
			}
		}

		popUp_comp[that['popId']] = treeSelectPopBody;

	} else {
		treeSelectPopBody = popUp_comp[that['popId']];
	}

	return treeSelectPopBody;
}

/**
 * @function
 * @summary 자산상세 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function showAssetDetailPop(param){
	//var url = "/itg/itam/opms/opmsDetailInfo.do" + param;
	var url = "/itg/itam/automation/goAssetDetailViewer.do" + param;
	window.open(getConstValue('CONTEXT') +url, "", "width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

/**
 * @function
 * @summary 작업기록 등록 팝업
 * @param {json} setPopUpProp
 * @return {object} popup
 */
function createMaintJobRegistPop(assetList, userId, userNm, parentGrid) {

	var maintJobRegistPop = null;
	var maintJobEditForm = [];
	
	function addMaintJob() {
		
		if(maintJobEditForm.isValid()){
			
			var params = maintJobEditForm.getInputData();
			
			var beginDtValue = params['job_begin_dt_time_day'];
			var endDtValue = params['job_end_dt_time_day'];
			var beginHourValue = params['job_begin_dt_time_hour'];
			var endHourValue = params['job_end_dt_time_hour'];
			var beginMinValue = params['job_begin_dt_time_min'];
			var endMinValue = params['job_end_dt_time_min'];
			
			if(beginDtValue > endDtValue) {
				alert("[작업종료일자]는 [작업시작일자]보다 빠를 수 없습니다.");
				return;
			}
			
			if(beginHourValue > endHourValue) {
				alert("[작업종료시간]은 [작업시작시간]보다 빠를 수 없습니다.");
				return;
			}
			
			if(beginMinValue > endMinValue) {
				if(beginHourValue >= endHourValue) {
					alert("[작업종료시간]은 [작업시작시간]보다 빠를 수 없습니다.");
					return;
				}
			}
			
			if(!confirm("작업기록 정보를 등록하시겠습니까?")) {
				return;
			}
			
			params['asset_list'] = assetList;
			
			jq.ajax({	
				"type" : "POST",
				"url" : "/itg/itam/maint/insertMaintJob.do",
				"contentType" : "application/json",
				"dataType" : "json",
				"async" : false,
				"data" : getArrayToJson(params),
				"success" : function(data) {
					if (data.success) {
						alert("등록되었습니다.");
						if(parentGrid != null) {
							parentGrid.searchList();
						}
						maintJobRegistPop.close();
					} else {
						alert(data.resultMsg);
					}
				}
			});
			
		} else {
			
			alert("입력 항목을 확인해주세요.");
			return;
			
		}
	}
	
	var addBtn = createBtnComp({visible: true, label: '등록', id:'addBtn', ui:'correct', scale:'medium'});
	attachBtnEvent(addBtn, addMaintJob);
	
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:-3, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var thatUserId = "";
	var thatUserNm = "";
	
		thatUserId = userId;//'$!{userVO.user_id}';
		thatUserNm = '[' + userId + '] ' + userNm; //'[' + '$!{userVO.user_id}' + '] ' + '$!{userVO.user_nm}';
		
	//}
	
	var setEditProp = {
		id: 'maintJobEditForm',
		title: '작업기록 등록',
		columnSize: 2,
		editOnly: true,
		border: false,
		formBtns: [],
		tableProps :[
		             {colspan:1, item:createDateFieldComp({label:'등록일자', name:'ins_dt', dateType : 'D', format: 'Y-m-d', dateValue: startDateObj, fixedLabelWidth:80, notNull: true})}
		             , {colspan:1, item:createDateFieldComp({label:'작업시작시간', name:'job_begin_dt_time', dateType:'DHM', format:'Y-m-d', dateValue:startDateObj, minuteIntervalThirty:true, notNull:true})}
		             , {colspan:1, item:createOutUserSelectPopComp({label:'등록자', name:'ins_user_nm', hiddenName:'ins_user_id', fixedLabelWidth:80, notNull:true})}
		             , {colspan:1, item:createDateFieldComp({label:'작업종료시간', name:'job_end_dt_time', dateType:'DHM', format:'Y-m-d', dateValue:endDateObj, minuteIntervalThirty:true, notNull:true})}
		             , {colspan:1, item:createTextFieldComp({label:'작업자', name:'job_user_nm', fixedLabelWidth:80, notNull: true})}
		             , {colspan:1, item:createTextFieldComp({label:'작업자 회사명', name:'job_corp_nm', notNull: true})}
		             , {colspan:2, item:createTextFieldComp({label:'작업제목', name:'job_title', fixedLabelWidth:80, notNull: true, maxLength:150})}
		             , {colspan:2, item:createTextAreaFieldComp({label:'작업내용', name:'job_desc', fixedLabelWidth:80, notNull: true, maxLength:2000})}
					],
		hiddenFields:[]
	}
	
	maintJobEditForm = createInnerEditorFormComp(setEditProp);
	
	var maintJobRegistPopProp	= {
		id						: 'maintJobRegistPop',
		title					: '작업기록 등록',
		width					: 730,
		height					: 320,
		closeBtn 				: true,
		buttons					: [addBtn],
		items					: [maintJobEditForm]
	};
	
	formEditable({id : 'maintJobEditForm' , editorFlag : true , excepArray :[]});
	
	maintJobEditForm.getFormField("ins_user_id").setValue(thatUserId);
	maintJobEditForm.getFormField("ins_user_nm").setValue(thatUserNm);
	
	maintJobRegistPop = createWindowComp(maintJobRegistPopProp);
	
	return maintJobRegistPop;
}

