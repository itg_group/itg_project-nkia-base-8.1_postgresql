/**
 * 
 * @file nkia_file
 * @description 파일 Grid 컴포넌트
 * @author 정영태
 * @copyright NKIA 2013
 */

/**
 * @function
 * @summary 파일다운로드 프레임
 * @param {string} frame_id 다운로드 Frame
 */
function createFileDownloadFrame(frame_id){
	if( Ext.getCmp(frame_id) == null ){
		Ext.create('nkia.custom.FileDownloadFrame', {
			id: frame_id,
			renderTo: Ext.getBody()
		});
	}
}

/**
 * @namespace nkia.custom.FileDownloadFrame
 * @description 파일다운로드 Frame
 */
Ext.define('nkia.custom.FileDownloadFrame', /** @lends nkia.custom.FileDownloadFrame **/{
    extend: 'Ext.Component',
    alias: 'widget.FileDownloadFrame',
    autoEl: {
        tag: 'iframe', 
        cls: 'x-hidden', 
        src: Ext.SSL_SECURE_URL
    },
    isLoadEvent: false,
    stateful: false,
    /**
     * load
     * @description 파일다운로드
     * @param {json} config 파일다운로드 프로퍼티
     * @property {string} url 파일다운로드 URL
	 * @property {string} params 파일다운로드 요청 파마리터
     */
    load: function(config){
        var e = this.getEl();
        e.dom.src = config.url + (config.params ? '?' + Ext.urlEncode(config.params) : '');
     	if( e.dom.attachEvent ){
     		if( !this.isLoadEvent ){
     			var me = this;
     			e.dom.attachEvent("onload", function(){
     				if( e.dom.contentDocument.body.childNodes[0] ){
     					var returnText = e.dom.contentDocument.body.childNodes[0].innerText;
     					if( String(returnText) == "undefined" || returnText == "" ){
     						returnText = e.dom.contentDocument.body.innerText;
     					}
     					
     					if( String(returnText).indexOf('404') != -1 || String(returnText).indexOf('FileNotFound') != -1 ) {
     						alert(getConstText({ isArgs: true, m_key: 'msg.common.file.notfound' }) )
     					}
     					me.isLoadEvent = true;
     				}
     			})
     		}
     	}else{
 			e.dom.onload = function() {
 				if( e.dom.contentDocument.body.childNodes[0] ){
 					var returnText = e.dom.contentDocument.body.childNodes[0].innerText;
 					if( String(returnText) == "undefined" || returnText == "" ){
 						returnText = e.dom.contentDocument.body.innerText;
 					}
 					
 					if( String(returnText).indexOf('404') != -1 || String(returnText).indexOf('FileNotFound') != -1 ) {
 						alert(getConstText({ isArgs: true, m_key: 'msg.common.file.notfound' }) )
 					}
 				}
 			}
 		}
    } 
});

/**
 * @namespace nkia.custom.DefaultFileupload
 * @description 파일업로드 패널
 */
Ext.define("nkia.custom.DefaultFileupload", /** @lends nkia.custom.DefaultFileupload **/ {
    extend: 'Ext.form.Panel',
    alias: 'widget.DefaultFileupload',
    
    data_url: getConstValue('CONTEXT') + "/itg/base/selectAtchFileList.do",
    
    // 허용확장자
    allowFileExt: null,
    
    // 전체파일의 Max Size
    fileAllMaxSize: null,
    
    // 개별파일의 Max Size
    fileUnitMaxSize: null,
    
    listFilesPanel: null,
    
    maxFileCount: null,
    
    listFilesStore: null,
    
    listFilesProxy: null,
    
    listFilesHeight: 250,
    
    tempFileCount: 0,
    
    atch_file_id: null,
    
   	download_frame_id: 'download_frame', 
    
    delete_file_info: [],
    
    readyState: false,
    
    defaultMessage: {FILE_IDX: 'nofile', ORIGNL_FILE_NAME: '<b>' + getConstText({m_key : "NO_FILE"}) + '</b>'},
    
    isSupportedHtml5 : true,
    
    dropMsgMask : null,
      
    /**
	 * onRender
	 * @description 파일업로드 패널 Render
	 */
    onRender: function() { 
        this.callParent(arguments);
        
        var agt = navigator.userAgent;
    	if(agt.indexOf("MSIE 6") != -1 || agt.indexOf("MSIE 7") != -1 || agt.indexOf("MSIE 8") != -1 || agt.indexOf("MSIE 9") != -1 ){
    		this.isSupportedHtml5 = false;
    	}
    	
        // Creates the GridPanel element which contains the list of filenames
        this.createFileList();
        this.add(this.listFilesPanel);
        
        // Create frame by filedownload
        this.download_frame_id = ( this['downloadFrameId'] ) ? this['downloadFrameId'] : this.download_frame_id;
        createFileDownloadFrame(this.download_frame_id);
        
        this.readyState = true;
    },
    /**
	 * createFileList
	 * @description 파일업로드 Grid 생성
	 */
    createFileList : function() {
    	var me = this;
    	var proxyParam = {};
    	var autoLoadFlag = false;
    	if(me.params != null) {
    		proxyParam = me.params;
    		autoLoadFlag = true;
    	}
    	this.listFilesProxy = Ext.create('nkia.custom.JsonProxy', {
			defaultPostHeader : "application/json",
			noCache : false,
			type: 'ajax', 
			url: this.data_url,
			jsonData : proxyParam,
			actionMethods: {
				create: 'POST',
				read: 'POST',
				update: 'POST'
			},
			reader: {
				type: 'json',
				root: 'gridVO.rows'
			}
		});  

    	this.listFilesStore = Ext.create('Ext.data.Store', {
    	    fields: ['ATCH_FILE_ID','FILE_IDX','ORIGNL_FILE_NAME'],
    		proxy: this.listFilesProxy, 
    		autoLoad: autoLoadFlag,
    		listeners: {
				load : function(store, records, options){
					var zipButtonCmp = Ext.getCmp("zipButton");
					if( me.isNoFile() ){
						if( zipButtonCmp ){
							zipButtonCmp.setVisible(false);	
						}
					}else{
						if( zipButtonCmp ){
							zipButtonCmp.setVisible(true);	
						}
					}
					
					if( store.data.length > 0 ){     
						// data exits      
						if(me.editorMode) {
							me.appendDelActionColumn();	// Append Delete Action Column
						}
						
			    		//grid.store.on('row', function(store, records, options){
			    		me.listFilesPanel.getView().on( 'celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
	                  		if( cellIndex == 0 ){
	                  			me.downloadFile(record);
	                  		}
	                  	});
	                  	
	                  	me.atch_file_id = store.data.items[0].raw.ATCH_FILE_ID;
	                  	
	                  	me.showFileCount(me.listFilesPanel, store.data.length);	// 건수 표기
	                  	
			    	}else{
			    		// data not exits
			    		me.listFilesStore.add(me.defaultMessage);
			    	} 
				},
				datachanged : function(store, records, options){
					if( store.data.length == 1 && store.data.items[0].raw.FILE_IDX == "nofile" ){ 
						me.showFileCount(me.listFilesPanel, 0);
	    			}else{
						me.showFileCount(me.listFilesPanel, store.data.length);
	    			}
				}  
    		}
    	});
    	
    	var buttons = null;
    	if( me.saveButton ){	// saveButton true일 경우 버튼 보여주기
    		buttons = [];
    		buttons.push({
    			text: getConstText({m_key : "SAVE"}),
	 			handler: Ext.Function.bind(this.saveAsFile, me)	
    		})
    	}
    	
    	// Creates the GridPanel
    	this.listFilesPanel = Ext.create('nkia.custom.grid', {
    		style: 'border-top-width: 1px; border-top-color: #b8c8d9; border-top-style: solid; border-left-width: 1px; border-left-color: #b8c8d9; border-left-style: solid; border-right-width: 1px; border-right-color: #b8c8d9; border-right-style: solid;',
    		margin: "0 0 0 0",
    		padding: "0 0 0 0",
    		store: this.listFilesStore,
    		dockedItems: [{ 
    			id: this.id+'_toolbar',
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
	                	name: 'temp_file_0',
	                	xtype: 'filefield',	
	                	buttonOnly: true,
	        			hideLabel: true,
	        			buttonConfig: {       
							text: getConstText({m_key : "FILE_ATCH"}),
					        iconCls: 'icon-add'
					    },
					    listeners: {
					    	afterrender: function(fileField){
					    		if( me.isSupportedHtml5 ){
					    			fileField.fileInputEl.set({
						                multiple:'multiple'
						            });
					    		}
					        },
					        change: function(fileField){
					        	// HTML5 지원여부에 따라서 Event가 바뀐다.
					        	if( me.isSupportedHtml5 ){
					        		var files = fileField.fileInputEl.dom.files;
					        		Ext.Array.each(files, function(file) {
					        			me.addFileSupportedHtml5(file);
					        		});
					    		}else{
					    			//Ext.Function.bind(me.addFileNotSupportedHtml5, me);
					    			me.addFileNotSupportedHtml5(fileField);
					    		}
					        }
				        },
	                    scope: this
				},'->',{
                	xtype: 'button',  
                	id: 'zipButton',
                	name: 'zip_btn',
                	iconCls: 'icon-disk',
//                	cls: "x-btn-default-small x-icon-text-left x-btn-icon-text-left x-btn-default-toolbar-small-icon-text-left",
//					overCls: "x-over x-btn-over x-btn-default-toolbar-small-over over",
//					ui: "x-btn-default-small",
                	text: '모든 파일 내려받기',
                	hidden: true,     
                	handler: Ext.Function.bind(this.downloadZipFile, me)
				}]
            }],      
    	    columns: [{
    	    	header: getConstText({m_key : "FILE_NAME"}), 
    	    	dataIndex: 'ORIGNL_FILE_NAME',
    	    	menuDisabled: true,
    	    	flex: 1
    	    }], 
    	    defaults: {
                flex: 1
            }, 
    	  	height: this.listFilesHeight,
    	    buttons: buttons,
    	    viewConfig: {
			    loadMask: false
			},
	        listeners: {
				render : function(grid){
					/*
                  	grid.store.on('load', function(store, records, options){
         				if( store.data.length > 0 ){     
							// data exits      
							if(me.editorMode) {
								me.appendDelActionColumn();	// Append Delete Action Column
							}
							
				    		//grid.store.on('row', function(store, records, options){
				    		me.listFilesPanel.getView().on( 'celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
		                  		if( cellIndex == 0 ){
		                  			me.downloadFile(record);
		                  		}
		                  	});
		                  	
		                  	me.atch_file_id = store.data.items[0].raw.ATCH_FILE_ID;
		                  	
		                  	me.showFileCount(grid, store.data.length);	// 건수 표기
		                  	
				    	}else{
				    		// data not exits
				    		me.listFilesStore.add(me.defaultMessage);
				    	} 
                  	});
                   	*/
                }
	        }
    	});
    	
    	if( me.isSupportedHtml5 ){
    		me.dropMsgMask = new Ext.LoadMask(me.container.dom, {
    			msg: "여기로 파일을 Drag&Drop 해주세요."
    		});
    		
    		var containerEl = me.container;
    		
        	containerEl.dom.addEventListener("dragenter", function (evt) {
        		//console.info("dragenter");
        		//me.showMsg(true);
        		return false;
        	}, false);

        	containerEl.dom.addEventListener("dragover", function (evt) {
        		//console.info("dragover");
        		evt.stopPropagation();
        		evt.preventDefault();
        		return false;
        	}, false);

        	containerEl.dom.addEventListener("dragleave", function(evt) {
        		//console.info("dragleave");
        		evt.stopPropagation();
        		evt.preventDefault();
        		//me.showMsg(false);
        		return false;
        	}, false);

        	containerEl.dom.addEventListener("drop", function (evt) {
        		//console.info("drop");
        		evt.stopPropagation();
        		evt.preventDefault(); 
        		
        		var fileField = me.getForm().findField('temp_file_0');
        		if(fileField.isVisible()){
        			//me.showMsg(false);
        			Ext.Array.each(evt.dataTransfer.files, function(file) { 
        				me.addFileSupportedHtml5(file);
        			});	
        		}
        	}, false);
    	}
    },
    
    showMsg: function(isVisible) {
    	var me = this;
    	var dropMsgMask = me.dropMsgMask 
        if( isVisible && !dropMsgMask.isVisible()  ){
        	me.dropMsgMask.show();
        }else{
        	me.dropMsgMask.hide();
        }
    },
    
    showFileCount: function(grid, fileCount){
    	grid.columns[0].setText(getConstText({m_key : "FILE_NAME"}) + " [" + fileCount + "]");
    },
    
    /**
     * downloadFile
     * @description 파일다운로드
     * @param {data} record 그리드 데이터
     */
    downloadFile: function(record){
    	var downloader = Ext.getCmp(this.download_frame_id);
		downloader.load({
			url: getConstValue('CONTEXT') + '/itg/base/downloadAtchFile.do?atch_file_id=' + record.get('ATCH_FILE_ID') + '&file_idx=' + record.get('FILE_IDX')
		});
    }, 
    /**
     * downloadZipFile
     * @description 파일다운로드 전체 : Zip으로 압축
     * @param {data} record 그리드 데이터
     */
    downloadZipFile: function(record){
    	var downloader = Ext.getCmp(this.download_frame_id)
		downloader.load({
			url: getConstValue('CONTEXT') + '/itg/base/downloadZipFile.do?atch_file_id='+this.atch_file_id+'&zip_file_name=AtchFile'
		});
    },
    
    /**
     * @private Event handler fired when the user selects a file.
     */
    addFileNotSupportedHtml5: function(fileButton) {
    	var me = this;
    	var fileEl = fileButton.inputEl.dom;
    	var fileName = fileEl.value;
    	
    	if( fileName != "" ){
    		var fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length);
    	
	    	var i = 0;
	    	
			// 파일이 최초등록인지 판단
			if( this.isNoFile() ){
				// 최초등록일 경우 Delete Column 생성
				this.initFileAppend();
			}else{
				// 최대 첨부파일 개수
				if( this.maxFileCount != null && this.maxFileCount != "" ){
					var atchFileCount = this.fileCount();
					if( atchFileCount >= this.maxFileCount ){
						alert(getConstText({m_key : "msg.common.max.file.count", isArgs : true, args : String(this.maxFileCount) }))
						fileButton.reset();
						return;
					} 
				}
			}
			
			// 파일첨부 허용 확장자 
			if( this.allowFileExt != null && this.allowFileExt != ""  ){
				var allowFileExtSplit = this.allowFileExt.split(",");
				var isAllow = false;
				for( var i = 0; i < allowFileExtSplit.length; i++ ){
					if( fileExt == allowFileExtSplit[i] ){
						isAllow = true;
						break;
					}
				}
				if( !isAllow ){
					alert(getConstText({m_key : "msg.common.allow.file.ext", isArgs : true, args : this.allowFileExt }))
					fileButton.reset();
					return;
				}
			}
			
			// 첨부파일 Button 생성
	        var fileCount = this.tempFileCount;
	    	var nextFileCount = ++this.tempFileCount;
	        var createFile = Ext.create('Ext.form.field.File', { 
	        	name: 'temp_file_'+nextFileCount, 
	        	buttonOnly: true,
				hideLabel: true,
				buttonConfig: { 
					text: getConstText({m_key : "FILE_ATCH"}),
			        iconCls: 'icon-add'
			    },
			    listeners: {
			        change: function(fileField){
			    		me.addFileNotSupportedHtml5(fileField);
			        }
		        },
	            scope: this
	        });
	        
	        var panel = this.listFilesPanel;
	        var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
	        for( var i = 0; i < panel_toolbar.items.length; i++ ){ 
	        	var toolbar_items = panel_toolbar.items.items[i];
	        	if( typeof toolbar_items.name != "undefined" && toolbar_items.name != "zip_btn"){
					toolbar_items.hide();        		
	        	}
	        }
	        panel_toolbar.insert(0, createFile);
			
	        // File List Grid에 파일명 추가 / 파일 객체와 파일리스트 동기화
	        this.listFilesStore.add({FILE_IDX: 'temp_file_'+fileCount, ORIGNL_FILE_NAME: fileName});
    	}
    },
    
    addFileSupportedHtml5: function(file) {
    	var fileName = file.name;
        	
    	if( fileName != "" ){
    		var fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length);
    	
	    	var i = 0;
	    	
			// 파일이 최초등록인지 판단
			if( this.isNoFile() ){
				// 최초등록일 경우 Delete Column 생성
				this.initFileAppend();
			}else{
				// 최대 첨부파일 개수
				if( this.maxFileCount != null && this.maxFileCount != "" ){
					var atchFileCount = this.fileCount();
					if( atchFileCount >= this.maxFileCount ){
						alert(getConstText({m_key : "msg.common.max.file.count", isArgs : true, args : String(this.maxFileCount) }))
						return;
					} 
				}
			}
			
			var fileCount = this.fileCount();
			// 파일첨부 허용 확장자 
			if( this.allowFileExt != null && this.allowFileExt != ""  ){
				var allowFileExtSplit = this.allowFileExt.split(",");
				var isAllow = false;
				for( var i = 0; i < allowFileExtSplit.length; i++ ){
					if( fileExt == allowFileExtSplit[i] ){
						isAllow = true;
						break;
					}
				}
				if( !isAllow ){
					alert(getConstText({m_key : "msg.common.allow.file.ext", isArgs : true, args : this.allowFileExt }))
					return;
				}
			}
	        // File List Grid에 파일명 추가 / 파일 객체와 파일리스트 동기화
	        this.listFilesStore.add({FILE_IDX: 'temp_file_'+fileCount, ORIGNL_FILE_NAME: fileName, file: file});
    	}
    },
    
    /**
     * onFileChange
     * @description 파일첨부 이벤트
     * @param {obejct} fileButton 파일버튼객체
     */
    onFileChange: function(fileButton) {
    	
    	var fileEl = fileButton.inputEl.dom;
    	var fileName = fileEl.value;
    	
    	if( fileName != "" ){
    		var fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length);
    	
	    	var i = 0;
	    	
			// 파일이 최초등록인지 판단
			if( this.isNoFile() ){
				// 최초등록일 경우 Delete Column 생성
				this.initFileAppend();
			}else{
				// 최대 첨부파일 개수
				if( this.maxFileCount != null && this.maxFileCount != "" ){
					var atchFileCount = this.fileCount();
					if( atchFileCount >= this.maxFileCount ){
						alert(getConstText({m_key : "msg.common.max.file.count", isArgs : true, args : String(this.maxFileCount) }))
						fileButton.reset();
						return;
					} 
				}
			}

			// 기등록된 파일인지 확인하는 로직 추가(2014.09.22 고은규)
			for(var i = 0; i < this.listFilesStore.data.items.length; i++){				
				var fileInfo = this.listFilesStore.data.items[i].raw.ORIGNL_FILE_NAME;

				var selFileName = fileInfo.substring(fileInfo.lastIndexOf("\\")+1, fileInfo.length); // 선택된 파일
				var inFileName = fileName.substring(fileName.lastIndexOf("\\")+1, fileName.length);  // 기등록된 파일
				
				if(inFileName == selFileName){
					alert(getConstText({m_key : "msg.common.file.comparison", isArgs : true}));
					fileButton.reset();
					return;
				}
			}
			
			// 파일첨부 허용 확장자 
			if( this.allowFileExt != null && this.allowFileExt != ""  ){
				var allowFileExtSplit = this.allowFileExt.split(",");
				var isAllow = false;
				for( var i = 0; i < allowFileExtSplit.length; i++ ){
					if( fileExt.toUpperCase() == allowFileExtSplit[i].toUpperCase() ){
						isAllow = true;
						break;
					}
				}
				if( !isAllow ){
					alert(getConstText({m_key : "msg.common.allow.file.ext", isArgs : true, args : this.allowFileExt }))
					fileButton.reset();
					return;
				}
			}
			
			// 첨부파일 Button 생성
			var me = this;
	        var fileCount = this.tempFileCount;
	    	var nextFileCount = ++this.tempFileCount;
	        var createFile = Ext.create('Ext.form.field.File', { 
	        	name: 'temp_file_'+nextFileCount, 
	        	buttonOnly: true,
				hideLabel: true,
				buttonConfig: { 
					text: getConstText({m_key : "FILE_ATCH"}),
			        iconCls: 'icon-add'
			    },
			    listeners: {
		            'change': Ext.Function.bind(this.onFileChange, this)
		        },
	            scope: this
	        });
	        
	        var panel = this.listFilesPanel;
	        var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
	        for( var i = 0; i < panel_toolbar.items.length; i++ ){ 
	        	var toolbar_items = panel_toolbar.items.items[i];
	        	if( typeof toolbar_items.name != "undefined" && toolbar_items.name != "zip_btn"){
					toolbar_items.hide();        		
	        	}
	        }
	        panel_toolbar.insert(0, createFile);
			
	        // File List Grid에 파일명 추가 / 파일 객체와 파일리스트 동기화
	        this.listFilesStore.add({FILE_IDX: 'temp_file_'+fileCount, ORIGNL_FILE_NAME: fileName});
    	}
        
        
    },
    
    /**
     * isNoFile
     * @description 첨부파일이 있는지 판단.
     * @return {boolean} isNoFile 첨부파일유무
     */
    isNoFile: function(){
    	var isNoFile = false;
    	var fileCount = this.fileCount();
    	if( fileCount == 0 ){
    		isNoFile = true;    
    	}else if( fileCount == 1 ){
    		var store = this.listFilesStore;
	    	if( store.data.items[0].raw.FILE_IDX == "nofile" ){ 
				isNoFile = true; 	
	    	}   
    	}
    	return isNoFile;
    },
    
    /**
     * isFileAttach
     * @description 신규 첨부파일이 있는지 판단.
     * @return {boolean} isFileAttach 신규 첨부파일 여부
     */
    isFileAttach: function(){ 
    	var me = this;
    	var isFileAttach = false;
    	if( me.isSupportedHtml5 ){
    		var fileStore = this.listFilesStore;
			for( var i = 0; i < fileStore.data.items.length; i++ ){
				if( fileStore.data.items[i].raw["file"] ){
					isFileAttach = true;
            		break;
				}
			}
    	}else{
    		var form = this.getForm();
            for( var i = form._fields.items.length - 1; i >= 0; i--){
            	var fileField = form._fields.items[i];
            	if( typeof fileField.value != "undefined" && fileField.value != "" ){
            		isFileAttach = true;
            	}else{
            		form._fields.remove(fileField, true);
            	}
            }	
    	}
        return isFileAttach;
    },
    
    /**
     * initFileAppend
     * @description 최초 파일 등록시 삭제 컬럼 추가
     */
    initFileAppend: function(){ 
    	// Panel에 Delete Action Column 추가
    	this.appendDelActionColumn();
        
        // 첨부파일 List Clear
        this.clearFileListAll();
    },
    
    /**
     * appendDelActionColumn
     * @description 삭제 컬럼 생성
     */
   	appendDelActionColumn: function(){
   		var panel = this.listFilesPanel;
   		if( Ext.getCmp('del_action_column') == null ){
   			var actionColumn = {
	    		id: 'del_action_column',
	            align: 'center',
	            width: 50,
	            menuDisabled: true,
	            items: [{
	            	icon: getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/delete.png',
	                tooltip: getConstText({m_key : "FILE_DELETE"}),
	                getClass: function(value,meta,record,rowIx,colIx, store) { 
	                	if( record.get('id') == "nofile" ){
	                		return 'x-hide-display';  //Hide the action icon	
	                	}
			        },
	                handler: Ext.Function.bind(this.deleteFile, this)
	            }]
			}
			
			var column = Ext.create('Ext.grid.column.Action', actionColumn);
	        panel.headerCt.insert(panel.columns.length, column);
	        panel.getView().refresh();
   		}
   	},
   	
   	/**
     * removeDelActionColumn
     * @description 삭제 컬럼 제거
     */
   	removeDelActionColumn: function(){
   		var panel = this.listFilesPanel;
   		if( Ext.getCmp('del_action_column') != null ){
	        panel.headerCt.remove(Ext.getCmp('del_action_column'));
	        panel.getView().refresh();
   		}
   	},
    
   	/**
   	 * fileCount
   	 * @description 첨부파일 수
   	 * @return {int} fileCount 첨부파일 수
   	 */
    fileCount: function(){
    	var storeSize = this.listFilesStore.count();
    	return storeSize;
    },
    
    /**
     * deleteFile
     * @description 첨부파일삭제
     * @param {object} grid	첨부파일Grid
     * @param {int} rowIndex 선택된 Row Index
     */
    deleteFile: function(grid, rowIndex) {
    	var me = this;
        var rec = grid.getStore().getAt(rowIndex);
        var rowId = rec.get('FILE_IDX');
		
        // 원본파일인 경우 삭제정보 Array에 담아준다.
        if( String(rowId).indexOf("temp_") == -1 ){
        	this.delete_file_info.push(String(rowId));
        }else{
        	if( !me.isSupportedHtml5 ){
        		// 파일 객체 삭제(Form 객체가 아닌 Toobar의 Item Remove)
				var panel = this.listFilesPanel;
				var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
				
				//panel_toolbar.items.removeAll(true);
				for( var i = panel_toolbar.items.length-1; i >= 0; i-- ){
		        	var toolbar_items = panel_toolbar.items.items[i];
		        	if( toolbar_items.name == rowId ){
		        		toolbar_items.hide();
		        		panel_toolbar.items.remove(toolbar_items, true);
		        		break; 
		        	}
		        }
	
		        // Form File Elements 삭제
		        var form = Ext.getCmp(this.id).getForm();
		        for( var i = 0; i < form._fields.items.length; i++){
		        	var filed = form._fields.items[i];
		        	if( filed.name == rowId ){
		        		form._fields.remove(filed, true);
		        		break;
		        	}
		        }
        	}
        }
        
        // 로우에서 삭제
       	grid.getStore().removeAt(rowIndex);
       	
        if( this.fileCount() == 0 ){
        	// 첨부파일이 없을 때
        	this.initFileList();
        }
    },
    
    /**
     * clearFileListAll
     * @description 첨부파일 목록 전체삭제(초기화에 사용)
     */
    clearFileListAll: function(){
    	var storeSize = this.listFilesStore.count();
    	// Removes filenames from the store of the GridPanel
        for( i = 0; i < storeSize; i++ ){
        	this.listFilesStore.removeAt(0);
        }
    },
    
    /**
     * initFileList
     * @description 첨부파일 객체 초기화
     */
    initFileList: function(){
    	var panel = this.listFilesPanel;
    	panel.headerCt.remove(1);
    	panel.getView().refresh();
    	
    	this.listFilesStore.add(this.defaultMessage);
    },
    
    /**
     * saveAsFile
     * @description 첨부파일 저장(신규등록, 수정 모두 처리)
     */
    saveAsFile: function(){
    	if( this.isNoFile() && this.atch_file_id == null ){
    		alert( getConstText({m_key : "NO_FILE"}) );
    		return;
    	}else{
			var form = Ext.getCmp(this.id).getForm();
			var atch_file_id = this.atch_file_id;
			var delete_file_info = this.delete_file_info;
			var file_all_max_size = this.fileAllMaxSize;
			var file_unit_max_size = this.fileUnitMaxSize;
			var url;
			if( atch_file_id != null ){
				url = getConstValue('CONTEXT') + '/itg/base/modifyRegistAtchFile.do';
			}else{
				url = getConstValue('CONTEXT') + '/itg/base/initRegistAtchFile.do';
			}
			
			var params = { atch_file_id : atch_file_id, /*delete_file_info: delete_file_info.join(','), */file_all_max_size : file_all_max_size, file_unit_max_size : file_unit_max_size };
			if( this.delete_file_info.length > 0 ){
				params['delete_file_info'] = delete_file_info.join(',');
			}
			
            form.submit({
                url: url,
                waitMsg: 'Uploading your file...',  
                params: params,
                scope: this,
                success: function(form, action) {
                	response = Ext.decode(action.response.responseText);
                    if(response.success){
                    	alert('File Upload', 'Succefully uploaded to the Server');
                    	if( response.atch_file_id != null ){
                    		this.params = { atch_file_id : response.atch_file_id }; 
                    		this.listFilesStore.getProxy().jsonData = this.params;
                    	}
                    	this.listFilesStore.reload();
                        this.clearToolbarAndField();	// toolbar&form clear
                    }
                },
                failure: function(form, action) {
                	response = Ext.decode(action.response.responseText);
                    if(response.failure){
                    	if( response.resultMsg ){
                    		alert('File Upload', response.resultMsg);
                    	}else{
							alert('File Upload', 'Fail uploaded to the Server');                    		
                    	}
                    	
                    	// 마스크 레이어를 사용중일 때
	                	var mask = Ext.getCmp("viewportMask");
	                	if( mask != null ){
	                		mask.hide();
	                	}
	                	
                    	this.listFilesStore.reload();
                        this.clearToolbarAndField();	// toolbar&form clear
                    }
                }
            });
    	}
    },
    
    /**
     * fileSaveHandler
     * @description 파일 저장 handler
     * @param {string} fileId 파일 Grid Id
     * @param {json} paramMap 파일 handler 파라미터
     * @param {string} callbackFunc Callback Function 명
     * @return {isFile} isFile 첨부파일 처리여부
     */
    fileSaveHandler: function(fileId, paramMap, callbackFunc){
    	var me = this;
    	var isFile = false;
    	var form = this.getForm();
		var file_all_max_size = this.fileAllMaxSize;
		var file_unit_max_size = this.fileUnitMaxSize;
		
		var formData;
		var xhr;
		if( me.isSupportedHtml5 ){
			formData = new FormData(),
	        xhr = new XMLHttpRequest();
		}
		
		var url, handlerType = "", params;
    	if( !this.isNoFile() && this.atch_file_id == null ){
    		handlerType = "regist";
			url = getConstValue('CONTEXT') + '/itg/base/initRegistAtchFile.do';
			if( me.isSupportedHtml5 ){
				formData.append('file_all_max_size', file_all_max_size);
				formData.append('file_unit_max_size', file_unit_max_size);
			}else{
				params = { file_all_max_size : file_all_max_size, file_unit_max_size : file_unit_max_size };
			}
		}
		
		if( this.atch_file_id != null && ( this.isFileAttach() || this.delete_file_info.length > 0 ) ){
			handlerType = "modify";
			var atch_file_id = this.atch_file_id;
			var delete_file_info = this.delete_file_info;
			url = getConstValue('CONTEXT') + '/itg/base/modifyRegistAtchFile.do';
			if( me.isSupportedHtml5 ){
				formData.append('atch_file_id', atch_file_id);
				if( this.delete_file_info.length > 0 ){
					formData.append('delete_file_info', delete_file_info.join(','));
				}
				formData.append('file_all_max_size', file_all_max_size);
				formData.append('file_unit_max_size', file_unit_max_size);
			}else{
				params = { atch_file_id : atch_file_id, /*delete_file_info: delete_file_info.join(','), */file_all_max_size : file_all_max_size, file_unit_max_size : file_unit_max_size };
				if( this.delete_file_info.length > 0 ){
					params['delete_file_info'] = delete_file_info.join(',');
				}
			}
		}     
		
		if( handlerType != "" ){
			isFile = true;
			if( me.isSupportedHtml5 ){
				var fileStore = this.listFilesStore;
				for( var i = 0; i < fileStore.data.items.length; i++ ){
					if( fileStore.data.items[i].raw["file"] ){
						formData.append('file_'+ i, fileStore.data.items[i].raw["file"]);
					}
				}
				
				xhr.open('POST', url, true);
				
				xhr.onloadend = function (evt) {
			        // Show a message containing the result of the upload
			        if (evt.target.status === 200) {
			        	response = Ext.decode(evt.target.responseText);
			        	if(response.success){
			        		me.clearFileListAll();		// 파일리스트 Clear
				        	me.clearToolbarAndField();	// toolbar&form clear
		                	if( handlerType == "regist" ){
		                		me.atch_file_id = response.atch_file_id;
		                		paramMap[fileId] = me.atch_file_id;	
		                	}else{
		                		//me.listFilesStore.reload();	// reload Store	
		                	}
		                	
		                  	// Callback Function
		                	if( callbackFunc != null ){
		                		eval(callbackFunc)();	
		                	}
			        	}else{
			        		if( response.resultMsg ){
		                		alert(response.resultMsg);
		                	}else{
								alert('Fail uploaded to the Server');                    		
		                	}
		                	me.listFilesStore.reload();
		                	me.clearToolbarAndField();	// toolbar&form clear
			        	}
			        } else {
			        	response = Ext.decode(evt.target.responseText);
		                if(response.failure){
		                	if( response.resultMsg ){
		                		alert(response.resultMsg);
		                	}else{
								alert('Fail uploaded to the Server');                    		
		                	}
		                	me.listFilesStore.reload();
		                	me.clearToolbarAndField();	// toolbar&form clear
		                }
			        }
			    };
			    xhr.send(formData);
			}else{
				form.submit({
		            url: url,
		            params: params,
		            scope: this,  
		            success: function(form, action) {
		            	response = Ext.decode(action.response.responseText);
		                if(response.success){
		                	this.clearFileListAll();		// 파일리스트 Clear
		                	this.clearToolbarAndField();	// toolbar&form clear
		                	if( handlerType == "regist" ){
		                		this.atch_file_id = response.atch_file_id;
		                		paramMap[fileId] = this.atch_file_id;	
		                	}else{
		                		this.listFilesStore.reload();	// reload Store	
		                	}
		                	
		                  	// Callback Function
		                	if( callbackFunc != null ){
		                		eval(callbackFunc)();	
		                	}
		                }
		            },
		            failure: function(form, action) {
		            	response = Ext.decode(action.response.responseText);
		                if(response.failure){
		                	if( response.resultMsg ){
		                		alert(response.resultMsg);
		                	}else{
								alert('Fail uploaded to the Server');                    		
		                	}
		                	this.listFilesStore.reload();
		                    this.clearToolbarAndField();	// toolbar&form clear
		                }
		            }
		        });
			}
		}  
		return isFile;
    },
    
    /**
     * getFileList
     * @description 파일목록 Reload
     * @param {string} atchFileId 파일ID
     */
    getFileList: function(atchFileId){
    	this.atch_file_id = atchFileId;
		this.params = { atch_file_id : atchFileId };
		if(this.listFilesStore != null) {
			this.listFilesStore.getProxy().jsonData = this.params;
			this.listFilesStore.reload();				
		}
    },
    
    /**
     * initDefaultFileupload
     * @description 파일업로드 초기화 수행(목록, 파일ID 등)
     */
    initDefaultFileupload: function(){
    	this.initFileAppend();
		this.initFileList();
		this.listFilesStore.getProxy().jsonData = {};
		
		// atch_file_id null
  		this.atch_file_id = null;
  		Ext.getCmp("zipButton").setVisible(false);
    },
    /**
     * clearToolbarAndField
     * @description 첨부파일 업로드시에 임시 파일 객체를 삭제하고 신규 파일객체를 생성
     */
    clearToolbarAndField: function(){
    	var me = this;
    	
    	// 파일 객체 삭제(Form 객체가 아닌 Toobar의 Item Remove)
		var panel = this.listFilesPanel;
		var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
		  
		for( var i = panel_toolbar.items.length-1; i >= 0; i-- ){
        	var toolbar_items = panel_toolbar.items.items[i];
        	if( toolbar_items.name && String(toolbar_items.name).indexOf("temp_") != -1 ){  
        		toolbar_items.hide();
        		panel_toolbar.items.remove(toolbar_items, true);
        	}
        }
        
        // Form File Elements 삭제
        var form = this.getForm();
        if( form._fields ){
        	for( var i = 0; i < form._fields.items.length; i++){
	        	var field = form._fields.items[i];
	        	if( field.name && String(field.name).indexOf("temp_") != -1 ){
	        		form._fields.remove(field, true);
	        	}
	        }	
        }
        
        var createFile = Ext.create('Ext.form.field.File', { 
        	name: 'temp_file_0', 
        	buttonOnly: true,
			hideLabel: true,
			buttonConfig: { 
				text: getConstText({m_key : "FILE_ATCH"}),
		        iconCls: 'icon-add'
		    },
		    listeners: { 
	            afterrender:function(fileField){
		    		if( me.isSupportedHtml5 ){
		    			fileField.fileInputEl.set({
			                multiple:'multiple'
			            });
		    		}
		        },
		        change: function(fileField){
		        	// HTML5 지원여부에 따라서 Event가 바뀐다.
		        	if( me.isSupportedHtml5 ){
		        		var files = fileField.fileInputEl.dom.files;
		        		Ext.Array.each(files, function(file) {
		        			me.addFileSupportedHtml5(file);
		        		});
		    		}else{
		    			me.addFileNotSupportedHtml5(fileField);
		    		}
		        }
	        },
            scope: this
        });
        
        panel_toolbar.insert(0, createFile);

        this.tempFileCount = 0;
        
        var delBtnInitF = false;
        if(this.listFilesStore.getCount() == 1) {
        	var firstRecord = this.listFilesStore.getAt(0);
        	if(firstRecord.raw["FILE_IDX"] == "nofile") {
        		delBtnInitF = true;
        	}
        }
        
        if(delBtnInitF) {
	        this.removeDelActionColumn();
        }
        
        this.delete_file_info = [];
    }
});

/**
 * @namespace nkia.custom.ProcessFileupload
 * @description 프로세스 파일업로드 패널
 */
Ext.define("nkia.custom.ProcessFileupload", {
    extend: 'Ext.form.Panel',
    alias: 'widget.processfileupload',
    
    data_url: getConstValue('CONTEXT') + "/itg/base/selectAtchFileListForProc.do",
    
    // 허용확장자
    allowFileExt: null,
    
    // 전체파일의 Max Size
    fileAllMaxSize: null,
    
    // 개별파일의 Max Size
    fileUnitMaxSize: null,
    
    listFilesPanel: null,
    
    maxFileCount: null,
    
    listFilesStore: null,
    
    listFilesProxy: null,
    
    tempFileCount: 0,
    
    atch_file_id: null,
    
    readyState: false,
    
   	download_frame_id: 'download_frame', 
    
    delete_file_info: [],
    
    defaultMessage: {FILE_IDX: 'nofile', ORIGNL_FILE_NAME: '<b>' + getConstText({m_key : "NO_FILE"}) + '</b>'},
    
    isSupportedHtml5 : true,
    
    dropMsgMask : null,
    
    initComponent: function(){
    	this.callParent(arguments);
    },
      
    /**
	 * onRender
	 * @description 파일업로드 패널 Render
	 */
    onRender: function() {
    	this.callParent(arguments);
    	
        var agt = navigator.userAgent;
    	if(agt.indexOf("MSIE 6") != -1 || agt.indexOf("MSIE 7") != -1 || agt.indexOf("MSIE 8") != -1 || agt.indexOf("MSIE 9") != -1 ){
    		this.isSupportedHtml5 = false;
    	}
         
        // Creates the GridPanel element which contains the list of filenames
        this.createFileList();
        this.add(this.listFilesPanel);
        
        // Create frame by filedownload
        this.download_frame_id = ( this['downloadFrameId'] ) ? this['downloadFrameId'] : this.download_frame_id;
        createFileDownloadFrame(this.download_frame_id);
        this.readyState = true;
    },
    
    countFileList: function(){
    	var me = this;
    	var listProxy = Ext.create('nkia.custom.JsonProxy', {
			defaultPostHeader : "application/json",
			noCache : false,
			type: 'ajax', 
			url: me["data_url"],
			jsonData : ( me['params'] ) ? me['params'] : null,
			actionMethods: {
				create: 'POST',
				read: 'POST',
				update: 'POST'
			},
			reader: {
				type: 'json',
				root: 'gridVO.rows'
			}
		}); 
		
    	var listStore = Ext.create('Ext.data.Store', {
    		fields: ['ATCH_FILE_ID','FILE_IDX','NODENAME','ORIGNL_FILE_NAME','UPD_DT'],
    		proxy: listProxy, 
    		autoLoad: true,
    		listeners: {
				load : function(store){
					me.showFileCount(me, store.data.length);	// 건수 표기
				}
    		}
    	});
    },
    
    /**
	 * createFileList
	 * @description 파일업로드 Grid 생성
	 */
    createFileList : function() {
    	var me = this;
    	this.listFilesProxy = Ext.create('nkia.custom.JsonProxy', {
			defaultPostHeader : "application/json",
			noCache : false,
			type: 'ajax', 
			url: this.data_url,
			jsonData : ( me['params'] ) ? me['params'] : null,
			actionMethods: {
				create: 'POST',
				read: 'POST',
				update: 'POST'
			},
			reader: {
				type: 'json',
				root: 'gridVO.rows'
			}
		});  
    	
    	// Creates the store
    	this.listFilesStore = Ext.create('Ext.data.Store', {
    	    fields: ['ATCH_FILE_ID','FILE_IDX','NODENAME','ORIGNL_FILE_NAME','UPD_DT'],
    		proxy: this.listFilesProxy, 
    		autoLoad: true,
    		//@@20130727 김도원 START
    		//load 함수가 listeners없이 선언되어 있어서 수정함
    		listeners: {
				load : function(store){
					var zipButtonCmp = Ext.getCmp("zipButton");
					if( me.isNoFile() ){
						if( zipButtonCmp ){
							zipButtonCmp.setVisible(false);	
						}
					}else{
						if( zipButtonCmp ){
							zipButtonCmp.setVisible(true);	
						}
					}
					
					if( store.data.length > 0 ){     
						// data exits      
						if(me.editorMode) {
							me.appendDelActionColumn();	// Append Delete Action Column
						}
						
			    		//grid.store.on('row', function(store, records, options){
			    		me.listFilesPanel.getView().on( 'celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
	                  		if( cellIndex == 0 || cellIndex == 1 ){
	                  			me.downloadFile(record);
	                  		}
	                  	});
	                  	
	                  	me.atch_file_id = store.data.items[0].raw.ATCH_FILE_ID;
	                  	
	                  	me.showFileCount(me, store.data.length);	// 건수 표기
	                  	
			    	}else{
			    		// data not exits
			    		me.listFilesStore.add(me.defaultMessage);
			    	} 
				},
				datachanged : function(store, records, options){
					if( store.data.length == 1 && store.data.items[0].raw.FILE_IDX == "nofile" ){ 
						me.showFileCount(me, 0);
	    			}else{
						me.showFileCount(me, store.data.length);
	    			}
				}  
    		}
    		//@@20130727 김도원 END
    	});
    	
    	var buttons = null;
    	if( me.saveButton ){	// saveButton true일 경우 버튼 보여주기
    		buttons = [];
    		buttons.push({
    			text: getConstText({m_key : "SAVE"}),
	 			handler: Ext.Function.bind(this.saveAsFile, me)	
    		})
    	}
    	
    	var dockedItem = [];
    	if( me.editorMode ){
    		dockedItem.push({ 
    			id: this.id+'_toolbar',
                xtype: 'toolbar',
                dock: 'bottom',
                items: [{
	                	name: 'temp_file_0',
	                	xtype: 'filefield',	
	                	buttonOnly: true,
	        			hideLabel: true,
	        			buttonConfig: { 
							text: getConstText({m_key : "FILE_ATCH"}),
					        iconCls: 'icon-add'
					    },
					    listeners: {
				            afterrender: function(fileField){
					    		if( me.isSupportedHtml5 ){
					    			fileField.fileInputEl.set({
						                multiple:'multiple'
						            });
					    		}
					        },
					        change: function(fileField){
					        	// HTML5 지원여부에 따라서 Event가 바뀐다.
					        	if( me.isSupportedHtml5 ){
					        		var files = fileField.fileInputEl.dom.files;
					        		Ext.Array.each(files, function(file) {
					        			me.addFileSupportedHtml5(file);
					        		});
					    		}else{
					    			//Ext.Function.bind(me.addFileNotSupportedHtml5, me);
					    			me.addFileNotSupportedHtml5(fileField);
					    		}
					        }
				        },
	                    scope: this
				},'->',{
                	xtype: 'button',  
                	id: 'zipButton',
                	name: 'zip_btn',
                	iconCls: 'icon-disk',
//                	cls: "x-btn-default-small x-icon-text-left x-btn-icon-text-left x-btn-default-toolbar-small-icon-text-left",
//					overCls: "x-over x-btn-over x-btn-default-toolbar-small-over over",
//					ui: "x-btn-default-small",
                	text: '모든 파일 내려받기',
                	hidden: true,
                	handler: Ext.Function.bind(this.downloadZipFile, me)
				}]
    		});
    	}
    	
    	// Creates the GridPanel
    	this.listFilesPanel = Ext.create('nkia.custom.grid', {
    		margin: '0 0 0 0',
    		padding: '0 0 0 0',
    		store: this.listFilesStore,
    		dockedItems: ( dockedItem.length > 0 ) ? dockedItem : null,
    	    columns: [{
    	    	header: getConstText({ isArgs: true, m_key: 'res.00027' }), 
    	    	dataIndex: 'NODENAME',
    	    	menuDisabled: true,
    	    	flex: 0.5
    	    },{
    	    	header: getConstText({m_key : "FILE_NAME"}), 
    	    	dataIndex: 'ORIGNL_FILE_NAME',
    	    	menuDisabled: true,
    	    	flex: 3
    	    },{
    	    	header: getConstText({ isArgs: true, m_key: 'res.00028' }), 
    	    	dataIndex: 'UPD_DT',
    	    	menuDisabled: true,
    	    	flex: 0.5
    	    }], 
    	    defaults: {
                flex: 1
            },
    	  	height: this.listFilesHeight,
    	    buttons: buttons,
    	    viewConfig: {
			    loadMask: false
			},
	        listeners: {
				render : function(grid){
					/*
                  	grid.store.on('load', function(store, records, options){
         				if( store.data.length > 0 ){
							// data exits   
         					if(me.editorMode) {
					    		me.appendDelActionColumn();	// Append Delete Action Column
         					}
				    		//grid.store.on('row', function(store, records, options){
				    		me.listFilesPanel.getView().on( 'celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
		                  		if( cellIndex == 1 ){
		                  			me.downloadFile(record);
		                  		}
		                  	});
		                  	
		                  	me.atch_file_id = store.data.items[0].raw.ATCH_FILE_ID;
				    	}else{
				    		// data not exits
				    		me.listFilesStore.add(me.defaultMessage);
				    	} 
                  	});
                  	*/
                }
	        }
    	});
    	
    	if( me.isSupportedHtml5 ){
    		me.dropMsgMask = new Ext.LoadMask(me.container.dom, {
    			msg: "여기로 파일을 Drag&Drop 해주세요."
    		});
    		
    		var containerEl = me.container;
    		
        	containerEl.dom.addEventListener("dragenter", function (evt) {
        		//console.info("dragenter");
        		//me.showMsg(true);
        		return false;
        	}, false);

        	containerEl.dom.addEventListener("dragover", function (evt) {
        		//console.info("dragover");
        		evt.stopPropagation();
        		evt.preventDefault();
        		return false;
        	}, false);

        	containerEl.dom.addEventListener("dragleave", function(evt) {
        		//console.info("dragleave");
        		evt.stopPropagation();
        		evt.preventDefault();
        		//me.showMsg(false);
        		return false;
        	}, false);

        	containerEl.dom.addEventListener("drop", function (evt) {
        		//console.info("drop");
        		evt.stopPropagation();
        		evt.preventDefault(); 
        		
        		var fileField = me.getForm().findField('temp_file_0');
        		if(fileField.isVisible()){
        			//me.showMsg(false);
        			Ext.Array.each(evt.dataTransfer.files, function(file) { 
        				me.addFileSupportedHtml5(file);
        			});	
        		}
        	}, false);
    	}
    },
    
    showFileCount: function(panel, fileCount){
    	var title = panel.originTitle;
    	if(panel.required){
    		title = getConstValue('NOT_NULL_SYMBOL') + title; 
    	}
    	panel.setTitle(title + " [ " + fileCount + " 건 ]");
    },
    
    /**
     * downloadFile
     * @description 파일다운로드
     * @param {data} record 그리드 데이터
     */
    downloadFile: function(record){
    	var downloader = Ext.getCmp(this.download_frame_id);
		downloader.load({
			url: getConstValue('CONTEXT') + '/itg/base/downloadAtchFile.do?atch_file_id=' + record.get('ATCH_FILE_ID') + '&file_idx=' + record.get('FILE_IDX')
		});
    }, 
    /**
     * downloadZipFile
     * @description 파일다운로드 전체 : Zip으로 압축
     * @param {data} record 그리드 데이터
     */
    downloadZipFile: function(record){
    	var downloader = Ext.getCmp(this.download_frame_id)
		downloader.load({
			url: getConstValue('CONTEXT') + '/itg/base/downloadZipFile.do?atch_file_id='+this.atch_file_id+'&zip_file_name=AtchFile'
		});
    },
    
    /**
     * @private Event handler fired when the user selects a file.
     */
    addFileNotSupportedHtml5: function(fileButton) {
    	var me = this;
    	var fileEl = fileButton.inputEl.dom;
    	var fileName = fileEl.value;
    	
    	if( fileName != "" ){
    		var fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length);
    	
	    	var i = 0;
	    	
			// 파일이 최초등록인지 판단
			if( this.isNoFile() ){
				// 최초등록일 경우 Delete Column 생성
				this.initFileAppend();
			}else{
				// 최대 첨부파일 개수
				if( this.maxFileCount != null && this.maxFileCount != "" ){
					var atchFileCount = this.fileCount();
					if( atchFileCount >= this.maxFileCount ){
						alert(getConstText({m_key : "msg.common.max.file.count", isArgs : true, args : String(this.maxFileCount) }))
						fileButton.reset();
						return;
					} 
				}
			}
			
			// 파일첨부 허용 확장자 
			if( this.allowFileExt != null && this.allowFileExt != ""  ){
				var allowFileExtSplit = this.allowFileExt.split(",");
				var isAllow = false;
				for( var i = 0; i < allowFileExtSplit.length; i++ ){
					if( fileExt == allowFileExtSplit[i] ){
						isAllow = true;
						break;
					}
				}
				if( !isAllow ){
					alert(getConstText({m_key : "msg.common.allow.file.ext", isArgs : true, args : this.allowFileExt }))
					fileButton.reset();
					return;
				}
			}
			
			// 첨부파일 Button 생성
	        var fileCount = this.tempFileCount;
	    	var nextFileCount = ++this.tempFileCount;
	        var createFile = Ext.create('Ext.form.field.File', { 
	        	name: 'temp_file_'+nextFileCount, 
	        	buttonOnly: true,
				hideLabel: true,
				buttonConfig: { 
					text: getConstText({m_key : "FILE_ATCH"}),
			        iconCls: 'icon-add'
			    },
			    listeners: {
			        change: function(fileField){
			    		me.addFileNotSupportedHtml5(fileField);
			        }
		        },
	            scope: this
	        });
	        
	        var panel = this.listFilesPanel;
	        var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
	        for( var i = 0; i < panel_toolbar.items.length; i++ ){ 
	        	var toolbar_items = panel_toolbar.items.items[i];
	        	if( typeof toolbar_items.name != "undefined" && toolbar_items.name != "zip_btn"){
					toolbar_items.hide();        		
	        	}
	        }
	        panel_toolbar.insert(0, createFile);
			
	        // File List Grid에 파일명 추가 / 파일 객체와 파일리스트 동기화
	        this.listFilesStore.add({NODENAME: this.nodeName, FILE_IDX: 'temp_file_'+fileCount, ORIGNL_FILE_NAME: fileName});
    	}
    },
    
    addFileSupportedHtml5: function(file) {
    	var fileName = file.name;
        	
    	if( fileName != "" ){
    		var fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length);
    	
	    	var i = 0;
	    	
			// 파일이 최초등록인지 판단
			if( this.isNoFile() ){
				// 최초등록일 경우 Delete Column 생성
				this.initFileAppend();
			}else{
				// 최대 첨부파일 개수
				if( this.maxFileCount != null && this.maxFileCount != "" ){
					var atchFileCount = this.fileCount();
					if( atchFileCount >= this.maxFileCount ){
						alert(getConstText({m_key : "msg.common.max.file.count", isArgs : true, args : String(this.maxFileCount) }))
						return;
					} 
				}
			}
			
			var fileCount = this.fileCount();
			// 파일첨부 허용 확장자 
			if( this.allowFileExt != null && this.allowFileExt != ""  ){
				var allowFileExtSplit = this.allowFileExt.split(",");
				var isAllow = false;
				for( var i = 0; i < allowFileExtSplit.length; i++ ){
					if( fileExt == allowFileExtSplit[i] ){
						isAllow = true;
						break;
					}
				}
				if( !isAllow ){
					alert(getConstText({m_key : "msg.common.allow.file.ext", isArgs : true, args : this.allowFileExt }))
					return;
				}
			}
	        // File List Grid에 파일명 추가 / 파일 객체와 파일리스트 동기화
	        this.listFilesStore.add({NODENAME: this.nodeName, FILE_IDX: 'temp_file_'+fileCount, ORIGNL_FILE_NAME: fileName, file: file});
    	}
    },
    
     /**
     * onFileChange
     * @description 파일첨부 이벤트
     * @param {obejct} fileButton 파일버튼객체
     */
    onFileChange: function(fileButton) {
    	var fileEl = fileButton.inputEl.dom;
    	var fileName = fileEl.value;
    	
    	if( fileName != "" ){
    		var fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length);
    	
	    	var i = 0;
	    	
			// 파일이 최초등록인지 판단
			if( this.isNoFile() ){
				// 최초등록일 경우 Delete Column 생성
				this.initFileAppend();
			}else{
				// 최대 첨부파일 개수
				if( this.maxFileCount != null && this.maxFileCount != "" ){
					var atchFileCount = this.fileCount();
					if( atchFileCount >= this.maxFileCount ){
						alert(getConstText({m_key : "msg.common.max.file.count", isArgs : true, args : String(this.maxFileCount) }))
						fileButton.reset();
						return;
					} 
				}
			}
			
			// 기등록된 파일인지 확인하는 로직 추가(2014.09.22 고은규)
			for(var i = 0; i < this.listFilesStore.data.items.length; i++){				
				var fileInfo = this.listFilesStore.data.items[i].raw.ORIGNL_FILE_NAME;

				var selFileName = fileInfo.substring(fileInfo.lastIndexOf("\\")+1, fileInfo.length); // 선택된 파일
				var inFileName = fileName.substring(fileName.lastIndexOf("\\")+1, fileName.length);  // 기등록된 파일
				
				if(inFileName == selFileName){
					alert(getConstText({m_key : "msg.common.file.comparison", isArgs : true}));
					fileButton.reset();
					return;
				}
			}			
			
			// 파일첨부 허용 확장자 
			if( this.allowFileExt != null && this.allowFileExt != ""  ){
				var allowFileExtSplit = this.allowFileExt.split(",");
				var isAllow = false;
				for( var i = 0; i < allowFileExtSplit.length; i++ ){
					if( fileExt.toUpperCase() == allowFileExtSplit[i].toUpperCase() ){
						isAllow = true;
						break;
					}
				}
				if( !isAllow ){
					alert(getConstText({m_key : "msg.common.allow.file.ext", isArgs : true, args : this.allowFileExt }))
					fileButton.reset();
					return;
				}
			}
			
			// 첨부파일 Button 생성
			var me = this;
	        var fileCount = this.tempFileCount;
	    	var nextFileCount = ++this.tempFileCount;
	        var createFile = Ext.create('Ext.form.field.File', { 
	        	name: 'temp_file_'+nextFileCount, 
	        	buttonOnly: true,
				hideLabel: true,
				buttonConfig: { 
					text: getConstText({m_key : "FILE_ATCH"}),
			        iconCls: 'icon-add'
			    },
			    listeners: {
		            'change': Ext.Function.bind(this.onFileChange, this)
		        },
	            scope: this
	        });
	        
	        var panel = this.listFilesPanel;
	        var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
	        for( var i = 0; i < panel_toolbar.items.length; i++ ){ 
	        	var toolbar_items = panel_toolbar.items.items[i];
	        	if( typeof toolbar_items.name != "undefined" && toolbar_items.name != "zip_btn"){
					toolbar_items.hide();        		
	        	}
	        }
	        panel_toolbar.insert(0, createFile);
			
	        // File List Grid에 파일명 추가 / 파일 객체와 파일리스트 동기화
	        this.listFilesStore.add({NODENAME: this.nodeName, FILE_IDX: 'temp_file_'+fileCount, ORIGNL_FILE_NAME: fileName});
    	}
    },
    /**
     * isNoFile
     * @description 첨부파일이 있는지 판단.
     * @return {boolean} isNoFile 첨부파일유무
     */
    isNoFile: function(){
    	var isNoFile = false;
    	var fileCount = this.fileCount();
    	if( fileCount == 0 ){
    		isNoFile = true;    
    	}else if( fileCount == 1 ){
    		var store = this.listFilesStore;
	    	if( store.data.items[0].raw.FILE_IDX == "nofile" ){ 
				isNoFile = true; 	
	    	}   
    	}
    	return isNoFile;
    },
    /**
     * isFileAttach
     * @description 신규 첨부파일이 있는지 판단.
     * @return {boolean} isFileAttach 신규 첨부파일 여부
     */
    isFileAttach: function(){ 
    	var me = this;
    	var isFileAttach = false;
    	if( me.isSupportedHtml5 ){
    		var fileStore = this.listFilesStore;
			for( var i = 0; i < fileStore.data.items.length; i++ ){
				if( fileStore.data.items[i].raw["file"] ){
					isFileAttach = true;
            		break;
				}
			}
    	}else{
    		var form = this.getForm();
            for( var i = form._fields.items.length - 1; i >= 0; i--){
            	var fileField = form._fields.items[i];
            	if( typeof fileField.value != "undefined" && fileField.value != "" ){
            		isFileAttach = true;
            	}else{
            		form._fields.remove(fileField, true);
            	}
            }	
    	}
        return isFileAttach;
    },
    /**
     * initFileAppend
     * @description 최초 파일 등록시 삭제 컬럼 추가
     */
    initFileAppend: function(){ 
    	// Panel에 Delete Action Column 추가
    	this.appendDelActionColumn();
        
        // 첨부파일 List Clear
        this.clearFileListAll();
    },
    /**
     * appendDelActionColumn
     * @description 삭제 컬럼 생성
     */
   	appendDelActionColumn: function(){
   		var panel = this.listFilesPanel;
   		if( Ext.getCmp('del_action_column') == null ){
   			var actionColumn = {
	    		id: 'del_action_column',
	            align: 'center',
	            width: 50,
	            menuDisabled: true,
	            items: [{
	            	icon: getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/delete.png',
	                tooltip: getConstText({m_key : "FILE_DELETE"}),
	                getClass: function(value,meta,record,rowIx,colIx, store) { 
	                	if( record.get('id') == "nofile" ){
	                		return 'x-hide-display';  //Hide the action icon	
	                	}
			        },
	                handler: Ext.Function.bind(this.deleteFile, this)
	            }]
			}
			
			var column = Ext.create('Ext.grid.column.Action', actionColumn);
	        panel.headerCt.insert(panel.columns.length, column);
	        panel.getView().refresh();
   		}
   	},
   	
   	/**
     * removeDelActionColumn
     * @description 삭제 컬럼 제거
     */
   	removeDelActionColumn: function(){
   		var panel = this.listFilesPanel;
   		if( Ext.getCmp('del_action_column') != null ){
	        panel.headerCt.remove(Ext.getCmp('del_action_column'));
	        panel.getView().refresh();
   		}
   	},
   	
    /**
   	 * fileCount
   	 * @description 첨부파일 수
   	 * @return {int} fileCount 첨부파일 수
   	 */
    fileCount: function(){
    	var storeSize = this.listFilesStore.count();
    	return storeSize;
    },
    /**
     * deleteFile
     * @description 첨부파일삭제
     * @param {object} grid	첨부파일Grid
     * @param {int} rowIndex 선택된 Row Index
     */
    deleteFile: function(grid, rowIndex, colIndex) {
    	var me = this;
        var rec = grid.getStore().getAt(rowIndex);
        var rowId = rec.get('FILE_IDX');
		
        // 원본파일인 경우 삭제정보 Array에 담아준다.
        if( String(rowId).indexOf("temp_") == -1 ){
        	this.delete_file_info.push(String(rowId));
        }else{
        	if( !me.isSupportedHtml5 ){
        		// 파일 객체 삭제(Form 객체가 아닌 Toobar의 Item Remove)
				var panel = this.listFilesPanel;
				var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
				
				//panel_toolbar.items.removeAll(true);
				for( var i = panel_toolbar.items.length-1; i >= 0; i-- ){
		        	var toolbar_items = panel_toolbar.items.items[i];
		        	if( toolbar_items.name == rowId ){
		        		toolbar_items.hide();
		        		panel_toolbar.items.remove(toolbar_items, true);
		        		break; 
		        	}
		        }
	
		        // Form File Elements 삭제
		        var form = Ext.getCmp(this.id).getForm();
		        for( var i = 0; i < form._fields.items.length; i++){
		        	var filed = form._fields.items[i];
		        	if( filed.name == rowId ){
		        		form._fields.remove(filed, true);
		        		break;
		        	}
		        }
        	}
        }
        
        // 로우에서 삭제
       	grid.getStore().removeAt(rowIndex);
       	
        if( this.fileCount() == 0 ){
        	// 첨부파일이 없을 때
        	this.initFileList();
        }
    },
    /**
     * clearFileListAll
     * @description 첨부파일 목록 전체삭제(초기화에 사용)
     */
    clearFileListAll: function(){
    	var storeSize = this.listFilesStore.count();
    	// Removes filenames from the store of the GridPanel
        for( i = 0; i < storeSize; i++ ){
        	this.listFilesStore.removeAt(0);
        }
    },
    
	/**
     * initFileList
     * @description 첨부파일 객체 초기화
     */
    initFileList: function(){
    	var panel = this.listFilesPanel;
    	panel.headerCt.remove(3);
    	panel.getView().refresh();
    	this.listFilesStore.add(this.defaultMessage);
    },
    
    /**
     * saveAsFile
     * @description 첨부파일 저장(신규등록, 수정 모두 처리)
     */
    saveAsFile: function(){
    	if( this.isNoFile() && this.atch_file_id == null ){
    		alert( getConstText({m_key : "NO_FILE"}) );
    		return;
    	}else{
			var form = Ext.getCmp(this.id).getForm();
			var atch_file_id = this.atch_file_id;
			var delete_file_info = this.delete_file_info;
			var file_all_max_size = this.fileAllMaxSize;
			var file_unit_max_size = this.fileUnitMaxSize;
			var url;
			if( atch_file_id != null ){
				url = getConstValue('CONTEXT') + '/itg/base/modifyRegistAtchFile.do';
			}else{
				url = getConstValue('CONTEXT') + '/itg/base/initRegistAtchFile.do';
			}
			
			var params = { atch_file_id : atch_file_id, /*delete_file_info: delete_file_info.join(','), */file_all_max_size : file_all_max_size, file_unit_max_size : file_unit_max_size };
			if( this.delete_file_info.length > 0 ){
				params['delete_file_info'] = delete_file_info.join(',');
			}
			
            form.submit({
                url: url,
                waitMsg: 'Uploading your file...',  
                params: params,
                scope: this,
                success: function(form, action) {
                	response = Ext.decode(action.response.responseText);
                    if(response.success){
                    	alert('File Upload', 'Succefully uploaded to the Server');
//                    	if( response.atch_file_id != null ){
//                    		this.params = { atch_file_id : response.atch_file_id }; 
//                    		this.listFilesStore.getProxy().jsonData = this.params;
//                    	}
                    	this.listFilesStore.reload();
                        this.clearToolbarAndField();	// toolbar&form clear
                    }
                },
                failure: function(form, action) {
                	response = Ext.decode(action.response.responseText);
                    if(response.failure){
                    	if( response.resultMsg ){
                    		alert('File Upload', response.resultMsg);
                    	}else{
							alert('File Upload', 'Fail uploaded to the Server');                    		
                    	}
                    	
                    	// 마스크 레이어를 사용중일 때
	                	var mask = Ext.getCmp("viewportMask");
	                	if( mask != null ){
	                		mask.hide();
	                	}
                    	
                    	this.listFilesStore.reload();
                        this.clearToolbarAndField();	// toolbar&form clear
                    }
                }
            });
    	}
    },
    
    /**
     * fileSaveHandler
     * @description 파일 저장 handler
     * @param {string} fileId 파일 Grid Id
     * @param {json} paramMap 파일 handler 파라미터
     * @param {string} callbackFunc Callback Function 명
     * @return {isFile} isFile 첨부파일 처리여부
     */
    fileSaveHandler: function(fileId, paramMap, callbackFunc){
    	var me = this;
    	var isFile = false;
    	var form = this.getForm();
		var file_all_max_size = this.fileAllMaxSize;
		var file_unit_max_size = this.fileUnitMaxSize;
		
		var taskId = paramMap["nbpm_task_id"];
		var taskName = paramMap["nbpm_task_name"];
		
		if( taskId == "" && taskName == "start" ){
			taskId = "REQUST_REGIST";
		}
		
		var formData;
		var xhr;
		if( me.isSupportedHtml5 ){
			formData = new FormData(),
	        xhr = new XMLHttpRequest();
		}
		
		var url, handlerType = "", params;
    	if( !this.isNoFile() && (this.atch_file_id == null || this.atch_file_id == "") ){
    		handlerType = "regist";
			url = getConstValue('CONTEXT') + '/itg/base/initRegistAtchFile.do';
			if( me.isSupportedHtml5 ){
				formData.append('task_id', taskId);
				formData.append('file_all_max_size', file_all_max_size);
				formData.append('file_unit_max_size', file_unit_max_size);
			}else{
				params = { task_id: taskId, file_all_max_size : file_all_max_size, file_unit_max_size : file_unit_max_size };
			}
		} else if( this.atch_file_id != null && this.atch_file_id != "" && ( this.isFileAttach() || this.delete_file_info.length > 0 ) ){
			handlerType = "modify";
			var atch_file_id = this.atch_file_id;
			var delete_file_info = this.delete_file_info;
			url = getConstValue('CONTEXT') + '/itg/base/modifyRegistAtchFile.do';
			if( me.isSupportedHtml5 ){
				formData.append('task_id', taskId);
				formData.append('atch_file_id', atch_file_id);
				if( this.delete_file_info.length > 0 ){
					formData.append('delete_file_info', delete_file_info.join(','));
				}
				formData.append('file_all_max_size', file_all_max_size);
				formData.append('file_unit_max_size', file_unit_max_size);
			}else{
				var params = { task_id: taskId, atch_file_id : atch_file_id, /*delete_file_info: delete_file_info.join(','), */file_all_max_size : file_all_max_size, file_unit_max_size : file_unit_max_size };
				if( this.delete_file_info.length > 0 ){
					params['delete_file_info'] = delete_file_info.join(',');
				}
			}
		}
		
		if( handlerType != "" ){
			isFile = true;
			if( me.isSupportedHtml5 ){
				var fileStore = this.listFilesStore;
				for( var i = 0; i < fileStore.data.items.length; i++ ){
					if( fileStore.data.items[i].raw["file"] ){
						formData.append('file_'+ i, fileStore.data.items[i].raw["file"]);
					}
				}
				
				xhr.open('POST', url, true);
				
				xhr.onloadend = function (evt) {
			        // Show a message containing the result of the upload
			        if (evt.target.status === 200) {
			        	response = Ext.decode(evt.target.responseText);
			        	if(response.success){
			        		me.clearFileListAll();		// 파일리스트 Clear
				        	me.clearToolbarAndField();	// toolbar&form clear
		                	if( handlerType == "regist" ){
		                		me.atch_file_id = response.atch_file_id;
		                		paramMap[fileId] = me.atch_file_id;	
		                	}else{
		                		//me.listFilesStore.reload();	// reload Store	
		                	}
		                	
		                  	// Callback Function
		                	if( callbackFunc != null ){
		                		eval(callbackFunc)();	
		                	}
			        	}else{
			        		if( response.resultMsg ){
		                		alert(response.resultMsg);
		                	}else{
								alert('Fail uploaded to the Server');                    		
		                	}
		                	me.listFilesStore.reload();
		                	me.clearToolbarAndField();	// toolbar&form clear
			        	}
			        } else {
			        	response = Ext.decode(evt.target.responseText);
		                if(response.failure){
		                	if( response.resultMsg ){
		                		alert(response.resultMsg);
		                	}else{
								alert('Fail uploaded to the Server');                    		
		                	}
		                	me.listFilesStore.reload();
		                	me.clearToolbarAndField();	// toolbar&form clear
		                }
			        }
			    };
			    xhr.send(formData);
			}else{
				form.submit({
		            url: url,
		            params: params,
		            scope: this,  
		            success: function(form, action) {
		            	response = Ext.decode(action.response.responseText);
		                if(response.success){
		                	this.clearFileListAll();		// 파일리스트 Clear
		                	this.clearToolbarAndField();	// toolbar&form clear
		                	if( handlerType == "regist" ){
		                		this.atch_file_id = response.atch_file_id;
		                		paramMap[fileId] = this.atch_file_id;	
		                	}else{
		                		this.listFilesStore.reload();	// reload Store	
		                	}
		                	
		                  	// Callback Function
		                	if( callbackFunc != null ){
		                		eval(callbackFunc)();	
		                	}
		                }
		            },
		            failure: function(form, action) {
		            	response = Ext.decode(action.response.responseText);
		                if(response.failure){
		                	if( response.resultMsg ){
		                		alert(response.resultMsg);
		                	}else{
								alert('Fail uploaded to the Server');                    		
		                	}
		                	this.listFilesStore.reload();
		                    this.clearToolbarAndField();	// toolbar&form clear
		                }
		            }
		        });
			}
		}
		return isFile;
    },
    
    /**
     * getFileList
     * @description 파일목록 Reload
     * @param {string} atchFileId 파일ID
     */
    getFileList: function(srId, atchFileId){
    	this.atch_file_id = atchFileId;
		this.params = { sr_id : srId }; 
		this.listFilesStore.getProxy().jsonData = this.params;
		this.listFilesStore.reload();	
    },
    
    /**
     * initDefaultFileupload
     * @description 파일업로드 초기화 수행(목록, 파일ID 등)
     */
    initDefaultFileupload: function(){
    	this.initFileAppend();
		this.initFileList();
	
		//Ext.getCmp(this.id + "_zipButton").setDisabled(true);
		
		this.listFilesStore.getProxy().jsonData = {};
		
		// atch_file_id null
  		this.atch_file_id = null;
    },
    /**
     * clearToolbarAndField
     * @description 첨부파일 업로드시에 임시 파일 객체를 삭제하고 신규 파일객체를 생성
     */
    clearToolbarAndField: function(){
    	var me = this;
    	
    	// 파일 객체 삭제(Form 객체가 아닌 Toobar의 Item Remove)
		var panel = this.listFilesPanel;
		var panel_toolbar = panel.getDockedComponent(this.id+'_toolbar');
		  
		for( var i = panel_toolbar.items.length-1; i >= 0; i-- ){
        	var toolbar_items = panel_toolbar.items.items[i];
        	if( toolbar_items.name && String(toolbar_items.name).indexOf("temp_") != -1 ){  
        		toolbar_items.hide();
        		panel_toolbar.items.remove(toolbar_items, true);
        	}
        }
        
        // Form File Elements 삭제
        var form = this.getForm();
        if( form._fields ){
        	for( var i = 0; i < form._fields.items.length; i++){
	        	var field = form._fields.items[i];
	        	if( field.name && String(field.name).indexOf("temp_") != -1 ){
	        		form._fields.remove(field, true);
	        	}
	        }	
        }
        
        var createFile = Ext.create('Ext.form.field.File', { 
        	name: 'temp_file_0', 
        	buttonOnly: true,
			hideLabel: true,
			buttonConfig: { 
				text: getConstText({m_key : "FILE_ATCH"}),
		        iconCls: 'icon-add'
		    },
		    listeners: { 
	           afterrender:function(fileField){
		    		if( me.isSupportedHtml5 ){
		    			fileField.fileInputEl.set({
			                multiple:'multiple'
			            });
		    		}
		        },
		        change: function(fileField){
		        	// HTML5 지원여부에 따라서 Event가 바뀐다.
		        	if( me.isSupportedHtml5 ){
		        		var files = fileField.fileInputEl.dom.files;
		        		Ext.Array.each(files, function(file) {
		        			me.addFileSupportedHtml5(file);
		        		});
		    		}else{
		    			me.addFileNotSupportedHtml5(fileField);
		    		}
		        }
	        },
            scope: this
        });
        
        panel_toolbar.insert(0, createFile);

        this.tempFileCount = 0;
        
        var delBtnInitF = false;
        if(this.listFilesStore.getCount() == 1) {
        	var firstRecord = this.listFilesStore.getAt(0);
        	if(firstRecord.raw["FILE_IDX"] == "nofile") {
        		delBtnInitF = true;
        	}
        }
        
        if(delBtnInitF) {
	        this.removeDelActionColumn();
        }
        
        this.delete_file_info = [];
        
    }
});

/**
 * @namespace nkia.custom.DownLoader
 * @description 파일다운로드만 가능한 패널
 */
Ext.define('nkia.custom.DownLoader', {
    extend: 'nkia.custom.DefaultFileupload',
    alias: 'widget.FileDownloadFrame',
    /**
	 * createFileList
	 * @description 파일다운로드 Grid 생성
	 */
    createFileList : function() {
    	var me = this;
    	var proxyParam = {};
    	var autoLoadFlag = false;
    	if(me.params != null) {
    		proxyParam = me.params;
    		autoLoadFlag = true;
    	}
    	this.listFilesProxy = Ext.create('nkia.custom.JsonProxy', {
			defaultPostHeader : "application/json",
			noCache : false,
			type: 'ajax', 
			url: this.data_url,
			jsonData : proxyParam,
			actionMethods: {
				create: 'POST',
				read: 'POST',
				update: 'POST'
			},
			reader: {
				type: 'json',
				root: 'gridVO.rows'
			}
		});  

    	this.listFilesStore = Ext.create('Ext.data.Store', {
    	    fields: ['ATCH_FILE_ID','FILE_IDX','ORIGNL_FILE_NAME'],
    		proxy: this.listFilesProxy, 
    		autoLoad: autoLoadFlag,
    		listeners: {
				load : function(store){
					/*
					var zipButtonCmp = Ext.getCmp(this.id + "_zipButton");
					if( me.isNoFile() ){
						if( zipButtonCmp ){
							zipButtonCmp.setDisabled(true);	
						}
					}else{
						if( zipButtonCmp ){
							zipButtonCmp.setDisabled(false);	
						}
					}
					*/
				}
    		}
    	});
    	//@@20130718 김도원 END
    	
    	var buttons = null;
    	if( me.saveButton ){	// saveButton true일 경우 버튼 보여주기
    		buttons = [];
    		buttons.push({
    			text: getConstText({m_key : "SAVE"}),
	 			handler: Ext.Function.bind(this.saveAsFile, me)	
    		})
    	}
    	
    	// Creates the GridPanel
    	this.listFilesPanel = Ext.create('nkia.custom.grid', {
    		style: 'border-top-width: 1px; border-top-color: #b8c8d9; border-top-style: solid; border-left-width: 1px; border-left-color: #b8c8d9; border-left-style: solid; border-right-width: 1px; border-right-color: #b8c8d9; border-right-style: solid;',
    		margin: "0 0 0 0",
    		padding: "0 0 0 0",
    		store: this.listFilesStore,
    	    columns: [{
    	    	dataIndex: 'ORIGNL_FILE_NAME',
    	    	menuDisabled: true,
    	    	flex: 1
    	    }], 
    	    defaults: {
                flex: 1
            }, 
    	  	height: this.listFilesHeight,
    	    //buttons: buttons,
	        listeners: {
				render : function(grid){
                  	grid.store.on('load', function(store, records, options){
         				if( store.data.length > 0 ){
							// data exits      
				    		me.listFilesPanel.getView().on( 'celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts){
		                  		if( cellIndex == 0 ){
		                  			var atchFileParams = {
		                  				atch_file_id : record.get('ATCH_FILE_ID')
		                  			  , file_idx: record.get('FILE_IDX')
		                  			}
		                  			
		                  			me.downloadFile(record, atchFileParams);
		                  		}
		                  	});
		                  	
		                  	me.atch_file_id = store.data.items[0].raw.ATCH_FILE_ID;
				    	}else{
				    		// data not exits
				    		me.listFilesStore.add(me.defaultMessage);
				    	} 
                  	});
                },
                afterrender: function(){
                	me.listFilesPanel.headerCt.getEl().setVisible( false );
                }
	        }
    	});
    }
});