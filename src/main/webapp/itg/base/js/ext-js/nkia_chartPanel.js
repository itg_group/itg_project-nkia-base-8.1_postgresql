/**
 * 
 * @file nkia_chartPanel
 * @description NKIA 퓨전차트 패널
 * @author ITG
 * @copyright NKIA 2013
 */

/**
 * @function
 * @summary ChartPanel 생성 부분
 * @param {json} setChartPanelProp
 * @property {string} id ChartPanel ID (필수)
 * @property {string} title ChartPanel Title
 * @property {json} fusionChartProp 차트를 미리 그려두지 않기위해 차트 Property를 넣어줍니다. (내부에서 생성)
 * @property {string} icon ChartPanel Icon
 * @property {object} listeners ChartPanel Listeners
 * @property {object} items ChartPanel Items (필수)
 * @property {int} columnWidth Chart Width값
 * @property {int} height Chart Height값 (필수)
 * @property {string} baseCls Chart Class값 
 * @property {string} bodyStyle Chart bodyStyle값
 * @author 정중훈
 * @returns {object} chart panel
 */
function createChartPanel(setChartPanelProp){
	var that = {};
	var chartProp = {};
	var chartItems = new Array();
	var chartPanel_comp;
	for( var prop in setChartPanelProp ){
		that[prop] = setChartPanelProp[prop];
	}
	
	// 패널의 높이값을 기본적으로 400을 줍니다.
	var panelHeight = (that["panelHeight"])? that["panelHeight"] : getConstValue('DEFAULT_CHART_PANEL_HEIGHT'); 
	
	for( var prop in that["fusionChartProp"] ){
		chartProp[prop] = that["fusionChartProp"][prop];
	}
	
	//@@정중훈 2013.07.24 차트를 미리 로드 시키지 않기위함 
	//@@정중훈 2014.02.04 차트 리스너 추가 
	var basicListeners = {
			afterlayout :  function(p){
				if( p.chartObj.resize != null ){
					var location = p.fusionChartProp.location;
					var chartDiv = Ext.get(location);
					
  	    		  	var cWidth = chartDiv.dom.clientWidth;
  	    		  	var cHeight = chartDiv.dom.clientHeight;
  	    		  	
					p.chartObj.resize(location, cWidth, cHeight);
				}
			},afterrender: function(p){
				p.chartObj = createFusionChart(p.fusionChartProp);
				p.chartObj.panel = p;
			}
		};
	
	var listeners = (that['listeners']) ? that['listeners'] : basicListeners;
	
	chartPanel_comp = Ext.create('nkia.custom.Panel', {
		id : that['id'] ,
		title : (that['title'] != "")? that['title'] : that['id'],
		height : panelHeight,
		minHeight :  ( that['minPanelHeight'] ) ? that['minPanelHeight'] : panelHeight,
		tools: that["tools"],
		tooltip: that['tooltip'],
		fusionChartProp : that['fusionChartProp'],
		icon: that['icon'] ? that['icon'] : getConstValue('CONTEXT')+'/itg/base/images/ext-js/common/icons/bullet_chart.gif',
		html: '<div id="'+ chartProp["location"] +'" style="width : 100%; " align="center" > </div>',
		listeners : listeners,
		chartResize: function( location, width, height ){
			this.chartObj.resize( location, width, height );
		},
		chartLoad: function( prop ){
			if( that["loadMask"] == null && that["loadMask"] != false ){
				this.showMask();
			}
			
			var isLoaded = this.chartObj.load( prop );
		},
		chartReload: function( location ){
			this.chartObj.reload( location );
		},
		showMask: function(){
			chartMask.show();
		},
		hideMask: function(){
			chartMask.hide();
		}
	});
	
	var chartMask = new Ext.LoadMask(chartPanel_comp, {msg:getConstText({ isArgs: true, m_key: 'msg.mask.common' })});
	
	return chartPanel_comp;
}
