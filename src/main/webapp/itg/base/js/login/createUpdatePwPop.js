var popUp_comp={};
var result = null;

//패스워드 수정 팝업 띄우기
function createUpdatePwPopup(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var data = that["paramMap"];
	if(null == popUp_comp[that['id']]){
		
		var updateBtn = createBtnComp({visible: true, label: '수정', id:'updateBtn', ui : 'correct', scale : 'medium'});
		var closeBtn = null;
		if(that["forceChangeYn"] !== "Y") {
			closeBtn = createBtnComp({visible: true, label: '닫기', id:'closeBtn', scale : 'medium'});
		}
		
		//패스워드 입력방법 텍스트
		var passInputText = {
				xtype: 'component',
				padding : '0 0 5 0',
				html: '<span style="color: blue; font-weight: bold;"><b>'+getConstText({ isArgs: true, m_key: 'msg.user.reg.00006'})+'</b></span>' 
		};
		
		var setEditProp = {
				id: 'updatePwEditForm',
				title: '비밀번호변경',
				editOnly: true,
				border: true,
				buttonAlign : "center",
				tableProps :[{colspan:2, tdHeight: 30, 
								item : createTextFieldComp({label : '변경전 비밀번호', labelAlign : 'left', labelWidth : 150, id : 'oldPw', name:'oldPw',  inputType: 'password', notNull:true,isNotColspan: true })
							},{colspan:2, tdHeight: 30, 
								item : createTextFieldComp({label : '새로운 비밀번호', labelAlign : 'left', labelWidth : 150, id : 'newPw', name:'newPw', inputType: 'password', vtype: 'password', notNull:true,isNotColspan: true})
							},{colspan:2, tdHeight: 30, 
								item : createTextFieldComp({label : '새로운 비밀번호 확인', labelAlign : 'left', labelWidth : 150, id : 'checkNewPw', name:'checkNewPw',inputType: 'password', vtype: 'password', notNull:true,isNotColspan: true})
							},{colspan:2, tdHeight: 30,
								item : passInputText
							}],
				hiddenFields:[ {name: 'user_id' , value : data.user_id}
								,{name: 'upd_dt' , value : data.UPD_DT}]
			}
		var updatePwEditForm = createEditorFormComp(setEditProp);
		
		var popupBtns = [updateBtn];
		if(that["forceChangeYn"] !== "Y") {
			popupBtns.push(closeBtn);
		}
		
		updatePwPopup = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: '사용자 비밀번호 변경 팝업',  
			width: 460,
			autoDestroy: false,
			resizable:false,
			closable: false,
			buttonAlign: that['buttonAlign'],
			buttons: popupBtns,
			bodyStyle: that['bodyStyle'],
			items: [updatePwEditForm],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		attachBtnEvent(updateBtn, userAgreeActionLink,  'update');
		if(that["forceChangeYn"] !== "Y") {
			attachBtnEvent(closeBtn, userAgreeActionLink,  'close');
		}
		popUp_comp[that['id']] = updatePwPopup;
		
	}else {
		
		// 데이터 새로 조회합니다.
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				var newResult = data.resultMap;
				theForm.setDataForMap(newResult);
			}
		});
		
		updatePwPopup = popUp_comp[that['id']];
	}
	
	formEditable({id : 'updatePwEditForm', editorFlag : true, excepArray :['']});
	
	
	return updatePwPopup;
}

function userAgreeActionLink(command) {
	
	var theForm = Ext.getCmp('updatePwEditForm');
	var paramMap = theForm.getInputData();
	
	switch(command) {
		
		case 'update' :
			
			var oldPassWord = theForm.getFieldValue('oldPw');
			var passWord = theForm.getFieldValue('newPw');
            var passWordCheck = theForm.getFieldValue('checkNewPw');
			
			if(oldPassWord == "") {
				alert("현재 비밀번호를 입력하세요");
				Ext.getCmp('oldPw').focus(true,10);
				return false;
				
			} else if(passWord == "") {
				alert("변경하실 비밀번호를 입력하세요");
				Ext.getCmp('newPw').focus(true,10);
				return false;
				
			} else if(passWordCheck == "") {
				alert("변경하실 비밀번호 확인을 입력하세요");
				Ext.getCmp('checkNewPw').focus(true,10);
				return false;
			} 
			
			var pwResult = checkPwResultTempPw(paramMap['oldPw'], paramMap['newPw'], paramMap['checkNewPw']);
			if(!pwResult) {
				alert("암호는 9~12자리 이내로 영문과 숫자 그리고 특수문자를 조합하며 연속된문자 및 연속된 키보드배열을 제외하고 입력해 주세요");
				return false;
			}
			
			if(passWord != passWordCheck) {
				alert("변경 비밀번호가 일치하지 않습니다");
				Ext.getCmp('newPw').focus(true,10);
				return false;
			}
			
			if(!theForm.isValid()) {
				alert("필수 입력 항목을 확인 바랍니다.");
				return false;
			} else {
				 RsaKeyGenDwr.generatorKey(function(data){
					 
					var rsa = new RSAKey();
				    rsa.setPublic(data.KeyModulus,  data.KeyExponent);
				    
				    paramMap['old_change_pw'] = rsa.encrypt(paramMap['oldPw']);
					paramMap['change_pw'] = rsa.encrypt(paramMap['newPw']);
					paramMap['change_pw_check'] = rsa.encrypt(paramMap['checkNewPw']);
					
					jq.ajax({ type:"POST"
						, url:'/itg/certification/updatePwPopupChange.do'
						, contentType: "application/json"
						, dataType: "json"
						, async: false
						, data : getArrayToJson(paramMap)
						, success:function(data){
							alert(data.resultMsg);
							if(data.resultBoolean){
								theForm.initData();
								updatePwPopup.doClose();
							}
						}
					});
				 });
			}
	
		break;
		
		case 'close' :
			
			// 체크박스 체크 
			if(paramMap.popupYn){
				
				paramMap["popup_show_yn"] = "N";
				
				jq.ajax({ type:"POST"
					, url:'/itg/certification/updateOneDayPwPopupShow.do'
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(paramMap)
					, success:function(data){
						theForm.initData();
						updatePwPopup.doClose();
					}
				});
				
			}else{
				theForm.initData();
				updatePwPopup.doClose();
			}
			
		break;
	}
	
}

function checkPwResultTempPw(cur_pw, chg_pw, chg_pw_chk) {
	var valid = false;
	var pw_arr = new Array();
	//pw_arr.push(cur_pw);
	pw_arr.push(chg_pw);
	pw_arr.push(chg_pw_chk);
	
	var passWordCheckVal = /^(?=.*[a-zA-Z])(?=.*[~!@#$%^&*()+=-])(?=.*[0-9]).*$/;
	
	for(var i=0; i<pw_arr.length; i++) {
		if(passWordCheckVal.test(pw_arr[i])){
			// 문자열 길이 체크 로직
			var str = pw_arr[i];
			var strLength = str.length;
			if(9 <= strLength){
				if(strLength <= 12){
					valid = true;
				} else {
					valid = false;
					break;
				}
			} else {
				valid = false;
				break;
			}
		} else {
			valid = false;
			break;
		}
	}
	if(valid){
		if(!checkSequenceKey(chg_pw)){
			valid = false;
		}
	}  
	return valid;
}
function checkSequenceKey(checkStr){
	 var checkLength = 3;
	 var str = 'qwertyuiopasdfghjklzxcvbn1234567890';
	  for(var i in str){
	      var s = '';
	      var at = parseInt(i);
	      var j = at;
	      while(j < (at + checkLength) && j < str.length){
	        s += str[j];
	        j++;
	      }
	      if(s.length > 2){
	    	  var regex = new RegExp(s);
	    	  if(regex.test(checkStr)){
	    		  return false;
	    	  }
	      }
	  }
	  
	  return true;
	}
