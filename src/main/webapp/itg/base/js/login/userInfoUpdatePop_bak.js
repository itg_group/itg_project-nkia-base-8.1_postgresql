var popUp_comp={};
var result = null;
//@@20130711 전민수 START
//createOutUserSelectPopComp 추가 : 대무자 
//createSearchDateFieldComp  추가 : 대무기간
//userInfoUpdateDetail       변경 : 부서명(cust_nm) colspan :1 -> colspan: 2 로 변경

//개인정보 수정 팝업 생성
function createUserInfoUpdatePopup(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var url = that["url"];
	var paramMap = that["paramMap"];
	
	if(null == popUp_comp[that['id']]){
	
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				result = data.resultMap;
			}
		});
		
		// 버튼 묶음
		var userInfoUpdateBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.modify' }), id:'userInfoUpdateBtn', ui:'correct', scale:'medium'});
		var userInfoCancelBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.cancel' }), id:'userInfoCancelBtn', scale:'medium'});

		//패스워드 입력방법 텍스트
		var passInputText = {
				xtype: 'component',
				padding : '0 0 10 105',
				html: '<span style="color: blue; font-weight: bold;"><b>'+getConstText({ isArgs: true, m_key: 'msg.user.reg.00006'})+'</b></span>' 
		};
		
		
		var userInfoUpdateDetail = {
				id: 'userInfoUpdateDetail',
				title : getConstText({ isArgs: true, m_key: 'res.title.login.userInfoUpdateDetail' }),
				columnSize : 2,
				editOnly : true,
				border: true,
				tableProps :[{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00001'}), name:'cust_nm', id:'cust_nm', width: 320, notNull:true})
				}, {
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00009'}), name:'dept_nm', id:'dept_nm', notNull : true, maxLength: 100, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00001'})})
				}, {
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00002'}), name:'user_nm', id:'user_nm', notNull : true, maxLength: 100})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00003'}), name:'user_id', id:'user_id', notNull : true, maxLength: 40})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00004'}), name:'pw', id :'pw' , inputType: 'password', maxLength: 30, notNull: true, vtype: 'password'})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00005'}), name:'pwCheck', id:'pwCheck' , inputType: 'password', maxLength: 30, notNull: true, vtype: 'password'})
				},{
					colspan:2, tdHeight: 30, 
					item : passInputText
				},{
					colspan : 1, tdHeight : 30,
					//item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00006'}), name:'position' , id:'position', maxLength: 100, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00003'})})
					item : createClassPopupComp({label: getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00006'}), targetName:'position_nm', targetHiddenName:'position', notNull:true })
				},{
					colspan : 1, tdHeight : 30,
					item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00007'}), name:'tel_no', id:'tel_no', notNull: true, firstNumberNotNull: false})
				},{
					colspan : 1, tdHeight : 30,
					item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00008'}), name:'hp_no', id:'hp_no', notNull: true, firstNumberNotNull: true, firstFieldLength:3})
				},{
					colspan : 1, tdHeight : 30,
					item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00009'}), name:'fax_no', id:'fax_no'})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00010'}), name:'email', id:'email', maxLength: 40, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00004'})})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00011'}), name:'use_yn', id:'use_yn'})
				},{
					colspan : 1, tdHeight : 30,
					item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00012'}), name:'cust_id', id:'cust_id'})
				},{
					colspan:1, tdHeight: 30, 
					item : createOutUserSelectPopComp({id:'userSelectPop',  popUpText: getConstText({ isArgs: true, m_key: 'res.title.system.user.tempuser'}), label:getConstText({ isArgs: true, m_key: 'res.common.label.tempusernm'}), name:'absent_nm', hiddenName:'absent_id', targetForm:'setEditorFormProp', gridId:'userSelectGrid', userId:'user_id', btnId: 'absent_nmBtn'})
				},{ 
					colspan:1, tdHeight: 30, 
					item : createSearchDateFieldComp({label:getConstText({ isArgs: true, m_key: 'res.common.label.tempuserdate'}), id : 'absent' , name:'absent', dateType : 'D', format: 'Y-m-d'})
				}]
			};
		
		var userInfoUpdateForm = createEditorFormComp(userInfoUpdateDetail);
		
		// 숨길 필드들 
		Ext.getCmp('use_yn').hide();
		Ext.getCmp('cust_id').hide();
		
		//필드값 setValue
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		theForm.setDataForMap(result);
		//필드값 setValue
		
		// 취소버튼을 눌렀을 경우
		attachBtnEvent(userInfoCancelBtn, actionLink,  'cancel');
		// 수정버튼을 눌렀을 경우
		attachBtnEvent(userInfoUpdateBtn, actionLink,  'update');
		
		userInfoUpdatePopup = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: getConstText({ isArgs: true, m_key: 'res.title.login.userInfoUpdatePopup' }),  
			width: 750,  
			autoDestroy: false,
			resizable:false, 
			//bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [userInfoUpdateBtn, userInfoCancelBtn],
			bodyStyle: that['bodyStyle'],
			items: [userInfoUpdateForm, userInfoCancelBtn],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = userInfoUpdatePopup;
		
	}else {
		// 데이터 새로 조회합니다.
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				var newResult = data.resultMap;
				theForm.setDataForMap(newResult);
			}
		});
		
		userInfoUpdatePopup = popUp_comp[that['id']];
	}
	
	userInfoUpdatePopup.on("show", function(){
		formEditable({id : 'userInfoUpdateDetail', editorFlag : true, excepArray :['']});
	});
	Ext.getCmp('cust_nm').setReadOnly(true);
	Ext.getCmp('user_nm').setReadOnly(true);
	Ext.getCmp('user_id').setReadOnly(true);
	 				
	return userInfoUpdatePopup;
}
//@@20130711 전민수 
//END

function actionLink(command) {
	var theForm = Ext.getCmp('userInfoUpdateDetail');
	
	switch(command) {
		
		case 'update' :
			
			if(theForm.isValid()){
				var paramMap = theForm.getInputData();
				var insertNameValue = theForm.getInputData();
				
				//암호화 시작
	            var passWord = "";
	            var passWordCheck = "";
	            
	            passWord = Ext.getCmp('userInfoUpdateDetail').getFieldValue('pw');
	            passWordCheck = Ext.getCmp('userInfoUpdateDetail').getFieldValue('pwCheck');
	            
	            if(passWord != passWordCheck){
	            	showMessage(getConstText({ isArgs: true, m_key: 'msg.system.result.00029' }));
					return false;
	            }
	            
				paramMap["insertNameValue"] = insertNameValue;
				
				jq.ajax({ type:"POST"
					, url:'/itg/certification/updateUserInfo.do'
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(paramMap)
					, success:function(data){
						result = data.resultMap;
						theForm.setDataForMap(result);
						showMessage(data.resultMsg);
						userInfoUpdatePopup.hide();
					}
				});
				
			}else{
				showMessage(getConstText({ isArgs: true, m_key: 'msg.common.00011' }));
			}
			
		break;
		
		case 'cancel' :
			
			theForm.initData();
			theForm.setDataForMap(result);
			userInfoUpdatePopup.hide();
			
		break;
	}
	
}

