var popUp_comp={};
var result = null;
var isFirstOpen = false;
//@@20130711 전민수 START
//createOutUserSelectPopComp 추가 : 대무자 
//createSearchDateFieldComp  추가 : 대무기간
//userInfoUpdateDetail       변경 : 부서명(cust_nm) colspan :1 -> colspan: 2 로 변경

//개인정보 수정 팝업 생성
function createUserInfoUpdatePopup(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var url = that["url"];
	var paramMap = that["paramMap"];
	
	if(null == popUp_comp[that['id']]){
		
		isFirstOpen = true;
		
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				result = data.resultMap;
			}
		});
		
		// 버튼 묶음
		var userInfoUpdateBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.modify' }), id:'userInfoUpdateBtn', ui:'correct', scale:'medium'});
		var userInfoCancelBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.cancel' }), id:'userInfoCancelBtn', scale:'medium'});
		
		//패스워드 입력방법 텍스트
		var passInputText = {
			xtype: 'component',
			height: 20,
			padding : '0 0 10 105',
			html: '<span style="color: blue; font-weight: bold;" id="passInputTxt"><b>'+getConstText({ isArgs: true, m_key: 'msg.user.reg.00006'})+'</b></span>',
			listeners: {
				afterrender : function() {
					jq("#passInputTxt").hide();
				}
			}
		};
		
		// 수정 에디터 폼
		var userInfoUpdateDetail = {
			id: 'userInfoUpdateDetail',
			title : getConstText({ isArgs: true, m_key: 'res.title.login.userInfoUpdateDetail' }),
			columnSize : 2,
			editOnly : true,
			border: true,
			tableProps :[
		      {colspan: 2, item: createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00001'}), name:'cust_nm', id:'cust_nm'})},
		      {colspan: 1, item: createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00002'}), name:'user_nm', id:'user_nm', maxLength: 100})},
		      {colspan: 1, item: createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00003'}), name:'user_id', id:'user_id', maxLength: 40})},
		      {colspan: 1, item: createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00006'}), name:'position', notNull:true, maxLength : FIELD_DOMAIN.NM_50})},
		      {colspan: 1, item: createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00010'}), name:'email', id:'email', notNull: true, maxLength: 40, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00004'})})},
		      {colspan: 1, item: createTextFieldComp({label : getConstText({ isArgs: true, m_key: 'res.common.label.tel' }), name:'tel_no', id:'tel_no'})},
		      {colspan: 1, item: createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00008'}), name:'hp_no', id:'hp_no', notNull: true, firstNumberNotNull: true, firstFieldLength:3})},
		      {colspan: 1, item: createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00012'}), name:'cust_id', id:'cust_id'})},
		      //{colspan: 1, item: createOutUserSelectPopComp({id:'userSelectPop',  popUpText: getConstText({ isArgs: true, m_key: 'res.title.system.user.tempuser'}), label:getConstText({ isArgs: true, m_key: 'res.common.label.tempusernm'}), name:'absent_nm', hiddenName:'absent_id', targetForm:'setEditorFormProp', gridId:'userSelectGrid', userId:'user_id', btnId: 'absent_nmBtn', isCustId:true, thatEditFormId:'userInfoUpdateDetail'})},
		      //{colspan: 2, item: createFromToDateContainer({label:getConstText({ isArgs: true, m_key: 'res.common.label.tempuserdate'}), name:'absent', dateType : 'D', format: 'Y-m-d'})},
		      {colspan: 1, item: createSingleCheckBoxFieldComp({fieldLabel: getConstText({ isArgs: true, m_key: 'res.common.label.ispasswordchange'}), id:'is_password_change', name:'is_password_change', checked : false, inputValue : 'Y', defaultValue: 'N'})},
		      {colspan: 1, item: createSingleCheckBoxFieldComp({fieldLabel: "그룹 수신 여부", id:'alarm_yn', name:'alarm_yn', checked : false, inputValue : 'Y', defaultValue: 'N'})},
		      {colspan: 2, item: createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.common.label.currentpassword'}), name:'current_pw', id :'current_pw' , inputType: 'password', notNull: false, maxLength: 30, isNotColspan: true})},
		      {colspan: 1, item: createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.common.label.changepassword'}), name:'change_pw', id :'change_pw' , inputType: 'password', notNull: false, vtype: 'password', maxLength: 30})},
		      {colspan: 1, item: createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.common.label.changerepassword'}), name:'change_pw_check', id:'change_pw_check' , inputType: 'password', notNull: false, vtype: 'password', maxLength: 30})},
		      {colspan: 2, item: passInputText}
		    ]
		};
		
		var userInfoUpdateForm = createEditorFormComp(userInfoUpdateDetail);
		
		// 숨길 필드들 
		Ext.getCmp('cust_id').hide();
		
		//필드값 setValue
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		theForm.setDataForMap(result);
		//필드값 setValue
		
		// 취소버튼을 눌렀을 경우
		attachBtnEvent(userInfoCancelBtn, actionLink,  'cancel');
		// 수정버튼을 눌렀을 경우
		attachBtnEvent(userInfoUpdateBtn, actionLink,  'update');
		
		userInfoUpdatePopup = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: getConstText({ isArgs: true, m_key: 'res.title.login.userInfoUpdatePopup' }),  
			width: 750,
			autoDestroy: false,
			resizable:false, 
			//bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [userInfoUpdateBtn, userInfoCancelBtn],
			bodyStyle: that['bodyStyle'],
			items: [userInfoUpdateForm, userInfoCancelBtn],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null;
	 			},
	 			hide:function(p) {
	 			},
	 			afterrender: function(){
	 				// ReadOnly 컬럼
	 				Ext.getCmp('cust_nm').setReadOnly(true);
	 				Ext.getCmp('user_nm').setReadOnly(true);
	 				Ext.getCmp('user_id').setReadOnly(true);
	 				
	 				formEditable({id : 'userInfoUpdateDetail', editorFlag : true, excepArray :['']});
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = userInfoUpdatePopup;
		
	}else {
		
		isFirstOpen = false;
		
		// 데이터 새로 조회합니다.
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				theForm.initData();
				var newResult = data.resultMap;
				theForm.setDataForMap(newResult);
			}
		});
		
		userInfoUpdatePopup = popUp_comp[that['id']];
	}
	
	// 히든 필드에 이전 비밀번호 세팅(확인용) - 2014.08.02. 정정윤
	//Ext.getCmp('current_pw_check').setValue(Ext.getCmp('change_pw_check').getValue());
	
	//첫 로드시 비밀번호 관련 컴포넌트들 사용불가로.. - 2014.08.02. 정정윤
	setPwObjsDisabled(true);
	
	if(isFirstOpen) {
		//비밀번호 변경 체크박스 클릭 이벤트 - 2014.08.02. 정정윤
		attachCustomEvent('change', Ext.getCmp("userInfoUpdateDetail").getFormField('is_password_change'), isPasswordChange);
	}
	
	// 전화번호필드가 2개로 구분된다면 군전화로 인식함
	var firstHomeTelNo = Ext.getCmp("tel_no");
	var formData = Ext.getCmp('userInfoUpdateDetail').getInputData();
	var telNoArr = formData['tel_no'].split("-");
	
	return userInfoUpdatePopup;
}
//@@20130711 전민수 
//END

function isPasswordChange( checkbox, newValue, oldValue, eOpts ){
	var checked = checkbox.getValue();
	if(checked) {
		setPwObjsDisabled(false);
		jq("#passInputTxt").show();
	} else {
		setPwObjsDisabled(true);
		jq("#passInputTxt").hide();
	}
}

function actionLink(command) {
	var theForm = Ext.getCmp('userInfoUpdateDetail');
	
	switch(command) {
		case 'update' :
			var paramMap = theForm.getInputData();
			
			if(paramMap['is_password_change'] == "Y") {
				if(paramMap['current_pw'] == "") {
					alert("현재 비밀번호를 입력하세요");
					Ext.getCmp("current_pw").focus(true,10);
					return false;
					
				} else if(paramMap['change_pw'] == "") {
					alert("변경하실 비밀번호를 입력하세요");
					Ext.getCmp("change_pw").focus(true,10);
					return false;
					
				} else if(paramMap['change_pw_check'] == "") {
					alert("변경하실 비밀번호 확인을 입력하세요");
					Ext.getCmp("change_pw_check").focus(true,10);
					return false;
				}
				var pwResult = checkPwResultUpdate(paramMap['current_pw'], paramMap['change_pw'], paramMap['change_pw_check']);
				if(!pwResult) {
					alert("암호는 9~12자리 이내로 영문과 숫자 그리고 특수문자를 조합하며 연속된문자 및 연속된 키보드배열을 제외하고 입력해 주세요");
					return false;
				}
			}
			
			if(theForm.isValid()){
				//비밀번호 변경 체크박스를 눌렀을 때만 비밀번호 검사 로직 수행 - 2014.08.02. 정정윤
				if(paramMap['is_password_change'] != "Y") {
					paramMap['change_pw'] = "";
					paramMap['change_pw_check'] = "";
					// 서버단에서 pw가 비어있으면 패스워드를 업데이트 하지 않는 로직 추가함 - 2014.09.22. 정정윤
				}
				
				// 전화번호 제일 앞에 대쉬(-)가 붙엇을 경우 제거하여 parameter 세팅 - 2014.10.05. 정정윤
				if(paramMap['tel_no'].substr(0,1) == "-") {
					paramMap['tel_no'] = paramMap['tel_no'].replace("-","");
				}
				
				if(!confirm("수정하시겠습니까?")) {
					return false;
				}
				
				RsaKeyGenDwr.generatorKey(function(data){
					var rsa = new RSAKey();
				    rsa.setPublic(data.KeyModulus,  data.KeyExponent);
				    
				     // 비밀번호를 RSA로 암호화한다.
				    paramMap["current_pw"] = rsa.encrypt(theForm.getFieldValue('current_pw'));
				    paramMap["change_pw"] = rsa.encrypt(theForm.getFieldValue('change_pw'));
				    paramMap["change_pw_check"] = rsa.encrypt(theForm.getFieldValue('change_pw_check'));
					
				    jq.ajax({ type:"POST"
						, url:'/itg/certification/updateUserInfo.do'
						, contentType: "application/json"
						, dataType: "json"
						, async: false
						, data : getArrayToJson(paramMap)
						, success:function(data){
							if(data.success){
								if(data.resultMap.IS_VALID == "false") {
			    					alert(data.resultMsg);
			    					setViewPortMaskFalse();
			    					return false;
			    				}
								alert(data.resultMsg);
								userInfoUpdatePopup.hide();
							}else{
								alert(data.resultMsg);
							}
						}
					});
				});
			}else{
				alert(getConstText({ isArgs: true, m_key: 'msg.common.00011' }));
			}
		break;
		
		case 'cancel' :
			
			//theForm.initData();
			//theForm.setDataForMap(result);
			//userInfoUpdatePopup.hide();
			userInfoUpdatePopup.doClose();
			
		break;
		
		case 'pwInit' :
			
			if(!confirm("확인버튼을 누르면 지금 즉시 비밀번호가 초기화 됩니다.\n계속 하시겠습니까?")) {
				return false;
			}
			
			var inputMap = theForm.getInputData();
			var paramMap = {};
			
			var user_id_sub_str = inputMap['user_id'].substr(0,4);
			var hp_no_sub_str = inputMap['hp_no_third'];
			
			paramMap['user_id'] = inputMap['user_id'];
			paramMap['initPw'] = ( user_id_sub_str + hp_no_sub_str + "!" );
			paramMap['email'] = inputMap['email'];
			
			jq.ajax({ type:"POST"
				, url:'/itg/certification/updatePwInit.do'
				, contentType: "application/json"
				, dataType: "json"
				, async: false
				, data : getArrayToJson(paramMap)
				, success:function(data){
					var alertString = "비밀번호가 초기화 되었습니다."
						+ "\n"
						+ "\n초기화 된 비밀번호는"
						+ "\n[ ID 앞4자리 + 핸드폰 뒤4자리 + !(느낌표) ] 입니다."
						//+ "\n"
						//+ "\nex) nkia7274!"
						;
					alert(alertString);
				}
			});
			
		break;
	}
	
}

// 비밀번호 변경 관련 오브젝트 사용가능/불가능 핸들링 - 2014.08.02. 정정윤
function setPwObjsDisabled(flag) {
	
	//Ext.getCmp('is_password_change').setValue('N');
	
	Ext.getCmp('current_pw').setValue('');
	Ext.getCmp('change_pw').setValue('');
	Ext.getCmp('change_pw_check').setValue('');
	
	Ext.getCmp('current_pw').setDisabled(flag);
	Ext.getCmp('change_pw').setDisabled(flag);
	Ext.getCmp('change_pw_check').setDisabled(flag);
}

function checkPwResultUpdate(current_pw, chg_pw, chg_pw_chk) {
	
	var valid = false;
	var pw_arr = new Array();
	//pw_arr.push(current_pw);
	pw_arr.push(chg_pw);
	pw_arr.push(chg_pw_chk);
	
	var passWordCheckVal = /^(?=.*[a-zA-Z])(?=.*[~!@#$%^&*()+=-])(?=.*[0-9]).*$/;
	
	for(var i=0; i<pw_arr.length; i++) {
		if(passWordCheckVal.test(pw_arr[i])){
			// 문자열 길이 체크 로직
			var str = pw_arr[i];
			var strLength = str.length;
			if(9 <= strLength){
				if(strLength <= 12){
					valid = true;
				} else {
					valid = false;
					break;
				}
			} else {
				valid = false;
				break;
			}
		} else {
			valid = false;
			break;
		}
	}
	if(valid){
		if(!checkSequenceKeyU(chg_pw)){
			valid = false;
		}
	}  
	return valid;
}
function checkSequenceKeyU(checkStr){
	 var checkLength = 3;
	 var str = 'qwertyuiopasdfghjklzxcvbn1234567890';
	  for(var i in str){
	      var s = '';
	      var at = parseInt(i);
	      var j = at;
	      while(j < (at + checkLength) && j < str.length){
	        s += str[j];
	        j++;
	      }
	      if(s.length > 2){
	    	  var regex = new RegExp(s);
	    	  if(regex.test(checkStr)){
	    		  return false;
	    	  }
	      }
	  }
	  return true;
	}