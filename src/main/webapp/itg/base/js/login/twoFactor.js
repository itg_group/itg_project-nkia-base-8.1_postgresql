var popUp_comp={};
var result = null;
var smsSw = false;


function createTwoFactorPopup(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	smsSw = false;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var data = that["paramMap"];
	
	var updateBtn = createBtnComp({visible: true, label: '인증', id:'updateBtn', ui : 'correct', scale : 'medium'});
	var smsBtn = createBtnComp({visible: true, label: '인증번호발송', id:'smsBtn', scale : 'medium',width : 100,isNotColspan: true})
	var closeBtn = null;
	closeBtn = createBtnComp({visible: true, label: '닫기', id:'closeBtn', scale : 'medium'});
	var enterFunction = function(){twoFactorActionLink('update')}
	var setEditProp = {
			id: 'twoFactorForm',
			title: '인증',
			editOnly: true,
			border: true,
			buttonAlign : "center",
			enterFunction : enterFunction,
			tableProps :[{colspan:1, tdHeight: 30, 
							item : createTextFieldComp({label : '인증번호', labelAlign : 'left', labelWidth : 150, id : 'cerPw', name:'cerPw',  notNull:true,isNotColspan: true,on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);} } })
						},{colspan:1, tdHeight: 30, 
							item : smsBtn	
						}],
			hiddenFields:[ {name: 'user_id' , value : data.user_id}
							,{name: 'upd_dt' , value : data.UPD_DT}]
		}
	var twoFactorForm = createEditorFormComp(setEditProp);
	
	var popupBtns = [updateBtn];
	popupBtns.push(closeBtn);
	
	twoFactorPopup = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		autoDestroy: false,
		resizable:false,
		closable: false,
		buttonAlign: that['buttonAlign'],
		buttons: popupBtns,
		bodyStyle: that['bodyStyle'],
		items: [twoFactorForm],
		listeners: {
			destroy:function(p) {
				popUp_comp[that['id']] = null
 			},
 			hide:function(p) {
 			}
 		}
	});
	
	attachBtnEvent(updateBtn, twoFactorActionLink,  'update');
	attachBtnEvent(closeBtn, twoFactorActionLink,  'close');
	attachBtnEvent(smsBtn, twoFactorActionLink,  'sms');
	popUp_comp[that['id']] = twoFactorPopup;
		
	
	
	return twoFactorPopup;
}

function twoFactorActionLink(command) {
	
	var theForm = Ext.getCmp('twoFactorForm');
	
	switch(command) {
		
		case 'update' :
			var cerPassWord = theForm.getFieldValue('cerPw');
			if (!smsSw){
				alert("인증번호 발급 후 진행하세요");
				return false;
			} else if (cerPassWord == ""){
				alert("인증번호를 입력하세요");
				return false;
			}
			if(!theForm.isValid()) {
				alert("필수 입력 항목을 확인 바랍니다.");
				return false;
			} else {
				paramMap["cerPw"] = cerPassWord;
				paramMap["user_id"] = this.paramMap.userId;
				jq.ajax({ type:"POST"
					, url:'/itg/certification/inspectionTF.do'
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(paramMap)
					, success:function(data){
						if(data.resultBoolean){
							alert(data.resultMsg);
							if(paramMap.type == "system"){
								twoFactorPopup.doClose();
								parent.changeTopMenu(paramMap.topMenuId,false,true);
							}else if(paramMap.type == "passwd"){
								twoFactorPopup.doClose();
								parent.pwUpdateRequest(paramMap.userId);
							}else if(paramMap.type == "privacy"){
								twoFactorPopup.doClose();
								parent.userInfoUpdate(paramMap.userId);
							}else{
								twoFactorPopup.doClose();
							}
						}else{
							alert(data.resultMsg);
						}
					}
				});
				 
			}
	
		break;
		
		case 'close' :
			// 체크박스 체크 
			if(this.paramMap.type == "system" || this.paramMap.type == "privacy" ){
				twoFactorPopup.doClose();
			}
			if(this.paramMap.type == "passwd"){
				twoFactorPopup.doClose();
				parent.logout();
			}
		break;
		
		case 'sms' : //인증번호 발송
			params = {}
			params["user_id"] = this.paramMap.userId;
			params["type"] = this.paramMap.type;
			if(!smsSw){
				jq.ajax({ type:"POST"
					, url:'/itg/base/pwChange.do'
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(params)
					, success:function(data){
						smsSw = true;
						alert(data.resultMsg);
					}
				});
			}
	}
	
}


