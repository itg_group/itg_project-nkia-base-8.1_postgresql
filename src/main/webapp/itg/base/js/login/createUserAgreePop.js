var popUp_comp={};
var result = null;

//패스워드 수정 팝업 띄우기
function createUserAgreePopup(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var data = that["paramMap"];
	
	if(null == popUp_comp[that['id']]){
		
		var updateBtn = createBtnComp({visible: true, label: '완료', id:'updateBtn', ui : 'correct', scale : 'medium'});
		var closeBtn = null;
		if(that["forceChangeYn"] !== "Y") {
			closeBtn = createBtnComp({visible: true, label: '닫기', id:'closeBtn', scale : 'medium'});
		}
		
		//패스워드 입력방법 텍스트
		var passInputText = {
				xtype: 'component',
				padding : '0 0 5 0',
				html: '<span style="color: blue; font-weight: bold;"><b>'+getConstText({ isArgs: true, m_key: 'msg.user.reg.00006'})+'</b></span>' 
		};
		
		var setEditProp = {
				id: 'updatePwEditForm',
				title: '사용자정보동의서',
				columnSize: 2,
				editOnly: true,
				border: true,
				buttonAlign : "center",
				tableProps :[{colspan:2, tdHeight: 30, 
								item : createTextFieldComp({label : '사용자정보동의서', labelAlign : 'left', labelWidth : 150, id : 'oldPw', name:'oldPw', width : 200, inputType: 'password', vtype: 'password', notNull:true})
							},{colspan:2, tdHeight: 30, 
								item : createTextFieldComp({label : '사용자정보동의서', labelAlign : 'left', labelWidth : 150, id : 'newPw', name:'newPw', width : 200,inputType: 'password', vtype: 'password', notNull:true})
							},{colspan:2, tdHeight: 30, 
								item : createTextFieldComp({label : '사용자정보동의서', labelAlign : 'left', labelWidth : 150, id : 'checkNewPw', name:'checkNewPw',width : 200,  inputType: 'password', vtype: 'password', notNull:true})
							},{colspan:2, tdHeight: 30,
								item : passInputText
							}],
				hiddenFields:[ {name: 'user_id' , value : data.USER_ID}
								,{name: 'upd_dt' , value : data.UPD_DT}]
			}
		var updatePwEditForm = createEditorFormComp(setEditProp);
		
		var popupBtns = [updateBtn];
		if(that["forceChangeYn"] !== "Y") {
			popupBtns.push(closeBtn);
		}
		
		userAgreePopup = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: '사용자 동의서',  
			//width: 500,
			autoDestroy: false,
			resizable:false,
			closable: false,
			buttonAlign: that['buttonAlign'],
			buttons: popupBtns,
			bodyStyle: that['bodyStyle'],
			items: [updatePwEditForm],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		attachBtnEvent(updateBtn, pwUpdateActionLink,  'update');
		if(that["forceChangeYn"] !== "Y") {
			attachBtnEvent(closeBtn, pwUpdateActionLink,  'close');
		}
		
		popUp_comp[that['id']] = userAgreePopup;
		
	}else {
		
		// 데이터 새로 조회합니다.
		var theForm = Ext.getCmp('userInfoUpdateDetail');
		jq.ajax({ type:"POST"
			, url: url 
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson(paramMap)
			, success:function(data){
				var newResult = data.resultMap;
				theForm.setDataForMap(newResult);
			}
		});
		
		userAgreePopup = popUp_comp[that['id']];
	}
	
	formEditable({id : 'updatePwEditForm', editorFlag : true, excepArray :['']});
	
	
	return userAgreePopup;
}

function pwUpdateActionLink(command) {
	
	var theForm = Ext.getCmp('updatePwEditForm');
	var paramMap = theForm.getInputData();
	
	switch(command) {
		
		case 'update' :
			 //필수값체크
			
			//등록로직들어감 (사용자정보동의서, 사용자정보 업데이트)
			
			//팝업끄고 비밀번호변경진행
			userAgreePopup.doClose();
			parent.pwUpdateRequest(this.paramMap.user_id);
			
		break;
		
		case 'close' :
			parent.logout();
//			// 체크박스 체크 
//			if(paramMap.popupYn){
//				
//				paramMap["popup_show_yn"] = "N";
//				
//				jq.ajax({ type:"POST"
//					, url:'/itg/certification/updateOneDayPwPopupShow.do'
//					, contentType: "application/json"
//					, dataType: "json"
//					, async: false
//					, data : getArrayToJson(paramMap)
//					, success:function(data){
//						theForm.initData();
//						userAgreePopup.doClose();
//					}
//				});
//				
//			}else{
//				theForm.initData();
//				userAgreePopup.doClose();
//			}
			
		break;
	}
	
}

function checkPwResult(cur_pw, chg_pw, chg_pw_chk) {
	
	var valid = false;
	var pw_arr = new Array();
	pw_arr.push(cur_pw);
	pw_arr.push(chg_pw);
	pw_arr.push(chg_pw_chk);
	
	var passWordCheckVal = /^(?=.*[a-zA-Z])(?=.*[~!@#$%^&*()+=-])(?=.*[0-9]).*$/;
	
	for(var i=0; i<3; i++) {
		if(passWordCheckVal.test(pw_arr[i])){
			// 문자열 길이 체크 로직
			var str = pw_arr[i];
			var strLength = str.length;
			if(9 <= strLength){
				if(strLength <= 12){
					valid = true;
				} else {
					valid = false;
					break;
				}
			} else {
				valid = false;
				break;
			}
		} else {
			valid = false;
			break;
		}
	}
	
	return valid;
}

