// 취득 라이선스 팝업 호출
function openSelAmStatusSummaryPop(params, searchAmStatusType) {
	var that = convertDomToArray(params);
	var prefixVal = that['viewMode'] == "maintAmount" ? "grid.itam.statistics.AMMAINT" : "grid.itam.statistics.AMROOT";
	
//	alert(that['viewMode'] + '\n' + prefixVal);
	
	var setPopUpProp = {
		id: 'amStatusSummaryPop',
		popUpText: "자산정보",
		title: "자산정보",
		prefix: prefixVal,
		height: 640,
		width: 1100,
		url: '/itg/itam/statistics/searchAssetList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		searchAmStatusType: searchAmStatusType,
		params: params
	}
	var popUp = createAmStatusSummaryPop(setPopUpProp);
	popUp.show();
}

///////////////////////////////
// 팝업 윈도우
function createAmStatusSummaryPop(setPopUpProp){	
	var that = convertDomToArray(setPopUpProp);
	var setAmStatusSummaryPop = null;
	
	//엑셀다운로드 버튼
	var excelDownAmStatusSummaryBtn = createBtnComp({label: getConstText({ isArgs: true, m_key: 'btn.common.exceldown' }), id:'excelDownAmStatusSummaryBtn',icon : getConstValue('CONTEXT') + '/itg/base/images/ext-js/common/icons/excel-icon.png'});
	attachBtnEvent(excelDownAmStatusSummaryBtn, doAmStatusSummaryExcelDownload, that['params']);
	
	var amStatusSummaryGridProp = {	
		id: 'amStatusSummaryGrid',
		title : that['title'],
		gridHeight: 574,
		resource_prefix: that['prefix'],				
		url: that['url'],
		params: that['params'],
		selfScroll : true,
		pagingBar : true,
		pageSize : 20,
		toolBarComp: [excelDownAmStatusSummaryBtn],
		autoLoad: true
	};
	var amStatusSummaryGrid = createGridComp(amStatusSummaryGridProp);
	
	var searchAmStatusType = that["searchAmStatusType"];
	
	amStatusSummaryGrid.on("beforerender", function(gridView){
		var gridHeaderCt = amStatusSummaryGrid.headerCt;
		var columns = gridHeaderCt.getGridColumns();
		for( var i = 0; i < columns.length; i++ ){
			if( searchAmStatusType == "INTRO_COST_YEAR" ){
				if( columns[i]["dataIndex"] == "FN_INTRO_COST" ){
					columns[i]["width"] = 140;
					columns[i]["hidden"] = false;
					break;
				}
			}
		}
	});
	
	amStatusSummaryGrid.on("afterrender", function(gridView){
		var gridHeaderCt = amStatusSummaryGrid.headerCt;
		var columns = gridHeaderCt.getGridColumns();
		var moveColumnIndex = 0;
		for( var i = 0; i < columns.length; i++ ){
			if( searchAmStatusType == "LIFE_CYCLE" ){
				if( columns[i]["dataIndex"] == "FN_ASSET_STATE_NM" ){
					moveColumnIndex = i;
					break;
				}
			}else if( searchAmStatusType == "INTRO_YEAR" ){
				if( columns[i]["dataIndex"] == "FN_INTRO_DT" ){
					moveColumnIndex = i;
					break;
				}
			}else if( searchAmStatusType == "INTRO_COST_YEAR" ){
				if( columns[i]["dataIndex"] == "FN_INTRO_COST" ){
					columns[i]["text"] = columns[i]["text"];
					moveColumnIndex = i;
					break;
				}
			}
		}
		if( moveColumnIndex != 0 ){
			gridHeaderCt.move(moveColumnIndex, 3); // move 1 col to 2 position (swap 1 and 2 cols)
			amStatusSummaryGrid.getView().refresh(); // refresh view (to rerender cells)
		}
	});
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		scale:'medium',
		handler: function() {
			setAmStatusSummaryPop.close();
		}
	});
	
	setAmStatusSummaryPop = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		title: that['title'],  
		height: that['height'], 
		width: that['width'],  
		autoDestroy: false,
		resizable:false,
		buttonAlign: that['buttonAlign'],
		buttons: [closeButton],
		bodyStyle: that['bodyStyle'],
		items: [amStatusSummaryGrid]
	});
	
	return setAmStatusSummaryPop;

}

function doAmStatusSummaryExcelDownload(params) {
	var that = convertDomToArray(params);
	var excelParam = {};

	if(!confirm("다운로드 하시겠습니까?")){
		return;
	}
	var headerVal = that['viewMode'] == "maintAmount" ? "grid.itam.statistics.AMMAINT.header" : "grid.itam.statistics.AMROOT.header";
	var titleVal = that['viewMode'] == "maintAmount" ? "grid.itam.statistics.AMMAINT.text" : "grid.itam.statistics.AMROOT.text";
	
	var header = headerVal;
	var prefixHeader = getConstText({ isArgs: true, m_key: header });

	var title = titleVal;
	var prefixTitle = getConstText({ isArgs: true, m_key: title });
	
	var excelAttrs = {
		sheet: [{ 	
			sheetName		: "자산목록",
			titleName		: "자산목록", 
			includeColumns	: prefixHeader,
			headerNames		: prefixTitle
		}]
	};
	excelParam['fileName'] = "자산목록";
	excelParam['excelAttrs'] = excelAttrs;
	goExcelDownLoad(getConstValue('CONTEXT') + "/itg/itam/statistics/searchAmSummaryExcel.do", params, excelParam);
};