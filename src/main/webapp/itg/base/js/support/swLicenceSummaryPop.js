// 취득 라이선스 팝업 호출
function openSelSwAssetLicenceSummaryPop(rowData, licenceType) {
	var swGroupId = rowData["SW_GROUP_ID"];
	
	var prefix = 'grid.support.statistics.sam.swLicenceCntPop';
	
	var setPopUpProp = {
		id: 'swAssetLicenceAssignedPop',
		popUpText: "소프트웨어 라이선스",
		title: "소프트웨어 자산별 취득 라이선스 정보",
		prefix: prefix,
		height: 600,
		width: 1100,
		url: '/itg/support/statistics/sam/selectSwLicenceCntPopList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		params: {sw_group_id : swGroupId, licence_type: licenceType}
	}
	var popUp = createSwAssetLicenceSummaryPop(setPopUpProp);
	popUp.show();
}

// 할당 라이선스 팝업 호출
function openSelSwAssetLicenceAssignedSummaryPop(rowData, licenceType) {
	var swGroupId = rowData["SW_GROUP_ID"];
	
	var prefix = 'grid.support.statistics.sam.swLicenceCntChgPop';
	
	var setPopUpProp = {
		id: 'swAssetLicenceAssignedPop',
		popUpText: "소프트웨어 라이선스",
		title: "할당 소프트웨어 목록",
		prefix: prefix,
		height: 600,
		width: 1100,
		url: '/itg/support/statistics/sam/selectSwLicenceCommonCntPopList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		params: {sw_group_id : swGroupId, licence_type: licenceType, licence_flag: "ASSIGNED"}
	}
	var popUp = createSwAssetLicenceSummaryPop(setPopUpProp);
	popUp.show();
}

// 기간만료 라이선스 팝업 호출
function openSelSwAssetLicenceExpSummaryPop(rowData, licenceType) {
	var swGroupId = rowData["SW_GROUP_ID"];
	
	var prefix = 'grid.support.statistics.sam.swLicenceCntExpPop';
	
	var setPopUpProp = {
		id: 'swAssetLicenceAssignedPop',
		popUpText: "소프트웨어 라이선스",
		title: "기간만료 소프트웨어 목록",
		prefix: prefix,
		height: 600,
		width: 1100,
		url: '/itg/support/statistics/sam/selectSwLicenceCommonCntPopList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		params: {sw_group_id : swGroupId, licence_type: licenceType, licence_flag: "EXP"}
	}
	var popUp = createSwAssetLicenceSummaryPop(setPopUpProp);
	popUp.show();
}

// 스펙변경 라이선스 팝업 호출
function openSelSwAssetLicenceSpecSummaryPop(rowData, licenceType) {
	var swGroupId = rowData["SW_GROUP_ID"];
	
	var prefix = 'grid.support.statistics.sam.swLicenceCntChgPop';
	
	var setPopUpProp = {
		id: 'swAssetLicenceAssignedPop',
		popUpText: "소프트웨어 라이선스",
		title: "스펙변경 소프트웨어 목록",
		prefix: prefix,
		height: 600,
		width: 1100,
		url: '/itg/support/statistics/sam/selectSwLicenceCommonCntPopList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		params: {sw_group_id : swGroupId, licence_type: licenceType, licence_flag: "SPEC"}
	}
	var popUp = createSwAssetLicenceSummaryPop(setPopUpProp);
	popUp.show();
}

// 라이선스 기준변경 팝업 호출
function openSelSwAssetLicenceApplyBaseSummaryPop(rowData, licenceType) {
	var swGroupId = rowData["SW_GROUP_ID"];
	
	var prefix = 'grid.support.statistics.sam.swLicenceCntChgPop';
	
	var setPopUpProp = {
		id: 'swAssetLicenceAssignedPop',
		popUpText: "소프트웨어 라이선스",
		title: "라이선스 기준변경 소프트웨어 목록",
		prefix: prefix,
		height: 600,
		width: 1100,
		url: '/itg/support/statistics/sam/selectSwLicenceCommonCntPopList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		params: {sw_group_id : swGroupId, licence_type: licenceType, licence_flag: "APPLY_BASE"}
	}
	var popUp = createSwAssetLicenceSummaryPop(setPopUpProp);
	popUp.show();
}

///////////////////////////////
// 팝업 윈도우
function createSwAssetLicenceSummaryPop(setPopUpProp){	
	var that = convertDomToArray(setPopUpProp);
	var setSwAssetLicenceSummaryPop = null;
	
	var swAssetLicenceSummaryGridProp = {	
		id: 'swAssetLicenceSummaryGrid',
		title : that['title'],
		gridHeight: 530,
		resource_prefix: that['prefix'],				
		url: that['url'],
		params: that['params'],
		pagingBar : false,
		autoLoad: true
	};
	
	var swAssetLicenceSummaryGrid = createGridComp(swAssetLicenceSummaryGridProp);
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		scale:'medium',
		handler: function() {
			setSwAssetLicenceSummaryPop.close();
		}
	});
	
	setSwAssetLicenceSummaryPop = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		title: that['title'],  
		height: that['height'], 
		width: that['width'],  
		autoDestroy: false,
		resizable:false,
		buttonAlign: that['buttonAlign'],
		buttons: [closeButton],
		bodyStyle: that['bodyStyle'],
		items: [swAssetLicenceSummaryGrid]
	});
	
	return setSwAssetLicenceSummaryPop;

}


function openSummarySwLicencePop(params) {
	
	var setPopUpProp = {
		id: 'summarySwLicencePop',
		popUpText: "소프트웨어 라이선스 현황",
		title: "상세보기",
		prefix: "grid.itam.sam.status.swLicence." + params['stat_type'],
		height: 600,
		width: 1100,
		url: '/itg/support/statistics/sam/selectSwLicenceSummaryPop.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		params: params
	}
	var popUp = createSummarySwLicencePop(setPopUpProp);
	popUp.show();
}

function createSummarySwLicencePop(setPopUpProp){	
	var that = convertDomToArray(setPopUpProp);
	var setSummarySwLicencePop = null;
	
	//엑셀다운로드 버튼 생성
	var excelDownloadSwLicenceStatsPopBtn = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.exceldown' }),
		icon: '/itg/base/images/ext-js/common/icons/excel-icon.png',
		scale:'small',
		handler: function() {
			doSwLicenceStatsPopExcelDownload(that['params']);
		}
	});
	
	// 할당 소프트웨어 목록
	var swSummaryLicenceAssetListGridProp = {	
		id: 'swSummaryLicenceAssetListGrid',
		title : that['title'],
		gridHeight: 530,
		resource_prefix: that['prefix'],				
		url: that['url'],
		params: that['params'],
		pagingBar : false,
		loadTitleCount: true,
		toolBarComp: [excelDownloadSwLicenceStatsPopBtn],
		autoLoad: true
	};
	var swSummaryLicenceAssetListGrid = createGridComp(swSummaryLicenceAssetListGridProp);
	
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		scale:'medium',
		handler: function() {
			setSummarySwLicencePop.close();
		}
	});
	
	setSummarySwLicencePop = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		title: that['title'],  
		height: that['height'], 
		width: that['width'],  
		autoDestroy: false,
		resizable:false,
		buttonAlign: that['buttonAlign'],
		buttons: [closeButton],
		bodyStyle: that['bodyStyle'],
		items: [swSummaryLicenceAssetListGrid]
	});
	
	return setSummarySwLicencePop;

}

function doSwLicenceStatsPopExcelDownload(params) {
	
	var licence_type = params['licence_type'];
	var licence_flag = params['licence_flag'];
	
	if("APPLYBASE" == licence_flag) {
		licence_flag = "라이선스 기준변경";
	} else if("SPEC" == licence_flag) {
		licence_flag = "스펙변경 라이선스";
	} else if("ASSIGNED" == licence_flag) {
		licence_flag = "할당 라이선스";
	} else if("EXP" == licence_flag) {
		licence_flag = "기간만료 라이선스";
	}
	
	var excelParam = {};

	if(!confirm("다운로드 하시겠습니까?")){
		return;
	}
	
	var header = "grid.itam.sam.status.swLicence." + params['stat_type'] +".header";
	var prefixHeader = getConstText({ isArgs: true, m_key: header });

	var title = "grid.itam.sam.status.swLicence." + params['stat_type'] +".text";
	var prefixTitle = getConstText({ isArgs: true, m_key: title });
	
	var excelAttrs = {
		sheet: [{ 	
			sheetName		: licence_flag + " 상세 현황", 
			titleName		: licence_flag + " 상세 현황", 
			includeColumns	: prefixHeader,
			headerNames		: prefixTitle
		}]
	};
	
	params['isExcel'] = "true";
	excelParam['fileName'] = licence_flag + " 상세 현황";
	excelParam['excelAttrs'] = excelAttrs;
	goExcelDownLoad("/itg/support/statistics/sam/selectSwLicenceStatPopExcelDown.do", params, excelParam);
};