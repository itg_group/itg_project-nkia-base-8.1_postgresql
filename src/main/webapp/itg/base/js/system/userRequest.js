var popUp_comp={};
var removeRow="";

function createUserRequestPopUp(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var theForm = Ext.getCmp('userReqForm');
	
	if(popUp_comp[that['id']] == null) {

		var setEditorFormProp = {
				id: 'userReqForm',
				editOnly: true,
				border: true,
				columnSize: 2,
				tableProps : [
								{colspan:1, item : createTreeSelectPopComp({popId:'deptSelectPop', popUpTitle: getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00001' }), targetName:'cust_nm', targetHiddenName:'cust_id', targetForm:'setEditorFormProp', treeId:'deptSelectTree', url: '/itg/base/searchDeptTree.do', targetLabel:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00002' }), btnId:'custSelectBtn', params: null, rnode_text: getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00003' }), notNull:true, leafSelect: false, expandLevel: 4, width: 245})}
							   ,{colspan:1, item : createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.label.system.00009'}), name:'dept_nm', notNull:true, width: 300, maxLength: 100, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00001'})})}
					           ,{colspan:2, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00004' }), name:'user_class', code_grp_id:'USER_CLASS', attachChoice:true, notNull:true, width: 300})}
					           ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00010' }), name:'user_nm', notNull:true, width: 300, maxLength: 100})}
					           ,{colspan:1, item : checkOverlapComp({text:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00011' }), id:'user_id_field', name:'user_id', btnId: 'userIdCheckBtn', maxLength: 40, btnText: getConstText({ isArgs: true, m_key: 'btn.common.validation' }), targetForm:'userReqForm', chkUrl:'/itg/system/userReq/selectUserIdComp.do', notNull:true, vtype: 'alphanum', disabled: true, successMsg: getConstText({ isArgs: true, m_key: 'msg.system.userReq.00001' }), failMsg: getConstText({ isArgs: true, m_key: 'msg.system.userReq.00002' })})}
					           ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.common.label.password' }), name:'pw', id:'pw', inputType: 'password', notNull:true, width: 300, maxLength: 30})}
					           ,{colspan:1, item : createTextFieldComp({label: getConstText({ isArgs: true, m_key: 'res.common.label.repassword' }), name:'pwCheck', id:'pwCheck', inputType: 'password', notNull:true, width: 300, maxLength: 30})}
					           ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00012' }), name:'serial_number', width: 300, maxLength : 20, vtype: 'serialNumber', emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00002'})})}
					           ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00013' }), name:'position', notNull:true, width: 300, maxLength: 100, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00003'}) })}
					           ,{colspan:2, item : createEmailFieldComp({label:getConstText({ isArgs: true, m_key: 'res.common.label.email' }), name:'email', width: 400})}
					           ,{colspan:1, item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.common.label.hp' }), name:'hp_no', width: 300, notNull: true, firstNumberNotNull: true, firstFieldLength:3})}
					           ,{colspan:1, item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.common.label.tel' }), name:'home_tel_no', width: 300, notNull: true, firstNumberNotNull: false})}
					           ,{colspan:2, item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.login.userInfoUpdate.00009' }), name:'fax_no', width: 300})}
					           //,{colspan:2, item : createPhoneFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00014' }), name:'tel_ext', width: 300})}
					           ,{colspan:2, item : createTextAreaFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00008' }), name:'req_cause', value: "", width: 600, maxLength: 2000, notNull: true, emptyText: getConstText({ isArgs: true, m_key: 'msg.user.reg.00005'})})}
					           ,{colspan:2, item : createOutUserSelectPopComp({id:'chargerSelectPop', popUpText: getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00015' }), label: getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00016' }), name:'charger_nm', hiddenName:'charger', targetForm:'userReqForm', gridId:'chargerSelectGrid', btnId: 'chargerSelBtn', notNull: true, width: 245, custId: getConstValue("CENTER_CUST_ID")})}
					           ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.userReq.00017' }), name:'overLapYNuser_id', id:'overLapYNuser_id', value: 'N', width: 200, notNull:false, maxLength: 100})}
				             ]
		}
		
		var eidtorFormPanel = createEditorFormComp(setEditorFormProp);
		
		formEditable({id : 'userReqForm' , editorFlag : true , excepArray :[]});
		
		Ext.getCmp('overLapYNuser_id').hide();
		
		var insertBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			ui:'correct',
	        handler: function() {
        		var theForm = Ext.getCmp('userReqForm');
        		if(theForm.isValid()){
	        		
	        		if(theForm.getFieldValue('overLapYNauth_id') == 'N'){
	    				showMessage(getConstText({ isArgs: true, m_key: 'msg.system.userReq.00003' }));
	    				return false;
	        		}
	
	        		if(theForm.getFieldValue('overLapYNauth_id') == ''){
	    				showMessage(getConstText({ isArgs: true, m_key: 'msg.system.userReq.00004' }));
	    				return false;
	        		}
	
	        		if(theForm.getFieldValue('pw') != theForm.getFieldValue('pwCheck') && !theForm.getFieldValue('pw')){
	    				showMessage(getConstText({ isArgs: true, m_key: 'msg.system.userReq.00005' }));
	    				return false;
	        		}
	        		
	    			if(theForm.getFieldValue('charger_nm') == null || theForm.getFieldValue('charger_nm') == ""){
	    				showMessage(getConstText({ isArgs: true, m_key: 'msg.system.userReq.00006' }));
	    				return false;
	    			}        			
        			
	    			/*
        			// 사용부대 선택시 군번을 필수 영역으로
	    			if("2" == theForm.getFieldValue('user_class')){
	    				if("" == theForm.getFieldValue('serial_number')){
	    					alert(getConstText({ isArgs: true, m_key: 'msg.system.userReq.00008' }));
	    					return false;
	    				}
	    			}
	    			*/
	    			
					var paramMap = theForm.getInputData();
					
					// 전화번호 제일 앞에 대쉬(-)가 붙엇을 경우 제거하여 parameter 세팅 - 2014.10.05. 정정윤
					if(paramMap['tel_no'].substr(0,1) == "-") {
						paramMap['tel_no'] = paramMap['tel_no'].replace("-","");
					}
					
		            jq.ajax({ 
						type:"POST", 
						url:'/itg/system/userReq/insertUserReq.do', 
						contentType: "application/json", 
						dataType: "json", 
						async: true, 
						data: getArrayToJson(paramMap), 
						success: function(data){
			    			if( data.success ){
			    				showMessage(data.resultMsg);

							}else{
								showMessage(data.resultMsg);
			    			}
			    			inputTablePop.close();
			    		}
					});
					
				}else{
					
					showMessage( getConstText({ isArgs: true, m_key: 'msg.common.00011' }));
					return false;
				}

	        }
		});
		
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				inputTablePop.close();
			}
		});
		
		inputTablePop = Ext.create('nkia.custom.windowPop',{

			id: that['id'],
			title: that['title'], 
			width: 700,
			autoDestroy: false,
			resizable:false, 
			//bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn, closeBtn],
			bodyStyle: that['bodyStyle'],
			items: [eidtorFormPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = inputTablePop;
		
	} else {
		inputTablePop = popUp_comp[that['id']];
		Ext.getCmp('userReqForm').initData();
	}
	
	return inputTablePop;
}