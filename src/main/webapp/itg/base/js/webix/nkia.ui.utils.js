/********************************************
 * Webix 공통 Function
 * Date: 2016-11-23
 ********************************************/
nkia.ui.utils.copyObj = function(obj) {
	var copy = {};
	for (var attr in obj) {
		if (obj.hasOwnProperty(attr)) {
			copy[attr] = obj[attr];
		}
	}
	return copy;
};

/**
 * @function
 * @summary json 확정 util
 * @author ITG
 * @since 1.0
 */
nkia.ui.utils.extend = function() {
	// Variables
    var extended = {};
    var deep = false;
    var i = 0;
    var length = arguments.length;
    var sourceObj = null;
    
    // Check if a deep merge
    if ( Object.prototype.toString.call( arguments[0] ) === '[object Boolean]' ) {
        deep = arguments[0];
        i++;
    }

    // Merge the object into the extended object
    var merge = function (obj) {
        for ( var prop in obj ) {
            if ( Object.prototype.hasOwnProperty.call( obj, prop ) ) {
                // If deep merge and property is an object, merge properties
                if ( deep && Object.prototype.toString.call(obj[prop]) === '[object Object]' ) {
                    extended[prop] = extend( true, extended[prop], obj[prop] );
                } else {
                    sourceObj[prop] = obj[prop];
                }
            }
        }
    };

    // Loop through each object and conduct a merge
    for ( ; i < length; i++ ) {
        var obj = arguments[i];
        if(sourceObj == null){
        	sourceObj = obj;
        }else{
	        merge(obj);
        }
    }
    return (deep)? extended : sourceObj;
};

/**
 * @function
 * @summary  모든 버튼 객체 숨기기
 * @param {string} ids
 * @author ITG
 * @since 1.0
 */
nkia.ui.utils.hides = function(ids) {
	for (var i = 0; i < ids.length; i++) {
		$$(ids[i]).hide();
	}
};

/**
 * @function
 * @summary  모든 버튼 객체 보이기
 * @param {string} ids
 * @author ITG
 * @since 1.0
 */
nkia.ui.utils.shows = function(ids) {
	for (var i = 0; i < ids.length; i++) {
		$$(ids[i]).show();
	}
};

/**
 * 컴포넌트 리셋
 * @param {} components
 */
nkia.ui.utils.clears = function(components) {
	for (var i = 0; i < components.length; i++) {
		var component = components[i];
		switch (component.config.view) {
			case "form":
				component._reset();
				break;
			case "datatable":
				component._removeAllRow();
				break;
			default:

		}
	}
};

/**
 * Disable 처리 판단
 * @param {} item
 * @param {} exceptions
 * @param {} isReadonly
 */
nkia.ui.utils.disabled = function(item, exceptions, isReadonly){
	var comp = $$(item.id);
	var isCompReadonly = isReadonly;
	if (exceptions) {
		if (isExistDataForArray(exceptions, item.id)) {
			isCompReadonly = !isReadonly;
		}
	}
	if(isCompReadonly){
		comp.enable();
	}else{
		comp.disable();
	}
};

/**
 * 메시지 Window
 * @param {} props
 */
nkia.ui.utils.notification = function(props){
	/**
	 * 메시지 : 여러개의 메시지가 뜨지 않도록 id 추가
	 * -> error : 클릭해야 사라짐
	 * -> info : 1초 후 사라짐
	 */
	var msgtype = props.type || "info";
	webix.message({
		id: "nkia_notification_message_" + msgtype,
		type: msgtype,
		text: props.message,
		expire: (msgtype==="error") ? -1 : 1000
	});
};

/**
 * 공통 Ajax
 * @param {} props
 */
nkia.ui.utils.ajax = function(props){
	// Show Progress 속성으로 제어할 수도 있음.
	if(props.viewId) {
		nkia.ui.utils.showProgress({id: props.viewId, isMask: props.isMask});
	}
	
	jq.ajax({
		type:"POST",
		contentType: "application/json",
		async: ( typeof props.async !== undefined ) ? props.async : true,
		dataType: "json",
		url: nkia.host + props.url,
		data: JSON.stringify(props.params||{}),
		success: function(response){
			if(props.viewId) {
				nkia.ui.utils.hideProgress({id: props.viewId, isMask: props.isMask});
			}  
			
			if(response.success){
				if(props.notification && nkia.ui.utils.check.nullValue(response.resultMsg) !== ""){
					nkia.ui.utils.notification({
						message: response.resultMsg,
						exprie: 2000
					});
				}
				props.success(response);
			}else{
				if(nkia.ui.utils.check.nullValue(response.resultMsg) !== ""){
					nkia.ui.utils.notification({
						type : 'error',
						message: response.resultMsg
					});
				}
				
				if( typeof props.failure != "undefined" ){
					props.failure(response);
				}
			}
		}
	});
};

/**
 * Progress 표시
 * @param {} props
 */
nkia.ui.utils.showProgress = function(props){
	var view = $$(props.id) || $$("app");
	if(view){
		var isMask = (props.isMask === undefined) ? true : props.isMask;
		if(isMask){
			view.disable();
			// Y Scroll이 있는 경우 height값을 수정.		
			if(view.$view.scrollHeight){
				jq(".webix_view.webix_scrollview.webix_disabled_view > .webix_disabled").css( "height", view.$view.scrollHeight );
			}
		}
		view.showProgress({
			type:"top",
			hide:true,
			delay: 30000
		});	
	}
};

/**
 * Progress 숨김
 * @param {} props
 */
nkia.ui.utils.hideProgress = function(props){
	var view = $$(props.id) || $$("app");
	if(view){
		var isMask = (props.isMask === undefined) ? true : props.isMask;
		if(isMask){
			view.enable();
		}
		view.hideProgress();	
	}
};

/**
 * Window 생성
 * @param {} props
 */
nkia.ui.utils.window = function(props) {
	if($$(props.id) === undefined){
		props.callFunc(props);
	}
	$$(props.id).show();
};

/**
 * Date Util
 * @param {} props
 * @return {}
 */
nkia.ui.utils.date = function(props) {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) {
	    dd='0'+dd;
	}
	if(mm<10) {
	    mm='0'+mm;
	}
	return yyyy + "-" + mm + "-" + dd;
};

/**
 * Array에서 유니크 요소만 반환
 * @param {} arry
 * @return {}
 */
nkia.ui.utils.array.unique = function(arry) {
	return arry.reduce(function(a,b){if(a.indexOf(b)<0)a.push(b);return a;},[]);;
};

/**
 * Attach Event
 * @param {} comp
 * @param {} callFunc
 * @param {} param
 */
nkia.ui.utils.attach.buttonEvent = function(button, callFunc, param) {
	if($$(button.id)){
		$$(button.id).attachEvent("onItemClick", function(){
			callFunc(param);
		});	
	}else{
		button.click = function(){
			callFunc(param);
		};
	}
};

/**
 * Custom Event 부여
 * @param {} eventName
 * @param {} component
 * @param {} callFunc
 * @param {} param
 */
nkia.ui.utils.attach.customEvent = function(eventName, component, callFunc, param) {
	if (typeof param != "undefined") {
		component.attachEvent(eventName, function() {
			callFunc(param);
		});
	} else {
		component.attachEvent(eventName, callFunc);
	}
	
	
};

/**
 * Value값 체크
 * @param {} value
 * @return {}
 */
nkia.ui.utils.check.nullValue = function(value) {
	if(value == null || value == 'null' || value == 'undefined' || value == ' ') {
		value = "";
	}
	return value;
};

/**
 * 헤더 버튼 붙이기
 * @param {} formProp
 * @param {} button
 */
nkia.ui.utils.attach.headerButton = function(formProp, button){
	if(formProp.header){
		if(formProp.header.buttons){
			formProp.header.buttons.items.push(button);
		}else{
			formProp.header.buttons = {
				items: [
					button
				]
			};
		}
	}else{
		formProp.header = {
			buttons: {
				items: [
					button
				]
			}
		};
	}
};

/**
 * 하단 버튼 붙이기
 * @param {} formProp
 * @param {} button
 */
nkia.ui.utils.attach.footerButton = function(formProp, button){
	if(formProp.footer){
		if(formProp.footer.buttons){
			formProp.footer.buttons.items.push(button);
		}else{
			formProp.footer.buttons = {
				items: [
					button
				]
			};
		}
	}else{
		formProp.footer = {
			buttons: {
				align: "center",
				items: [
					button
				]
			}
		};
	}
};

/**
 * lowercae로 변경된 데이터를 return
 * @param {} data
 * @return {}
 */
nkia.ui.utils.convert.data = function(data) {
	var convertData = {};
	for( var key in data ){
		convertData[key.toLowerCase()] = nkia.ui.utils.check.nullValue(data[key]);
	}
	return convertData;
};

/**
 * 줄바꿈 처리
 * @param {} data
 * @return {string}
 */
nkia.ui.utils.convert.linebreak = function(value) {
	var convertValue = "";
	if(nkia.ui.utils.check.nullValue(value) != ""){
		convertValue = replaceAll(value, '/n', '&#13;&#10;');
	}
	return convertValue;
};

/**
 * 팝업창의 width 값을 현재 클라이언트의 해상도와 계산하여 최적값 리턴
 * @author 정정윤 (jyjeong@nkia.co.kr)
 * @since 2017.01.27
 * @return {integer}
 */
nkia.ui.utils.getWidePopupWidth = function() {
	// ITG 최적사이즈로 기본값 세팅
	var width = CONSTRAINT.APP_RESOLUTION_WIDTH;
	
	if( document.body.clientWidth < width ){
		// 현재 클라이언트 브라우저의 크기가 ITG 최적사이즈 보다 작으면 브라우저에 맞게 크기 재 계산
		width = document.body.clientWidth - 20;
	} else {
		// 현재 클라이언트 크기가 ITG 최적사이즈보다 크면 와이드 넓이로 리턴
		width = document.body.clientWidth - 100;
	}
	
	return width;
};

/**
 * 팝업창의 height 값을 현재 클라이언트의 해상도와 계산하여 최적값 리턴
 * @author 정정윤 (jyjeong@nkia.co.kr)
 * @since 2017.05.08
 * @return {integer}
 */
nkia.ui.utils.getWidePopupHeight = function() {
	// ITG 최적사이즈로 기본값 세팅
	var height = CONSTRAINT.APP_RESOLUTION_HEIGHT;
	
	if( document.body.clientHeight < height ){
		// 현재 클라이언트 브라우저의 크기가 ITG 최적사이즈 보다 작으면 브라우저에 맞게 크기 재 계산
		height = document.body.clientHeight - 80;
	} else {
		// 현재 클라이언트 크기가 ITG 최적사이즈보다 크면 와이드 넓이로 리턴
		height = document.body.clientHeight - 30;
	}
	
	return height;
};

/**
 * 기준 key를 통해, 서로 다른 두 데이터 간에 키 충돌이 나는 값인지 비교
 * @author 정정윤 (jyjeong@nkia.co.kr)
 * @since 2017.02.22
 * @param {array} keyArray - 유일해야 하는 key id 목록
 * @param {object} baseData - 검증 대상이 되는 기준 데이터 Map
 * @param {array} targetData - 비교 대상이 되는 대상 데이터 Array
 * @return {boolean}
 */
nkia.ui.utils.isUniqueViolationData = function(keyArray, baseObject, targetArray) {
	
	var isEqual = false;
	var baseObj = {};
	
	// 데이터타입이 배열일 경우 첫번째 인덱스의 Object만 검사
	if(baseObject && baseObject.length) {
		baseObj = baseObject[0];
	}
	
	if(typeof targetArray == "undefined" || typeof targetArray.length == "undefined") {
		var errMsg = "targetArray is bad.\n input : " + targetArray;
		throw new Error(errMsg);
	}
	
	// deep copy
	var cData = JSON.parse(JSON.stringify(baseObj));
	var cTargets = JSON.parse(JSON.stringify(targetArray));
	// copy data 초기화
	delete cData.CONCAT_KEY_STRING;
	for(var i in cTargets) {
		delete cTargets[i].CONCAT_KEY_STRING;
	}
	
	// 각각 배열 인덱스 내의 object에 인덱스 별 로 CONCAT_KEY_STRING 라는 key를 추가하여 키 데이터들을 하나의 문장으로 만듬
	for(var i in keyArray) {
		if(cData.CONCAT_KEY_STRING) {
			cData.CONCAT_KEY_STRING = cData.CONCAT_KEY_STRING + cData[keyArray[i]];
		} else {
			cData.CONCAT_KEY_STRING = cData[keyArray[i]];
		}
	}
	
	for(var i in keyArray) {
		for(var j in cTargets) {
			if(cTargets[j].CONCAT_KEY_STRING) {
				cTargets[j].CONCAT_KEY_STRING = cTargets[j].CONCAT_KEY_STRING + cTargets[j][keyArray[i]];
			} else {
				cTargets[j].CONCAT_KEY_STRING = cTargets[j][keyArray[i]];
			}
		}
	}
	
	// 같은 키가 있는지 비교하여 같은 키가 있으면 isEqual true
	for(var i in cTargets) {
		if(cData.CONCAT_KEY_STRING == cTargets[i].CONCAT_KEY_STRING) {
			isEqual = true;
			// key 배열을 string 형태로 저장했던 컬럼 삭제
			delete cData.CONCAT_KEY_STRING;
			delete cTargets[i].CONCAT_KEY_STRING;
			break;
		}
	}
	
	return isEqual;
};

