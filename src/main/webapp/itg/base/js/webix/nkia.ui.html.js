/********************************************
 * Nkia UI Html Function
 * Date: 2016-01-18
 ********************************************/


/**
 * @function
 * @summary json 확정 util
 * @author ITG
 * @since 1.0
 */
nkia.ui.html.icon = function(icon, css, isLink) {
	var fontawesomeIcon = "";
	
	// 2017-02-24 정정윤 추가
	// 하이퍼링크 걸리지 않도록 단순 아이콘만 표시할 수 있게 마우스포인터 변수화
	var mousePointer = " style='cursor:pointer'";
	if(isLink === false) {
		mousePointer = "";
	}
	
	switch (icon) {
		case "arrow_up":
			fontawesomeIcon = "fa-chevron-circle-up";
			break;
		case "arrow_down":
			fontawesomeIcon = "fa-chevron-circle-down";
			break;
		case "arrow_left":
			fontawesomeIcon = "fa-chevron-circle-left";
			break;
		case "arrow_right":
			// fontawesomeIcon = "fa-chevron-circle-right";
			// 커스텀 아이콘으로 변경
			fontawesomeIcon = "custom_arrow_right";
			break;
		case "ticket":
			fontawesomeIcon = "fa-ticket";
			break;
		case "check":
			fontawesomeIcon = "fa-check";
			break;
		case "remove":
			fontawesomeIcon = "fa-remove";
			break;
		case "star":
			fontawesomeIcon = "fa-star";
			break;
		case "star_empty":
			fontawesomeIcon = "fa-star-o";
			break;
		case "circle":
			fontawesomeIcon = "fa-circle";
			break;
		case "circle_empty":
			fontawesomeIcon = "fa-circle-o";
			break;
		case "refresh":
			fontawesomeIcon = "fa-refresh";
			break;
		case "search":
	         fontawesomeIcon = "fa-search";
	         break;
		case "check_on":
	         fontawesomeIcon = "fa-check check_on";
	         break;
		case "check_off":
	         fontawesomeIcon = "fa-check check_off";
	         break;
		case "add":
	         fontawesomeIcon = "fa-plus";
	         break;
		default:
	}
	
	return "<div style='width:100%; height:100%; display:table;'>"
			+ "<div style='width:100%; height:100%; display:table-cell; vertical-align:middle; text-align:center;'>"
				+ "<span" + mousePointer + " class='webix_icon " + fontawesomeIcon + " " + css + "'>"
				+ "</span>"
			+ "</div>"
		+ "</div>";
};

/**
 * @function
 * @summary 프로세스 그리드 필드 관련 
 * @author ITG
 * @since 2018.07.31 
 * 
 * headerBtnType {ALL : 추가/삭제, ADD : 추가 , DEL : 삭제, EMPTY : 버튼없음 }
 */
nkia.ui.html.processGridField = function(label, required, gridId, popUpType, headerBtnType) {
	var fieldLabel = "";
	
	// 2018.07.31 dhkim
	// 프로세스 그리드 필드 헤더 or 라벨 템플릿
	var templateType = "";
	var resultTemplate = "";
	var insertOnClick = ""; // 추가 버튼
	var deleteOnclick = ""; // 삭제 버튼
	var workStartOnClick = "";
	var workEndOnClick = "";
	var workCancelOnClick = "";
	var centerInClick = "";    //센터출입요청 - 출퇴실
	var centerOutClick = "";
	
	if(typeof headerBtnType == 'undefined' || headerBtnType == '' || headerBtnType == null ){
		if(typeof gridId == 'undefined' || gridId == '' || gridId == null){
			headerBtnType = "EMPTY";
		}else{
			headerBtnType = "ALL";
		}
	}
	
	// 버튼이 필요한 경우 -> 그리드 헤더(그리드ID, 팝업타입에 따라 구분)
	// 버튼이 필요없는 경우 -> 폼 라벨
	if(headerBtnType == "EMPTY"){
		// 라벨 템플릿 세팅(form 필드 라벨로 세팅)
		templateType = "label";
	}else {
		if(typeof popUpType !== 'undefined' && popUpType != '' && popUpType != null){
			templateType = "header";
			insertOnClick = "<button type=\"button\" class=\"webixtype_form\" onClick=\"clickProcessGridInsertBtn('"+gridId+"','"+popUpType+"');\">"+getConstText({isArgs : true, m_key : 'btn.common.append'})+"</button>";
			deleteOnclick = "<button type=\"button\" class=\"webixtype_base\" onClick=\"clickProcessGridDeleteBtn('"+gridId+"','"+popUpType+"');\">"+getConstText({isArgs : true, m_key : 'btn.common.delete'})+"</button>";
			if(popUpType == "changeAlarm"){
				headerBtnType = "changeAlarm";
				workStartOnClick  = "<button type=\"button\" class=\"webixtype_form\" onClick=\"clickProcessGridChangeAlarmBtn('"+gridId+"','workStart');\">작업시작</button>";
				workEndOnClick    = "<button type=\"button\" class=\"webixtype_form\" onClick=\"clickProcessGridChangeAlarmBtn('"+gridId+"','workEnd');\">작업완료</button>";
				workCancelOnClick = "<button type=\"button\" class=\"webixtype_form\" onClick=\"clickProcessGridChangeAlarmBtn('"+gridId+"','workCancel');\">작업취소</button>";
				insertOnClick = "<button type=\"button\" class=\"webixtype_form\" onClick=\"clickProcessGridInsertBtn('"+gridId+"','"+popUpType+"');\">설정</button>";
				deleteOnclick = "<button type=\"button\" class=\"webixtype_base\" onClick=\"clickProcessGridDeleteBtn('"+gridId+"','"+popUpType+"');\">삭제</button>";
			}else if(popUpType == "centerGuestInfo"){
				headerBtnType = "centerGuestInfo";
				centerInClick  = "<button type=\"button\" class=\"webixtype_form\" onClick=\"clickProcessGridCenterInOutBtn('"+gridId+"','in');\">입실</button>";
				centerOutClick    = "<button type=\"button\" class=\"webixtype_form\" onClick=\"clickProcessGridCenterInOutBtn('"+gridId+"','out');\">퇴실</button>";
				
			}
			
		}else{
			templateType = "defaultHeader";
			insertOnClick = "<button type=\"button\" class=\"webixtype_form\" onClick=\"$$('"+gridId+"')._addRow();\">"+getConstText({isArgs : true, m_key : 'btn.common.addRow'})+"</button>";
			deleteOnclick = "<button type=\"button\" class=\"webixtype_base\" onClick=\"$$('"+gridId+"')._removeRow();\">"+getConstText({isArgs : true, m_key : 'btn.common.delete'})+"</button>";
			
		}
	}
	
	switch (templateType) {
		case "defaultHeader": // 헤더 - 행추가, 삭제
		case "header":        // 헤더 - 추가(커스텀), 삭제(커스텀)
			//롯데UBIT 알림대상자설정(변경요청)작업시작 버튼 커스텀
			var strBtnTemplate = "";
			strBtnTemplate += "<!-- 버튼 --><div class=\"webix_view webix_control webix_el_button\" style=\"display: inline-block; vertical-align: top; border-width: 0px; margin-top: 5px; margin-left: 0px; width: 90px; height: 30px;\">";
			strBtnTemplate += "<div class=\"webix_el_box\" style=\"width:90px; height:30px\">";
			strBtnTemplate += workStartOnClick;
			strBtnTemplate += "</div>";
			strBtnTemplate += "</div>";
			
			//롯데UBIT 알림대상자설정(변경요청)작업완료 버튼 커스텀
			var endBtnTemplate = "";
			endBtnTemplate += "<!-- 버튼 --><div class=\"webix_view webix_control webix_el_button\" style=\"display: inline-block; vertical-align: top; border-width: 0px; margin-top: 5px; margin-left: 0px; width: 90px; height: 30px;\">";
			endBtnTemplate += "<div class=\"webix_el_box\" style=\"width:90px; height:30px\">";
			endBtnTemplate += workEndOnClick;
			endBtnTemplate += "</div>";
			endBtnTemplate += "</div>";
			
			//롯데UBIT 알림대상자설정(변경요청)작업취소 버튼 커스텀
			var cancelBtnTemplate = "";
			cancelBtnTemplate += "<!-- 버튼 --><div class=\"webix_view webix_control webix_el_button\" style=\"display: inline-block; vertical-align: top; border-width: 0px; margin-top: 5px; margin-left: 0px; width: 90px; height: 30px;\">";
			cancelBtnTemplate += "<div class=\"webix_el_box\" style=\"width:90px; height:30px\">";
			cancelBtnTemplate += workCancelOnClick;
			cancelBtnTemplate += "</div>";
			cancelBtnTemplate += "</div>";
			
			//롯데UBIT 센터출입요청 - 출/퇴실처리 버튼 커스텀
			var centerInBtnTemplate = "";
			centerInBtnTemplate += "<!-- 버튼 --><div class=\"webix_view webix_control webix_el_button\" style=\"display: inline-block; vertical-align: top; border-width: 0px; margin-top: 5px; margin-left: 0px; width: 90px; height: 30px;\">";
			centerInBtnTemplate += "<div class=\"webix_el_box\" style=\"width:90px; height:30px\">";
			centerInBtnTemplate += centerInClick;
			centerInBtnTemplate += "</div>";
			centerInBtnTemplate += "</div>";
			
			//롯데UBIT 센터출입요청 - 출/퇴실처리 버튼 커스텀
			var centerOutBtnTemplate = "";
			centerOutBtnTemplate += "<!-- 버튼 --><div class=\"webix_view webix_control webix_el_button\" style=\"display: inline-block; vertical-align: top; border-width: 0px; margin-top: 5px; margin-left: 0px; width: 90px; height: 30px;\">";
			centerOutBtnTemplate += "<div class=\"webix_el_box\" style=\"width:90px; height:30px\">";
			centerOutBtnTemplate += centerOutClick;
			centerOutBtnTemplate += "</div>";
			centerOutBtnTemplate += "</div>";
			
			// 추가버튼 포맷
			var addBtnTemplate = "";
			addBtnTemplate += "<!-- 버튼 --><div class=\"webix_view webix_control webix_el_button\" style=\"display: inline-block; vertical-align: top; border-width: 0px; margin-top: 5px; margin-left: 0px; width: 90px; height: 30px;\">";
			addBtnTemplate += "<div class=\"webix_el_box\" style=\"width:90px; height:30px\">";
			addBtnTemplate += insertOnClick;
			addBtnTemplate += "</div>";
			addBtnTemplate += "</div>";
			
			// 삭제버튼 포맷
			var delBtnTemplate = "";
			delBtnTemplate += "<!-- 버튼 --><div class=\"webix_view webix_control webix_el_button\" style=\"display: inline-block; vertical-align: top; border-width: 0px; margin-top: 5px; margin-left: 0px; width: 90px; height: 30px;\">";
			delBtnTemplate += "<div class=\"webix_el_box\" style=\"width:90px; height:30px\">";
			delBtnTemplate += deleteOnclick;
			delBtnTemplate += "</div>";
			delBtnTemplate += "</div>";
			

			resultTemplate += "<table border=0 cellpadding=0 cellspacing=0 style=\"width: 100%;\">"; 
			resultTemplate += "<tr><td align=\"left\" nowrap style=\"width: 50%\">";
			
			// 필수여부체크
			if(required === true){
				resultTemplate +=  "<!-- 타이틀 --><div class='label_template' style='margin-left: -6px; margin-bottom:-14px;'>"+label+"<font color='red' style='padding-left:4px'>*</font> </div>";
			}else{
				resultTemplate +=  "<!-- 타이틀 --><div class='label_template' style='margin-left: -6px; margin-bottom:-14px;'>"+label+"</div>";
			}	
			
			resultTemplate += "</td><td align=\"right\" nowrap style=\"width: 50%\">";
			
			if(headerBtnType == "ALL") {
				resultTemplate += addBtnTemplate;
				resultTemplate += delBtnTemplate;
			} else if(headerBtnType == "changeAlarm"){
				if(gridId == "alarm_trget_result_grid"){
					resultTemplate += strBtnTemplate;
					resultTemplate += endBtnTemplate;
					resultTemplate += cancelBtnTemplate;					
				}
				resultTemplate += addBtnTemplate;
				resultTemplate += delBtnTemplate;
			} else if(headerBtnType == "centerGuestInfo"){
				resultTemplate += centerInBtnTemplate;
				resultTemplate += centerOutBtnTemplate;
			}else if(headerBtnType == "ADD"){
				resultTemplate += addBtnTemplate;
			} else if(headerBtnType == "DEL"){
				resultTemplate += delBtnTemplate;
			}
			resultTemplate += "</td></tr></table>";
			
		break;
		case "label":
		default: // 라벨 템플릿 세팅(form 필드 라벨로 세팅)
			
			resultTemplate += "<div class='webix_view webix_layout_line' style='white-space: nowrap; margin-left: 0px; margin-top: 0px; width: 1553px; height: 30px;'>"
			resultTemplate += "<div class='webix_view webix_control webix_el_label label_template webix_inp_top_label webix_required' style='display: inline-block; vertical-align: top; border-width: 0px; margin-top: -5px; margin-left: -3px; width: 1553px; height: 30px;'>"
				
			// 필수여부 체크	
			if(required === true){
				resultTemplate += "<div style='height:100%;line-height:24px'>"+label+"<font color='red' style='padding-left:4px'>*</font> </div>"	
			}else{
				resultTemplate += "<div style='height:100%;line-height:24px'>"+label+"</div>"	
			}
				
			resultTemplate += "</div>"	
			resultTemplate += "</div>"

		break;	
	}
	
	return resultTemplate;
};


