/********************************************
 * nkia.ui.Field.js
 * Date: 2021-03-22
 * Version : WEBIX_NKIA_UI_RELEASE_INFO 참고 
 *  - 설치 버전 : v20210301
 *  - 현재 버전 : v20210325   
 ********************************************/

// 연계필드 아이콘 변수
var _SYSTEM_LINKED_ICON = {
	DCA_EQUAL_SYMBOL : '<span style="padding : 0 5 0 5;"><img src="/itg/base/images/ext-js/simple/status/interface_dca_equal.gif" style="vertical-align:middle;" />&nbsp;</span>',
	DCA_UNEQUAL_SYMBOL : '<span style="padding : 0 5 0 5;"><img src="/itg/base/images/ext-js/simple/status/interface_dca_unequal.gif" style="vertical-align:middle;" />&nbsp;</span>',
	EMS_EQUAL_SYMBOL : '<span style="padding : 0 5 0 5;"><img src="/itg/base/images/ext-js/simple/status/interface_ems_equal.gif" />&nbsp;</span>',
	EMS_UNEQUAL_SYMBOL : '<span style="padding : 0 5 0 5;"><img src="/itg/base/images/ext-js/simple/status/interface_ems_equal.gif" />&nbsp;</span>',
	TOOLTIP_SYMBOL_PREFIX : '<span name="n_tooltip" class="tooltip_icon_span" onclick="showTooltip(\'',
	TOOLTIP_SYMBOL_MIDDLE : '\',',
	TOOLTIP_SYMBOL_SUFFIX : '); webix.html.preventEvent(event)" >&nbsp;<img src="/itg/base/images/ext-js/common/icons/information.png" style="vertical-align:top;" /></span>'	
};
var suggestData =[];

/********************************************
 * showTooltip
 * Date: 2018-03-08
 * Version:
 *	1.0 - 툴팁(도움말) Open 함수
 ********************************************/
function showTooltip(value, width, height) {
	
	if($$("tooltip_popup")) {
		$$("tooltip_popup").close();
	}
	
	// 불필요한 HTML 태그 제거 정규식 적용
	var replaceValue = value.replace(/<(\/html|html)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/script|script)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/iframe|iframe)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/embed|embed)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/a|a)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/img|img)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/head|head)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/body|body)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/form|form)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/textarea|textarea)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/select|select)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/input|input)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/div|div)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/span|span)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/table|table)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/tr|tr)([^>]*)>/gi,"");
	replaceValue = replaceValue.replace(/<(\/td|td)([^>]*)>/gi,"");

	var popup = webix.ui({
	    view:"popup",
	    id:"tooltip_popup",
	    height:height,
	    width:width,
	    left:event.clientX,
	    top:event.clientY,
	    body:{
	        template:replaceValue
	    },
	    css: "tooltip_popup"
	});
	
	popup.show();
	
	return false;
}

/********************************************
 * Text
 * Date: 2016-11-17
 * Version:
 *	1.0 - Text Field 지원
 ********************************************/
nkia.ui.fields.Text = function() {
	// Webix Properties
	// 속성을 초기화합니다.
	this.view = "text";
	this.id = "";
	this.label = "";
	this.name = "";
	this.required = false;
	this.disabled = false;
	this.readonly = false;
	this.value = "";
	this.placeholder = "";
	this.hidden = false;
	this.gravity = 1;
	this.bottomLabel = "";
	this.click = "";
	this.hotkey = "";
	this.inputType = "";
	this.attributes = {};
	this.on = {};
	this.maxlength = "";
	this.maxsize = "";
	this.css = "";
	this.decimalSize = 1;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.width = 0;
	this.isProcAfterHide = false;
	this.suggest = [];
	this.suggestParams = {};
	this.suggestUrl = "";
	this.formName = "";
	this.suggestType = "";
	// Custom Properties
	this.visible = true;
	
	this.propsValidate = function(props) {
		nkia.ui.validator.text(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

//		if(props.readonly !== undefined){
//			this.disabled = props.readonly;
//		}
		
		if(this.width === 0){
			delete this.width;
		}
		
		if(this.readonly === true) {
			this.css = "nkia_field_readonly";
		}

		if(this.bottomLabel === ""){
			delete this.bottomLabel;
		}
		
		// 길이제한 주석처리 _ 20171030 좌은진
		if(props.maxlength !== undefined){
			this.attributes.maxlength = props.maxlength;
		}

		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		// 입력 형식 체크
		if(props.inputType !== undefined){
			var isNotNull = (props.required) ? (props.required) : false;
			switch(props.inputType) {
				case 'price' :
					this.format = "1,111";
					break;
				case 'integer' :
					this.format = "1";
					break;
				case 'double' :
					// 실수 필드 (입력 시 쉼표 및 소수점 제공)
					var decimalSize = this.decimalSize*1;
					var decimalStr = "";
					for(var j=0; j<decimalSize; j++) {
						decimalStr += "0";
					}
					this.format = "1,111." + decimalStr;
					break;
				case 'phone' :
					// 핸드폰 필드 ( 000-0000-0000 )
					this.pattern = {mask: "###-####-####", allow: /[0-9]/g};
					break;
				case 'time' :
					this.pattern = {mask: "##", allow: /[0-9]/g};
					break;
			}
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
		
		/*
		if(this.required && this.invalidMessage === ""){
			this.invalidMessage = this.label + " 을[를] 입력해주십시오.";
		}
		*/
			
		if(this.suggestUrl != ""){
			this.css = "suggest"
			if(this.suggestType == "client"){
				webix.ajax().sync().post(
						nkia.host + this.suggestUrl,
						JSON.stringify(this.suggestParams),
						function (text) {
							var response = JSON.parse(text);
							var options = [];
		
							var rows = response.gridVO.rows;
							if(rows.length > 0){
									rows.forEach(function(row){
										options.push({
											id: row.ID,
											value: row.VALUE
										});
									});
		
							}
							suggestData = options;
						}
					);
				this.suggest = suggestData;
			}else if(this.suggestType == "server"){
				this.suggest = {
			            body: {
			                url:this.suggestUrl,
			                id : this.name + "_suggest",
			                parentId : this.name,
			                parentForm : this.formName,
			                dataFeed: function(filtervalue, filter){
			                    if(filtervalue.length<2) return;
			                    var urldata = "params="+encodeURIComponent(filtervalue);
			                    this.load(this.config.url+"?"+urldata, this.config.datatype);
			                },
			                on:{
			                    "onBeforeLoad":function(){
			                    	var master = this.getParentView().config.master;
			                        if(master)
			                            $$(master).showProgress('','suggest');
			                    	
			                    },
			                    "onAfterLoad":function(){
			                        webix.delay(function(){
			                            var master = this.getParentView().config.master;
			                            if(master)                                
			                            		$$(master).hideProgress();
			                        }, this, null, 2000);
			                    }
			                }
			            },
					     on:{
						     onValueSuggest: function(node) {
						     	$$(this.config.body.parentForm)._setFieldValue(this.config.body.parentId+"_hidden",node.id);
						      },
					     }
					}
			}
		}else{
			delete this.suggest;
		}
		
	};
	
};
//webix.extend(webix.ui.datatable, webix.ProgressBar);
//webix.extend($$("searchForm").elements["sr_id"], webix.ProgressBar);
webix.extend(webix.ui.text, webix.ProgressBar);

/**
 * Text 필드 검증 함수
 * @param {} props
 */
nkia.ui.validator.text = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] text 필드에 name property가 없습니다."
		});
		return;
	}
};

/**
 * createTextFieldComp
 *  텍스트 필드 생성
 * @param {} textProps
 * @return {}
 */
function createTextFieldComp(textProps) {
	var props = textProps||{};
	var text = {};
	try {
		// Body 영역
		text = new nkia.ui.fields.Text();
		text.setProps(props);
	} catch(e) {
		alert(e.message);
	}
	return text;
}

/********************************************
 * number
 * Version:
 *	1.0 - Text Field 지원
 ********************************************/
nkia.ui.fields.number = function() {
	// Webix Properties
	// 속성을 초기화합니다.
	this.view = "text";
	this.id = "";
	this.label = "";
	this.name = "";
	this.required = false;
	this.disabled = false;
	this.readonly = false;
	this.value = "";
	this.placeholder = "";
	this.hidden = false;
	this.gravity = 1;
	this.bottomLabel = "";
	this.click = "";
	this.hotkey = "";
	this.inputType = "";
	this.attributes = {};
	this.on = {};
	this.maxlength = "";
	this.maxsize = "";
	this.css = "";
	this.decimalSize = 1;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.width = 0;
	this.isProcAfterHide = false;

	// Custom Properties
	this.visible = true;
	
	this.propsValidate = function(props) {
		nkia.ui.validator.text(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

//		if(props.readonly !== undefined){
//			this.disabled = props.readonly;
//		}
		
		if(this.width === 0){
			delete this.width;
		}
		
		if(this.readonly === true) {
			this.css = "nkia_field_readonly";
		}

		if(this.bottomLabel === ""){
			delete this.bottomLabel;
		}
		
		/* 길이제한 주석처리 _ 20171030 좌은진
		if(props.maxlength !== undefined){
			this.attributes.maxlength = props.maxlength;
		}
		*/

		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		this.format = "1";
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
		
		/*
		if(this.required && this.invalidMessage === ""){
			this.invalidMessage = this.label + " 을[를] 입력해주십시오.";
		}
		*/
	};
	
};

/**
 * 넘버 필드 검증 함수
 * @param {} props
 */
nkia.ui.validator.number = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] text 필드에 name property가 없습니다."
		});
		return;
	}
};

/**
 * createNumberFieldComp
 *  넘버 필드 생성
 * @param {} textProps
 * @return {}
 */
function createNumberFieldComp(numberProps) {
	var props = numberProps||{};
	var text = {};
	try {
		// Body 영역
		text = new nkia.ui.fields.number();
		text.setProps(props);
	} catch(e) {
		alert(e.message);
	}
	return text;
}
/********************************************
 * TextArea
 * Date: 2016-11-17
 * Version:
 *	1.0 - TextArea 지원
 ********************************************/
nkia.ui.fields.TextArea = function() {
	// Webix Properties
	this.view = "textarea";
	this.id = "";
	this.label = "";
	this.name = "";
	this.width = 0;
	this.height = 0;
	this.required = false;
	this.disabled = false;
	this.readonly = false;
	this.value = "";
	this.placeholder = "";
	this.hidden = false;
	this.maxlength = "";
	this.maxsize = "";
	this.css = "";
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.defaultValue = "";
	this.isProcAfterHide = false;

	// Custom Properties
	this.visible = true;

	this.propsValidate = function(props) {
		nkia.ui.validator.textarea(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

//		if(props.readonly !== undefined){
//			this.disabled = props.readonly;
//		}
		
		if(this.readonly === true) {
			this.css = "nkia_field_readonly";
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		if(this.placeholder != ""){
			this.placeholder = nkia.ui.utils.convert.linebreak(this.placeholder);	
		}
		
		if(this.defaultValue != ""){
			this.defaultValue = nkia.ui.utils.convert.linebreak(this.defaultValue);	
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};
};

/**
 * Resize Textarea 생성
 */
nkia.ui.fields.ResizeTextArea = function() {
	// Custom Properties
	this.name = "";
	this.rows = [];
	this.resizable = false;
	this.originHeight = 0;

	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.resizeRender = function(props){
		var _this = this;
		var rows = [], cols = [];
		
		if(typeof props.height == "undefined") {
			props["height"] = 100;
		}
		
		var textarea = new nkia.ui.fields.TextArea();
		textarea.setProps(props);
		cols.push(textarea);

		// Resize 버튼 생성
		rows.push({ view: "label", height: 19 });
		rows.push(createBtnComp({ type:"form", label: "▲", exclude: true, on: { onItemClick: function () { _this.resize(this, 1);} }}));
		rows.push(createBtnComp({ label: "▼", exclude: true, on: { onItemClick: function () { _this.resize(this, 3); } }}));

		cols.push({
			width: 36,
			rows: rows
		});
		
		this.rows.push({
			cols: cols
		});
	};

	this.resize = function(button, multiple){
		
		var textarea = button.getFormView().elements[this.name];
		
		// 폼 크기 재조정을 위한 TextArea Height를 제외한 Form Height를 기준으로 변수 선언
		var parentForm = $$(this.formId);
		var formBaseHeight = -1;
		if(parentForm) {
			formBaseHeight = parentForm.$height - textarea.config.height;
		}
		
		if(this.originHeight === 0){
			this.originHeight = textarea.config.aheight + 22; // aheight + 22 = height
		}
		textarea.config.height = this.originHeight * multiple;

		// 버튼 컬러 체인지
		var buttonView = button.$view;
		var previousView = buttonView.previousSibling;
		var nextView = buttonView.nextSibling;
	    buttonView.getElementsByTagName("button")[0].className = "webixtype_form";
		while(previousView){
			if(previousView.getElementsByTagName("button").length > 0){
				previousView.getElementsByTagName("button")[0].className = "webixtype_base";
			}
			previousView = previousView.previousSibling;	
		}
		while(nextView){
			if(nextView.getElementsByTagName("button").length > 0){
				nextView.getElementsByTagName("button")[0].className = "webixtype_base";
			}
			nextView = nextView.nextSibling;	
		}
		
		// 폼 크기 재조정
		if(formBaseHeight > -1) {
			parentForm.define("height", formBaseHeight + textarea.config.height);
			parentForm.resize();
		}
		
		// Resize시에 값에 html tag가 있으면, tag때문에 text가 잘리는 현상이 발견되어 조치
		// 먼저 값을 가져오고, clear한 다음 값을 셋팅해주는 로직으로 해결
		var value = null;
		if(parentForm){
			value = parentForm._getFieldValue(this.name);
		}
		
		if(value != null && value != ""){
			parentForm._setFieldValue(this.name, "");
		}
		
		textarea.resize();
		
		if(value != null && value != ""){
			parentForm._setFieldValue(this.name, value);
		}
		
	};
};

//프라퍼티 검증함수
nkia.ui.validator.textarea = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] textarea 필드에 name property가 없습니다."
		});
		return;
	}
};

/**
 * createTextAreaFieldComp
 *  TextArea Field 생성
 * @param {} textareaProps
 * @return {}
 */
function createTextAreaFieldComp(textareaProps) {
	var props = textareaProps||{};
	var textarea = {};
	try {
		// Body 영역
		if(typeof props.resizable !== undefined && props.resizable){
			textarea = new nkia.ui.fields.ResizeTextArea();
			textarea.setProps(props);
			textarea.resizeRender(props);
		}else{
			textarea = new nkia.ui.fields.TextArea();
			textarea.setProps(props);
		}
	} catch(e) {
		alert(e.message);
	}
	return textarea;
}

/********************************************
 * Select
 * Date: 2016-11-17
 * Version:
 *	1.0 - Select box 지원
 ********************************************/

webix.protoUI({
    name:"select",
    defaults: {
        template: function(t, e) {
            var i = e.Be(t.options),
                s = "x" + webix.uid(),
                n = e.qt("select") + "id='" + s + "' style='width:" + e.Ee(t) + "px;'>",
                a = webix.$$(i);
            if (a && a.data && a.data.each) a.data.each(function(e) {
                n += "<option" + (e.id == t.value ? " selected='true'" : "") + " value='" + e.id + "' title='" +e.title +"'>" + e.value + "</option>";
            });
            else
                for (var r = 0; r < i.length; r++) n += "<option" + (i[r].id == t.value ? " selected='true'" : "") + " value='" + i[r].id + "' title='" +i[r].title +"'>" + i[r].value + "</option>";
            return n += "</select>", e.$renderInput(t, n, s)
        }
    }
}, webix.ui.select);


nkia.ui.fields.Select = function() {
	// Webix Properties
	//속성을 초기화합니다.
	this.view = "select";
	this.id = "";
	this.label = "";
	this.name = "";
    this.options = [];
	this.on = {};
	this.required = false;
	this.disabled = false;
	//this.readonly = false; // Readonly 속성이 동작하지 않음.
	this.hidden = false;
	this.width = 0;
	this.value = "";

	// Custom Properties
	this.url = "/itg/base/searchCodeDataList.do";
	this.displayField = "code_text";
	this.valueField = "code_id";
    this.code_grp_id = "";
    this.params = {};
	this.attachChoice = false;
	this.attachAll = false;
	this.visible = true;
	this.filteredData = "";
	this.attachNone = false;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;
	this.bottomLabel = "";

	this.propsValidate = function(props) {
		nkia.ui.validator.select(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

		if(props.readonly !== undefined){
			this.disabled = props.readonly;
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		if(this.bottomLabel === ""){
			delete this.bottomLabel;
		}
		
		if(this.width === 0){
			delete this.width;
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};
 
	this.attachEvent = function() {
		this.on["onBeforeRender"] = webix.once(function(){
			var _this = this;
			this._setDataBinding(_this);
		});
	};

};

//프라퍼티 검증함수
nkia.ui.validator.select = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] select 필드에 name property가 없습니다."
		});
		return;
	}
};

//컴포넌트 확장영역
webix.extend(webix.ui.select, {
	_getText: function() {
		var _this = this;
		var items = _this.config.options;
		var codeValue = _this.config.value;
		var textValue = "";
		if(items != "undefined" && items != null) {
			if(codeValue==null || codeValue==="") {
				textValue = items[0].value;
			} else {
				for(var i=0; i<items.length; i++) {
					if(items[i].id == codeValue) {
						textValue = items[i].value;
						break;
					}
				}
			}
		}
		return "" + textValue;
	},
	_reload: function(params) {
		var _this = this;
		
		_this.config.params = params;
		if("code_grp_id" in params){
			_this.config.code_grp_id = params.code_grp_id;
		}
		
		this._setDataBinding(_this);
		var items = _this.config.options;
		if(items != "undefined" && items != null) {
			_this.setValue(items[0].id);
		}
	},
	_setDataBinding: function(view){
		if("code_grp_id" in view.config){
			view.config.params.code_grp_id = view.config.code_grp_id;
		}

		webix.ajax().sync().post(
			nkia.host + view.config.url,
			JSON.stringify(view.config.params),
			function (text) {
				var response = JSON.parse(text);
				var options = [];

				var rows = response.gridVO.rows;
				if(rows.length > 0){
					if(view.config.attachChoice){
						options.push({ id: "", value: getConstText({isArgs : true, m_key : 'res.common.combo.select'}), title: "선택"  });
					}

					if(view.config.attachAll){
						options.push({ id: "", value: getConstText({isArgs : true, m_key : 'res.common.combo.all'}), title: "전체"  });
					}
					
					if(view.config.attachNone){
						options.push({ id: "", value: "----", title: "----"  });
					}
					
					if(view.config.filteredData) {
						rows.forEach(function(row){
							// 필터 데이터에 있는 값이면 store에 추가하지 않는다.
							var filter = view.config.filteredData;
							var isFiltered = false;
							for(var i in filter) {
								if(row[view.config.valueField] === filter[i]) {
									isFiltered = true;
									break;
								}
							}
							if(!isFiltered){
									options.push({
										id: row[view.config.valueField],
										value: row[view.config.displayField],
										title: row[view.config.displayField]
									});
								}
						});
					} else {
						rows.forEach(function(row){
							options.push({
								id: row[view.config.valueField],
								value: row[view.config.displayField],
								title: row[view.config.displayField]
							});
						});
					}

				}else{
					// 코드 데이터가 없을 시, 아래 알림을 주고 싶으면 주석 해제
//					nkia.ui.utils.notification({
//						type: "error",
//						message: "[" + view.config.label + "] 데이터가 없습니다."
//					});
					
					if(view.config.attachChoice){
						options.push({ id: "", value: getConstText({isArgs : true, m_key : 'res.common.combo.select'}), title: "선택"  });
					}

					if(view.config.attachAll){
						options.push({ id: "", value: getConstText({isArgs : true, m_key : 'res.common.combo.all'}), title: "전체"  });
					}
					
					if(view.config.attachNone){
						options.push({ id: "", value: "----", title: "----"  });
					}
				}
				view.data.options = options;
				view.refresh();
			}
		);
	}
});

/**
 * createCodeComboBoxComp
 *  콤보박스 생성
 * @param {} selectProps
 * @return {}
 */
function createCodeComboBoxComp(selectProps) {
	var props = selectProps||{};
	var select = {};
	try {
		// Body 영역
		select = new nkia.ui.fields.Select();
		select.setProps(props);
		select.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	return select;
}

/********************************************
 * Radio
 * Date: 2016-11-17
 * Version:
 *	1.0 - Radio 지원
 ********************************************/
nkia.ui.fields.Radio = function() {
	// Webix Properties
	this.view = "radio";
	this.id = "";
	this.label = "";
	this.name = "";
	this.options = [{id: " ", value: " "}];	// 초기값이 없으면 오류발생(버그)
	this.on = {};
	this.required = false;
	this.disabled = false;
	//this.readonly = false; // Readonly 속성이 동작하지 않음.
	this.vertical = false;
	this.hidden = false;

	// Custom Properties
	this.url = "/itg/base/searchCodeDataList.do";
	this.displayField = "code_text";
	this.valueField = "code_id";
    this.code_grp_id = "";
    this.params = {};
	this.visible = true;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;
	this.filteredData = "";

	this.propsValidate = function(props) {
		nkia.ui.validator.radio(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

		if(props.readonly !== undefined){
			this.disabled = props.readonly;
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};

	this.attachEvent = function() {
		this.on["onBeforeRender"] = webix.once(function(){
			var _this = this;
			setDataBinding(_this);
		});
		this.on["onItemClick"] = function(id, e){
			var trg = e.target||e.srcElement;
			if(trg != this.getInputNode()) {
				webix.html.preventEvent(event);
			}
		};
	};
	
	function setDataBinding(view){
		if("code_grp_id" in view.config){
			view.config.params.code_grp_id = view.config.code_grp_id;
		}

		webix.ajax().sync().post(
			nkia.host + view.config.url,
			JSON.stringify(view.config.params),
			function (text) {
				var response = JSON.parse(text);
				var options = [];

				var rows = response.gridVO.rows;
				if(rows.length > 0){
					
					if(view.config.filteredData) {
						rows.forEach(function(row){
							// 필터 데이터에 있는 값이면 store에 추가하지 않는다.
							var filter = view.config.filteredData;
							var isFiltered = false;
							for(var i in filter) {
								if(row[view.config.valueField] === filter[i]) {
									isFiltered = true;
									break;
								}
							}
							if(!isFiltered){
									options.push({
										id: row[view.config.valueField],
										value: row[view.config.displayField]
									});
								}
						});
					} else {
						rows.forEach(function(row, index){
							options.push({
								id: row[view.config.valueField],
								value: row[view.config.displayField]
							});
						});
					}

					if (view.config.value == ""||view.config.value == undefined) {
						// radio에서 첫번째 선택되게 설정
						view.config.value = options[0].id;
					}

				}else{
					webix.message({
						type:"error",
						text: "[" + view.config.label + "] 데이터가 없습니다.",
						expire:-1
					});
				}
				view.data.options = options;
			}
		);
	}
};

//프라퍼티 검증함수
nkia.ui.validator.radio = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] radio 필드에 name property가 없습니다."
		});
		return;
	}
};

//컴포넌트 확장영역
webix.extend(webix.ui.radio, {
	_getText: function() {
		var _this = this;
		var items = _this.config.options;
		var codeValue = _this.config.value;
		var textValue = "";
		if(items != "undefined" && items != null) {
			for(var i=0; i<items.length; i++) {
				if(items[i].id == codeValue) {
					textValue = items[i].value;
					break;
				}
			}
		}
		return "" + textValue;
	}
});


/**
 * @method {} createCodeRadioComp : Radio Function
 * @property {string} id		  : Radio Id
 *
 * Webix Radio가 Afterrender에서 DataBinding이 안되어서, beforerender에 데이터 바인딩 구현.
 */
function createCodeRadioComp(radioProps) {
	var props = radioProps||{};
	var radio = {};
	try {
		// Body 영역
		radio = new nkia.ui.fields.Radio();
		radio.setProps(props);
		radio.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	return radio;
}

/********************************************
 * DatePicker
 * Date: 2018-02-22
 * Version:
 *	1.0 - DatePicker 지원
 *	2.0 - Webix5 고도화 커스터마이징 작업
 ********************************************/
webix.protoUI({
    name:"datepicker",
    $setValue: function(t) {
        var e = webix.$$(this.s.popup.toString()),
            i = e.gd;
        t = this.Tx(t), i.selectDate(t, !0), this.s.value = t ? i.config.date : "", this.s.text = t ? this.MD(this.s.value) : "", this.ND();
    },
    getValue:function(){
    	var t = this.s.type,
            e = "time" == t,
            i = this.config.timepicker,
            s = this.s.value;
        if (this.se) {
            if (this.s.editable) {
                var n = this.Gx || (e ? webix.i18n.timeFormatDate : i ? webix.i18n.fullDateFormatDate : webix.i18n.dateFormatDate);
                s = n(this.getInputNode().value)
            }
        } else s = this.Tx(s) || null;
        
        var form = this.getFormView();
        var hour = "";
        var minute = "";
        if (this.s.dateType.indexOf('H') != -1) {
			hour = form._getFieldValue(this.s.name + '_hour');
		}

		if (this.s.dateType.indexOf('M') != -1) {
			minute = form._getFieldValue(this.s.name + '_min');
		}
        
        if (this.s.stringResult) {
            var a = webix.i18n.parseFormatStr;
            return e && (a = webix.i18n.parseTimeFormatStr), !this.Hx || "month" != t && "year" != t || (a = this.Hx), s ? a(s) + hour + minute : "";
        	//return result.join(this._settings.separator);
        }
        
        return s || null
	},
	Tx: function(t) {
	    var e = this.s.type,
		i = "time" == e;
	    if (isNaN(parseFloat(t)) || (t = "" + t), "string" == typeof t && t) {
		var s = null;
		s = "month" != e && "year" != e || !this.Gx ? i ? webix.i18n.parseTimeFormatDate : webix.i18n.parseFormatDate : this.Gx,
		    t = s(t)
	    }
	    if (t) {
		if (i && webix.isArray(t)) {
		    var n = new Date;
		    n.setHours(t[0]), n.setMinutes(t[1]), t = n
		}
		isNaN(t.getTime()) && (t = "")
	    }
	    return t
	},
	MD: function(t) {
	    var e = "time" == this.s.type,
		i = this.config.timepicker,
		s = this.Hx || (e ? webix.i18n.timeFormatStr : i ? webix.i18n.fullDateFormatStr : webix.i18n.dateFormatStr);
	    return s(t);
	},
	ND: function() {
	    var t = this.getInputNode();
	    t.value == webix.undefined ? t.innerHTML = this.s.text || this.Yx() : t.value = this.s.text || ""
	},
	$prepareValue:function(value){
		// Date Set Value 처리
    	if(typeof value === "string" && value.length >= 14){
    		value = value.substr(0, 10);
    	}
		if (this.s.multiselect){
			if (typeof value === "string")
				value = value.split(this.s.separator);
			else if (value instanceof Date){
				value = [value];
			} else if (!value){
				value = [];
			}

			for (var i = 0; i < value.length; i++){
				value[i] = this._prepareSingleValue(value[i]);
			}

			return value;
		} else{ 
			return this._prepareSingleValue(value);
			return value;
		}
	},
	_prepareSingleValue:function(value){
		var type = this.s.type;
		var timeMode = type == "time";

		//setValue("1980-12-25")
		if(!isNaN(parseFloat(value)))
			value = ""+value;

		if (typeof value=="string" && value){
			var formatDate = null;
			if((type == "month" || type == "year") && this._formatDate){
				formatDate = this._formatDate;
			}
			else
				formatDate = (timeMode?webix.i18n.parseTimeFormatDate:webix.i18n.parseFormatDate);
			value = formatDate(value);
		}

		if (value){
			//time mode
			if(timeMode){
				//setValue([16,24])
				if(webix.isArray(value)){
					var time = new Date();
					time.setHours(value[0]);
					time.setMinutes(value[1]);
					value = time;
				}
			}
			//setValue(invalid date)
			if(isNaN(value.getTime()))
				value = "";
		}

		return value;
	}
}, webix.ui.datepicker);

nkia.ui.fields.DatePicker = function() {
	// Webix Properties
	//속성을 초기화합니다.
	this.view = "datepicker";
	this.id = "";
	this.label = "";
	this.name = "";
	this.type = "calendar";
	this.required = false;
	this.disabled = false;
	this.readonly = false;
	this.timepicker = false;
	this.format = "%Y-%m-%d";
	this.value = "";
	this.placeholder = "";
	this.stringResult = true;
	this.gravity = 1;
	this.hidden = false;
	this.on = {};
	this.css = "";
	// Custom Properties
	this.dateType = "";
	this.dateValue = "";
	this.visible = true;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;

	this.propsValidate = function(props) {
		nkia.ui.validator.datepicker(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		
		if(props.dateType == "yyyy"){
			this.type = "year";
			this.format = "%Y";
		} else if(props.dateType == "mm"){
			this.type = "month";
			this.format = "%Y-%m";
		}

//		if(props.readonly){
//			this.disabled = props.readonly;
//		}
		
		if(this.readonly === true) {
			this.css = "nkia_field_readonly";
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		if(props.label == "~"){
			this.labelAlign = "center";
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};
};

//프라퍼티 검증함수
nkia.ui.validator.datepicker = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] DatePicker 필드에 name property가 없습니다."
		});
		return;
	}
};

nkia.ui.fields.Date = function() {
	// Webix Properties

	// Custom Properties
	this.hourUnit = 1;
	this.minuteUnit = 1;
	this.dateType = "";
	this.dateValue = {};
	this.rows = [];
	this.hideSubLabel = false;
	this.timeFieldType = "combo";
	this.isProcAfterHide = false;

	this.propsValidate = function(props) {
		nkia.ui.validator.date(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props){
		var cols = [];
		var timepickerCols = [];
		
		if(this.dateType === 'yyyy') {
			var datePicker = new nkia.ui.fields.DatePicker();
			datePicker.setProps(props);
			cols.push(datePicker);
		} else if(this.dateType === 'mm') {
			var datePicker = new nkia.ui.fields.DatePicker();
			datePicker.setProps(props);
			cols.push(datePicker);
		} else { 
			if (this.dateType.indexOf('D') != -1) {
				var datePicker = new nkia.ui.fields.DatePicker();
				if(this.dateType.indexOf('H') != -1 || this.dateType.indexOf('M') != -1){
					props.gravity = 2;
				}
				if(this.dateValue.day){
					props.value = this.dateValue.day;
				}
				
				datePicker.setProps(props);
				cols.push(datePicker);
			}
			
			if (this.dateType.indexOf('H') != -1) {
				var hour = new nkia.ui.fields.Select();
				var hourProps = {
					label: "시",
					id: props.name + '_hour',
					name: props.name + '_hour',
					required: props.required,
					readonly: props.readonly,
					visible: props.visible,
					options: this.getOptions(24, this.hourUnit),
					value: this.dateValue.hour || "00",
					isProcAfterHide: this.isProcAfterHide
				};
				
				if(this.timeFieldType === "text") {
					
					hour = new nkia.ui.fields.Text();
					
					delete hourProps.options;
					
					hourProps["inputType"] = "time";
					
					hourProps["value"] = this.dateValue.hour || "00";
					
					hourProps["on"] = {
						onChange: function(newVal, oldVal) {
							if(newVal != "" && newVal != 0) {
								if(newVal < 0 || newVal > 23){
									nkia.ui.utils.notification({
										type: "error",
										message: "0 ~ 23 까지 입력가능합니다."
									});
									webix.html.addCss(this.getNode(), 'webix_invalid');
									this.setValue("00");
								} else {
									webix.html.removeCss(this.getNode(), 'webix_invalid');
									var value = Math.floor(newVal);
									if(value < 10) {
										value = "0"+value
									}
									this.setValue(value);
								}
							} else {
								this.setValue("00");
							}
							return false;
						},
						onKeyPress: function(keycode, e) {
							// '시' 필드의 입력이 끝나면 '분' 필드로 자동 포커스
							var value = this.getValue();
							if(value.length == 2) {
								var parentForm = this.getFormView();
								var minField = parentForm._getField(props.name + '_min');
								if(minField) {
									minField.focus();
								}
							}
						}
					};
				}
				
				// 보조 라벨(시,분) 숨기기 옵션일 때, 보조 라벨 만들지 않음
				if(this.hideSubLabel === true) {
					delete hourProps["label"];
				}
				hour.setProps(hourProps);
				timepickerCols.push(hour);
				/*
				timepickerCols.push({
					view:"label",
	    			label: "시"
				});
				*/
			}
			
			if (this.dateType.indexOf('M') != -1) {
				var minute = new nkia.ui.fields.Select();
				var minuteProps = {
					label: "분",
					id: props.name + '_min',
					name: props.name + '_min',
					required: props.required,
					readonly: props.readonly,
					visible: props.visible,
					options: this.getOptions(60, this.minuteUnit),
					value: this.dateValue.min || "00",
					isProcAfterHide: this.isProcAfterHide
				};
				
				if(this.timeFieldType === "text") {
					
					minute = new nkia.ui.fields.Text();
					
					delete minuteProps.options;
					
					minuteProps["inputType"] = "time";
					
					minuteProps["value"] = this.dateValue.min || "00";
					
					minuteProps["on"] = {
						onChange: function(newVal, oldVal) {
							if(newVal != "" && newVal != 0) {
								if(newVal < 0 || newVal > 59){
									nkia.ui.utils.notification({
										type: "error",
										message: "0 ~ 59 까지 입력가능합니다."
									});
									webix.html.addCss(this.getNode(), 'webix_invalid');
									this.setValue("00");
									return false;
								} else {
									webix.html.removeCss(this.getNode(), 'webix_invalid');
									var value = Math.floor(newVal);
									if(value < 10) {
										value = "0"+value
									}
									this.setValue(value);
								}
							} else {
								this.setValue("00");
							}
							return false;
						}
					};
				}
				
				// 보조 라벨(시,분) 숨기기 옵션일 때, 보조 라벨 만들지 않음
				if(this.hideSubLabel === true) {
					delete minuteProps["label"];
				}
				minute.setProps(minuteProps);
				timepickerCols.push(minute);
				/*
				timepickerCols.push({
					view:"label",
	    			label: "분"
				});
				*/
			}
		}

		if(timepickerCols.length > 0){
			/*
			var fieldset = {
				view: "nkia.fieldset",
				body: {
					rows: [{
						cols: timepickerCols
					}]
				}
			};
			*/
			var fieldset = {
				cols: timepickerCols
			};
			cols.push(fieldset);
		}

		this.rows.push({
			cols: cols
		});
	};

	this.getOptions = function(count, unit){
		var options = [];
		var value = 0, displayValue;
		count = count / unit;
		for (var i = 0; i < count; i++) {
			displayValue = (value < 10) ? "0" + value : value;
			value = value + unit;
			options.push({ id : displayValue, value : displayValue });
		}
		return options;
	};
};

//프라퍼티 검증함수
nkia.ui.validator.date = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] Date 필드에 name property가 없습니다."
		});
		return;
	}
};

/**
 * @method {} createDateFieldComp : DatePicker Function
 * @property {string} id			: DatePicker Id
 */
function createDateFieldComp(dateProps) {
	var props = dateProps||{};
	var date = {};
	try {
		// Body 영역
		date = new nkia.ui.fields.Date();
		date.setProps(props);
		date.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return date;
}

nkia.ui.fields.SearchDate = function() {
	// Webix Properties

	// Custom Properties
	this.id = "";
	this.label = "";
	this.name = "";
	this.rows = [];
	this.visible = true;
	this.start_dateValue = {};
	this.end_dateValue = {};
	this.on = {};
	this.dateType = "D";
	this.emptySubLabel = false;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;

	this.propsValidate = function(props) {
		nkia.ui.validator.searchdate(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props){
		var cols = [];
		var startDatePicker = new nkia.ui.fields.DatePicker();
		startDatePicker.setProps(this.getDatePickerProps(props, true, "_startDate"));
		cols.push(startDatePicker);

		var endDatePicker = new nkia.ui.fields.DatePicker();
		endDatePicker.setProps(this.getDatePickerProps(props, false, "_endDate"));
		cols.push(endDatePicker);

	 	this.rows.push({
			cols: cols
		});
	};

	this.getDatePickerProps = function(props, isLabel, suffixName){
		var startValue = "", endValue = "";
		if(props.start_dateValue){
			startValue = this.start_dateValue.day;
		}
		if(props.end_dateValue){
			endValue = this.end_dateValue.day;
		}
		
		// From-To 날짜의 endDate에는 벨리데이션이 걸리지 않도록 함
		var isRequired = false;
		if(suffixName == "_endDate") {
			isRequired = false;
		} else {
			if(props.required) {
				isRequired = props.required
			}
		}
		
		if(isLabel) {
			// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
			if(props.emsYn){
				if( props.amValue == props.emsValue ){
					props.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + props.label;
				}else{
					props.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + props.label;
				}
			}
			
			if(props.dcaYn){
				if( props.amValue == props.dcaValue ){
					props.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + props.label;
				}else{
					props.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + props.label;
				}
			}
			
			if(props.helper){
				props.label = props.label 
				+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
				+ props.helper 
				+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
				+ this.helperWidth + "," + this.helperHeight
				+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
			}
		}
		
		var datePickerProps = {
//			label: (isLabel) ? props.label : (props.label) ? "~" : "",
			label: (isLabel) ? props.label : (props.label) ? ( (props.emptySubLabel) ? "" : "~" ) : "",
			placeholder: (isLabel) ? "From" : "To",
			name: props.name + suffixName,
			elementsConfig: props.elementsConfig,
			dateType: (props.dateType) ? (props.dateType) : "D",
			value: (isLabel) ? startValue : endValue,
			required: isRequired,
			readonly: props.readonly,
			visible: props.visible,
			gravity: (isLabel) ? 1 : 1
		};
		return datePickerProps;
	};
	
	// From-To 날짜필드의 경우 시작일과 종료일에 대한 벨리데이션을 onChange 이벤트에 걸어준다.
	this.attachEvent = function() {
		
		var _startDateFieldProp = this.rows[0].cols[0];
		var _endDateFieldProp = this.rows[0].cols[1];
		
		_startDateFieldProp.on["onChange"] = function(newVal, oldVal){
			
			var startDateField = $$(_startDateFieldProp.id);
			var endDateField = $$(_endDateFieldProp.id);
			
			if(startDateField !== "") {
				if(endDateField.getValue() === "") {
					endDateField.setValue(startDateField.getValue());
				} else {
					var startDateVal = startDateField.getValue();
					var endDateVal = endDateField.getValue();
					if(new Date(startDateVal) > new Date(endDateVal)) {
						alert("시작일은 종료일 이전으로 선택 가능합니다.");
						startDateField.setValue(oldVal);
					}
				}
			}
		};
		
		_endDateFieldProp.on["onChange"] = function(newVal, oldVal){
			
			var startDateField = $$(_startDateFieldProp.id);
			var endDateField = $$(_endDateFieldProp.id);
			
			if(endDateField !== "") {
				if(startDateField.getValue() === "") {
					startDateField.setValue(endDateField.getValue());
				} else {
					var startDateVal = startDateField.getValue();
					var endDateVal = endDateField.getValue();
					if(new Date(startDateVal) > new Date(endDateVal)) {
						alert("종료일은 시작일 이후로 선택 가능합니다.");
						endDateField.setValue(oldVal);
					}
				}
			}
		};
	};
};

//프라퍼티 검증함수
nkia.ui.validator.searchdate = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] Search Date 필드에 name property가 없습니다."
		});
		return;
	}
};

/**
 * @method {} createSearchDateFieldComp : DatePicker Function
 * @property {string} id			: DatePicker Id
 */
function createSearchDateFieldComp(searchdateProps){
	var props = searchdateProps||{};
	var searchdate = {};
	try {
		// Body 영역
		searchdate = new nkia.ui.fields.SearchDate();
		searchdate.setProps(props);
		searchdate.setRender(props);
		searchdate.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	return searchdate;
}

/********************************************
 * Checkbox
 * Date: 2016-11-17
 * Version:
 *	1.0 - Checkbox 지원
 ********************************************/
nkia.ui.fields.Checkbox = function() {
	// Webix Properties
	this.view = "checkbox";
	this.id = "";
	this.label = "";
	this.name = "";
	this.required = false;
	this.disabled = false;
	//this.readonly = false; // Readonly 속성이 동작하지 않음.
	this.hidden = false;
	this.value = "";
	this.checkValue = "1";
	this.uncheckValue = "0";
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.on = {};
	this.isProcAfterHide = false;
	this.labelRight ="";

	// Custom Properties
	this.visible = true;

	this.propsValidate = function(props) {
		nkia.ui.validator.checkbox(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

		if(props.readonly){
			this.disabled = props.readonly;
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};
	
	this.attachEvent = function() {
		this.on["onItemClick"] = function(id, e){
			var trg = e.target||e.srcElement;
			if(trg != this.getInputNode()) {
				webix.html.preventEvent(event);
			}
		};
	};
};

//프라퍼티 검증함수
nkia.ui.validator.checkbox = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] checkbox 필드에 name property가 없습니다."
		});
		return;
	}
};


/**
 * @method {} createSingleCheckBoxFieldComp : Single Checkbox Function
 * @property {string} id			: Single Checkbox Id
 */
function createSingleCheckBoxFieldComp(checkboxProps) {
	var props = checkboxProps||{};
	var checkbox = {};
	try {
		// Body 영역
		checkbox = new nkia.ui.fields.Checkbox();
		checkbox.setProps(props);
		checkbox.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	return checkbox;
}

/********************************************
 * MultiSelect
 * Date: 2016-11-17
 * Version:
 *	1.0 - Checkbox 지원
 ********************************************/
nkia.ui.fields.MultiSelect = function() {
	// Webix Properties
	this.view = "multicombo";
	this.id = "";
	this.label = "";
	this.name = "";
	this.required = false;
	this.disabled = false;
	this.readonly = false;
	this.hidden = false;
	this.options = []; //멀티셀렉트시
	//this.suggest = [];   //멀티콤보시
	this.on = {};
	this.css = "";

	// Custom Properties
	this.url = "/itg/base/searchCodeDataList.do";
	this.displayField = "CODE_TEXT";
	this.valueField = "CODE_ID";
    this.code_grp_id = "";
    this.params = {};
	this.visible = true;
	this.filteredData = "";
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;

	this.propsValidate = function(props) {
		nkia.ui.validator.multiselect(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

//		if(props.readonly){
//			this.disabled = props.readonly;
//		}
		
		if(this.readonly === true) {
			this.css = "nkia_field_readonly";
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};

	this.dataBindig = function(props) {
		var _this = this;
		var params = _this.params||{};
		if(_this.code_grp_id !== ""){
			params.code_grp_id = _this.code_grp_id;
		}
		webix.ajax().sync().post(
			nkia.host + _this.url,
			JSON.stringify(params),
			function (text) {
				var response = JSON.parse(text);
				var options = [];

				var rows = response.gridVO.rows;
				if(rows.length > 0){
					if(_this.filteredData) {
						rows.forEach(function(row){
							// 필터 데이터에 있는 값이면 store에 추가하지 않는다.
							var filter = _this.filteredData;
							var isFiltered = false;
							for(var i=0; i<filter.length; i++) {
								if(row[_this.valueField] === filter[i]) {
									isFiltered = true;
									break;
								}
							}
							if(!isFiltered) {
								options.push({
									id: row[_this.valueField],
									value: row[_this.displayField],
									title: row[_this.displayField]
								});
							}
						});
					} else {
						rows.forEach(function(row){
							options.push({
								id: row[_this.valueField],
								value: row[_this.displayField]
							});
						});
					}
					
				}else{
					nkia.ui.utils.notification({
						type: "error",
						message: "[" + this.label + "] 데이터가 없습니다."
					});
				}
				_this.options = options;
			}
		);
	};
};

//프라퍼티 검증함수
nkia.ui.validator.multiselect = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] multiselect 필드에 name property가 없습니다."
		});
		return;
	}
};

webix.extend(webix.ui.multicombo, {
	_reload: function(params) {
		var _this = this;
		
		_this.config.params = params;
		if("code_grp_id" in params){
			_this.config.code_grp_id = params.code_grp_id;
		}
		
		this._setDataBinding(_this);
		var items = _this.config.options;
	},
	_setDataBinding: function(view){
		if("code_grp_id" in view.config){
			view.config.params.code_grp_id = view.config.code_grp_id;
		}

		webix.ajax().sync().post(
			nkia.host + view.config.url,
			JSON.stringify(view.config.params),
			function (text) {
				var response = JSON.parse(text);
				var options = [];

				var rows = response.gridVO.rows;
				var cnt = $$(view.data.id).getPopup().getList().data.order.length;
				var list = [];
				for(var z=0; z<cnt;z++){
					list.push($$(view.data.id).getPopup().getList().data.order[z]);
				}
				for(var x=0; x<list.length;x++){
					$$(view.data.id).getPopup().getList().remove(list[x]);
				}
				if(rows.length > 0){
					if(view.config.filteredData) {
						rows.forEach(function(row){
							// 필터 데이터에 있는 값이면 store에 추가하지 않는다.
							var filter = view.config.filteredData;
							var isFiltered = false;
							for(var i in filter) {
								if(row[view.config.valueField] === filter[i]) {
									isFiltered = true;
									break;
								}
							}
							if(!isFiltered){
								options.push({
									id: row[view.config.valueField],
									value: row[view.config.displayField],
									title: row[view.config.displayField]
								});
								}
						});
					} else {
						rows.forEach(function(row){
							options.push({
								id: row[view.config.valueField],
								value: row[view.config.displayField],
								title: row[view.config.displayField]
							});
						});
					}
				}
				$$(view.data.id).getPopup().getList().parse(options);
			}
		);
	}
});

/**
 * @method {} createMultiSelectFieldComp : Multi Select Function
 * @property {string} id			: Multi Select Checkbox Id
 */
function createMultiSelectFieldComp(multiselectProps) {
	var props = multiselectProps||{};
	var multiselect = {};
	try {
		// Body 영역
		multiselect = new nkia.ui.fields.MultiSelect();
		multiselect.setProps(props);
		multiselect.dataBindig();
	} catch(e) {
		alert(e.message);
	}
	return multiselect;
}

/********************************************
 * Search
 * Date: 2016-11-17
 * Version:
 *	1.0 - Search Field 지원
 ********************************************/
nkia.ui.fields.Search = function() {
	// Webix Properties
	//속성을 초기화합니다.
	this.view = "search";
	this.id = "";
	this.label = "";
	this.name = "";
	this.required = false;
	this.disabled = false;
	this.readonly = true;	// 검색필드는 기본적으로 readonly: true
	this.oriReadonly = false;
	this.hidden = false;
	this.placeholder = "";
	this.click = "";
	this.css = "";
	this.keepEvent = false; // readonly와 함께 사용, 이벤트 유지여부
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;

	// Custom Properties
	this.visible = true;

	this.propsValidate = function(props) {
		nkia.ui.validator.search(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

		if(props.readonly !== undefined && props.readonly === true){
//			this.disabled = props.readonly;
			// 팝업필드가 readonly 일 때는 click 이벤트를 제거 하고 일반 text field로 리턴한다.
			this.oriReadonly = true;
			this.css = "nkia_field_readonly";
			// 이벤트를 유지하는 속성이 없을 경우 text 필드로 리턴한다.
			if(props.keepEvent !== true) {
				this.click = "";
				this.view = "text";
			}
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};
};

//프라퍼티 검증함수
nkia.ui.validator.search = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] search 필드에 name property가 없습니다."
		});
		return;
	}
};


/**
 * @method {} createSearchFieldComp : Search Field Function
 * @property {string} id			: Search Field Id
 */
function createSearchFieldComp(searchProps) {
	var props = searchProps||{};
	var search = {};
	try {
		// Body 영역
		search = new nkia.ui.fields.Search();
		search.setProps(props);
	} catch(e) {
		alert(e.message);
	}
	return search;
}

/********************************************
 * Union: 필드 조합
 * Date: 2016-11-17
 * Version:
 *	1.0 - Union Field 지원
 ********************************************/
nkia.ui.fields.Union = function() {
	// Custom Properties
	this.rows = [];
	this.items = [];
	this.labelConfig = "left";

	this.propsValidate = function(props) {
		nkia.ui.validator.union(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function() {
		var cols = [];
		var rows = [];
		var buttonCols = [];
		if(this.labelConfig == "top"){
			this.items.forEach(function(field, index){
				if(field instanceof nkia.ui.common.Button){
					buttonCols.push(field);
				}else{
					rows.push(field);
				}
			});
			this.rows.push({
				rows: rows
			});
		}else{
			this.items.forEach(function(field, index){
				if(field instanceof nkia.ui.common.Button){
					buttonCols.push(field);
				}else{
					cols.push(field);
				}
			});
			
			// 버튼 Cols. Label Top이 되면서 생성하였음.
			if(buttonCols.length > 0){
				cols.push({ 
					rows : [{},{ 
						cols: buttonCols 
					}]
				});
			}
			
			this.rows.push({
				cols: cols
			});
		}
	};
};

//프라퍼티 검증함수
nkia.ui.validator.union = function(props) {
	if(props.items === undefined || props.items.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] Union 필드에 items property가 없습니다."
		});
		return;
	}
};


/**
 * @method {} createUnionFieldComp : Union Field Function
 * @property {string} id		   : Union Field Id
 */
function createUnionFieldComp(unionProps) {
	var props = unionProps||{};
	var union = {};
	try {
		// Body 영역
		union = new nkia.ui.fields.Union();
		union.setProps(unionProps);
		union.setRender();
	} catch(e) {
		alert(e.message);
	}
	return union;
}


/********************************************
 * Label
 * Date: 2029-10-19
 * Version:
 *	1.0 - Label 지원
 ********************************************/
nkia.ui.fields.Label = function() {
	// Webix Properties
	// 속성을 초기화합니다.
	this.view = "label";
	this.id = "";
	this.label = "";
	this.name = "";
	this.width = 80;
	//this.widthinfi = false;
	this.css = "webix_inp_label";
	// Custom Properties
	this.visible = true;
	this.required = false;
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		
		if(this.width === 0){
			delete this.width;
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		if(this.required === true){
			this.label += "<font color='red' style='padding-left:4px'>*</font>"
		}
		
	};
	
};


/**
 * createLabelComp
 *  라벨 필드 생성
 * @param {} Label
 * @return {}
 */
function createLabelComp(labelProps) {
	var props = labelProps||{};
	var label = {};
	try {
		// Body 영역
		label = new nkia.ui.fields.Label();
		label.setProps(props);
	} catch(e) {
		alert(e.message);
	}
	return label;
}

nkia.ui.fields.Combo = function() {
	// Webix Properties
	//속성을 초기화합니다.
	this.view = "combo";
	this.id = "";
	this.label = "";
	this.name = "";
    this.options = [];
    this.suggest = [];
	this.on = {};
	this.required = false;
	this.disabled = false;
	//this.readonly = false; // Readonly 속성이 동작하지 않음.
	this.hidden = false;
	this.width = 0;
	this.value = "";

	// Custom Properties
	this.url = "/itg/base/searchCodeDataList.do";
	this.displayField = "CODE_TEXT";
	this.valueField = "CODE_ID";
    this.code_grp_id = "";
    this.params = {};
	this.attachChoice = false;
	this.attachAll = false;
	this.visible = true;
	this.filteredData = "";
	this.attachNone = false;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.isProcAfterHide = false;
	this.bottomLabel = "";

	this.propsValidate = function(props) {
		nkia.ui.validator.select(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

		if(props.readonly !== undefined){
			this.disabled = props.readonly;
		}
		
		if(props.visible !== undefined){
			this.hidden = !props.visible;
		}
		
		if(this.bottomLabel === ""){
			delete this.bottomLabel;
		}
		
		if(this.width === 0){
			delete this.width;
		}
		
		// EMS/DCA 연계(자산) 라벨 앞에 아이콘 표시
		if(props.emsYn){
			if( props.amValue == props.emsValue ){
				this.label = _SYSTEM_LINKED_ICON.EMS_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.EMS_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.dcaYn){
			if( props.amValue == props.dcaValue ){
				this.label = _SYSTEM_LINKED_ICON.DCA_EQUAL_SYMBOL + this.label;
			}else{
				this.label = _SYSTEM_LINKED_ICON.DCA_UNEQUAL_SYMBOL + this.label;
			}
		}
		
		if(props.helper){
			this.label = this.label 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_PREFIX 
			+ props.helper 
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_MIDDLE
			+ this.helperWidth + "," + this.helperHeight
			+ _SYSTEM_LINKED_ICON.TOOLTIP_SYMBOL_SUFFIX;
		}
	};
 
	this.attachEvent = function() {
		this.on["onBeforeRender"] = webix.once(function(){
			var _this = this;
			this._setDataBinding(_this);
		});
	};

};

//프라퍼티 검증함수
nkia.ui.validator.select = function(props) {
	if(props.name === undefined || props.name.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "[" + props.label + "] select 필드에 name property가 없습니다."
		});
		return;
	}
};

//컴포넌트 확장영역
webix.extend(webix.ui.combo, {
	_getText: function() {
		var _this = this;
		var items = _this.config.options;
		var codeValue = _this.config.value;
		var textValue = "";
		if(items != "undefined" && items != null) {
			if(codeValue==null || codeValue==="") {
				textValue = items[0].value;
			} else {
				for(var i=0; i<items.length; i++) {
					if(items[i].id == codeValue) {
						textValue = items[i].value;
						break;
					}
				}
			}
		}
		return "" + textValue;
	},
	_reload: function(params) {
		var _this = this;
		
		_this.config.params = params;
		if("code_grp_id" in params){
			_this.config.code_grp_id = params.code_grp_id;
		}
		
		this._setDataBinding(_this);
		var items = _this.config.options;
		if(items != "undefined" && items != null) {
			_this.setValue(items[0].id);
		}
	},
	_setDataBinding: function(view){
		if("code_grp_id" in view.config){
			view.config.params.code_grp_id = view.config.code_grp_id;
		}

		webix.ajax().sync().post(
			nkia.host + view.config.url,
			JSON.stringify(view.config.params),
			function (text) {
				var response = JSON.parse(text);
				var options = [];

				var rows = response.gridVO.rows;
				if(rows.length > 0){
					rows.forEach(function(row){
						options.push({
							id: row.ID,
							value: row.VALUE
						});
					});
				}else{
					// 코드 데이터가 없을 시, 아래 알림을 주고 싶으면 주석 해제
//					nkia.ui.utils.notification({
//						type: "error",
//						message: "[" + view.config.label + "] 데이터가 없습니다."
//					});
				}
				$$(view.data.id).getPopup().getList().parse(options);
			}
		);
	}
});

/**
 * createEditComboBoxComp
 *  콤보박스 생성
 * @param {} selectProps
 * @return {}
 */
function createEditComboBoxComp(selectProps) {
	var props = selectProps||{};
	var select = {};
	try {
		// Body 영역
		select = new nkia.ui.fields.Combo();
		select.setProps(props);
		select.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	return select;
}




/**
 * 웹에디터생성
 */
nkia.ui.fields.WebEditor = function() {
	this.rows = [];
	this.view = "nic-editor";
	this.id = "";
	this.name = "";
	this.label = "";
	this.isReadOnly = "";
	this.height = 400;
	this.required = false;
	this.tableAlign = "left";
	
	// 프로퍼티 세팅
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	// 이벤트 연결
	this.attachEvent = function() {
		var _this = this;
		if(!_this.on) {
			_this.on = {};
		}
	};
	
	this.setRender = function(props){

	};
	
};


/**
 * createWebEditorFieldComp
 *  createWebEditorFieldComp Field 생성
 * @param {} textareaProps
 * @return {}
 */
function createWebEditorFieldComp(editorProps) {
	var props = editorProps||{};
	var modules = [];
	var webeditor = {};
	try {
			
		webeditor = new nkia.ui.fields.WebEditor();
		webeditor.setProps(props);
		webeditor.setRender(props);
		//webeditor.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	
	return webeditor;
}



nkia.ui.fields.Iframe = function() {
	this.rows = [];
	this.view = "iframe";
	this.id = "";
	this.name = "";
	this.label = "";
	this.src = "";
	this.height = "";
	this.width = "";
	// 프로퍼티 세팅
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	// 이벤트 연결
	this.attachEvent = function() {
		var _this = this;
		if(!_this.on) {
			_this.on = {};
		}
	};
	
	this.setRender = function(props){

	};
	
};

/**
 * createIframeFieldComp
 *  createIframeFieldComp Field 생성
 * @param {} textareaProps
 * @return {}
 */
function createIframeFieldComp(editorProps) {
	var props = editorProps||{};
	var modules = [];
	var iframe = {};
	try {
			
		iframe = new nkia.ui.fields.Iframe();
		iframe.setProps(props);
		iframe.setRender(props);
		//webeditor.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	
	return iframe;
}


// 엑셀템플릿 자동 생성(By Poi)
// *더미(공간 유지)*
// label 은 반드시 " " 로 포함되어야 함
nkia.ui.fields.Dummy = function() {
	// Webix Properties
	this.view = "";
	this.id = "";
	this.label = "";
	this.name = "";
	this.required = false;
	this.disabled = false;
	//this.readonly = false; // Readonly 속성이 동작하지 않음.
	this.hidden = false;
	this.value = "";
	this.checkValue = "1";
	this.uncheckValue = "0";
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.on = {};
	this.isProcAfterHide = false;

	// Custom Properties
	this.visible = true;

	this.propsValidate = function(props) {
		nkia.ui.validator.checkbox(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		this.label = " ";
	};
	
	this.attachEvent = function() {
		this.on["onItemClick"] = function(id, e){
		};
	};
};

function createDummy(checkboxProps) {
	var props = checkboxProps||{};
	var checkbox = {};
	try {
		// Body 영역
		checkbox = new nkia.ui.fields.Dummy();
		checkbox.setProps(props);
		checkbox.attachEvent();
	} catch(e) {
		alert(e.message);
	}
	return checkbox;
}


