var nkia = {
	host: ""
};

nkia.ui = {};
//컴포넌트의 프라퍼티 생성자 정의
nkia.ui.common = {};
nkia.ui.grid = {};
nkia.ui.tree = {};
nkia.ui.form = {};
nkia.ui.fields = {};
nkia.ui.file = {};
nkia.ui.html = {};

// 팝업 요소 관리
nkia.ui.popup = {
	nbpm: {},
	itam: {
		maintjob: {},
		excel: {},
		assetstate: {},
		assetdisuse: {},
		batchEdit: {},
		amdbauth: {}
	},
	system: {},
	workflow: {}
};

//컴포넌트의 프라퍼티 검증 함수 정의
nkia.ui.validator = {};
//util 관련 기능을 정의
nkia.ui.utils = {
	array: {},
	attach: {},
	check: {},
	convert: {}
};
//Component Caching
nkia.ui.caching = {};

// 자산관리 자동화 요소 관리
nkia.itam = {
	atm: {}
};

// npbm Component
nkia.nbpm = {
	fields: {},
	components: {
		extend: {}
	},
	validator: {}
};

// Custom 요소 설정
nkia.ui.custom = {
	design: false
};

// UI Renderer - 화면을 그리고 progress bar를 생성한다.
nkia.ui.render = function(view, fileCompIds){
	var id = view.id || "app";
	webix.ui(view);
	webix.extend($$(id), webix.ProgressBar);

	// View 객체 Caching 생성
	if(view.caching){
		nkia.ui.cache(view.caching);
	}
	      
	// 파일 Header 생성 및 Drag&Drop 
	if(view.files){
		view.files.forEach(function(file){
			$$(file)._header();
			$$(file).addDropZone($$(file + "_list").$view, "");
		});
	}
	
	// 첨부파일 객체의 ID가 직접 넘어왔을 경우 파일 header 생성 및 Drag&Drop Zone 생성
	if(fileCompIds && fileCompIds!=null) {
		for(var i=0; i<fileCompIds.length; i++) {
			var fileCompId = fileCompIds[i];
			$$(fileCompId)._header();
			$$(fileCompId).addDropZone($$(fileCompId + "_list").$view, "");
		}
	}

	if(document.getElementById("loading-mask")){
		webix.html.remove(document.getElementById("loading-mask"));
	}

	if(document.getElementById("loading")){
		webix.html.remove(document.getElementById("loading"));
	}
};

//UI Caching
nkia.ui.cache = function(components){
	components.forEach(function(component){
		nkia.ui.caching[component] = $$(component);
	});
};

webix.expansion = function(target, expansionObj) {
	if(!target) {
		target = {};
	}
	for(var prop in expansionObj) {
		//if(expansionObj[prop] instanceof Object) {
			target[prop] = expansionObj[prop];
		//}
	}
};

webix.attachEvent("onBeforeAjax",
	function(mode, url, data, request, headers, files, promise){
		if(files === null){
			headers["Content-type"]= headers["Content-type"] ? "" : "application/json";
		}
		//parent.parent.counter_reset();
	}
);

//****************** Skin 설정 ****** /
webix.skin.flat = {
	topLayout:"space",
	//bar in accordion
	barHeight:32,			//!!!Set the same in skin.less!!!
	tabbarHeight: 36,
	rowHeight:30,
	toolbarHeight:32,
	listItemHeight:36,		//list, grouplist, dataview, etc.
	inputHeight: 25,
	buttonHeight: 30,
	inputPadding: 3,
	menuHeight: 30,
	pagerHeight: 40,
	labelTopHeight: 22,
	propertyItemHeight: 28,

	inputSpacing: 4,
	borderWidth: 1,

	sliderHandleWidth: 14,
	sliderPadding: 10,
	sliderBorder: 1,
	vSliderPadding:15,
	vSliderHeight:100,
	
	//margin - distance between cells
	layoutMargin:{ space:0, wide:0, clean:0, head:0, line:-1, toolbar:0, form:3, accordion: 10  },
	//padding - distance insede cell between cell border and cell content
	layoutPadding:{ space:30, wide:10, clean:0, head:0, line:0, toolbar:3, form:2, accordion: 0  },
	//space between tabs in tabbar
	tabMargin:0,
	tabOffset: 0,
	tabBottomOffset: 6,
	tabTopOffset:1,

	customCheckbox: true,
	customRadio: true,

	popupPadding: 8,

	calendarHeight: 70,
	padding:0,
	accordionType: "accordion",

	optionHeight: 32
};

//****************** locale 설정 ****** /
webix.i18n.parseFormat = "%Y-%m-%d";

webix.i18n.locales["ko-KR"]={
  groupDelimiter:",",
  groupSize:3,
  decimalDelimiter:".",
  decimalSize:2,

  dateFormat:"%Y-%m-%d",
  timeFormat:"%h:%i %A",
  longDateFormat:"%d %F %Y",
  fullDateFormat:"%m/%d/%Y %h:%i %A",
  am:["오전","오전"],
  pm:["오후","오후"],

  price:"${obj}",
  priceSettings:{
     groupDelimiter:",",
     groupSize:3,
     decimalDelimiter:".",
     decimalSize:2
  },
  fileSize: ["b","Kb","Mb","Gb","Tb","Pb","Eb"],

  calendar: {
    monthFull:["1월", "2월", "3월", "4월", "5월", "6월",
        "7월", "8월", "9월", "10월", "11월", "12월"
    ],
    monthShort:["1월", "2월", "3월", "4월", "5월", "6월",
        "7월", "8월", "9월", "10월", "11월", "12월"
    ],
    dayFull:["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
        "Friday", "Saturday"
    ],
    dayShort:["일", "월", "화", "수", "목", "금", "토"],
    hours: "시",
    minutes: "분",
    done:"완료",
    clear: "삭제",
    today: "오늘"
  },

  controls:{
    select:"Select"
  },

  dataExport:{
    page:"Page",
    of:"of"
  },

  PDFviewer:{
        of:"of",
        automaticZoom:"Automatic Zoom",
        actualSize:"Actual Size",
        pageFit:"Page Fit",
        pageWidth:"Page Width",
        pageHeight:"Page Height"
  },

  aria:{
        increaseValue:"Increase value",
        decreaseValue:"Decrease value",
        navMonth:["Previous month", "Next month"],
        navYear:["Previous year", "Next year"],
        navDecade:["Previous decade", "Next decade"],
        removeItem:"Remove item",
        pages:["First page", "Previous page", "Next page", "Last page"],
        page:"Page",
        headermenu:"Header menu",
        openGroup:"Open column group",
        closeGroup:"Close column group",
        closeTab:"Close tab",
        showTabs:"Show more tabs",
        resetTreeMap:"Reset tree map",
        navTreeMap:"Level up",
        nextTab:"Next tab",
        prevTab:"Previous tab",
        multitextSection:"Add section",
        multitextextraSection:"Remove section",
        showChart:"Show chart",
        hideChart:"Hide chart",
        resizeChart:"Resize chart"
    }
};

webix.i18n.setLocale("ko-KR");

//webix 확장 / 변경 메소드
webix.proxy.baseproxy = {
	$proxy : true,
	load:function(view, callback){
		var params = {};

		// Parameters
		if("config" in view && view.config.params){
			params = view.config.params;
		}
		/*
		if("dynamicFlag" in view && view.config.dynamicFlag) {
			var node_id = view.id;
			params.up_node_id = node_id;
			params.expandLevel = 0;
		}
		*/
		webix.ajax().bind(view).post(this.source, JSON.stringify(params), callback);
	},
	save : function(view, update, dp, callback) {
		webix.ajax().post(url, data, callback);
	},
	result : function(state, view, dp, text, data, loader) {
		dp.processResult(state, data, details);
	}
};

//데이터드라이버에서 에러발생 시 에러를 처리를 위해 json 드라이버를 오버라이딩한다.
webix.DataDriver.customjson = webix.clone(webix.DataDriver.json);
webix.expansion(webix.DataDriver.customjson, {
	toObject : function(text, response){
		var jsonObj = webix.DataDriver.json.toObject.call(this, text, response);
		if(!(jsonObj instanceof Object)) {
			// denial page로 redirect 되었을 경우, 해당 페이지에 있는 알림코드값을 출력해준다.
			var alertCode = "";
			var DENIAL_TEXT = "NKIA_DENIAL___";
			if(text.indexOf(DENIAL_TEXT) > -1) {
				alertCode = text.substring(text.indexOf(DENIAL_TEXT)+DENIAL_TEXT.length, text.lastIndexOf("___"));
				if(alertCode == "") {
					alertCode = "ERROR";
				}
			} else {
				alertCode = "ERROR";
			}
			alert( getConstText({ m_key : alertCode }) );
		}
		return jsonObj;
	}
});

// Form Hidden Data Setter 제공으로 override하였음.
webix.AtomDataLoader._load_when_ready = function(){
	this._ready_for_data = true;
	if (this._settings.url)
		this.url_setter(this._settings.url);
	if (this._settings.data)
		this.data_setter(this._settings.data);
	if (this._settings.hiddens){
		this.hidden_data_setter(this._settings.hiddens);
	}
};

// Form Hidden Data Setter
webix.AtomDataLoader.hidden_data_setter = function(value){
	this.setValues(value);
	return true;
};

/**
 * Grid Header Checkbox - 전체선택시에 disabled 처리로 Custom
 */
webix.ui.datafilter.masterCheckbox = webix.extend({
	refresh:function(master, node, config){
		node.onclick = function(){
			this.getElementsByTagName("input")[0].checked = config.checked = !config.checked;
			var column = master.getColumnConfig(config.columnId);
			var checked = config.checked ? column.checkValue : column.uncheckValue;
			master.data.each(function(obj){
				if(obj){ //dyn loading
					// Custom 영역. 체크박스가 disabled인 경우에는 체크하지 않는다.
					if(typeof obj.check_disabled === "undefined" || obj.check_disabled === false ){
						obj[config.columnId] = checked;
						master.callEvent("onCheck", [obj.id, config.columnId, checked]);
						this.callEvent("onStoreUpdated", [obj.id, obj, "save"]);
						// master checkbox 상태에 따른 css add/remove
						if(checked) {
							master.addRowCss(obj.id, "webix_row_select");
						} else {
							master.removeRowCss(obj.id, "webix_row_select");
						}
					}
				}
			});
			master.refresh();
		};
	}
}, webix.ui.datafilter.masterCheckbox);

/**
 * Native Browser에서 사용되는 clipbuffer 재정의
 * focus() 함수를 변경하였음.
 */
webix.clipbuffer = {
	_area: null,
	_blur_id: null,
	_ctrl: 0,

	/*! create textarea or returns existing
	 **/
	init: function() {
		// returns existing textarea
		if (this._area !== null)
			return this._area;

		webix.destructors.push(this);
		// creates new textarea
		this._area = document.createElement('textarea');
		this._area.className = "webix_clipbuffer";
		this._area.setAttribute("webixignore", 1);
		document.body.appendChild(this._area);

		webix.event(document.body, 'keydown', webix.bind(function(e){
			var key = e.keyCode;
			var ctrl = !!(e.ctrlKey || e.metaKey);
			if (key === 86 && ctrl)
				webix.delay(this._paste, this, [e], 100);
		}, this));

		return this._area;
	},
	destructor: function(){
		this._area = null;
	},
	/*! set text into buffer
	 **/
	set: function(text) {
		this.init();
		this._area.value = text;
		this.focus();
	},
	/*! select text in textarea
	 **/
	focus: function() {
		// if there is native browser selection, skip focus
		if(!this._isSelectRange()){
			this.init();
			// 변경점 추가 - 탭 닫힐 때 Grid Reload 이벤트를 줄 경우에 발생하지만.. 알고보면 select()문제.
			if(this._area.innerText != ""){
				this._area.focus();
				this._area.select();
			}
		}
	},
	/*! checks document selection
	 **/
	_isSelectRange: function() {
		var text = "";
		if (typeof window.getSelection != "undefined") {
			text = window.getSelection().toString();
		} else if (typeof document.selection != "undefined" && document.selection.type == "Text") {
			text = document.selection.createRange().text;
		}
		return !!text;
	},
	/*! process ctrl+V pressing
	 **/
	_paste: function(e) {
		var trg = e.target || e.srcElement;
		if (trg === this._area) {
			var text = this._area.value;
			var last_active = webix.UIManager.getFocus();
			if (last_active && (!last_active.getEditor || !last_active.getEditor())){
				last_active.callEvent("onPaste", [text]);
				this._area.select();
			}
		}
	}
};

/**
 * 필드 입력 패턴 정의
 */
webix.patterns = {
	price:{ mask:"###,###,###,###,###,###", allow:/[0-9]/g }
//	phone:{ mask:"###-###-####",   allow:/[0-9]/g },
//	card: { mask:"#### #### #### ####", allow:/[0-9]/g },
//	date: { mask:"####-##-## ##:##",    allow:/[0-9]/g }
};

