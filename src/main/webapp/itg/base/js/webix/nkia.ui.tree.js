//Tree에서 사용하기 위한 데이터드라이버 정의
var __TREE_URL_PREFIX = "treeproxy->";

webix.proxy.treeproxy = webix.clone(webix.proxy.baseproxy);
webix.expansion(webix.proxy.treeproxy, {
	load : function(view, callback) {
		var params = {};

		// Parameters
		if("config" in view && view.config.params){
			params = view.config.params;
		}
		webix.ajax().bind(view).post(this.source, JSON.stringify(params), callback);
	}
});

//Tree에서 사용하기 위한 데이터드라이버 정의
webix.DataDriver.treejson = webix.clone(webix.DataDriver.customjson);
webix.expansion(webix.DataDriver.treejson, {
	dataPosition : 0,
	getRecords : function(data) {
		if (data && data.treeVO && data.treeVO.children){
			// id가 0인거는 root로 change
			if(data.treeVO.children[0].id == "0"){
				data.treeVO.children[0].id = "root";
			}
			data = data.treeVO.children;
		}
		return data;
	},
	child: "children"
});

/********************************************
 * Tree
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Filter, Expand/Collpase, Tree 지원
 ********************************************/
nkia.ui.tree.Tree = function() {
	// Webix Properties
	this.view = "tree";
	this.id = "";
	this.select = true;
	this.template = "{common.icon()} {common.folder()} <span>#text#<span>";
	this.url = __TREE_URL_PREFIX;
	this.datatype = "treejson";
	this.filterMode = {
		showSubItems:false
	};
	this.checkBoxShow = false;
	this.on = {};

	// Custom Properties
	this.params = {};
	this.expandLevel = 0;
	this.checkbox = false;

	this.propsValidate = function(props) {
		nkia.ui.validator.tree(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				if(i === "url") {
					this[i] = this.url + nkia.host + props[i];
				} else {
					this[i] = props[i];
				}
			}
		}

		if(props.width){
			this.width = props.width;
		}

		if(props.height){
			this.height = props.height;
		}

		if(props.checkbox){
			this.checkBoxShow = true;
			this.template = "{common.icon()} {common.checkbox()} {common.folder()} <span>#text#</span>";
			this.threeState = true;
		}

		if(props.expandLevel){
			this.params.expandLevel = props.expandLevel;
		}
	};

	this.attachEvent = function() {
		this.on.onAfterLoad = function() {
			// Level에 따른 tree expand
			var visibleCount = this.count();
			var expandLevel = this.config.expandLevel;
			if(expandLevel > 0 && visibleCount > 0){
				this.config.loadedExpandLevel(this, this.getFirstId(), visibleCount, expandLevel);
			}
		};
	};

	this.loadedExpandLevel = function(tree, firstId, visibleCount, expandLevel) {
		var firstItem = tree.getItem(firstId);
		// 첫번쨰 item 펼치기
		if(firstItem && firstItem.$level < expandLevel){
			tree.open(firstId, false);

			if(firstItem.$level < expandLevel){
				// Open a child
				tree.config.loadedExpandLevel(tree, tree.getFirstChildId(firstId), firstItem.$count, expandLevel);
			}
		}
		// Next Sibling 펼치기
		if(visibleCount > 1){
			var siblingId = firstId;
			for(var i = 1; i < visibleCount; i++){
				var nextSiblingId = tree.getNextSiblingId(siblingId);
				var nextSiblingItem = tree.getItem(nextSiblingId);

				if(nextSiblingItem.$level < expandLevel){
					tree.open(nextSiblingId, false);

					if(nextSiblingItem.$level < expandLevel){
						// Open a child
						tree.config.loadedExpandLevel(tree, tree.getFirstChildId(nextSiblingId), nextSiblingItem.$count, expandLevel);
					}
				}
				siblingId = nextSiblingId;
			}
		}
	};
};

//데이터테이블 프라퍼티 검증함수
nkia.ui.validator.tree = function(props) {
	if(props.id === undefined || props.id === ""){
		nkia.ui.utils.notification({
			type: "error",
			message: "Tree ID가 없습니다."
		});
		return;
	}
	if(props.url === undefined || props.url === ""){
		nkia.ui.utils.notification({
			type: "error",
			message: "Tree URL이 없습니다."
		});
		return;
	}
};

//컴포넌트 확장영역
webix.extend(webix.ui.tree, {
	_reload : function(newParams) {
		var _this = this;
		_this.clearAll();

		if(newParams) {
			_this.config.params = nkia.ui.utils.copyObj(newParams);
			if(_this.config.expandLevel){
				_this.config.params.expandLevel = _this.config.expandLevel;
			}
		}
		_this.load(__TREE_URL_PREFIX + _this.config.url.source, function() {
			_this.refresh();
		});
	},
	_getSelectedItem : function() {
		return this.getSelectedItem();
	},
	_getCheckedItems : function(config) {
		/**
		 * config.leafOnly : 체크된 항목 중 leaf 항목만 반환
		 * config.otherData : 
		 */ 
		var _this = this;
		var checkedList = new Array();
		
		var checkedArray = _this.getChecked();
		checkedArray.forEach(function(id){
			var item = _this.getItem(id);
			if(config !== undefined){
				var data = item;
				if(config.otherData !== undefined && config.otherData === true){
					data = item.otherData;
				}
				if(config.leafOnly){
					if(data.leaf){
						if(config.excludeLevel){
							if(config.excludeLevel < item.$level){
								checkedList.push(data);
							}
						}else{
							checkedList.push(data);
						}
					}
				}else{
					if(config.excludeLevel){
						if(config.excludeLevel < item.$level){
							checkedList.push(data);
						}
					}else{
						checkedList.push(data);
					}
				}
			}else{
				checkedList.push(item);
			}
		});
		return checkedList;
	},
	_checkAll : function() {
		if(this.config.checkbox == true) {
			this.checkAll();
		}
	},
	_uncheckAll : function() {
		if(this.config.checkbox == true) {
			this.uncheckAll();
		}
	},
	_selectNode : function(id) {
		this.open(id);
		var parentId = this.getParentId(id);
		if(parentId){
			this.open(parentId);
			// 재귀호출
			this._selectNode(parentId);
		}
		this.select(id);
	},
	/**
	 * 타이틀 변경
	 * @param {} text
	 */
	_setTitleText: function(text){
		var _this = this;
		var headerTitleSpan = document.getElementById(_this.config.id + "_header_title");
		if(typeof headerTitleSpan != "undefined") {
			headerTitleSpan.innerText = text;
		}
	}
	
});

/**
 * @method {} createTreeComponent : Tree Function
 * @param {json} setTreeProp : Tree Propertis
 * @property {string} id			: Tree Id
 * @property {string} title 		: Tree Title
 * @property {string} url			: Tree Data URL
 * @property {string} height 		: Tree Height
 * @property {string} expandLevel	: Tree 확장 레벨
 * @property {string} expColButton : 트리 전체 펼침/접는 버튼( true | false )
 * @property {string} params		: Request Parameter
 *
 * @returns {object} tree
 */
function createTreeComponent(treeProp) {
	var props = treeProp||{};
	var modules = [];

	try {
		// Header 영역
		if(props.header) {
			// Expand/Collpase Button 영역
			if(props.expColable){
				props.header.buttons = {
					items: [
	                    createIconComp({ icon:"plus", click: function() { $$(props.id).openAll(); } }),
	                    createIconComp({ icon:"minus", click: function() { $$(props.id).closeAll(); } })
	                ]
				}
			}
			props.header.id = props.id + "_header";
			props.header.icon = "tree";
			var header = new nkia.ui.common.Toolbar(props.header);
			modules.push(header);
		}

		// Filter 영역
		if(props.filterable){
			var filter = createTextFieldComp({label: getConstText({isArgs : true, m_key : 'res.common.search'}), name: "search_tree",id : props.id +"_search_tree"});
			filter.on = {
				onTimedKeyPress: function() {
					$$(props.id).filter("#text#",this.getValue());
				}
			};
			
			modules.push({
				view: "form",
		        padding: 4,
		        elements:[
		            filter
		        ]
			});
		}
		
		if(props.filterForm) {
			for(var count=0 ; count<props.filterForm.length ; count++) {
				var formItem = props.filterForm[count];
				
				modules.push({
					view: "form", 
					elements: [ formItem ]
				});
			}
		}
		
		// Body 영역
		var tree = new nkia.ui.tree.Tree();
		tree.setProps(props);
		tree.attachEvent();
		modules.push(tree);
	} catch(e) {
		alert(e.message);
	}
	return modules;
}
