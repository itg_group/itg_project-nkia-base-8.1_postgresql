/********************************************
 * nkia.ui.form.js
 * Date: 2021-03-22
 * Version : WEBIX_NKIA_UI_RELEASE_INFO 참고 
 *  - 설치 버전 : v20210301
 *  - 현재 버전 : v20210513   
 ********************************************/

// helper 변수 영역
var __TOOLTIP_FORFORM_SYMBOL_PREFIX = '<span name="n_tooltip" class="tooltip_icon_span" onclick="showTooltip(\'';
var __TOOLTIP_FORFORM_SYMBOL_MIDDLE = '\',';
var __TOOLTIP_FORFORM_SYMBOL_SUFFIX = '); webix.html.preventEvent(event)" >&nbsp;<img src="/itg/base/images/ext-js/common/icons/information.png" alt="툴팁아이콘" style="vertical-align:top;padding-top:5px;" /></span>';

nkia.ui.form.Form = function() {
	// Webix Properties
	this.view = "form";
	this.id = "";
	this.scroll = false;
	this.elementsConfig = {
		labelPosition: "top"
		//labelPosition: "left",
		//labelAlign: "right",
		//labelWidth: 180
	};
	this.elements = [];
	
	// Custom Properties
	this.hiddens = {};	// Hidden 셋팅
	this.buttons = [];	// 버튼 객체 셋팅
	this.files = [];	// 파일 객체 셋팅
	this.editorMode = true;
	this.cachedData = null;	// data
	this.taskName = "";
	this.height = 0;
	this.addPaddingLayerForCellMerge = false;		// *셀머지(COLSPAN)에 따른 간격 조정* - ( false(default) : padding layer 추가 안함. ture : padding layer 추가 )
	this.widthPaddingLayer = 40;					// *셀머지(COLSPAN)에 따른 간격 조정* - ( Padding Lyaer 가로 폭 - FIELD 양 옆으로 각 1개씩 삽입됨 )

	this.propsValidate = function(props) {
		nkia.ui.validator.form(props);
	};

	this.setProps = function(props) {
		var _this = this;
		
		var setItem = true;
		// 2017-09-06 molee
		// 검색 폼 등의 폼 자체에 fields가 설정 되어 있는지 여부
		// itemNoSet = true : 항목 없음
		// itemNoSet = false : 디폴트, 항목 있음
		if (props.fields.itemNoSet) {
			setItem = false;
		}
		
		if (setItem) {
			this.propsValidate(props);
			
			// fields 구성
			for(var i in props) {
				if(typeof this[i] !== "undefined" && props.hasOwnProperty(i)) {
					this[i] = props[i];
				}
			}
			
			
			if(props.fields){
				// Field Elements Config 변경
				if(props.fields.elementsConfig){
					this.elementsConfig = props.fields.elementsConfig;
				}
				
				var fields = props.fields;
				var colSize = fields.colSize;
				
				if(typeof fields.colSize === "undefined") colSize = 1;
				
				// Form Elements JSON 생성
				var rows = [], cols = [], calCol = 0, fieldCol = 0;
				
				fields.items.forEach(function(field, index){
					fieldCol = (field.colspan) ? field.colspan : 1;
					
					// FormId와 Gravity 설정
					field.item.formId = _this.id;
					field.item.gravity = 1 * fieldCol;
					
					var paddingLabel = {
						view: "label",
						width: _this.widthPaddingLayer,
						hidden: (typeof field.item.visible != "undefined") ? !field.item.visible : false 
					}
					
					cols.push(paddingLabel);	
					cols.push(field.item);
					cols.push(paddingLabel);	
					
					// Start *셀머지(COLSPAN)에 따른 간격 조정*
					if ( _this.addPaddingLayerForCellMerge != null && _this.addPaddingLayerForCellMerge == true ){
						if ( props != null && props.fields != null && props.fields.colSize != null && props.fields.colSize > fieldCol){
							var nPaddingOrder = 0;
							for ( p = 1 ; p < fieldCol ; p++ ){
								var fieldName = ( field != null && field.item != null && field.item.name != null ? field.item.name : "" )
								for ( pp = 0 ; pp < 2 ; pp++){
									nPaddingOrder++;
									var labelId = fieldName + "_addpadding_" + nPaddingOrder;
									var paddingLabel4ColSpan = {
											view: "label",
											id: labelId,
											width: _this.widthPaddingLayer,
											hidden: (typeof field.item.visible != "undefined") ? !field.item.visible : false
										}
									cols.push(paddingLabel4ColSpan);
								}
							}
						}
					}
					// End *셀머지(COLSPAN)에 따른 간격 조정*
										
					calCol = calCol + fieldCol;
					if(calCol % colSize === 0){
						rows.push({cols: cols.splice(0)});
						cols.length = 0;
						calCol = 0;
					}else{
						if((fields.items.length - 1) == index){
							rows.push({cols: cols.splice(0)});
						}
					}
					
					// Button 객체 컨트롤을 위하여 따로 담아둔다.
					if(field.item instanceof nkia.ui.fields.Union){
						field.item.items.forEach(function(innerItem){
							if(innerItem instanceof nkia.ui.common.Button){
								_this.buttons.push(innerItem);
							}
						});
					}else if(field.item instanceof nkia.ui.common.Button){
						_this.buttons.push(field.item);
					}else if(field.item instanceof nkia.ui.file.FileRender){
						var fileChildItems = field.item.rows;
						
						fileChildItems.forEach(function(fileChild, index){
							
							_this.files.push(fileChild);
						});
					}else if(field.item instanceof nkia.ui.fields.SearchDate){
						if(_this.elementsConfig.labelPosition == "top"){
							var dateFields = field.item.rows[0].cols;
							dateFields.forEach(function(date, index){
								if(date.label == "~"){
									date.label = "　";
								}
							});
						}
					}else{
						_this.findButton(field.item, _this.buttons);
					}
					
					
					if(field.item.hiddens){
						props.fields.hiddens = nkia.ui.utils.extend(props.fields.hiddens, field.item.hiddens);
					}
				});
				this.elements = rows;

				if(props.fields.hiddens){
					this.hiddens = props.fields.hiddens;
				}
			}
			
		} else {
			// fields 구성 안함
			for(var i in props) {
				if(typeof this[i] !== "undefined" && props.hasOwnProperty(i)) {
					this[i] = props[i];
				}
			}
			
			if(props.fields){
				// Field Elements Config 변경
				if(props.fields.elementsConfig){
					this.elementsConfig = props.fields.elementsConfig;
				}
				
				var fields = props.fields;
				var colSize = fields.colSize;
				if(typeof fields.colSize === "undefined") colSize = 1;
				
				// Form Elements JSON 생성
				var rows = [], cols = [], calCol = 0, fieldCol = 0;
				this.elements = rows;
			}
		}
		
		if(this.height == 0) {
			delete this.height;
		}
	};
	
	// 재귀호출로 Button을 찾는다.
	this.findButton = function(item, buttons){
		var _this = this;
		if(item instanceof nkia.ui.common.Button){
			if(!item.exclude){
				buttons.push(item);
			}
		}
		
		if(item.rows && item.rows.length > 0){
			item.rows.forEach(function(innerItem){
				_this.findButton(innerItem, buttons);	
			})
		}
		if(item.cols && item.cols.length > 0){
			item.cols.forEach(function(innerItem){
				_this.findButton(innerItem, buttons);	
			})
		}
	}
};

//데이터테이블 프라퍼티 검증함수
nkia.ui.validator.form = function(props) {
	if(props.id === undefined || props.id === ""){
		nkia.ui.utils.notification({
			type: "error",
			message: "Form ID가 없습니다."
		});
		return;
	}
	
	if(typeof props.fields === "undefined" || typeof props.fields.items === "undefined" || props.fields.items.length === 0){
		nkia.ui.utils.notification({
			type: "error",
			message: "Form Field가 없습니다."
		});
		return;
	}
};

//컴포넌트 확장영역
webix.extend(webix.ui.form, {
	/**
	 * Sync에서 Cached Data 불러오기
	 * @return {}
	 */
	_getCachedData: function() {
		return this.config.cachedData
	},
	/**
	 * Field 가져오기
	 * @param {} fieldName
	 * @return {}
	 */
	_getField: function(fieldName) {
		if(this.elements[fieldName]){
			return this.elements[fieldName];
		}
		return null;
	},
	/**
	 * Field Value 가져오기
	 * @param {} fieldName
	 * @return {}
	 */
	_getFieldValue: function(fieldName) {
		if(this.elements[fieldName]){
			var field = this._getField(fieldName);
			// Select Box에서 데이터 셋팅(화면렌더이전)에 데이터를 가져오는 경우 처리 때문에 직접 value를 access.
			if(field.config.view == "select"){
				return field.config.value;
			}
			return this.elements[fieldName].getValue();
		}
		if(this.config.hiddens[fieldName]){
			return this.config.hiddens[fieldName];
		}
		return "";
	},
	/**
	 * 필드와 Hidden에 대한 입력된 모든 데이터를 반환한다.
	 * @return {}
	 */
	_getValues: function() {
		var values = this.getValues();
		if(this.config.hiddens){
			values = nkia.ui.utils.extend(values, this.config.hiddens);
		}
		return values;
	},
	
	// *value trim 처리 후 반환* 
	_getValuesWithTrim: function() {
		var values = this.getValues();
		if(this.config.hiddens){
			values = nkia.ui.utils.extend(values, this.config.hiddens);
		}
		var fnl_values = null;
		if ( values != null && typeof values == "object"){
			fnl_values = {};
			for ( pkey in values ){
				fnl_values[""+pkey] = values[pkey].trim();
			}
		}else{
			fnl_values = values;
		}
		
		return fnl_values;
	},	
	
	/**
	 * 필드 숨기기
	 * Resize가 필요한 경우없는 경우에는 false로 호출
	 * @param {} fieldName
	 * @param {} isResize
	 */
	_hideField: function(fieldName, isResize){
		var field = this._getField(fieldName);
		if(field==null) {
			  field = this._getField(fieldName + "_startDate");
			  if(field==null) {
			    field = this._getField(fieldName + "_endDate");
			  }
			}
		if(field){
			var view = field.$view;
			// 숨김 시, 필수여부 제거
			field.config.required = false;
			var prevView = view.previousSibling;
			var isFindLabel = false;
			while(!isFindLabel){
				if(prevView && prevView.className && prevView.className.indexOf("webix_el_label") != -1){
					isFindLabel = true;					
				}else{
					if(prevView == null){
						view = view.parentNode;
						prevView = view.previousSibling;
					}else{
						prevView = prevView.previousSibling;
					}
				}
			}
			var nextView = view.nextSibling;
			
			view.style.display = "none";
			prevView.style.display = "none";
			nextView.style.display = "none";
			
			// Start *셀머지(COLSPAN)에 따른 간격 조정*
			if ( this.addPaddingLayerForCellMerge != null && this.addPaddingLayerForCellMerge == true ){
				var nPaddingOrder = 0;
				while ( 1 ){
					nPaddingOrder++;
					if ( $$(fieldName + "_addpadding_" + nPaddingOrder) == null ){break;}
					else {
						$$(fieldName + "_addpadding_" + nPaddingOrder).$view.style.display = "none";
					}
					if ( nPaddingOrder > 30 ) {break;} // 무한루프 방지용
				}			
			}			
			// End *셀머지(COLSPAN)에 따른 간격 조정*
			
			if(prevView.previousSibling == null && nextView.nextSibling == null){
				view.parentNode.style.display = "none";
			}
			
			// outline display 제어 소스
			var outlineView = view.parentNode;
			while(outlineView.className.indexOf("webix_layout_line") == -1){
			 	outlineView = outlineView.parentNode;
			}
			var displayNoneCount = 0;
			var outlineChildView = outlineView.childNodes;
			for( var i = 0; i < outlineChildView.length; i++ ){
				if(outlineChildView[i].style.display == "none"){
					displayNoneCount++;
				}
			}
			if(displayNoneCount == outlineChildView.length){
				outlineView.style.display = "none";
			}
			
			if(isResize){
				this.define("height", jq(view.parentNode.parentNode).height() + 2);
				this.resize();	
			}
		}
	},
	/**
	 * 필드를 배열로 받아서 숨겨준다.
	 * 여러 필드제어시에 Resize가 비번히 호출되면 성능문제가 발생되므로 제공되는 함수
	 * @param {} fieldNames
	 */
	_hideFields: function(fieldNames){
		if(Array.isArray(fieldNames)){
			for(var i = 0, len = fieldNames.length; i < len; i++){
				if(i == (len - 1)){
					this._hideField(fieldNames[i], true);
				}else{
					this._hideField(fieldNames[i], false);
				}
			}
		}
	},
	/**
	 * Form에 대한 제어이며, Field, Button, File에 대한 Readonly 처리를 담당한다.
	 * @param {} controlProps
	 */
	_setControl : function(controlProps) {
		var editorMode = controlProps.editorMode;
		var exceptions = controlProps.exceptions;

		// Form Element Control
		var fields = this.elements;
		for( var key in fields ){
			var field = fields[key];
			var fieldType = field.config.view;
			
			// 벨리데이션 표시 제거
			webix.html.removeCss(field.getNode(), 'webix_invalid');
			
			if( typeof field.config.originReadonly === "undefined" ){
				field.config.originReadonly = field.config.readonly;
				if( typeof field.config.readonly === "undefined" ){
					field.config.originReadonly = field.config.disabled;
				}
				if(fieldType=="search") {
					field.config.originReadonly = field.config.oriReadonly;
				}
			}
			
			var fieldEditorMode = editorMode;
			if( exceptions ){
				// 예외처리가 있는 경우
				if (isExistDataForArray(exceptions, field.config.name)) {
					fieldEditorMode = !editorMode;
				}
			}
			
			if( field.config.originReadonly ){
				if(fieldType=="select" || fieldType=="radio" || fieldType=="checkbox") {
					field.disable();
				} else if(fieldType=="nic-editor"){
					$$(field.config.id).isReadOnly(true);
				} else {
					field.define("originReadonly", true);
					field.define("readonly", true);
					field.define("css", "nkia_field_readonly");
					field.refresh();
				}
			}else{
				field.config["originReadonly"] = false;
				if( fieldEditorMode ){
					if(fieldType=="select" || fieldType=="radio"|| fieldType=="checkbox") {
						field.enable();
					} else if(fieldType=="nic-editor"){
						$$(field.config.id).isReadOnly(false);
					} else {
						field.define("readonly", false);
						webix.html.removeCss(field.getNode(), 'nkia_field_readonly_false');
						webix.html.removeCss(field.getNode(), 'nkia_field_readonly');
						webix.html.addCss(field.getNode(), 'nkia_field_readonly_false');
						field.refresh();
						
						if(fieldType=="search") {
							field.define("oriReadonly", false);
							if(field.config.keepEvent===false) {
								field.define("readonly", true);
								field.refresh();
								var view = field.$view;
								var viewIcons = view.getElementsByClassName("webix_input_icon fa-search");
								if(viewIcons && viewIcons!=null && viewIcons.length>0){
									viewIcons[0].style.display = "block";
								}
							}
						}
					}
				}else{
					if(fieldType=="select" || fieldType=="radio"|| fieldType=="checkbox") {
						field.disable();
					} else if(fieldType=="nic-editor"){
						$$(field.config.id).isReadOnly(true);
					}else {
						field.define("readonly", true);
						webix.html.removeCss(field.getNode(), 'nkia_field_readonly_false');
						webix.html.removeCss(field.getNode(), 'nkia_field_readonly');
						webix.html.addCss(field.getNode(), 'nkia_field_readonly');
						field.refresh();
						
						if(fieldType=="search") {
							field.define("oriReadonly", true);
							if(field.config.keepEvent===false) {
								field.define("click", "");
								var view = field.$view;
								var viewIcons = view.getElementsByClassName("webix_input_icon fa-search");
								if(viewIcons && viewIcons!=null && viewIcons.length>0){
									viewIcons[0].style.display = "none";
								}
							}
						}
					}
				}	
			}
			
		}
		
		// 버튼 Control
		var buttons = this.config.buttons;
		if(buttons && buttons.length > 0){
			buttons.forEach(function(button, index){
				nkia.ui.utils.disabled(button, exceptions, editorMode);
			});
		}
		
		// File Control
//		var files = this.config.files;
//		if(files && files.length > 0){
//			files.forEach(function(file, index){
//				nkia.ui.utils.disabled(file, exceptions, editorMode, true);
//			});
//		}
		
		// File Control
		var files = this.config.files;
		if(files && files.length > 0){
			var fileCompId = files[1].id.replace('_list', '');
			if(editorMode){
				$$(fileCompId).enable();
				jq('.webix_remove_upload').show();
			}else{
				$$(fileCompId).disable();
				jq('.webix_remove_upload').hide();
			}
		}
	},
	/**
	 * Data를 Field 또는 Hidden에 Binding처리
	 * 파일이 있는 경우에는 파일 목록을 요청한다.
	 * cachedData 속성에 원본 data를 저장한다.
	 * @param {} data
	 * @param {} doReset
	 */
	_syncData: function(data, doReset) {
		if(typeof doReset === undefined || doReset){
			this._reset();
		}
		
		this._setValues(nkia.ui.utils.convert.data(data));
		
		var files = this.config.files;
		if(files && files.length > 0){
			files.forEach(function(file, index){
				if(file instanceof nkia.ui.file.File){
					if(data[file.id.toUpperCase()]){
						$$(file.id)._getFileList(data[file.id.toUpperCase()]);
					}
				}
			});
		}
		this.config.cachedData = data;
	},
	/**
	 * Form Validation Check
	 * Validation이 유효하지 않는 경우 메시지를 보여준다.
	 * @return {}
	 */
	_validate: function() {
		var formFieldMap = this.elements;
		
		var focusKey = "";
		var isNull = false;
		var isChrLengthCheck = false;
		var validate = true;
		
		/** 필수값 validation */
		for(var key in formFieldMap) {
			var fieldComp = formFieldMap[key];
			var fieldData = fieldComp.config;
			var isDisabled = fieldData.disabled;
			var isReadonly = fieldData.readonly;
			if(fieldData.view == "search") {
				isReadonly = fieldData.oriReadonly;
			}
			
			// field 값 조회
			var fieldValue = this._getFieldValue(key);
			if(typeof fieldValue == "string"){
				fieldValue = fieldValue.trim();
			} else if(typeof fieldValue == "number"){
				fieldValue = String(fieldValue);
			}
			
			// invalid CSS 제거
			if(fieldData.view == "nic-editor"){
				webix.html.removeCss(fieldComp.getNode(), 'webix_invalid_webEditor');
			}else{
				webix.html.removeCss(fieldComp.getNode(), 'webix_invalid');
			}
			  
			
			if(fieldData.required && (!isDisabled && !isReadonly)) {
				if(fieldValue == ""){
					if(focusKey == ""){
						focusKey = key;
					}
					validate = false;
					// invalid CSS 추가
					if(fieldData.view == "nic-editor"){
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid_webEditor');
					}else{
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
					}
					
				}
			}
		}
		if(!validate){
			// focus
			formFieldMap[focusKey].focus();
			nkia.ui.utils.notification({
				type : 'error',
				message : getConstText({isArgs : true, m_key : 'res.ubit.center.000006'})
			});
		}	 /** 필수값 validation 끝 */
		else{
			
			/**  필수값 체크 외의 validation 정의 */
			for(var key in formFieldMap){
				var fieldComp = formFieldMap[key];
				var fieldData = fieldComp.config;
				var isDisabled = fieldData.disabled;
				var isReadonly = fieldData.readonly;
				if(fieldData.view == "search") {
					isReadonly = fieldData.oriReadonly;
				}
				
				// field 값 조회
				var fieldValue = this._getFieldValue(key);	
				
				/** 문자열 byte validation */
				var maxSize = (typeof fieldData.maxsize == "undefined") ? "" : fieldData.maxsize;
				maxSize = (maxSize == "") ? ( (typeof fieldData.maxlength == "undefined") ? maxSize : fieldData.maxlength ) : maxSize;
				if(maxSize > 0 && (!isDisabled && !isReadonly)) {
					var valueBytes = getByteSize(fieldValue);
					
					if(valueBytes > maxSize) {
						// invalid CSS 추가
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
						formFieldMap[key].focus();
						nkia.ui.utils.notification({
							type : 'error',
							message : '값의 길이가 너무 깁니다. </br> ' + valueBytes + ' / ' + maxSize + ' bytes'
						});
						validate = false;
						break;
					}
				}  /** 문자열 byte validation 끝 */
			
				/** HTML 태그 validation */
				var htmlTagValid = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
				if(fieldData.view != "textarea" && fieldData.view != "nic-editor"){
					if(htmlTagValid.test(fieldValue)){
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
						formFieldMap[key].focus();
						nkia.ui.utils.notification({
							type : 'error',
							message : 'HTML 태그는 입력하실 수 없습니다.'
						});
						validate = false;
						break;
					}
				}
				/** HTML 태그 validation 끝*/
				
				/** IP 형식 validation */
				//var ipValid =  /^([0-9]{1,3}\.){3}[0-9]{1,3}$/;
				var ipValid = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/;
				if(fieldData.inputType == "ip" && fieldValue != ""){
					if(!ipValid.test(fieldValue)){
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
						formFieldMap[key].focus();
						nkia.ui.utils.notification({
							type : 'error',
							message : 'IP 형식에 맞지 않습니다.'
						});
						validate = false;
						break;
					}
				}
				/** IP 형식 validation 끝*/
				
				/** 메일 형식 validation */
				var mailValid =  /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
				if(fieldData.inputType == "mail" && fieldValue != ""){
					if(!mailValid.test(fieldValue)){
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
						formFieldMap[key].focus();
						nkia.ui.utils.notification({
							type : 'error',
							message : 'mail 형식에 맞지 않습니다.'
						});
						validate = false;
						break;
					}
				}
				/** IP 형식 validation 끝*/
				
				/** 숫자,정수(금액) 형식 validation */
				var priceValid = /^[+-]?\d*([0-9]+)$/;
				if( ( fieldData.inputType == "price" || fieldData.inputType == "integer" )  && fieldValue != ""){
					if(!priceValid.test(fieldValue)){
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
						formFieldMap[key].focus();
						nkia.ui.utils.notification({
							type : 'error',
							message : '숫자 및 음수 기호(-)만 입력가능 합니다. (쉼표 불가)'
						});
						validate = false;
						break;
					}
				}
				/** 숫자,정수(금액) 형식 validation 끝*/
				
				/** 숫자,실수 형식 validation */
				var doubleValid = /^([0-9.-]+)$/;
				if(fieldData.inputType == "double" && fieldValue != ""){
					if(!doubleValid.test(fieldValue)){
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
						formFieldMap[key].focus();
						nkia.ui.utils.notification({
							type : 'error',
							message : '소수점 숫자(.) 및 음수 기호(-)만 입력가능 합니다.'
						});
						validate = false;
						break;
					}
				}
				/** 숫자,실수 형식 validation 끝*/
				
				/** 시간 형식 validation */
				var timeValid = /^([0-9]{2})[0-9]{2}$/;
				if(fieldData.inputType == "texttime" && fieldValue != ""){
					if(!timeValid.test(fieldValue)){
						webix.html.addCss(fieldComp.getNode(), 'webix_invalid');
						formFieldMap[key].focus();
						nkia.ui.utils.notification({
							type : 'error',
							message : '시간형식만 입력 가능합니다. ([00~23][00~59])'
						});
						validate = false;
						break;
					}
				}
			}
		}
		
		// 첨부파일 필수체크
		var files = this.config.files;
		if(files && files.length > 0){
			files.some(function(file, index){
				if(file instanceof nkia.ui.file.File){
					if(file.required){
						var fileRows = $$(file.id)._getRows();
						if(fileRows.length == 0){
							validate = false;
							return true;
						}
					}
				}
			});
		}
		return validate;
	},
	/**
	 * 필드와 Hidden에 대한 데이터를 셋팅한다.
	 * @param {} values
	 */
	_setValues: function(values) {
		for(key in values){
			var isExist = false;
			
			var field = this._getField(key);
			if(field != null){
				if(field.config.view != "nic-editor"){
					isExist = true;
					if(field.data.view == "label"){
						isExist = false;
					}
					
					if( typeof values[key] == "object")
					// 날짜 value가 object 형태로 넘어왔을 경우 날짜값 세팅 분기
					if(values[key] && values[key]!=null && typeof values[key] == "object") {
						// 데이터가 참조되어 변경되지 않도록 deep copy
						var dateObj = JSON.parse(JSON.stringify(values[key]));
						// 날짜값 parsing하여 value에 세팅
						if(field.data.dateType && field.data.dateType=="D") {
							values[key] = dateObj.day;
						} else if(field.data.dateType && field.data.dateType=="DH") {
							values[key] = dateObj.day;
							values[key+"_hour"] = dateObj.hour;
						} else if(field.data.dateType && field.data.dateType=="DHM") {
							values[key] = dateObj.day;
							values[key+"_hour"] = dateObj.hour;
							values[key+"_min"] = dateObj.min;
						}
					}
					// 히든 필드 세팅
					if(typeof this.config.hiddens[key] !== "undefined"){
						// Hidden 객체에도 값을 셋팅해준다. 다른 객체에서 히든필드를 사용할 수 있어서 셋팅해준다.
						this.config.hiddens[key] = values[key];
					}
				}else{
					$$(field.config.id).setValue(values[key]);
					delete values[key];
				}
			}else{
				if(typeof this.config.hiddens[key] !== "undefined"){
					// Hidden 객체에도 값을 셋팅해준다.
					this.config.hiddens[key] = values[key];
					isExist = true;
				}
			}
			if(!isExist){
				delete values[key];
			}
		}
		this.setValues(values, true);
	},
	/**
	 * 필드 리셋 및 파일이 있는 경우 파일도 리셋한다.
	 */
	_reset: function(){
		
		var _this = this;
		
		_this.clear();
		
		var files = _this.config.files;
		if(files && files.length > 0){
			var initFileValues = {};
			files.forEach(function(file, index){
				if(file instanceof nkia.ui.file.File){
					$$(file.id)._clear();
					initFileValues[file.id] = "";
				}
			});
			// 폼에 남아있는 첨부파일 ID도 삭제
			_this._setValues(initFileValues);
		}
		
		var hiddens = _this.config.hiddens;
		if(hiddens && JSON.stringify(hiddens) != "{}") {
			var initHiddenValues = {};
			for(key in hiddens) {
				initHiddenValues[key] = "";
			}
			this._setValues(initHiddenValues);
		}
		
		this.config.cachedData = null;
	},
	/**
	 * Field Value 입력하기
	 * @param {} fieldName
	 * @return {}
	 */
	_setFieldValue: function(fieldName, value) {
		if(this.elements[fieldName]){
			this.elements[fieldName].setValue(value);
		}
		if(this.config.hiddens[fieldName] != undefined){
			this.config.hiddens[fieldName] = value;
		}
	},
	/**
	 * 필드를 배열로 받아서 보여준다.
	 * 여러 필드제어시에 Resize가 비번히 호출되면 성능문제가 발생되므로 제공되는 함수
	 * @param {} fieldNames
	 */
	_showFields: function(fieldNames){
		if(Array.isArray(fieldNames)){
			for(var i = 0, len = fieldNames.length; i < len; i++){
				if(i == (len - 1)){
					this._showField(fieldNames[i], true);
				}else{
					this._showField(fieldNames[i], false);
				}
			}
		}
	},
	/**
	 * 필드 보여주기
	 * Resize가 필요한 경우없는 경우에는 false로 호출
	 * @param {} fieldName
	 * @param {} isResize
	 */
	_showField: function(fieldName, isResize){
		var field = this._getField(fieldName);
		if(field==null) {
			  field = this._getField(fieldName + "_startDate");
			  if(field==null) {
			    field = this._getField(fieldName + "_endDate");
			  }
			}
		if(field){
			var view = field.$view;
			var prevView = view.previousSibling;
			var isFindLabel = false;
			while(!isFindLabel){
				if(prevView && prevView.className && prevView.className.indexOf("webix_el_label") != -1){
					isFindLabel = true;					
				}else{
					if(prevView == null){
						view = view.parentNode;
						prevView = view.previousSibling;
					}else{
						prevView = prevView.previousSibling;
					}
				}
			}
			var nextView = view.nextSibling;
			
			view.style.display = "inline-block";
			prevView.style.display = "inline-block";
			nextView.style.display = "inline-block";
			
			if(prevView.previousSibling == null && nextView.nextSibling == null){
				view.parentNode.style.display = "block";
			}
			
			// outline 제어 소스
			var outlineView = view.parentNode;;
			while(outlineView.className.indexOf("webix_layout_line") == -1){
			 	outlineView = outlineView.parentNode;
			}
			if(outlineView.style.display == "none"){
				outlineView.style.display = "inline-block";
			}
			
			if(isResize){
				this.define("height", jq(view.parentNode.parentNode).height() + 2);
				this.resize();	
			}
		}
	},
	/**
	 * 타이틀 변경
	 * @param {} text
	 */
	_setTitleText: function(text){
		var _this = this;
		var headerTitleSpan = document.getElementById(_this.config.id + "_header_title");
		if(typeof headerTitleSpan != "undefined") {
			headerTitleSpan.innerText = text;
		}
	},
	/**
	 * 필드 숨기기
	 * isProcAfterHide=true 인 필드 숨김처리
	 * @param {} fieldName
	 * @param {} isResize
	 */
	_procAfterHide: function(){
		var _this = this;
		var afterHideFields = [];
		var elements = _this.elements;
		for(var elName in elements) {
			var isAfterHide = elements[elName].config.isProcAfterHide;
			if(isAfterHide === true) {
				var blankField = _this._getField(elName);
				blankField.$view.style.display = "none";
			}
		}
		
	}
});

/**
 * @method {} createFormComp : Form Function
 * @property {string} id			: Form Id
 * @property {object} header 		: Form Header
 * @property {object} fields 		: Form Fields
 * @property {object} footer 		: Form Footer
 * @returns {object} form
 */
function createFormComp(formProps) {
	var props = formProps||{};
	var modules = [];

	try {
		// Header 영역
		if(props.header) {
			
			var titleFullText = props.header.title;
			if(props.helper) {
				var helperWidth = props.helperWidth ? props.helperWidth : 200;
				var helperHeight = props.helperHeight ? props.helperHeight : 100;
				titleFullText += 
					__TOOLTIP_FORFORM_SYMBOL_PREFIX 
					+ props.helper 
					+ __TOOLTIP_FORFORM_SYMBOL_MIDDLE
					+ helperWidth + "," + helperHeight
					+ __TOOLTIP_FORFORM_SYMBOL_SUFFIX;
			}
			
			props.header.title = titleFullText;
			props.header.id = props.id + "_header";
			props.header.icon = props.header.icon||"form"; 
			var header = new nkia.ui.common.Toolbar(props.header);
			modules.push(header);
		}

		// Body 영역
		var form = new nkia.ui.form.Form();
		form.setProps(props);
		modules.push(form);

		// Footer 영역
		if(props.footer) {
			//var footer = new nkia.ui.common.Toolbar(props.footer);
			
			var footer = createButtons(props.footer.buttons);
			
			modules.push(footer);
		}
	} catch(e) {
		alert(e.message);
	}
	return modules;
}
