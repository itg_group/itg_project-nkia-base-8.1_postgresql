/********************************************
 * nkia.ui.grid.js
 * Date: 2021-03-22
 * Version : WEBIX_NKIA_UI_RELEASE_INFO 참고 
 *  - 설치 버전 : v20210301
 *  - 현재 버전 : v20210325   
 ********************************************/

//그리드에서 사용하는 메시지 모음. 추후 리소스 화 필요
var __GRID_COMMON_MSG = {
	DO_NOT_EDIT				: "편집할 수 없는 정보입니다.",
	PAGE_SIZE_NOT_EXIST		: "PageSize(argument) is not define",
	SELECTED_DATA_NOT_EXIST	: getConstText({isArgs : false, m_key : 'SELECTED_DATA_NOT_EXIST'}),
	COMBO_ATTACH_CHOICE		: getConstText({isArgs : false, m_key : 'COMBO_ATTACH_CHOICE'}),
	COMBO_ATTACH_ALL		: getConstText({isArgs : false, m_key : 'COMBO_ATTACH_ALL'}),
	TOTAL_DATA_TEXT			: getConstText({isArgs : false, m_key : 'TOTAL_DATA_TEXT'}),
	SELECTED_DATA_TEXT		: getConstText({isArgs : false, m_key : 'SELECTED_DATA_TEXT'}),
	ROW_UNIT_TEXT			: getConstText({isArgs : false, m_key : 'ROW_UNIT_TEXT'})
};
//그리드에서 사용하는 벨리데이션 관련 메시지 모음. 추후 리소스 화 필요
var __GRID_VALID_MSG = {
	NOT_INTEGER : "정수 및 음수 기호(-)만 입력가능 합니다.",
	NOT_DOUBLE : "소수점 숫자(.) 및 음수 기호(-)만 입력가능 합니다.",
	NOT_PRICE : "숫자 및 음수 기호(-)만 입력가능 합니다. (쉼표 불가)",
	NOT_NATURAL : "자연수만 입력 가능합니다.",
	NOT_IP : "IP(CIDR)형식만 입력가능 합니다.",
	NOT_TEXTTIME : "시간형식만 입력 가능합니다. ([00~23][00~59])",
	NOT_HPNO : "연락처를 확인바랍니다. (010-0000-0000)"
};
//네임스페이스로 정의된 함수는 가급적 사용하지 않습니다.
var __GRID_URL_PREFIX = "gridproxy->";
var __GRID_EMPTY_DATA_URL = "/itg/base/selectDummyDataForMemoryGrid.do";
var __CHECK_BOX_SUFFIX = "_check";
var __ALL_CHECK_BOX_SUFFIX = "_allCheck";
var __CHECK_HEADER_SUFFIX = "_CHECK_HEADER";
var __CHECK_BOX_HEADER_ID = "masterCheckbox";
var __TOTAL_COUNT_SPAN_SUFFIX = "_totalCount";
var __CHECK_COUNT_SPAN_SUFFIX = "_checkCount";
var __PAGER_SUFFIX = "_pager";
//grid pager의 page size 설정 
var __ROWCOUNT_SELECT_BOX_SUFFIX = "_rowCount";
var __ROWCOUNT_OPTIONS = "";
var __ROWCOUNT_LIST = [10, 20, 30, 40, 50, 100];
//var __EDITABLE_CELL_COLOR_CODE = "#F7FAFF"; // 푸른색 계열
//var __EDITABLE_CELL_COLOR_CODE = "#FFF1F1"; // 붉은색 계열
var __EDITABLE_CELL_COLOR_CODE = "#FFF5EA"; // 노란색 계열
var __EMPTY_SELECTBOX_ID = "__NO_DATA_IN_GRID_COMBOBOX__";
for(var i = 0 ; i < __ROWCOUNT_LIST.length; i++) {
	__ROWCOUNT_OPTIONS = __ROWCOUNT_OPTIONS + "<option value=" + __ROWCOUNT_LIST[i] + ">" + __ROWCOUNT_LIST[i] + "</option>";
}
// helper 변수 영역
var __TOOLTIP_SYMBOL_PREFIX = '<span name="n_tooltip" class="tooltip_icon_span" onclick="showTooltip(\'';
var __TOOLTIP_SYMBOL_MIDDLE = '\',';
var __TOOLTIP_SYMBOL_SUFFIX = '); webix.html.preventEvent(event)" >&nbsp;<img src="/itg/base/images/ext-js/common/icons/information.png" style="vertical-align:top;" /></span>';

//그리드의 네임스페이스를 정의
nkia.ui.grid = {};
nkia.ui.grid.column = {};
nkia.ui.grid.column.handler = {};
nkia.ui.grid.body = {};

//그리드에서 사용할 커스텀 프록시 - 데이터 요청을 위한 request 처리를 담당
webix.proxy.gridproxy = webix.clone(webix.proxy.baseproxy);
webix.expansion(webix.proxy.gridproxy, {
	load : function(view, callback) {
		var params = view.config.params || {};
		var pager = view.config.pager;
		var gridDataDrvier = view.data.driver;
		var start = 1;
		var page = 1;
		var pageSize = 0;
		var url = this.source;
		if (view.config.pageable) {
			//최초로딩시에는 페이저를 참조할 수 없다.
			if(pager) {
				page = view.config.pager.page !== 0 ? view.config.pager.page + 1 : 1;
				pageSize = view.config.pageSize || 10;
			}else{
				pageSize = view.config.pageSize || 10;
			}
			//페이징 시에 서버에서 반드시 사용되는 파라메터를 세팅한다.
			start = ((page - 1) * pageSize);
			params.page = page;
			params.start = start;
			params.limit = pageSize;
		}
		
		//Sort 관련 정보를 세팅
		if(arguments.length >= 3) {
			if(arguments[2] && arguments[2].sort) {
				var dir = arguments[2].sort.dir;
				var sortColumn = arguments[2].sort.id;
				var thisColumn = view.getColumnConfig(sortColumn);
				// 정렬이 필요한 컬럼을 따로 부여한 컬럼인 경우 해당 컬럼 기준으로 sorting이 되도록 한다.
				if(thisColumn.orderColumn) {
					sortColumn = thisColumn.orderColumn;
				}
				params.sort_column = sortColumn + " " + dir;
			}
		}
		
		//autoLoad가 true		: 해당 url로 ajax 바인딩
		//autoLoad가 false	: dummy request를 보낸 후, autoLoad의 값을 true로 변경하여 최초에는 데이터가 로드 되지않고, 
		//					. 향후 reload시 ajax 바인딩을 통해 데이터 호출을 할 수 있도록 만들어준다.
		if(view.config.autoLoad && ""!=url) {
			webix.ajax().bind(view).post(url, JSON.stringify(params), callback);
		} else {
			webix.ajax().bind(view).post(__GRID_EMPTY_DATA_URL, JSON.stringify(params), callback);
			view.config.autoLoad = true;
		}
	}
});

//그리드에서 사용하기 위한 데이터드라이버 정의 - 서버 데이터를 그리드 표시 데이터로 파싱하는 작업을 담당
webix.DataDriver.gridjson = webix.clone(webix.DataDriver.customjson);
webix.expansion(webix.DataDriver.gridjson, {
	getRecords : function(data) {
		var rowsData = [];
		if(data.gridVO) {
			rowsData  = data.gridVO.rows;
		}
		return rowsData;
	},
	getInfo : function(data) {
		var responseData = data.hasOwnProperty("gridVO") ? data.gridVO : undefined;
		if(responseData) {
			return {
				size : (responseData.totalCount || 0),
				from : (responseData.cursorPosition || 0),
				parent : (responseData.parent || 0),
				config : (responseData.config || {}),
				key : (responseData.webix_security)
			};
		}
		return {};
	}
});

//그리드 페이저 프라퍼티 세팅
nkia.ui.grid.GridPager = function(pagerId, props) {
	var pagerProps = props || {};
	this.gridId = props.id;
	this.id = pagerId;
	this.view = "pager";
	this.size = 10;
	this.group = 10;
	this.height = 40;
	this.template = "{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}";
	var gridId = props.id;
	var rowSizeSelectBox = " Size <select id='" + gridId + __ROWCOUNT_SELECT_BOX_SUFFIX + "' onchange=\"javascript:nkia.ui.grid.GridPager.changeRowCount(this, '" + gridId+ "');\">" + __ROWCOUNT_OPTIONS + "</select>";
	this.template  = this.template + rowSizeSelectBox + "　<font color='#0082e2'>Page {common.page()} / #limit#</font>";
	this.css = props.pagerCss || "";
	
	if(pagerProps.pageSize !== undefined) {
		this.size = pagerProps.pageSize;
	}
	if(pagerProps.group !== undefined) {
		this.group = pagerProps.group;
	}
	
	//그리드 페이저에 대한 이벤트 정의
	this.on = {
		"onAfterRender" : function() {
			//그리드의 세팅된 사이즈에 맞게 셀렉트 박스를 선택한다.
			var rowCount = this.config.size;
			var rowCountSelect = document.getElementById(gridId + __ROWCOUNT_SELECT_BOX_SUFFIX);
			var rowCountOptions = rowCountSelect.options;
			var count = rowCountOptions.length;
			for(var i = 0 ; i < count ; i++) {
				if(parseInt(rowCountOptions[i].value) === rowCount) {
					rowCountOptions.selectedIndex = i;
					break;
				}
			};
		},
		"onItemClick" : function(id, e, node) {
			/* 
			 * todo :: 현재 페이지 선택 시 한번이라도 로딩된 페이지는 서버 요청을 안하도록 되어 있다. 
			 * 이에 페이지 클릭 시마다 로드되도록 수정
			 */
			var passTarget = ["prev", "first", "next", "last"]
			if(passTarget.indexOf(id) !== -1) {
				//추가일 경우에도 리로드가 되어야 하는지?;
				return true;
			} else {
				$$(this.config.gridId)._goPage(parseInt(id));				
				return false;
			}
		}
	};	
};

//그리드 페이저의 페이지 사이즈 셀렉트박스 변경 시에 호출되는 함수 - rowcount에 맞도록 그리드를 재설정
nkia.ui.grid.GridPager.changeRowCount = function(selectBox, gridId) {	
	var pageSize = parseInt(selectBox.value.trim(), 10);
	$$(gridId)._changePageSize(pageSize);
};

//체크박스 그리드 요청 시 체크박스 셀을 만들어주는 함수
nkia.ui.grid.column.getCheckBoxColumn = function(gridId) {
	return {
		id : gridId + __CHECK_HEADER_SUFFIX,
		width : 50,	
		checkValue : true,
		uncheckValue : false,
		tooltip : false,
		template : "{common.checkbox()}",
		css:{'text-align': 'center'},
		header : { 
			content : "masterCheckbox",
			contentId : __CHECK_BOX_HEADER_ID
		}
	};
};

//리소스를 통해 그리드에서 사용되는 컬럼을 생성한다.
nkia.ui.grid.column.createColumns = function(grid, gridResources) {
	var gridTexts = [];
	var gridHeaders = [];
	var gridWidths = [];
	var flexs = [];
	var sortable = [];
	var newColumns = [];
	webix.ajax().sync().post(
		nkia.host + "/itg/base/getGridMsgSource.do",
		{ resource_prefix : gridResources },
		function (data){
			var headerData = JSON.parse(data);
			if(headerData.gridText !== null) {
				newColumns = nkia.ui.grid.column.createColumnsForObject(grid, headerData);
			}
		}
	);
	return newColumns;
};

//객체를 통해 그리드에서 사용되는 컬럼을 생성한다.
nkia.ui.grid.column.createColumnsForObject = function(grid, columns){
	var gridTexts = columns.gridText.trim().split(",");
	var gridHeaders = columns.gridHeader.trim().split(",");
	var gridWidths = columns.gridWidth.trim().split(",");
	var flexs = columns.gridFlex.trim().split(",");
	var sortable =  columns.gridSortable.trim().split(",");
	var align =  columns.gridAlign.trim().split(",");
	var xtype = columns.gridXtype.trim().split(",");
	var bgColor = null;
	if(columns.bgColor) {
		bgColor = columns.bgColor.trim().split(",");
	}
	
	var length = gridTexts.length;
	var newColumns = [];
	
	for(var i = 0 ; i < length ; i++) {
		var column = {};
		if("hidden" === xtype[i]) {
			column = {
				id:gridHeaders[i],
				header: {text : gridTexts[i], css : {'text-align': 'center'}} ,
				hidden: true
			};
		} else if("text" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				editor : "text",
				tooltip : false
			};
		} else if("ip" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				xtype : "ip",
				editor : "text",
				tooltip : false
			};
		} else if("integer" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				// validation 체크를 위해 xtype 이라는 속성 부여
				xtype : "integer",
				editor : "text",
				validate: webix.rules.isNumber,
				tooltip : false,
				format : webix.Number.numToStr({
					groupDelimiter:"",
					groupSize:0,
					decimalDelimiter:"",
					decimalSize:0
				})
			};
		} else if("natural" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				// validation 체크를 위해 xtype 이라는 속성 부여
				xtype : "natural",
				editor : "text",
				validate: webix.rules.isNumber,
				tooltip : false,
				format : webix.Number.numToStr({
					groupDelimiter:"",
					groupSize:0,
					decimalDelimiter:"",
					decimalSize:0
				})
			};
		} else if(xtype[i].indexOf("double") > -1) {
			
			var cellDecimalSize = 1;
			if("double_2" === xtype[i]) {
				cellDecimalSize = 2;
			} else if("double_3" === xtype[i]) {
				cellDecimalSize = 3;
			} else if("double_4" === xtype[i]) {
				cellDecimalSize = 4;
			} else if("double_5" === xtype[i]) {
				cellDecimalSize = 5;
			}
			
			// Start *GRID 실수형 DATA 표시 개선* *ITAM 정기점검* - 기존 로직은 제거해도 무방하나, 하위호환성을 위해 남겨둠.
			var strCellCss = {'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE};
			var strEditor = "text";
			
			
			if ( xtype[i].indexOf("_") > -1 ){
				var arrTempXType = xtype[i].split("_");
				if ( arrTempXType.length >= 2 && arrTempXType[1] != null && isNaN(Number(arrTempXType[1])) == false && Number(arrTempXType[1]) >= 1 ){
					cellDecimalSize = Math.floor(Number(arrTempXType[1]));
				} 
				// xtype 이 dobule_{숫자}_na 형태인 경우 수정불가 처리.
				if ( arrTempXType.length >= 3 && arrTempXType[2] != null && arrTempXType[2].trim().toLowerCase() == "na" ){
					strCellCss = {'text-align': align[i], 'background-color': "#FFFFFF" };
					strEditor = null;
				} 
			}
			// End *GRID 실수형 DATA 표시 개선* *ITAM 정기점검*
			
			
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css: strCellCss , 								// *GRID 실수형 DATA 표시 개선*
				// validation 체크를 위해 xtype 이라는 속성 부여
				xtype : "double",
				editor : strEditor , 							// *GRID 실수형 DATA 표시 개선*
				tooltip : false,
				format : webix.Number.numToStr({
					groupDelimiter:"",
					groupSize:0,
					decimalDelimiter:".",
					decimalSize: cellDecimalSize
				})
			};
		} else if("price" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				// validation 체크를 위해 xtype 이라는 속성 부여
				xtype : "price",
				editor : "text",
				tooltip : false,
				format : webix.Number.numToStr({
					groupDelimiter:",",
					groupSize:3,
					decimalDelimiter:"",
					decimalSize:0
				})
			};
		} else if("texttime" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				// validation 체크를 위해 xtype 이라는 속성 부여
				xtype : "texttime",
				tooltip : false,
				editor : "text"
			};
		}else if("hpno" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				// validation 체크를 위해 xtype 이라는 속성 부여
				xtype : "hpno",
				tooltip : false,
				editor : "text"
			};
		}else if("date" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				editor : "date",
				tooltip : false,
				format : webix.Date.dateToStr("%Y-%m-%d")
			};
		}else if("month" === xtype[i]) { //month 추가
			webix.editors.$popup.month = {
					view:"popup",
					body:{
						view:"calendar",
						type:"month",
						icons : true,
						borderless:true
					}
			};
			webix.editors.month = webix.extend({
			popupType:"month"
			}, webix.editors.date);
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
					id:gridHeaders[i],
					header: {text : headerText, css : {'text-align': 'center'}} ,
					width:(parseInt(gridWidths[i],10)),
					minWidth:(parseInt(gridWidths[i],10)),
					fillspace: (flexs[i] === "1") ? true : false,
					css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
					editor : "month",
					format : webix.Date.dateToStr("%Y-%m"),
			};
		}else if("time" === xtype[i]) { //time 추가
			var configA = webix.Date.dateToStr("%Y-%m-%d")
			var configB = webix.Date.dateToStr("%Y-%m-%d at %H:%i")

			webix.editors.$popup.abc = {
					view:"popup",
					body:{
						view:"calendar",
						calendarTime:"%H:%i",
						type:"date",
						icons : true,
						borderless:true,
						timepicker : true
					}
			};
			webix.editors.abc = webix.extend({
			popupType:"abc"
			}, webix.editors.date);
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
					id:gridHeaders[i],
					header: {text : headerText, css : {'text-align': 'center'}} ,
					width:(parseInt(gridWidths[i],10)),
					minWidth:(parseInt(gridWidths[i],10)),
					fillspace: (flexs[i] === "1") ? true : false,
					css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
					editor : "abc",
					tooltip : false,
					format : configB
			};

		}else if("checkbox" === xtype[i]) {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				tooltip : false,
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
				template : "{common.checkbox()}"
			};
		} else if("combo" === xtype[i]) {
			
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			
			var comboUrl = "/itg/base/searchCodeDataList.do";
			var comboEditors = grid.editprops;
			
			if(comboEditors.length > 0){
				
				for( var j=0; j<comboEditors.length; j++ ){
					var comboEditor = comboEditors[j];
					if(comboEditor.column == gridHeaders[i]){
						
						var comboParams = {};
						
						if(comboEditor.url) {
							comboUrl = comboEditor.url;
						}
						if(comboEditor.params) {
							comboParams = comboEditor.params;
						}
						
						// 콤보박스 Store 생성
						var resultList = new Array();
						jq.ajax({
							type:"POST",
							contentType: "application/json",
							async: false,
							dataType: "json",
							url: nkia.host + comboUrl,
							data: JSON.stringify(comboParams),
							success: function(response){
								if(response.success){
									var responseList = [];
									if(response.gridVO) {
										responseList = response.gridVO.rows;
									}
									if(responseList) {
										if(comboEditor.attachChoice===true) {
											resultList.push({id:__EMPTY_SELECTBOX_ID, value: __GRID_COMMON_MSG.COMBO_ATTACH_CHOICE});
										} else if(comboEditor.attachAll===true) {
											resultList.push({id:__EMPTY_SELECTBOX_ID, value: __GRID_COMMON_MSG.COMBO_ATTACH_ALL});
										}
										for (var i in responseList){
											var comboStore = {};
											comboStore.id = responseList[i].CODE_ID;
											comboStore.value = responseList[i].CODE_TEXT;
											resultList.push(comboStore);
										}
									}
								}
							}
						});
						
						column = {
							id:gridHeaders[i],
							header: {text : headerText, css : {'text-align': 'center'}} ,
							width:(parseInt(gridWidths[i],10)),
							minWidth:(parseInt(gridWidths[i],10)),
							fillspace: (flexs[i] === "1") ? true : false,
							css:{'text-align': align[i], 'background-color':__EDITABLE_CELL_COLOR_CODE},
							editor: 'select',
							tooltip : false,
							options: resultList
						};
						
						break;
					}
				}
			}
		} else if("actioncolumn" === xtype[i]) {
			
			var headerValue = gridHeaders[i];
			var actionColumns = grid.actionColumns;
			
			var actionColumn;
			
			for( var j=0; j<actionColumns.length; j++ ){
				actionColumn = actionColumns[j];
				if(actionColumn.column == headerValue){
					// 이벤트 바인드 처리
					if(grid.onClick){
						for(var key in actionColumn.onClick){
							grid.onClick[key] = actionColumn.onClick[key];	
						}
					}else{
						grid.onClick = actionColumn.onClick;
					}
					
					var _template;
					if(typeof actionColumn.template == "function") {
						_template = actionColumn.template;
					} else {
						_template = function(obj, a, b, col) {
							var myAction = col.myAction;
							var actCol = JSON.parse(JSON.stringify(myAction.column));
							var actTemplate = JSON.parse(JSON.stringify(myAction.template));
							var actCondition = {};
							if(typeof myAction.conditions != "undefined") {
								actCondition = JSON.parse(JSON.stringify(myAction.conditions));
							}
							if(actCondition!=null && JSON.stringify(actCondition)!="{}") {
								for(var key in actCondition) {
									var conditionArray = actCondition[key];
									var result = false;
									for(var idx in conditionArray) {
										var checkValue = conditionArray[idx];
										if(checkValue.charAt(0) == "!") {
											checkValue = checkValue.replace("!", "");
											if(obj[key] && obj[key] != checkValue) {
												return actTemplate;
											}
											if(obj[key] == ""){
												if(obj[key] != checkValue) {
													return actTemplate;
												}
											}
										} else {
											if(obj[key] && obj[key] == checkValue) {
												return actTemplate;
											}
										}
									}
									if(result) {
										return actTemplate;
									} else {
										return "";
									}
								}
							} else {
								return actTemplate;
							}
						};
					}
					
					column = {
						id:headerValue,
						header: {text : gridTexts[i], css : {'text-align': 'center'}} ,
						width:(parseInt(gridWidths[i],10)),
						minWidth:(parseInt(gridWidths[i],10)),
						fillspace: (flexs[i] === "1") ? true : false,
						css:{'text-align': align[i] },
						myAction: actionColumn,
						tooltip : false,
						template: _template
					};
					break;
				}
			}
		} else if("icon" === xtype[i]) {
			var iconProps = grid.iconprops;
			if(iconProps.length > 0){
				for( var j = 0; j < iconProps.length; j++ ){
					var iconProp = iconProps[j];
					if(iconProp.column == gridHeaders[i]){
						column = {
							id:gridHeaders[i],
							header: {text : gridTexts[i], css : {'text-align': 'center'}} ,
							width:(parseInt(gridWidths[i],10)),
							minWidth:(parseInt(gridWidths[i],10)),
							fillspace: (flexs[i] === "1") ? true : false,
							css:{'text-align': align[i] },
							tooltip : false,
							orderColumn: iconProp.targetColumn
						};
						if(sortable.length > 0 && sortable[i] === "1") {
							column.sort = "server";
						}
						break;
					}
				}
			}
		} else {
			var requiredColumns = grid.requiredColumns;
			var headerIds = gridHeaders[i];
			var headerText = gridTexts[i];
			if(requiredColumns.length > 0){
				for( var j=0; j<requiredColumns.length; j++ ){
					if(headerIds == requiredColumns[j]) {
						headerText = (headerText + "<font color=red> *</font>");
					}
				}
			}
			
			var cellCss = {
				'text-align' : align[i]
			};
			if(bgColor != null) {
				var cellColor = bgColor[i];
				if(bgColor[i] == "na") {
					cellColor = "#FFFFFF";
				}
				cellCss['background-color'] = cellColor;
			}
			var tool;
			if(gridHeaders[i] == "DISPLAY_HP_NO"){
				tool = "#HP_NO#";
			}else{
				tool = false;
			}
			column = {
				id:gridHeaders[i],
				header: {text : headerText, css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css: cellCss,
				tooltip : tool
			};
			if(sortable.length > 0 && sortable[i] === "1") {
				if(grid.sortConfig != "server"){
					column.sort = grid.sortConfig;
				}else{
					column.sort = "server";
				}
			}
		}
		newColumns.push(column);
	}
	
	// 추가되는 Columns 처리
	if(grid.appendColumns.length > 0){
		grid.appendColumns.forEach(function(column){
			column.header = {text : column.text, css : {'text-align': 'center'}};
			
			// 이벤트 바인드 처리
			if(grid.onClick){
				for(var key in column.onClick){
					grid.onClick[key] = column.onClick[key];	
				}
			}else{
				grid.onClick = column.onClick;
			}
			newColumns.push(column);
		});
	}
	
	// Header Merge 정보가 있을 시, Header 컬럼 Merge 수행
	if(grid.headerMerges.length > 0) {
		
		var mergeList = grid.headerMerges;
		
		for(var m=0; m<mergeList.length; m++) {
			
			var mergePropList = mergeList[m];
			
			for(var i=0; i<mergePropList.length; i++) {
				var mergeRange = 0;
				var mergeCount = 0;
				for(var j=0; j<newColumns.length; j++) {
					var isFirst = false;
					if(mergePropList[i].column == newColumns[j].id) {
						mergeRange = mergePropList[i].merge_range;
						mergeCount = 1;
						isFirst = true;
						var newHeader = new Array();
						newHeader.push( {text: mergePropList[i].view_text, colspan: mergeRange, css: "webix_hcell_bg"} );
						newColumns[j].header = new Array().concat(newHeader, newColumns[j].header);
						mergeCount++;
					}
					if(!isFirst && mergeCount>1) {
						if(mergeCount > mergeRange) {
							var mergeRange = 1;
							var mergeCount = 1;
						} else {
							newColumns[j].header = new Array().concat("", newColumns[j].header);
							mergeCount++;
						}
					}
				}
			}
		}
	}
	
	return newColumns;
};

//그리드의 바디 영역의 프라퍼티를 만들어 주는 함수
nkia.ui.grid.body.getDataTableBody = function(bodyProps) {
	var grid = new nkia.ui.grid.BaseGrid();
	grid.setProps(bodyProps);

	//프라퍼티의 컬럼 세팅 정보가 초기 값인 Array가 아닐 경우 세팅되서 넘어오는 값은 Object이다. 
	if(grid.columns.constructor !== Array) {
		grid.setColumnsForObject();
	} else {
		grid.setColumns();
	}
	
	//그리드 데이터 로드 호출 시 이벤트 (비활성화 시켜 클릭 할 수 없도록 한다.)
	grid.on.onBeforeLoad = function() {
		$$(this.config.id).disable();
	};

	//그리드 데이터 로드 완료 이벤트
	grid.on.onAfterLoad =  function() {
		this.checkedRows = {};
		if (this.count() < 1) {
			this.showOverlay(PAGE_MASSEGE.MSG_NO_DATA);
			if(this.config.pager) {
				// 데이터 없을 시, 페이저 숨김
				this.config.pager.page = 0;
				var pager = this.getPager();
				pager.hide();
			}
		} else {
			this.hideOverlay();
			if(this.config.pager) {
				var pager = this.getPager();
				pager.show();
			}
		}
		
		// 그리드 조건에 대한 icon 처리
		if(this.config.iconprops) {
			this._setRowIconByConditions(this.config.iconprops);
		}
		
		// Cell Merge 처리
		if(this.config.cellMerges && this.config.cellMerges.length > 0) {
			this._setCellMerges(this.config.cellMerges);
		}
		
		// Row Merge 처리
		if(this.config.rowMerges && this.config.rowMerges.length > 0) {
			this._setRowMerges(this.config.rowMerges);
		}
		
		$$(this.config.id).enable();
		
		// 뱃지ID가 있다면 뱃지의 카운트를 업데이트한다.
		this._setBadgeCount();
	};
	
	//그리드 데이터 렌더 완료 이벤트 (Grid의 View에 변화가 있을때마다 계속 호출되니, 주의해서 사용할 것)
	grid.on.onAfterRender =  function(data) {
		// 그리드가 새로 렌더링 될 때마다 헤더 카운트를 업데이트 한다.
		this._updateTotalCount(data.order.length);
		
		if(this.config.checkbox === true) {
			// 체크박스 그리드의 경우 렌더링 될 때마다 체크된 행의 카운트를 업데이트 한다.
			if(this._getSelectedRows()) {
				var checkCount = this._getSelectedRows().length;
				this._updateCheckCount(checkCount);
			}
			// 디펜던시 그리드의 경우 선택된 값 css 핸들링
			if(this.config.dependency && (this.config.dependency.targetGridId || this.config.dependency.data)){
				this._setDisabledRowByOtherGridKeys();
			}
			
			// 그리드 조건에 대한 Row 처리
			if(this.config.rowConditions) {
				var conditionType = this.config.rowConditions.type;
				var condtions = this.config.rowConditions.conditions;
				if(conditionType == "disabled") {
					// 특정 조건에 부합하는 Row는 선택할 수 없도록 해준다.
					this._setRowDisabledByConditions(condtions);
				} else if(conditionType == "read") {
					// 특정 조건에 부합하는 Row는 '읽음' 상태로 표시 해준다.
					this._setRowReadByConditions(condtions);
				} else if (conditionType == "checkbox_disabled") {
					this._setCheckboxDisabledByConditions(condtions);
				}
			}
		}
		
		// 뱃지ID가 있다면 뱃지의 카운트를 업데이트한다.
		this._setBadgeCount();
	};
	
	//입력가능 그리드에서 특정 row만 입력하지 못하게 하고 싶을때, 
	//back or front 에서 [컬럼명+'_readonly'] 라는 맵 key를 생성하여 true값을 주면 
	//해당 row 내의 해당 column은 입력 불가 상태가 된다.
	grid.on.onBeforeEditStart = function(id) {
		var row = id.row;
		var column = id.column;
		
		//var isReadOnly = this.getItem(row)[column+"_readonly"];
		var isReadOnly = false;
		
		if(isReadOnly && (isReadOnly===true || isReadOnly==='true')) {
			alert(__GRID_COMMON_MSG.DO_NOT_EDIT);
			return false;
		}
	};
	
	// 입력가능 그리드에서 값 입력 완료 시, 벨리데이션 체크
	grid.on.onAfterEditStop = function(state, editor, ignoreUpdate) {
		
		var thisXtype = editor.config.xtype;
		
		if(thisXtype && state.value != "") {
			
			switch(thisXtype) {
				case 'natural' :
					var valid = /^([1-9][0-9]*)$/;
					if(!valid.test(state.value)) {
						alert(__GRID_VALID_MSG.NOT_NATURAL);
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = "";
						this.updateItem(editor.row, thisRow);
					}
					break;
				case 'integer' :
					var valid = /^([0-9-]+)$/;
					if(!valid.test(state.value)) {
						alert(__GRID_VALID_MSG.NOT_INTEGER);
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = "";
						this.updateItem(editor.row, thisRow);
					}
					break;
				case 'double' :
					var valid = /^([0-9.-]+)$/;
					if(!valid.test(state.value)) {
						alert(__GRID_VALID_MSG.NOT_DOUBLE);
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = "";
						this.updateItem(editor.row, thisRow);
					}
					break;
				case 'price' :
					var valid = /^[+-]?\d*([0-9]+)$/;
					if(!valid.test(state.value)) {
						alert(__GRID_VALID_MSG.NOT_PRICE);
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = "0";
						this.updateItem(editor.row, thisRow);
					}
					break;
				case 'ip' :
					var valid = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/;
					if(!valid.test(state.value)) {
						alert(__GRID_VALID_MSG.NOT_IP);
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = "";
						this.updateItem(editor.row, thisRow);
					}
					break;
				case 'texttime' :
					var valid = /^([0-1]?[0-9]|2[0-3]):?[0-5][0-9]$/;
					var value = state.value;
					if(!valid.test(state.value)) {
						alert(__GRID_VALID_MSG.NOT_TEXTTIME);
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = "";
						this.updateItem(editor.row, thisRow);
					}else if(valid.test(state.value)){
						var thisRow = this.getItem(editor.row);
						if(value.length==3){
							thisRow[editor.column] = value.substr(0,1)+":"+value.substr(1,2);
						}else if(value.length==4){
							thisRow[editor.column] = value.substr(0,2)+":"+value.substr(2,2);
						}
					}
					break;
				case 'hpno' :
					var valid =  /^\d{3}-?\d{4}-?\d{4}$/;
					var value = state.value;
					if(!valid.test(state.value)) {
						alert(__GRID_VALID_MSG.NOT_HPNO);
						var thisRow = this.getItem(editor.row);
						thisRow[editor.column] = "";
						this.updateItem(editor.row, thisRow);
					}else if(valid.test(state.value)){
						var thisRow = this.getItem(editor.row);
						if(value.length==11){
							thisRow[editor.column] = value.substr(0,3)+"-"+value.substr(3,4)+"-"+value.substr(7,4);
						}
					}
					break;
			}
			
			// 에디터 그리드에서 수행할 Function
			var func = this.config.afterEditFunc;
			if(func != null){
				func(this, editor);
				return false;
			}
		}
	};
	
	// 그리드 데이터 선택 이벤트
	grid.on.onItemClick = function(id, e, node) {
		var gridId = this.config.id;
		var checkBoxColumnId = gridId + __CHECK_HEADER_SUFFIX;
		var clickRowId = id.row;
		var clickColumnId = id.column;
		var item = this._convertGridDataValue(this.getItem(clickRowId));
		
		// item이 비어 있으면 item click 이벤트 실행하지 않음
		if( !item || JSON.stringify(item) === JSON.stringify({}) ) {
			return false;
		}
		
		// checkbox 그리드 일 시 이벤트
		if(bodyProps.checkbox === true) {
			
			// 클릭한 컬럼이 체크박스 컬럼일때만 선택 이벤트 수행하도록 변경
			if(clickColumnId && clickColumnId != checkBoxColumnId) {
				return false;
			}
			
			// checkbox 상태가 disable이면 item click 이벤트 실행하지 않음
			if(typeof item["check_disabled"] !== "undefined" && item["check_disabled"] === true ){
				return false;
			}
			
			if(typeof item[checkBoxColumnId] === "undefined" || item[checkBoxColumnId] === false ){
				this.addRowCss(clickRowId, "webix_row_select");
				item[checkBoxColumnId] = true;
			}else{
				this.removeRowCss(clickRowId, "webix_row_select");
				item[checkBoxColumnId] = false;
			}
			  
			this.updateItem(clickRowId, item);
			
		// checkbox 그리드가 아닐 시 이벤트
		} else {
			if(typeof item["check_disabled"] !== "undefined" && item["check_disabled"] === true ){
				this.unselect(id);
				return false;
			}
		}
	};
	
	if(bodyProps.checkbox === true) {
		
		var checkboxColumns = nkia.ui.grid.column.getCheckBoxColumn(grid.id);
		
		grid.columns.splice(0, 0, checkboxColumns);
		
		// 그리드 컬럼 내에서 checkbox template을 썼을 경우만 동작하므로 따로 분기가 필요없이 이벤트를 바인딩 시켜준다.
		grid.on["onCheck"] = function(rowId, column, state) {
			var gridId = this.config.id;
			var checkBoxId = gridId + __CHECK_HEADER_SUFFIX;
			var item = this._convertGridDataValue(this.getItem(rowId));
			
			if(typeof item["check_disabled"] !== "undefined" && item["check_disabled"] === true ){
				state = false;
				return false;
			}
			
			if(state===true) {
				this.addRowCss(rowId, "webix_row_select");
				item[checkBoxId] = true;
			} else {
				this.removeRowCss(rowId, "webix_row_select");
				item[checkBoxId] = false;
			}
			
			this.updateItem(rowId, item);
		};
	}
	
	return grid;
};

//그리드 프라퍼티(!) 생성자
nkia.ui.grid.BaseGrid = function() {
	//속성을 초기화합니다.
	this.id = "";
	this.view = "datatable";
	this.columns = [];
	this.data = [];
	this.resizeColumn = false;
	this.autoheight = false;
	this.height = 0;
	this.prerender = true;
	this.autoLoad = true;
	this.select = true;
	this.clipboard = true;
	this.blockselect = false;
	this.minimizeCount = false;
	this.required = false;
	this.editable = true;
	this.editaction = "click";
	this.datatype = "gridjson";
	this.pageable = false;
	this.pageSize = 10;
	this.params = {};
	this.url = __GRID_URL_PREFIX;
	this.resource = "";
	this.checkbox = false;
	this.on = {};
	this.onClick = {};
	this.dependency = {};
	this.keys = [];
	this.badgeId = "";
	this.spans = false;
	this.checkedRows = {};
	this.actionColumns = [];
	this.appendColumns = [];
	this.requiredColumns = [];
	this.editprops = [];
	this.iconprops = [];
	this.rowConditions = {};
	this.cellMerges = [];
	this.rowMerges = [];
	this.headerMerges = [];
	// GUI-디자이너에서 사용하는 속성
	this.insertQuerykey = "";
	this.deleteQuerykey = "";
	this.editorMode = false;
	this.drag = false;
	this.helper = "";
	this.helperWidth = 200;
	this.helperHeight = 100;
	this.afterEditFunc=null;
	this.scroll = true;
	this.scrollAlignY = true;
	this.scrollX = true;
	this.scrollY = true;
	this.rowHeight = 23;
	this.rowLineHeight = 0;
	this.headerRowHeight = 23;
	this.css = "";
	this.scheme = "";
	this.minHeight = "";
	this.fixedRowHeight = false;
	this.sortConfig = "server";
	this.leftSplit = 0;
	this.tooltip = true;
	
	this.propsValidate = function(props) {
		nkia.ui.validator.grid(props);
	};
	
	var attachEvent = {};
	
	this.ready = function() {
		//기존에 정의된 이벤트들이 있어 on으로 이벤트를 덮어쓸경우 문제가 발생함. 화면 렌더링 이후 attachEvent를 통해 추가하도록 해야함 
		for(var eventName in attachEvent) {
			this.attachEvent(eventName, attachEvent[eventName]);
		}
	};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				if (i === "url") {
					this[i] = this.url + nkia.host + props[i];
				} else if (i === "params") {
					//파라메터의 경우 딥카피를 하지 않을 경우 기존 프라퍼티 객체를 바라보게 되므로 수정 시 문제 소지가 있음.
					this[i] = nkia.ui.utils.copyObj(props[i]);
				} else if (i === "on") {
/*					if ((props[i].hasOwnProperty("onItemDblClick") && typeof(props[i].onItemDblClick) == "function") || 
						(props[i].hasOwnProperty("onItemClick") && typeof(props[i].onItemClick) == "function")) {
						this.css = {"cursor": "pointer !important"};
					}*/
					attachEvent = props[i];
				} else {
					this[i] = props[i];
				}
			} else {
				if (i == "rowLink" && props[i]) {
					this.css = {"cursor": "pointer !important"};
				}
			}
		}

		
		// checkbox grid의 경우 select event를 사용하지 않으나, props에 설정되어 있으면 설정이 우선된다.
		this.select = (typeof props.select == "undefined")? !this.checkbox : props.select;
		
		// autoLoad가 false 이고, url이 없다면 메모리 그리드 사용을 위해 페이징을 없앤다.
		if(this.autoLoad===false && this.url==__GRID_URL_PREFIX) {
			this.pageable = false;
		}
		
		// merge를 사용할 경우 spans를 부여하고, 페이징/체크박스/선택 은 불가로 만들어준다.
		if(props.cellMerges || props.rowMerges) {
			this.spans = [];
			this.pageable = false;
			this.select = false;
			this.checkbox = false;
		}
		
		if(this.height == 0){
			delete this.height;
		}
		
		if(props.data === undefined){
			delete this.data;
		}
		
		if(props.required === true){
			this.required = true;
		}
	};
	
	//직접입력그리드 계산식이벤트
	this.afterEditFunc = function(gridObj, cellObj){
		var row = gridObj.getItem(cellObj.row);
		
		var cntCell = row.CNT;
		var priceCell = row.COST;
		
		if(typeof cntCell != "undefined" && typeof priceCell != "undefined"){
			var calcResult = cntCell * priceCell;
			
			var result = webix.Number.format(calcResult,{
				groupDelimiter:",",
				groupSize:3
			});
			
			row["RESULT"] = result;
			gridObj.refresh();
		}
	};
	
	this.setColumns = function() {
		this.columns = nkia.ui.grid.column.createColumns(this, this.resource);
	};

	this.setColumnsForObject = function() {
		this.columns = nkia.ui.grid.column.createColumnsForObject(this, this.columns);
	};
};

//데이터테이블 프라퍼티 검증함수
nkia.ui.validator.grid = function(props) {
	//반드시 있어야함
};

//그리드에 Progress Bar를 붙임
webix.extend(webix.ui.datatable, webix.ProgressBar);
//그리드의 확장 메소드
webix.extend(webix.ui.datatable, {
	//그리드 데이터를 새롭게 로드한다.
	_reload : function(newParams, isGoPage, callback) {
		var _this = this;
		var pageNum = 1;
		
		if(_this.getHeaderContent(__CHECK_BOX_HEADER_ID)) {
			_this.getHeaderContent(__CHECK_BOX_HEADER_ID).uncheck();
		}
		
		_this.clearAll();
		
		if(_this.config.pager) {
			if(isGoPage === true) {
				pageNum = this._getPageNum();
			}
			_this.config.pager.page = 0;
		}
		
		if(newParams) {
			_this.config.params = nkia.ui.utils.copyObj(newParams);
		}
		
		var doStuff;
		if(callback) {
			var originCallback = callback;
			doStuff = function() {
				originCallback.apply(this, arguments);
				_this.refresh();
			};
		} else {
			doStuff = function() {
				_this.refresh();
			};
		}
		
		if(isGoPage === true) {
			this._goPage(pageNum-1, callback);
		} else {
			_this.load(__GRID_URL_PREFIX + _this.config.url.source, doStuff);
		}
	},
	
	_goPage : function(pageNum, callback) {
		var _this = this;
		if(_this.getHeaderContent(__CHECK_BOX_HEADER_ID)) {
			_this.getHeaderContent(__CHECK_BOX_HEADER_ID).uncheck();
		}
		_this.clearAll();
		_this.config.pager.page = pageNum;
		
		var doStuff;
		if(callback) {
			var originCallback = callback;
			doStuff = function() {
				originCallback.apply(this, arguments);
				_this.refresh();
			};
		} else {
			doStuff = function() {
				_this.refresh();
			};
		}
		
		_this.load(__GRID_URL_PREFIX + _this.config.url.source, doStuff);
	},	
	//그리드의 페이지 사이즈를 변경한다.
	_changePageSize : function(pageSize) {
		if(!pageSize) {
			throw new Error(__GRID_COMMON_MSG.PAGE_SIZE_NOT_EXIST);
		}
		var _this = this;
		if(_this.getHeaderContent(__CHECK_BOX_HEADER_ID)) {
			_this.getHeaderContent(__CHECK_BOX_HEADER_ID).uncheck();
		}
		_this.clearAll();
		_this.config.pageSize = pageSize;
		_this.config.pager.size = pageSize;
		_this.load(__GRID_URL_PREFIX + _this.config.url.source, function() {
			_this.refresh();
		});
		
	},
	//그리드의 모든 데이터 행을 탐색한다.
	_eachRow : function(handler, all) {
		var totalDataSize = this.data.order.length;
		var pager = this.config.pager;
		// 현재 페이지 번호, 1 페이지로 default setting
		var nowPageNum = 1;
		// loop를 수행할 데이터 개수, 전체 데이터 개수로 default setting
		var dataCount = totalDataSize;
		// 페이징이 있는 그리드 일 경우
		if(pager) {
			// 총 페이지 개수
			var totalPageNum = this.config.pager.limit;
			// 한 페이지 내에 들어갈 데이터 개수
			var pageDataSize = this.config.pageSize;
			// 현재 페이지 번호(인덱스 번호 이므로 +1 해준다)
			nowPageNum = this.config.pager.page + 1;
			// 한 페이지 내에 들어갈 데이터 개수로 default setting
			dataCount = pageDataSize;
			// 현재 페이지 번호가 마지막 페이지 일 경우 마지막 페이지 데이터 개수를 구한다.
			if(nowPageNum == totalPageNum) {
				dataCount = totalDataSize - ((totalPageNum-1) * pageDataSize);
			}
		}
		start = ((nowPageNum - 1) * pageDataSize);
		for(var i = start, count = 0 ; count < dataCount; i++, count++) {
			handler.call(this, this.getIdByIndex(i));
		}
	},
	//그리드의 데이터를 조회한다. (페이징이 있을 시 현재 페이지 내의 데이터만)
	_getRows : function() {
		var rowList = [];
		if (this.config.pageable) {
			this._eachRow(function(row) {
				var item = this._convertGridDataValue(this.getItem(row));
				rowList.push(item);
			}, true);		
		} else {
			this.eachRow(function(row) {
				var item = this._convertGridDataValue(this.getItem(row));
				rowList.push(item);
			}, true);
		}
		return rowList;
	},
	//선택된 row의 데이터를 조회한다.
	_getSelectedRows : function() {
		var selectedRowList = new Array();
		// 체크박스 그리드일 경우 체크된 row 배열 리턴
		if(this.config.checkbox === true) {
			var checkBoxId = this.config.id + __CHECK_HEADER_SUFFIX;
			// 페이징이 있는 그리드일 경우 커스텀 _eachRow 함수 사용 (사용하지 않으면 전체 그리드 데이터 탐색함)
			if (this.config.pageable) {
				this._eachRow(function(row) {
					var item = this._convertGridDataValue(this.getItem(row));
					if(item){
						if(typeof item[checkBoxId] !== "undefined" && item[checkBoxId] === true ){
							selectedRowList.push(item);
						}
					}
				}, true);
			} else {
				this.eachRow(function(row) {
					var item = this._convertGridDataValue(this.getItem(row));
					if(item){
						if(typeof item[checkBoxId] !== "undefined" && item[checkBoxId] === true ){
							selectedRowList.push(item);
						}
					}
				}, true);
			}
		} 
		// 일반 그리드일 경우 현재 선택된 row를 배열로 리턴(1-item array)
		else if(this.getSelectedItem()){
			var item = this._convertGridDataValue(this.getSelectedItem());
			selectedRowList.push(item);
		}
		return selectedRowList;
	},
	//그리드 컬럼 정보를 변경한다.(객체로 변경)
	_updateHeader : function(columns) {
		this.config.columns = nkia.ui.grid.column.createColumnsForObject(this.config, columns);
		if(this.config.checkbox === true) {
			var checkboxColumns = nkia.ui.grid.column.getCheckBoxColumn(this.config.id);
			this.config.columns.splice(0, 0, checkboxColumns);
		}
		this.refreshColumns();
	},
	//그리드 컬럼 정보를 변경한다.(리소스로 변경)
	_updateHeaderFromResource : function(resource) {
		this.config.columns = nkia.ui.grid.column.createColumns(this.config, resource);
		if(this.config.checkbox === true) {
			var checkboxColumns = nkia.ui.grid.column.getCheckBoxColumn(this.config.id);
			this.config.columns.splice(0, 0, checkboxColumns);
		}
		this.refreshColumns(this.config.columns,true);
	},
	//그리드의 전체 카운트를 변경한다.
	_updateTotalCount : function(totalCount) {
		var totalCountSpan = document.getElementById(this.config.id + __TOTAL_COUNT_SPAN_SUFFIX);
		if(totalCountSpan) {
			totalCountSpan.innerText = totalCount;
		}
	},
	//그리드의 체크row 카운트를 변경한다.
	_updateCheckCount : function(checkCount) {
		var checkCountSpan = document.getElementById(this.config.id + __CHECK_COUNT_SPAN_SUFFIX);
		if(checkCountSpan) {
			checkCountSpan.innerText = checkCount;
		}
	},
	//그리드 row를 추가한다.
	_addRow : function(data) {
		var rows = this._getRows();
		var keys = [];
		var keyLength = 0;
		var keyCheckCount = 0;
		
		// 인자값으로 전달된 데이터가 없으면 현재 그리드의 컬럼정보를 읽어, 빈 값들로 구성된 store를 만들어서 add 한다.
		if(!data || data==null || data=={}) {
			var cols = this.config.columns;
			var emptyStore = {};
			for(var i in cols) {
				var colId = cols[i].id;
				// 체크박스 헤더는 put하지 않는다.
				if(colId.indexOf(__CHECK_HEADER_SUFFIX) < 0) {
					emptyStore[colId] = "";
				}
			}
			this.add(emptyStore);
		} else {
			// deep copy
			var copyData = JSON.parse(JSON.stringify(data));
			
			if(this.config.keys && this.config.keys.length > 0) {
				keys = this.config.keys;
				keyLength = keys.length;
				
				// 키 배열의 값을 한 문장으로 만듬
				if(copyData.length > 0 && !copyData[0].CONCAT_KEY_STRING) {
					for(var i in keys) {
						for(var j=0; j<copyData.length; j++) {
							if(copyData[j].CONCAT_KEY_STRING) {
								copyData[j].CONCAT_KEY_STRING = copyData[j].CONCAT_KEY_STRING + copyData[j][keys[i]];
							} else {
								copyData[j].CONCAT_KEY_STRING = copyData[j][keys[i]];
							}
						}
					}
				}
				if(rows.length > 0 && !rows[0].CONCAT_KEY_STRING) {
					for(var i in keys) {
						for(var j=0; j<rows.length; j++) {
							if(rows[j].CONCAT_KEY_STRING) {
								rows[j].CONCAT_KEY_STRING = rows[j].CONCAT_KEY_STRING + rows[j][keys[i]];
							} else {
								rows[j].CONCAT_KEY_STRING = rows[j][keys[i]];
							}
						}
					}
				}
				
				// 같은 키가 있는지 비교하여 같은 키가 있으면 add 하지 않음
				for(var i=0; i<copyData.length; i++) {
					var isExist = false;
					for(var j=0; j<rows.length; j++) {
						if(rows[j].CONCAT_KEY_STRING == copyData[i].CONCAT_KEY_STRING) {
							isExist = true;
							break;
						}
					}
					if(!isExist) {
						// webix 고유 rowid, check disabled 제거 후 add
						delete copyData[i].id;
						delete copyData[i].check_disabled;
						this.add(copyData[i]);
					}else{
						delete copyData[i].id;
						delete copyData[i].check_disabled;
					}
				}
			} else {
				// 키가 없으면 같은 값 있는지 체크하지 않고 추가
				for(var i=0; i<copyData.length; i++) {
					delete copyData[i].id;
					delete copyData[i].check_disabled;
					this.add(copyData[i]);
				}
			}
		}
		
		// 전체 체크박스 reset
		if(this.getHeaderContent(__CHECK_BOX_HEADER_ID)) {
			this.getHeaderContent(__CHECK_BOX_HEADER_ID).uncheck();
		}
		
		this.hideOverlay();
		this.enable();
		this.hideProgress();
	},
	//그리드 row를 삭제한다.
	_removeRow : function(rowId) {
		var isRemove = false;
		if(typeof rowId != "undefined") {
			//인자값으로 rowId가 넘어올 경우 해당 row만 삭제한다.
			this.remove(rowId);
			isRemove = true;
		}else{
			//rowId가 없을 경우 이벤트를 호출한 그리드의 선택된 값을 모두 삭제한다.
			var selectedRows = this._getSelectedRows();
			if(selectedRows.length > 0){
				for(var i in selectedRows){
					this.remove(selectedRows[i].id);
				}
				isRemove = true;	
			}
		}
		if(isRemove){
			if(this.getHeaderContent(__CHECK_BOX_HEADER_ID)) {
				this.getHeaderContent(__CHECK_BOX_HEADER_ID).uncheck();
			}
			if(this.count() < 1) {
				this.showOverlay(PAGE_MASSEGE.MSG_NO_DATA);
				if(this.config.pager) {
					this.config.pager.page = 0;
				}
			}
		}else{
			nkia.ui.utils.notification({
                type: 'error',
                message: __GRID_COMMON_MSG.SELECTED_DATA_NOT_EXIST
            });
		}
		
		this.hideOverlay();
		this.enable();
		this.hideProgress();
	},
	//그리드 전체 row를 삭제한다.
	_removeAllRow : function() {
		this.clearAll();
		if(this.count() < 1) {
			this.showOverlay(PAGE_MASSEGE.MSG_NO_DATA);
			if(this.config.pager) {
				this.config.pager.page = 0;
			}
		}
		if(this.getHeaderContent(__CHECK_BOX_HEADER_ID)) {
			this.getHeaderContent(__CHECK_BOX_HEADER_ID).uncheck();
		}
		
		this.hideOverlay();
		this.enable();
		this.hideProgress();
	},
	//의존관계에 있는 그리드의 key와 중복되는 값이 있는 Row는 사용 불가로 만든다.
	_setDisabledRowByOtherGridKeys : function() {
		var thisRows = this._getRows();
		var targetRows = [];
		var keys = this.config.keys;
		var keyLength = 0;
		
		if(this.config.dependency.targetGridId !== undefined) {
			var targetGrid = $$(this.config.dependency.targetGridId);
			if(targetGrid){
				targetRows = targetGrid._getRows();
			}else {
				if(this.config.dependency.data !== undefined){
					targetRows = this.config.dependency.data;
				}
			}
		} else {
			if(this.config.dependency.data !== undefined){
				targetRows = this.config.dependency.data;
			}
		}
		
		if(thisRows.length > 0 && thisRows[0] !== undefined){
			
			// 키 배열의 값을 한 문장으로 만듬
			if(targetRows.length > 0 && !targetRows[0].CONCAT_KEY_STRING) {
				for(var i in keys) {
					for(var j=0; j<targetRows.length; j++) {
						if(targetRows[j].CONCAT_KEY_STRING) {
							targetRows[j].CONCAT_KEY_STRING = targetRows[j].CONCAT_KEY_STRING + targetRows[j][keys[i]];
						} else {
							targetRows[j].CONCAT_KEY_STRING = targetRows[j][keys[i]];
						}
					}
				}
			}
			if(thisRows.length > 0 && !thisRows[0].CONCAT_KEY_STRING) {
				for(var i in keys) {
					for(var j=0; j<thisRows.length; j++) {
						if(thisRows[j].CONCAT_KEY_STRING) {
							thisRows[j].CONCAT_KEY_STRING = thisRows[j].CONCAT_KEY_STRING + thisRows[j][keys[i]];
						} else {
							thisRows[j].CONCAT_KEY_STRING = thisRows[j][keys[i]];
						}
					}
				}
			}
			
			// 같은 키가 있는지 비교하여 같은 키가 있으면 체크박스 선택 못하도록 disable
			for(var i=0; i<thisRows.length; i++) {
				if(thisRows[i]) {
					for(var j=0; j<targetRows.length; j++) {
						if(thisRows[i].CONCAT_KEY_STRING == targetRows[j].CONCAT_KEY_STRING) {
							thisRows[i]["check_disabled"] = true;
							var checkboxHtml = this.getItemNode(thisRows[i]["id"]);
							jq(checkboxHtml).find("input[type=checkbox]").prop("disabled", true);
							this.addRowCss(thisRows[i]["id"], "disabled");
							break;
						}
					}
				}
			}
		}
	},
	//현재 페이지 번호 리턴 : 페이징이 없는 그리드에선 무조건 1 리턴
	_getPageNum : function() {
		var pager = this.config.pager;
		var pageNum = 1;
		if(pager) {
			pageNum = this.config.pager.page + 1;
		}
		return pageNum;
	},
	// Checked Row 함수. 
	_setCheckedRow : function(row) {
		var checkboxHtml = this.getItemNode(row.id||row);
		if(typeof checkboxHtml != "undefined"){
			jq(checkboxHtml).find("input[type=checkbox]").prop("checked", true);
		}
		row[this.config.id + __CHECK_HEADER_SUFFIX] = true;
	},
	// Disabled Row 함수. 
	_setDisabledRow : function(row) {
		var checkboxHtml = this.getItemNode(row.id||row);
		if(typeof checkboxHtml != "undefined"){
			jq(checkboxHtml).find("input[type=checkbox]").prop("disabled", true);
			this.addRowCss(row.id, "disabled");
		}
		row["check_disabled"] = true;
	},
	// Tab grid에서의 Badge Count 업데이트
	_setBadgeCount : function() {
		// 뱃지ID가 있다면 뱃지의 카운트를 업데이트한다.
		if(this.config.badgeId) {
			var badge = document.getElementById(this.config.badgeId);
			if(badge) {
				badge.innerText = this.data.order.length;
			}
		}
	},
	// 그리드 값에 대한 가공이 필요할 시 이곳에서 값 가공
	_convertGridDataValue : function(row) {
		if(row === null || typeof(row) !== 'object') {
			return {};
		}
		//var copyrow = JSON.parse(JSON.stringify(row));
		// -> deep copy 사용시 check 값이 참조되지 않아 deep copy를 사용하지 않음.
		var copyrow = row;
		
		// date 타입처리용 변수 생성
		var dateColumns = null;
		var columns = this.config.columns;
		if(columns.length > 0) {
			dateColumns = {};
			for(var i=0; i<columns.length; i++) {
				if(columns[i]["editor"] && columns[i]["editor"] == "date") {
					dateColumns[columns[i]["id"]] = "editor";
				}
			}
		}
		
		for(var rkey in copyrow) {
			//그리드 내부 select box에서 빈 값은 자동으로 Webix에서 id 값을 할당해주는 문제가 있음.
			//-> 해당 id를 빈 값으로 치환하여 다시 map을 만들어 리턴
			if(copyrow[rkey] == __EMPTY_SELECTBOX_ID) {
				copyrow[rkey] = null;
			}
			// date 타입의 컬럼은 'YYYY-MM-DD' 형식으로 리턴해준다.
			if(dateColumns!=null && typeof dateColumns[rkey] != "undefined") {
				var value = copyrow[rkey];
				copyrow[rkey] = webix.i18n.parseFormatStr(value);
			}
		}
		return copyrow;
	},
	//인자값과 일치하는 데이터를 가진 row는 disabled 시키는 함수.
	_setRowDisabledByConditions : function(conditions) {
		var _this = this;
		var thisRows = _this._getRows();
		var gridNode = _this.getNode();
		// 현재 그리드에 있는 checkbox를 모두 불러온다.
		var checkboxNodes = jq(gridNode).find("input[type=checkbox]");
		// deep copy
		var copyConditions = JSON.parse(JSON.stringify(conditions));
		for(var i=0; i<thisRows.length; i++) {
			for(var key in copyConditions) {
				var conditionArray = copyConditions[key];
				for(var j=0; j<conditionArray.length; j++) {
					var value = conditionArray[j];
					// 첫번째 문자가 느낌표(!)이면 해당 value에 해당하지 않는 값들을 처리 (not 조건)
					if(value.charAt(0) == "!") {
						value = value.replace("!", "");
						if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] != value) {
							// 0번 인덱스는 헤더영역이므로 i+1
							var checkboxNode = checkboxNodes[i+1];
							if(checkboxNode != null && typeof checkboxNode != "undefined"){
								thisRows[i]["check_disabled"] = true;
								jq(checkboxNode).prop("disabled", true);
							}
							thisRows[i]["check_disabled"] = true;
							_this.addRowCss(thisRows[i]["id"], "disabled");
							break;
						}
					} else {
						if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] == value) {
							// 0번 인덱스는 헤더영역이므로 i+1
							var checkboxNode = checkboxNodes[i+1];
							if(checkboxNode != null && typeof checkboxNode != "undefined"){
								thisRows[i]["check_disabled"] = true;
								jq(checkboxNode).prop("disabled", true);
							}
							thisRows[i]["check_disabled"] = true;
							_this.addRowCss(thisRows[i]["id"], "disabled");
							break;
						}
					}
				}
			}
		}
	},
	//인자값과 일치하는 데이터를 가진 row의 checkbox는 disabled 시키는 함수.
	_setCheckboxDisabledByConditions : function(conditions) {
		var _this = this;
		var thisRows = _this._getRows();
		var gridNode = _this.getNode();
		// 현재 그리드에 있는 checkbox를 모두 불러온다.
		var checkboxNodes = jq(gridNode).find("input[type=checkbox]");
		// deep copy
		var copyConditions = JSON.parse(JSON.stringify(conditions));
		for(var i=0; i<thisRows.length; i++) {
			for(var key in copyConditions) {
				var conditionArray = copyConditions[key];
				for(var j=0; j<conditionArray.length; j++) {
					var value = conditionArray[j];
					// 첫번째 문자가 느낌표(!)이면 해당 value에 해당하지 않는 값들을 처리 (not 조건)
					if(value.charAt(0) == "!") {
						value = value.replace("!", "");
						if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] != value) {
							// 0번 인덱스는 헤더영역이므로 i+1
							var checkboxNode = checkboxNodes[i+1];
							if(checkboxNode != null && typeof checkboxNode != "undefined"){
								thisRows[i]["check_disabled"] = true;
								jq(checkboxNode).prop("disabled", true);
							}
							thisRows[i]["check_disabled"] = true;
							break;
						}
					} else {
						if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] == value) {
							// 0번 인덱스는 헤더영역이므로 i+1
							var checkboxNode = checkboxNodes[i+1];
							if(checkboxNode != null && typeof checkboxNode != "undefined"){
								thisRows[i]["check_disabled"] = true;
								jq(checkboxNode).prop("disabled", true);
							}
							thisRows[i]["check_disabled"] = true;
							break;
						}
					}
				}
			}
		}
	},
	//인자값과 일치하는 데이터를 가진 row는 '읽음' 상태로 표시해주는 함수.
	_setRowReadByConditions : function(conditions) {
		var thisRows = this._getRows();
		// deep copy
		var copyConditions = JSON.parse(JSON.stringify(conditions));
		for(var i in thisRows) {
			for(var key in copyConditions) {
				var conditionArray = copyConditions[key];
				for(var idx in conditionArray) {
					var value = conditionArray[idx];
					if(value.charAt(0) == "!") {
						value = value.replace("!", "");
						if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] != value) {
							this.addRowCss(thisRows[i]["id"], "read");
							break;
						}
					} else {
						if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] == value) {
							this.addRowCss(thisRows[i]["id"], "read");
							break;
						}
					}
				}
			}
		}
	},
	//조건에따라 아이콘 표시 컨트롤을 해주는 함수
	_setRowIconByConditions : function(iconProps) {
		var thisRows = this._getRows();
		for(var i in thisRows) {
			for(var j in iconProps) {
				var iconProp = iconProps[j];
				var iconColumn = iconProp.column;
				var iconCondition = JSON.parse(JSON.stringify(iconProp.conditions));
				var icons = iconProp.icon;
				for(var key in iconCondition) {
					var conditionArray = iconCondition[key];
					for(var idx in conditionArray) {
						var value = conditionArray[idx];
						if(value.charAt(0) == "!") {
							value = value.replace("!", "");
							if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] != value) {
								thisRows[i][iconColumn] = icons.correctIcon;
								this.updateItem(thisRows[i].id, thisRows[i]);
								break;
							} else {
								if(icons.incorrectIcon) {
									thisRows[i][iconColumn] = icons.incorrectIcon;
									if(typeof thisRows[i].id != "undefined") {
										this.updateItem(thisRows[i].id, thisRows[i]);
										break;
									}
								}
							}
						} else {
							if(thisRows[i][key] != null && typeof thisRows[i][key] != "undefined" && thisRows[i][key] == value) {
								thisRows[i][iconColumn] = icons.correctIcon;
								this.updateItem(thisRows[i].id, thisRows[i]);
								break;
							} else {
								if(icons.incorrectIcon) {
									thisRows[i][iconColumn] = icons.incorrectIcon;
									if(typeof thisRows[i].id != "undefined") {
										this.updateItem(thisRows[i].id, thisRows[i]);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	},
	//선택되지 않은 row의 데이터를 조회한다.
	_getUnSelectedRows : function() {
		var rowList = this._getRows();
		var selectedRowList = this._getSelectedRows();
		
		// deep copy, 먼저 전체 Row를 담아준다
		var unSelectedRowList = JSON.parse(JSON.stringify(rowList));
		
		for(var i in unSelectedRowList) {
			for(var j in selectedRowList) {
				if(unSelectedRowList[i].id == selectedRowList[j].id) {
					// 동일한 key가 있으면 해당 배열에서 제거
					unSelectedRowList.splice(i, 1);
				}
			}
		}
		return unSelectedRowList;
	},
	//Cell Merge 처리
	_setCellMerges : function(mergeKeys) {
		var rowList = this._getRows();
		var mergeList = new Array();
		
		for(var i in mergeKeys) {
			var mergeRange = 1;
			var baseId = "";
			var baseText = "";
			
			// 이전 값과 비교를 위해 두번째 인덱스 부터 loop 시작
			for(var j=1; j<rowList.length; j++) {
				
				// merge가 시작 될 row의 데이터 세팅
				if(mergeRange == 1) {
					baseId = rowList[j-1].id;
					baseText = rowList[j-1][mergeKeys[i]];
					if(baseText == null) {
						baseText = "";
					}
				}
				
				// 같은 데이터 중에서 마지막 row인지 체크할 변수
				var isLastSameDataRow = true;
				// 비교 텍스트 변수
				var preRowText = "";
				var curRowText = "";
				
				for(var k=0; k<=i; k++) {
					preRowText += rowList[j-1][mergeKeys[k]];
					curRowText += rowList[j][mergeKeys[k]];
					if(k==i) {
						if(preRowText == curRowText) {
							mergeRange++;
							isLastSameDataRow = false;
							// 마지막 검사 일때는 마지막 row여부 값 true로 변경
							if(j == (rowList.length-1)) {
								isLastSameDataRow = true;
							}
						} else {
							isLastSameDataRow = true;
						}
						preRowText = "";
						curRowText = "";
					}
				}
				
				if(isLastSameDataRow) {
					if(mergeRange > 1) {
						
						var merge = new Array();
						merge.push(baseId);
						merge.push(mergeKeys[i]);
						merge.push(1);
						merge.push(mergeRange);
						merge.push(baseText);
						merge.push("cell_merge_v_align"); // css
						mergeList.push(merge);
						
						mergeRange = 1;
						baseId = "";
						baseText = "";
					}
				}
			}
		}
		this.addSpan(mergeList);
		this.refresh();
	},
	//Row Merge 처리
	_setRowMerges : function(mergePropList) {
		var rowList = this._getRows();
		var mergeList = new Array();
		
		for(var i=0; i<mergePropList.length; i++) {
			
			for(var j=0; j<rowList.length; j++) {
				
				var column = mergePropList[i].column;
				var compareVal = mergePropList[i].compare_value;
				var viewText = mergePropList[i].view_text;
				if(!viewText) {
					viewText = compareVal;
				}
				var mergeRange = mergePropList[i].merge_range;
				var css = mergePropList[i].css;
				if(!viewText) {
					css = null;
				}
				
				if(rowList[j][column] == mergePropList[i].compare_value) {
					
					if(rowList[j].id != rowList[j]["MERGED_ID"]) {
						var merge = new Array();
						merge.push(rowList[j].id);
						merge.push(column);
						merge.push(mergeRange);
						merge.push(1);
						merge.push(viewText);
						merge.push(css);
						
						mergeList.push(merge);
						
						rowList[j]["MERGED_ID"] = rowList[j].id;
					}
				}
			}
		}
		this.addSpan(mergeList);
		this.refresh();
	},
	//그리드 헤더머지 정보를 받아 컬럼 정보를 변경한다.
	_updateHeaderMergeFromResource : function(headerMergeProp) {
		if(headerMergeProp && headerMergeProp.length > 0) {
			this.config.headerMerges = headerMergeProp;
			this._updateHeaderFromResource(this.config.resource);
		}
	},
	//에디터 그리드의 값 입력 완료여부 체크
	_checkCellEditorState : function() {
		
		var checkVaild = true;
		var editor = this.getEditState();
		
		// 에디터가 있을 경우 직전 값과 현재 입력값이 다르면 edit mode 이므로 edit mode를 종료시킴
		if(editor) {
			var editorXtype = editor.config.xtype;
			var beforeEditorValue = editor.value;
			var afterEditorValue = editor.getValue();
			
			if(beforeEditorValue != afterEditorValue) {
				checkVaild = false;
			} else {
				checkVaild = true;
			}
		}
		
		this.editCancel();
		return checkVaild;
	},
	//타이틀 변경
	_setTitleText: function(text){
		var _this = this;
		var headerTitleSpan = document.getElementById(_this.config.id + "_header_title");
		if(typeof headerTitleSpan != "undefined") {
			headerTitleSpan.innerText = text;
		}
	},
	// *GRID 타이틀 가져오기*
	_getTitleText: function(){
		var _this = this;
		var headerTitleSpan = document.getElementById(_this.config.id + "_header_title");
		var text = "";
		if(typeof headerTitleSpan != "undefined") {
			text = headerTitleSpan.innerText;
			
			if(text.indexOf("[") != -1){
				text = text.substring(text, text.indexOf("[") - 3);
			}
		}
		return text;
	},	
	//그리드 숨기기
	_hide: function(){
		var _this = this;
		_this.config.editorMode = false;
		
		var gridNode = _this.getNode();
		var prevView = gridNode.previousSibling;
		var nextView = gridNode.nextSibling;
		
		// 그리드 헤더 숨김
		if(prevView!=null && prevView.className.indexOf("_toolbar")>-1) {
			prevView.style.display = "none";
		}
		
		// 그리드 페이징 숨김
		if(nextView!=null && nextView.className.indexOf("_pager")>-1) {
			nextView.style.display = "none";
		}
		
		// 그리드 숨김
		_this.hide();
	},
	//그리드 나오게
	_show: function(){
		this.show();
		var _this = this;
		_this.config.editorMode = true;
		
		var gridNode = _this.getNode();
		var prevView = gridNode.previousSibling;
		var nextView = gridNode.nextSibling;
		
		// 그리드 헤더 숨김
		if(prevView!=null && prevView.className.indexOf("_toolbar")>-1) {
			prevView.style.display = "";
		}
		
		// 그리드 페이징 숨김
		if(nextView!=null && nextView.className.indexOf("_pager")>-1) {
			nextView.style.display = "";
		}		
	},
	//그리드 필드숨기기
	_hideGridField: function(formName){
		var _this = this;
		_this.config.editorMode = false;
		
		var form = $$(formName);
		var formView = form.$view;
		var formHeight = form.$height;
		//var formChildNodes = formView.childNodes[0];
		var gridHeight = _this.$height;
		
		var gridNode = _this.getNode();
		var prevView = gridNode.previousSibling;
		var nextView = gridNode.nextSibling;
		
		var hideArea = gridNode.parentNode.parentNode;
		
		if(hideArea!=null) {
			hideArea.style.display = "none";
		}
		
		/*// 그리드 헤더 숨김
		if(prevView!=null) {
			prevView.style.display = "none";
		}
		
		// 그리드 페이징 숨김
		if(nextView!=null) {
			nextView.style.display = "none";
		}*/
		
		//form 높이 수정 (폼높이 - 그리드높이 - 그리드헤더높이(40))
		//해당화면일시 -9 추가, 히스토리일시 -9 제거
		formHeight = formHeight-gridHeight-40-9;
		form.define("height", formHeight);
		form.resize();	
		
		// 그리드 숨김
		_this.hide();
	},
	//그리드필드 나오게
	_showGridField: function(formName){
		this.show();
		var _this = this;
		_this.config.editorMode = true;
		
		var form = $$(formName);
		var formView = form.$view;
		var formHeight = form.$height;
		var gridHeight = _this.$height;
		
		var gridNode = _this.getNode();
		var prevView = gridNode.previousSibling;
		var nextView = gridNode.nextSibling;
		

		var hideArea = gridNode.parentNode.parentNode;
		
		if(hideArea!=null) {
			hideArea.style.display = "";
			//hideArea.style.height = "191px";
		}
		
		formHeight = formHeight + gridHeight + 40 + 9;
		form.define("height", formHeight);
		form.resize();	
		
	/*	// 그리드 헤더 나오게
		if(prevView!=null) {
			prevView.style.display = "";
		}
		
		// 그리드 페이징 나오게
		if(nextView!=null) {
			nextView.style.display = "";
		}*/
		
	}
});

/**
 * @method {} createGridComp : 그리드 컴포넌트를 위한 프라퍼티를 생성하는 함수
 * @param {json} gridProps : Grid Properties
 * @property {string} id			: Grid Id
 * @property {Object} params 		: Request Parameter
 * @property {string} url			: Grid Data URL
 * @property {string} height 		: Grid Height
 * @property {string} pageable		: 페이저 설정 여부
 * @property {string} resource 		: 그리드 컬럼 설정 정보
 * @property {Object} on			: 이벤트 핸들러
 *
 * @returns {object} Grid
 */
function createGridComp(gridProps) {
	var props = gridProps||{};
	var grid = {};
	var modules = [];
	
	// props.header.headerSubDesc : /*GRID 헤더 설명문구*/ // SAMPLE : <span style='font-size:9pt; font-weight:bold;color:#888888;'>목록을 더블클릭 하시면, 상세정보를 확인 할 수 있습니다.</span>

	try {
		// Header 영역
		if(props.header) {
			if(props.header.template){
				var templateMap = {
					view: "template", 
					borderless:true, 
					css: "webix_sidebar_popup_title",
					template: props.header.template, 
					height: props.header.height
				};
				modules.push(templateMap);
			}else{
				var totalCountSpanId = props.id + __TOTAL_COUNT_SPAN_SUFFIX;
				var checkCountSpanId = props.id + __CHECK_COUNT_SPAN_SUFFIX;
				var titleFullText = "<span id='" + props.id + "_header_title" + "'>" + props.header.title + "</span>";
				
				var titleCountType = "";
				if(props.checkbox===true) {
					titleCountType += "check";
				} else {
					titleCountType += "normal";
				}
				if(props.minimizeCount===true) {
					titleCountType += "-mini";
				}
				//그리드가 필수일 경우
				if(props.required===true) {
					titleCountType += "-required";
				}
				// 카운터 헤더유형에 따라 헤더 Text를 생성한다.
				// titleFullText += getGridTitleCountText(titleCountType, totalCountSpanId, checkCountSpanId);
				
				// 카운터 헤더유형에 따라 헤더 Text를 생성한다.
				// molee 현황마법사의 header에서 전체 건수가 출력되지 않도록 설정
				if (props.header.headerNoCnt == true) {
					titleFullText;
				} else {
					titleFullText += getGridTitleCountText(titleCountType, totalCountSpanId, checkCountSpanId);
				}
				
				if(props.helper) {
					var helperWidth = props.helperWidth ? props.helperWidth : 200;
					var helperHeight = props.helperHeight ? props.helperHeight : 100;
					titleFullText += 
						__TOOLTIP_SYMBOL_PREFIX 
						+ props.helper 
						+ __TOOLTIP_SYMBOL_MIDDLE
						+ helperWidth + "," + helperHeight
						+ __TOOLTIP_SYMBOL_SUFFIX;
				}
				
				// *GRID 헤더 설명문구*
				if (props.header.headerSubDesc != null && props.header.headerSubDesc.trim() != "" ) {
					titleFullText += "&nbsp;&nbsp;"
						+props.header.headerSubDesc;
				}				
				
				props.header.icon = "grid";
				props.header.title = titleFullText;
				props.header.id = props.id+"_header";
				modules.push(new nkia.ui.common.Toolbar(props.header));
			}
		}
		
		grid = nkia.ui.grid.body.getDataTableBody(props);
		
		modules.push(grid);
		
		// autoLoad가 false 이고, url이 없다면 메모리 그리드 사용을 위해 페이징을 없앤다.
		if(props.autoLoad===false && (props.url===undefined || props.url==="")) {
			props.pageable = false;
		}
		
		if(props.pageable) {
			var pagerId = grid.id + __PAGER_SUFFIX;
			grid.pager = pagerId;
			modules.push(new nkia.ui.grid.GridPager(pagerId, props));
		}

		// Footer 영역
		if(props.footer) {
			modules.push(new nkia.ui.common.Toolbar(props.footer));
		}
	} catch(e) {
		alert(e.message);
	}
	return modules;
}

/**
 * @method {} getGridTitleCountText : 그리드 타이틀의 헤더를 가져온다
 * @param {string} headerType 		: Grid 헤더 카운트 표시유형
 * @param {string} totalCountSpanId : 전체 데이터 카운트 표시 span의 id
 * @param {string} checkCountSpanId : 선택 데이터 카운트 표시 span의 id
 *
 * @returns {string} Grid Header String
 */
function getGridTitleCountText(headerType, totalCountSpanId, checkCountSpanId) {
	
	var result = "";
	
	var LEFT_SQUARE_BRACKET = "[ ";
	var RIGHT_SQUARE_BRACKET = " ]";
	var TOTAL_TEXT = __GRID_COMMON_MSG.TOTAL_DATA_TEXT + " ";
	var SELECT_TEXT = __GRID_COMMON_MSG.SELECTED_DATA_TEXT + " ";
	
	switch(headerType) {
	
		case 'normal' :
			
			result = (
				"&nbsp;&nbsp;&nbsp;"
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0082e2'>"
				+ 		TOTAL_TEXT
				+ 		"<span id='" + totalCountSpanId + "'>0</span>" + " " + __GRID_COMMON_MSG.ROW_UNIT_TEXT
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
			);
			break;
			
		case 'normal-mini' :
			
			result = (
				"&nbsp;"
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0082e2'>"
				+ 		"<span id='" + totalCountSpanId + "'>0</span>"
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
			);
			break;
			
		case 'normal-required' :
			
			result = (
				"&nbsp;&nbsp;&nbsp;"
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0082e2'>"
				+ 		TOTAL_TEXT
				+ 		"<span id='" + totalCountSpanId + "'>0</span>" + " " + __GRID_COMMON_MSG.ROW_UNIT_TEXT
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
				+    "<font color='red' style='padding-left:4px'>*</font>"
			);
			break;	
			
		case 'check' :
			
			result = (
				"&nbsp;&nbsp;&nbsp;"
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0077ee'>"
				+ 		SELECT_TEXT
				+ 		"<span id='" + checkCountSpanId + "'>0</span>" + " " + __GRID_COMMON_MSG.ROW_UNIT_TEXT
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
				+ 	" / "
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0082e2'>"
				+ 		TOTAL_TEXT
				+ 		"<span id='" + totalCountSpanId + "'>0</span>" + " " + __GRID_COMMON_MSG.ROW_UNIT_TEXT
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
			);
			break;
			
		case 'check-mini' :
			
			result = (
				"&nbsp;"
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0077ee'>"
				+ 		"<span id='" + checkCountSpanId + "'>0</span>"
				+ 	"</font>"
				+ 	" / "
				+ 	"<font color='#0082e2'>"
				+ 		"<span id='" + totalCountSpanId + "'>0</span>" + " " + __GRID_COMMON_MSG.ROW_UNIT_TEXT
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
			);
			break;
			
		case 'check-required' :
			
			result = (
				"&nbsp;&nbsp;&nbsp;"
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0077ee'>"
				+ 		SELECT_TEXT
				+ 		"<span id='" + checkCountSpanId + "'>0</span>" + " " + __GRID_COMMON_MSG.ROW_UNIT_TEXT
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
				+ 	" / "
				+ LEFT_SQUARE_BRACKET
				+ 	"<font color='#0082e2'>"
				+ 		TOTAL_TEXT
				+ 		"<span id='" + totalCountSpanId + "'>0</span>" + " " + __GRID_COMMON_MSG.ROW_UNIT_TEXT
				+ 	"</font>"
				+ RIGHT_SQUARE_BRACKET
				+    "<font color='red' style='padding-left:4px'>*</font>"
			);
			break;	
		
	}
	
	return result;
}



