/********************************************
 * nkia.ui.Field.js
 * Date: 2021-03-22
 * Version : WEBIX_NKIA_UI_RELEASE_INFO 참고 
 *  - 설치 버전 : v20210301
 *  - 현재 버전 : v20210409   
 ********************************************/

/********************************************
 * Header
 * Date: 2016-11-17
 * Version: 1.0 - template header
 ********************************************/
nkia.ui.common.Header = function(props) {
    this.view = "template";
    this.id = props.id + "_header";
    this.type = "header";
    this.template = props.title;
    this.css = props.css;
};

/********************************************
 * Toolbar
 * Date: 2016-11-17
 * Version: 1.0 - header, button 지원
 ********************************************/
nkia.ui.common.Toolbar = function(props) {
    this.view = "toolbar";
	this.id = props.id || "_header";
	var _SPAN_ID_SUFFIX = "_title";
    var elements = [];

    if(props.title){
    	var title = "<span id='" + props.id + _SPAN_ID_SUFFIX + "'>" + props.title + "</span>";
    	switch (props.icon) {
			case "form":
				title = "<img src='/itg/base/images/ext-js/common/icons/bullet_detail.gif' align='absmiddle'> "+title+"</img>";
				break;
			case "search":
				title = "<img src='/itg/base/images/ext-js/common/icons/bullet_search.gif' align='absmiddle'> "+title+"</img>";
				break;
			case "grid":
				title = "<img src='/itg/base/images/ext-js/common/icons/bullet_list.gif' align='absmiddle'> "+title+"</img>";
				break;
			case "tree":
				title = "<img src='/itg/base/images/ext-js/common/icons/bullet_tree.gif' align='absmiddle'> "+title+"</img>";
				break;
			case "chart":
				title = "<img src='/itg/base/images/ext-js/common/icons/bullet_chart.gif' align='absmiddle'> "+title+"</img>";
				break;
			default:
		}
    	
        var headerProps = {
            view:"label",
            type:"header",
            label: title || "",
            data: props.data
        };
        elements.push(headerProps);
    }

    if(props.buttons){
        var buttons = props.buttons;
        //elements.push({});
        buttons.items.forEach(function(button){
            elements.push(button);
        });
        if(buttons.align && buttons.align == "center"){
            elements.push({});
        }
    }
    this.elements = elements;
};

/********************************************
 * Button
 * Date: 2016-11-17
 * Version: 1.0 - button 생성과 event binding
 ********************************************/
nkia.ui.common.Button = function(props) {
    this.view = "button";
    this.id = props.id || "";
    this.type = props.type || "";
    this.exclude = props.exclude || false;
    this.selectType = props.selectType || "single";
    
    var label = ""; 
    if(props.label){
    	label = props.label.replace('&nbsp;',' ');
    	label = replaceAll(label, '&nbsp',' ');
    }
    this.hotkey = props.hotkey || "";
    
    switch (this.type) {
		case "excel":
			this.type = "image";
			this.image = "/itg/base/images/ext-js/common/icons/excel-icon.png";
			break;
		default:
	}
	
	if(label.length > 6 && typeof props.width === "undefined"){
		props.width = label.length * 18; 
	}
    
    this.label = label;
    this.icon = props.icon || "";
    this.width = props.width || 90;
	this.hidden = ( props.visible !== undefined ) ? !props.visible : false;
	this.disabled = ( props.readonly !== undefined ) ? props.readonly : false;

    if(props.click){
        this.click = props.click;
    }
    if(props.on){
        this.on = props.on;
    }
};

/**
 * @method {} createBtnComp : Button Function
 * @property {string} id	: Button Id
 * @property {string} label : Button label
 */
function createBtnComp(buttonProps) {
	var props = buttonProps||{};
    var button = {};
	try {
		// Body 영역
		button = new nkia.ui.common.Button(props);
	} catch(e) {
		alert(e.message);
	}
	return button;
}

function createButtons(buttonsProps){
    var props = buttonsProps||{};
    var buttons = [];
	try {
        buttons.push({});
        props.items.forEach(function(button){
            buttons.push(button);
        });
        if(props.align && props.align == "center"){
            buttons.push({});
        }
	} catch(e) {      
		alert(e.message);
	}
	return { cols: buttons, padding: 5, height: 40, css: props.css || "webix_bottom_button" };
}

/**
 * 
 */
nkia.ui.common.Window = function() {
	// Webix Properties
	this.view = "window";
	this.id = "";
	this.position = "center";
	this.height = 800;
	this.width = 500;
	this.modal = true;
	this.head = {};
	this.body = {};
	this.css = "";
	
	// Custom Properties
	this.header = {};
	this.footer = {};
	this.closable = true;
	
	
	this.widthByWindowSizePercent = 0; // *팝업창 크기 최대치 자동 설정* -- 단위 % , null or 0~10 : 기능 사용안함 , 11~100 : 비율
	this.heightByWindowSizePercent = 0; // *팝업창 크기 최대치 자동 설정* -- 단위 % , null or 0~10 : 기능 사용안함 , 11~100 : 비율
	
	
	this.propsValidate = function(props) {
		nkia.ui.validator.window(props);
	};
	
	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		
		if(this.body){
			this.body.padding = 1;
		}
		
		// *팝업창 크기 최대치 자동 설정*
		if ( this.widthByWindowSizePercent == null || isNaN(Number(this.widthByWindowSizePercent)) == true || Number(this.widthByWindowSizePercent) <= 10 ) {
			this.widthByWindowSizePercent = 0;
		} else {
			this.width = Math.ceil( nkia.ui.utils.getWidePopupWidth() * ( this.widthByWindowSizePercent / 100 ) ) ;
		}
		if ( this.heightByWindowSizePercent == null || isNaN(Number(this.heightByWindowSizePercent)) == true || Number(this.heightByWindowSizePercent) <= 10 ) {
			this.heightByWindowSizePercent = 0;
		} else {
			this.height = Math.ceil( nkia.ui.utils.getWidePopupHeight() * ( this.heightByWindowSizePercent / 100 ) ) ;
		}
			
	};
	
	// 닫기 버튼
	this.closeButton = function(type){
		var _this = this;
		
		var closeButton = "";
		switch (type) {
			case "icon":
				closeButton = createIconComp({icon: "close", hotkey: "esc", align: 'right', click: this.close.bind(_this)});
				break;
			default:
				closeButton = createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.close'}), hotkey: "esc", align: 'right', click: this.close.bind(_this)});
		}
		return closeButton;
	}
	
	// Header 정보
	this.setHeader = function() {
		if(this.closable){
			if(this.header.buttons && this.header.buttons.items){
				this.header.buttons.items.push(
					this.closeButton("icon")
				)
			}else{
				this.header.buttons = {
					items: [
						this.closeButton("icon")
					]
				}
			}
		}
		this.head = new nkia.ui.common.Toolbar(this.header);
	};
	
	// Footer 정보
	this.setFooter = function() {
		if(this.closable){
			if(this.footer.buttons && this.footer.buttons.items){
				this.footer.buttons.items.push(
					this.closeButton()
				)
			}else{
				this.footer.buttons = {
					align: "center",
					items: [
					    this.closeButton()
					]
				}
			}
		}
		
		if(this.footer && this.footer.buttons && this.body.rows){
			this.footer.buttons.css = "webix_layout_bottom"
			this.body.rows.push(createButtons(this.footer.buttons));
		}
		
	};
	
	// Render
	this.setRender = function() {
		webix.ui(this).hide();
	};
	
	// Close Event
	this.close = function(){
		var _this = this;
		if($$("app")) {
			nkia.ui.utils.hideProgress({id: 'app', isMask: true});
		}
		setTimeout(function(){ $$(_this.id).close(); }, 200);
	};
};

//데이터테이블 프라퍼티 검증함수
nkia.ui.validator.window = function(props) {
	if(props.id === undefined){
		webix.message({
			type:"error",
			text: "id가 없습니다.",
			expire:-1
		});
		return;
	}
};

function createWindow(windowProps){
	var props = windowProps||{};
	var window;
	try {
		// Body 영역
		window = new nkia.ui.common.Window();
		window.setProps(props);
		
		if(props.header){
			window.setHeader();
		}
		window.setFooter();

		window.setRender();
	} catch(e) {
		alert(e.message);
	}
	return window;
}

/********************************************
 * Icon
 * Date: 2016-11-17
 * Version: 1.0 - icon 생성과 event binding
 ********************************************/
nkia.ui.common.Icon = function(props) {
    this.view = "icon";
    this.id = props.id || "";
    this.icon = props.icon || "";
	this.hidden = ( props.visible !== undefined ) ? !props.visible : false;
	this.disabled = ( props.readonly !== undefined ) ? props.readonly : false;

    if(props.click){
        this.click = props.click;
    }
    if(props.on){
        this.on = props.on;
    }
};

/**
 * @method {} createIconComp : Icon Function
 * @property {string} id	: Icon Id
 * @property {string} icon : Icon Type
 */
function createIconComp(iconProps) {
	var props = iconProps||{};
    var icon = {};
	try {
		// Body 영역
		icon = new nkia.ui.common.Icon(props);
	} catch(e) {
		alert(e.message);
	}
	return icon;
}