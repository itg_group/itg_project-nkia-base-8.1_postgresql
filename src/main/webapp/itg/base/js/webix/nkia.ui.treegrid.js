//Tree에서 사용하기 위한 데이터드라이버 정의
var __TREE_GRID_URL_PREFIX = "treegridproxy->";
var __TREE_GRID_CHECK_BOX_SUFFIX = "_check";
var __TREE_GRID_ALL_CHECK_BOX_SUFFIX = "_allCheck";
var __TREE_GRID_CHECK_HEADER_SUFFIX = "_CHECK_HEADER";
var __TREE_GRID_CHECK_BOX_HEADER_ID = "masterCheckbox";
var __TREE_GRID_EMPTY_SELECTBOX_ID = "__NO_DATA_IN_GRID_COMBOBOX__";

//Tree 그리드의 네임스페이스를 정의
nkia.ui.treegrid = {};
nkia.ui.treegrid.column = {};
nkia.ui.treegrid.column.handler = {};
nkia.ui.treegrid.body = {};

webix.proxy.treegridproxy = webix.clone(webix.proxy.baseproxy);
webix.expansion(webix.proxy.treegridproxy, {
	load : function(view, callback) {
		var params = view.config.params || {};
		var url = this.source;
		//autoLoad가 true		: 해당 url로 ajax 바인딩
		//autoLoad가 false	: dummy request를 보낸 후, autoLoad의 값을 true로 변경하여 최초에는 데이터가 로드 되지않고, 
		//					. 향후 reload시 ajax 바인딩을 통해 데이터 호출을 할 수 있도록 만들어준다.
		if(view.config.autoLoad && ""!=url) {
			webix.ajax().bind(view).post(url, JSON.stringify(params), callback);
		} else {
			webix.ajax().bind(view).post(__GRID_EMPTY_DATA_URL, JSON.stringify(params), callback);
			view.config.autoLoad = true;
		}
	}
});

//Tree에서 사용하기 위한 데이터드라이버 정의
webix.DataDriver.treegridjson = webix.clone(webix.DataDriver.customjson);
webix.expansion(webix.DataDriver.treegridjson, {
	dataPosition : 0,
	getRecords : function(data) {
		if (data && data.treeVO){
			if(data.treeVO.children != null) {
				// id가 0인거는 root로 change
				if(data.treeVO.children[0].id == "0"){
					data.treeVO.children[0].id = "root";
				}
				data = data.treeVO.children;
			}
		}
		return data;
	},
	child: function(data) {
		return data.children;
	}
});

//체크박스 그리드 요청 시 체크박스 셀을 만들어주는 함수
nkia.ui.treegrid.column.getCheckBoxColumn = function(gridId) {
	return {
		id : gridId + __TREE_GRID_CHECK_HEADER_SUFFIX,
		width : 50,	
		checkValue : true,
		uncheckValue : false,
		template : "{common.checkbox()}",
		css:{'text-align': 'center'},
		header : { 
			content : "masterCheckbox",
			contentId : __TREE_GRID_CHECK_BOX_HEADER_ID
		}
	};
};

//리소스를 통해 그리드에서 사용되는 컬럼을 생성한다.
nkia.ui.treegrid.column.createColumns = function(grid, gridResources) {
	var gridTexts = [];
	var gridHeaders = [];
	var gridWidths = [];
	var flexs = [];
	var sortable = [];
	var newColumns = [];
	webix.ajax().sync().post(
		nkia.host + "/itg/base/getGridMsgSource.do",
		{ resource_prefix : gridResources },
		function (data){
			var headerData = JSON.parse(data);
			if(headerData.gridText !== null) {
				newColumns = nkia.ui.treegrid.column.createColumnsForObject(grid, headerData);
			}
		}
	);
	return newColumns;
};

//객체를 통해 그리드에서 사용되는 컬럼을 생성한다.
nkia.ui.treegrid.column.createColumnsForObject = function(grid, columns){
	var gridTexts = columns.gridText.trim().split(",");
	var gridHeaders = columns.gridHeader.trim().split(",");
	var gridWidths = columns.gridWidth.trim().split(",");
	var flexs = columns.gridFlex.trim().split(",");
	var align =  columns.gridAlign.trim().split(",");
	var xtype = columns.gridXtype.trim().split(",");
	
	var length = gridTexts.length;
	var newColumns = [];
	
	for(var i = 0 ; i < length ; i++) {
		var column = {};
		if("hidden" === xtype[i]) {
			continue;
		} else if("tree" === xtype[i]) {
			var thatColumn = gridHeaders[i];
			var treeTemplate = function(data, common) {
				return common.treetable(data, common) + data[thatColumn];
			}
			if(grid.checkbox === true) {
				treeTemplate = "{common.treetable()} {common.treecheckbox()} <span>#text#</span>";
			}
			column = {
				id:gridHeaders[i],
				header: {text : gridTexts[i], css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i] },
				template: treeTemplate
			};
		} else if("checkbox" === xtype[i]) {
			column = {
				id:gridHeaders[i],
				header: {text : gridTexts[i], css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i] },
				template : "{common.checkbox()}"
			};
		} else {
			column = {
				id:gridHeaders[i],
				header: {text : gridTexts[i], css : {'text-align': 'center'}} ,
				width:(parseInt(gridWidths[i],10)),
				minWidth:(parseInt(gridWidths[i],10)),
				fillspace: (flexs[i] === "1") ? true : false,
				css:{'text-align': align[i] }
			};
		}
		newColumns.push(column);
	}
	
	// 추가되는 Columns 처리
	if(grid.appendColumns.length > 0){
		grid.appendColumns.forEach(function(column){
			column.header = {text : column.text, css : {'text-align': 'center'}};
			// 이벤트 바인드 처리
			if(grid.onClick){
				for(var key in column.onClick){
					grid.onClick[key] = column.onClick[key];	
				}
			}else{
				grid.onClick = column.onClick;
			}
			newColumns.push(column);
		});
	}
	
	// Header Merge 정보가 있을 시, Header 컬럼 Merge 수행
	if(grid.headerMerges.length > 0) {
		
		var mergeList = grid.headerMerges;
		
		for(var m=0; m<mergeList.length; m++) {
			
			var mergePropList = mergeList[m];
			
			for(var i=0; i<mergePropList.length; i++) {
				var mergeRange = 0;
				var mergeCount = 0;
				for(var j=0; j<newColumns.length; j++) {
					var isFirst = false;
					if(mergePropList[i].column == newColumns[j].id) {
						mergeRange = mergePropList[i].merge_range;
						mergeCount = 1;
						isFirst = true;
						var newHeader = new Array();
						newHeader.push( {text: mergePropList[i].view_text, colspan: mergeRange, css: "webix_hcell_bg"} );
						newColumns[j].header = new Array().concat(newHeader, newColumns[j].header);
						mergeCount++;
					}
					if(!isFirst && mergeCount>1) {
						if(mergeCount > mergeRange) {
							var mergeRange = 1;
							var mergeCount = 1;
						} else {
							newColumns[j].header = new Array().concat("", newColumns[j].header);
							mergeCount++;
						}
					}
				}
			}
		}
	}
	
	return newColumns;
};

/********************************************
 * Tree Grid
 * Date: 2017-03-04
 * Auth: 정정윤 jyjeong@nkia.co.kr
 * Version:
 *	1.0 - 최초 개발
 ********************************************/
nkia.ui.treegrid.Grid = function() {
	// Webix Properties
	this.view = "treetable";
	this.id = "";
	this.select = true;
	this.template = "{common.icon()} {common.folder()} <span>#text#<span>";
	this.url = __TREE_GRID_URL_PREFIX;
	this.datatype = "treegridjson";
	this.filterMode = {
		showSubItems:false
	};
	this.checkBoxShow = false;
	this.on = {};

	// Custom Properties
	this.params = {};
	this.expandLevel = 0;
	this.autoLoad = true;
	this.checkbox = false;
	this.checkgrid = false;
	this.resource = "";
	this.columns = [];
	this.appendColumns = [];
	this.headerMerges = [];
	this.threeState = false;
	this.rowHeight = 23;
	this.rowLineHeight = 0;
	this.headerRowHeight = 23;
	var attachEvent = {};
	
	this.ready = function() {
		//기존에 정의된 이벤트들이 있어 on으로 이벤트를 덮어쓸경우 문제가 발생함. 화면 렌더링 이후 attachEvent를 통해 추가하도록 해야함 
		for(var eventName in attachEvent) {
			this.attachEvent(eventName, attachEvent[eventName]);
		}
	};

	this.propsValidate = function(props) {
		nkia.ui.validator.treegrid(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				if(i === "url") {
					this[i] = this.url + nkia.host + props[i];
				} else if(i === "on") {
					attachEvent = props[i];
				} else {
					this[i] = props[i];
				}
			}
		}

		if(props.width){
			this.width = props.width;
		}

		if(props.height){
			this.height = props.height;
		}
		
		// checkbox와 checkgrid는 같이 사용 불가능
		if(props.checkbox){
			this.threeState = true;
			this.checkgrid = false;
			this.select = (typeof props.select == "undefined")? !this.checkbox : props.select;
		}
		
		if(props.checkgrid){
			this.checkbox = false;
			this.select = (typeof props.select == "undefined")? !this.checkgrid : props.select;
		}

		if(props.expandLevel){
			this.params.expandLevel = props.expandLevel;
		}
		
	};
	
	this.loadedExpandLevel = function(tree, firstId, visibleCount, expandLevel) {
		var firstItem = tree.getItem(firstId);
		// 첫번쨰 item 펼치기
		if(firstItem && firstItem.$level < expandLevel){
			tree.open(firstId, false);

			if(firstItem.$level < expandLevel){
				// Open a child
				tree.config.loadedExpandLevel(tree, tree.getFirstChildId(firstId), firstItem.$count, expandLevel);
			}
		}
		// Next Sibling 펼치기
		if(visibleCount > 1){
			var siblingId = firstId;
			for(var i = 1; i < visibleCount; i++){
				var nextSiblingId = tree.getNextSiblingId(siblingId);
				var nextSiblingItem = tree.getItem(nextSiblingId);

				if(nextSiblingItem.$level < expandLevel){
					tree.open(nextSiblingId, false);

					if(nextSiblingItem.$level < expandLevel){
						// Open a child
						tree.config.loadedExpandLevel(tree, tree.getFirstChildId(nextSiblingId), nextSiblingItem.$count, expandLevel);
					}
				}
				siblingId = nextSiblingId;
			}
		}
	};
	
	this.setColumns = function() {
		this.columns = nkia.ui.treegrid.column.createColumns(this, this.resource);
	};

	this.setColumnsForObject = function() {
		this.columns = nkia.ui.treegrid.column.createColumnsForObject(this, this.columns);
	};
	
};

//데이터테이블 프라퍼티 검증함수
nkia.ui.validator.treegrid = function(props) {
	if(props.id === undefined || props.id === ""){
		nkia.ui.utils.notification({
			type: "error",
			message: "Tree ID가 없습니다."
		});
		return;
	}
	if(props.url === undefined || props.url === ""){
		nkia.ui.utils.notification({
			type: "error",
			message: "Tree URL이 없습니다."
		});
		return;
	}
};

// 컴포넌트 확장영역 : 
// treetable이 datatable을 상속해서 구현하기때문에 datatable의 함수를 실행시키는 문제가 있음
// -> treetable의 protoUI를 오버라이딩 해서 구현 시킨다.
webix.protoUI({
    name:"treetable",
    //트리그리드를 로드한다.
    _reload : function(newParams, callback) {
		var _this = this;
		_this.clearAll();

		if(newParams) {
			_this.config.params = nkia.ui.utils.copyObj(newParams);
			if(_this.config.expandLevel){
				_this.config.params.expandLevel = _this.config.expandLevel;
			}
		}
		
		var doStuff;
		if(callback) {
			var originCallback = callback;
			doStuff = function() {
				originCallback.apply(this, arguments);
				_this.refresh();
			};
		} else {
			doStuff = function() {
				_this.refresh();
			};
		}
		
		_this.load(__GRID_URL_PREFIX + _this.config.url.source, doStuff);
	},
	//트리그리드 컬럼 정보를 변경한다.(리소스로 변경)
	_updateHeaderFromResource : function(resource) {
		this.config.columns = nkia.ui.treegrid.column.createColumns(this.config, resource);
		this.refreshColumns();
	},
	//체크된 값을 리턴한다.
	_getCheckedItem : function() {
		var _this = this;
		var resultItem = new Array();
		if(this.config.checkbox === true) {
			resultItem = [];
			var checkedItem = _this.getChecked();
			for(var i=0; i<checkedItem.length; i++) {
				var item = _this.getItem(checkedItem[i]);
				resultItem.push(item);
			}
		} else if(this.config.checkgrid === true) {
			resultItem = [];
			var checkBoxId = _this.config.id + __TREE_GRID_CHECK_HEADER_SUFFIX;
			_this.eachRow(function(row) {
				var item = _this._convertTreeGridDataValue(_this.getItem(row));
				if(item){
					if(typeof item[checkBoxId] != "undefined" && item[checkBoxId] === true ){
						resultItem.push(item);
					}
				}
			});
		}
		return resultItem;
	},
	//id배열을 인자로 받아 체크상태로 만든다.
	_checkItem : function(idArray, isExpand) {
		var _this = this;
		if(this.config.checkbox === true) {
			_this.uncheckAll();
			for(var i=0; i<idArray.length; i++) {
				_this.checkItem(idArray[i]);
				if(isExpand === true) {
					_this._openNode(idArray[i]);
				}
			}
		}else if(this.config.checkgrid === true) {
			for(var i=0; i<idArray.length; i++) {
				_this.addRowCss(idArray[i], "webix_row_select");
				var rowItem = _this.getItem(idArray[i]);
				rowItem[_this.config.id + __TREE_GRID_CHECK_HEADER_SUFFIX] = true; 
			}
		}
	},
	//전체 체크
	_checkAll : function() {
		if(this.config.checkbox == true) {
			this.checkAll();
		}
	},
	//전체 체크 해제
	_uncheckAll : function() {
		if(this.config.checkbox == true) {
			this.uncheckAll();
		}
	},
	//특정 노드 열기
	_openNode : function(id) {
		this.open(id);
		var parentId = this.getParentId(id);
		if(parentId){
			this.open(parentId);
			// 재귀호출
			this._openNode(parentId);
		}
	},
	// 그리드 값에 대한 가공이 필요할 시 이곳에서 값 가공
	_convertTreeGridDataValue : function(row) {
		if(row === null || typeof(row) !== 'object') {
			return {};
		}
		//var copyrow = JSON.parse(JSON.stringify(row));
		// -> deep copy 사용시 check 값이 참조되지 않아 deep copy를 사용하지 않음.
		var copyrow = row;
		
		// date 타입처리용 변수 생성
		var dateColumns = null;
		var columns = this.config.columns;
		if(columns.length > 0) {
			dateColumns = {};
			for(var i=0; i<columns.length; i++) {
				if(columns[i]["editor"] && columns[i]["editor"] == "date") {
					dateColumns[columns[i]["id"]] = "editor";
				}
			}
		}
		
		for(var rkey in copyrow) {
			//그리드 내부 select box에서 빈 값은 자동으로 Webix에서 id 값을 할당해주는 문제가 있음.
			//-> 해당 id를 빈 값으로 치환하여 다시 map을 만들어 리턴
			if(copyrow[rkey] == __TREE_GRID_EMPTY_SELECTBOX_ID) {
				copyrow[rkey] = null;
			}
			// date 타입의 컬럼은 'YYYY-MM-DD' 형식으로 리턴해준다.
			if(dateColumns!=null && typeof dateColumns[rkey] != "undefined") {
				var value = copyrow[rkey];
				copyrow[rkey] = webix.i18n.parseFormatStr(value);
			}
		}
		return copyrow;
	}
}, webix.ui.treetable);

/**
 * @method {} createTreeGridComp : Tree Grid Function
 * @param {json} setTreeProp : Tree Propertis
 * @property {string} id			: Tree Id
 * @property {string} title 		: Tree Title
 * @property {string} url			: Tree Data URL
 * @property {string} height 		: Tree Height
 * @property {string} expandLevel	: Tree 확장 레벨
 * @property {string} expColButton : 트리 전체 펼침/접는 버튼( true | false )
 * @property {string} params		: Request Parameter
 *
 * @returns {object} tree
 */
function createTreeGridComp(treeProp) {
	var props = treeProp||{};
	var modules = [];

	try {
		// Header 영역
		if(props.header) {
			// Expand/Collpase Button 영역
			if(props.expColable){
				if(props.header.buttons) {
					props.header.buttons.items.push(createIconComp({ icon:"plus", click: function() { $$(props.id).openAll(); } }));
					props.header.buttons.items.push(createIconComp({ icon:"minus", click: function() { $$(props.id).closeAll(); } }));
				} else {
					props.header.buttons = {
						items: [
		                    createIconComp({ icon:"plus", click: function() { $$(props.id).openAll(); } }),
		                    createIconComp({ icon:"minus", click: function() { $$(props.id).closeAll(); } })
		                ]
					}
				}
			}
			props.header.icon = "grid";
			var header = new nkia.ui.common.Toolbar(props.header);
			modules.push(header);
		}
		
		// Filter 영역
		if(props.filterable){
			var filter = createTextFieldComp({label: "검색", name: "search_tree_grid"});
			filter.on = {
				onTimedKeyPress: function() {
					$$(props.id).filter("#text#",this.getValue());
				}
			};
			
			modules.push({
				view: "form",
		        padding: 4,
		        elements:[
		            filter
		        ]
			});
		}

		// Body 영역
		var treeGrid = new nkia.ui.treegrid.Grid();
		treeGrid.setProps(props);
		
		//프라퍼티의 컬럼 세팅 정보가 초기 값인 Array가 아닐 경우 세팅되서 넘어오는 값은 Object이다. 
		if(treeGrid.columns.constructor !== Array) {
			treeGrid.setColumnsForObject();
		} else {
			treeGrid.setColumns();
		}
		
		treeGrid.on.onBeforeLoad = function() {
			$$(this.config.id).disable();
		};
		
		treeGrid.on.onAfterLoad = function() {
			// Level에 따른 tree expand
			var visibleCount = this.count();
			var expandLevel = this.config.expandLevel;
			
			if(expandLevel > 0 && visibleCount > 0) {
				this.config.loadedExpandLevel(this, this.getFirstId(), visibleCount, expandLevel);
			}
			
			$$(this.config.id).enable();
		};
		
		if(treeGrid.checkgrid === true) {
			
			var checkboxColumns = nkia.ui.treegrid.column.getCheckBoxColumn(treeGrid.id);
			
			treeGrid.columns.splice(0, 0, checkboxColumns);
			
			// 그리드 컬럼 내에서 checkbox template을 썼을 경우만 동작하므로 따로 분기가 필요없이 이벤트를 바인딩 시켜준다.
			treeGrid.on["onCheck"] = function(rowId, column, state) {
				var gridId = this.config.id;
				var checkBoxId = gridId + __TREE_GRID_CHECK_HEADER_SUFFIX;
				var item = this.getItem(rowId);
				
				if(typeof item["check_disabled"] !== "undefined" && item["check_disabled"] === true ){
					state = false;
					return false;
				}
				
				if(state===true) {
					this.addRowCss(rowId, "webix_row_select");
					item[checkBoxId] = true;
				} else {
					this.removeRowCss(rowId, "webix_row_select");
					item[checkBoxId] = false;
				}
				
				this.updateItem(rowId, item);
			};
		}
		
		modules.push(treeGrid);
	} catch(e) {
		alert(e.message);
	}
	return modules;
}
