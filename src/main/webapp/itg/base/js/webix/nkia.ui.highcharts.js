/********************************************
 * nkia.ui.highcharts.js
 * Date: 2021-03-22
 * Version : WEBIX_NKIA_UI_RELEASE_INFO 참고 
 *  - 설치 버전 : v20210301
 *  - 현재 버전 : v20210325   
 ********************************************/

nkia.ui.highchart = {};


/**
 * 하이차트 생성
 */
nkia.ui.highchart.HighChart = function() {
	this.id = "";
	this.divId = "";
	this.view = "template";
	this.template = "";
	this.chartType = "";
	this.renderChartType = "";
	this.name = "";
	this.content = "";
	this._url = ""; // url은 Webix 내부 속성이므로 언더바를 붙여 사용
	this.params = {};
	this.chartTitle = ""; // 차트 제목
	this.height = 0; // 차트 높이
	this.width = 0; // 차트 길이
	this.yTitle = ""; // Y축에 표시될 제목 (Y축 타이틀이 없는 차트도 있음)
	this.yAxisTitle = "";
	this.valueSuffix = ""; // 값의 단위
	this.dataHeaders = []; // 데이터 헤더명(컬럼명) * Array
	this.dataHeaderNms = []; // 데이터 헤더명(한글) * Array
	this.standardHeader = ""; // 기준헤더명(집합 기준컬럼 / ex:분류체계별도입현황 -> CLASS_NM)
	this.valInterval = 1000 // 값 간격
	this.series = []; // 데이터
	this.settings = {}; // 차트 생성 시 필요한 정보
	this.showInLegend = true;
	this.colorArray = [];
	this.chartPlotOpts = {};
	this.backgroundColor = "";
	this.on = {};
	
	this.isAutoLoad = true;		/*HIGHCHART 생성시 그래프 표시 여부*/
	
	this.propsValidate = function(props) {
		nkia.ui.validator.HighChart(props);
	};
	
	// 프로퍼티 세팅
	this.setProps = function(props) {
		for(var i in props) {
			if (i === "url") {
				this._url = props[i];
			}
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		if(this.id && this.id!="") {
			this.divId = this.id + "_div";
			this.template = "<div id='" + this.divId + "' style='width:100%; height:100%;'></div>";
		}
	};
	
	// 이벤트 연결
	this.attachEvent = function() {
		var _this = this;
		if(!_this.on) {
			_this.on = {};
		}
		_this.on["onAfterRender"] = webix.once(function(){
			if ( _this.isAutoLoad == true ){	_this._reload(); 	} /*HIGHCHART 생성시 그래프 표시 여부*/
		});
	};
	
	// 차트 재 로드
	this._reload = function(newParams) {
		var _this = this;
		if(newParams) {
			_this.params = newParams;
		}
		_this.setSeries();
		_this.setOptions();
		_this.renderChart();
	};
	
	// 차트에 사용할 시리즈(데이터) 세팅
	this.setSeries = function() {
		
		var _this = this;
		var params = _this.params||{};
		
		nkia.ui.utils.ajax({
			url: nkia.host + _this._url,
			params: params,
			isMask: false,
			async: false,
			notification: false,
			success : function(data) {
				if (data.success) {
					if(typeof data.gridVO != "undefined" && data.gridVO != null) {
						var options = [];
						var gridList = data.gridVO.rows;
						var series = [];
						var headers = _this.dataHeaders;
						var standardHeader = _this.standardHeader;
						
						if(gridList.length > 0){
							
							if(_this.chartType == "halfDonut") {
								var seriesDataList = new Array();
								for(var i=0; i<gridList.length; i++) {
									var gridData = {};
									gridData = gridList[i];
									var subSeriesData = {};
									var gridHeader = gridData[standardHeader];
									var gridValue = gridData[headers[0]];
									if(typeof gridValue != "number") {
										gridValue = gridValue.split(",").join("");
									}
									subSeriesData['name'] = gridHeader;
									subSeriesData['y'] = (gridValue * 1);
									seriesDataList.push( subSeriesData );
								}
								var seriesData = {};
								seriesData['type'] = "pie";
								seriesData['innerSize'] = "50%";
								seriesData['name'] = "값";
								seriesData['data'] = seriesDataList;
								series.push(seriesData);
							} 
							// 도넛형
							else if (_this.chartType == "donut") {
								var seriesDataList = new Array();
								for(var i=0; i<gridList.length; i++) {
									var gridData = {};
									gridData = gridList[i];
									var subSeriesData = {};
									var gridHeader = gridData[standardHeader];
									var gridValue = gridData[headers[0]];
									if(typeof gridValue != "number") {
										gridValue = gridValue.split(",").join("");
									}
									subSeriesData['name'] = gridHeader;
									subSeriesData['y'] = (gridValue * 1);
									seriesDataList.push( subSeriesData );
								}
								var seriesData = {};
								seriesData['type'] = "pie";
								seriesData['innerSize'] = "50%";
								seriesData['name'] = "값";
								seriesData['data'] = seriesDataList;
								series.push(seriesData);
							} 
							// 원형
							else if (_this.chartType == "pie") {
								var seriesDataList = new Array();
								for(var i=0; i<gridList.length; i++) {
									var gridData = {};
									gridData = gridList[i];
									var subSeriesData = {};
									var gridHeader = gridData[standardHeader];
									var gridValue = gridData[headers[0]];
									if(typeof gridValue != "number") {
										gridValue = gridValue.split(",").join("");
									}
									var yData = (gridValue * 1);
									if(yData < 1) {
										yData = null;
									}
									subSeriesData['name'] = gridHeader;
									subSeriesData['y'] = yData;
									seriesDataList.push( subSeriesData );
								}
								var seriesData = {};
								seriesData['name'] = "값";
								seriesData['data'] = seriesDataList;
								series.push(seriesData);
							}
							// 디폴트
							else {
								for(var i=0; i<gridList.length; i++) {
									var gridData = {};
									gridData = gridList[i];
									var subSeriesDataList = new Array();
									for(var j=0; j<headers.length; j++) {
										var gridValue = gridData[headers[j]];
										if(typeof gridValue != "number") {
											gridValue = gridValue.split(",").join("");
										}
										subSeriesDataList.push( (gridValue * 1) );
									}
									var subSeriesData = {};
									subSeriesData['name'] = gridData[standardHeader];
									subSeriesData['data'] = subSeriesDataList;
									series.push(subSeriesData);
								}
							}
							
							_this.series = series;
							
						}else{
							nkia.ui.utils.notification({
								type: "error",
								message: ( this.label != null && this.label != "undefined" ? "[" + this.label + "] " : "" ) + "데이터가 없습니다."
							});
						}
					}
				}
			}
		});
	};
	
	// 차트 옵션 세팅
	this.setOptions = function() {
		
		var _this = this;
		
		if(typeof _this.width == "undefined" || _this.width == 0) {
			_this.width = null;
		}
		if(typeof _this.height == "undefined" || _this.height == 0) {
			_this.height = null;
		}
		
		var standardHeader = _this.standardHeader;
		
		var dataUrl = _this._url;
		var dataParams = _this.params;
		
		var dataHeaders = _this.dataHeaders;
		var dataHeaderNms = _this.dataHeaderNms;
		
		var ceiling = ( _this.ceiling ) ? _this.ceiling : null;
		var isShowLegend = ( typeof _this.showInLegend != "undefined" ) ? _this.showInLegend : true;
		
		var valInterval = ( _this.valInterval ) ? _this.valInterval : 100;
		var valueSuffix = ( _this.valueSuffix ) ? ( "("+_this.valueSuffix+")" ) : "";
		var yTitle = ( _this.yTitle ) ? _this.yTitle : "";
		var yAxisTitle = "";
		
		if(yTitle != "") {
			yAxisTitle = yTitle;
			if(valueSuffix != "") {
				yAxisTitle = ( yAxisTitle + " " + valueSuffix );
			}
			_this.yAxisTitle = yAxisTitle;
		}
		
		if(typeof _this.colorArray == "undefined" || _this.colorArray == null || _this.colorArray.length < 1) {
			_this.colorArray = [
	              "#FFCC00", "#00CC00", "#ff4466", "#33bbff", "#bb77ff"
	              , "#ff9922", "#77dd00", "#ff55aa", "#00dddd", "#8877ff"
	              , "#ff5533", "#88aa00", "#cc00aa", "#009966", "#3355ff"
	              , "#cc0022", "#776600", "#880099", "#005522", "#004488"
			];
		}
		
		if(_this.chartType == "halfDonut" || _this.chartType == "halfPie") {
			_this.renderChartType = null;
			_this.chartPlotOpts['pie'] = {
				startAngle: -90,
				endAngle: 90,
				formatter:function() {
					if(this.y != 0) {
						return "<b>" + this.point.name + "</b>" + this.y + valueSuffix;
					}
				},
				showInLegend: isShowLegend
			};
		} else if(_this.chartType == "donut") {
			_this.renderChartType = 'pie';
			_this.chartPlotOpts['pie'] = {
				borderWidth: 0,
				dataLabels: {
					enabled: true, 
					formatter:function() {
						if(this.y != 0) {
							return "<b>" + this.point.name + "</b>" + this.y + valueSuffix;
						}
					}
				},
				showInLegend: isShowLegend
			};
		} else if(_this.chartType == "pie") {
			_this.renderChartType = _this.chartType;
			_this.chartPlotOpts['pie'] = {
				borderWidth: 0.5,
				dataLabels: {
					enabled: true, 
					formatter:function() {
						if(this.y != 0) {
							return "<b>" + this.point.name + "</b>" + this.y + valueSuffix + " (" + this.point.percentage.toFixed(1) + "%)";
						}
					}
				},
				showInLegend: isShowLegend
			};
		} else if(_this.chartType == "stackColumn") {
			_this.renderChartType = 'column';
			_this.chartPlotOpts['column'] = {
				stacking: 'normal',
				showInLegend: isShowLegend
			};
		} else {
			_this.renderChartType = _this.chartType;
			_this.chartPlotOpts[_this.chartType] = {
				borderWidth: 0,
				dataLabels: {
					enabled: true, 
					formatter:function() {
						if(this.y != 0) {
							var valTxt = this.y;
							var returnTxt = valTxt;
							return returnTxt;
						}
					}
				},
				showInLegend: isShowLegend
			};
		}
	};
	
	// 차트 렌더링
	this.renderChart = function() {
		
		var _this = this;
		
		var highchart = jq('#'+_this.divId).highcharts();
		if(typeof highchart != "undefined") {
			highchart.destroy();
		}
		
		Highcharts.setOptions({
			colors : _this.colorArray
		});
		
		var chartObj = new Highcharts.Chart({
			credits: {
	            enabled: false
	        },
	        exporting: {
	    		enabled: false 
			},
			colors: Highcharts.map(Highcharts.getOptions().colors, function(color) {
				return {
					linearGradient: {x1: 0, x2: 0, y1:0, y2:1},
					stops: [
					        [0.05, Highcharts.Color(color).brighten(+0.12).get('rgb')],
					        [0.95, color]
					]
				};
			}),
			chart : {
				zoomType: 'xy',
				renderTo : _this.divId,
				type : _this.renderChartType,
				width : _this.width,
				height : _this.height,
				reflow : true,
				backgroundColor : ( _this.backgroundColor ) ? _this.backgroundColor : '#FFFFFF'
			},
			title: {
	            text: _this.chartTitle,
	            x: -20 //center
	        },
	        xAxis: {
	            categories: _this.dataHeaderNms
	        },
	        yAxis: {
	        	tickInterval: _this.valInterval,
	        	ceiling: _this.ceiling,
	            title: {
	                text: _this.yAxisTitle
	            },
	            stackLabels: {
	            	enabled:true
	            },
	            plotLines: [{
	                value: 0,
	                width: 1,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	        	// pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	            valueSuffix: _this.valueSuffix
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        plotOptions: _this.chartPlotOpts,
	        series: _this.series
		});
		
	};
	
};

//프라퍼티 검증함수
nkia.ui.validator.HighChart = function(props) {
	if(props.id === undefined || props.id.length === 0 
	|| props._url === undefined || props._url.length === 0
	|| props.dataHeaders === undefined || props.dataHeaders.length === 0
	|| props.standardHeader === undefined || props.standardHeader.length === 0) {
		nkia.ui.utils.notification({
			type: "error",
			message: "Chart 객체에 'id' 혹은 'url', 'dataHeaders', 'standardHeader' property가 없습니다."
		});
		return;
	}
};

//컴포넌트 확장영역
webix.extend(webix.ui.template, {
	/**
	 * 차트 재 로드
	 * @return {}
	 */
	_reload: function(newPrarams) {
		var _this = this;
		_this.config._reload(newPrarams);
	},
	/**
	 * 타이틀 변경
	 * @param {} text
	 */
	_setTitleText: function(text){
		var _this = this;
		var headerTitleSpan = document.getElementById(_this.config.id + "_header_title");
		if(typeof headerTitleSpan != "undefined") {
			headerTitleSpan.innerText = text;
		}
	}
});

/**
 * createHighChartComp
 *  createHighChartComp Field 생성
 * @param {} textareaProps
 * @return {}
 */
function createHighChartComp(chartProps) {
	var props = chartProps||{};
	var modules = [];
	var chart = {};
	try {
		
		// Header 영역
		if(props.header) {
			props.header.id = props.id + "_header";
			props.header.icon = props.header.icon||"chart"; 
			var header = new nkia.ui.common.Toolbar(props.header);
			modules.push(header);
		}
		
		chart = new nkia.ui.highchart.HighChart();
		chart.setProps(props);
		chart.attachEvent();
		
		modules.push(chart);

		// Footer 영역
		if(props.footer && props.footer.buttons) {
			var footer = createButtons(props.footer.buttons);
			modules.push(footer);
		}
		
	} catch(e) {
		alert(e.message);
	}
	
	return modules;
}

