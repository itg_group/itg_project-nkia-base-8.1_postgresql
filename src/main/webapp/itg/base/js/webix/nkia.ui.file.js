webix.protoUI({
	name:"nkia.uploader",
	addFile:function(name, size, type){
		var file = null;
		if (typeof name == "object"){
			file = name;
			name = file.name;
			size = file.size;
		}

		var format;
		if(file.header === undefined){
			format = this._format_size(size);
		}else{
			format = size;
		}
		type = type || name.split(".").pop();

		// 첨부파일 확장자 체크
		if(file.status === undefined){
			var allowFileExt = this.config.allowFileExt;
			if(allowFileExt.length > 0){
				var isAllow = false;
				for(var i = 0; i < allowFileExt.length; i++){
					if(type.toUpperCase() === allowFileExt[i].toUpperCase()){
						isAllow = true;
						break;
					}
				}
				if(!isAllow){
					nkia.ui.utils.notification({
						type: "error",
						message: "파일명: " + name + "<br/> 허용된 확장자가 아닙니다.<br/> 허용된 확장자는 아래와 같습니다. 확인해주세요.<br/>&nbsp;[" + this.config.allowFileExt.join(", ") + "]"
					});
					return;
				}
			}
		}
		
		// 첨부파일 개별 unit Size 체크 로직
		var fileUnitMaxSize = Number(this.config.fileUnitMaxSize);
		var fileUnitMaxSizeByte = fileUnitMaxSize * 1024 * 1024;			// byte 단위 변환
		if(size > fileUnitMaxSizeByte) {
			nkia.ui.utils.notification({
				type: "error",
				message: getConstText({ isArgs : true, m_key : 'msg.common.file.unit.max.size', args: fileUnitMaxSize})
			});
			return false;
		}
		
		// 첨부파일 전체 size 체크
		var fileAllMaxSize = Number(this.config.fileAllMaxSize);
		var fileAllMaxSizeByte = fileAllMaxSize * 1024 * 1024;			// 파일 전체 사이즈 제한 용량 (byte 단위 변환)
		var fileAllList = $$(this.config.id)._getRows();
		var fileAllSizeSum = 0;
		for(var i = 0; i < fileAllList.length; i++) {
			fileAllSizeSum = fileAllSizeSum + Number(fileAllList[i].size);		// 기존 파일목록의 사이즈 합계
		}
		fileAllSizeSum = fileAllSizeSum + size;			// 새로 추가한 파일의 사이즈 합산
		if(fileAllSizeSum > fileAllMaxSizeByte) {
			nkia.ui.utils.notification({
				type: "error",
				message: getConstText({ isArgs : true, m_key : 'msg.common.file.all.max.size', args: fileAllMaxSize})
			});
			return false;
		}

		// File Struct Custom
		var file_struct = {
			file:file,
			name:name,
			id:webix.uid(),
			size:size,
			sizetext:format,
			type:type,
			context:this._last_file_context,
			status:(file.status) ? file.status: "client",
			header: (file.header) ? file.header: false,
			register: (file.register) ? file.register: this.config.register,
			date: (file.date) ? file.date: nkia.ui.utils.date(),
			disabled: this.config.disabled,
			isProcess: this.config.isProcess,
			isTemplate: this.config.isTemplate,
			taskName: (file.taskName) ? file.taskName: this.config.taskName,
			isStream: this.config.isStream,
		    temCode: this.config.temCode
		};

		if (this.callEvent("onBeforeFileAdd", [file_struct])){
			if (!this.config.multiple) {
				var fileList = this._getRows();
				if(fileList && fileList.length > 0) {
					this._clear();
				}
			}
			var id = this.files.add(file_struct);
			this.callEvent("onAfterFileAdd", [file_struct]);
			if (id && this.config.autosend)
				this.send(id);
		}
		
		this._setBadgeCount();
	},
	addDropZone:function(id, hover_text){
		var node = webix.toNode(id);
		var extra_css = "";
		if (hover_text)
			extra_css = " "+webix.html.createCss({ content:'"'+hover_text+'"' }, ":before");
       
		var setting = ( this._settings === undefined ) ? this.config :  this._settings;
		
		var fullcss = "webix_drop_file"+extra_css;
		//web
		webix.event(node,"dragover", webix.html.preventEvent);
		webix.event(node,"dragover", function(e){
			if(!setting.disabled){
				webix.html.addCss(node, fullcss, true);
			}
		});
		webix.event(node,"dragleave", function(e){
			webix.html.removeCss(node, fullcss);
		});

		webix.event(node,"drop", webix.bind(function(e){
			if(!setting.disabled){
				webix.html.removeCss(node, fullcss);

				var data=e.dataTransfer;
				if(data&&data.files.length)
					for (var i = 0; i < data.files.length; i++)
						this.addFile(data.files[i]);
			}
			return webix.html.preventEvent(e);
		}, this));
	},
	_format_size: function(size) {
		var index = 0;
		while (size > 1024){
			index++;
			size = size/1024;
		}
		return Math.round(size*100)/100+" "+webix.i18n.fileSize[index];
	}
}, webix.ui.uploader);

webix.type(webix.ui.list, {
	name:"nkia.uploader.list",
	template:function(obj, common){
		var html = "";
		if(obj.file && obj.file.atch_file_id !== undefined && obj.file.file_idx !== undefined){
			
			if(obj.isStream === true){
				html += "<a href='"+nkia.host+"/itg/base/downloadAtchFileForStream.do?atch_file_id="+obj.file.atch_file_id+"&file_idx="+obj.file.file_idx+"' download='"+obj.name+"'>"+obj.name+"</a>";
				
			}else{
				html += "<a href='"+nkia.host+"/itg/base/downloadAtchFile.do?atch_file_id="+obj.file.atch_file_id+"&file_idx="+obj.file.file_idx+"' download='"+obj.name+"'>"+obj.name+"</a>";
				
			}
			
			if(!obj.disabled){
				html += common.removeIcon(obj);
			}
		}else{
			html += common.fileName(obj);
			if(!obj.disabled){
				html += common.removeIcon(obj);
			}
		}
		html += common.fileSize(obj);
		html += common.date(obj);
		html += common.register(obj);
		if(obj.isProcess){
			html += common.process(obj);
		}
		return html;
	},
	percent:function(obj){
		if (obj.status == 'transfer')
			return "<div style='width:60px; text-align:center; float:right'>"+obj.percent+"%</div>";
		return "<div class='webix_upload_"+obj.status+"'><span class='"+(obj.status =="error"?"error_icon":"fa-check webix_icon")+"'></span></div>";
	},
	fileName:function(obj){
		if(obj.header){
			return "<font style='font-weight:bold'>"+obj.name+"</font>";
		}else{
			return obj.name;
		}
	},
	fileSize:function(obj){
		if(obj.header){
			return "<div style='width:120px; text-align:center; float:right; font-weight:bold'>"+obj.sizetext+"&nbsp;&nbsp;</div>";
		}else{
			return "<div style='width:120px; text-align:right; float:right'>"+obj.sizetext+"&nbsp;&nbsp;</div>";
		}
	},
	removeIcon:function(obj){
		if(obj.header){
			return "<div class='webix_remove_upload' style='width:70px; text-align:center; font-weight: bold;'>삭제</div>";
		}else{
			return "<div class='webix_remove_upload' style='width:70px; text-align:center'><span class='cancel_icon'></span></div>";
		}
	},
	process:function(obj){
		if(obj.header){
			return "<div style='width:150px; text-align:center; float:right; font-weight: bold;'><span>"+obj.taskName+"</span></div>";
		}else{
			return "<div style='width:150px; text-align:center; float:right'><span>"+obj.taskName+"</span></div>";
		}
	},
	register:function(obj){
		if(obj.header){
			return "<div style='width:120px; text-align:center; float:right; font-weight: bold;'><span>"+obj.register+"</span></div>";
		}else{
			return "<div style='width:120px; text-align:center; float:right'><span>"+obj.register+"</span></div>";
		}
	},
	date:function(obj){
		if(obj.header){
			return "<div style='width:100px; text-align:center; float:right; font-weight: bold;'><span>"+obj.date+"</span></div>";
		}else{
			return "<div style='width:100px; text-align:center; float:right'><span>"+obj.date+"</span></div>";
		}
	},
	on_click:{
		"cancel_icon":function(ev, id){
			var uploader = webix.$$(this.config.uploader);
			var fileInfo = uploader.files.getItem(id);
			if(fileInfo.status == "server"){
				uploader.config.delete_file_info.push(fileInfo.file.file_idx);
			}
			webix.$$(this.config.uploader).files.remove(id);
			uploader._setBadgeCount();
		}
	}
});

nkia.ui.file.File = function() {
	// Webix Properties
	//속성을 초기화합니다.
	this.view = "nkia.uploader";
	this.id = "";
	this.label = getConstText({isArgs : true, m_key : 'btn.common.file.select'});
	this.autosend = false;
	this.disabled = false;
	this.multiple = true;
	this.link = "";
	this.formData = {};
	this.width = 90;
	this.on = {};

	// Custom Properties
	this.rows = [];
	this.sequenceUrl = nkia.host + "/itg/base/getAtchFileSeq.do";
	this.selectUrl = nkia.host + "/itg/base/selectAtchFileList.do";
	this.selectProcessUrl = nkia.host + "/itg/base/selectAtchFileListForProc.do";
	this.selectProcessTemplateUrl = nkia.host + "/itg/base/selectAtchFileListForTemplate.do";
	this.insertUrl = nkia.host + "/itg/base/initRegistAtchFile.do";
	this.updateUrl = nkia.host + "/itg/base/modifyRegistAtchFile.do";
	this.deleteUrl = nkia.host + "/itg/base/deleteAtchFileByAjax.do";
	this.required = false;
	this.atch_file_id = "";
	this.delete_file_info = [];
	this.allowFileExt = CONSTRAINT.DEFAULT_FILE_ALLOW_EXT.split(",");
	this.register = "";
	this.isProcess = false;
	this.isTemplate = false;
	this.taskName = "";
	this.badgeId = "";
	this.reqType = "";
	this.reqDocId = "";
	this.fileAllMaxSize = CONSTRAINT.DEFAULT_FILE_ALL_MAX_SIZE.split(",");
	this.fileUnitMaxSize = CONSTRAINT.DEFAULT_FILE_UNIT_MAX_SIZE.split(",");
	//첨부파일의 상단 타이틀도 필요없는 경우 사용하는 변수
	this.isTitle = true;
	//첨부파일 저장 방식이 DB저장방식인지 여부 
	this.isStream = false;
	//파일 삭제시 파일이 저장된 row도 삭제할지 여부 
	this.deleteRowYN = "N";
	//첨부파일 upload 성공여부
	this.isUploadSuccess = true;
	//첨부파일 exception 메세지
	this.resultMsg = "";

	this.propsValidate = function(props) {
		nkia.ui.validator.file(props);
	};

	this.setProps = function(props) {
		this.propsValidate(props);
		for (var i in props) {
			if (this[i] != undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}

		if (props.readonly != undefined) {
			this.disabled = props.readonly;
		}

		// Parameters Setting
		if (props.params != undefined) {
			this.formData = props.params; 
		}

		if (typeof this.allowFileExt == "string") {
			this.allowFileExt = this.allowFileExt.split(",");
		}
	};

	this.attachEvent = function() {
		this.on = {
			onAfterFileAdd: function(){
				this.isUploadSuccess = true;
				this.resultMsg = "";
			},
			onFileUpload: function(item, response) {
				if (item.failure) {
					this.isUploadSuccess = false;
					this.resultMsg = item.resultMsg;
				}
			},
			onUploadComplete: function(item, response) {
				if (this.isUploadSuccess) {
					this.callback(this.config.atch_file_id);
				} else {
					var message = this.resultMsg;
					if (message == null || message.indexOf("error") != -1 || message == "") {
						message = "파일 전송 처리중에 일부 오류가 발생하였습니다.<br/>누락된 파일을 확인하시기 바랍니다.";
					}

					nkia.ui.utils.notification({
						type: "error",
						message: message
					});

					this._getFileList(this.config.atch_file_id);
				}
				this.delete_file_info = [];
			},
			onFileUploadError: function(item, response){
			}
		};
	};
};

//프라퍼티 검증함수
nkia.ui.validator.file = function(props) {
	if(typeof props.id === "undefined" || props.id.length === 0) {
		nkia.ui.utils.notification({
			type: "error",
			message: "첨부파일 ID가 없습니다."
		});
		return;
	}
};

//컴포넌트 확장영역
webix.extend(webix.ui.uploader, {
	/**
	 * 첨부파일 헤더 생성
	 */
	_header: function() {
		var header = {
			header: true,
			name: "파일명",
			register: "등록자",
			date: "등록일",
			size: "Size",
			status: "server"
		};
		
		if(this.config.isProcess){
			header.taskName = "작업명";
		}

		this.addFile(header, 0, "");
	},
	/**
	 * 첨부파일 초기화. 
	 * 파일 목록 삭제, 헤더 생성, 삭제 배열 초기화 수행
	 */
	_clear: function() {
		this.files.clearAll();
		this._header();
		this.delete_file_info = [];
		this._setBadgeCount();
		//20171012 molee
		//첨부파일 id가 초기화되지 않아 추가
		this.config.atch_file_id = '';
	}, 
	/**
	 * 첨부파일 등록/수정
	 * 파일 등록/수정 처리 후에 callback 함수를 호출해주며, form 파라미터가 있는 경우 file id에 atch_file_id 값을 삽입하여준다.
	 * params에 파라미터를 셋팅하면 추가된 파라미터가 전달된다.
	 * @param {} callback
	 * @param {} form
	 * @param {} params
	 */
	_send: function(callback, form, params) {
		var _this = this;
		var isUpload = false;
		var config = _this.config;
		var browerVersion = navigator.appVersion;
		// 업로드 목록을 체크
		if(params !== undefined && config.formData.task_id ){
			if(params.workType !== undefined){
				if(params.workType=="update" && config.formData.task_id=="REQUST_RCEPT"){
					config.formData.task_id="REQUST_REGIST";
				}							
			}
		}
		if(!_this.isUploaded()){
			isUpload = true;
			if(config.atch_file_id){
				// 수정
				config.upload = config.updateUrl;
				if(config.delete_file_info.length > 0){
					// 삭제가 있는 경우 파라미터에 추가
					config.formData.delete_file_info = config.delete_file_info.join(',');
					config.formData.deleteRowYN = config.deleteRowYN;
				}
			}else{
				// 신규 등록: 첨부파일 ID를 취득한다.    
				webix.ajax().sync().post(
					config.sequenceUrl,
					JSON.stringify({}),
					function (text) {
						config.atch_file_id = text;
						if(form){
							var map = {};
							map[config.id] = config.atch_file_id;
							form._setValues(map);
						}
					}
				);
				// url setting
				config.upload = config.insertUrl;
			}
			config.formData.atch_file_id = config.atch_file_id;
			if(params){
				nkia.ui.utils.extend(config.formData, params);
			}
			_this.send();
			// 삭제파일정보 초기화
			_this.config.delete_file_info = [];
			// callback 처리
			// IE 9이하에서는 WEBIX API가 html5가 지원 안될 시 callback을 받지 못하는 문제가 있음
			if(browerVersion.indexOf("MSIE 9")>-1 || browerVersion.indexOf("MSIE 8")>-1) {
				callback(config.atch_file_id);
			} else {
				_this.callback = callback;
			}
			
		}else{
			// 삭제파일이 있는 경우에도 서버에 요청을 보낸다.
			if(config.atch_file_id !== "" && config.delete_file_info.length > 0){
				isUpload = true;
				// IE 버전의 HTML5 지원여부에 따른 처리 분기
				if(browerVersion.indexOf("MSIE 9")>-1 || browerVersion.indexOf("MSIE 8")>-1) {
					nkia.ui.utils.ajax({
						url: config.deleteUrl,
						params: {
							atch_file_id: config.atch_file_id,
							delete_file_info: config.delete_file_info.join(','),
							deleteRowYN: config.deleteRowYN
						},
						isMask: false,
						async: false,
						success: function(response){
							var interval_callback = setInterval(function () {
								callback(_this.config.atch_file_id);
							    clearInterval(interval_callback);
							}, 1500);
							_this.config.delete_file_info = [];
						}
					});
				} else {
					var formData = new FormData();

					var headers = {};
					var details = {
						atch_file_id: config.atch_file_id,
						delete_file_info: config.delete_file_info.join(','),
						deleteRowYN: config.deleteRowYN
					};

					var xhr = new XMLHttpRequest();
					var url = config.updateUrl;   
					if(webix.callEvent("onBeforeAjax",["POST", url, details, xhr, headers, formData])){
						for(var key in details){
							formData.append(key, details[key]);
						}

						xhr.onload = webix.bind(function(e){
							if (!xhr.aborted){
								if(xhr.responseText){
									var response = JSON.parse(xhr.responseText);
									if(response.success){
										_this.callback(_this.config.atch_file_id);
										
									}  
									if(response.failure){
										nkia.ui.utils.notification({
											type: "error",
											message: response.resultMsg
										});
										_this._getFileList(_this.config.atch_file_id);
									}
									
									_this.config.delete_file_info = [];
								}
							}
						}, this);
						xhr.open('POST', url, true);

						for(var header in headers){
							xhr.setRequestHeader(header, headers[header]);
						}
						xhr.send(formData);
						
						// callback 처리
						_this.callback = callback;
					}
				}
			}
		}
		if(!isUpload){
			callback(config.atch_file_id);
		}
	},
	/**
	 * atchFileId로 파일 정보를 불러온다.
	 * 프로세스의 경우에는 srId로 파일 정보를 불러온다.
	 * @param {} atchFileId
	 * @param {} srId
	 * @return {}
	 */
	_getFileList: function(atchFileId, srId){
		var _this = this;
		
		_this._clear();
		_this.config.atch_file_id = atchFileId;

		var url = _this.config.selectUrl;
		var params = {
			atch_file_id: atchFileId
		};
		if(_this.config.isProcess){
			url = _this.config.selectProcessUrl;
			params = {
				sr_id: srId
			};
		}
		if(_this.config.isTemplate){
			url = _this.config.selectProcessTemplateUrl;
			
			if(_this.config.temCode){
				params = {
						req_type: _this.config.reqType
					   ,tem_code: _this.config.temCode	
					   ,req_doc_id: _this.config.reqDocId
				};
			
			}else if(_this.config.reqDocId){
				params = {
						req_type: _this.config.reqType	
					   ,req_doc_id: _this.config.reqDocId
				};	
			}else {
				params = {
						req_type: _this.config.reqType	
					   ,req_doc_id: _this.config.reqDocId
				};	
			}
			
			
		}
		
		// Count 처리로 인하여 sync 요청
		webix.ajax().sync().post(
			url,
			JSON.stringify(params),
			function (text) {
				var response = JSON.parse(text);
				var gridVO = response.gridVO;
				if(gridVO !== null && gridVO.rows !== null && gridVO.rows.length > 0){
					var rows = gridVO.rows;
					rows.forEach(function(file) {
						var fileData = {
							atch_file_id: file.atch_file_id,
							file_idx: file.file_idx,
							name: file.orignl_file_name,
							size: file.file_size,
							register: file.upd_user_nm,
							date: file.upd_dt,
							status: "server"
						};

						if(_this.config.isProcess){
							fileData.taskName = file.nodename;
						}

						_this.addFile(fileData, file.file_size, file.file_extsn);
					});
				}
			}
		);
	},
	/**
	 * 현재 파일 목록을 가져온다.
	 * @return {}
	 */
	_getRows: function(){
		var fileRows = [];
		var fileList = $$(this.config.id + "_list");
		if(fileList && fileList.count() > 0){
			fileList.data.each(function(item){
			    if(!item.header){
			    	fileRows.push(item);
			    }
			});
		}
		return fileRows;
	},
	/**
	 * 파일 건 수를 가져온다.
	 * @return {}
	 */
	_getFileCount: function(){
		var fileCount = 0;
		var fileList = $$(this.config.id + "_list");
		if(fileList && fileList.count() > 0){
			fileList.data.each(function(item){
			    if(!item.header){
			    	fileCount++;
			    }
			});
		}
		return fileCount;
	},
	/**
	 * Tab Badge 영역에 파일 건 수를 업데이트 한다.
	 */
	_setBadgeCount : function() {
		// 뱃지ID가 있다면 뱃지의 카운트를 업데이트한다.
		if(this.config.badgeId) {
			var badge = document.getElementById(this.config.badgeId);
			if(badge) {
				// Header를 제외해야하기 떄문에 -1 처리를 한다.
				badge.innerText = this.files.data.order.length - 1;
			}
		}
	}
});

nkia.ui.file.FileList = function() {
	// Webix Properties
	//속성을 초기화합니다.
	this.view = "list";
	this.id = "";
	this.type = "nkia.uploader.list";
	this.autoheight = true;
	this.scroll = false;

	// Custom Properties
	this.height = 0;
	this.isProcess = false;
	this.isTemplate = false;
	this.reqType = "";

	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		// id setting
		this.id = props.id + "_list";

		if (props.height === 0) {
			delete this.height;
		}else{
			this.autoheight = false;
			this.scroll = true;
		}
	};
};

nkia.ui.file.FileRender = function() {
	// Custom Properties
	this.rows = [];
	this.isProcess = false;
	this.isTemplate = false;
	this.required = false;
	this.isTitle = true;
	this.processLabel = "서식파일 다운로드";

	this.setProps = function(props) {
		for (var i in props) {
			if (this[i] != undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	}

	this.setRender = function(props) {
		// File 생성
		var file = new nkia.ui.file.File();
		props.link = props.id + "_list";
		file.setProps(props);
		file.attachEvent();

		// 첨부파일 상위 타이틀이 없는 경우 추가
		var cols = [];
		if (this.isTitle) {
			cols = [
			  	{ view:"label", width: 10 },{ view: "label", label: getConstText({isArgs : true, m_key : 'res.ubit.center.000007'}), css: "label_file" }, file
			]

			if(this.isTemplate) {
				cols = [
			        { view: "label", label: this.processLabel, css: "label_template" }, file
				]
			}
		} else {
			cols = [
			  	{ view:"label", width: 3 },{ view: "label", label: "첨부파일", css: "label_file" }, file
			]
		}

		this.rows.push({
			cols: cols,
			padding: 3,
			id: props.id + "_list_header"
		});

		// File List 생성
		var fileList = new nkia.ui.file.FileList();
		fileList.setProps(props);
		this.rows.push(fileList);
	};
};

/**
 * 파일첨부 컴포넌트 (파일저장방식)
 * @method {} createAtchFileComp : File Function
 * @property {string} id		 : File Id
 */
function createAtchFileComp(fileProps) {
	var props = fileProps||{};
	var file = {};
	try {
		// Body 영역
		file = new nkia.ui.file.FileRender();
		file.setProps(props);
		file.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return file;
}

/**
 * 파일첨부 컴포넌트 (DB저장방식)
 * @method {} createAtchFileForStreamComp : File Function
 * @property {string} id		 : File Id
 */
function createAtchFileForStreamComp(fileProps) {
	var props = fileProps||{};
	var file = {};
	props['isStream']  = true;
	props['insertUrl'] =  nkia.host + "/itg/base/initRegistAtchFileForStream.do";
	props['updateUrl'] =  nkia.host + "/itg/base/modifyRegistAtchFileForStream.do";
	props['deleteUrl'] =  nkia.host + "/itg/base/deleteAtchFileForStreamByAjax.do";
	
	try {
		// Body 영역
		file = new nkia.ui.file.FileRender();
		file.setProps(props);
		file.setRender(props);
	} catch(e) {
		alert(e.message);
	}
	return file;
}
