/**
 * @author Shea Frederick - http://www.vinylfox.com
 * @class Ext.ux.form.HtmlEditor.Image
 * @extends Ext.util.Observable
 * <p>A plugin that creates an image button in the HtmlEditor toolbar for inserting an image. The method to select an image must be defined by overriding the selectImage method. Supports resizing of the image after insertion.</p>
 * <p>The selectImage implementation must call insertImage after the user has selected an image, passing it a simple image object like the one below.</p>
 * <pre>
 *      var img = {
 *         Width: 100,
 *         Height: 100,
 *         ID: 123,
 *         Title: 'My Image'
 *      };
 * </pre>
 */
Ext.ux.form.HtmlEditor.Image = Ext.extend(Ext.util.Observable, {
	// Image language text
	langTitle: 'Insert Image',
    urlSizeVars: ['width','height'],
    basePath: 'image.php',
    init: function(cmp){
        this.cmp = cmp;
        this.cmp.on('render', this.onRender, this);
        this.cmp.on('initialize', this.onInit, this, {delay:100, single: true});
    },
    onEditorMouseUp : function(e){
        Ext.get(e.getTarget()).select('img').each(function(el){
        	
            var w = el.getAttribute('width'), h = el.getAttribute('height'), src = el.getAttribute('src')+' ';
            src = src.replace(new RegExp(this.urlSizeVars[0]+'=[0-9]{1,5}([&| ])'), this.urlSizeVars[0]+'='+w+'$1');
            src = src.replace(new RegExp(this.urlSizeVars[1]+'=[0-9]{1,5}([&| ])'), this.urlSizeVars[1]+'='+h+'$1');
            el.set({src:src.replace(/\s+$/,"")});
        }, this);
        
    },
    onInit: function(){
        Ext.EventManager.on(this.cmp.getDoc(), {
			'mouseup': this.onEditorMouseUp,
			buffer: 100,
			scope: this
		});
    },
    onRender: function() {
    	var toolbar = this.cmp.getToolbar();
        var btn = Ext.create('Ext.button.Button', {
            iconCls: 'x-edit-image',
            handler: this.selectImage,
            scope: this,
            tooltip: {
                title: this.langTitle
            },
            overflowText: this.langTitle
        });
        
        toolbar.add(btn);
    },
    selectImage: function(){
    	var me = this;
    	
    	/*
    	 * Create Btns
    	 */
    	var uploadHandler = function(){
    		var form = uploadPanel.getForm();
            if(form.isValid()){
            	
                form.submit({
                    url: "/itg/base/uploadWebEditorImage.do",
                    waitMsg: '업로드 중입니다.',
                    success: function(form, action) {
                    	response = Ext.decode(action.response.responseText);
                        if(response.success){
                        	var img = {};
                        	img.src = "/webeditor/images/" + action.result.resultData.uploadFileName;
                        	img.id = "insertImg";
                        	img.name = "insertImg";
                        	
                        	me.insertImage(img);
                        }
                        
                        win.close();
                    },
                    failure: function(form, action) {
                    	response = Ext.decode(action.response.responseText);
                        if(response.failure){
                        	if( response.resultMsg ){
                        		Ext.Msg.alert('File Upload', response.resultMsg);
                        	}else{
    							Ext.Msg.alert('File Upload', 'Fail uploaded to the Server');                    		
                        	}
                        }
                        
                        win.close();
                    }
                });
            }
        }
    	
    	var uploadBtn = createBtnComp({
    		id: "uploadBtn",
	        label: "적용",
	        ui: "correct",
	        scale: "medium"
	    });
    	
    	attachBtnEvent(uploadBtn, uploadHandler);
    	
    	/*
    	 * Create Ext.form.Panel
    	 */
    	var uploadPanel = Ext.create("Ext.form.Panel", {
    	    width: 400,
    	    bodyPadding: 10,
    	    //frame: true,
    	    items: [{
    	        xtype: "filefield",
    	        name: "image",
    	        fieldLabel: "이미지",
    	        labelWidth: 50,
    	        msgTarget: "side",
    	        allowBlank: false,
    	        anchor: "100%",
    	        buttonText: "선택"
    	    }]
    	});
    	
    	/*
    	 * Create Ext.window
    	 */
    	var windowPop = {
    		id: "popImageUpload",
    		title: "이미지 업로드",
    		width: 400,
    		height: 150,
    		buttons: [uploadBtn],
    		closeBtn: true,
    		items: [uploadPanel]
    	};
    	
    	var win = createWindowComp(windowPop);
    	
    	win.show();
    },
    
    /**
     * 
     * @param img - Object[ src, width, height, id, title, alt ] variable
    **/
    insertImage: function(img) {
    	var me = this;
    	
    	var result = Ext.String.format("<img src='{0}' name='{1}'>", img.src, img.name);
    	
    	me.cmp.insertAtCursor(result);
		
    	if( Ext.isWebKit ){
    		/*
    		var getElement = function( imgObj ){
    			var elList = doc.getElementsByName( imgObj.id );
    			var i = 0;
    			for( i=0; i < elList.length; i++ ){
    				if( elList[i].src.indexOf(imgObj.src) > -1 ){
    					break;
    				}
    			}
    			
    			return elList[i];
    		};
    		
    		var initClickEvent = function(){
    			var elList = doc.getElementsByName( img.id );
    			var i = 0;
    			for( i=0; i < elList.length; i++ ){
    				var imgObj = Ext.get( elList[i] );
    				if( imgObj.$cache.events.click == null ){
    					imgObj.on("click", imgClickHandler);
    					//console.log( "imgObj.$cache.events : ", imgObj.$cache.events );
    				} else {
    					//console.log("haveEvent : ", imgObj.$cache.events.click);
    				}
    			}
    		};
    		
    		var doc = me.cmp.getDoc();
    		
    		var prevValue = null;
    		var imgResizer = null;
    		var imgClickHandler = function(){
    			if( imgResizer != null ){
    				me.cmp.setValue(prevValue);
    				imgResizer = null;
    				//return;
    			}
    			
    			prevValue = me.cmp.getValue();
    			
    			//var imgTag = doc.getElementsByName( "imgage" );
        		var imgTag = this;
        		//var imgExtObj = Ext.get(imgTag[0]);
        		var imgExtObj = Ext.get(imgTag);
    			
    			imgResizer = Ext.create('Nkia.board.ImageResizer', {
    				el: imgTag,
    				preserveRatio: false
    			});
    			
    			imgResizer.constrainTo = doc;
    			
    			imgResizer.on("resize", function(){
    				var nextWidth = imgExtObj.getWidth();
    				var nextHeight = imgExtObj.getHeight();
    				
    				imgExtObj.un("click", imgClickHandler);
    				
    				imgResizer.destroy();
    				
    				me.cmp.setValue(prevValue);
    				
    				imgTag = getElement(img);
    				imgExtObj = Ext.get(imgTag);
    				
    				imgExtObj.setSize(nextWidth, nextHeight);
    				
    				imgExtObj.on("click", imgClickHandler);
    			});
    		};
    		
    		initClickEvent();
    		*/
    	}
    }
});