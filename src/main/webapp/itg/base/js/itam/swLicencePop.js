function openSelSwAssetLicenceAssignPop(swManagerInfoSelData, amSwSelRows) {
//	var swConfId = rowData["CONF_ID"];
//	var swConfNm = rowData["CONF_NM"];
//	var licenceType = rowData["LICENCE_TYPE"];
//	var swGroupId = rowData["SW_GROUP_ID"];
//	var licenceApplyBase = rowData["LICENCE_APPLY_BASE"];
	
	var setPopUpProp = {
		id: 'assetLicenceAssignPop',
		popUpText: "소프트웨어 라이선스 할당",
		title: "소프트웨어 라이선스 할당",
		prefix: 'grid.itam.sam.unAssignedSw',
		height: 600,
		width: 1100,
		url: '/itg/itam/sam/selectLicenceUnassignedSwList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		swManagerInfoSelData: swManagerInfoSelData,
		amSwSelRows: amSwSelRows
	}
	var popUp = createSelectSwAssetLicenceAssignPop(setPopUpProp);
	popUp.show();
}

function createSelectSwAssetLicenceAssignPop(setPopUpProp){	
	var that = convertDomToArray(setPopUpProp);
	var setSwAssetLicenceAssignPop = null;
	
	// validation Check 를 위한...
	var customFieldProp = {};
	customFieldProp["ASSIGN_LICENCE_CNT"] = { xtype: "numberfield", name: 'ASSIGN_LICENCE_CNT', minValue: 1, allowDecimals: true  };
	customFieldProp["APPLY_LICENCE_FACTOR"] = {xtype: "numberfield", name: 'APPLY_LICENCE_FACTOR', minValue: 0.25, maxValue: 1, allowDecimals: true, decimalPrecision: 2, step: 0.25 };
	
	var swManagerInfoSelData = that["swManagerInfoSelData"];
	var swGroupId = swManagerInfoSelData["SW_GROUP_ID"];
	var swMngtLicenceScope = swManagerInfoSelData["SW_MNGT_LICENCE_SCOPE"];
	
	var amSwSelRows = that["amSwSelRows"];
	var licenceType = amSwSelRows[0].raw["LICENCE_TYPE"];
	var licenceTypeNm = amSwSelRows[0].raw["FN_LICENCE_TYPE_NM"];
	var licenceApplyBase = amSwSelRows[0].raw["LICENCE_APPLY_BASE"];
	var licenceApplyBaseNm = amSwSelRows[0].raw["FN_LICENCE_APPLY_BASE_NM"];
	var assignConfIdArray = new Array();
	var assignConfNmArray = new Array();
	var assignSwOptionIdArray = new Array();
	var excludeConfIdArray = new Array();
	var isBaseLicenceOption = false;
	var popupRightTitle = "";
	popupRightTitle += "<font color='red' style='float:right'>&nbsp;" + licenceApplyBaseNm + "</font>";
	popupRightTitle += "<font color='blue' style='float:right'>&nbsp;| " + licenceTypeNm + " |&nbsp;</font>";
	popupRightTitle += "<font color='green' style='float:right'>";
	for( var i = 0; i < amSwSelRows.length; i++ ){
		var amSwSelData = amSwSelRows[i].raw;
		var swConfId = amSwSelData["CONF_ID"]
		assignConfIdArray.push(swConfId);
		assignConfNmArray.push(amSwSelData["CONF_NM"]);
		assignSwOptionIdArray.push(amSwSelData["SW_GROUP_OPTION_ID"]);
		if( i == amSwSelRows.length - 1 ){
			popupRightTitle += amSwSelData["CONF_NM"];
		}else{
			popupRightTitle += amSwSelData["CONF_NM"] + " / ";
		}
		
		var baseLicenceOptionYn = amSwSelData["BASE_LICENCE_OPTION_YN"];
		if( baseLicenceOptionYn == "Y" ){
			excludeConfIdArray = new Array();
			excludeConfIdArray.push(swConfId)
			isBaseLicenceOption = true;
		}else{
			if( !isBaseLicenceOption ){
				excludeConfIdArray.push(swConfId);
			}
		}
	}
	popupRightTitle += "</font>";
	
	var params = { sw_group_id : swGroupId, licence_apply_base : licenceApplyBase, sw_mngt_licence_scope: swMngtLicenceScope, exclude_sw_cond_ids : excludeConfIdArray, exclude_sw_group_option_ids : assignSwOptionIdArray };
	if(swManagerInfoSelData["processPop"]) {
		params["selected_conf_id"] = swManagerInfoSelData.selected_conf_id;
	}
	// 미할당 소프트웨어 목록(라이선스 적용기준에 따라서 라이선스 할당 수를 계산해준다.)
	var swAssetLicenceUnassignGridProp = {	
		id: 'swAssetLicenceAssignGrid',
		title: "미할당 소프트웨어 목록" + popupRightTitle,  
		gridHeight: 530,
		resource_prefix: that['prefix'],				
		url: that['url'],
		params: params,
		selModel: true,
		pagingBar : false,
		autoLoad: true,
		cellEditing: true,
		editorMode: true,
		selfScroll: true,
		loadTitleCount: true,
		selectionMode: 'MULTI',
		customFieldProp: customFieldProp
	};
	var swAssetLicenceUnassignGrid = createGridComp(swAssetLicenceUnassignGridProp);

	var isCancelSelect = false;
	swAssetLicenceUnassignGrid.on('beforeitemmousedown',function( gridView, record, item, index, evt, eOpts ){
//		console.info("beforeitemmousedown = " + evt.target.offsetParent.cellIndex)
		var cellIndex = evt.target.offsetParent.cellIndex;
		if( cellIndex == "14" || cellIndex == "15" ){
			isCancelSelect = true;
		}else{
			isCancelSelect = false;
		}
	});

	swAssetLicenceUnassignGrid.on('beforeselect',function(gridView, record, eOpts){
//		console.info("beforeselect = " + isCancelSelect)
		if( isCancelSelect ){
			return false;
		}
	});  
	
	swAssetLicenceUnassignGrid.on('beforedeselect',function(gridView, record, eOpts){
//		console.info("beforedeselect = " + isCancelSelect)
		if( isCancelSelect ){
			return false;
		}
	});
	
	swAssetLicenceUnassignGrid.on('beforeedit',function(editor ,e){    
		var field = e.field;
		if( field == "ASSIGN_LICENCE_CNT" ){
			if( licenceType == "COUNT" || licenceType == "SITE" || licenceType == "ULA" ){
		    	// 카운트, 사이트, ULA라이선스 일 경우에는 할당 라이선스 수를 변경 할 수 없다.
		        return false;//this will disable the cell editing
		    } 
		}
		/*
		else if( field == "APPLY_LICENCE_FACTOR" ){
			if( licenceType == "COUNT" || licenceType == "SITE" || licenceType == "ULA" ){
		    	// 카운트, 사이트, ULA라이선스 일 경우에는 할당 라이선스 수를 변경 할 수 없다.
		        return false;//this will disable the cell editing
		    }
		}
		*/
		return true;
	});
	
	swAssetLicenceUnassignGrid.on('edit', function(editor, e) {
		var field = e.field;
		var changeValue = e.value;
		if( field == "APPLY_LICENCE_FACTOR" ){
			e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["ASSIGN_LICENCE_CNT"] * changeValue);
		}
		/* 직접 할당할 경우에는 수정하지 않는다.
		if( field == "ASSIGN_LICENCE_CNT" ){
			if( licenceApplyBase == "BASE_CPU_CNT"){
				e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["AM_SL_CPU_CNT"] * e.record.raw["APPLY_LICENCE_FACTOR"] * changeValue);
			}else if( licenceApplyBase == "BASE_CPU_CORE_CNT"){
				e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["AM_SL_CPU_CORE_CNT"] * e.record.raw["APPLY_LICENCE_FACTOR"] * changeValue);
			}else{
				e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["APPLY_LICENCE_FACTOR"] * changeValue);
			}
		}
		*/
		//swAssetLicenceUnassignGrid.view.selModel.select(e.record, true, true);
	});
	
	
	// 등록버튼 생성
	var acceptBtn = new Ext.button.Button({
		text: '할당',
		ui:'correct',
		handler: function() {
			var gridSelModel = swAssetLicenceUnassignGrid.view.selModel;
			if( gridSelModel.hasSelection() ){
				// 마스크를 띄웁니다.
				setComponentMask("assetLicenceAssignPop", true);
				
				if(!confirm( "[" + assignConfNmArray.join(" / ") +  "] 자산에 라이선스를 할당하시겠습니까?" )){
					// 마스크를 숨깁니다.
					setComponentMask("assetLicenceAssignPop", false);
					return;
				}
				var sw_conf_ids = assignConfIdArray;
				var sw_option_ids = assignSwOptionIdArray;
				var assign_licence_state = "USED";
				var assign_data_list = swAssetLicenceUnassignGrid.getSelectedGridData();
				jq.ajax({ 
					type:"POST", 
					url: getConstValue('CONTEXT') + '/itg/itam/sam/insertAmSwLicenceAssign.do', 
					contentType: "application/json", 
					dataType: "json", 
					async: false, 
					data : getArrayToJson({sw_conf_ids : sw_conf_ids, assign_licence_state : assign_licence_state, assign_data_list : assign_data_list, sw_option_ids: sw_option_ids, sw_mngt_licence_scope: swMngtLicenceScope }), 
					success:function(data){
						if( data.success ){
							alert(data.resultMsg);
							setComponentMask("assetLicenceAssignPop", false);
							searchSwManagerInfoList();
							setSwAssetLicenceAssignPop.close();
						}else{
							alert(data.resultMsg);
							setComponentMask("assetLicenceAssignPop", false);
						}
					}
				});
				
			}else{
				alert("선택된 소프트웨어가 없습니다.");
				return;
			}
		}
	});
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		handler: function() {
			setSwAssetLicenceAssignPop.close();
		}
	});
	
	setSwAssetLicenceAssignPop = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		title: that['title'],  
		height: that['height'], 
		width: that['width'],  
		autoDestroy: false,
		resizable:false,
		buttonAlign: that['buttonAlign'],
		buttons: [acceptBtn, closeButton],
		bodyStyle: that['bodyStyle'],
		items: [swAssetLicenceUnassignGrid]
	});
	
	return setSwAssetLicenceAssignPop;

}

function openSelSwAssetLicenceRetrievePop(rowData) {
	var setPopUpProp = {
		id: 'swAssetLicenceRetrievePop',
		popUpText: "소프트웨어 라이선스 회수",
		title: "소프트웨어 라이선스 회수",
		prefix: 'grid.itam.sam.assignedSw',
		height: 600,
		width: 1100,
		url: '/itg/itam/sam/selectLicenceAssignedSwList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		amSwSelData: rowData
	}
	var popUp = createSelectSwAssetLicenceRetrievePop(setPopUpProp);
	popUp.show();
}

function createSelectSwAssetLicenceRetrievePop(setPopUpProp){	
	var that 		= convertDomToArray(setPopUpProp);
	var setSwAssetLicenceRetrievePop = null;
	
	var amSwSelData = that["amSwSelData"];
	var swConfId = amSwSelData["CONF_ID"];
	var swConfNm = amSwSelData["CONF_NM"];
	var licenceTypeNm = amSwSelData["FN_LICENCE_TYPE_NM"];
	var licenceApplyBaseNm = amSwSelData["FN_LICENCE_APPLY_BASE_NM"];
	var swGroupOptionId = amSwSelData["SW_GROUP_OPTION_ID"];
	var baseLicenceOptionYn = amSwSelData["BASE_LICENCE_OPTION_YN"];
	
	var popupRightTitle = "";
	popupRightTitle += "<font color='red' style='float:right'>&nbsp;" + licenceApplyBaseNm + "</font>";
	popupRightTitle += "<font color='blue' style='float:right'>&nbsp;| " + licenceTypeNm + " |&nbsp;</font>";
	popupRightTitle += "<font color='green' style='float:right'>";
	popupRightTitle += amSwSelData["CONF_NM"];
	popupRightTitle += "</font>";
	
	// 할당 소프트웨어 목록
	var swAssetLicenceAssignGridProp = {	
		id: 'swAssetLicenceAssignGrid',
		title : "할당 소프트웨어 목록"  + popupRightTitle,
		gridHeight: 530,
		resource_prefix: that['prefix'],				
		url: that['url'],
		params: { conf_id : swConfId },
		selModel: true,
		pagingBar : false,
		autoLoad: true,
		loadTitleCount: true
	};
	var swAssetLicenceAssignGrid = createGridComp(swAssetLicenceAssignGridProp);
	
	var retrieveBtn = new Ext.button.Button({
		text: '회수',
		ui:'correct',
		handler: function() {
			var gridSelModel = swAssetLicenceAssignGrid.view.selModel;
			if( gridSelModel.hasSelection() ){
				// 마스크를 띄웁니다.
				setComponentMask("swAssetLicenceRetrievePop", true);
				
				var confirmMsg = "[" + swConfNm +  "] 자산에 할당된 라이선스를 회수 하시겠습니까?";
				if( swGroupOptionId != null && baseLicenceOptionYn == "Y" ){
					confirmMsg = "라이선스 기준 옵션 [" + swConfNm +  "] 자산입니다. 연관된 옵션 라이선스도 함께 회수됩니다. 할당된 라이선스를 회수 하시겠습니까?";
				}
				
				if(!confirm( confirmMsg )){
					// 마스크를 숨깁니다.
					setComponentMask("swAssetLicenceRetrievePop", false);
					return;
				}
				
				var retrieve_data_list = swAssetLicenceAssignGrid.getSelectedGridData();
				jq.ajax({ 
					type:"POST", 
					url: getConstValue('CONTEXT') + '/itg/itam/sam/deleteAmSwLicenceRetrieve.do', 
					contentType: "application/json", 
					dataType: "json", 
					async: false, 
					data : getArrayToJson({sw_conf_id : swConfId, retrieve_data_list : retrieve_data_list, sw_group_option_id : swGroupOptionId, base_licence_option_yn : baseLicenceOptionYn }), 
					success:function(data){
						if( data.success ){
							alert(data.resultMsg);
							setComponentMask("swAssetLicenceRetrievePop", false);
							searchSwManagerInfoList();
							setSwAssetLicenceRetrievePop.close();
						}else{
							setComponentMask("swAssetLicenceRetrievePop", false);
							alert(data.resultMsg);
						}
					}
				});
				
			}else{
				alert("선택된 소프트웨어가 없습니다.");
				return;
			}
		}
	});
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		handler: function() {
			setSwAssetLicenceRetrievePop.close();
		}
	});
	
	setSwAssetLicenceRetrievePop = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		title: that['title'],  
		height: that['height'], 
		width: that['width'],  
		autoDestroy: false,
		resizable:false,
		buttonAlign: that['buttonAlign'],
		buttons: [retrieveBtn, closeButton],
		bodyStyle: that['bodyStyle'],
		items: [swAssetLicenceAssignGrid]
	});
	
	return setSwAssetLicenceRetrievePop;

}


function openSelSwAssetLicenceAssignedPop(rowData, actionType) {
	var swConfId = rowData["CONF_ID"];
	var swConfNm = rowData["CONF_NM"];
	var licenceType = rowData["LICENCE_TYPE"];
	var swGroupId = rowData["SW_GROUP_ID"];
	var licenceApplyBase = rowData["LICENCE_APPLY_BASE"];
	
	var prefix = 'grid.itam.sam.assignedSw';
	if( actionType == "EDIT" ){
		prefix = 'grid.itam.sam.assignedSwEdit';
	}
	
	var setPopUpProp = {
		id: 'swAssetLicenceAssignedPop',
		popUpText: "소프트웨어 라이선스",
		title: "[" + rowData["CONF_NM"] +  "] 라이선스",
		prefix: prefix,
		height: 600,
		width: 1100,
		url: '/itg/itam/sam/selectLicenceAssignedSwList.do',
		buttonAlign: 'center',
		bodyStyle: 'background-color: white;',
		actionType: actionType,
		amSwSelData: rowData
	}
	var popUp = createSwAssetLicenceAssignedPop(setPopUpProp);
	popUp.show();
}

function createSwAssetLicenceAssignedPop(setPopUpProp){	
	var that 		= convertDomToArray(setPopUpProp);
	var setSwAssetLicenceAssignedPop = null;
	
	// validation Check 를 위한...
	var customFieldProp = {};
	customFieldProp["ASSIGN_LICENCE_CNT"] = { xtype: "numberfield", name: 'ASSIGN_LICENCE_CNT', minValue: 1, allowDecimals: true };
	customFieldProp["APPLY_LICENCE_FACTOR"] = {xtype: "numberfield", name: 'APPLY_LICENCE_FACTOR', minValue: 0.25, maxValue: 1, allowDecimals: true, decimalPrecision: 2, step: 0.25 };
	
	var amSwSelData = that["amSwSelData"];
	var swConfId = amSwSelData["CONF_ID"];
	var swConfNm = amSwSelData["CONF_NM"];
	var licenceType = amSwSelData["LICENCE_TYPE"];
	var swGroupId = amSwSelData["SW_GROUP_ID"];
	var licenceApplyBase = amSwSelData["LICENCE_APPLY_BASE"];
	var licenceTypeNm = amSwSelData["FN_LICENCE_TYPE_NM"];
	var licenceApplyBaseNm = amSwSelData["FN_LICENCE_APPLY_BASE_NM"];
	var swGroupOptionId = amSwSelData["SW_GROUP_OPTION_ID"];
	var baseLicenceOptionYn = amSwSelData["BASE_LICENCE_OPTION_YN"];
	
	var popupRightTitle = "";
	popupRightTitle += "<font color='red' style='float:right'>&nbsp;" + licenceApplyBaseNm + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font>";
	popupRightTitle += "<font color='blue' style='float:right'>&nbsp;| " + licenceTypeNm + " |&nbsp;</font>";
	popupRightTitle += "<font color='green' style='float:right'>";
	popupRightTitle += amSwSelData["CONF_NM"];
	popupRightTitle += "</font>";
	
	var assignSwListBtn = new Ext.button.Button({
		text: "엑셀다운로드",
		icon: '/itg/base/images/ext-js/common/icons/excel-icon.png',
		handler: function() {
			
			if(!confirm("다운로드 하시겠습니까?")){
				return;
			}
			
			var paramMap = {};
			var excelParam = {};
			
			paramMap['conf_id'] = swConfId;
			paramMap['sw_group_id'] = swGroupId;
			paramMap['licence_type'] = licenceType;
			
			var excelSwAssignAttrs = {
				sheet: [
					{ 
					  sheetName		: "할당 소프트웨어 목록", 
					  titleName		: "할당 소프트웨어 목록",
					  includeColumns: "RNUM,AM_SL_CONF_NM,FN_AM_SV_MODEL_NM,ASSIGN_AM_SL_CPU_CNT,ASSIGN_AM_SL_CPU_CORE_CNT,AM_SL_CPU_CLOCK,ASSIGN_SW_EDITION,ASSIGN_SW_VERSION,ASSIGN_SW_INST_PATH,APPLY_LICENCE_FACTOR,ASSIGN_LICENCE_CNT",
					  headerNames	: "NO.,설치서버명,모델,CPU 수,CPU Core 수,CPU Clock(MHz),소프트웨어 Edition,소프트웨어 버전,소프트웨어 설치경로,적용팩터,할당 라이선스 수"
					}
					
				]
			};
			
			excelParam['fileName'] = "할당SW목록";
			excelParam['excelAttrs'] = excelSwAssignAttrs;
			goExcelDownLoad("/itg/itam/sam/searchLicenceAssignedGridExcelDown.do", paramMap, excelParam);
			
		}
	});
	
	// 할당 소프트웨어 목록
	var swAssetLicenceAssignedGridProp = {	
		id: 'swAssetLicenceAssignedGrid',
		title : "할당 소프트웨어 목록" + popupRightTitle,
		gridHeight: 530,
		resource_prefix: that['prefix'],				
		url: that['url'],
		params: { conf_id : swConfId },
		pagingBar : false,
		cellEditing: ( that["actionType"] == "EDIT" ) ? true : false,
		editorMode: ( that["actionType"] == "EDIT" ) ? true : false,  
		customFieldProp: customFieldProp,
		loadTitleCount: true,
		toolBarComp: [assignSwListBtn],
		autoLoad: true
	};
	var swAssetLicenceAssignedGrid = createGridComp(swAssetLicenceAssignedGridProp);
	
	var acceptBtn;
	if( that["actionType"] == "EDIT" ){
		var licenceType = licenceType;
		var licenceApplyBase = licenceApplyBase;
		
		swAssetLicenceAssignedGrid.on('beforeedit',function(editor ,e){
			var rawData = e.record.raw;
			if( rawData["AM_SL_SPEC_CHANGE"] != "NONE" ){
				alert("서버 스펙변경이 감지되었습니다. 서버 스펙변경 라이선스 관리에서 수정 후 할당 라이선스를 변경 하실 수 있습니다.");
				return false;
			}else{
				var field = e.field;
				if( field == "ASSIGN_LICENCE_CNT" ){
					if( licenceType == "COUNT" || licenceType == "SITE" || licenceType == "ULA" ){
				    	// 카운트, 사이트, ULA라이선스 일 경우에는 할당 라이선스 수를 변경 할 수 없다.
				        return false;//this will disable the cell editing
				    } 
				}
			}
			return true;
		});
		
		swAssetLicenceAssignedGrid.on('edit', function(editor, e) {
			var field = e.field;
			var changeValue = e.value;
			if( field == "APPLY_LICENCE_FACTOR" ){
				if( licenceApplyBase == "BASE_CPU_CNT"){
					e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["AM_SL_CPU_CNT"] * changeValue);
				}else if( licenceApplyBase == "BASE_CPU_CORE_CNT"){
					e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["AM_SL_CPU_CORE_CNT"] * changeValue);
				}else{
					e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["ASSIGN_LICENCE_CNT"] * changeValue);
				}
			}
			/* 직접 할당할 경우에는 수정하지 않는다.
			if( field == "ASSIGN_LICENCE_CNT" ){
				if( licenceApplyBase == "BASE_CPU_CNT"){
					e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["AM_SL_CPU_CNT"] * e.record.raw["APPLY_LICENCE_FACTOR"] * changeValue);
				}else if( licenceApplyBase == "BASE_CPU_CORE_CNT"){
					e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["AM_SL_CPU_CORE_CNT"] * e.record.raw["APPLY_LICENCE_FACTOR"] * changeValue);
				}else{
					e.record.set('ASSIGN_LICENCE_CNT', e.record.raw["APPLY_LICENCE_FACTOR"] * changeValue);
				}
			}
			*/
		});
		
		
		// 등록버튼 생성
		acceptBtn = new Ext.button.Button({
			text: '변경',
			ui:'correct',
			handler: function() {
				// 마스크를 띄웁니다.
				setComponentMask("swAssetLicenceAssignedPop", true);
				
				var assigned_data_list = swAssetLicenceAssignedGrid.getGridData();
				var amSlSpecChangeCnt = 0;
				for( var i = assigned_data_list.length - 1; i >= 0; i-- ){
					var recordData = assigned_data_list[i];
					if( recordData["am_sl_spec_change"] != "NONE" ){
						assigned_data_list.splice(i, 1);
						amSlSpecChangeCnt++;
					}
					
				}
				
				var confirmMsg = "[" + swConfNm +  "] 자산에 할당된 라이선스를 변경 하시겠습니까?";
				if( swGroupOptionId != null && baseLicenceOptionYn == "Y" ){
					if( amSlSpecChangeCnt > 0 ){
						confirmMsg = "라이선스 기준 옵션 [" + swConfNm +  "] 자산이고 서버 스펙 변경 " + amSlSpecChangeCnt + " 건이 감지되었습니다. 연관된 옵션 라이선스도 함께 변경됩니다. 서버 스펙 변경대상을 제외한 설치 소프트웨어의 할당된 라이선스를 변경 하시겠습니까?";
					}else{
						confirmMsg = "라이선스 기준 옵션 [" + swConfNm +  "] 자산입니다. 연관된 옵션 라이선스도 함께 변경됩니다. 할당된 라이선스를 변경 하시겠습니까?";
					}
				}else{
					if( amSlSpecChangeCnt > 0 ){
						confirmMsg = "[" + swConfNm +  "] 자산에 서버 스펙 변경 " + amSlSpecChangeCnt + " 건이 감지되었습니다. 서버 스펙 변경대상을 제외한 설치 소프트웨어의 할당된 라이선스를 변경 하시겠습니까?";
					}
				}
				
				if(!confirm( confirmMsg )){
					// 마스크를 숨깁니다.
					setComponentMask("swAssetLicenceAssignedPop", false);
					return;
				}
				
				jq.ajax({ 
					type:"POST", 
					url: getConstValue('CONTEXT') + '/itg/itam/sam/updateAmSwLicenceAssigned.do', 
					contentType: "application/json", 
					dataType: "json", 
					async: false, 
					data : getArrayToJson({assigned_data_list : assigned_data_list, sw_group_option_id : swGroupOptionId, base_licence_option_yn : baseLicenceOptionYn}), 
					success:function(data){
						if( data.success ){
							alert(data.resultMsg);
							setComponentMask("swAssetLicenceAssignedPop", false);
							searchSwManagerInfoList();
							setSwAssetLicenceAssignedPop.close();
						}else{
							alert(data.resultMsg);
							setComponentMask("swAssetLicenceAssignedPop", false);
						}
					}
				});
					
			}
		});
		
	}
	
	
	//닫기버튼 생성
	var closeButton = new Ext.button.Button({
		text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
		handler: function() {
			setSwAssetLicenceAssignedPop.close();
		}
	});
	
	setSwAssetLicenceAssignedPop = Ext.create('nkia.custom.windowPop',{
		id: that['id'],
		title: that['title'],  
		height: that['height'], 
		width: that['width'],  
		autoDestroy: false,
		resizable:false,
		buttonAlign: that['buttonAlign'],
		buttons: ( that["actionType"] == "EDIT") ? [acceptBtn, closeButton] : [closeButton],
		bodyStyle: that['bodyStyle'],
		items: [swAssetLicenceAssignedGrid]
	});
	
	return setSwAssetLicenceAssignedPop;

}


