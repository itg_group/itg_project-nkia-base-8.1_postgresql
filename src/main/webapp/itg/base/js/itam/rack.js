function rackInfoPop(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var selectRackPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {

		var setGridProp = {	
				id: 'rackListGrid',
				title: getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00004' }),
				gridHeight: getConstValue('SHORTGrid_HEIGHT'),
				resource_prefix: 'grid.itam.locationManage.rackInfo',
				url: '/itg/itam/locationManage/searchRackPop.do',
				params: that['params'],
				pagingBar : true,
				pageSize : 10
		};

		var rackSearchGrid = createGridComp(setGridProp);
		
		var rackSearchBtn   = createBtnComp({visible: true, label:getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:that['id']+'SearchBtn', ui:'correct'});
		attachBtnEvent(rackSearchBtn, actionLink,  'searchAttr');
		
		var setSearchFormProp = {
				id: that['id']+'SearchForm',
				formBtns: [rackSearchBtn],			
				columnSize: 2,
				tableProps : [
					             {colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00001' }), width: 250, name:'model_nm'})}
					            ,{colspan:1, item : createTextFieldComp({label:getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00008' }), width: 250, name:'rack_type'})}
				             ]
		}

		var rackSearchForm = createSeachFormComp(setSearchFormProp);


		var panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			//flex:1 ,
			items: [rackSearchForm,rackSearchGrid]
		});

		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			items: [panel]
		});

		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
			handler: function() {
				dbSelectEvent();	
			}
		});
		
		/*** Private Function Area Start *********************/		
		
		function actionLink(command) {

			switch(command) {		
				case 'searchAttr' :
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
			
					rackSearchGrid.searchList(paramMap);
				break;
			
			}
		}
		
		function dbSelectEvent(){
			var selectedRecords = rackSearchGrid.view.selModel.getSelection();
			if(selectedRecords.length <1){
				showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.rack.00008' }));
			}else{
				
				if(that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
				} else {
					var param = {};
					param['rack_plan_code'] = that['rackPlanCode'];
					param['rack_code'] = selectedRecords[0].raw.RACK_CODE;
					param['mount_cnt'] = selectedRecords[0].raw.MOUNT_CNT;
					param['rack_plan_type'] = 'rack';
/*					
					if(!confirm("가로와 세로를 변경하여 등록하시겠습니까?") == true){
						param['width_size'] = selectedRecords[0].raw.WIDTH_SIZE;
						param['height_size'] = selectedRecords[0].raw.HEIGHT_SIZE;
					}else{
						param['width_size'] = selectedRecords[0].raw.HEIGHT_SIZE;
						param['height_size'] = selectedRecords[0].raw.WIDTH_SIZE;	
					}*/
					
					param['width_size'] = selectedRecords[0].raw.WIDTH_SIZE;
					param['height_size'] = selectedRecords[0].raw.HEIGHT_SIZE;
					
					jq.ajax({type:"POST"
						, url: '/itg/itam/locationManage/updateType.do'
						, contentType: "application/json"
						, dataType: "json" 
						, data : getArrayToJson(param)
						, success:function(data){
							showMessage(data.resultMsg);					
							reLoad();
						}
					});
					selectRackPop.close();
				}
			}
		}		
		
		/*********************** Private Function Area End ***/
		attachCustomEvent('beforeitemdblclick', rackSearchGrid, dbSelectEvent);		
		
		var windowProp = {
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'],
			width: that['width'],
			layout:{
				type: 'border'
			},
			buttons: [acceptBtn],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		};
		selectRackPop = createWindowComp(windowProp);
		popUp_comp[that['id']] = selectRackPop;
		
	} else {
		selectRackPop = popUp_comp[that['id']];
	}
	
	return selectRackPop;
}

//랙 자산등록
function createSetAssetPop(setPopUpProp){	
	var storeParam = {};
	var that = this;
	var setAssetPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		// 조회 그리드
		var setSearchGridProp = {	
				id: that['id']+'Grid',								// Grid id
				title: getConstText({ isArgs: true, m_key: 'res.label.itam.lifecycle.req.00012' }),
    			gridHeight: 210,	
				resource_prefix: 'grid.itam.locationManage.rackAsset.floorAsset',			// Resource Prefix
				//url : '/itg/itam/oper/searchAssetList.do',
				url : '/itg/itam/locationManage/searchAssetList.do',
				pagingBar : true,
				selModel: true,
				pageSize : 10,
				border : true
		};
		var assetSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
				id: 'selAssetGrid',
				title : getConstText({ isArgs: true, m_key: 'res.00020' }),
				gridHeight: 180,
				resource_prefix: 'grid.itam.locationManage.rackAsset.setFloorAsset',				
				params: null,		
				pagingBar : false,
				autoLoad: false,
				dragPlug: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
				actionBtnEvent: actionBtnEvent,
				isMemoryStore: true
		};
		var assetSaveListGrid = createGridComp(setSaveGridProp);
		
		var setTreeProp = {				
				id : that['id']+'Tree',
				url : '/itg/itam/amdb/searchAmClassAllTree.do',
				title : getConstText({ isArgs: true, m_key: 'res.title.itam.class' }),
				dynamicFlag : false,
				searchable : true,
				searchableTextWidth : 60,
				expandAllBtn : true,
				collapseAllBtn : true,
				rnode_text : getConstText({ isArgs: true, m_key: 'res.00003' }),
				expandLevel: 2
		}
		var staticClassTree = createTreeComponent(setTreeProp);	
		// 높이 길이 체크 무시
		staticClassTree['syncSkip'] = true;
		attachCustomEvent('select', staticClassTree, treeNodeClick);

		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.choice' }),
			handler: function() {
				checkGridRowCopyToGrid(that['id']+'Grid', 'selAssetGrid', { primaryKeys: ['ASSET_ID'] });
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var operAssetSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				var searchForm = Ext.getCmp(that['id']+'SearchForm');
				var paramMap = searchForm.getInputData();
				Ext.getCmp(that['id']+'Grid').searchList(paramMap);	
			}
		});
		
		var operAssetSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp(that['id']+'SearchForm').initData();
			}
		});
		
		var setAssetSearchFormProp = {
			id: that['id']+'SearchForm',
			formBtns: [operAssetSearchBtn, operAssetSearchInitBtn],
			columnSize: 3,
			tableProps : [
						  {colspan:1, tdHeight: 28, item : createCodeComboBoxComp({ label: getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_OPER_ASSET', width: 200, padding: getConstValue('WITH_FIELD_PADDING')})}
						 ,{colspan:1, tdHeight: 28, item : createTextFieldComp({name:'search_value',width: 300, padding: getConstValue('WITH_FIELD_PADDING')})}
						 ,{colspan:1, tdHeight: 28, item : createHiddenFieldComp({name:'class_id', padding: getConstValue('WITH_FIELD_PADDING')})}
				         ]
		}
		var searchAssetForm = createSeachFormComp(setAssetSearchFormProp);
		
		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: 515,
			autoScroll: true,
			items: [staticClassTree]
		});
		
		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			flex:1 ,
			items: [searchAssetForm, assetSearchListGrid, choiceBtnpanel, assetSaveListGrid]
		});
		
		var complexProp = {
				height : '100%',
				panelItems : [{
					flex: 1,
					items : left_panel
				},{
					flex: 3,
					items : right_panel
				}]
		};
		var complexPanel = createHorizonPanel(complexProp);		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			scale:'medium',
			ui:'correct',
			handler: function() {
				dbClickEvent();
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale:'medium',
			handler: function() {
				setAssetPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		function actionBtnEvent(grid, rowIndex, colIndex){
			grid.getStore().removeAt(rowIndex); 
		}
		
		function InsertfloorCntPop(){
			var textBoxLabel = createDisplayTextFieldComp({value: getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00034' }), width: 60, height: 30});
			//var setTextbox = createTextFieldComp({id:'floorCnt', padding: '7 0 0 3', name:'floorCnt', width: 50, vType:'number', notNull:true});
			var setTextbox = createNumberFieldComp({id:'floorCnt', name:'floorCnt', vtype: 'number', minValue:1, notNull:true, width:50});
			var submitBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'res.common.insert' }), ui:'correct'});
			
			var setRackForm = Ext.create('Ext.panel.Panel', {
				id: 'setRackForm',
				width: 120,
				height: 60,
				border: true,
				layout: {
				 	type: 'table',
				 	columns: 2, 
			    	tableAttrs: {
			    		style: {
			    			width:'100%'
			    		}
			    	},
			    	trAttrs:{
			            width:'100%'
			        }
				},
			    items: [textBoxLabel, setTextbox]
			});	
			
			submitBtn.on('click', function(){ 
				if(Ext.getCmp('floorCnt').getValue() == null || Ext.getCmp('floorCnt').getValue() == ""){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.rack.00009' }));
					return false;
				}
		
				var selectedRecords = assetSearchListGrid.view.selModel.getSelection();
				var param = {};
				param['rack_plan_code'] = that['rackPlanCode'];
				param['rack_floor'] = that['rackFloor'];
				param['floor_cnt'] = Ext.getCmp('floorCnt').getValue();
				param['orderByStyle'] = true;
				jq.ajax({ type:"POST"
					, url:"/itg/itam/locationManage/searchRackPlanInfo.do"
					, contentType: "application/json"
					, dataType: "json"
					, async: false
					, data : getArrayToJson(param)
					, success:function(data){
						var fCnt = that['rackFloor'];
						for(var i=1; i <= Ext.getCmp('floorCnt').getValue(); i++){
							fCnt = fCnt - 1;
							if(fCnt < 0 ){
								showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.rack.00010' }));
								return false;
							}
/*							if(data.gridVO.rows[fCnt].ASSET_ID != null){
								showMessage("선택한 랙의 위치에 이미 자산이 등록되어 있습니다.");
								Ext.getCmp('floorCnt').setValue();
								return false;
							}*/
						}

						var paramMap = {};
						var items = getGrid('selAssetGrid').getStore().data.items;
						
						paramMap['0'] = param;
						for(var i=1; i<=items.length; i++){
/*							//키값이 대문자로 들어오기 때문에 소문자로 변환
							for(var key in items[i-1].data){
								map = items[i-1].data;
								map[key.toLowerCase()] = map[key];
								delete map[key]; //대문자 key removeAll
							}*/
							var map = {};
							map['asset_id'] = items[i-1].data.ASSET_ID;
							paramMap[i] = map;
						}

						jq.ajax({type:"POST"
							, url: '/itg/itam/locationManage/updateRackFloorInfo.do'
							, contentType: "application/json"
							, dataType: "json" 
							, data : getArrayToJson(paramMap)
							, success:function(data){
								var floorGrid = getGrid("rackMountGrid").getStore();
								floorGrid.proxy.jsonData={rack_plan_code:that['rackPlanCode']};
								floorGrid.load();
								var assetListGrid = getGrid("rackAssetList_grid").getStore();
								assetListGrid.proxy.jsonData={rack_plan_code:that['rackPlanCode'], rack_floor:that['rackFloor']};
								assetListGrid.load();
								inFloorCountPop.close();
								setAssetPop.close();
							}
						});
					}
				});			
			});
			
			var windowProp = {	
				id: 'selectRackTypePop',
				title: getConstText({ isArgs: true, m_key: 'res.label.itam.rack.00030' }),
				width: 130,
				height: 100, 
				buttons: [submitBtn],
				items: [setRackForm],
				closeBtn: false
			}
			
			var inFloorCountPop = createWindowComp(windowProp);
			inFloorCountPop.show();
		}
		function treeNodeClick( tree, record, index, eOpts ) {
			var param = {};
			var up_class_id = record.raw.up_node_id;
			var class_id = record.raw.node_id;
			var theForm = Ext.getCmp(that['id']+'SearchForm');
			theForm.setFieldValue('class_id', class_id);
			
			var getOrderGrid = getGrid(that['id']+'Grid').getStore();
			getOrderGrid.proxy.jsonData={class_id: class_id};
			getOrderGrid.load();
		}

		function dbClickEvent(){
			if(getGrid('selAssetGrid').getStore().data.items <1){
				showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.rack.00011' }));
			}else{
				InsertfloorCntPop(); // 자산등록 층수받는 팝업생성
			}
		}		
		
		/*********************** Private Function Area End ***/
		
		setAssetPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['popUpText'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setAssetPop;
		
	} else {
		setAssetPop = popUp_comp[that['id']];
	}
	
	return setAssetPop;

}