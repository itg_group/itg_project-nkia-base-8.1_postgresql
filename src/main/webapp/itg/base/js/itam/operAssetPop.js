var popUp_comp={};

/**
 *  이중화 조회 팝업 
 *  
 * @method {} createHaGroupSearchPop : 이중화 조회 팝업 Function
 * @param {} setPopUpProp   
 * @property 1{*} popupId	    : 팝업 ID 	
 * @property 2{*} popupWidth 	: 팝업 가로 폭
 * @property 2{*} popupHeight 	: 팝업 세로 폭
 * @property 2{*} sourcePrefix 	: 이중화 그룹 그리드 prefix	
 * @property 2{*} targetPrefix 	: 선택된 이중화 그룹 그리드 prefix
 *    	
 */
function createHaGroupSearchPop(setPopUpProp){
	var that = {};
	var haGroupSearchPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	var choiceBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.choice' })});
	attachBtnEvent(choiceBtn, actionLink,  'choiceBtn');
	
	var searchFormId = 'searchHaGroupForm';	// searchFormId
    var searchHaGroupForm = getAssetSearchForm(searchFormId, 'searchHaBtn', 'searchHaGroup', 2, searchBtnEvent);
    											
    // 이중화 그룹 그리드
    var haGroupGridProp = {	
    	id: 'haGroupGrid',	
    	gridHeight: 200,
    	resource_prefix: that['sourcePrefix'],				
    	url: '/itg/itam/oper/searchPopHaList.do',	
    	params: null,								
    	selModel: true,
    	autoLoad: false
    }
    var haGroupGrid = createGridComp(haGroupGridProp);
    
    Ext.getCmp('haGroupGrid').searchList({conf_type: that['classType'], conf_id: that['classType']});
    
    // 이중화 그룹에 속한 구성자원 그리드 
    var haAssetGridProp = {	
    	id: 'haAssetGrid',	
    	gridHeight: 200,
    	resource_prefix: 'grid.itam.ha.asset',				
    	url: '/itg/itam/oper/searchPopHaRelAssetList.do',	
    	params: null,								
    	autoLoad: true
    }
    var haAssetGrid = createGridComp(haAssetGridProp);
    
    // 그리드 병합
	var selectGrid = Ext.create('Ext.panel.Panel',{
		border: false,
		width : '100%',
		layout: {
			type : 'hbox',
      		pack : 'center',
          	align : 'top'
		},
		items: [{
					flex: 1,
					items: haGroupGrid
				},{
	    			flex: 1,
	    			items: haAssetGrid
    			}]
	});
	var setHaGroupGrid = getAssetGridByMemoryVariability('setHaGroupGridProp', that['targetPrefix']);
	
	// 버튼 패널
	var btnPanel = Ext.create('Ext.panel.Panel',{
		border: false,
		layout: { 
      		type : 'vbox',
          	align : 'center'
      	}, 
		items: [choiceBtn]
	});
	
	// 패널 병합
	var complexPanel = Ext.create('Ext.panel.Panel',{
		border: false,
		layout:{
			type: 'vbox',
			align: 'stretch'
		},
		items: [{ items: searchHaGroupForm },
		        { flex: 1, items: selectGrid },
		        { items: btnPanel },
		        { flex: 1, items: setHaGroupGrid}]
	});
	
	// 적용버튼
	var applyBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.apply' }), id: "applyBtn", ui:'correct', scale:'medium'});
	attachBtnEvent(applyBtn, actionLink,  'applyBtn');
	
	var windowProp = {	
		id: that['popupId'],
		title: getConstText({ isArgs: true, m_key: 'res.00019' }),
		width: that['popupWidth'],
		height: that['popupHeight'], 
		buttons: [applyBtn],
		items: [complexPanel],
		closeBtn: true
	}
	haGroupSearchPop = createWindowComp(windowProp);
	
	attachCustomEvent('itemclick', haGroupGrid, gridItemClick);
	
	/**
	 * 검색 폼에서 text에서 엔터키 칠때 호출 
	 */
	function searchBtnEvent(){
		actionLink('haSearch');
	}
	
	/*** 이중화 그리드 클릭이벤트 정의 *********************/
	function gridItemClick( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e){
		var ha_grp_id = tdEl.data.HA_GRP_ID;
		// 이중화그룹에 연계되어진 구성자원 조회
		Ext.getCmp('haAssetGrid').searchList({ha_grp_id: ha_grp_id});		
	}
	 
	// Action에 따른 Logic 수행
	function actionLink(command) {
		switch(command) {
			case 'haSearch':	
				var searchForm = Ext.getCmp(searchFormId);
				var paramMap = searchForm.getInputData();
				Ext.getCmp('haGroupGrid').searchList(paramMap);	
			break;
			case 'choiceBtn':	
				checkGridRowCopyToGrid('haGroupGrid', 'setHaGroupGridProp', { primaryKeys: [that['key']] });
			break;
			case 'applyBtn':	
				gridAllRowCopyToGrid( 'setHaGroupGridProp', that['opernerGrid'], true, null, { primaryKeys: [that['key']] });

				haGroupSearchPop.close();
			break;
		}
	}
	
	return haGroupSearchPop;
}

/**
 * 맵이들어있는 배열형태에서 맵속성들을 하나의 통합맵객체로 합치는 기능
 *  
 * @method {} mapAddAction : 자산수정기능 팝업 
 * @param {} setPopUpProp   
 * @property 1{*} popupId	    : 팝업 ID 	
 *    	
 */
function mapAddAction(mapArray){
	var totalMap = {};
	var singleMap;
	
	for(var i=0; i<mapArray.length; i++){
		singleMap = mapArray[i];
	
		for( var prop in singleMap ){
			totalMap[prop] = singleMap[prop];
		}
	}
	return totalMap;
}


/**
 *  자산수정기능 팝업 
 *  
 * @method {} createOperAssetPop : 자산수정기능 팝업 
 * @param {} setPopUpProp   
 * @property 1{*} popupId	    : 팝업 ID 	
 *    	
 */
function createOperAssetPop(setPopUpProp){
	
	var that = {};
	var operAssetPopBody = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['popId']] == null) {
		var assetNote = createBasicAssetTab(that['assetId'], that['assetType'], that['confId'], that['editMode'], 'OPER');
		
		var btnArray = [];
		
		if(that['editMode'] == true || that['editMode'] == undefined){
			var basicInfoUpdateBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.insert' }), ui:'correct', scale:'medium'});
			attachBtnEvent(basicInfoUpdateBtn, basicActionLink,  'basicInfoUpdateBtn');
			
			btnArray.push(basicInfoUpdateBtn);
		}else{
			btnArray = null;
		}
		
		// @@20130719 전민수 START
		// 1.팝업의 width변경(900->950)
		var operAssetPopBody = Ext.create('nkia.custom.windowPop',{
			id: that['popId'],
			title: that['popUpTitle'],  
			height: 750, 
			width: 950,  
			autoDestroy: true,
			resizable:false, 
			layout: 'fit',
			buttonAlign: 'center',
			buttons: btnArray,
			bodyStyle: 'background-color: white; ',
			items: [assetNote],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['popId']] = null
	 			},
	 			hide:function(p) {

	 			}
	 		}
		});
		// @@20130719 전민수 END
		
		popUp_comp[that['popId']] = operAssetPopBody;
		/**
		 * 자산의 기본정보부분(일반정보, 스펙정보, 유지보수정보, 이력정보, 첨부파일정보) 버튼 이벤트 정의
		 */
		function basicActionLink(command) {
			var theForm = Ext.getCmp('setGeneralInfoProp');
			var theSpecForm = Ext.getCmp('setSpecInfoProp');
			
			switch(command) {
				case 'basicInfoUpdateBtn' :
					if(theForm.isValid() == true && theSpecForm.isValid() == true){
						
						var specMap = Ext.getCmp('setSpecInfoProp').getInputData();
						
						var assetMap = Ext.getCmp('setAssetInfoProp').getInputData();
						var maintMap = Ext.getCmp('setMaintInfoProp').getInputData();
						
						var amInfraTotalMap = [];
						amInfraTotalMap.push(Ext.getCmp('setGeneralInfoProp').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwBasicForm').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwHostIpForm').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwPowerForm').getInputData());
						amInfraTotalMap.push(Ext.getCmp('hwDiscardForm').getInputData());
						var infraTotalMap = mapAddAction(amInfraTotalMap);
						
						var supplyMap = Ext.getCmp('supplyAssetInfoForm').getInputData();
						
						var logicBasicInfoMap = Ext.getCmp('logicBasicInfoNoteForm').getInputData();
						var logicSpecInfoMap = Ext.getCmp('logicSpecInfoNoteForm').getInputData();
						
						var fileMap = Ext.getCmp('ci_atch_file_id_grid').getInputData();
		
						var operUserMap = dynamicGridUpdateEvent('createOperUserGrid', 'conf_id', that['confId'], 'operUserStore', 'asset_id', that['assetId']);
						var cpuInfoMap = dynamicGridUpdateEvent('operAssetCpuProp', 'conf_id', that['confId'], 'cpuStore', 'asset_id', that['assetId']);
						var memInfoMap = dynamicGridUpdateEvent('operAssetMemProp', 'conf_id', that['confId'], 'memStore', 'asset_id', that['assetId']);
						var ipInfoMap = dynamicGridUpdateEvent('operAssetIpProp', 'conf_id', that['confId'], 'ipStore', 'asset_id', that['assetId']);
						var innerDiskMap = dynamicGridUpdateEvent('operAssetInnerDiskProp', 'conf_id', that['confId'], 'innerDiskGridStore', 'asset_id', that['assetId']);
						var relSvAsset = dynamicRelGridEditEvent('operAssetSvProp','rel_conf_id', that['confId'], 'svStore', '/itg/itam/oper/insertRelInfo.do', 'SV');
						var relNwAsset = dynamicRelGridEditEvent('operAssetNwProp','rel_conf_id', that['confId'], 'nwStore', '/itg/itam/oper/insertRelInfo.do', 'NW');
						var relScAsset = dynamicRelGridEditEvent('operAssetScProp','rel_conf_id', that['confId'], 'scStore', '/itg/itam/oper/insertRelInfo.do', 'SC');
						
						// @@20130726 전민수 START
						// 5.서비스연계에 대한 로직이 없었으므로 신규 추가함
						var relServiceAsset = dynamicRelGridEditEvent('operAssetServicePropGrid','conf_id', that['confId'], 'serviceRelStore','/itg/itam/oper/insertserviceRelInfo.do', 'SE');
						// @@20130726 전민수 END
						
						// @@20130719 전민수 START
						// 4.자산원장에서 인프라연계 논리서버에 대한 삭제로직이 없었으므로 신규 추가함 
						var relSvlAsset = dynamicRelGridEditEvent('operAssetServerLogicProp','rel_conf_id', that['confId'], 'svlStore', '/itg/itam/oper/insertRelInfo.do', 'SVL');
						var svlArray = relSvlAsset['svlStore'];
						var confIdArray = [];
						for(var t=0; t<svlArray.length; t++){
							confIdArray.push("'"+svlArray[t].conf_id+"'"); 
						}
						var svlConfId = confIdArray.join();

						relSvlAsset['svlConfId'] = svlConfId;
						// @@20130719 전민수 END
						
						// @@20130719 전민수 START
						// 5.자산원장에서 서비스연계 논리서버에 대한 삭제로직이 없었으므로 신규 추가함 
						// @@20130719 전민수 END
						var relStAsset = dynamicRelGridEditEvent('operAssetStProp','rel_conf_id', that['confId'], 'stStore', '/itg/itam/oper/insertRelInfo.do', 'ST');
						var relBkAsset = dynamicRelGridEditEvent('operAssetBackUpProp', 'rel_conf_id', that['confId'], 'bkStore', '/itg/itam/oper/insertRelInfo.do', 'BK');
						var relSwAsset = dynamicRelGridEditEvent('operAssetSoftWareStProp','rel_conf_id', that['confId'], 'swStore','/itg/itam/oper/insertRelInfo.do', 'SW');
						var relHaAsset = dynamicRelGridEditEvent('operAssetHAProp','conf_id', that['confId'], 'haStore','/itg/itam/oper/insertHaInfo.do', 'HA');
						
						var UpdateDataMap = {};
						UpdateDataMap['specMap'] = specMap;
						UpdateDataMap['assetMap'] = assetMap;
						UpdateDataMap['maintMap'] = maintMap;
						UpdateDataMap['infraTotalMap'] = infraTotalMap;
						
						UpdateDataMap['supplyMap'] = supplyMap;
						UpdateDataMap['logicBasicInfoMap'] = logicBasicInfoMap;
						UpdateDataMap['logicSpecInfoMap'] = logicSpecInfoMap;
						
						// @@20130719 전민수 START
						// 2.첨부파일기능 추가
						UpdateDataMap['fileMap'] = fileMap;
						// @@20130719 전민수 END
						
						UpdateDataMap['operUserMap'] = operUserMap;
						UpdateDataMap['cpuMap'] = cpuInfoMap;
						UpdateDataMap['memMap'] = memInfoMap;
						UpdateDataMap['ipMap'] = ipInfoMap;
						UpdateDataMap['innerDiskMap'] = innerDiskMap;
						
						UpdateDataMap['relSvAssetMap'] = relSvAsset;
						UpdateDataMap['relNwAssetMap'] = relNwAsset;
						UpdateDataMap['relScAssetMap'] = relScAsset;
						// @@20130726 전민수 START
      					// 5.서비스연계에 대한 로직이 없었으므로 신규 추가함
						UpdateDataMap['relServiceAssetMap'] = relServiceAsset;
						// @@20130726 전민수 END
						// @@20130719 전민수 START
						// 4.자산원장에서 인프라연계 논리서버에 대한 삭제로직이 없었으므로 신규 추가함 
						UpdateDataMap['relSvlAssetMap'] = relSvlAsset;
						// @@20130719 전민수 END
						UpdateDataMap['relStAssetMap'] = relStAsset;
						UpdateDataMap['relBkAssetMap'] = relBkAsset;
						UpdateDataMap['relSwAssetMap'] = relSwAsset;
						UpdateDataMap['relHaAssetMap'] = relHaAsset;
						
						var tangible = that['tangible'];

						var url = "/itg/itam/oper/updateAssetBasicInfo.do";
						if(tangible == 'N'){
							url = "/itg/itam/oper/updateServerLogicInfo.do";
						}
						// @@20130719 전민수 START
						// 3.첨부파일기능 신규추가로인한 선처리로 checkFileSave내부함수 호출과 후처리 콜백함수 호출
						var callbackSub = function(){ 
							jq.ajax({ type:"POST"
									, url: url
									, contentType: "application/json"
									, dataType: "json"
									, async: false
									, data : getArrayToJson(UpdateDataMap)
									, success:function(data){
										if( data.success ){
											//getGrid('operAssetList').searchList();
											dynamicRelTableChange(that['assetId'], that['confId']);
											operAssetPopBody.close();
						    				showMessage(data.resultMsg);
						    			}else{
						    				showMessage(data.resultMsg);
						    			}
									}
								});
						}
						
						var isFile = checkFileSave("ci_atch_file_id", fileMap, callbackSub);
						if( !isFile ){
							eval(callbackSub)();			
						}
					}else{
						showMessage(getConstText({ isArgs: true, m_key: 'msg.common.00011' }));
						return false;
					}
				break;
			}
		}
		
	} else {
		operAssetPopBody = popUp_comp[that['popId']];
	}
	
	return operAssetPopBody;
}

/**
 * 공통 연계정보들 수정후 테이블의 DISPLAY 필드 데이터 변경 
 */
function dynamicRelTableChange(assetId, confId){
	var count = getRelCount(assetId, confId);
	
	var dynamicField = Ext.getCmp('relAssetTableForm').query('.field');
	
	for(var i=0; i<dynamicField.length; i++){
		if(dynamicField[i].assetType != undefined){
			if(dynamicField[i].assetType == 'SV'){
				dynamicField[i].setValue(count[0]);
			}else if(dynamicField[i].assetType == 'NW'){
				dynamicField[i].setValue(count[1]);
			}else if(dynamicField[i].assetType == 'SC'){
				dynamicField[i].setValue(count[2]);
			}else if(dynamicField[i].assetType == 'ST'){
				dynamicField[i].setValue(count[4]);
			}else if(dynamicField[i].assetType == 'BK'){
				dynamicField[i].setValue(count[5]);
			}else if(dynamicField[i].assetType == 'SW'){
				dynamicField[i].setValue(count[6]);
			}else if(dynamicField[i].assetType == 'HA'){
				dynamicField[i].setValue(count[7]);
			}
		}
	}
}