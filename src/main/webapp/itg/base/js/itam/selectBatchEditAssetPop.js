/***** 커스텀 필요한 변수 영역 시작 ********************************************************/
function getStdLevel() {
	// 기준레벨 : 분류체계별 속성수정이 가능한 분류체계의 최소레벨
	// 예) 분류체계(1) - 하드웨어(2) - 네트워크(3) - 스위치(4) - L2(5) 일때 서브테이블(AM_NW)의 최소 단위인 (3)레벨 리턴
	return 3;
}
/***** 커스텀 필요한 변수 영역 끝 **********************************************************/



//2014.01.16 정정윤 추가
//2015.06.23 정정윤 수정 (POC버전 적용)
//자산 일괄수정에서, 자산선택 팝업은 최하위 분류에서만 자산이 선택가능할 수 있는 분기가 있으므로 function 새로 추가..
function createSelectBatchEditAssetPop(setPopUpProp){	
	
	var that = this;
	
	var storeParam = {};
	var setAssetPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		
		searchGrid = that['id']+'Grid';
		storeParam = that['params'];
		
		// 조회 그리드
		var setSearchGridProp = {	
			id: that['id']+'Grid',
			//title : getConstText({ isArgs: true, m_key: 'res.title.itam.maint.assetlist' }),
			title : '구성자원 목록',
			gridHeight: 290,
			resource_prefix: that['sPrefix'],				
			url: that['url'],
			params: ( that['params'] ) ? that['params'] : null,
			selModel: true,
			pagingBar : true,
			pageSize : 10,
			autoLoad: false,
			selfScroll: true
		};
		var assetSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅)그리드 초기화 버튼
		var saveGridInitBtn = new Ext.button.Button({
			text: '목록 전체 제거',
			handler: function() {
				getGrid(that['tGridId']).getStore().removeAll();
			}
		});
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
			id: that['tGridId'],
			//title : getConstText({ isArgs: true, m_key: 'res.label.itam.maint.req.00014' }),
			title : '선택된 구성자원 목록',
			gridHeight: 250,
			resource_prefix: that['tPrefix'],				
			params: null,		
			pagingBar : false,
			autoLoad: false,
			dragPlug: true,
			border: true,
			actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
			actionBtnEvent: actionBtnEvent,
			isMemoryStore: true,
			selfScroll: true,
			toolBarComp: [saveGridInitBtn]
		};
		var assetSaveListGrid = createGridComp(setSaveGridProp);
		
		// 트리
		var setTreeProp = {				
			id : 'classTree',
			url : '/itg/itam/statistics/searchClassTree.do',
			title : getConstText({ isArgs: true, m_key: 'res.00016' }),
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : false,
			collapseAllBtn : false,
			params: ( that['tree_params'] ) ? that['tree_params'] : null,
			rnode_text :  getConstText({ isArgs: true, m_key: 'res.00003' }),
			expandLevel: 3
		}
		var staticClassTree = createTreeComponent(setTreeProp);	
		// 높이 길이 체크 무시
		//staticClassTree['syncSkip'] = true;
		attachCustomEvent('select', staticClassTree, treeNodeClick);

		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: "선 택 ▼",
			scale: "medium",
			handler: function() {
				
				var sGridSelModel = Ext.getCmp("setAssetGrid").selModel;
				var sGridSelCnt = sGridSelModel.selected.items.length;
				var tGridData = Ext.getCmp(that['tGridId']).store.data;
				
				if(sGridSelCnt < 1) {
					
					alert("선택된 정보가 없습니다.");
					return;
					
				} else {
					
					// 최대 선택 자산 수 제한
					if((tGridData.items.length + sGridSelCnt) > 100) {
						alert("최대 선택 구성자원의 수는 100개를 초과할 수 없습니다.");
						return;
					}
					
					// 분류체계별 속성 수정일때 동일한 분류만 선택하도록
					if(storeParam['edit_type'] == 'CLASS') {
						
						var treeSeletedItem = Ext.getCmp('classTree').view.selModel.getSelection()[0].raw;
						var selectGridClassType = treeSeletedItem.class_type;
						var selectGridClassNm = treeSeletedItem.text;
						
						if(tGridData.items.length > 0) {
							
							var targetGridClassType = tGridData.items[0].data.CLASS_TYPE;
							var targetGridClassNm = "";
							
							if(targetGridClassType != selectGridClassType) {
								
								var classInfoList = getClassInfo(); //jjy
								
								for(var i=0; i<classInfoList.length; i++) {
									if( classInfoList[i].CLASS_TYPE == targetGridClassType ) {
										targetGridClassNm = classInfoList[i].CLASS_NM;
										break;
									}
								}
								
								alert(
									"기존 선택된 구성자원과 동일한 분류체계의 구성자원을 선택해주세요."
									+ "\n\n"
									+ "　* 현재 선택한 구성자원 : [ " + selectGridClassNm + " ]"
									+ "\n"
									+ "　* 기존 선택된 구성자원 : [ " + targetGridClassNm + " ]"
								);
								
								return;
							}
						}
					}
				}
				
				Ext.suspendLayouts();
				checkGridRowCopyToGrid(that['id']+'Grid', that['tGridId'], { primaryKeys: ['ASSET_ID'] });
				Ext.resumeLayouts(true);
				
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var assetSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				searchBtnEvent();
			}
		});
		
		var assetSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp('assetPopSearchForm').initData();
				//Ext.getCmp('search_type').setValue('ALL');
			}
		});
		
		var editTypeField = createTextFieldComp({id:'editTypeField', name:'editTypeField'});
		editTypeField.setValue(storeParam['edit_type']);
		
		// 검색 폼 (하이닉스용)
		/*
		var vendor_nm = {colspan:1, tdHeight: 30, item : contractComboBox('vendor_id', 'vendor', '제조사', 'vendor_id')};
		var supply_nm = {colspan:1, tdHeight: 30, item : contractComboBox('supply_id', 'vendor', '공급사', 'supply_id')};
		var workId = createTextFieldComp({label:"", name:'work_id', id:'work_id', width:0 });
		var workPopComp = createUnionFieldContainer(
				{id:'work_container'
				,label:"업무분류"
				,width:400
				,items:[
				        createTextFieldComp({ id:'work_nm', name:'work_nm', width:200, hideLabel:true, readOnly:true})
						,createPopBtnComp({	id:'work_container_PopOpBtn', handler:function(){ openSelectWorkPop(Ext.getCmp('work_id'), Ext.getCmp('work_nm')); }})
						,createClearBtnComp({id:'work_container_PopClrBtn', handler:function(){ Ext.getCmp('work_nm').setValue(""); Ext.getCmp('work_id').setValue(""); }})
					]
				}
			);
		
		var region = {colspan:2, item : createCodeComboBoxComp({label:'지역', name:'region', code_grp_id:'REGION', attachAll:true, width: 300})};
		var location = {colspan:1, item : contractComboBox('location', 'location', '위치', 'location' )};
		var asset_auto = {colspan:2, item : createCodeComboBoxComp({label:'사용구분', name:'asset_auto', code_grp_id:'ASSET_AUTO', attachAll:true, width: 300})};
		var asset_use = {colspan:1, item : createCodeComboBoxComp({label:'자산용도', name:'asset_use', code_grp_id:'ASSET_USE', attachAll:true, width: 300})};
		
		var setAssetSearchFormProp = {
			id: 'assetPopSearchForm',
			formBtns: [assetSearchBtn, assetSearchInitBtn],
			columnSize: 3,
			tableProps : [
			              {
							colspan:1, tdHeight: 30, item: createCodeComboBoxComp({label:'조회구분', id:'search_type', name:'search_type', code_grp_id:'SEARCH_ASSET_BATCH', width: 230})
						  }
			             ,{
							colspan:1, tdHeight: 30, item: createTextFieldComp({name:'search_value', width: 160, padding: getConstValue('WITH_FIELD_PADDING')})
						  }
			              ,vendor_nm
			              ,{
			            	  colspan:2, tdHeight: 30, item: workPopComp
			              }
			              ,supply_nm
			              ,region
			      		  ,location
			    		  ,asset_auto
			    		  ,asset_use
						 ],
			hiddenFields : [workId,editTypeField]
		}
		vendor_nm.item.store.proxy.jsonData = {code_type :'vendor', higher_class : 'ROOT'};
		supply_nm.item.store.proxy.jsonData = {code_type :'vendor', higher_class : 'ROOT'};
		*/
		
		/*
		var setAssetSearchFormProp = {
			id: 'assetPopSearchForm',
			formBtns: [assetSearchBtn, assetSearchInitBtn],
			columnSize: 2,
			tableProps : [
			              {
							colspan:1, tdHeight: 30, item: createCodeComboBoxComp({label:'조회구분', id:'search_type', name:'search_type', code_grp_id:'SEARCH_ASSET_BATCH', width: 230})
						  }
			             ,{
							colspan:1, tdHeight: 30, item: createTextFieldComp({name:'search_value', width: 160, padding: getConstValue('WITH_FIELD_PADDING')})
						  }
						 ],
			hiddenFields : []
		}
		*/
		
		var setAssetSearchFormProp = {
			id: 'assetPopSearchForm',
			formBtns: [assetSearchBtn, assetSearchInitBtn],
			columnSize: 2,
			tableProps : [
			             {
							colspan:2, tdHeight: 30, item: createUnionCodeComboBoxTextContainer({label:'조회구분', comboboxName: 'search_type', textFieldName: 'search_value', code_grp_id: 'SEARCH_ASSET_BATCH', widthFactor: 0.5 })
						  }
						 ],
			hiddenFields : []
		}
		
		
		var searchAssetForm = createSeachFormComp(setAssetSearchFormProp);
		
		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: 750,
			autoScroll: true,
			items: [staticClassTree]
		});
		
		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'west',
			height: 750,
			//flex:1 ,
			items: [searchAssetForm, assetSearchListGrid, choiceBtnpanel, assetSaveListGrid]
		});
		
		var complexProp = {
				height : '100%',
				panelItems : [{
					flex: 1,
					items : left_panel
				},{
					flex: 4,
					items : right_panel
				}]
		};
		var complexPanel = createHorizonPanel(complexProp);		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: "다 음",
			ui:'correct',
			scale:'medium',
			handler: function() {
				if(getGrid(that['tGridId']).getStore().data.items <1){
					alert(getConstText({ isArgs: true, m_key: 'msg.itam.maint.req.00037' }));
				}else{
					//setComponentMask(that['id'], true);
					
					getGrid(that['sGridId']).getStore().removeAll();
					var appendable = false;
					Ext.suspendLayouts();
					gridAllRowCopyToGrid( that['tGridId'],  that['sGridId'], appendable, null, { primaryKeys: ['asset_id'] });
					Ext.resumeLayouts(true);
					attrGridDisabled(true);
					Ext.getCmp(that['sGridId']).getSelectionModel().selectAll(true);
					
					//setComponentMask(that['id'], false);
					setAssetPop.close();
				}
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale:'medium',
			handler: function() {
				setAssetPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		
		function actionBtnEvent(grid, rowIndex, colIndex){
			grid.getStore().removeAt(rowIndex); 
		};
		
		function treeNodeClick( tree, record, index, eOpts ) {
			
			var classType = 'ROOT';
			var classId = '-1';
			var level = '1';
			var nodeId = '-1';
			
			level = record.raw.node_level;
			classType = record.raw.class_type;
			classId = record.raw.class_id;
			nodeId = record.raw.node_id;
			
			var leaf = record.raw.leaf;
			
			var params = ( that['params'] ) ? that['params'] : {};
			params['class_id'] = nodeId;
			params['node_level'] = level;
			params['class_type'] = classType;
			params['tangible_asset_yn'] = 'Y';
			
			//var assetGrid = Ext.getCmp(that['id']+'Grid');
			
			if(storeParam['edit_type']=="COMMON") {
				Ext.getCmp(that['id']+'Grid').searchList(params);
			} else {
				if(level >= getStdLevel()) {
					Ext.getCmp(that['id']+'Grid').searchList(params);
				} else {
					Ext.getCmp(that['id']+'Grid').getStore().removeAll(); 
				}
			}
		}
		
		function gridCellClick( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {
			
		}
		
		/*********************** Private Function Area End ***/
		
		setAssetPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setAssetPop;
		
	} else {
		setAssetPop = popUp_comp[that['id']];
	}
	
	return setAssetPop;
}

function contractComboBox(id, codeType, label, name){
	var param				= {};
	param['code_type']		= codeType;
	param['code_group']		= name;

	var contract_proxy	= Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader	: "application/json",
		noCache				: false,
		type				: 'ajax',
		url					: '/itg/itam/statistics/searchCodeDataList.do',
		jsonData			: param,
		actionMethods		: {
								create	: 'POST',
								read	: 'POST',
								update	: 'POST'
		},
		reader				: {
								type	: 'json',
								root	: 'gridVO.rows'
		}
	});
	
	var contractProp_val 				= {label: label, id: id, name: name, width: 300, attachAll:true, editable:true, editMode: false};
	contractProp_val['proxy']			= contract_proxy;
	contractProp_val['valueField']		= 'CODE_ID';
	contractProp_val['displayField']	= 'CODE_TEXT';
	    
	var contractComboBox 				=  createComboBoxComp(contractProp_val);
	   
	return contractComboBox;
}


function openSelectWorkPop(idField, nameField) {

	var setPopUpProp = {
		popId: "workPop",
		treeId: "workPop__treeId",
		popUpTitle: getConstText({ isArgs: true, m_key: '업무분류 선택' }),
		url: '/itg/itam/opms/searchWorkTreeComp.do',
		title: getConstText({ isArgs: true, m_key: '업무분류 선택' }),
		targetField: nameField,
		targetHiddenField: idField,
		leafSelect: false,
		rootSelect: true,
		expandAllBtn : "workPop__expandAllBtn",
		collapseAllBtn : "workPop__collapseAllBtn",
		expandLevel: 3
	}
	var popUp = createTreeSelectPop(setPopUpProp);
	popUp.show();

}

function searchBtnEvent(){
	
	var edit_type = Ext.getCmp('editTypeField').getValue();
	var seletedObj = Ext.getCmp('classTree').view.selModel.getSelection();
	
	var isSelectedTree = false;
	isSelectedTree = ( seletedObj != null && seletedObj != "" );
	
	var classType = 'ROOT';
	var classId = '-1';
	var level = '1';
	var nodeId = '-1';
	
	// 검색 벨리데이션 시작
	if( isSelectedTree ){
		var seletedItem = seletedObj[0].raw;
		
		level = seletedItem.node_level;
		classType = seletedItem.class_type;
		classId = seletedItem.id;
		nodeId = seletedItem.node_id;
		
		if( edit_type != 'COMMON' && level < getStdLevel() ) {
			alert("분류체계별 속성은 " + level + "단계 분류체계에서 검색할 수 없습니다.\n>>검색가능 분류체계 : 서버, 스토리지 ...");
			return;
		}
	} else {
		if( edit_type != 'COMMON' ) {
			alert("분류체계별 속성은 트리 선택 후 검색이 가능합니다.\n>>검색가능 분류체계 : 서버, 스토리지 ...");
			return;
		}
	}
	
	var searchForm = Ext.getCmp('assetPopSearchForm');
	var paramMap = searchForm.getInputData();
	var edit_type = Ext.getCmp('editTypeField').getValue();
	
	paramMap['edit_type'] = edit_type;
	paramMap['class_type'] = classType;
	paramMap['class_id'] = nodeId;
	paramMap['node_level'] = level;
	
	Ext.getCmp('setAssetGrid').searchList(paramMap);	
}

function getClassInfo() {
	
	var resultList = new Array();
	
	jq.ajax({ type:"POST"
		, url:'/itg/itam/batchEdit/selectClassTypeClassName.do'
		, contentType: "application/json"
		, dataType: "json"
		, async: false
		, data : getArrayToJson({})
		, success:function(data){
			resultList = data.gridVO.rows;
		}
	});
	
	return resultList;
	
}
