var popUp_comp={};

/**
 * 물리자산 선택 팝업 
 * @param {} setPopUpProp
 * @return {}
 */
function createPhysiServerSelectPop(setPopUpProp){
	
	var storeParam = {};
	var that = this;
	var physiServerSelectPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		
		var logicAssetSearchBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.search' }), id:'operAssetSearchBtn', ui:'correct'});
		var logicAssetSearchInitBtn = createBtnComp({visible: true, label: getConstText({ isArgs: true, m_key: 'btn.common.reset' }), id:'operAssetSearchInitBtn'});
	
		attachBtnEvent(logicAssetSearchBtn, logicActionLink,  'search');	
		attachBtnEvent(logicAssetSearchInitBtn, logicActionLink,  'searchInit');	
		
		var setLogicSearchFormProp = {
			id: that['id']+'SearchForm',
			formBtns: [logicAssetSearchBtn, logicAssetSearchInitBtn],
			columnSize: 3,
			tableProps : [
						  {colspan:1, tdHeight: 28, item : createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_OPER_ASSET', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})}
						 ,{colspan:1, tdHeight: 28, item : createTextFieldComp({name:'search_value', width: 200, padding: getConstValue('WITH_FIELD_PADDING')})}
						 ,{colspan:1, tdHeight: 28, item : createHiddenFieldComp({name:'class_id', padding: getConstValue('WITH_FIELD_PADDING')})}
			        	 ]
		}
		var searchLogicForm = createSeachFormComp(setLogicSearchFormProp);
		
		var setLogicGridProp = {	
				context: getConstValue('CONTEXT'),						// Context
				id: that['id']+'_grid',								// Grid id
				title: getConstText({ isArgs: true, m_key: 'res.title.system.vendor.search' }),		// Grid Title
				height: getConstValue('SHORT_GRID_HEIGHT'),
				resource_prefix: 'grid.itam.logic.asset',				// Resource Prefix
				url: getConstValue('CONTEXT') + that['url'],	// Data Url
				params: that['params'],
				pagingBar : true,
				pageSize : 15
		};
		var logicServerGrid = createGridComp(setLogicGridProp);
		
		function actionLink(command) {

			switch(command) {		
				case 'searchAttr' :
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
			
					logicServerGrid.searchList(paramMap);
				break;
			
			}
		}		
		
		var complexPanel = Ext.create('nkia.custom.Panel',{
			border: false,
			items: [searchLogicForm, logicServerGrid]
		})
			
		var confIdField = that['confIdField'];
		var confNmField = that['confNmField'];
		var tagetEtcField = that['tagetEtcField'];
		
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' })
		});
		
		function dbSelectEvent(){
			var selectedRecords = logicServerGrid.view.selModel.getSelection();
			if(selectedRecords.length <1){
				showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.vendor.00018' }));
			}else{
				
				if(that['afterFunction'] != null) {
					that['afterFunction'](selectedRecords[0].raw, that['mappingObj']);
				} else {
					var conf_id = selectedRecords[0].raw.CONF_ID;
					var conf_nm = selectedRecords[0].raw.CONF_NM;
					confIdField.setValue(conf_id);
					confNmField.setValue(conf_nm);
					physiServerSelectPop.close();
					
					goPhysiInfo(conf_nm, conf_id);
				}
			}
		}
		
		attachCustomEvent('beforeitemdblclick', logicServerGrid, dbSelectEvent);		
		attachBtnEvent(acceptBtn, dbSelectEvent);

		// Action에 따른 Logic 수행
		function logicActionLink(command) {
			switch(command) {
				case 'search':	
					var searchForm = Ext.getCmp(that['id']+'SearchForm');
					var paramMap = searchForm.getInputData();
					Ext.getCmp(that['id']+'_grid').searchList(paramMap);	
				break;
				case 'searchInit':	
					Ext.getCmp(that['id']+'SearchForm').initData();
				break;
			}
		}
		
		physiServerSelectPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false, 
			bodyPadding: that['bodyPadding'],
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
					
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = physiServerSelectPop;
		
	} else {
		physiServerSelectPop = popUp_comp[that['id']];
	}
	
	return physiServerSelectPop;
}