//자산조회 팝업
function createAssetPop(setPopUpProp){	
	var storeParam = {};
	var that = this;
	var setAssetPop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}
	
	if(popUp_comp[that['id']] == null) {
		// 조회 그리드
		var setSearchGridProp = {	
				id: that['id']+'Grid',
				title : getConstText({ isArgs: true, m_key: 'res.title.itam.maint.assetlist' }),
				gridHeight: 210,
				resource_prefix: 'grid.itam.lifecycle.asset',				
				url: '/itg/itam/lifeCycle/searchLifeCycleAsset.do',
				params: null,
				selModel: true,
				pagingBar : true,
				pageSize : 10,
				autoLoad: true
		};
		var assetSearchListGrid = createGridComp(setSearchGridProp);
		
		// 저장(세팅) 그리드
		var setSaveGridProp = {	
				id: 'selAssetGrid',
				title : getConstText({ isArgs: true, m_key: 'res.label.itam.maint.req.00014' }),
				gridHeight: 187,
				resource_prefix: 'grid.itam.lifecycle.setAsset',				
				params: null,		
				pagingBar : false,
				autoLoad: false,
				dragPlug: true,
				border: true,
				actionIcon: '/itg/base/images/ext-js/common/icons/delete.png',
				actionBtnEvent: actionBtnEvent,
				isMemoryStore: true
		};
		var assetSaveListGrid = createGridComp(setSaveGridProp);
		
		var setTreeProp = {				
				id : that['id']+'Tree',
				url : '/itg/itam/amdb/searchAmClassAllTree.do',
				title : getConstText({ isArgs: true, m_key: 'res.00016' }),
				dynamicFlag : false,
				searchable : true,
				expandAllBtn : false,
				collapseAllBtn : false,
				params: null,
				rnode_text :  getConstText({ isArgs: true, m_key: 'res.00003' }),
				expandLevel: 3
		}
		var staticClassTree = createTreeComponent(setTreeProp);	
		// 높이 길이 체크 무시
		staticClassTree['syncSkip'] = true;
		attachCustomEvent('select', staticClassTree, treeNodeClick);

		// 선택버튼
		var choiceBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.choice' }),
			handler: function() {
				checkGridRowCopyToGrid(that['id']+'Grid', 'selAssetGrid', { primaryKeys: ['ASSET_ID'] });
			}
		});
		var choiceBtnpanel = createHorizonPanel({ panelBtns: [choiceBtn] });

		var operAssetSearchBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.search' }),
			ui:'correct',
			handler: function() {
				var searchForm = Ext.getCmp(that['id']+'SearchForm');
				var paramMap = searchForm.getInputData();
				Ext.getCmp(that['id']+'Grid').searchList(paramMap);	
			}
		});
		
		var operAssetSearchInitBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.reset' }),
			handler: function() {
				Ext.getCmp(that['id']+'SearchForm').initData();
			}
		});
		
		var setAssetSearchFormProp = {
			id: that['id']+'SearchForm',
			formBtns: [operAssetSearchBtn, operAssetSearchInitBtn],
			columnSize: 3,
			tableProps : [{
							colspan:1, tdHeight: 30, item: createCodeComboBoxComp({label:getConstText({ isArgs: true, m_key: 'res.label.system.00012' }), name:'search_type', code_grp_id:'SEARCH_ASSET', width: 250, padding: getConstValue('WITH_FIELD_PADDING')})
						},{
							colspan:1, tdHeight: 30, item: createTextFieldComp({name:'search_value', padding: getConstValue('WITH_FIELD_PADDING')})
						}]
		}
		var searchAssetForm = createSeachFormComp(setAssetSearchFormProp);
		
		// 좌측 패널 : Tree
		var left_panel = Ext.create('Ext.panel.Panel',{
			region:'west',
			height: 515,
			autoScroll: true,
			items: [staticClassTree]
		});
		
		// 우측 패널 : CheckGrid <-> Choice Button <-> SelGrid
		var right_panel = Ext.create('Ext.panel.Panel',{
			border: false,
			region:'center',
			flex:1 ,
			items: [searchAssetForm, assetSearchListGrid, choiceBtnpanel, assetSaveListGrid]
		});
		
		var complexProp = {
				height : '100%',
				panelItems : [{
					flex: 1,
					items : left_panel
				},{
					flex: 3,
					items : right_panel
				}]
		};
		var complexPanel = createHorizonPanel(complexProp);		

		// 적용버튼 생성
		var acceptBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.regist' }),
			ui:'correct',
			handler: function() {
				if(getGrid('selAssetGrid').getStore().data.items <1){
					showMessage(getConstText({ isArgs: true, m_key: 'msg.itam.service.00006' }));
				}else{
					getGrid('popUp_grid').getStore().removeAll();
					var appendable = false;
					gridAllRowCopyToGrid('selAssetGrid', 'popUp_grid', appendable, null, { primaryKeys: ['asset_id'] });
					setAssetPop.close();
				}
			}
		});
		
		//닫기버튼 생성
		var closeButton = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			handler: function() {
				setAssetPop.close();
			}
		});
		
		/*** Private Function Area Start *********************/
		
		function actionBtnEvent(grid, rowIndex, colIndex){
			grid.getStore().removeAt(rowIndex); 
		};
		
		
		function treeNodeClick( tree, record, index, eOpts ) {
			var class_id = record.raw.node_id;
			
			var grid_store = getGrid(that['id']+'Grid').getStore();
			grid_store.proxy.jsonData={class_id: class_id};
			grid_store.reload();
		}	
		
		/*********************** Private Function Area End ***/
		
		setAssetPop = Ext.create('nkia.custom.windowPop',{
			id: that['id'],
			title: that['title'],  
			height: that['height'], 
			width: that['width'],  
			autoDestroy: false,
			resizable:false,
			buttonAlign: that['buttonAlign'],
			buttons: [acceptBtn, closeButton],
			bodyStyle: that['bodyStyle'],
			items: [complexPanel],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
				},
				hide:function(p) {
				}
			}
		});
		
		popUp_comp[that['id']] = setAssetPop;
		
	} else {
		setAssetPop = popUp_comp[that['id']];
	}
	
	return setAssetPop;

}