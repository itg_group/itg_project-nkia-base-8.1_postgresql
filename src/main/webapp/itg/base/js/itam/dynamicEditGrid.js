/**
 * 다이나믹 그리드(GridEdit, GridInsert/Delete)
 * @method {} createGridComp : 다이나믹 그리드 생성 Function()
 * @param {} 
 * @return {} Grid Store
 */
function createDynamicAssetDetailGrid(setGridProp ){
	
	var that = {};
	var grid_comp;
	for( var prop in setGridProp ){
		that[prop] = setGridProp[prop];
	}
	
	var id = that['id'];
	
	jq.ajax({ type:"POST"
			, url: getConstValue('CONTEXT') + "/itg/base/getGridMsgSource.do"
			, contentType: "application/json"
			, dataType: "json"
			, async: false
			, data : getArrayToJson({resource_prefix : that['resource_prefix']})
			, success:function(data){
				if(data.gridText != null) {
					gridText = data.gridText.split(",");
					gridHeader = data.gridHeader.split(",");
					gridWidth = data.gridWidth.split(",");
					gridFlex = ( data.gridFlex ) ? data.gridFlex.split(",") : "";
					gridXtype = ( data.gridXtype ) ? data.gridXtype.split(",") : "";
					gridFormat = ( data.gridFormat ) ? data.gridFormat.split(",") : "";
					gridEtype = ( data.gridEtype ) ? data.gridEtype.split(",") : "";
				}
			}
	});
	
	var dynamicAssetDetailGridProxy = createGridProxyComp(id, that['url'], that['params']);//id,url,param

	var dynamicAssetDetailGridStore = Ext.create('Ext.data.Store', {
		id: 'dynamicAssetDetailGridStore',
		fields : gridHeader,
		remoteSort: true,
		proxy: dynamicAssetDetailGridProxy,
		autoLoad: ( typeof that['autoLoad'] != "undefined" ) ? that['autoLoad'] : true
	});

	var gridColumns = new Array(gridHeader.length);
	for(var i = 0 ; i < gridHeader.length ; i++) {
		var gridColumn = {};
			gridColumn["text"] = gridText[i];
			gridColumn["width"] = parseInt(gridWidth[i]);
			gridColumn["dataIndex"] = gridHeader[i];
			gridColumn["etype"] = gridEtype[i];
			//showMessage(gridHeader[i]);
			if( gridFlex.length > 0 ){		// flex 
				if( isNa(gridFlex[i]) ){ gridColumn["flex"] = gridFlex[i]; }
			}
			if(gridXtype[i] == "combo"){
				gridColumn["editor"] = createCodeComboBoxComp({name:gridHeader[i], code_grp_id:gridHeader[i], width: parseInt(gridWidth[i]), hideLabel: true})
			} else if(gridXtype[i] == "date"){
				gridColumn["renderer"] = Ext.util.Format.dateRenderer(gridFormat[i]);
				gridColumn["editor"] = {xtype:'datefield', name:gridHeader[i], format: gridFormat[i], disabledDays: [0, 6]}
			} else if(gridXtype[i] == "button"){
				gridColumn["editor"] = createBtnComp({label: "테스트"})
			} else if(gridXtype[i] == "custom"){
				var customMap = that['customEvent'];
				gridColumn["editor"] = customMap[gridHeader[i].toLowerCase()];
			} else if(gridXtype[i] == "text"){ 
				gridColumn["editor"] = {xtype: 'textfield', name:gridHeader[i] }
			} else if(gridXtype[i] == "actioncolumn"){
				gridColumn["xtype"] = gridXtype[i];
				gridColumn["items"] = [{
			        icon: that['actionIcon'],
			        handler: function(grid, rowIndex, colIndex) {
			        	that['actionBtnEvent'](grid, rowIndex, colIndex);
			        }
			    }];
			}  
			
		gridColumns[i] = gridColumn;
	}
	
	var toolbarBtn = [];
	if(that['toolBarComp'] != null) {
		for(var i = 0 ; i < that['toolBarComp'].length ; i++) {
			toolbarBtn.push(that['toolBarComp'][i]);
		}
	}
	
	// 에디트 모드 설정 
	var editMode;
	if(that['editMode'] == 'ROW'){
		editMode = Ext.create('Ext.grid.plugin.RowEditing', {
	    });
	}else if(that['editMode'] == 'CELL'){
		editMode = Ext.create('Ext.grid.plugin.CellEditing', {
	    });
	}else{
		editMode = null;
	}
	
	// 체크 박스 객체 생성
	var checkBox = null;
	if(that['selModel'] == true ) {
		checkBox = new Ext.selection.CheckboxModel();
	}
	
	grid_comp = Ext.create('nkia.custom.grid', {
		  id : id
		, title: that['title']
		, plugins: [editMode]
		, height: ( that['gridHeight'] ) ? that['gridHeight'] : getConstValue('MIDDLE_GRID_HEIGHT')
		, minHeight: ( that['gridHeight'] ) ? that['gridHeight'] : getConstValue('MIDDLE_GRID_HEIGHT')
		, store: dynamicAssetDetailGridStore
		, columns: gridColumns
		, columnLines: true
		, width: ( that['gridWidth'] ) ? that['gridWidth'] : '100%'
		, autoScroll: true
		, autoLoad: false
		, selModel: checkBox
		, tools: toolbarBtn
		, hidden : ( that['hidden'] ) ? that['hidden'] : false
		, forceFit :( that['forceFit'] ) ? that['forceFit'] : true
		, viewConfig: {
			emptyText:( that['emptyMsg'] ) ? that['emptyMsg'] : getConstText({ isArgs: true, m_key: 'msg.common.00001' })
		}
		, listeners: {
			columnresize: function( ct, column, width, eOpts ) {
				if(column.getEditor() != null) {
					if(column.getEditor().xtype == "combobox") {
						var comboBoxWidth = width;
						column.getEditor().setWidth(comboBoxWidth);	
						column.getEditor().defaultListConfig.width = comboBoxWidth;
						column.getEditor().defaultListConfig.minWidth =comboBoxWidth;						
					}
				}
				if(column.getIndex() == ct.headerCounter - 1) {
					this.doLayout();
				}
	    	},
	    	//그리드 cell validation
	    	edit: function( editor, e, eOpts ){
	    		var gridHeader = editor.grid.columns.length;
	    		
	    		for(var i=0; i<gridHeader; i++){
	    			var etype = editor.grid.columns[i].etype;
	    			var text = editor.grid.columns[i].text;
	    			var dataIndex = editor.grid.columns[i].dataIndex;
	    			if(etype != 'na' || etype != 'undefined'){
	    				if(etype == "number"){
	    					var compareValue = editor.grid.store.data.items[e.rowIdx].data[dataIndex];
	    					if(compareValue != null && compareValue != ""){
		    					if(isNaN(parseInt(compareValue))){
		    						showMessage(text+getConstText({ isArgs: true, m_key: 'msg.itam.oper.req.00003' }));
		    						editor.grid.store.getAt(e.rowIdx).set(dataIndex, '')
		    					};
	    					}
	    				}
	    			}
	    		}
	    		
	    	}
		}
	});
	
	return grid_comp;
	
}