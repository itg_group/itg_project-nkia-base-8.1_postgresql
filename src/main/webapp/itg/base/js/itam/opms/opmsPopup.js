function selectTimeStamp(){
	var time = new Date();  //time이라는 변수에 현재 시간을 생성 합니다.
	var timeStamp = time.getDate(); //현재 시간에서 date만 출력합니다.
	timeStamp = time-1000;
	return timeStamp.toString();
}

/*
 * shhong 2014-03-09
 */
function ObjectCopy(obj){
	return JSON.parse(JSON.stringify(obj));
}

/**
 * 자산 반입확인 분류 팝업 
 * @param treeType : HW 하드웨어만
 */
function newAssetInsertRegistClassPopup(treeType){
	

//todo 그리드 널체크
	
	var windowComp 		= "";
	var amClassTree		= "";
	var conFirmBtn	 	= createBtnComp({visible: true,label:"적용" ,id:"conFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	
	
	//to do CARRY_ID값 가져오기 Ext.getCmp('newAssetHigherList')
	var selectList = getGrid("newAssetHigherList").view.selModel.getSelection();
	
	if (selectList.length == 0){
		alert('반입할 자산을 선택하지 않았습니다.');
		return;
	}
	
	var paramMap = {};
	for( var i=0; i < selectList.length; i++ ){
		paramMap[i] = selectList[i].raw;
		
		//var storeIndex = selectList[i].raw["RNUM"] - 1;
		//var param_store = grid.getStore().data.items[storeIndex].data;
	}
	
	
	
	var conFirmFn	= function(){
		var node 		= amClassTree.getSelectionModel().getSelection();
		if(node.length>0){
			
			var id 		= node[0].get("id");
			var classtype	= node[0].raw.class_type;
			var text		= node[0].get("text");
			var leaf		= node[0].get("leaf");
			var closable = true;
			
			var listMap = {}
				, map	= {}
				, grid	= jq("#newAssetHigherList-body").find(".x-grid-row")
				, idx	= 0
				, xgc	= ".x-grid-cell"
				, gci	= ".x-grid-cell-inner";	//shhong 2014-03-09
			
			//var logical_yn	= node[0].get("logical_yn");
			//var param	= [];
			
			if(leaf){ 
				showMessageBox({msg : "[" + text + "]분류로 등록하시겠습니까?" ,type : "CONFIRM" ,fn : function(btns){
					if(btns == "ok"){
						for( var i=0; i < selectList.length; i++ ){
							idx							= selectList[i].raw["RNUM"] - 1;
							paramMap[i]['CLASS_ID']		= id;
							paramMap[i]['CLASS_TYPE']	= classtype;
							paramMap[i]['MODEL_ID']		= grid.eq(idx).find(xgc).eq(6).find(gci).html().replace(/&nbsp;/g, '');
							
							var item = {
									"PART_NO"		: grid.eq(idx).find(xgc).eq(10).find(gci).html().replace(/&nbsp;/g, '')
									,"ASSET_DESC"	: grid.eq(idx).find(xgc).eq(11).find(gci).html().replace(/&nbsp;/g, '')
									,"SERIAL_NO"	: grid.eq(idx).find(xgc).eq(15).find(gci).html().replace(/&nbsp;/g, '')
									,"INTRO_DT"		: grid.eq(idx).find(xgc).eq(16).find(gci).html().replace(/&nbsp;/g, '')
									,"WARRANTY_LIFE": grid.eq(idx).find(xgc).eq(17).find(gci).html().replace(/&nbsp;/g, '')
									,"CARRY_ID"		: grid.eq(idx).find(xgc).eq(20).find(gci).text()
									,"CARRY_SEQ"	: grid.eq(idx).find(xgc).eq(22).find(gci).text()
							};
							
//							alert(	''
//									+'1:'+grid.eq(idx).find(xgc).eq(19).find(gci).text()+'\n'
//									+'2:'+grid.eq(idx).find(xgc).eq(21).find(gci).text()+'\n'
//									+'3:['+grid.eq(idx).find(xgc).eq(9).find(gci).html().replace(/&nbsp;/g, '')+']\n'
//									+'4:['+grid.eq(idx).find(xgc).eq(10).find(gci).html().replace(/&nbsp;/g, '')+']\n'
//									+'5:['+grid.eq(idx).find(xgc).eq(14).find(gci).html().replace(/&nbsp;/g, '')+']\n'
//									+'6:['+grid.eq(idx).find(xgc).eq(16).find(gci).html().replace(/&nbsp;/g, '')+']\n'
//									+'7:'+grid.eq(idx).find(xgc).eq(15).find(gci).text()
//									);
							
							listMap[i] = item;
							
						}
						
						listMap = ObjectCopy(listMap);

						jq.ajax({ type:"POST"
							, url: "/itg/itam/newAsset/saveOthersCarry.do"
							, contentType: "application/json"
							, dataType: "json"
							, async: false
							, data : getArrayToJson(listMap)
							, success:function(data){
								if( data.success ){
									jq.ajax({ type:"POST"
										, url: "/itg/itam/newAsset/insertAssetInfo.do"
										, contentType: "application/json"
										, dataType: "json"
										, async: true
										, data : getArrayToJson(paramMap) 
										, success:function(data){
											if( data.success ){
												alert('반입확인되었습니다.');
												newAssetActionLink('search');
											}else{
												alert('반입확인 실패하였습니다.');
											}
										}
									});
									
								}else{
									alert(data.resultMsg);
								}
							}
						});

						
						
						
						windowComp.close();
						
						}else{
							windowComp.close();
						/*param["CLASS_ID"] = id;
						if(treeType == "LOG"){
							param["CONF_NM"] = text;
							param["TAB_ID"] = selectTimeStamp();
							param["TANGIBLE_ASSET_YN"] = 'N';
							param["LOGICAL_YN"] = logical_yn;
							//registAddTab('/itg/itam/opms/opmsDetailInfo.do', param, closable); //OK눌렀을 때 Param 인서트 메소드 호출
						}else{
							param["CONF_NM"] = text;
							param["TAB_ID"] = selectTimeStamp();
							param["TANGIBLE_ASSET_YN"] = 'Y';
							//parent.registAddTab('/itg/itam/opms/opmsDetailInfo.do', param, closable);//OK눌렀을 때 인서트 메소드 호출
						}*/
						}
					}});
			}else{
				showMessageBox({msg : "마지막 분류만 선택가능합니다." ,type : "ALERT"});
			}
		}else{
			showMessageBox({msg : "선택된 분류가 없습니다." ,type : "ALERT"});
		}
	}
	
	var setOperClassTreeProp = {
		id : 'operClassTree',
		url : '/itg/itam/opms/selectConfClassTree.do',
		title : getConstText({ isArgs: true, m_key: 'res.title.itam.class' }),
		height: 330,
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		collapseAllBtn : true,
		params: {treeType : treeType},
		rnode_text : '#springMessage("res.00003")',
		expandLevel: 2
	}
	amClassTree = createTreeComponent(setOperClassTreeProp);
	
	var complexPanel = createFlexPanel({items : [amClassTree]});
	
	var classTreePopProp		= {
			 id: "classTreePop" 
			,title: "자산분류"
			,width: 300
			,height: 400
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [conFirmBtn]
			,closeBtn : true
			,items: complexPanel
			,resizable:true
	 }
	
	windowComp = createWindowComp(classTreePopProp);
	attachBtnEvent(conFirmBtn	,conFirmFn);
	windowComp.show();
}


/**
 * 자산 분류 팝업 
 * @param treeType : HW SW 기타 등등 단건 입력
 */

function openRegistClassPopup(treeType){ 
	
	
	var windowComp 		= "";
	var amClassTree		= "";
	var conFirmBtn	 	= createBtnComp({visible: true,label:"적용" ,id:"conFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	var paramMap = {};

	
	
	var conFirmFn	= function(){
		var node 		= amClassTree.getSelectionModel().getSelection();
		if(node.length>0){
			
			var id 		= node[0].get("id");
			var classtype	= node[0].raw.class_type;
			var text		= node[0].get("text");
			var leaf		= node[0].get("leaf");
			var logical_yn	= node[0].get("logical_yn");
			var closable = true;
			
			
			var param	= [];
			
			if(leaf){ 
				showMessageBox({msg : "[" + text + "]분류로 등록하시겠습니까?" ,type : "CONFIRM" ,fn : function(btns){
					if(btns == "ok"){
						param["CLASS_ID"] = id;
						
						if(treeType == "LOG"){
							param["CONF_NM"] = text;
							param["TAB_ID"] = selectTimeStamp();
							param["TANGIBLE_ASSET_YN"] = 'N';
							param["LOGICAL_YN"] = logical_yn;
							param["CLASS_TYPE"] = classtype;
							//registAddTab('/itg/itam/opms/opmsDetailInfo.do', param, closable); 
							registAddTab('/itg/itam/automation/goAssetDetailViewer.do', param, closable); 
							
						}else{
							param["CONF_NM"] = text;
							param["TAB_ID"] = selectTimeStamp();
							param["TANGIBLE_ASSET_YN"] = 'Y';
							param["CLASS_TYPE"] = classtype;
							//parent.registAddTab('/itg/itam/opms/opmsDetailInfo.do', param, closable);
							parent.registAddTab('/itg/itam/automation/goAssetDetailViewer.do', param, closable);
						}
						windowComp.close();

						
					}else{
							windowComp.close();

					}
				}});
		}else{
				showMessageBox({msg : "마지막 분류만 선택가능합니다." ,type : "ALERT"});
		}
	}else{
			showMessageBox({msg : "선택된 분류가 없습니다." ,type : "ALERT"});
	}
}
	
	var setOperClassTreeProp = {
		id : 'operClassTree',
		url : '/itg/itam/opms/selectConfClassTree.do',
		title : getConstText({ isArgs: true, m_key: 'res.title.itam.class' }),
		height: 330,
		dynamicFlag : false,
		searchable : true,
		expandAllBtn : true,
		collapseAllBtn : true,
		params: {treeType : treeType},
		rnode_text : '#springMessage("res.00003")',
		expandLevel: 2
	}
	amClassTree = createTreeComponent(setOperClassTreeProp);
	
	var complexPanel = createFlexPanel({items : [amClassTree]});
	
	var classTreePopProp		= {
			 id: "classTreePop" 
			,title: "자산분류"
			,width: 300
			,height: 400
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [conFirmBtn]
			,closeBtn : true
			,items: complexPanel
			,resizable:true
	 }
	
	windowComp = createWindowComp(classTreePopProp);
	attachBtnEvent(conFirmBtn	,conFirmFn);
	windowComp.show();
}



function callAjax(url,data,callback){
	jq.ajax({ type:"POST"
		, url: url
		, contentType: "application/json"
		, dataType: "json" 
		, data : data
		, success: callback
	});
}





/**
 * 논리 등록 상위 장비 등록 팝업...
 * @param compProperty
 */
function openConfPop(compProperty){
	
	var that 			= convertDomToArray(compProperty);
	var targetId		= that["targetId"];
	var targetId1		= that["targetId1"];
	var targetNm		= that["targetNm"];
	var targetForm 	= Ext.getCmp(that["targetForm"]);
	var title				= that["title"];
	var type 			= that["type"];
	var confGrid		= "";
	var amClassTree	= "";
	var windowComp	= "";
	var searchForm	= "";
	var tangibleAssetYn = "";
	var tangibleAssetYnCombo = "";
	var assetState = "ACTIVE";
	
	if(type == "LOG"){
		tangibleAssetYnCombo = createHiddenFieldComp({name : "tangible_asset_yn" ,value : "Y"});
		tangibleAssetYn = "Y"
	}else{
		tangibleAssetYnCombo = createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.opms.00017'})  ,name:'tangible_asset_yn'	,code_grp_id:'YN'		,width : 205 ,attachAll : true});
	}
	
	var searchConfList = function (){
		var paramMap = searchForm.getInputData();
		var treeData = amClassTree.getSelectionModel().getSelection()[0];
		paramMap["type"] = type;
		paramMap["tangible_asset_yn"] = tangibleAssetYn;
		paramMap["asset_state"] = assetState;
		if(treeData){
			paramMap["class_id"] = treeData.data.id;
		}
		confGrid.searchList(paramMap);
	}
	
	var searchConfInit = function (){
		Ext.getCmp('confSearchForm').initData();
	}
	
	//적용
	var confirmMng = function (){
		var row 	= gridSelectedRows("confList");
		if(row){
			var param = [];
			showMessageBox({msg : getConstText({isArgs: true, m_key: 'msg.itam.opms.00012'}) ,type : "CHOICE" ,fn : function(btns){
					var conf_id =  row.raw.CONF_ID;
					var conf_nm =  row.raw.CONF_NM;
					var class_id =  row.raw.CLASS_ID;
					var asset_id =  row.raw.ASSET_ID;
					var closable = true;
					if(btns == "yes"){
						fieldSetValue(targetForm,targetId,[conf_id]);
						fieldSetValue(targetForm,targetId1,[asset_id]);
						fieldSetValue(targetForm,targetNm,[conf_nm]);
						param["CLASS_ID"] = class_id;
						param["LOGICAL_YN"] = "Y";
						param["TANGIBLE_ASSET_YN"] = 'N';
						param["CONF_NM"] = conf_nm;
						param["TAB_ID"] = selectTimeStamp();
						//registAddTab('/itg/itam/opms/opmsDetailInfo.do', param, closable);
						registAddTab('/itg/itam/automation/goAssetDetailViewer.do', param, closable);
						windowComp.close();
					}else if(btns == "no"){
						fieldSetValue(targetForm,targetId,[conf_id]);
						fieldSetValue(targetForm,targetId1,[asset_id]);
						fieldSetValue(targetForm,targetNm,[conf_nm]);
						openRegistClassPopup("LOG");
						windowComp.close();
					}else{	
						return;
					}
			}});
		}else{
			showMessageBox({msg :  getConstText({isArgs: true, m_key: 'msg.itam.opms.00010'}) ,type : "ALERT"}); 
		}
	}
	
	var amClassTreeNodeClick = function(obj,selData){
		searchForm.initData();
		var class_id = selData.data.id;
		var paramMap = [];
		paramMap["class_id"] = class_id;
		paramMap["type"] = type;
		paramMap["tangible_asset_yn"] = tangibleAssetYn;
		paramMap["asset_state"] = assetState;
		confGrid.searchList(paramMap);
	}
	
	//검색 버튼
	var searchConfBtn	= createBtnComp({visible: true,label: getConstText({ isArgs: true, m_key: 'btn.common.search' })	,id:'searchUserBtn' ,ui:'correct'});
	var conFirmBtn	 	= createBtnComp({visible: true,label: getConstText({isArgs: true, m_key: 'btn.common.apply'}) ,id:"confConFirmBtn" 	,ui    : "correct"	,scale: "medium"});
	var searchConfInitBtn = createBtnComp({visible: true, label: getConstText({isArgs: true, m_key: 'btn.common.reset'}) , id:'searchConfInitBtn'});
	
	//검색조건
	var custCombo	= createReqZoneComboBoxComp({label: getConstText({isArgs: true, m_key: 'res.label.system.00023'}), id: 'cust_id', name: 'cust_id', attachAll: true})
	var operSate		= createCodeComboBoxComp({label : getConstText({isArgs: true, m_key: 'res.label.itam.oper.00068'}) ,name:'asset_state'	,code_grp_id:'ASSET_STATE' ,attachAll : true});
	var searchKey		= createCodeComboBoxComp({name:'searchKey'	,code_grp_id:'SEARCH_OPER_ASSET'	,hideLabel : true ,attachAll : true, widthFactor:0.3});
	//검색값
	var searchValue	= createTextFieldComp({name:'searchValue'	,padding 	: '0 0 0 5' ,hideLabel : true, widthFactor:0.5 });
	//컨테이너
	var searchContainer		= createUnionFieldContainer({label : getConstText({ isArgs: true, m_key: 'btn.common.search' }),items : [searchKey,searchValue] ,id : "searchContainer", fixedLayout: true, fixedWidth: 520});
	
	var setSearchFormProp 	= {
								 id				: 'confSearchForm'
								,columnSize	: 1
								,width 		: 685
								,formBtns	: [searchConfBtn, searchConfInitBtn]//검색버튼을 생성 formBtns라는 속성을 추가하여 생성한 조회버튼을 추가함
								,tableProps : [
												   {colspan:1, tdHeight: 22, item : searchContainer}
												   ]
								,enterFunction : searchConfList
							  }
	//검색폼
	searchForm	= createSeachFormComp(setSearchFormProp);
	
	var setOperClassTreeProp = {
			id : 'confClassTree',
			url : '/itg/itam/opms/selectConfClassTree.do',
			title : getConstText({ isArgs: true, m_key: 'res.title.itam.class' }),
			height: 300,
			dynamicFlag : false,
			searchable : true,
			expandAllBtn : true,
			collapseAllBtn : true,
			params: {treeType : type},
			rnode_text : '#springMessage("res.00003")',
			expandLevel: 4
	}
	amClassTree = createTreeComponent(setOperClassTreeProp);
	
	attachCustomEvent('select', amClassTree, amClassTreeNodeClick);
	
	//구성리스트
	var setConfGridProp 	= {	 
									 id						: "confList"
									,title					: title
									,gridHeight			: 286
									,gridWidth			: 685
									,resource_prefix	:  'grid.itam.confNA'
									,url					: '/itg/itam/opms/selectOpmsConfList.do'
									,params				: {type : type,tangible_asset_yn : tangibleAssetYn, asset_state: assetState}
									,pagingBar 			: true
									,multiSelect			: false
									,selModel			: false
									,border 				: true
									,pageSize 			: 10
								};
	
	confGrid = createGridComp(setConfGridProp);
	
	//뷰포트 생성
	var viewportProperty = {
			borderId	: 'borderId' 
			,viewportId : 'viewId' 
			,west: {
				minWidth: 180 
				,maxWidth: 685 
				,items : [amClassTree] 
			},
			 center: { 
				 	minWidth: 180 
					,maxWidth: 685 
					,items : [searchForm,confGrid] 
			}
	}
	// Viewport 생성
	
	var codePopupProp		= {
			id:   "confPop"
			,title: title
			,width: 900
			,height: 475
			,modal: true
			,layout: 'fit'
			,buttonAlign: 'center'
			,bodyStyle: 'background-color: white; '
			,buttons: [conFirmBtn]
			,closeBtn : true
			,items: createBorderViewPortComp(viewportProperty,{ isLoading: true, isResize : false} )
			,resizable:true
	  }

	windowComp = createWindowComp(codePopupProp);
	
	//버튼 이벤트
	/*attachBtnEvent(delConfBtn	,deSelConf);
	attachBtnEvent(addConfBtn	,addSelConf);*/
	attachBtnEvent(conFirmBtn	,confirmMng);
	attachBtnEvent(searchConfBtn	,searchConfList);
	attachBtnEvent(searchConfInitBtn	,searchConfInit);
	
	windowComp.show();
}



