/**
 * TreeGrid 생성
 * id : treeGrid의 ID 부여
 * url : treeGrid의 데이터를 들고올 URL
 * title : treeGrid의 제목 
 * resource_prefix : treeGrid의 리소스 정보를 가진 부분 
 * paramMap : treeGrid URL에 넘겨야할 
 * @param compProperty
 * @returns
 */
function createTreeGroupGrid(compProperty){
	
	var that = {};
	var treeGrid_comp;
	
	for( var prop in compProperty ){
		that[prop] = compProperty[prop];
	}
	
	var id 				= that['id'];
	var params 			= that['params'] 			? that['params'] 		: {};
	var expandLevel 	= that['expandLevel'] 		? that['expandLevel'] 	: 0;
	var rootText 		= that['rootText'] 			? that['rootText'] 		: getConstText({ isArgs: true, m_key: 'res.common.all' });
	var dynamicFlag 	= that['dynamicFlag'] 		? that['dynamicFlag'] 	: false;
	var expandAllBtn 	= that['expandAllBtn'] 		? that['expandAllBtn'] 	: false;
	var collapseAllBtn 	= that['collapseAllBtn'] 	? that['collapseAllBtn']: false;
	var searchable 		= that['searchable'] 		? that['searchable'] 	: false;
	var extraToolbar 	= that['extraToolbar'] 		? that['extraToolbar'] 	: "";
	var gridFormItem 	= that['gridFormItem'] 		? that['gridFormItem'] 	: null;
	params['expandLevel'] 	= expandLevel;
	
	var gridText 	= '';
	var gridHeader 	= '';
	var gridWidth 	= '';
	var gridFlex 	= '';
	var gridAlign 	= '';
	var gridSortable= '';
	var gridXtype 	= '';
	var gridTpl 	= '';
	var gridFormat 	= '';
	var gridHidden 	= '';
	
	if(gridFormItem != null && gridFormItem.length > 0){
		gridText 	= gridFormItem[0];
		gridHeader 	= gridFormItem[1];
		gridWidth 	= gridFormItem[2];
		gridAlign 	= gridFormItem[3];
		gridFlex 	= gridFormItem[4];
		gridSortable= gridFormItem[5];
		gridXtype 	= gridFormItem[6];
		gridTpl 	= gridFormItem[7];
		gridHidden 	= gridFormItem[8];
	}else{
		jq.ajax({ type:"POST"
			, url		: getConstValue('CONTEXT') + "/itg/base/getGridMsgSource.do"
			, dataType	: "json"
			, async		: false
			, data 		: getArrayToJson({resource_prefix : that['resource_prefix']})
			, contentType: "application/json"
			, success	:	function(data){
								if(data.gridText != null) {
									gridText 	= data.gridText.split(",");
									gridHeader 	= data.gridHeader.split(",");
									gridWidth 	= data.gridWidth.split(",");
									gridAlign 	= data.gridAlign.split(",");
									gridFlex 	= data.gridFlex 	? data.gridFlex.split(",") 		: "";
									gridSortable= data.gridSortable ? data.gridSortable.split(",") 	: "";
									gridXtype 	= data.gridXtype  	? data.gridXtype.split(",") 	: "";
									gridTpl 	= data.gridTpl 	 	? data.gridTpl.split(",") 		: "";
									gridFormat 	= data.gridFormat 	? data.gridFormat.split(",") 	: "";					
								}
							}
		});
	}
	
	var arrayFields = new Array();
	
	 // Field 생성 부분 
   for ( var i = 0; i < gridHeader.length; i++) {
	   var tempFieldArray = new Array();
	   tempFieldArray = {
			   name : gridHeader[i]
	   		  ,type : "string"
	   }
	   arrayFields.push(tempFieldArray);
   }
    
   // Model 생성 부분
   Ext.define('treeGrid_model',{
	   id : id+ '_model', 
	   extend: 'Ext.data.Model',
	   fields: arrayFields
   });
	
	var gridColumns = new Array(gridHeader.length);
	
	for(var i = 0 ; i < gridHeader.length ; i++) {
		
		var gridColumn = {};
		gridColumn["text"] 		= gridText[i];
		gridColumn["width"] 	= parseInt(gridWidth[i]);
		gridColumn["dataIndex"] = gridHeader[i]
		gridColumn["style"] 	= 'text-align:center';
		
		if(gridAlign.length > 0){
			gridColumn["align"] = gridAlign[i];
		}
		if(gridFlex.length 	> 0){
			if(isNa(gridFlex[i])){
				gridColumn["flex"] = gridFlex[i];
			}
		}
		if( gridSortable.length > 0 ){	// sortable
			gridColumn["sortable"] = compareValueToBoolean(gridSortable[i]);
		}
		
		if( gridTpl.length > 0 ){		// tpl
			if(isNa(gridTpl[i])){ 
				gridColumn["tpl"] = gridTpl[i]; 
			}
		}
		if( gridHidden.length > 0 ){		// grid hidden
			if(isNa(gridHidden[i]) ){ 
				gridColumn["hidden"] = gridHidden[i]; 
			}
		}
		
		if( gridXtype.length > 0 ){		// xtype
			if( isNa(gridXtype[i]) ){ 
				if(i==0){
					gridColumn["xtype"] = 'treecolumn';
				}else{
					switch(gridXtype[i]){
						case "radio":
							gridColumn["hideable"]	= false;
							gridColumn["width"] 	= 30;
							gridColumn["renderer"] 	= renderRadioBox;
							gridColumn["editor"] 	= {xtype:'radio'}
						break;
						case "date":
							var dateFormatFunc = Ext.util.Format.dateRenderer(( gridFormat[i] ) ? gridFormat[i] : "Y-m-d");
							gridColumn["editor"] = {xtype:'datefield', format: ( gridFormat[i] ) ? gridFormat[i] : "Y-m-d"};
							gridColumn["renderer"] = function( value ){
														if( typeof value == "string" ){
															return value;
														} else {
															return dateFormatFunc(value);
														}
													};
						break;
						case "text":
							gridColumn["editor"] = {xtype:'textfield'};
						break;
						case "reply":
							gridColumn["renderer"] = getReplyImage;
						break;
						case"actioncolumn":
							gridColumn["xtype"] = gridXtype[i];
							gridColumn["items"] = [{icon	: that['actionIcon'],
													handler	: function(grid, rowIndex, colIndex) {
					        										that['actionBtnEvent'](grid, rowIndex, colIndex);
					        								  }
					    						  }];
						break;
						case "combo":
							var comboComp = that['comboComp'][gridHeader[i]];
							if(comboComp){
								gridColumn["editor"] 	= comboComp;
								gridColumn["renderer"] 	= function(value){
																	var comboStore 	= comboComp.store;
																	var valKey 		= comboComp.valueField;
																	var disKey 		= comboComp.displayField;
																	var index 		= comboStore.findExact(valKey,value);
																	if(index > -1){
																		var rs = comboStore.getAt(index).data[disKey];
																		return rs;
														 			}else{
														 				return comboComp.emptyText;
														 			}
															};
														}
						break;
						case "updown":
							gridColumn["xtype"] = "actioncolumn";
							gridColumn["items"] = [{
				                    icon   : '/itg/base/images/ext-js/simple/btn-icons/btn_up.gif',  // Use a URL in the icon config
				                    handler: function(grid, rowIndex, colIndex) {
				                    		gridData = grid.store.getAt(rowIndex);
				                    		if(rowIndex != 0){
				                    			grid.store.removeAt(rowIndex);
					   	                		grid.store.insert(rowIndex-1,gridData);
				                    		}
				                    	}				                    	
									},{
				                    icon   : '/itg/base/images/ext-js/simple/btn-icons/btn_down.gif',  // Use a URL in the icon config
				                    handler: function(grid, rowIndex, colIndex) {
				                    		gridData = grid.store.getAt(rowIndex);
				                    		if(rowIndex != grid.store.count()-1){
				                    			grid.store.removeAt(rowIndex);
					   	                		grid.store.insert(rowIndex+1,gridData);
				                    		}
				                    	}
									}];
						break;
						case "check":
							gridColumn['xtype'] = "customcheckcolumn";
							gridColumn['listeners'] = {checkchange: that["checkClickEvent"] == null ? Ext.emptyFn : that["checkClickEvent"] };
						break;
						case "actionifcolumn":
							gridColumn['xtype'] = "actionifcolumn";
							var param	= that['btnColumn'][gridHeader[i]];
							param	= convertDomToArray(param);
							gridColumn['compareColumn'] = param['compareColumn'];
							gridColumn['compareFlag'] 	= param['compareFlag'];
							gridColumn['compareValArr'] = param['compareValArr'];
							gridColumn['handler']	= param['handler'];
							gridColumn['tooltip'] 	= gridText[i];
							gridColumn['icon'] 		= param['icon'];
						break;
						case "customifcheck":
							gridColumn['xtype'] = "customifcheck";
							var param	= that['chkColumn'][gridHeader[i]];
							param	= convertDomToArray(param);
							gridColumn['compareColumn'] = param['compareColumn'];
							gridColumn['compareFlag'] 	= param['compareFlag'];
							gridColumn['compareValArr'] = param['compareValArr'];
							gridColumn['listeners'] 	= {checkchange: param['checkClickEvent'] == null ? Ext.emptyFn : param["checkClickEvent"] };
						break;
						default:
							gridColumn["xtype"] = gridXtype[i];
						break;
					}
				}
			}
		}
		gridColumns[i] = gridColumn;
	}
	
	var tree_proxy = Ext.create('nkia.custom.JsonProxy', {
									id: id+ '_proxy',
									defaultPostHeader : "application/json",
									noCache : false,
									type: 'ajax',
									url: that['url'],
									jsonData : ( params ) ? params : {},
									actionMethods: {
										create	: 'POST',
										read	: 'POST',
										update	: 'POST'
									},
									reader: {
										type	: 'json'
									}
								});	
	
		
	var treeStore = Ext.create('Ext.data.TreeStore', {
		id: id + "_store",
		model: 'treeGrid_model',
		proxy: tree_proxy,
		autoLoad: false,
		root: {
			text	: rootText,
			id		: 'root',
			node_id	: 'root'
		},
		listeners: {
			beforeexpand:function(node) {
				if(dynamicFlag) {
					var arrayParam = {};
					var node_id = node.raw.id;
					arrayParam["up_node_id"] = node_id;
					arrayParam["expandLevel"] = 0;
					tree_proxy.jsonData = arrayParam;
				}
			}
		}
	});
	
	
	Ext.suspendLayouts();
	var treeGrid_comp = Ext.create('nkia.custom.TreePanel', {
    	id 		: id,
    	itemId	: that['itemId'] ? that['itemId'] : '',
    	title	: that['title'],
    	icon	: getConstValue('CONTEXT')+'/itg/base/images/ext-js/common/icons/bullet_all.gif',
         width	: '100%',
        height	: that['height'] ? that['height'] : '100%',
        maxHeight: that['height'] ? that['height'] : '100%',
        cls		: that['cls'] ? that['cls'] : null,
        minHeight	: 100,
        useArrows	: true,
        autoScroll	: true,
        expandAllBtn 	: expandAllBtn,
        rootVisible: that['rootVisible'] ? that['rootVisible'] : false,
        collapseAllBtn 	: collapseAllBtn,
        searchable	: searchable,
        searchableTextWidth: that['searchableTextWidth'] ? that['searchableTextWidth'] : getConstValue('DEFAULT_SEARCH_TREE_WIDTH_FIELD'),
        dynamicFlag: dynamicFlag,
        extraToolbar: extraToolbar,
        animate : false,
        columns: gridColumns,
        store: treeStore,
        columnLines	: that['columnLines'] ? that['columnLines'] : false,
        viewConfig: {
        	deferEmptyText:( that['emptyMsg'] ) ? that['emptyMsg'] : getConstText({ isArgs: true, m_key: 'msg.common.00001' }),
        	loadMask: ( that['loadMask'] ) ? that['loadMask'] : true,
        	loadingText:( that['loadMsg'] ) ? that['loadMsg'] : getConstText({ isArgs: true, m_key: 'msg.mask.common' }),
        	onExpand: function(){
        		Ext.resumeLayouts(true);
        	},
        	onBeforeExpand: function(){
        		Ext.suspendLayouts();
        	}
        },
		listeners: {
        	afterrender:function(obj) {
        		obj.readyState = true;
        		obj.expandNode(obj.getRootNode());
        		if( expandLevel > 1 ){
        			var ojbStore = obj.getStore();
	        		var interval_expand = setInterval(function () {
			  			if (!ojbStore.isLoading()) {
			  				var rootNode = ojbStore.getRootNode();
							if( rootNode.firstChild ){	// Root의 ChildNode가 있을 경우
								rootNode.firstChild.cascadeBy(function (node) {
									if( node.getDepth() < expandLevel ) { node.expand(); }
									if( node.getDepth() == expandLevel ) { return false; }
								});
							}
					    	clearInterval(interval_expand);
			 		 	}
					}, 50);
        		}
        	}
	}});
	
	Ext.resumeLayouts(true);
	
    return treeGrid_comp;
}