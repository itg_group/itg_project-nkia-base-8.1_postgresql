<%--
by Mr.Hwang
RequestMapping
--%>
<%@page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@page import="org.springframework.web.context.WebApplicationContext" %>
<%@page import="java.util.Collections" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Enumeration"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>RequestMappingList</title>
<link rel="stylesheet" type="text/css" href="/css/common/bbs.css" />
</head>
<body>

<table class="bbs_list">
<thead>
	<tr>
		<th scope="col">Num</th><th scope="col">RequestMapping URL</th ><th scope="col">RequestMapping Class</th>
	</tr>
</thead>
<tbody>
<%
//weblogic.servlet.internal.WebAppServletContext sc = (weblogic.servlet.internal.WebAppServletContext) getServletConfig().getServletContext();
Enumeration<String> em = getServletConfig().getServletContext().getAttributeNames() ;
Object o = null ;
Object o1 = null ;
int count = 0 ;
while(em.hasMoreElements()){
	o1 = getServletConfig().getServletContext().getAttribute(em.nextElement().toString()) ;

	if(o1 instanceof org.springframework.web.context.support.XmlWebApplicationContext ){
		org.springframework.web.context.support.XmlWebApplicationContext o2
			=(org.springframework.web.context.support.XmlWebApplicationContext) o1 ;

		if(o2.getDisplayName().indexOf("action-servlet") > 1){

			/*Action����*/
			org.springframework.web.context.support.XmlWebApplicationContext o3
				= (org.springframework.web.context.support.XmlWebApplicationContext) o2.getServletConfig().getServletContext().getAttribute("org.springframework.web.servlet.FrameworkServlet.CONTEXT.action") ;

			String[] temp  = o3.getBeanDefinitionNames() ;
			for(int i=0 ; i<  temp.length ;i++ ){

				if(o3.getBean(temp[i]) != null){

					if(o3.getBean(temp[i]) instanceof org.springframework.web.servlet.mvc.annotation.DefaultAnnotationHandlerMapping){

						java.util.Map<String, Object> map =
						((org.springframework.web.servlet.mvc.annotation.DefaultAnnotationHandlerMapping)
								o3.getBean(temp[i])).getHandlerMap() ;

						java.util.Set<String> mapKey = map.keySet() ;
						java.util.Iterator<String> mapKeyList = mapKey.iterator() ;

						String sKey = "" ;
						Object oKeyValue = null ;

						while(mapKeyList.hasNext()){
							sKey = mapKeyList.next().toString() ;

							out.print("<tr><td>"+(count++)+"</td>") ;
							out.print("<td style='text-align:left'>"+sKey+"</td>") ;
							out.print("<td style='text-align:left'>"+map.get(sKey).getClass().getName()+"</td></tr>") ;
						}
					}
				}
			}
		}
	}
}
%>
</tbody>
</table>

</body>
</html>