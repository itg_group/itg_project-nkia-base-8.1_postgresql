<%@ page autoFlush="true" %>
<%@ include file="/itg/base/SSO/sso_util.jsp" %>

<%@ page import="WiseAccess.*" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="egovframework.com.cmm.service.EgovProperties" %>

<%
	// 브라우저 캐싱 방지
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
	//String engineIP   = "10.10.11.90";
	//int	enginePort = 7000;
	
	String context = request.getContextPath();
	String engineIP = EgovProperties.getProperty("SSO.HostName");
	int	enginePort = Integer.parseInt(EgovProperties.getProperty("SSO.PortNumber"));
%>
 
<link rel="stylesheet" href="/itg/base/SSO/css/common.css" type="text/css">
<%-- <script type="text/javascript" src="/itg/base/SSO/js/jquery.js"></script>--%>
<script type="text/javascript" src="/itg/base/component/jquery/js/jquery-1.7.2.js"></script>
<script type="text/javascript">
     var jq = jQuery.noConflict();
</script>
<title>TouchEn wiseaccess [Raonsecure]</title>
<meta http-equiv='Content-Type' content='text/html;' charset='utf-8'>
<meta http-equiv='Pragma' content='No-Cache' charset="utf-8">