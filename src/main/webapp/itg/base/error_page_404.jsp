<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>
<% response.setStatus(HttpServletResponse.SC_OK); %>
<!doctype html>
<html lang="ko">
<!--
*//**
 * 
 * @author <a href="mailto:hjko@nkia.co.kr"> Hanjo Ko
 * 
 *//*
-->

<head>
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>
<!-- ITG Base Page parse -->
<title>IT 운영관리자동화</title>
<!-- ITG Base CSS parse -->
<link href="/itg/base/css/ext-js/fonts/nanumgothic.css" rel="stylesheet" type="text/css">
</head>


<style type="text/css">
<!--
#error_box		{ 
	position:absolute;
	background:url(/itg/base/images/ext-js/simple/status/error_page.gif) no-repeat;
	width:420px; height:160px; 
	left:50%; top:50%;
	margin-left: -210px; 
	margin-top: -80px;
	border:none;
	text-align:center;
	z-index:1; }
#error_message		{ 
	position:absolute;
	width:270px; height:160px; 
	left:150px; top:0;
	border:none;
	font-family:'맑은 고딕', 'Malgun Gothic', 'Nanum Gothic', '나눔고딕', NanumGothic, '돋움', Dotum, sans-serif; 
	text-align:left;
	z-index:2; }	
#message_title 		{ font-family:Arial, Helvetica, sans-serif; font-size:90px; font-weight:normal; color:#cccccc; line-height:85px; margin-top:5px; }
#message_sub 		{ font-size:18px; font-weight:bold; color:#aaaaaa; line-height:24px; margin-top:0; white-space:nowrap; }
#message_help	 	{ font-size:14px; font-weight:normal; color:#222222; line-height:18px; margin-top:18px;  }
-->
</style>
</head>
<body topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0" bgcolor="#ffffff">

	<div id="error_box">
	<div id="error_message">
			<div id="message_title">404</div>
			<div id="message_sub">페이지를 찾을 수 없습니다.</div>
<!-- 			<div id="message_help">장애문의 : IDC운영팀 서울센터담당 (02-2626-3922)</div> -->
		</div>
	</div>
	
</body>
</html>