var setGridProp = {	
		id: 'sipGridList',										// Grid id
		resource_prefix: 'grid.slms.sip',					// Resource Prefix
		url: '/itg/slms/doc/searchSipList.do',			// Data Url
		params: null											// Data Parameters
};

Ext.define('Ext.app.SipGrid', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.sipgrid',
	title: 'test',
	setGridProp : {	
			id: 'sipGridList',										// Grid id
			resource_prefix: 'grid.slms.sip',					// Resource Prefix
			url: '/itg/slms/doc/searchSipList.do',			// Data Url
			params: null											// Data Parameters
	},
	items: createGridComp(this.setGridProp)
});
