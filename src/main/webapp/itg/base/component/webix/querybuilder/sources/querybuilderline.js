import "./less/query.less";
import { locale } from "./locales";
webix.i18n.querybuilder = locale;
webix.protoUI({
    name: "querybuilderline",
    defaults: {
        height: 36,
        padding: 0, margin: 0,
        borderless: true
    },
    $init() {
        this.$view.className += " webix_querybuilder_line";
        this.$ready.unshift(this._setqueryline);
        this.$ready.push(this._setForm);
    },
    setFilters(filters) {
        this.config.filters = filters;
        this._setRuleSelect(this.config.value, this.config.fields, true);
    },
    _setqueryline() {
        let select = {
            view: "richselect",
            minWidth: 100, maxWidth: this.config.inputMaxWidth, inputWidth: this.config.inputWidth, width: this.config.inputWidth,
            height: 38, inputHeight: 38, maxHeight: 38,
            inputPadding: 0,
            options: [],
            css: "",
            name: ""
        };
        this.cols_setter([
            Object.assign({}, select, { css: "webix_querybuilder_value_select", name: "key" }),
            Object.assign({}, select, { css: "webix_querybuilder_rule_select", name: "rule", hidden: true }),
            { view: "button", type: "htmlbutton", css: "webix_querybuilder_close", width: 20, inputWidth: 20, name: "close",
                label: `<span class="fa fa-trash-o" title="${locale.delete_rule}"></sapn>`, click: () => {
                    this._masterQuery._deleteRow(this);
                }
            }, { gravity: 0.001 }
        ]);
    },
    _setForm() {
        this._valueSelect = this.elements.key;
        this._ruleSelect = this.elements.rule;
        if (this.config.columnMode) {
            if (!this.$view.classList.contains("webix_column_qb")) {
                this.define({ height: 150, margin: 0, rows: [] });
                webix.html.addCss(this.$view, "webix_column_qb");
                this.elements.close.getNode().style.display = "block";
            }
        }
        else {
            if (this.$view.classList.contains("webix_column_qb")) {
                webix.html.removeCss(this.$view, "webix_column_qb");
                this.define({ height: 38, margin: 10, cols: [] });
                this.elements.close.getNode().style.display = "inline-block";
            }
        }
        this._setKeySelect();
    },
    _setPreselectedKeys(rule) {
        this._setKeySelect(rule);
        this._setRuleSelect(rule, this.config.fields);
    },
    _setKeySelect(rule) {
        let options = [{ id: "not_selected", value: locale.default_option, type: "string" }, ...this.config.fields];
        let obj = {};
        this._valueSelect.getList().parse(options);
        if (rule) {
            this._valueSelect.setValue(rule.key);
            this._updateRules(rule);
        }
        else {
            rule = obj;
        }
        if (this.eventSelEl) {
            webix.eventRemove(this.eventSelEl);
        }
        this.eventSelEl = webix.event(this._valueSelect, "change", (e) => {
            rule.key = this._valueSelect.getValue();
            if (rule.key === "not_selected") {
                this._changeRule("not_sel");
                if (this._ruleSelect) {
                    this._ruleSelect.hide();
                }
                if (this._ruleInput) {
                    this._ruleInput.hide();
                }
                this._getTopQuery().callEvent("onKeySelect", [this]);
                return;
            }
            rule.rule = undefined;
            rule.value = null;
            this._updateRules(rule);
            this._setRuleSelect(rule, options);
            this._getTopQuery().callEvent("onKeySelect", [this]);
        });
    },
    _setRuleSelect(rule, options, update) {
        this._ruleSelect.getList().clearAll();
        if (rule.key !== "not_selected") {
            let filters = this._setFilterRule(rule.key, options);
            let optionsList = [];
            filters.forEach((item, index, array) => {
                for (let key in locale) {
                    if (key === item.id) {
                        optionsList.push({ id: item.id, value: locale[key] });
                    }
                }
                this._ruleSelect.getList().parse(optionsList);
            });
            if (!rule.rule) {
                rule.rule = filters[0].id;
                rule.value = null;
            }
            this._ruleSelect.show();
            this._ruleSelect.$view.style.display = "";
            this._ruleSelect.setValue(rule.rule);
            if (!update) {
                this._setRuleEl(rule);
            }
            this._updateRules(rule);
        }
        if (!update) {
            if (this.eventSelRule) {
                webix.eventRemove(this.eventSelRule);
            }
            this.eventSelRule = webix.event(this._ruleSelect, "change", (e) => {
                this.config.filters.forEach((item) => {
                    for (let key in locale) {
                        if (key === item.id) {
                            if (locale[key] === this._ruleSelect.getList().data.pull[this._ruleSelect.getValue()].value) {
                                rule.rule = item.id;
                            }
                        }
                    }
                });
                this._setRuleEl(rule, options);
                this._updateRules(rule);
                if (rule.value || rule.value === 0) {
                    this._callChangeMethod();
                }
            });
        }
    },
    _setFilterRule(selectedItem, optionsArray) {
        let ruleTypesArray = [];
        for (let option in optionsArray) {
            if (optionsArray[option].id === selectedItem) {
                this._ruleType = optionsArray[option].type;
            }
        }
        for (let key in this.config.filters) {
            if (this._ruleType === "date" && this.config.filters[key].type === "number") {
                ruleTypesArray.push(this.config.filters[key]);
            }
            if (this.config.filters[key].type === this._ruleType || this.config.filters[key].type === "any") {
                ruleTypesArray.push(this.config.filters[key]);
            }
        }
        return ruleTypesArray;
    },
    _updateRules(rule) {
        if (rule === "not_sel") {
            this.config.value = null;
            return;
        }
        let obj = {};
        if (!this.config.value) {
            this.config.value = obj;
        }
        if (rule) {
            this.config.value = { key: rule.key, rule: rule.rule, value: rule.value };
        }
        return this.config.value;
    },
    _getTopQuery() {
        let parent = this._masterQuery;
        return parent._getTopQuery ? parent._getTopQuery() : parent;
    },
    _callChangeMethod() {
        this._getTopQuery().callEvent("onChange", []);
    },
    _changeRule(rule) {
        this._updateRules(rule);
        this._callChangeMethod();
    },
    _setRuleEl(rule) {
        let value = this.config.value.value;
        let customEl = [this._datepicker, this._datepickerRange, this._slider, this._inputText];
        customEl.forEach((item) => {
            if (item) {
                item.hide();
            }
        });
        if (rule.rule === "is_null" || rule.rule === "is_not_null") {
            rule.value = null;
            this._changeRule(rule);
            return;
        }
        else if (rule.rule === "is_not_empty" || rule.rule === "is_empty") {
            rule.value = "";
            this._changeRule(rule);
            return;
        }
        this.config.value = rule = this._setCustomRuleEl(rule, value);
        if (this._ruleInput) {
            this._ruleInput.show();
        }
        this.config.fields.forEach((item, index, array) => {
            if (item.validate && item.id === rule.key) {
                this._ruleInput.define("validate", item.validate);
                this._ruleInput.refresh();
            }
            else if (!item.validate && item.id === rule.key) {
                this.markInvalid(this._ruleInput.config.name, false);
            }
        });
        this._handleEvents(rule);
    },
    _setCustomRuleEl(rule, value) {
        let el = {
            minWidth: 100, maxWidth: this.config.inputMaxWidth, width: this.config.inputWidth,
            inputWidth: this.config.inputWidth, height: 38, inputHeight: 38, maxHeight: 38,
            name: "value"
        };
        if (rule && rule.rule) {
            if (!rule.key) {
                return;
            }
            if (this._ruleType === "date" && rule.rule !== "is_null" && rule.rule !== "is_not_null") {
                if (value && typeof value === "object" && value.length) {
                    value = "";
                }
                else if (!this.config.value.value) {
                    value = new Date();
                }
                if (rule.rule === "between" || rule.rule === "not_between") {
                    if (!this._datepickerRange) {
                        if (value && !value.start || value && !value.end) {
                            value = { start: value, end: value };
                        }
                        this._datepickerRange = webix.$$(this.addView(Object.assign({}, el, { view: "daterangepicker", value }), this.queryView({}, 'all').length - 2));
                    }
                    else if (this._datepickerRange) {
                        if (rule.value && rule.value.start) {
                            value = value;
                        }
                        else if (rule.value && rule.value.start === '' || !rule.value) {
                            value = { start: new Date(), end: new Date() };
                        }
                        else if (rule.value) {
                            value = { start: rule.value, end: rule.value };
                        }
                    }
                    this._ruleInput = this._datepickerRange;
                }
                else if (rule.rule !== "between" && rule.rule !== "not_between") {
                    if (value && value.end) {
                        value = value.start;
                    }
                    else if (!value || Array.isArray(rule.value)) {
                        value = new Date();
                    }
                    if (!this._datepicker) {
                        this._datepicker = webix.$$(this.addView(Object.assign({}, el, { view: "datepicker", value }), this.queryView({}, 'all').length - 2));
                    }
                    else if (this._datepicker) {
                        if (rule.value && rule.value.start) {
                            value = rule.value.start;
                        }
                        else if (rule.value && rule.value.start === '' || !rule.value) {
                            value = new Date();
                        }
                        else if (rule.value) {
                            value = rule.value;
                        }
                    }
                    this._ruleInput = this._datepicker;
                }
            }
            else if ((rule.rule === "between" || rule.rule === "not_between") && (this._ruleType !== "date")) {
                if (!this._slider) {
                    if (!value || !value.length) {
                        value = [[0, 0], [0, 100]];
                    }
                    else if (typeof value[1][0] === "undefined") {
                        value = [value, [0, 100]];
                    }
                    this._slider = webix.$$(this.addView(Object.assign({}, el, { view: "rangeslider", value: value[0], min: value[1][0], max: value[1][1], title(obj) {
                            let v = obj.value[0].length ? obj.value[0] : obj.value;
                            return (v[0] === v[1] ? v[0] : v[0] + " - " + v[1]);
                        } }), this.queryView({}, 'all').length - 2));
                }
                else if (this._slider) {
                    if (rule.value === 0 || (rule.value && !rule.value.length) || (!rule.value) || (!rule.value[1])) {
                        value = [[0, 0], [this._slider.config.min, this._slider.config.max]];
                    }
                    this._slider.define({ min: value[1][0], max: value[1][1] });
                }
                this._ruleInput = this._slider;
            }
            else {
                if (!this._inputText) {
                    this._inputText = webix.$$(this.addView(Object.assign({}, el, { view: "text", css: "webix_querybuilder_rule_input", type: "string" }), this.queryView({}, 'all').length - 2));
                    value = this._setInputType(value, rule);
                }
                value = this._setInputType(value, rule, true);
                this._ruleInput = this._inputText;
            }
            rule.value = value;
            this._updateRules(rule);
            if (Array.isArray(value)) {
                this._ruleInput.setValue(value[0]);
                return rule;
            }
            this._ruleInput.setValue(value);
        }
        return rule;
    },
    _setInputType(value, rule, input) {
        if (this._ruleType === "number") {
            if (typeof rule.value !== "number" || (!input)) {
                value = 0;
            }
            this._inputText.define("type", "number");
        }
        else {
            if (typeof rule.value !== "string" || (!input)) {
                value = "";
            }
            this._inputText.define("type", "string");
        }
        this._inputText.refresh();
        return value;
    },
    _handleEvents(rule) {
        // add listener to update rules onchange
        let timer;
        this.attachEvent("onDestruct", () => this._ruleInput.destructor());
        if (this._ruleInput) {
            if (this.eventObjInput) {
                this._ruleInput.detachEvent(this.eventObjInput);
            }
            if (this.eventObjPress) {
                this._ruleInput.detachEvent(this.eventObjPress);
            }
            this.eventObjInput = this._ruleInput.attachEvent("onChange", (newValue) => {
                if (this._ruleType === "number" && !Array.isArray(newValue)) {
                    newValue = Number(newValue);
                }
                if (this.config.value.value === newValue) {
                    return;
                }
                if (Array.isArray(newValue)) {
                    newValue = [newValue, [this._slider.config.min, this._slider.config.max]];
                }
                rule.value = newValue;
                this._changeRule(rule);
            });
            if (this._ruleInput.config.css === "webix_querybuilder_rule_input") {
                this.eventObjPress = this._ruleInput.attachEvent("onKeyPress", (e) => {
                    if (timer) {
                        clearTimeout(timer);
                    }
                    timer = setTimeout(() => {
                        if (this._ruleType === "number") {
                            rule.value = Number(this._inputText.getValue());
                        }
                        else {
                            rule.value = this._inputText.getValue();
                        }
                        this._changeRule(rule);
                    }, 250);
                });
            }
        }
    },
    _getValue() {
        return this.config.value;
    },
    setValue(value) {
        this.config.value = value;
        this._setPreselectedKeys(this.config.value);
    },
    getFilterHelper() {
        let result;
        let confValue = this.config.value;
        let confFilters = this.config.filters;
        let filterFunction = (obj) => {
            if (!confValue) {
                return true;
            }
            for (let filter in confFilters) {
                if (confFilters[filter].id === confValue.rule) {
                    if (confValue.value === null) {
                        result = confFilters[filter].fn(obj[confValue.key], confValue.value);
                    }
                    else if (this._ruleType === "date") {
                        let keyItem = obj[confValue.key] ? obj[confValue.key].getTime() : obj[confValue.key];
                        let confItem = confValue.value;
                        if (typeof confItem === 'object' && confItem.start) {
                            let confStart = confItem.start;
                            let confEnd = confItem.end;
                            confStart = (confStart ? confStart.getTime() : confStart);
                            confEnd = (confEnd ? confEnd.getTime() : confEnd);
                            result = confFilters[filter].fn(keyItem, confStart, confEnd);
                        }
                        else {
                            if (typeof confItem === 'string') {
                                confItem = webix.i18n.parseFormatDate(confItem);
                            }
                            confItem = confItem.getTime();
                            result = confFilters[filter].fn(keyItem, confItem);
                        }
                    }
                    else {
                        if (typeof confValue.value === 'string' && typeof obj[confValue.key] === 'string') {
                            result = confFilters[filter].fn(obj[confValue.key].toLowerCase(), confValue.value.toLowerCase());
                        }
                        else if (this._ruleType === 'number' && typeof obj[confValue.key] === 'number' && !Array.isArray(confValue.value)) {
                            result = confFilters[filter].fn(obj[confValue.key], Number(confValue.value));
                        }
                        else {
                            // range
                            result = confFilters[filter].fn(obj[confValue.key], confValue.value[0][0], confValue.value[0][1]);
                        }
                    }
                }
            }
            return result;
        };
        return filterFunction;
    }
}, webix.ui.form, webix.EventSystem);
