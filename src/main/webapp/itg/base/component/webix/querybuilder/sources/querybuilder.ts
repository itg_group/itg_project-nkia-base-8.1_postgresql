import * as types from "./interfaces";
import "./less/query.less";
import {locale} from "./locales";
import "./qbfilter";
import {qbsorting} from "./qbsorting";
import {qbsql} from "./qbsql";
import "./querybuilderline";

(webix.i18n as types.IExtendedi18n).querybuilder = locale;

let querybuilder = {
	name: "querybuilder",
	defaults: {
		type: "space",
		fields: [],
		sorting: false,
		filtering: true,
		columnMode: false,
		maxLevel: 999,
		inputMaxWidth: 210,
		inputWidth: false,
		borderless: true
	},
	$init():void {
		this.$view.className += " webix_querybuilder_wrap";
		let confValue:types.IQueryBuilderConfig = {
			glue: "and",
			rules: []
		};
		this.config.value = confValue;
		this.config.glue = "and";
		this.$ready.unshift(this._setLayout);
	},
	_filters:[
		{id: "less", name:  locale.less, fn: (a, b) => a < b, type: "number"},
		{id: "less_or_equal", name: locale.less_or_equal, fn: (a, b) => a <= b, type: "number"},
		{id: "greater", name: locale.greater, fn: (a, b) => a > b, type: "number"},
		{id: "greater_or_equal", name: locale.greater_or_equal, fn: (a, b) => a >= b, type: "number"},
		{id: "between", name: locale.between, fn: (a, b, c) => (!b || a > b) && (!c || a < c), type: "number"},
		{id: "not_between", name: locale.not_between, fn: (a, b, c) => (!b || a <= b) || (!c || a >= c), type: "number"},
		{id: "begins_with", name: locale.begins_with, fn: (a, b) => a.lastIndexOf(b, 0) === 0, type: "string"},
		{id: "not_begins_with", name: locale.not_begins_with, fn: (a, b) => a.lastIndexOf(b, 0) !== 0, type: "string"},
		{id: "contains", name: locale.contains, fn: (a, b) => a.indexOf(b) !== -1, type: "string"},
		{id: "not_contains", name: locale.not_contains, fn: (a, b) => b.indexOf(a) === -1, type: "string"},
		{id: "ends_with", name: locale.ends_with, fn: (a, b) => a.indexOf(b, a.length - b.length) !== -1, type: "string"},
		{id: "not_ends_with", name: locale.not_ends_with, fn: (a, b) => a.indexOf(b, a.length - b.length) === -1, type: "string"},
		{id: "is_empty", name: locale.is_empty, fn: (a) => a.length === 0, type: "string"},
		{id: "is_not_empty", name: locale.is_not_empty, fn: (a) => a.length > 0, type: "string"},
		{id: "equal", name: locale.equal, fn: (a, b) => a === b, type: "any"},
		{id: "not_equal", name: locale.not_equal, fn: (a, b) => a !== b, type: "any"},
		{id: "is_null", name: locale.is_null, fn: (a) => a === null, type: "any"},
		{id: "is_not_null", name: locale.is_not_null, fn: (a) => a !== null, type: "any"}
	],
	_deleteRow(el:types.IQueryLine):void {
		let layout = this.queryView({css:"webix_qb_section"});
		layout.removeView(el);
		this._callChangeMethod();

		if (layout.getChildViews().length <= 1) {
			let parentEl = this._masterQuery;
			if (parentEl){
				parentEl._deleteRow(this.config.id);
			}
		}
	},
	_addRow(ui:types.IQueryLine|types.IQueryBuilderConfig):string {
		let layout = this.queryView({ css:"webix_qb_section" });
		return layout.addView(ui, layout.getChildViews().length-1);
	},
	_addRule():string {
		let line:string = this._addRow({
			view: "querybuilderline",
			inputWidth:this.config.inputWidth, inputMaxWidth: this.config.inputMaxWidth,
			fields: this.config.fields,
			filters: this._filters,
			columnMode: this.config.columnMode
		});
		(webix.$$(line) as any)._masterQuery = this;
		return line;
	},
	_addGroup(withRow:boolean):string {
		let newView:string = this._addRow({
			view: "querybuilder",
			inputWidth:this.config.inputWidth,
			inputMaxWidth: this.config.inputMaxWidth,
			maxLevel:this.config.maxLevel-1,
			fields: this.config.fields,
			columnMode: this.config.columnMode
		});
		(webix.$$(newView) as any).setFilters(this._filters);
		(webix.$$(newView) as any)._masterQuery = this;

		if (withRow){
			(webix.$$(newView) as any)._addRule();
		}
		return newView;
	},
	_getTopQuery():types.IQueryBuilder {
		let parent:types.IQueryBuilder = this.getParentView();
		if(this._masterQuery && this._masterQuery._getTopQuery){
			parent = parent.getParentView();
			return parent._getTopQuery ? parent._getTopQuery() : parent;
		} else {
			return this;
		}
	},
	_callChangeMethod():void {
		this._getTopQuery().callEvent("onChange", []);
	},
	_setLayout():void {
		let levelIndicator:number|boolean = this.config.maxLevel > 1 ? true:false;

		let cols:any[] = [
		{
			template: `<div class="webix_querybuilder_ifbuttons">
				<button class="webix_querybuilder_and webix_active">${locale.and}</button>
				<button class="webix_querybuilder_or">${locale.or}</button>
			</div>`,
			onClick: {
				webix_querybuilder_or:(e) =>{
					this.config.glue = "or";
					this._setActiveButtons(this.$view);
				},
				webix_querybuilder_and:(e) =>{
					this.config.glue = "and";
					this._setActiveButtons(this.$view, "and");
				}
			},
			height: 34,
			width: 87
		},
		{
			css:"webix_qb_section",
			rows: [
			{
				template: `<div class="webix_querybuilder_buttons">
					${levelIndicator?`<button class="webixbutton webix_querybuilder_group">+ ${locale.add_group}</button>`:""}
					<button class="webixbutton webix_querybuilder_rule">+ ${locale.add_rule}</button>
				</div>`,
				onClick: {
					webix_querybuilder_rule:(e) => this._addRule(),
					webix_querybuilder_group:(e) => this._addGroup(true)
				},
				height: 14,
				minWidth: 220
			}],
			margin: 5
		}];

		if(this.config.sorting) {
			this.$view.innerHTML = "<div class='webix_querybuilder_sorting'></div>";

			webix.ui(this._sortMultiselect = {
				view:"multiselect",
				label:"Sort by",
				container:this.$view.childNodes[0],
				suggest: {
					body:{
						data:this.config.fields
					}
				},
				align: "right",
				width: 300,
				inputHeight: 38,
				labelWidth: 57,
				on: {
					onChange: () => {
						this._callChangeMethod();
					}
				}
			});

			webix.ui(this._sortSelect = {
				view:"select",
				container:this.$view.childNodes[0],
				options:["asc", "desc"],
				width: 60,
				inputHeight: 38,
				height: 38,
				on: {
					onChange: () => {
						if(this._getSortingValues().sortBy) {
							this._callChangeMethod();
						}
					}
				}
			});
			webix.extend(this, qbsorting);
		}

		if (this.config.filtering === false){
			this.config.padding = 0;
			this.cols_setter([{ height: 1}]);
		} else {
			this.cols_setter(cols);
		}
	},
	$getSize(dx:number, dy:number):number[]{
		if(this.config.sorting){
			dy = dy+50;
		}
		return webix.ui.layout.prototype.$getSize.call(this, dx, dy);
	},
	_checkItemRules(item):void {
		let layout:types.IQueryBuilder = this.queryView({css:"webix_qb_section"});

		if (item.glue && item.glue === "and") {
			this._setActiveButtons(this.$view, "and");
		} else if (item.glue && item.glue === "or") {
			this._setActiveButtons(this.$view);
		}
		if (item.rules) {
			item.rules.forEach(el => {
				let rule;
				if (!el.glue) {
					rule = this._addRule();
				} else {
					rule = this._addGroup();
				}
				(webix.$$(rule) as any).setValue(el);
			});
		}
	},
	_setActiveButtons(container:HTMLElement, and:boolean, item:types.IQueryBuilderConfig):void {
		let btnAnd:Element = container.querySelector(".webix_querybuilder_and");
		let btnOr:Element = container.querySelector(".webix_querybuilder_or");

		if(btnAnd) {
			btnAnd.classList.remove("webix_active");
			if (and) {
				btnOr.classList.remove("webix_active");
				btnAnd.className += " webix_active";
			} else {
				btnAnd.classList.remove("webix_active");
				btnOr.className += " webix_active";
			}
		}
		this._callChangeMethod();
	},
	_eachLine(cb){
		let layout:types.IQueryBuilder = this.queryView({ css:"webix_qb_section" });
		let cells:types.IQueryBuilder[] = layout.getChildViews();
		for (let i = 0; i < cells.length; i++) {
			if (cells[i].setFilters){
				cb(cells[i]);
			}
		}
	},
	setFilters(filters:types.IQueryFilter[]):void {
		this._filters = filters;
		this._eachLine(cell => cell.setFilters(filters));
	},
	validate() {
		this._eachLine(cell => cell.validate());
	},
	getFilters():types.IQueryFilter[] {
		return this._filters;
	},
	_getValue():types.qbValue {
		const rules = [];
		this._eachLine(a => {
			if(a._getValue(rules)){
				rules.push(a._getValue(rules))
			}
		});
		if(rules.length) {
			return {glue: this.config.glue, rules}
		} else {
			return null;
		}
	},
	getValue():types.qbValue {
		return [this._getValue(), this.config.fields]
	},
	setValue(value:types.qbValue):void {
		let firstValue: types.IFieldInfo | types.IQueryRule = value[0];
		let newValue;
		// check array elements: rules+fields/rules/fields
		if(value[1]) {
			newValue = firstValue;
			this.config.fields = value[1];
		} else if(!value[0]){
			newValue = value;
		} else if(!Array.isArray(firstValue)) {
			newValue = firstValue;
		} else {
			this.config.fields = firstValue;
		}
		this.reconstruct();

		if(newValue){
			this.config.glue = newValue.glue;
			this._checkItemRules(newValue);
		}

		if(this.config.sorting) {
			let fields = [].concat(this.config.fields).map(a => a.value);
			let list = this.getSortingElements()[0].getPopup().getList();
			list.clearAll();
			list.parse(fields);
		}
	},
	focus():void {
		let selectEl:HTMLElement = this.$view.querySelector(".webix_active");
		if(selectEl){
			selectEl.focus();
		}
	},
	getFilterHelper():types.FilterHelper {
		let result:boolean;
		let childsArr:types.FilterHelper[] = [];
		let glue:string = this.config.glue;


		this._eachLine(a => childsArr.push(a.getFilterHelper()));

		let filterFunction = (obj):boolean => {
			if(!childsArr.length) {
				return true;
			}
			if (glue === "and") {
				result = true;
				childsArr.forEach(function(item) {
					if (!item(obj)) {
						result = false;
					}
				});
			} else {
				result = false;
				childsArr.forEach(function(item) {
					if (item(obj)) {
						result = true;
					}
				});
			}
			return result;
		};
		return filterFunction;
	}
};

webix.protoUI(querybuilder, qbsql, webix.ui.layout, webix.EventSystem);