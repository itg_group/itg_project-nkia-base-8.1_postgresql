export let qbsql = {
    $init() {
        this.config.sqlDateFormat = this.config.sqlDateFormat || webix.Date.dateToStr("%Y-%m-%d %H:%i:%s", false);
    },
    sqlOperators: {
        equal: { op: '= ?' },
        not_equal: { op: '!= ?' },
        less: { op: '< ?' },
        less_or_equal: { op: '<= ?' },
        greater: { op: '> ?' },
        greater_or_equal: { op: '>= ?' },
        between: { op: 'BETWEEN ?', sep: ' AND ' },
        not_between: { op: 'NOT BETWEEN ?', sep: ' AND ' },
        begins_with: { op: 'LIKE(?)', mod: '{0}%' },
        not_begins_with: { op: 'NOT LIKE(?)', mod: '{0}%' },
        contains: { op: 'LIKE(?)', mod: '%{0}%' },
        not_contains: { op: 'NOT LIKE(?)', mod: '%{0}%' },
        ends_with: { op: 'LIKE(?)', mod: '%{0}' },
        not_ends_with: { op: 'NOT LIKE(?)', mod: '%{0}' },
        is_empty: { op: '= \"\"', no_val: true },
        is_not_empty: { op: '!= \"\"', no_val: true },
        is_null: { op: 'IS NULL', no_val: true },
        is_not_null: { op: 'IS NOT NULL', no_val: true }
    },
    toSQL(config, value) {
        value = value || this.getValue()[0];
        config = config || { placeholders: false };
        const values = [];
        const code = this._getSqlString(value, values, config);
        return { code, values };
    },
    _getSqlString(rulesObj, values, config, glueRule) {
        if (!rulesObj) {
            return "";
        }
        if (rulesObj.glue) {
            let code = "";
            rulesObj.rules.forEach((item, index, array) => {
                code += (index < array.length - 1) ? this._getSqlString(item, values, config, rulesObj.glue.toUpperCase()) : this._getSqlString(item, values, config, 'last');
            });
            return glueRule ? this._putBrackets(code, glueRule) : code;
        }
        else {
            this._convertValueToSql(rulesObj, values);
            return glueRule !== 'last' ? `${rulesObj.key} ${this._convertRuleToSql(rulesObj, config)} ${glueRule} ` : `${rulesObj.key} ${this._convertRuleToSql(rulesObj, config)}`;
        }
    },
    _putBrackets(ruleString, glue) {
        return glue !== 'last' ? `( ${ruleString} ) ${glue.toUpperCase()} ` : `( ${ruleString} )`;
    },
    _convertValueToSql(el, values) {
        const format = this.config.sqlDateFormat;
        for (let key in this.sqlOperators) {
            if (key === el.rule) {
                let operatorsItem = this.sqlOperators[key];
                let value;
                if (operatorsItem.no_val) {
                    return;
                }
                else if (operatorsItem.mod) {
                    value = operatorsItem.mod.replace("{0}", `${el.value}`);
                }
                else {
                    if (Array.isArray(el.value)) {
                        for (let i = 0; i < el.value.length; i++) {
                            if (Array.isArray(el.value[i])) {
                                el.value = el.value[0];
                            }
                            values.push(el.value[i]);
                        }
                        return;
                    }
                    else if (Object.prototype.toString.call(el.value) === '[object Date]' || el.value.start) {
                        if (el.value.start && el.value.start.getFullYear) {
                            let start = [el.value.start, el.value.end].map((item) => format(item));
                            for (let i = 0; i < start.length; i++) {
                                values.push(start[i]);
                            }
                            return;
                        }
                        else if (Object.prototype.toString.call(el.value) === '[object Date]') {
                            el.value = format(el.value);
                        }
                    }
                    value = el.value;
                }
                values.push(value);
            }
        }
    },
    _convertRuleToSql(el, config) {
        const format = this.config.sqlDateFormat;
        for (let key in this.sqlOperators) {
            if (key === el.rule) {
                let operatorsItem = this.sqlOperators[key];
                let operator = this.sqlOperators[key].op;
                if (!config.placeholders) {
                    if (operatorsItem.mod) {
                        operator = operator.replace("?", `"${operatorsItem.mod.replace("{0}", `${el.value}`)}"`);
                    }
                    else if (operatorsItem.sep) {
                        if (Array.isArray(el.value)) {
                            operator = operator.replace("?", `${el.value[0]}${operatorsItem.sep}${el.value[1]}`);
                        }
                        else if (el.value.start) {
                            operator = operator.replace("?", `"${format(el.value.start)}"${operatorsItem.sep}"${format(el.value.end)}"`);
                        }
                    }
                    else {
                        operator = operator.replace("?", typeof el.value === "string" ? `"${el.value}"` : `${el.value}`);
                    }
                }
                else {
                    if (operatorsItem.sep) {
                        operator = operator.replace("?", `?${operatorsItem.sep}?`);
                    }
                }
                return operator;
            }
        }
    }
};
