import "./less/query.less";
import { locale } from "./locales";
webix.ui.datafilter.queryBuilder = webix.extend({
    getValue() {
        let value = [];
        value.push(...this._qb.getValue());
        if (this._qb._getSortingValues) {
            value.push(this._qb._getSortingValues());
        }
        return value;
    },
    setValue(node, value) {
        if (value) {
            this._qb.setValue(value);
            if (this._qb.config.sorting) {
                this._qb._setSortingValues(value[2]);
                this._master.sort(this._qb.getSortingHelper());
            }
        }
    },
    refresh(master, node, value) {
        node.component = master.config.id;
        master.registerFilter(node, value, this);
        node._comp_id = master.config.id;
        if (value.value && (JSON.stringify(this.getValue(value)[0]) !== JSON.stringify(value.value[0]))) {
            this.setValue(node, value.value);
            if (!this._qb.config.sorting) {
                this._master.filterByAll();
            }
        }
        webix.event(node, "click", () => this._filterShow(node));
    },
    compare(el, rules, obj) {
        return this._qb.getFilterHelper()(obj);
    },
    render(master, config) {
        config.css = "webix_ss_filter";
        config.compare = (el, rules, obj) => this.compare(el, rules, obj);
        let buttonSort = {};
        let qb = {
            view: "querybuilder", fields: config.fields || [], sorting: config.sorting || false, filtering: config.filtering === false ? false : true,
            columnMode: config.columnMode || false, maxLevel: config.maxLevel || 999, inputMaxWidth: config.inputMaxWidth || 210, inputWidth: config.inputWidth || false,
            borderless: config.borderless === false ? false : true
        };
        let buttonSave = this._buttonCreate(locale.filter, () => {
            if (this._qb) {
                let helper = this._qb.getFilterHelper();
                master.filter(helper, undefined, undefined);
                this._popup.hide();
            }
        });
        if (config.sorting) {
            buttonSort = this._buttonCreate(locale.sort, () => {
                if (this._qb) {
                    master.sort(this._qb.getSortingHelper());
                    this._popup.hide();
                }
            });
        }
        let buttonCancel = this._buttonCreate(locale.cancel, () => {
            this._popup.hide();
        });
        let body = { margin: 5, rows: [qb, { cols: [buttonSave, buttonCancel, {}, buttonSort] }] };
        let popup = {
            view: "popup",
            width: 1280,
            body
        };
        if (config.popupConfig) {
            webix.extend(popup, config.popupConfig, true);
        }
        this._popup = webix.ui(popup);
        this._qb = this._popup.getBody().getChildViews()[0];
        master.attachEvent("onDestruct", () => {
            this._popup.destructor();
        });
        this._master = master;
        return '<div class="webix_qb_filter"><i class="fa fa-filter" aria-hidden="true"></i></div>' + (config.label || "");
    },
    _filterShow(node) {
        this._popup.show(node.querySelector(".webix_qb_filter .fa"));
    },
    _buttonCreate(label, click) {
        return {
            view: "button",
            value: label,
            align: "right",
            width: 120,
            click
        };
    }
}, webix.EventSystem);
