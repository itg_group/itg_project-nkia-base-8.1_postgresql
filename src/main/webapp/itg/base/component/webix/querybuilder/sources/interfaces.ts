export interface IQueryBuilderConfig {
	rules: (IQueryRule | IQueryBuilderConfig)[];
	glue: "or" | "and";
}

export type FieldType = "number" | "string" | "date";

export interface IFieldInfo {
	id: string;
	value ? : string; // optional, text label
	type ? : FieldType; // optional, FieldType.String by default
	validate ? : string|boolean;
}

export interface IQueryRule {
	key?: string;
	rule?: string;
	value?: string | number | Idate | string[] | number[]| number[][];
}

export interface Idate {
	start: object | string;
	end: object | string;
	getTime();
}

export interface IQuerySubRule {
	rules: (IQueryRule | IQuerySubRule)[];
}

export type IRuleTypes = "contains" | "equal" | "notequal" | "less" | "greater";

export interface IQueryFilter {
	id: string;
	name: string;
	fn: (a: any, b: any, c?: any) => boolean;
	type: string;
}

export type FilterHelper = (obj: any) => boolean
export type SortingHelper = (obj1: any, obj2: any) => number

export interface IExtendedi18n extends webix.i18n {
	querybuilder: ILocales;
}

export interface ILocales {
	or: string;
	and: string;
	delete_rule: string;
	add_rule: string;
	add_group: string;
	less: string;
	less_or_equal: string;
	greater: string;
	greater_or_equal: string;
	between: string;
	not_between: string;
	begins_with: string;
	not_begins_with: string;
	contains: string;
	not_contains: string;
	ends_with: string;
	not_ends_with: string;
	is_empty: string;
	is_not_empty: string;
	equal: string;
	not_equal: string;
	is_null: string;
	is_not_null: string;
	default_option: string;
	cancel: string;
	filter: string;
	sort: string;
}

export interface IConfig {
	value: IQueryBuilderConfig;
	id: string;
}


export interface IQueryBuilder extends webix.ui.layout {
	config: IConfig;
	childsIdArray:string[];
	_getTopQuery();
	// api of form input
	setValue(config: IQueryBuilderConfig)
	getValue():IQueryBuilderConfig;
	focus();
	setFilters(filters: IQueryFilter[]);
	getFilters():IQueryFilter;

	// create a filter function from the provided config
	getFilterHelper():FilterHelper;
}

export interface IQueryLine extends webix.ui.view {
	// api of form input
	setValue(config: IQueryRule)
	getValue():IQueryRule;
	focus();
	setFilters(filters: IQueryFilter[]);
	validate();
	// create a filter function from the provided config
	getFilterHelper():FilterHelper;
}

export interface IQuerybuilderConfig {
	view: string;
	fields: [IFieldInfo];
	sorting: boolean;
	filtering: boolean;
	columnMode: boolean;
	maxLevel: number;
	inputMaxWidth: number;
	inputWidth: boolean;
	borderless: boolean;
}

export interface ISorting {
	sortBy: string;
	sortAs: string;
}

export interface IFilter extends HTMLElement {
	component: string | number;
	_comp_id: string | number;
	compare();
}

export interface IDatatable extends webix.ui.datatable {
	_settings: {id: string | number};
}

export interface Ioperator {
	op: string;
	sep?: string;
	mod?: string;
	no_val?: boolean;
}

export type qbValue = IQueryBuilderConfig | IFieldInfo[];