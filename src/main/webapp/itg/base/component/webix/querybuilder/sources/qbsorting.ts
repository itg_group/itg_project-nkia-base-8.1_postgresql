import * as types from "./interfaces";

export let qbsorting = {
	_getSortingValues():types.ISorting {
		let multiValue:string = this.getSortingElements()[0].getValue();
		let selectValue:string = this.getSortingElements()[1].getValue();
		return {sortBy: multiValue, sortAs: selectValue}
	},
	_setSortingValues(sortingValues:types.ISorting):void {
		this.getSortingElements()[0].setValue(sortingValues.sortBy);
		this.getSortingElements()[1].setValue(sortingValues.sortAs);
	},
	getSortingElements():(webix.ui.multiselect | webix.ui.select)[] {
		let multiSortEl:webix.ui.multiselect = (webix.$$(this._sortMultiselect.id) as webix.ui.multiselect);
		let selectSortEl:webix.ui.select = (webix.$$(this._sortSelect.id) as webix.ui.select);
		return [multiSortEl, selectSortEl]
	},
	getSortingHelper():types.SortingHelper {
		let multiValue:string[] = this._getSortingValues().sortBy.split(",");
		let sortByItem = [];
		for(let i=0; i < multiValue.length; i++) {
			for (let j = 0; j < this.config.fields.length; j++) {
				if(this.config.fields[j].id === multiValue[i]) {
					sortByItem.push(this.config.fields[j])
				}
			}
		}
		this._i = 0;

		let sortingFunction = (obj1, obj2):number => {
			if(this._i === 0 && sortByItem.length) {
				return this._getValueSort(obj1, obj2, sortByItem);
			} else if(this._sortingValue && !sortByItem.length) {
				return this._getValueSort(obj1, obj2);
			}
		}
		return sortingFunction;
	},
	_getValueSort(obj1:object, obj2:object, sortByItem:types.IFieldInfo[]):number {
		let selectValue:string = this._getSortingValues().sortAs;
		let value;

		if(selectValue === "asc" || !selectValue) {
			value = this._getsorted(obj1, obj2, sortByItem);
		} else {
			value = this._getsorted(obj1, obj2, sortByItem)*-1;
		}
		return value;
	},
	_getsorted(obj1:object, obj2:object, sortByItem:types.IFieldInfo[]):number {
		let value;
		let item;

		if(sortByItem) {
			item = sortByItem[this._i];
			this._sortingValue = {id: item.id, type: item.type};
		} else {
			item = this._sortingValue;
		}

		let a = obj1[item.id];
		let b = obj2[item.id];
		if(item.type === "number") {
			value = a>b?1:(a<b?-1:0);
		} else if(item.type === "date"){
			a = a-0;
			b = b-0;
			value = a>b?1:(a<b?-1:0);
		} else {
			if (!b) {
				return 1;
			}
			if (!a) {
				return -1;
			}
			a = a.toString().toLowerCase();
			b = b.toString().toLowerCase();
			value = a>b?1:(a<b?-1:0);
		}
		if(sortByItem && sortByItem[this._i+1] && a === b) {
			this._i += 1;
			value = this._getValueSort(obj1, obj2, sortByItem);
		} else {
			this._i = 0;
		}
		return value;
	}
}