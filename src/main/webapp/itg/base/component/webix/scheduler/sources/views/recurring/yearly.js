webix.protoUI({
	name:"yearly",
	defaults:{
		css: "webix_yearly"
	},
	setValue:function(data){
		if(typeof data == "object"&&data[0]=="year"){
			var topParent = this.getTopParentView();
			if(!data[1]&&data[1]==="")
				data[1] = 1;
			if(data[2]&&data[2]!=="")
				topParent.$$("typeWeekY").setValue(1);
			else{
				topParent.$$("typeWeekY").setValue(0);
				data[2] = (data[5].getDay()+6)%7+1;
				data[3] = Math.ceil(data[5].getDate()/7);
			}
			topParent.$$("typeWeekY").callEvent("onItemClick",[]);
			this.setValues({yearCount:data[1],weekDayY:data[2],weekCountY:data[3],monthDayY:data[5].getMonth()});
		}
	},
	getValue:function(){
		var data = this.getValues();
		var res = ["year",data.yearCount,"","","",""];
		var topParent = this.getTopParentView();
		if(topParent.$$("typeWeekY").getValue()){
			res[2] = data.weekDayY;
			res[3] = data.weekCountY;
		}
		res[5] = data.monthDayY;
		return res;
	}
}, webix.ui.form);