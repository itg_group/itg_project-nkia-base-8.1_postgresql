webix.protoUI({
	name:"datetext",
	defaults:{
		/*template:function(obj,common){
			return common._render_div_block(obj, common);
		},*/
		icon: "angle-right"
	},
	$renderIcon:function(){
		var config = this.config,
			height = config.aheight - 2*config.inputPadding,
			padding = (height - 18)/2-1;
		return config.icon?("<span style=height:"+(height-padding)+"px;padding-top:"+padding+"px;' class='webix_input_icon  fa-"+config.icon+"'></span>"):"";
	},
	$init:function(){
		this.attachEvent("onAfterRender", webix.once(this._after_render, this));
	},
	_after_render:function(){
		webix.event(this.getInputNode(), "keydown", function(e){
			var code = e.which || e.keyCode;
			if(code == 32 || code == 13 || code ==39) //space, enter, right
				webix.html.triggerEvent(document.activeElement, "MouseEvents", "click");
		}, this);
	},
	setValue:function(value, forceReset){
		var oldvalue = this.config.value;
		var changed = true;
		if (oldvalue == value){
			changed = false;
			if(!forceReset)
				return false;
		}

		this.config.value = value;
		if (this.getInputNode())
			this.$setValue(value);

		if(changed)
			this.callEvent("onChange", [value, oldvalue]);
	},
	$setValue:function(value){
		this.config.value = (value||"");
		var format = (this.config.dateFormatStr||webix.i18n.dateFormatStr);
		this.config.text = this.getInputNode().innerHTML = (typeof this.config.value=="object"?(format(this.config.value)):this.config.value);
	},
	dateFormat_setter:function(value){
		this.config.dateFormatStr = webix.Date.dateToStr(value);
		return webix.Date.strToDate(value);
	},
	getValue:function(){
		return this.config.value||null;
	},
	getInputNode: function(){
		return this.$view.getElementsByTagName("DIV")[1];
	}
}, webix.ui.richselect);