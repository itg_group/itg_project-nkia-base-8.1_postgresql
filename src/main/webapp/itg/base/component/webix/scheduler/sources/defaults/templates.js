export const values = {
	day_event_style: function(obj){
		var style = "";
		if(obj.color){
			var rgb = webix.color.toRgb(obj.color);
			style += ";border-color:"+obj.color+";";
			style += "background-color:rgba("+rgb.toString()+",0.3);";
			var hsv = webix.color.rgbToHsv(rgb[0],rgb[1],rgb[2]);
			hsv[2] /= 1.6;
			var color = "rgb("+webix.color.hsvToRgb(hsv[0],hsv[1],hsv[2])+")";
			style += "color:"+color;
		}
		return style;
	},
	multi_day_event_style: function(obj){
		var style = "";
		if(obj.color){
			var rgb = webix.color.toRgb(obj.color);

			style += ";background-color:rgba("+rgb.toString()+",0.3);";
			var hsv = webix.color.rgbToHsv(rgb[0],rgb[1],rgb[2]);
			hsv[2] /= 1.6;
			var color = "rgb("+webix.color.hsvToRgb(hsv[0],hsv[1],hsv[2])+")";
			style += "color:"+color;
		}
		return style;
	},
	selected_event : function(obj){
		var html = "", fts="", fte="";
		var start = obj.start_date;
		var end = obj.end_date;

		if(!start) return html;
		html += "<div  class='selected_event "+scheduler.templates.event_class(obj)+"'  >";
		html += "<div class='event_title'>"+obj.text+"</div>";

		if(webix.Date.datePart(start,true).valueOf()==webix.Date.datePart(end,true).valueOf()){
			var fd = webix.i18n.dateFormatStr(start);
			fts = webix.i18n.timeFormatStr(start);
			fte = webix.i18n.timeFormatStr(end);
			html += "<div class='event_text'>"+fd+"</div>";
			html += "<div class='event_text'>"+scheduler.locale.labels.label_from+" "+fts+" "+scheduler.locale.labels.label_to+" "+fte+"</div>";
		}
		else{
			var fds = webix.i18n.longDateFormatStr(start);
			var fde = webix.i18n.longDateFormatStr(end);
			/*if not "all-day" event*/
			if(!(webix.Date.datePart(start,true).valueOf()==start.valueOf()&&webix.Date.datePart(end,true).valueOf()==end.valueOf())){
				fts = webix.i18n.timeFormatStr(start)+" ";
				fte = webix.i18n.timeFormatStr(end)+" ";
			}
			html += "<div class='event_text'>"+scheduler.locale.labels.label_from+" "+fts+fds+"</div>";
			html += "<div class='event_text'>"+scheduler.locale.labels.label_to+" "+fte+fde+"</div>";
		}
		if(obj.details&&obj.details!==""){
			html += "<div class='event_title'>"+scheduler.locale.labels.label_details+"</div>";
			html += "<div class='event_text'>"+obj.details+"</div>";
		}
		html += "</div>";
		return html;
	},
	week_title: function(date){
		var start = webix.Date.weekStart(date);
		var end = webix.Date.add(start,6,"day",true);
		return "<div aria-live='assertive' aria-atomic='true'>"+webix.i18n.headerWeekFormatStr(start)+"&nbsp;-&nbsp;"+webix.i18n.headerWeekFormatStr(end)+"</div>";
	},
	day_title:function(date){
		return "<div aria-live='assertive' aria-atomic='true'>"+webix.i18n.headerFormatStr(date)+"</div>";
	},
	calendar_event : function(day){
		return "<div class='webix_cal_day_event'>"+day.getDate()+"</div>"+"<div class='webix_cal_event_marker'></div>";
	},
	calendar_day : function(day){
		return day.getDate();
	},
	event_date: function(value){
		var date = new Date(parseInt(value,10));
		var isCurrent = (webix.Date.datePart(date).valueOf() == webix.Date.datePart(new Date()).valueOf());
		return "<span class='webix_unit_header_inner"+(isCurrent?" today":"")+"'>"+webix.i18n.weekDateFormatStr(date)+"</span>";
	},
	event_time : function(obj){
		var start = obj.start_date;
		var end = obj.end_date;
		if(!(webix.Date.datePart(start,true).valueOf()==start.valueOf()&&webix.Date.datePart(end,true).valueOf()==end.valueOf()))
			return webix.i18n.timeFormatStr(start);
		else
			return scheduler.locale.labels.label_allday;
	},
	event_marker : function(obj){
		var style = "";
		if(obj.color){
			var rgb = webix.color.toRgb(obj.color);
			style += ";border-color:"+obj.color+";";
			style += "background-color:rgba("+rgb.toString()+",0.9);";

		}
		return "<div class='webix_event_marker' ><div style='"+style+"'></div></div>";
	},
	event_title: function(obj,type){
		return "<div class='webix_event_overall "+type.eventClass(obj,type)+"'><div class='webix_event_time'>"+type.timeStart(obj)+"</div>"+type.marker(obj,type)+"<div class='webix_event_text' "+(obj.textColor?"style='color:"+obj.textColor:"")+"'>"+obj.text+"</div></div>";
	},
	month_event_title : function(obj,type){
		return "<div class='webix_event_overall "+type.eventClass(obj,type)+"'><div class='webix_event_time'>"+type.timeStart(obj)+"</div>"+type.marker(obj,type)+"<div class='webix_event_text' "+(obj.textColor?"style='color:"+obj.textColor:"")+"'>"+obj.text+"</div></div>";
	},
	day_event: function(obj){
		return obj.text;
	},
	multi_day_event: function(obj){
		return obj.text;
	},
	new_event_data: function(){
		var hours = (webix.Date.add(new Date(),1,"hour",true)).getHours();
		var start = webix.Date.copy(this.coreData.getValue());
		start.setHours(hours);
		var end = webix.Date.add(start,1,"hour",true);
		return {start_date:start,end_date:end};
	},
	event_class: webix.template("")
};