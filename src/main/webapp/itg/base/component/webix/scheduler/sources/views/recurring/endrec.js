webix.protoUI({
	name:"endrec",
	setValue: function(value){
		var skipEvent = false;
		var oldvalue = this.config.value;
		if (oldvalue == value){
			skipEvent = true;
		}
		else{
			this.config.value = value;
		}
		if (this.getInputNode())
			this.$setValue(value);
		if(!skipEvent)
			this.callEvent("onChange", [value, oldvalue]);
	},
	$setValue:function(date){
		this.config.value = (date||"");
		var recView = this.getTopParentView().$$("recurring");
		var text;
		if(typeof date=="object"){
			if(date.valueOf()==recView.config.endlessDate.valueOf())
				text = scheduler.locale.labels.recurring.never;
			else
				text = (this.config.dateFormatStr||webix.i18n.dateFormatStr)(this.config.value);
		}
		this.config.text = this.getInputNode().innerHTML = (text||this.config.value);
	}
}, webix.ui.datetext);