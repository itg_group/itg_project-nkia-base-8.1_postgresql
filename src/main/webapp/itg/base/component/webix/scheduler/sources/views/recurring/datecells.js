webix.protoUI({
	name:"datecells",
	defaults:{
		css: "webix_datecells",
		x_count:7,
		cellHeight:36
	},
	_id:"webix_d_c",
	$init:function(){
		this._dataobj = this.$view;
	},
	on_click:{
		webix_cal_day_num:function(e,id){
			if(id)
				this.define("activeCell",id);
		}
	},
	activeCell_setter:function(id){
		this.config.activeCell = id;
		this.render();
		return id;
	},
	render:function(){
		if(this.isVisible(this.config.id)){
			var c = 1;
			var html = "<table cellspacing='0' cellpadding='0' class='webix_rec_table'><tbody>";
			var obj = this.config.data;
			var cellWidth = (this.$width/this.config.x_count);

			for(var i in obj){
				var style = "";
				var className = "webix_cal_day_num";

				style += ";height:"+this.config.cellHeight+"px;line-height:"+this.config.cellHeight+"px;";
				if(c%this.config.x_count==1)
					html += "<tr>";
				html += "<td style='"+(cellWidth?("width:"+cellWidth+"px"):"")+"'>";

				if(this.config.activeCell==i)
					className+=" webix_cal_selected_day";

				html += "<div webix_d_c='"+i+"' class='"+className+"' style='"+style+"'>"+obj[i]+"</div>";
				html += "</td>";
				if(c%this.config.x_count===0)
					html += "</tr>";
				c++;
			}
			html += "</tbody></table>";
			this.$view.innerHTML = html;
		}
	},
	$setSize:function(x,y){
		if(webix.ui.view.prototype.$setSize.call(this,x,y)){
			this.render();
		}
	},
	setValue:function(value){
		this.define("activeCell",value);
	},
	getValue:function(){
		return this.config.activeCell;
	}
}, webix.MouseEvents, webix.ui.view, webix.EventSystem);