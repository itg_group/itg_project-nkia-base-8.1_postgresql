export const values = {
	init_date : new Date(),
	form_date : "%d-%m-%Y %H:%i",
	form_all_day : "%d-%m-%Y",
	parse_date : "%Y-%m-%d %H:%i",
	week_date: "%D, %d %M",
	dyn_load_date : "%Y-%m-%d",
	item_date : "%d.%m.%Y",
	all_day_date : "%l, %d %F %Y",
	day_header_date : "%d.%m.%Y",
	week_header_date : "%d.%m.%Y",
	hour_date : "%H:%i",
	scale_hour : "%H",
	calendar_date : "%F %Y",
	calendar_hour : "%H:%i",
	form_rules : {
		end_date:function(value,obj){
			return (obj.start_date.valueOf() < value.valueOf());
		}
	},
	multi_day: true,
	multi_day_limit: 3,
	views : [],
	tab_views : [],
	server_utc: false,
	separate_short_events: true,
	recurring: false,
	endless_date: new Date(9999,1,1),
	recurring_dialogs: false
};

export function init(){
	var config  = scheduler.config;
	var labels = scheduler.locale.labels;

	if(!config.form){
		config.form = [
			{view:"text",		label:labels.label_event, labelWidth: 90,	name:"text"},
			{view:"datetext",	label:labels.label_start, labelWidth: 90,	id:"start_date",name:"start_date", dateFormat:config.form_date},
			{view:"datetext",	label:labels.label_end, labelWidth: 90,	id:"end_date",name:"end_date", dateFormat:config.form_date},
			{view:"checkbox",	id:"allDay", name:"allDay", label:labels.label_allday, labelWidth: 100,  value:0},
			{view:"textarea",	label:labels.label_details,	name:"details", labelWidth: 90,		height:150},
			{}
		];
	}
	if(!config.calendar_hour){
		var hourPart = config.hour_date.match(/%[h,H,g,G]/gi);
		var aPart = config.hour_date.match(/%[a,A]/gi);
		config.calendar_hour = hourPart[0]+(aPart?" "+aPart[0]:"");
	}
	if(!config.bottom_toolbar){
		config.bottom_toolbar = [
			{ view:"button", id:"today", name: "today", type:"flat", label:labels.icon_today, inputWidth:scheduler.xy.icon_today, align:"center", width:scheduler.xy.icon_today+6},
			{ view:"segmented", id:"buttons",value:(config.mode||"week"),align:"center", multiview:true, options:[
				{id:"day", value:labels.day_tab, width:scheduler.xy.day_tab},
				{id:"week", value:labels.week_tab, width:scheduler.xy.week_tab},
				{id:"month", value:labels.month_tab, width:scheduler.xy.month_tab}
			]},
			{ view:"ariabutton", label:labels.icon_add, type:"icon", icon:"plus", name:"add", id:"add", align:"center",width:45, batch:"default"},
			{ view:"label", label:"", width:45, batch:"readonly"}
		];
	}
	if(!config.day_toolbar){
		config.day_toolbar = [
			{view:"ariabutton", label:labels.icon_prev_day, type:"iconprev", id:"prev", align:"left", width: 40},
			{view:"label", id:"date", align:"center"},
			{view:"ariabutton", label:labels.icon_next_day, type:"iconnext", id:"next", align:"right", width: 40}
		];
	}
	if(!config.week_toolbar){
		config.week_toolbar = [
			{view:"ariabutton", label:labels.icon_prev_week, type:"iconprev", name: "prevWeek", id:"prevWeek", width: 40, align:"left"},
			{view:"label", id:"weekTitle", align:"center"},
			{view:"ariabutton", label:labels.icon_next_week, type:"iconnext", id:"nextWeek", align:"right", width: 40}
		];
	}
	if(!config.selected_toolbar){
		config.selected_toolbar = [
			{view:"button", width:scheduler.xy.icon_back, type:"flat", name:"back", id:"back", align:"center", label:labels.icon_back},
			{view:"button",  inputWidth:scheduler.xy.icon_edit, name:"edit", id:"edit", align:"right", label:labels.icon_edit}
		];
	}
	if(!config.form_toolbar){
		config.form_toolbar = [
			{view:"button", type:"flat", width:scheduler.xy.icon_cancel, name:"cancel", id:"cancel",align:"center", label:labels.icon_cancel},
			{view:"button", inputWidth:scheduler.xy.icon_save, name:"save", id:"save", align:"right", label:labels.icon_save}
		];
	}
	if(!config.date_toolbar){
		config.date_toolbar = [
			{view:"button", type:"flat", width:scheduler.xy.icon_cancel, name:"cancel_date", id:"cancel_date", align:"center", label:labels.icon_cancel},
			{view:"label", id:"datetype", align:"center"},
			{view:"button", width: scheduler.xy.icon_done, name:"done", id:"done", align:"right", label:labels.icon_done}
		];
	}
}