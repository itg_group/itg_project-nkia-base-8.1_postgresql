webix.protoUI({
	name:"weeklist",
	defaults:{
		css: "webix_weeklist",
		elementsConfig: {labelWidth: 2},
		rows:[
			{view:"checkbox",name: 1, id:1,labelRight:webix.i18n.calendar.dayFull[1]},
			{view:"checkbox",name: 2,id:2,labelRight:webix.i18n.calendar.dayFull[2]},
			{view:"checkbox",name: 3,id:3,labelRight:webix.i18n.calendar.dayFull[3]},
			{view:"checkbox",name: 4,id:4,labelRight:webix.i18n.calendar.dayFull[4]},
			{view:"checkbox",name: 5,id:5,labelRight:webix.i18n.calendar.dayFull[5]},
			{view:"checkbox",name: 6,id:6,labelRight:webix.i18n.calendar.dayFull[6]},
			{view:"checkbox",name: 7,id:7,labelRight:webix.i18n.calendar.dayFull[0]}
		]
	},
	getValue:function(){
		var values = this.getValues();
		var result = [];
		for(var d in values)
			if(values[d])
				result.push(d);
		return result.join();
	},
	setValue:function(values){
		var result={};
		for(let i=1;i < 8;i++)
			result[i] =0;
		if(typeof values!="object")
			values = values.split(",");
		for(let i=0; i< values.length;i++)
			result[parseInt(values[i],10)] = 1;
		this.setValues(result);
	}
}, webix.MouseEvents, webix.EventSystem, webix.ui.form);