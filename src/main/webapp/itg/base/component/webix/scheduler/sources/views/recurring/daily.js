webix.protoUI({
	name:"daily",
	setValue:function(data){
		if(typeof data == "object"&&data[0]=="day"){
			if(!data[1]||data[1]==="")
				data[1] = 1;
			this.setValues({dayCount:data[1]});
		}
	},
	getValue:function(){
		var data = this.getValues();
		return ["day",data.dayCount,"","","",""];
	}
}, webix.ui.form);