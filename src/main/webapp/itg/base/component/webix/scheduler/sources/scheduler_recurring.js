export var code = function(){

	var labels = scheduler.locale.labels;
	var xy = scheduler.xy;
	var config = scheduler.config;
	var templates = scheduler.templates;


	webix.attachEvent("onAfterSchedulerInit", function(scheduler){
		if(!config.recurring) return false;

		webix.delay(function(){
			scheduler.$$("editForm").define("dataFeed", function(data){
				data = webix.copy(this.getTopParentView().getItem(data));
				delete data.id;
				if (data.rec_type){
					data.endRepeat = data.end_date;
					data.end_date = new Date(data.start_date.valueOf()+data.event_length*1000);
				}
				this.setValues(data);
			});	
			scheduler.$$("editForm").getValueBase = scheduler.$$("editForm").getValues;
			scheduler.$$("editForm").getValues = function(){
				var data = this.getValueBase();
				if (data.rec_type){
					data.event_length = Math.round((data.end_date - data.start_date) / 1000);
					data.end_date = data.endRepeat||data.end_date;
				}
				return data;
			};
		});

		scheduler.getEventsBase = scheduler.getEvents;
		scheduler.getEvents = function(from,to){
			to = to||webix.Date.add(from, 1, "month",true);

			var evs = this.getEventsBase(from, to);
			var out = [];
			for (var i = 0; i < evs.length; i++){
				if (evs[i].rec_type)
					this.repeat_date(evs[i], out, from, to);
				else
					out.push(evs[i]);
			}

			out.sort(function(a,b){
				a = a.start_date;
				b = b.start_date;
				return a>b?1:(a<b?-1:0);
			});
			return out;
		};
		scheduler._saveEventBase = scheduler._saveEvent;
		scheduler._saveEvent = function(){
			if(!this.config.editEvent && this.$$("editForm").validate()){
				var data = this.$$("editForm").getValues();
				if(data.event_pid){
					data.id = webix.uid();
					if(data.details&&this._trim(data.text)===""&&this._trim(data.details)!=="")
						data.text = data.details;

					data.event_length = data.start_date.valueOf()/1000;
					this._add_rec_marker(data,data.start_date.valueOf());
					this.add(data);
					this.setCursor(data.id);
					this.setDate();
					this.$$("views").back();
					return true;
				}
			}
			this._saveEventBase.call(this);
		};

		scheduler._editEventBase = scheduler._editEvent;
		scheduler._editEvent = function(){
			var item = this.getItem(this.getCursor());
			if((item.rec_type || item.event_pid) && config.recurring_dialogs){
				webix.rec_dialog({
					height:xy.confirm_height,
					width:xy.confirm_width,
					message: labels.recurring.edit_message,
					callback: function(result) {
						if (result == 1){
							if(item.event_pid)
								scheduler.setCursor(item.event_pid);
							scheduler._editEventBase.call(scheduler);
						}
						else if (result == 2)
							scheduler._applyRecExeption("edit");
					},
					labelSeries:labels.recurring.edit_series,
					labelEvent:labels.recurring.edit_occurrence,
					labelCancel:labels.icon_cancel

				});
			}
			else
				scheduler._editEventBase.call(this);
		};
		scheduler._deleteEventBase = scheduler._deleteEvent;
		scheduler._deleteEvent = function(){
			var item = this.getItem(this.getCursor());
			if((item.rec_type || item.event_pid) && config.recurring_dialogs){
				webix.rec_dialog({
					height:xy.confirm_height,
					width:xy.confirm_width,

					message: labels.recurring.delete_message,
					callback: function(result) {
						if (result == 1){
							scheduler.remove(scheduler.getCursor());
							scheduler.setDate();
							scheduler.$$("views").back(1);
						}
						else if (result == 2)
							scheduler._applyRecExeption("remove");
					},
					labelSeries:labels.recurring.delete_series,
					labelEvent:labels.recurring.delete_occurrence,
					labelCancel:labels.icon_cancel

				});
			}
			else
				scheduler._deleteEventBase.call(this);
		};


		scheduler._applyRecExeption = function(mode){
			var data, id;
			if( mode == "edit" ){
				data = webix.copy(this.getItem(this.getCursor()));

				if(data.rec_type){
					this.define("editEvent",false);

					if(this.$$("rec_type") && !this.$$("rec_type").config.hidden)
						this._recTypeState = "hide";

					this.$$("form").show();

					this.$$("editForm").clear();

					data.event_pid = data.id;
					id = this._last_selected;

					if (!id && !id[1])
						return scheduler._editEventBase.call(this);

					data.start_date = new Date(id[1]*1000);
					data.end_date = new Date(id[1]*1000+data.event_length*1000);

					delete data.id;

					data.rec_type = data.rec_pattern = "";

					data.end_date = new Date(data.start_date.valueOf()+data.event_length*1000);
					this.$$("editForm").setValues(data);
					this._setDefaultDates();
				}
				else
					scheduler._editEventBase.call(this);
			}
			else if( mode == "remove" ){
				data = webix.copy(this.getItem(this.getCursor()));
				data.event_pid = data.id;
				id = this._last_selected;

				if (!id || !id[1])
					return ;

				data.start_date = new Date(id[1]*1000);
				data.end_date = new Date(id[1]*1000+data.event_length*1000);

				data.id = webix.uid();
				data.rec_type = data.rec_pattern=  "none";

				data.end_date = new Date(data.start_date.valueOf()+data.event_length*1000);
				data.event_length = data.start_date.valueOf()/1000;

				this._add_rec_marker(data,data.start_date.valueOf());
				this.add(data);
				//this.setCursor(data.id);
				this.setDate();
				this.$$("views").back();
			}
		};


		scheduler._rec_markers = {};
		scheduler._rec_markers_pull = {};
		scheduler._add_rec_marker = function(ev,time){
			ev._pid_time = time;
			this._rec_markers[ev.id] = ev;
			if (!this._rec_markers_pull[ev.event_pid]) this._rec_markers_pull[ev.event_pid] = {};
			this._rec_markers_pull[ev.event_pid][time]=ev;
		};
		scheduler._get_rec_marker = function(time, id){
			var ch = this._rec_markers_pull[id];
			if (ch) return ch[time];
			return null;	 
		};
		scheduler._get_rec_markers = function(id){
			return (this._rec_markers_pull[id]||[]);	
		};

		scheduler.data.scheme({
			$init:function(data){
				if(typeof data.start_date == "string")
					data.start_date	= webix.i18n.fullDateFormatDate(data.start_date);
				if(typeof data.end_date == "string")
					data.end_date 	= webix.i18n.fullDateFormatDate(data.end_date);

				if (data.event_pid)
					scheduler._add_rec_marker(data, data.event_length*1000);
			},
			$serialize:function(data){
				var obj = {};
				obj.start_date = webix.i18n.fullDateFormatStr(webix.Date.copy(data.start_date));
				obj.end_date = webix.i18n.fullDateFormatStr(webix.Date.copy(data.end_date));
				webix.extend(obj,data);
				return obj;
			}
		});
		

		scheduler.transponse_size={
			day:1, week:7, month:1, year:12 
		};
		scheduler.day_week=function(sd,day,week){
			sd.setDate(1);
			week = (week-1)*7;
			var cday = sd.getDay();
			var nday=(day==7?0:(day*1))+week-cday+1;
			sd.setDate(nday<=week?(nday+7):nday);
		};
		scheduler.transpose_day_week=function(sd,list,cor,size,cor2){
			var cday = (sd.getDay()||(webix.Date.startOnMonday?7:0))-cor;
			for (var i=0; i < list.length; i++) {
				if (list[i]>cday)
					return sd.setDate(sd.getDate()+list[i]*1-cday-(size?cor:cor2));
			}
			scheduler.transpose_day_week(sd,list,cor+size,null,cor);
		};	
		scheduler.transpose_type = function(type){
			var f = "transpose_"+type;
			if (!webix.Date[f]) {
				var str = type.split("_");
				var day = 60*60*24*1000;
				var step = scheduler.transponse_size[str[0]]*str[1];
				
				if (str[0]=="day" || str[0]=="week"){
					var days = null;
					if (str[4]){
						days=str[4].split(",");
						if (webix.Date.startOnMonday){
							for (var i=0; i < days.length; i++)
								days[i]=(days[i]*1)||7;
							days.sort();
						}
					}
					
					
					webix.Date[f] = function(nd,td){
						var delta = Math.floor((td.valueOf()-nd.valueOf())/(day*step));
						if (delta>0)
							nd.setDate(nd.getDate()+delta*step);
						if (days)
							scheduler.transpose_day_week(nd,days,1,step);
					};
					webix.Date.add[type] = function(nd,inc){
						if (days){
							for (var count=0; count < inc; count++)
								scheduler.transpose_day_week(nd,days,0,step);	
						} else
							nd.setDate(nd.getDate()+inc*step);
						
						return nd;
					};
				}
				else if (str[0]=="month" || str[0]=="year"){
					webix.Date[f] = function(nd,td){
						var delta = Math.ceil(((td.getFullYear()*12+td.getMonth()*1)-(nd.getFullYear()*12+nd.getMonth()*1))/(step));
						if (delta>=0)
							nd.setMonth(nd.getMonth()+delta*step);
						if (str[3])
							scheduler.day_week(nd,str[2],str[3]);
					};
					webix.Date.add[type] = function(nd,inc){
						nd.setMonth(nd.getMonth()+inc*step);
						if (str[3])
							scheduler.day_week(nd,str[2],str[3]);
						return nd;
					};
				}
			}
		};
		scheduler.data.attachEvent("onStoreUpdated", function(id, data, mode){
			if (mode == "delete" || mode == "update" || (mode == "insert" && !data.rec_type && data.event_pid)){
				this.blockEvent();
				var sub = scheduler._get_rec_markers(id);
				scheduler._rec_markers_pull[id] = null;
				for (var i in sub) {
					if(sub.hasOwnProperty(i)){
						id = sub[i].id;
						if(this.getItem(id))
							this.remove(id);
					}
				}
				this.unblockEvent();
			}

		});
		scheduler.repeat_date=function(ev,stack,from,to){
			if (ev.rec_type == "none")
				return;

			var td = new Date(ev.start_date.valueOf());
			ev.rec_pattern = ev.rec_type.split("#")[0];
			
			scheduler.transpose_type(ev.rec_pattern);
			webix.Date["transpose_"+ev.rec_pattern](td, from);
			
			while (td<ev.start_date || (td.valueOf()+ev.event_length*1000)<=from.valueOf())
				td = webix.Date.add(td,1,ev.rec_pattern,true);
			while (td < to && td < ev.end_date){
				var ch = this._get_rec_marker(td.valueOf(),ev.id);
				if (!ch){
					var ted = new Date(td.valueOf()+ev.event_length*1000);
					var copy=webix.copy(ev);
					//copy._timed = ev._timed;
					copy.text = ev.text;
					copy.start_date = td;
					copy.event_pid = ev.id;
					copy.id = ev.id+"#"+Math.ceil(td.valueOf()/1000);
					copy.end_date = ted;
			
					var shift = copy.start_date.getTimezoneOffset() - copy.end_date.getTimezoneOffset();
					if (shift){
						if (shift>0) 
							copy.end_date = new Date(td.valueOf()+ev.event_length*1000-shift*60*1000);
						else {
							copy.end_date = new Date(copy.end_date.valueOf() + shift*60*1000);
						}
					}
									
					stack.push(copy);
				}
				td = webix.Date.add(td,1,ev.rec_pattern,true);
			}		
		};

	});


	webix.attachEvent("onBeforeSchedulerInit",function(){
		if(!config.recurring) return true;

		if(!config.form){
			config.form = [
				{view:"text",		label:labels.label_event,	name:"text", labelWidth: 90},
				{view:"datetext",	label:labels.label_start,	id:"start_date",	name:"start_date", dateFormat:config.form_date, labelWidth: 90},
				{view:"datetext",	label:labels.label_end,		id:"end_date",		name:"end_date", 	  dateFormat:config.form_date, labelWidth: 90},
				{view:"checkbox",	id:"allDay",	name:"allDay", label:labels.label_allday,  value:0, labelWidth: 100},
				{view:"rectext",	label:labels.recurring.repeat,	id:"rec_type",	name:"rec_type", readonly:true, labelWidth: 90},
				{view:"textarea",	label:labels.label_details,	id:"details",	name:"details",		height:110, labelWidth: 90},
				{view:"text",	hidden: true, id:"event_length", name:"event_length"}
			];
		}
			
		if(!config.recurring_bar){
			config.recurring_bar = [
				{view:"label", width:xy.icon_cancel, id:"recCancel", name:"recCancel",css:"cancel",align:"center",label:labels.icon_cancel,batch:"default"},
				{view:"label", width:xy.icon_cancel, id:"recEndCancel", name:"recEndCancel", css:"cancel",align:"center",label:labels.icon_cancel,batch:"endRepeat"},
				{},
				{view:"button", width:xy.icon_done, id:"recDone", name:"recDone",align:"right",label:labels.icon_done,batch:"default"},
				{view:"button", width:xy.icon_done, id:"recDone2", name:"recDone2", align:"right",label:labels.icon_done,batch:"other"},

				{view:"button", width:xy.icon_done, id:"recEndDone", name:"recEndDone", align:"right",label:labels.icon_done,batch:"endRepeat"}
			];
		}
		/*list of recurring types in the first view*/
		if(!config.recurring_views_list)
			config.recurring_views_list = [
					{id:"none",value:labels.recurring.none, readonly:true},
					{id:"daily",value:labels.recurring.daily},
					{id:"weekly",value:labels.recurring.weekly},
					{id:"monthly",value:labels.recurring.monthly},
					{id:"yearly",value:labels.recurring.yearly}
			];
		if(!scheduler.templates.rec_list_item){
			scheduler.templates.rec_list_item = function(obj){
				return obj.value +(!obj.readonly?"<div class='webix_arrow_icon'></div>":"");
			};
		}
		/* the initial view, the with "repeat" options*/
		if(!config.rec_init_form)
			config.rec_init_form = [
				{height:10},
				{view:"reclist",id:"recList", autoheight: true, scroll: false, select:true,css:"rec_list",value:"none",template:scheduler.templates.rec_list_item, datatype:"json",data:config.recurring_views_list, labelWidth:100},
				{ maxHeight: 42},

				{view:"endrec", id:"endRepeat", name:"endRepeat", labelWidth:xy.recurring.label_end_repeat,  label:labels.recurring.end_repeat},

				{}
			];
		if(!config.end_repeat_form)
			config.end_repeat_form = [
				{
					type:"clean",
					paddingX: 10,
					paddingY: 5,
					cols:[
						{view:"radio", css:"webix_endby_radio",vertical:true, id:"endBy",width:290, labelWidth:xy.recurring.label_end_by, height:80, options:[
							{ value:labels.recurring.endless_repeat, id: "0" },
							{ value:labels.recurring.end_repeat_label, id: "1" }
						]},
						{}
					]
				},
				{view:"calendar", id:"endByDate", width:0, css:"end_rep_calendar",icons: false},{}
			];
		/*Daily view*/
		if(!config.daily_form)
			config.daily_form = [

				{
					type:"clean",
					css: "webix_rec_counter",
					cols:[
						{view:"label", label:labels.recurring.every,width:xy.recurring.label_every},
						{view:"counter", id:"dayCount", name:"dayCount", value:1,width:125},
						{view:"label", label:labels.recurring.day}
					]
				},
				{}
			];

		templates.selected_event_base = templates.selected_event;
		templates.selected_event = function(ev,common){
			if (!ev.rec_type)
				return templates.selected_event_base(ev);
			else{
				var id = common.getTopParentView()._last_selected;

				if (!id || !id[1])
					return templates.selected_event_base(ev);

				var sd = ev.start_date;
				var ed = ev.end_date;
				ev.start_date = new Date(id[1]*1000);
				ev.end_date = new Date(id[1]*1000+ev.event_length*1000);

				var html = templates.selected_event_base(ev);

				ev.start_date = sd;
				ev.end_date = ed;
				return html;
			}
		};

		/*Weekly view*/

		if(!config.weekly_form)
			config.weekly_form = [
				{
					height:10
				},
				{
					type:"clean",
					css: "webix_rec_counter",
					paddingX: 8,
					cols:[
						{view:"label", borderless: true, label:labels.recurring.every,width:xy.recurring.label_every},
						{view:"counter", id:"weekCount", name:"weekCount", value:1,width:125},
						{view:"label", label:labels.recurring.week}
					]
				},
				{view:"weeklist",id:"weekDays", name:"weekDays"},
				{}
			];
		
		
		/*Monthly view*/
		var monthDays = {};
		for(var i=1;i<32;i++)
			monthDays[i]=i;
			
		if(!scheduler.templates.rec_month_d)
			scheduler.templates.rec_month_d = function(){
				return "<div class='radio'><div class='on'></div></div><div class='text'>"+labels.recurring.month_day+"</div>";
			};
		if(!scheduler.templates.rec_month_w)
			scheduler.templates.rec_month_w = function(){
				return "<div class='radio'><div class='on'></div></div><div class='text'>"+labels.recurring.week_day+"</div>";
			};
		
		if(!scheduler.templates.rec_month_d_blured)
			scheduler.templates.rec_month_d_blured = function(){
				return "<div class='radio blured'></div><div class='text'>"+labels.recurring.month_day+"</div>";
			};
		if(!scheduler.templates.rec_month_w_blured)
			scheduler.templates.rec_month_w_blured = function(){
				return "<div class='radio blured'></div><div class='text'>"+labels.recurring.week_day+"</div>";
			};
			
		if(!config.week_day_counters)
			config.week_day_counters = [
				{id:1,value:labels.recurring.counters[0]},
				{id:2,value:labels.recurring.counters[1]},
				{id:3,value:labels.recurring.counters[2]},
				{id:4,value:labels.recurring.counters[3]},
				{id:5,value:labels.recurring.counters[4]}
			];
		if(!config.week_day_labels)
			config.week_day_labels = [
				{id:1,value:webix.i18n.calendar.dayFull[1]},
				{id:2,value:webix.i18n.calendar.dayFull[2]},
				{id:3,value:webix.i18n.calendar.dayFull[3]},
				{id:4,value:webix.i18n.calendar.dayFull[4]},
				{id:5,value:webix.i18n.calendar.dayFull[5]},
				{id:6,value:webix.i18n.calendar.dayFull[6]},
				{id:7,value:webix.i18n.calendar.dayFull[0]}
			];
		if(!config.monthly_form)
			config.monthly_form = [

				{
					type:"clean",
					paddingX: 10,
					cols:[
						{view:"label", label:labels.recurring.every,width:xy.recurring.label_every},
						{view:"counter", id:"monthCount", name:"monthCount", value:1,width:125},
						{view:"label", label:labels.recurring.month},
						{view:"text", hidden: true, name:"monthType",value:"Day",width:1}
					]
				},

				{
					view:"accordion",
					id:"monthTypeSelect",
					//borderless: true,
					rows:[
						{
							headerAltHeight:50,
							headerHeight:50,
							id:"typeDayM",
							header:scheduler.templates.rec_month_d,
							headerAlt:scheduler.templates.rec_month_d_blured,
							body:{
								view:"datecells",
								id:"monthDayM",
								name:"monthDayM",
								data:monthDays
							}
						},
						{
							headerAltHeight:50,
							headerHeight:50,
							id:"typeWeekM",
							header:scheduler.templates.rec_month_w,
							headerAlt:scheduler.templates.rec_month_w_blured,
							collapsed: true,
							body:{
								type:"clean",
								rows:[
									{height: 10},
									{
										type:"clean",
										paddingX:10,
										cols:[
											{view:"select",value:1,id:"weekCountM",name:"weekCountM", css:"", options:config.week_day_counters},
											{view:"select",value:1,id:"weekDayM",name:"weekDayM", css:"", options:config.week_day_labels}
										]
									},
									{}
								]
							}
						}
					]
				}
			];
		if(!config.year_months){
			config.year_months = (scheduler.locale.date?scheduler.locale.date.month_short:webix.i18n.calendar.monthShort);
		}
		/*Yearly view*/
		if(!config.yearly_form)
			config.yearly_form = [

				{
					type:"clean",
					cols:[
						{view:"label", label:labels.recurring.every,width:xy.recurring.label_every},
						{view:"counter", id:"yearCount", name:"yearCount", value:1,width:125},
						{view:"label", label:labels.recurring.year}
					]
				},
				{
					view:"datecells",
					id:"monthDayY",
					borderless: true,
					data:config.year_months,
					x_count:4,
					height:130
				},
				{
					view:"checkbox",label:labels.recurring.week_day, id:"typeWeekY", name:"typeWeekY", labelWidth:250, value:1
				},

				{
					id:"yearWeekRow",
					type:"clean",
					cols:[
						{view:"select",value:1,id:"weekCountY",name:"weekCountY",options:config.week_day_counters},
						{view:"select",value:1,id:"weekDayY",name:"weekDayY",options:config.week_day_labels}
					]
				},
				{}
				
			];
		
		/*recurring subviews*/
		if(!config.recurring_views)
			config.recurring_views = [
				{id:"recForm", view:"form", css: "webix_rec_form", padding:0, scroll: false, elements: config.rec_init_form},
				{id:"endRepeatForm", view:"form",type: "clean", elements: config.end_repeat_form},
				{id:"daily",view:"daily",elements: config.daily_form},
				{id:"weekly", view:"weekly", type: "clean", elements: config.weekly_form},
				{id:"monthly", view:"monthly", elements: config.monthly_form, margin:10},
				{id:"yearly", view:"yearly", padding: 10, margin:10, type: "clean", elements: config.yearly_form}
			];
		
		/*adds recurring view into cells collection of scheduler's multiview*/
		config.views.push({
			view:"recurring",
			id:"recurring"
		});
		
		/*recurring view definition*/
		webix.protoUI({
			name:"recurring",
			defaults:{
				padding:0,
				rows:[
					{view:"toolbar",id:"reccuringBar", css:"webix_subbar", elements:config.recurring_bar, visibleBatch:"default"},
					{id:"recViews", animate: false, cells:config.recurring_views}
				],
				typeToId:{"day":"daily","week":"weekly","month":"monthly","year":"yearly"},
				endlessDate: config.endless_date
			},
			$init: function() {
				this.name = "Recurring";
				this.$view.className += " webix_recurring";
				this.$ready.push(this._setEvents);
			},
			_setEvents:function(){
				var idToType = {};
				for(var type in this.config.typeToId)
					idToType[this.config.typeToId[type]] = type;
				this.define("idToType",idToType);
				webix.delay(function(){
					var topParent = this.getTopParentView();
					topParent.$$("rec_type").attachEvent("onItemClick",this._showRecViewsList);
					topParent.$$("reccuringBar").attachEvent("onItemClick",webix.bind(function(id){
						id = this.innerId(id);
						switch(id){
							case "recDone2":
							case "recEndCancel":
								this.$$("recViews").back();
								this.$$("reccuringBar").showBatch("default");
								break;
							case "recCancel":
								this.$$("views").back();
								break;
							case "recDone":
								this.$$("recurring")._applyRecurrence();
								this.$$("views").back();
								break;
							case "recEndDone":
								var endDate;
								if(this.$$("endBy").getValue()=="1")
									endDate = this.$$("endByDate").getValue();
								else
									endDate = this.$$("recurring").config.endlessDate;
								this.$$("endRepeat").setValue(endDate);
								this.$$("recViews").back();
								this.$$("reccuringBar").showBatch("default");
								break;
						}
					},topParent));
					topParent.$$("recList").attachEvent("onItemClick",webix.bind(this._showRecView,topParent));
					topParent.$$("endRepeat").attachEvent("onItemClick",webix.bind(function(){
						this._showRecView.call(topParent,"endRepeatForm");
						topParent.$$("reccuringBar").showBatch("endRepeat");
						this._setEndDate();
					},this));
					
					if(topParent.$$("typeWeekY"))
						topParent.$$("typeWeekY").attachEvent("onItemClick",function(){
							if(this.getValue())
								topParent.$$("yearWeekRow").show();
							else
								topParent.$$("yearWeekRow").hide();
						});
					if(topParent.$$("endBy"))
						topParent.$$("endBy").attachEvent("onItemClick",function(){
							if(this.getValue() == "1")
								topParent.$$("endByDate").show();
							else
								topParent.$$("endByDate").hide();
						});
					topParent.$$("editForm").attachEvent("onItemClick", webix.bind(function(id){
						id = this.innerId(id);
						if(id=="end_date"){
							this._showDateForm(id);
						}
					},topParent));			
					topParent.$$("weekly").elements.weekDays = topParent.$$("weekDays");
					topParent.$$("monthly").elements.monthDayM = topParent.$$("monthDayM");
					topParent.$$("yearly").elements.monthDayY = topParent.$$("monthDayY");
					topParent.$$("editForm").elements.endRepeat = topParent.$$("endRepeat");

					// hide rec_type field in case of editing series occurrence
					topParent.$$("views").attachEvent("onViewChange",function(v1,v2){
						if(topParent._recTypeState && topParent.$$("form").config.id == v2){
							if(topParent._recTypeState == "hide"){
								topParent.$$("rec_type").hide();
								topParent._recTypeState = "show";
							}
							else if(topParent._recTypeState == "show")
								topParent.$$("rec_type").show();
						}
					});

				},this);
			},
			_showRecViewsList:function(){
				var topParent = this.getTopParentView(); 
				topParent.$$("recurring").show();
				if(topParent.$$("recForm")){
					topParent.$$("recForm").show();
					topParent.$$("recurring").setValue();
					topParent.$$("endRepeat").setValue(topParent.$$("endRepeat").config.value);
				}
			},
			_showRecView:function(id){

				if(this.$$(id)){
					this.$$(id).show();
					this.$$("reccuringBar").showBatch("other");
				}

				if(!this.$$("endRepeat").getValue())
					this.$$("endRepeat").setValue(this.$$("recurring").config.endlessDate);
			},
			setValue:function(){
				var topParent = this.getTopParentView();
				var recType = (topParent.$$("rec_type").getValue()||"_____").split("_");
				var config = topParent.$$("recurring").config;
				topParent.$$("recList").setValue(recType[0]&&recType[0]!==""?config.typeToId[recType[0]]:"none");
				var typeArr;
				var startDate = topParent.$$("start_date").getValue();
				for(var type in config.typeToId){
					if(type==recType[0]){
						recType.push(startDate);
						typeArr = webix.copy(recType);
					}else{
						typeArr = [type,1,"","","",startDate];
					}
					topParent.$$(config.typeToId[type]).setValue(typeArr);
				}
			},
			getValue:function(){
				var topParent = this.getTopParentView();
				var type = topParent.$$("recList").getSelectedId();
				return (type=="none"?["","","","","",""]:topParent.$$(type).getValue());
			},
			_applyRecurrence:function(){
				var topParent = this.getTopParentView();
				var value = this.getValue();
				var startDate = topParent.$$("start_date").getValue();
				var endDate = topParent.$$("end_date").getValue();
				var length = endDate.valueOf()-startDate.valueOf();
				topParent.$$("event_length").setValue(length/1000);

				if(!value[0]){
					topParent.$$("endRepeat").setValue(topParent.$$("end_date").getValue());
					topParent.$$("rec_type").setValue("");
					topParent.$$("event_length").setValue("");
					return;
				}
				else if(value[0]=="week"){
					startDate = webix.Date.add(startDate,(-1)*(startDate.getDay()+6)%7,"day",true);
				}
				else if(value[0]=="month"){
					if(value[5])
						startDate.setDate(value[5]);
				}
				else if(value[0]=="year"&&value[5]){
					startDate.setMonth(value[5]);
				}
				topParent.$$("start_date").setValue(startDate);
				topParent.$$("end_date").setValue(new Date(startDate.valueOf()+length));
				value.pop();
				var repeat = "";
				if(topParent.$$("endBy").getValue()=="0")
					repeat = "no";
				topParent.$$("rec_type").setValue(value.join("_")+"#"+repeat);
			},
			_setEndDate:function(){
				var topParent = this.getTopParentView();
				var values = topParent.$$("editForm").getValues();
				var end =  values.end_date;
				if(end.valueOf() == this.config.endlessDate.valueOf()){
					topParent.$$("endBy").setValue("0");
					topParent.$$("endByDate").setValue(scheduler.config.end_by||webix.Date.add(topParent.$$("end_date").getValue(),1,"year",true),true);
				}
				else{
					topParent.$$("endBy").setValue("1");
					topParent.$$("endByDate").define("minDate",end);
					topParent.$$("endByDate").setValue(end);
				}
				this._setEndByVisibility();
			},
			_setEndByVisibility:function(){
				var topParent = this.getTopParentView();
				var value = topParent.$$("endBy").getValue();
				if(value == "1")
					topParent.$$("endByDate").show();
				else
					topParent.$$("endByDate").hide();
			}
		}, webix.DataLoader, webix.ui.form, webix.EventSystem, webix.Settings);
	});

};