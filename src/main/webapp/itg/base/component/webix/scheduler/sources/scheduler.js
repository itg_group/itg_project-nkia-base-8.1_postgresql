import "less/scheduler.less";

import * as modLocale from "defaults/locale.js";
import * as modConfig from "defaults/config.js";
import * as modXY from "defaults/xy.js";
import * as modTemplates from "defaults/templates.js";

import * as rec from "scheduler_recurring.js";

import "views/dayevents.js";
import "views/datetext.js";
import "views/weekevents.js";
import "views/ariabutton.js";

import "views/recurring/datecells.js";
import "views/recurring/rectext.js";
import "views/recurring/endrec.js";
import "views/recurring/reclist.js";
import "views/recurring/daily.js";
import "views/recurring/weekly.js";
import "views/recurring/weeklist.js";
import "views/recurring/monthly.js";
import "views/recurring/yearly.js";
import "views/recurring/dialog.js";


if (!window.scheduler)
	window.scheduler = {	
		config:{},
		templates:{},
		xy:{},
		locale:{}
	};

if(!scheduler.locale)
	scheduler.locale = {};
scheduler.locale.labels = modLocale.values;
scheduler.config = modConfig.values;
scheduler.xy = modXY.values;
scheduler.templates = modTemplates.values;





/*DHX:Depend touchui.css*/

webix.ready(function(){
	webix.callEvent("onBeforeSchedulerInit",[]);
	
	if (scheduler.locale&&scheduler.locale.date)
		webix.Date.Locale = scheduler.locale.date;
	var config  = scheduler.config;
	var labels = scheduler.locale.labels;
	var templates = scheduler.templates;

	modConfig.init();
	
	/*List types*/
	var listType = {
		cssNoEvents:"no_events",
		timeStart:templates.event_time,
		marker:templates.event_marker, 
		eventClass:templates.event_class
	};
	scheduler.types = {
		event_list:webix.extend({
			name:"EventsList",
			css:"events",
			headerHeight: scheduler.xy.list_header_height,

			height:scheduler.xy.list_height,
			templateHeader:templates.event_date,
			template:templates.event_title
		},listType),
		day_event_list:webix.extend({
			name:"DayEventsList",
			css:"day_events",
			height:scheduler.xy.month_list_height,
			template:templates.month_event_title
		},listType),
		multi_day_list: {
			name:"MultiDayEvents",
			height: 32,
			css:"multi_day_events",
			classname:function(obj, common, marks){
				var css = "webix_list_item";
				if (marks && marks.$css)
					css += " "+marks.$css;
				return css;
			},
			aria:function(obj, common, marks){
				return "role='option'"+(marks && marks.webix_selected?" aria-selected='true' tabindex='0'":" tabindex='-1'");
			},
			templateCss: scheduler.templates.event_class,
			templateStart:webix.template("<div webix_l_id='#id#' {common.aria()} class='{common.classname()} {common.templateCss()}' style='width:{common.widthSize()}; height:{common.heightSize()}; overflow:hidden;{common.templateColor()};'>"),
			template: templates.multi_day_event,
			templateColor: function(obj){
				return scheduler.templates.multi_day_event_style(obj);
			}
		}
	};
	webix.type(webix.ui.weekevents, scheduler.types.event_list);
	webix.type(webix.ui.list, scheduler.types.day_event_list);
	webix.type(webix.ui.list, scheduler.types.multi_day_list);

	webix.DataDriver.scheduler = {
		records:"/*/event"
	};
	webix.extend(webix.DataDriver.scheduler,webix.DataDriver.xml);
	
	/*the views of scheduler multiview*/
	
	var tabViews = [
		{
			id:"day",
			rows:[
				{	
					id:"dayBar",
					view:"toolbar",
					css:"webix_topbar",
					cols: config.day_toolbar
				},
				{
					id:"multiDayList",
					view:"list",
					autoheight: true,
					type:"MultiDayEvents",
					yCount: config.multi_day_limit
				},
				{
					id:"dayList",
					view:"dayevents"
				}
			]
		},
		{
			id:"week",
			rows:[
				{
					id:"weekBar",
					view:"toolbar",
					css:"webix_topbar",
					elements: config.week_toolbar
				},
				{
					id: "weekEventsList",
					view:"weekevents",
					type:"EventsList",
					startDate:new Date(),
					noEvents: labels.label_no_events
				}
			]
		},
		{
			id:"month",
			view: "scrollview",
			scroll:false,
			body: {
				rows:[
					{
						id:"calendar",
						view:"calendar",
						width: 0,
						icons: false,
						minHeight: 310,
						dayWithEvents: templates.calendar_event,
						dayNoEvents:templates.calendar_day,
						calendarHeader:config.calendar_date
					},
					{
						id:"calendarDayEvents",
						view:"list",
						type:"DayEventsList"
					}

				]
			}

		}
	].concat(config.tab_views);

	var delete_button = {view:"button", height:44,	label:labels.icon_delete,	id:"delete", type:"danger" ,css:"delete"};
	if(screen.width>500){
		delete_button.width = 300;
		delete_button = { type:"clean",cols:[{},delete_button,{}] };
	}

	var views = [
		{
			rows:[
				{
					view:"multiview",
					id:"tabViews",
					animate: (webix.env.touch?{}:false),
					cells: tabViews
				},
				{
					//view:"toolbar",
					id:"bottomBar",
					css: "webix_bottombar",
					height: 55,
					padding: 5,
					visibleBatch:"default",
					cols: config.bottom_toolbar
				}
			]
		},
		{
			id:"event",
			/*animate:{
				type:"slide",
				subtype:"in",
				direction:"top"
			},*/

			rows:[
				{
					id:"eventBar",
					view:"toolbar",

					css:"webix_subbar",
					elements: config.selected_toolbar
				},
				{
					type: "clean",
					paddingX: 10,
					rows:[
						{
							id:"eventTemplate",
							view:"template",
							css: "webix_event_view",
							template:templates.selected_event
						},
						delete_button,
						{	view:"template",height:20}
					]
				}

			]
		},
		{
			id:"form",
			rows:[
				{	
					id:"editBar",
					view:"toolbar",
					css: "webix_subbar",
					elements:config.form_toolbar
				},
				{
					id:"editForm",
					view:"form",
					scroll: true,
					elements: config.form,
					rules: config.form_rules
				}
			]
		},
		{
			id:"formdate",
			rows:[
				{	
					id:"dateBar",
					view:"toolbar",
					css: "webix_subbar",
					elements:config.date_toolbar
				},
				{
					id:"dateForm",
					type: "clean",
					rows:[
						{view:"calendar", height:310, timepickerHeight: 35,css:"form_calendar", icons: false, id:"formCalendar", width: 0,  calendarTime:config.calendar_hour, timepicker:1,hourStart:0},
						{view:"template", css:"form_template"}
					] 
				}
			]
		}
	].concat(config.views);
	
	webix.protoUI({
		name:"scheduler",
		version:"{{version}}",
		defaults:{
			rows:[
				{
					view:"multiview",
					id:"views",
					animate: (webix.env.touch?{}:false),
					cells: views
				}
			],
			color:"#color#",
			textColor:"#textColor#"
		},
		_setDateMethods: function(){
			webix.i18n.dateFormat = config.item_date;
			webix.i18n.weekDateFormat = config.week_date;
			webix.i18n.formDate = config.form_date;
			webix.i18n.formAllDayFormat = config.form_all_day;
			webix.i18n.timeFormat = config.hour_date;

			webix.i18n.fullDateFormat = config.parse_date;
			webix.i18n.fullDateFormatUTC = config.server_utc;

			webix.i18n.longDateFormat = config.all_day_date;
			webix.i18n.headerFormatStr = webix.Date.dateToStr( config.day_header_date);
			webix.i18n.headerWeekFormatStr = webix.Date.dateToStr( config.week_header_date);
			webix.i18n.loadDateFormat = config.dyn_load_date;

			var helpers = ["formDate","formAllDayFormat","loadDateFormat","weekDateFormat"];
			for( var i=0; i<helpers.length; i++){
				var key = helpers[i];
				var utc = webix.i18n[key+"UTC"];
				webix.i18n[key+"Str"] = webix.Date.dateToStr(webix.i18n[key], utc);
				webix.i18n[key+"Date"] = webix.Date.strToDate(webix.i18n[key], utc);
			}
			webix.i18n.setLocale();
		},
		$init: function(){
			this.name = "scheduler";
			this.$view.className += " webix_scheduler";
			/*date format functions*/
			this._setDateMethods(config);
			
			this.data.provideApi(this);
			this.data.scheme({
				$init:function(data){
					if(typeof data.start_date=="string")
						data.start_date	= webix.i18n.fullDateFormatDate(data.start_date);
					if(typeof data.end_date=="string")
						data.end_date 	= webix.i18n.fullDateFormatDate(data.end_date);
				},
				$update:function(data){
					if(typeof data.start_date=="string")
						data.start_date	= webix.i18n.fullDateFormatDate(data.start_date);
					if(typeof data.end_date=="string")
						data.end_date 	= webix.i18n.fullDateFormatDate(data.end_date);
				},
				$serialize:function(data){
					var obj = {};
					obj.start_date = webix.i18n.fullDateFormatStr(webix.Date.copy(data.start_date));
					obj.end_date = webix.i18n.fullDateFormatStr(webix.Date.copy(data.end_date));
					webix.extend(obj,data);
					return obj;
				}
			});
			this.$ready.push(this._initStructure);
		
			webix.callEvent("onAfterSchedulerInit",[this]);
			
		},
		syncData:function(target, start, end, filter){

			var events = this.getEvents(start, end);

			var tpull = target.data.pull = {};
			var torder = target.data.order = webix.toArray();
			events = this._correctEventsDates(start, events);
			for (var i=0; i<events.length; i++){
				if(!filter || filter.call(target, events[i], start, end)){
					var id = events[i].id;
					torder.push(id);
					tpull[id]=events[i];
				}
			}
		},
		_syncAllViews:function(){	
			this.coreData.callEvent("onChange",[]);
			this.$$("multiDayList").getParentView().resize();
		},
		getWeekTitle: function(date){
			return scheduler.templates.week_title(date);
		},
		getDayTitle:function(date){
			return scheduler.templates.day_title(date);
		},
		getTodayMultiEvents:function(){
			var scheduler = this.getTopParentView();
			var d = webix.Date.datePart(scheduler.coreData.getValue(),true);
			var next = webix.Date.add(d,1,"day",true);
			scheduler.syncData(this, d, next,function(ev){
				var dateStart = webix.Date.datePart(ev.start_date,true);
				var dateEnd = webix.Date.datePart(ev.end_date,true);
				return !webix.Date.equal(dateStart, dateEnd) && ev.start_date.valueOf()<=d.valueOf()&& ev.end_date.valueOf()>=next.valueOf();
			});
			this.resize();
		},
		getWeekEvents:function(){
			var scheduler = this.getTopParentView();
			var d =  webix.Date.weekStart(scheduler.coreData.getValue());
			var next = webix.Date.add(d,6,"day",true);
			var filter = null;

			if(config.multi_day && scheduler.$$("weekEventsList").isVisible())
				filter = function(ev){
					var dateStart = webix.Date.datePart(ev.start_date,true);
					var dateEnd = webix.Date.datePart(ev.end_date,true);
					return webix.Date.equal(dateStart, dateEnd) || ev.start_date.valueOf()>d.valueOf() || ev.end_date.valueOf()<next.valueOf()
						|| ev.start_date.valueOf()<=d.valueOf()&& ev.end_date.valueOf()>=next.valueOf();
				};
			scheduler.syncData(this, d, next, filter);
		},
		getTodayEvents:function(){
			var scheduler = this.getTopParentView();
			var d = webix.Date.datePart(scheduler.coreData.getValue(),true);
			var next = webix.Date.add(d,1,"day",true);
			var filter = null;

			if(config.multi_day && scheduler.$$("dayList").isVisible())
				filter = function(ev){
					var dateStart = webix.Date.datePart(ev.start_date,true);
					var dateEnd = webix.Date.datePart(ev.end_date,true);
					return webix.Date.equal(dateStart, dateEnd) || ev.start_date.valueOf()>d.valueOf() || ev.end_date.valueOf()<next.valueOf();
				};
			scheduler.syncData(this, d, next, filter);
		},
		_setMonthFlags:function(){
			if (!this.isVisible()) return;
			var top = this.getTopParentView();

			var start = webix.Date.datePart(webix.Date.copy(this.getVisibleDate()));
			start.setDate(1);
			var end = webix.Date.add(start,1,"month",true);

			top._eventsByDate = {};
			var events = top.getEvents(start, end);

			while(start<end){
				var next = webix.Date.add(start,1,"day",true);
				for (var i=0; i<events.length; i++)
					if (events[i].start_date < next && events[i].end_date > start )
						top._eventsByDate[start.valueOf()]=true;
				start = next;
			}
		},
		_initHotKeys:function(){
			
			function click(view){
				var id = view.getSelectedId() || view.locate(document.activeElement);
				if(id) this._on_event_clicked(id);
			}

			webix.UIManager.addHotKey("enter", webix.bind(click, this), this.$$("dayList"));
			webix.UIManager.addHotKey("enter", webix.bind(click, this), this.$$("weekEventsList"));
			webix.UIManager.addHotKey("enter", webix.bind(click, this), this.$$("multiDayList"));
			webix.UIManager.addHotKey("enter", webix.bind(click, this), this.$$("calendarDayEvents"));
		},
		_initStructure:function(){
			this._initToolbars();
			this._initMonth();
			this._initHotKeys();

			//store current date
			this.coreData = new webix.DataValue();
			this.coreData.setValue(config.init_date);

			this.$$("dayList").define("date",this.coreData);
			
			this.selectedEvent = new webix.DataRecord();
			
			if(this.config.readonly){
				this.define("readonly",this.config.readonly);
			}
			else if(config.readonly)
				this.define("readonly",true);
			

			if(this.$$("date")){
				var dayTitle =  this.getDayTitle(config.init_date);
				this.$$("date").setValue(dayTitle);
				this.$$("date").config.label = this.$$("date").config.value;
				this.$$("date").bind(this.coreData, null, webix.i18n.headerFormatStr);
			}

			if(this.$$("weekTitle")){
				var weekTitle = this.getWeekTitle(config.init_date);
				this.$$("weekTitle").setValue(weekTitle);
				this.$$("weekTitle").config.label = weekTitle;
				this.$$("weekTitle").bind(this.coreData, null, webix.bind(this.getWeekTitle,this));
			}


			this.data.attachEvent("onStoreUpdated", this._sortDates);
			this.data.attachEvent("onStoreUpdated", webix.bind(this._syncAllViews, this));

			if(config.multi_day){
				this.$$("multiDayList").define("dataFeed", this.getTodayMultiEvents);
				this.$$("multiDayList").bind(this.coreData);
			}

			this.$$("weekEventsList").define("dataFeed", this.getWeekEvents);
			this.$$("weekEventsList").bind(this.coreData);

			//custom data binding for day-list
			this.$$("dayList").define("dataFeed", this.getTodayEvents);
			this.$$("dayList").bind(this.coreData);

			this.$$("calendarDayEvents").define("dataFeed", this.getTodayEvents);
			this.$$("calendarDayEvents").bind(this.coreData);

			this.$$("calendar").bind(this.coreData);
			this.$$("calendar").attachEvent("onBeforeRender", this._setMonthFlags);
			/*to redraw calendar when coreDate got changed */
			this.coreData.attachEvent("onChange", webix.bind(function(){
				if(!this._isFormCalendarZoomed){
					if(this._load_mode&&this._load_url)
						this.load.apply(this,[this._load_url]);
					this.$$("calendar").render();
				}
			},this));
			this.data.attachEvent("onClearAll", webix.bind(function(){
				this._loaded_ranges = {};
			},this));

			this.data.attachEvent("onIdChange",webix.bind(function(){
				this._syncAllViews();
			},this));
			
			this.$$("eventTemplate").bind(this);


			this.$$("weekEventsList").attachEvent("onItemClick", webix.bind(this._on_event_clicked, this));
			this.$$("dayList").attachEvent("onItemClick", webix.bind(this._on_event_clicked, this));
			this.$$("multiDayList").attachEvent("onItemClick", webix.bind(this._on_event_clicked, this));
			this.$$("calendarDayEvents").attachEvent("onItemClick", webix.bind(this._on_event_clicked, this));

			this.$$("tabViews").attachEvent("onViewChange", webix.bind(this._onViewChanged, this));
			this.attachEvent("onBeforeEventShow", webix.bind(this._onViewChanged, this));

			this.$$(config.mode||"week").show(false,false);
			this._initForm();

		},
		_initForm: function(){

			this.$$("editForm").bind(this);

			/*Start and End date selection*/
			this.dateField = new webix.DataValue();
			this.dateField.setValue("start_date");
			this.$$("datetype").bind(this.dateField, null, function(value){
				return (value == "start_date"?labels.label_start:labels.label_end);
			});

			this.$$("formCalendar").attachEvent("onAfterZoom",webix.bind(function(zoom){
				this._isFormCalendarZoomed = !!zoom;
				if(zoom){
					this.$$("cancel_date").hide();
				}
			},this));
			this.$$("formCalendar").attachEvent("onBeforeRender", webix.bind(function(){
				this.$$("cancel_date").show();
			},this));
		},
		_onViewChanged:function(){
			this.$$("multiDayList").unselectAll();
			this.$$("dayList").unselectAll();
			this.$$("weekEventsList").unselectAll();
			this.$$("calendarDayEvents").unselectAll();
		},
		_on_event_clicked:function(id){
			this._last_selected = (id||"").toString().split("#");
			if (this._last_selected[1])
				this.setCursor(null);
			this.setCursor(this._last_selected[0]);
			if(this.callEvent("onBeforeEventShow",[this._last_selected[0]]))
				this.$$("event").show();

		},
		/*Sorts dates asc, gets hash of dates with event*/
		_sortDates:function(){
			this.blockEvent();
			this.sort(function(a,b){
				return a.start_date < b.start_date?-1:1;
			});
			this.unblockEvent();
		},
		/* Month Events view: sets event handlers */
		_initMonth:function(){
			this.$$("calendar").attachEvent("onDateSelect",webix.bind(function(date){
				this._isMonthCalendarZoomed = false;
				this.setDate(date);
			},this));

			this.$$("calendar").attachEvent("onChange", webix.bind(function(date){
				if(!this._isMonthCalendarZoomed){
					this.$$("calendar").blockEvent();
					this.setDate(date);
					this.$$("calendar").unblockEvent();
				}
			},this));

			this.$$("calendar").attachEvent("onAfterZoom", webix.bind(function(zoom){
				this._isMonthCalendarZoomed = !!zoom;
				if(!zoom)
					this.$$("calendar").callEvent("onBeforeMonthChange",[null, this.$$("calendar").config.date]);
			}, this));

			this.$$("calendar").attachEvent("onBeforeMonthChange",webix.bind(function(oldDate, date){
				if(!this._isFormCalendarZoomed){
					var today = new Date();
					if(date.getMonth()===today.getMonth()&&date.getYear()===today.getYear())
						date = today;
					else
						date.setDate(1);

					if(!this._isMonthCalendarZoomed){
						if(this.$$("tabViews").config.animate){
							webix.ui.animateView(this.$$("month").getBody(), webix.bind(function(){
								this.setDate(date);
							},this), {type: "slide", direction: (oldDate < date ?"left":"right") });
						} else {
							this.setDate(date);
						}
					}
				}
			},this));

			this.$$("calendar").config.dayTemplate = webix.bind(function(date){
				if(this._eventsByDate && this._eventsByDate[date.valueOf()])
					return this.$$("calendar").config.dayWithEvents(date);
				return this.$$("calendar").config.dayNoEvents(date);
			},this);
		},
		getEvents:function(from,to){

			if (!to) to = new Date(9999,0,1);

			var result = [];
			var evs = this.data.getRange();

			for(var i = 0; i < evs.length;i++){
				if ((evs[i].start_date<to) && (evs[i].end_date>from))
					result.push(webix.copy(evs[i]));
			}

			return result;
		},
		_correctEventsDates: function(date,events){

			var event;
			var result = [];
			if(events)
				for(var i=0; i< events.length;i++){
					event = {
						start_date: webix.Date.copy(events[i].start_date),
						end_date: webix.Date.copy(events[i].end_date)
					};
					webix.extend(event,events[i]);
					if(event.start_date.valueOf()< date.valueOf()){
						event.start_date = webix.Date.add(webix.Date.datePart(event.start_date),1,"day",true);
					}
					var id = (id||"").toString().split("#");
					var endDate = event.rec_type?new Date(id[1]*1000+event.event_length*1000):event.end_date;
					if(endDate.valueOf()> webix.Date.add(date,1,"day",true).valueOf()){
						endDate = webix.Date.datePart(endDate);
						event.end_date = webix.Date.add(endDate,1,"day",true);
					}
					result.push(event);
				}
			return result;
		},
		/*applies selected date to all lists*/
		setDate:function(date, inc, mode){
			if (!date)
				date = this.coreData.getValue();
			else 
				date = webix.Date.datePart(date,true);

			if (inc)
				date = webix.Date.add(date, inc, mode, true);

			this.coreData.setValue(date);
		},
		_initToolbars:function(){
			this.attachEvent("onItemClick", function(id){
				
				var view_id = this.innerId(id);
				switch(view_id){
					case "today":
						this.setDate(new Date());	
						break;
					case "add":
						if(this.innerId(this.$$("views").getActiveId()) == "form"){
							var self = this;
							webix.confirm({
								height:scheduler.xy.confirm_height,
								width:scheduler.xy.confirm_width,
								title: labels.icon_close,
								message: labels.confirm_closing,
								callback: function(result) {
									if (result){
										self._addEvent();
									}
								},
								ok:labels.icon_yes,
								cancel:labels.icon_no,
								css:"confirm"
				
							});
						}else{
							this._addEvent();
						}
						break;
					case "prev":
						this.setDate(null, -1, "day");
						break;
					case "next":
						this.setDate(null, 1, "day");
						break;
					case "prevWeek":
						this.setDate(null, -1, "week");
						break;
					case "nextWeek":
						this.setDate(null, 1, "week");
						break;
					case "edit":
						this._editEvent();
						break;
					case "cancel_date":
					case "back":
						this.$$("views").back();
						break;
					case "cancel":
						/*if(!this.config.editEvent)
							this.remove(this.getCursor());*/
						this.callEvent("onAfterCursorChange",[this.getCursor()]);
						this.$$("views").back();
						break;
					case "save":
						this._saveEvent();
						break;
					case "delete":
						this._deleteEvent();
						break;
					case "start_date":
					case "end_date":
						if(this.$$(view_id).name == "datetext")
							this._showDateForm(view_id);
						break;
					case "done":
						if(this._isFormCalendarZoomed){
							this.$$("formCalendar").selectDate(this.$$("formCalendar").config.date,true);
							this._isFormCalendarZoomed = false;
						}
						else{
							var field = this.dateField.getValue();
							var date = this.$$("formCalendar").getValue();
							this.$$(field).setValue(date);
							this._setDefaultDates();
							this.$$("views").back();
							this.$$("editForm").validate();
						}

						break;
					case "allDay":
						this._setAllDay();
						break;
					default:
						//do nothing
						break;
				}		
			});
			this.attachEvent("onAfterTabClick", function(id, button){
				this.$$(button).show();
			});
			this.attachEvent("onBeforeTabClick", function(id, button){
				return this._confirmViewChange(button);
			});
		},
		readonly_setter:function(val){
			if(this.$$("add")){
				if (val){
					this.$$("bottomBar").showBatch("readonly");
					this.$$("edit").hide();
					this.$$("delete").hide();
				} else {
					this.$$("bottomBar").showBatch("default");
					this.$$("edit").show();
					this.$$("delete").show();
				}
			}
			return val;
		},
		/*removes "No events" background*/
		_clearNoEventsStyle:function(){
			if(this.dataCount())
				this.$view.className = this.$view.className.replace(RegExp(this.type.cssNoEvents, "g"),"");
			else 
				this.$view.className += " "+this.type.cssNoEvents;
		},
		_saveEvent: function(){
			//alert(this.$$("editForm").validate())
			if(this.$$("editForm").validate()){
				if(!this.config.editEvent){
					var data = this.$$("editForm").getValues();
					data.id = webix.uid();
					if(data.details&&this._trim(data.text)===""&&this._trim(data.details)!=="")
						data.text = data.details;
					this.add(data);
					this.setCursor(data.id);
				} else {
					this.$$("editForm").save();
				}
				//webix.dp(this).save();
				this.setDate();
				this.$$("views").back();
			}
		},
		/*deletes the cursored event*/
		_deleteEvent: function(){
			var self = this;
			webix.confirm({
				height:scheduler.xy.confirm_height,
				width:scheduler.xy.confirm_width,
				title: labels.icon_delete,
				text: labels.confirm_deleting,
				callback: function(result) {
					if (result){
						self.remove(self.getCursor());
						self.setDate();
						self.$$("views").back(1);
						
					}
				},
				ok:labels.icon_yes,
				cancel:labels.icon_no,
				css:"confirm",
				header:false
			});
		},
		/*adds the new event*/
		_addEvent:function(){
			/*if(this.$$("delete"))
				this.$$("delete").hide();*/
			this.define("editEvent",false);				
			this.$$("form").show();
			
			
			this.$$("editForm").clear();
			this.$$("editForm").clearValidation();
			this.$$("editForm").setValues(templates.new_event_data.call(this));
			this._setDefaultDates();
		},
		eventDefaultDate:function(){
			
		},
		/* edit selected event*/
		_editEvent: function(){
			if(this.$$("delete"))
				this.$$("delete").show();
			this.define("editEvent",true);
			this.$$("form").show();
			if(this.$$("rec_type"))
				this.$$("rec_type").setValue(this.$$("rec_type").getValue(),true);
			this._setDefaultDates();
		},
		/*cofirm the view changing (necessary for edit form)*/
		_confirmViewChange:function(button){
			if(this.innerId(this.$$("views").getActiveId()) == "form"){
				var self = this;
				if(button!= "today")
					webix.confirm({
						height:scheduler.xy.confirm_height,
						width:scheduler.xy.confirm_width,
						title: labels.icon_close,
						text: labels.confirm_closing,
						callback: function(result) {
							if (result){
								self.$$(button).show();
								self.$$("buttons").setValue(button);
							}
						},
						labelOk:labels.icon_yes,
						labelCancel:labels.icon_no,
						css:"confirm"
					});
				return false;
			}
			return true;
		},
		/*displayes calendar for form dates*/
		_showDateForm:function(name){
			this.dateField.setValue(name);
			if(this.$$("editForm").elements.allDay)
				this.$$("formCalendar").define("timepicker", !this.$$("editForm").elements.allDay.getValue());
			this.$$("formdate").show();
			webix.delay(function(){
				this.$$("formCalendar").resize();
				this.$$("formCalendar").selectDate(webix.Date.copy(this.$$(name).getValue()),true);
			},this);
		},
		/*checks time of the date*/
		_checkAllDay:function(date){
			return date.valueOf()== webix.Date.datePart(date,true).valueOf();
		},
		/*onclick handler for addDay toggle*/
		_setAllDay:function(){
			var format = (this.$$("allDay").getValue()=="1"?webix.i18n.formAllDayFormatStr:webix.i18n.formDateStr);
			var elements = this.$$("editForm").elements;
			var fields = [this.$$("start_date"),this.$$("end_date")];
			for(var i=0;i<2;i++){
				fields[i].config.dateFormatStr = format;
				if(elements.allDay.getValue()!=1)
					fields[i].setValue(this._event_date[i],true);
			}
			if(elements.allDay.getValue()==1){
				fields[0].setValue(webix.Date.datePart(this._event_date[0],true),true);
				var end = webix.Date.datePart(this._event_date[1],true);
				if(end.valueOf()<=fields[0].getValue().valueOf())
					end = webix.Date.add(end,1,"day",true);
				fields[1].setValue(end,true);
			}
		},
		/*called when editing form is shown. Creates copies of event dates and set allDay state*/
		_setDefaultDates:function(){
			this._event_date = [];
			var elements = this.$$("editForm").elements;
			this._event_date[0] = this.$$("start_date").getValue();
			this._event_date[1] = this.$$("end_date").getValue();
			var value = (this._checkAllDay(this._event_date[0])&&this._checkAllDay(this._event_date[1])&&(this._event_date[1].valueOf()>this._event_date[0].valueOf())?1:0);
			if(!elements.allDay)
				return;
			elements.allDay.setValue(value);
			var format = (elements.allDay.getValue()?webix.i18n.formAllDayFormatStr:webix.i18n.formDateStr);
			var fields = [this.$$("start_date"),this.$$("end_date")];
			for(var i=0;i<2;i++){
				fields[i].config.dateFormatStr = format;
				fields[i].setValue(this._event_date[i],true);

			}
		},
		/*remove leading and end whitespaces*/
		_trim: function(value) {
			value = value.replace(/^ */g, "");
			value = value.replace(/ *$/g, "");
			return value;
		},
		setLoadMode:function(mode){
			if (mode=="all") mode="";
			this._load_mode=mode;
		},
		_loaded_ranges:{},
		load: function(url){
			this._load_url = url;
			var args = arguments;
			if (this._load_mode && typeof url == "string"){
				url+=(url.indexOf("?")==-1?"?":"&")+"timeshift="+(new Date()).getTimezoneOffset();
				var to;
				var from= this.coreData.getValue();
				var lf =  webix.i18n.loadDateFormatStr;

				from = webix.Date[this._load_mode+"Start"](new Date(from.valueOf()));
				to = from;

				to=webix.Date.add(to,1,this._load_mode,true);
				if (this._loaded_ranges[lf(from)])
					from=webix.Date.add(from,1,this._load_mode,true);

				var temp_to=to;
				do {
					to = temp_to;
					temp_to=webix.Date.add(to,-1,this._load_mode,true);
				} while (temp_to>from && this._loaded_ranges[lf(temp_to)]);

				if (to<=from)
					return false; //already loaded
				args[0] = url+"&from="+lf(from)+"&to="+lf(to);

				while(from<to){
					this._loaded_ranges[lf(from)]=true;
					from=webix.Date.add(from,1,this._load_mode,true);
				}
			}
			webix.DataLoader.prototype.load.apply(this,args);
		}
	}, webix.IdSpace, webix.DataLoader, webix.ui.layout, webix.EventSystem, webix.Settings);
});


rec.code();
// #include scheduler/scheduler_recurring.js