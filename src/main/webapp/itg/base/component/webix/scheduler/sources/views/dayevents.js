webix.protoUI({
	name:"dayevents",
	defaults:{
		hourFormat:"%H",
		hourClock:12,
		firstHour:0,
		lastHour:24,
		timeScaleWidth:25,
		minEventHeight: 20,
		timeScaleHeight: 40,
		scroll:true,
		scaleBorder:1,
		eventOffset:5,
		separateShortEvents: true,
		date: new Date()
	},
	$init:function(config){ 
		this.name = "DayEvents";

		this._dataobj = this.$view.firstChild;
		this._dataobj.style.position = "relative";
		this.$view.className += " webix_dayevents";
		this.data.provideApi(this,true);
		this.data.attachEvent("onStoreUpdated",webix.bind(this.render,this));
		

		if(window.scheduler){
			config.hourFormat = scheduler.config.scale_hour;
			config.timeScaleWidth = scheduler.xy.scale_width;
			config.minEventHeight = scheduler.xy.min_event_height;
			config.timeScaleHeight = scheduler.xy.scale_height*2;
			config.separateShortEvents = scheduler.config.separate_short_events;
		}
	},
	render: function(){
		if (!this.isVisible(this.config.id) || this.$blockRender)
			return;

		if (this.callEvent("onBeforeRender",[this.data])){
			this._renderScale();
			this.type.color = this.config.color;
			this.type.textColor = this.config.textColor;
			this._prepareEvents();
			if(window.scheduler&&scheduler.templates){
				this.type.template = scheduler.templates.day_event;
				this.type.templateCss = scheduler.templates.event_class;
			}
			this._dataobj.firstChild.innerHTML = this.data.getRange().map(this._toHTML,this).join("");
			this._htmlmap = null; //clear map, it will be filled at first getItemNode
			this.callEvent("onAfterRender",[]);

		}
	},
	getItemNode:function(search_id){
		if (this._htmlmap)
			return this._htmlmap[search_id];
			
		//fill map if it doesn't created yet
		this._htmlmap={};
		
		var t = this._dataobj.firstChild.childNodes;
		for (var i=0; i < t.length; i++){
			var id = t[i].getAttribute(this._id); //get item's
			if (id)
				this._htmlmap[id]=t[i];
		}
		//call locator again, when map is filled
		return this.getItemNode(search_id);
	},
	_toHTML:function(obj){
		var mark = this.data.getMark(obj.id, "webix_selected")?" webix_selected":"";
		//check if related template exist
		this.callEvent("onItemRender",[obj]);
		var content = (obj.$template?this.type["template"+obj.$template]:this.type.template)(obj,this.type,mark);
		return this.type.templateStart(obj,this.type, mark)+content+this.type.templateEnd(obj, this.type,mark);
	},
	_renderScale:function(){
		var html = "<div role='presentation'></div>";
		for(var h = this.config.firstHour; h<this.config.lastHour; h++){
			html += this.hourScaleItem(h);
		}
		this._dataobj.innerHTML = html;
	},
	on_click:{
	},
	hourScaleItem: function(hour){
		var hourFormat = this.config.hourFormat;


		var  d = new Date();
		d.setHours(hour);
		hour = webix.Date.dateToStr(hourFormat)(d);

		var html = "";
		var timeScaleWidth = this.config.timeScaleWidth;
		var hourHeight = this.config.timeScaleHeight;
		var sectionWidth = Math.floor(this.config.timeScaleWidth);
		var heightTop = Math.floor(hourHeight/2);
		var eventZoneWidth = this.$width-this.config.scaleBorder-timeScaleWidth;
		html += "<div style='width: 100%; height:"+hourHeight+"px;' class='webix_dayevents_scale_item'>";
		html += "<div class='webix_dayevents_scale_hour' style='width:"+sectionWidth+"px; height:"+hourHeight+"px;line-height:"+hourHeight+"px;'>"+hour+"</div>";
		html += "<div class='webix_dayevents_scale_event'  style='width:"+eventZoneWidth+"px'>";
		html += "<div class='webix_dayevents_scale_top' style='height:"+heightTop+"px;width:"+eventZoneWidth+"px'></div>";
		html += "</div>";
		html += "</div>";
		return html;		
	},
	type:{
		aria:function(obj, common,sel){
			return "role='option'"+(sel?" aria-selected='true' tabindex='0'":" tabindex='-1'");
		},
		templateStart:webix.template("<div webix_l_id='#id#' {common.aria()} class='{common.classname()} {common.templateCss()}' style='left:#$left#px;top:#$top#px;width:#$width#px;height:#$height#px;padding:{common.padding}px;overflow:hidden; {common.templateColor()} ;'>"),
		template:"#text#",
		templateEnd:webix.template("</div>"),
		templateCss:webix.template(""),
		templateColor: function(obj){
			return (window.scheduler?scheduler.templates.day_event_style(obj):"");
		},
		classname:function(obj, common, sel){
			var css = "webix_dayevents_event_item"+sel;
			return css;
		},
		padding:4
	},
	_prepareEvents:function(){
		var evs = this.data.getRange();
		var stack = [];
		var ev,i,j,k,_is_sorder,_max_sorder,_sorder_set;
		this._minMappedDuration = Math.ceil(this.config.minEventHeight * 60 / (this.config.timeScaleHeight+1));  // values could change along the way
		for(i=0; i< evs.length;i++){
			ev=evs[i];
			ev.$inner=false;
			while (stack.length && this._getEventEndDate(stack[stack.length-1]).valueOf()<=ev.start_date.valueOf()){
				stack.splice(stack.length-1,1);
			}
			_sorder_set = false;
			
			for(j=0;j< stack.length;j++){
				if(this._getEventEndDate(stack[j]).valueOf()<=ev.start_date.valueOf()){
					_sorder_set = true;
					ev.$sorder=stack[j].$sorder;
					stack.splice(j,1);
					ev.$inner=true;
					break;
				}
			}
			
			if (stack.length) stack[stack.length-1].$inner=true;
			
			if(!_sorder_set){
				if(stack.length){
					if(stack.length<=stack[stack.length-1].$sorder){
						if(!stack[stack.length-1].$sorder)
							ev.$sorder = 0; 
						else
							for(j=0;j<stack.length;j++){
								_is_sorder = false;
								for(k=0;k<stack.length;k++){
									if(stack[k].$sorder==j){
										_is_sorder = true;
										break;
									}
								}
								if(!_is_sorder){
									ev.$sorder = j; 
									break;
								}	
							}
						ev.$inner = true;
					}
					else{
						_max_sorder = stack[0].$sorder;
						for(j =1;j < stack.length; j++)
							if(stack[j].$sorder>_max_sorder)
								_max_sorder = stack[j].$sorder;
						ev.$sorder = _max_sorder+1;
						ev.$inner = false;
					}
				}
				else 
					ev.$sorder = 0; 
			}
			stack.push(ev);
			if (stack.length>(stack.max_count||0)) stack.max_count=stack.length;
		}
		
		for (let i=0; i < evs.length; i++){ 
			evs[i].$count=stack.max_count;
			this._setPosition(evs[i]);
		}
	},
	_setPosition:function(ev){
		
		var date = this.config.date.getValue?this.config.date.getValue():this.config.date;
		
		var start = webix.Date.copy(ev.start_date);
		var end = webix.Date.copy(ev.end_date);
		var sh = start.getHours();
		var eh = end.getHours();
		if(webix.Date.datePart(start,true).valueOf()>webix.Date.datePart(end,true).valueOf()){
			end = start;
		}
		
		if(webix.Date.datePart(start,true).valueOf()<webix.Date.datePart(date,true).valueOf()){
			start = webix.Date.datePart(date);
		}
		if(webix.Date.datePart(end,true).valueOf()>webix.Date.datePart(date,true).valueOf()){
			end = webix.Date.datePart(date,true);
			end.setMinutes(0);
			end.setHours(this.config.lastHour);
		}
		if (sh < this.config.firstHour || eh >= this.config.lastHour){
			if (sh < this.config.firstHour){
				start.setHours(this.config.firstHour);
				start.setMinutes(0);
			}
			if (eh >= this.config.lastHour){
				end.setMinutes(0);
				end.setHours(this.config.lastHour);
			}
		}
		var temp_width = Math.floor((this.$width-this.config.timeScaleWidth-this.config.eventOffset)/ev.$count);
		ev.$left=ev.$sorder*(temp_width)+this.config.timeScaleWidth+this.config.eventOffset;
		if (!ev.$inner) temp_width=temp_width*(ev.$count-ev.$sorder);
		ev.$width = temp_width-this.config.eventOffset-this.type.padding*2;
		
		var sm = start.getHours()*60+start.getMinutes();
		var em = (end.getHours()*60+end.getMinutes())||(this.config.lastHour*60);
		ev.$top = Math.round((sm-this.config.firstHour*60)*(this.config.timeScaleHeight+1)/60); //42px/hour
		ev.$height = Math.max(this.config.minEventHeight,(em-sm)*(this.config.timeScaleHeight+1)/60-1)-this.type.padding*2;
	},
	_getEventEndDate: function(ev) {
		var end_date = ev.end_date;
		if (this.config.separateShortEvents) {
			var ev_duration = (ev.end_date - ev.start_date) / 60000; // minutes
			if (ev_duration < this.config.minEventHeight) {
				end_date = webix.Date.add(end_date, this._minMappedDuration - ev_duration, "minute",true);
			}
		}
		return end_date;
	},
	$setSize:function(x,y){ 
		if (webix.ui.view.prototype.$setSize.call(this,x,y)){
			this.render();
		}
	}
}, webix.ui.list);
