webix.protoUI({
	name:"rec_dialog",
	defaults:{
		height:210,
		css: "webix_scheduler dialog",
		head: false,
		body:{
			type:"clean",
			rows:[
				{id: "webix_confirm_message",  template: "<div class='webix_dialog_text'>#text#</div>",
					data:{ text: "You have forgot to define the text :) " }
				},
				{	height:44, view:"button", label:"Series", click:function(){
					this.getParentView().getParentView()._callback(1);
				}
				},
				{ 	height:44, view:"button", label:"Occurrence", click:function(){
					this.getParentView().getParentView()._callback(2);
				}
				},
				{	height:44, view:"button", css: "cancel", label:"Cancel", click:function(){
					this.getParentView().getParentView()._callback(false);
				}
				}
			]
		}
	},
	labelSeries_setter:function(value){
		var body = this._body_cell._cells[1];
		body.config.label = value;
		body.render();
	},
	labelEvent_setter:function(value){
		var body = this._body_cell._cells[2];
		body.config.label = value;
		body.render();
	},
	labelCancel_setter:function(value){
		var body = this._body_cell._cells[3];
		body.config.label = value;
		body.render();
	}
},webix.ui.alert);
webix.rec_dialog = webix.single(webix.ui.rec_dialog);