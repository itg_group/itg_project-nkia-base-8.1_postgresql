webix.protoUI({
	name:"weekly",
	setValue:function(data){
		if(typeof data == "object"&&data[0]=="week"){
			if(!data[1]&&data[1]==="")
				data[1] = 1;
			if(!data[4]||data[4]==="")
				data[4] = (data[5].getDay()+6)%7+1;

			this.setValues({weekCount:data[1],weekDays:""+data[4]});
		}
	},
	getValue:function(){
		var data = this.getValues();
		if(!data.weekDays)
			data.weekDays = 1;
		return ["week",data.weekCount,"","",data.weekDays,""];
	}
}, webix.ui.form);