export const values = {
	confirm_height : 231,
	confirm_width : 250,
	scale_width : 40,
	min_event_height: 20,
	scale_height : 20,
	week_tab:60,
	day_tab:60,
	month_tab:68,
	icon_today : 55,
	icon_save : 90,
	icon_done : 90,
	icon_cancel : 80,
	icon_edit : 90,
	icon_back : 70,
	list_header_height: 30,
	list_height: 50,
	all_day: 120,
	month_list_height: 50,
	recurring: {
		label_end_repeat: 140,
		label_end_by: 240,
		label_every: 65,
		work_days:180
	}
};