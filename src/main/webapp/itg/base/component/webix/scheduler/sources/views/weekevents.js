webix.protoUI({
	name:"weekevents",
	on_click: {
	},
	render:function(id,data){
		if (!this.isVisible(this.config.id))
			return;

		//full reset
		if (this.callEvent("onBeforeRender",[this.data])){
			this.units = null;
			this._htmlmap = null; //clear map, it will be filled at first getItemNode
			this._setUnits();
			if(this.units){
				data = this._getUnitRange();
				if(!data.length)
					this.$view.firstChild.innerHTML = "<div class='webix_noevents'>"+this.config.noEvents+"</div>";
				else
					this.$view.firstChild.innerHTML = data.map(this._toHTML, this).join("");
			}
			this.callEvent("onAfterRender",[]);
		}
	},
	_toHTML:function(obj){
		var mark = this.data.getMark(obj.id, "webix_selected")?" webix_selected":"";
		//check if related template exist
		this.callEvent("onItemRender",[obj]);
		if(obj.$unit){
			return this.type.templateStartHeader(obj,this.type)+this.type.templateHeader.call(this,obj.$unit)+this.type.templateEnd(obj, this.type);
		}
		return this.type.templateStart(obj,this.type, mark)+(obj.$template?this.type["template"+obj.$template]:this.type.template)(obj,this.type)+this.type.templateEnd(obj, this.type);
	},
	_getUnitRange:function(){
		var data,i,u,unit;
		data = [];
		for(u in this.units){
			data.push({$unit:u});
			unit = this.units[u];
			for(i=0;i < unit.length;i++){
				var event = webix.extend({},unit[i]);
				data.push(event);
			}
		}

		return webix.toArray(data);
	},
	_setUnits: function(){
		var scheduler = this.getTopParentView();
		var d = scheduler.coreData.getValue();

		var start = webix.Date.weekStart(d);
		var end = webix.Date.add(start,1,"week",true);

		this.units = {};
		while(start.valueOf()<end.valueOf()){
			var next = webix.Date.add(start,1,"day",true);
			var result = scheduler._correctEventsDates(start,scheduler.getEvents(start, next));
			if(result.length)
				this.units[start.valueOf()] = result;

			start = webix.Date.add(start,1,"day",true);
		}
	},

	type:{
		headerHeight: 28,
		margin:0, padding:0,
		aria:function(obj, common, sel){
			return " role='option'"+(sel?" aria-selected='true' tabindex='0'":" tabindex='-1'");
		},
		templateHeader: function(value){
			var date = new Date(parseInt(value,10));
			var isCurrent = (webix.Date.datePart(date).valueOf() == webix.Date.datePart(new Date()).valueOf());
			return "<span class='webix_unit_header_inner"+(isCurrent?" current":"")+"'>"+webix.i18n.weekDateFormatStr(date)+"</span>";
		},
		templateStart:function(obj,type, sel){
			if(obj.$unit)
				return type.templateStartHeader.apply(this,arguments);
			var className = "webix_list_item webix_list_"+(type.css)+"_item"+(obj.$selected?"_selected":"")+sel;
			var style = "width:auto; height:"+type.height+"px; padding:"+type.padding+"px; margin:"+type.margin+"px; overflow:hidden;"+(type.layout&&type.layout=="x"?"float:left;":"");
			var aria = type.aria(obj, type, sel);
			return "<div webix_l_id='"+obj.id+"' class='"+className+"' style='"+style+"' "+aria+">";
		},
		templateStartHeader:function(obj,type){
			var className = "webix_unit_header webix_unit_"+(type.css)+"_header"+(obj.$selected?"_selected":"");
			var style = "width:auto; height:"+type.headerHeight+"px; overflow:hidden;";
			return "<div webix_unit_id='"+obj.$unit+"' class='"+className+"' style='"+style+"'>";
		}
	}
}, webix.ui.list);