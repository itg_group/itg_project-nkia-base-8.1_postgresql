webix.protoUI({
	name:"ariabutton",
	$cssName:"button",
	$renderInput:function(obj){
		var css = "class='webixtype_"+(obj.type||"base")+"' ";
		return "<button type='button' aria-label='"+webix.template.escape(obj.label||obj.value)+"'"+css+"></button>";
	}
}, webix.ui.button);

