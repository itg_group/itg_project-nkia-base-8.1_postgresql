webix.protoUI({
	name:"monthly",
	defaults:{
		css: "webix_monthly"
	},
	setValue:function(data){
		if(typeof data == "object"&&data[0]=="month"){
			var topParent = this.getTopParentView();
			if(!data[1]&&data[1]==="")
				data[1] = 1;
			if(data[2]&&data[2]!=="")
				topParent.$$("typeWeekM").expand();
			else{
				data[2] = (data[5].getDay()+6)%7+1;
				data[3] = Math.ceil(data[5].getDate()/7);
			}
			this.setValues({monthCount:data[1],weekDayM:data[2],weekCountM:data[3],monthDayM:data[5].getDate()});
		}
	},
	getValue:function(){
		var data = this.getValues();
		var res = ["month",data.monthCount,"","","",""];
		var topParent = this.getTopParentView();
		if(topParent.$$("typeDayM").config.collapsed){
			res[2] = data.weekDayM;
			res[3] = data.weekCountM;
		}
		else{
			res[5] = data.monthDayM;
		}
		return res;
	}
}, webix.ui.form);