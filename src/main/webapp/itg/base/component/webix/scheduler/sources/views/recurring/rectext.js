webix.protoUI({
	name:"rectext",
	$setValue:function(value){
		this.config.value = (value||"");
		var type = "none";
		var recView = this.getTopParentView().$$("recurring");
		var arr = this.config.value.split("_");
		if(arr[0]!=="")
			type = recView.config.typeToId[arr[0]];
		this.config.text = this.getInputNode().innerHTML = scheduler.locale.labels.recurring[type];
	}
}, webix.ui.datetext);