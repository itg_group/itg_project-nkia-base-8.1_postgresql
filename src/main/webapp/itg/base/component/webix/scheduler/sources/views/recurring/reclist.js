webix.protoUI({
	name:"reclist",
	defaults:{
		//padding:10
	},
	$init:function(){
		this.$view.className += " webix_reclist";
	},
	setValue:function(value){
		if(this.getItem(value))
			this.select(value);
	},
	getValue:function(){
		return this.getSelected();
	}
},webix.ui.list);