import "less/kanban.less";
import "./list";
import "./dataview";

function defaultSetter(a, sub){
	var status = sub.config.status;

	if (typeof status === "object"){
		for (var key in status)
			a[key] = status[key];
	} else if (typeof status === "function"){
		status.call(sub, a, true);
	} else {
		a.status = status;
	}
}
function defaultFilter(sub){
	var status = sub.config.status;

	if (typeof status === "object")
		return function(a){
			for (var key in status){
				if (a[key] != status[key])
					return false;
			}
			return true;
		};

	if (typeof status === "function")
		return function(a){ return status.call(sub, a); };

	return function(a){ return a.status === status; };
}


webix.protoUI({
	name:"kanban",
	$init(){
		this.data.provideApi(this, true);
		
		this.$ready.push(function(){
			this.reconstruct();
			this.data.attachEvent("onStoreUpdated", webix.bind(this._applyOrder, this));
		});

		//override default api of datastore
		this.serialize = this._serialize;
	},
	_serialize(){
		var d = [];
		this.eachList(function(l){ d = d.concat(l.serialize()); });
		return d;
	},
	_applyOrder(id, data,mode){
		if (!id){
			this._syncData();
			return;
		}

		if (mode == "add"){
			this._assignList(data);
			this.getOwnerList(id).add(data);
		} else if (mode === "delete"){
			this._sublists[data.$list].remove(id);
		} else if (mode === "move"){
			// has no sense
		} else if (mode === "update" || mode === "paint"){
			this._assignList(data);
			this.getOwnerList(id).updateItem(id);
		}
	},
	setListStatus(id, list){
		for (var i=0; i<this._sublists; i++){
			if (this._sublists[i] === list){
				defaultSetter(this.getItem(id), list);
				return;
			}
		}
	},
	reconstruct(){
		this._prepareLists();
		this._syncData();
	},
	_prepareLists(){
		this._sublists = this.queryView(a => a.$kanban, "all");
		this._subfilters = [];

		for (var i = 0; i < this._sublists.length; i++) {
			var sub = this._sublists[i];
			this._subfilters[i] = defaultFilter(sub);

			if (this.config.icons){
				sub.type.icons = sub.type.icons || this.config.icons;
			}

			sub.config.master = this.config.id;
		}
	},
	_syncData(){
		var i, sets = [];
		for (i=0; i<this._sublists.length; i++) sets[i] = [];

		this.data.each(function(item){
			let j = this._assignList(item);
			if (j >= 0)
				sets[j].push(item);
		}, this);

		for (i=0; i<this._sublists.length; i++){
			var data = sets[i];
			if (data.length && data[0].$index)
				data.sort( (a,b) => a.$index > b.$index ? 1 : -1 );

			this._sublists[i].clearAll();
			this._sublists[i].data.importData(data);
		}
	},
	_assignList(data){
		for (var i = 0; i < this._sublists.length; i++) {
			if (this._subfilters[i](data)){
				return data.$list = i;
			}
		}
		return -1;
	},
	getSelectedId(){
		var selected = null;
		this.eachList(function(list){ selected = list.getSelectedId() || selected; });
		return selected;
	},
	select(id){
		this.getOwnerList(id).select(id);
	},
	getOwnerList(id){
		var item = this.getItem(id);
		return item ? this._sublists[item.$list] : null;
	},
	eachList(code){
		for (var i=0; i<this._sublists.length; i++){
			code.call(this, this._sublists[i], this._sublists[i].config.status);
		}
	}
}, webix.DataLoader, webix.EventSystem, webix.ui.headerlayout );