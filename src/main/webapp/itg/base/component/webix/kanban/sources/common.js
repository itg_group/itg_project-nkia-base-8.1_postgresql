export var type = {
	height:"auto",
	icons:[
		{ icon:"pencil" }
	],
	tagDemiliter: ",",
	templateTags: function(obj,common){
		var res = [];
		if(obj.tags){
			var tags = obj.tags.split(common.tagDemiliter);
			for (var i = 0; i < tags.length; i++){
				res.push(`<span class='webix_kanban_tag'>${tags[i]}</span>`);
			}
		}
		return "<div  class='webix_kanban_tags'>" +(res.length?res.join(" "):"&nbsp;")+"</div>";
	},
	templateIcons: function(obj,common){
		var icons = [];
		var icon = null;
		var html = "";
		for (var i = 0; i < common.icons.length; i++){
			icon = common.icons[i];
			if(!icon.show || icon.show(obj)){
				html = "<span webix_icon_id='"+(icon.id||icon.icon||icon)+"' class='webix_kanban_icon' title='"+(icon.tooltip||"")+"'>";
				html += "<span class='fa-"+(icon.icon||icon)+" webix_icon'></span>";

				if(icon.template){
					html += "<span class='webix_kanban_icon_text'>"+webix.template(icon.template)(obj)+"</span>";
				}
				html += "</span>";
				icons.push(html);
			}
		}
		return "<div  class='webix_kanban_icons'>" + icons.join(" ") + "</div>";
	},
	templateNoAvatar: webix.template("<span class='webix_icon fa-user'></span>"),
	templateAvatar: webix.template(""),
	templateBody: webix.template("#text#"),
	templateFooter: function(obj,common){
		var tags = common.templateTags(obj,common);
		return (tags?tags:"&nbsp;")+ common.templateIcons(obj,common);
	},
	templateStart:webix.template("<div webix_l_id='#id#' class='{common.classname()} webix_kanban_list_item' style='width:{common.width}px; height:{common.height}px;'>"),
	template:function(obj, common){
		//var img = (obj.img)?"<img src='"+obj.img+"'/>":"";
		var avatarResult = common.templateAvatar(obj,common);
		var avatar = "<div class='webix_kanban_user_avatar' webix_icon_id='$avatar'>"+(avatarResult?avatarResult:common.templateNoAvatar(obj,common))+"</div>";
		var body = "<div class='webix_kanban_body'>"+common.templateBody(obj,common)+"</div>";
		var footer = "<div class='webix_kanban_footer'>"+common.templateFooter.call(this,obj,common)+ "</div>";
		var style = "border-left-color:"+obj.color;
		return avatar+"<div class='webix_kanban_list_content' style='"+style+"'>"+body+footer+"</div>";
	}
};

webix.KanbanView = {
	$kanban:true,
	on_context:{},

	$skin:function(){
		// prevent default list's item skin height
	},

	getKanban(){
		return webix.$$(this.config.master);
	},

	_kanban_event(s, t, i){
		this.attachEvent(s,function(...rest){
			rest[i] = this;
			return  this.getKanban().callEvent(t, rest);
		});
	},
	_fixOrder(){
		this.data.each((a,i) => a.$index = i+1);
	},
	move(sid,tindex,tobj, details){
		if (!webix.isArray(sid)){
			var kanban = this.getKanban();
			if (tobj !== this){
				if (kanban.callEvent("onBeforeStatusChange", [sid,this.config.status,this])){
					kanban.setListStatus(sid, tobj);
					webix.DataMove.move.call(this, sid,tindex,tobj, details);
					this._fixOrder();
					kanban.callEvent("onAfterStatusChange", [sid,this.config.status,this]);
				}
				return;
			}
		}

		webix.DataMove.move.call(this, sid,tindex,tobj, details);
	},
	_setHandlers(){
		this.attachEvent("onAfterSelect", function(){
			this.eachOtherList(function(list){
				list.unselect();
			});
		});

		this._kanban_event("onBeforeSelect", "onListBeforeSelect", 2);
		this._kanban_event("onAfterSelect", "onListAfterSelect", 1);
		this._kanban_event("onBeforeContextMenu", "onListBeforeContextMenu", 3);
		this._kanban_event("onAfterContextMenu", "onListAfterContextMenu", 3);
		this._kanban_event("onItemClick", "onListItemClick", 3);
		this._kanban_event("onItemDblClick", "onListItemDblClick", 3);
		this._kanban_event("onBeforeDrag", "onListBeforeDrag", 2);
		this._kanban_event("onBeforeDrop", "onListBeforeDrop", 2);
		this._kanban_event("onBeforeDragIn", "onListBeforeDragIn", 2);
		this._kanban_event("onDragOut", "onListDragOut", 2);

		this.on_click.webix_kanban_user_avatar = this._handle_icons;
		this.on_click.webix_kanban_icon = this._handle_icons;
	},
	_handle_icons(e,id,node){
		var icon = node.getAttribute("webix_icon_id");
		var all = this.type.icons;

		//per-icon click handlers
		if (all){
			for (var i=0; i<all.length; i++){
				if (typeof all[i] === "object" && (all[i].id || all[i].icon) === icon){
					if (all[i].click){
						all[i].click.call(this,id,e,node,this);
					}
				}
			}
		}

		if (icon === "$avatar"){
			this.getKanban().callEvent("onAvatarClick", [id, e, node, this]);
			return true;
		}
		else
			this.getKanban().callEvent("onListIconClick", [icon, id, e, node, this]);
		return false;
	},
	$dragCreate: function(a,e){
		var text = webix.DragControl.$drag(a,e);
		if (!text) return false;
		var drag_container = document.createElement("DIV");
		drag_container.innerHTML=text;
		drag_container.className="webix_drag_zone webix_kanban_drag_zone";
		document.body.appendChild(drag_container);
		return drag_container;
	},
	$dragPos: function(pos){
		pos.x = pos.x - 4;
		pos.y = pos.y - 4;
	},
	eachOtherList:function(code){
		var self = this.config.id;
		var master = this.getKanban();

		master.eachList(function(view){
			if (view.config.id != self)
				code.call(webix.$$(self), view);
		});
	},
	defaults:{
		drag:true,
		select:true
	}
};