/*
@license
Webix Kanban v.5.2.1
This software is covered by Webix Commercial License.
Usage without proper license is prohibited.
(c) XB Software Ltd.
*/
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/codebase/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	__webpack_require__(2);
	
	__webpack_require__(4);
	
	__webpack_require__(6);
	
	function defaultSetter(a, sub) {
		var status = sub.config.status;
	
		if ((typeof status === "undefined" ? "undefined" : _typeof(status)) === "object") {
			for (var key in status) {
				a[key] = status[key];
			}
		} else if (typeof status === "function") {
			status.call(sub, a, true);
		} else {
			a.status = status;
		}
	}
	function defaultFilter(sub) {
		var status = sub.config.status;
	
		if ((typeof status === "undefined" ? "undefined" : _typeof(status)) === "object") return function (a) {
			for (var key in status) {
				if (a[key] != status[key]) return false;
			}
			return true;
		};
	
		if (typeof status === "function") return function (a) {
			return status.call(sub, a);
		};
	
		return function (a) {
			return a.status === status;
		};
	}
	
	webix.protoUI({
		name: "kanban",
		$init: function $init() {
			this.data.provideApi(this, true);
	
			this.$ready.push(function () {
				this.reconstruct();
				this.data.attachEvent("onStoreUpdated", webix.bind(this._applyOrder, this));
			});
	
			//override default api of datastore
			this.serialize = this._serialize;
		},
		_serialize: function _serialize() {
			var d = [];
			this.eachList(function (l) {
				d = d.concat(l.serialize());
			});
			return d;
		},
		_applyOrder: function _applyOrder(id, data, mode) {
			if (!id) {
				this._syncData();
				return;
			}
	
			if (mode == "add") {
				this._assignList(data);
				this.getOwnerList(id).add(data);
			} else if (mode === "delete") {
				this._sublists[data.$list].remove(id);
			} else if (mode === "move") {
				// has no sense
			} else if (mode === "update" || mode === "paint") {
				this._assignList(data);
				this.getOwnerList(id).updateItem(id);
			}
		},
		setListStatus: function setListStatus(id, list) {
			for (var i = 0; i < this._sublists; i++) {
				if (this._sublists[i] === list) {
					defaultSetter(this.getItem(id), list);
					return;
				}
			}
		},
		reconstruct: function reconstruct() {
			this._prepareLists();
			this._syncData();
		},
		_prepareLists: function _prepareLists() {
			this._sublists = this.queryView(function (a) {
				return a.$kanban;
			}, "all");
			this._subfilters = [];
	
			for (var i = 0; i < this._sublists.length; i++) {
				var sub = this._sublists[i];
				this._subfilters[i] = defaultFilter(sub);
	
				if (this.config.icons) {
					sub.type.icons = sub.type.icons || this.config.icons;
				}
	
				sub.config.master = this.config.id;
			}
		},
		_syncData: function _syncData() {
			var i,
			    sets = [];
			for (i = 0; i < this._sublists.length; i++) {
				sets[i] = [];
			}this.data.each(function (item) {
				var j = this._assignList(item);
				if (j >= 0) sets[j].push(item);
			}, this);
	
			for (i = 0; i < this._sublists.length; i++) {
				var data = sets[i];
				if (data.length && data[0].$index) data.sort(function (a, b) {
					return a.$index > b.$index ? 1 : -1;
				});
	
				this._sublists[i].clearAll();
				this._sublists[i].data.importData(data);
			}
		},
		_assignList: function _assignList(data) {
			for (var i = 0; i < this._sublists.length; i++) {
				if (this._subfilters[i](data)) {
					return data.$list = i;
				}
			}
			return -1;
		},
		getSelectedId: function getSelectedId() {
			var selected = null;
			this.eachList(function (list) {
				selected = list.getSelectedId() || selected;
			});
			return selected;
		},
		select: function select(id) {
			this.getOwnerList(id).select(id);
		},
		getOwnerList: function getOwnerList(id) {
			var item = this.getItem(id);
			return item ? this._sublists[item.$list] : null;
		},
		eachList: function eachList(code) {
			for (var i = 0; i < this._sublists.length; i++) {
				code.call(this, this._sublists[i], this._sublists[i].config.status);
			}
		}
	}, webix.DataLoader, webix.EventSystem, webix.ui.headerlayout);

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports) {

	// removed by extract-text-webpack-plugin
	"use strict";

/***/ }),
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	var _common = __webpack_require__(5);
	
	webix.protoUI({
		name: "kanbanlist",
		$init: function $init() {
			this.$view.className += " webix_kanban_list";
			this.$ready.push(webix.bind(this._setHandlers, this));
		},
	
		type: _common.type
	}, webix.ui.list, webix.KanbanView);

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	"use strict";
	
	exports.__esModule = true;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var type = exports.type = {
		height: "auto",
		icons: [{ icon: "pencil" }],
		tagDemiliter: ",",
		templateTags: function templateTags(obj, common) {
			var res = [];
			if (obj.tags) {
				var tags = obj.tags.split(common.tagDemiliter);
				for (var i = 0; i < tags.length; i++) {
					res.push("<span class='webix_kanban_tag'>" + tags[i] + "</span>");
				}
			}
			return "<div  class='webix_kanban_tags'>" + (res.length ? res.join(" ") : "&nbsp;") + "</div>";
		},
		templateIcons: function templateIcons(obj, common) {
			var icons = [];
			var icon = null;
			var html = "";
			for (var i = 0; i < common.icons.length; i++) {
				icon = common.icons[i];
				if (!icon.show || icon.show(obj)) {
					html = "<span webix_icon_id='" + (icon.id || icon.icon || icon) + "' class='webix_kanban_icon' title='" + (icon.tooltip || "") + "'>";
					html += "<span class='fa-" + (icon.icon || icon) + " webix_icon'></span>";
	
					if (icon.template) {
						html += "<span class='webix_kanban_icon_text'>" + webix.template(icon.template)(obj) + "</span>";
					}
					html += "</span>";
					icons.push(html);
				}
			}
			return "<div  class='webix_kanban_icons'>" + icons.join(" ") + "</div>";
		},
		templateNoAvatar: webix.template("<span class='webix_icon fa-user'></span>"),
		templateAvatar: webix.template(""),
		templateBody: webix.template("#text#"),
		templateFooter: function templateFooter(obj, common) {
			var tags = common.templateTags(obj, common);
			return (tags ? tags : "&nbsp;") + common.templateIcons(obj, common);
		},
		templateStart: webix.template("<div webix_l_id='#id#' class='{common.classname()} webix_kanban_list_item' style='width:{common.width}px; height:{common.height}px;'>"),
		template: function template(obj, common) {
			//var img = (obj.img)?"<img src='"+obj.img+"'/>":"";
			var avatarResult = common.templateAvatar(obj, common);
			var avatar = "<div class='webix_kanban_user_avatar' webix_icon_id='$avatar'>" + (avatarResult ? avatarResult : common.templateNoAvatar(obj, common)) + "</div>";
			var body = "<div class='webix_kanban_body'>" + common.templateBody(obj, common) + "</div>";
			var footer = "<div class='webix_kanban_footer'>" + common.templateFooter.call(this, obj, common) + "</div>";
			var style = "border-left-color:" + obj.color;
			return avatar + "<div class='webix_kanban_list_content' style='" + style + "'>" + body + footer + "</div>";
		}
	};
	
	webix.KanbanView = {
		$kanban: true,
		on_context: {},
	
		$skin: function $skin() {
			// prevent default list's item skin height
		},
	
		getKanban: function getKanban() {
			return webix.$$(this.config.master);
		},
		_kanban_event: function _kanban_event(s, t, i) {
			this.attachEvent(s, function () {
				for (var _len = arguments.length, rest = Array(_len), _key = 0; _key < _len; _key++) {
					rest[_key] = arguments[_key];
				}
	
				rest[i] = this;
				return this.getKanban().callEvent(t, rest);
			});
		},
		_fixOrder: function _fixOrder() {
			this.data.each(function (a, i) {
				return a.$index = i + 1;
			});
		},
		move: function move(sid, tindex, tobj, details) {
			if (!webix.isArray(sid)) {
				var kanban = this.getKanban();
				if (tobj !== this) {
					if (kanban.callEvent("onBeforeStatusChange", [sid, this.config.status, this])) {
						kanban.setListStatus(sid, tobj);
						webix.DataMove.move.call(this, sid, tindex, tobj, details);
						this._fixOrder();
						kanban.callEvent("onAfterStatusChange", [sid, this.config.status, this]);
					}
					return;
				}
			}
	
			webix.DataMove.move.call(this, sid, tindex, tobj, details);
		},
		_setHandlers: function _setHandlers() {
			this.attachEvent("onAfterSelect", function () {
				this.eachOtherList(function (list) {
					list.unselect();
				});
			});
	
			this._kanban_event("onBeforeSelect", "onListBeforeSelect", 2);
			this._kanban_event("onAfterSelect", "onListAfterSelect", 1);
			this._kanban_event("onBeforeContextMenu", "onListBeforeContextMenu", 3);
			this._kanban_event("onAfterContextMenu", "onListAfterContextMenu", 3);
			this._kanban_event("onItemClick", "onListItemClick", 3);
			this._kanban_event("onItemDblClick", "onListItemDblClick", 3);
			this._kanban_event("onBeforeDrag", "onListBeforeDrag", 2);
			this._kanban_event("onBeforeDrop", "onListBeforeDrop", 2);
			this._kanban_event("onBeforeDragIn", "onListBeforeDragIn", 2);
			this._kanban_event("onDragOut", "onListDragOut", 2);
	
			this.on_click.webix_kanban_user_avatar = this._handle_icons;
			this.on_click.webix_kanban_icon = this._handle_icons;
		},
		_handle_icons: function _handle_icons(e, id, node) {
			var icon = node.getAttribute("webix_icon_id");
			var all = this.type.icons;
	
			//per-icon click handlers
			if (all) {
				for (var i = 0; i < all.length; i++) {
					if (_typeof(all[i]) === "object" && (all[i].id || all[i].icon) === icon) {
						if (all[i].click) {
							all[i].click.call(this, id, e, node, this);
						}
					}
				}
			}
	
			if (icon === "$avatar") {
				this.getKanban().callEvent("onAvatarClick", [id, e, node, this]);
				return true;
			} else this.getKanban().callEvent("onListIconClick", [icon, id, e, node, this]);
			return false;
		},
	
		$dragCreate: function $dragCreate(a, e) {
			var text = webix.DragControl.$drag(a, e);
			if (!text) return false;
			var drag_container = document.createElement("DIV");
			drag_container.innerHTML = text;
			drag_container.className = "webix_drag_zone webix_kanban_drag_zone";
			document.body.appendChild(drag_container);
			return drag_container;
		},
		$dragPos: function $dragPos(pos) {
			pos.x = pos.x - 4;
			pos.y = pos.y - 4;
		},
		eachOtherList: function eachOtherList(code) {
			var self = this.config.id;
			var master = this.getKanban();
	
			master.eachList(function (view) {
				if (view.config.id != self) code.call(webix.$$(self), view);
			});
		},
		defaults: {
			drag: true,
			select: true
		}
	};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	var _common = __webpack_require__(5);
	
	var dtype = webix.copy(_common.type);
	dtype.width = 150;
	
	webix.protoUI({
		name: "kanbandataview",
		$init: function $init() {
			this.$view.className += " webix_kanban_list";
			this.$ready.push(webix.bind(this._setHandlers, this));
		},
	
		defaults: {
			prerender: true
		},
		type: dtype
	}, webix.ui.dataview, webix.KanbanView);

/***/ })
/******/ ]);