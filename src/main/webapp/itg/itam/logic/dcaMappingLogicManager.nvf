<!doctype html>
<html lang="ko">
<!--
 *
 * DCA-논리서버 매핑 페이지
 * 2015.9.14
 * @author <a href="mailto:jyjeong@nkia.co.kr"> 
 * 
 *
-->
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
<script>
webix.ready(function() {
	
	var physiSearchForm = createFormComp({
		id: "physiSearchForm",
		elementsConfig: {
    		labelPosition: "left"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검 색", type: "form", click: "searchList_physi" }),
					createBtnComp({ label:"초기화", click: "clearSearchForm_physi" })
				]
			}
		},
		fields: {
			colSize: 2,
			items: [
				{colspan: 2, item: createUnionFieldComp({
                    items: [
                            createCodeComboBoxComp({label: '조회구분', name: 'search_type', code_grp_id: 'ASSET_SEARCH_TYPE', attachChoice: true, width: 250 }),
                			createTextFieldComp({ name: "physi_search_value", placeholder: '#springMessage("msg.placeholder.search")', on: { onKeyPress:"searchListByEnterKey_physi" } })
                        ]
                    })
				}
			]
		}
	});
	
	var physiGrid = createGridComp({
		id : 'physiGrid',
		resizeColumn : true,
		checkbox : false,
		pageable : true,
		pageSize : 50,
		url : "/itg/itam/logic/selectPhysiList.do",
		resource : "grid.itam.logic.dca.mapping.physi",
		params : {
			asset_state : 'ACTIVE'
		},
		header : {
			title : "적용 대상 물리서버 목록",
		}
	});
	
	var logicSearchForm = createFormComp({
		id: "logicSearchForm",
		elementsConfig: {
    		labelPosition: "left"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검 색", type: "form", click: "searchList_logic" }),
					createBtnComp({ label:"초기화", click: "clearSearchForm_logic" })
				]
			}
		},
		fields: {
			colSize: 2,
			items: [
				{colspan: 2, item: createUnionFieldComp({
                    items: [
                            createCodeComboBoxComp({label: '조회구분', name: 'logic_search_type', code_grp_id: 'MAPPING_DCA_LOGIC', attachChoice: true, width: 250 }),
                			createTextFieldComp({ name: "logic_search_value", placeholder: '#springMessage("msg.placeholder.search")', on: { onKeyPress:"searchListByEnterKey_logic" } })
                        ]
                    })
				}
			]
		}
	});
	
	var dcaLogicGrid = createGridComp({
		id : 'dcaLogicGrid',
		resizeColumn : true,
		checkbox : true,
		pageable : false,
		url : "/itg/itam/logic/selectDcaLogicList.do",
		checkbox: true,
		resource : "grid.itam.logic.dca.mapping.logic",
		header : {
			title : "DCA 수집 논리서버 목록",
		}
	});
	
	var view = {
			view: "scrollview",
			id: "app",
			body: {
				rows: [{
					cols: [
					       {rows: new Array().concat(physiSearchForm, physiGrid)},
					       {rows: new Array().concat(logicSearchForm, dcaLogicGrid)}
					       ]
				},
				createButtons({
			        align: "center",
			        items: [
			            createBtnComp({visible: true, label: '매 핑', type:'form', id:'mappingLogicBtn', click:"mappingSvLogic"})
			        ]
			    })
				]
			}
		};
		
		nkia.ui.render(view);
	
});

/**
 * 물리서버 검색
 */
function searchList_physi() {
	var paramData = $$("physiSearchForm")._getValues();
	paramData['asset_state'] = 'ACTIVE';
	$$("physiGrid")._reload(paramData);
}

/**
 * 물리서버 검색조건 초기화
 */
function clearSearchForm_physi(){
	$$("physiSearchForm")._reset();
}

/**
 * 검색(키 입력시 : Enter Key = 13)
 */
function searchListByEnterKey_physi(keyCode){
	if(keyCode){
		if(keyCode == "13"){ //enter key
			searchList_physi();
		}	
	}
}

/**
 * 논리서버 검색
 */
function searchList_logic() {
	var paramData = $$("logicSearchForm")._getValues();
	$$("dcaLogicGrid")._reload(paramData);
}

/**
 * 논리서버 검색조건 초기화
 */
function clearSearchForm_logic(){
	$$("logicSearchForm")._reset();
}

/**
 * 검색(키 입력시 : Enter Key = 13)
 */
function searchListByEnterKey_logic(keyCode){
	if(keyCode){
		if(keyCode == "13"){ //enter key
			searchList_logic();
		}	
	}
}

// DCA 수집서버와 물리서버 매핑
function mappingSvLogic(){
	var physiGridSelItem = $$("physiGrid")._getSelectedRows();
	var logicGridSelItem = $$("dcaLogicGrid")._getSelectedRows();
	
	if(physiGridSelItem.length < 1) {
		alert("적용대상 물리서버를 선택해주세요.");
		return;
	} else if(logicGridSelItem.length < 1) {
		alert("DCA 수집 논리서버를 선택해주세요.");
		return;
	} else {
		var msg = (
				"선택 된 [ " + logicGridSelItem.length + " ] 개의 논리서버를 "
				+ "\n"
				+ "[ " + physiGridSelItem[0].ASSET_ID + " - " + physiGridSelItem[0].CONF_NM + " ]"
				+ "\n"
				+ "물리서버와 매핑하시겠습니까?"
			);
		
		if(!confirm(msg)){
			return false;
		}
		
		var physiAssetId = physiGridSelItem[0].ASSET_ID;
		var physiConfId = physiGridSelItem[0].CONF_ID;
		var physiClassId = physiGridSelItem[0].CLASS_ID;
		var physiClassType = physiGridSelItem[0].CLASS_TYPE;
		var logicArray = new Array();
		
		for(var i=0; i<logicGridSelItem.length; i++) {
			var logicMap = logicGridSelItem[i];
			logicMap["PHYSI_ASSET_ID"] = physiAssetId;
			logicMap["PHYSI_CONF_ID"] = physiConfId;
			logicMap["CLASS_ID"] = ( physiClassId );
			logicMap["CLASS_TYPE"] = ( physiClassType );
			logicArray.push(logicMap);
		}
		
		var param = {};
		param["logicArray"] = logicArray;
		
		nkia.ui.utils.ajax({
			viewId: 'scrollview',
			url: "/itg/itam/logic/insertDcaLogicToAm.do",
			params: param,
			async: false,
			isMask: true,
			success: function(response){
				nkia.ui.utils.notification({
					message: '매핑이 완료되었습니다.'
				});
				searchList_physi();
				searchList_logic();
			}
		});
		
	}
	
}


</script>
</head>
<body>
</body>
</html>