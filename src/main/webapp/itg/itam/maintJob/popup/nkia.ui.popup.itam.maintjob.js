/********************************************
 * Incident Process History Tab Component
 * Date: 2017-02-14
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description : 작업기록이력 등록 팝업
 ********************************************/

nkia.ui.popup.itam.maintjob.RegistMaintJob = function() {
	// 속성 Properties
	this.id = "";
	this.title = "작업기록이력 등록";
	this.closable = true;
	this.width = 750;
	this.actionUrl = "/itg/itam/maint/insertMaintJob.do";
	this.popupParamData = {};
	this.type = "";
	this.startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:-3, min:0});
	this.endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
	
		this.insertForm = createFormComp({
			id: this.id + '_insertForm',
			elementsConfig: {
				labelPosition: "top"
			},
			header: {
				title: this.title
			},
			fields: {
				colSize: 2,
				items: [
				       {colspan:1, item:createDateFieldComp({label:'등록일자', name:'ins_dt', dateType : 'D', required: true})}, 
				       {colspan:1, item:createDateFieldComp({label:'작업시작시간', name:'job_begin_dt_time', dateType:'DHM', minuteIntervalThirty:true, required: true})} ,
				       { colspan: 1, item: createUnionFieldComp({
						    items: [
						            createSearchFieldComp({label: "등록자", 	name: 'ins_user_nm', click:  this.openUserSelectPop.bind(_this), required: true}),
									createBtnComp({
										label: "초기화",
										click: function() {
											$$(_this.id + "_insertForm")._setValues({
												ins_user_id: '',
												ins_user_nm: ''
											});
										}
									})
						        ]
						    })
						},
			           {colspan:1, item:createDateFieldComp({label:'작업종료시간', name:'job_end_dt_time', dateType:'DHM', minuteIntervalThirty:true, required: true})},
			           {colspan:1, item:createTextFieldComp({label:'작업자', name:'job_user_nm', fixedLabelWidth:80, required: true})},
			           {colspan:1, item:createTextFieldComp({label:'작업자 회사명', name:'job_corp_nm', required: true})},
			           {colspan:2, item:createTextFieldComp({label:'작업제목', name:'job_title', fixedLabelWidth:80, required: true, maxLength:150})},
			           {colspan:2, item:createTextAreaFieldComp({label:'작업내용', name:'job_desc', fixedLabelWidth:80, required: true, maxLength:2000, resizable: true})}
					]
			},
			hiddens : {
					ins_user_id : ''
			}
		});	
		
	}// end setRender
	
	
	// 팝업 윈도우 구성
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: this.insertForm
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '등 록', type: "form", click: this.registMaintJob.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
		});
	} // end setWindow
	
	// 팝업 기본값 세팅
	this.setDefaultValues  =function(){
		var thatUserId = this.popupParamData["thatUserId"];
		var thatUserNm = this.popupParamData["thatUserNm"];
		$$(this.id + '_insertForm')._setValues({
																			ins_user_nm: thatUserNm, 
																			ins_user_id: thatUserId, 
																			job_begin_dt_time: this.startDateObj,
																			job_end_dt_time: this.endDateObj,
																		});
	}
		
	// 적용(선택) 이벤트
	this.registMaintJob = function(){
		var _this = this;
		var dataForm = $$(this.id + '_insertForm');
		var dataMap = dataForm._getValues();
		dataMap["asset_list"] = this.popupParamData["assetList"];
		var jobBeginDtTime = dataMap['job_begin_dt_time'].replace(/-/gi,'');
		var jobEndDtTime = dataMap['job_end_dt_time'].replace(/-/gi,'');
		
		if(jobBeginDtTime > jobEndDtTime) {
			nkia.ui.utils.notification({
				type: 'error',
				message: "[작업종료일자]는 [작업시작일자]보다 빠를 수 없습니다."
			});
			return;
		}
		
		if(!dataForm._validate()){
			return;
		}
		
		if(!confirm("작업기록 정보를 등록하시겠습니까?")) {
			return;
		}
		
		nkia.ui.utils.ajax({
			viewId: "scrollview",
			url: this.actionUrl,
			params: dataMap,
			async: false,
			isMask: true,
			success: function(response){
				nkia.ui.utils.notification({
					message: '등록되었습니다.'
				});
				_this.window.close();
			}
		});
		
	};	// end registMaintJob

	// 사용자 선택 팝업
	this.openUserSelectPop = function(){
		nkia.ui.utils.window({
			id: "userSelectPop",
			title : "사용자선택팝업",
			width: 900,
			reference: {
				form: $$(this.id + '_insertForm'),
				fields : {
					id: "ins_user_id",
					name: "ins_user_nm"
				}
			},
			callFunc: createUserWindow
		});
	}
}; // end nkia.ui.popup.itam.maintjob.RegistMaintJob


/**
 * 자산 팝업
 * @param {} 작업기록이력 등록 팝업
 */
function createMaintJobWindow(popupProps) {
	var props = popupProps||{};
	var popupObject;
	try {
		switch (props.type) {
			case "registPop":
				popupObject = new nkia.ui.popup.itam.maintjob.RegistMaintJob;
				break;
			default:
				popupObject = new nkia.ui.popup.itam.maintjob.RegistMaintJob;
		}
		popupObject.setProps(props);
		popupObject.setRender(props);
		popupObject.setWindow();
		popupObject.setDefaultValues();// 팝업 기본값 세팅
	} catch(e) {
		alert(e.message);
	}
}


