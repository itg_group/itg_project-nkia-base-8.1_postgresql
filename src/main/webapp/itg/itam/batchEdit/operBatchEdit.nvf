<!doctype html>
<html lang="ko">
<!--
*//**
 * 자산 담당자 일괄 수정
 * 2018.04.14
 * @author <a href="mailto:jyjeong@nkia.co.kr"> JeongYoun, Jeong
 * 
 *//*
-->
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/include/page_meta.nvf")
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
<script type="text/javascript" src='/itg/itam/batchEdit/popup/nkia.ui.popup.itam.user.js'></script><!-- 사용자 선택 팝업 createUserWindow() -->
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")

<script type="text/javascript" charset="utf-8">


webix.ready(function() {
	
	var searchform = createFormComp({
		id: "searchform",
		elementsConfig: {
    		labelPosition: "left"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검 색", type: "form", click: function() { searchList(); } }),
					createBtnComp({ label:"초기화", click: function() { resetFormData(); } })
				],
				css: "webix_layout_form_bottom"
			}
		},
		fields: {
			colSize: 1,
			items: [
				{colspan: 1, item: createUnionFieldComp({
                    items: [
                            createCodeComboBoxComp({
                				label: '조회구분',
                				name: 'search_type',
                				code_grp_id: 'SEARCH_OPER',
                				attachChoice: true,
                				width: 250
                			}),
                			createTextFieldComp({
                				name: "search_value",
                				placeholder: '#springMessage("msg.placeholder.search")',
                				on: {
                					onKeyPress:"pressEnterKey"
                				}
                			})
                        ]
                    })
				}
			]
		}
	});
	
	var grid = createGridComp({
		id : 'grid',
		keys : ["CUST_ID", "OPER_USER_ID", "OPER_USER_TYPE"],
		resizeColumn : true,
		pageable : false,
		checkbox : true,
		url : "/itg/itam/batchEdit/oper/searchAssetOperList.do",
		resource : "grid.itam.batch.manager",
		header : {
			title : "구성자원에 등록된 담당자 목록"
		},
		dependency:{
			targetGridId: "subGrid"
		}
	});
	
	var subGrid = createGridComp({
		id : 'subGrid',
		keys : ["CUST_ID", "OPER_USER_ID", "OPER_USER_TYPE"],
		resizeColumn : true,
		select : false,
		autoLoad : false,
		resource : "grid.itam.batch.selected.manager",
		header : {
			title : "선택된 변경대상 담당자"
		},
		actionColumns:[{
			column: "DEL_ACTION",
			template: nkia.ui.html.icon("remove", "remove_row"),
			onClick: {
				"remove_row": removeData
			}
		}]
	});
	
	var editform = createFormComp({
		id: "editform",
		header: {
			title: "담당자 일괄 변경"
		},
		height: 220,
		fields: {
			colSize: 2,
			items: [
                //베이스의 경우 '담당자(정)'을 필드를 통해 별도 관리하는 것으로 함. (컴포넌트로 관리 시 불필요)
				{colspan: 1, item: createCodeComboBoxComp({label: '담당유형', name: 'oper_user_type_div', code_grp_id: 'OPER_USER_TYPE_DIV', on: {
					onChange: function(val) {
						// 정 선택일 경우
						if('MAIN' == val) {
							$$('editform')._setValues({oper_user_type: ''});
							$$('editform')._getField('oper_user_type').disable();
						} else {
							$$('editform')._getField('oper_user_type').enable();
						}
					}
				}})},
			    {colspan: 1, item: createCodeComboBoxComp({label: '담당구분 선택', name: 'oper_user_type', code_grp_id: 'OPER_USER_TYPE', attachChoice: true, required: true, readonly: true})},
				{colspan: 2, item: createUnionFieldComp({
                    items: [
                            createSearchFieldComp({
                				label: '담당자 선택',
                				name: 'oper_user_nm',
                				required: true,
                				click: function() { 
                					var param = {}; 
            						nkia.ui.utils.window({
            							id: "userSelectPop",
            							title:'담당자 선택',  
            							width: 900,
            							popupParamData: param,
            							reference: {form: $$('editform'), fields : {id: "oper_user_id", name: "oper_user_nm"} },
            							callFunc: createUserWindow // nkia.ui.popup.system.user.js
            						}); 
                				}
                			}),
                			createBtnComp({
                				label: "초기화",
                				click: function() { initPopupField(); }
                			})
                        ]
                    })
				},
				{colspan: 1, item: createCodeComboBoxComp({label: '고객사', name: 'customer_id', url: '/itg/system/customer/searchCustomerCodeListByUserId.do', attachChoice: true, required: true })}
			],
			hiddens: {
				oper_user_id : ""
			}
		}
	}); 
	
	/**
	 * 최종화면
	 */
	var view = {
		view:"scrollview",
		id:"app",
		body: {
			rows: [{
				cols: [{
					rows: new Array().concat(searchform, grid)
				},{
					view: "template",
					template: nkia.ui.html.icon("arrow_right", "add_row"),
					width: 30,
					onClick: {
						"add_row": addData
					}
				},{
					rows: new Array().concat(subGrid, editform, createButtons({
						align: "center",
						items: [
						        createBtnComp({label:"담당자변경", type:"form", id:"getFormDataBtn", click: function() { changeOper(); }})
						]
					}))
				}]
			}]
		}
	};
	
	nkia.ui.render(view);
});

/**
 * 데이터 추가
 */
function addData() {
	
	var data = $$("grid")._getSelectedRows();
	
	if(data.length < 1) {
		nkia.ui.utils.notification({
			type: "error",
			message: "선택된 정보가 없습니다."
		});
		return false;
	} else {
		$$("subGrid")._addRow(data);
		$$("grid")._reload();
	}
}

/**
 * 데이터 삭제
 */
function removeData(e, row, html) {
	$$("subGrid")._removeRow(row);
	$$("grid")._reload();
}

/**
 * 검색 폼 초기화
 */
function resetFormData() {
	$$("searchform")._reset();
}

/**
 * 검색
 */
function searchList() {
	var data = $$("searchform")._getValues();
	$$("grid")._reload(data);
}

/**
 * 엔터키 이벤트
 */
function pressEnterKey(keycode) {
	if(keycode && keycode=="13") {
		searchList();
		return false;
	}
}

/**
 * 팝업 필드 초기화
 */
function initPopupField() {
	var values = {
		oper_user_id : "",
		oper_user_nm : ""
	};
	$$("editform")._setValues(values);
}

/**
 * 담당자 일괄 변경
 */
function changeOper() {
	
	var form = $$("editform");
	var grid = $$("subGrid");
	var formData = form._getValues();
	var gridData = grid._getRows();
	var updateUrl = '/itg/itam/batchEdit/oper/updateAssetOper.do';
	
	if(form._validate()) {
		
		if('MAIN' == formData['oper_user_type_div']) {
			updateUrl = '/itg/itam/batchEdit/oper/updateAssetOperMain.do';
		} else {
			updateUrl = '/itg/itam/batchEdit/oper/updateAssetOper.do';
		}
		
		if(gridData.length < 1) {
			nkia.ui.utils.notification({
				type: "error",
				message: "선택된 변경대상 담당자가 없습니다."
			});
			return false;
		} else {
			var confirmMsg = '';
			confirmMsg += '선택된 담당자를\n';
			if('MAIN' != formData['oper_user_type_div']) {
				confirmMsg += '[ ' + form._getField("oper_user_type")._getText() + ' ] - ';
			}
			confirmMsg += '[ ' + formData["oper_user_nm"] + ' ]\n';
			confirmMsg += '으로 변경하시겠습니까?';
			
			if(!confirm(confirmMsg)) {
				return false;
			} else {
				var params = {};
				params["formData"] = formData;
				params["gridData"] = gridData;
				
				nkia.ui.utils.ajax({
					url: updateUrl,
					params: params,
					isMask: true,
					async: false,
					success: function(response) {
						nkia.ui.utils.notification({
							type: "info",
							message: "변경 되었습니다."
						});
						var data = $$("searchform")._getValues();
						$$("grid")._reload(data);
						$$("subGrid")._removeAllRow();
					}
				});
			}
		}
	}
}

</script>
</body>
</html>