/********************************************
 * Date: 2017-02-16
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  엑셀다운로드 팝업
 ********************************************/

// 분류체계별 엑셀 다운로드
nkia.ui.popup.itam.batchEdit.ExcelByClassType = function() {
	// 속성 Properties
	this.id = "";
	this.title = "엑셀다운로드";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.popupParamData = {};
	this.url = "/itg/itam/batchEdit/batchEditAssetExcelDown.do";
	
	/* 엑셀템플릿 자동 생성(By Poi) */
	this.excelmaketype = "jxl"; // jxl(default) , poi
	this.url_jxl = "/itg/itam/batchEdit/batchEditAssetExcelDown.do";
	this.url_poi = "/itg/itam/batchEdit/batchEditAssetExcelDownByPoi.do";
	
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		if ( this.excelmaketype != null && this.excelmaketype == "poi" ) { this.url = this.url_poi; } else { this.url = this.url_jxl; } /* 엑셀템플릿 자동 생성(By Poi) */
	};
	
	this.setRender = function(props) {
		var _this = this;
		var classType = this.popupParamData["class_type"];
		var classId = this.popupParamData["class_id"];
		var tangibleAssetYn = this.popupParamData["tangible_asset_yn"];
		this.tabBar = new Array();
		var tabBody = new Array();
		
		var svPhysiTabBarMap = {id: 'sp_tab', CODE_ID: 'SV', value: "서버(물리)", SV_TYPE:'PHY'};
		var svLogicTabBarMap = {id: 'sl_tab', CODE_ID: 'SV', value: "서버(논리)", SV_TYPE:'LOG'};
		var nwPhysiTabBarMap = {id: 'nw_tab', CODE_ID: 'NW', value: "NW(물리)"};
		var nwLogicTabBarMap = {id: 'nw_tab', CODE_ID: 'NW', value: "NW(논리)"};
		var scTabBarMap = {id: 'sc_tab', CODE_ID: 'SC', value: "보안"};
		var stTabBarMap = {id: 'st_tab', CODE_ID: 'ST', value: "스토리지"};
		var etTabBarMap = {id: 'et_tab', CODE_ID: 'ET', value: "기타"};
		var rkTabBarMap = {id: 'rk_tab', CODE_ID: 'RK', value: "Rack"};
		var fmTabBarMap = {id: 'fm_tab', CODE_ID: 'FM', value: "기반설비"};
		var lnTabBarMap = {id: 'ln_tab', CODE_ID: 'LN', value: "회선"};
		var swTabBarMap = {id: 'sw_tab', CODE_ID: 'SW', value: "소프트웨어"};
		var dcTabBarMap = {id: 'dc_tab', CODE_ID: 'DC', value: "문서"};		

		if(classType == "SV"){
			if('Y' == tangibleAssetYn) {
				this.tabBar = [svPhysiTabBarMap];
			} else {
				this.tabBar = [svLogicTabBarMap];
			}
		}else if(classType == "NW"){	
			if('Y' == tangibleAssetYn) {
				this.tabBar = [nwPhysiTabBarMap];
			} else {
				this.tabBar = [nwLogicTabBarMap];
			}
		}else if(classType == "SC"){	
			this.tabBar = [scTabBarMap];
		}else if(classType == "ST"){
			this.tabBar = [stTabBarMap];
		}else if(classType == "ET"){
			this.tabBar = [etTabBarMap];
		}else if(classType == "RK"){
			this.tabBar = [rkTabBarMap];
		}else if(classType == "FM"){
			this.tabBar = [fmTabBarMap];
		}else if(classType == "LN"){	
			this.tabBar = [lnTabBarMap];
		}else if(classType == "SW"){
			this.tabBar = [swTabBarMap];
		}else if(classType == "DC"){	
			this.tabBar = [dcTabBarMap];
		}
		
		for(var i=0; i < this.tabBar.length; i++){
			var tabClassType = this.tabBar[i].CODE_ID;
			var tabId = this.tabBar[i].id;
			
			var paramMap = {};
			paramMap['class_id'] =classId;
			paramMap['class_type'] = tabClassType;
			
			var excelAttrGrid = createGridComp({
				id : tabId+'_grid',
				resizeColumn : false,
				height : 370,
				url : "/itg/itam/batchEdit/selectEditAttrList.do",
				resource : "grid.itam.statistics.excelDownPop",
				checkbox : true,
				params : paramMap,
				header: {
					title: "수정 속성 선택"
				}
			});

			// 각 탭별로 grid 구성
			tabBody.push({
				id: tabId,
				rows: excelAttrGrid
			});
		} // end for()
		
		// 실제 탭을 구성합니다.
		this.tabGrid = {
				view:"tabview", // view 형식 : tabview로 입력
				id:"assetTypeTab", // 탭의 ID
				animate:false,
				tabbar:{
					options: this.tabBar // tabbar 배치
				},
				cells:new Array().concat(tabBody) // tab body 배치
		};
		
	} // end setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.tabGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: getConstText({isArgs : true, m_key : 'btn.common.exceldown'}), type: "form", click: this.downExcelAssetList.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
	} // end setWindow
	
	// 엑셀 다운로드
	this.downExcelAssetList = function() {
		var _this = this;
		var sheet = [];
		var tabArray = this.tabBar;
		var tabComp = this.tabBar[0];
		var classTypeArr = [];
		var isEmptyRow = false;
		
		var attrList = $$(tabComp.id+'_grid')._getSelectedRows();
		var title = tabComp.value;
		
		if(attrList.length > 0){
			
			var headerNames = "";
			var includeColumns = "";
			var htmlTypes = "";
			var tblCols = "";
			var notNull = "";
			var colType = " ";
			var unit = "";
			var displayTypes = "";		// 엑셀템플릿 자동 생성(By Poi)
			var columnsAligns = "";		// 엑셀템플릿 자동 생성(By Poi)
			
			var classType = this.popupParamData["class_type"];
			if(classType == "SW") {
				tblCols += "AM_ASSET.ASSET_ID, AM_SW.CONF_ID";
				
				// 자산 정보를 불러오는데 필요한 필수요소들은 유저가 선택하는게 아닌 무조건 들어가야 하는 정보이므로 소스코드에 세팅
				headerNames		+= "IT자산번호,구성ID";
				includeColumns 	+= "ASSET_ID,CONF_ID";
				htmlTypes 		+= "TEXT,TEXT";
				notNull 		+= "Y,Y";
				colType 		+= "VARCHAR2,VARCHAR2";
				unit 			+= ",";
				displayTypes 	+= "R,R";						// 엑셀템플릿 자동 생성(By Poi) -- R(ReadOnly) , U(Update)
				columnsAligns 	+= "CENTER,CENTER";				// 엑셀템플릿 자동 생성(By Poi) -- 20210315 기준 미사용
				
			}else if(classType == "DC") {
				tblCols += "AM_ASSET.ASSET_ID, AM_DC.CONF_ID";
				
				// 자산 정보를 불러오는데 필요한 필수요소들은 유저가 선택하는게 아닌 무조건 들어가야 하는 정보이므로 소스코드에 세팅
				headerNames		+= "IT자산번호,구성ID";
				includeColumns 	+= "ASSET_ID,CONF_ID";
				htmlTypes 		+= "TEXT,TEXT";
				notNull 		+= "Y,Y";
				colType 		+= "VARCHAR2,VARCHAR2";
				unit 			+= ",";
				displayTypes 	+= "R,R";						// 엑셀템플릿 자동 생성(By Poi) -- R(ReadOnly) , U(Update)
				columnsAligns 	+= "CENTER,CENTER";				// 엑셀템플릿 자동 생성(By Poi) -- 20210315 기준 미사용
				
			}else{
				tblCols += "AM_ASSET.ASSET_ID, AM_INFRA.CONF_ID";
				
				// 자산 정보를 불러오는데 필요한 필수요소들은 유저가 선택하는게 아닌 무조건 들어가야 하는 정보이므로 소스코드에 세팅
				headerNames		+= "IT자산번호,구성ID";
				includeColumns 	+= "ASSET_ID,CONF_ID";
				htmlTypes 		+= "TEXT,TEXT";
				notNull 		+= "Y,Y";
				colType 		+= "VARCHAR2,VARCHAR2";
				unit 			+= ",";
				displayTypes 	+= "R,R";						// 엑셀템플릿 자동 생성(By Poi) -- R(ReadOnly) , U(Update)
				columnsAligns 	+= "CENTER,CENTER";				// 엑셀템플릿 자동 생성(By Poi) -- 20210315 기준 미사용
			}
			
			for ( var j = 0; j < attrList.length; j++) {
				headerNames 	+= ( "," + attrList[j].COMMENTS );
				includeColumns 	+= ( "," + attrList[j].COLUMN_NAME );
				htmlTypes 		+= ( "," + attrList[j].HTML_TYPE );
				tblCols 		+= ( "," + attrList[j].TBL_COL );
				notNull 		+= ( "," + attrList[j].NOT_NULL );
				colType 		+= ( "," + attrList[j].COL_TYPE );
				unit	 		+= ( "," + attrList[j].UNIT );
				displayTypes 	+= ( "," + ( attrList[j].ATTR_DISPLAY_TYPE == null ? "" : attrList[j].ATTR_DISPLAY_TYPE ) );	// 엑셀템플릿 자동 생성(By Poi)
				columnsAligns 	+= ( "," + ( attrList[j].COLUMN_ALIGN == null ? "" : attrList[j].COLUMN_ALIGN ) );				// 엑셀템플릿 자동 생성(By Poi) -- 20210315 기준 미사용
			}
			
			// 속성명에 있는 HTML 태그 제거
			headerNames = headerNames.replace(/<[^>]*>/g, '');
			
			var sheetMap = { 
					"sheetName": title, 
					"titleName": title, 
					"headerNames":headerNames,
					"tblCols":tblCols, 
					"notNull":notNull, 
					"colTypes":colType,
					"includeColumns": includeColumns, 
					"htmlTypes":htmlTypes,
					"unit":unit,
					"displayTypes":displayTypes,	// 엑셀템플릿 자동 생성(By Poi) - 수정가능여부
					"columnsAligns":columnsAligns,	// 엑셀템플릿 자동 생성(By Poi) - 셀정렬 - 20210315 기준 미사용
					"freezeRow" : 5,				// 엑셀템플릿 자동 생성(By Poi) - 틀고정ROW
					"headerWrap" : "Y"				// 엑셀템플릿 자동 생성(By Poi) - 제목ROW 자동줄바뀜여부
					};

			var excelAttrs = {"sheet":sheetMap};
			var paramMap = this.popupParamData;
			paramMap['class_type_arr'] = tabComp.CODE_ID;
			
			var getResultUrl = this.url;
			
			var excelParam = {};
			excelParam['fileName'] = '일괄수정 자산목록';
			excelParam["optionsModifyValue"] = "Y";	// 엑셀템플릿 자동 생성(By Poi) - 수정용 템플릿 생성
			excelParam['excelAttrs'] = excelAttrs;
			goExcelDownLoad(getResultUrl, paramMap, excelParam);
			_this.window.close();
			
			
		}else{
			nkia.ui.utils.notification({
				type: 'error',
				message: '속성을 선택하여 주십시오.'
			});
			return false;
		}
			
		
	}
		
}; // end nkia.ui.popup.itam.excel.ExcelByClassType


/**
 * 엑셀 다운로드 속성선택 팝업
 * @param {}
 */
function createExcelTabWindow(popupProps) {
	var props = popupProps||{};
	
	var popupObject;
	try {
		switch (props.type) {
			case "assetByClassType":	// 분류체계별 자산 엑셀 다운로드
				popupObject = new nkia.ui.popup.itam.batchEdit.ExcelByClassType;
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
				break;
			default:
				popupObject = new nkia.ui.popup.itam.batchEdit.ExcelByClassType;
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
		}
		
	} catch(e) {
		alert(e.message);
	}
}


