/*******************************************************************************
 * User Date: 2016-11-17 Version: 1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ******************************************************************************/
nkia.ui.popup.itam.User = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 900;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.url = "/itg/system/user/searchUserList.do";
	this.resource = "grid.system.user.pop";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 150;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};

	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		var _this = this;

		this.tree = createTreeComponent({
			id : this.id + '_deptTree',
			url : '/itg/base/searchDeptTree.do',
			header : {
				title : getConstText({
					isArgs : true,
					m_key : 'res.00034'
				})
			},
			expColable : true,
			filterable : true,
			width : 250,
			params : this.params,
			expandLevel : 3,
			on : {
				onItemClick : this.doTreeClick.bind(_this)
			}
		});

		this.searchForm = createFormComp({
			id : this.id + '_searchForm',
			header : {
				icon : "search",
				title : PAGE_MASSEGE.SEARCH_TITLE
			},
			fields : {
				elementsConfig : {
					labelPosition : "left"
				},
				colSize : 1,
				items : [ {
					item : createUnionFieldComp({
						items : [ createCodeComboBoxComp({
							label : getConstText({
								isArgs : true,
								m_key : 'res.label.system.00012'
							}),
							name : 'search_type',
							code_grp_id : 'SEARCH_USER',
							attachChoice : true,
							width : 250
						}), createTextFieldComp({
							name : "search_value",
							placeholder : getConstText({
								isArgs : true,
								m_key : 'msg.placeholder.search'
							}),
							on : {
								onKeyPress : this.doKeyPress.bind(_this)
							}
						}) ]
					})
				} ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : PAGE_MASSEGE.SEARCH_BTN,
						type : "form",
						click : this.doSearch.bind(_this)
					}), createBtnComp({
						label : PAGE_MASSEGE.RESET_BTN,
						click : this.clearSearch.bind(_this)
					}) ],
					css : "webix_layout_form_bottom"
				}
			}
		});

		var title = getConstText({
			isArgs : true,
			m_key : 'res.00012'
		});

		if (this.type == "process") {
			title = "담당자 목록";
		}

		if (this.type == "multitask") {
			this.on = {};
		} else if (props.type == "multioperChangeManage") {
			this.on = {
				onItemClick : function() {
					if (_this.bindEvent != null) {
						_this.bindEvent.addUser(_this);
					} else {
						_this.addUser(id, e);
					}
				}
			}
			
		} 
		else {
			this.on = {
				onItemDblClick : function(id, e) {
					if (_this.bindEvent != null) {
						_this.bindEvent.choice(_this);
					} else {
						_this.choice(id, e);
					}
				}
			}
		}

		this.grid = createGridComp({
			id : this.id + '_grid',
			header : {
				title : title
			},
			checkbox : this.checkbox,
			minHeight : this.gridHeight,
			resource : this.resource,
			params : this.params,
			url : this.url,
			pageable : true,
			pageSize : this.pageSize,
			keys : [ "USER_ID" ],
			dependency : this.dependency,
			on : this.on
		});
	};

	this.setWindow = function() {
		var _this = this;
		var bodyScrollView = {
			view : "scrollview",
			body : {
				cols : [ {
					rows : this.tree
				}, {
					rows : new Array().concat(this.searchForm, this.grid)
				} ]
			}
		};

		this.window = createWindow({
			id : this.id,
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : [ bodyScrollView ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});
	}

	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e) {
		var treeItem = $$(this.id + '_deptTree').getItem(id);

		var form = $$(this.id + '_searchForm');
		var values = form._getValues();

		if (getConstValue("CENTER_CUST_ID") == treeItem.up_node_id) {
			values.dept_nm = treeItem.text;
		} else {
			values.cust_id = treeItem.node_id;
		}

		if (this.params) {
			if (this.params.oper_type) {
				values["oper_type"] = this.params.oper_type;
			}
			if (this.params.process_type) {
				values["process_type"] = this.params.process_type;
			}
		}

		$$(this.id + '_grid')._reload(values);
	}

	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode) {
		if (keyCode && keyCode == "13") {
			this.doSearch();
		}
	}

	// 검색 버튼 이벤트
	this.doSearch = function() {
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if (this.params) {
			if (this.params.oper_type) {
				data["oper_type"] = this.params.oper_type;
			}
			if (this.params.process_type) {
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e) {
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if (gridItem) {
			var reference = this.reference;
			var fields = reference.fields;

			var user_id = nkia.ui.utils.check.nullValue(gridItem.USER_ID);
			var user_nm = nkia.ui.utils.check.nullValue(gridItem.USER_NM);
			var cust_nm = nkia.ui.utils.check.nullValue(gridItem.CUST_NM);
			var cust_id = nkia.ui.utils.check.nullValue(gridItem.CUST_ID);
			var customer_id = nkia.ui.utils.check.nullValue(gridItem.CUSTOMER_ID);
			var customer_nm = nkia.ui.utils.check.nullValue(gridItem.CUSTOMER_NM);
			//var taskNm = nkia.ui.utils.check.nullValue(PROCESS_META.TASK_NAME.toLocaleLowerCase());
			//var reqType = nkia.ui.utils.check.nullValue(PROCESS_META.REQ_TYPE.toLowerCase());
			/*
			if(taskNm == "start"){
				taskNm = "requst_regist";
			}
			*/
			var values = {};
			values[fields["id"]] = user_id; // 사용자ID
			values[fields["name"]] = user_nm; // 사용자명
			if (fields["cust_id"])
				values[fields["cust_id"]] = cust_id; // 부서ID
			if (fields["cust_nm"])
				values[fields["cust_nm"]] = cust_nm; // 부서명
			/*
			if(PROCESS_META.PROCESS_TYPE){
				if(PROCESS_META.PROCESS_TYPE == "CHANGE" || PROCESS_META.PROCESS_TYPE == "INCIDENT" || PROCESS_META.PROCESS_TYPE == "PROBLEM"){
					values["customer_id"] = customer_id; // 고객사ID
					values["customer_id_nm"] = customer_nm; // 고객사명       					
				}else{
					values["customer_id"] = customer_id; // 고객사ID
					values["customer_nm"] = customer_nm; // 고객사명
				}
			}
			*/
			
			// 담당자 부가 정보
			/*
			if (this.type == "process") {
				// 부재중 체크
				var absent_yn = nkia.ui.utils.check
						.nullValue(gridItem.ABSENT_YN);
				if (absent_yn == "Y") {
					user_id = nkia.ui.utils.check.nullValue(gridItem.ABSENT_ID);
					user_nm = nkia.ui.utils.check.nullValue(gridItem.ABSENT_NM);
				}
				values[fields["grp_select_yn"]] = "N"; // 그룹선택 여부
			}
			*/

			// 사용자 상세 정보
			if (fields["user_detail"]) {
				var detailInfo = nkia.ui.utils.check
						.nullValue(gridItem.CUST_NM)
						+ "(내선번호:"
						+ nkia.ui.utils.check.nullValue(gridItem.TEL_NO) + ")";
						/*+ " - email:"
						+ nkia.ui.utils.check.nullValue(gridItem.EMAIL) + ")";*/
				values[fields["user_detail"]] = detailInfo; // 사용자 상세
			}
			/*
			if(reqType == "service"&&(taskNm == "requst_regist"||taskNm=="requst_modify")){
				var params = {};
				params["cust_id"] = cust_id;
				nkia.ui.utils.ajax({
					url : "/itg/customize/getTlInfoByCust.do",
					params : params,
					isMask : false,
					async : false,
					success : function(response) {
				    	if(response.resultMap != null){
				    		data = {};
				    		data["ROLE_OPETR_01"]= response.resultMap.USER_NM;
				    		data["ROLE_OPETR_01_grp_select_yn"]= 'N';
				    		data["ROLE_OPETR_01_user_id"]= response.resultMap.USER_ID;
				    		$$(taskNm)._setValues(data);
						}
					}
				});
			}
			*/
			reference.form._setValues(values);

			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 담당자가 없습니다.'
			});
		}
	};
};

/*******************************************************************************
 * User Group Date: 2016-11-17 Version: 1.0 - Header, Header버튼, field 생성, Fotter
 * 버튼 지원
 ******************************************************************************/
nkia.ui.popup.system.UserGroup = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.reference = {};
	this.params = {};
	this.gridHeight = 224;
	this.bindEvent = null;

	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.chargerUserGrid = [];
	this.chargerDeptGrid = [];
	this.chargerUserGrpGrid = [];

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		var _this = this;

		this.tree = createTreeComponent({
			id : props.id + '_chargerGrpTree',
			url : '/itg/workflow/charger/searchAllChargerGrpTree.do',
			header : {
				title : "담당자 그룹 목록"
			},
			expColable : true,
			filterable : true,
			width : 300,
			params : this.params,
			expandLevel : 3,
			on : {
				onItemClick : this.doTreeClick.bind(_this),
				onItemDblClick : function(id, e) {
					if (_this.bindEvent != null) {
						_this.bindEvent.choice(_this);
					} else {
						_this.choice(id, e);
					}
				}
			}
		});

		this.chargerUserGrid = createGridComp({
			id : props.id + '_chargerUserGrid',
			header : {
				title : "담당자 그룹 사용자 목록"
			},
			resource : 'grid.system.charger.user',
			url : "/itg/workflow/charger/searchChargerGrpUserTotal.do",
			//height : this.gridHeight,
			select : false,
			pageable : false,
			autoLoad : false
		});

		// this.chargerDeptGrid = createGridComp({
		// id: props.id + '_chargerDeptGrid',
		// header: {
		// title: "담당자 그룹 부서 목록"
		// },
		// resource: 'grid.system.charger.dept',
		// url: "/itg/workflow/charger/searchChargerGrpDept.do",
		// select: false,
		// height: this.gridHeight,
		// pageable: false,
		// autoLoad: false
		// });
//		this.chargerUserGrpGrid = createGridComp({
//			id : props.id + '_chargerUserGrpGrid',
//			header : {
//				title : "담당자 그룹 사용자그룹 목록"
//			},
//			resource : 'grid.system.charger.usergrp',
//			url : '/itg/workflow/charger/searchChargerGrpUserGrp.do',
//			select : false,
//			height : this.gridHeight,
//			pageable : false,
//			autoLoad : false
//		});
	};

	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id : this.id,
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : [ {
					cols : [
							{
								rows : this.tree
							},
							{
								//rows : new Array().concat(this.chargerUserGrid,
								//		this.chargerUserGrpGrid)
								row : this.chargerUserGrid
							} ]
				} ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});
	}

	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e) {
		var treeItem = $$(this.id + '_chargerGrpTree').getItem(id);

		var params = {
			grp_id : treeItem.node_id
		}

		$$(this.id + '_chargerUserGrid')._reload(params);
		// $$(this.id + '_chargerDeptGrid')._reload(params);
		//$$(this.id + '_chargerUserGrpGrid')._reload(params);
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e) {
		var item = $$(this.id + "_chargerGrpTree").getSelectedItem();
		if (item) {
			if (item.$level == 1) {
				nkia.ui.utils.notification({
					type : 'error',
					message : '최상위 그룹은 선택 할 수 없습니다.'
				});
				return;
			}

			var reference = this.reference;
			var fields = reference.fields;
			var user_id = nkia.ui.utils.check.nullValue(item.node_id);
			var user_nm = nkia.ui.utils.check
					.nullValue(item.text.split("[")[0]);

			var values = {};
			values[fields["id"]] = user_id; // 사용자ID
			values[fields["name"]] = user_nm; // 사용자명
			values[fields["grp_select_yn"]] = "Y"; // 그룹선택 여부

			reference.form._setValues(values);

			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 담당자 그룹이 없습니다.'
			});
		}
	};
};

/*******************************************************************************
 * ProcessUser Date: 2016-11-17 Version: 1.0 - Header, Header버튼, field 생성,
 * Fotter 버튼 지원
 ******************************************************************************/
nkia.ui.popup.system.ProcessUser = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.type = "";
	this.closable = true;
	this.width = 500;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.reference = {};

	// 구성요소 Properties
	this.user = {};
	this.userGroup = {};
	this.window = {};
	this.params = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		props.bindEvent = this;

		this.user = new nkia.ui.popup.system.User();
		this.user.setProps(props);
		this.user.setRender(props);

		this.userGroup = new nkia.ui.popup.system.UserGroup();
		this.userGroup.setProps(props);
		this.userGroup.setRender(props);
	};

	this.setWindow = function() {
		var _this = this;
		var bodyScrollView = {
			view : "scrollview",
			body : {
				rows : [
						{
							view : "template",
							template : "<font style='color:red'><b>*&nbsp;담당자 목록에서 담당자를 선택하거나 더블 클릭 해주세요.</b></font>",
							height : 30
						},
						{
							cols : [
									{
										rows : this.user.tree
									},
									{
										rows : new Array().concat(
												this.user.searchForm,
												this.user.grid)
									} ]
						} ]
			}
		};
		var bodyScrollViewGrp = {
			view : "scrollview",
			body : {
				rows : [
						{
							view : "template",
							template : "<font style='color:red'><b>*&nbsp;담당자 그룹 목록에서 그룹을 선택하거나 더블 클릭 해주세요.</b></font>",
							height : 30
						},
						{
							cols : [
									{
										rows : this.userGroup.tree
									},
									{
										rows : new Array()
												.concat(
														this.userGroup.chargerUserGrid,
														this.userGroup.chargerUserGrpGrid)
									} ]
						} ]
			}
		};

		var userTabItem = {
			header : "담당자",
			body : {
				id : "userTab",
				rows : [ bodyScrollView ]
			}
		};

		var userGroupTabItem = {
			header : "담당자 그룹",
			body : {
				id : "userGroupTab",
				rows : [ bodyScrollViewGrp ]
			}
		};

		var body = {
			view : "tabview",
			id : 'userTabView',
			animate : false
		// ,
		// cells: [{
		// header: "담당자",
		// body: {
		// id: "userTab",
		// rows: [{
		// view: "template",
		// template: "<font style='color:red'><b>*&nbsp;담당자 목록에서 담당자를 선택하거나 더블
		// 클릭 해주세요.</b></font>",
		// height: 30
		// },{
		// cols: [{
		// rows: this.user.tree
		// },{
		// rows: new Array().concat(this.user.searchForm, this.user.grid)
		// }]
		// }]
		// }
		// },{
		// header: "담당자 그룹",
		// body: {
		// id: "userGroupTab",
		// rows: [{
		// view: "template",
		// template: "<font style='color:red'><b>*&nbsp;담당자 그룹 목록에서 그룹을 선택하거나 더블
		// 클릭 해주세요.</b></font>",
		// height: 30
		// },{
		// cols: [{
		// rows: this.userGroup.tree
		// },{
		// rows: new Array().concat(this.userGroup.chargerUserGrid,
		// this.userGroup.chargerDeptGrid)
		// }]
		// }]
		// }
		// }
		// ]
		}
		if(this.params.display_type == "user"){
			body.cells = [ userTabItem];
		}else{
			body.cells = [ userTabItem, userGroupTabItem ];
		}
		


		this.window = createWindow({
			id : this.id,
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : [ body ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});
	}

	this.choice = function() {
		var active_tab = $$("userTabView").getTabbar().getValue();
		if (active_tab == "userTab") {
			var gridItem = $$(this.id + "_grid").getSelectedItem();
			if (gridItem) {
				var reference = this.reference;
				var fields = reference.fields;

				var user_id = nkia.ui.utils.check.nullValue(gridItem.USER_ID);
				var user_nm = nkia.ui.utils.check.nullValue(gridItem.USER_NM);

				// 부재중 체크
				var absent_yn = nkia.ui.utils.check
						.nullValue(gridItem.ABSENT_YN);
				if (absent_yn == "Y") {
					user_id = nkia.ui.utils.check.nullValue(gridItem.ABSENT_ID);
					user_nm = nkia.ui.utils.check.nullValue(gridItem.ABSENT_NM);
				}

				var values = {};
				values[fields["id"]] = user_id; // 사용자ID
				values[fields["name"]] = user_nm; // 사용자명
				values[fields["grp_select_yn"]] = "N"; // 그룹선택 여부

				reference.form._setValues(values);

				this.window.close();
			} else {
				nkia.ui.utils.notification({
					type : 'error',
					message : '선택된 담당자가 없습니다.'
				});
			}
		} else if (active_tab == "userGroupTab") {
			var item = $$(this.id + "_chargerGrpTree").getSelectedItem();
			if (item) {
				if (item.$level == 1) {
					nkia.ui.utils.notification({
						type : 'error',
						message : '최상위 그룹은 선택 할 수 없습니다.'
					});
					return;
				}

				// 해당 담당자 그룹에 사용자, 부서 모두 없는 경우 적용할 수 없음.
				var chargerUserGrid = $$(this.id + '_chargerUserGrid')
						._getRows();
				// var chargerDeptGrid = $$(this.id +
				// '_chargerDeptGrid')._getRows();
				/*var chargerUserGrpGrid = $$(this.id + '_chargerUserGrpGrid')
						._getRows();*/
				if (chargerUserGrid.length == 0
						/*&& chargerUserGrpGrid.length == 0*/) {
					nkia.ui.utils.notification({
						type : 'error',
						message : '선택된 담당자 그룹에는 매핑된 담당자가 없습니다.'
					});
					return;
				}

				var reference = this.reference;
				var fields = reference.fields;
				var user_id = nkia.ui.utils.check.nullValue(item.node_id);
				var user_nm = nkia.ui.utils.check.nullValue(item.text
						.split("[")[0]);

				var values = {};
				values[fields["id"]] = user_id; // 사용자ID
				values[fields["name"]] = user_nm; // 사용자명
				values[fields["grp_select_yn"]] = "Y"; // 그룹선택 여부

				reference.form._setValues(values);

				this.window.close();
			} else {
				nkia.ui.utils.notification({
					type : 'error',
					message : '선택된 담당자 그룹이 없습니다.'
				});
			}
		}
	};
};

/*******************************************************************************
 * MultiTaskUser Date: 2016-11-17 Version: 1.0 - Header, Header버튼, field 생성,
 * Fotter 버튼 지원
 ******************************************************************************/
nkia.ui.popup.system.MultiTaskUser = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.reference = {};

	// 구성요소 Properties
	this.user = {};
	this.selectedUser = {};
	this.window = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		props.bindEvent = this;
		props.gridHeight = 200;
		props.resource = "grid.nbpm.multitask.user.pop";
		props.checkbox = true;
		props.pageSize = 5;
		props.type = this.type;
		props.dependency = {
			targetGridId : this.id + '_selectUser'
		}
		this.user = new nkia.ui.popup.system.User();
		this.user.setProps(props);
		this.user.setRender(props);

		this.selectedUser = createGridComp({
			id : this.id + '_selectUser',
			header : {
				title : "선택된 담당자 목록"
			},
			resource : 'grid.nbpm.multitask.user.pop.sel',
			checkbox : true,
			pageable : false,
			autoLoad : false,
			keys : [ "USER_ID" ],
			dependency : {
				data : this.reference.data
			}
		});
	};

	this.setWindow = function() {
		var _this = this;

		var arrow = {
			height : 30,
			cols : [ {
				view : "template",
				template : nkia.ui.html.icon("arrow_down", "add_user"),
				onClick : {
					"add_user" : function() {
						_this.addUser();
					}
				}
			}, {
				view : "template",
				template : nkia.ui.html.icon("arrow_up", "del_user"),
				onClick : {
					"del_user" : function() {
						_this.delUser();
					}
				}
			} ]
		}

		var body = {
			cols : [
					{
						rows : this.user.tree,
						width : 250
					},
					{
						rows : new Array().concat(this.user.searchForm,
								this.user.grid, arrow, this.selectedUser)
					} ]
		}

		this.window = createWindow({
			id : this.id,
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : [ body ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});

		this.bindData();
	}

	this.bindData = function() {
		// 선택된 사용자 목록
		if (this.reference.data.length > 0) {
			var selectUser = $$(this.id + '_selectUser');
			selectUser._addRow(this.reference.data);
		}
	}

	this.addUser = function() {
		var data = $$(this.id + '_grid')._getSelectedRows();
		$$(this.id + '_selectUser')._addRow(data);

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	this.delUser = function() {
		$$(this.id + '_selectUser')._removeRow();

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	this.choice = function() {
		var rows = $$(this.id + "_selectUser")._getRows();
		// var rows = $$(this.id+'_grid')._getRows();
		if (rows.length > 0) {
			var reference = this.reference;
			var targetGrid = reference.grid;
			targetGrid._addRow(rows);

			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 담당자가 없습니다.'
			});
		}
	};
};

/*******************************************************************************
 * MultiTaskTempUser Date: 2018-07-09 Version: 1.0 - Header, Header버튼, field 생성,
 * Fotter 버튼 지원
 ******************************************************************************/
nkia.ui.popup.system.MultiTaskTempUser = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.reference = {};
	this.params = {};

	// 구성요소 Properties
	this.user = {};
	this.selectedUser = {};
	this.window = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		props.bindEvent = this;
		props.gridHeight = 150;
		props.resource = "grid.nbpm.multitask.user.pop";
		props.checkbox = true;
		props.pageSize = 5;
		props.type = this.type;
		props.dependency = {
			targetGridId : this.id + '_selectUser'
		}

		this.user = new nkia.ui.popup.system.User();
		this.user.setProps(props);
		this.user.setRender(props);

		this.selectedUser = createGridComp({
			id : this.id + '_selectUser',
			header : {
				title : "선택된 담당자 목록"
			},
			minHeight : 150,
			resource : 'grid.nbpm.multitask.user.pop.sel',
			checkbox : true,
			pageable : false,
			autoLoad : false,
			keys : [ "USER_ID" ],
			dependency : {
				data : this.reference.data
			}
		});
		this.addBtn = createBtnComp({
			label : '▼추 가',
			click : function() {
				_this.addUser();
			}
		});
		this.delBtn = createBtnComp({
			label : '▲삭 제',
			click : function() {
				_this.delUser();
			}
		});
	};

	this.setWindow = function() {
		var _this = this;

		var arrow = {
			height : 30,
			cols : [ {
				// view: "template",
				// template: nkia.ui.html.icon("arrow_down", "add_user"),
				// onClick: {
				// "add_user": function(){
				// _this.addUser();
				// }
				// }
				view : "button",
				id : "addBtn",
				value : "▼추 가",
				click : function() {
					_this.addUser();
				}

			}, {
				// view: "template",
				// template: nkia.ui.html.icon("arrow_up", "del_user"),
				// onClick: {
				// "del_user": function(){
				// _this.delUser();
				// }
				// }
				view : "button",
				id : "delBtn",
				value : "▲삭 제",
				click : function() {
					_this.delUser();
				}
			} ]
		}

		var bodyScrollView = {
			view : "scrollview",
			body : {
				cols : [
						{
							rows : this.user.tree,
							width : 250
						},
						{
							rows : new Array().concat(this.user.searchForm,
									this.user.grid, arrow, this.selectedUser)
						} ]
			}
		};

		this.window = createWindow({
			id : this.id,
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : [ bodyScrollView ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});

		this.bindData();
	}

	this.bindData = function() {
		// 선택된 사용자 목록
		if (this.reference.data.length > 0) {
			var selectUser = $$(this.id + '_selectUser');
			selectUser._addRow(this.reference.data);
		}
	}

	this.addUser = function() {
		var data = $$(this.id + '_grid')._getSelectedRows();
		$$(this.id + '_selectUser')._addRow(data);

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	this.delUser = function() {
		$$(this.id + '_selectUser')._removeRow();

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	this.choice = function() {
		var _this = this;
		var rows = $$(this.id + "_selectUser")._getRows();
		// var rows = $$(this.id+'_grid')._getRows();
		if (rows.length > 0) {
			var reference = _this.reference;
			var targetGrid = reference.grid;

			var params = this.params;
			var userList = new Array();
			for (var i = 0; i < rows.length; i++) {
				var userMap = {};
				userMap["worker_user_id"] = rows[i].USER_ID;
				userList.push(userMap);
			}
			params["multiUsers"] = userList;

			nkia.ui.utils.ajax({
				url : "/itg/nbpm/insertMultiTempInfo.do",
				params : params,
				isMask : false,
				async : false,
				success : function(response) {
					// targetGrid._addRow(rows);
					targetGrid._reload(this.params);
					_this.window.close();
				}
			});

		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 담당자가 없습니다.'
			});
		}
	};
};

/*******************************************************************************
 * DependencyUser Popup Date: 2016-11-11 Version: 1.0 - 최초등록 Description : 의존관계가
 * 있는 사용자 선택 팝업
 ******************************************************************************/
nkia.ui.popup.system.dependencyUser = function(prop) {

	this.id = "";
	this.title = "";
	this.popupParamData = {};
	this.subType = "charger";
	this.mainUrl = "/itg/system/user/searchUserList.do";
	this.subUrl = "/itg/workflow/charger/searchChargerGrpUser.do";
	this.acceptUrl = "/itg/workflow/charger/insertChargerGrpCharger.do";

	// 운영자 key 컬럼 : 사용자 ID
	this.key_column = [ "USER_ID" ];

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	// 데이터 선택팝업
	this.setRender = function(props) {

		var tree = createTreeComponent({
			id : props.id + "_tree",
			url : '/itg/base/searchDeptTree.do',
			filterable : true,
			expColable : true,
			params : this.popupParamData,
			expandLevel : 3,
			width : 250,
			header : {
				title : getConstText({
					isArgs : true,
					m_key : 'res.00034'
				})
			},
			on : {
				onItemClick : this.clickTree.bind(this)
			}
		});

		var searchForm = createFormComp({
			id : props.id + "_searchForm",
			elementsConfig : {
				labelPosition : "left"
			},
			header : {
				title : "검색",
				icon : "search"
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : "검 색",
						type : "form",
						click : this.search.bind(this)
					}), createBtnComp({
						label : "초기화",
						click : this.searchClear.bind(this)
					}) ],
					css : "webix_layout_form_bottom"
				}
			},
			fields : {
				colSize : 2,
				items : [ {
					item : createUnionFieldComp({
						items : [ createCodeComboBoxComp({
							label : getConstText({
								isArgs : true,
								m_key : 'res.label.system.00012'
							}),
							name : 'search_type',
							code_grp_id : 'SEARCH_USER',
							attachChoice : true,
							width : 250
						}), createTextFieldComp({
							name : "search_value",
							placeholder : getConstText({
								isArgs : true,
								m_key : 'msg.placeholder.search'
							}),
							on : {
								onKeyPress : this.searchByEnterKey.bind(this)
							}
						}) ]
					})
				} ]
			}
		});

		var mainGrid = createGridComp({
			id : props.id + "_mainGrid",
			pageable : true,
			pageSize : 10,
			checkbox : true,
			keys : this.key_column,
			url : this.mainUrl,
			params : this.popupParamData,
			resource : "grid.itam.managerBaseList",
			header : {
				title : this.title
			},
			dependency : {
				targetGridId : props.id + "_subGrid"
			}
		});

		var subGrid = createGridComp({
			id : props.id + "_subGrid",
			checkbox : false,
			select : false,
			// pageable: true,
			// pageSize : 10,
			resource : "grid.itam.managerBaseSelList",
			url : props.subUrl,
			params : {
				grp_id : props.popupParamData.grp_id
			},
			keys : this.key_column,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns : [ {
				column : "DEL_ACTION",
				template : nkia.ui.html.icon("remove", "remove_row"),
				onClick : {
					"remove_row" : this.removeSelGrid.bind(this)
				}
			} ]
		});

		var popupFooter = createButtons({
			align : "center",
			items : [ createBtnComp({
				label : '적 용',
				type : "form",
				click : this.choice.bind(this)
			}) ]
		});

		var body = {
			rows : [ {
				view : "accordion",
				multi : true,
				cols : [ {
					header : "부서정보",
					body : {
						rows : tree
					}
				}, {
					rows : new Array().concat(searchForm, mainGrid)
				}, {
					view : "template",
					template : nkia.ui.html.icon("arrow_right", "add_row"),
					width : 30,
					onClick : {
						"add_row" : this.addSelGrid.bind(this)
					}
				}, {
					width : 340,
					rows : new Array().concat(subGrid, popupFooter)
				} ]
			} ]
		};

		this.setWindow = function() {
			var _this = this;
			this.window = createWindow({
				id : this.id,
				width : nkia.ui.utils.getWidePopupWidth(),
				height : 670,
				header : {
					title : this.title
				},
				body : body,
				closable : true
			});
		}
	};

	// 선택되어 있는 서브 그리드 목록 getter
	this.getSelGridStoreData = function() {
		return $$(this.id + "_subGrid")._getRows();
	};

	// 서브 그리드 row 추가
	this.addSelGrid = function() {
		var data = $$(this.id + "_mainGrid")._getSelectedRows();
		if (data.length < 1) {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(this.id + "_subGrid")._addRow(data);

		this.reloadGrid(true);
	};

	// 서브 그리드 row 제거
	this.removeSelGrid = function(e, row, html) {
		var data = $$(this.id + "_subGrid")._getSelectedRows();
		if (data != null && data.length > 0) {
			if (data.length < 1) {
				nkia.ui.utils.notification({
					type : 'error',
					message : '선택된 정보가 없습니다.'
				});
				return false;
			}
			$$(this.id + "_subGrid")._removeRow();
		} else {
			$$(this.id + "_subGrid")._removeRow(row);
		}

		this.reloadGrid(true);
	};

	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e) {
		var treeItem = $$(this.id + "_tree").getItem(id);

		var form = $$(this.id + "_searchForm");
		var searchParams = form._getValues();

		searchParams.cust_id = treeItem.node_id;
		searchParams.selUserList = this.getSelGridStoreData();

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		$$(this.id + "_mainGrid")._reload(searchParams);
	};

	// 적용 이벤트
	this.choice = function() {
		var _this = this;
		var rows = $$(this.id + "_subGrid")._getRows();
		var paramMap = {};

		if (rows != null && rows.length > 0) {

			if (this.subType != undefined && this.subType == "sanctionLine") {
				// 결재라인 관리 팝업일 경우

				if (rows.length > 1) {
					nkia.ui.utils.notification({
						type : 'error',
						message : "한명의 담당자를 선택하십시오."
					});
					return false;
				}

				if (!confirm('저장하시겠습니까?')) {
					return false;
				}

				var acceptUrl = this.acceptUrl;
				var params = this.popupParamData;
				var line_id = params.line_id;
				var proc_auth_id = params.proc_auth_id;
				var process_type = params.process_type;
				var regist_user_id = USER_ID;
				var sanction_id = rows[0].USER_ID;

				var paramMap = {};
				paramMap['line_id'] = line_id;
				paramMap['sanction_id'] = sanction_id;
				paramMap['proc_auth_id'] = proc_auth_id;
				paramMap['regist_user_id'] = regist_user_id;

				nkia.ui.utils.ajax({
					url : acceptUrl,
					params : paramMap,
					success : function(data) {
						if (data.success) {
							reloadSanctionLineEditForm(true, "editTrueBtn");
							reloadSanctionGrid(false, line_id, process_type);
							$$("sanctionLineTree")._reload();

							nkia.ui.utils.notification({
								type : 'info',
								message : data.resultMsg
							});
							_this.window.close();
						} else {
							nkia.ui.utils.notification({
								type : 'error',
								message : data.resultMsg
							});
						}
					}
				});

			} else {
				// 담당자 그룹 팝업일 경우
				var acceptUrl = this.acceptUrl;
				var users = [];
				var USER_YN = "Y";
				var GRP_ID = this.popupParamData.grp_id;
				paramMap['grp_id'] = GRP_ID;
				paramMap['user_yn'] = USER_YN;

				if (!confirm('저장하시겠습니까?')) {
					return false;
				}

				for (var i = 0; i < rows.length; i++) {
					users.push(rows[i].USER_ID);
				}
				paramMap['charger_id'] = users;

				nkia.ui.utils.ajax({
					url : acceptUrl,
					params : paramMap,
					success : function(data) {
						if (data.success) {
							reloadChargerGrid(false, GRP_ID);

						} else {
							nkia.ui.utils.notification({
								type : 'error',
								message : data.resultMsg
							});
						}
					}
				});
				this.window.close();
			}
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '사용자를 선택하지 않았습니다.'
			});
			return false;
		}
	};

	// 검색 이벤트
	this.search = function() {
		var searchParams = $$(this.id + "_searchForm")._getValues();
		var treeParams = $$(this.id + "_tree").getSelectedItem();

		if (typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		searchParams.selUserList = this.getSelGridStoreData();

		$$(this.id + "_mainGrid")._reload(searchParams);
	};

	// 그리드 갱신 이벤트
	this.reloadGrid = function(isGoPage) {
		var searchParams = $$(this.id + "_searchForm")._getValues();
		var treeParams = $$(this.id + "_tree").getSelectedItem();

		if (typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		searchParams.selUserList = this.getSelGridStoreData();

		$$(this.id + "_mainGrid")._reload(searchParams, isGoPage);
	};

	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode) {
		if (keyCode) {
			if (keyCode == "13") {
				this.search();
			}
		}
	};

	// 검색창 초기화 이벤트
	this.searchClear = function() {
		$$(this.id + "_searchForm")._reset();
	};

};
nkia.ui.popup.system.priorUser = function(prop) {

	this.id = "";
	this.title = "";
	this.popupParamData = {};
	this.subType = "charger";
	this.mainUrl = "/itg/system/user/searchUserList.do";
	this.subUrl = "/itg/workflow/charger/searchChargerGrpUser.do";
	this.acceptUrl = "/itg/workflow/charger/insertChargerGrpCharger.do";

	// 운영자 key 컬럼 : 사용자 ID
	this.key_column = [ "USER_ID" ];

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	// 데이터 선택팝업
	this.setRender = function(props) {

		var tree = createTreeComponent({
			id : props.id + "_tree",
			url : '/itg/base/searchDeptTree.do',
			filterable : true,
			expColable : true,
			params : this.popupParamData,
			expandLevel : 3,
			width : 250,
			header : {
				title : getConstText({
					isArgs : true,
					m_key : 'res.00034'
				})
			},
			on : {
				onItemClick : this.clickTree.bind(this)
			}
		});

		var searchForm = createFormComp({
			id : props.id + "_searchForm",
			elementsConfig : {
				labelPosition : "left"
			},
			header : {
				title : "검색",
				icon : "search"
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : "검 색",
						type : "form",
						click : this.search.bind(this)
					}), createBtnComp({
						label : "초기화",
						click : this.searchClear.bind(this)
					}) ],
					css : "webix_layout_form_bottom"
				}
			},
			fields : {
				colSize : 2,
				items : [ {
					item : createUnionFieldComp({
						items : [ createCodeComboBoxComp({
							label : getConstText({
								isArgs : true,
								m_key : 'res.label.system.00012'
							}),
							name : 'search_type',
							code_grp_id : 'SEARCH_USER',
							attachChoice : true,
							width : 250
						}), createTextFieldComp({
							name : "search_value",
							placeholder : getConstText({
								isArgs : true,
								m_key : 'msg.placeholder.search'
							}),
							on : {
								onKeyPress : this.searchByEnterKey.bind(this)
							}
						}) ]
					})
				} ]
			}
		});

		var mainGrid = createGridComp({
			id : props.id + "_mainGrid",
			pageable : true,
			pageSize : 10,
			checkbox : true,
			keys : this.key_column,
			url : this.mainUrl,
			params : this.popupParamData,
			resource : "grid.itam.managerBaseList",
			header : {
				title : this.title
			},
			dependency : {
				targetGridId : props.id + "_subGrid"
			}
		});

		var subGrid = createGridComp({
			id : props.id + "_subGrid",
			checkbox : false,
			select : false,
			// pageable: true,
			// pageSize : 10,
			resource : "grid.itam.managerBaseSelList",
			url : props.subUrl,
			params : {
				grp_id : props.popupParamData.grp_id
			},
			keys : this.key_column,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns : [ {
				column : "DEL_ACTION",
				template : nkia.ui.html.icon("remove", "remove_row"),
				onClick : {
					"remove_row" : this.removeSelGrid.bind(this)
				}
			} ]
		});

		var popupFooter = createButtons({
			align : "center",
			items : [ createBtnComp({
				label : '적 용',
				type : "form",
				click : this.choice.bind(this)
			}) ]
		});

		var body = {
			rows : [ {
				view : "accordion",
				multi : true,
				cols : [ {
					header : "부서정보",
					body : {
						rows : tree
					}
				}, {
					rows : new Array().concat(searchForm, mainGrid)
				}, {
					view : "template",
					template : nkia.ui.html.icon("arrow_right", "add_row"),
					width : 30,
					onClick : {
						"add_row" : this.addSelGrid.bind(this)
					}
				}, {
					width : 340,
					rows : new Array().concat(subGrid, popupFooter)
				} ]
			} ]
		};

		this.setWindow = function() {
			var _this = this;
			this.window = createWindow({
				id : this.id,
				width : nkia.ui.utils.getWidePopupWidth(),
				height : 670,
				header : {
					title : this.title
				},
				body : body,
				closable : true
			});
			var idStr = "";
			var nmStr = "";
			var task_nm = this.popupParamData.panelId;
			
			idStr = $$(task_nm)._getFieldValue('prior_consulter_id').split(',');
			//기존값이 있을경우 다시셋팅 
			if(!nkia.ui.utils.check.nullValue(idStr[0]) == ""){
				var paramMap = {};
				paramMap['idArray'] = idStr; 
				nkia.ui.utils.ajax({
					url: "/itg/customize/searchUserInfo.do",
					params: paramMap,
					isMask: false,
					async: false,
					success: function(data) {
						var row = data.resultMap.targetList;
						row.forEach(function (item,i) {
							$$("multiUserPopup_subGrid")._addRow([item]);
						})
					}
				});
			}
		}
	};
	// 선택되어 있는 서브 그리드 목록 getter
	this.getSelGridStoreData = function() {
		return $$(this.id + "_subGrid")._getRows();
	};

	// 서브 그리드 row 추가
	this.addSelGrid = function() {
		var data = $$(this.id + "_mainGrid")._getSelectedRows();
		if (data.length < 1) {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(this.id + "_subGrid")._addRow(data);

		this.reloadGrid(true);
	};

	// 서브 그리드 row 제거
	this.removeSelGrid = function(e, row, html) {
		var data = $$(this.id + "_subGrid")._getSelectedRows();
		if (data != null && data.length > 0) {
			if (data.length < 1) {
				nkia.ui.utils.notification({
					type : 'error',
					message : '선택된 정보가 없습니다.'
				});
				return false;
			}
			$$(this.id + "_subGrid")._removeRow();
		} else {
			$$(this.id + "_subGrid")._removeRow(row);
		}

		this.reloadGrid(true);
	};

	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e) {
		var treeItem = $$(this.id + "_tree").getItem(id);

		var form = $$(this.id + "_searchForm");
		var searchParams = form._getValues();

		searchParams.cust_id = treeItem.node_id;
		searchParams.selUserList = this.getSelGridStoreData();

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		$$(this.id + "_mainGrid")._reload(searchParams);
	};

	// 적용 이벤트
	this.choice = function() {
		var _this = this;
		var rows = $$(this.id + "_subGrid")._getRows();
		var paramMap = {};

		if (rows != null && rows.length > 0) {
			
			var params 		   = this.popupParamData;
			var paramMap = [];
			var nm_str = "";
			var id_str = "";
			
			rows.forEach(function (item,i) {
				nm_str += item.USER_NM + "("+item.CUST_NM+")" + ",";
				id_str += item.USER_ID + ",";
			});
			nm_str = nm_str.substr(0, nm_str.length-1);
			id_str = id_str.substr(0, id_str.length-1);
			$$(params.panelId)._setFieldValue('prior_consulter_id',id_str);
			$$(params.panelId)._setFieldValue('prior_consulter_nm',nm_str);
			_this.window.close();
			
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '사용자를 선택하지 않았습니다.'
			});
			return false;
		}
	};

	// 검색 이벤트
	this.search = function() {
		var searchParams = $$(this.id + "_searchForm")._getValues();
		var treeParams = $$(this.id + "_tree").getSelectedItem();

		if (typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		searchParams.selUserList = this.getSelGridStoreData();

		$$(this.id + "_mainGrid")._reload(searchParams);
	};

	// 그리드 갱신 이벤트
	this.reloadGrid = function(isGoPage) {
		var searchParams = $$(this.id + "_searchForm")._getValues();
		var treeParams = $$(this.id + "_tree").getSelectedItem();

		if (typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		searchParams.selUserList = this.getSelGridStoreData();

		$$(this.id + "_mainGrid")._reload(searchParams, isGoPage);
	};

	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode) {
		if (keyCode) {
			if (keyCode == "13") {
				this.search();
			}
		}
	};

	// 검색창 초기화 이벤트
	this.searchClear = function() {
		$$(this.id + "_searchForm")._reset();
	};

};
/*******************************************************************************
 * 변경요청 참석자 팝업 Version: 1.0 - 최초등록 Description : 의존관계가
 * 있는 사용자 선택 팝업
 ******************************************************************************/
nkia.ui.popup.system.changeAttendees = function(prop) {

	this.id = "";
	this.title = "";
	this.popupParamData = {};
	this.subType = "charger";
	this.mainUrl = "/itg/system/user/searchUserList.do";

	// 운영자 key 컬럼 : 사용자 ID
	this.key_column = [ "USER_ID" ];

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	// 데이터 선택팝업
	this.setRender = function(props) {

		var tree = createTreeComponent({
			id : props.id + "_tree",
			url : '/itg/base/searchDeptTree.do',
			filterable : true,
			expColable : true,
			params : this.popupParamData,
			expandLevel : 3,
			width : 250,
			header : {
				title : getConstText({
					isArgs : true,
					m_key : 'res.00034'
				})
			},
			on : {
				onItemClick : this.clickTree.bind(this)
			}
		});

		var searchForm = createFormComp({
			id : props.id + "_searchForm",
			elementsConfig : {
				labelPosition : "left"
			},
			header : {
				title : "검색",
				icon : "search"
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : "검 색",
						type : "form",
						click : this.search.bind(this)
					}), createBtnComp({
						label : "초기화",
						click : this.searchClear.bind(this)
					}) ],
					css : "webix_layout_form_bottom"
				}
			},
			fields : {
				colSize : 2,
				items : [ {
					item : createUnionFieldComp({
						items : [ createCodeComboBoxComp({
							label : getConstText({
								isArgs : true,
								m_key : 'res.label.system.00012'
							}),
							name : 'search_type',
							code_grp_id : 'SEARCH_USER',
							attachChoice : true,
							width : 250
						}), createTextFieldComp({
							name : "search_value",
							placeholder : getConstText({
								isArgs : true,
								m_key : 'msg.placeholder.search'
							}),
							on : {
								onKeyPress : this.searchByEnterKey.bind(this)
							}
						}) ]
					})
				} ]
			}
		});

		var mainGrid = createGridComp({
			id : props.id + "_mainGrid",
			pageable : true,
			pageSize : 10,
			checkbox : true,
			keys : this.key_column,
			url : this.mainUrl,
			params : this.popupParamData,
			resource : "grid.itam.managerBaseList",
			header : {
				title : this.title
			},
			dependency : {
				targetGridId : props.id + "_subGrid"
			}
		});

		var subGrid = createGridComp({
			id : props.id + "_subGrid",
			checkbox : false,
			select : false,
			// pageable: true,
			// pageSize : 10,
			resource : "grid.itam.managerBaseSelList",		
			keys : this.key_column,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns : [ {
				column : "DEL_ACTION",
				template : nkia.ui.html.icon("remove", "remove_row"),
				onClick : {
					"remove_row" : this.removeSelGrid.bind(this)
				}
			} ]
		});

		var popupFooter = createButtons({
			align : "center",
			items : [ createBtnComp({
				label : '적 용',
				type : "form",
				click : this.choice.bind(this)
			}) ]
		});

		var body = {
			rows : [ {
				view : "accordion",
				multi : true,
				cols : [ {
					header : "부서정보",
					body : {
						rows : tree
					}
				}, {
					rows : new Array().concat(searchForm, mainGrid)
				}, {
					view : "template",
					template : nkia.ui.html.icon("arrow_right", "add_row"),
					width : 30,
					onClick : {
						"add_row" : this.addSelGrid.bind(this)
					}
				}, {
					width : 340,
					rows : new Array().concat(subGrid, popupFooter)
				} ]
			} ]
		};

		this.setWindow = function() {
			var _this = this;
			this.window = createWindow({
				id : this.id,
				width : nkia.ui.utils.getWidePopupWidth(),
				height : 670,
				header : {
					title : this.title
				},
				body : body,
				closable : true
			});
		}
	};

	// 선택되어 있는 서브 그리드 목록 getter
	this.getSelGridStoreData = function() {
		return $$(this.id + "_subGrid")._getRows();
	};

	// 서브 그리드 row 추가
	this.addSelGrid = function() {
		var data = $$(this.id + "_mainGrid")._getSelectedRows();
		if (data.length < 1) {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(this.id + "_subGrid")._addRow(data);

		this.reloadGrid(true);
	};

	// 서브 그리드 row 제거
	this.removeSelGrid = function(e, row, html) {
		var data = $$(this.id + "_subGrid")._getSelectedRows();
		if (data != null && data.length > 0) {
			if (data.length < 1) {
				nkia.ui.utils.notification({
					type : 'error',
					message : '선택된 정보가 없습니다.'
				});
				return false;
			}
			$$(this.id + "_subGrid")._removeRow();
		} else {
			$$(this.id + "_subGrid")._removeRow(row);
		}

		this.reloadGrid(true);
	};

	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e) {
		var treeItem = $$(this.id + "_tree").getItem(id);

		var form = $$(this.id + "_searchForm");
		var searchParams = form._getValues();

		searchParams.cust_id = treeItem.node_id;
		searchParams.selUserList = this.getSelGridStoreData();

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		$$(this.id + "_mainGrid")._reload(searchParams);
	};

	// 적용 이벤트
	this.choice = function() {
		var _this = this;
		var rows = $$(this.id + "_subGrid")._getRows();
		var paramMap = {};

		if (rows != null && rows.length > 0) {
			$$("attendees_grid")._removeAllRow();
			$$("attendees_grid")._addRow(rows);
			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '사용자를 선택하지 않았습니다.'
			});
			return false;
		}
	};

	// 검색 이벤트
	this.search = function() {
		var searchParams = $$(this.id + "_searchForm")._getValues();
		var treeParams = $$(this.id + "_tree").getSelectedItem();

		if (typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		searchParams.selUserList = this.getSelGridStoreData();

		$$(this.id + "_mainGrid")._reload(searchParams);
	};

	// 그리드 갱신 이벤트
	this.reloadGrid = function(isGoPage) {
		var searchParams = $$(this.id + "_searchForm")._getValues();
		var treeParams = $$(this.id + "_tree").getSelectedItem();

		if (typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}

		if (this.popupParamData) {
			if (this.popupParamData.oper_type) {
				searchParams["oper_type"] = this.popupParamData.oper_type;
			}
			if (this.popupParamData.process_type) {
				searchParams["process_type"] = this.popupParamData.process_type;
			}
		}

		searchParams.selUserList = this.getSelGridStoreData();

		$$(this.id + "_mainGrid")._reload(searchParams, isGoPage);
	};

	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode) {
		if (keyCode) {
			if (keyCode == "13") {
				this.search();
			}
		}
	};

	// 검색창 초기화 이벤트
	this.searchClear = function() {
		$$(this.id + "_searchForm")._reset();
	};

};

/*******************************************************************************
 * MultioperChangeManage 작업요청 담당자관리 Date: 2018-04-24 Version: 1.0 - Header,
 * Header버튼, field 생성, Fotter 버튼 지원
 ******************************************************************************/
nkia.ui.popup.system.MultioperChangeManage = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.reference = {};

	// 구성요소 Properties
	this.user = {};
	this.selectedUser = {};
	this.window = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		// props.bindEvent = this;
		// props.gridHeight = 200;
		// props.resource = "grid.nbpm.multitask.user.pop";
		// props.checkbox = false;
		// props.select = true;
		// props.pageSize = 5;
		//		
		// props.dependency = {
		// targetGridId: 'workMultiTaskGrid'
		// }
		// // 검토자 N명 세팅시 필요 파라미터
		// this.event_name = props.params.event_name;
		// this.table_name = props.params.table_name;
		// this.oper_type = props.params.oper_type;
		// this.user_id = props.params.user_id;
		// this.user_nm = props.params.user_nm;
		// this.user = new nkia.ui.popup.system.User();
		// this.type = props.type;
		// this.user.setProps(props);
		// this.user.setRender(props);
		//		
		props.bindEvent = this;
		props.gridHeight = 150;
		props.resource = "grid.nbpm.multitask.user.pop";
		props.checkbox = false;
		props.select = true;
		props.pageSize = 5;

		// props.dependency = {
		// targetGridId: this.id + '_selectUser'
		// }

		props.dependency = {
			targetGridId : 'workMultiTaskGrid'
		}

		this.event_name = props.params.event_name;
		this.table_name = props.params.table_name;
		// this.oper_type = props.params.oper_type;
		this.user_id = props.params.user_id;
		this.user_nm = props.params.user_nm;
		this.user = new nkia.ui.popup.system.User();
		this.type = props.type;
		this.user.setProps(props);
		this.user.setRender(props);
	};

	this.setWindow = function() {
		var _this = this;

		var form = createFormComp({
			id : 'form',
			header : {
				title : "변경 정보"
			},
			fields : {
				colSize : 1,
				items : [ {
					colspan : 1,
					item : createTextFieldComp({
						label : "변경 전 담당자",
						name : "USER_NM",
						readonly : true,
						value : this.user_nm
					})
				}, {
					colspan : 1,
					item : createTextFieldComp({
						label : "변경 후 담당자",
						name : "CHANGE_USER_NM",
						readonly : true
					})
				}

				]
			}
		});

		var rows = new Array().concat(this.user.tree);

		var bodyScrollView = {
			view : "scrollview",
			body : {
				cols : [
						{
							rows : rows
						},
						{
							rows : new Array().concat(this.user.searchForm,
									this.user.grid, form)
						} ]
			}
		};

		// var body = {
		// cols: [{
		// rows: rows,
		// width: 250
		// },{
		// rows: new Array().concat(this.user.searchForm, this.user.grid, form)
		// }]
		// }

		this.window = createWindow({
			id : this.id,
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : [ bodyScrollView ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '적 용',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});
	}

	this.addUser = function() {
		var data = $$(this.id + '_grid')._getSelectedRows();
		var userNm = data[0].USER_NM;

		$$('form')._setFieldValue("CHANGE_USER_NM", userNm);
	}

	this.choice = function() {
		var _this = this;
		var changeYn = false;
		var rows = $$(this.id + '_grid')._getSelectedRows();
		var userNm = $$('form')._getFieldValue("USER_NM");
		var reference = this.reference;
		var targetGrid = reference.grid;
		var targetGridRows = targetGrid._getRows();
		var selectUserNm = null;
		var selectUserId = null;

		if (rows != "") {
			selectUserNm = rows[0].USER_NM;
			selectUserId = rows[0].USER_ID;
		}

		var equalsUserNameYn = false;
		var selectTargetCd = "";
		var selectTargetCdNm = "";

		// 부모grid data validation check
		for (var a = 0; a < targetGridRows.length; a++) {
			var parentUserNm = targetGridRows[a].USER_NM;

			if (selectUserNm == parentUserNm) {
				equalsUserNameYn = true;
				break;
			}
		}

		for (var i = 0; i < targetGridRows.length; i++) {
			var parentId = targetGridRows[i].id;
			var parentUserNm = targetGridRows[i].USER_NM;

			if (rows != "") {
				if (equalsUserNameYn) {
					alert("이미 [결재자정보]에 있는 담당자 입니다.");
					break;
				} else {
					if (userNm == parentUserNm) {
						var changeYn = true;

						var params = {};
						params = _this.user.params;
						params.change_yn = changeYn;
						params.asis_user_id = this.user_id;
						params.before_worker_id = _this.user.params.user_id;
						params.after_worker_id = selectUserId;

						if (selectTargetCd != "") {
							params.help_trgt_cd = selectTargetCd;
						}

						if (_this.user.params.task_id) {
							params.nbpm_task_id = _this.user.params.task_id
									.toString();
							params.id = _this.user.params.task_id.toString();
						} else {
							params.nbpm_task_id = _this.user.params.task_id;
						}

						// 변경로직
						nkia.ui.utils
								.ajax({
									url : '/itg/customize/workerEdit/updateWorkerData.do',
									params : params,
									success : function(data) {
										if (data.success) {
											reloadWorkerGrids();
											nkia.ui.utils.notification({
												type : 'info',
												message : data.resultMsg
											});

											_this.window.close();
										} else {
											nkia.ui.utils.notification({
												type : 'error',
												message : data.resultMsg
											});
										}
									}
								});
					}
				}
			} else {
				alert("선택된 담당자가 없습니다.");
				break;
			}
		}
	};
};
/*******************************************************************************
 * Form 담당자 관리 그룹 사용자그룹 트리 팝업 Date: 2019-10-11 Version: 1.0 - Header, Header버튼,
 * field 생성, Fotter 버튼 지원
 ******************************************************************************/
nkia.ui.popup.system.userGrp = function() {
	this.window = {};
	this.tree = [];
	this.id = "";
	this.title = "";
	this.popupParamData = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		var params = props.popupParamData.params;
		params['is_all_tree'] = "Y";

		this.tree = createTreeComponent({
			id : props.id + "_tree",
			url : '/itg/system/user/searchUserGrpChargerGrp.do',
			expColable : true,
			filterable : true,
			checkbox : true,
			params : params,
			expandLevel : 4,
			on : {
				onItemDblClick : this.choice.bind(this)
			}
		});
	};
	this.setWindow = function() {
		var _this = this;

		this.window = createWindow({
			id : this.id,
			header : {
				title : this.title
			},
			body : {
				rows : this.tree
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(this)
					}) ]
				}
			},
			closable : (this.closable === undefined) ? true : this.closable
		});
	}


	this.choice = function() {
		var _this = this;
		var inputData = $$('chargerGrpEditForm').getValues();

		var selectedRecords = $$(this.id + "_tree")._getCheckedItems();

		if (selectedRecords) {
			var grp_id = inputData.GRP_ID;
			var node_id = [];
			var USER_YN = "N";

			for (var i = 0; i < selectedRecords.length; i++) {
				if(selectedRecords[i].leaf){
					node_id.push(selectedRecords[i].node_id);
				}
			}
			var paramMap = {};
			paramMap['grp_id'] = grp_id;
			paramMap['charger_id'] = node_id;
			paramMap['user_yn'] = USER_YN;

			var requestData = paramMap;
			var url = '/itg/workflow/charger/insertChargerGrpCharger.do';
			var callback = function(resultData) {
				if (resultData.success) {
					reloadChargerGrid(false, grp_id);
					// selectTreeNode(true, true);
					nkia.ui.utils.notification({
						message : resultData.resultMsg
					});
				} else {
					nkia.ui.utils.notification({
						message : resultData.resultMsg
					});
					return;
				}
			}
			callAjax(url, requestData, callback);
			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 사용자그룹정보가 없습니다.'
			});
		}
		// //
	};
};

/*******************************************************************************
 * admin Date: 2019-10-16 Version: 1.0 - Header, Header버튼, field 생성, Fotter 버튼
 * 지원
 ******************************************************************************/
nkia.ui.popup.system.admin = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.type = "";
	this.closable = true;
	this.width = 500;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.reference = {};

	// 구성요소 Properties
	this.user = {};
	this.userGroup = {};
	this.window = {};
	this.params = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		props.bindEvent = this;

		this.user = new nkia.ui.popup.system.User();
		this.user.setProps(props);
		this.user.setRender(props);

		this.userGroup = new nkia.ui.popup.system.UserGroup();
		this.userGroup.setProps(props);
		this.userGroup.setRender(props);
	};

	this.setWindow = function() {
		var _this = this;
		var bodyScrollView = {
			view : "scrollview",
			body : {
				rows : [
						{
							view : "template",
							template : "<font style='color:red'><b>*&nbsp;담당자 목록에서 담당자를 선택하거나 더블 클릭 해주세요.</b></font>",
							height : 30
						},
						{
							cols : [
									{
										rows : this.user.tree
									},
									{
										rows : new Array().concat(
												this.user.searchForm,
												this.user.grid)
									} ]
						} ]
			}
		};
		var bodyScrollViewGrp = {
			view : "scrollview",
			body : {
				rows : [
						{
							view : "template",
							template : "<font style='color:red'><b>*&nbsp;담당자 그룹 목록에서 그룹을 선택하거나 더블 클릭 해주세요.</b></font>",
							height : 30
						},
						{
							cols : [
									{
										rows : this.userGroup.tree
									},
									{
										rows : new Array()
												.concat(
														this.userGroup.chargerUserGrid,
														this.userGroup.chargerUserGrpGrid)
									} ]
						} ]
			}
		};

		var userTabItem = {
			header : "담당자",
			body : {
				id : "userTab",
				rows : [ bodyScrollView ]
			}
		};

		var userGroupTabItem = {
			header : "담당자 그룹",
			body : {
				id : "userGroupTab",
				rows : [ bodyScrollViewGrp ]
			}
		};

		var body = {
			view : "tabview",
			id : 'userTabView',
			animate : false
		}

		body.cells = [ userTabItem, userGroupTabItem ];

		this.window = createWindow({
			id : this.id,
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : [ body ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '등 록',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});
	}

	this.choice = function() {
		var active_tab = $$("userTabView").getTabbar().getValue();
		if (active_tab == "userTab") {
			var gridItem = $$(this.id + "_grid").getSelectedItem();
			if (gridItem) {
				var reference = this.reference;
				var fields = reference.fields;
				var paramMap = this.params;
				var targetGrid = this.params.targetGrid;
				paramMap["admin_id"] = gridItem.USER_ID;
				paramMap["grp_yn"] = "N";
				nkia.ui.utils.ajax({
					url : "/itg/workflow/charger/updateSubReqAdmin.do",
					params : paramMap,
					isMask : false,
					async : false,
					success : function(response) {
						// targetGrid._addRow(rows);
						// targetGrid._reload(this.params);
						// _this.window.close();
						alert("완료 되었습니다.");
						paramMap['center_id'] = $$("chargerGrpForm")
								._getFieldValue("CENTER");
						$$(targetGrid)._reload(this.params);
						// this.window.close();
					}
				});
				this.window.close();
			} else {
				nkia.ui.utils.notification({
					type : 'error',
					message : '선택된 담당자가 없습니다.'
				});
			}
		} else if (active_tab == "userGroupTab") {
			var item = $$(this.id + "_chargerGrpTree").getSelectedItem();
			if (item) {
				if (item.$level == 1) {
					nkia.ui.utils.notification({
						type : 'error',
						message : '최상위 그룹은 선택 할 수 없습니다.'
					});
					return;
				}

				// 해당 담당자 그룹에 사용자, 부서 모두 없는 경우 적용할 수 없음.
				var chargerUserGrid = $$(this.id + '_chargerUserGrid')
						._getRows();
				// var chargerDeptGrid = $$(this.id +
				// '_chargerDeptGrid')._getRows();
				var chargerUserGrpGrid = $$(this.id + '_chargerUserGrpGrid')
						._getRows();
				if (chargerUserGrid.length == 0
						&& chargerUserGrpGrid.length == 0) {
					nkia.ui.utils.notification({
						type : 'error',
						message : '선택된 담당자 그룹에는 매핑된 담당자가 없습니다.'
					});
					return;
				}

				var reference = this.reference;
				var fields = reference.fields;
				var paramMap = this.params;
				var targetGrid = this.params.targetGrid;
				paramMap["admin_id"] = item.node_id;
				paramMap["grp_yn"] = "Y";
				nkia.ui.utils.ajax({
					url : "/itg/workflow/charger/updateSubReqAdmin.do",
					params : paramMap,
					isMask : false,
					async : false,
					success : function(response) {
						// targetGrid._addRow(rows);
						// targetGrid._reload(this.params);
						// _this.window.close();
						alert("완료 되었습니다.");
						paramMap['center_id'] = $$("chargerGrpForm")
								._getFieldValue("CENTER");
						$$(targetGrid)._reload(this.params);
						// this.window.close();
					}
				});
				this.window.close();
			} else {
				nkia.ui.utils.notification({
					type : 'error',
					message : '선택된 담당자 그룹이 없습니다.'
				});
			}
		}
	};
};
/*******************************************************************************
 * ZoneSelect Date: 2019-11-12 Version: 1.0 - Header, Header버튼, field 생성, Fotter
 * 버튼 지원
 ******************************************************************************/
nkia.ui.popup.system.ZoneSelect = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 800;
	this.height = nkia.ui.utils.getWidePopupHeight();
	//this.url = "/itg/system/user/searchUserList.do";
	this.url = "/itg/itam/dns/searchDnsZoneList.do"
	this.resource = "grid.process.dns.zone.del";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 150;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};

	// 구성요소 Properties
	this.window = {};	
	this.searchForm = [];
	this.grid = [];
	this.selectedUser = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		var _this = this;

		var fieldItems = [];
		fieldItems.push({
			colspan: 1, item : createUnionFieldComp({
				items: [
					createCodeComboBoxComp({
						label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
						name: 'mgmt_place',
						code_grp_id: 'MGMT_PLACE',
						attachChoice: true
					}),
					createTextFieldComp({
						name: "zone_id_nm",
						placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
						on:{
							onKeyPress: this.doKeyPress.bind(_this)
						}
					})	
				]
			})
		});		
		fieldItems.push({
			colspan: 1, item : createCodeComboBoxComp({
				label: '고객사',
				name: 'cust_id',
				url: '/itg/system/customer/searchCustomerCodeListByUserId.do',		
				attachAll : true 
		})	
		});		
		fieldItems.push({
			colspan: 1, item : createCodeComboBoxComp({
				label: '조회 구분',
				name: 'DNS_MAPPING',
				code_grp_id: 'DNS_MAPPING',		
				attachAll : true 
		})	
		});		
		fieldItems.push({colspan:1, item :createTextFieldComp({name : 'dummy1'})});		

		 this.searchForm = createFormComp({
             id: this.id + '_searchForm',
             header: {
                    icon: "search",
                    title: PAGE_MASSEGE.SEARCH_TITLE
         },
         fields: {
             elementsConfig: {
                     labelPosition: "left"
             },
             colSize: 2,
             items: fieldItems
         },
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : PAGE_MASSEGE.SEARCH_BTN,
						type : "form",
						click : this.doSearch.bind(_this)
					}), createBtnComp({
						label : PAGE_MASSEGE.RESET_BTN,
						click : this.clearSearch.bind(_this)
					}) ],
					css : "webix_layout_form_bottom"
				}
			}
		});

		var title = getConstText({
			isArgs : true,
			m_key : 'res.00012'
		});

		if (this.type == "process") {
			title = "도메인 목록";
		}

		if (this.type == "multitask") {
			this.on = {};
		} else if (props.type == "multioperChangeManage") {
			this.on = {
				onItemClick : function() {
					if (_this.bindEvent != null) {
						_this.bindEvent.addUser(_this);
					} else {
						_this.addUser(id, e);
					}
				}
			}
			
		} else if ( props.type == "zoneSelect"){
			this.on = {
					onItemDblClick : function(id, e) {
						if (_this.bindEvent != null) {
							_this.bindEvent.addUser(_this);
						} else {
							_this.addUser(id, e);
						}
					}
				}
			}
		else {
			this.on = {
				onItemDblClick : function(id, e) {
					if (_this.bindEvent != null) {
						_this.bindEvent.choice(_this);
					} else {
						_this.choice(id, e);
					}
				}
			}
		}

		this.grid = createGridComp({
			id : this.id + '_grid',
			header : {
				title : "도메인 목록"
			},
			checkbox : this.checkbox,
			minHeight : this.gridHeight,
			resource : this.resource,
			params : this.params,
			url : this.url,
			pageable : true,
			pageSize : this.pageSize,
			keys : [ "ZONE_ID" ],
			dependency : {
				data : this.reference.data
			},
			autoLoad : false,
			on : this.on
		});
		
		
		this.selectedUser = createGridComp({
			id : this.id + '_selectUser',
			header : {
				title : "선택된 도메인 목록"
			},
			resource : 'grid.process.dns.zone.del',
			checkbox : true,
			pageable : false,
			autoLoad : false,
			keys : [ "ZONE_ID" ],
			dependency : {
				data : this.reference.data
			},
			minHeight : this.gridHeight
		});
		
		
		
		
		
		
		
	};

	this.setWindow = function() {
		var _this = this;
		
		
		var arrow = {
				height : 30,
				cols : [ {
					view : "template",
					template : nkia.ui.html.icon("arrow_down", "add_user"),
					onClick : {
						"add_user" : function() {
							_this.addUser();
						}
					}
				}, {
					view : "template",					template : nkia.ui.html.icon("arrow_up", "del_user"),
					onClick : {
						"del_user" : function() {
							_this.delUser();
						}
					}
				} ]
			}		
		
	
		
		
		
		var bodyScrollView = {
			view : "scrollview",
			body : {
				cols : [  {
					rows : new Array().concat(this.searchForm, this.grid, arrow, this.selectedUser)
				} ]
			}
		};

		this.window = createWindow({
			id : this.id,
			width : nkia.ui.utils.getWidePopupWidth(),
	    	height : nkia.ui.utils.getWidePopupHeight()	,
			header : {
				title : "DNS Zone 조회"
			},
			body : {
				rows : [ bodyScrollView ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.closable
		});
		
		this.bindData();		
		$$(this.id + '_searchForm')._hideField('dummy1');
	}

	
	this.bindData = function() {
		// 선택된 사용자 목록
		if (this.reference.data.length > 0) {
			var selectUser = $$(this.id + '_selectUser');
			selectUser._addRow(this.reference.data);			
			
		//	this.reference.grid.clearAll();
		}
	}

	this.addUser = function() {
		var data = $$(this.id + '_grid')._getSelectedRows();
		$$(this.id + '_selectUser')._addRow(data);

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	this.delUser = function() {
		$$(this.id + '_selectUser')._removeRow();

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}	
	

	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode) {
		if (keyCode && keyCode == "13") {
			this.doSearch();
		}
	}

	// 검색 버튼 이벤트
	this.doSearch = function() {
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if (this.params) {
			if (this.params.oper_type) {
				data["oper_type"] = this.params.oper_type;
			}
			if (this.params.process_type) {
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function() {
		var rows = $$(this.id + "_selectUser")._getRows();
		this.reference.grid.clearAll();
		// var rows = $$(this.id+'_grid')._getRows();
		if (rows.length > 0) {
			var reference = this.reference;
			var targetGrid = reference.grid;
			targetGrid._addRow(rows);

			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 담당자가 없습니다.'
			});
		}
	};
};

/********************************************
 * RecordSerch
 * Date: 2019-11-12
 * Version:
 ********************************************/
nkia.ui.popup.system.RecordSerch = function(type) {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.reference = {};
	
	// 구성요소 Properties
	this.record = {};
	this.selectedRecord = {};
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		props.bindEvent = this;
		props.gridHeight = 200;
		props.resource = "grid.process.service.recordSerch";
		props.checkbox = true;
		props.pageSize = 5;
		props.type = this.type;
		props.dependency = {
			targetGridId: this.id + '_selectRecord'
		}
		this.record = new nkia.ui.popup.system.Record();
		this.record.setProps(props);
		this.record.setRender(props);
		
		this.selectedRecord = createGridComp({
			id: this.id + '_selectRecord',
			header: {
				title: "선택된 레코드 목록"
			},
		    resource: 'grid.process.service.recordSerch',
			checkbox: true,
			pageable: false,
			autoLoad: false,
			keys: ["AM_RECORD_ID"],
			dependency: {
				data: this.reference.data
			}
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		
		var arrow = {
			height: 30,
			cols: [
			{
				view: "template",
				template: nkia.ui.html.icon("arrow_down", "add_record"),
	 			onClick: {
	 				"add_record": function(){
	 					_this.addRecord();
	 				}
	 			}
			},{
				view: "template",
	 			template: nkia.ui.html.icon("arrow_up", "del_record"),
	 			onClick: {
	 				"del_record": function(){
	 					_this.delRecord();
	 				}
	 			}
			}]
        }
        
		var body = {
				view : "scrollview",
				body : {
					cols : [  {
                rows: new Array().concat(this.record.searchForm, this.record.grid, arrow, this.selectedRecord)
					} ]
				}
		}

		this.window = createWindow({
	    	id: this.id,
	    	width : nkia.ui.utils.getWidePopupWidth(),
	    	height : nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [body]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	    
	    this.bindData();
	}
	
	this.bindData = function(){
		// 선택된 사용자 목록
	    if(this.reference.data.length > 0){
	    	var selectRecord = $$(this.id + '_selectRecord');
	    	selectRecord._addRow(this.reference.data);
	    }
	}
	
	this.addRecord = function(){
		var data = $$(this.id + '_grid')._getSelectedRows();
		var gubun = null;
		
		if(type == "recordSerch"){
			gubun = "CHANGE";
		}else if(type == "recordSerchDelete"){
			gubun = "DELETE";
		}
		
		
	    for (var i = 0; i < data.length; i++) {
			data[i].GUBUN_NM = gubun;
	    }
	    
		$$(this.id + '_selectRecord')._addRow(data);
		
		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}
	
	this.delRecord = function(){
		$$(this.id + '_selectRecord')._removeRow();
		
		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}
	
	this.choice = function(){
		var rows = $$(this.id + "_selectRecord")._getRows();
//		var rows = $$(this.id+'_grid')._getRows();
		if(rows.length > 0){
			var reference = this.reference;
			var targetGrid = reference.grid;
			targetGrid._removeAllRow();
			targetGrid._addRow(rows);
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 Record가  없습니다.'
            });
        }
	};
};

nkia.ui.popup.system.Record = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 900;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.url = "/itg/itam/dns/searchDnsRecordList.do";
	this.resource = "grid.process.service.recordSerch";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 150;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
				
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_record_search',
	                    				code_grp_id: 'RECORD_SEARCH',
	                    				attachChoice: true,
	                    				width: 250
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	},
					{ colspan: 2, item : createUnionFieldComp({
						items: [
							createCodeComboBoxComp({ //관리구분
								label: getConstText({isArgs : true, m_key : 'res.dns.record.000001'}),
								name: 'search_mgmt_place',
								code_grp_id: 'MGMT_PLACE',
								attachChoice: true
							}),
							createCodeComboBoxComp({// 레코드명
								label: getConstText({isArgs : true, m_key : 'res.dns.record.000002'}),
								name: 'search_record_type_cd',
								code_grp_id: 'RECORD_TYPE_CD',
								attachChoice: true
							})
							]
						})
					}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = getConstText({isArgs : true, m_key : 'res.dns.record.000004'});
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			minHeight: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["AM_RECORD_ID"],
			dependency: this.dependency
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
			                rows: this.tree
			            },{
			                rows: new Array().concat(this.searchForm, this.grid)
			            }]
					}
				};

		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if(this.params){
			if(this.params.oper_type){
				data["oper_type"] = this.params.oper_type; 
			}
			if(this.params.process_type){
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	this.addRecord = function(id){
		var data = $$(this.id + '_grid')._getSelectedRows();
		$$(this.id + '_selectRecord')._addRow(data);
		
		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			var reference = this.reference;
			var fields = reference.fields;
			             
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 담당자가 없습니다.'
            });
        }
	};
};

/********************************************/
nkia.ui.popup.system.lowformatlist = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 1500;
	this.height = nkia.ui.utils.getWidePopupHeight();
	//this.url = "/itg/system/user/searchUserList.do";
	this.url = "/itg/itam/lowformat/searchHostInfo.do"
	this.resource = "grid.itam.gethostnm";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 150;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};

	// 구성요소 Properties
	this.window = {};	
	this.searchForm = [];
	this.grid = [];
	this.selectedUser = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		var _this = this;
		
		
		var fieldItems = [];
		
		fieldItems.push({colspan : 2, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.monitoring.label.itam.opms.00001'}), name : "class_id", 	required : true, popUpType : "amdbPopup", srId : "$!{dataMap.sr_id}"})});
		fieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.common.label.hostnm'}), name : "host_nm"})});
		fieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.label.itam.oper.00006'}), name : "main_ip", required : false})});
		fieldItems.push({colspan : 1, item : createCodeComboBoxComp({ // 센터구분
				label : getConstText({ isArgs : true, m_key : 'res.label.itam.opms.00020'}),
				name : 'location_cd',
				code_grp_id : 'CENTER',
				attachChoice : true
			}) });
		fieldItems.push({colspan : 1, item : createCodeComboBoxComp({// 고객사
			label : getConstText({ isArgs : true, m_key : 'res.common.status'}),
			name : 'asset_state',
			code_grp_id : 'ASSET_STATE',
			attachChoice : true
		}) });	
		
		 this.searchForm = createFormComp({
             id: this.id + '_searchForm',
             header: {
                    icon: "search",
                    title: PAGE_MASSEGE.SEARCH_TITLE
         },
         fields: {
             elementsConfig: {
                     labelPosition: "left"
             },
             colSize: 3,
             items : fieldItems,
			 hiddens: {			 
		       		CONF_ID : ""				
			    }
         },
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : PAGE_MASSEGE.SEARCH_BTN,
						type : "form",
						click : this.doSearch.bind(_this)
					}), createBtnComp({
						label : PAGE_MASSEGE.RESET_BTN,
						click : this.clearSearch.bind(_this)
					}) ],
					css : "webix_layout_form_bottom"
				}
			}
		});


		this.on = {
			onItemDblClick : function(id, e) {
				if (_this.bindEvent != null) {
					_this.bindEvent.addUser(_this);
				} else {
					_this.addUser(id, e);
				}
				//_this.addUser(id, e);
			}
		}	

		this.grid = createGridComp({
			id : this.id + '_grid',
			header : {
				title : "장비 목록"
			},
			checkbox : this.checkbox,
			minHeight : this.gridHeight,
			resource : this.resource,			
			params : this.params,
			url : this.url,
			pageable : true,
			pageSize : this.pageSize,
			
			keys : [ "HOST_NM" ],			
			dependency : {
				data : this.reference.data
			},
			autoLoad : false,
			on : this.on,
			minHeight : 200
		});
		
		
		this.selectedUser = createGridComp({
			id : this.id + '_selectUser',
			header : {
				title : "선택된 장비 목록",
				buttons : {
					align : "right",
					items : [
								createBtnComp({
									id:"btn1",
									label: getConstText({ isArgs : true, m_key : 'res.common.delete'}) /*삭제*/,
									type: "form",
									click: function() {
									
										_this.delUser();
					
									
									}
								})															
							]
				}
			},
			resource : 'grid.process.lowformat',
			checkbox : true,
			pageable : false,
			autoLoad : false,
			minHeight : this.gridHeight,			
			keys : [ "HOST_NM" ],
			dependency : {
				data : this.reference.data
			},
			minHeight : 200
		});	
	};

	this.setWindow = function() {
		var _this = this;
		
		
		var arrow = {
				height : 30,
				cols : [ {
					view : "template",
					template : nkia.ui.html.icon("arrow_down", "add_user"),
					onClick : {
						"add_user" : function() {
							_this.addUser();
						}
					}
				}, {
					view : "template",
					template : nkia.ui.html.icon("arrow_up", "del_user"),
					onClick : {
						"del_user" : function() {
							_this.delUser();
						}
					}
				} ]
			}		
		
	
		
		
		
		var bodyScrollView = {
			view : "scrollview",
			body : {
				cols : [  {
					rows : new Array().concat(this.searchForm, this.grid, this.selectedUser)
				} ]
			}
		};

		this.window = createWindow({
			id : this.id,
			width : nkia.ui.utils.getWidePopupWidth(),
			height : nkia.ui.utils.getWidePopupHeight()	,
			header : {
				title : "장비 List 조회"
			},
			body : {
				rows : [ bodyScrollView ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.choice.bind(_this)
		});
		
		this.bindData();		
				
	}

	
	this.bindData = function() {
		// 선택된 사용자 목록
		if (this.reference.data.length > 0) {
			var selectUser = $$(this.id + '_selectUser');
			selectUser._addRow(this.reference.data);			
			
			
		}
	}

	this.addUser = function() {
		var data = $$(this.id + '_grid')._getSelectedRows();
		
		$$(this.id + '_selectUser')._addRow(data);

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	this.delUser = function() {
		$$(this.id + '_selectUser')._removeRow();

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}	
	

	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode) {
		if (keyCode && keyCode == "13") {
			this.doSearch();
		}
	}

	// 검색 버튼 이벤트
	this.doSearch = function() {
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if (this.params) {
			if (this.params.oper_type) {
				data["oper_type"] = this.params.oper_type;
			}
			if (this.params.process_type) {
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function() {
		var rows = $$(this.id + "_selectUser")._getRows();
		this.reference.grid.clearAll();
		// var rows = $$(this.id+'_grid')._getRows();
		if (rows.length > 0) {
			var reference = this.reference;
			var targetGrid = reference.grid;
			targetGrid._addRow(rows);

			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 담당자가 없습니다.'
			});
		}
	};
};

/********************************************/
nkia.ui.popup.system.centermove = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 1500;
	this.height = nkia.ui.utils.getWidePopupHeight();	
	this.url = "/itg/itam/serviceCenterMove/serviceCenterMoveList.do"
	this.resource = "grid.process.center.move.popup";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 150;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};

	// 구성요소 Properties
	this.window = {};	
	this.searchForm = [];
	this.grid = [];
	this.selectedUser = {};

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		var _this = this;
		var loc_info = $$('requst_regist')._getFieldValue('center_id');
		
		var fieldItems = [];
		
		fieldItems.push({colspan : 2, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.monitoring.label.itam.opms.00001'}), name : "class_id", 	required : true, popUpType : "amdbPopup", srId : "$!{dataMap.sr_id}"})});
		fieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.common.label.hostnm'}), name : "host_nm", on : {	onKeyPress : this.doKeyPress.bind(_this)}})}); //doKeyPress
		//fieldItems.push({colspan : 1, item : createTextFieldComp({label : getConstText({ isArgs : true, m_key : 'res.label.itam.oper.00006'}), name : "main_ip", required : false})});
		/*
		 *  createTextFieldComp({
							name : "search_value",
							placeholder : getConstText({
								isArgs : true,
								m_key : 'msg.placeholder.search'
							})
						})
		 * */
		
		fieldItems.push({colspan : 2, item : createPopBtnFieldComp({label : getConstText({	isArgs : true, m_key : 'res.center.move.label.customer.0001'}), name : "customer_id_nm", 	required : true, popUpType : "monitoringcustomer", srId : "$!{dataMap.sr_id}"})});
		
		fieldItems.push({colspan : 1, item : createCodeComboBoxComp({ // 센터구분
				label : getConstText({ isArgs : true, m_key : 'res.label.itam.opms.00020'}),
				name : 'location_cd',
				value : loc_info,
				code_grp_id : 'CENTER',
				attachChoice : true,
				readonly : true
			}) });
		
		
		 this.searchForm = createFormComp({
             id: this.id + '_searchForm',
             header: {
                    icon: "search",
                    title: PAGE_MASSEGE.SEARCH_TITLE
         },
         fields: {
             elementsConfig: {
                     labelPosition: "left"
             },
             colSize: 3,
             items : fieldItems
         },
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : PAGE_MASSEGE.SEARCH_BTN,
						type : "form",
						click : this.doSearch.bind(_this)
					}), createBtnComp({
						label : PAGE_MASSEGE.RESET_BTN,
						click : this.clearSearch.bind(_this)
					}) ],
					css : "webix_layout_form_bottom"
				}
			}
		});


		this.on = {
			onItemDblClick : function(id, e) {
				if (_this.bindEvent != null) {
					_this.bindEvent.addUser(_this);
				} else {
					_this.addUser(id, e);
				}
				//_this.addUser(id, e);
			}
		}	

		this.grid = createGridComp({
			id : this.id + '_grid',
			header : {
				title : "장비 목록"
			},
			checkbox : this.checkbox,
			minHeight : this.gridHeight,
			resource : this.resource,			
			params : this.params,
			url : this.url,
			pageable : true,
			pageSize : this.pageSize,
			
			keys : [ "HOST_NM" ],			
			dependency : {
				data : this.reference.data
			},
			autoLoad : false,
			on : this.on
		});
		
		
		this.selectedUser = createGridComp({
			id : this.id + '_selectUser',
			header : {
				title : "선택된 장비 목록",
				buttons : {
					align : "right",
					items : [
								createBtnComp({
									id:"btn1",
									label: getConstText({ isArgs : true, m_key : 'res.common.delete'}) /*삭제*/,
									type: "form",
									click: function() {
									
										_this.delUser();
					
									
									}
								})															
							]
				}
			},
			resource : 'grid.process.center.move',
			checkbox : true,
			pageable : false,
			autoLoad : false,
			minHeight : this.gridHeight,			
			keys : [ "HOST_NM" ],
			dependency : {
				data : this.reference.data
			}
		});	
	};

	this.setWindow = function() {
		var _this = this;
		
		
		var arrow = {
				height : 30,
				cols : [ {
					view : "template",
					template : nkia.ui.html.icon("arrow_down", "add_user"),
					onClick : {
						"add_user" : function() {
							_this.addUser();
						}
					}
				}, {
					view : "template",
					template : nkia.ui.html.icon("arrow_up", "del_user"),
					onClick : {
						"del_user" : function() {
							_this.delUser();
						}
					}
				} ]
			}		
		
	
		
		
		
		var bodyScrollView = {
			view : "scrollview",
			body : {
				cols : [  {
					rows : new Array().concat(this.searchForm, this.grid, this.selectedUser)
				} ]
			}
		};

		this.window = createWindow({
			id : this.id,
			width : nkia.ui.utils.getWidePopupWidth(),
			height : nkia.ui.utils.getWidePopupHeight()	,
			header : {
				title : "장비 List 조회"
			},
			body : {
				rows : [ bodyScrollView ]
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '선 택',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : this.choice.bind(_this)
		});
		
		this.bindData();		
				
	}

	
	this.bindData = function() {
		// 선택된 사용자 목록
		if (this.reference.data.length > 0) {
			var selectUser = $$(this.id + '_selectUser');
			selectUser._addRow(this.reference.data);			
			
			
		}
	}

	this.addUser = function() {
		var data = $$(this.id + '_grid')._getSelectedRows();
		
		$$(this.id + '_selectUser')._addRow(data);

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}

	this.delUser = function() {
		$$(this.id + '_selectUser')._removeRow();

		// 사용자 목록 새로고침
		$$(this.id + '_grid')._reload(null, true);
	}	
	

	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode) {
		if (keyCode && keyCode == "13") {
			this.doSearch();
		}
	}

	// 검색 버튼 이벤트
	this.doSearch = function() {
		var form = $$(this.id + '_searchForm');
		var data = form._getValues();
		if (this.params) {
			if (this.params.oper_type) {
				data["oper_type"] = this.params.oper_type;
			}
			if (this.params.process_type) {
				data["process_type"] = this.params.process_type;
			}
		}
		$$(this.id + '_grid')._reload(data);
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();
		var loc_info = $$('requst_regist')._getFieldValue('center_id');
		$$(this.id + '_searchForm')._setFieldValue("location_cd", loc_info);	
	}

	// 적용(선택) 이벤트
	this.choice = function() {
		var rows = $$(this.id + "_selectUser")._getRows();
		this.reference.grid.clearAll();
		// var rows = $$(this.id+'_grid')._getRows();
		if (rows.length > 0) {
			var reference = this.reference;
			var targetGrid = reference.grid;
			targetGrid._addRow(rows);

			this.window.close();
		} else {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 담당자가 없습니다.'
			});
		}
	};
};


/**

/**
 * 사용자 팝업
 * 
 * @param {}
 *            popupProps
 */
function createUserWindow(popupProps) {
	var props = popupProps || {};
	var user;
	try {
		switch (props.type) {
		case "process":
			user = new nkia.ui.popup.system.ProcessUser();
			break;
		case "multitask":
			user = new nkia.ui.popup.system.MultiTaskUser();
			break;
		case "multiTaskTemp":
			user = new nkia.ui.popup.system.MultiTaskTempUser();
			break;
		case "zoneSelect":
			user = new nkia.ui.popup.system.ZoneSelect();
			break;
		case "dependencyUser":
			user = new nkia.ui.popup.system.dependencyUser();
			break;
		case "attendees":
			user = new nkia.ui.popup.system.changeAttendees();
			break;
		case "multioperChangeManage":
			user = new nkia.ui.popup.system.MultioperChangeManage(); // (관리화면)작업요청
																		// 담당자관리
			break;
		case "usergrp":
			user = new nkia.ui.popup.system.userGrp(); // 담당자그룹관리 에서 사용자그룹등록
			break;
		case "admin":
			user = new nkia.ui.popup.system.admin(); // 세부유형별 관리자관리 에서 사용자및
														// 사용자그룹등록
			break;
		case "recordSerch":
			user = new nkia.ui.popup.system.RecordSerch(props.type);
				break;	
		case "recordSerchDelete":
				user = new nkia.ui.popup.system.RecordSerch(props.type);
				break;
		case "lowformatlist":
			user = new nkia.ui.popup.system.lowformatlist();		
			break;
		case "centermove":
			user = new nkia.ui.popup.system.centermove();		
			break;		
		case "priorUser":
			user = new nkia.ui.popup.system.priorUser();		
			break;
		default:
			user = new nkia.ui.popup.itam.User();
		}
		user.setProps(props);
		user.setRender(props);
		user.setWindow();
	} catch (e) {
		alert(e.message);
	}
}
