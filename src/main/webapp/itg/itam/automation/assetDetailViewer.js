/*************************************************************
 * 
 * 사이트의 특성에 따라 커스텀 스크립팅이 필요한 함수들을 모아두는 페이지 입니다.
 * 
 *************************************************************/

/**
 * getTabListHistoryData
 * 탭 리스트의 이력을 모듈에 보내줄 데이터를 만들어주는 함수
 * (탭 리스트는 이력을 위한 데이터 가공이 필요함)
 */
function getTabListHistoryData(tabList) {
	
	var _HIST_DATA_SUFFIX = "_HIST";
	var resultList = new Array();
	
	// Tab List 추가에 따른 커스텀 처리 필요함.
	var relInfraData = {};
	var installInfoData = {};
	
	for(var i=0; i<tabList.length; i++) {
		if(tabList[i]["entity_type"] == "REL_INFRA") {
			// VM은 이력등록 제외
			if('VM' != tabList[i]["tab_id"]) {
				// 연관장비
				relInfraData[tabList[i]["tab_id"]] = tabList[i]["data"];
			}
			relInfraData["entity_mng_type"] = tabList[i]["entity_mng_type"];
		} else if(tabList[i]["entity_type"] == "INSTALL") {
			// 설치정보
			installInfoData[tabList[i]["tab_id"]] = tabList[i]["data"];
		}
	}
	
	// 연관장비
	resultList.push({
		type : "TL",
		entity_type : "REL_INFRA" + _HIST_DATA_SUFFIX,
		data : relInfraData
	});
	
	// 설치정보
	resultList.push({
		type : "TL",
		entity_type : "INSTALL" + _HIST_DATA_SUFFIX,
		data : installInfoData
	});
	
	return resultList;
}

/**
 * 자산상세 팝업 오픈
 * @param param
 */
function openAssetDetailPop(param){
//	param 형식 :  "?asset_id="+asset_id+"&conf_id="+conf_id+"&class_id="+class_id+"&view_type="+view_type+"&entity_class_id="+entity_class_id;
	var url = "/itg/itam/automation/goAssetDetailViewer.do" + param;
	window.open(getConstValue('CONTEXT') +url, "assetPop", "width=1200,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

// 자산원장 속성 커스텀 이벤트
function setCutomEvent(params) {
	/*
	 * 아래의 경우 이벤트 처리 안함
	 * 
	 * 1. 자산 상태가 [폐기]일 경우
	 * 2. 뷰 유형이 [tab_view]일 경우
	 */
	if(!params
			|| 'DISUSE' == params.asset_state
			|| 'tab_view' == params.view_type) {
		return false;
	}
	
	// 센터위치 비활성화 (상면코드 선택시 자동 선택됨)
//	if($$('LOCATION_CD')) {
//		$$('LOCATION_CD').disable();
//	}
	
	// IT자원 신규등록일 경우
	if('regist' == params.view_type || 'info' == params.view_type) {
		// 분류체계 셋팅
		if($$('CLASS_ID_POP_NM')) {
			$$('CLASS_ID_POP_NM').setValue(params.class_id_nm);
		}
		// 구성용도 셋팅
		if($$('CONF_TYPE_POP_NM')) {
			$$('CONF_TYPE_POP_NM').setValue(params.conf_type_nm);
		}
		// 구성종류 셋팅
		if($$('CONF_KIND_CD')) {
			$$('CONF_KIND_CD').setValue(params.conf_kind_cd);
		}
		
		/*
		// 가상화 구분
		if($$('CI_IF_TYPE')) {
			$$('CI_IF_TYPE').config.filteredData = ['LCLOUD','DRAAS'];
			$$('CI_IF_TYPE').refresh();
		}
		*/
	} else {
		// 반입일 수정불가
		if($$('INDUCTION_DT')) {
			$$('INDUCTION_DT').disable();
		}
		
		/*
		// 가상화 구분
		if($$('CI_IF_TYPE')) {
			// 가상화 구분 비활성화
			$$('CI_IF_TYPE').disable();
		}
		*/
	}
	
	/*
	// 가상화 구분
	if($$('CI_IF_TYPE')) {
		var ciIfType = $$('CI_IF_TYPE').getValue();
		
		if($$('PHYSI_CONF_ID_POP_NM')) {
			// 가상화 구분이 [자체가상화]일 경우
			if('VIRTUAL' == ciIfType) {
				// 소유주체 활성 처리
				$$('PHYSI_CONF_ID_POP_NM').enable();
			} else {
				// 소유주체 비활성 처리
				$$('PHYSI_CONF_ID_POP_NM').disable();
			}
		}
		
		// 가상화 구분 변경 이벤트
		$$('CI_IF_TYPE').attachEvent("onChange", function(newv, oldv){
			if($$('PHYSI_CONF_ID_POP_NM')) {
				// 가상화 구분이 [자체가상화]일 경우
				if('VIRTUAL' == newv) {
					// 물리서버명 활성 처리
					$$('PHYSI_CONF_ID_POP_NM').enable();
				} else {
					// 물리서버명 비활성 처리
					$$('PHYSI_CONF_ID_POP_NM').disable();
					$$('PHYSI_CONF_ID_POP_NM').setValue('');
				}
			}
		});
	}
	*/
	// 소유형태
	/*
	if($$('ASSET_OWN_TYPE_CD')) {
		var ownType = $$('ASSET_OWN_TYPE_CD').getValue();
		
		if($$('OWNER')) {
			// 소유형태가 [임대]일 경우
			if('RENT' == ownType) {
				// 소유주체 활성 처리
				$$('OWNER').enable();
			} else {
				// 소유주체 비활성 처리
				$$('OWNER').disable();
			}
		}
		
		// 소유형태 변경 이벤트
		$$('ASSET_OWN_TYPE_CD').attachEvent("onChange", function(newv, oldv){
			if($$('OWNER')) {
				// 소유형태가 [임대]일 경우
				if('RENT' == newv) {
					// 소유주체 활성 처리
					$$('OWNER').enable();
				} else {
					// 소유주체 비활성 처리
					$$('OWNER').setValue('');
					$$('OWNER').disable();
				}
			}
		});
	}
	
	// 고객사
	if($$('CUSTOMER_ID_POP_NM')) {
		$$('CUSTOMER_ID_POP_NM').attachEvent("onChange", function(newv, oldv){
		    // 분류체계가 문서가 아닐경우
			if('DC' != params.class_type) {
				if($$('RS')) {
			    	$$('RS')._removeAllRow();
			    }
			    
			    nkia.ui.utils.notification({
					type: 'info',
					message: '[주의]<br><br>고객사를 변경할 경우<br>관계정보 > 서비스 탭의 데이터가 삭제됩니다'
				});
		    }
		});
	}
	*/
	/*
	if($$('CONF_KIND_CD')) {
		var confKindCd = $$('CONF_KIND_CD').getValue();
		
		if('PHYSI' == confKindCd) {
			if(params.view_type == "regist") {
				if(params.mng_type == "ASSETCONF") {
					if($$('ASSET_ID_POP_NM')) {
						$$('ASSET_ID_POP_NM').disable();					
					}
				} else {
					if($$('ASSET_ID_POP_NM')) {
						$$('ASSET_ID_POP_NM').disable();					
					}
				}
			} else {
				if($$('ASSET_ID_POP_NM')) {
					$$('ASSET_ID_POP_NM').enable();				
				}
			}
		} else {
			// 자산번호 비활성 처리
			if($$('ASSET_ID_POP_NM')) {
				$$('ASSET_ID_POP_NM').disable();			
			}
			
		}
		
		$$('CONF_KIND_CD').attachEvent("onChange", function(newv, oldv){
			
			if($$('CONF_KIND_CD')) {
				// 구성분류가 물리구성일 때
				if('PHYSI' == newv) {
					if(params.view_type == "regist") {
						// 자산번호 활성 처리
						if(params.mng_type == "ASSETCONF") {
							if($$('ASSET_ID_POP_NM')) {
								$$('ASSET_ID_POP_NM').disable();					
							}
						} else {
							if($$('ASSET_ID_POP_NM')) {
								$$('ASSET_ID_POP_NM').enable();				
							}
						}					
					} else {
						if($$('ASSET_ID_POP_NM')) {
							$$('ASSET_ID_POP_NM').enable();				
						}
					}
					
				} else {
					// 자산번호 비활성 처리
//					$$('ASSET_ID').setValue('');
					if($$('ASSET_ID_POP_NM')) {
						$$('ASSET_ID_POP_NM').disable();			
					}
				}
			}
		});
	}
	*/
	
	// 자산상태
	if($$('ASSET_STATE')) {
		var assetState = $$('ASSET_STATE').getValue();
		/*
		if($$('DISK_FORMAT_TYPE_CD')) {
			// 자산상태가 [폐기]일 경우
			
			if('DISUSE' == assetState) {
				// 데이타파기방법 활성 처리
				$$('DISK_FORMAT_TYPE_CD').enable();
			} else {
				// 데이타파기방법 비활성 처리
				$$('DISK_FORMAT_TYPE_CD').disable();
			}
		}
		*/
		if($$('OUT_DT')) {
			// 자산상태가 [폐기]일 경우
			if('DISUSE' == assetState) {
				// 반출일 활성 처리
				$$('OUT_DT').enable();
			} else {
				// 반출일 비활성 처리
				$$('OUT_DT').disable();
			}
		}
		
		// 소유형태 변경 이벤트
		$$('ASSET_STATE').attachEvent("onChange", function(newv, oldv){
			/*
			if($$('DISK_FORMAT_TYPE_CD')) {
				// 자산상태가 [폐기]일 경우
				if('DISUSE' == newv) {
					// 데이타파기방법 활성 처리
					$$('DISK_FORMAT_TYPE_CD').enable();
				} else {
					// 데이타파기방법 비활성 처리
					$$('DISK_FORMAT_TYPE_CD').setValue('');
					$$('DISK_FORMAT_TYPE_CD').disable();
				}
			}
			*/
			
			if($$('OUT_DT')) {
				// 자산상태가 [폐기]일 경우
				if('DISUSE' == newv) {
					// 반출일 활성 처리
					$$('OUT_DT').enable();
				} else {
					// 반출일 비활성 처리
					$$('OUT_DT').setValue('');
					$$('OUT_DT').disable();
				}
			}
		});
	}
	
	// 가상화 HOST 여부
	if($$('VIRTUAL_YN')) {
		var virtualYn = $$('VIRTUAL_YN').getValue();
		
		// 가상화방식 활성 처리
		if($$('VIRTUAL_TYPE_CD')) {
			// 가상화 HOST 여부가 [Y]일 경우
			if('Y' == virtualYn) {
				// 가상화방식 활성 처리
				$$('VIRTUAL_TYPE_CD').enable();
			} else {
				// 가상화방식 비활성 처리
				$$('VIRTUAL_TYPE_CD').disable();
			}
		}
		
		// 가상화 HOST 여부 변경 이벤트
		$$('VIRTUAL_YN').attachEvent("onChange", function(newv, oldv){
			if($$('VIRTUAL_TYPE_CD')) {
				// 가상화 HOST 여부가  [Y]일 경우
				if('Y' == newv) {
					// 가상화방식 활성 처리
					$$('VIRTUAL_TYPE_CD').enable();
				} else {
					// 가상화방식 비활성 처리
					$$('VIRTUAL_TYPE_CD').setValue('');
					$$('VIRTUAL_TYPE_CD').disable();
				}
			}
		});
	}
	
	// 제조사 코드
	/*
	if($$('VENDOR_CD')) {
		var vendorCd = $$('VENDOR_CD').getValue();
		
		if($$('VENDOR_DETAIL')) {
			// 제조사 코드가 [직접입력]일 경우
			if('NOTCODE' == vendorCd) {
				// 제조사명 활성 처리
				$$('VENDOR_DETAIL').enable();
			} else {
				// 제조사명 비활성 처리
				$$('VENDOR_DETAIL').disable();
			}
		}
		
		// 제조사 코드 변경 이벤트
		$$('VENDOR_CD').attachEvent("onChange", function(newv, oldv){
			if($$('VENDOR_DETAIL')) {
				// 제조사 코드가 [직접입력]일 경우
				if('NOTCODE' == newv) {
					// 제조사명 활성 처리
					$$('VENDOR_DETAIL').enable();
				} else {
					// 제조사명 비활성 처리
					$$('VENDOR_DETAIL').setValue('');
					$$('VENDOR_DETAIL').disable();
				}
			}
		});
	}
	
	// 유지보수여부
	if($$('MAINT_CONTRACT_YN')) {
		var vendorCd = $$('MAINT_CONTRACT_YN').getValue();
		
		if($$('MAINT_VENDOR')) {
			// 유지보수여부가 [Y]일 경우
			if('Y' == vendorCd) {
				// 유지보수업체명 활성 처리
				$$('MAINT_VENDOR').enable();
			} else {
				// 유지보수업체명 비활성 처리
				$$('MAINT_VENDOR').disable();
			}
		}
		
		// 유지보수여부 변경 이벤트
		$$('MAINT_CONTRACT_YN').attachEvent("onChange", function(newv, oldv){
			if($$('MAINT_VENDOR')) {
				// 유지보수여부가 [Y]일 경우
				if('Y' == newv) {
					// 유지보수업체명 활성 처리
					$$('MAINT_VENDOR').enable();
				} else {
					// 유지보수업체명 비활성 처리
					$$('MAINT_VENDOR').setValue('');
					$$('MAINT_VENDOR').disable();
				}
			}
		});
	}
	
	// 상면코드
	if($$('CENTER_POSITION_CD_POP_NM')) {
		var centerPositionCd = $$('CENTER_POSITION_CD_POP_NM').getValue();
		
		if($$('POSITION_DETAIL')) {
			// 상면코드가 [기타]일 경우
			if(centerPositionCd.match(/기타위치/gi)) {
				// 기타위치 활성 처리
				$$('POSITION_DETAIL').enable();
			} else {
				// 기타위치 비활성 처리
				$$('POSITION_DETAIL').disable();
			}
		}
		
		// 상면코드 변경 이벤트
		$$('CENTER_POSITION_CD_POP_NM').attachEvent("onChange", function(newv, oldv){
			if($$('POSITION_DETAIL')) {
				// 상면코드가 [기타]일 경우
				if(newv.match(/기타위치/gi)) {
					// 기타위치 활성 처리
					$$('POSITION_DETAIL').enable();
				} else {
					// 기타위치 비활성 처리
					$$('POSITION_DETAIL').setValue('');
					$$('POSITION_DETAIL').disable();
				}
			}
		});
	}
	*/
	// 이중화 여부
	if($$('DUAL_YN')) {
		var dualYn = $$('DUAL_YN').getValue();
		
		if($$('DUAL_TYPE_CD')) {
			// 이중화 여부 [Y]일 경우
			if('Y' == dualYn) {
				// 이중화 유형 활성 처리
				$$('DUAL_TYPE_CD').enable();
			} else {
				// 이중화 유형 비활성 처리
				$$('DUAL_TYPE_CD').disable();
			}
		}
		
		if($$('DUAL_INFRA_POP_NM')) {
			// 이중화 여부 [Y]일 경우
			if('Y' == dualYn) {
				// 이중화 연결장비 활성 처리
				$$('DUAL_INFRA_POP_NM').enable();
			} else {
				// 이중화 연결장비 비활성 처리
				$$('DUAL_INFRA_POP_NM').disable();
			}
		}
		
		// 이중화 여부 변경 이벤트
		$$('DUAL_YN').attachEvent("onChange", function(newv, oldv){
			if($$('DUAL_TYPE_CD')) {
				// 이중화 여부 [Y]일 경우
				if('Y' == newv) {
					// 이중화 유형 활성 처리
					$$('DUAL_TYPE_CD').enable();
				} else {
					// 이중화 유형 비활성 처리
					$$('DUAL_TYPE_CD').setValue('');
					$$('DUAL_TYPE_CD').disable();
				}
			}
			
			if($$('DUAL_INFRA_POP_NM')) {
				// 이중화 여부 [Y]일 경우
				if('Y' == newv) {
					// 이중화 연결장비 활성 처리
					$$('DUAL_INFRA_POP_NM').enable();
				} else {
					// 이중화 연결장비 비활성 처리
					$$('DUAL_INFRA_POP_NM').setValue('');
					$$('DUAL_INFRA_POP_NM').disable();
					
					for(var key in params.formData) {
						for(var formKey in params.formData[key]) {
							if(params.formData[key][formKey]['DUAL_INFRA']) {
								$$(formKey)._setFieldValue('DUAL_INFRA', '');
							}
						}
					}
				}
			}
		});
	}
}

//자산원장 속성 정합성 체크
function checkCutomValidate(formObj) {
	// 자산상태
	if ($$('ASSET_STATE')) {
		if('OUT' == $$('ASSET_STATE').getValue()) {
			// 데이터파기방법
			/*
			if($$('DISK_FORMAT_TYPE_CD') && '' == nkia.ui.utils.check.nullValue($$('DISK_FORMAT_TYPE_CD').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[데이타파기방법]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'DISK_FORMAT_TYPE_CD');
				return false;
			}
			*/
			// 반출(삭제)일
			if($$('OUT_DT') && '' == nkia.ui.utils.check.nullValue($$('OUT_DT').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[반출(삭제)일]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'OUT_DT');
				return false;
			}
		}
	}
	
	// 소유형태
	/*
	if ($$('ASSET_OWN_TYPE_CD')) {
		if('RENT' == $$('ASSET_OWN_TYPE_CD').getValue()) {
			// 소유주체
			if($$('OWNER') && '' == nkia.ui.utils.check.nullValue($$('OWNER').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[소유주체]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'OWNER');
				return false;
			}
		}
	}
	
	// 상면코드
	if($$('CENTER_POSITION_CD_POP_NM')) {
		var centerPositionCd = $$('CENTER_POSITION_CD_POP_NM').getValue();
		
		// 상면코드가 [기타]일 경우
		if(centerPositionCd.match(/기타위치/gi)) {
			// 기타위치
			if($$('POSITION_DETAIL') && '' == nkia.ui.utils.check.nullValue($$('POSITION_DETAIL').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[기타위치]를 입력해주세요.'
				});
				
				setItemFocus(formObj, 'POSITION_DETAIL');
				return false;
			}
		}
	}
	
	// 제조사 코드
	if($$('VENDOR_CD')) {
		var vendorCd = $$('VENDOR_CD').getValue();
		
		// 제조사 코드가 [직접입력]일 경우
		if('NOTCODE' == vendorCd) {
			// 제조사명
			if($$('VENDOR_DETAIL') && '' == nkia.ui.utils.check.nullValue($$('VENDOR_DETAIL').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[제조사명]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'VENDOR_DETAIL');
				return false;
			}
		}
	}
	*/
	
	if($$('CONF_KIND_CD')) {
		var conf_kind_cd = $$('CONF_KIND_CD').getValue();
		
		if(conf_kind_cd == "LOGIC" || conf_kind_cd == "CLUSTER" || conf_kind_cd == "VM") {
			if('' == nkia.ui.utils.check.nullValue($$('UP_CONF_ID_POP_NM').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[상위구성명]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'UP_CONF_ID_POP_NM');
				return false;
			}
			if('' == nkia.ui.utils.check.nullValue($$('PHYSI_CONF_ID_POP_NM').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[물리구성명]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'PHYSI_CONF_ID_POP_NM');
				return false;
			}
		} 
	}
	
	
	
	// 가상화 HOST 여부
	if($$('VIRTUAL_YN')) {
		var virtualYn = $$('VIRTUAL_YN').getValue();
		
		// 가상화 HOST 여부가 [Y]일 경우
		if('Y' == virtualYn) {
			// 가상화 방식
			if($$('VIRTUAL_TYPE_CD') && '' == nkia.ui.utils.check.nullValue($$('VIRTUAL_TYPE_CD').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[가상화 방식]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'VIRTUAL_TYPE_CD');
				return false;
			}
		}
	}
	
	// 유지보수여부
	if($$('MAINT_CONTRACT_YN')) {
		var vendorCd = $$('MAINT_CONTRACT_YN').getValue();
		
		// 유지보수여부가 [Y]일 경우
		if('Y' == vendorCd) {
			// 유지보수업체명
			if($$('MAINT_VENDOR') && '' == nkia.ui.utils.check.nullValue($$('MAINT_VENDOR').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[유지보수업체명]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'MAINT_VENDOR');
				return false;
			}
		}
	}
	
	// 이중화 여부
	if($$('DUAL_YN')) {
		var dualYn = $$('DUAL_YN').getValue();
		
		// 이중화 여부 [Y]일 경우
		if('Y' == dualYn) {
			// 이중화 유형
			if($$('DUAL_TYPE_CD') && '' == nkia.ui.utils.check.nullValue($$('DUAL_TYPE_CD').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[이중화 유형]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'DUAL_TYPE_CD');
				return false;
			}
			
			// 이중화 연결장비
			if($$('DUAL_INFRA_POP_NM') && '' == nkia.ui.utils.check.nullValue($$('DUAL_INFRA_POP_NM').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[이중화 연결장비]를 입력해주세요.'
				});
				
				setItemFocus(formObj, 'DUAL_INFRA_POP_NM');
				return false;
			}
		}
	}
	
	// 랙U사이즈
	if($$('RACK_ORDER')) {
		var rackOrder = $$('RACK_ORDER').getValue();
		var regexp = /^\d+((\-)?\d+)?$/;
		
		if(rackOrder) {
			if( !regexp.test(rackOrder) ) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '숫자, 숫자사이 [-]포함 으로 입력해주세요.'
				});
				
				setItemFocus(formObj, 'RACK_ORDER');
				return false;
			}
		}
	}
	
	// 물리장비명
	if($$('PHYSI_CONF_ID_POP_NM')) {
		// 가상화 구분
		/*
		if ($$('CI_IF_TYPE')) {
			if('VIRTUAL' == $$('CI_IF_TYPE').getValue()) {
				if('' == nkia.ui.utils.check.nullValue($$('PHYSI_CONF_ID_POP_NM').getValue())) {
					nkia.ui.utils.notification({
						type: 'error',
						message: '[물리장비명]을 입력해주세요.'
					});
					
					setItemFocus(formObj, 'PHYSI_CONF_ID_POP_NM');
					return false;
				}
			}
		} else {
			if('' == nkia.ui.utils.check.nullValue($$('PHYSI_CONF_ID_POP_NM').getValue())) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '[물리장비명]을 입력해주세요.'
				});
				
				setItemFocus(formObj, 'PHYSI_CONF_ID_POP_NM');
				return false;
			}
		}
		*/
	}
	
	return true;
}

// 속성 포커스 셋팅
function setItemFocus(formObj, id) {
	var isSetFocus = false;
	var tabComp = $$("assetDetailViewer");
	
	for(var i=0; i<formObj.formData.length; i++) {
		for(key in formObj.formData[i]) {
			
			if(undefined != formObj.formData[i][key][id]) {
				for(var j=0; j<formObj.formItemIds.length; j++){
					
					if(key == formObj.formItemIds[j]["entity_id"]) {
						$$(formObj.formItemIds[j]["up_id"]).show();
						tabComp.showView(formObj.formItemIds[j]["comp_id"]);
						$$(id).focus();
						
						isSetFocus = true;
						break;
					}
				}
			}
			
			if(isSetFocus) break;
		}
		
		if(isSetFocus) break;
	}
}