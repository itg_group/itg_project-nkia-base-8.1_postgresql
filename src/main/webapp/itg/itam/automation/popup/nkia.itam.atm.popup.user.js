/********************************************
 * User Popup Events
 * Date: 2017-01-12
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 사용자 팝업 필드 관련 이벤트
 ********************************************/
nkia.itam.atm.popup.User = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.popup_show_type = prop.popup_show_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	this.width = 950;
	this.height = nkia.ui.utils.getWidePopupHeight();
	
	this.openPopup = function() {
		
		var _this = this;
		
		var tree = createTreeComponent({
			id: _this.popup_object_id + "_tree",
	        url: '/itg/base/searchDeptTree.do',
	        filterable: true,
	        expColable: true,
	        params: {use_yn: 'Y'},
	        expandLevel: 3,
	        width: 250,
	        header: {
	        	title: getConstText({isArgs : true, m_key : 'res.00034'})
	        },
	        on: {
	        	onItemClick: this.clickTree.bind(_this)
	        }
	    });
		
		var searchForm = createFormComp({
			id: _this.popup_object_id + "_searchForm",
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(_this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(_this) })
					],
    		        css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: [
			            { item : createUnionFieldComp({
		                            items: [
		                                createCodeComboBoxComp({
		                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
		                    				name: 'search_type',
		                    				code_grp_id: 'SEARCH_USER',
		                    				attachChoice: true,
		                    				width: 250
		                    			}),
		                    			createTextFieldComp({
		                    				name: "search_value",
		                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
		                    				on:{
		       									onKeyPress: this.searchByEnterKey.bind(_this)
		     								}	
		                    			})
		                            ]
		                        })
		              	}
			        ]
			}
		});
		
		var grid = createGridComp({
			id : _this.popup_object_id + "_grid",
			select : true,
			pageable : true,
			pageSize : 10,
			params: {use_yn: 'Y'},
			url : "/itg/system/user/searchUserList.do",
			resource : "grid.system.user.pop",
			header : {
				title : "사용자 조회"
			},
			on:{
				onItemDblClick: this.choice.bind(_this)
			}
		});
		
		var bodyScrollView = {
	        view: "scrollview",
	        body: {
				cols: [{
                    rows: tree
                },{
                    rows: new Array().concat(searchForm, grid)
                }]
			}
		};
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: _this.width,
	    	height: _this.height,
	    	header: {
	    		title: "사용자 조회"
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e){
		
		var treeItem = $$(this.popup_object_id + "_tree").getItem(id);
		
		var form = $$(this.popup_object_id + "_searchForm");
		var searchParams = form._getValues();
		
		searchParams.cust_id = treeItem.node_id;
		searchParams.use_yn = 'Y';
		
		$$(this.popup_object_id + "_grid")._reload(searchParams);
	};
	
	// 값 선택 이벤트
	this.choice = function(){
		
		var selectedItem = $$(this.popup_object_id + "_grid")._getSelectedRows();
		if(selectedItem.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		
		var values = {};
		values[this.field_id] = selectedItem[0].USER_ID;
		
		// 팝업 출력유형에 따라 세팅
		if(this.popup_show_type == "ID"){
			values[this.name_field_id] = selectedItem[0].USER_ID;
		}else if(this.popup_show_type == "IDNAME"){
			values[this.name_field_id] = selectedItem[0].USER_NM + " (" + selectedItem[0].USER_ID + ")";
		}else{
			values[this.name_field_id] = selectedItem[0].USER_NM;
		}
		
        $$(this.target_form_id)._setValues(values)
        
        this.window.close();
	};
	
	// 검색 이벤트
	this.search = function(){
		var searchParams = $$(this.popup_object_id + "_searchForm")._getValues();
		var treeParams = $$(this.popup_object_id + "_tree").getSelectedItem();
		searchParams.use_yn = 'Y';

		if(typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}
		
		$$(this.popup_object_id + "_grid")._reload(searchParams);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.popup_object_id + "_searchForm")._reset();
	};
	
};