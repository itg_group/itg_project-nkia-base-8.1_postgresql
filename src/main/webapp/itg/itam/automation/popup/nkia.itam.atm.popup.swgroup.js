/********************************************
 * SwGroup Popup Events
 * Date: 2017-01-10
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : S/W그룹 팝업 필드 관련 이벤트
 ********************************************/
nkia.itam.atm.popup.SwGroup = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	
	this.openPopup = function() {
		
		var _this = this;
		
		var tree = createTreeComponent({
			id: _this.popup_object_id + "_tree",
	        url: '/itg/itam/sam/searchSwGroupTree.do',
	        filterable: true,
	        expColable: true,
	        params: {swTrgetSe: 'SV'},
	        expandLevel: 2,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: 300,
	    	header: {
	    		title: "S/W그룹 목록"
	    	},
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type : "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	// 선택 이벤트
	this.choice = function(){
		
		var treeItem = $$(this.popup_object_id + "_tree").getSelectedItem();
		
		if(treeItem){
			var values = {};
			values[this.field_id] = treeItem.node_id;
			values[this.name_field_id] = treeItem.text;
			
            $$(this.target_form_id)._setValues(values)
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
	
};