/********************************************
 * Project Popup Events
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 사업조회 팝업 필드 관련 이벤트
 ********************************************/
nkia.itam.atm.popup.Project = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	
	this.openPopup = function() {
		
		var _this = this;
		
		var searchForm = createFormComp({
			id: _this.popup_object_id + "_searchForm",
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(_this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(_this) })
					],
    		        css: "webix_layout_form_bottom_top"
				}
			},
			fields: {
				colSize: 2,
				items: [
					{colspan: 1, item: createTextFieldComp({label: "사업번호", name: "search_project_id", on: { onKeyPress: this.searchByEnterKey.bind(_this) }})},
					{colspan: 1, item: createTextFieldComp({label: "사업명",name: "search_project_nm", on: { onKeyPress: this.searchByEnterKey.bind(_this) }})}
				]
			}
		});
		
		var grid = createGridComp({
			id : _this.popup_object_id + "_grid",
			select : true,
			pageable : true,
			pageSize : 10,
			url : "/itg/itam/project/searchProject.do",
			resource : "grid.itam.project.pop",
			pagerCss : "webix_pager_no_margin",
			header : {
				title : "사업 조회",
			},
			on:{
				onItemDblClick: this.choice.bind(_this)
			}
		});
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: 800,
	    	header: {
	    		title: "사업 조회"
	    	},
	    	body: {
	    		rows: new Array().concat(searchForm, grid)
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	// 선택 이벤트
	this.choice = function(){
		
		var selectedItem = $$(this.popup_object_id + "_grid")._getSelectedRows();
		if(selectedItem.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		
		var values = {};
		values[this.field_id] = selectedItem[0].PROJECT_ID;
		values[this.name_field_id] = selectedItem[0].PROJECT_NM;
		
        $$(this.target_form_id)._setValues(values)
        
        this.window.close();
	};
	
	// 검색 이벤트
	this.search = function(){
		var searchParams = $$(this.popup_object_id + "_searchForm")._getValues();
		$$(this.popup_object_id + "_grid")._reload(searchParams);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.popup_object_id + "_searchForm")._reset();
	};
	
};