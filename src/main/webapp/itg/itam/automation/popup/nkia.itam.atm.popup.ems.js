/********************************************
 * EMS Popup Events
 * Date: 2020-1-14
 * Version: 1.0 - 최초등록
 * Author: TaeYeon,Kim
 * Description : EMS ID 팝업 필드 관련 이벤트
 ********************************************/
nkia.itam.atm.popup.Ems = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.customer_id_nm = prop.customer_id_nm;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.popup_show_type = prop.popup_show_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	this.width = 800;
	this.height = nkia.ui.utils.getWidePopupHeight();
	
	this.openPopup = function() {
		
		var _this = this;
		
		var searchForm = createFormComp({
			id: _this.popup_object_id + "_searchForm",
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(_this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(_this) })
					],
    		        css: "webix_layout_form_bottom_top"
				}
			},
			fields: {
				colSize: 2,
				items: [
					{colspan: 1, item: createTextFieldComp({label: "IP", name: "MAIN_IP", on: { onKeyPress: this.searchByEnterKey.bind(_this) }})},
					{colspan: 1, item: createTextFieldComp({label: "EMS ID", name: "EMS_ID", on: { onKeyPress: this.searchByEnterKey.bind(_this) }})},
					{colspan: 1, item: createTextFieldComp({label: "호스트명", name: "HOST_NM", on: { onKeyPress: this.searchByEnterKey.bind(_this) }})}
				],
				hiddens: {
			    	CUSTOMER_ID_NM : _this.customer_id_nm
			    }
			}
		});
		
		var grid = createGridComp({
			id : _this.popup_object_id + "_grid",
			select : true,
			pageable : true,
			pageSize : 10,
			url : "/itg/itam/ems/searchEmsRealList.do",
			params: {CUSTOMER_ID_NM : _this.customer_id_nm},
			resource : "grid.itam.ems.list",
			pagerCss : "webix_pager_no_margin",
			header : {
				title : "센터 EMS ID 목록"
			},
			on:{
				onItemDblClick: this.choice.bind(_this)
			}
		});
		
		var bodyScrollView = {
	        view: "scrollview",
	        body: {
				cols: [{
	                rows: new Array().concat(searchForm, grid)
	            }]
			}
		};
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: _this.width,
	    	height: _this.height,
	    	header: {
	    		title: "센터 EMS ID 목록"
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	// 선택 이벤트
	this.choice = function(){
		var _this = this;
		var selectedItem = $$(this.popup_object_id + "_grid")._getSelectedRows();
		if(selectedItem.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		
		// EMS ID 등록여부 체크
		nkia.ui.utils.ajax({
			url: "/itg/itam/automation/selectEmsIdCount.do",
			params: {conf_id: _this.id, ems_id: selectedItem[0].EMS_ID},
			async: false,
			isMask: true,
			success: function(response){
				if (response.success) {
					var values = {};
					values[_this.field_id] = selectedItem[0].EMS_ID;
					
					// 팝업 출력유형에 따라 세팅
					if(this.popup_show_type == "ID"){
						values[_this.name_field_id] = selectedItem[0].EMS_ID;
						values['AGENT_ID'] = selectedItem[0].AGENT_ID;
					}else if(this.popup_show_type == "IDNAME"){
						values[_this.name_field_id] = selectedItem[0].EMS_ID + "(" + selectedItem[0].EMS_ID + ")";
						values['AGENT_ID'] = selectedItem[0].AGENT_ID;
					}else{
						values[_this.name_field_id] = selectedItem[0].EMS_ID;
						values['AGENT_ID'] = selectedItem[0].AGENT_ID;
					}
					
			        $$(_this.target_form_id)._setValues(values);
			        $$(_this.target_form_id).config.hiddens['AGENT_ID'] = selectedItem[0].AGENT_ID;
			        
			        _this.window.close();
				} else {
					nkia.ui.utils.notification({
						message: response.resultMsg
					});
				}
			}
		});
	};
	
	// 검색 이벤트
	this.search = function(){
		var searchParams = $$(this.popup_object_id + "_searchForm")._getValues();
		$$(this.popup_object_id + "_grid")._reload(searchParams);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.popup_object_id + "_searchForm")._reset();
	};
	
};