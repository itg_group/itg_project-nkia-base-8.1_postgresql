/********************************************
 * Asset Location Popup Events
 * Date: 2017-01-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 설치위치 팝업 필드 관련 이벤트
 ********************************************/
nkia.itam.atm.popup.Location = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	this.name_field_suffix = prop.name_field_suffix;
	this.width = 300;
	this.height = nkia.ui.utils.getWidePopupHeight();
	
	this.reference = prop.reference;
	
	this.openPopup = function() {
		
		var _this = this;
		
		var searchForm = createFormComp({
			id: _this.popup_object_id + "_searchForm",
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			fields: {
				colSize: 1,
				items: [
					{colspan: 1, item: createCodeComboBoxComp({label: '센터위치', name: 'locationTopCode', code_grp_id: 'CENTER', value: 'D1', attachChoice: true, on: { onChange: this.searchTreeList.bind(_this) }})}
				]
			}
		});
		
		var tree = createTreeComponent({
			id: _this.popup_object_id + "_tree",
	        url: '/itg/itam/location/searchLocation.do',
	        filterable: true,
	        expColable: true,
	        params: {use_yn: 'Y', locationTopCode: 'D1'},
	        expandLevel: 2,
	        on: {
	        	//onAfterSelect: this.afterSelect.bind(_this)
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		var locationForm = createFormComp({
			id: _this.popup_object_id + "_form",
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			fields: {
				colSize: 1,
				items: [
					{colspan: 1, item: createTextFieldComp({label: '기타위치', name: 'POSITION_DETAIL_POP' })}
				]
			}
		});
		
		var bodyScrollView = {
	        view: "scrollview",
	        body: {
				cols: [{
	                rows: new Array().concat(searchForm, tree, locationForm)
	            }]
			}
		};
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: _this.width,
	    	height: _this.height,
	    	header: {
	    		title: "위치 선택"
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type : "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	this.searchTreeList = function() {
		var _this = this;
		var searchParams = $$(_this.popup_object_id + '_searchForm')._getValues();
		searchParams['use_yn'] = 'Y';
		
		$$(_this.popup_object_id + '_tree')._reload(searchParams);
	};
	
	this.afterSelect = function(id) {
		var _this = this;
		var treeItem = $$(_this.popup_object_id + "_tree").getSelectedItem();
		var form = $$(_this.popup_object_id + "_form");
		
		if(!id.match(/NOTCODE/gi)) {
			form._setValues({POSITION_DETAIL_POP: ''});
		}
	};
	
	// 선택 이벤트
	this.choice = function(){
		var _this = this;
		// 보여지는 name필드 뒤에 붙는 suffix(자바 원장자동화에서 정의)
		var _NAME_FIELD_SUFFIX = this.name_field_suffix;
		
		var treeItem = $$(this.popup_object_id + "_tree").getSelectedItem();
		var form = $$(_this.popup_object_id + "_form");
		
		if(!treeItem['node_id'].match(/NOTCODE/gi)) {
			form._setValues({POSITION_DETAIL_POP: ''});
		}
		
		var formData = form._getValues();
		
		if(treeItem){
			var values = {};
			
			var locationDetail = this.getLocationDetail(treeItem.node_id);
			var locType = locationDetail.LOC_TYPE;
			var locLeaf = treeItem.leaf;
			
			if(locType == null) {
				nkia.ui.utils.notification({
	                type: 'error',
	                message: '최상위 계층은 선택하실 수 없습니다.'
	            });
				return false;
			}
			
			if(locLeaf == false) {
				nkia.ui.utils.notification({
	                type: 'error',
	                message: '마지막 노드만 선택할 수 있습니다.'
	            });
				return false;
			}
			
			
			if(locationDetail.LOC_CODE.match(/NOTCODE/gi)) {
				if(!formData['POSITION_DETAIL_POP']) {
					nkia.ui.utils.notification({
		                type: 'error',
		                message: '기타위치을 입력해 주세요.'
		            });
					return false;
				}
			} else {
				form._setValues({POSITION_DETAIL_POP: ''});
			}
			
			if(_this.reference && _this.reference.grid) {
				_this.reference.selectedItem['LOC_CD'] = locationDetail.LOC_CODE;
				_this.reference.selectedItem['LOC_CD_NM'] = locationDetail.LOC_NM;
				_this.reference.selectedItem['POSITION_DETAIL'] = formData['POSITION_DETAIL_POP'];
				_this.reference.grid._addRow(_this.reference.selectedItem);
			} else {
				if(locType == "AREA") {
					values[this.field_id] = locationDetail.LOC_CODE;
					values[this.name_field_id] = locationDetail.LOC_NM;
					values["FLO_DIT_CD"] = "";
					values["FLO_DIT_CD" + _NAME_FIELD_SUFFIX] = "";
					values["LOCATION_CD"] = locationDetail.LOCATION_CD;
					values["POSITION_DETAIL"] = formData['POSITION_DETAIL_POP'];
				} else if(locType == "FLOOR") {
					values[this.field_id] = locationDetail.UP_LOC_CODE;
					values[this.name_field_id] = locationDetail.UP_LOC_NM;
					values["FLO_DIT_CD"] = locationDetail.LOC_CODE;
					values["FLO_DIT_CD" + _NAME_FIELD_SUFFIX] = locationDetail.LOC_NM;
					values["LOCATION_CD"] = locationDetail.LOCATION_CD;
				}
				
	            $$(this.target_form_id)._setValues(values);
			}
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
	
	// 선택한 트리의 값의 상세정보를 구하는 함수
	this.getLocationDetail = function(locCode) {
		
		var locationInfo = {};
		
		nkia.ui.utils.ajax({
			viewId: 'assetDetailViewer',
			url: '/itg/itam/location/selectLocation.do',
			params: { loc_code : locCode },
			async: false,
			isMask: false,
			success: function(response){
				locationInfo = response.resultMap;
			}
		});
		
		return locationInfo;
	}
};

function createLocWindow(popupProps) {
	var props = popupProps;
	var _this = this;
	
	var popupHandler;
	
	popupHandler = new nkia.itam.atm.popup.Location(popupProps);
	popupHandler.openPopup();
}