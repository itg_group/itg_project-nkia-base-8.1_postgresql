/********************************************
 * Namespace
 ********************************************/
nkia.itam.atm.popup = {};

/********************************************
 * createPopupForAssetDetail
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 팝업 유형에 따라 이벤트 분류
 ********************************************/
function createPopupForAssetDetail(compProperty) {
	
	this.props = compProperty;
	var _this = this;
	
	var popupHandler;
	var popup;
	
	try {
		// SWGROUP, LOCATION, FLOOR, SWOPTION
		switch(_this.props.popup_type) {
			case 'CUST' :
				// 부서 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Dept(_this.props);
				break;
			case 'USER' :
				// 사용자 조회 팝업
				popupHandler = new nkia.itam.atm.popup.User(_this.props);
				break;
			case 'VENDOR' :
				// 업체 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Vendor(_this.props);
				break;
			case 'PROJECT' :
				// 사업 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Project(_this.props);
				break;
			case 'SUPPLY' :
				// 공급계약 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Supply(_this.props);
				break;
			case 'SWGROUP' :
				// S/W그룹 조회 팝업
				popupHandler = new nkia.itam.atm.popup.SwGroup(_this.props);
				break;
			case 'LOCATION' :
				// 설치위치 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Location(_this.props);
				break;
			case 'CONF' :
				// 구성명 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Conf(_this.props);
				break;
			case 'CONF_CL' :
				// 구성 클러스터명 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Conf(_this.props);
				break;	
			case 'CONF_PHYSI' :
				// 물리 구성 ID 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Conf(_this.props);
				break;		
			case 'ASSET' :
				// 자산명 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Asset(_this.props);
				break;	
			case 'CUSTOMER' :
				// 고객사 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Customer(_this.props);
				break;
			case 'SWOPTION' :
				// S/W옵션 - 팝업 이벤트 없음
				popupHandler = null;
				break;
			case 'CLASS' :
				// 분류체계 - 팝업 이벤트 없음
				popupHandler = new nkia.itam.atm.popup.Class(_this.props);
				break;
			case 'FLOOR' :
				// 설치층 - 팝업 이벤트 없음
				popupHandler = null;
				break;
			case 'EMS' :
				// EMS 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Ems(_this.props);
				break;
			case 'MAINT' :
				// 유지보수정보 조회 팝업
				popupHandler = new nkia.itam.atm.popup.Maint(_this.props);
				break;	
			default :
				var errMsg = "[" + _this.props.popup_type + "] " + " is not define.";
				console.log(errMsg);
				throw new Error(errMsg);
			
		}
		
		if(popupHandler != null) {
			popup = popupHandler.openPopup();
		}
		
	} catch(e) {
		alert(e.message);
	}
}

/********************************************
 * clickPopupClearBtn
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 팝업 초기화 버튼 클릭 이벤트
 ********************************************/
function clickPopupClearBtn(compProperty) {
	
	this.props = compProperty;
	var _this = this;
	
	// 객체 접근을 위한 객체ID 변수
	var _TARGET_FORM_ID = _this.props.target_form_id;		// 필드가 속한 에디터 폼의 ID
	var _HIDDEN_FIELD_ID = _this.props.field_id;			// 코드값 필드의 ID (히든 필드)
	var _NAME_FIELD_ID = _this.props.name_field_id;			// 보여지는 값 필드의 ID (텍스트 필드)
	var _CLEAR_BTN_ID = _this.props.clear_btn_id;			// 초기화 버튼의 ID
	var _NAME_FIELD_SUFFIX = _this.props.name_field_suffix;	// 보여지는 name필드 뒤에 붙는 suffix(자바 원장자동화에서 정의)
	
	// 폼 객체
	var myForm = $$(_TARGET_FORM_ID);
	
	// 필드 초기화
	var values = {};
	values[_HIDDEN_FIELD_ID] = "";
	values[_NAME_FIELD_ID] = "";
	
	// 필드 초기화 버튼 클릭시 같이 초기화 되어야 하는 필드가 있다면 하단 분기로 구현
	switch(_HIDDEN_FIELD_ID) {
	
		case 'VENDOR_ID' :
			// 제조사 필드 초기화 시 모델 필드도 초기화 대상으로 추가
			values["MODEL_ID"] = "";
			values["MODEL_ID" + _NAME_FIELD_SUFFIX] = "";
			break;
		case 'MODEL_ID' :
			// 모델 필드 초기화 시 제조사 필드도 초기화 대상으로 추가
			values["VENDOR_ID"] = "";
			values["VENDOR_ID" + _NAME_FIELD_SUFFIX] = "";
			break;
		case 'SW_GROUP_ID' :
			// S/W그룹 필드 초기화 시 S/W옵션 필드도 초기화 대상으로 추가
			values["SW_GROUP_OPTION_ID"] = "";
			values["SW_GROUP_OPTION_ID" + _NAME_FIELD_SUFFIX] = "";
			break;
		case 'LCN_DIT_CD' :
			// 설치위치 필드 초기화 시 설치층 필드도 초기화 대상으로 추가
			values["FLO_DIT_CD"] = "";
			values["FLO_DIT_CD" + _NAME_FIELD_SUFFIX] = "";
			break;
		case 'CENTER_POSITION_CD' :
			// 상면코드 필드 초기화 시 센터위치 필드도 초기화 대상으로 추가
			values["LOCATION_CD"] = "";
			break;
		case 'EMS_ID' :
			// 센터 EMS ID 필드 초기화 시 AGENT ID 필드도 초기화 대상으로 추가
			values["AGENT_ID"] = "";
			break;
	}
	
	myForm._setValues(values);
}

/********************************************
 * clickPopupSaveBtn
 * Date: 2020-1-14
 * Version: 1.0 - 최초등록
 * Author: TaeYeon,Kim
 * Description : 팝업 저장 버튼 클릭 이벤트
 ********************************************/
function clickPopupSaveBtn(compProperty) {
	
	this.props = compProperty;
	var _this = this;
	
	// 객체 접근을 위한 객체ID 변수
	var _TARGET_FORM_ID = _this.props.target_form_id;		// 필드가 속한 에디터 폼의 ID
	var _HIDDEN_FIELD_ID = _this.props.field_id;			// 코드값 필드의 ID (히든 필드)
	var _NAME_FIELD_ID = _this.props.name_field_id;			// 보여지는 값 필드의 ID (텍스트 필드)
	var _CLEAR_BTN_ID = _this.props.clear_btn_id;			// 초기화 버튼의 ID
	var _NAME_FIELD_SUFFIX = _this.props.name_field_suffix;	// 보여지는 name필드 뒤에 붙는 suffix(자바 원장자동화에서 정의)
	
	// 폼 객체
	var paramMap = {};
	
	// 필드 초기화 버튼 클릭시 같이 초기화 되어야 하는 필드가 있다면 하단 분기로 구현
	switch(_HIDDEN_FIELD_ID) {
	
		case 'EMS_ID' :
			_this.props['ems_id'] = $$(_TARGET_FORM_ID)._getValues()['EMS_ID'];
			_this.props['agent_id'] = $$(_TARGET_FORM_ID)._getValues()['AGENT_ID'];
			
			// EMS ID, AGENT ID 저장
			nkia.ui.utils.ajax({
				viewId: "scrollview",
				url: "/itg/itam/automation/updateEmsInfo.do",
				params: _this.props,
				async: false,
				isMask: true,
				success: function(response){
					nkia.ui.utils.notification({
						message: response.resultMsg
					});
				}
			});
			
			break;		
	}
	
}
