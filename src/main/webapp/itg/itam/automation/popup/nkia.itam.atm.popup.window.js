// 자산원장 윈도우 팝업 
var windowPopupOpen = function(props){
	var popupType = props.popup_type;
	
	switch(popupType) {
		case 'ASSET' :		 
			/** 자산 원장 팝업 */
			// 객체 접근을 위한 객체ID 변수
			var _TARGET_FORM_ID = props.target_form_id;		// 필드가 속한 에디터 폼의 ID
			var _HIDDEN_FIELD_ID = props.field_id;			// 코드값 필드의 ID (히든 필드)
			
			var myForm = $$(_TARGET_FORM_ID);
			var fieldValue = myForm._getFieldValue(_HIDDEN_FIELD_ID);
			
			if(typeof fieldValue != 'undefined' && fieldValue != "") {
				nkia.ui.utils.ajax({
					url: "/itg/itam/automation/selectAssetInfo.do",
					params: {confId : fieldValue},
					isMask: true,
					success: function(response){
						var resultMap = response.resultMap;
						var viewType = 'pop_view';
						var assetId = resultMap.ASSET_ID;
						var confId = resultMap.CONF_ID;
						var classId = resultMap.CLASS_ID;
						var entityClassId = classId;
						if(resultMap.TANGIBLE_ASSET_YN == 'N'){
							entityClassId = classId + '-0';
						}
						var params = "?asset_id="+assetId+"&conf_id="+confId+"&class_id="+classId+"&view_type="+viewType+"&entity_class_id="+entityClassId;
						openAssetDetailPop(params);
					}
				});
			}
			
			break;
		case 'CLUSTER' :		 
			/** 클러스터 상세 팝업 */
			// 객체 접근을 위한 객체ID 변수
			var _TARGET_FORM_ID = props.target_form_id;		// 필드가 속한 에디터 폼의 ID
			var _HIDDEN_FIELD_ID = props.field_id;			// 코드값 필드의 ID (히든 필드)
			
			var myForm = $$(_TARGET_FORM_ID);
			var fieldValue = myForm._getFieldValue(_HIDDEN_FIELD_ID);
			
			if(typeof fieldValue != 'undefined' && fieldValue != "") {
				var url = "/itg/itam/cluster/clusterDetailPop.do?cl_id=" + encodeURI(fieldValue);
				window.open(getConstValue('CONTEXT') +url, "clusterPop", "width=1200,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
			}
			
			break;
	}
}