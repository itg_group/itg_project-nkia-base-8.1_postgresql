/********************************************
 * 분류체계 조회 팝업
 * Date: 2019-10-11
 * Version: 1.0 - 최초등록
 * Author: 김태연
 * Description : 분류체계 팝업 필드 관련 이벤트
 ********************************************/
nkia.itam.atm.popup.Class = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.class_id = prop.class_id;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	this.name_field_suffix = prop.name_field_suffix;
	this.width = 300;
	this.height = nkia.ui.utils.getWidePopupHeight();
	
	this.openPopup = function() {
		
		var _this = this;
		
		var tree = createTreeComponent({
			id: _this.popup_object_id + "_tree",
	        url: '/itg/itam/opms/selectConfClassTree.do',
	        filterable: true,
	        expColable: true,
	        params: {use_yn: 'Y', treeType: 'UP_CLASS', class_id: _this.class_id},
	        expandLevel: 2,
	        header: {
				title: "분류체계"
			},
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		var bodyScrollView = {
	        view: "scrollview",
	        body: {
				cols: [{
	                rows: tree
	            }]
			}
		};
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: _this.width,
	    	height: _this.height,
	    	header: {
	    		title: "분류체계 선택"
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type : "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	// 선택 이벤트
	this.choice = function(){
		
		// 보여지는 name필드 뒤에 붙는 suffix(자바 원장자동화에서 정의)
		var _NAME_FIELD_SUFFIX = this.name_field_suffix;
		
		var treeItem = $$(this.popup_object_id + "_tree").getSelectedItem();
		
		if(treeItem){
			var values = {};
			
			if(Boolean(treeItem.leaf)) {
				values[this.field_id] = treeItem.node_id;
				values[this.name_field_id] = treeItem.text;
			} else {
				alert("최하위 계층을 선택해 주세요.");
				return false;
			}
			
            $$(this.target_form_id)._setValues(values);
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};