/**
 * 구성자원 팝업
 */
nkia.itam.atm.popup.Conf = function(prop) {
	
	//var _TANGIBLE_ASSET_YN = "Y";	//hmsong 사용하지 않는 속성
	//var _CENTER_ASSET_YN = "Y";	//hmsong 사용하지 않는 속성		
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	this.conf_type = prop.conf_type;
	this.mng_type = prop.mng_type;
	this.width = nkia.ui.utils.getWidePopupWidth();
	this.height = nkia.ui.utils.getWidePopupHeight();
	
	var __CONF_TYPE = "ROOT";	
	var __OPER_STATE_CD = "";
	var __MNG_TYPE = this.mng_type;
	var __SEARCH_AUTOMATION_COMBOLIST_IGNORE = ["search_type"];	//검색조건 속성이 createCodeComboBoxComp 인 경우 별도의 xml 처리가 없이도 조회가 가능함. 자동 조회 기능을 적용하지 않을 속성ID 기재.
	var __SEARCH_TYPE_CODE_GRP_ID = "SEARCH_CONF_" + __CONF_TYPE + "_STATISTICS";	//조회구분 공통코드
	var __GRIDPARAMS = {mng_type : __MNG_TYPE, conf_type : __CONF_TYPE}; //mng_type: 조회 대상이 자산(ASSET)인지 구성(CONF)인지
	var __RESOURCE = "grid." + __MNG_TYPE.toLowerCase() + ".statistics.AM" + __CONF_TYPE;	//목록의 그리드 프로퍼티
	var treeParams = {};
	treeParams = {mng_type : __MNG_TYPE};
	
	// CMDB개선 : 필요한 파라미터 정의.
	// 선택한 필드가 물리구성번호일 때는 자산정보를 가진 것만 조회해야 함
	if(this.field_id == "PHYSI_CONF_ID") {
		__MNG_TYPE = "ASSETCONF";
		__GRIDPARAMS.conf_kind_cd = 'PHYSI';
		__GRIDPARAMS.mng_type = __MNG_TYPE;
		__SEARCH_TYPE_CODE_GRP_ID = "SEARCH_ASSETCONF_" + __CONF_TYPE + "_STATISTICS";
		__RESOURCE = "grid.assetconf.statistics.AM" + __CONF_TYPE;
	}		
	//클러스터
	if(this.field_id == "CLUSTER_ID") {
		__CONF_TYPE = "CFCL";	
		__GRIDPARAMS.conf_kind_cd = 'CLUSTER';
		__SEARCH_TYPE_CODE_GRP_ID = "SEARCH_CONF_" + __CONF_TYPE + "_STATISTICS";
		__RESOURCE = "grid.conf.statistics.AM" + __CONF_TYPE;
		treeParams = {mng_type : __MNG_TYPE, select_conf_type : __CONF_TYPE};
	}
	//랙 구성
	if(this.field_id == "RACK_CONF_ID") {
		__CONF_TYPE = "CFRK";	
		__GRIDPARAMS.conf_type = __CONF_TYPE;
		__SEARCH_TYPE_CODE_GRP_ID = "SEARCH_CONF_" + __CONF_TYPE + "_STATISTICS";
		__RESOURCE = "grid.conf.statistics.AM" + __CONF_TYPE;
		treeParams = {mng_type : __MNG_TYPE, select_conf_type : __CONF_TYPE};
	}
	//백업구성
	if(this.field_id == "BACKUP_INFO_ID") {
		__CONF_TYPE = "CFBK";	
		__GRIDPARAMS.conf_type = __CONF_TYPE;
		__SEARCH_TYPE_CODE_GRP_ID = "SEARCH_CONF_" + __CONF_TYPE + "_STATISTICS";
		__RESOURCE = "grid.conf.statistics.AM" + __CONF_TYPE;
		treeParams = {mng_type : __MNG_TYPE, select_conf_type : __CONF_TYPE};
	}
	
	this.openPopup = function() {
		
		var _this = this;
		
		
		
		var tree = createTreeComponent({
			id: _this.popup_object_id + "_tree",
	        url: '/itg/itam/statistics/searchClassTree.do',
	        filterable: true,
	        expColable: true,
	        params: treeParams,
			expandLevel: 3, 
	        header: {
	        	title: "분류체계"
	        },
	        width: 200,
	        on: {
	        	onItemClick: this.clickTree.bind(_this)
	        }
	    });
		
		var searchForm = createFormComp({
			id: _this.popup_object_id + "_searchForm",
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(_this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(_this) })
					],
    		        css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 3,
				items: [
			            { colspan: 3, item : createUnionFieldComp({
		                            items: [
		                                createCodeComboBoxComp({
		                    				label: '조회구분',
		                    				name: 'search_type',
											id: 'search_type',
		                    				code_grp_id: __SEARCH_TYPE_CODE_GRP_ID,
		                    				width: 200,
		                    				attachAll: true,		                    			
		                    			}),
		                    			createTextFieldComp({
		                    				name: "search_value",
		                    				on:{
		       									onKeyPress: this.searchByEnterKey.bind(_this)
		     								}
		                    			})
		                            ]
		                        })
		              	},		     
			            { colspan: 1, item : createCodeComboBoxComp({
				            	label: "구성종류", 
				            	name: "conf_kind_cd", 
				            	code_grp_id: "CONF_KIND_CD",
				            	readonly: (__GRIDPARAMS.conf_kind_cd != null && __GRIDPARAMS.conf_kind_cd != "" ? true : false),
				            	attachChoice: true,
				            	value : (__GRIDPARAMS.conf_kind_cd != null && __GRIDPARAMS.conf_kind_cd != "" ? __GRIDPARAMS.conf_kind_cd : "")
	        				})
			            },
			            { colspan: 1, item : createCodeComboBoxComp({
	            				label: (__MNG_TYPE == "ASSET" ? "자산상태" : "구성상태"), 
	            				name: (__MNG_TYPE == "ASSET" ? "asset_state_cd" : "oper_state_cd"), 
	            				code_grp_id: (__MNG_TYPE == "ASSET" ? "ASSET_STATE_CD" : "OPER_STATE_CD"), 
	            				readOnly: false,
								attachChoice: true
            				})
			            }
			        ]
			        		
			}
		});
		
		/**
		 *  분류체계별 그리드 분기 처리
		 */
		var grid = createGridComp({
			id : _this.popup_object_id + "_grid",
			resizeColumn : true,
			pageable : true,
			pageSize : 50,
			url : "/itg/itam/statistics/searchAssetList.do",
			resource : __RESOURCE,
			params : __GRIDPARAMS,
			header : {
				title : (__MNG_TYPE == "ASSET" ? "자산목록" : "구성목록"), 
			},
			on:{
				onItemDblClick: this.choice.bind(_this)
			}
		});
		
		var bodyScrollView = {
	        view: "scrollview",
	        body: {
				cols: [{
                    rows: tree
                },{
	                rows: new Array().concat(searchForm, grid)
	            }]
			}
		};
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: _this.width,
	    	height: _this.height,
	    	header: {
	    		title: "구성 목록 조회"
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	//분류체계, 조회폼의 값을 합쳐서 리턴
	this.returnMapScriptMade = function(id) {
		var treeItem;
		
		if(id != null && id != "") {
			treeItem = $$(this.popup_object_id + "_tree").getItem(id);
		} else {
			treeItem = $$(this.popup_object_id + "_tree").getSelectedItem();
		}
		
		var form = $$(this.popup_object_id + "_searchForm");
		var searchParams = form._getValues();
		
		if(typeof treeItem != "undefined") {
			__CONF_TYPE = treeItem.node_id; 
		} 
		
		searchParams["mng_type"] = __MNG_TYPE;
		searchParams["conf_type"] = __CONF_TYPE;
		
		// 선택한 필드가 물리구성번호일 때는 자산정보 가지고 있는 것만 조회
		if(__GRIDPARAMS["conf_kind_cd"] != null && __GRIDPARAMS["conf_kind_cd"] != "") {
			searchParams["conf_kind_cd"] = __GRIDPARAMS.conf_kind_cd;
		}
		
		var returnMap = {};
	
		for (var key in searchParams) {
			var data  = searchParams[key];
			
			if(data != null && data != ""){
				returnMap[key] = data;		
			}		
		}
		
		
		// 20200316 임현대 : 복수선택 검색을 위한 처리 isMultiSearch
		for ( ele in $$(this.popup_object_id + "_searchForm").elements){
			var tempElement = $$(this.popup_object_id + "_searchForm").elements[ele];
			if ( tempElement.config.view == "multiselect" && returnMap[ele] != null ){
				if ( returnMap[ele] != "" ){
					returnMap[ele+"_list"] = returnMap[ele].split(","); 
					returnMap[ele] = "'" + returnMap[ele].replace(/,/g,"','")+ "'";
					returnMap[ele] = returnMap[ele].replace("(NONE)","");
				}
				else {
					returnMap[ele+"_list"] = ""; 
					returnMap[ele] = "";
				}
			}
			// 콤보박스 검색쿼리 자동 생성을 위한 처리(JAVA 레벨에서 쿼리 생성)
			else if ( tempElement.config.view == "select" && returnMap[ele] != null && __SEARCH_AUTOMATION_COMBOLIST_IGNORE != null && Array.isArray(__SEARCH_AUTOMATION_COMBOLIST_IGNORE) == true ){
				returnMap["search_automation_combofield"] = "";
				returnMap["search_automation_combofield_value"] = "";
			}
		} // for ( ele in $$(this.popup_object_id + "_searchForm").elements){
	
		// 20200402 임현대 : 콤보 전체 조회를 위해 코드목록 포함
		var comblist = ["search_type"];
		for ( idx in comblist ){
			if ( $$(comblist[idx]) != null && $$(comblist[idx]).data != null && $$(comblist[idx]).data.options != null ){
				var combvalue = []; 
				var optTemp = $$(comblist[idx]).data.options;
				for(i in optTemp){ 
					if ( optTemp[i] != null && optTemp[i].id != null && optTemp[i].id != "") {
						combvalue[combvalue.length]=optTemp[i].id;
					}
				}
				if ( combvalue.length > 0 ) { 
					returnMap[comblist[idx] + "_combolist"] = combvalue; //////////////////////////////////////
				}
			} 
		}
		
		return returnMap;
	}
	
	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e){
		
		var returnMap = this.returnMapScriptMade(id);
		
		$$(this.popup_object_id + "_grid")._reload(returnMap);
	};
	
	// 값 선택 이벤트
	this.choice = function(){
		
		var selectedItem = $$(this.popup_object_id + "_grid")._getSelectedRows();
		if(selectedItem.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
			
		var values = {};
		values[this.field_id] = selectedItem[0].CONF_ID;
		values[this.name_field_id] = selectedItem[0].CONF_NM;
	 	$$(prop.target_form_id)._setValues(values);
	    this.window.close();	
	     
	    
	};
	
	// 검색 이벤트
	this.search = function(){
		var returnMap = this.returnMapScriptMade();
		
		$$(this.popup_object_id + "_grid")._reload(returnMap);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.popup_object_id + "_searchForm")._reset();
	};
	
};