/********************************************
 * Update History Tab Component
 * Date: 2021-05-12
 * Version: 1.0 - 최초등록
 * Author: hmsong
 * Description : 자산정보의 수정이력만 조회하는 수정이력 컴포넌트
 ********************************************/
nkia.itam.atm.comp.AssetUpdateHistory = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.tangible_asset_yn = prop.tangible_asset_yn;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.item_array = prop.itemArray;
	this.entity_id = prop.entity_id;
	this.entity_nm = prop.entity_nm;
	this.ins_user_id_nm = prop.ins_user_id_nm;
	this.ins_dt = prop.ins_dt;
	this.entity_mng_type = "ASSET";
	
	// 검색용 날짜객체 (최근 30일)
	this.start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	this.end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	/********************************************
	 * Function : getObject
	 * 수정이력 페이지 요소들을 하나의 객체화 하여 리턴
	 ********************************************/
	this.getObject = function() {
		
		var _this = this;
		
		// 페이지 생성 객체들을 담아줄 변수
		var pageObjects;
		
		var insInfoForm = createFormComp({
			id: "insInfoForm"  + "_" + this.entity_mng_type,
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "등록정보"
			},
			fields: {
				colSize: 2,
				items: [
				    {colspan: 1, item: createTextFieldComp({label: "등록자", name: "ins_user_id_nm", value: _this.ins_user_id_nm, readonly: true })},
				    {colspan: 1, item: createTextFieldComp({label: "등록일", name: "ins_dt", value: _this.ins_dt, readonly: true })}
				]
			}
		});
		
		var modSearchForm = createFormComp({
			id: "modSearchForm" + "_" + this.entity_mng_type,
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: this.searchHistoryList.bind(_this) }),
						createBtnComp({ label:"초기화", click: this.clearSearchForm.bind(_this) })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
				    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : _this.start_date_obj, end_dateValue: _this.end_date_obj})},
					{colspan: 1, item: createUnionFieldComp({
	                    items: [
	                            createCodeComboBoxComp({
	                				label: '수정자',
	                				name: 'modSearchKey',
	                				code_grp_id: 'MOD_SEARCH_KEY',
	                				attachChoice: true,
	                				width: 250
	                			}),
	                			createTextFieldComp({
	                				label: "　",
	                				name: "modSearchValue",
	                				on: { 
	                					onKeyPress: this.searchByEnterKey.bind(_this) 
	                				}
	                			})
	                        ]
	                    })
					}
				]
			}
		});
		
		// 개별 속성 이력 그리드 생성
		var attrHisGrid = createGridComp({
	        id : "attrHisGrid" + "_" + _this.entity_mng_type,
	        header: {
	            title: "자산 속성 수정이력"
	        },
	        resizeColumn:true,
	        select: false,
	        params:  {
	        	ASSET_ID : _this.asset_id,
	        	CONF_ID : _this.conf_id,
	        	updDt_startDate : _this.start_date_obj.day,
	        	updDt_endDate : _this.end_date_obj.day,
				entity_mng_type : _this.entity_mng_type
			},
	        url: "/itg/itam/assetHistory/selectOpmsHisAttrList.do",
	        resource: "grid.itam.hisAttrList",
	        on : {
	        	onItemClick : this.attrHisGridClick
	        }
	    });
		
		pageObjects = new Array().concat(insInfoForm, modSearchForm, attrHisGrid);
		
		return pageObjects;
	};
	
	
	
	/********************************************
	 * Function : searchHistoryList
	 * 수정이력 검색 이벤트
	 ********************************************/
	this.searchHistoryList = function() {
		
		var searchParams = $$("modSearchForm" + "_" + this.entity_mng_type)._getValues();
		searchParams['ASSET_ID'] = this.asset_id;
		searchParams['CONF_ID'] = this.conf_id;
		searchParams['entity_mng_type'] = this.entity_mng_type;
		$$("attrHisGrid" + "_" + this.entity_mng_type)._reload(searchParams);
	}
	
	
	
	/********************************************
	 * Function : searchByEnterKey
	 * 엔터키 입력시 검색 이벤트
	 ********************************************/
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.searchHistoryList();
			}	
		}
	};
	
	
	
	/********************************************
	 * Function : clearSearchForm
	 * 검색폼 초기화 이벤트
	 ********************************************/
	this.clearSearchForm = function() {
		$$("modSearchForm" + "_" + this.entity_mng_type)._reset();
	}
	
	
	
	/********************************************
	 * Function : attrHisGridClick
	 * 속성 수정이력 그리드 cell 클릭 이벤트
	 ********************************************/
	this.attrHisGridClick = function(rowId, event, node) {
		var searchParam = $$("modSearchForm" + "_" + this.config.params.entity_mng_type)._getValues();
		var clickRecord = $$("attrHisGrid" + "_" + this.config.params.entity_mng_type).getItem(rowId);

		for (var prop in clickRecord) {
			searchParam[prop] = clickRecord[prop];
		}
		
		searchParam["entity_mng_type"] = this.config.params.entity_mng_type;
		searchParam["ASSET_ID"] = this.config.params.ASSET_ID;
		searchParam["CONF_ID"] = this.config.params.CONF_ID;
		var grid = createGridComp({
			id : "compAttrTotModList",
			resizeColumn : true,
			select : false,
			params : searchParam,
			url : "/itg/itam/assetHistory/selectOpmsHisAttrDetailList.do",
			resource : "grid.itam.attrModHisList",
			header : {
				title : "[" + searchParam.COL_NAME + "] 수정이력"
			}
		});
		
		var bodyScrollView = {
	        view: "scrollview",
	        body: {
				cols: [{
                    rows: grid
                }]
			}
		};
		
		nkia.ui.utils.window({
			id:"compAttrModListPop",
			callFunc: function() {
				this.window = createWindow({
			    	id: "compAttrModListPop",
			    	width: 800,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "[" + searchParam.COL_NAME + "] 수정이력"
			    	},
			    	body: {
			    		rows: [bodyScrollView]
			    	},
			    	closable: true
			    })
			}
		});
	};
	
};