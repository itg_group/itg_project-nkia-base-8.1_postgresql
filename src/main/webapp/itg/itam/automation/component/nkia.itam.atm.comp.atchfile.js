/********************************************
 * File Component
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 첨부파일 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.AtchFile = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.user_id = prop.conn_user_id;
	this.atch_file_id = prop.atch_file_id;
	
	var readonly = false;
	if(this.view_type === "tab_view" || this.view_type === "pop_view") {
		readonly = true;
	}
	
	this.getObject = function() {
		
		var _this = this;
		
		var file = createAtchFileComp({
			id: _this.id,
			readonly: false,
			register: _this.user_id,
			height: 150
		});
		
		return file;
	};
	
};