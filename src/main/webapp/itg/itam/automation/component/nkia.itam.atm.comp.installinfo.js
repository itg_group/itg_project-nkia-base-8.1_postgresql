/********************************************
 * Install Infomation Component
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 설치 장비 정보 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.InstallInfoList = function(prop) {
	
	// 탭 그리드의 경우 반드시 Prefix를 붙일 것
	var _TAB_BAR_SUFFIX = "_tabbar";
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.item_array = prop.itemArray;
	this.column_info = prop.columnInfo;
	this.add_btn_suffix = "_ADD_BTN";
	this.del_btn_suffix = "_DEL_BTN";
	this.his_btn_suffix = "_HIS_BTN";
	this.ems_id = prop.ems_id;
	this.dca_id = prop.dca_id;
	
	this.getObject = function() {
		
		var _this = this;
		
		var url = '/itg/itam/opms/selectOpmsInstallInfoList.do';
		
		// 해당 컴포넌트 그리드 생성
		var tabBarOptionsArray = [];
		var tabGridArray = [];
		
		for (i = 0; i < _this.item_array.length; i++) {

			var _item_code_id = _this.item_array[i].CODE_ID;
			var _item_code_nm = _this.item_array[i].CODE_NM;
			var _column_resource = _this.column_info[_item_code_id];
			var _badge_id = _item_code_id+"_badge";
			
			var resourceHeader = _column_resource["gridHeader"].split(",");
			
			// 첫번 째 헤더가 데이터의 key 이므로
			var key_column = new Array();
			key_column.push(resourceHeader[0]);
			
			var gridCheckbox = true;
			var gridHeader = {
				title: _this.title + " - " + _item_code_nm
			};
			
			// DCA 연계 장비 일 경우 사용자가 데이터를 수정하지 못하도록 그리드의 xtype을 모두 na로 변경한다.
			// 그리고 그리드 checkbox를 사용하지 않는다.
			if(_this.dca_id) {
				var newXtype = new Array();
				for(var j in resourceHeader) {
					newXtype.push("na");
				}
				_column_resource["gridXtype"] = newXtype.toString();
				gridCheckbox = false;
				gridHeader["buttons"] = {
					items: [
						createBtnComp({id:_item_code_id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
					]
				};
			} else {
				// DCA 연계 장비가 아닌 경우, 사용자가 수동으로 추가/제거 가능하도록 버튼을 제공한다.
				gridHeader["buttons"] = {
					items: [
						createBtnComp({id:_item_code_id+_this.add_btn_suffix, label:"추가", click: this.addRow.bind(_this), type: "form" }),
						createBtnComp({id:_item_code_id+_this.del_btn_suffix, label:"제거", click: this.removeRow.bind(_this) }),
						createBtnComp({id:_item_code_id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
					]
				};
			}
			
			var gridProps = {
				id : _item_code_id,
		        header: gridHeader,
		        resizeColumn:true,
		        params:  {
					conf_id : _this.conf_id,
					asset_id : _this.asset_id,
					ins_type : _item_code_id,
					view_type : _this.view_type
				},
				columns: _column_resource,
		        url: url,
		        height: 200,
		        checkbox: gridCheckbox,
		        keys: key_column,
		        badgeId: _badge_id
			};
			
			// 네트워크 인터페이스의 경우 그리드 내의 콤보박스 정보 추가
			if(_item_code_id=="NIF") {
				gridProps["editprops"] =  [{
					column: "MANAGED",
					attachChoice: true,
					params: {code_grp_id : "YN"}
				}];
			}
			
			var grid = createGridComp(gridProps);
			
			tabBarOptionsArray.push({id:_item_code_id+_TAB_BAR_SUFFIX, value:_item_code_nm + "<span id='"+_badge_id+"' class='webix_badge'>0</span>"});
			tabGridArray.push({id:_item_code_id+_TAB_BAR_SUFFIX, rows: grid});
		}
		
		var elementsConfig = {
			labelPosition: "left",
			labelWidth: 150,
			labelAlign: "right"
		}
		
		var tabGrid = {
			view:"tabview",
			id:_this.id,
			animate:false,
			tabbar:{options:tabBarOptionsArray},
			cells:new Array().concat(tabGridArray)	
		};
		
		return tabGrid;
	};
	
	// 그리드 row 추가
	this.addRow = function(btnId, e) {
		var gridId = btnId.replace(this.add_btn_suffix, "");
		$$(gridId)._addRow();
	}
	
	// 선택 그리드 row 삭제
	this.removeRow = function(btnId, e) {
		var gridId = btnId.replace(this.del_btn_suffix, "");
		$$(gridId)._removeRow();
	};
	
	// 이력 팝업
	this.openHisPop = function(btnId, e) {
		
		var _this = this;
		
		// 검색용 날짜객체 (최근 30일)
		var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
		var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		var gridId = btnId.replace(this.his_btn_suffix, "");
		
		var url = "";
		var resource = "";
		
		// 이력 조회용 컬럼 추가
		var _column_resource = JSON.parse(JSON.stringify(_this.column_info[gridId]));
		
		// 수정불가 Xtype으로 만들어준다.
		var resourceHeader = _column_resource["gridHeader"].split(",");
		var newXtype = new Array();
		for(var j in resourceHeader) {
			newXtype.push("na");
		}
		_column_resource["gridXtype"] = newXtype.toString();
		
		_column_resource["gridHeader"] = "INS_DT,INS_USER_NM," + _column_resource["gridHeader"];
		_column_resource["gridText"] = "수정일,수정자," + _column_resource["gridText"];
		_column_resource["gridWidth"] = "200,200," + _column_resource["gridWidth"];
		_column_resource["gridXtype"] = "na,na," + _column_resource["gridXtype"];
		_column_resource["gridAlign"] = "left,left," + _column_resource["gridAlign"];
		_column_resource["gridFlex"] = "na,na," + _column_resource["gridFlex"];
		_column_resource["gridSortable"] = "na,na," + _column_resource["gridSortable"];
		
		if("NIF" == gridId) {
			// 설치 NIF
			url = "/itg/itam/assetHistory/selectOpmsHisInsNifDetailList.do";
		} else if("DISK" == gridId) {
			// 설치 디스크
			url = "/itg/itam/assetHistory/selectOpmsHisInsDiskDetailList.do";
		}
		
		var historyPopSearchForm = createFormComp({
			id: "historyPopSearchForm",
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
						createBtnComp({ label:"초기화", click: initHistoryPop })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
				    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
				]
			}
		});
		
		var historyPopGrid = createGridComp({
			id : "historyPopGrid",
			resizeColumn : true,
			select : false,
			params:  {
	        	ASSET_ID : _this.asset_id,
	        	CONF_ID : _this.conf_id,
	        	updDt_startDate : start_date_obj.day,
	        	updDt_endDate : end_date_obj.day
			},
			url : url,
			columns: _column_resource,
			header : {
				title : "수정이력"
			},
			cellMerges: ["INS_DT", "INS_USER_NM"]
		});
		
		nkia.ui.utils.window({
			id:"historyPopup",
			callFunc: function() {
				this.window = createWindow({
			    	id: "historyPopup",
			    	width: 900,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "수정이력"
			    	},
			    	body: {
			    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
			    	},
			    	closable: true
			    })
			}
		});
		
		function searchHistoryPop() {
			var searchParam = $$("historyPopSearchForm")._getValues();
			searchParam["ASSET_ID"] = _this.asset_id;
			searchParam["CONF_ID"] = _this.conf_id;
			$$("historyPopGrid")._reload(searchParam);
		}
		
		function initHistoryPop() {
			$$("historyPopSearchForm")._reset();
		}
		
	};
	
};