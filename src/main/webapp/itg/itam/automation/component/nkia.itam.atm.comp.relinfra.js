/********************************************
 * Relation Infra Component
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 연관 장비 정보 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.RelInfraList = function(prop) {
	
	// 탭 그리드의 경우 반드시 Prefix를 붙일 것
	var _TAB_BAR_SUFFIX = "_tabbar";
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	//this.customer_id = prop.customer_id;	//hmsong cmdb 개선 작업 : 용도 불분명 
	//this.business_type_cd = prop.business_type_cd;	//hmsong cmdb 개선 작업 : 용도 불분명
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.item_array = prop.itemArray;
	this.add_btn_suffix = "_ADD_BTN";
	this.del_btn_suffix = "_DEL_BTN";
	this.his_btn_suffix = "_HIS_BTN";
	this.entity_mng_type = prop.entity_mng_type;
	
	var __SEARCH_AUTOMATION_COMBOLIST_IGNORE = ["search_type"]; //검색조건 속성이 createCodeComboBoxComp 인 경우 별도의 xml 처리가 없이도 조회가 가능함. 자동 조회 기능을 적용하지 않을 속성ID 기재.
	
	// 구성의 key 컬럼 : 구성ID -> 
	this.key_column = ["CONF_ID"];
//	this.key_column = ["ASSET_ID", "CONF_ID"]; //hmsong cmdb 개선 작업 : 용도 불분명
//	this.key_column_rs = ["SERVICE_ID"]; //hmsong cmdb 개선 작업 : 용도 불분명
	
	this.getObject = function() {
		
		var _this = this;
		
		// 해당 컴포넌트 그리드 생성
		var tabBarOptionsArray = [];
		var tabGridArray = [];
		
		for (i = 0; i < _this.item_array.length; i++) {
			var _item_root_type = _this.item_array[i].CODE_ID;
			var _item_class_nm = _this.item_array[i].CODE_NM;
			var _badge_id = _item_root_type+"_badge";
			var _resource = "";
			
			if(this.entity_mng_type == "ASSET") {
				_resource = "grid.asset.statistics.AM" + _item_root_type;
			} else {
				_resource = "grid.conf.statistics.AM" + _item_root_type;
			}
			
			var url = '';
			var _addEvent = this.openPopup.bind(_this);
			url = '/itg/itam/opms/selectOpmsRelConfList.do';
			
			//일괄저장 또는 일괄등록 화면에서 저장 시
			if (_this.view_type == 'infmodifiy') {
				url = '/itg/itam/opms/selectOpmsInfoRelConfList.do';
			}
			
			var header = {
	            title: _this.title + " - " + _item_class_nm
	        };
			
			if (_this.auth_type == "U") {
				header['buttons'] = {
					items: [
						createBtnComp({id:_item_root_type+_this.add_btn_suffix, label:"추가", click: _addEvent, type: "form" }),
	                    createBtnComp({id:_item_root_type+_this.del_btn_suffix, label:"제거", click: this.removeRow.bind(_this) }),
	                    createBtnComp({id:_item_root_type+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
					]
				};
			} else {
				header['buttons'] = {
					items: [
						createBtnComp({id:_item_root_type+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
					]
				};
			}
			
			var grid = createGridComp({
				id : _item_root_type,
		        header: header,
		        params:  {
		        	conf_id : _this.conf_id,
					asset_id : _this.asset_id,
					conf_type : _item_root_type
				},
		        url: url,
		        resizeColumn : true,
		        height: 200,
		        resource : _resource,
		        checkbox: true,
//		        resource: "grid.itam.relConf" + _item_root_type,
//		        keys: ('RS' == _item_root_type ? _this.key_column_rs : _this.key_column),
		        keys : _this.key_column,
		        badgeId: _badge_id,
		        on:{
					onItemDblClick: function(id, e) {
						if('RS' != $$(_this.id).getTabbar().getValue().replace(_TAB_BAR_SUFFIX, '')) {
							var clickRecord = this.getItem(id);
							_this.assetInfoPopup(clickRecord);
						}
					}
				}
			});
			
			tabBarOptionsArray.push({id:_item_root_type+_TAB_BAR_SUFFIX, value:_item_class_nm + "<span id='"+_badge_id+"' class='webix_badge'>0</span>"});
			tabGridArray.push({id:_item_root_type+_TAB_BAR_SUFFIX, rows: grid});
			
		}
		
		var elementsConfig = {
			labelPosition: "left",
			labelWidth: 150,
			labelAlign: "right"
		};
		
		var tabGrid = {
			view:"tabview",
			id:_this.id,
			animate:false,
			tabbar: {
				options: tabBarOptionsArray
			},
			cells: new Array().concat(tabGridArray)	
		};
		
		return tabGrid;
		
	};
	
	// 선택 그리드 row 삭제
	this.removeRow = function(btnId, ev) {
		var gridId = btnId.replace(this.del_btn_suffix, "");
		$$(gridId)._removeRow();
	};
	
	/**************************************
	 * 원장 상세조회
	 *************************************/
	this.assetInfoPopup = function(clickRecord) {
		var asset_id 		= clickRecord.ASSET_ID;
		var conf_id 		= clickRecord.CONF_ID;
		var class_id		= clickRecord.CLASS_ID;
		var class_type		= clickRecord.CLASS_TYPE;
		var conf_type		= clickRecord.CONF_TYPE;
		var conf_nm			= clickRecord.CONF_NM;
		var mng_type		=  "";
	 	var entity_class_id = "";
		var entity_conf_type_id = "";	
		
		if((class_id != null && class_id != "") && (conf_type == null || conf_type == "") ){
			mng_type = "ASSET";
		}
		if((class_id == null || class_id == "") && (conf_type != null && conf_type != "") ){
			mng_type = "CONF";
		}
		if((class_id != null && class_id != "") && (conf_type != null && conf_type != "") ){
			if("$!{mng_type}" == "ASSETCONF"){
				mng_type = "ASSETCONF";
				entity_class_id = class_id;
				entity_conf_type_id = conf_type;
			} else {
				mng_type = "CONF";
			}
			
		}
		var entity_root_id = (mng_type == "ASSET" ? class_id : conf_type);
		var view_type		= "tab_view";
		var tab_title = "";
		if(mng_type == "ASSET"){
			tab_title = "["+asset_id+"] "+ asset_nm;
		} else {
			tab_title = "["+conf_id+"] "+ conf_nm;
		}

		var url = '/itg/itam/automation/goAssetDetailViewer.do' 
			+ "?asset_id=" + asset_id
			+ "&conf_id=" + conf_id
			+ "&class_id=" + class_id
			+ "&class_type=" + class_type
			+ "&view_type=" + view_type
			+"&entity_root_id="+entity_root_id+"&asset_state_cd="+asset_state_cd+"&mng_type="+mng_type+"&conf_type="+conf_type
			+"&entity_class_id="+entity_class_id+"&entity_conf_type_id="+entity_conf_type_id
		;
		
		window.open(
			getConstValue('CONTEXT') + url,
			"assetDetailPop"+conf_id,
			"width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no"
		);
	}
	
	/**************************************
	 * 팝업 영역
	 *************************************/
	// 데이터 선택팝업
	this.openPopup = function(btnId, ev) {
		
		// 연관장비 컴포넌트는 Grid ID = Class Type
		var parentGridId = btnId.replace(this.add_btn_suffix, "");
		var parentRows = $$(parentGridId)._getRows();
		
		// 각 요소 ID 세팅
		this.pop_window_id = parentGridId + "_pop_window";
		this.pop_tree_id = parentGridId + "_pop_tree";
		this.pop_search_form_id = parentGridId + "_pop_searchForm";
		this.pop_main_grid_id = parentGridId + "_pop_main_grid";
		this.pop_sub_grid_id = parentGridId + "_pop_sub_grid";
		this.class_type = parentGridId;
		this.conf_type = parentGridId;
		this.tree_root_type = parentGridId;
		
		this.mng_type = "";
		var resource = "";
		var search_type = "";
		
		if(this.entity_mng_type == "ASSET") {
			this.mng_type = this.entity_mng_type;
			var resource = "grid.asset.statistics.AM" + this.tree_root_type;
			var search_type = 'SEARCH_ASSET_' + parentGridId + '_STATISTICS';
		} else {
			this.mng_type = "ASSETCONF";
			var resource = "grid.assetconf.statistics.AM" + this.tree_root_type;
			var search_type = 'SEARCH_ASSETCONF_' + parentGridId + '_STATISTICS';
		}
				
		var tree = createTreeComponent({
			id: this.pop_tree_id,
	        url: '/itg/itam/opms/selectRelConfClassTree.do',
	        filterable: true,
	        expColable: true,
	        params: {mng_type : this.mng_type, tree_root_type: this.tree_root_type},
	        expandLevel: 3,
	        width: 250,
	        header: {
	        	title: "분류체계"
	        },
	        on: {
	        	onItemClick: this.clickTree.bind(this)
	        }
	    });
		
		var fieldItems = [];
		fieldItems.push({colspan: 2, item: createUnionFieldComp({items: [ createCodeComboBoxComp({label: '조회구분',id: 'search_type',name: 'search_type',code_grp_id: search_type,width: 200,attachAll: true,required: false}),createTextFieldComp({name: "search_value",placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),on: {onKeyPress:this.searchByEnterKey.bind(this)}})]})});
		fieldItems.push({colspan: 1, item: createCodeComboBoxComp({label: (this.mng_type == "ASSET" ? "자산상태" : "구성상태"), name: (this.mng_type == "ASSET" ? "asset_state_cd" : "oper_state_cd"), attachChoice: true, code_grp_id: (this.mng_type == "ASSET" ? "ASSET_STATE_CD" : "OPER_STATE_CD")})});
		if(this.mng_type != "ASSET") {
			fieldItems.push({colspan: 1, item : createCodeComboBoxComp({label: "구성종류", name: "conf_kind_cd", code_grp_id: "CONF_KIND_CD",readonly: false,attachChoice: true})});
		} else {
			fieldItems.push({colspan: 1, item: createDummy({name : 'dummy_1'})});
		}
		
		var searchForm = createFormComp({
			id: this.pop_search_form_id,
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(this) })
					],
    		        css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: fieldItems
			}
		});
		
		var params = {};
		if(this.mng_type == "ASSET") {
			params["class_type"] = parentGridId;
		} else {
			params["conf_type"] = parentGridId;
		}
		
		params["class_mng_type"] = "AM";
		params["mng_type"] = this.mng_type;
		params["exp_conf_id_list"] = "'"+this.conf_id+"'";
		
		var mainGrid = createGridComp({
			id : this.pop_main_grid_id,
			pageable : true,
			pageSize : 10,
			checkbox: true,
			keys: this.key_column,
			autoLoad: true,
			url : "/itg/itam/statistics/searchAssetList.do",
			resource : resource,
			params : params,
//			resource : "grid.itam.conf" + parentGridId,
//			params : { class_type: this.class_type, exp_conf_id_list: "'"+this.conf_id+"'", tangible_asset_yn: 'Y', asset_state_cd: 'ACTIVE' },
			header : {
				title : this.title
			},
			dependency : {
				targetGridId: this.pop_sub_grid_id,
				data: parentRows
			}
		});
		
		var subGrid = createGridComp({
			id : this.pop_sub_grid_id,
			checkbox: false,
			select: false,
			resource : "grid.itam.conf.sel",
			autoLoad : false,
			keys: this.key_column,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns: [{
				column: "DEL_ACTION",
				template: nkia.ui.html.icon("remove", "remove_row"),
				onClick: {
					"remove_row": this.removeSelGrid.bind(this)
				}
			}]
		});
		
		var popupFooter = createButtons({
			align: "center",
			items: [
				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(this)})
			]
		});
		
		var body = {
    		rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    header: "분류체계",
                    body: {
                    	rows: tree
                    }
                },{
                    rows: new Array().concat(searchForm, mainGrid)
                },{
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_right", "add_row"),
			 		width: 30,
			 		onClick: {
			 			"add_row": this.addSelGrid.bind(this)
			 		}
                },{
                	width: 300,
                    rows: new Array().concat(subGrid)
                }]
    		}]
    	};
		
		this.window = createWindow({
	    	id: this.pop_window_id,
	    	width: nkia.ui.utils.getWidePopupWidth(),
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
		
		$$(this.pop_sub_grid_id)._removeAllRow();
		$$(this.pop_sub_grid_id)._addRow(this.getParentGridStoreData(parentGridId));
		
		$$(this.pop_window_id).show();
		
	};
	
	// 서비스 데이터 선택팝업
	this.openRSPopup = function(btnId, ev) {
		for(var i=0;i<formItemIds.length;i++){
			var id	= formItemIds[i]["comp_id"];
			var type 		= formItemIds[i]["type"];
			
			if(type == "F" || type == "CF"){
				if($$(id)._getValues()['CUSTOMER_ID']) {
					this.customer_id = $$(id)._getValues()['CUSTOMER_ID'];
				}
			}
		}
		
		this.customer_id = this.customer_id ? this.customer_id : ' ';
		
		var parentGridId = btnId.replace(this.add_btn_suffix, "");
		var parentRows = $$(parentGridId)._getRows();
		
		// 각 요소 ID 세팅
		this.pop_window_id = parentGridId + "_pop_window";
		this.pop_tree_id = parentGridId + "_pop_tree";
		this.pop_sub_grid_id = parentGridId + "_pop_sub_grid";
		this.class_type = parentGridId;
		
		var tree = createTreeComponent({
			id : this.pop_tree_id,
			url : '/itg/itam/amdb/searchAmdbServiceAllTreeMap.do',
			width: 300,
			expColable: true, // 전체펼침/접기 표시 여부
			filterable: true, // 검색필드 표시 여부
			checkbox: true, // 체크박스 트리 사용
			select: false,
			params: {
				search_cust_id: this.customer_id
			},
			expandLevel: 2,
			header: {
				title: "서비스 정보"
			}
		});
		
		var subGrid = createGridComp({
			id : this.pop_sub_grid_id,
			checkbox: false,
			select: false,
			resource : "grid.itam.relSelService",
			autoLoad : false,
			keys: this.key_column_rs,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns: [{
				column: "DEL_ACTION",
				template: nkia.ui.html.icon("remove", "remove_row"),
				onClick: {
					"remove_row": this.removeSelGrid.bind(this)
				}
			}]
		});
		
		var popupFooter = createButtons({
			align: "center",
			items: [
				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(this)})
			]
		});
		
		var body = {
    		rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    rows: tree
                },{
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_right", "add_row"),
			 		width: 30,
			 		onClick: {
			 			"add_row": this.addRSSelGrid.bind(this)
			 		}
                },{
                	width: 400,
                    rows: new Array().concat(subGrid)
                }]
    		}]
    	};
		
		this.window = createWindow({
	    	id: this.pop_window_id,
	    	width: nkia.ui.utils.getWidePopupWidth(),
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(this)})
	    			]
	    		}
	    	},
	    	closable: true
	    });
		
		$$(this.pop_sub_grid_id)._removeAllRow();
		$$(this.pop_sub_grid_id)._addRow(this.getParentGridStoreData(parentGridId));
		
		$$(this.pop_window_id).show();
		
	};
	
	// 선택되어 있는 부모 그리드 목록 getter
	this.getParentGridStoreData = function(parentGridId) {
		return $$(this.tree_root_type)._getRows();
	};
	
	// 선택되어 있는 서브 그리드 목록 getter
	this.getSelGridStoreData = function() {
		return $$(this.pop_sub_grid_id)._getRows();
	};
	
	// 연관 서비스 서브 그리드 row 추가
	this.addRSSelGrid = function() {
		// 체크된 데이터를 추출 시에는 _getCheckedItems() 함수를 사용합니다.
		var data = $$(this.pop_tree_id)._getCheckedItems();
		
		// 선택된 데이터가 없으면 메시지를 띄워줍니다.
		if(!data || data.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			
			return false;
		}
		
		var addData = new Array();
		
		for(var i=0; i<data.length; i++) {
			if(Boolean(data[i].leaf)) {
				data[i].otherData['CUST_ID'] = this.customer_id;
				addData.push(data[i].otherData);
			}
		}

		$$(this.pop_sub_grid_id)._addRow(addData);
	};
	
	// 서브 그리드 row 추가
	this.addSelGrid = function() {
		var data = $$(this.pop_main_grid_id)._getSelectedRows();
		if(data.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(this.pop_sub_grid_id)._addRow(data);
		
		this.reloadGrid(true);
	};
	
	// 서브 그리드 row 제거
	this.removeSelGrid = function(e, row, html) {
		var data = $$(this.pop_sub_grid_id)._getSelectedRows();
		if(data!=null && data.length > 0) {
			if(data.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '선택된 정보가 없습니다.'
				});
				return false;
			}
			$$(this.pop_sub_grid_id)._removeRow();
		} else {
			$$(this.pop_sub_grid_id)._removeRow(row);
		}
		
		this.reloadGrid(true);
	};
	
	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e){
		var returnMap = this.returnMapScriptMade(id);
		
		$$(this.pop_main_grid_id)._reload(returnMap);
	};
	
	// 값 선택 이벤트
	this.choice = function(){
		var rows = $$(this.pop_sub_grid_id)._getRows();
		$$(this.tree_root_type)._removeAllRow();
        $$(this.tree_root_type)._addRow(rows);
        
        this.window.close();
	};
	
	//분류체계, 조회폼의 값을 합쳐서 리턴
	this.returnMapScriptMade = function(id) {
		var searchParams = $$(this.pop_search_form_id)._getValues();
		
		var treeParams;
		
		if(id != null && id != "") {
			treeParams = $$(this.pop_tree_id).getItem(id);
		} else {
			treeParams = $$(this.pop_tree_id).getSelectedItem();
		}
		
		searchParams["mng_type"] = this.mng_type;
		
		if(typeof treeParams != "undefined") {
			if(this.mng_type == "ASSET") {
				searchParams["class_id"] = treeParams.node_id;
				searchParams["class_type"] = treeParams.class_type;
			} else {
				searchParams["conf_type"] = treeParams.node_id;
			}
		} else {
			if(this.mng_type == "ASSET") {
				searchParams["class_type"] = this.class_type;
			} else {
				searchParams["conf_type"] = this.conf_type;
			}
		}
		
		var returnMap = {};
		
		for (var key in searchParams) {
			var data  = searchParams[key];
			
			if(data != null && data != ""){
				returnMap[key] = data;		
			}		
		}		
		
		// 20200316 임현대 : 복수선택 검색을 위한 처리 isMultiSearch
		for ( ele in $$(this.pop_search_form_id).elements){
			var tempElement = $$(this.pop_search_form_id).elements[ele];
			if ( tempElement.config.view == "multiselect" && returnMap[ele] != null ){
				if ( returnMap[ele] != "" ){
					returnMap[ele+"_list"] = returnMap[ele].split(","); 
					returnMap[ele] = "'" + returnMap[ele].replace(/,/g,"','")+ "'";
					returnMap[ele] = returnMap[ele].replace("(NONE)","");
				}
				else {
					returnMap[ele+"_list"] = ""; 
					returnMap[ele] = "";
				}
			}
			// 콤보박스 검색쿼리 자동 생성을 위한 처리(JAVA 레벨에서 쿼리 생성)
			else if ( tempElement.config.view == "select" && returnMap[ele] != null && __SEARCH_AUTOMATION_COMBOLIST_IGNORE != null && Array.isArray(__SEARCH_AUTOMATION_COMBOLIST_IGNORE) == true ){
				returnMap["search_automation_combofield"] = "";
				returnMap["search_automation_combofield_value"] = "";
			}
		} // for ( ele in $$(this.popup_object_id + "_searchForm").elements){
	
		// 20200402 임현대 : 콤보 전체 조회를 위해 코드목록 포함
		var comblist = ["search_type"];
		for ( idx in comblist ){
			if ( $$(comblist[idx]) != null && $$(comblist[idx]).data != null && $$(comblist[idx]).data.options != null ){
				var combvalue = []; 
				var optTemp = $$(comblist[idx]).data.options;
				for(i in optTemp){ 
					if ( optTemp[i] != null && optTemp[i].id != null && optTemp[i].id != "") {
						combvalue[combvalue.length]=optTemp[i].id;
					}
				}
				if ( combvalue.length > 0 ) { 
					returnMap[comblist[idx] + "_combolist"] = combvalue; //////////////////////////////////////
				}
			} 
		}
		
		returnMap.exp_conf_id_list = "'"+this.conf_id+"'";
		
		return returnMap;
	}
	
	// 검색 이벤트
	this.search = function(){
		var returnMap = this.returnMapScriptMade();
		
		$$(this.pop_main_grid_id)._reload(returnMap);
	};
	
	// 그리드 갱신 이벤트
	this.reloadGrid = function(isGoPage){
		if(!$$(this.pop_main_grid_id)) return false;
		
		var returnMap = this.returnMapScriptMade();
		
		$$(this.pop_main_grid_id)._reload(returnMap, isGoPage);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.pop_search_form_id)._reset();
	};
	
	// 이력 팝업
	this.openHisPop = function(btnId, e) {
		
		var _this = this;
		
		// 검색용 날짜객체 (최근 30일)
		var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
		var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		var rootType = btnId.replace(this.his_btn_suffix, "");
		
		var historyPopSearchForm = createFormComp({
			id: "historyPopSearchForm",
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
						createBtnComp({ label:"초기화", click: initHistoryPop })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
				    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
				]
			}
		});
		
		var _url = '/itg/itam/assetHistory/selectOpmsHisRelInfraList.do';
		var _resource = 'grid.itam.conf.his';
		
		// 연관 서비스
//		if('RS' == classType) {
//			_url = '/itg/itam/assetHistory/selectOpmsHisRelServiceList.do';
//			_resource = 'grid.itam.service.his';
//		}
		
		var historyPopGrid = createGridComp({
			id : "historyPopGrid",
			resizeColumn : true,
			select : false,
			params:  {
	        	ASSET_ID : _this.asset_id,
	        	CONF_ID : _this.conf_id,
	        	ROOT_TYPE : rootType,
	        	updDt_startDate : start_date_obj.day,
	        	updDt_endDate : end_date_obj.day
			},
			url : _url,
			resource: _resource,
			header : {
				title : "수정이력"
			},
			cellMerges: ["INS_DT", "INS_USER_NM"]
		});
		
		nkia.ui.utils.window({
			id:"historyPopup",
			callFunc: function() {
				this.window = createWindow({
			    	id: "historyPopup",
			    	width: 900,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "수정이력"
			    	},
			    	body: {
			    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
			    	},
			    	closable: true
			    })
			}
		});
		
		function searchHistoryPop() {
			var searchParam = $$("historyPopSearchForm")._getValues();
			searchParam["ASSET_ID"] = _this.asset_id;
			searchParam["CONF_ID"] = _this.conf_id;
			searchParam["ROOT_TYPE"] = rootType;
			$$("historyPopGrid")._reload(searchParam);
		}
		
		function initHistoryPop() {
			$$("historyPopSearchForm")._reset();
		}
		
	};
};