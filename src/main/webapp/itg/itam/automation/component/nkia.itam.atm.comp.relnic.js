/********************************************
 * Relation NIC Component
 * Date: 2018-10-12
 * Version: 1.0 - 최초등록
 * Author: JangYoung Park
 * Description : NIC정보 컴포넌트
 ********************************************/
nkia.itam.atm.comp.RelNicList = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.add_btn_suffix = "_ADD_BTN";
	this.del_btn_suffix = "_DEL_BTN";
	this.his_btn_suffix = "_HIS_BTN";

	
	this.getObject = function() {
		
		var _this = this;
		
		var url = '/itg/itam/opms/selectOpmsRelNicList.do';
		
		var gridHeader = {
			title: _this.title
		};
		
		if (_this.auth_type == "U") {
			gridHeader["buttons"] = {
				items: [
					createBtnComp({id:_this.id+_this.add_btn_suffix, label:"추가", click: this.addRow.bind(_this), type: "form" }),
                    createBtnComp({id:_this.id+_this.del_btn_suffix, label:"제거", click: this.removeRow.bind(_this) }),
                    createBtnComp({id:_this.id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
				]
			};
		} else {
			gridHeader["buttons"] = {
				items: [
					createBtnComp({id:_this.id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
				]
			};
		}
		
		var grid = createGridComp({
			id : _this.id,
	        header: gridHeader,
	        height: 200,
	        resizeColumn:true,
	        checkbox: true,
	        params:  {
	        	conf_id : _this.conf_id
			},
	        url: url,
	        resource: "grid.itam.comp.relnic",
			requiredColumns: ["MAIN_YN"],
			editprops : [{
			        column: "MAIN_YN",
			        params: {code_grp_id : "YN"},
			        attachChoice: true
			
			},{
		        column: "DUAL_YN",
		        params: {code_grp_id : "YN"},
		        attachChoice: true
			},{
		        column: "NIC_USE",
		        params: {code_grp_id : "ASSET_USE"},
		        attachChoice: true
			}],
	        actionColumns : [{
		        column: "CHOICE", // 액션 컬럼의 헤더명 (그리드 리소스)
		        template: nkia.ui.html.icon("search", "CHOICE"), 
		        onClick: { // 이벤트 연결(실행 할 이벤트명을 template 과 매칭)
		            "CHOICE": function(prop, row) {
		            	var gridItem = this.getItem(row);
		            	nkia.ui.utils.window({
	            			id: "confPopup",
	            			reference: gridItem,
	            			gridParams : { class_type : 'ST'},
	            			treeParams : { class_type : 'ST'},
	            			url : '/itg/itam/statistics/searchAssetList.do',
	            			checkbox : false,
	            			select : true,
	            			resource: "grid.itam.statistics.AMROOT",
	            			choiceFunc: _this.setConfPopupChoiceData.bind(_this),
	            			callFunc: createAssetWindow
	            		});
		            }
		        }
		    }]
		});
		
		return grid;
		
	};
	
	
	// 선택 그리드 row 삭제
	this.addRow = function(btnId, ev) {
		$$(this.id)._addRow();
	};

    // 팝업에서 선택한 자원 세팅
    this.setConfPopupChoiceData = function(row,data){
    	
        var _this = this;
        var selRow = data;
        var gridrow = row;
        gridrow.ST_CONF_ID = selRow.CONF_ID;
        gridrow.ST_CONF_ID_NM = selRow.CONF_NM;
        $$(_this.id).refresh(gridrow.id);
    }	

	// 선택 그리드 row 삭제
	this.removeRow = function(btnId, ev) {
		var gridId = btnId.replace(this.del_btn_suffix, "");
		$$(gridId)._removeRow();
	};
	
	// 이력 팝업
	this.openHisPop = function(btnId, e) {
		var _this = this;
		
		// 검색용 날짜객체 (최근 30일)
		var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
		var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		var classType = btnId.replace(this.his_btn_suffix, "");
		
		var historyPopSearchForm = createFormComp({
			id: "historyPopSearchForm",
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
						createBtnComp({ label:"초기화", click: initHistoryPop })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
				    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
				]
			}
		});
		
		var historyPopGrid = createGridComp({
			id : "historyPopGrid",
			resizeColumn : true,
			select : false,
			params:  {
	        	CONF_ID : _this.conf_id,
	        	updDt_startDate : start_date_obj.day,
	        	updDt_endDate : end_date_obj.day
			},
			url : "/itg/itam/assetHistory/selectOpmsHisRelNicList.do",
			resource: "grid.itam.relnic.his",
			header : {
				title : "수정이력"
			},
			cellMerges: ["INS_DT", "INS_USER_NM"]
		});
		
		nkia.ui.utils.window({
			id:"historyPopup",
			callFunc: function() {
				this.window = createWindow({
			    	id: "historyPopup",
			    	width: 900,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "수정이력"
			    	},
			    	body: {
			    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
			    	},
			    	closable: true
			    })
			}
		});
		
		function searchHistoryPop() {
			var searchParam = $$("historyPopSearchForm")._getValues();
			searchParam["ASSET_ID"] = _this.asset_id;
			searchParam["CONF_ID"] = _this.conf_id;
			searchParam["CLASS_TYPE"] = classType;
			$$("historyPopGrid")._reload(searchParam);
		}
		
		function initHistoryPop() {
			$$("historyPopSearchForm")._reset();
		}
	};
	
	
	
};