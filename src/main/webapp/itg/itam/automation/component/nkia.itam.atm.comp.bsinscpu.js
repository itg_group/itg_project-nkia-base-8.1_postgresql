/********************************************
 * Billing Install CPU Core Component
 * Date: 2017-04-25
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 과금 CPU Core 증설량 목록 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.BsInsCpu = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	
	this.getObject = function() {
		
		var _this = this;
		
		var grid = createGridComp({
	        id : _this.id,
	        height: 200,
	        resizeColumn: true,
	        autoLoad: true,
	        header: {
				title: _this.title
			},
			resource: "grid.itam.automation.billing.ins.core",
			url: "/itg/itam/opms/searchBsInsCpuView.do",
	        params:  {
	        	asset_id : _this.asset_id,
	        	conf_id : _this.conf_id
			}
	    });
		
		return grid;
	};
};