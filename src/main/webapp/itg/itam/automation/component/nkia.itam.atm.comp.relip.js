/********************************************
 * Relation IP Component
 * Date: 2019-10-23
 * Version: 1.0 - 최초등록
 * Author: 김태연
 * Description : IP정보 컴포넌트
 ********************************************/
nkia.itam.atm.comp.RelIPList = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.ems_id = prop.ems_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.add_btn_suffix = "_ADD_BTN";
	this.del_btn_suffix = "_DEL_BTN";
	this.his_btn_suffix = "_HIS_BTN";
	this.ems_btn_suffix = "_EMS_BTN";
	
	this.getObject = function() {
		
		var _this = this;
		
		var url = '/itg/itam/opms/selectOpmsRelIPList.do';
		
		var gridHeader = {
			title: _this.title
		};
		
		var btnArr = [];
		
		if (_this.auth_type == "U") {
			if(_this.ems_id) {
				btnArr.push(createBtnComp({id:_this.id+_this.ems_btn_suffix, label:"EMS수집 IP정보", click: this.openEmsPop.bind(_this), type: "form" }));
			} else {
				btnArr.push(createBtnComp({id:_this.id+_this.add_btn_suffix, label:"추가", click: this.addRow.bind(_this), type: "form" }));
			}
			
			btnArr.push(createBtnComp({id:_this.id+_this.del_btn_suffix, label:"제거", click: this.removeRow.bind(_this) }));
		}
		
		btnArr.push(createBtnComp({id:_this.id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) }));
		
		gridHeader["buttons"] = {
			items: btnArr
		};
		
		var grid = createGridComp({
			id : _this.id,
			keys: ["IP"],
	        header: gridHeader,
	        height: 200,
	        resizeColumn:true,
	        checkbox: true,
	        params:  {
	        	conf_id : _this.conf_id
			},
	        url: url,
	        resource: "grid.itam.comp.relip",
	        editprops : [{
				column: "IP_TYPE_CD",
				attachChoice: true,
				params: {code_grp_id : "IP_TYPE"}
			}],
			requiredColumns: ["IP_TYPE_CD", "IP", "SUBNET_MASK"]
		});
		
		return grid;
		
	};
	
	// 선택 그리드 row 삭제
	this.addRow = function(btnId, ev) {
		$$(this.id)._addRow();
	};
	
	
	// 선택 그리드 row 삭제
	this.removeRow = function(btnId, ev) {
		var gridId = btnId.replace(this.del_btn_suffix, "");
		$$(gridId)._removeRow();
	};
	
	// 이력 팝업
	this.openHisPop = function(btnId, e) {
		var _this = this;
		
		// 검색용 날짜객체 (최근 30일)
		var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
		var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		var classType = btnId.replace(this.his_btn_suffix, "");
		
		var historyPopSearchForm = createFormComp({
			id: "historyPopSearchForm",
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
						createBtnComp({ label:"초기화", click: initHistoryPop })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
				    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
				]
			}
		});
		
		var historyPopGrid = createGridComp({
			id : "historyPopGrid",
			resizeColumn : true,
			select : false,
			params:  {
	        	CONF_ID : _this.conf_id,
	        	updDt_startDate : start_date_obj.day,
	        	updDt_endDate : end_date_obj.day
			},
			url : "/itg/itam/assetHistory/selectOpmsHisRelIPList.do",
			resource: "grid.itam.relip.his",
			header : {
				title : "수정이력"
			},
			cellMerges: ["INS_DT", "INS_USER_NM"]
		});
		
		nkia.ui.utils.window({
			id:"historyPopup",
			callFunc: function() {
				this.window = createWindow({
			    	id: "historyPopup",
			    	width: 900,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "수정이력"
			    	},
			    	body: {
			    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
			    	},
			    	closable: true
			    })
			}
		});
		
		function searchHistoryPop() {
			var searchParam = $$("historyPopSearchForm")._getValues();
			searchParam["ASSET_ID"] = _this.asset_id;
			searchParam["CONF_ID"] = _this.conf_id;
			searchParam["CLASS_TYPE"] = classType;
			$$("historyPopGrid")._reload(searchParam);
		}
		
		function initHistoryPop() {
			$$("historyPopSearchForm")._reset();
		}
	};
	
	// EMS수집 팝업
	this.openEmsPop = function(btnId, e) {
		var _this = this;
		
		var emsPopGrid = createGridComp({
			id : 'emsPopGrid',
			resizeColumn : true,
			select : false,
			checkbox: true,
			params:  {
	        	EMS_ID : _this.ems_id
			},
			url : "/itg/itam/opms/selectEmsIPList.do",
			resource: "grid.itam.relip.ems",
			header : {
				title : "EMS수집 IP정보"
			}
		});
		
		// 값 선택 이벤트
		var choice = function(){
			var rows = $$('emsPopGrid')._getSelectedRows();
			
			// 선택된 데이터가 없으면 메시지를 띄워줍니다.
			if(!rows || rows.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '선택된 정보가 없습니다.'
				});
				
				return false;
			}
			
			$$(_this.id)._addRow(rows);
	        
	        $$('emsPopup').close();
		};
		
		nkia.ui.utils.window({
			id:"emsPopup",
			callFunc: function() {
				this.window = createWindow({
			    	id: "emsPopup",
			    	width: 900,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "EMS수집 IP정보"
			    	},
			    	body: {
			    		rows: new Array().concat(emsPopGrid)
			    	},
			    	footer: {
			    		buttons: {
			    			align: "center",
			    			items: [
			    				createBtnComp({label: '적 용', type: "form", click: function() {choice();}})
			    			]
			    		}
			    	},
			    	closable: true
			    })
			}
		});
	};
};