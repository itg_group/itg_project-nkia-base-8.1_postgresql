/********************************************
 * Relation Service Component
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 연관 서비스 목록 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.RelServiceList = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.add_btn_suffix = "_ADD_BTN";
	this.del_btn_suffix = "_DEL_BTN";
	this.his_btn_suffix = "_HIS_BTN";
	
	// 서비스의 key 컬럼 : 서비스 ID
	this.key_column = ["SERVICE_ID"];
	
	this.pop_window_id = "";
	this.pop_search_form_id = "";
	this.pop_main_grid_id = "";
	this.pop_sub_grid_id = "";
	
	this.getObject = function() {
		
		var _this = this;
		
		var url = '/itg/itam/opms/selectOpmsRelServiceList.do';
		if(_this.view_type == 'infmodifiy'){
			url = '/itg/itam/opms/selectOpmsInfoServiceList.do';
		}
		
		var gridHeader = {
			title: _this.title
		};
		
		if (_this.auth_type == "U") {
			gridHeader["buttons"] = {
				items: [
					createBtnComp({id:_this.id+_this.add_btn_suffix, label:"추가", click: this.openPopup.bind(_this), type: "form" }),
					createBtnComp({id:_this.id+_this.del_btn_suffix, label:"제거", click: this.removeRow.bind(_this) }),
					createBtnComp({id:_this.id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
				]
			};
		} else {
			gridHeader["buttons"] = {
				items: [
					createBtnComp({id:_this.id+_this.his_btn_suffix, label:"제거", click: this.openHisPop.bind(_this) })
				]
			};
		}
		
		var grid = createGridComp({
	        id : _this.id,
	        header: gridHeader,
	        resizeColumn:true,
	        params:  {
	        	asset_id : _this.asset_id,
	        	conf_id : _this.conf_id
			},
	        url: url,
	        height: 200,
	        resource: "grid.itam.relService",
	        checkbox: true,
	        keys: _this.key_column
	    });
		
		return grid;
	};
	
	// 선택 그리드 row 삭제
	this.removeRow = function(btnId, ev) {
		var gridId = btnId.replace(this.del_btn_suffix, "");
		$$(gridId)._removeRow();
	};
	
	/**************************************
	 * 팝업 영역
	 *************************************/
	// 데이터 선택팝업
	this.openPopup = function(btnId, ev) {
		
		var parentGridId = btnId.replace(this.add_btn_suffix, "");
		var parentRows = $$(parentGridId)._getRows();
		
		// 각 요소 ID 세팅
		this.pop_window_id = parentGridId + "_pop_window";
		this.pop_search_form_id = parentGridId + "_pop_searchForm";
		this.pop_main_grid_id = parentGridId + "_pop_main_grid";
		this.pop_sub_grid_id = parentGridId + "_pop_sub_grid";
		
		var searchForm = createFormComp({
			id: this.pop_search_form_id,
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(this) })
					],
    		        css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 1,
				items: [{ 
					colspan: 1,
					item : createUnionFieldComp({
						items: [
							createCodeComboBoxComp({
								label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
								name: 'searchKey',
								code_grp_id: 'SER_POP_SEARCH_TYPE',
								attachChoice: true,
								width: 250
							}),
							createTextFieldComp({
								name: "searchValue",
								placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
								on:{
									onKeyPress: this.searchByEnterKey.bind(this)
								}
							})
						]
					})
				},{
					colspan: 1,
					item : createCodeComboBoxComp({
						label: getConstText({isArgs : true, m_key : 'res.label.itam.opms.00014'}),
						name: 'service_effect',
						code_grp_id: 'SERVICE_EFFECT',
						attachChoice: true
					})
				}]
			}
		});
		
		var mainGrid = createGridComp({
			id : this.pop_main_grid_id,
			pageable : true,
			pageSize : 10,
			checkbox: true,
			keys: this.key_column,
			url : "/itg/itam/opms/selectOpmsServiceList.do",
			resource : "grid.itam.relService",
			header : {
				title : this.title
			},
			dependency : {
				targetGridId: this.pop_sub_grid_id,
				data: parentRows
			}
		});
		
		var subGrid = createGridComp({
			id : this.pop_sub_grid_id,
			checkbox: false,
			select: false,
			resource : "grid.itam.relSelService",
			autoLoad : false,
			keys: this.key_column,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns: [{
				column: "DEL_ACTION",
				template: nkia.ui.html.icon("remove", "remove_row"),
				onClick: {
					"remove_row": this.removeSelGrid.bind(this)
				}
			}]
		});
		
		var popupFooter = createButtons({
			align: "center",
			items: [
				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(this)})
			]
		});
		
		var body = {
    		rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    rows: new Array().concat(searchForm, mainGrid)
                },{
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_right", "add_row"),
			 		width: 30,
			 		onClick: {
			 			"add_row": this.addSelGrid.bind(this)
			 		}
                },{
                	width: 400,
                    rows: new Array().concat(subGrid, popupFooter)
                }]
    		}]
    	};
		
		this.window = createWindow({
	    	id: this.pop_window_id,
	    	width: nkia.ui.utils.getWidePopupWidth(),
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	closable: true
	    });
		
		$$(this.pop_sub_grid_id)._removeAllRow();
		$$(this.pop_sub_grid_id)._addRow(this.getParentGridStoreData());
		
		$$(this.pop_window_id).show();
		
	};
	
	// 선택되어 있는 부모 그리드 목록 getter
	this.getParentGridStoreData = function() {
		return $$(this.id)._getRows();
	};
	
	// 선택되어 있는 서브 그리드 목록 getter
	this.getSelGridStoreData = function() {
		return $$(this.pop_sub_grid_id)._getRows();
	};
	
	// 서브 그리드 row 추가
	this.addSelGrid = function() {
		var data = $$(this.pop_main_grid_id)._getSelectedRows();
		if(data.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(this.pop_sub_grid_id)._addRow(data);
		
		this.reloadGrid(true);
	};
	
	// 서브 그리드 row 제거
	this.removeSelGrid = function(e, row, html) {
		var data = $$(this.pop_sub_grid_id)._getSelectedRows();
		if(data!=null && data.length > 0) {
			if(data.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '선택된 정보가 없습니다.'
				});
				return false;
			}
			$$(this.pop_sub_grid_id)._removeRow();
		} else {
			$$(this.pop_sub_grid_id)._removeRow(row);
		}
		
		this.reloadGrid(true);
	};
	
	// 값 선택 이벤트
	this.choice = function(){
		var rows = $$(this.pop_sub_grid_id)._getRows();
		$$(this.id)._removeAllRow();
        $$(this.id)._addRow(rows);
        
        this.window.close();
	};
	
	// 검색 이벤트
	this.search = function(){
		var searchParams = $$(this.pop_search_form_id)._getValues();
		
		searchParams.selUserList = this.getSelGridStoreData();
		
		$$(this.pop_main_grid_id)._reload(searchParams);
	};
	
	// 그리드 갱신 이벤트
	this.reloadGrid = function(isGoPage){
		var searchParams = $$(this.pop_search_form_id)._getValues();
		
		searchParams.selUserList = this.getSelGridStoreData();
		
		$$(this.pop_main_grid_id)._reload(searchParams, isGoPage);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.pop_search_form_id)._reset();
	};
	
	// 이력 팝업
	this.openHisPop = function(btnId, e) {
		
		var _this = this;
		
		// 검색용 날짜객체 (최근 30일)
		var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
		var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		var historyPopSearchForm = createFormComp({
			id: "historyPopSearchForm",
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
						createBtnComp({ label:"초기화", click: initHistoryPop })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
				    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
				]
			}
		});
		
		var historyPopGrid = createGridComp({
			id : "historyPopGrid",
			resizeColumn : true,
			select : false,
			params:  {
	        	ASSET_ID : _this.asset_id,
	        	CONF_ID : _this.conf_id,
	        	updDt_startDate : start_date_obj.day,
	        	updDt_endDate : end_date_obj.day
			},
			url : "/itg/itam/assetHistory/selectOpmsHisRelServiceList.do",
			resource: "grid.itam.service.his",
			header : {
				title : "수정이력"
			},
			cellMerges: ["INS_DT", "INS_USER_NM"]
		});
		
		nkia.ui.utils.window({
			id:"historyPopup",
			callFunc: function() {
				this.window = createWindow({
			    	id: "historyPopup",
			    	width: 900,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "수정이력"
			    	},
			    	body: {
			    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
			    	},
			    	closable: true
			    })
			}
		});
		
		function searchHistoryPop() {
			var searchParam = $$("historyPopSearchForm")._getValues();
			searchParam["ASSET_ID"] = _this.asset_id;
			searchParam["CONF_ID"] = _this.conf_id;
			$$("historyPopGrid")._reload(searchParam);
		}
		
		function initHistoryPop() {
			$$("historyPopSearchForm")._reset();
		}
		
	};
	
};