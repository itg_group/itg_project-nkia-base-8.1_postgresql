/********************************************
 * Install Software Component
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 설치 소프트웨어 정보 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.InstallSwList = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.add_btn_suffix = "_ADD_BTN";
	this.update_btn_suffix = "_UPDATE_BTN";
	this.del_btn_suffix = "_DEL_BTN";
	this.detail_btn_suffix = "_DETAIL_BTN";
	// 설치SW의 key 컬럼 : SW그룹ID, SW명, 버전, 에디션, 설치경로
	this.key_column = ["SW_GROUP_ID", "SW_NM", "SW_VERSION", "SW_EDITION", "SW_INST_PATH"];
	
	this.getObject = function() {
		
		var _this = this;
		
		var url = '/itg/itam/opms/selectOpmsInsSwList.do';
		
		var gridHeader = {
				title: _this.title
		};
		
		// 권한이 무조건 U로 넘어옴...
//		if (_this.auth_type == "U") {
//			gridHeader["buttons"] = {
//				items: [
//					createBtnComp({id:_this.id+_this.add_btn_suffix, label:"추가", click: this.openPopup.bind(_this), type: "form" }),
//					createBtnComp({id:_this.id+_this.update_btn_suffix, label:"수정", click: this.openUpdatePopup.bind(_this), type: "form" }),
//			        createBtnComp({id:_this.id+_this.del_btn_suffix, label:"제거", click: this.removeRow.bind(_this) })
//				]
//			};
//		}
		
		var grid = createGridComp({
	        id : _this.id,
	        header: gridHeader,
	        resizeColumn:true,
	        params:  {
	        	conf_id : _this.conf_id
			},
	        url: url,
	        height: 200,
	        resource: "grid.itam.oper.installedSw",
	        checkbox: false,
	        keys: this.key_column,
	        rowDisabledConditions: {
	        	SW_AD_YN : ["Y"]
	        }
	    });
		
		return grid;
	};
	
	// 선택 그리드 row 삭제
	this.removeRow = function(btnId, ev) {
		var gridId = btnId.replace(this.del_btn_suffix, "");
		var gridItems = $$(gridId)._getSelectedRows();
		$$(gridId)._removeRow();
	};
	
	/**************************************
	 * 팝업 영역
	 *************************************/
	
	// 데이터 등록팝업
	this.openPopup = function(btnId, ev) {
		
		var _this = this;
		
		// 실제 팝업 각 요소 ID 세팅
		var parentId = btnId.replace(this.add_btn_suffix, "");
		this.pop_window_id = parentId + "_pop_window";
		this.pop_form_id = parentId + "_pop_form";
		
		var form = createFormComp({
			id: this.pop_form_id,
			header: {
				title: this.title+" 등록"
			},
			fields: {
				colSize: 2,
				items: [
			        { colspan: 2, item: createUnionFieldComp({
					    items: [
					            createSearchFieldComp({
									label: "S/W 그룹",
									name: 'sw_group_id_nm',
									required: true,
									click: this.openSwGroupPopup.bind(this)
								}),
								createBtnComp({
									label: "초기화",
									click: function() {
										$$(this.pop_form_id)._setValues({
											sw_group_id : "",
											sw_group_id_nm : ""
										});
									}
								})
					        ]
					    })
					},
					{ colspan: 1, item: createTextFieldComp({label: "S/W 명", name: "sw_nm", required: true }) },
			        { colspan: 1, item: createTextFieldComp({label: "S/W 버전", name: "sw_version", required: true }) },
			        { colspan: 1, item: createDateFieldComp({label: "S/W 설치일", name: "sw_inst_dt", dateType:'D'}) },
			        { colspan: 1, item: createTextFieldComp({label: "S/W Edition", name: "sw_edition", required: true, placeholder: "-" }) },
			        { colspan: 1, item: createTextFieldComp({label: "S/W 설치경로", name: "sw_inst_path", required: true }) }
			    ],
			    hiddens: {
			    	sw_group_id : ""
			    }
			}
		});
		
		this.window = createWindow({
	    	id: this.pop_window_id,
	    	width: 700,
	    	height: 670,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: new Array().concat(form)
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.regist.bind(this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
		
		
		$$(this.pop_window_id).show();
		
	};
	
	// 데이터 수정팝업
	this.openUpdatePopup = function(btnId, ev) {
		
		var _this = this;
		
		// 실제 팝업 각 요소 ID 세팅
		var parentId = btnId.replace(this.update_btn_suffix, "");
		this.pop_window_id = parentId + "_pop_window";
		this.pop_form_id = parentId + "_pop_form";
		
		// 수정 대상 그리드 Row의 데이터
		var gridData = $$(parentId)._getSelectedRows();
		
		if(gridData.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		} else if(gridData.length > 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '1개의 데이터를 선택해주세요.'
			});
			return false;
		}
		
		var form = createFormComp({
			id: this.pop_form_id,
			header: {
				title: this.title+" 등록"
			},
			fields: {
				colSize: 2,
				items: [
			        { colspan: 2, item: createUnionFieldComp({
					    items: [
					            createSearchFieldComp({
									label: "S/W 그룹",
									name: 'sw_group_id_nm',
									required: true,
									click: this.openSwGroupPopup.bind(this)
								}),
								createBtnComp({
									label: "초기화",
									click: function() {
										$$(this.pop_form_id)._setValues({
											sw_group_id : "",
											sw_group_id_nm : ""
										});
									}
								})
					        ]
					    })
					},
					{ colspan: 1, item: createTextFieldComp({label: "S/W 명", name: "sw_nm", required: true }) },
			        { colspan: 1, item: createTextFieldComp({label: "S/W 버전", name: "sw_version", required: true }) },
			        { colspan: 1, item: createDateFieldComp({label: "S/W 설치일", name: "sw_inst_dt", dateType:'D'}) },
			        { colspan: 1, item: createTextFieldComp({label: "S/W Edition", name: "sw_edition", required: true, placeholder: "-" }) },
			        { colspan: 1, item: createTextFieldComp({label: "S/W 설치경로", name: "sw_inst_path", required: true }) }
			    ],
			    hiddens: {
			    	sw_group_id: ""
			    }
			}
		});
		
		this.window = createWindow({
	    	id: this.pop_window_id,
	    	width: 700,
	    	height: 670,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: new Array().concat(form)
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '수 정', type: "form", click: this.update.bind(this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
		
		$$(this.pop_window_id).show();
		
		var copyMap = JSON.parse(JSON.stringify(gridData[0]));
		var lowerMap = {};
		for(var key in copyMap) {
			lowerMap[key.toLowerCase()] = copyMap[key];
		}
		
		$$(this.pop_form_id)._setValues(lowerMap);
		
	};
	
	// SW그룹 선택 팝업
	this.openSwGroupPopup = function(btnId, ev) {
		
		var parentFormId = btnId.replace(this.add_btn_suffix, "");
		
		var _this = this;
		
		// 팝업 내, SW그룹 조회 트리 팝업 함수 생성
		var tree = createTreeComponent({
			id: "sw_group_tree",
	        url: "/itg/itam/sam/searchSwGroupTree.do",
	        filterable: true,
	        expColable: true,
	        params: {swTrgetSe : "SV"},
	        expandLevel: 3,
	        on: {
	        	onItemDblClick: function(id) {
	        		var treeItem = this.getItem(id);
	        		var values = {};
	        		values["sw_group_id"] = treeItem.node_id;
	        		values["sw_group_id_nm"] = treeItem.text;
	        		$$(_this.pop_form_id)._setValues(values);
	        		_this.innerWindow.close();
	        	}
	        }
	    });
		
		_this.innerWindow = createWindow({
	    	id: "sw_group_tree_window",
	    	width: 300,
	    	header: {
	    		title: "S/W 그룹 선택"
	    	},
	    	body: {
	    		rows: tree
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택',
    		            	type : "form",
    		            	click: function() {
    		            		var treeItem = $$("sw_group_tree").getSelectedItem();
    			        		var values = {};
    			        		values["sw_group_id"] = treeItem.node_id;
    			        		values["sw_group_id_nm"] = treeItem.text;
    			        		$$(_this.pop_form_id)._setValues(values);
    			        		_this.innerWindow.close();
    			        	}
    		            })
    		        ]
	    		}
	    	},
	    	closable: true
	    });
		
		$$("sw_group_tree_window").show();
	};
	
	// 적용버튼 클릭
	this.regist = function(btnId, ev) {
		
		var _this = this;
		var form = $$(_this.pop_form_id);
		
		// 동일한 값이 있는지 비교할 대상 그리드 Row의 데이터 (전체 Row가 비교 대상)
		var checkGridData = $$(_this.id)._getRows();
		
		if(form.validate()) {
			
			var formData = form._getValues();
			var upperValues = {};
			
			for(var key in formData) {
				upperValues[key.toUpperCase()] = formData[key];
			}
			
			// 수동 등록 시, 자동수집여부 N 으로 세팅
			upperValues["SW_AD_YN"] = "N";
			upperValues["SW_AD_YN_NM"] = "아니오";
			
			var rows = [];
			rows.push(upperValues);
			
			// 그리드 내에 동일한 데이터가 있는지 공통 모듈로 체크
			var isEqualData = nkia.ui.utils.isUniqueViolationData(_this.key_column, rows, checkGridData);
			
			// 그리드 내에 동일한 데이터가 있을 경우 데이터 추가 불가
			if(isEqualData) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '이미 동일한 데이터가 존재합니다.'
				});
				return false;
			} else {
				$$(_this.id)._addRow(rows);
			}
			
			_this.window.close();
		}
	};
	
	// 수정버튼 클릭
	this.update = function(btnId, ev) {
		
		var _this = this;
		var form = $$(_this.pop_form_id);
		
		// 동일한 값이 있는지 비교할 대상 그리드 Row의 데이터 (선택된 Row를 빼고 나머지 Row가 비교 대상)
		var checkGridData = $$(_this.id)._getUnSelectedRows();
		
		if(form.validate()) {
			
			var formData = form._getValues();
			var upperValues = {};
			
			for(var key in formData) {
				upperValues[key.toUpperCase()] = formData[key];
			}
			
			// 수동 등록 시, 자동수집여부 N 으로 세팅
			upperValues["SW_AD_YN"] = "N";
			upperValues["SW_AD_YN_NM"] = "아니오";
			
			var rows = [];
			rows.push(upperValues);
			
			// 그리드 내에 동일한 데이터가 있는지 공통 모듈로 체크
			var isEqualData = nkia.ui.utils.isUniqueViolationData(_this.key_column, rows, checkGridData);
			
			// 그리드 내에 동일한 데이터가 있을 경우 데이터 추가 불가
			if(isEqualData) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '이미 동일한 데이터가 존재합니다.'
				});
				return false;
			} else {
				$$(_this.id)._removeRow();
				$$(_this.id)._addRow(rows);
			}
			
			_this.window.close();
		}
	};
	
	// 두 데이터 간에 동일한 데이터가 있는지 체크하는 함수
//	this.isEqualDataByKey = function(data, target) {
//		
//		var _this = this;
//		
//		var resultCount = 0;
//		var keys = _this.key_column;
//		var keySize = keys.length;
//		
//		// deep copy
//		var cData = JSON.parse(JSON.stringify(data));
//		var cTarget = JSON.parse(JSON.stringify(target));
//		
//		// 키 배열의 값을 한 문장으로 만듬
//		if(cData && cData.length > 0 && cTarget && cTarget.length > 0) {
//			for(var i in keys) {
//				for(var j in cData) {
//					if(cData[j].CONCAT_KEY_STRING) {
//						cData[j].CONCAT_KEY_STRING = cData[j].CONCAT_KEY_STRING + cData[j][keys[i]];
//					} else {
//						cData[j].CONCAT_KEY_STRING = cData[j][keys[i]];
//					}
//				}
//			}
//			for(var i in keys) {
//				for(var j in cTarget) {
//					if(cTarget[j].CONCAT_KEY_STRING) {
//						cTarget[j].CONCAT_KEY_STRING = cTarget[j].CONCAT_KEY_STRING + cTarget[j][keys[i]];
//					} else {
//						cTarget[j].CONCAT_KEY_STRING = cTarget[j][keys[i]];
//					}
//				}
//			}
//		}
//		
//		console.log(cData);
//		console.log(cTarget);
//		
//		// 같은 키가 있는지 비교하여 같은 키가 있으면 체크박스 선택 못하도록 disable
//		for(var i in thisRows) {
//			if(thisRows[i]) {
//				for(var j in targetRows) {
//					if(thisRows[i].CONCAT_KEY_STRING == targetRows[j].CONCAT_KEY_STRING) {
//						thisRows[i]["check_disabled"] = true;
//						var checkboxHtml = this.getItemNode(thisRows[i]["id"]);
//						jq(checkboxHtml).find("input[type=checkbox]").prop("disabled", true);
//						this.addRowCss(thisRows[i]["id"], "disabled");
//						break;
//					}
//				}
//			}
//		}
//		
//	}
	
};