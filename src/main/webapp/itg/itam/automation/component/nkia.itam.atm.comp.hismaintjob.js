/********************************************
 * Incident Process History Tab Component
 * Date: 2017-01-13
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description : 작업기록이력 탭 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.MaintJobHistory = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.tangible_asset_yn = prop.tangible_asset_yn;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.req_type = "MAINT_JOB";
	
	// 검색용 날짜객체 (최근 30일)
	this.start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	this.end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	/********************************************
	 * Function : getObject
	 * 작업기록이력 페이지 요소들을 하나의 객체화 하여 리턴
	 ********************************************/
	this.getObject = function() {
		
		var _this = this;
		
		// 페이지 생성 객체들을 담아줄 변수
		var pageObjects;
		
		var searchForm = createFormComp({
			id: "maintJobHisSearchForm",
			elementsConfig: {
				labelPosition: "left"
			},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.searchHistoryList.bind(_this) }),
						createBtnComp({ label:"초기화", click: this.clearSearchForm.bind(_this) })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
					{colspan: 1, item: 
	        			createSearchDateFieldComp({
							name: "search_job_dt", 
							label: "작업일자",
							dateType:"D", 
							start_dateValue : _this.start_date_obj, 
							end_dateValue: _this.end_date_obj
	                    })
					}]
			}
		}); // end searchForm
		
		var popAdSize = 0;
		
		var maintJobHisGrid = createGridComp({
			id : 'maintJobHisGrid',
			header : {
				title : "작업기록이력"
			},
			resizeColumn : true,
			checkbox : false,
			pageable : true,
			pageSize : 50,
			url : "/itg/itam/maint/selectMaintJobListByConfId.do",
			resource : "grid.itam.opms.maintJobList",
			params : {
				conf_id : _this.conf_id,
				asset_id : _this.asset_id,
				search_job_dt_startDate : _this.start_date_obj.day,
				search_job_dt_endDate : _this.end_date_obj.day
			},
			on:{
				onItemClick: this.showMaintJobDetailPop
			}
		});
		
		pageObjects = new Array().concat(searchForm, maintJobHisGrid);
		
		return pageObjects;
		
	} // end getObject
	
	/********************************************
	 * Function : searchHistoryList
	 * 작업기록이력 검색 이벤트
	 ********************************************/
	this.searchHistoryList = function() {
		var searchParams = $$("maintJobHisSearchForm")._getValues();
		searchParams["conf_id"] = this.conf_id;
		searchParams["asset_id"] = this.asset_id;
		
		$$("maintJobHisGrid")._reload(searchParams); // 페이지 유지
	}
	
	/********************************************
	 * Function : clearSearchForm
	 * 검색폼 초기화 이벤트
	 ********************************************/
	this.clearSearchForm = function() {
		$$("maintJobHisSearchForm")._reset();
	}
	
	/********************************************
	 * Function : showMaintJobDetailPop
	 * 작업기록이력 상세조회 팝업 이벤트
	 ********************************************/
	this.showMaintJobDetailPop = function(rowId, event, node) {
		var clickRecord = $$("maintJobHisGrid").getItem(rowId);
		
		if(typeof clickRecord == "undefined" || clickRecord == null) {
			alert("조회할 정보가 없습니다.");
			return false;
		}
		
		var maintJobDetailForm = createFormComp({
			id: "maintJobDetailForm",
			elementsConfig: {
				labelPosition: "top"
			},
			header: {
				title: "작업기록이력 상세조회",
			},
			fields: {
				colSize: 2,
				items: [
					{colspan:1, item:createTextFieldComp({label:'등록일자', name:'ins_dt', fixedLabelWidth:80, value:clickRecord.INS_DT, readonly: true})}
					, {colspan:1, item:createTextFieldComp({label:'작업시작시간', name:'job_begin_dt_time', value:clickRecord.JOB_BEGIN_DT_TIME, readonly: true})}
		             , {colspan:1, item:createTextFieldComp({label:'등록자', name:'ins_user_nm', fixedLabelWidth:80, value:clickRecord.INS_USER_NM, readonly: true})}
		             , {colspan:1, item:createTextFieldComp({label:'작업종료시간', name:'job_end_dt_time', value:clickRecord.JOB_END_DT_TIME, readonly: true})}
		             , {colspan:1, item:createTextFieldComp({label:'작업자', name:'job_user_nm', fixedLabelWidth:80, value:clickRecord.JOB_USER_NM, readonly: true})}
		             , {colspan:1, item:createTextFieldComp({label:'작업자 회사명', name:'job_corp_nm', value:clickRecord.JOB_CORP_NM, readonly: true})}
		             , {colspan:2, item:createTextFieldComp({label:'작업제목', name:'job_title', fixedLabelWidth:80, value:clickRecord.JOB_TITLE, readonly: true})}
		             , {colspan:2, item:createTextAreaFieldComp({label:'작업내용', name:'job_desc', fixedLabelWidth:80, value:clickRecord.JOB_DESC, resizable: true, readonly: true})}
					]
			}
		});
		
		nkia.ui.utils.window({
			id:"maintJobDetailPop",
			callFunc: function() {
				this.window = createWindow({
			    	id: "maintJobDetailPop",
			    	width: 730,
			    	header: {
			    		title: "작업기록이력 상세조회"
			    	},
			    	body: {
			    		rows: maintJobDetailForm
			    	},
			    	closable: true
			    })
			}
		}); // end nkia.ui.utils.window
		
	} // end showMaintJobDetailPop
	
	/********************************************
	 * Function : createMaintJobRegistPop
	 * 작업기록이력 등록 팝업 이벤트
	 ********************************************/
	this.createMaintJobRegistPop = function() {
		
		var _this = this;
		
		var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:-3, min:0});
		var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		var maintJobRegistForm = createFormComp({
			id: "maintJobRegistForm",
			elementsConfig: {
				labelPosition: "top"
			},
			header: {
				title: "작업기록이력 등록",
			},
			fields: {
				colSize: 2,
				items: [
				       {colspan:1, item:createDateFieldComp({label:'등록일자', name:'ins_dt', dateType : 'D',  dateValue: startDateObj, required: true})}, 
				       {colspan:1, item:createDateFieldComp({label:'작업시작시간', name:'job_begin_dt_time', dateType:'DHM', dateValue:startDateObj, minuteIntervalThirty:true, required: true})} ,
//				       {colspan:1, item:createOutUserSelectPopComp({label:'등록자', name:'ins_user_nm', hiddenName:'ins_user_id', fixedLabelWidth:80, notNull:true})},
				       {colspan:1, item:createTextFieldComp({label:'등록자', name:'ins_user_nm', hiddenName:'ins_user_id', fixedLabelWidth:80, required: true})},
			           {colspan:1, item:createDateFieldComp({label:'작업종료시간', name:'job_end_dt_time', dateType:'DHM', dateValue:endDateObj, minuteIntervalThirty:true, required: true})},
			           {colspan:1, item:createTextFieldComp({label:'작업자', name:'job_user_nm', fixedLabelWidth:80, required: true})},
			           {colspan:1, item:createTextFieldComp({label:'작업자 회사명', name:'job_corp_nm', required: true})},
			           {colspan:2, item:createTextFieldComp({label:'작업제목', name:'job_title', fixedLabelWidth:80, required: true, maxLength:150})},
			           {colspan:2, item:createTextAreaFieldComp({label:'작업내용', name:'job_desc', fixedLabelWidth:80, required: true, maxLength:2000, resizable: true})}
					]
			}
		});
		
		nkia.ui.utils.window({
			id:"maintJobRegistPop",
			callFunc: function() {
				_this.window = createWindow({
			    	id: "maintJobRegistPop",
			    	width: 750,
			    	header: {
			    		title: "작업기록이력 등록"
			    	},
			    	body: {
			    		rows: maintJobRegistForm
			    	},
			    	footer: {
			    		buttons: {
			    			align: "center",
			    			items: [
		    		            createBtnComp({label: '등 록', type : "form", click: _this.registMaintJob.bind(_this)})
		    		        ]
			    		}
			    	},
			    	closable: true
			    })
			}
		}); // end nkia.ui.utils.window
		
	} // end createMaintJobRegistPop
	
	
	/********************************************
	 * Function : registMaintJob
	 * 작업기록이력 등록 이벤트
	 ********************************************/
	this.registMaintJob = function() {

		var _this = this;
		
		var dataMap = $$("maintJobRegistForm")._getValues();
		var assetList = new Array();
		assetList.push({
			ASSET_ID : asset_id
			, CONF_ID : conf_id
		});
		dataMap["asset_list"] = assetList;
		
		var beginDtValue = dataMap['job_begine_dt_time'];
		var endDtValue = dataMap['job_end_dt_time'];
		
		if(beginDtValue > endDtValue) {
			alert("[작업종료일자]는 [작업시작일자]보다 빠를 수 없습니다.");
			return;
		}
		if(!confirm("작업기록 정보를 등록하시겠습니까?")) {
			_this.createMaintJobRegistPop.close();
			return;
		}
		/********************************************
		 * Function : callback
		 * 작업기록이력 등록 이벤트
		 ********************************************/
		var callback = function(data){
			alert("등록되었습니다.");
			_this.window.close();
			_this.searchHistoryList();
		};
		
		nkia.ui.utils.ajax({
			viewId: 'assetDetailViewer',
			url: '/itg/itam/maint/insertMaintJob.do',
			params: dataMap,
			isMask: false,
			success: callback
		});
		
	} // end registMaintJob
	
	
	
};