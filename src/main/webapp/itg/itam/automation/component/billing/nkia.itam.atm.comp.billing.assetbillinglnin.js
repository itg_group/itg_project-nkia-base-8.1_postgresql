/********************************************
 * Line Billing Information Tab Component
 * Date: 2017-06-01
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 과금정보(LN) 조회 탭 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.billing.LN_IN = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.center_asset_yn = prop.center_asset_yn;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	
	/********************************************
	 * Function : getObject
	 * 요소들을 하나의 객체화 하여 리턴
	 ********************************************/
	this.getObject = function() {
		
		var _this = this;
		
		// 페이지 생성 객체들을 담아줄 변수
		var pageObjects;
		
		var formData = {};
		nkia.ui.utils.ajax({
			async: false,
			url: "/itg/itam/billing/searchAssetBillingLn.do",
			params: {conf_id : props.conf_id},
			isMask: false,
			notification: false,
			success: function(response) {
				if(response.gridVO && response.gridVO.rows) {
					var data = response.gridVO.rows;
					if(data.length > 0) {
						formData = data[0];
					}
				}
			}
		});
		
		// 과금정보 등록 폼
		var lnBillingForm = createFormComp({
			id: "lnBillingForm",
			header: {
				title: "과금 정보"
			},
			fields: {
				colSize: 2, // 열 수
				items: [
				    { colspan: 1, item: createCodeComboBoxComp({label: "과금 대상 여부", name: "BILL_YN", code_grp_id: 'YN', attachChoice: true, readonly: true, value:((formData.BILL_YN) ? formData.BILL_YN : "")}) },
				    { colspan: 1, item: createDateFieldComp({label: "원가과금 시작일", name: "BS_BGN_DT", dateType:'D', readonly: true, value:((formData.BS_BGN_DT) ? formData.BS_BGN_DT : "")}) },
				    { colspan: 1, item: createTextFieldComp({label: "회선비 (단위: 원)", name: "LN_PRC", inputType:"price", readonly: true, value:((formData.LN_PRC) ? formData.LN_PRC : "")}) },
				    { colspan: 1, item: createDateFieldComp({label: "원가과금 종료일", name: "BS_END_DT", dateType:'D', readonly: true, value:((formData.BS_END_DT) ? formData.BS_END_DT : "")}) },
				    { colspan: 1, item: createTextFieldComp({label: "운영비 (단위: 원)", name: "MA_PRC", inputType:"price", readonly: true, value:((formData.MA_PRC) ? formData.MA_PRC : "")}) }
				]
			}
		});
		
		// 청구처별 사용량 그리드
		var cmprcLnGrid = createGridComp({
			id : 'cmprcLnGrid',
			resizeColumn : true,
			height : 250,
			resource : "grid.itam.automation.billing.custrate.in.nwln",
			url : "/itg/itam/billing/searchBsCmprc.do",
			params : {conf_id : props.conf_id},
			header: {
				title: "사용처 관리"
			}
		});
		
		pageObjects = new Array().concat(lnBillingForm, cmprcLnGrid);
		
		return pageObjects;
	};
	
};