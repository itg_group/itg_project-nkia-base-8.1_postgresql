/********************************************
 * Storage Billing Information Tab Component
 * Date: 2017-06-01
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 과금정보(ST) 조회 탭 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.billing.ST_OUT = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.center_asset_yn = prop.center_asset_yn;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	
	/********************************************
	 * Function : getObject
	 * 요소들을 하나의 객체화 하여 리턴
	 ********************************************/
	this.getObject = function() {
		
		var _this = this;
		
		// 페이지 생성 객체들을 담아줄 변수
		var pageObjects;
		
		var formData = {};
		nkia.ui.utils.ajax({
			async: false,
			url: "/itg/itam/billing/searchAssetBillingSt.do",
			params: {conf_id : props.conf_id},
			isMask: false,
			notification: false,
			success: function(response) {
				if(response.gridVO && response.gridVO.rows) {
					var data = response.gridVO.rows;
					if(data.length > 0) {
						formData = data[0];
					}
				}
			}
		});
		
		// 스토리지 과금정보 등록 폼
		var stBillingForm = createFormComp({
			id: "stBillingForm",
			header: {
				title: "과금 정보"
			},
			fields: {
				colSize: 2, // 열 수
				items: [
				    { colspan: 1, item: createCodeComboBoxComp({label: "과금 대상 여부", name: "BILL_YN", code_grp_id: 'YN', attachChoice: true, readonly: true, value:((formData.BILL_YN) ? formData.BILL_YN : "")}) },
				    { colspan: 1, item: createDateFieldComp({label: "원가과금 시작일", name: "BS_BGN_DT", dateType:'D', readonly: true, value:((formData.BS_BGN_DT) ? formData.BS_BGN_DT : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "과금 분류", name: "ST_COST_CLS", code_grp_id: 'ST_COST_CLS', attachChoice: true, readonly: true, value:((formData.ST_COST_CLS) ? formData.ST_COST_CLS : "")}) },
				    { colspan: 1, item: createDateFieldComp({label: "원가과금 종료일", name: "BS_END_DT", dateType:'D', readonly: true, value:((formData.BS_END_DT) ? formData.BS_END_DT : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "스토리지 등급", name: "ST_LV", code_grp_id: 'ST_ALL_LV', attachChoice: true, readonly: true, value:((formData.ST_LV) ? formData.ST_LV : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "스토리지 유형", name: "ST_SE", code_grp_id: 'ST_SE', attachChoice: true, readonly: true, value:((formData.ST_SE) ? formData.ST_SE : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "Disk 유형", name: "DISK_SE", code_grp_id: 'DISK_SE', attachChoice: true, readonly: true, value:((formData.DISK_SE) ? formData.DISK_SE : "")}) }
				]
			}
		});
		
		pageObjects = new Array().concat(stBillingForm);
		
		return pageObjects;
	};
	
};