/********************************************
 * Server Billing Information Tab Component
 * Date: 2017-06-01
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 과금정보(SV) 조회 탭 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.billing.SV_OUT = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.center_asset_yn = prop.center_asset_yn;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	
	/********************************************
	 * Function : getObject
	 * 요소들을 하나의 객체화 하여 리턴
	 ********************************************/
	this.getObject = function() {
		
		var _this = this;
		
		// 페이지 생성 객체들을 담아줄 변수
		var pageObjects;
		
		var formData = {};
		nkia.ui.utils.ajax({
			async: false,
			url: "/itg/itam/billing/searchAssetBillingSv.do",
			params: {conf_id : props.conf_id},
			isMask: false,
			notification: false,
			success: function(response) {
				if(response.gridVO && response.gridVO.rows) {
					var data = response.gridVO.rows;
					if(data.length > 0) {
						formData = data[0];
					}
				}
			}
		});
		
		// 서버 과금정보 등록 폼
		var svBillingForm = createFormComp({
			id: "svBillingForm",
			header: {
				title: "과금 정보"
			},
			fields: {
				colSize: 2, // 열 수
				items: [
				    { colspan: 1, item: createCodeComboBoxComp({label: "과금 대상 여부", name: "BILL_YN", code_grp_id: 'YN', attachChoice: true, readonly: true, value:((formData.BILL_YN) ? formData.BILL_YN : "")}) },
				    { colspan: 1, item: createDateFieldComp({label: "원가과금 시작일", name: "BS_BGN_DT", dateType:'D', readonly: true, value:((formData.BS_BGN_DT) ? formData.BS_BGN_DT : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "과금 분류", name: "SV_COST_CLS", code_grp_id: 'SV_COST_CLS', attachChoice: true, readonly: true, value:((formData.SV_COST_CLS) ? formData.SV_COST_CLS : "")}) },
				    { colspan: 1, item: createDateFieldComp({label: "원가과금 종료일", name: "BS_END_DT", dateType:'D', readonly: true, value:((formData.BS_END_DT) ? formData.BS_END_DT : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "서버 종류", name: "SV_SE", code_grp_id: 'SV_ALL_SE', attachChoice: true, readonly: true, value:((formData.SV_SE) ? formData.SV_SE : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "서버 등급", name: "SV_LV", code_grp_id: 'SV_ALL_LV', attachChoice: true, readonly: true, value:((formData.SV_LV) ? formData.SV_LV : "")}) },
				    { colspan: 1, item: createCodeComboBoxComp({label: "용도", name: "SV_USE", code_grp_id: 'SV_ALL_USE', attachChoice: true, readonly: true, value:((formData.SV_USE) ? formData.SV_USE : "")}) }
				]
			}
		});
		
		// 메모리 증설량 그리드
		var insMemoryGrid = createGridComp({
			id : 'insMemoryGrid',
			resizeColumn : true,
			height : 250,
			resource : "grid.itam.automation.billing.ins.memory",
			url : "/itg/itam/billing/searchBsInsMemory.do",
			params : {conf_id : _this.conf_id},
			header: {
				title: "메모리 증설량"
			}
		});
		
		// CPU Core 증설량 그리드
		var insCpuGrid = createGridComp({
			id : 'insCpuGrid',
			resizeColumn : true,
			height : 250,
			resource : "grid.itam.automation.billing.ins.core",
			url : "/itg/itam/billing/searchBsInsCpu.do",
			params : {conf_id : _this.conf_id},
			header: {
				title: "CPU Core 증설량"
			}
		});
		
		// 스토리지 증설량 그리드
		var insStorageGrid = createGridComp({
			id : 'insStorageGrid',
			resizeColumn : true,
			height : 250,
			resource : "grid.itam.automation.billing.ins.storage",
			url : "/itg/itam/billing/searchBsInsStorage.do",
			params : {conf_id : _this.conf_id},
			header: {
				title: "스토리지 증설량"
			}
		});
		
		// 청구처별 사용량(서버비중) 그리드
		var custRateSvGrid = createGridComp({
			id : 'custRateSvGrid',
			resizeColumn : true,
			height : 250,
			resource : "grid.itam.automation.billing.custrate.out.sv.sv",
			url : "/itg/itam/billing/searchBsCustRate.do",
			params : {conf_id : _this.conf_id, conf_type : "SV"},
			header: {
				title: "청구처별 사용량 - 서버 비중"
			}
		});
		
		// 청구처별 사용량(스토리지비중) 그리드
		var custRateStGrid = createGridComp({
			id : 'custRateStGrid',
			resizeColumn : true,
			height : 250,
			resource : "grid.itam.automation.billing.custrate.out.sv.st",
			url : "/itg/itam/billing/searchBsCustRate.do",
			params : {conf_id : _this.conf_id, conf_type : "ST"},
			header: {
				title: "청구처별 사용량 - 스토리지 비중"
			}
		});
		
		var custRateGridTab = {
			view:"tabview",
			id:"custRateGridTab",
			animate:false,
			tabbar: {
				options: [{
					id:"custRateSvGridTab",
					value: "서버 비중"
				},{
					id:"custRateStGridTab",
					value: "스토리지 비중"
				}]
			},
			cells: [{
				id:"custRateSvGridTab",
				rows: new Array().concat(custRateSvGrid)
			},{
				id:"custRateStGridTab",
				rows: new Array().concat(custRateStGrid)
			}]
		};
		
		pageObjects = new Array().concat(svBillingForm, insMemoryGrid, insCpuGrid, insStorageGrid, custRateGridTab);
		
		return pageObjects;
	};
	
};