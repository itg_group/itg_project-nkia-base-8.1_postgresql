/********************************************
 * Manager Component
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 담당자 정보 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.ManagerList = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.add_btn_suffix = "_ADD_BTN";
	this.del_btn_suffix = "_DEL_BTN";
	this.his_btn_suffix = "_HIS_BTN";
	this.entity_mng_type = prop.entity_mng_type; //entity_mng_type 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
	
	// 운영자 key 컬럼 : 사용자 ID
	this.key_column = ["USER_ID"];
	
	this.pop_window_id = "";
	this.pop_tree_id = "";
	this.pop_search_form_id = "";
	this.pop_main_grid_id = "";
	this.pop_sub_grid_id = "";
	
	/**************************************
	 * 컴포넌트 영역
	 *************************************/
	this.getObject = function() {
		
		var _this = this;
		
		var url = '/itg/itam/opms/selectOpmsSelMngUserList.do';
		if(_this.view_type == 'infmodifiy'){
			url = '/itg/itam/opms/selectOpmsInfoSelMngUserList.do';
		}
		
		var gridHeader = {
			title: _this.title
		};
		
		if (_this.auth_type == "U") {
			gridHeader["buttons"] = {
				items: [
					createBtnComp({id:_this.id+_this.add_btn_suffix, label:"추가", click: this.openPopup.bind(_this), type: "form" }),
					createBtnComp({id:_this.id+_this.del_btn_suffix, label:"제거", click: this.removeRow.bind(_this) }),
					createBtnComp({id:_this.id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
				]
			};
		} else {
			gridHeader["buttons"] = {
				items: [
					createBtnComp({id:_this.id+_this.his_btn_suffix, label:"이력조회", click: this.openHisPop.bind(_this) })
				]
			};
		}
		
		var grid = createGridComp({
	        id : _this.id,
	        header: gridHeader,
	        resizeColumn:true,
	        params:  {
	        	conf_id : _this.conf_id
			},
	        url: url,
	        height: 200,
	        resource: "grid.itam.managerList",
	        checkbox: true,
	        keys: _this.key_column,
	        editprops : [{
				column: "OPER_USER_TYPE",
				attachChoice: true,
				params: {code_grp_id : "OPER_USER_TYPE"}
			}],
			requiredColumns: ["OPER_USER_TYPE"]
	    });
		
		return grid;
	};
	
	// 선택 그리드 row 삭제
	this.removeRow = function(btnId, ev) {
		var gridId = btnId.replace(this.del_btn_suffix, "");
		$$(gridId)._removeRow();
	};
	
	/**************************************
	 * 팝업 영역
	 *************************************/
	// 데이터 선택팝업
	this.openPopup = function(btnId, ev) {
		
		var parentGridId = btnId.replace(this.add_btn_suffix, "");
		var parentRows = $$(parentGridId)._getRows();
		
		// 각 요소 ID 세팅
		this.pop_window_id = parentGridId + "_pop_window";
		this.pop_tree_id = parentGridId + "_pop_tree";
		this.pop_search_form_id = parentGridId + "_pop_searchForm";
		this.pop_main_grid_id = parentGridId + "_pop_main_grid";
		this.pop_sub_grid_id = parentGridId + "_pop_sub_grid";
		
		var tree = createTreeComponent({
			id: this.pop_tree_id,
	        url: '/itg/base/searchDeptTree.do',
	        filterable: true,
	        expColable: true,
	        params: {},
	        expandLevel: 3,
	        width: 250,
	        header: {
	        	title: getConstText({isArgs : true, m_key : 'res.00034'})
	        },
	        on: {
	        	onItemClick: this.clickTree.bind(this)
	        }
	    });
		
		var searchForm = createFormComp({
			id: this.pop_search_form_id,
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(this) })
					],
    		        css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: [{ 
					item : createUnionFieldComp({
						items: [
							createCodeComboBoxComp({
								label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
								name: 'search_type',
								code_grp_id: 'SEARCH_USER',
								attachChoice: true,
								width: 250
							}),
							createTextFieldComp({
								name: "search_value",
								placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
								on:{
									onKeyPress: this.searchByEnterKey.bind(this)
								}
							})
						]
					})
				}]
			}
		});
		
		var mainGrid = createGridComp({
			id : this.pop_main_grid_id,
			pageable : true,
			pageSize : 10,
			checkbox: true,
			keys: this.key_column,
			url : "/itg/system/user/searchUserList.do",
			resource : "grid.itam.managerBaseList",
			header : {
				title : this.title
			},
			dependency : {
				targetGridId: this.pop_sub_grid_id,
				data: parentRows
			}
		});
		
		var subGrid = createGridComp({
			id : this.pop_sub_grid_id,
			checkbox: false,
			select: false,
			resource : "grid.itam.managerBaseSelList",
			autoLoad : false,
			sortConfig: 'text',
			keys: this.key_column,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns: [{
				column: "DEL_ACTION",
				template: nkia.ui.html.icon("remove", "remove_row"),
				onClick: {
					"remove_row": this.removeSelGrid.bind(this)
				}
			}]
		});
		
		var popupFooter = {
			align: "center",
			items: [
				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(this)})
			]
		};
		
		var body = {
    		rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    header: "부서정보",
                    body: {
                    	rows: tree
                    }
                },{
                    rows: new Array().concat(searchForm, mainGrid)
                },{
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_right", "add_row"),
			 		width: 30,
			 		onClick: {
			 			"add_row": this.addSelGrid.bind(this)
			 		}
                },{
                	width: 340,
                    rows: new Array().concat(subGrid)
                }]
    		}]
    	};
		
		this.window = createWindow({
	    	id: this.pop_window_id,
	    	width: nkia.ui.utils.getWidePopupWidth(),
	    	height: nkia.ui.utils.getWidePopupHeight(),
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	footer: {
	    		buttons: popupFooter
	    	},
	    	closable: true
	    });
		
		$$(this.pop_sub_grid_id)._removeAllRow();
		$$(this.pop_sub_grid_id)._addRow(this.getParentGridStoreData());
		
		$$(this.pop_window_id).show();
		
	};
	
	// 선택되어 있는 부모 그리드 목록 getter
	this.getParentGridStoreData = function() {
		return $$(this.id)._getRows();
	};
	
	// 선택되어 있는 서브 그리드 목록 getter
	this.getSelGridStoreData = function() {
		return $$(this.pop_sub_grid_id)._getRows();
	};
	
	// 서브 그리드 row 추가
	this.addSelGrid = function() {
		var data = $$(this.pop_main_grid_id)._getSelectedRows();
		if(data.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(this.pop_sub_grid_id)._addRow(data);
		
		this.reloadGrid(true);
	};
	
	// 서브 그리드 row 제거
	this.removeSelGrid = function(e, row, html) {
		var data = $$(this.pop_sub_grid_id)._getSelectedRows();
		if(data!=null && data.length > 0) {
			if(data.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '선택된 정보가 없습니다.'
				});
				return false;
			}
			$$(this.pop_sub_grid_id)._removeRow();
		} else {
			$$(this.pop_sub_grid_id)._removeRow(row);
		}
		
		this.reloadGrid(true);
	};
	
	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e){
		var treeItem = $$(this.pop_tree_id).getItem(id);
		
		var form = $$(this.pop_search_form_id);
		var searchParams = form._getValues();
		
		searchParams.cust_id = treeItem.node_id;
		searchParams.selUserList = this.getSelGridStoreData();
		
		$$(this.pop_main_grid_id)._reload(searchParams);
	};
	
	// 값 선택 이벤트
	this.choice = function(){
		var rows = $$(this.pop_sub_grid_id)._getRows();
		$$(this.id)._removeAllRow();
        $$(this.id)._addRow(rows);
        
        this.window.close();
	};
	
	// 검색 이벤트
	this.search = function(){
		var searchParams = $$(this.pop_search_form_id)._getValues();
		var treeParams = $$(this.pop_tree_id).getSelectedItem();

		if(typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}
		
		searchParams.selUserList = this.getSelGridStoreData();
		
		$$(this.pop_main_grid_id)._reload(searchParams);
	};
	
	// 그리드 갱신 이벤트
	this.reloadGrid = function(isGoPage){
		var searchParams = $$(this.pop_search_form_id)._getValues();
		var treeParams = $$(this.pop_tree_id).getSelectedItem();

		if(typeof treeParams != "undefined") {
			searchParams.cust_id = treeParams.node_id;
		}
		
		searchParams.selUserList = this.getSelGridStoreData();
		
		$$(this.pop_main_grid_id)._reload(searchParams, isGoPage);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.pop_search_form_id)._reset();
	};
	
	// 이력 팝업
	this.openHisPop = function(btnId, e) {
		
		var _this = this;
		
		// 검색용 날짜객체 (최근 30일)
		var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
		var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
		
		var historyPopSearchForm = createFormComp({
			id: "historyPopSearchForm",
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
						createBtnComp({ label:"초기화", click: initHistoryPop })
					]
				}
			},
			fields: {
				colSize: 1,
				items: [
				    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
				]
			}
		});
		
		var historyPopGrid = createGridComp({
			id : "historyPopGrid",
			resizeColumn : true,
			select : false,
			params:  {
	        	ASSET_ID : _this.asset_id,
	        	CONF_ID : _this.conf_id,
				entity_mng_type: _this.entity_mng_type,
	        	updDt_startDate : start_date_obj.day,
	        	updDt_endDate : end_date_obj.day,
				updDt_endDate : end_date_obj.day
			},
			url : "/itg/itam/assetHistory/selectOpmsHisOperList.do",
			resource: "grid.itam.manager.his",
			header : {
				title : "수정이력"
			},
			cellMerges: ["INS_DT", "INS_USER_NM"]
		});
		
		nkia.ui.utils.window({
			id:"historyPopup",
			callFunc: function() {
				this.window = createWindow({
			    	id: "historyPopup",
			    	width: 900,
			    	height: nkia.ui.utils.getWidePopupHeight(),
			    	header: {
			    		title: "수정이력"
			    	},
			    	body: {
			    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
			    	},
			    	closable: true
			    })
			}
		});
		
		function searchHistoryPop() {
			var searchParam = $$("historyPopSearchForm")._getValues();
			searchParam["ASSET_ID"] = _this.asset_id;
			searchParam["CONF_ID"] = _this.conf_id;
			searchParam["entity_mng_type"] =  _this.entity_mng_type,
			$$("historyPopGrid")._reload(searchParam);
		}
		
		function initHistoryPop() {
			$$("historyPopSearchForm")._reset();
		}
		
	};
	
};