/********************************************
 * 가상서버 목록 컴포넌트
 * Date: 2019-10-14
 * Version: 1.0 - 최초등록
 * Author: 김태연
 * Description : 가상서버 목록 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.RelVMList = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	
	//  key 컬럼 : 구성 ID
	this.key_column = ["CONF_ID"];
	
	this.getObject = function() {
		
		var _this = this;
		
		var url = '/itg/itam/opms/selectOpmsRelVMList.do';
		
		var grid = createGridComp({
	        id : _this.id,
	        header: {
	            title: _this.title
	        },
	        height: 200,
	        resizeColumn:true,
	        params:  {
	        	conf_id : _this.conf_id
			},
	        url: url,
	        resource: "grid.itam.comp.vm",
	        keys: _this.key_column
	    });
		
		return grid;
	};
	
};