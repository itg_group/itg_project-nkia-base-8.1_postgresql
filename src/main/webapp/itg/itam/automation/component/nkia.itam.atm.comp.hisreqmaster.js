/********************************************
 * Master Process History Tab Component
 * Date: 2018-04-06
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 전체요청이력 탭 컴포넌트 객체 리턴
 ********************************************/
nkia.itam.atm.comp.MasterReqHistory = function(prop) {
	
	this.id = prop.id;
	this.title = prop.title;
	this.asset_id = prop.asset_id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.tangible_asset_yn = prop.tangible_asset_yn;
	this.auth_type = prop.authType;
	this.view_type = prop.view_type;
	this.entity_mng_type = prop.entity_mng_type; //entity_mng_type 설명 : 불러오는 속성의 정보가 '자산'정보인지, '구성'정보인지 담고 있는 값. CLASS_ID에 매핑된 정보면 ASSET, CONF_TYPE에 매핑된 정보면 CONF.
	
	// 검색용 날짜객체 (최근 30일)
	this.start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	this.end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	/********************************************
	 * Function : getObject
	 * 장애요청이력 페이지 요소들을 하나의 객체화 하여 리턴
	 ********************************************/
	this.getObject = function() {
		
		var _this = this;
		
		// 페이지 생성 객체들을 담아줄 변수
		var pageObjects;
		
		var masterHistSearchForm = createFormComp({
			id: "masterHistSearchForm",
			elementsConfig: {
	    		labelPosition: "top"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검색", type: "form", click: this.searchHistoryList.bind(_this)}),
						createBtnComp({ label:"초기화", click: this.clearSearchForm.bind(_this) })
					]
				}
			},
			fields: {
				colSize: 2,
				items: [
				    {colspan: 1, item: createCodeComboBoxComp({label: "요청유형", name: "req_type", code_grp_id: 'REQ_TYPE', attachChoice: true }) },
				    {colspan: 1, item: createSearchDateFieldComp({label:"요청일", name: "req_dt", dateType:"D", start_dateValue : _this.start_date_obj, end_dateValue: _this.end_date_obj})}
				]
			}
		});
		
		var masterHistGrid = createGridComp({
	        id : "masterHistGrid" + "_" + _this.entity_mng_type,
	        header: {
	            title: "요청 이력"
	        },
	        resizeColumn:true,
	        select: false,
	        params:  {
	        	conf_id : _this.conf_id,
	        	req_dt_startDate : _this.start_date_obj.day,
	        	req_dt_endDate : _this.end_date_obj.day
			},
	        url: "/itg/itam/automation/searchProcMasterHist.do",
	        resource: "grid.itam.hist.master",
	        on : {
	        	onItemDblClick: this.openProcPage.bind(_this)
	        }
	    });
		
		pageObjects = new Array().concat(masterHistSearchForm, masterHistGrid);
		
		return pageObjects;
	};
	
	
	
	/********************************************
	 * Function : searchHistoryList
	 * 요청이력 검색 이벤트
	 ********************************************/
	this.searchHistoryList = function() {
		
		var searchParams = $$("masterHistSearchForm")._getValues();
		searchParams["conf_id"] = this.conf_id;
		
		$$("masterHistGrid" + "_" + this.entity_mng_type)._reload(searchParams);
	}
	
	
	
	/********************************************
	 * Function : clearSearchForm
	 * 검색폼 초기화 이벤트
	 ********************************************/
	this.clearSearchForm = function() {
		$$("masterHistSearchForm")._reset();
	}
	
	
	
	/********************************************
	 * Function : openProcPage
	 * 요청이력 그리드 더블클릭 시 요청 상세페이지 조회
	 ********************************************/
	this.openProcPage = function(rowId, event, node) {
		
		var clickRecord = $$("masterHistGrid" + "_" + this.entity_mng_type).getItem(rowId);
		clickRecord["TAB_ID"] = clickRecord["SR_ID"];
		var closable = true;
		
		parent.addProcessTab('/itg/nbpm/provide/goDetailPage.do', clickRecord, closable);
	};
	
};