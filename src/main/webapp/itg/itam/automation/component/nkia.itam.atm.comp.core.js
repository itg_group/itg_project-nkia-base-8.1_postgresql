/********************************************
 * Namespace
 ********************************************/
nkia.itam.atm.comp = {};
nkia.itam.atm.comp.billing = {};
/********************************************
 * createItamAutomationGridComponent
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 컴포넌트의 유형에 따라 분류
 ********************************************/
function createItamAutomationGridComponent(compProperty) {
	
	this.props = compProperty;
	var _this = this;
	
	var objectHandler;
	var component;
	
	try {
		switch(_this.props.component_type) {
		
			case 'FILE' :
				// 첨부파일
				objectHandler = new nkia.itam.atm.comp.AtchFile(_this.props);
				break;
			case 'MANAGER' :
				// 담당자 그리드
				objectHandler = new nkia.itam.atm.comp.ManagerList(_this.props);
				break;
			case 'REL_LOGIC' :
				// 하위 논리서버 그리드
				objectHandler = new nkia.itam.atm.comp.RelLogicList(_this.props);
				break;
			case 'REL_SERVICE' :
				// 연관 서비스 그리드
				objectHandler = new nkia.itam.atm.comp.RelServiceList(_this.props);
				break;
			case 'INSTALL_SW' :
				// 설치소프트웨어 그리드
				objectHandler = new nkia.itam.atm.comp.InstallSwList(_this.props);
				break;
			case 'REL_INFRA' :
				// 연관장비 탭 그리드
				objectHandler = new nkia.itam.atm.comp.RelInfraList(_this.props);
				break;
			case 'INSTALL' :
				// 설치정보 탭 그리드
				objectHandler = new nkia.itam.atm.comp.InstallInfoList(_this.props);
				break;
			case 'BS_INS_MEMORY' :
				// 과금 - 메모리 증설량 그리드
				objectHandler = new nkia.itam.atm.comp.BsInsMemory(_this.props);
				break;
			case 'BS_INS_CPU' :
				// 과금 - CPU Core 증설량 그리드
				objectHandler = new nkia.itam.atm.comp.BsInsCpu(_this.props);
				break;
			case 'BS_INS_STORAGE' :
				// 과금 - 스토리지 증설량 그리드
				objectHandler = new nkia.itam.atm.comp.BsInsStorage(_this.props);
				break;
			case 'BS_SVC_STOP' :
				// 과금 - 서비스 중단일 그리드
				objectHandler = new nkia.itam.atm.comp.BsSvcStop(_this.props);
				break;
			case 'REL_DISK' :
				// 연관 disk 정보
				objectHandler = new nkia.itam.atm.comp.RelDiskList(_this.props);
				break;
			case 'REL_IP' :
				// 연관 IP 정보
				objectHandler = new nkia.itam.atm.comp.RelIPList(_this.props);
				break;
			case 'REL_NIC' :
				// 연관 nic 정보
				objectHandler = new nkia.itam.atm.comp.RelNicList(_this.props);
				break;		
			case 'DUAL_INFO' :
				// 이중화 정보
				objectHandler = new nkia.itam.atm.comp.DualInfo(_this.props);
				break;
			case 'REL_VM' :
				// 가상서버 정보
				objectHandler = new nkia.itam.atm.comp.RelVMList(_this.props);
				break;
			default :
				var errMsg = "[" + _this.props.component_type + "] " + " is not define.";
				console.log(errMsg);
				throw new Error(errMsg);
			
		}
		
		component = objectHandler.getObject();
		
		return component;
		
	} catch(e) {
		alert(e.message);
	}
}

/********************************************
 * createItamAutomationTabComponent
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 탭 자체가 컴포넌트인 요소 모음 (수정이력 등)
 ********************************************/
function createItamAutomationTabComponent(compProperty) {
	
	this.props = compProperty;
	var _this = this;
	
	var tabHandler;
	var tab;
	
	try {
		
		switch(_this.props.component_type) {
		
			case 'UPDATE' :
				// 자산+구성 수정 이력
				tabHandler = new nkia.itam.atm.comp.UpdateHistory(_this.props);
				break;
			case 'ASSET_UPDATE' :
				// 자산 수정 이력
				tabHandler = new nkia.itam.atm.comp.AssetUpdateHistory(_this.props);
				break;	
			case 'CONF_UPDATE' :
				// 구성 수정 이력
				tabHandler = new nkia.itam.atm.comp.ConfUpdateHistory(_this.props);
				break;	
			case 'MASTER_ASSET_REQ' :
				// 전체 요청 이력(자산)
				tabHandler = new nkia.itam.atm.comp.MasterReqAssetHistory(_this.props);
				break;	
			case 'MASTER_CONF_REQ' :
				// 전체 요청 이력(구성)
				tabHandler = new nkia.itam.atm.comp.MasterReqConfHistory(_this.props);
				break;
			case 'PROBLEM_REQ' :
				// 문제요청 이력
				tabHandler = new nkia.itam.atm.comp.ProblemReqHistory(_this.props);
				break;
			case 'INCIDENT_REQ' :
				// 장애요청 이력
				tabHandler = new nkia.itam.atm.comp.IncidentReqHistory(_this.props);
				break;
			case 'SERVICE_REQ' :
				// 서비스요청 이력
				tabHandler = new nkia.itam.atm.comp.ServiceReqHistory(_this.props);
				break;
			case 'CHANGE_REQ' :
				// 변경요청 이력
				tabHandler = new nkia.itam.atm.comp.ChangeReqHistory(_this.props);
				break;
			case 'MAINT_JOB' :
				// 작업기록이력
				tabHandler = new nkia.itam.atm.comp.MaintJobHistory(_this.props);
				break;
			case 'ASSET_BILLING' :
				var pageClassType = _this.props.class_type;
				if(pageClassType == "SL") {
					pageClassType = "SV";
				}
				// 과금정보
				if(_this.props.center_asset_yn == "Y") {
					if(pageClassType == "SV" && _this.props.tangible_asset_yn == "N") {
						tabHandler = new nkia.itam.atm.comp.billing.SV_IN(_this.props);
					} else if(pageClassType == "ST") {
						tabHandler = new nkia.itam.atm.comp.billing.ST_IN(_this.props);
					} else if(pageClassType == "NW") {
						tabHandler = new nkia.itam.atm.comp.billing.NW_IN(_this.props);
					} else if(pageClassType == "LN") {
						tabHandler = new nkia.itam.atm.comp.billing.LN_IN(_this.props);
					}
				} else if(_this.props.center_asset_yn == "N") {
					if(pageClassType == "NW") {
						tabHandler = new nkia.itam.atm.comp.billing.NW_OUT(_this.props);
					} else if(pageClassType == "LN") {
						tabHandler = new nkia.itam.atm.comp.billing.LN_OUT(_this.props);
					}
				}
				break;
			default :
				var errMsg = "[" + _this.props.component_type + "] " + " is not define.";
				console.log(errMsg);
				throw new Error(errMsg);
			
		}
		
		tab = tabHandler.getObject();
		
		return tab;
		
	} catch(e) {
		alert(e.message);
	}
	
}