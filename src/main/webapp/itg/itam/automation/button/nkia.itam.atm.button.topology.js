/********************************************
 * Topology Button Events
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 토폴로지 버튼 클릭 이벤트
 ********************************************/
nkia.itam.atm.button.Topology = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.tangible_asset_yn = prop.tangible_asset_yn;
	
	this.getObject = function(props) {
		
		var _this = this;
		
		var button = createBtnComp({
			visible: true,
			type: "form",
			label: '토폴로지',
			id:_this.id,
			on: {
	        	onItemClick: this.openPop.bind(_this)
	        }
		});
		
		return button;
	};
	
	// 버튼 클릭 이벤트
	this.openPop = function(){
		
		var param = "conf_id=" + this.conf_id 
			+ "&class_type=" + this.class_type
			+ "&tangible_asset_yn=" + this.tangible_asset_yn;
		
		var url = "/itg/itam/topology/goTopology.do?" + param;
		
		window.open(
			getConstValue('CONTEXT') + url,
			"topologyPop",
			"width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no"
		);
	};
	
};