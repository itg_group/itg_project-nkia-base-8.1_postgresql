/********************************************
 * Namespace
 ********************************************/
nkia.itam.atm.button = {};

/********************************************
 * createItamAutomationButton
 * Date: 2016-11-11
 * Version: 1.0 - 최초등록
 * Author: JeongYun,Jeong
 * Description : 버튼의 유형에 따라 분류
 ********************************************/
function createItamAutomationButton(compProperty) {
	
	this.props = compProperty;
	var _this = this;
	
	var objectHandler;
	var button;
	
	try {
		
		switch(_this.props.button_type) {
		
			case 'TOPOLOGY' :
				// 토폴로지 버튼
				objectHandler = new nkia.itam.atm.button.Topology(_this.props);
				break;
			case 'RACK_INFO' :
				// 랙실장도 다운로드 버튼
				objectHandler = new nkia.itam.atm.button.RackInfo(_this.props);
				break;
			default :
				var errMsg = "[" + _this.props.button_type + "] " + " is not define.";
				console.log(errMsg);
				throw new Error(errMsg);
		}
		
		button = objectHandler.getObject();
		
		return button;
		
	} catch(e) {
		alert(e.message);
	}
}
