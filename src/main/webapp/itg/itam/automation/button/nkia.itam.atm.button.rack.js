/********************************************
 * Topology Button Events
 * Date: 2019-12-12
 * Version: 1.0 - 최초등록
 * Author: TYKIM
 * Description : 랙실장도 다운로드 버튼 클릭 이벤트
 ********************************************/
nkia.itam.atm.button.RackInfo = function(prop) {
	
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.conf_id = prop.conf_id;
	this.class_type = prop.class_type;
	this.location_cd_nm = prop.location_cd_nm;
	this.center_position_cd = prop.center_position_cd;
	this.center_position_cd_nm = prop.center_position_cd_nm;
	this.customer_id_nm = prop.customer_id_nm;
	this.tangible_asset_yn = prop.tangible_asset_yn;
	
	this.getObject = function(props) {
		
		var _this = this;
		
		var button = createBtnComp({
			visible: true,
			type: "form",
			label: '랙실장도 다운로드',
			id:_this.id,
			on: {
	        	onItemClick: this.fnExcelDown.bind(_this)
	        }
		});
		
		return button;
	};
	
	/**
	 * 랙실장도 다운로드
	 */
	this.fnExcelDown = function() {
		var _this = this;
		var paramMap = {CENTER_POSITION_CD: _this.center_position_cd};
		
		var headerMap = {
			header: 'STAGE,HOST_NM,STAGE,CLASS_TYPE_NM,CUSTOMER_ID_NM,CONF_NM,CONFIDENTIALITY_CD_NM,INTEGRITY_CD_NM,AVAILABILITY_CD_NM,SECURITY_LEVEL_CD_NM',
			text: '구분,랙 실장도,구분,대분류,고객사,단위업무,C,I,A,보안/자산등급',
			width: '20,100,20,60,100,100,20,20,20,60',
			excelXtype: 'na,rack,na,rack,rack,rack,rack,rack,rack,rack'
		}
		
		var excelParam = {};
		excelParam['fileName'] 			= _this.center_position_cd_nm;
		excelParam['sheetName'] 		= _this.center_position_cd_nm;
		excelParam['titleName'] 		= "[" + _this.center_position_cd_nm + "] - [" + _this.customer_id_nm + "]";
		excelParam['includeColumns'] 	= headerMap.header;
		excelParam['headerNames'] 		= headerMap.text;
		excelParam['defaultColumns'] 	= headerMap.text;
		excelParam['url'] 				= '/itg/itam/opms/rackInfoDownload.do';
		excelParam['groupHeaders']		= [
			{span:'2', text: '랙 실장도'},
			{span:'1', text: '구분'},
			{span:'1', text: '대분류'},
			{span:'1', text: '고객사'},
			{span:'1', text: '단위업무'},
			{span:'3', text: '중요도'},
			{span:'1', text: '보안/자산등급'}
		];
		excelParam['headerWidths']		= headerMap.width;
		excelParam['excelXtype']		= headerMap.excelXtype;
		
		// 그리드에 출력될 컬럼명
		var headerNm = excelParam['defaultColumns'];
		var headerId = excelParam['includeColumns'];
		
		var excelAttrs = {
			sheet : [ {
				sheetName : excelParam['sheetName'],
				titleName : excelParam['titleName'],
				headerWidths : excelParam['headerWidths'],
				headerNames : headerNm,
				includeColumns : headerId,
				groupHeaders : excelParam['groupHeaders'],
				align : excelParam['align'],
				excelXtype : excelParam['excelXtype']
			} ]
		};
		var excelDownParam = {};
		excelDownParam['fileName'] = excelParam["fileName"];
		excelDownParam['excelAttrs'] = excelAttrs;
		
		goExcelDownLoad(excelParam['url'], paramMap, excelDownParam);
	}
	
};