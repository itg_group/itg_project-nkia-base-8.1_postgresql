/********************************************
 * Form
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.itam.DeptTree = function() {
	//this.url = "/itg/system/dept/searchStaticAllDeptTree.do";
	
	//this.url = "/itg/system/customer/searchStaticAllCustomerTree.do";
	this.url = "/itg/itam/statistics/searchCustTree.do";
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		_this.props = props;
		
		var params = ( typeof props.params == "undefined" ) ? {} : props.params;
		params['is_all_tree'] = 'Y';
		
		var tree = createTreeComponent({
			id: props.id + "_tree",
	        url: this.url,
	        filterable: true,
	        params: params,
	        expandLevel: 4,
	        on: {
	        	onItemDblClick: this.choice.bind(_this)
	        }
	    });
		
		var bodyScrollView = {
		        view: "scrollview",
		        body: {
					cols: [{
		                rows: new Array().concat(tree)
		            }]
				}
			};
		
		_this.window = createWindow({
	    	id: props.id,
	    	width : nkia.ui.utils.getWidePopupWidth()/2,
	    	height : nkia.ui.utils.getWidePopupHeight()*3/5	,
	    	header: {
	    		title: props.title
	    	},
	    	body: {
	    		rows: [bodyScrollView] 
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: ( props.closable === undefined ) ? true : props.closable 
	    });
	};
	
	this.choice = function(){
		var treeItem = $$(this.props.id + "_tree").getSelectedItem();
		if(treeItem){
			var reference = this.props.reference;
			var fields = reference.fields;
			
			var values = {};
			values[fields["id"]] = treeItem.node_id;	// 부서 ID
			values[fields["name"]] = treeItem.text;		// 부서 명 

			if(treeItem.text == "조직정보"){
				alert('최상위 노드는 선택할 수 없습니다.');
			}else{
				reference.form._setValues(values);
			}
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 부서정보가 없습니다.'
            });
        }
	};
};

nkia.ui.popup.itam.zoneDetail = function() {

	this.setProps = function(props) {
		for ( var i in props) {
			if (this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};

	this.setRender = function(props) {
		var _this = this;
		_this.props = props;

		var param = props["param"];		
		var editform = createFormComp({
			id : props.id +"_editform",
			header : {
				title : "입력폼",
			},

			fields : {
				colSize : 3, // 열 수
				items : [
						{
							colspan : 1,
							item : createTextFieldComp({
								label : "Zone도메인",
								name : "ZONE_ID_NM",
								readonly: true,
								on : {
									//onKeyPress : "pressEnterKey"
								},
								required : true,
								maxlength : 50,							
								width: 250,
								value : param["ZONE_ID_NM"]
							})
						},
						{
							colspan : 1,
							item : createSearchFieldComp({
								label: '#springMessage("res.title.system.cust")',   //상위요청지역
                           		name  : "customer_cd_nm",  
                           		value : param["CUSTOMER_CD_NM"],
								width: 250,
								width: 250,
								click: function(){								
									nkia.ui.utils.window({
										id: "deptTreeWindow",								
										url: "/itg/system/customer/searchStaticAllCustomerTree.do",
										type: "deptTree",
										title: '#springMessage("res.00061")',   //상위요청지역
										reference: {
											form: $$(props.id +"_editform"),
											fields: {
												id: "customer_id",
												name: "customer_cd_nm"
											}
										},
										callFunc: createDeptWindow
									});
								}					
							})
						},
						{
							colspan : 1,
							item : createTextFieldComp({
								label : "구매사이트",
								name : "BUY_SITE_CN",
								value : param["BUY_SITE_CN"],
								on : {
								//	onKeyPress : "pressEnterKey"
								},
								required : false,
								maxlength : 50,
								width: 250
								
							})
						}, {
							colspan : 1,
							item : createTextFieldComp({
								label : "구매사이트 웹",
								name : "BUY_SITE_URL_CN",
								value : param["BUY_SITE_URL_CN"],
								on : {
								//	onKeyPress : "pressEnterKey"
								},
								required : false,
								maxlength : 50,
								width: 250
								
							})
						}, {
							colspan : 1,
							item : createDateFieldComp({
								label : "구매일",
								name : "BUY_DT",
								value : param["BUY_DT"],
								dateType : 'D',
								required : false,
								readonly: true,
								width: 250
							})
						}, {
							colspan : 1,
							item : createDateFieldComp({
								label : "날짜(년)",
								name : "EXPIRE_DT",
								value : param["EXPIRE_DT"],
								dateType : 'D',
								required : false,
								readonly: true,
								width: 250 
							})
						}, {
							colspan : 1,
							item : createTextFieldComp({
								label : "구매담당자",
								name : "BUY_USER",
								value : param["BUY_USER"],
								width: 250
							})
						}, {
							colspan : 1,
							item : createTextFieldComp({
								label : "관리담당자",
								name : "MGMT_USER",
								value : param["MGMT_USER"],
								required : false,
								width: 250
							})
						}, {
							colspan : 1,
							item : 
							createCodeComboBoxComp({
								label: 'DNS서버',
								name: 'MGMT_PLACE_NM',
								code_grp_id: 'MGMT_PLACE',
								value : param["MGMT_PLACE"],
								attachChoice: true,
								readonly: true,
								width: 250, 
							})
						}, 
						{
							colspan : 2,
							item : createTextAreaFieldComp({
								label : "텍스트박스",
								name : "textarea",
								resizable : true,
								value : param["CUSTOMER_CD"],
								height : 100,
								on : {
									//onKeyPress : "pressEnterKey"
								}
							})
						}
			            
						
						],
				// 히든 필드
				hiddens : {
					
					customer_id : param["CUSTOMER_CD"]
				}
			}
		});

				
		_this.window = createWindow({
			id : props.id,
			
			width : this.width,
			height : this.height,
			header : {
				title : this.title
			},
			body : {
				rows : editform
			},
			footer : {
				buttons : {
					align : "center",
					items : [ createBtnComp({
						label : '적 용',
						type : "form",
						click : this.choice.bind(_this)
					}) ]
				}
			},
			closable : true
		});

	} // end this.setRender
	
	this.choice = function() {

		alert('집가고싶다');
		this.window.close();
	}

} // downExcelFile


function createDeptWindow(popupProps) {
	var props = popupProps||{};
	var dept;
	try {
		
		dept = new nkia.ui.popup.itam.DeptTree();
		dept.setProps(props);
		dept.setRender(props);
	} catch(e) {
		alert(e.message);
	}
}

function createDnsZoneWindow(popupProps) {
	var props = popupProps||{};	
	var dnszone;
	try {
		
		dnszone = new nkia.ui.popup.itam.zoneDetail();
		dnszone.setProps(props);
		dnszone.setRender(props);
	} catch(e) {
		alert(e.message);
	}
}
