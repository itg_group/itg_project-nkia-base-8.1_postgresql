<!-- 
	유지보수관리 > 공급 계약관리 화면 
	@version 1.0
	@author 송성일
	@since 2017. 11. 15
-->
<!DOCTYPE HTML>
<html>
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
</body>
</html>
<script type="text/javascript" src='/itg/system/user/popup/nkia.ui.popup.system.user.js'></script><!-- 사용자 선택 팝업 createUserWindow() -->
<script type="text/javascript" src='/itg/system/vendor/popup/nkia.ui.popup.system.vendor.js'></script><!-- 업체 선택 팝업 createVendorWindow() -->
<script type="text/javascript" src='/itg/itam/project/popup/nkia.ui.popup.system.project.js'></script><!-- 사업 선택 팝업 createProjectWindow() -->
<script type="text/javascript" src='/itg/itam/maint/popup/nkia.ui.popup.itam.maint.js'></script><!-- 유지보수 선택 팝업 createMaintWindow() -->
<script type="text/javascript" src='/itg/itam/statistics/popup/nkia.ui.popup.itam.excel.js'></script><!-- 자산 엑셀다운로드 팝업 createExcelTabWindow() -->
<script type="text/javascript" src='/itg/itam/maint/popup/nkia.ui.popup.itam.selectAsset.js'></script><!-- 자산(멀티선택) 선택 팝업 createSelectAssetWindow() -->
<script type="text/javascript" charset="utf-8">

var searchForm_initData = {}; // 검색 폼
var detail1Form_initData = {}; // 유지보수 요청 폼 

webix.ready(function() {
	console.clear();
	console.log(">>> webix.ready() :: supplyManager.nvf ");
	var fieldItems = [];
	var buttonItems = [];
	var startDateObj = createDateTimeStamp('DHM', {year:-1, month:0, day:+1, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	/**************************
	 * 검색 영역 : [Form]
	 **************************/
	fieldItems = [];
	fieldItems.push(/*공급계약번호*/{ colspan:1, item: createTextFieldComp({label:"#springMessage('res.label.itam.supply.00007')", name:"supply_id", on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);}} }) } );
	fieldItems.push(/*공급계약명*/{ colspan:1, item: createTextFieldComp({label:"#springMessage('res.label.itam.supply.00003')", name:"supply_nm", on:{ onKeyPress: function(keycode) { pressEnterKey(keycode);}} }) } );
	fieldItems.push(/*계약업체명*/{ colspan:1, item: createUnionFieldComp({
	    items: [ createSearchFieldComp({ label:'#springMessage("res.label.itam.supply.00004")', name:'supply_com_nm', click:function(){
						var param = {}; // $$("searchform")._getValues();
						nkia.ui.utils.window({
							id: "vendorSelectPop",
							title:'#springMessage("res.label.itam.supply.00004")' + ' 선택', // 계약업체 선택 
							width: 900,
							popupParamData: param,
							reference:{form:$$('searchForm'), fields:{id:"supply_com_id", name:"supply_com_nm" }},
							callFunc: createVendorWindow // nkia.ui.popup.system.vendor.js
						});
	    			}}),
				createBtnComp({ label:"초기화", click: function() { $$("searchForm")._setValues({ sub_project_id:"", supply_com_nm:"" }); } })
	           ] }) } ); 
	fieldItems.push(/*사업명*/{ colspan:1, item: createUnionFieldComp({
	    items: [ createSearchFieldComp({ label:'#springMessage("res.label.itam.supply.00016")', name:'project_nm', click:function(){
						var param = {}; // $$("searchform")._getValues();
						nkia.ui.utils.window({
							id: "userSelectPop",
							title:'#springMessage("res.label.itam.supply.00017")' + ' 선택', // 계약업체 선택 
							width: 900,
							popupParamData: param,
							reference:{form:$$('searchForm'), fields:{id:"sub_project_id", name:"project_nm" }},
							callFunc: createProjectWindow // nkia.ui.popup.system.project.js 
						});
	    			}}),
				createBtnComp({label:"초기화", click: function() { $$("searchForm")._setValues({ sub_project_id:"", project_nm:"" }); } })
	           ] }) } ); 
	fieldItems.push(/*공급계약방법*/{ colspan:1, item: createCodeComboBoxComp({label:'#springMessage("res.label.itam.supply.00008")', name:'supply_method', code_grp_id: 'SUPPLY_METHOD', attachAll:true }) } );
	fieldItems.push(/*공급계약일*/{colspan:1, item: createSearchDateFieldComp({label:'#springMessage("res.label.itam.supply.00009")', name:"search", dateType:'D', start_dateValue : startDateObj, end_dateValue: endDateObj })} );
	
	buttonItems = [];
	buttonItems.push(/*[검색]*/createBtnComp({ label:"#springMessage('btn.common.search')", type: "form", click: function() { actionLink("searchAttr"); } }) );
	buttonItems.push(/*[초기화]*/createBtnComp({ label:"#springMessage('res.common.reset')", click: function() { actionLink("searchFormInit"); } }) );
	
	var searchForm = createFormComp({
		id: "searchForm",
		elementsConfig: { labelPosition: "left", labelWidth:130 },
		header: {
			title: "#springMessage('res.common.search')",
			icon: "search" 
		},
		footer: {buttons: { align: "center", items: buttonItems, css: "webix_layout_form_bottom" } },
		fields: { colSize: 2, items: fieldItems,
		    hiddens: { // 히든 필드
		    	contractId:"", // 계약 ID
		    	supply_com_id:"", // 계약업체
		    	sub_project_id:"" // 사업ID
		    }
		}
	});
	
	/**************************
	 * 유지보수작업조회 영역 : [Grid]
	 **************************/
	buttonItems = [];
	buttonItems.push(/*[신규등록]*/createBtnComp({ id:"newInputBtn", label:"#springMessage('btn.common.newInsert')", type: "form", width : 120, visible:false, click: function() { actionLink("newInput"); } }) );
	buttonItems.push(/*[엑셀다운로드]*/createBtnComp({ id:"excelBtn", label:"#springMessage('btn.common.exceldown')", type: "form", width : 120, click: function() { actionLink("excelDown"); } }) );
	
	var baseGrid = createGridComp({
		id:"baseGrid",
		keys: ["ASSET_ID"],
		resizeColumn : true,
		pageable : true,
		pageSize : 10,
		height : 200, // height : 200,
		autoLoad :false, // 자동조회 안함
		url :'/itg/itam/supply/searchSupply.do', 
		resource:"grid.itam.supply",
		header: { title: '#springMessage("res.title.itam.supply")', buttons: { items: buttonItems } },
		params : {},
		on:{ onItemClick:gridCellClick , onItemDblClick:gridCellDblClick }
	});	
	
	/**************************
	 * 유지보수요청 영역 : tabpage1 [Form]
	 **************************/
	fieldItems = [];
	fieldItems.push(/*공급계약번호*/{colspan:1, item: createTextFieldComp({label:"#springMessage('res.label.itam.supply.00007')", name:"supply_id", readonly:true }) } );
	fieldItems.push(/*공급계약명*/{colspan:1, item: createTextFieldComp({label:"#springMessage('res.label.itam.supply.00003')", name:"supply_nm", required:true }) } ); // maxLength:200
	fieldItems.push(/*공급계약방법*/{ colspan:1, item: createCodeComboBoxComp({label:'#springMessage("res.label.itam.supply.00008")', name:'supply_method', code_grp_id: 'SUPPLY_METHOD', attachChoice:true, required:true }) } );
	fieldItems.push(/*공급계약종류*/{ colspan:1, item: createCodeComboBoxComp({label:'#springMessage("res.label.itam.supply.00021")', name:'supply_type', code_grp_id: 'SUPPLY_TYPE', attachChoice:true, required:true }) } );
	fieldItems.push(/*공급계약일*/{colspan:1, item: createDateFieldComp({label:'#springMessage("res.label.itam.supply.00009")', name:"supply_start_dt", dateType:'D', required:true })} );
	fieldItems.push(/*계약부서*/{colspan:1, item: createTextFieldComp({label:"#springMessage('res.label.itam.supply.00011')", name:"cust_nm", required:true }) } );
	fieldItems.push(/*계약담당자*/{ colspan:1, item: createUnionFieldComp({
	    items: [ createSearchFieldComp({ label:'#springMessage("res.label.itam.supply.infra.00012")', name:'user_nm', required:true }),
				createBtnComp({ label:"초기화", click: function() { $$("detail1Form")._setValues({ contract_user_id:"", user_nm:"", cust_nm:"" }); } })
	           ] }) } ); 
	 fieldItems.push(/*계약업체명*/{ colspan:1, item: createUnionFieldComp({
	    items: [ createSearchFieldComp({ label:'#springMessage("res.label.itam.supply.00004")', name:'supply_com_nm', required:true }),
				createBtnComp({ label:"초기화", click: function() { $$("detail1Form")._setValues({ sub_project_id:"", supply_com_nm:"" }); } })
	           ] }) } ); 
	fieldItems.push(/*세부사업명*/{ colspan:1, item: createUnionFieldComp({
	    items: [ createSearchFieldComp({ label:'#springMessage("res.label.itam.supply.00016")', name:'project_nm', required:true }),
				createBtnComp({ label:"초기화", click: function() { $$("detail1Form")._setValues({ sub_project_id:"", project_nm:"" }); } })
	           ] }) } ); 
		           
	fieldItems.push(/*구매수량(개)*/{colspan:1, item: createTextFieldComp({label:"#springMessage('res.label.itam.supply.00022')", name:"buy_amount", placeholder:"수량을 입력해주세요.", inputType:"price" }) } ); // defaultZeroVal:false
	fieldItems.push(/*총구매비용(원)*/{colspan:1, item: createTextFieldComp({label:"#springMessage('res.label.itam.supply.00015')", name:"tot_cost", placeholder:"금액을 입력해주세요.", inputType:"price" }) } ); // defaultZeroVal:false
	fieldItems.push(/*_tempColumn1*/{ colspan: 1, item: createTextFieldComp({label:"_tempColumn1",id:"_tempColumn1",name:"_tempColumn1",readonly:true}) } );
	fieldItems.push(/*비고*/{colspan:2, item: createTextAreaFieldComp({label:"#springMessage('res.label.itam.supply.00018')", name:"supply_desc", resizable:true, height:100}) } ); // maxLength:1000
	fieldItems.push(/*첨부파일*/{colspan:2, item: createAtchFileComp({label:"#springMessage('res.label.itam.supply.00019')", id:"atch_file_id", name:"atch_file_id", register: "$!{UserSession.getSesssionUserNm()}", height:100, allowFileExt: ["hwp","pdf","txt","pptx"]}) } );
	
	var detail1Form = createFormComp({
		id: "detail1Form",
		//elementsConfig: { labelPosition: "left"  },
		header: {
			title:'#springMessage("res.label.itam.maint.result.00001")', // 요청정보 
			icon: "form", // 검색폼은 헤더에 아이콘을 넣어줍니다.
		},
		//footer: {buttons: { align: "center", items: buttonItems, css: "webix_layout_form_bottom" } },
		fields: { colSize: 2, items: fieldItems,
		    hiddens: { // 히든 필드
		    	supply_state:"", // 계약체결상태 1:임시저장, 2:계약체결, 3:자산확정
		    	confirm:"", 
		    	contract_user_id:"", // 계약담당자ID
		    	supply_com_id:"", // 계약업체ID
		    	project_id:"", // 사업ID
		    	maint_com_id:"" // 유지보수업체ID
		    }
		}
	});
	
	/**************************
	 * 탭 컴포넌트 생성 : [tabview] 
	 **************************/
	var seq = 0, tabHeadItem = [], tabBodyItem = [];
	tabHeadItem.push({ id:"detail1Tab", value:"#springMessage('res.title.itam.maint.contract')" }); // 유지보수계약관리
	tabBodyItem.push({ id:"detail1Tab", rows:new Array().concat(detail1Form)});
	
	var detail_tab = { view:"tabview", id:"mainTab", css: "webix_layout_form_bottom", animate:false, tabbar:{ options: tabHeadItem, on:{ onAftertabClick:function(){actionLink("mainTab_"+$$("mainTab").getTabbar().getValue());}} }, cells:new Array().concat(tabBodyItem) };
	
	/**************************
	 * 화면 생성
	 **************************/
	buttonItems = [];
	buttonItems.push(/*[계약체결]*/createBtnComp({ id:"stateInsertBtn", label:"#springMessage('btn.itam.00014')", type: "form", click:function(){actionLink("stateInsert");}, visible:false }) );
	buttonItems.push(/*[임시저장]*/createBtnComp({ id:"insertBtn", label:"#springMessage('btn.itam.00002')", type: "form", click:function(){actionLink("insert");}, visible:false }) );
	buttonItems.push(/*[수정]*/createBtnComp({ id:"updateBtn", label:"#springMessage('btn.common.modify')", type: "form", click:function(){actionLink("update");}, visible:false }) );
	buttonItems.push(/*[삭제]*/createBtnComp({ id:"deleteBtn", label:"#springMessage('btn.common.delete')", type: "form", click:function(){actionLink("delete");}, visible:false }) );
	buttonItems.push(/*[취소]*/createBtnComp({ id:"cancelBtn", label:"#springMessage('btn.common.cancel')", type: "form", click:function(){actionLink("cancel");}, visible:false }) );
	
	var view = {
		view: "scrollview", id: "app",
		body: {
			rows: [{ cols: [{ rows: new Array().concat(searchForm, baseGrid, detail_tab) }] },
			createButtons({ align: "center", items: buttonItems}) ]
		}
	};
	nkia.ui.render(view);
	
	/**************************
	 * 초기화
	 **************************/
	if($$("_tempColumn1")) $$("_tempColumn1").$view.style.display = "none"; // 임시컬럼 숨기기 
	if($$("_tempColumn2")) $$("_tempColumn2").$view.style.display = "none"; // 임시컬럼 숨기기 
	if($$("_tempColumn3")) $$("_tempColumn3").$view.style.display = "none"; // 임시컬럼 숨기기 
	
	formInit();
	// Search Form 기본 데이터 입력
	searchForm_initData = {};
	//searchForm_initData["contractId"] = "";
	//searchForm_initData["equipGubun"] = "#springMessage('res.billing.untpc.label.00008')";
	$$("searchForm")._setValues(nkia.ui.utils.copyObj(searchForm_initData));
	
	/**************************
	 * 초기 데이타 백업
	 **************************/
	searchForm_initData = $$("searchForm")._getValues();
	detail1Form_initData = $$("detail1Form")._getValues();
	
	/**************************
	 * 조회 실행
	 **************************/
	pageInit();
	searchList();
});

</script>
<script type="text/javascript" charset="utf-8">

/**************************
 * 주요함수 모음
 **************************/

/**
 * 액션 클릭 
 */
function actionLink(command) {
	//console.log(">>> actionLink() => command="+command+"");
	switch(command) {
		case "mainTab_detail1Tab" : break; // 탭페이지 클릭
		case "mainTab_assetGridTab" : break; // 탭페이지 클릭
		case "searchAttr" : searchList(); break; // [조회]
		case "searchFormInit" : resetSearchFormData(); break; // [초기화]
		//case "excelDown" : fnExcelDown(); break; // [엑셀다운로드 ] 기본형
		case "excelDown" : createExcelTabPop(); break; // [엑셀다운로드 ] 컬럼선택 팝업 형 
		case "newInput" : fn_newInput(); break; // [신규등록] 
		case "gridInput" :  fn_gridInput(); break; // [추가/수정] 
		//case "appReq" : fn_appReq(); break; // [등록] update
		case "stateInsert" : // [계약체결]
			$$("detail1Form")._setValues({supply_state:"2"}); // 계약체결상태 1:임시저장, 2:계약체결, 3:자산확정
			var paramMap = $$("detail1Form")._getValues();
			if("" != nullToSpace(paramMap.supply_id)){
				fn_update(); 
			}else{
				fn_insert();
			}
			break; // [] insert  
		case "insert" : 
			$$("detail1Form")._setValues({supply_state:"1"}); // 계약체결상태 1:임시저장, 2:계약체결, 3:자산확정
			fn_insert(); break; // [등록][임시저정] insert  
		case "update" : fn_update(); break; // [수정] 
		case "delete" : fn_delete(); break; // [삭제] 
		case "cancel" : fn_cancel(); break; // [취소] 
		case "maintHis" : fn_detailHisList(); break; // [수정이력] 
		case "detail1Form_user_nm_click" :
			var param = {}; // $$("searchform")._getValues();
			nkia.ui.utils.window({
				id: "contractUserPop",
				title:'#springMessage("res.00010")' + ' 선택', // 요청자 선택 
				width: 900,
				popupParamData: param,
				reference:{form:$$('detail1Form'), fields:{id:"contract_user_id", name:"user_nm", cust_nm:"cust_nm"}},
				callFunc: createUserWindow // nkia.ui.popup.system.user.js
			});
			break;
		case "detail1Form_supply_com_nm_click" :
			var param = {}; // $$("searchform")._getValues();
			nkia.ui.utils.window({
				id: "vendorSelectPop",
				title:'#springMessage("res.label.itam.supply.00004")' + ' 선택', // 계약업체 선택 
				width: 900,
				popupParamData: param,
				reference:{form:$$('detail1Form'), fields:{id:"supply_com_id", name:"supply_com_nm" }},
				callFunc: createVendorWindow // nkia.ui.popup.system.vendor.js
			});
			break;
		case "detail1Form_project_nm_click" :
			var param = {}; // $$("searchform")._getValues();
			nkia.ui.utils.window({
				id: "userSelectPop",
				title:'#springMessage("res.label.itam.supply.00017")' + ' 선택', // 계약업체 선택 
				width: 900,
				popupParamData: param,
				reference:{form:$$('detail1Form'), fields:{id:"project_id", name:"project_nm" }},
				callFunc: createProjectWindow // nkia.ui.popup.system.project.js 
			});
			break;
		
        default:
        	alert("[개발메시지] actionLink() command="+command+" 유형이 정의되지 않았습니다. ");
        	return;
	}
}

/**
 * 폼 초기화 
 */
function formInit(){
	//$$("searchForm")._hideFields( ["maint_req_type", "contract_state", "or_contract_state"] ); // 요청타입, 요청상태, 요청상태2
	var paramMap = {};
	//paramMap.maint_req_type = '';
	$$("searchForm")._setValues(paramMap);
}

/**
 * 페이지 초기화 
 */
function pageInit(){
	//nkia.ui.utils.hides(['insertBtn', 'cancelBtn','updateBtn','deleteBtn', 'stateInsertBtn']); // [등록][취소][수정][삭제][추가/삭제][수정이력][계약체결]
	//nkia.ui.utils.shows(['newInputBtn']); // [신규]
	// 버튼 Dsiable 로 초기화.
	$$("newInputBtn").hide(); // [신규]
	$$("insertBtn").hide(); // [등록] insert
	$$("updateBtn").hide(); // [수정] update
	$$("deleteBtn").hide(); // [삭제] delete
	$$("cancelBtn").hide(); // [취소]
	$$("stateInsertBtn").hide(); // [계약체결]
	
    // Form 값 초기화 / 컨트롤 초기화 
	$$("detail1Form")._reset();
	$$("detail1Form")._setValues(nkia.ui.utils.copyObj(detail1Form_initData));
	$$("detail1Form")._setControl({editorMode:false, exceptions:[]});
	// 첨부파일 초기화
	nkia.ui.utils.hides(['atch_file_id']);
	$$("atch_file_id")._clear();
	
	$$("newInputBtn").show(); // [신규]
}

/**
 * 검색 폼 초기화
 */
function resetSearchFormData(){
	pageInit();
	$$("searchForm")._reset();
   	$$("atch_file_id")._clear();
	$$("searchForm")._setValues(nkia.ui.utils.copyObj(searchForm_initData));
}

/**
 * 검색 
 */
function searchList(){
	var paramMap = $$("searchForm")._getValues();
	//paramMap.login_user_id = '$!{login_user_id}'; // 로그인 사용자 아이디 
	
	if("" != nullToSpace(paramMap.search_date)){
		paramMap.search_startDate = paramMap.search_date;
		paramMap.search_endDate = paramMap.search_date;
	}
	delete paramMap.search_date;
	
	pageInit();
	$$("baseGrid")._removeAllRow();
	$$("baseGrid")._reload(paramMap);
}

/**
 * 키 이벤트 (검색)
 */
function pressEnterKey(keyCode){
	if(keyCode){
		if(keyCode == "13"){ //enter key
			searchList();
			return false;
		}	
	}
}

/**
 * 그리드 셀 클릭
 **/
function gridCellClick(rowId, event, node){
	var clickRecord = $$("baseGrid").getItem(rowId);
    var selectedItemColumn = rowId.column;
	
	pageInit();
	var rowMap = {};
	for(var key in clickRecord){
		rowMap[key.toLowerCase()] = clickRecord[key]; // 소문자로 복사
	}
	//console.log(">> gridCellClick() :: clickRecord => " + JSON.stringify(clickRecord));
	//console.log(">> gridCellClick() :: rowMap => " + JSON.stringify(rowMap));
	
	if(rowMap["contract_dt_day"]) rowMap["contract_dt"] = rowMap["contract_dt_day"]; // 계약체결일
	if(rowMap["contract_start_dt_day"]) rowMap["contract_start_dt"] = rowMap["contract_start_dt_day"]; // 유지보수시작일
	if(rowMap["contract_end_dt_day"]) rowMap["contract_end_dt"] = rowMap["contract_end_dt_day"]; // 유지보수종료일
	if(rowMap["supply_start_dt_day"]) rowMap["supply_start_dt"] = rowMap["supply_start_dt_day"]; // 공급계약일
	if(rowMap["supply_state_code"]) rowMap["supply_state"] = rowMap["supply_state_code"]; // 계약체결상태 1:임시저장, 2:계약체결, 3:자산확정
    
    // grid selectRow 값을 form에 넣는다. 
	$$("detail1Form")._setValues(nkia.ui.utils.copyObj(rowMap)); 
	
	// [초기화] 비활성화
	var columnList = [];
	var _tempMap = $$("detail1Form")._getValues();
	for(var _tempName in _tempMap){
		columnList.push(_tempName); 
	}
	$$("detail1Form")._setControl({editorMode:true, exceptions:columnList}); 
	nkia.ui.utils.hides(['atch_file_id']);
	
	// 편집가능여부확인
	if("1" == nullToSpace(rowMap.supply_state_code)){ // 계약체결상태 {1:임시저장, 2:체결}
		nkia.ui.utils.shows(['updateBtn','deleteBtn','stateInsertBtn']); // [수정][삭제][계약체결]
		$$("detail1Form")._setControl({editorMode:true, exceptions:['maint_id', 'cust_nm']}); 
		nkia.ui.utils.shows(['atch_file_id']); 
		
		$$("detail1Form").elements["user_nm"].define("click", " actionLink('detail1Form_user_nm_click'); ");
		$$("detail1Form").elements["supply_com_nm"].define("click", " actionLink('detail1Form_supply_com_nm_click'); ");
		$$("detail1Form").elements["project_nm"].define("click", " actionLink('detail1Form_project_nm_click'); ");
	}
	
	// 특정 컬럼 disable
	$$("atch_file_id")._getFileList(clickRecord.FILE_ID); 
}

/**
 * 그리드 셀 더블 클릭
 **/
function gridCellDblClick(rowId, event, node){
	var clickRecord = this.getItem(rowId);
    var selectedItemColumn = rowId.column;
}

/**
 * 트리 노드 클릭
 */
function treeNodeClick(nodeId, e, element) {
	var clickRecord = this.getItem(nodeId);
}

/**
 * 엑셀 다운로드
 */
function fnExcelDown(){
	var paramMap = $$("searchForm")._getValues();
	alert("현재화면에는 적용되지 않습니다. 샘플입니다.");
	return;
	
	var excelParam = {};
	excelParam['fileName'] 			= "#springMessage('res.title.itam.supply')";
	excelParam['sheetName'] 		= "#springMessage('res.title.itam.supply')";
	excelParam['titleName'] 		= "#springMessage('res.title.itam.supply')";
	excelParam['includeColumns'] 	= "#springMessage('excel.itam.supply.colid')";
	excelParam['headerNames'] 		= "#springMessage('excel.itam.supply.colnm')";
	excelParam['defaultColumns'] 	= "#springMessage('excel.itam.supply.colnm')";
	excelParam['url'] 				= '/itg/itam/supply/searchSupplyExcelDown.do';
	
	// 그리드에 출력될 컬럼명
	var headerNm = excelParam['defaultColumns'];
	var headerId = excelParam['includeColumns'];
	
	var excelAttrs = {
		sheet : [ {
			sheetName : excelParam['sheetName'],
			titleName : excelParam['titleName'],
			headerWidths : excelParam['headerWidths'],
			headerNames : headerNm,
			includeColumns : headerId,
			groupHeaders : excelParam['groupHeaders'],
			align : excelParam['align']
		} ]
	};
	var excelDownParam = {};
	excelDownParam['fileName'] = excelParam["fileName"];
	excelDownParam['excelAttrs'] = excelAttrs;
	
	goExcelDownLoad(excelParam['url'], paramMap, excelDownParam);
}

/**
 * 엑셀다운로드 팝업 생성
 */
function createExcelTabPop(){
	var paramMap = $$("searchForm")._getValues();
	var excelParam = {};
	excelParam['fileName'] 			= "#springMessage('res.title.itam.supply')";
	excelParam['sheetName'] 		= "#springMessage('res.title.itam.supply')";
	excelParam['titleName'] 		= "#springMessage('res.title.itam.supply')";
	excelParam['includeColumns'] 	= "#springMessage('excel.itam.supply.colid')";
	excelParam['headerNames'] 		= "#springMessage('excel.itam.supply.colnm')";
	excelParam['defaultColumns'] 	= "#springMessage('excel.itam.supply.colnm')";
	excelParam['url'] 				= '/itg/itam/supply/searchSupplyExcelDown.do';
	nkia.ui.utils.window({id: "assetExcelPop", type: "Dynamic", param: paramMap, excelParam: excelParam, title: "엑셀다운로드", callFunc: createExcelTabWindow }); // OLD goDynamicExcelDownLoad
}

/**************************
 * 처리함수 모음
 **************************/

/**
 * [신규등록]
 */
function fn_newInput(){
	nkia.ui.utils.hides(['newInputBtn','deleteBtn','updateBtn']); // [신규][삭제][수정][수정이력]
	nkia.ui.utils.shows(['insertBtn','cancelBtn', 'stateInsertBtn']); // [등록][취소][계약체결]
	
	$$("detail1Form").elements["user_nm"].define("click", " actionLink('detail1Form_user_nm_click'); ");
	$$("detail1Form").elements["supply_com_nm"].define("click", " actionLink('detail1Form_supply_com_nm_click'); ");
	$$("detail1Form").elements["project_nm"].define("click", " actionLink('detail1Form_project_nm_click'); ");
	
	$$("detail1Form")._reset();
	$$('detail1Form')._setValues(nkia.ui.utils.copyObj(detail1Form_initData));
	
	$$("atch_file_id")._clear();
	nkia.ui.utils.shows(['atch_file_id']);
	
	// 비활성화 : 요청제목, 요청부서, 반려의견
	$$("detail1Form")._setControl({editorMode:true, exceptions:['maint_id', 'cust_nm', 'XXuser_nm','reject_comment']}); 
	//$$('detail1Form')._setValues({contract_state:'2'}); // 기본값:등록
}

/**
 * [등록] insert
 */
function fn_insert(){
	var isValid = false;
	isValid = $$("detail1Form")._validate();
	if(isValid != true){
		return;
	}
	var checkFlag = vaildCheck("insert");
	if(checkFlag != true){
		return;
	}
	
	if(!confirm('#springMessage("msg.common.confirm.00001")')){ // 저장하시겠습니까? 
		return false;
	}
	
	var paramMap = $$("detail1Form")._getValues();
	
	var callbackSub = function(atch_file_id) {
		paramMap['atch_file_id'] = atch_file_id;
 		nkia.ui.utils.ajax({url:'/itg/itam/supply/insertSupply.do', // 공급계약 등록 insert 
			params:paramMap, 
			viewId:'insert',
			isMask:true,
			success:function(data){
				if(data.success){
					pageInit();
					searchList();
					nkia.ui.utils.notification({type:'info', message:data.resultMsg, exprie:2000});
				}else{
					nkia.ui.utils.notification({type:'error', message:data.resultMsg});
				}
			}
		});
	}
	$$("atch_file_id")._send(callbackSub, $$("detail1Form"));
}

/**
 * [수정] update
 */
function fn_update(){
	var isValid = false;
	isValid = $$("detail1Form")._validate();
	if(isValid != true){
		return;
	}
	var checkFlag = vaildCheck("update");
	if(checkFlag != true){
		return;
	}
	
	if(!confirm('#springMessage("msg.common.confirm.00002")')){ // 수정하시겠습니까? 
		return false;
	}
	
	var paramMap = $$("detail1Form")._getValues();
	
	var callbackSub = function(atch_file_id) {
		paramMap['atch_file_id'] = atch_file_id;
		nkia.ui.utils.ajax({url:'/itg/itam/supply/updateSupply.do', // 공급계약 수정 
			params:paramMap, 
			viewId:'update',
			isMask:true,
			success:function(data){
				if(data.success){
					pageInit();
					searchList();
					nkia.ui.utils.notification({type:'info', message:data.resultMsg, exprie:2000});
				}else{
					nkia.ui.utils.notification({type:'error', message:data.resultMsg});
				}
			}
		});
	}
	$$("atch_file_id")._send(callbackSub, $$("detail1Form"));
}

/**
 * [삭제] delete
 */
function fn_delete(){
	var checkFlag = vaildCheck("delete");
	if(checkFlag != true){
		return;
	}
	
	if(!confirm('#springMessage("msg.common.confirm.00003")')){ // 삭제하시겠습니까? 
		return false;
	}
	
	var detailMap = $$("detail1Form")._getValues();
	var paramMap = {supply_id:detailMap.supply_id};
	paramMap.supply_id = detailMap.supply_id;
	paramMap.atch_file_id = detailMap.atch_file_id;
	
	nkia.ui.utils.ajax({url:'/itg/itam/supply/deleteSupply.do', // 공급계약 삭제 
		params:paramMap,
		viewId:'delete',
		isMask:true,
		success:function(data){
			if(data.success){
				pageInit();
				searchList();
				nkia.ui.utils.notification({type:'info', message:data.resultMsg, exprie:2000});
			}else{
				nkia.ui.utils.notification({type:'error', message:data.resultMsg});
			}
		}
	});
}

/**
 * [취소] 상세Form 초기화
 */
function fn_cancel(){
	pageInit();
}

/**************************
 * 사용자 함수 모음
 **************************/

/**
 * 값 유효성 체크 
 */
function vaildCheck(command){
	var checkFlag = true;
	var paramMap = $$("detail1Form")._getValues();
	
	if("update" == command || "delete" == command){
		if("" == nullToSpace(paramMap.supply_id)){ // update/delete시 PK값 존재여부 체크 
			nkia.ui.utils.notification({type:'error', message:"#springMessage('msg.common.00002')"}); // 선택된 내역이 없습니다.
			checkFlag = false;
		}
	}
	
    switch (command) { 
		case "insert": // 요청_[등록]
		case "update": // 요청_[수정]
		case "delete": // 요청_[삭제]
			break;
        default:
			checkFlag = false;
        	alert("[개발메시지] vaildCheck() [" + cmd+"] 유형이 정의되지 않았습니다. ");
        	return;
    }
    
	return checkFlag;
}

/**
 * 자산(구성)정보 상세보기 팝업
 */
function showAssetDetailPop(param){
	//console.log(">>> showAssetDetailPop() :: 자산(구성)정보 상세보기 팝업 ");
	//var url = "/itg/itam/opms/opmsDetailInfo.do" + param;
	var url = "/itg/itam/automation/goAssetDetailViewer.do" + param;
	window.open(getConstValue('CONTEXT') +url, "assetPop", "width=1200,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
}

/**
 * List를 소문자이름으로 복사한다.
 */
function copyListLower(sourceList){
	var targetList = [];
	for (var r=0; r<sourceList.length; r++) {
		var sourceMap = sourceList[r];
		var targetMap = new Object();
		for(var sourceName in sourceMap){
			targetMap[sourceName.toLowerCase()] = sourceMap[sourceName]; // 소문자로 복사
		}
		targetList.push(targetMap);
	}
	return targetList;
}


</script>