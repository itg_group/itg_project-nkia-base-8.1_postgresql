/********************************************
 * User
 * Date: 2016-11-17
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.itam.Maint = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 630;A
	this.url = "/itg/itam/maint/searchMaint.do"; 
	this.resource = "grid.itam.maint.pop2"; 
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	
	// 구성요소 Properties
	this.window = {};
	this.searchForm = [];
	this.grid = [];
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	//elementsConfig: { labelPosition: "left" },
		        colSize: 2,
		        items: [
		            {colspan:1, item : createTextFieldComp({label:getConstText({isArgs : true, m_key : 'res.label.itam.maint.00001'}), name:'search_maint_id', width: 300, on:{ onKeyPress: this.doKeyPress.bind(_this) }})},
					{colspan:1, item : createTextFieldComp({label:getConstText({isArgs : true, m_key : 'res.label.itam.maint.00002'}), name:'search_maint_nm', width: 300, on:{ onKeyPress: this.doKeyPress.bind(_this) }})} 
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = getConstText({isArgs : true, m_key : 'res.00056'}); // 유지보수 목록
		
		//if(this.type == "process"){
		//	title = "담당자 목록";
		//}
		
		if(this.type == "multitask"){
			this.on = {};
		}else{
			this.on = {
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["MAINT_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
			return false;
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		$$(this.id + '_grid')._reload(form._getValues());
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			var reference = this.reference;
			var fields = reference.fields;
			//console.log(">>> nkia.ui.popup.itam.maint.js - choice() :: gridItem=", gridItem);
			
			var values = {};
			if(fields["id"]) values[fields["id"]] = nkia.ui.utils.check.nullValue(gridItem.MAINT_ID); // 유지보수ID
			if(fields["name"]) values[fields["name"]] = nkia.ui.utils.check.nullValue(gridItem.MAINT_NM); // 유지보수명
			if(fields["maint_com_id"]) values[fields["maint_com_id"]] = nkia.ui.utils.check.nullValue(gridItem.MAINT_COM_ID); // 유지보수 업체아이디
			if(fields["maint_com_nm"]) values[fields["maint_com_nm"]] = nkia.ui.utils.check.nullValue(gridItem.MAINT_COM_NM); // 유지보수 업체명
			if(fields["tech_user_nm"]) values[fields["tech_user_nm"]] = nkia.ui.utils.check.nullValue(gridItem.TECH_USER_NM); // 유지보수 담당자명 
			if(fields["tech_user_tel"]) values[fields["tech_user_tel"]] = nkia.ui.utils.check.nullValue(gridItem.TECH_USER_TEL); // 유지보수담당자 연락처
			if(fields["tech_user_email"]) values[fields["tech_user_email"]] = nkia.ui.utils.check.nullValue(gridItem.TECH_USER_EMAIL); // 유지보수 담당자 이메일
			
			// 유지보수 상세 정보
			//if(values[fields["user_detail"]]){
			//	var detailInfo = nkia.ui.utils.check.nullValue(gridItem.CUST_NM)
			//		+ "(tel:"
			//		+ nkia.ui.utils.check.nullValue(gridItem.TEL_NO)
			//		+ "/email:"
			//		+ nkia.ui.utils.check.nullValue(gridItem.EMAIL) + ")";
			//	values[fields["user_detail"]] = detailInfo;	// 유지보수 상세 
			//}
			
            reference.form._setValues(values);
             
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 정보가 없습니다.'
            });
        }
	};
};

/**
 * 유지보수 팝업
 * @param {} popupProps
 */
function createMaintWindow(popupProps) {
	var props = popupProps||{};
	var user;
	try {
		switch (props.type) {
			case "___type":
				//user = new nkia.ui.popup.XXX.XXXXX();
				//break;	
			default:
				user = new nkia.ui.popup.itam.Maint();
		}
		user.setProps(props);
		user.setRender(props);
		user.setWindow();
	} catch(e) {
		alert(e.message);
	}
}
