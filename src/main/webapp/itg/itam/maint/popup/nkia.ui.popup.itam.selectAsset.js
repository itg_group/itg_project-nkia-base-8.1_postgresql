/********************************************
 * Asset
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회
 ********************************************/
nkia.ui.popup.itam.SelectAsset = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	//this.url = "/itg/itam/oper/searchAssetList.do"; // 자산 목록
	this.url = "/itg/itam/maintReq/searchMaintReqAsset.do"; // 유지보수계약 자산목록
	this.resource = "grid.itam.maint.req.pop";
	//url: "/itg/itam/maint/searchAsset.do", // 자산 목록
	//resource: "grid.itam.maint.asset", 
	this.gridParams = {};
	this.gridHeight = 300;
	this.treeParams = {};
	this.checkbox = false;
	this.expandLevel = 3;
	this.reference = {};
	this.keys = ["CONF_ID"];
	this.dependency = {};
	this.type = "itam";
	this.bindEvent = null;
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	
	//console.log(">>> createSelectAssetWindow > this=", this);
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_classTree',
	        url: '/itg/itam/amdb/searchAmClassAllTree.do',
	        header: {
	            title: getConstText({isArgs : true, m_key : 'res.title.itam.class'})
	        },
	        expColable: true,
	        filterable: true,
	        width: 200,
	        params: this.treeParams,
	        expandLevel: 3,
	        on: {
	        	onItemClick: this.doTreeClick.bind(_this)
	        }
	    });
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            { item:createUnionFieldComp({
                            items: [
                                createCodeComboBoxComp({
                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}), // 조회분류
                    				name: 'search_type',
                    				code_grp_id: 'SEARCH_ASSET',
                    				attachChoice: true,
                    				width: 250
                    			}),
                    			createTextFieldComp({
                    				name: "search_value",
                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}), // 조회값 
                    				on:{
       									onKeyPress: this.doKeyPress.bind(_this)
     								}	
                    			})
                            ]
                        })
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}), // 조회 
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)}) // 초기화 
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = getConstText({isArgs : true, m_key : 'res.title.itam.maint.assetlist'}); // 자산목록
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			height : 150,
		    resource: this.resource,
		    params: this.gridParams,
			url: this.url,
			select: false,
			pageable: true,
			checkbox: this.checkbox,
			dependency: this.dependency,
			keys: this.keys,
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: this.tree
	                },{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트 - 검색폼 초기화
	this.doTreeClick = function(id, e){
		$$(this.id + '_searchForm')._reset();
		this.doSearch(id);  
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
			return false;
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(treeId){
		var form = $$(this.id + '_searchForm');
		var values = form._getValues();
		
		// Tree 데이터
		var treeItem;
		if(treeId !== undefined){
			treeItem = $$(this.id + '_classTree').getItem(treeId);
		}else{
			treeItem = $$(this.id + '_classTree')._getSelectedItem();
		}
		
		if(treeItem){
			values.class_id = treeItem.node_id;
		}
		$$(this.id + '_grid')._reload(values);
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var assetRows = $$(this.id + "_grid")._getSelectedRows();
		if(assetRows.length > 0){
			var reference = this.reference;
            reference.grid._addRow(assetRows);
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 자산이 없습니다.'
            });
        }
	};
};


/********************************************
 * AssetAndService
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회 + 서비스 선택 팝업(프로세스에서 사용)
 ********************************************/
nkia.ui.popup.itam.SelectAsset01 = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.reference = {};
	this.actionColumns = [];
	
	// 구성요소 Properties
	this.asset = {};
	this.selectedAssetGrid = {};
	this.serviceGrid = {};
	this.helpContent = {};
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		props.bindEvent = this;
		props.checkbox = true;
		props.dependency = {
			data: this.reference.data.asset,
			targetGridId: props.id + '_selectedAssetGrid'
		}
		this.asset = new nkia.ui.popup.itam.SelectAsset();
		this.asset.setProps(props);
		this.asset.setRender(props);
		
		this.selectedAssetGrid = createGridComp({
			id: props.id + '_selectedAssetGrid',
			header: {
				title: "선택된 자산"
			},
			height : 150,
		    resource: props["target_resource"], // grid.itam.maint.req.popSet 계약자산  / grid.itam.maint.setAsset 자산
			select: false,
			pageable: false,
			autoLoad: false,
			keys: ["CONF_ID"],
			actionColumns: [{
				column: "ACTION",
				template: function(data){
					return nkia.ui.html.icon("remove", "delete_row");
				},
				onClick: {
					"delete_row": function(e, row, html){
						this._removeRow(row);
					}
				}
			}]
		});
		
		this.helpContent = {
 			view: "template",
 			template: "<font style='color:red'><b>*&nbsp;하단 연관 서비스 목록에서 선택(체크)하시면<br/>&nbsp;&nbsp;서비스 조회에도 적용이 됩니다.</b></font>",
 			height: 46
 		}
	};
	
	this.setWindow = function(){
		var _this = this;
		
		var popupFooter = createButtons({
			align: "center",
			items: [
				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
			]
		});
		
		var split = {
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_down", "add_asset"),
			 		height: 30,
			 		onClick: {
			 			"add_asset": function(e, row, html){
			 				var data = $$(_this.id + '_grid')._getSelectedRows();
			 				if(data.length > 0){
			 					$$(_this.id + '_selectedAssetGrid')._addRow(data);
			 				}
			 			}
			 		}
                };
		
		var body = {
			rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    header: "분류체계정보",
                    body: {
                    	rows: this.asset.tree
                    }
                },{
                    //rows: new Array().concat(this.asset.searchForm, this.asset.grid, split, this.selectedAssetGrid, this.helpContent, popupFooter)
					rows: new Array().concat(this.asset.searchForm, this.asset.grid, split, this.selectedAssetGrid, popupFooter)
                }]
    		}]
		}
		
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	closable: this.closable 
	    });
	    
	    this.bindData();
	}
	
	this.bindData = function(){
		// 선택된 구성 목록
		$$(this.id + '_selectedAssetGrid')._removeAllRow(); // 초기화 
	    if(this.reference.data.asset.length > 0){
	    	$$(this.id + '_selectedAssetGrid')._addRow(this.reference.data.asset);
	    }
	}
	
	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e){
		var treeItem = $$(this.id + '_chargerGrpTree').getItem(id);
		
		var params = {
			grp_id: treeItem.node_id
		}
		
		$$(this.id + '_chargerUserGrid')._reload(params);
		$$(this.id + '_chargerDeptGrid')._reload(params);
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var assetRows = $$(this.id + "_selectedAssetGrid")._getRows();
		if(assetRows.length > 0){
			var reference = this.reference;
			reference.grid.asset._removeAllRow();
            reference.grid.asset._addRow(assetRows);
            
            if(reference.grid.service){
            	var serviceRows = $$(this.id + "_serviceGrid")._getSelectedRows();
            	reference.grid.service._removeAllRow();
            	if(serviceRows.length > 0){
	            	reference.grid.service._addRow(serviceRows);
            	}
            	reference.grid.service._setBadgeCount();
            }
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 자산이 없습니다.'
            });
        }
	};
};

/**
 * 자산 팝업
 * @param {} popupProps
 */
function createSelectAssetWindow(popupProps) {
	var props = popupProps||{};
	//console.log(">>> createSelectAssetWindow > props=", props);
	var asset;
	try {
		switch (props.type) {
			case "AssetMulti":
				asset = new nkia.ui.popup.itam.SelectAsset01();
				break;
			default:
				asset = new nkia.ui.popup.itam.SelectAsset01();
		}
		asset.setProps(props);
		asset.setRender(props);
		asset.setWindow();
	} catch(e) {
		alert(e.message);
	}
}



