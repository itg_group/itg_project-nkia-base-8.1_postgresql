<!doctype html>
<html lang="ko">
<!--
*//**
 * 논리서버 목록 페이지
 * 2013.8.29
 * @author <a href="mailto:eungyu@nkia.co.kr"> Eungyu Ko
 * 
 *//*
-->
<head>
#parse("/itg/base/include/page_meta.nvf")
<style type="text/css">
<!--
	html {overflow-x: auto; overflow-y: scroll; }
//-->
</style>
<!-- ITG Base Page parse -->
<!-- ITG Base CSS parse -->
#parse("/itg/base/include/page_base.nvf")
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_extjs_inc.nvf")
<script type="text/javascript" src='$!{context}/itg/base/js/itam/opms/opmsPopup.js?_ct=$!{cachedate}'></script>
<script>
var type = 'LOG';
var class_id = '0';
var level = '1';
var class_mng_type = "$!{class_mng_type}";
var target = '전체 (논리서버)';
var defaultGridPagingSize = 100;
Ext.onReady(function() {
	
	Ext.EventManager.onWindowResize(function() {
		callResizeLayout();
	}, this);
	
	var searchBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.search")', id:'searchBtn', ui:'correct'});
	var initBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.reset")', id:'initBtn'});
	var excelBtn = createBtnComp({visible: true, label: '#springMessage("btn.common.exceldown")', id:'excelBtn', icon :'${context}/itg/base/images/ext-js/common/icons/excel-icon.png'});
	var deleteLogicBtn = createBtnComp({visible: true, label: '서버 삭제', id:'deleteLogicBtn', icon :'${context}/itg/base/images/ext-js/common/icons/delete.png'});
	var changePhysiBtn = createBtnComp({visible: true, label: '물리서버 변경', id:'changePhysiBtn', icon :'${context}/itg/base/images/ext-js/common/icons/drive_disk.png'});
	var registMaintJobBtn = createBtnComp({visible: true, label: '작업기록 등록', id:'registMaintJobBtn', icon :'${context}/itg/base/images/ext-js/common/icons/gray-component.gif'});
	
	attachBtnEvent(searchBtn, actionLink,  'searchAttr');
	attachBtnEvent(initBtn, actionLink,  'searchFormInit');
	attachBtnEvent(excelBtn, actionLink,  'excelDown');
	attachBtnEvent(deleteLogicBtn, actionLink,  'deleteLogic');
	attachBtnEvent(changePhysiBtn, actionLink,  'changePhysi');
	attachBtnEvent(registMaintJobBtn, actionLink,  'registMaintJob');

	// 시작 종료 날짜 셋팅
	var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var endDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});	
	
	var setSearchFormProp = {
			id: 'searchform',
			title: '#springMessage("res.common.search")',			
			columnSize: 4,
			formBtns: [searchBtn,initBtn],
			collapsible: true
	}
	var searchFormPanel = createSeachFormComp(setSearchFormProp);	
	
	makeSearchForm(type, level, class_id);

	//분류체계 트리
	var setClassTree = {
		id : 'classTree',
		url : '/itg/itam/statistics/searchClassTree.do',
		title : '#springMessage("res.label.class.00001")',
		dynamicFlag : false,
		searchable : true,
		searchableTextWidth : 60,
		expandAllBtn : true,
		collapseAllBtn : true, 
		//extraToolbar : multiCombo,
		params: {class_mng_type: class_mng_type},
		rnode_text : '#springMessage("res.00003")',
		expandLevel: 3
	}
	var classTree = createTreeComponent(setClassTree);
	attachCustomEvent('select', classTree, treeNodeClick);
	
	var setAssetList = {
		id: 'assetList',
		title: '논리서버 목록',
		gridHeight: 300,
		resource_prefix: 'grid.itam.statistics.'+class_mng_type+type,
		url: '/itg/itam/statistics/searchAssetList.do',
		params: {class_type: type, class_mng_type: class_mng_type, asset_state : 'ACTIVE', is_logic_statistics:'Y', tangible_asset_yn: 'N'},
		toolBarComp: [registMaintJobBtn, excelBtn],
		pagingBar : true,
		pageSize : defaultGridPagingSize,
		selfScroll : true,
		border : true,
		autoLoad: true,
		nestedGrid : false,
		selModel: true,
		timeout: 600000
	};
	var assetList = createGridComp(setAssetList);
	
	attachCustomEvent('celldblclick', assetList, gridDblCellClick);
	
	// Viewport 설정
	var viewportProperty = {
		west: { 
			minWidth: 170, 
			maxWidth: 250, 
			items : [classTree] 
		},			
		center: { 
			items : [searchFormPanel, assetList]
		}
	}
	
	searchFormPanel.setFieldValue("search_group_nm", ["CH", "FP","MN","NT","UN"]);
	
	// Viewport 생성
	createBorderViewPortComp(viewportProperty, { isLoading: true, isResize : true, resizeFunc: "callResizeLayout"} );
	
	Ext.getCmp('asset_state').value = 'ACTIVE';
	
	// originalHeight 세팅
	setOrignalHeightPanel(assetList, assetList.getHeight());
	
	Ext.getCmp("searchform").on("collapse", function() {
		callResizeLayout();
	});
	
	Ext.getCmp("searchform").on("expand", function() {
		assetList.setHeight(assetList.originalHeight);
		callResizeLayout();
	});
})

//Layout Resise Event Handler
function callResizeLayout(){
	resizeBorderLayout("viewportPanel", "borderPanel", ["searchform"], "assetList");
}

function treeNodeClick( tree, record, index, eOpts ) {
	class_id = record.raw.node_id;
	//type = record.raw.class_type;
	level = record.raw.node_level;
	
	target = record.raw.text;
	pnode = record;
	if(record.raw.node_level < 2){
		target = "전체";
	} else {
		for(i=0; i < level - 1; i++){
			pnode = pnode.parentNode;
			target = pnode.raw.text + " > " + target;
		}
	}
	target = target+" (논리서버)";
	
	makeAssetGrid(type, class_id);
	
	var theForm = Ext.getCmp('searchform');
	theForm.initData();
	theForm.setFieldValue('search_target',target);
	theForm.setFieldValue('asset_state','ACTIVE');
}

function actionLink(command) {

	switch(command) {
		case 'searchAttr' :
			var searchForm = Ext.getCmp('searchform');
			var paramMap = searchForm.getInputData();
			paramMap['class_type'] = type;
			paramMap['class_id'] = class_id;
			paramMap['class_mng_type'] = class_mng_type;
			paramMap['asset_cust_id'] = paramMap['ASSET_CUST_ID'];			
			paramMap['vendor_id'] = paramMap['VENDOR_ID'];
			paramMap['tangible_asset_yn'] = 'N';
			paramMap['is_logic_statistics'] = 'Y';
				
			Ext.getCmp('assetList').searchList(paramMap);
		break;
		
		case 'searchFormInit' :
			
			var treeObj = Ext.getCmp('classTree').view.selModel.getSelection();
			var targetTxt = "전체";
			
			if( typeof treeObj != "undefined" && treeObj.length > 0 ) {
				var selTreeItem = treeObj[0];
				var pnodeTxt = selTreeItem;
				targetTxt = selTreeItem.raw.text;
				if(selTreeItem.raw.node_level > 0){
					for(i=0; i < level - 1; i++){
						pnodeTxt = pnodeTxt.parentNode;
						targetTxt = pnodeTxt.raw.text + " > " + targetTxt;
					}
				}
			}
			
			if(targetTxt == "서버") {
				targetTxt = "전체";
			}
			targetTxt = targetTxt + " (논리서버)";
			var theForm = Ext.getCmp('searchform');
			
			theForm.initData();
			theForm.setFieldValue('search_target', targetTxt);
			theForm.setFieldValue('asset_state','ACTIVE');
			
		break;
		
		case 'excelDown' :
			var setPopUpProp = {
					title: 'Excel',
		        	id: 'Excel',
		        	buttonAlign : 'center',
		        	//bodyPadding: '5 5 5 5 ',
		        	//pro_auth_id_map: pro_auth_id,
		        	bodyStyle: 'background-color: white; ',
		        	//emptyMsg: "#springMessage('msg.system.result.00010')",
		        	border: false
		   	}
			//화면처리
			var excelPopUp = createExcelPopUp(setPopUpProp);
			excelPopUp.show();
		break;		
		
		case 'deleteLogic' :
			
			var searchForm = Ext.getCmp('searchform');
			var logicGrid = Ext.getCmp("assetList");
			var logicGridSelItem = logicGrid.getSelectionModel().selected.items;
			
			if(logicGridSelItem.length < 1) {
				alert("삭제할 논리서버를 선택해주세요.");
				return;
			}
			
			setViewPortMaskTrue();
			
			if(!confirm("삭제할 논리서버의 연관 정보도 모두 삭제됩니다.\n정말 삭제하시겠습니까?")){
				setViewPortMaskFalse();
				return false;
			}
			
			var paramMap = {};
			var logicConfIdArray = new Array();
			
			for(var i=0; i<logicGridSelItem.length; i++) {
				var confId = logicGridSelItem[i].raw.CONF_ID;
				logicConfIdArray.push(confId);
			}
			
			paramMap["logicConfIdArray"] = logicConfIdArray;
			
			jq.ajax({ type:"POST"
				, url:"/itg/itam/logic/deleteLogicServer.do"
				, contentType: "application/json"
				, dataType: "json"
				, async: false
				, data : getArrayToJson(paramMap)
				, success:function(data){
					if( data.success ){
						alert("삭제가 완료되었습니다.");
						
						// 로직 처리 후 목록 화면 갱신
						var searchParam = searchForm.getInputData();
						searchParam['class_type'] = type;
						searchParam['class_id'] = class_id;
						searchParam['class_mng_type'] = class_mng_type;
						searchParam['asset_cust_id'] = searchParam['ASSET_CUST_ID'];			
						searchParam['vendor_id'] = searchParam['VENDOR_ID'];
						searchParam['tangible_asset_yn'] = 'N';
						searchParam['is_logic_statistics'] = 'Y';
						
						logicGrid.searchList(searchParam);
						
						setViewPortMaskFalse();
					}else{
						setViewPortMaskFalse();
						showMessage(data.resultMsg);
					}
				}
			});
			
		break;
		
		case 'changePhysi' :
			
			var searchForm = Ext.getCmp('searchform');
			var logicGrid = Ext.getCmp("assetList");
			var logicGridSelItem = logicGrid.getSelectionModel().selected.items;
			
			if(logicGridSelItem.length < 1) {
				alert("정보를 변경할 논리서버를 선택해주세요.");
				return false;
			}
			
			var paramMap = {};
			var logicConfIdArray = new Array();
			
			for(var i=0; i<logicGridSelItem.length; i++) {
				var confId = logicGridSelItem[i].raw.CONF_ID;
				logicConfIdArray.push(confId);
			}
			
			var popParam = {
				searchForm : searchForm
				, grid : logicGrid
				, class_id : class_id
				, class_type : type
				, logicConfIdArray : logicConfIdArray
			}
			
			// itg/base/js/itam/opms/opmsPopup.js
			var physiPop = createPhysiServerListPop(popParam);
			physiPop.show();

		break;
		
		case 'registMaintJob' :
			
			var selectedList = Ext.getCmp('assetList').view.selModel.getSelection();
			var assetList = new Array();
			
			if(selectedList.length > 0) {
				
				var thatUserId = '$!{userVO.user_id}';
				var thatUserNm = '$!{userVO.user_nm}';
				
				for(var i = 0 ; i < selectedList.length ; i++) {
					
					var rowData = selectedList[i];
					
					assetList.push({
						ASSET_ID : rowData.raw.ASSET_ID
						, CONF_ID : rowData.raw.CONF_ID
					});
				}
				
				var maintJobRegistPop = createMaintJobRegistPop(assetList, thatUserId, thatUserNm);
				maintJobRegistPop.show();
				
			} else {
				alert("선택된 논리서버가 없습니다.");
				return;
			}
			
		break;
	}
}

function makeSearchForm(type, level, class_id){
	
	var searchForm = Ext.getCmp('searchform');
	searchForm.removeAll(true);
	searchForm.setTitle('검색');
	
	var searchTarget = {colspan:2, tdHeight: 30, item: createTextFieldComp({label:'조회대상', name:'search_target', id:'searchT', readOnly:true})};
	var searchContainer	= {colspan:2, tdHeight: 30, item: createUnionCodeComboBoxTextContainer({label : '조회구분', comboboxName: 'search_type', textFieldName: 'search_value', code_grp_id: 'SEARCH_SV_STATISTICS', widthFactor:0.5, fixedLayout: true, fixedWidth: 120})};
	var searchDate = {colspan:2, tdHeight: 30, item : createUnionCodeComboBoxDateContainer({label:'일자구분', comboboxName: 'search_date_type', dateFieldName: 'search', code_grp_id: 'SEARCH_ASSET_STATISTICS_DATE', widthFactor: 0.5, dateType:'D', isFromToDate: true })};
	var asset_state = {colspan:1, item : createCodeComboBoxComp({label:'자산상태', name:'asset_state', code_grp_id:'ASSET_STATE', attachAll:true, id:'asset_state'})};

	itemArray = [searchTarget, searchContainer, searchDate, asset_state];
	var searchItem = parseTdItem(itemArray);
	
	var form = {
		id: "searchForm",
		xtype:'fieldset',
        layout: {
            type: 'table',
            columns: 2
        },
        items: searchItem
	};
	searchForm.add(form);
	searchForm.setFieldValue('search_target',target);

	callResizeLayout();
}

function comboLocCodeSelect(){
	var sel_up_code = Ext.getCmp('region').value;
	var sub_code_cmp = Ext.getCmp('bd_code');
	sub_code_cmp.store.proxy.jsonData['region_bd'] = sel_up_code; 
	sub_code_cmp.store.load();
	sub_code_cmp.setValue("");
}

function comboBdCodeSelect(){
	var sel_up_code = Ext.getCmp('bd_code').value;
	var sub_code_cmp = Ext.getCmp('loc_code');
	sub_code_cmp.store.proxy.jsonData['bd_loc'] = sel_up_code; 
	sub_code_cmp.store.load();
	sub_code_cmp.setValue("");
}

function makeAssetGrid(type, id){
	var paramMap = {class_id : id, class_type : type, class_mng_type : class_mng_type, asset_state : 'ACTIVE', tangible_asset_yn:'N', is_logic_statistics:'Y'};
	Ext.getCmp('assetList').searchList(paramMap);
}


function contractComboBox(id, codeType, label, name){
	var param = {};
		param['code_type'] = codeType;
		
	var contract_proxy = Ext.create('nkia.custom.JsonProxy', {
		defaultPostHeader : "application/json",
		noCache : false,
		type: 'ajax',
		url: '/itg/itam/statistics/searchCodeDataList.do',
		jsonData :param,
		actionMethods: {
			create: 'POST',
			read: 'POST',
			update: 'POST'
		},
		reader: {
			type: 'json',
			root: 'gridVO.rows'
		}
	});
	
	var contractProp_val = {label: label, id: id, name: name, width: 250, attachAll:true};
		contractProp_val['proxy'] = contract_proxy;
		contractProp_val['valueField'] = 'CODE_ID';
	    contractProp_val['displayField'] = 'CODE_TEXT';
	    
	   var contractComboBox =  createComboBoxComp(contractProp_val);
	   
	   return contractComboBox;
}

function makeExcelTab(type){

	var id = 'excelGrid';
	var itemArray 	= [];
	var gridCompArr= [];
	
	itemArray.push({CODE_ID: 'SV', CODE_NM: "논리서버", SV_TYPE:'LOG'});
	
	for(i=0;i<itemArray.length;i++){

		var itemCodeId 	= itemArray[i].CODE_ID;
		var itemCodeNm 	= itemArray[i].CODE_NM;
		var tabId = '';
		
		var paramMap = {};
		paramMap['class_type'] = itemCodeId;
		paramMap['sv_type'] = itemArray[i].SV_TYPE;
		tabId = itemArray[i].SV_TYPE;
			
		var excelAttrList = {
			id: tabId,
			title: itemCodeNm,
			gridHeight: 250,
			resource_prefix: 'grid.itam.statistics.excelDownPop',
			url: '/itg/itam/statistics/searchClassTypeAttrList.do',
			params: paramMap,
			autoLoad: true,
			multiSelect	: true,
			selModel	: true,
			loadTitleCount : false
		};
		var assetList = createGridComp(excelAttrList);
		
		gridCompArr.push(assetList);
		
	}
	
	//탭생성
	var tabBasicProperty = {
		tab_id: "Tab",
		width	: '100%',
		layout: 'center',
		cls: 'dualTab',
		item 	: gridCompArr
    }
	tabComp = createTabComponent(tabBasicProperty);
	
	return tabComp;
}


function createExcelPopUp(setPopUpProp){
	var storeParam = {};
	var that = this;
	var inputTablePop = null;
	
	for( var prop in setPopUpProp ){
		that[prop] = setPopUpProp[prop];
	}

	if(popUp_comp[that['id']] == null) {
		
		var insertBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.apply' }),
			ui:'correct',
			scale:'medium',
	        handler: function() {
				makeExcelData();
	        }
		});
		
		var closeBtn = new Ext.button.Button({
			text: getConstText({ isArgs: true, m_key: 'btn.common.close' }),
			scale:'medium',
			handler: function() {
				inputTablePop.close();
			}
		});
		
		var excelTab = makeExcelTab(type);
		
		inputTablePop = Ext.create('nkia.custom.windowPop',{

			id: that['id'],
			title: 'Excel', 
			width: 700,
			autoDestroy: false,
			resizable:false, 
			buttonAlign: that['buttonAlign'],
			buttons: [insertBtn, closeBtn],
			bodyStyle: that['bodyStyle'],
			items: [excelTab],
			listeners: {
				destroy:function(p) {
					popUp_comp[that['id']] = null
	
	 			},
	 			hide:function(p) {
	 			}
	 		}
		});
		
		popUp_comp[that['id']] = inputTablePop;
		
	} else {
		inputTablePop = popUp_comp[that['id']];
	}
	
	return inputTablePop;
}

function makeExcelData(){
	var sheet = [];
	var tabArray = Ext.getCmp("Tab").items.items;
	var classTypeArr = [];
	
	for(var i = 0; i < tabArray.length; i++){
		var gridData = getGrid(tabArray[i].id).view.selModel.getSelection();
		var type = tabArray[i].id;
		var title = "논리서버";
		
		if(tabArray.length > 0 && gridData.length > 0){
			// 탭 활성화
			if(Ext.getCmp("Tab").items.items[i].rendered == true){
				var headerNames = "";
				var includeColumns = "";
				var htmlTypes = "";
				
				// 자산 정보를 불러오는데 필요한 필수요소들은 유저가 선택하는게 아닌 무조건 들어가야 하는 정보이므로 소스코드에 세팅
				headerNames		+= "IT자산번호,구성ID,단위업무명,분류체계";
				includeColumns 	+= "ASSET_ID,CONF_ID,CONF_NM,PATH_CLASS_NM";
				htmlTypes 		+= "TEXT,TEXT,TEXT,TEXT";
				
				for ( var j = 0; j < gridData.length; j++) {
					headerNames 	+= ( "," + gridData[j].raw.COMMENTS );
					includeColumns 	+= ( "," + gridData[j].raw.COLUMN_NAME );
					htmlTypes 		+= ( "," + gridData[j].raw.HTML_TYPE );
				}
				
				if(gridData.length != 0){
					classTypeArr.push(tabArray[i].id);
					var sheetArr = { "sheetName": title, "titleName": title, "headerNames":headerNames, "includeColumns": includeColumns, "htmlTypes":htmlTypes };
					sheet.push(sheetArr);
				}
				
			// 탭 비활성화
			}else if(Ext.getCmp("Tab").items.items[i].rendered == false){
				classTypeArr.push(tabArray[i].id);
				var pHeader = 'grid.itam.statistics.'+class_mng_type+tabArray[i].id+'.header';
				var prefixHeader = getConstText({ isArgs: true, m_key: pHeader });
				
				alert(pHeader);
				var pTitle = 'grid.itam.statistics.'+class_mng_type+tabArray[i].id+'.text';
				var prefixTitle = getConstText({ isArgs: true, m_key: pTitle });
				
				var sheetArr = {"sheetName": title, "titleName": title, "headerNames":prefixTitle, "includeColumns": prefixHeader};
				sheet.push(sheetArr);			
			}
		}
	}
	
	if( sheet.length > 0 ){
		var excelAttrs = {"sheet":sheet};
		var paramMap = {};
		
		paramMap = Ext.getCmp('searchform').getInputData();
		paramMap['class_id'] = class_id;
		paramMap['class_type_arr'] = classTypeArr;
		paramMap['class_mng_type'] = class_mng_type;
		paramMap['tangible_asset_yn'] = 'N';
		paramMap['is_logic_statistics'] = 'Y';
		
		var excelParam = {};
		excelParam['fileName'] = '#springMessage("res.title.itam.excel.assetlist.logic")';
		excelParam['excelAttrs'] = excelAttrs;
		
		goExcelDownLoad("$!{context}/itg/itam/statistics/searchExcel.do", paramMap, excelParam);
	}else{
		alert( '#springMessage("msg.common.00015")');
		return;
	}
}

function gridDblCellClick( gridCmp, tdEl, cellIndex, record, trEl, rowIndex, e) {
	if( cellIndex != 0 ){
		var clickRecord = record.raw;
		var closable = true;
		parent.addTab(clickRecord, closable);
	}
}

// 검색폼에 엔터이벤트 추가
function searchBtnEvent(){	
	actionLink('searchAttr');
};

</script>
<style type="text/css">
#searchT input { 	
	font-weight:bold; 
	background: rgb(244,246,247);
	background: -moz-linear-gradient(-45deg,  rgba(244,246,247,1) 0%, rgba(235,241,244,1) 41%, rgba(222,236,242,1) 84%, rgba(222,234,239,1) 84%, rgba(230,240,247,1) 100%);
	background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,rgba(244,246,247,1)), color-stop(41%,rgba(235,241,244,1)), color-stop(84%,rgba(222,236,242,1)), color-stop(84%,rgba(222,234,239,1)), color-stop(100%,rgba(230,240,247,1)));
	background: -webkit-linear-gradient(-45deg,  rgba(244,246,247,1) 0%,rgba(235,241,244,1) 41%,rgba(222,236,242,1) 84%,rgba(222,234,239,1) 84%,rgba(230,240,247,1) 100%);
	background: -o-linear-gradient(-45deg,  rgba(244,246,247,1) 0%,rgba(235,241,244,1) 41%,rgba(222,236,242,1) 84%,rgba(222,234,239,1) 84%,rgba(230,240,247,1) 100%);
	background: -ms-linear-gradient(-45deg,  rgba(244,246,247,1) 0%,rgba(235,241,244,1) 41%,rgba(222,236,242,1) 84%,rgba(222,234,239,1) 84%,rgba(230,240,247,1) 100%);
	background: linear-gradient(135deg,  rgba(244,246,247,1) 0%,rgba(235,241,244,1) 41%,rgba(222,236,242,1) 84%,rgba(222,234,239,1) 84%,rgba(230,240,247,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f4f6f7', endColorstr='#e6f0f7',GradientType=1 );
	text-indent: 5px;
	text-shadow: 1px 1px 3px #aad;
	border-top: 1px solid #eee;
	border-left: 1px solid #eee;
	color: #47e;
	box-shadow: 3px 3px 3px #eef;
	font-size: 10pt;
	text-transform:uppercase;
	border-radius:5px;
}
.alertRow {
	background-color: #FFCC33;
}
.blankCell {
	background-color: red;
}
</style>
</head>
<body>
<form name="theForm" method="post">
<div id="loading-mask"></div>
<div id="loading">
  <div class="loading-indicator">#springMessage("msg.mask.load")</div>
</div>
</body>
</html>