/********************************************
 * Date: 2017-02-20
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  자산상태 변경 팝업
 ********************************************/

// 분류체계별 엑셀 다운로드
nkia.ui.popup.itam.assetdisuse.RegistAssetDisuse = function() {
	// 속성 Properties
	this.id = "";
	this.title = "폐기처리내용 등록";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.popupParamData = {};
	this.callbackFunc = "";	// 자산상태 변경 후 실행 function
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var startDateObj = createDateTimeStamp('DHM', {year:0, month:0, day:0});
		this.editForm = createFormComp({
			id: this.id + "_form",
			fields: {
				colSize: 1,
				items: [
				        {item: createDateFieldComp({label: "폐기일자", name: "disuse_dt", dateType : 'D', dateValue: startDateObj, notNull: true})},
				        {item: createTextAreaFieldComp({label:'처분내용', name:'disuse_desc', resizable: true})}
				        ]
			}
		});
		
	} // end setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.editForm)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '등 록', type: "form", click: this.registAssetDisuse.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
	} // end setWindow
	
	// 자산상태 변경 event
	this.registAssetDisuse = function(){
		var _this = this;
		if(!confirm("선택한 자산을 폐기처리하시겠습니까?")) {
			return false;
		}
		var assetList = this.popupParamData.assetList;
		var dataMap = $$(this.id + "_form")._getValues();
		dataMap["assetList"] = assetList;
		
		nkia.ui.utils.ajax({
			viewId: "scrollview",
			url: "/itg/itam/statistics/insertAssetDisuse.do",
			params: dataMap,
			async: false,
			isMask: true,
			success: function(response){
				nkia.ui.utils.notification({
					message: '폐기처리되었습니다.'
				});
				_this.window.close();
				if(_this.callbackFunc){
					_this.callbackFunc();
				}
			}
		});
		
	}
	
}; // end nkia.ui.popup.itam.excel.ChangAssetState


/**
 * 폐기내용등록 팝업
 * @param {}
 */
function createRegistAssetDisuseWindow(popupProps) {
	var props = popupProps||{};
	var popupObject;
	try {
		switch (props.type) {
			case "AssetDisuse":	// 분류체계별 자산 엑셀 다운로드
				popupObject = new nkia.ui.popup.itam.assetdisuse.RegistAssetDisuse;
				break;
			default:
				popupObject = new nkia.ui.popup.itam.assetdisuse.RegistAssetDisuse;
		}
		popupObject.setProps(props);
		popupObject.setRender(props);
		popupObject.setWindow();
		
	} catch(e) {
		alert(e.message);
	}
}


