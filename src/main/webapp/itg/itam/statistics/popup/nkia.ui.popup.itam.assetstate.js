/********************************************
 * Date: 2017-02-20
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  자산상태 변경 팝업
 ********************************************/

// 분류체계별 엑셀 다운로드
nkia.ui.popup.itam.assetstate.ChangAssetState = function() {
	// 속성 Properties
	this.id = "";
	this.title = "자산상태변경";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.popupParamData = {};
	this.callbackFunc = "";	// 자산상태 변경 후 실행 function
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var filteredDataArr = this.popupParamData.filteredDataArr;
		this.editForm = createFormComp({
			id: this.id + "_form",
			fields: {
				colSize: 1,
				items: [
				        {item: createCodeComboBoxComp({label: "자산상태", name: "pop_asset_state", code_grp_id: "ASSET_STATE", filteredData: filteredDataArr})}
				        ]
			}
		});
		
	} // end setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.editForm)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '변 경', type: "form", click: this.changeAssetState.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
	} // end setWindow
	
	// 자산상태 변경 event
	this.changeAssetState = function(){
		var _this = this;
		if(!confirm("선택된 자산의 자산상태를 변경하시겠습니까?")) {
			return false;
		}
		var assetList = this.popupParamData.assetList;
		var dataMap = $$(this.id + "_form")._getValues();
		dataMap["asset_list"] = assetList;
		
		nkia.ui.utils.ajax({
			viewId: "scrollview",
			url: "/itg/itam/statistics/changeAssetState.do",
			params: dataMap,
			async: false,
			isMask: true,
			success: function(response){
				nkia.ui.utils.notification({
					message: '자산상태가 변경되었습니다.'
				});
				_this.window.close();
				if(_this.callbackFunc){
					_this.callbackFunc();
				}
			}
		});
		
	}
	
}; // end nkia.ui.popup.itam.excel.ChangAssetState


/**
 * 엑셀 다운로드 속성선택 팝업
 * @param {}
 */
function createChangeAssetStateWindow(popupProps) {
	var props = popupProps||{};
	var popupObject;
	try {
		switch (props.type) {
			case "ChangAssetState":	// 분류체계별 자산 엑셀 다운로드
				popupObject = new nkia.ui.popup.itam.assetstate.ChangAssetState;
				break;
			default:
				popupObject = new nkia.ui.popup.itam.assetstate.ChangAssetState;
		}
		popupObject.setProps(props);
		popupObject.setRender(props);
		popupObject.setWindow();
		
	} catch(e) {
		alert(e.message);
	}
}


