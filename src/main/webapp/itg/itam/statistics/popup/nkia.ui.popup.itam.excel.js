/********************************************
 * Date: 2017-02-16
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  엑셀다운로드 팝업
 ********************************************/

// 분류체계별 엑셀 다운로드
nkia.ui.popup.itam.excel.ExcelByClassType = function() {
	// 속성 Properties
	this.id = "";
	this.title = "엑셀다운로드";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.popupParamData = {};
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		var classType = this.popupParamData["class_type"];
		this.tabBar = new Array();
		var tabBody = new Array();
		
		var svPhysiTabBarMap = {id: 'sv_tab', CODE_ID: 'SV', value: "서버", SV_TYPE:'PHY'};
		var stTabBarMap = {id: 'st_tab', CODE_ID: 'ST', value: "스토리지"};
		var nwTabBarMap = {id: 'nw_tab', CODE_ID: 'NW', value: "네트워크"};
		var bkTabBarMap = {id: 'bk_tab', CODE_ID: 'BK', value: "백업장비"};
		var scTabBarMap = {id: 'sc_tab', CODE_ID: 'SC', value: "보안"};
		var swTabBarMap = {id: 'sw_tab', CODE_ID: 'SW', value: "소프트웨어"};
		var apTabBarMap = {id: 'ap_tab', CODE_ID: 'AP', value: "응용시스템"};
		var diTabBarMap = {id: 'di_tab', CODE_ID: 'DI', value: "DBInstance"};
		
		if(classType == 'ROOT'){
			this.tabBar = [svPhysiTabBarMap,stTabBarMap,nwTabBarMap,bkTabBarMap,scTabBarMap,swTabBarMap,apTabBarMap,diTabBarMap];
		}else if(classType == 'HW'){
			this.tabBar = [svPhysiTabBarMap,stTabBarMap,nwTabBarMap,bkTabBarMap,scTabBarMap];
		}else if(classType == "SV"){
			this.tabBar = [svPhysiTabBarMap/*, svLogicTabBarMap*/];
		}else if(classType == "ST"){
			this.tabBar = [stTabBarMap];
		}else if(classType == "NW"){	
			this.tabBar = [nwTabBarMap];
		}else if(classType == "SC"){	
			this.tabBar = [scTabBarMap];
		}else if(classType == "BK"){	
			this.tabBar = [bkTabBarMap];
		}else if(classType == "SW"){
			this.tabBar = [swTabBarMap];
		}else if(classType == "AP"){	
			this.tabBar = [apTabBarMap];
		}else if(classType == "DI"){	
			this.tabBar = [diTabBarMap];
		}
		for(var i=0; i < this.tabBar.length; i++){
			var tabClassType = this.tabBar[i].CODE_ID;
			var tabId = this.tabBar[i].id;
			
			var paramMap = {};
			paramMap['class_type'] = tabClassType;
			if(tabClassType == 'SV'){
				paramMap['sv_type'] =this.tabBar[i].SV_TYPE;
			}
			
			var excelAttrGrid = createGridComp({
				id : tabId+'_grid',
				resizeColumn : false,
				height : 370,
				url : "/itg/itam/statistics/searchClassTypeAttrList.do",
				resource : "grid.itam.statistics.excelDownPop",
				checkbox : true,
				params : paramMap,
				header: {
					title: "속성 선택"
				}
			});
			
			// 각 탭별로 grid 구성
			tabBody.push({
				id: tabId,
				rows: excelAttrGrid
			});
		} // end for()
		
		// 실제 탭을 구성합니다.
		this.tabGrid = {
				view:"tabview", // view 형식 : tabview로 입력
				id:"assetTypeTab", // 탭의 ID
				animate:false,
				tabbar:{
					options: this.tabBar // tabbar 배치
				},
				cells:new Array().concat(tabBody) // tab body 배치
		};
		
	} // end setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.tabGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.downExcelAssetList.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
	} // end setWindow
	
	// 엑셀 다운로드
	this.downExcelAssetList = function() {
		var _this = this;
		var sheet = [];
		var tabArray = this.tabBar;
		var classTypeArr = [];
		var isEmptyRow = false;
		
		for(var i = 0; i < tabArray.length; i++){
			var gridData = $$(tabArray[i].id+'_grid')._getSelectedRows();
			var title = tabArray[i].value;
			
			if(gridData.length < 1) {
				isEmptyRow = true;
				break;
			}
			
			if(tabArray.length > 0 && gridData.length > 0){
				var headerNames = "";
				var includeColumns = "";
				var htmlTypes = "";
				var popupShowTypes = "";
				
				// 자산 정보를 불러오는데 필요한 필수요소들은 유저가 선택하는게 아닌 무조건 들어가야 하는 정보이므로 소스코드에 세팅
				headerNames		+= "IT자산번호,구성ID,단위업무명,분류체계";
				includeColumns 	+= "ASSET_ID,CONF_ID,CONF_NM,PATH_CLASS_NM";
				htmlTypes 		+= "TEXT,TEXT,TEXT,TEXT";
				popupShowTypes 	+= ",,,";
				
				for ( var j = 0; j < gridData.length; j++) {
					headerNames 	+= ( "," + gridData[j].COMMENTS );
					includeColumns 	+= ( "," + gridData[j].COLUMN_NAME );
					htmlTypes 		+= ( "," + gridData[j].HTML_TYPE );
					popupShowTypes 	+= ( "," + gridData[j].POPUP_SHOW_TYPE );
				}
				
				// 속성명에 있는 HTML 태그 제거
				headerNames = headerNames.replace(/<[^>]*>/g, '');
				
				if(gridData.length > 0){
					classTypeArr.push(tabArray[i].CODE_ID);
					var sheetArr = { "sheetName": title, "titleName": title, "headerNames":headerNames, "includeColumns": includeColumns, "htmlTypes":htmlTypes, "popupShowTypes":popupShowTypes };
					sheet.push(sheetArr);
				}
			}
			
		} // end for()
		
		if(isEmptyRow) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '속성을 선택하지 않은 분류체계가 있습니다.'
			});
			return false;
		} else {
			var excelAttrs = {"sheet":sheet};
			var paramMap = this.popupParamData;
			paramMap['class_type_arr'] = classTypeArr;
			
			var getResultUrl = "/itg/itam/statistics/searchExcel.do";
			
			var excelParam = {};
			excelParam['fileName'] = '자산목록';
			excelParam['excelAttrs'] = excelAttrs;
			goExcelDownLoad(getResultUrl, paramMap, excelParam);
			_this.window.close();
		}
	}
		
}; // end nkia.ui.popup.itam.excel.ExcelByClassType


// 일반 엑셀 다운로드
nkia.ui.popup.itam.excel.ExcelByDynamic = function() {
	// 속성 Properties
	this.id = "";
	this.param = {};	// 조회조건
	this.excelParam = {};	// excelParam 필수 항목 :  url,fileName, sheetName, titleName, includeColumns, headerNames, defaultColumns 
	this.title = "엑셀다운로드";
	this.closable = true;
	this.width = 450;
	this.height = 500;
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var propSelectPop = null;
		var propNmArray = [];
		var propIdArray = [];
		var colNm;
		var colId;
		var addData = {};
		
		var tempExcelParam = this.excelParam;
		var param = this.param;
		
		this.excelAttrGrid = createGridComp({
			id : this.id+'_grid',
			resizeColumn : true,
			checkbox : true,
			resource : "grid.popup.excel",
			autoLoad : false,
			header : {
				title : "속성 선택",
			}
		});
		
	} // end this.setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.excelAttrGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.downExcelFile.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
		
	} // end setWindow
	
	this.setGridAttrData = function() {
		var popGrid = $$(this.id+'_grid');
		var gridData = new Array();
		var colNm = this.excelParam['headerNames'].split(",");
		var colId = this.excelParam['includeColumns'].split(",");
		
//		popGrid._addRow(gridData);
		var rowDataArr = new Array();
		for ( var i = 0; i < colNm.length; i++) {
			var rowDataMap = {
					'RNUM' : i,
					'COL_NM' : colNm[i],
					'COL_ID' : colId[i],
			}
			rowDataArr.push(rowDataMap);
		}
		popGrid._addRow(rowDataArr);
		
	} // end setGridArrtData
	
	
	this.downExcelFile = function(){
		var _this = this;
		var propNmArray = [];
		var propIdArray = [];
		var selectedRecords = $$(this.id+'_grid')._getSelectedRows();
		if (selectedRecords.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택한 속성이 없습니다.'
			});
		} else {
			for ( var i = 0; i < selectedRecords.length; i++) {
				var col_nm = nullToSpace(selectedRecords[i].COL_NM);
				var col_id = nullToSpace(selectedRecords[i].COL_ID);
				
				// 속성명에 있는 HTML 태그 제거
				col_nm = col_nm.replace(/<[^>]*>/g, '');
				propNmArray.push(col_nm);
				propIdArray.push(col_id);
			}
			
			var headerNm = propNmArray.join();
			var headerId = propIdArray.join();
			
			var excelAttrs = {
					sheet : [ {
						sheetName : this.excelParam['sheetName'],
						titleName : this.excelParam['titleName'],
						headerWidths : this.excelParam['headerWidths'],
						headerNames : headerNm,
						includeColumns : headerId,
						groupHeaders : this.excelParam['groupHeaders'],
						align : this.excelParam['align']
					} ]
				};
			
			var excelParam = {};
			excelParam['fileName'] = this.excelParam["fileName"];
			excelParam['excelAttrs'] = excelAttrs;

			goExcelDownLoad(this.excelParam['url'], this.param, excelParam);
			_this.window.close();
		}
		
	} // downExcelFile
	
} // end nkia.ui.popup.itam.excel.ExcelByDynamic 

/**
 * 엑셀 다운로드 속성선택 팝업
 * @param {}
 */
function createExcelTabWindow(popupProps) {
	var props = popupProps||{};
	
	var popupObject;
	try {
		switch (props.type) {
			case "assetByClassType":	// 분류체계별 자산 엑셀 다운로드
				popupObject = new nkia.ui.popup.itam.excel.ExcelByClassType;
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
				break;
			case "Dynamic":	// dynamic 일반 엑셀 다운로드(url 데이터 호출)
				popupObject = new nkia.ui.popup.itam.excel.ExcelByDynamic;
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
				popupObject.setGridAttrData();
				break;
			default:
				popupObject = new nkia.ui.popup.itam.excel.ExcelByClassType;
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
		}
		
		
	} catch(e) {
		alert(e.message);
	}
}