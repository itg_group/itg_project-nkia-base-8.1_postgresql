/********************************************
 * Asset
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회
 ********************************************/
nkia.ui.popup.itam.Asset = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 1200;
	this.height = 600;
	this.url = "/itg/itam/oper/searchAssetList.do";
	this.resource = "grid.itam.oper.asset";
	this.gridParams = {};
	this.gridHeight = 300;
	this.treeParams = {};
	this.checkbox = true;
	this.expandLevel = 3;
	this.reference = {};
	this.keys = [ "CONF_ID" ];
	this.dependency = {};
	this.type = "itam";
	this.bindEvent = null;
	this.choiceFunc = null;
	this.select = false;
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_classTree',
	        url: '/itg/itam/statistics/searchClassTree.do',
	        header: {
	            title: getConstText({isArgs : true, m_key : 'res.title.itam.class'})
	        },
	        expColable: true,
	        filterable: true,
	        width: 200,
	        params: this.treeParams,
	        expandLevel: 3,
	        on: {
	        	onItemClick: this.doTreeClick.bind(_this)
	        }
	    });
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 3,
		        items: [
		            {colspan: 2, item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_type',
	                    				code_grp_id: 'SEARCH_OPER_ASSET',
	                    				attachChoice: true,
	                    				value: 'HOST_NM',
	                    				width: 250
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	},
	              	{colspan: 1, item: createTextFieldComp({label: '고객사정보', name: 'customer_id_nm', on: { onKeyPress:this.doKeyPress.bind(_this) } })},
	              	{colspan: 1, item: createCodeComboBoxComp({label: '센터위치', name: 'location_cd', code_grp_id: 'CENTER', attachChoice: true})},
	              	{colspan: 1, item: createTextFieldComp({label: '상면코드', name: 'center_position_cd_nm', on: { onKeyPress:this.doKeyPress.bind(_this) } })},
	              	//{colspan: 1, item: createCodeComboBoxComp({label: 'EMS관제', name: 'ems_collect_yn', code_grp_id: 'YN', attachChoice: true})},
    				{colspan: 1, item: createSingleCheckBoxFieldComp({label: "즐겨찾는 CI", name: "bookmark_type", checkValue:"MY_LIST", uncheckValue:"N"})}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = getConstText({isArgs : true, m_key : 'res.title.itam.maint.assetlist'});
		
		if(this.type == "itsm"){
			title = "구성 목록";
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
		    resource: this.resource,
		    params: this.gridParams,
		    autoLoad: false,
			url: this.url,
			select: this.select,
			pageable: true,
			checkbox: this.checkbox,
			dependency: this.dependency,
			minHeight: 150,
			keys: this.keys,
			on:{
				onItemDblClick: function(id, e){
					if( _this.bindEvent != null ){
						_this.bindEvent.choice(_this);
					}else{
						_this.choice(id, e);
					}
				}
			}
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
						cols: [{
							rows: this.tree
			            },{
			            	rows: new Array().concat(this.searchForm, this.grid)
			            }]
					}
				};
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트 - 검색폼 초기화
	this.doTreeClick = function(id, e) {
		$$(this.id + "_searchForm")._reset();
		var treeItem = $$(this.id + '_classTree').getItem(id);

		var form = $$(this.id + "_searchForm");
		var searchParams = form._getValues();

		searchParams.class_id = treeItem.node_id;
		searchParams.class_type = treeItem.class_type;
		searchParams.class_mng_type = treeItem.class_mng_type;
		
		var gridParams = this.gridParams;
		
		var searchTypeGrpId = 'SEARCH_OPER_ASSET';
		
		// 그리드 헤더 변경
		if('LN' == searchParams.class_type) {
			searchTypeGrpId = 'SEARCH_OPER_LN_ASSET';
			$$(this.id + '_grid')._updateHeaderFromResource(this.resource + '.ln');
		} else {
			searchTypeGrpId = 'SEARCH_OPER_ASSET';
			$$(this.id + '_grid')._updateHeaderFromResource(this.resource);
		}
		
		// 검색 콤보박스 갱신
		$$(this.id + "_searchForm").elements["search_type"]._reload({code_grp_id : searchTypeGrpId});
	
		for(var i in gridParams) {
			if(gridParams.hasOwnProperty(i)) {
				searchParams[i] = gridParams[i];
			}
		}
		
		$$(this.id + "_grid")._reload(searchParams);
	};

	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode) {
		if (keyCode && keyCode == "13") {
			this.doSearch();
		}
	}

	// 검색 버튼 이벤트
	this.doSearch = function(treeId){
		
		var searchParams = $$(this.id + '_searchForm')._getValues();
		var treeParam = $$(this.id + '_classTree')._getSelectedItem();

		if(typeof treeParam != "undefined") {
			searchParams.class_id = treeParam.node_id;
			searchParams.class_type = treeParam.class_type;
			searchParams.class_mng_type = treeParam.class_type;
		}else {
			searchParams['class_type'] = this.treeParams.class_type;
		}	
		
		var gridParams = this.gridParams;
		
		for(var i in gridParams) {
			if(gridParams.hasOwnProperty(i)) {
				searchParams[i] = gridParams[i];
			}
		}
		$$(this.id + '_grid')._reload(searchParams);
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();	
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e) {

		var selectedItem = $$(this.id + "_grid")._getSelectedRows();
		if (selectedItem.length < 1) {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 정보가 없습니다.'
			});
			return false;
		}
		if (this.choiceFunc != null) {
			this.choiceFunc(this.reference,selectedItem[0]);
		} else {
			// 요청팝업의 경유, validation 
			var reference = this.reference;
//			if(this.type == "CI"){
//				if(!isProcCivalidation(selectedItem)){
//					return false;
//				}
//				if(selectedItem[0].CLASS_TYPE == "HW"){
//					if(confirm("[CI없음] 선택시 기존에 추가했던 구성자원은 삭제됩니다. \n선택하시겠습니까?")){
//						reference.grid.asset._removeAllRow();
//					}
//				}else{
//					var refRows = reference.grid.asset._getRows();
//					for(var i = 0; i < refRows.length; i++){
//						if(refRows[i].CLASS_TYPE == "HW"){
//							reference.grid.asset._removeRow(refRows[i].id);
//							break;
//						}
//					}
//				}
//				
//			}
			
			//reference.grid.asset._removeAllRow();
	        reference.grid.asset._addRow(selectedItem); 
	        if(typeof PROCESS_META !== "undefined") {
		        if(PROCESS_META.TASK_NAME == "CHANGE_PLAN"){
		        	if($$('change_plan')._getFieldValue("snapshot_set") != "스냅샷 설정전입니다."){
		        		alert("구성자원 추가로 인하여 스냅샷설정을 다시 진행하여 주십시오.");
		        		$$('change_plan')._setValues({snapshot_set:'스냅샷 설정전입니다.'});
		        	}
		        }
	        }
		}        
        this.window.close();
	}
	
	function isProcCivalidation(gridRows){
		var isRootCiExists = false;
		for(var i = 0; i < gridRows.length; i++){
			if(gridRows[i].CLASS_TYPE == "HW"){
				isRootCiExists = true;
				break;
			}
		}
		
		if(isRootCiExists && gridRows.length > 1){
			nkia.ui.utils.notification({
                type: 'error',
                message: '[CI없음] 선택시 다른 CI 선택이 불가합니다.'
            });
			return false;
		}
		return true;
	}
};

/********************************************
 * Asset
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회
 ********************************************/
nkia.ui.popup.itam.BatchAsset = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 1200;
	this.height = 600;
	this.url = "/itg/itam/batchEdit/searchBatchEditAssetPop.do";
	this.resource = "grid.itam.batchEdit.assetPopS";
	this.gridParams = {class_type: 'ROOT'};
	this.gridHeight = 300;
	this.treeParams = {};
	this.checkbox = true;
	this.expandLevel = 3;
	this.reference = {};
	this.keys = [ "ASSET_ID", "CONF_ID" ];
	this.dependency = {};
	this.type = "itam";
	this.bindEvent = null;
	this.choiceFunc = null;
	this.select = false;
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_classTree',
	        url: '/itg/itam/statistics/searchClassTree.do',
	        header: {
	            title: getConstText({isArgs : true, m_key : 'res.title.itam.class'})
	        },
	        expColable: true,
	        filterable: true,
	        width: 200,
	        params: this.treeParams,
	        expandLevel: 3,
	        on: {
	        	onItemClick: this.doTreeClick.bind(_this)
	        }
	    });
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 2,
		        items: [
		            {colspan: 1, item : createUnionFieldComp({
	                            items: [
	                                createCodeComboBoxComp({
	                    				label: getConstText({isArgs : true, m_key : 'res.label.system.00012'}),
	                    				name: 'search_type',
	                    				code_grp_id: 'SEARCH_OPER_ASSET',
	                    				attachChoice: true
	                    			}),
	                    			createTextFieldComp({
	                    				name: "search_value",
	                    				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
	                    				on:{
	       									onKeyPress: this.doKeyPress.bind(_this)
	     								}	
	                    			})
	                            ]
	                        })
	              	},
	              	{colspan: 1, item: createTextFieldComp({label: '고객사정보', name: 'customer_id_nm', on: { onKeyPress:this.doKeyPress.bind(_this) } })},
	              	{colspan: 1, item: createTextFieldComp({label: '상면코드', name: 'center_position_cd_nm', on: { onKeyPress:this.doKeyPress.bind(_this) } })},
    				{colspan: 1, item: createCodeComboBoxComp({label: 'EMS관제', name: 'ems_collect_yn', code_grp_id: 'YN', attachChoice: true})},
    				{colspan: 1, item: createSingleCheckBoxFieldComp({label: "즐겨찾는 CI", name: "bookmark_type", checkValue:"MY_LIST", uncheckValue:"N"})}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = getConstText({isArgs : true, m_key : 'res.title.itam.maint.assetlist'});
		
		if(this.type == "itsm"){
			title = "구성 목록";
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
		    resource: this.resource,
		    params: this.gridParams,
			url: this.url,
			select: this.select,
			pageable: true,
			checkbox: this.checkbox,
			dependency : {
				targetGridId: this.id + '_subGrid',
				data: this.reference.grid.asset
			},
			minHeight: 150,
			keys: this.keys
		});
		
		this.subGrid = createGridComp({
			id : this.id + '_subGrid',
			checkbox: false,
			select: false,
			resource : "grid.itam.batchEdit.assetPopT",
			autoLoad : false,
			keys: this.keys,
			header : {
				title : "선택된 " + this.title
			},
			actionColumns: [{
				column: "ACTION",
				template: nkia.ui.html.icon("remove", "remove_row"),
				onClick: {
					"remove_row": this.removeSelGrid.bind(this)
				}
			}]
		});
	};
	
	// 서브 그리드 row 제거
	this.removeSelGrid = function(e, row, html) {
		var data = $$(this.id + '_subGrid')._getSelectedRows();
		if(data!=null && data.length > 0) {
			if(data.length < 1) {
				nkia.ui.utils.notification({
					type: 'error',
					message: '선택된 정보가 없습니다.'
				});
				return false;
			}
			$$(this.id + '_subGrid')._removeRow();
		} else {
			$$(this.id + '_subGrid')._removeRow(row);
		}
		
		this.reloadGrid(true);
	};
	
	// 서브 그리드 row 추가
	this.addSelGrid = function() {
		var data = $$(this.id + '_grid')._getSelectedRows();
		if(data.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(this.id + '_subGrid')._addRow(data);
		
		this.reloadGrid(true);
	};
	
	// 그리드 갱신 이벤트
	this.reloadGrid = function(isGoPage){
		var searchParams = $$(this.id + '_searchForm')._getValues();
		
		if($$(this.id + '_classTree')) {
			var treeParams = $$(this.id + '_classTree').getSelectedItem();
	
			if(typeof treeParams != "undefined") {
				searchParams.class_id = treeParams.node_id;
				searchParams.class_type = treeParams.class_type;
				searchParams.class_mng_type = treeParams.class_mng_type;
			} else {
				searchParams.class_type = this.gridParams.class_type;
			}
		}
		
		$$(this.id + '_grid')._reload(searchParams, isGoPage);
	};
	
	this.setWindow = function(){
		var _this = this;
		 var bodyScrollView = {
			        view: "scrollview",
			        body: {
			        	view:"accordion",
						multi:true,
						cols: [{
							header: "분류체계",
			                body: {
			                	rows: this.tree
			                }
			            },{
			            	rows: new Array().concat(this.searchForm, this.grid)
			            },{
		                    view: "template",
					 		template: nkia.ui.html.icon("arrow_right", "add_row"),
					 		width: 30,
					 		onClick: {
					 			"add_row": this.addSelGrid.bind(this)
					 		}
		                },{
			            	rows: new Array().concat(this.subGrid),
			            	width: 300
			            }]
					}
				};
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [bodyScrollView]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// Tree 클릭 이벤트 - 검색폼 초기화
	this.doTreeClick = function(id, e) {
		$$(this.id + "_searchForm")._reset();
		var treeItem = $$(this.id + '_classTree').getItem(id);

		var form = $$(this.id + "_searchForm");
		var searchParams = form._getValues();

		searchParams.class_id = treeItem.node_id;
		searchParams.class_type = treeItem.class_type;
		searchParams.class_mng_type = treeItem.class_mng_type;
		
		var gridParams = this.gridParams;
	
		for(var i in gridParams) {
			if(gridParams.hasOwnProperty(i)) {
				searchParams[i] = gridParams[i];
			}
		}
		
		
		$$(this.id + "_grid")._reload(searchParams);
	};

	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode) {
		if (keyCode && keyCode == "13") {
			this.doSearch();
		}
	}

	// 검색 버튼 이벤트
	this.doSearch = function(treeId){
		
		var searchParams = $$(this.id + '_searchForm')._getValues();
		var treeParam = $$(this.id + '_classTree')._getSelectedItem();

		if(typeof treeParam != "undefined") {
			searchParams.class_id = treeParam.node_id;
			searchParams.class_type = treeParam.class_type;
			searchParams.class_mng_type = treeParam.class_type;
		}else {
			searchParams['class_type'] = this.treeParams.class_type;
		}	
		
		var gridParams = this.gridParams;
		
		for(var i in gridParams) {
			if(gridParams.hasOwnProperty(i)) {
				searchParams[i] = gridParams[i];
			}
		}
		$$(this.id + '_grid')._reload(searchParams);
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();	
	}

	// 검색 초기화 버튼 이벤트
	this.clearSearch = function() {
		$$(this.id + '_searchForm')._reset();
	}

	// 적용(선택) 이벤트
	this.choice = function(id, e) {

		var selectedItem = $$(this.id + "_subGrid")._getRows();
		if (selectedItem.length < 1) {
			nkia.ui.utils.notification({
				type : 'error',
				message : '선택된 정보가 없습니다.'
			});
			return false;
		}
		if (this.choiceFunc != null) {
			this.choiceFunc(this.reference,selectedItem[0]);
		} else {
			// 요청팝업의 경유, validation 
			var reference = this.reference;
			reference.grid.asset._addRow(selectedItem); 
		}        
        this.window.close();
	}
	
	function isProcCivalidation(gridRows){
		var isRootCiExists = false;
		for(var i = 0; i < gridRows.length; i++){
			if(gridRows[i].CLASS_TYPE == "HW"){
				isRootCiExists = true;
				break;
			}
		}
		
		if(isRootCiExists && gridRows.length > 1){
			nkia.ui.utils.notification({
                type: 'error',
                message: '[CI없음] 선택시 다른 CI 선택이 불가합니다.'
            });
			return false;
		}
		return true;
	}
};


/********************************************
 * AssetAndService
 * Date: 2016-11-17
 * Version:
 *	1.0 - 자산조회 + 서비스 선택 팝업(프로세스에서 사용)
 ********************************************/
nkia.ui.popup.itam.AssetAndService = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.reference = {};
	this.actionColumns = [];
	
	// 구성요소 Properties
	this.asset = {};
	this.selectedAssetGrid = {};
	this.serviceGrid = {};
	this.helpContent = {};
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		props.bindEvent = this;
		props.checkbox = true;
		props.dependency = {
			data: this.reference.data.asset,
			targetGridId: props.id + '_selectedAssetGrid'
		}
		this.asset = new nkia.ui.popup.itam.Asset();
		this.asset.setProps(props);
		this.asset.setRender(props);
		
		this.selectedAssetGrid = createGridComp({
			id: props.id + '_selectedAssetGrid',
			header: {
				title: "선택된 자산"
			},
		    resource: 'grid.nbpm.asset.serviceexp.sel',
			select: false,
			pageable: false,
			autoLoad: false,
			keys: ["CONF_ID"],
			actionColumns: [{
				column: "DEL_ACTION",
				template: function(data){
					return nkia.ui.html.icon("remove", "delete_row");
				},
				onClick: {
					"delete_row": function(e, row, html){
						this._removeRow(row);
//						_this.getRelServicetList();
						
						// 자산 목록 새로고침
			        	$$(_this.id + '_grid')._reload(null, true);
					}
				}
			}]
		});
		
		this.serviceGrid = createGridComp({
			id: props.id + '_serviceGrid',
			header: {
				title: "선택한 자산의 연관 서비스"
			},
		    resource: 'grid.nbpm.service.serviceexp1',
		    url: '/itg/nbpm/common/searchSelecetedConfRelServiceList.do',
		    minimizeCount: true,
			select: false,
			pageable: false,
			checkbox: true,
			keys: ["SERVICE_ID"],
			autoLoad: false
		});
		
		this.helpContent = {
 			view: "template",
 			template: "<font style='color:red'><b>*&nbsp;하단 연관 서비스 목록에서 선택(체크)하시면<br/>&nbsp;&nbsp;서비스 조회에도 적용이 됩니다.</b></font>",
 			height: 46
 		}
	};
	
	this.setWindow = function(){
		var _this = this;
		
		var popupFooter = createButtons({
			align: "center",
			items: [
				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
			]
		});
		
		var body = {
			rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    header: "분류체계정보",
                    body: {
                    	rows: this.asset.tree
                    }
                },{
                    rows: new Array().concat(this.asset.searchForm, this.asset.grid)
                },{
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_right", "add_asset"),
			 		width: 30,
			 		onClick: {
			 			"add_asset": function(e, row, html){
			 				var data = $$(_this.id + '_grid')._getSelectedRows();
			 				if(data.length > 0){
			 					$$(_this.id + '_selectedAssetGrid')._addRow(data);
//			 					_this.getRelServicetList();
			 					
			 					// 자산 목록 새로고침
					        	$$(_this.id + '_grid')._reload(null, true);
			 				}
			 				
			 			}
			 		}
                },{
                	width: 250,
                    //rows: new Array().concat(this.selectedAssetGrid, this.helpContent, this.serviceGrid, popupFooter)
                    rows: new Array().concat(this.selectedAssetGrid)
                }]
    		}]
		}
		
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
	    				createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
	    			]
	    		}
	    	},
	    	closable: this.closable 
	    });
	    
	    this.bindData();
	}
	
	this.bindData = function(){
		// 선택된 구성 목록
	    if(this.reference.data.asset
	    		&& this.reference.data.asset.length > 0){
	    	$$(this.id + '_selectedAssetGrid')._addRow(this.reference.data.asset);
	    }
		
		// 선택된 서비스 목록
	    if(this.reference.data.service
	    		&& this.reference.data.service.length > 0){
	    	var serviceGrid = $$(this.id + '_serviceGrid');
	    	serviceGrid._addRow(this.reference.data.service);
	    	
	    	var serviceRows = serviceGrid._getRows();
    		serviceRows.forEach(function(row){
    			serviceGrid._setCheckedRow(row);
			});
	    }
	}
	
	// 자산의 연관 서비스 목록
	this.getRelServicetList = function(){
		var _this = this;
		var serviceGrid = $$(_this.id + '_serviceGrid');
		
		var data = $$(_this.id + '_selectedAssetGrid')._getRows();
		if(data.length > 0){
			var checkedServiceData = serviceGrid._getSelectedRows();
			
			// 자산의 연관 서비스 목록가져오기
			var conf_ids = [];
			data.forEach(function(item){
				conf_ids.push(item.CONF_ID);
			});
			
			nkia.ui.utils.ajax({
		        url: '/itg/nbpm/common/searchSelecetedConfRelServiceList.do',
				params: { conf_ids : conf_ids },
				async: false,
		        success: function(response){
		        	// 서비스 데이터 삭제
		        	serviceGrid._removeAllRow();
		        	
		        	// 서비스 데이터 추가
		        	if(response.gridVO && response.gridVO.rows.length > 0){
		        		serviceGrid._addRow(response.gridVO.rows);
						
		        		// 이전 체크된 데이터가 있는 경우 유지시켜준다.
		        		if(checkedServiceData.length > 0){
			        		var serviceData = serviceGrid._getRows();
			        		checkedServiceData.forEach(function(checkedData){
			        			serviceData.some(function(data){
			        				if(checkedData.SERVICE_ID == data.SERVICE_ID){
			        					serviceGrid._setCheckedRow(data);
			        					return true;
			        				}
								});
							});
			        	}
		        	}
		        	
		        	// 자산 목록 새로고침
		        	$$(_this.id + '_grid')._reload(null, true);
				}
			});
		}else{
			// 서비스 데이터 삭제
		  	serviceGrid._removeAllRow();
		  	
		  	// 자산 목록 새로고침
		  	$$(_this.id + '_grid')._reload(null, true);
		}
	}
	
	// Tree 클릭 이벤트
	this.doTreeClick = function(id, e){
		var treeItem = $$(this.id + '_chargerGrpTree').getItem(id);
		
		var params = {
			grp_id: treeItem.node_id
		}
		
		$$(this.id + '_chargerUserGrid')._reload(params);
		$$(this.id + '_chargerDeptGrid')._reload(params);
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var assetRows = $$(this.id + "_selectedAssetGrid")._getRows();
		if(assetRows.length > 0){
			var reference = this.reference;
			reference.grid.asset._removeAllRow();
            reference.grid.asset._addRow(assetRows);
            
            if(reference.grid.service){
            	var serviceRows = $$(this.id + "_serviceGrid")._getSelectedRows();
            	reference.grid.service._removeAllRow();
            	if(serviceRows.length > 0){
	            	reference.grid.service._addRow(serviceRows);
            	}
            	reference.grid.service._setBadgeCount();
            }
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 자산이 없습니다.'
            });
        }
	};
};

/**
 * 자산 팝업
 * @param {} popupProps
 */
function createAssetWindow(prop) {
	this.props = prop
	var _this = this;
	
	var popupHandler;
	var popup;
	try {
		switch (_this.props.type) {
			case "ciAndService":
				asset = new nkia.ui.popup.itam.AssetAndService();
				break;
			case "batch":
				asset = new nkia.ui.popup.itam.BatchAsset(_this.props);
				break;
			default:
				asset = new nkia.ui.popup.itam.Asset(_this.props);
		}
		asset.setProps(_this.props);
		asset.setRender(_this.props);
		asset.setWindow();
	} catch(e) {
		alert(e.message);
	}
}

/********************************************
 * Asset
 * Date: 2017-01-23
 * Version:
 *	1.0 - 서비스조회
 ********************************************/
nkia.ui.popup.itam.Service = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.keys = ["SERVICE_ID"];
	this.resource = "grid.nbpm.service.serviceexp.sel";
	this.gridParams = {};
	this.gridHeight = 300;
	this.treeParams = {};
	this.checkbox = true;
	this.expandLevel = 2;
	this.reference = {};
	this.dependency = {};
	this.bindEvent = null;
	this.actionColumns = [{
		column: "DEL_ACTION",
		template: function(data){
			return nkia.ui.html.icon("remove", "delete_row");
		},
		onClick: {
			"delete_row": function(e, row, html){
				this._removeRow(row);
			}
		}
	}]
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.tree = createTreeComponent({
			id: this.id + '_serviceTree',
	        url: '/itg/nbpm/common/searchServiceList.do',
	        header: {
	            title: getConstText({isArgs : true, m_key : 'res.label.itam.service.00001'})
	        },
	        expColable: true,
	        filterable: true,
	        width: 300,
	        checkbox: this.checkbox,
	        params: this.treeParams,
	        expandLevel: this.expandLevel
	    });
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: getConstText({isArgs : true, m_key : 'res.label.nbpm.sdesk.00007'})
		    },
		    resource: this.resource,
		    autoLoad: false,
		    select: false,
		    keys: this.keys,
			actionColumns: this.actionColumns
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: this.tree
	                },{
	                    rows: this.grid
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var assetRows = $$(this.id + "_grid")._getSelectedRows();
		if(assetRows.length > 0){
			var reference = this.reference;
            reference.grid._addRow(assetRows);
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 서비스가 없습니다.'
            });
        }
	};
};


/********************************************
 * ServiceAndAsset
 * Date: 2016-01-23
 * Version:
 *	1.0 - 서비스조회 + 자산 선택 팝업(프로세스에서 사용)
 ********************************************/
nkia.ui.popup.itam.ServiceAndAsset = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.reference = {};
	this.actionColumns = [];
	
	// 구성요소 Properties
	this.service = {};
	this.helpContent = {};
	this.assetGrid = {};
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		props.bindEvent = this;
		props.actionColumns = [{
			column: "DEL_ACTION",
			template: function(data){
				return nkia.ui.html.icon("remove", "delete_row");
			},
			onClick: {
				"delete_row": function(e, row, html){
					this._removeRow(row);
					_this.getRelAssetList();
				}
			}
		}]
		this.service = new nkia.ui.popup.itam.Service();
		this.service.setProps(props);
		this.service.setRender(props);
		
		this.assetGrid = createGridComp({
			id: props.id + '_assetGrid',
			header: {
				title: "선택한 서비스의 연관 구성 목록"
			},
		    resource: 'grid.nbpm.asset.serviceexp',
			select: false,
			pageable: false,
			checkbox: true,
			keys: ["CONF_ID"],
			autoLoad: false
		});
		
		this.helpContent = {
 			view: "template",
 			template: "<font style='color:red'><b>*&nbsp;하단 연관 구성 목록에서 선택(체크)하시면 구성 자원 조회에도 적용이 됩니다.</b></font>",
 			height: 30
 		}
	};
	
	this.setWindow = function(){
		var _this = this;
		
		var body = {
			rows: [{
    			view:"accordion",
    			multi:true,
    			cols: [{
                    header: "서비스 정보",
                    body: {
                    	rows: this.service.tree
                    }
                },{
                    view: "template",
			 		template: nkia.ui.html.icon("arrow_right", "add_service"),
			 		width: 30,
			 		onClick: {
			 			"add_service": function(e, row, html){
			 				var data = $$(_this.id + '_serviceTree')._getCheckedItems({
			 					excludeLevel: 1,
			 					otherData: true
			 				});
			 				if(data.length > 0){
			 					$$(_this.id + '_grid')._addRow(data);
			 					// 서비스 연관 자산 목록
			 					_this.getRelAssetList();
			 				}else{
			 					nkia.ui.utils.notification({
					                type: 'error',
					                message: '선택된 서비스가 없습니다.'
					            });
			 				}
			 			}
			 		}
                },{
                    rows: new Array().concat(this.service.grid, this.helpContent, this.assetGrid)
                }]
    		}]
		}
		
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: body,
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	    
	 	this.bindData();   
	}
	
	this.bindData = function(){
		// 선택된 서비스 목록
	    if(this.reference.data.service.length > 0){
	    	$$(this.id + '_grid')._addRow(this.reference.data.service);
	    }
	    
	    // 선택된 구성 목록
	    if(this.reference.data.asset.length > 0){
	    	var assetGrid = $$(this.id + '_assetGrid');
	    	assetGrid._addRow(this.reference.data.asset);
	    	
	    	var assetRows = assetGrid._getRows();
    		assetRows.forEach(function(row){
    			assetGrid._setCheckedRow(row);
			});
	    }
	}
	
	// 서비스의 연관 자산 목록
	this.getRelAssetList = function(){
		var _this = this;
		var assetGrid = $$(_this.id + '_assetGrid');
		
		var data = $$(_this.id + '_grid')._getRows();
		if(data.length > 0){
			var checkedAssetData = assetGrid._getSelectedRows();

			// 서비스의 연관 자산 목록가져오기
			var service_ids = [];
			data.forEach(function(item){
				service_ids.push(item.SERVICE_ID);
			});
			
			nkia.ui.utils.ajax({
		        url: '/itg/nbpm/common/searchSelecetedConfRelAssetList.do',
				params: { service_ids : service_ids },
				async: false,
		        success: function(response){
		        	// 자산 데이터 삭제
		        	assetGrid._removeAllRow();
		        	
		        	// 자산 데이터 추가
		        	if(response.gridVO && response.gridVO.rows.length > 0){
		        		assetGrid._addRow(response.gridVO.rows);
						
		        		// 이전 체크된 데이터가 있는 경우 유지시켜준다.
		        		if(checkedAssetData.length > 0){
			        		var assetData = assetGrid._getRows();
			        		checkedAssetData.forEach(function(checkedData){
			        			assetData.some(function(assetData){
			        				if(checkedData.CONF_ID == assetData.CONF_ID){
			        					assetGrid._setCheckedRow(assetData);
			        					return true;
			        				}
								});
							});
			        	}
		        	}
				}
			});
		}else{
			// 자산 데이터 삭제
		  	assetGrid._removeAllRow();
		}
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var serviceRows = $$(this.id + "_grid")._getRows();
		if(serviceRows.length > 0){
			var reference = this.reference;
			reference.grid.service._removeAllRow();
            reference.grid.service._addRow(serviceRows);
            
            if(reference.grid.asset){
            	var assetRows = $$(this.id + "_assetGrid")._getSelectedRows();
            	reference.grid.asset._removeAllRow();
            	if(assetRows.length > 0){
	            	reference.grid.asset._addRow(assetRows);
            	}
            	reference.grid.asset._setBadgeCount();
            }
            
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 서비스가 없습니다.'
            });
        }
	};
};

/**
 * 서비스 팝업
 * @param {} popupProps
 */
function createServiceWindow(popupProps) {
	var props = popupProps||{};
	var service;
	try {
		switch (props.type) {
			case "ciAndService":
				service = new nkia.ui.popup.itam.ServiceAndAsset();
				break;
			default:
				service = new nkia.ui.popup.itam.Service();
		}
		service.setProps(props);
		service.setRender(props);
		service.setWindow();
	} catch(e) {
		alert(e.message);
	}
}





