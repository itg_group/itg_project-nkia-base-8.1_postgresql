/**
 * nic 팝업
 */
nkia.ui.popup.itam.Nic = function(prop) {
	var _CLASS_ID = "";
	var _CLASS_MNG_TYPE = "";
	var _CLASS_TYPE = "";
	var _ASSET_STATE = "";
	var _TANGIBLE_ASSET_YN = "Y";
	var _CENTER_ASSET_YN = "Y";		
	// prop 이 어떤 정보를 가지고 있는지는 console 로 확인 후 원하는 정보를 꺼내쓰세요
	this.id = prop.id;
	this.popup_object_id = prop.popup_object_id;
	this.popup_type = prop.popup_type;
	this.field_id = prop.field_id;
	this.name_field_id = prop.name_field_id;
	this.target_form_id = prop.target_form_id;
	
	this.openPopup = function() {
		
		var _this = this;
		
		var treeParams = {};
		
		/**
		 *  분류체계별 트리 분기 처리
		 */		
		if (props.class_type == 'SV') {
			if (this.id == 'SV_BOX_CONF_ID_POP') {
					_CLASS_ID =  '151';
					_CLASS_TYPE =  'SV';
				    _CLASS_MNG_TYPE =  'SV';
			} else if(this.id == 'AP_CONF_ID_POP') {
					_CLASS_TYPE =  'AP';
				    _CLASS_MNG_TYPE =  'AP';
			} else if(this.id == 'ST_CONF_ID_POP') {
					_CLASS_TYPE = 'ST';
					_CLASS_MNG_TYPE = 'ST';
			}
		} else if (props.class_type == 'DI') {
			if (this.id == 'SW_CONF_ID_POP') {
				_CLASS_TYPE =  'SW';
			    _CLASS_MNG_TYPE =  'SW';
			} else if(this.id == 'AP_CONF_ID_POP') {
				_CLASS_TYPE =  'AP';
			    _CLASS_MNG_TYPE =  'AP'; 
			} else if (this.id == 'SV_CONF_ID_POP') {
				_CLASS_ID = '152';
				_CLASS_TYPE = 'SV';
				_CLASS_MNG_TYPE = 'SV';
			}
		}
		
		treeParams = {class_mng_type : _CLASS_MNG_TYPE, class_id : _CLASS_ID, class_type : _CLASS_TYPE};
		
		var tree = createTreeComponent({
			id: _this.popup_object_id + "_tree",
	        url: '/itg/itam/statistics/searchClassTree.do',
	        filterable: true,
	        expColable: true,
	        params: treeParams,
			expandLevel: 3, 
	        header: {
	        	title: "분류체계"
	        },
	        width: 200,
	        on: {
	        	onItemClick: this.clickTree.bind(_this)
	        }
	    });
		
		var searchForm = createFormComp({
			id: _this.popup_object_id + "_searchForm",
			elementsConfig: {
	    		labelPosition: "left"
	    	},
			header: {
				title: "검색",
				icon: "search"
			},
			footer: {
				buttons: {
					align: "center",
					items: [
						createBtnComp({ label:"검 색", type: "form", click: this.search.bind(_this) }),
						createBtnComp({ label:"초기화", click: this.searchClear.bind(_this) })
					],
    		        css: "webix_layout_form_bottom"
				}
			},
			fields: {
				colSize: 2,
				items: [
			            { item : createUnionFieldComp({
		                            items: [
		                                createCodeComboBoxComp({
		                    				label: '조회구분',
		                    				name: 'search_type',
		                    				code_grp_id: 'SEARCH_'+_CLASS_TYPE+'_STATISTICS',
		                    				width: 200,
		                    				attachChoice: true,		                    			
		                    			}),
		                    			createTextFieldComp({
		                    				name: "search_value",
		                    				width: 200,
		                    				on:{
		       									onKeyPress: this.searchByEnterKey.bind(_this)
		     								}
		                    			}),
		                    			createCodeComboBoxComp({
		                    				label: '자산상태', 
		                    				name: 'asset_state', 
		                    				code_grp_id: 'ASSET_STATE', 
		                    				value: 'ACTIVE',
		                    				width: 200,
		                    				readOnly: true
		                    			})		                    			
		                            ]
		                        })
		              	}
			        ]
			}
		});
		
		/**
		 *  분류체계별 그리드 분기 처리
		 */
		
		var gridParams = {class_type : _CLASS_TYPE, class_mng_type : _CLASS_MNG_TYPE, class_id : _CLASS_ID};
		var grid = createGridComp({
			id : _this.popup_object_id + "_grid",
			resizeColumn : true,
			pageable : true,
			pageSize : 50,
			url : "/itg/itam/statistics/searchAssetList.do",
			resource : "grid.itam.statistics.AM"  + _CLASS_TYPE,
			params : gridParams,
			header : {
				title : "자산목록"
			},
			on:{
				onItemDblClick: this.choice.bind(_this)
			}
		});
		
		_this.window = createWindow({
	    	id: _this.popup_object_id,
	    	width: 950,
	    	header: {
	    		title: "자산 목록 조회"
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: tree
	                },{
	                    rows: new Array().concat(searchForm, grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: true
	    });
	};
	
	// 트리노드 클릭 이벤트
	this.clickTree = function(id, e){
		
		var treeItem = $$(this.popup_object_id + "_tree").getItem(id);
		
		var form = $$(this.popup_object_id + "_searchForm");
		var searchParams = form._getValues();
		
		searchParams.class_id = treeItem.node_id;
		searchParams.class_type = treeItem.class_type;
		searchParams.class_mng_type = _CLASS_MNG_TYPE;
		
		$$(this.popup_object_id + "_grid")._reload(searchParams);
	};
	
	// 값 선택 이벤트
	this.choice = function(){
		
		var selectedItem = $$(this.popup_object_id + "_grid")._getSelectedRows();
		if(selectedItem.length < 1) {
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
			
			var values = {};
			values[this.field_id] = selectedItem[0].CONF_ID;
			values[this.name_field_id] = selectedItem[0].CONF_NM;
		    $$(prop.target_form_id)._setValues(values)
	        this.window.close();				 
	};
	
	// 검색 이벤트
	this.search = function(){
		var searchParams = $$(this.popup_object_id + "_searchForm")._getValues();
		var treeParams = $$(this.popup_object_id + "_tree").getSelectedItem();

		if(typeof treeParams != "undefined") {
			searchParams._CLASS_ID = treeParams.node_id;
			searchParams._CLASS_TYPE = treeParams.class_type;
		}
		
		searchParams["class_id"] = _CLASS_ID;
		searchParams["class_type"] = _CLASS_TYPE;
		searchParams["class_mng_type"] = _CLASS_MNG_TYPE;
		searchParams["tangible_asset_yn"] = 'Y';
		$$(this.popup_object_id + "_grid")._reload(searchParams);
	};
	
	// 엔터키 검색 이벤트
	this.searchByEnterKey = function(keyCode){
		if(keyCode){
			if(keyCode == "13"){
				this.search();
			}	
		}
	};
	
	// 검색창 초기화 이벤트
	this.searchClear = function(){
		$$(this.popup_object_id + "_searchForm")._reset();
	};
	
};

function createConfWindow(popupProps) {
	var props = popupProps;
	var _this = this;
	
	var popupHandler;
	var popup;
	
	try {
		switch (_this.props.popup_type) {
//			case "process":
//				user = new nkia.ui.popup.system.ProcessUser();
//				break;
//			case "multitask":
//				user = new nkia.ui.popup.system.MultiTaskUser();
//				break;
//			case "multiTaskTemp":
//				user = new nkia.ui.popup.system.MultiTaskTempUser();
//				break;	
//			case "dependencyUser":
//				user = new nkia.ui.popup.system.dependencyUser();
//				break;
//			case "workReqDoc":
//				user = new nkia.ui.popup.system.WorkReqDoc();
//				break;
//			case "grpOperDetail":
//				user = new nkia.ui.popup.system.GrpOperDetail();
//				break;
//			case "singleOperDetail":
//				user = new nkia.ui.popup.system.SingleOperDetail();
//				break;
			default:
				popupHandler = new nkia.ui.popup.itam.Nic(_this.props);
//				user = new nkia.ui.popup.itam.Conf();
		}
		if(popupHandler != null) {
			popup = popupHandler.openPopup();
		}
		
	} catch(e) {
		alert(e.message);
	}
}
