<!DOCTYPE HTML>
<html>
<head>
<!-- 
/**
* 자산 입고 요청
* @author <a href="mailto:ejjwa@nkia.co.kr"> Eunjin Jwa
* 
*/
-->
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
<script type="text/javascript" charset="utf-8">
webix.ready(function() {
	
	var searchForm = createFormComp({
		id: "searchForm",
		elementsConfig: {
    		labelPosition: "left" // 라벨 좌측위치를 위해 입력, 검색폼은 왼쪽 정렬로 사용합니다.
    	},
		header: {
			title: "검색",
			icon: "search", // 검색폼은 헤더에 아이콘을 넣어줍니다.
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검 색", type: "form", click: function(){searchList(); }  }),
					createBtnComp({ label:"초기화", click: function() { resetFormData(); } })
				],
		        css: "webix_layout_form_bottom" // 검색폼의 Footer CSS 문제 때문에 좌측 CSS를 입력해주셔야 합니다.
			}
		},
		fields: {
			colSize: 3, // 열 수
			items: [
				{ colspan: 2, item: createSearchDateFieldComp({label: "요청일", name: "search_ins_dt", dateType:"dd", required: true}) },
				{ colspan: 1, item: createTextFieldComp({name: "DUMMY"}) }
			]
		}
	});
	
	var tempSeqDelBtn=   createBtnComp({visible: true, label: '#springMessage("btn.common.delete")', type:'form', click:  "fn_deleteTempSeq" })
	var tempSeqGrid = createGridComp({
		id : 'tempSeqGrid',
		header: {
			title: "일괄 등록 요청 목록",
			buttons: {
				items: [
				        tempSeqDelBtn
				]
			}
		},
		height: 220,
		pageable: false,
		pageSize: 10,
		autoLoad: false,
		params: {status: 'REGIST'},
		url: "/itg/itam/introduction/searchTempSeqList.do",
		resource: "grid.itam.assetExcel.temp.seq",
		on: {
			onItemClick: function(row){
				var rowData = this.getItem(row);
				tempSeqGridCellClick(rowData);
			}
		}
	});
	
	var btn1= createBtnComp({visible: true, label: '입고 확정', type:'form', click:  "fn_cnfirmTempAsset" })
	var btn2=   createBtnComp({visible: true, label: '#springMessage("btn.common.delete")', type:'form', click:  "fn_deleteTempAsset" })
	var tempAssetGrid = createGridComp({
		id : 'tempAssetGrid',
		header: {
			title: "입고 대상 자원 목록",
			buttons: {
				items: [
				        btn1,
				        btn2
				]
			}
		},
		pageable: true,
		pageSize: 50,
		autoLoad: false,
		checkbox : true,
		url: "/itg/itam/introduction/searchTempAssetList.do",
		resource: "grid.itam.assetExcel.temp.asset",
		on: {
			onItemDblClick: function(row){
				var rowData = this.getItem(row);
				viewAssetDetailTab(rowData);
			}
		}
	});
	
	var tempAllAssetGrid = createGridComp({
		id : 'tempAllAssetGrid',
		header: {
			title: "일괄 등록 자원 목록(전체)",
		},
		pageable: true,
		pageSize: 50,
		autoLoad: false,
		url: "/itg/itam/introduction/searchTempAssetList.do",
		resource: "grid.itam.assetExcel.temp.asset.all",
	});
	
	/**
	 * == 자산 목록 탭 생성 ==
	 */  
	// 자산 목록 탭 bar
	var tabGridBar = [{
		id: "tabGrid1",
		value: "입고 대상 자원 목록",
	},{
		id: "tabGrid2",
		value: "일괄 등록 자원 목록(전체)",
	}];
	
	// 자산 목록 탭 body
	var tabGridBody = [{
		id: "tabGrid1", // 개별 탭의 id, tabBar에서 부여한 id와 동일하게 세팅하여야 합니다.
		rows: tempAssetGrid
	},{
		id: "tabGrid2", // 개별 탭의 id, tabBar에서 부여한 id와 동일하게 세팅하여야 합니다.
		rows: tempAllAssetGrid
	}];
	
	var tabGridComp = {
			view:"tabview", // view 형식 : tabview로 입력
			id:"tabGridComp", // 탭의 ID
			animate:false,
			tabbar: {
				id: 'tabGridBarComp'
				, options: tabGridBar
			},
			cells: tabGridBody
		};
	
	var innerTabBar = [{
		id: "tab1",
		value: "자산 입고 요청",
		css: "inner_tab" // <- 작은탭 구현시 필수
	}];
	
	// 탭의 body 를 구성합니다.
	var tabItem1 = [{
		id: "tab1", // 개별 탭의 id, tabBar에서 부여한 id와 동일하게 세팅하여야 합니다.
		rows: new Array().concat(searchForm, tabGridComp)
	}];
	
	// 실제 탭을 구성합니다.
	var tabComp = {
		view:"tabview", // view 형식 : tabview로 입력
		id:"tabComp", // 탭의 ID
		animate:false,
		tabbar: {
			id: 'tabBarComp'
			, options: innerTabBar
			, multiview: true
			, height: 25 
		},
		multiview: {
			id: 'views',
			keepViews:true
		},
		cells: tabItem1
	};
	
	var view = {
		view: "scrollview",
		id: "app",
		body: {
			rows: [{
				cols: [{
					rows: new Array().concat(tabComp)
				}]
			}
		    ]
		}
	};
	
	nkia.ui.render(view);
	$$("searchForm")._hideFields(['DUMMY']);
	
	var date = new Date();
	var startDate = new Date();
	startDate.setMonth(date.getMonth() - 1)
	$$("searchForm")._setFieldValue('search_ins_dt_startDate', startDate);
	$$("searchForm")._setFieldValue('search_ins_dt_endDate', date);
	
	searchList();
	
});

function searchList(){
	if(!$$("searchForm")._validate()) return false;

	var paramMap = $$("searchForm")._getValues();
	
	$$("tempAllAssetGrid")._reload(paramMap);
	paramMap['STATUS'] = 'CONFIRM';
	$$("tempAssetGrid")._reload(paramMap);
} 

function resetFormData(){
	 $$("searchForm")._reset();
}

function tempSeqGridCellClick(rowData){
	var paramMap = {};
	
	$$("tempAllAssetGrid")._reload(paramMap);
	paramMap['STATUS'] = 'CONFIRM';
	$$("tempAssetGrid")._reload(paramMap);
}


//임시 자산 검토 완료 처리
function fn_cnfirmTempAsset(){
var assetList = $$("tempAssetGrid")._getSelectedRows();
	
	if(assetList.length < 1) {
		nkia.ui.utils.notification({
			type: 'error',
			message: "선택된 자산이 없습니다."
		});
		return;
	}
	
		
	var paramMap = {
			paramList : assetList
	}
	if(confirm("선택된 자산을 입고 요청하시겠습니까?")) {
		// 실제 데이터 등록 ajax
		nkia.ui.utils.ajax({
			viewId: 'app',
			isMask: true,
			url: "/itg/itam/introduction/insertOpmsInfoData.do",
			params: paramMap,
			success: function(response){
				nkia.ui.utils.notification({
					message: '정상 처리 되었습니다. <br/>등록된 자원은 [운영IT자원 조회] 메뉴에서 확인 가능합니다.'
				});
				$$("tempAssetGrid")._reload();
				$$("tempAllAssetGrid")._reload();
			}
		});
	}
}

// 자산 상세화면 호출
function viewAssetDetailTab(rowData){
	
	var asset_id 		= rowData.ASSET_TEMP_ID;
	var conf_id 		= rowData.CONF_TEMP_ID;
	var class_id		= rowData.CLASS_ID;
	var class_type	= rowData.CLASS_TYPE;
	var conf_nm		= rowData.HOST_NM;
	var logical_yn		= rowData.LOGICAL_YN;
	var tangible_asset_yn	= rowData.TANGIBLE_ASSET_YN;
	
	var url = "/itg/itam/automation/goAssetDetailViewer.do";
	var view_type		= "infmodifiy";
	
	 $$("views").addView({
		  id: conf_id
		 , view : 'iframe'
		, src: url + "?asset_id="+asset_id+"&conf_id="+conf_id+"&class_id="+class_id+"&class_type="+class_type+"&view_type="+view_type+"&entity_class_id="+class_id+"&tangible_asset_yn="+tangible_asset_yn
	 });

	var tabBarComp =$$('tabBarComp');
	// 탭바 추가
	 $$('tabBarComp').addOption({
		 id: conf_id,
		 value: conf_nm,
		 close: true,
		 css: "inner_tab"
	 }, true);
	
	 $$("tabBarComp").refresh();	// refresh를 해야 tabbar가 보여짐
	
}

function reloadList(){
	$$("tempAssetGrid")._reload();
}

function closeTab(){
	var id=$$("tabComp").getValue();
	 $$("tabComp").removeView(id);
}


//임시 등록 요청 삭제
function fn_deleteTempSeq(){
	var gridList = $$("tempSeqGrid")._getSelectedRows();
	if(gridList.length < 1) {
		nkia.ui.utils.notification({
			type: 'error',
			message: "선택된 요청이 없습니다."
		});
		return;
	}
	
	if(confirm("선택된 요청을 삭제하시겠습니까?")) {
		var paramMap = {
				paramList : gridList
		}
		
		// 실제 데이터 등록 ajax
		nkia.ui.utils.ajax({
			viewId: 'app',
			url: "/itg/itam/introduction/deleteTempSeq.do",
			params: paramMap,
			success: function(response){
				nkia.ui.utils.notification({
					message: '#springMessage("msg.common.result.00004")'
				});
				$$("tempSeqGrid")._reload();
				$$("tempAllAssetGrid")._removeAllRow();
				$$("tempAssetGrid")._removeAllRow();
			}
		});
	}
	
}

//임시 자산 삭제 처리
function fn_deleteTempAsset(){
	var assetList = $$("tempAssetGrid")._getSelectedRows();
	var paramMap = {
			paramList : assetList
	}
	
	if(assetList.length < 1) {
		nkia.ui.utils.notification({
			type: 'error',
			message: "선택된 자산이 없습니다."
		});
		return;
	}
	
	if(confirm("선택된 자산을 삭제하시겠습니까?")) {
		// 실제 데이터 등록 ajax
		nkia.ui.utils.ajax({
			viewId: 'app',
			url: "/itg/itam/introduction/deleteTempAsset.do",
			params: paramMap,
			success: function(response){
				nkia.ui.utils.notification({
					message: '#springMessage("msg.common.result.00004")'
				});
				$$("tempAssetGrid")._reload();
				$$("tempAllAssetGrid")._reload();
			}
		});
	}
}


</script>
</body>
</html>		
