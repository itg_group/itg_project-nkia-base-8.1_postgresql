/********************************************
 * Date: 2019-02-01
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  분류체계별 속성권한 부분 지정 팝업
 ********************************************/
nkia.ui.popup.itam.amdbauth.setPartGrpAuth = function() {
	// 속성 Properties
	this.id = "";
	this.title = "권한속성 상세설정";
	this.closable = true;
	this.width = 1000;
	this.height = nkia.ui.utils.getWidePopupHeight();
	this.popupParamData = {};
	this.callbackFunc = "";	
	// 구성요소 Properties
	this.window = {};
	this.callbackFunc;		// 저장 후 실행 함수
	
	var entity_id = '';
	var user_grp_id = '';
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
		
		entity_id = this.popupParamData.ENTITY_ID;
		user_grp_id = this.popupParamData.USER_GRP_ID;
	};
	
	this.setRender = function(props) {
		
		// 그룹속성그리드
		this.grpAttrGrid = createGridComp({
			id: this.id + "_grpAttrGrid",
			header: {
				title: "그룹속성정보"
			},
			resizeColumn : true,
			pageable : false,
			url : "/itg/itam/amdb/selectGrpAttrGrid.do",
			resource : "grid.itam.grpAttrGrid",
			checkbox : true,
			autoLoad: true,
			params : {
				entity_id : entity_id ,
				user_grp_id : user_grp_id
			}
		});
		
		// 수정속성그리드
		this.grpAttrUpdGrid = createGridComp({
			id: this.id + "_grpAttrUpdGrid",
			header: {
				title: "수정속성정보",
				buttons: {
					items: [
						createBtnComp({
							id:"btn1",
							label:"제외",
							type: "form",
							click: function() {
								var sourceGridId = _this.id + "_grpAttrUpdGrid";
								var targetGridId = _this.id + "_grpAttrGrid";
				 				moveGridData(sourceGridId, targetGridId);
							}
						})]
				}
			},
			resizeColumn : true,
			pageable : false,
			url : "/itg/itam/amdb/selectGrpAttrAuthSelGrid.do",
			resource : "grid.itam.grpAttrGrid",
			checkbox : true,
			autoLoad: true,
			params : {
				entity_id : entity_id ,
				user_grp_id : user_grp_id,
				auth_type : 'U'
			},
			keys: ["COL_ID"]
		});
		
		// 조회속성그리드
		this.grpAttrViewGrid = createGridComp({
			id: this.id + "_grpAttrViewGrid",
			header: {
				title: "조회속성정보",
				buttons: {
					items: [
						createBtnComp({
							id:"btn2",
							label:"제외",
							type: "form",
							click: function() {
								var sourceGridId = _this.id + "_grpAttrViewGrid";
								var targetGridId = _this.id + "_grpAttrGrid";
				 				moveGridData(sourceGridId, targetGridId);
							}
						})]
				}
			},
			resizeColumn : true,
			pageable : false,
			url : "/itg/itam/amdb/selectGrpAttrAuthSelGrid.do",
			resource : "grid.itam.grpAttrGrid",
			checkbox : true,
			autoLoad: true,
			params : {
				entity_id : entity_id ,
				user_grp_id : user_grp_id,
				auth_type : 'R'
			},
			keys: ["COL_ID"]
		});
		
		var _this = this;
		
		// 수정속성정보 ADD 버튼
		this.addUpdAttrBtn = { // 화살표 버튼
		        view: "template",
		 		template: nkia.ui.html.icon("arrow_right", "addUpdAttr"),
		 		width: 30,
		 		onClick: { // 이벤트 연결
		 			"addUpdAttr": function(){
		 				var sourceGridId = _this.id + "_grpAttrGrid";
		 				var targetGridId = _this.id + "_grpAttrUpdGrid";
		 				var selRows = $$(sourceGridId)._getSelectedRows();
		 				
		 				if(selRows.length == 0){
		 					nkia.ui.utils.notification({
		 						type: 'error',
		 						message: '선택된 정보가 없습니다.'
		 					});
		 					return false;
		 				}
		 				
		 				// 출력유형이 '조회(R)' 인 속성은 수정속성정보가 될 수 없다.
		 				var addTargetList = [];
		 				for(var i = 0; i < selRows.length; i++){
		 					if(selRows[i].DISPLAY_TYPE != "R"){
		 						addTargetList.push(selRows[i]);
		 						$$(sourceGridId)._removeRow(selRows[i].id);
		 					}
		 				}
		 				if(addTargetList.length == 0){
		 					nkia.ui.utils.notification({
		 						type: 'error',
		 						message: '수정속성은 출력유형이 수정인것만 추가하실 수 있습니다.'
		 					});
		 				}else{
		 					$$(targetGridId)._addRow(addTargetList);
		 				}
		 			}
		 		}
		    };
		
		// 조회속성정보 ADD 버튼
		this.addViewAttrBtn = { // 화살표 버튼
		        view: "template",
		 		template: nkia.ui.html.icon("arrow_right", "addViewAttr"),
		 		width: 30,
		 		onClick: { // 이벤트 연결
		 			"addViewAttr": function(){
		 				var sourceGridId = _this.id + "_grpAttrGrid";
		 				var targetGridId = _this.id + "_grpAttrViewGrid";
		 				moveGridData(sourceGridId, targetGridId);
		 			}
		 		}
		    };
	} // end setRender
	
	
	
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.grpAttrGrid)
					},{
						rows: new Array().concat(this.addUpdAttrBtn, this.addViewAttrBtn)
					},{
	                	width: 450,
	                    rows: new Array().concat(this.grpAttrUpdGrid, this.grpAttrViewGrid)
	                }
					]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '저 장', type: "form", click: this.registGrpAttr.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
		
	} // end setWindow
	
	
	/**
	 * 권한속성상세설정 저장
	 */
	this.registGrpAttr = function(){
		var _this = this;
		
		var grpAttrRows = $$(_this.id + "_grpAttrGrid")._getRows();
		var grpAttrUpdRows = $$(_this.id + "_grpAttrUpdGrid")._getRows();
		var grpAttrViewRows = $$(_this.id + "_grpAttrViewGrid")._getRows();

		if(grpAttrUpdRows.length == 0 && grpAttrViewRows.length == 0){
			nkia.ui.utils.notification({
				type: 'error',
				message: '수정/조회 속성을 선택하여주십시오.'
			});
			return false;
		}
		if(!confirm('저장하시겠습니까?')){
			return false;
		}

		var dataMap = {
				entity_id : entity_id,
				user_grp_id : user_grp_id,
				updAuthAttrList : grpAttrUpdRows,
				viewAuthAttrList : grpAttrViewRows
		};
		
		nkia.ui.utils.ajax({
			viewId: "scrollview",
			url: "/itg/itam/amdb/insertPartGrpAuth.do",
			params: dataMap,
			async: false,
			isMask: true,
			success: function(response){
				nkia.ui.utils.notification({
					message: '권한속성이 저장되었습니다.'
				});
				_this.window.close();
				if(_this.callbackFunc){
					_this.callbackFunc();
				}
			}
		});
		
	}
	
	/**
	 * 그리드간 데이터 이동
	 */
	function moveGridData(sourceGridId, targetGridId){
		var selRows = $$(sourceGridId)._getSelectedRows();
		if(selRows.length == 0){
			nkia.ui.utils.notification({
				type: 'error',
				message: '선택된 정보가 없습니다.'
			});
			return false;
		}
		$$(targetGridId)._addRow(selRows);
		$$(sourceGridId)._removeRow();
	}
	
}; // nkia.ui.popup.itam.amdbauth.setPartGrpAuth


/**
 * 분류체계별 권한관리 팝업 생성
 * @param {}
 */
function createAmdbAuthWindow(popupProps) {
	var props = popupProps||{};
	var popupObject;
	try {
		switch (props.type) {
			case "partGrpAuth":	// 부분 권한 설정 팝업
				popupObject = new nkia.ui.popup.itam.amdbauth.setPartGrpAuth;
				break;
			default:
				popupObject = new nkia.ui.popup.itam.amdbauth.setPartGrpAuth;
		}
		popupObject.setProps(props);
		popupObject.setRender(props);
		popupObject.setWindow();
		
	} catch(e) {
		alert(e.message);
	}
}


