<!-- 
	자산/구성관리 > 기준정보관리 > 서비스관리 
	@version 1.0
	@author 장성수
	@since 2018. 03. 23
-->
<!doctype html>
<html lang="ko">
<head>
<!-- ITG Base Page parse -->
#parse("/itg/base/constraints/itg_constraint.nvf")
<!-- ITG Base Javascript parse -->
#parse("/itg/base/include/page_webix_inc.nvf")
</head>
<body>
#parse("/itg/base/include/page_loading.nvf")
<script type="text/javascript" charset="utf-8">
var baseService_id = "0000000";
webix.ready(function(){

	var fieldItems = [];
	var buttonItems = [];
	
	/***************************************************************************
	 * 트리 영역 : [Tree]
	 **************************************************************************/	
	
	// 고객사정보 트리 영역
	var selCustTree = createTreeComponent({
		id : 'selCustTree',
		url : '/itg/itam/statistics/searchCustTree.do',
		select: true,
		width: 300,
		expColable: true, // 전체펼침/접기 표시 여부
		filterable: true, // 검색필드 표시 여부
		expandLevel: 2,
		header: {
			title: '#springMessage("res.title.system.cust")' //고객사정보
		},
		on: {
        	onItemClick: custTreeNodeClick
        }
	});

	
	/***************************************************************************
	 * 서비스 관리정보 영역 : [Tree]
	 **************************************************************************/	

	buttonItems = [];
	// 신규등록
	buttonItems.push(createBtnComp({id:'serviceInsertModeBtn',label: '#springMessage("btn.common.newInsert")',type:'form',visible:false,click: function() { actionLink('modeChange'); }}));
	
	// 서비스관리정보 트리 영역
	var amdbServiceTree = createTreeComponent({
		id: 'amdbServiceTree',
		select: true,
		drag: true,
		width: 300,
		url: '/itg/itam/amdb/searchAmdbServiceAllTree.do',
		expandLevel: 2,
		on: {
			onItemClick: treeNodeClick
		},
		params: {
			service_id: ' ' //첫 로드시 select 0개
		},
		expColable: false, // 전체펼침/접기 표시 여부
		header: {
			title: '#springMessage("res.label.itam.service.00001")', //서비스관리정보
			buttons: {
				items: buttonItems
			}
		}
	});
	
	
	/***************************************************************************
	 * 서비스 정보 영역 : [Form]
	 **************************************************************************/
	
	
	fieldItems = [];
	// 서비스 ID
	fieldItems.push({colspan:1,item: createTextFieldComp({label:'#springMessage("res.label.itam.service.00005")', name:'service_id', readonly:true}) });
	// 정렬순서
	fieldItems.push({colspan:1,item: createTextFieldComp({label:'#springMessage("res.label.itam.service.00008")', name:'display_no', required:true}) });
	// 상위 서비스명
	fieldItems.push({colspan:1,item: createTextFieldComp({label:'#springMessage("res.label.itam.service.00004")', name:'up_service_nm', readonly:true, maxsize:10}) });
	// 서비스영향도
	//fieldItems.push({colspan:1,item: createTextFieldComp({label:'#springMessage("res.label.itam.service.00007")', name:'service_effect', maxsize:20}) });
	// 서비스명
	fieldItems.push({colspan:1,item: createTextFieldComp({label:'#springMessage("res.label.itam.service.00006")', name:'service_nm', required:true, maxsize:200}) });
	// 사용여부
	fieldItems.push({colspan:1,item: createCodeRadioComp({id:'use_yn_radio',label:'#springMessage("res.label.itam.service.00009")', name:'use_yn', code_grp_id:'YN',required:true}) });
	// 비고
	fieldItems.push({colspan:1,item: createTextAreaFieldComp({label:'#springMessage("res.label.itam.service.00010")', name:'service_desc', resizable: false, maxsize: 2000}) });
	
	
	
	var setEditUserProp = createFormComp({
		id: 'setEditUserProp',
		header: {
			title: '#springMessage("res.label.itam.service.00017")' // 서비스정보
		},
		fields: {
			colSize: 2,
			items: fieldItems,
			hiddens: {
				cust_id: ' ',
				up_service_id: ' ',
				cust_nm: ' '
			}
		}
	});
	
	/***************************************************************************
	 * 하단 탭 영역 : [Form]
	 **************************************************************************/
	
	fieldItems = [];
	// 용도
	fieldItems.push({colspan: 1, item: createCodeComboBoxComp({label: '용도', name: 'business_type_cd', code_grp_id: 'BUSINESS_TYPE_CD', attachChoice: true, on: {
		onChange: function() { actionLink('searchAsset'); }
	}})});
	
	var assetSearchForm = createFormComp({
		id: 'assetSearchForm',
		elementsConfig: {
    		labelPosition: "left"
    	},
    	header: {
			title: "검색",
			icon: "search"
		},
		fields: {
			colSize: 2,
			items: fieldItems
		}
	});
	
	buttonItems = [];
	//구성정보연계
	buttonItems.push(createBtnComp({id:'assetInsertBtn',label:'#springMessage("btn.itam.00044")',visible:false,autowidth:true,click: function() { actionLink('assetInsert'); } }) );
	//토폴로지
	buttonItems.push(createBtnComp({id:'topologyBtn',label:'#springMessage("btn.itam.00040")',visible:false,click: function() { actionLink('topology'); } }) );
	
	// 연계구성정보 그리드
	var assetListGrid = createGridComp({
		id : 'assetListGrid',
		resizeColumn : true,
		pageable: false,
		autoLoad: false,
		// 탭 그리드 사용시 badge id를 세팅해주어야 합니다.
		// badge : 그리드 탭에 표시되는 데이터 카운트정보
		badgeId: 'assetListGrid_badge',
		url : '/itg/itam/amdb/searchServiceToRelConfList.do',
		resource : 'grid.cipop.asset',
		header: {
			title: '#springMessage("res.label.itam.service.00020")', // 연계구성정보
			buttons: {
				items: buttonItems
			}
		},
		on:{
			onItemDblClick: relAssetGridCellDblClick
		}
	});
	
	
	buttonItems = [];
	// 담당자추가
	buttonItems.push(createBtnComp({id:'userInsertBtn',label:'#springMessage("btn.itam.00013")',visible:false,click: function() { actionLink('userInsert'); } }) );
	// 순서초기화
	buttonItems.push(createBtnComp({id:'orderRollBackBtn',label:'#springMessage("btn.system.00009")',visible:false,click: function() { actionLink('orderReset'); } }) );
	
	// 담당자정보 그리드
	var userListGrid = createGridComp({
		id : 'userListGrid',
		resizeColumn : true,
		pageable: false,
		autoLoad: false,
		drag: true,
		// 탭 그리드 사용시 badge id를 세팅해주어야 합니다.
		// badge : 그리드 탭에 표시되는 데이터 카운트정보
		badgeId: 'userListGrid_badge',
		url : '/itg/itam/amdb/searchAmdbServiceGridList.do',
		resource : 'grid.itam.service',
		header : {
			title: '#springMessage("res.label.itam.service.00002")', // 담당자정보
			buttons: {
				items: buttonItems
			}
		},
		actionColumns : [{
			column: 'ACTION',
			template: nkia.ui.html.icon('remove', 'remove_row'),
			onClick: {
				'remove_row': function(e, row, html){
					$$('userListGrid')._removeRow(row);
					return false;
				}
			}
		}]
	});
	
	
	
	buttonItems = [];
	// 순서초기화
	buttonItems.push(createBtnComp({id:'lowServiceRollbackBtn',label:'#springMessage("btn.system.00009")',visible:false,click: function() { actionLink("lowServiceRollback"); } }) );
	
	// 하위서비스목록
	var lowServiceListGrid = createGridComp({
		id : 'lowServiceListGrid',
		resizeColumn : true,
		pageable: false,
		autoLoad: false,
		drag: true,
		// 탭 그리드 사용시 badge id를 세팅해주어야 합니다.
		// badge : 그리드 탭에 표시되는 데이터 카운트정보
		badgeId: 'lowServiceListGrid_badge',
		url : '/itg/itam/amdb/searchAmdbServiceLowServiceList.do',
		resource : 'grid.itam.service.lowService',
		header: {
			title: '#springMessage("res.label.itam.service.00018")', // 하위서비스목록 
			buttons: {
				items: buttonItems
			}
		}
	});
	
	
	var innerTabBar = [{
		id: 'assetListGrid_tab',
		value: '#springMessage("btn.itam.00045")', //연계구성정보
		css: 'inner_tab' // <- 작은탭 구현시 필수
	},{
		id: 'userListGrid_tab',
		value: '#springMessage("res.label.itam.service.00002")', //담당자정보
		css: 'inner_tab'
	},{
		id: 'lowServiceListGrid_tab',
		value: '#springMessage("res.label.itam.service.00018")', //하위서비스목록
		css: 'inner_tab'
	}];
	
	var innerTabItem = [{
		id: 'assetListGrid_tab',
		rows: new Array().concat(assetSearchForm, assetListGrid)
	},{
		id: 'userListGrid_tab',
		rows: userListGrid
	},{
		id: 'lowServiceListGrid_tab',
		rows: lowServiceListGrid
	}];
	

	var innerTabGrid = {
		view:'tabview',
		id:'sampleInnerTab',
		animate:false,
		tabbar:{
			options: innerTabBar,
			height: 25, // <- 작은탭 구현 시 25값으로 필수입력
			optionWidth: 150 // <- 작은탭 구현 시 150값으로 필수입력
		},
		cells:new Array().concat(innerTabItem)
	};


	var buttonItems_footer = [];
	buttonItems_footer.push(createBtnComp({id:'serviceUpdateBtn',label:'#springMessage("btn.common.insert")',type:'form',visible:false,click: function() { actionLink('update'); } }) );
	buttonItems_footer.push(createBtnComp({id:'serviceDeleteBtn',label:'#springMessage("btn.common.delete")',type:'base',visible:false,click: function() { actionLink('delete'); } }) );
	buttonItems_footer.push(createBtnComp({id:'serviceInsertBtn',label:'#springMessage("btn.common.insert")',type:'form',visible:false,click: function() { actionLink('insert'); } }) );
	buttonItems_footer.push(createBtnComp({id:'serviceInsertBackBtn',label:'#springMessage("btn.common.cancel")',type:'base',visible:false,click: function() {actionLink('back'); } }) );
	
	var view = {
		view: 'scrollview',
		id: 'app',
		body: {
			rows: [{
				cols: [{
					rows: selCustTree
				},{
					rows: amdbServiceTree
				},{
					rows: [
						{
						cols: [{
							rows: new Array().concat(setEditUserProp,innerTabGrid)
						}]
						},
						createButtons({
							align: 'center',
							items: buttonItems_footer
						})
					]
				}]
			}]
		}
	};

	nkia.ui.render(view);

}); // end of webix.ready
</script>









<script type="text/javascript" charset="utf-8">
/*******************************************************************************
 * 주요함수 모음
 ******************************************************************************/

var paramsMap = {}; // 현재 데이터 저장 > 취소시 사용
var paramMap = {};
var comboCodeId = baseService_id;  // 서비스 등록시 선택된 노드의 서비스ID를 기억하고 있는 변수
var codeId = baseService_id;	   // 마지막에 선택된 콤보박스의 코드ID를 기억하고 있는 변수 
var treeNodeLv = null;
var oldOrderList = []; //정렬순서 변경에 사용하는 변수


function actionLink(command) {
	var theForm = $$('setEditUserProp');
	if(comboCodeId == null){
		nkia.ui.utils.notification({
			type:'error', 
			message:'#springMessage("msg.itam.service.00009")' //서비스를 선택해주세요.
		});
		return false;
	}
	switch(command) {
		case 'modeChange' :	
			// 현재 데이터 저장 > 취소시 사용
			paramsMap = theForm._getValues();
			
			var node = $$('amdbServiceTree')._getSelectedItem();
			var itemCnt = $$('amdbServiceTree').count();
			
			// 서비스가 클릭되어 있지 않으면 에러메세지
			if(itemCnt > 0 && node == undefined){
				nkia.ui.utils.notification({
					type:'error',
					message:"#springMessage('msg.itam.service.00013')" //상위서비스를 선택해주세요.
				});
				return false;
			}

			$$('serviceDeleteBtn').hide();
			$$('serviceUpdateBtn').hide();
			
			$$('serviceInsertBtn').show();
			$$('serviceInsertBackBtn').show();
			$$('userInsertBtn').show();
			//$$('orderRollBackBtn').show();
			$$('lowServiceRollbackBtn').show();
			$$('assetInsertBtn').show();
			$$('topologyBtn').show();	
			
			$$('assetListGrid')._removeAllRow();
			$$('userListGrid')._removeAllRow();
			$$('lowServiceListGrid')._removeAllRow();
			theForm._reset();
			
			theForm._setControl({
				editorMode: true,
				exceptions: ['cust_nm','service_id','display_no','up_service_nm']
			});
			
			//상위 있는 경우만 상위 서비스명 세팅
			if(itemCnt != 0) theForm._setFieldValue('up_service_nm', node.text);
			theForm._setFieldValue('use_yn','Y');
			break;
		case 'insert' :
			if(theForm._validate()){
				if(!confirm('#springMessage("msg.common.confirm.00004")')){
					return false;
				}
								
				var node = $$('selCustTree')._getSelectedItem();
				var cust_id = node.node_id;
				var cust_nm = node.text;
				
				theForm._setValues({'cust_id' : cust_id});
				theForm._setValues({'cust_nm' : cust_nm});
				
				//폼데이터 가져오기
				paramMap = {};
				var map = {};
				var items = $$('userListGrid')._getRows();
				
				
				var nodeId = $$('amdbServiceTree')._getSelectedItem();
				if(nodeId == undefined){
					theForm._setValues({'up_service_id' : null});
				}else{
					theForm._setValues({'up_service_id' : nodeId.node_id});
				}
				
				paramMap[0] = theForm._getValues();
				
				var assetItems =$$('assetListGrid')._getRows();
				var assetArr;
				if(assetItems.length > 0){
					for(var i=0; i < assetItems.length; i++){
						if( i == 0){
							assetArr = "'"+assetItems[i].CONF_ID+"'";
						}else{						
							assetArr = assetArr+",'"+assetItems[i].CONF_ID+"'";
						}
					}
				}else{
					assetArr = "''";
				}
				var formParam = paramMap[0];
				formParam['assetArr'] = assetArr;
				
				for(var i=0; i<=items.length-1;i++){
					//키값이 대문자로 들어오기 때문에 소문자로 변환
					for(var key in items[i]){
						map = items[i];
						map[key.toLowerCase()] = map[key];
						delete map[key]; //대문자 key removeAll
					}
					paramMap[i+1] = map;
				}
				
				
				nkia.ui.utils.ajax({
					url: '$!{context}/itg/itam/amdb/insertAmdbServiceInfo.do',
					params: paramMap,
					async: false,
					success: function(data){
						gridReload('insert');
						nkia.ui.utils.notification({
							type:'info', 
							message: data.resultMsg
						});
					}
				});
			}else{
				nkia.ui.utils.notification({
					type:'error', 
					message:"#springMessage('msg.common.00012')" //누락 및 입력된 항목을 확인해주세요.
				});
			}
			break;
		case 'update' :
			if(theForm._validate()){
				
				//정렬순서 숫자 정수 체크
				var display_no = theForm._getFieldValue('display_no');
				if(display_no != parseInt(display_no,10)){
					
					nkia.ui.utils.notification({
						type:'error', 
						message:'#springMessage("msg.itam.service.00016")' //정렬순서 값은 정수만 입력해 주세요.
					});
					
					return false;
				}
				
				//정렬 순서 숫자 입력 제한
				/*if(display_no > oldOrderList.length || display_no < 1){
					
					nkia.ui.utils.notification({
						type:'error', 
						message:'#springMessage("msg.itam.service.00015")' //정렬순서 값을 다시 확인해 주세요.
					});
					
					return false;
				}*/
				
				// 수정하시겠습니까?
				if(!confirm('#springMessage("msg.common.confirm.00002")')){
					return false;
				}			
				
				//폼데이터 가져오기
				paramMap = {};
				var map = {};
				var items = $$('userListGrid')._getRows();
				var selectedTreeItem = $$("amdbServiceTree")._getSelectedItem();
				
				paramMap[0] = theForm._getValues();
				paramMap["oldOrderList"] = oldOrderList;
				paramMap["selUpServiceId"] = selectedTreeItem.up_node_id;

				var assetItems = $$('assetListGrid')._getRows();
				var assetArr;
				if(assetItems.length > 0){
					for(var i=0; i < assetItems.length; i++){
						if( i == 0){
							assetArr = "'"+assetItems[i].CONF_ID+"'";
						}else{						
							assetArr = assetArr+",'"+assetItems[i].CONF_ID+"'";
						}
					}
				}else{
					assetArr = "''";
				}
				var formParam = paramMap[0];
				formParam['assetArr'] = assetArr;
				
				for(var i=0; i<=items.length-1; i++){
					//키값이 대문자로 들어오기 때문에 소문자로 변환
					for(var key in items[i]){
						map = items[i];
						map[key.toLowerCase()] = map[key];
						delete map[key]; //대문자 key removeAll
					}
					paramMap[i+1] = map;
				}
				
				nkia.ui.utils.ajax({
					url: '$!{context}/itg/itam/amdb/updateAmdbServiceInfo.do',
					params: paramMap,
					async: false,
					success:function(data){

				        //하위서비스목록 순서변경
				        var lowServiceList = $$('lowServiceListGrid')._getRows();
						if(lowServiceList.length > 0){
							var orderInsertMap = {};
							for(var i=0; i<lowServiceList.length; i++){
								orderInsertMap['service_id'] = lowServiceList[i].SERVICE_ID;
								orderInsertMap['display_no'] = i+1;
								orderInsertMap['use_yn'] = theForm._getFieldValue('use_yn');
								
								
								nkia.ui.utils.ajax({
									url: '/itg/itam/amdb/updateAmdbServiceLowService.do',
									params: orderInsertMap,
									async: false,
									success: function(data){
										//하위 서비스 목록 순서 변경 결과
						    		}
								});
							}
						}
						
						gridReload('update');
						nkia.ui.utils.notification({
							type:'info',
							message: data.resultMsg
						});
					}
				}); // end of ajax
			}else{
				nkia.ui.utils.notification({
					type:'error', 
					message:"#springMessage('msg.common.00012')" //누락 및 입력된 항목을 확인해주세요.
				});
			}
			break;
		case 'delete' :
			var lowerServices = $$("lowServiceListGrid")._getRows();
			
			if(lowerServices.length == 0){
				//삭제하시겠습니까?
				if(!confirm('#springMessage("msg.common.confirm.00003")')){
					return false;
				}
			}else{
				//하위 서비스까지 모두 삭제됩니다. 삭제하시겠습니까?
				if(!confirm('#springMessage("msg.itam.service.00014")')){
					return false;
				}
			}
			
			paramMap = {};
			paramMap['service_id'] = theForm._getFieldValue('service_id');

			nkia.ui.utils.ajax({
				url: '$!{context}/itg/itam/amdb/deleteAmdbServiceInfo.do',
				params: paramMap,
				async: false,
				success: function(data){
					
					if(data.success){
						gridReload('delete');
						nkia.ui.utils.notification({
							type:'info',
							message: data.resultMsg //'#springMessage("msg.common.result.00003")' - 삭제되었습니다.
						});
					}else {
						nkia.ui.utils.notification({
							type: 'error',
							message: data.resultMsg
						});
					}
				}
			});			
			break;
		case 'back' :
			var up_service_id = paramsMap.up_service_id;
			var up_service_nm = paramsMap.up_service_nm;
			var service_id = paramsMap.service_id;
			var service_nm = paramsMap.service_nm;
			var service_effect = paramsMap.service_effect;
			var display_no = paramsMap.display_no;
			var use_yn = paramsMap.use_yn;
			var service_desc = paramsMap.service_desc;
			var leaf = paramsMap.leaf;
			var cust_nm = paramsMap.cust_nm;
			var cust_id = paramsMap.cust_id;
			
			theForm._setFieldValue('leaf', leaf);
			theForm._setFieldValue('service_id', service_id);
			theForm._setFieldValue('up_service_nm', up_service_nm);
			theForm._setFieldValue('service_nm', service_nm);
			theForm._setFieldValue('display_no', display_no);
			theForm._setFieldValue('service_effect', service_effect);
			theForm._setFieldValue('use_yn', use_yn);
			theForm._setFieldValue('service_desc', service_desc);
			theForm._setFieldValue('cust_nm', cust_nm);

			$$('userListGrid')._reload({service_id: service_id});
			$$('lowServiceListGrid')._reload({service_id: service_id});
			
			$$('serviceInsertBtn').hide(); // 저장버튼
			$$('serviceInsertBackBtn').hide(); // 취소버튼
			
			if (up_service_id == null || up_service_id == ""){
				$$('serviceInsertModeBtn').show();
			}else{
				$$('serviceInsertModeBtn').show();
				$$('serviceDeleteBtn').show();
			}
			
			$$('setEditUserProp')._setControl({
				editorMode: false,
				exceptions: ['serviceInsertModeBtn']
			});			
			break;
		case 'orderReset' :
			//담당자정보 순서 초기화
			
			var state = $$("userListGrid").$lastSort;
			
			if(state){
				if(state.length != $$("userListGrid")._getRows().length){
					$$("userListGrid")._reload();
				}else{
					$$("userListGrid")._removeAllRow();
					$$("userListGrid")._addRow(state);
				}
			}else{
				$$("userListGrid")._reload();
			}
			
			break;
		case 'lowServiceRollback' :
			//하위서비스목록 순서 초기화
			var service_id = theForm._getFieldValue('service_id');
			$$('lowServiceListGrid')._reload({service_id: service_id});
			break;
		case 'userInsert' :
			
			//담당자 추가 팝업 왼쪽 부서 트리 속성 정의
			var userCustTree = createTreeComponent({
				id : 'userCustTree',
				url : '/itg/itam/statistics/searchDeptTree.do',
				width: 250,
				expColable: true, // 전체펼침/접기 표시 여부
				filterable: true, // 검색필드 표시 여부
				expandLevel: 3,
				header: { 
					title: '#springMessage("res.title.system.dept")' //부서정보
				},
				on: {
					onItemClick: function(id, e, node){
						// 선택된 node_id 가져오기
						var treeItem = this.getItem(id);
						var nodeId = treeItem.node_id;
						// 사용자목록 그리드 리로드
						$$('userListGridPop')._reload({cust_id : nodeId});
						$$('searchForm')._reset();
					}
				}
			});
			
			
			fieldItems = [];
			//조회분류
			fieldItems.push({
				colspan:2,
				item:createUnionFieldComp({
					items: [
					    //조회분류
			            createCodeComboBoxComp({label:'#springMessage("res.00018")',name:'search_user',code_grp_id:'SEARCH_USER',attachAll:true}),
			            createTextFieldComp({name: "search_value",on:{onKeyPress: function(keycode){pressEnterKey(keycode,'user')}},placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}) })
			        ]
				})
			});

			buttonItems = [];
			// 검색
			buttonItems.push(createBtnComp({label:"#springMessage('btn.common.search')",type: "form",height:10,click: function() { search('user'); } }) );
			// 초기화
			buttonItems.push(createBtnComp({label:"#springMessage('res.common.reset')",height:10,click: function() { $$('searchForm')._reset(); } }) );
			
			//검색 폼 속성 정의
			var searchForm = createFormComp({
				id: "searchForm",
				elementsConfig: {
					labelPosition: 'left'
				},
				header: {
					title: "#springMessage('res.common.search')",
					icon: "search"
				},
				fields: { 
					items: fieldItems
				},
				footer: {
					buttons:{ 
						align: "center",
						items: buttonItems,
						css: "webix_layout_form_bottom"
					}
				}
			});
			
			//사용자목록 그리드 속성 정의
			var userListGridPop = createGridComp({
				id : 'userListGridPop',
				keys: ['USER_ID'],
				resizeColumn : true,
				pageable : true,
				pageSize : 10,
				checkbox : true,
				url : '/itg/system/user/searchUserList.do',
				resource : 'grid.itam.managerBaseList',
				header: {
					title: '#springMessage("res.00058")' //담당자 목록
				},
				dependency : {
					targetGridId: 'userListSubGridPop'
				}
			});
			
			
			//선택된 사용자목록 그리드 속성 정의
			var userListSubGridPop = createGridComp({
				id : 'userListSubGridPop',
				keys: ['USER_ID'],
				resizeColumn : true,
				autoLoad : false,
				resource : 'grid.itam.managerBaseSelList',//'grid.itam.service',
				header: {
					title: '#springMessage("res.label.itam.service.00016")' // 선택된 담당자 목록
				},
				actionColumns: [{
					column: 'DEL_ACTION',
					template: nkia.ui.html.icon('remove', 'remove_row'),
					onClick: {
						'remove_row': function(e, row, html){
							$$('userListSubGridPop')._removeRow(row);
							
							var param = $$("userCustTree")._getSelectedItem();
							//cust_id로 값을 넘겨야 검색 조건으로 검색가능
							if( param != undefined){
								param['cust_id'] = param.node_id;
								$$('userListGridPop')._reload(param,true);
							}else{
								$$('userListGridPop')._reload();
							}
							
							return false;
						}
					}
				}]
			});
									
			// 화살표 버튼
			var arrow = {
				view: 'template',
			 	template: nkia.ui.html.icon("arrow_down", "add_row"),
			 	height: 25,
			 	onClick: {
			 		'add_row': function(){
			 			// 체크 된 데이터 추출
			 			var data = $$('userListGridPop')._getSelectedRows();
			 			if(data.length < 1) {
			 				nkia.ui.utils.notification({
			 					type: 'error',
			 					message: '#springMessage("msg.itam.amdb.entity.00012")' //선택된 정보가 없습니다.
			 				});
			 				return false;
			 			}
			 			// 서브그리드에 데이터 추가
			 			$$('userListSubGridPop')._addRow(data);
			 			var param = $$("userCustTree")._getSelectedItem();
			 			
						//cust_id로 값을 넘겨야 검색 조건으로 검색가능
						if( param != undefined){
							param['cust_id'] = param.node_id;
							$$('userListGridPop')._reload(param,true);
						}else{
							$$('userListGridPop')._reload();
						}
			 			return false;
			 		}
			 	}
			};
			
			// 조절가능한 선
			var resizer = {
					view : 'resizer'
			}

			buttonItems = [];
			// 등록
			buttonItems.push(createBtnComp({label:"#springMessage('btn.common.regist')",type:'form',click: function(){ regist('user'); } }) );
			
			var bodyScrollView = {
		        view: "scrollview",
		        body: {
					cols: [{
						rows: userCustTree
					},
					{
						rows: new Array().concat(searchForm,userListGridPop,arrow,userListSubGridPop)
					}]
				}
			};
			
			//팝업창 속성 정의
			var userPopupWindow = createWindow({
		    	id: "userPopupWindow",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "#springMessage('res.label.itam.service.00015')" //담당자 조회
		    	},
				body: {
					rows: [bodyScrollView]
				},
		    	footer: {
		    		buttons: {
		    			align: "center",
		    			items: buttonItems
		    		}
		    	},
		    	closable: true
		    });
			
			// 팝업창 오픈
			$$('userPopupWindow').show();
			
			// 담당자정보 팝업창에 가져오기
			var userData = $$('userListGrid')._getRows();
			$$('userListSubGridPop')._addRow(userData);

			break;
		case 'assetInsert' :
			var gridHeaderResource = 'grid.cipop.asset.pop';
			
			//왼쪽 분류체계 트리 속성 정의
			var ciListTree = createTreeComponent({
				id : 'ciListTree',
				url : '/itg/itam/statistics/searchClassTree.do',
				width: 250,
				expColable: true, // 전체펼침/접기 표시 여부
				filterable: true, // 검색필드 표시 여부
				params: {
					class_mng_type: "AM"
				},
				expandLevel: 3,
				header: {
					title: "#springMessage('res.00016')" //분류체계
				},
				on: {
					onItemClick: function(id, e, node){
						// 선택된 node_id 가져오기
						var treeItem = this.getItem(id);
						var nodeId = treeItem.node_id;
						var classType = treeItem.class_type;
						var searchTypeGrpId = 'SEARCH_ROOT_STATISTICS';
		
						// 그리드 헤더 변경
						if('LN' == classType) {
							$$('ciListGrid')._updateHeaderFromResource(gridHeaderResource + '.ln');
						} else {
							$$('ciListGrid')._updateHeaderFromResource(gridHeaderResource);
						}
						
						// 검색 콤보박스 갱신
						searchTypeGrpId = 'SEARCH_' + classType + '_STATISTICS';
						$$("searchForm").elements["search_asset"]._reload({code_grp_id : searchTypeGrpId});
						
						// 자산목록 그리드 리로드
						$$('ciListGrid')._reload({class_id : nodeId, class_type: classType});
						$$('searchForm')._reset();
					}
				}
			});
			
			fieldItems = [];
			//조회분류
			fieldItems.push({
				colspan:2,
				item:createUnionFieldComp({
					items: [
					    //조회분류
			            createCodeComboBoxComp({label:'#springMessage("res.00018")',name:'search_asset',code_grp_id:'SEARCH_ROOT_STATISTICS',attachAll:true}),
			            createTextFieldComp({name: "search_value",on:{onKeyPress: function(keycode){pressEnterKey(keycode,'ci')}},placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}) })
			        ]
				})
			},
			{colspan: 1, item: createCodeComboBoxComp({label: '용도', name: 'business_type_cd', code_grp_id: 'BUSINESS_TYPE_CD', attachChoice: true})},
			{colspan: 1, item: createCodeComboBoxComp({label: '고객사', name: 'customer_id', url: '/itg/system/customer/searchCustomerCodeListByUserId.do', attachChoice: true })}
			);

			buttonItems = [];
			// 검색
			buttonItems.push(createBtnComp({label:"#springMessage('btn.common.search')",type: "form",click: function() { search('ci'); } }) );
			// 초기화
			buttonItems.push(createBtnComp({label:"#springMessage('res.common.reset')",click: function() { $$('searchForm')._reset(); } }) );
			
			//검색 폼 속성 정의
			var searchForm = createFormComp({
				id: "searchForm",
				elementsConfig: {
					labelPosition: 'left'
				},
				header: {
					title: "#springMessage('res.common.search')",
					icon: "search"
				},
				fields: { 
					colSize: 2,
					items: fieldItems
				},
				footer: {
					buttons:{ 
						align: "center",
						items: buttonItems,
						css: "webix_layout_form_bottom"
					}
				}
			});
			
			//자산목록 그리드 속성 정의
			var ciListGrid = createGridComp({
				id : 'ciListGrid',
				keys: ['CONF_ID'],
				resizeColumn : true,
				pageable : true,
				pageSize : 10,
				checkbox : true,
				autoLoad: false,
				url : '/itg/itam/amdb/searchServiceConfList.do',
				resource : 'grid.cipop.asset',
				header: {
					title: '#springMessage("res.title.itam.maint.assetlist")' //자산목록
				},
				dependency : {
					targetGridId: 'ciListSubGrid'
				}
			});
			
			
			//선택된 자산목록 그리드 속성 정의
			var ciListSubGrid = createGridComp({
				id : 'ciListSubGrid',
				keys: ['CONF_ID'],
				resizeColumn : true,
				drag: true,
				sortConfig: 'text',
				autoLoad : false, // 메모리 그리드 사용 (데이터 로드 없음)
				resource : 'grid.cipop.getasset',
				header: {
					title: '#springMessage("res.00020")' //선택된 자산목록
				},
				actionColumns: [{
					column: 'ACTION', // 액션 컬럼의 헤더명 (그리드 리소스)
					template: nkia.ui.html.icon('remove', 'remove_row'),
					onClick: { // 이벤트 연결(실행 할 이벤트명 매칭)
						'remove_row': function(e, row, html){
							
							$$('ciListSubGrid')._removeRow(row);
							
				 			var param =	$$("ciListTree")._getSelectedItem();
							//class_id로 값을 넘겨야 검색 조건으로 검색가능
							if( param != undefined){
								param['class_id'] = param.node_id;
								$$('ciListGrid')._reload(param,true);
							}else{
								$$('ciListGrid')._reload();
							}
							return false;
						}
					}
				}]
			});
									
			// 화살표 버튼
			var arrow = {
				view: 'template',
			 	template: nkia.ui.html.icon("arrow_right", "add_asset"),
			 	width: 30,
			 	onClick: {
			 		'add_asset': function(){
			 			// 체크 된 데이터 추출
			 			var data = $$('ciListGrid')._getSelectedRows();
			 			
			 			// 선택 한 데이터가 없을 시 메시지를 띄워줍니다.
			 			if(data.length < 1) {
			 				nkia.ui.utils.notification({
			 					type: 'error',
			 					message: '#springMessage("msg.itam.amdb.entity.00012")' //선택된 정보가 없습니다.
			 				});
			 				return false;
			 			}
			 			
			 			// 서브그리드에 데이터 추가
			 			$$('ciListSubGrid')._addRow(data);
			 			
			 			var param =	$$("ciListTree")._getSelectedItem();
						//class_id로 값을 넘겨야 검색 조건으로 검색가능
						if( param != undefined){
							param['class_id'] = param.node_id;
							$$('ciListGrid')._reload(param,true);
						}else{
							$$('ciListGrid')._reload();
						}
			 			return false;
			 		}
			 	}
			};
			
			// 조절가능한 선
			var resizer = {
					view : 'resizer'
			}

			buttonItems = [];
			// 등록
			buttonItems.push(createBtnComp({label:"#springMessage('btn.common.regist')",type:'form',click: function(){ regist("ci"); } }) );
			
			var bodyScrollView = {
		        view: "scrollview",
		        body: {
		        	view:"accordion",
    				multi:true,
					cols: [
						{
		    			    header: "분류체계정보",
		                    body: {
		                    	rows: ciListTree
		                    }
			            },
						{
							rows: new Array().concat(searchForm,ciListGrid)
						},
						arrow,
						{
							width: 300,
							rows: new Array().concat(ciListSubGrid)
						}
					]
				}
			};
			
			//팝업창 속성 정의
			var ciPopupWindow = createWindow({
		    	id: "ciPopupWindow",
		    	width: 1200,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	move: true,
		    	header: {
		    		title: '#springMessage("res.label.itam.service.00019")' //서비스 구성연계
		    	},
				body: {
					rows: [bodyScrollView]
				},
		    	footer: {
		    		buttons: {
		    			align: "center",
		    			items: buttonItems
		    		}
		    	},
		    	closable: true
		    });
			
			// 팝업창 오픈
			$$("ciPopupWindow").show();
			
			// 연계구성정보 팝업창에 가져오기
			var assetData = $$('assetListGrid')._getRows();
			$$('ciListSubGrid')._addRow(assetData);
			
			break;
		case 'topology' :
			var selectedItem = $$('amdbServiceTree')._getSelectedItem();
			if(selectedItem != undefined){
				var conf_id = selectedItem.node_id;
				var class_type = 'SERVICE';
				var tangible_asset_yn = 'Y';
				var param = 'conf_id=' + conf_id + '&class_type=' + class_type + '&tangible_asset_yn=' + tangible_asset_yn;
				var url = '/itg/itam/topology/goTopology.do?' + param;
				window.open(getConstValue('CONTEXT') + url,"topologyPop","width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no");
			}else{
				nkia.ui.utils.notification({
					type:'error', 
					message: '#springMessage("msg.itam.amdb.entity.00012")' //선택된 정보가 없습니다.
				});
			}
			break;
		case 'searchAsset' :
			var searchParams = nkia.ui.utils.extend($$("setEditUserProp")._getValues(), $$("assetSearchForm")._getValues());
			
			$$('assetListGrid')._reload(searchParams);
			
			break;
	}
} // end of actionLink



// 부서정보 트리 클릭 이벤트
function custTreeNodeClick(id, e, node) {
	
	// 선택된 아이템의 node_id 가져오기
	var treeItem = this.getItem(id);
	var nodeId = treeItem.node_id;
	// 클릭된 트리 하이라이트
	$$('selCustTree').select(id);

	paramMap = {};
	paramMap['search_cust_id'] =nodeId;
	paramMap['expandLevel'] = '2';
	
	$$('amdbServiceTree')._reload(paramMap,true);
	
	$$('assetListGrid')._removeAllRow();
	$$('userListGrid')._removeAllRow();
	$$('lowServiceListGrid')._removeAllRow();
	$$("setEditUserProp")._reset();
	
	$$('topologyBtn').hide();
	$$('assetInsertBtn').hide();
	$$('userInsertBtn').hide();
	$$('orderRollBackBtn').hide();
	$$('lowServiceRollbackBtn').hide();
	$$('serviceDeleteBtn').hide();
	$$('serviceUpdateBtn').hide();
	$$('serviceInsertBtn').hide();
	$$('serviceInsertBackBtn').hide();

	
	$$('serviceInsertModeBtn').show();
	
	$$('setEditUserProp')._setControl({
		editorMode: false,
		exceptions: ['serviceInsertModeBtn']
	});
	return false;
}


// 서비스관리정보 트리 클릭 이벤트
function treeNodeClick(id, e, node) {
	//선택된 트리 파란색 블럭처리
	$$('amdbServiceTree').select(id);
	
	var param = {};
	var treeItem = this.getItem(id);
	var serviceId = treeItem.node_id;
	var upServiceId = treeItem.up_node_id;
	comboCodeId = serviceId;
	treeNodeLv = treeItem.node_level;
	param['service_id'] = serviceId;
	
	$$('lowServiceListGrid')._reload(param);
	$$('userListGrid')._reload(param);
	$$('assetListGrid')._reload(param);
	
	$$('serviceInsertBtn').hide();
	$$('serviceInsertBackBtn').hide();
	
	nkia.ui.utils.ajax({
		url: '/itg/itam/amdb/selectAmdbServiceBaseInfo.do',
		params: param,
		async: false,
		success:function(data){
			if( data.success ){
				$$('setEditUserProp')._syncData(data.resultMap);
				if(treeNodeLv == 1){
					$$('setEditUserProp')._setControl({
						editorMode: false,
						exceptions: ['serviceInsertModeBtn'/*,'display_no'*/]
					});
					$$('topologyBtn').hide();
					$$('userInsertBtn').hide();
					$$('orderRollBackBtn').hide();
					$$('lowServiceRollbackBtn').hide();
					$$('serviceDeleteBtn').hide();
					$$('serviceUpdateBtn').hide();
					$$('serviceInsertBtn').hide();
					$$('serviceInsertBackBtn').hide();
					$$('assetInsertBtn').hide();	
				}else{
					// 탭 우측상단 버튼 컨트롤
					$$('userInsertBtn').show();
					//$$('orderRollBackBtn').show();
					$$('lowServiceRollbackBtn').show();
					$$('assetInsertBtn').show();
					$$('topologyBtn').show();
					$$('serviceDeleteBtn').show();
					$$('serviceUpdateBtn').show();
					
					$$('setEditUserProp')._setControl({
						editorMode: true,
						exceptions: ['cust_nm','service_id','up_service_nm']
					});
				}
			}else{
				nkia.ui.utils.notification({
					type: 'error',
					message: data.resultMsg
				});
			}
		}
	});
	
	
	var selectedTreeItem = $$("amdbServiceTree")._getSelectedItem();
	
	//정렬순서 list 저장
	oldOrderList = [];
	$$("amdbServiceTree").data.each(function(obj){
		var parentId = selectedTreeItem.up_node_id;
		if(parentId == obj.up_node_id){
			oldOrderList.push(obj);
		}					
	});
	
	return false;
}

// 검색 이벤트
function search(mode){
	var paramMap = $$('searchForm')._getValues();
	var searchType;
	if(mode == 'ci'){ // 분류체계 팝업일 경우
		var selClass = $$('ciListTree')._getSelectedItem();
		
		if(selClass) {
			paramMap['class_id'] = selClass['node_id'];
			paramMap['class_type'] = selClass['class_type'];
		} else {
			paramMap['class_type'] = 'ROOT';
		}
		
		searchType = paramMap.search_asset;
		paramMap['search_type'] = searchType;
		
		$$('ciListGrid')._reload(paramMap);
	}else if(mode == 'user'){ // 담당자추가 팝업일 경우
		searchType = paramMap.search_user;
		paramMap['search_type'] = searchType;
		$$('userListGridPop')._reload(paramMap);
	}
}
// 그리드 리로드(수정,등록,삭제)
function gridReload(command){
    
	var node_id =$$('selCustTree')._getSelectedItem().node_id;		
		$$('amdbServiceTree')._reload({
			search_cust_id: node_id,
			expandLevel:2
		});
		
//	if(command != "update"){
		$$('setEditUserProp')._reset();
		$$('assetListGrid')._removeAllRow();
		$$('userListGrid')._removeAllRow();
		$$('lowServiceListGrid')._removeAllRow();
		
		$$('assetInsertBtn').hide();
		$$('topologyBtn').hide();
		$$('serviceInsertBtn').hide();
		$$('serviceInsertBackBtn').hide();
		$$('serviceUpdateBtn').hide();
		$$('orderRollBackBtn').hide();
		$$('userInsertBtn').hide();
		$$('serviceDeleteBtn').hide();
		$$('lowServiceRollbackBtn').hide();
		
		$$('setEditUserProp')._setControl({
			editorMode: false,
			exceptions: ['serviceInsertModeBtn']
		});
//	}
	return false;
}

// 분류체계 팝업창 등록 이벤트
function regist(mode){
	
	if(mode == 'ci'){
		// 선택된 자산
		var ciList = $$('ciListSubGrid')._getRows();
		// 타켓 자산 그리드 리셋
		$$('assetListGrid')._removeAllRow();
		//선택된 자산을 타켓 자산 그리드에 등록
		$$('assetListGrid')._addRow(ciList);
		// 팝업창 닫기
		$$('ciPopupWindow').close();
	}else if(mode == 'user'){
		// 선택된 사용자
		var userList = $$('userListSubGridPop')._getRows();
		if(userList.length < 1){
			nkia.ui.utils.notification({
				type:'error', 
				message:'#springMessage("msg.itam.service.00006")' //담당자를 선택해주세요.
			});
			return false;
		}
		// 타겟 사용자 그리드 리셋
		$$('userListGrid')._removeAllRow();
		//선택된 자산을 타겟 사용자 그리드에 등록
		$$('userListGrid')._addRow(userList);
		$$("userListGrid").$lastSort = userList;
		// 팝업창 닫기
		$$('userPopupWindow').close();
	}
}

/**
 * 키 이벤트 (검색)
 */
function pressEnterKey(keyCode,mode){
	if(keyCode){
		if(keyCode == "13"){ // enter key
			switch(mode){
			case "ci":
				search('ci');
				break;
			case "user":
				search('user');
				break;
			}
		}else{
			return keyCode;
		}
	}
	return false;
}

/**
 * 연계구성정보 그리드 더블 클릭 - 원장 상세조회
 */
function relAssetGridCellDblClick(id, e) {
	
	var clickRecord = this.getItem(id);
	
	var asset_id 		= clickRecord.ASSET_ID;
	var conf_id 		= clickRecord.CONF_ID;
	var class_id		= clickRecord.CLASS_ID;
	var class_type		= clickRecord.CLASS_TYPE;
	var host_nm			= clickRecord.HOST_NM;
	var view_type		= "tab_view";
	var entity_class_id = class_id;
	var tab_title = "["+asset_id+"] "+host_nm;
	var center_asset_yn		= "$!{center_asset_yn}" ? "$!{center_asset_yn}" : "N";
	
	var url = '/itg/itam/automation/goAssetDetailViewer.do' 
		+ "?asset_id=" + asset_id
		+ "&conf_id=" + conf_id
		+ "&class_id=" + class_id
		+ "&class_type=" + class_type
		+ "&view_type=" + view_type
		+ "&entity_class_id=" + entity_class_id
		+ "&center_asset_yn=" + center_asset_yn
	;
	
	window.open(
		getConstValue('CONTEXT') + url,
		"assetDetailPop"+conf_id,
		"width=1024,height=600,history=no,resizable=yes,status=no,scrollbars=yes,member=no"
	);
}

</script>
</body>
</html>