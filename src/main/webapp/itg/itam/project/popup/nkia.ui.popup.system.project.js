/********************************************
 * Project
 * Date: 2017-11-20
 * Version:
 *	1.0 - Header, Header버튼, field 생성, Fotter 버튼 지원
 ********************************************/
nkia.ui.popup.system.Project = function() {
	// 속성 Properties
	this.id = "";
	this.title = "";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.url = "/itg/itam/project/searchProject.do";
	this.resource = "grid.itam.project";
	this.params = {};
	this.reference = {};
	this.checkbox = false;
	this.gridHeight = 300;
	this.pageSize = 10;
	this.bindEvent = null;
	this.dependency = {};
	
	// 구성요소 Properties
	this.window = {};
	this.tree = [];
	this.searchForm = [];
	this.grid = [];
	
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		
		this.searchForm = createFormComp({
			id: this.id + '_searchForm',
			header: {
				icon: "search",
	        	title: PAGE_MASSEGE.SEARCH_TITLE
		    },
		    fields: {
		    	elementsConfig: {
		    		labelPosition: "left"
		    	},
		        colSize: 1,
		        items: [
		            {item : createTextFieldComp({
                				name: "search_project_nm",
                				placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
                				on:{ onKeyPress: this.doKeyPress.bind(_this) }	
                			})
	              	}
		        ]
		    },
		    footer: {
		        buttons: {
		        	align: "center",
		            items: [
		                createBtnComp({label: PAGE_MASSEGE.SEARCH_BTN, type:"form", click: this.doSearch.bind(_this)}),
		                createBtnComp({label: PAGE_MASSEGE.RESET_BTN, click: this.clearSearch.bind(_this)})
		            ],
		            css: "webix_layout_form_bottom"
		        }
		    }
		});
		
		var title = getConstText({isArgs : true, m_key : 'res.00012'});
		
		this.on = {
			onItemDblClick: function(id, e){
				if( _this.bindEvent != null ){
					_this.bindEvent.choice(_this);
				}else{
					_this.choice(id, e);
				}
			}
		}
		
		this.grid = createGridComp({
			id: this.id + '_grid',
			header: {
	        	title: title
		    },
			checkbox: this.checkbox,
			height: this.gridHeight,
		    resource: this.resource,
		    params: this.params,
			url: this.url,
			pageable: true,
			pageSize: this.pageSize,
			keys: ["PROJECT_ID"],
			dependency: this.dependency,
			on: this.on
		});
	};
	
	this.setWindow = function(){
		var _this = this;
		this.window = createWindow({
	    	id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
	    		rows: [{
	    			cols: [{
	                    rows: new Array().concat(this.searchForm, this.grid)
	                }]
	    		}]
	    	},
	    	footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '선 택', type: "form", click: this.choice.bind(_this)})
    		        ]
	    		}
	    	},
	    	closable: this.closable 
	    });
	}
	
	// 검색 키프레스 이벤트
	this.doKeyPress = function(keyCode){
		if(keyCode && keyCode == "13"){
			this.doSearch();
		}
	}
	
	// 검색 버튼 이벤트
	this.doSearch = function(){
		var form = $$(this.id + '_searchForm');
		$$(this.id + '_grid')._reload(form._getValues());
	}
	
	// 검색 초기화 버튼 이벤트
	this.clearSearch = function(){
		$$(this.id + '_searchForm')._reset();
	}
	
	// 적용(선택) 이벤트
	this.choice = function(id, e){
		var gridItem = $$(this.id + "_grid").getSelectedItem();
		if(gridItem){
			var reference = this.reference;
			var fields = reference.fields;
			
			var vendor_id = nkia.ui.utils.check.nullValue(gridItem.PROJECT_ID);
			var vendor_nm = nkia.ui.utils.check.nullValue(gridItem.PROJECT_NM);
			//var cust_nm = nkia.ui.utils.check.nullValue(gridItem.CUST_NM);
			
			var values = {};
			values[fields["id"]] = vendor_id;			// 사업ID
			values[fields["name"]] = vendor_nm;		// 사업명
			//if(fields["cust_nm"]) values[fields["cust_nm"]] = cust_nm; // 부서명 
			
            reference.form._setValues(values);
             
            this.window.close();
		}else{
            nkia.ui.utils.notification({
                type: 'error',
                message: '선택된 사업가 없습니다.'
            });
        }
	};
};

/**
 * 사업 팝업
 * @param {} popupProps
 */
function createProjectWindow(popupProps) {
	var props = popupProps||{};
	var comp;
	try {
		switch (props.type) {
			default:
				comp = new nkia.ui.popup.system.Project();
		}
		comp.setProps(props);
		comp.setRender(props);
		comp.setWindow();
	} catch(e) {
		alert(e.message);
	}
}
