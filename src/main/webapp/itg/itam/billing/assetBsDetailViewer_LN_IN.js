
/**
 * 에디터폼 데이터 세팅
 */
function setFormDatas(parentConfId) {
	
	// 자산 마스터 정보 로드
	var form = $$("assetInfoForm");
	
	nkia.ui.utils.ajax({
		async: false,
		url: "/itg/itam/billing/searchAssetBillingMaster.do",
		params: {conf_id : parentConfId},
		isMask: false,
		notification: false,
		success: function(response) {
			if(response.gridVO && response.gridVO.rows) {
				var data = response.gridVO.rows;
				if(data.length > 0) {
					form._setValues(data[0]);
				}
			}
		}
	});
	
	// 자산 과금 정보 로드
	reloadBillingInfo(parentConfId);
}

/**
 * 과금 정보 로드
 */
function reloadBillingInfo(parentConfId) {
	
	var form = $$("lnBillingForm");
	
	nkia.ui.utils.ajax({
		async: false,
		url: "/itg/itam/billing/searchAssetBillingLn.do",
		params: {conf_id : parentConfId},
		isMask: false,
		notification: false,
		success: function(response) {
			if(response.gridVO && response.gridVO.rows) {
				var data = response.gridVO.rows;
				if(data.length > 0) {
					form._setValues(data[0]);
				}
			}
		}
	});
}

/**
 * select박스 간의 연동 설정
 */
function setSelectBoxRelation() {
	
	var form = $$("lnBillingForm");
	
	var billYn = form._getField("BILL_YN");
	var bsBgnDt = form._getField("BS_BGN_DT");
    var bsEndDt = form._getField("BS_END_DT");
    var lnPrc = form._getField("LN_PRC");
	var maPrc = form._getField("MA_PRC");
	
	// 과금 여부가 '예'이면 전체 필드 필수값, '아니오' 혹은 빈 값 이면 필수값 없음
	billYn.attachEvent("onChange", function(newVal, oldVal) {
		if(newVal == "Y") {
			var objectArray = [bsBgnDt, bsEndDt, lnPrc, maPrc];
			setFieldsRequired(objectArray, true);
		} else {
			var objectArray = [bsBgnDt, bsEndDt, lnPrc, maPrc];
			setFieldsRequired(objectArray, false);
		}
	});
}

/**
 * 필드 필수체크 핸들러
 */
function setFieldsRequired(objectArray, isRequired) {
	for(var i in objectArray) {
		objectArray[i].setValue("");
		objectArray[i].config.required = isRequired;
		objectArray[i].refresh();
	}
}


/**
 * 사용처 row 데이터 추가
 */
function addCmprcLnData() {
	$$("cmprcLnGrid")._addRow();
}

/**
 * 사용처 row 데이터 삭제
 */
function removeCmprcLnData(e, row, html) {
	$$("cmprcLnGrid")._removeRow();
}

/**
 * 사용처 데이터 리로드
 */
function reloadCmprcLnData(parentConfId) {
	
	var isEditFinish = $$("cmprcLnGrid")._checkCellEditorState();
	
	if(!isEditFinish) {
		alert("입력 활성화 상태에서는 실행할 수 없습니다.");
		return false;
	}
	
	$$("cmprcLnGrid")._reload({conf_id : parentConfId});
}

/**
 * 청구처별 사용량 - 데이터 입력 벨리데이션
 */
function cmprcLnEditorChange(state, editor, ignoreUpdate) {
	
	var rowId = editor.row;
	var columnId = editor.column;
	var oldValue = state.old;
	var newValue = state.value;
	
	var rows = this._getRows();
	var thisRow = this.getItem(rowId);
	
	if(columnId == "BS_BGN_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금시작일이 과금종료일보다 크거나 같을 경우 입력불가
		var clickRow = this.getItem(rowId);
		if(clickRow["BS_END_DT"]!="" && newValue.split("-").join("") >= clickRow["BS_END_DT"].split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금시작일은 과금종료일보다 같거나 클 수 없습니다."
			});
			clickRow[columnId] = oldValue;
			return false;
		}
	} else if(columnId == "BS_END_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금종료일이 과금시작일보다 작거나 같을 경우 입력불가
		var clickRow = this.getItem(rowId);
		if(clickRow["BS_BGN_DT"]!="" && newValue.split("-").join("") <= clickRow["BS_BGN_DT"].split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금종료일은 과금시작일보다 같거나 작을 수 없습니다."
			});
			clickRow[columnId] = oldValue;
			return false;
		}
	}
}





/**
 * 저장 이벤트
 */
function saveAssetBilling(parentConfId) {
	
	var form = $$("lnBillingForm");
	var cmprcLnGrid = $$("cmprcLnGrid");
	
	// 벨리데이션 체크
	if( form._validate() ) {
		
		var isValid = true;
		var validMsg = "";
		
		// 데이터 가공 : 아래 변수에 정의된 KEY 들만 데이터로 만들어준다. * 컬럼명 앞,뒤에 골뱅이(@) 붙여줄것
		var _CMPRC_LN_HEADER = "@BS_INDX@BS_BGN_DT@BS_END_DT@CMPRC_SE@";
		
		var formData = form._getValues();
		formData["CONF_ID"] = parentConfId;
		
		var format = webix.Date.dateToStr("%Y-%m-%d");
		var bsBgnDt = format(formData["BS_BGN_DT"]);
		var bsEndDt = format(formData["BS_END_DT"]);
		
		if(bsBgnDt!="" && bsEndDt!="") {
			if(bsBgnDt.split("-").join("") >= bsEndDt.split("-").join("")) {
				nkia.ui.utils.notification({
					type: "error",
					message: "과금정보 입력폼의 과금시작일은 과금종료일보다 같거나 클 수 없습니다."
				});
				return false;
			}
		}
		
		var cmprcLnData = [];
		
		// 청구처별 사용량 영역 Start ************************************************************************
		cmprcLnGrid.eachRow(function(row) {
			var myRow = cmprcLnGrid.getItem(row);
			var newRow = {};
			cmprcLnGrid.eachColumn(function(columnid) {
				if(_CMPRC_LN_HEADER.indexOf("@"+columnid+"@") > -1) {
					if(columnid!="BS_INDX" && (typeof myRow[columnid]=="undefined" || (myRow[columnid]+"")=="" || myRow[columnid]==null)) {
						validMsg = "[청구처별 사용량]의 입력항목은 모두 필수입니다.";
						isValid = false;
					} else {
						newRow[columnid] = myRow[columnid];
						if(typeof newRow[columnid]=="undefined") {
							newRow[columnid] = "";
						}
					}
				}
			}, true);
			newRow["CONF_ID"] = parentConfId;
			cmprcLnData.push(newRow);
		});
		
		if(!isValid) {
			nkia.ui.utils.notification({
				type: "error",
				message: validMsg
			});
			formData = {};
			cmprcLnData = [];
			isValid = true;
			return false;
		}
		// 청구처별 사용량 영역 End ************************************************************************
		
		// 그리드 입력 활성화 상태 여부 체크
		if(!cmprcLnGrid._checkCellEditorState()) {
			alert("입력 활성화 상태에서는 실행할 수 없습니다.");
			return false;
		}
		
		// 등록여부 확인
		if(!confirm("저장하시겠습니까?")) {
			return false;
		}
		
		var params = {};
		params["parent_conf_id"] = parentConfId;
		params["FORM"] = formData;
		params["CMPRC_SE_LN"] = cmprcLnData;
		
		nkia.ui.utils.ajax({
			viewId: 'assetBillingDetailViewer',
			url: "/itg/itam/billing/changeAssetBillingLnIn.do",
			params: params,
			async: false,
			isMask: true,
			notification: true,
			success: function(response){
				alert("저장되었습니다.");
				parent.reloadList();
				parent.closeTab();
			}
		});
		
	}
}

/**
 * 과금 정보 이력 조회 팝업
 */
function openBsFormHisPop(_ASSET_ID, _CONF_ID) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsLnList.do",
		resource: "grid.itam.bs.ln.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}

/**
 * 공통비 청구처 이력 조회 팝업
 */
function openBsCmprcCustRateHisPop(_ASSET_ID, _CONF_ID) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsCmprcCustRateList.do",
		resource: "grid.itam.bs.cmprc.custrate.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}