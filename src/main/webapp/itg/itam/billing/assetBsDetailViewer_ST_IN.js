
/**
 * 에디터폼 데이터 세팅
 */
function setFormDatas(parentConfId) {
	
	// 자산 마스터 정보 로드
	var form = $$("assetInfoForm");
	
	nkia.ui.utils.ajax({
		async: false,
		url: "/itg/itam/billing/searchAssetBillingMaster.do",
		params: {conf_id : parentConfId},
		isMask: false,
		notification: false,
		success: function(response) {
			if(response.gridVO && response.gridVO.rows) {
				var data = response.gridVO.rows;
				if(data.length > 0) {
					form._setValues(data[0]);
				}
			}
		}
	});
	
	// 자산 과금 정보 로드
	reloadBillingInfo(parentConfId);
}

/**
 * 과금 정보 로드
 */
function reloadBillingInfo(parentConfId) {
	
	var form = $$("stBillingForm");
	
	nkia.ui.utils.ajax({
		async: false,
		url: "/itg/itam/billing/searchAssetBillingSt.do",
		params: {conf_id : parentConfId},
		isMask: false,
		notification: false,
		success: function(response) {
			if(response.gridVO && response.gridVO.rows) {
				var data = response.gridVO.rows;
				if(data.length > 0) {
					form._setValues(data[0]);
				}
			}
		}
	});
}

/**
 * select박스 간의 연동 설정
 */
function setSelectBoxRelation() {
	
	var form = $$("stBillingForm");
	
	var billYn = form._getField("BILL_YN");
	var bsBgnDt = form._getField("BS_BGN_DT");
    var bsEndDt = form._getField("BS_END_DT");
    var stCostCls = form._getField("ST_COST_CLS");
	var stLv = form._getField("ST_LV");
	var stSe = form._getField("ST_SE");
	var diskSe = form._getField("DISK_SE");
	
	// 과금 여부가 '예'이면 전체 필드 필수값, '아니오' 혹은 빈 값 이면 필수값 없음
	billYn.attachEvent("onChange", function(newVal, oldVal) {
		if(newVal == "Y") {
			var objectArray = [bsBgnDt, bsEndDt, stCostCls, stLv, stSe, diskSe];
			setFieldsRequired(objectArray, true);
		} else {
			var objectArray = [bsBgnDt, bsEndDt, stCostCls, stLv, stSe, diskSe];
			setFieldsRequired(objectArray, false);
		}
	});
	
	stCostCls.attachEvent("onChange", function(newVal, oldVal) {
		
		stLv.setValue("");
		
		stLv._reload({code_grp_id : "ST_" + newVal + "_LV"});
		
		// 과금 분류가 '예'일 때, 
		if(billYn.getValue() == "Y") {
//			if(newVal == "NEW" || newVal == "LEG" || newVal == "EXP") {
//				// 과금분류가 '신규/기존/손해배상제외'일 경우만 서버종류, 용도, 서버등급은 필수
//				var objectArray = [svSe, svUse, svLv];
//				setFieldsRequired(objectArray, true);
//			} else if(newVal == "COL") {
//				// 과금분류가 'CoLocation'일 경우 서버등급, 용도 필수 해제
//				var objectArray = [svUse, svLv];
//				setFieldsRequired(objectArray, false);
//			} else {
//				var objectArray = [svUse];
//				setFieldsRequired(objectArray, false);
//			}
		}
	});
}

/**
 * 필드 필수체크 핸들러
 */
function setFieldsRequired(objectArray, isRequired) {
	for(var i in objectArray) {
		objectArray[i].setValue("");
		objectArray[i].config.required = isRequired;
		objectArray[i].refresh();
	}
}






/**
 * 저장 이벤트
 */
function saveAssetBilling(parentConfId) {
	
	var form = $$("stBillingForm");
	
	// 벨리데이션 체크
	if( form._validate() ) {
		
		var isValid = true;
		var validMsg = "";
		
		var formData = form._getValues();
		formData["CONF_ID"] = parentConfId;
		
		var format = webix.Date.dateToStr("%Y-%m-%d");
		var bsBgnDt = format(formData["BS_BGN_DT"]);
		var bsEndDt = format(formData["BS_END_DT"]);
		
		if(bsBgnDt!="" && bsEndDt!="") {
			if(bsBgnDt.split("-").join("") >= bsEndDt.split("-").join("")) {
				nkia.ui.utils.notification({
					type: "error",
					message: "과금정보 입력폼의 과금시작일은 과금종료일보다 같거나 클 수 없습니다."
				});
				return false;
			}
		}
		
		// 등록여부 확인
		if(!confirm("저장하시겠습니까?")) {
			return false;
		}
		
		var params = {};
		params["parent_conf_id"] = parentConfId;
		params["FORM"] = formData;
		
		nkia.ui.utils.ajax({
			viewId: 'assetBillingDetailViewer',
			url: "/itg/itam/billing/changeAssetBillingStIn.do",
			params: params,
			async: false,
			isMask: true,
			notification: true,
			success: function(response){
				alert("저장되었습니다.");
				parent.reloadList();
				parent.closeTab();
			}
		});
		
	}
}

/**
 * 과금 정보 이력 조회 팝업
 */
function openBsFormHisPop(_ASSET_ID, _CONF_ID) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsStList.do",
		resource: "grid.itam.bs.st.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}