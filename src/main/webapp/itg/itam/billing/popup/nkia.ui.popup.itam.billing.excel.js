/********************************************
 * Date: 2017-02-16
 * Version: 1.0 - 최초등록
 * Author: Eunjin Jwa
 * Description :  엑셀다운로드 팝업
 ********************************************/

// 분류체계별 엑셀 다운로드
nkia.ui.popup.itam.excel.ExcelByClassTypeForBilling = function() {
	// 속성 Properties
	this.id = "";
	this.title = "엑셀다운로드";
	this.closable = true;
	this.width = 500;
	this.height = 600;
	this.popupParamData = {};
	// 구성요소 Properties
	this.window = {};
	
	this.setProps = function(props) {
		for(var i in props) {
			if(this[i] !== undefined && props.hasOwnProperty(i)) {
				this[i] = props[i];
			}
		}
	};
	
	this.setRender = function(props) {
		var _this = this;
		var classType = this.popupParamData["class_type"];
		this.tabBar = new Array();
		var tabBody = new Array();
		
		var centerAssetYn = this.popupParamData["center_asset_yn"];
		
		var svLogicTabBarMap = {id: 'sl_tab', CODE_ID: 'SV', value: "논리서버", SV_TYPE:'LOG'};
		var nwTabBarMap = {id: 'nw_tab', CODE_ID: 'NW', value: "네트워크"};
		var stTabBarMap = {id: 'st_tab', CODE_ID: 'ST', value: "스토리지"};
		var lnTabBarMap = {id: 'ln_tab', CODE_ID: 'LN', value: "전용회선"};

		if(classType == 'ROOT'){
			if(centerAssetYn == "Y") {
				this.tabBar = [svLogicTabBarMap, nwTabBarMap, stTabBarMap, lnTabBarMap];
			} else {
				this.tabBar = [nwTabBarMap, lnTabBarMap];
			}
		}else if(classType == 'HW'){
			if(centerAssetYn == "Y") {
				this.tabBar = [svLogicTabBarMap, nwTabBarMap, stTabBarMap, lnTabBarMap];
			} else {
				this.tabBar = [nwTabBarMap, lnTabBarMap];
			}
		}else if(classType == "SV"){
			this.tabBar = [svLogicTabBarMap];
			this.popupParamData["class_mng_type"] = "SL";
		}else if(classType == "LOG"){
			this.tabBar = [svLogicTabBarMap];
			this.popupParamData["class_mng_type"] = "SL";
		}else if(classType == "NW"){	
			this.tabBar = [nwTabBarMap];
		}else if(classType == "ST"){
			this.tabBar = [stTabBarMap];
		}else if(classType == "LN"){	
			this.tabBar = [lnTabBarMap];
		}
		
		for(var i=0; i < this.tabBar.length; i++){
			var tabClassType = this.tabBar[i].CODE_ID;
			var tabId = this.tabBar[i].id;
			
			var paramMap = {};
			paramMap['class_type'] = tabClassType;
			if(tabClassType == 'SV'){
				paramMap['sv_type'] = this.tabBar[i].SV_TYPE;
				paramMap['class_mng_type'] = "SL";
			}
			
			var excelAttrGrid = createGridComp({
				id : tabId+'_grid',
				resizeColumn : false,
				height : 370,
				url : "/itg/itam/statistics/searchClassTypeAttrList.do",
				resource : "grid.itam.statistics.excelDownPop",
				checkbox : true,
				params : paramMap,
				header: {
					title: "속성 선택"
				}
			});
			
			// 각 탭별로 grid 구성
			tabBody.push({
				id: tabId,
				rows: excelAttrGrid
			});
		} // end for()
		
		// 실제 탭을 구성합니다.
		this.tabGrid = {
				view:"tabview", // view 형식 : tabview로 입력
				id:"assetTypeTab", // 탭의 ID
				animate:false,
				tabbar:{
					options: this.tabBar // tabbar 배치
				},
				cells:new Array().concat(tabBody) // tab body 배치
		};
		
	} // end setRender
	
	this.setWindow = function() {
		var _this = this;
		this.window = createWindow({
			id: this.id,
	    	width: this.width,
	    	height: this.height,
	    	header: {
	    		title: this.title
	    	},
	    	body: {
				rows: [{
					cols: [{
						rows: new Array().concat(this.tabGrid)
					}]
				}]
			},
			footer: {
	    		buttons: {
	    			align: "center",
	    			items: [
    		            createBtnComp({label: '적 용', type: "form", click: this.downExcelAssetList.bind(_this)})
    		        ]
	    		}
	    	},
			closable: true
		});
	} // end setWindow
	
	// 엑셀 다운로드
	this.downExcelAssetList = function() {
		var _this = this;
		var sheet = [];
		var tabArray = this.tabBar;
		var classTypeArr = [];
		
		for(var i = 0; i < tabArray.length; i++){
			var gridData = $$(tabArray[i].id+'_grid')._getSelectedRows();
			var title = tabArray[i].value;
			if(tabArray.length > 0 && gridData.length > 0){
				var headerNames = "";
				var includeColumns = "";
				var htmlTypes = "";
				
				// 자산 정보를 불러오는데 필요한 필수요소들은 유저가 선택하는게 아닌 무조건 들어가야 하는 정보이므로 소스코드에 세팅
				headerNames		+= "IT자산번호,구성ID,구성명,분류체계";
				includeColumns 	+= "ASSET_ID,CONF_ID,CONF_NM,PATH_CLASS_NM";
				htmlTypes 		+= "TEXT,TEXT,TEXT,TEXT";
				
				for ( var j = 0; j < gridData.length; j++) {
					headerNames 	+= ( "," + gridData[j].COMMENTS );
					includeColumns 	+= ( "," + gridData[j].COLUMN_NAME );
					htmlTypes 		+= ( "," + gridData[j].HTML_TYPE );
				}
				
				if(gridData.length != 0){
					classTypeArr.push(tabArray[i].CODE_ID);
					var sheetArr = { "sheetName": title, "titleName": title, "headerNames":headerNames, "includeColumns": includeColumns, "htmlTypes":htmlTypes };
					sheet.push(sheetArr);
				}
			}
			
		} // end for()
		
		var excelAttrs = {"sheet":sheet};
		var paramMap = this.popupParamData;
		paramMap['class_type_arr'] = classTypeArr;
		
		var getResultUrl = "/itg/itam/billing/searchExcel.do";
		
		var excelParam = {};
		excelParam['fileName'] = '과금대상자산목록';
		excelParam['excelAttrs'] = excelAttrs;
		goExcelDownLoad(getResultUrl, paramMap, excelParam);
		_this.window.close();
	}
		
}; // end nkia.ui.popup.itam.excel.ExcelByClassType


/**
 * 엑셀 다운로드 속성선택 팝업
 * @param {}
 */
function createExcelTabWindow(popupProps) {
	var props = popupProps||{};
	
	var popupObject;
	try {
		switch (props.type) {
			case "assetByClassType":	// 분류체계별 자산 엑셀 다운로드
				popupObject = new nkia.ui.popup.itam.excel.ExcelByClassTypeForBilling;
				popupObject.setProps(props);
				popupObject.setRender(props);
				popupObject.setWindow();
				break;
		}
		
		
	} catch(e) {
		alert(e.message);
	}
}


