
/**
 * 에디터폼 데이터 세팅
 */
function setFormDatas(parentConfId) {
	
	// 자산 마스터 정보 로드
	var form = $$("assetInfoForm");
	
	nkia.ui.utils.ajax({
		async: false,
		url: "/itg/itam/billing/searchAssetBillingMaster.do",
		params: {conf_id : parentConfId},
		isMask: false,
		notification: false,
		success: function(response) {
			if(response.gridVO && response.gridVO.rows) {
				var data = response.gridVO.rows;
				if(data.length > 0) {
					form._setValues(data[0]);
				}
			}
		}
	});
	
	// 자산 과금 정보 로드
	reloadBillingInfo(parentConfId);
}

/**
 * 과금 정보 로드
 */
function reloadBillingInfo(parentConfId) {
	
	var form = $$("svBillingForm");
	
	nkia.ui.utils.ajax({
		async: false,
		url: "/itg/itam/billing/searchAssetBillingSv.do",
		params: {conf_id : parentConfId},
		isMask: false,
		notification: false,
		success: function(response) {
			if(response.gridVO && response.gridVO.rows) {
				var data = response.gridVO.rows;
				if(data.length > 0) {
					form._setValues(data[0]);
				}
			}
		}
	});
}

/**
 * select박스 간의 연동 설정
 */
function setSelectBoxRelation() {
	
	var form = $$("svBillingForm");
	
	var billYn = form._getField("BILL_YN");
	var bsBgnDt = form._getField("BS_BGN_DT");
    var bsEndDt = form._getField("BS_END_DT");
    var svCostCls = form._getField("SV_COST_CLS");
	var svSe = form._getField("SV_SE");
	var svUse = form._getField("SV_USE");
	var svLv = form._getField("SV_LV");
	
	// 과금 여부가 '예'이면 전체 필드 필수값, '아니오' 혹은 빈 값 이면 필수값 없음
	billYn.attachEvent("onChange", function(newVal, oldVal) {
		if(newVal == "Y") {
			var objectArray = [bsBgnDt, bsEndDt, svCostCls, svSe, svUse, svLv];
			setFieldsRequired(objectArray, true);
		} else {
			var objectArray = [bsBgnDt, bsEndDt, svCostCls, svSe, svUse, svLv];
			setFieldsRequired(objectArray, false);
		}
	});
	
	svCostCls.attachEvent("onChange", function(newVal, oldVal) {
		
		svSe.setValue("");
		svUse.setValue("");
		svLv.setValue("");
		
		svSe._reload({code_grp_id : "SV_" + newVal + "_SE"});
		svUse._reload({code_grp_id : "SV_" + newVal + "_USE"});
		svLv._reload({code_grp_id : "SV_" + newVal + "_LV"});
		
		// 과금 분류가 '예'일 때, 
		if(billYn.getValue() == "Y") {
			if(newVal == "NEW" || newVal == "LEG" || newVal == "EXP") {
				// 과금분류가 '신규/기존/손해배상제외'일 경우만 서버종류, 용도, 서버등급은 필수
				var objectArray = [svSe, svUse, svLv];
				setFieldsRequired(objectArray, true);
			} else if(newVal == "TTE") {
				// 과금분류가 'CoLocation'일 경우 서버등급, 용도 필수 해제
				var objectArray = [svUse, svLv];
				setFieldsRequired(objectArray, false);
			} else {
				var objectArray = [svUse];
				setFieldsRequired(objectArray, false);
			}
		}
	});
}

/**
 * 필드 필수체크 핸들러
 */
function setFieldsRequired(objectArray, isRequired) {
	for(var i in objectArray) {
		objectArray[i].setValue("");
		objectArray[i].config.required = isRequired;
		objectArray[i].refresh();
	}
}

/**
 * 과금 시작일 종료일 벨리데이션
 */
function bsDateValid(state, editor, ignoreUpdate) {
	
	var rowId = editor.row;
	var columnId = editor.column;
	var oldValue = state.old;
	var newValue = state.value;
	
	var rows = this._getRows();
	
	if(columnId == "BS_BGN_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금시작일이 과금종료일보다 크거나 같을 경우 입력불가
		var clickRow = this.getItem(rowId);
		if(clickRow["BS_END_DT"]!="" && newValue.split("-").join("") >= clickRow["BS_END_DT"].split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금시작일은 과금종료일보다 같거나 클 수 없습니다."
			});
			clickRow[columnId] = oldValue;
			return false;
		}
	} else if(columnId == "BS_END_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금종료일이 과금시작일보다 작거나 같을 경우 입력불가
		var clickRow = this.getItem(rowId);
		if(clickRow["BS_BGN_DT"]!="" && newValue.split("-").join("") <= clickRow["BS_BGN_DT"].split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금종료일은 과금시작일보다 같거나 작을 수 없습니다."
			});
			clickRow[columnId] = oldValue;
			return false;
		}
	}
}

/**
 * 메모리 증설량 데이터 추가
 */
function addInsMemoryData() {
	$$("insMemoryGrid")._addRow();
}

/**
 * 메모리 증설량 데이터 삭제
 */
function removeInsMemoryData(e, row, html) {
	$$("insMemoryGrid")._removeRow();
}

/**
 * 메모리 증설량 데이터 리로드
 */
function reloadInsMemoryData() {
	
	var isEditFinish = $$("insMemoryGrid")._checkCellEditorState();
	
	if(!isEditFinish) {
		alert("입력 활성화 상태에서는 실행할 수 없습니다.");
		return false;
	}
	
	$$("insMemoryGrid")._reload();
}

/**
 * CPU Core 증설량 데이터 추가
 */
function addInsCpuData() {
	$$("insCpuGrid")._addRow();
}

/**
 * CPU Core 증설량 데이터 삭제
 */
function removeInsCpuData(e, row, html) {
	$$("insCpuGrid")._removeRow();
}

/**
 * CPU Core 증설량 데이터 리로드
 */
function reloadInsCpuData() {
	
	var isEditFinish = $$("insCpuGrid")._checkCellEditorState();
	
	if(!isEditFinish) {
		alert("입력 활성화 상태에서는 실행할 수 없습니다.");
		return false;
	}
	
	$$("insCpuGrid")._reload();
}

/**
 * Storage 증설량 데이터 삭제
 */
function removeInsStorageData(e, row, html) {
	$$("insStorageGrid")._removeRow();
}

/**
 * Storage 증설량 데이터 리로드
 */
function reloadInsStorageData() {
	
	var isEditFinish = $$("insStorageGrid")._checkCellEditorState();
	
	if(!isEditFinish) {
		alert("입력 활성화 상태에서는 실행할 수 없습니다.");
		return false;
	}
	
	$$("insStorageGrid")._reload();
}

/**
 * Storage 증설량 스토리지 자산 선택 팝업
 */
function createInsStorageAssetListPopup(parentGridId, parentConfId) {
	
	var parentRows = $$(parentGridId)._getRows();
	
	var _POPUP_ID = "InsStoragePop";
	var _SEARCH_FORM_ID = _POPUP_ID + "SearchForm";
	var _TREE_ID = _POPUP_ID + "Tree";
	var _MAIN_GRID_ID = _POPUP_ID + "MainGrid";
	
	var popupObj = null;
	
	var tree = createTreeComponent({
		id: _TREE_ID,
        url: '/itg/itam/opms/selectRelConfClassTree.do',
        filterable: true,
        expColable: true,
        params: {class_type: "ST", conf_id: parentConfId},
        expandLevel: 3,
        width: 250,
        header: {
        	title: "분류체계"
        },
        on: {
        	onItemClick: _clickTree
        }
    });
	
	var searchForm = createFormComp({
		id: _SEARCH_FORM_ID,
		elementsConfig: {
    		labelPosition: "left"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검 색", type: "form", click: _search }),
					createBtnComp({ label:"초기화", click: _searchClear })
				],
		        css: "webix_layout_form_bottom"
			}
		},
		fields: {
			colSize: 1,
			items: [
				{colspan: 1, item : createUnionFieldComp({
					items: [
							createCodeComboBoxComp({
								label: '조회구분',
                				name: 'search_type',
                				code_grp_id: 'SEARCH_ST_STATISTICS',
                				attachChoice: true,
                				width: 250
							}),
							createTextFieldComp({
								name: "search_value",
								placeholder: getConstText({isArgs : true, m_key : 'msg.placeholder.search'}),
								on:{
									onKeyPress: _searchByEnterKey
								}
							})
						]
					})
				},
				{colspan: 1, item: createCodeComboBoxComp({label: '자산상태', name: 'asset_state', attachAll: true, code_grp_id: 'ASSET_STATE', value:'ACTIVE'})}
			]
		}
	});
	
	var mainGrid = createGridComp({
		id : _MAIN_GRID_ID,
		pageable : true,
		pageSize : 10,
		checkbox: true,
		url : "/itg/itam/statistics/searchAssetList.do",
		resource : "grid.itam.confST",
		params : { class_type: "ST", exp_conf_id_list: "'"+parentConfId+"'", tangible_asset_yn: 'Y', asset_state: 'ACTIVE' },
		header : {
			title : "스토리지 장비 목록"
		}
	});
	
	var body = {
		rows: [{
			view:"accordion",
			multi:true,
			cols: [{
                header: "분류체계",
                body: {
                	rows: tree
                }
            },{
                rows: new Array().concat(searchForm, mainGrid)
            }]
		}]
	};
	
	popupObj = createWindow({
    	id: _POPUP_ID,
    	width: nkia.ui.utils.getWidePopupWidth() - 200,
    	height: 670,
    	header: {
    		title: "스토리지 장비 선택"
    	},
    	body: body,
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
		            createBtnComp({label: '선 택', type: "form", click: _choice})
		        ]
    		}
    	},
    	closable: true
    });
	
	$$(_POPUP_ID).show();
	
	// 트리 클릭
	function _clickTree(id, e) {
		var treeItem = $$(_TREE_ID).getItem(id);
		var searchParams = $$(_SEARCH_FORM_ID)._getValues();
		
		searchParams.class_id = treeItem.node_id;
		searchParams.class_type = "ST";
		searchParams.exp_conf_id_list = "'"+parentConfId+"'";
		searchParams.tangible_asset_yn = "Y";
		
		$$(_MAIN_GRID_ID)._reload(searchParams);
	}
	
	// 검색 이벤트
	function _search() {
		var searchParams = $$(_SEARCH_FORM_ID)._getValues();
		var treeParams = $$(_TREE_ID).getSelectedItem();

		if(typeof treeParams != "undefined") {
			searchParams.class_id = treeParams.node_id;
		}
		
		searchParams.class_type = "ST";
		searchParams.exp_conf_id_list = "'"+parentConfId+"'";
		searchParams.tangible_asset_yn = "Y";
		
		$$(_MAIN_GRID_ID)._reload(searchParams);
	}
	
	// 엔터키 검색 이벤트
	function _searchByEnterKey(keyCode) {
		if(keyCode){
			if(keyCode == "13"){
				_search();
			}
		}
	}
	
	// 검색창 초기화 이벤트
	function _searchClear() {
		$$(_SEARCH_FORM_ID)._reset();
	}
	
	// 값 선택 이벤트
	function _choice() {
		var selRows = $$(_MAIN_GRID_ID)._getSelectedRows();
		
		// 스토리지의 CONF_ID 값을 REL_CONF_ID로 치환하여 내려준다.
		for(var i=0; i<selRows.length; i++) {
			selRows[i]["REL_CONF_ID"] = selRows[i]["CONF_ID"];
			selRows[i]["RNUM"] = "";
			selRows[i]["BS_BGN_DT"] = "";
			selRows[i]["BS_END_DT"] = "";
			delete selRows[i]["CONF_ID"];
		}
		
		$$(parentGridId)._addRow(selRows);
        
        $$(_POPUP_ID).close();
	}
}

/**
 * 청구처별 사용량 - 서버비중 부서 선택 팝업
 */
function createCustRateSvPopup(parentGridId, parentConfId) {
	
	var parentRows = $$(parentGridId)._getRows();
	
	var _POPUP_ID = "custRateSvPop";
	var _TREE_ID = _POPUP_ID + "Tree";
	
	var popupObj = null;
	
	var tree = createTreeComponent({
		id: _TREE_ID,
        url: '/itg/itam/billing/searchDeptTree.do',
        filterable: true,
        expColable: true,
        checkbox: true,
        params: {},
        expandLevel: 3,
        header: {
        	title: "계열사/부서"
        }
    });
	
	popupObj = createWindow({
    	id: _POPUP_ID,
    	width: 400,
    	height: 670,
    	header: {
    		title: "계열사/부서 선택"
    	},
    	body: {
    		rows: tree
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
		            createBtnComp({label: '선 택', type : "form", click:_choice})
		        ]
    		}
    	},
    	closable: true
    });
	
	$$(_POPUP_ID).show();
	
	
	
	// 값 선택 이벤트
	function _choice() {
		
		var treeParams = $$(_TREE_ID)._getCheckedItems({leafOnly:true});
		
		if(treeParams.length < 1) {
			
			nkia.ui.utils.notification({
				type: "error",
				message: "선택된 계열사/부서 정보가 없습니다."
			});
			return false;
			
		} else {
			
			var selRows = new Array();
			
			for(var i=0; i<treeParams.length; i++) {
				
				var leaf = treeParams[i].leaf;
				if(leaf == false) {
					nkia.ui.utils.notification({
						type: "error",
						message: "최하위 계열사/부서 정보만 선택가능합니다."
					});
					return false;
				}
				
				var subCorpNm = treeParams[i].otherData.SUB_CORP_NM;
				var custId = treeParams[i].node_id;
				var custNm = treeParams[i].otherData.CUST_NM;
				if(custNm == null || custNm == "null") {
					custNm = "";
				}
				
				selRows.push({
					SUB_CORP_NM : subCorpNm,
					CUST_NM : custNm,
					RATE : "",
					SERVICE_NM : "",
					SEL_SERVICE_BTN : "",
					BS_BGN_DT : "",
					BS_END_DT : "",
					CUST_ID : custId,
					SERVICE_ID : "",
					CONF_ID : parentConfId,
					CONF_TYPE : "SV"
				});
			}
			
			$$(parentGridId)._addRow(selRows);
			
			$$(_POPUP_ID).close();
		}
	}
}

/**
 * 청구처별 사용량 - 서버비중 업무 선택 팝업
 */
function createServiceSvPopup(parentGridId, parentConfId, parentRowId) {
	
	var _POPUP_ID = "serviceSvPop";
	var _GRID_ID = _POPUP_ID + "Grid";
	var _SEARCH_FORM_ID = _POPUP_ID + "SearchForm";
	
	var popupObj = null;
	
	var parentSelRow = $$(parentGridId).getItem(parentRowId);
	
	var searchForm = createFormComp({
		id: _SEARCH_FORM_ID,
		elementsConfig: {
    		labelPosition: "left"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검 색", type: "form", click: _search }),
					createBtnComp({ label:"초기화", click: _searchClear })
				],
		        css: "webix_layout_form_bottom"
			}
		},
		fields: {
			colSize: 1,
			items: [
				{colspan: 1, item: createTextFieldComp({label: '업무명', name: 'service_nm', on:{onKeyPress: _searchByEnterKey} })}
			]
		}
	});
	
	var grid = createGridComp({
		id : _GRID_ID,
		pageable : false,
		url : "/itg/itam/billing/searchService.do",
		resource : "grid.itam.billing.custrate.service",
		params : {cust_id: parentSelRow["CUST_ID"]},
		header : {
			title : "업무 목록"
		},
		on : {
			onItemDblClick: function(rowId) {
				var row = this.getItem(rowId);
					
				parentSelRow["SERVICE_ID"] = row["SERVICE_ID"];
				parentSelRow["SERVICE_NM"] = row["SERVICE_NM"];
				
				$$(parentGridId).updateItem(parentRowId, parentSelRow);
				
				popupObj.close();
			}
		}
	});
	
	popupObj = createWindow({
    	id: _POPUP_ID,
    	width: 600,
    	height: 670,
    	header: {
    		title: "업무 선택"
    	},
    	body: {
    		rows: new Array().concat(searchForm, grid)
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
		            createBtnComp({label: '선 택', type : "form", click:_choice})
		        ]
    		}
    	},
    	closable: true
    });
	
	$$(_POPUP_ID).show();
	
	
	
	// 검색 이벤트
	function _search() {
		var searchParams = $$(_SEARCH_FORM_ID)._getValues();
		$$(_GRID_ID)._reload(searchParams);
	}
	
	// 엔터키 검색 이벤트
	function _searchByEnterKey(keyCode) {
		if(keyCode){
			if(keyCode == "13"){
				_search();
			}
		}
	}
	
	// 검색창 초기화 이벤트
	function _searchClear() {
		$$(_SEARCH_FORM_ID)._reset();
	}
	
	// 값 선택 이벤트
	function _choice() {
		
		var row = $$(_GRID_ID)._getSelectedRows();
		
		if(row.length < 1) {
			nkia.ui.utils.notification({
				type: "error",
				message: "선택 된 업무가 없습니다."
			});
			return false;
		} else {
			var parentSelRow = $$(parentGridId).getItem(parentRowId);
			parentSelRow["SERVICE_ID"] = row[0]["SERVICE_ID"];
			parentSelRow["SERVICE_NM"] = row[0]["SERVICE_NM"];
			
			$$(parentGridId).updateItem(parentRowId, parentSelRow);
			
			$$(_POPUP_ID).close();
		}
	}
}

/**
 * 청구처별 사용량 - 서버비중 업무 데이터 삭제
 */
function removeServiceSvData(e, row, html) {
	$$("custRateSvGrid")._removeRow();
}

/**
 * 청구처별 사용량 - 서버비중 데이터 입력 벨리데이션
 */
function custRateSvEditorChange(state, editor, ignoreUpdate) {
	
	var rowId = editor.row;
	var columnId = editor.column;
	var oldValue = state.old;
	var newValue = state.value;
	
	var rows = this._getRows();
	
	if(columnId == "BS_BGN_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금시작일이 과금종료일보다 크거나 같을 경우 입력불가
		var targetColumn = rows[0]["BS_END_DT"];
		if(targetColumn!="" && newValue.split("-").join("") >= targetColumn.split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금시작일은 과금종료일보다 같거나 클 수 없습니다."
			});
			var clickRow = this.getItem(rowId);
			clickRow[columnId] = oldValue;
			return false;
		}
		// 과금시작일 변경 시, 모든 Row의 과금시작일 일괄 변경
		for(var i=0; i<rows.length; i++) {
			rows[i][columnId] = newValue;
			this.updateItem(rows[i].id, rows[i]);
			this.refresh();
		}
	} else if(columnId == "BS_END_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금종료일이 과금시작일보다 작거나 같을 경우 입력불가
		var targetColumn = rows[0]["BS_BGN_DT"];
		if(targetColumn!="" && newValue.split("-").join("") <= targetColumn.split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금종료일은 과금시작일보다 같거나 작을 수 없습니다."
			});
			var clickRow = this.getItem(rowId);
			clickRow[columnId] = oldValue;
			return false;
		}
		// 과금종료일 변경 시, 모든 Row의 과금종료일 일괄 변경
		for(var i=0; i<rows.length; i++) {
			rows[i][columnId] = newValue;
			this.updateItem(rows[i].id, rows[i]);
			this.refresh();
		}
	} else if(columnId == "RATE") {
		// 100 이상 입력 못하도록
		if(newValue > 100) {
			nkia.ui.utils.notification({
				type: "error",
				message: "비중을 100 이상 입력할 수 없습니다."
			});
			var clickRow = this.getItem(rowId);
			clickRow[columnId] = oldValue;
			return false;
		}
		// 비중의 합이 100을 넘기지 못하도록
		var summary = 0;
		for(var i=0; i<rows.length; i++) {
			if(rows[i][columnId]!="") {
				summary = summary + (rows[i][columnId]*1);
			}
			if(summary > 100) {
				nkia.ui.utils.notification({
					type: "error",
					message: "비중의 합은 100을 초과할 수 없습니다."
				});
				var clickRow = this.getItem(rowId);
				clickRow[columnId] = oldValue;
				return false;
			}
		}
	}
}

/**
 * 청구처별 사용량 - 서버비중 업무 데이터 리로드
 */
function reloadServiceSvData(parentConfId) {
	
	var isEditFinish = $$("custRateSvGrid")._checkCellEditorState();
	
	if(!isEditFinish) {
		alert("입력 활성화 상태에서는 실행할 수 없습니다.");
		return false;
	}
	
	$$("custRateSvGrid")._reload({conf_id : parentConfId, conf_type : "SV"});
}




/**
 * 청구처별 사용량 - 스토리지비중 부서 선택 팝업
 */
function createCustRateStPopup(parentGridId, parentConfId) {
	
	var parentRows = $$(parentGridId)._getRows();
	
	var _POPUP_ID = "custRateStPop";
	var _TREE_ID = _POPUP_ID + "Tree";
	
	var popupObj = null;
	
	var tree = createTreeComponent({
		id: _TREE_ID,
        url: '/itg/itam/billing/searchDeptTree.do',
        filterable: true,
        expColable: true,
        checkbox: true,
        params: {},
        expandLevel: 3,
        header: {
        	title: "계열사/부서"
        }
    });
	
	popupObj = createWindow({
    	id: _POPUP_ID,
    	width: 400,
    	height: 670,
    	header: {
    		title: "계열사/부서 선택"
    	},
    	body: {
    		rows: tree
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
		            createBtnComp({label: '선 택', type : "form", click:_choice})
		        ]
    		}
    	},
    	closable: true
    });
	
	$$(_POPUP_ID).show();
	
	
	
	// 값 선택 이벤트
	function _choice() {
		
		var treeParams = $$(_TREE_ID)._getCheckedItems({leafOnly:true});
		
		if(treeParams.length < 1) {
			
			nkia.ui.utils.notification({
				type: "error",
				message: "선택된 계열사/부서 정보가 없습니다."
			});
			return false;
			
		} else {
			
			var selRows = new Array();
			
			for(var i=0; i<treeParams.length; i++) {
				
				var leaf = treeParams[i].leaf;
				if(leaf == false) {
					nkia.ui.utils.notification({
						type: "error",
						message: "최하위 계열사/부서 정보만 선택가능합니다."
					});
					return false;
				}
				
				var subCorpNm = treeParams[i].otherData.SUB_CORP_NM;
				var custId = treeParams[i].node_id;
				var custNm = treeParams[i].otherData.CUST_NM;
				if(custNm == null || custNm == "null") {
					custNm = "";
				}
				
				selRows.push({
					SUB_CORP_NM : subCorpNm,
					CUST_NM : custNm,
					RATE : "",
					SERVICE_NM : "",
					SEL_SERVICE_BTN : "",
					BS_BGN_DT : "",
					BS_END_DT : "",
					CUST_ID : custId,
					SERVICE_ID : "",
					CONF_ID : parentConfId,
					CONF_TYPE : "ST"
				});
			}
			
			$$(parentGridId)._addRow(selRows);
			
			$$(_POPUP_ID).close();
		}
	}
}

/**
 * 청구처별 사용량 - 스토리지비중 업무 선택 팝업
 */
function createServiceStPopup(parentGridId, parentConfId, parentRowId) {
	
	var _POPUP_ID = "serviceSvPop";
	var _GRID_ID = _POPUP_ID + "Grid";
	var _SEARCH_FORM_ID = _POPUP_ID + "SearchForm";
	
	var popupObj = null;
	
	var searchForm = createFormComp({
		id: _SEARCH_FORM_ID,
		elementsConfig: {
    		labelPosition: "left"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검 색", type: "form", click: _search }),
					createBtnComp({ label:"초기화", click: _searchClear })
				],
		        css: "webix_layout_form_bottom"
			}
		},
		fields: {
			colSize: 1,
			items: [
				{colspan: 1, item: createTextFieldComp({label: '업무명', name: 'service_nm', on:{onKeyPress: _searchByEnterKey} })}
			]
		}
	});
	
	var grid = createGridComp({
		id : _GRID_ID,
		pageable : false,
		url : "/itg/itam/billing/searchService.do",
		resource : "grid.itam.billing.custrate.service",
		params : {},
		header : {
			title : "업무 목록"
		},
		on : {
			onItemDblClick: function(rowId) {
				var row = this.getItem(rowId);
					
				var parentSelRow = $$(parentGridId).getItem(parentRowId);
				parentSelRow["SERVICE_ID"] = row["SERVICE_ID"];
				parentSelRow["SERVICE_NM"] = row["SERVICE_NM"];
				
				$$(parentGridId).updateItem(parentRowId, parentSelRow);
				
				popupObj.close();
			}
		}
	});
	
	popupObj = createWindow({
    	id: _POPUP_ID,
    	width: 600,
    	height: 670,
    	header: {
    		title: "업무 선택"
    	},
    	body: {
    		rows: new Array().concat(searchForm, grid)
    	},
    	footer: {
    		buttons: {
    			align: "center",
    			items: [
		            createBtnComp({label: '선 택', type : "form", click:_choice})
		        ]
    		}
    	},
    	closable: true
    });
	
	$$(_POPUP_ID).show();
	
	
	
	// 검색 이벤트
	function _search() {
		var searchParams = $$(_SEARCH_FORM_ID)._getValues();
		$$(_GRID_ID)._reload(searchParams);
	}
	
	// 엔터키 검색 이벤트
	function _searchByEnterKey(keyCode) {
		if(keyCode){
			if(keyCode == "13"){
				_search();
			}
		}
	}
	
	// 검색창 초기화 이벤트
	function _searchClear() {
		$$(_SEARCH_FORM_ID)._reset();
	}
	
	// 값 선택 이벤트
	function _choice() {
		
		var row = $$(_GRID_ID)._getSelectedRows();
		
		if(row.length < 1) {
			nkia.ui.utils.notification({
				type: "error",
				message: "선택 된 업무가 없습니다."
			});
			return false;
		} else {
			var parentSelRow = $$(parentGridId).getItem(parentRowId);
			parentSelRow["SERVICE_ID"] = row[0]["SERVICE_ID"];
			parentSelRow["SERVICE_NM"] = row[0]["SERVICE_NM"];
			
			$$(parentGridId).updateItem(parentRowId, parentSelRow);
			
			$$(_POPUP_ID).close();
		}
	}
}

/**
 * 청구처별 사용량 - 스토리지비중 업무 데이터 삭제
 */
function removeServiceStData(e, row, html) {
	$$("custRateStGrid")._removeRow();
}

/**
 * 청구처별 사용량 - 스토리지비중 데이터 입력 벨리데이션
 */
function custRateStEditorChange(state, editor, ignoreUpdate) {
	
	var rowId = editor.row;
	var columnId = editor.column;
	var oldValue = state.old;
	var newValue = state.value;
	
	var rows = this._getRows();
	
	if(columnId == "BS_BGN_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금시작일이 과금종료일보다 크거나 같을 경우 입력불가
		var targetColumn = rows[0]["BS_END_DT"];
		if(targetColumn!="" && newValue.split("-").join("") >= targetColumn.split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금시작일은 과금종료일보다 같거나 클 수 없습니다."
			});
			var clickRow = this.getItem(rowId);
			clickRow[columnId] = oldValue;
			return false;
		}
		// 과금시작일 변경 시, 모든 Row의 과금시작일 일괄 변경
		for(var i=0; i<rows.length; i++) {
			rows[i][columnId] = newValue;
			this.updateItem(rows[i].id, rows[i]);
			this.refresh();
		}
	} else if(columnId == "BS_END_DT") {
		var format = webix.Date.dateToStr("%Y-%m-%d");
		newValue = format(newValue);
		// 과금종료일이 과금시작일보다 작거나 같을 경우 입력불가
		var targetColumn = rows[0]["BS_BGN_DT"];
		if(targetColumn!="" && newValue.split("-").join("") <= targetColumn.split("-").join("")) {
			nkia.ui.utils.notification({
				type: "error",
				message: "과금종료일은 과금시작일보다 같거나 작을 수 없습니다."
			});
			var clickRow = this.getItem(rowId);
			clickRow[columnId] = oldValue;
			return false;
		}
		// 과금종료일 변경 시, 모든 Row의 과금종료일 일괄 변경
		for(var i=0; i<rows.length; i++) {
			rows[i][columnId] = newValue;
			this.updateItem(rows[i].id, rows[i]);
			this.refresh();
		}
	} else if(columnId == "RATE") {
		// 100 이상 입력 못하도록
		if(newValue > 100) {
			nkia.ui.utils.notification({
				type: "error",
				message: "비중을 100 이상 입력할 수 없습니다."
			});
			var clickRow = this.getItem(rowId);
			clickRow[columnId] = oldValue;
			return false;
		}
		// 비중의 합이 100을 넘기지 못하도록
		var summary = 0;
		for(var i=0; i<rows.length; i++) {
			if(rows[i][columnId]!="") {
				summary = summary + (rows[i][columnId]*1);
			}
			if(summary > 100) {
				nkia.ui.utils.notification({
					type: "error",
					message: "비중의 합은 100을 초과할 수 없습니다."
				});
				var clickRow = this.getItem(rowId);
				clickRow[columnId] = oldValue;
				return false;
			}
		}
	}
}

/**
 * 청구처별 사용량 - 스토리지비중 업무 데이터 리로드
 */
function reloadServiceStData(parentConfId) {
	
	var isEditFinish = $$("custRateStGrid")._checkCellEditorState();
	
	if(!isEditFinish) {
		alert("입력 활성화 상태에서는 실행할 수 없습니다.");
		return false;
	}
	
	$$("custRateStGrid")._reload({conf_id : parentConfId, conf_type : "ST"});
}






/**
 * 저장 이벤트
 */
function saveAssetBilling(parentConfId) {
	
	var form = $$("svBillingForm");
	var insMemoryGrid = $$("insMemoryGrid");
	var insCpuGrid = $$("insCpuGrid");
	var insStorageGrid = $$("insStorageGrid");
	var custRateSvGrid = $$("custRateSvGrid");
	var custRateStGrid = $$("custRateStGrid");
	
	// 벨리데이션 체크
	if( form._validate() ) {
		
		var isValid = true;
		var vaildMsg = "";
		
		// 데이터 가공 : 아래 변수에 정의된 KEY 들만 데이터로 만들어준다. * 컬럼명 앞,뒤에 골뱅이(@) 붙여줄것
		var _MEM_HEADER = "@BS_INDX@COST_CLS@MEM_SIZE@UNIT_INSTALL_DT@BS_BGN_DT@BS_END_DT@";
		var _CPU_HEADER = "@BS_INDX@COST_CLS@CORE_CNT@UNIT_INSTALL_DT@BS_BGN_DT@BS_END_DT@";
		var _ST_HEADER = "@BS_INDX@COST_CLS@REL_CONF_ID@RAID_SE@DISK_SIZE@UNIT_INSTALL_DT@BS_BGN_DT@BS_END_DT@";
		var _CUSTRATE_SV_HEADER = "@BS_INDX@RATE@BS_BGN_DT@BS_END_DT@CUST_ID@SERVICE_ID@";
		var _CUSTRATE_ST_HEADER = "@BS_INDX@RATE@BS_BGN_DT@BS_END_DT@CUST_ID@SERVICE_ID@";
		
		var formData = form._getValues();
		formData["CONF_ID"] = parentConfId;
		
		var format = webix.Date.dateToStr("%Y-%m-%d");
		var bsBgnDt = format(formData["BS_BGN_DT"]);
		var bsEndDt = format(formData["BS_END_DT"]);
		
		if(bsBgnDt!="" && bsEndDt!="") {
			if(bsBgnDt.split("-").join("") >= bsEndDt.split("-").join("")) {
				nkia.ui.utils.notification({
					type: "error",
					message: "과금정보 입력폼의 과금시작일은 과금종료일보다 같거나 클 수 없습니다."
				});
				return false;
			}
		}
		
		var memData = [];
		var cpuData = [];
		var stData = [];
		var custSvData = [];
		var custStData = [];
		
		// 메모리 증설량 영역 Start ************************************************************************
		insMemoryGrid.eachRow(function(row) {
			var myRow = insMemoryGrid.getItem(row);
			var newRow = {};
			insMemoryGrid.eachColumn(function(columnid) {
				if(_MEM_HEADER.indexOf("@"+columnid+"@") > -1) {
					if(columnid!="BS_INDX" && (typeof myRow[columnid]=="undefined" || (myRow[columnid]+"")=="" || myRow[columnid]==null)) {
						isValid = false;
					} else {
						newRow[columnid] = myRow[columnid];
						if(typeof newRow[columnid]=="undefined") {
							newRow[columnid] = "";
						}
					}
				}
			}, true);
			newRow["CONF_ID"] = parentConfId;
			memData.push(newRow);
		});
		
		if(!isValid) {
			nkia.ui.utils.notification({
				type: "error",
				message: "[메모리 증설량]의 입력항목은 모두 필수입니다."
			});
			formData = {};
			memData = [];
			cpuData = [];
			stData = [];
			custSvData = [];
			custStData = [];
			isValid = true;
			return false;
		}
		// 메모리 증설량 영역 End ************************************************************************
		
		// CPU Core 증설량 영역 Start ************************************************************************
		insCpuGrid.eachRow(function(row) {
			var myRow = insCpuGrid.getItem(row);
			var newRow = {};
			insCpuGrid.eachColumn(function(columnid) {
				if(_CPU_HEADER.indexOf("@"+columnid+"@") > -1) {
					if(columnid!="BS_INDX" && (typeof myRow[columnid]=="undefined" || (myRow[columnid]+"")=="" || myRow[columnid]==null)) {
						isValid = false;
					} else {
						newRow[columnid] = myRow[columnid];
						if(typeof newRow[columnid]=="undefined") {
							newRow[columnid] = "";
						}
					}
				}
			}, true);
			newRow["CONF_ID"] = parentConfId;
			cpuData.push(newRow);
		});
		
		if(!isValid) {
			nkia.ui.utils.notification({
				type: "error",
				message: "[CPU Core 증설량]의 입력항목은 모두 필수입니다."
			});
			formData = {};
			memData = [];
			cpuData = [];
			stData = [];
			custSvData = [];
			custStData = [];
			isValid = true;
			return false;
		}
		// CPU Core 증설량 영역 End ************************************************************************
		
		// 스토리지 증설량 영역 Start ************************************************************************
		insStorageGrid.eachRow(function(row) {
			var myRow = insStorageGrid.getItem(row);
			var newRow = {};
			insStorageGrid.eachColumn(function(columnid) {
				if(_ST_HEADER.indexOf("@"+columnid+"@") > -1) {
					if(columnid!="BS_INDX" && (typeof myRow[columnid]=="undefined" || (myRow[columnid]+"")=="" || myRow[columnid]==null)) {
						isValid = false;
					} else {
						newRow[columnid] = myRow[columnid];
						if(typeof newRow[columnid]=="undefined") {
							newRow[columnid] = "";
						}
					}
				}
			}, true);
			newRow["CONF_ID"] = parentConfId;
			stData.push(newRow);
		});
		
		if(!isValid) {
			nkia.ui.utils.notification({
				type: "error",
				message: "[스토리지 증설량]의 입력항목은 모두 필수입니다."
			});
			formData = {};
			memData = [];
			cpuData = [];
			stData = [];
			custSvData = [];
			custStData = [];
			isValid = true;
			return false;
		}
		// 스토리지 증설량 영역 End ************************************************************************
		
		// 청구처별 사용량 [서버] 영역 Start ************************************************************************
		var svSummary = 0;
		custRateSvGrid.eachRow(function(row) {
			var myRow = custRateSvGrid.getItem(row);
			var newRow = {};
			custRateSvGrid.eachColumn(function(columnid) {
				if(columnid == "RATE") {
					if(myRow[columnid]!="") {
						svSummary = svSummary + (myRow[columnid]*1);
					}
				}
				if(_CUSTRATE_SV_HEADER.indexOf("@"+columnid+"@") > -1) {
					if(columnid!="BS_INDX" && (typeof myRow[columnid]=="undefined" || (myRow[columnid]+"")=="" || myRow[columnid]==null)) {
						validMsg = "[청구처별 사용량-서버비중]의 입력항목은 모두 필수입니다.";
						isValid = false;
					} else {
						newRow[columnid] = myRow[columnid];
						if(typeof newRow[columnid]=="undefined") {
							newRow[columnid] = "";
						}
					}
				}
			}, true);
			newRow["CONF_ID"] = parentConfId;
			newRow["CONF_TYPE"] = "SV";
			custSvData.push(newRow);
		});
		
		if(custRateSvGrid._getRows().length > 0 && (svSummary*1) != 100) {
			validMsg = "[청구처별 사용량-서버비중]의 비중의 합이 100이어야 합니다.";
			isValid = false;
		}
		
		if(!isValid) {
			nkia.ui.utils.notification({
				type: "error",
				message: validMsg
			});
			formData = {};
			memData = [];
			cpuData = [];
			stData = [];
			custSvData = [];
			custStData = [];
			isValid = true;
			return false;
		}
		// 청구처별 사용량 [서버] 영역 End ************************************************************************
		
		// 청구처별 사용량 [스토리지] 영역 Start ************************************************************************
		var stSummary = 0;
		custRateStGrid.eachRow(function(row) {
			var myRow = custRateStGrid.getItem(row);
			var newRow = {};
			custRateStGrid.eachColumn(function(columnid) {
				if(columnid == "RATE") {
					if(myRow[columnid]!="") {
						stSummary = stSummary + (myRow[columnid]*1);
					}
				}
				if(_CUSTRATE_ST_HEADER.indexOf("@"+columnid+"@") > -1) {
					if(columnid!="BS_INDX" && (typeof myRow[columnid]=="undefined" || (myRow[columnid]+"")=="" || myRow[columnid]==null)) {
						validMsg = "[청구처별 사용량-스토리지비중]의 입력항목은 모두 필수입니다.";
						isValid = false;
					} else {
						newRow[columnid] = myRow[columnid];
						if(typeof newRow[columnid]=="undefined") {
							newRow[columnid] = "";
						}
					}
				}
			}, true);
			newRow["CONF_ID"] = parentConfId;
			newRow["CONF_TYPE"] = "ST";
			custStData.push(newRow);
		});
		
		if(custRateStGrid._getRows().length > 0 && (stSummary*1) != 100) {
			validMsg = "[청구처별 사용량-스토리지비중]의 비중의 합이 100이어야 합니다.";
			isValid = false;
		}
		
		if(!isValid) {
			nkia.ui.utils.notification({
				type: "error",
				message: validMsg
			});
			formData = {};
			memData = [];
			cpuData = [];
			stData = [];
			custSvData = [];
			custStData = [];
			isValid = true;
			return false;
		}
		// 청구처별 사용량 [스토리지] 영역 End ************************************************************************
		
		// 그리드 입력 활성화 상태 여부 체크
		if(!(
				insMemoryGrid._checkCellEditorState()
				&& insCpuGrid._checkCellEditorState()
				&& insStorageGrid._checkCellEditorState()
				&& custRateSvGrid._checkCellEditorState()
				&& custRateStGrid._checkCellEditorState()
		)) {
			alert("입력 활성화 상태에서는 실행할 수 없습니다.");
			return false;
		}
		
		// 등록여부 확인
		if(!confirm("저장하시겠습니까?")) {
			return false;
		}
		
		var params = {};
		params["parent_conf_id"] = parentConfId;
		params["FORM"] = formData;
		params["BS_INS_MEMORY"] = memData;
		params["BS_INS_CPU"] = cpuData;
		params["BS_INS_STORAGE"] = stData;
		params["CUST_RATE_SV"] = custSvData;
		params["CUST_RATE_ST"] = custStData;
		
		nkia.ui.utils.ajax({
			viewId: 'assetBillingDetailViewer',
			url: "/itg/itam/billing/changeAssetBillingSvOut.do",
			params: params,
			async: false,
			isMask: true,
			notification: true,
			success: function(response){
				alert("저장되었습니다.");
				parent.reloadList();
				parent.closeTab();
			}
		});
		
	}
}

/**
 * 과금 정보 이력 조회 팝업
 */
function openBsFormHisPop(_ASSET_ID, _CONF_ID) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsSvList.do",
		resource: "grid.itam.bs.sv.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}

/**
 * 메모리증설량 이력 조회 팝업
 */
function openBsMemoryHisPop(_ASSET_ID, _CONF_ID) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsInsMemoryList.do",
		resource: "grid.itam.bs.mem.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}

/**
 * CPU증설량 이력 조회 팝업
 */
function openBsCpuHisPop(_ASSET_ID, _CONF_ID) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsInsCpuList.do",
		resource: "grid.itam.bs.cpu.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}

/**
 * 스토리지증설량 이력 조회 팝업
 */
function openBsStorageHisPop(_ASSET_ID, _CONF_ID) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsInsStorageList.do",
		resource: "grid.itam.bs.storage.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, historyPopGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}

/**
 * 청구처 이력 조회 팝업
 */
function openBsCustRateHisPop(_ASSET_ID, _CONF_ID, _CLASS_TYPE) {
	
	// 검색용 날짜객체 (최근 30일)
	var start_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:-30, hour:0, min:0});
	var end_date_obj = createDateTimeStamp('DHM', {year:0, month:0, day:0, hour:0, min:0});
	
	var historyPopSearchForm = createFormComp({
		id: "historyPopSearchForm",
		elementsConfig: {
    		labelPosition: "top"
    	},
		header: {
			title: "검색",
			icon: "search"
		},
		footer: {
			buttons: {
				align: "center",
				items: [
					createBtnComp({ label:"검색", type: "form", click: searchHistoryPop }),
					createBtnComp({ label:"초기화", click: initHistoryPop })
				]
			}
		},
		fields: {
			colSize: 1,
			items: [
			    {colspan: 1, item: createSearchDateFieldComp({label:"수정일", name: "updDt", dateType:"D", start_dateValue : start_date_obj, end_dateValue: end_date_obj})}
			]
		}
	});
	
	var changePopGrid = createGridComp({
		id : "changePopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	CONF_TYPE : _CLASS_TYPE,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsBsCustRateList.do",
		resource: "grid.itam.bs.custrate.change",
		header : {
			title : "과금 변경 정보"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	var historyPopGrid = createGridComp({
		id : "historyPopGrid",
		resizeColumn : true,
		select : false,
		params:  {
        	ASSET_ID : _ASSET_ID,
        	CONF_ID : _CONF_ID,
        	CONF_TYPE : _CLASS_TYPE,
        	updDt_startDate : start_date_obj.day,
        	updDt_endDate : end_date_obj.day
		},
		url : "/itg/itam/assetHistory/selectOpmsHisBsCustRateList.do",
		resource: "grid.itam.bs.custrate.his",
		header : {
			title : "수정이력"
		},
		cellMerges: ["INS_DT", "INS_USER_NM"]
	});
	
	
	var tabBarOptionsArray = [{
		id:"changePopGridTab",
//		value:"과금 변경 정보 <span id='changePopGrid_badge' class='webix_badge'>0</span>" 뱃지 안먹음....
		value:"과금 변경 정보"
	},{
		id:"historyPopGridTab",
//		value:"수정이력 <span id='historyPopGrid_badge' class='webix_badge'>0</span>" 뱃지 안먹음....
		value:"수정이력"
	}];
	
	var tabGridArray = [{
		id:"changePopGridTab",
		rows: changePopGrid
	},{
		id:"historyPopGridTab",
		rows: historyPopGrid
	}];
	
	var tabGrid = {
		view:"tabview",
		id:"custRateHisTab",
		animate:false,
		tabbar: {
			options: tabBarOptionsArray
		},
		cells: new Array().concat(tabGridArray)	
	};
	
	nkia.ui.utils.window({
		id:"historyPopup",
		callFunc: function() {
			this.window = createWindow({
		    	id: "historyPopup",
		    	width: 900,
		    	height: nkia.ui.utils.getWidePopupHeight(),
		    	header: {
		    		title: "수정이력"
		    	},
		    	body: {
		    		rows: new Array().concat(historyPopSearchForm, tabGrid)
		    	},
		    	closable: true
		    })
		}
	});
	
	function searchHistoryPop() {
		var searchParam = $$("historyPopSearchForm")._getValues();
		searchParam["ASSET_ID"] = _ASSET_ID;
		searchParam["CONF_ID"] = _CONF_ID;
		searchParam["CONF_TYPE"] = _CLASS_TYPE;
		$$("historyPopGrid")._reload(searchParam);
	}
	
	function initHistoryPop() {
		$$("historyPopSearchForm")._reset();
	}
	
}
